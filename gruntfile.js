/**
 * Aislend Demo
 */
module.exports = function (grunt) {
    require("load-grunt-tasks")(grunt);
    const sass = require('node-sass');
    // Project Config
    grunt.initConfig({
        // Package File
        pkg: grunt.file.readJSON("package.json"),

        /* ======================================================================== */
        /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
        /* ======================================================================== */

        /**
         * Uglify JS script
         * */
        uglify: {
            // Production config
            prod: {
                files: [{
                    expand: true,
                    cwd: "public/src/js",
                    src: ["*.js", "!*.min.js"],
                    dest: "public/dist/js/",
                    ext: ".min.js"
                }]
            },

            // Development config
            dev: {
                options: {
                    compress: false,
                    beautify: true,
                    mangle: false,
                    output: {
                        comments: 'all'
                    }
                },

                files: [{
                    expand: true,
                    cwd: "public/src/js",
                    src: ["*.js", "!*.min.js"],
                    dest: "public/dist/js/",
                    ext: ".min.js"
                }]
            }
        },

        /* ======================================================================== */
        /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
        /* ======================================================================== */

        /**
         * sass task
         * */
        sass: {
            options: {
                implementation: sass,
                sourceMap: true,
                outputStyle: "expanded"
            },
            dist: {
                files: {
                    "public/dist/css/style.min.css": "public/src/scss/style.scss"
                }
            }
        },

        /* ======================================================================== */
        /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
        /* ======================================================================== */

        /**
         *  Watch files for changes
         * */
        watch: {
            scripts: {
                files: ["public/src/js/*.js"],
                tasks: ["uglify:dev"]
            },
            styles: {
                files: ["public/src/scss/**/*.scss"],
                tasks: ["sass"]
            }
        },

        /**
         * Apply vendor prefixes
         * */
        postcss: {
            options: {
                processors: [
                    require("autoprefixer")({
                        browsers: "last 2 versions"
                    }) // add vendor prefixes
                ]
            },
            dist: {
                files: {
                    "public/dist/css/style.min.css": "public/dist/css/style.min.css"
                }
            }
        },

        /**
         * combine media queries
         * */
        combine_mq: {
            options: {
                beautify: false
            },
            dist: {
                files: {
                    "public/dist/css/style.min.css": "public/dist/css/style.min.css"
                }
            }
        },

        /**
         * Browser sync
         * */
        browserSync: {
            bsFiles: {
                src: ["public/dist/css/*.css", "public/dist/js/*.js", "public/index.php"]
            },
            options: {
                server: {
                    baseDir: "./"
                },
                watchTask: true
            }
        }
    });

    /* ======================================================================== */
    /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */
    /* ======================================================================== */

    // Uglify
    grunt.loadNpmTasks("grunt-contrib-uglify");

    // Sass
    grunt.loadNpmTasks("grunt-sass");

    // Watch changes
    grunt.loadNpmTasks("grunt-contrib-watch");

    // postcss task
    grunt.loadNpmTasks("grunt-postcss");

    // combine media queries
    grunt.loadNpmTasks("grunt-combine-mq");

    // Browser sync
    grunt.loadNpmTasks("grunt-browser-sync");

    // Register Tasks
    grunt.registerTask("default", [
        "sass",
        "browserSync",
        "watch"
    ]);

    // production mode
    grunt.registerTask("prod", [
        "sass",
        "postcss",
        "combine_mq",
        "uglify:prod"
    ]);
};
