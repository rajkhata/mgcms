<?php

namespace App\Repositories;
use App\Models\SmsContent;
use App\Models\MailTemplate;

use App\Mail\NotifyMail;
use App\Jobs\NotifySms;
use App\Models\Restaurant;


use Illuminate\Http\Request;
use Mail;

class Notification 
{


  



/*
Notify method 
@params  $params


        $params=[
            'customer'=>[
               'sms'=>[
                     'template'=>'res_sm',
                     'data'=>'datahere in array',
                     'mobile'=>'mobile'
                  ],
                'mail'=>[
                     'template'=>'res_sm',
                     'email'=>'email',
                     'data'=>'datahere in array'
                  ]  
              ],
              'manager'=>[
               'sms'=>[
                     'template'=>'res_sm',
                     'data'=>'datahere in array',
                        'mobile'=>'mobile'

                  ],
                'mail'=>[
                     'template'=>'res_sm',
                     'email'=>'email',
                     'data'=>'datahere in array'
                  ]  
              ]

            ];


*/

  public function notifiy(array $params,$restaurant_id){
        //echo '<pre>';
        //print_r($params);
     
     foreach($params as $user_type=>$user){
         foreach($user as $key=> $data){
             if($key=='sms'){
               if(isset($data['template']) && !empty($data['template']) && !empty($data['phone']) && !empty($data['data'])){
                  $this->sendSms($data['template'],$restaurant_id,['to'=>$data['phone']],$data['data'], null, $user_type);
               }
               if(isset($data['template_data']) && !empty($data['template_data']) && !empty($data['phone']) && !empty($data['data'])){
                  $this->sendSms(null,$restaurant_id,['to'=>$data['phone']],$data['data'],$data['template_data'],$user_type);
               }

             }elseif($key=='mail'){
                if(!empty($data['template']) && !empty($data['email']) && !empty($data['data'])){

                 $this->sendMail($data['template'],$restaurant_id,['to'=>$data['email']],$data['data'],$user_type);
                }
             }
             // print_r($key);
              //print_r($data);
              
         }
      }
  
  }

   public function sendSms($template_name,$restaurant_id,array $config,array $data,$template_data=null,$user_type){

    if(count($config) && isset($config['to']) && !empty($config['to'])  &&  count($data) && !empty($restaurant_id)){

        if(!empty($template_name)){
          $finaltemplate=$this->getTemplate('sms',$template_name,$restaurant_id,$data,$user_type);
              if($finaltemplate!=false){

               //$rsp=dispatch(new NotifySms($config['to'],$finaltemplate));
                  if(isset($finaltemplate['content']) && !empty($finaltemplate['content'])){
                      $text = json_decode($finaltemplate['content'], true);
                      \Log::info($text['content']);
                      if(isset($text['content']) && !empty($text['content'])){
                          $resp=NotifySms::dispatch($config['to'],$text['content']);
                      }
                  }

                //->onQueue('processing')

              }

        }	else if(!empty($template_data)){
                $resp=NotifySms::dispatch($config['to'],$template_data);

        }
      		 

  	 }

   }
      public function sendMail($template_name,$restaurant_id,array $config,array $data, $user_type){
        \Log::info('Senmailcalled');
        //\Log::info($data);
        //\Log::info($config);
       
      if(count($config) && isset($config['to']) && !empty($config['to'])  &&  count($data) && !empty($template_name) && !empty($restaurant_id)){
         \Log::info(' inner sendMail');

          $finaltemplate=$this->getTemplate('email',$template_name,$restaurant_id,$data, $user_type);
            //\Log::info($finaltemplate);
            if($finaltemplate!=false){
              if (!empty($config['to']) && strpos($config['to'], ',') !== false) {
                  $config['to'] = explode(',', $config['to']);
              }
              $mail=Mail::to($config['to']);

              /*if(isset($config['cc']) && !empty($config['cc'])){
                $mail=$mail->cc($cc);
              } */
              if(isset($config['bcc']) && !empty($config['bcc'])){
                $mail=$mail->bcc($config['bcc']);
              }  
              
              $data=[
              'content'=>$finaltemplate['content'],
              'subject'=>$finaltemplate['subject'],
              ];
              \Log::info($data);
              $mail=$mail->send(new NotifyMail($data));

             

           }   
         
      }else{

         

      }  
    
    }


    public function getEmailByTemplateId($template_name, $parent_restaurant_id){

        return MailTemplate::select(['id','subject','content'])->where('restaurant_id', $parent_restaurant_id)->where('template_name',$template_name)->where('status',1)->first();
    }

    public function getSmsByTemplateId($sms_name, $parent_restaurant_id){

        return SmsContent::select(['id','content'])->where('restaurant_id', $parent_restaurant_id)->where('sms_name',$sms_name)->where('status',1)->first();
    }

    

    public function getFinalTemplate(array $data,$template_content){

         if($template_content){
    	
           $content=$template_content;   
			preg_match_all("/\[(.*?)\]/",$content, $options_array);
	        
	        if(isset($options_array[1]) && count($options_array[1])) {
	        	foreach($options_array[1] as $key){

	        		if (array_key_exists(strtolower($key),$data))
	        		{ 

                		$content=preg_replace('/\['.strtolower($key).'\]/',$data[strtolower($key)],$content);

	        		}elseif (array_key_exists(strtoupper($key),$data)){

                	    $content=preg_replace('/\['.strtoupper($key).'\]/', $data[strtoupper($key)],$content);
	        		}		
                }

                return $content;        


	        }
	        return $content;
         }
    }

    public function getTemplate($type='email',$template_name,$restaurant_id,array $data,$user_type){
            $resp=[];
             if(!empty($restaurant_id)){

          $restaurant=Restaurant::select(['restaurant_name','address','street','phone','parent_restaurant_id','lat','lng'])->where('id',$restaurant_id)->first();

            $data['restaurant_name']=$restaurant->restaurant_name;
            $data['restaurant_address']=$restaurant->address;
            $data['restaurant_address_street']=$restaurant->street;
            $data['restaurant_phone']=$restaurant->phone;
            $data['parent_restaurant_id']=$restaurant->parent_restaurant_id;
            $data['restaurant_latitude']=$restaurant->lat;
            $data['restaurant_longitude']=$restaurant->lng;
          }

       if($type=='email' && !empty($restaurant_id)){
       if($user_type=='customer'){
           //header
           $header_template=$this->getEmailByTemplateId('header_layout', $data['parent_restaurant_id']);
           //footer
           $footer_template=$this->getEmailByTemplateId('footer_layout', $data['parent_restaurant_id']);
       }else{
           //header
           $header_template=$this->getEmailByTemplateId('header_layout_restaurant', $data['parent_restaurant_id']);
           //footer
           $footer_template=$this->getEmailByTemplateId('footer_layout_restaurant', $data['parent_restaurant_id']);
       }
       $template=$this->getEmailByTemplateId($template_name, $data['parent_restaurant_id']);
       //\Log::info($data);
       $resp['subject']=$template->subject;
       $final_template = $header_template->content.$template->content.$footer_template->content;

       }else if($type=='sms'){

           $final_template = $template=$this->getSmsByTemplateId($template_name, $data['parent_restaurant_id']);
           \Log::info($final_template);
       }

       if($template && !empty($restaurant_id) && count($data)){

        $content_mail=  $this->getFinalTemplate($data, $final_template);
       	 
	     	 	$resp['content']=$content_mail;
          return $resp;
       }

              return false;

    }


}
