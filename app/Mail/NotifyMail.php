<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyMail extends Mailable implements ShouldQueue  //to enable queueee
//class NotifyMail extends Mailable  
{
    use Queueable, SerializesModels;
    protected $data;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
                $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
   /* public function build()
    {
        \Log::info('MAIL  CALLED');
        return $this->view('emails.mail-template')
            ->subject($this->data['subject'])
            ->with($this->data);
    }*/

    public function build()
    {
        \Log::info('MAIL CALLED');
        $resp=$this->view('emails.mail-template')
            ->subject($this->data['subject'])
            ->with($this->data);

        if(isset($this->data['from_address']) && isset($this->data['from_name']) && !empty($this->data['from_address']) && !empty($this->data['from_name'])){

            $resp=$resp->from($this->data['from_address'], $this->data['from_name']);
        }
        return $resp;
    }
}
