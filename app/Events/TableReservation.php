<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Modules\Reservation\Entities\Reservation;

class TableReservation
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $reservation;
    public $by;
    /**
     * Create a new event instance.
     *
     * @return void
     */
     public function __construct(Reservation  $reservation, $by)
    {
        $this->reservation = $reservation;
        $this->by = $by;

       
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
