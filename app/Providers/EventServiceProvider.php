<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
         'App\Events\TableReservation' => [
            'App\Listeners\TableReservationListener',
        ],
        'App\Events\TableReservationModify' => [
            'App\Listeners\TableReservationModifyListener',
        ],
        'App\Events\TableReservationCancel' => [
            'App\Listeners\TableReservationCancelListener',
        ],
        'App\Events\TableWaitlist' => [
            'App\Listeners\TableWaitlistListener',
        ],
        'App\Events\TableReservationRefund' => [
            'App\Listeners\TableReservationRefundListener',
        ],
        'App\Events\TableReservationPaymentLink' => [
            'App\Listeners\TableReservationPaymentLinkListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
