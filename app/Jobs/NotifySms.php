<?php

namespace App\Jobs;

use Exception;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifySms implements ShouldQueue { //enable  queue
//class NotifySms {
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
//  public $queue = 'sms';
    public $userMobNo;
    public $SmsText;
    public function __construct($userMobNo,$SmsText)
    {
         $this->userMobNo=$userMobNo;
         $this->SmsText=$SmsText;

    }

    

    public function handle()
    {

     \Log::info('SMS API CALLED');
       

          $userMobNo=$this->userMobNo;
          $SmsText=$this->SmsText;
      
           /******SMS API CONFIGS************/
            $CAT_AUTH_URL = config('sms.SMS_AUTH_URL');
            $CAT_SENDMSG_URL = config('sms.SMS_SEND_URL');
            $CAT_API_ID = config('sms.SMS_API_ID');
            $CAT_AC_USERNAME = config('sms.SMS_USERNAME');
            $CAT_AC_PASSWORD = config('sms.SMS_PASSWORD');
            $CAT_FROM = config('sms.SMS_FROM');
            $COUNTRY_CODE_US_MOB = config('sms.SMS_COUNTRY_CODE');
            /*********************************/
       
       $smsResponse = false;
       if($userMobNo && $SmsText)
       {

        $userMobNo=$COUNTRY_CODE_US_MOB.$userMobNo ; 
         \Log::info('mu kcvd kcdhcdh');\Log::info($userMobNo);
            $SmsText = urlencode($SmsText);
            //auth call
           $url_auth = $CAT_AUTH_URL."?user=".$CAT_AC_USERNAME."&password=".$CAT_AC_PASSWORD."&api_id=".$CAT_API_ID;
            $ret_auth = $this->sendRequest($url_auth);// do auth call
            $sess = explode(":",$ret_auth); // explode Auth response. 

            if ($sess[0] == "OK") {
                $sess_id = trim($sess[1]); // remove any whitespace
                $url_send = $CAT_SENDMSG_URL."?session_id=".$sess_id."&to=".$userMobNo."&text=".$SmsText."&mo=1&from=".$CAT_FROM;
                 $ret_send = $this->sendRequest($url_send);// do sendmsg call
                $send = explode(":",$ret_send);
                if ($send[0] == "ID") {
                    $smsResponse = true;
                }else if($send[0] == "ERR"){
                    $smsResponse = $send[1];
                }   
            }
             \Log::info('SMS API response');
             \Log::info($SmsText);
             \Log::info(json_encode($smsResponse));

            \Log::info('SMS API response');
             \Log::info($SmsText);
             \Log::info(json_encode($smsResponse));


        }
        
    }

    public function sendRequest($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
     public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
    }
}

