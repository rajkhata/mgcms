<?php

namespace App\Listeners;

use App\Events\TableReservation;
use App\Events\TableReservationPaymentLink;
use App\Helpers\CommonFunctions;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use  App\Repositories\Notification ;
use App\Models\CmsUser;

class TableReservationPaymentLinkListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $notiobj;
    public function __construct(Notification $notification)
    {
        $this->notiobj=$notification;
    }


    /**
     * Handle the event.
     *
     * @param  TableReservation  $event
     * @return void
     */
    public function handle(TableReservationPaymentLink $event)
    {
        $params = [];
        $restaurant_id=$event->reservation->restaurant_id;
        $restaurant_manager=CmsUser::where('restaurant_id',$restaurant_id)->first();
        $by=$event->by;
        $notification_setting = app('Modules\Reservation\Http\Controllers\GlobalSettingsController')->getNotificationSetting($restaurant_id);
        $start_date_time=CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $event->reservation->start_time);
        \Log::info($notification_setting);
        \Log::info('Restaurant manager email=>'.$restaurant_manager->email);
        if($by=='Customer'){
            \Log::info("New Reservation by".$event->reservation->email);
            $cancellation_charge_type = !empty($event->reservation->cancellation_charge_type)?str_replace('_', ' ', $event->reservation->cancellation_charge_type):'';
            $data=[
                'reservation_id'=>$event->reservation->receipt_no,
                'name'=>$event->reservation->fname.' '.$event->reservation->lname,
                'start_date_time'=>$start_date_time->format('D, M d, Y'),
                'fname'=>$event->reservation->fname,
                'lname'=>$event->reservation->lname,
                'email'=>$event->reservation->email,
                'phone'=>$event->reservation->mobile,
                'party_size'=>$event->reservation->reserved_seat,
                'time'=>$start_date_time->format('h:i A'),
                'date'=>$start_date_time->format('d-m-Y'),
                'occasion'=>$event->reservation->special_occasion,
                'special_instruction'=>$event->reservation->user_instruction,
                'restaurant_name'=>$event->reservation->restaurant_name,
                'parent_restaurant_id'=>$event->reservation->parent_restaurant_id,
                'payment_token' => $event->reservation->payment_link_token,
                'cancellation_charge' => $event->reservation->cancellation_charge,
                'cancellation_charge_type' => $cancellation_charge_type
            ];

            if($notification_setting['cust_resv_sms_flag']){
                $params['customer']['sms'] = [
                    'template'=>'reservation_payment_link_customer',
                    'data'=>$data,
                    'phone'=>$event->reservation->mobile
                ];
            }
            if($notification_setting['cust_resv_email_flag']){
                $params['customer']['mail'] = [
                    'template'=>'reservation_payment_link_customer',
                    'email'=>$event->reservation->email,
                    'data'=>$data
                ];
            }
            /*if($notification_setting['manager_resv_incoming_email_flag']){
                $emails = !empty($notification_setting['manager_resv_emails'])?str_replace(array('[', ']', '"'), '', ($notification_setting['manager_resv_emails'])):'';
                $emails = !empty($emails)?$emails:$restaurant_manager->email;
                \Log::info($emails);
                $params['manager']['mail'] = [
                    'template'=>'reservation_customer_notify_manager',
                    'email'=>$emails,
                    'data'=>$data
                ];
            }*/
            if(!empty($params) && count($params)>0){
                $this->notiobj->notifiy($params,$restaurant_id);
            }
        }

    }
}
