<?php

namespace App\Listeners;

use App\Events\TableReservationModify;
use App\Helpers\CommonFunctions;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use  App\Repositories\Notification ;

class TableReservationModifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $notiobj;
    public function __construct(Notification $notification)
    {
        $this->notiobj=$notification;
    }

    /**
     * Handle the event.
     *
     * @param  TableReservationModify  $event
     * @return void
     */

    public function handle(TableReservationModify $event)
    {
        $params = [];
        $restaurant_id=$event->reservation->restaurant_id;
        $by=$event->by;
        $notification_setting = app('Modules\Reservation\Http\Controllers\GlobalSettingsController')->getNotificationSetting($restaurant_id);
        $start_date_time=CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $event->reservation->start_time);
        if($by=='Manager'){
            \Log::info("Reservation updated by Manager for ".$event->reservation->email);
            $data=[
                'reservation_id'=>$event->reservation->receipt_no,
                'name'=>$event->reservation->fname.' '.$event->reservation->lname,
                'start_date_time'=>$start_date_time->format('D, M d, Y'),
                'fname'=>$event->reservation->fname,
                'lname'=>$event->reservation->lname,
                'email'=>$event->reservation->email,
                'phone'=>$event->reservation->mobile,
                'party_size'=>$event->reservation->reserved_seat,
                'time'=>$start_date_time->format('h:i A'),
                'date'=>$start_date_time->format('d-m-Y'),
                'occasion'=>$event->reservation->special_occasion,
                'special_instruction'=>$event->reservation->user_instruction,
                'restaurant_name'=>$event->reservation->restaurant_name,
                'parent_restaurant_id'=>$event->reservation->parent_restaurant_id,
                'payment_toekn'=>$event->reservation->payment_link_token
            ];

            if($notification_setting['cust_resv_sms_flag']){
                $params['customer']['sms'] = [
                    'template'=>'reservation_modify_manager',
                    'data'=>$data,
                    'phone'=>$event->reservation->mobile
                ];
            }
            if($notification_setting['cust_resv_email_flag']){
                $params['customer']['mail'] = [
                    'template'=>'reservation_modify',
                    'email'=>$event->reservation->email,
                    'data'=>$data
                ];
            }
            /*  ,
              'manager'=>[
               'sms'=>[
                     'template'=>'res_sm',
                     'data'=>'datahere in array',
                        'mobile'=>'mobile'

                  ],
                'mail'=>[
                     'template'=>'res_sm',
                     'email'=>'email',
                     'data'=>'datahere in array'
                  ]  
              ]*/

            if(!empty($params) && count($params)>0){
                $this->notiobj->notifiy($params,$restaurant_id);
            }
        }
    }
}
