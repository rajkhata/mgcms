<?php

namespace App\Listeners;
use App\Events\TableWaitlist;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use  App\Repositories\Notification ;
use App\Models\CmsUser;

class TableWaitlistListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $notiobj;
    public function __construct(Notification $notification)
    {
        $this->notiobj=$notification;
    }


    /**
     * Handle the event.
     *
     * @param  TableWaitlist  $event
     * @return void
     */
    public function handle(TableWaitlist $event)
    {

        $restaurant_id=$event->reservation->restaurant_id;
        $restaurant_manager=CmsUser::where('restaurant_id',$restaurant_id)->first();
        $by=$event->by;
        \Log::info('Restaurant manager email=>'.$restaurant_manager->email);
        if($by=='Manager'){

            \Log::info("New Waitlist by".$event->reservation->email);
            $start_day = date('l', strtotime($event->reservation->start_time));
            $start_date = date('d M Y', strtotime($event->reservation->start_time));
            $start_time = date('h:i A', strtotime($event->reservation->start_time));

            $data=[
                'name'=>$event->reservation->fname.' '.$event->reservation->lname,
                'email'=>$event->reservation->email,
                'mobile'=>$event->reservation->mobile,
                'first_name'=>$event->reservation->fname,
                'start_day'=>$start_day,
                'start_date'=>$start_date,
                'start_time'=>$start_time,
                'party_size'=>$event->reservation->reserved_seat,
            ];
            $sms_customer=[
                //  'template'=>'',
                //'template_data'=>$event->custom_message,
                'data'=>$data,
                'mobile'=>$event->reservation->mobile
            ];
            if(!empty($event->custom_message)){
                $sms_customer['template_data']=$event->custom_message;
            }else{
                $sms_customer['template']='reservation_customer';

            }
            $params=[
                'customer'=>[
                    'sms'=>$sms_customer
                ]


            ];
            $this->notiobj->notifiy($params,$restaurant_id);

        }

    }
}

