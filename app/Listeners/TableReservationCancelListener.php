<?php

namespace App\Listeners;

use App\Helpers\CommonFunctions;
use App\Models\CmsUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Events\TableReservationCancel;

use  App\Repositories\Notification ;

class TableReservationCancelListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $notiobj;
    public function __construct(Notification $notification)
    {
        $this->notiobj=$notification;
    }

    /**
     * Handle the event.
     *
     * @param  TableReservationCancel  $event
     * @return void
     */
    public function handle(TableReservationCancel $event)
    {

        \Log::info("Reservation cancelled for ".$event->reservation->email);
        $params = [];

        $restaurant_id=$event->reservation->restaurant_id;
        $by=$event->by;
        $notification_setting = app('Modules\Reservation\Http\Controllers\GlobalSettingsController')->getNotificationSetting($restaurant_id);
        $restaurant_manager=CmsUser::where('restaurant_id',$restaurant_id)->first();
        $start_date_time=CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $event->reservation->start_time);

        if($by=='Manager'){

            $data=[
                'reservation_id'=>$event->reservation->receipt_no,
                'name'=>$event->reservation->fname.' '.$event->reservation->lname,
                'start_date_time'=>$start_date_time->format('D, M d, Y'),
                'fname'=>$event->reservation->fname,
                'lname'=>$event->reservation->lname,
                'email'=>$event->reservation->email,
                'phone'=>$event->reservation->mobile,
                'party_size'=>$event->reservation->reserved_seat,
                'time'=>$start_date_time->format('h:i A'),
                'date'=>$start_date_time->format('d-m-Y'),
                'occasion'=>$event->reservation->special_occasion,
                'special_instruction'=>$event->reservation->user_instruction,
                'restaurant_name'=>$event->reservation->restaurant_name,
                'parent_restaurant_id'=>$event->reservation->parent_restaurant_id,
            ];

            if($notification_setting['cust_resv_sms_flag']){
                $params['customer']['sms'] = [
                    'template'=>'reservation_cancel_manager',
                    'data'=>$data,
                    'phone'=>$event->reservation->mobile
                ];
            }
            if($notification_setting['cust_resv_email_flag']){
                $params['customer']['mail'] = [
                    'template'=>'reservation_cancel',
                    'email'=>$event->reservation->email,
                    'data'=>$data
                ];
            }
            if($notification_setting['manager_resv_incoming_email_flag']){
                $emails = !empty($notification_setting['manager_resv_emails'])?str_replace(array('[', ']', '"'), '', ($notification_setting['manager_resv_emails'])):'';
                $emails = !empty($emails)?$emails:$restaurant_manager->email;
                $params['manager']['mail'] = [
                    'template'=>'reservation_cancel_manager',
                    'email'=>$emails,
                    'data'=>$data
                ];
            }
            /*  ,
              'manager'=>[
               'sms'=>[
                     'template'=>'res_sm',
                     'data'=>'datahere in array',
                        'mobile'=>'mobile'

                  ],
                'mail'=>[
                     'template'=>'res_sm',
                     'email'=>'email',
                     'data'=>'datahere in array'
                  ]  
              ]*/

            if(!empty($params) && count($params)>0){
                $this->notiobj->notifiy($params,$restaurant_id);
            }
        }
    }
}
