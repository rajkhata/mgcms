<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class UserOrderDetail extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_order_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_order_id', 'item', 'quantity', 'unit_price', 'total_item_amt', 'item_description', 'item_id',
        'item_price_id', 'user_id', 'order_addon_data', 'status', 'order_token', 'special_instruction', 'item_price_desc'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function getOrderDetail($orderId)
    {
        return self::where('user_order_id', $orderId)->get();
    }

    /*public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
    public function order()
    {
        return $this->hasMany('App\Models\UserOrder');
    }

    public function address()
    {
        return $this->hasMany('App\Models\UserAddresses');
    }

    public function card()
    {
        return $this->hasMany('App\Models\UserCards');
    }*/

    public function giftdetails()
    {
        return $this->hasMany('App\Models\GiftcardCoupons', 'order_detail_id');
    }

     public function addonoptions()
    {
        return $this->hasMany('Modules\MenuManagement\Entities\OrderDetailOptions','user_order_detail_id');
    }
}
