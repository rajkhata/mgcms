<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'language_id','state_id','country_id','city_name','neighbouring','locality','state_code','latitude','longitude','sales_tax','status','time_zone','city_name_alias','is_browse_only','seo'
    ];

}
