<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuCategoryLanguage;

class MenuAttribute extends Model
{
    protected $guarded = ['id']; //← the field name
    protected  $table='menu_attribute';

    protected $hidden = [
        'created_at', 'updated_at'
    ];


    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    public function attributeValues()
    {
        return $this->hasMany('App\Models\MenuAttributeValue', 'attribute_id');
    }




}
