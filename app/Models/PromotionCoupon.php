<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionCoupon extends Model
{
    protected $table = 'promotion_coupon';
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }	
}
