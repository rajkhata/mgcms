<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiftcardCoupons extends Model
{
    protected $table = 'gift_card_coupons';
    protected $guarded = [];


    public function user()
    {
        return $this->hasOne('App\Models\CmsUser', 'id', 'by_user');
    }

} 