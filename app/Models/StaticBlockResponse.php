<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticBlockResponse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'static_block_id', 'enquiry_type', 'response', 'parent_restaurant_id', 'restaurant_id', 'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }
}
