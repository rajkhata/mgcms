<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuMealTypePrice extends Model
{
    protected $fillable = [ 'menu_meal_type_id','menu_item_id','price'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function MenuMealType()
    {
        return $this->belongsTo('App\Models\MenuMealType','menu_meal_type_id','id');
    }

    public function MenuItem()
    {
        return $this->belongsTo('App\Models\MenuItem','menu_item_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
