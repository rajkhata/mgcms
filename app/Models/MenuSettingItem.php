<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSettingItem extends Model
{
    protected $fillable = ['restaurant_id','language_id','menu_category_id','menu_setting_category_id','status','name','side_flag','quantity_flag','price'];

    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    public function MenuCategory()
    {
        return $this->belongsTo('App\Models\MenuCategory','menu_category_id','id');
    }

    public function MenuSettingCategory()
    {
        return $this->belongsTo('App\Models\MenuSettingCategory','menu_setting_category_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
