<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class RestaurantCarryoutCustomeTime extends Model
{
    protected $table = 'cms_restaurant_carryout_custom_time';
    protected $fillable = ['restaurant_id', 'is_dayoff','calendar_date', 'slot_detail'];
}
?>