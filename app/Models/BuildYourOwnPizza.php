<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildYourOwnPizza extends Model
{
    protected $table = 'build_your_own_pizzas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id', 'parent_id', 'label', 'setting', 'is_multiselect', 'is_customizable', 'is_preference', 'priority', 'status', 'language_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\BuildYourOwnPizza', 'parent_id', 'id');
    }

    /*public function children() {
        return $this->hasMany('App\Models\BuildYourOwnPizza', 'parent_id', 'id');
    }*/
}
