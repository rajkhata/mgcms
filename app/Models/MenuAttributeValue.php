<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuCategoryLanguage;

class MenuAttributeValue extends Model
{
    protected $guarded = ['id']; //← the field name

    protected $hidden = [
        'created_at', 'updated_at'
    ];


    public function menuAttribute()
    {
        return $this->belongsTo('App\Models\MenuAttribute','attribute_id','id');
    }




}
