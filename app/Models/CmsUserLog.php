<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsUserLog extends Model
{
    protected $table = 'cms_user_logs';
    /**
}
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id','user_id','updated_id','module_name','restaurant_id','tablename','action_type','previous_action_data','action_data'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];
    public function cmsuser(){
        return $this->belongsTo('App\Models\CmsUser', 'user_id');

    }

    public function restaurant() {
        return $this->belongsTo('App\Models\Restaurant', 'restaurant_id');
    }


} 