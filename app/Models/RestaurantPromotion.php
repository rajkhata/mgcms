<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantPromotion extends Model
{
    //use SoftDeletes;

    //protected $dates = ['deleted_at'];
    protected $table = 'restaurant_promotions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'amount_or_percent', 'condition', 'condition_amount', 'discount', 'start_date', 'end_date'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'/*, 'deleted_at'*/
    ];

}
