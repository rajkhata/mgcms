<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSettingCategory extends Model
{
    protected $fillable = ['restaurant_id','language_id','menu_category_id','status','name','is_multi_select','is_mandatory','maximum_selection','minimum_selection','equal_selection'];

    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    public function MenuCategory()
    {
        return $this->belongsTo('App\Models\MenuCategory','menu_category_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
