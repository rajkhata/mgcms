<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Language;
use App\Models\ItemAddonGroup;

class ItemAddonGroupsLanguage extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    protected $table = 'item_addon_groups_language';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_addon_group_id', 'group_name', 'language_id', 'prompt'
    ];

    public function itemAddonGroups() {
        return $this->belongsTo(ItemAddonGroups::class);
    }

    public function language() {
        return $this->belongsTo(Language::class);
    }

}
