<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticBlock extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id', 'block_title', 'block_data', 'language_id', 'sort_order', 'status','block_type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
