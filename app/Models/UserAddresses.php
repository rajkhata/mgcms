<?php

namespace App;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddresses extends Model
{
     protected $table = 'user_addresses';

     protected $fillable = [
        'user_id', 'name', 'label', 'email', 'address1', 'address2', 'street', 'city', 'state', 'zipcode', 'phone', 'address_type', 'latitude', 'longitude', 'language_id'
    ];

     public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
