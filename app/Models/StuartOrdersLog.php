<?php

namespace App\Models;

 
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class StuartOrdersLog extends Model
{
 
    protected $table = 'stuart_orders_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ma_order_id', 'ma_restaurant_id' , 'request_obj', 'response_obj'];

     

}
