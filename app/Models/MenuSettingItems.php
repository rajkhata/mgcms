<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSettingItems extends Model
{
    protected $table = 'menu_setting_items';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id', 'language_id', 'menu_category_id', 'menu_setting_category_id', 'name', 'price', 'description', 'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    /*public function parent() {
        return $this->belongsTo('App\Models\BuildYourOwnPizza', 'parent_id', 'id');
    }

    /*public function children() {
        return $this->hasMany('App\Models\BuildYourOwnPizza', 'parent_id', 'id');
    }*/
}
