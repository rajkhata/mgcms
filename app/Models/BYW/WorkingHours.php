<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class WorkingHours extends Model
{
	protected $table = 'cms_restaurant_resv_week_time';
    	protected $guarded = ['id'];
}
?>
