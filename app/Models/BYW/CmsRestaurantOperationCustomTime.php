<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;


class CmsRestaurantOperationCustomTime extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'cms_restaurant_operation_custom_time';
    protected $guarded = ['id'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }	
}
