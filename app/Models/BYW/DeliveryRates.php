<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class DeliveryRates extends Model
{
    protected $table = 'delivery_rates';
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

}
