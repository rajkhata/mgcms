<?php

namespace App\Models\BYW;
 
use Illuminate\Database\Eloquent\Model;
 
 

class CmsUser extends Model 
{
     

    protected $table = 'cms_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    	
   // protected $fillable123 =  self::getTableColumns();
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public static function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
     	
}
