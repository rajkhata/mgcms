<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;


class RestaurantVisiblityWeekTime extends Model
{
    protected $table = 'cms_restaurant_visibility_duration_week_time';
     protected $guarded = ['id'];
}
?>
