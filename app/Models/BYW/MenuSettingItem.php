<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class MenuSettingItem extends Model
{
    protected $guarded = ['id'];
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
