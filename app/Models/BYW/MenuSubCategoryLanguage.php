<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuSubCategory;

class MenuSubCategoryLanguage extends Model {

    protected $guarded = ['id'];

    protected $table = 'menu_sub_categories_language';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function menuSubCategory() {
        return $this->belongsTo(MenuSubCategory::class, 'menu_sub_categories_id', 'id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
