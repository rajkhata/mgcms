<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;


class RestaurantDeliveryWeekTime extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'cms_restaurant_delivery_week_time';
   protected $guarded = ['id'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];
}
