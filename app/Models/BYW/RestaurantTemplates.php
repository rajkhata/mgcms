<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class RestaurantTemplates extends Model
{
    protected $table = 'restaurant_templates';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }	
}
