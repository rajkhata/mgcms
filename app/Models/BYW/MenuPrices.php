<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class MenuPrices extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
