<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class RestaurantRegister extends Model
{
    //
    protected $table = 'restaurant_registers';	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];
         /**
     * @param Post $query
     * @param int  $int
     *
     * @return mixed
     */
    public function scopeById($query, $int)
    {
        return $query->where('id', '=', $int);
    }
    
    /**
     * @param Post   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByTitle($query, $string)
    {
        return $query->where('name', '=', $string);
    }
    /**
     * @param Post   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByTitleLike($query, $string)
    {
        return $query->where('name', 'like', $string . '%');
    }
    
   
    /**
     * @param Post   $query
     * @param string $string
     *
     * @return mixed
     */
    public function scopeByCreatedAt($query, $string)
    {
        $date = \Carbon\Carbon::parse($string)->format('Y-m-d');
        return $query->whereDate('created_at', '=', $date);
    }
   
    /**
     * @return array
     */
    public function toFieldsAmiGrid(): array
    {
        return [
            'id',
            'name',
            'created_at',
             
        ];
    }	
}
