<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class SBPages extends Model
{
    protected $table = 'sb_pages';
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }	
}
