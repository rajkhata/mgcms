<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;


class RestaurantVisiblityCustomTime extends Model
{
    protected $table = 'cms_restaurant_visibility_duration_custom_time';
     protected $guarded = ['id'];
}
?>
