<?php

namespace App\Models\BYW\NewMenu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifierOptionGroupLanguage extends Model {

   protected $guarded = ['id'];
    protected $table = 'modifier_item_option_groups_language';
     

}
