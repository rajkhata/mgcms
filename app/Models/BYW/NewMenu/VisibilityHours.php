<?php

namespace App\Models\BYW\NewMenu;

use Illuminate\Database\Eloquent\Model;

class VisibilityHours extends Model
{
	protected $table = 'cms_restaurant_visibility_duration_week_time';
    protected $guarded = ['id'];
}
