<?php

namespace App\Models\BYW\NewMenu;

use Illuminate\Database\Eloquent\Model;
use RestaurantAutomation\Models\Language;
use RestaurantAutomation\Models\NewMenu\ItemAddonGroups;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemAddonGroupsLanguage extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    protected $table = 'item_addon_groups_language';

    protected $guarded = ['id'];

     

}
