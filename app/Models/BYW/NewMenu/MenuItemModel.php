<?php

namespace App\Models\BYW\NewMenu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RestaurantAutomation\Models\NewMenu\MenuItemsLanguage;

class MenuItemModel extends Model {

    protected $guarded = ['id'];
    protected $table = 'new_menu_items';

 
}
