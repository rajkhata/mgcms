<?php

namespace App\Models\BYW\NewMenu;

use Illuminate\Database\Eloquent\Model;
use RestaurantAutomation\Models\Restaurant;
use RestaurantAutomation\Models\Language;
use RestaurantAutomation\Models\NewMenu\MenuItemModel;
use RestaurantAutomation\Models\NewMenu\ItemAddonGroupsLanguage;
use RestaurantAutomation\Models\NewMenu\ItemAddons;

use Illuminate\Database\Eloquent\SoftDeletes;

class ItemAddonGroups extends Model {
    
 
 
    protected $table = 'item_addon_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
