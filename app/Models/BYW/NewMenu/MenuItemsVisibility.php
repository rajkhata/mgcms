<?php

namespace App\Models\BYW\NewMenu;

use Illuminate\Database\Eloquent\Model;

class MenuItemsVisibility extends Model {

    protected $table = 'menu_items_visibility';
    protected $guarded = ['id'];

}
