<?php

namespace App\Models\BYW\NewMenu;

use Illuminate\Database\Eloquent\Model;
use RestaurantAutomation\Models\Language;

class Localization extends Model {

     
    protected $table = 'localization';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}
