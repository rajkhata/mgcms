<?php

namespace App\Models\BYW\NewMenu;

use Illuminate\Database\Eloquent\Model;
use RestaurantAutomation\Models\Language;
use RestaurantAutomation\Models\NewMenu\MenuItemModel;

class MenuItemsLanguage extends Model {

    protected $table = 'new_menu_items_language';
    protected $guarded = ['id'];

}
