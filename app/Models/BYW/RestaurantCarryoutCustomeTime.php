<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;


class RestaurantCarryoutCustomeTime extends Model
{
    protected $table = 'cms_restaurant_carryout_custom_time';
     protected $guarded = ['id'];
}
?>
