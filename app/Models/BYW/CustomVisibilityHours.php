<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class CustomVisibilityHours extends Model
{
	protected $table = 'cms_restaurant_visibility_duration_custom_time';
    	protected $guarded = ['id'];
	public function getTableColumns() {
        	return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    	}
}
?>
