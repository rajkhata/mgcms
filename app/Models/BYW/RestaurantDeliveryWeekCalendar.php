<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;


class RestaurantDeliveryWeekCalendar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'cms_restaurant_delivery_week_time';
    protected $guarded = ['id'];

     
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    } 
}
