<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;
use Config;

class NewMenuItems extends Model
{
    protected $table = 'restaurants';	
    protected $guarded = ['id'];

    
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }


}
