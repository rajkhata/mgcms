<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class Cusines extends Model
{
    protected $table = 'cusine';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }	
}
