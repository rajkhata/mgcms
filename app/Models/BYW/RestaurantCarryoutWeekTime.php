<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;


class RestaurantCarryoutWeekTime extends Model
{
    protected $table = 'cms_restaurant_carryout_week_time';
    protected $guarded = ['id'];
}
?>
