<?php

namespace App\Models\BYW;

use Illuminate\Database\Eloquent\Model;

class CustomWorkingHours extends Model
{
	protected $table = 'cms_restaurant_resv_custom_time';
    	protected $guarded = ['id'];
	public function getTableColumns() {
        	return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    	}
}
?>
