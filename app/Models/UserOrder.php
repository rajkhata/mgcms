<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Spatie\Permission\Traits\HasRoles;

class UserOrder extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'user_orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref_order_id', 'user_id', 'restaurant_id', 'surname', 'fname', 'lname', 'user_ip', 'state_code', 'phone', 'apt_suite', 'address',
        'latitude', 'longitude', 'city', 'order_amount', 'pay_via_card', 'deal_discount', 'pay_via_point', 'redeem_point', 'deal_id',
        'deal_title', 'promocode_discount', 'promocodeid', 'order_type', 'created_at', 'updated_at', 'user_comments',
        'restaurants_comments', 'special_checks', 'zipcode', 'payment_status', 'status', 'delivered_by', 'delivery_time', 'tax',
        'tip_amount', 'tip_percent', 'delivery_charge', 'delivery_address', 'frozen_status', 'user_sess_id', 'payment_gatway',
        'stripes_token', 'stripe_charge_id', 'host_code', 'card_number', 'encrypt_card_number', 'name_on_card', 'card_type',
        'expired_on', 'billing_zip', 'payment_receipt', 'order_type1', 'order_type2', 'email', 'miles_away', 'stripe_card_id',
        'user_card_id', 'total_amount', 'new_order', 'approved_by', 'is_read', 'crm_update_at', 'host_name', 'crm_comments',
        'is_deleted', 'is_reviewed', 'review_id', 'remarks', 'agent', 'actual_amount', 'order_pass_through', 'efax_sent',
        'assignMuncher', 'cronUpdate', 'cronUpdateNotification', 'city_id', 'cronsmsupdate', 'cod', 'manual_update', 'ds_order_id', 'state_code','flat_discount','promocode_discount','additional_charge','additional_charge_name','service_tax','order_state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function getOrdersByUser($userId,$emailId)
    {
        return self::where('user_id', $userId)->whereOr('email',$emailId)->whereNotIn('user_orders.status', ['pending','refund'])->get();
    }

    /*public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
    public function order()
    {
        return $this->hasMany('App\Models\UserOrder');
    }

    public function address()
    {
        return $this->hasMany('App\Models\UserAddresses');
    }

    public function card()
    {
        return $this->hasMany('App\Models\UserCards');
    }*/

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant', 'restaurant_id');
    }

    public function details()
    {
        return $this->hasMany('App\Models\UserOrderDetail', 'user_order_id');
    }
    public function scopeWithoutTimestamps()
    {
        $this->timestamps = false;
        return $this;
    }
}
