<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnquirySendMailLog extends Model
{
    protected $table = 'enquiry_sendmail_log';	
    protected $fillable = [
        'restaurant_id','cms_user','enquiry_id', 'order_id', 'message','sent_to','sent_type','created_at','updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

}


//CREATE TABLE `enquiry_sendmail_log` (
//  `id` bigint(20) NOT NULL,
//  `restaurant_id` bigint(20) NOT NULL,
//  `cms_user` int(11) NOT NULL,
//  `enquiry_id` bigint(20) NOT NULL,
//  `message` text NOT NULL,
//  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
//  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
//) ENGINE=InnoDB DEFAULT CHARSET=utf8;
//
//
//ALTER TABLE `enquiry_sendmail_log`
//  ADD PRIMARY KEY (`id`);
//
//ALTER TABLE `enquiry_sendmail_log`
//  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;