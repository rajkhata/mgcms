<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSubCategory extends Model
{

    protected $fillable = [ 'restaurant_id','language_id','menu_category_id','name','pos_id','status','priority','description','sub_description','image_svg', 'thumb_image_med_svg', 'image_png', 'thumb_image_med_png', 'image_png_selected', 'thumb_image_med_png_selected', 'image_class','parent','is_popular','is_favourite','promotional_banner_url','promotional_banner_image','promotional_banner_image_mobile','promo_banners'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    public function MenuCategory()
    {
        return $this->belongsTo('App\Models\MenuCategory','menu_category_id','id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\MenuSubCategory', 'parent');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public function children()
    {
        return $this->hasMany('App\Models\MenuSubCategory', 'parent');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
