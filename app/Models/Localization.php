<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class Localization extends Authenticatable
{
    use Notifiable;

    protected $table = 'localization';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lang_id', 'key', 'value', 'status', 'platform', 'updated_at'
    ];


    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
   /* public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
    public function order()
    {
        return $this->hasMany('App\Models\UserOrder');
    }

    public function address()
    {
        return $this->hasMany('App\Models\UserAddresses');
    }

    public function card()
    {
        return $this->hasMany('App\Models\UserCards');
    }*/
}
