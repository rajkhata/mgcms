<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;

class MybagOptions extends Model
{
    protected $table = 'mybag_options';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

}
