<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryHours extends Model
{
	protected $table = 'cms_restaurant_delivery_week_time';
    protected $fillable = ['restaurant_id', 'is_dayoff', 'calendar_day', 'slot_detail'];
}
?>