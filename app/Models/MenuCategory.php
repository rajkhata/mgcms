<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuCategoryLanguage;

class MenuCategory extends Model
{
    protected $fillable = [ 'restaurant_id','language_id','name','status','slug','parent', 'image_svg', 'thumb_image_med_svg',
                            'image_png', 'thumb_image_med_png', 'image_png_selected', 'thumb_image_med_png_selected',
                            'image_class','product_type','is_delivery', 'is_carryout','description', 'sub_description',
                            'priority','cat_attachment','is_popular','is_favourite','image','image_icon','pos_id',
                            'promotional_banner_url','promotional_banner_image','promotional_banner_image_mobile','promo_banners',
                            'meta_title', 'meta_keyword'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
    
    public function menuCategoryLanguage() {
        return $this->hasMany(MenuCategoryLanguage::class, 'menu_categories_id', 'id');
    }

    public function MenuSubCategory()
    {
        return $this->hasMany('App\Models\MenuSubCategory', 'menu_category_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\MenuCategory', 'parent');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public function children()
    {
        return $this->hasMany('App\Models\MenuCategory', 'parent');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

}
