<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Restaurant;
use App\Models\Language;

class ItemAddonGroups extends Model {
    
    public $timestamps = false;
    
    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    protected $table = 'item_addon_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id', 'group_name', 'language_id', 'prompt', 'is_required', 'quantity_type', 'quantity'
    ];

    public function restaurant() {
        return $this->belongsTo(Restaurant::class);
    }

    public function language() {
        return $this->belongsTo(Language::class);
    }
}
