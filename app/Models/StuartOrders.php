<?php

namespace App\Models;

 
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class StuartOrders extends Model
{
 
    protected $table = 'stuart_orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ma_order_id', 'ma_restaurant_id', 'job_id', 'status', 'package_type', 'assignment_code', 'pickup_at', 'dropoff_at', 'ended_at', 'distance', 'duration', 'traveled_time', 'traveled_distance',
  'driver', 'comment', 'request_obj', 'response_obj'];

     

}
