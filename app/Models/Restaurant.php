<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ModifierCategories;
use Illuminate\Support\Facades\DB;

class Restaurant extends Model
{
    protected $table = 'restaurants';	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','pause_service_message',
        'free_delivery','parent_restaurant_id','rest_code','restaurant_name','language_id','supported_languages','description','lat','lng','address',
        'street','zipcode','mobile','phone','accept_cc','accept_dc','delivery','takeout','dinning','reservation',
        'menu_available','total_seats','city_id','closed','inactive','price','delivery_area','minimum_delivery',
        'min_partysize','delivery_charge','sentiments','cash','delivery_charge_type','attire_desc','good_for_group_desc',
        'fax','allowed_zip','serve_alcohol','delivery_geo','order_pass_through','featured','pre_paid_enable','menu_sort_order',
        'cod','delivery_provider_id','producer_key','delivery_provider_apikey','header_color_code','source_url','borough',
        'landmark','restaurant_image_name','restaurant_video_name','restaurant_logo_name','neighbourhood','nbd_latitude',
        'nbd_longitude','delivery_desc','notable_chef_desc','parking_desc','ratings','facebook_url','twitter_url','gmail_url',
        'pinterest_url','instagram_url','yelp_url','tripadvisor_url','foursquare_url','twitch_url','youtube_url','status',
        'tip', 'reservation_fee', 'reservation_w_payment', 'reservation_wo_payment', 'reservation_w_dinein',
        'reservation_wo_payment', 'reservation_w_dinein', 'delivery_from', 'delivery_to', 'carryout_from',
        'carryout_to', 'reservation_from', 'reservation_to', 'custom_from', 'support_from','reservation_settings','email', 'slug',
        'delivery_interval','delivery_order_gap','delivery_asap_gap','carryout_asap_gap','carryout_interval','carryout_order_gap','is_delivery_geo','delivery_zipcode',
        'contact_address','contact_street','contact_zipcode', 'title', 'meta_tags','google_verification_code','gtm_code','google_map_id','google_client_id',
        'facebook_client_id','netcore_api_key','service_tax','additional_charge','header_script','footer_script','kpt','kpt_calender',
        'currency_symbol','currency_code','email_data','common_message','common_message_status','common_message_url','referral_code','referral_code_status','referral_code_amount'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    
    protected $casts = [
        'reservation_settings' => 'array',
    ];
    /**
     * Return only brand restaurants
     * @param $query
     * @return mixed
     */
    public function scopeBrand($query) {
        return $query->select('id', 'restaurant_name')
                    ->Where(function ($query) {
                        $query->Where('parent_restaurant_id', '0')
                            ->orWhereNull('parent_restaurant_id');
                    })
                    ->where('status', 1)
                    ->orderBy('id', 'DESC')
                    ->get();
    }

    public function scopeActive($query)
    {
        return $query->select('id', 'restaurant_name', 'parent_restaurant_id')
                    ->where('status', 1)
                    ->get();
    }

    public function scopeGroup($query)
    {
        $allRestaurants = self::active();

        $groupRestData = [];
        foreach($allRestaurants as $res) {
            if(isset($groupRestData[$res['parent_restaurant_id']])) {
                $groupRestData[$res['parent_restaurant_id']]['branches'][] = [
                    'id'    => $res['id'],
                    'restaurant_name' => $res['restaurant_name'],
                ];
            } else {
                $groupRestData[$res['id']] = [
                    'id'    => $res['id'],
                    'restaurant_name' => $res['restaurant_name'],
                    'branches' => []
                ];
            }
        }
        return $groupRestData;
    }

    public static function getSiblingRestaurantIds($parent_restaurant_id)
    {
        return Restaurant::where('parent_restaurant_id', $parent_restaurant_id)->pluck('id')->toArray();
    }
    
    public function modifier_categories() {
        return $this->hasMany(ModifierCategories::class);
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    /**
     * Amit Malakar 14-02-19
     * Returns ENUM field values of a DB field
     * @param $name
     * @return array
     */
    public static function getPossibleEnumValues($name){
        $instance = new static;
        $type = DB::select( DB::raw('SHOW COLUMNS FROM '.$instance->getTable().' WHERE Field = "'.$name.'"') )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach(explode(',', $matches[1]) as $value){
            $v = trim( $value, "'" );
            $enum[] = $v;
        }
        return $enum;
    }

}
