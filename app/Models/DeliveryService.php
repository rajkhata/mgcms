<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryService extends Model
{
    protected $fillable = [
        'id','provider_name','short_name','api_key','producer_key','order_creation_url','order_ready_url','order_cancel_url','msg_sent_url','status'
    ];
}
