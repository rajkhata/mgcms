<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryServicesLogs extends Model
{
    protected $table = 'delivery_services_logs';
    /**
}
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id','provider_id','order_id','order_source_id','order_key','restaurant_id','order_receipt','created_date','requested_by','request_data','response_data'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];
} 