<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $fillable = [ 'restaurant_id','language_id','menu_category_id','menu_sub_category_id','name','size_price','status','description','sub_description','thumb_image_med','image_caption','size_price','product_type','gift_wrapping_fees','gift_message','display_order','slot_code','is_popular','is_favourite'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    public function MenuCategory()
    {
        return $this->belongsTo('App\Models\MenuCategory','menu_category_id','id');
    }

    public function MenuMealTypePrice()
    {
        return $this->hasMany('App\Models\MenuMealTypePrice','menu_item_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
