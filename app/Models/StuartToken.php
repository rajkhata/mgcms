<?php

namespace App\Models;

 
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class StuartToken extends Model
{
 
    protected $table = 'stuart_token';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'access_token','access_token','expires_in'
    ];

     

}
