<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
	protected $primaryKey = 'id';
	
	protected $table = 'cms_fields';

	public function getFieldLabel($key)
    {
        return $this->where('key', $key)->value('label');
    }

}
