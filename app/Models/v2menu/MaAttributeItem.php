<?php

namespace App\Models\v2menu;

use Illuminate\Database\Eloquent\Model;

class MaAttributeItem extends Model
{
    protected $table = 'ma_attribute_item';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }	
}
