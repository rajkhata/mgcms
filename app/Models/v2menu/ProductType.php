<?php

namespace App\Models\v2menu;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class ProductType extends Authenticatable
{
    use Notifiable;

    protected $table = 'ma_product_type';

    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

     
}
