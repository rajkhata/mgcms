<?php

namespace App\Models\v2menu;

use Illuminate\Database\Eloquent\Model;

class MaAttributeSet extends Model
{
    protected $table = 'ma_attribute_set';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

    
}
