<?php

namespace App\Models\v2menu;

use Illuminate\Database\Eloquent\Model;

class MaAttribute extends Model
{
    protected $table = 'ma_attribute';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }	
    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }
}
