<?php

namespace App\Models\v2menu;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuCategoryLanguage;

class MenuCategory extends Model
{
      protected $table = 'ma_menu_categories';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

}
