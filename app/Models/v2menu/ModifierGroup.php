<?php

namespace App\Models\v2menu;

use Illuminate\Database\Eloquent\Model;

class ModifierGroup extends Model
{
    protected $table = 'v2_modifier_group';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];

   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }	
}
