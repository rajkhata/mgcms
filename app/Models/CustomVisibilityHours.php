<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomVisibilityHours extends Model
{
	protected $table = 'cms_restaurant_visibility_duration_custom_time';
    protected $fillable = ['restaurant_id', 'is_dayoff','calendar_date', 'slot_detail'];
}
?>