<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class UserOrderAddons extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_order_addons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_order_detail_id', 'user_order_id', 'menu_addons_id', 'menu_addons_option_id', 'addons_name',
        'addons_option', 'price', 'quantity', 'selection_type', 'priority', 'was_free'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
    public function getAddOnsdetail($Id)
    {
        return self::where('user_order_detail_id', $Id)->get();
    }

   /* public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
    public function order()
    {
        return $this->hasMany('App\Models\UserOrder');
    }

    public function address()
    {
        return $this->hasMany('App\Models\UserAddresses');
    }

    public function card()
    {
        return $this->hasMany('App\Models\UserCards');
    }*/
}
