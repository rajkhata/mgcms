<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuMealType extends Model
{
    protected $fillable = [ 'restaurant_id','language_id','name','status','from_time','to_time'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
