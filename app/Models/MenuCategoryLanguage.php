<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuCategory;

class MenuCategoryLanguage extends Model {

    protected $fillable = ['menu_categories_id', 'name', 'language_id', 'description', 'sub_description'];

    protected $table = 'menu_categories_language';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function menuCategory() {
        return $this->belongsTo(MenuCategory::class, 'menu_categories_id', 'id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
