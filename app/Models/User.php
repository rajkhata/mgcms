<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id', 'username', 'fname', 'lname', 'email', 'password', 'mobile', 'phone', 'display_pic_url', 'billing_address', 'shipping_address', 'user_group_id', 'status', 'stripe_customer_id', 'dob', 'doa', 'category', 'description','tags','items', 'language_id', 'is_registered'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','old_password',
    ];
    public function reservations()
    {
        return $this->hasMany('Modules\Reservation\Entities\Reservation', 'user_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }


    public function order()
    {
        return $this->hasMany('App\Models\UserOrder');
    }
    public function active_orders()
    {
        return $this->order()->whereNotIn('user_orders.status', ['pending']);;
    }

    public function address()
    {
        return $this->hasMany('App\Models\UserAddresses','user_id','id');
    }

    public function card()
    {
        return $this->hasMany('App\Models\UserCards');

    }

    public function orders()
    {
        return $this->hasMany('App\Models\UserOrder', 'user_id', 'id');
    }

}
