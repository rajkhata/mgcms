<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class CmsUser extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    
    protected $table = 'cms_users';
//    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password',
    ];*/

    protected $fillable = [
        'restaurant_id','state_id','city_id','password','salt','name','email','memail','phone','mobile','role','title','status','api_token'
    ];

    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    public function City()
    {
        return $this->belongsTo('App\Models\City','city_id','id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];*/
    protected $hidden = [
       'api_token', 'remember_token','created_at','updated_at'
    ];
}
