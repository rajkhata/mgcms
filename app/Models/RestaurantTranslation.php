<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantTranslation extends Model
{
    protected $fillable = ['restaurants_id','language_id','restaurant_name','description','address','street','source_url','borough','delivery_desc','notable_chef_desc','parking_desc'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurants_id','id');
    }

    public function Languages()
    {
        return $this->belongsTo('App\Models\Languages','language_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
