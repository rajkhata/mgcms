<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomDeliveryHours extends Model
{
	protected $table = 'cms_restaurant_delivery_custom_time';
    protected $fillable = ['restaurant_id', 'is_dayoff','calendar_date', 'slot_detail'];
}
?>