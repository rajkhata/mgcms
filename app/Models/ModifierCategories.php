<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Restaurant;

class ModifierCategories extends Model {
    
    public $timestamps = false;
    
    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    protected $table = 'modifier_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name', 'restaurant_id', 'is_required'
    ];

    public function restaurant() {
        return $this->belongsTo(Restaurant::class);
    }

}
