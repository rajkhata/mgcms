<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Language;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use DB;


class CmsUserActionLogs {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $activity = [];
        $user = auth()->user();
        $response = $next($request);
        $activity['user_id']=$user->id;
        $activity['user_name']=$user->name;
        $activity['user_email']=$user->email;
        
        $activity['request_url'] = $request->getUri();
        $activity['response_http_code'] = intval($response->getStatusCode());
        $activity['response_time'] = $this->getResponseDispatchTimeHumanized();
        $activity['response'] = $response->getContent();
        $activity['timestamp'] = date('Y-m-d H:i:s', strtotime('now'));
        $activity['payload'] = $this->getRequestPayload();
        
        if (\Request::isMethod('GET')) {
            $activity['method'] = "GET";
            $action = "List View";
        }
        if (\Request::isMethod('POST')) {
            $activity['method'] = "POST";
            $action = "Create Records";
        }
        
         if (\Request::isMethod('PATCH')) {
            $activity['method'] = "PUT";
            $action = "Update Records";
        }
        
        if (\Request::isMethod('PUT')) {
            $activity['method'] = "PUT";
             $action = "Update Records";
        }
        
        if (\Request::isMethod('DELETE')) {
            $activity['method'] = "DELETE";
             $action = "Delete Records";
        }
        
        $userActivity = json_encode($activity);
        $action_name = \Request::route()->getName();
        
       $header =[
            'user-agent'=>$request->header('user-agent'),
            'referer'=>$request->header('referer'),
            'host'=>$request->header('host')
               ];
       
        $activity['action_name'] = $action_name;
        $data = [
            'cms_user_id'=>$user->id,
            'cms_user_name'=>$user->name,
            'cms_user_email'=>$user->email,
            'method'=>$activity['method'],
            'action_name'=> $action_name,
            'action'=>$action,
            'ip_address'=>$request->ip(),
            'user_agent'=>json_encode($header,1),
            'activity'=>$userActivity
        ];
       
        DB::table("log_cmsuser_action")->insert($data);
        return $next($request);
        
//        $path = Route::getCurrentRoute()->getPath();
//        print($path);
        
       // $name = \Request::route()->getName();
        
       
      
    }
    
    
    protected function getResponseDispatchTimeHumanized()
    {
        $timeTaken = microtime(true) - LARAVEL_START;
        return number_format($timeTaken, 2) . ' seconds';
    }

    protected function getRequestPayload()
    {
        $payload = [];

        if (\Request::isMethod('GET')) {
            $payload = \Request::query();
        }

        if (\Request::isMethod('POST')) {
            $payload = array_merge($payload, \Request::input());
        }
        
        if (\Request::isMethod('PATCH')) {
            $payload = array_merge($payload, \Request::input());
        }
        
        if (\Request::isMethod('PUT')) {
            $payload = array_merge($payload, \Request::input());
        }
        
        if (\Request::isMethod('DELETE')) {
            $payload = array_merge($payload, \Request::input());
        }

        return $payload;
     }
       
    

}
