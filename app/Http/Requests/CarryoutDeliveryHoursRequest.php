<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarryoutDeliveryHoursRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slot_time' => 'required',
           
        ];
    }

    public function messages()
    {
        return [
            'slot_time.required' => 'Slot time should not be empty',
           // 'slot_name.max' => 'Slot name length cannot exceed 20 character',
           
        ];
    }
}
