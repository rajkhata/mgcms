<?php
function rangeMonth ($datestr) {
    date_default_timezone_set (date_default_timezone_get());
    $dt = strtotime ($datestr);
    return array (
        date ('Y-m-d', strtotime ('first day of this month', $dt)),
        date ('Y-m-d', strtotime ('last day of this month', $dt))
    );
}

function rangeWeek ($datestr){
    date_default_timezone_set(date_default_timezone_get());
    $dt = strtotime($datestr);
    return array(
        date('N', $dt) == 1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt)),
        date('N', $dt) == 7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt))
    );
}

function dateDiff($start_date, $end_date){
    $start_date = new DateTime($start_date);
    $end_date = new DateTime($end_date);
    $days = $end_date->diff($start_date)->format('%a');
    return $days+1;
}

/*
 * $time format is H:i:s
 */
function addMinutesInTime($time, $time_duration)
{
    return date('H:i', strtotime("$time_duration minutes", strtotime($time)));
}

function getDateTime($date, $time)
{
    return date('Y-m-d H:i:s', strtotime("$date $time"));
}

function getDayOfWeekByDate($date)
{
    return strtolower(date("D", strtotime($date)));
}

function getSlotsFromTimeRange($start_time, $end_time, $slot_interval, $format_am_pm=false)
{
    $array_of_time = array();
    $start_time = strtotime($start_time); //change to strtotime
    $end_time = strtotime($end_time); //change to strtotime
    $add_mins = $slot_interval * 60;
    while ($start_time < $end_time) // loop between time
    {
        if($format_am_pm){
            $array_of_time[] = date("h:i A", $start_time);
        }else{
            $array_of_time[] = date("H:i", $start_time);
        }
        $start_time += $add_mins; // to check endtie=me
    }
    return $array_of_time;
}

function getSlotsRangeFromTimeRange($start_time, $end_time, $slot_interval)
{
    $array_of_time = array();
    $start_time = strtotime($start_time); //change to strtotime
    $end_time = strtotime($end_time); //change to strtotime
    $add_mins = $slot_interval * 60;
    while ($start_time < $end_time) // loop between time
    {
        $start_time_range = date("H:i", $start_time);
        $start_time += $add_mins; // to check endtie=me
        $array_of_time[] = $start_time_range.'-'.date("H:i", $start_time);
    }
    return $array_of_time;
}

function getSlotWithIntevals($start_time, $end_time, $slot_interval)
{
    $array_of_time = array();
    $start_time = strtotime($start_time); //change to strtotime
    $end_time = strtotime($end_time); //change to strtotime
    $add_mins = $slot_interval * 60;

    $array_of_time[]=date("H:i", $start_time);
    while ($start_time < $end_time) // loop between time
    {
        $start_time_range = date("H:i", $start_time);
        $start_time += $add_mins; // to check endtie=me
        $array_of_time[] = date("H:i", $start_time);
    }
    array_pop($array_of_time);
    return $array_of_time;
}

function timeBefore($time, $time_blocks) {
    $time = strtotime($time);
    $closest_before = NULL;
    foreach ($time_blocks as $t=>$time_var) {
        $time_var['time'] = strtotime($time_var['time']);
        if ($time_var['time'] <= $time) {
            if (!$closest_before) {
                $closest_before = $time_var['time'];
            } else {
                if ($time_var['time'] > $closest_before) {
                    $closest_before = $time_var['time'];
                }
            }
        }
    }
    $closest_before = ($closest_before) ? date('H:i', $closest_before): '';
    return $closest_before;
}

function timeAfter($time, $time_blocks) {
    $time = strtotime($time);
    $closest_after = NULL;
    foreach ($time_blocks as $t=>$time_var) {
        $time_var['time'] = strtotime($time_var['time']);
        if ($time_var['time'] > $time) {
            if (!$closest_after) {
                $closest_after = $time_var['time'];
            } else {
                if ($time_var['time'] < $closest_after) {
                    $closest_after = $time_var['time'];
                }
            }
        }
    }
    $closest_after = ($closest_after) ? date('H:i', $closest_after): '';
    return $closest_after;
}

function getTableIDAndCountArr($table_availability)
{
    $tableCountArr = array();
    $table_availability = json_decode($table_availability, true);
    foreach ($table_availability as $table){
        if($table['table_count']>0){
            $tableCountArr[$table['table_type_id']] = $table['table_count'];
        }
    }
    return $tableCountArr;
}

function isInputTimeExistInRange($start_end_time_range, $input_time)
{
    $start_end_time_range = explode('-', $start_end_time_range);
    $start_time = strtotime($start_end_time_range[0]);
    $end_time = strtotime($start_end_time_range[1]);
    $input_time = strtotime($input_time);
    if ($start_time - $input_time <= 0 && $input_time - $end_time < 0)
    {
        return true;
    }
    return false;
}

/*
 * this function check time fall in time range
 * $start_end_time_range = 08:00-10:00
 * $input_time = 09:00
 * return true
 */
function isInputTimeExistInTimeRange($start_end_time_range, $input_time)
{
    $start_end_time_range = explode('-', $start_end_time_range);
    $start_time = strtotime($start_end_time_range[0]);
    $end_time = strtotime($start_end_time_range[1]);
    $input_time = strtotime($input_time);
    if ($start_time - $input_time <= 0 && $input_time - $end_time <= 0)
    {
        return true;
    }
    return false;
}

function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}

function isStartTimeEndTimeExistInTimeRangeArr($start_end_time_range_arr, $start_time, $end_time)
{
    $start_end_time_range_arr = array_unique($start_end_time_range_arr);
    foreach ($start_end_time_range_arr as $start_end_time_range_key=>$start_end_time_range){
        if(isInputTimeExistInTimeRange($start_end_time_range, $start_time) && isInputTimeExistInTimeRange($start_end_time_range, $end_time)){
            return true;
        }
    }
    return false;
}

function rangesNotOverlapClosed($start_time1,$end_time1,$start_time2,$end_time2)
{
    $utc = new DateTimeZone('UTC');

    $start1 = new DateTime($start_time1,$utc);
    $end1 = new DateTime($end_time1,$utc);
    if($end1 < $start1)
        //throw new Exception('Range is negative.');
        return true;
    $start2 = new DateTime($start_time2,$utc);
    $end2 = new DateTime($end_time2,$utc);
    if($end2 < $start2)
        //throw new Exception('Range is negative.');
        return true;
    return ($end1 <= $start2) || ($end2 <= $start1);
}

function rangesNotOverlapOpen($start_time1,$end_time1,$start_time2,$end_time2)
{
    $utc = new DateTimeZone('UTC');

    $start1 = new DateTime($start_time1,$utc);
    $end1 = new DateTime($end_time1,$utc);
    if($end1 < $start1)
        return true;
        //throw new Exception('Range is negative.');

    $start2 = new DateTime($start_time2,$utc);
    $end2 = new DateTime($end_time2,$utc);
    if($end2 < $start2)
        return true;
        //throw new Exception('Range is negative.');

    return ($end1 <= $start2) || ($end2 <= $start1);
}

function slot_overlap_check($start_time1,$end_time1,$start_time2,$end_time2)
{
    $utc = new DateTimeZone('UTC');

    $start1 = new DateTime($start_time1,$utc);
    $end1 = new DateTime($end_time1,$utc);
    if($end1 < $start1)
        //throw new Exception('Range is negative.');
        return 0;
    $start2 = new DateTime($start_time2,$utc);
    $end2 = new DateTime($end_time2,$utc);
    if($end2 < $start2)
        //throw new Exception('Range is negative.');
        return 0;
    if((($start2>=$start1 && $start2<$end2) || ($end2>$start1 && $end2<=$end2) ) || ($start2==$start1 && $end2==$end2)  || ($start2 < $start1 && $end2>$end2)){
     
           return 1;
    }else{

        return  0;
    }                

}

 function generateReservationReceipt() {
        $timestamp = date('mdhis');
        $keys = rand(0, 9);
        $randString = 'M' . $timestamp . $keys;
        return $randString;
}

function getSpecialOccasionNameAndIcon($special_occasion){
    $special_occasion_info['icon'] = '';
    $special_occasion_info['name'] = '';
    if($special_occasion){
        $special_occasion_info['name'] = $special_occasion;
        if($special_occasion == config('reservation.special_occasion.anniversary') || $special_occasion == config('reservation.special_occasion.birthday')){
            $special_occasion_info['icon'] = 'icon-birthday';
        } else if($special_occasion == config('reservation.special_occasion.business_meal')){
            $special_occasion_info['icon'] = 'icon-business-meal';
        } else if($special_occasion == config('reservation.special_occasion.date_night')){
            $special_occasion_info['icon'] = 'icon-date-time';
        } else if($special_occasion == config('reservation.special_occasion.celebration')){
            $special_occasion_info['icon'] = 'icon-celebration';
        } else {
            $special_occasion_info['icon'] = 'icon-anniversary';
        }
    }
    return $special_occasion_info;
}

function getCustomerTypeAndIcon($customer_type){
    $customer_type_info['icon'] = '';
    $customer_type_info['name'] = '';
    if($customer_type == config('reservation.guest_category.VIP')){
        $customer_type_info['icon'] = 'icon-vip';
        $customer_type_info['name'] = $customer_type;
    }
    return $customer_type_info;
}
/*
 * generate random unique color
 */
function randomHex() {
    //$chars = 'AZCXEQ0123';
    /*$color = '#';
    for ( $i = 0; $i < 6; $i++ ) {
        $color .= $chars[rand(0, strlen($chars) - 1)];
    }*/
    $color = '#'.substr(uniqid(),-6);
    return $color;
}
/*
 * this function will return array of first and last activity
 * e.g
 * ['first_activity_date' => '2018-09-08 13:00:00', 'last_activity_date']
 */
function getFirstAndLastActivityDates($userObj)
{
    $firstAndLastActivityArr = [];
    $first_reservation_activity = (isset($userObj->reservations) && count($userObj->reservations) > 0) ? $userObj->reservations[count($userObj->reservations) - 1]->created_at : null; // PE-2462
    $first_order_activity = (isset($userObj->orders) &&  count($userObj->orders) > 0) ? $userObj->orders[count($userObj->orders) - 1]->created_at : null;

    $last_reservation_activity = (isset($userObj->reservations) && count($userObj->reservations) > 0) ? $userObj->reservations[0]->created_at : null; // PE-2462 - $userObj->reservations[0]->start_time
    $last_order_activity = (isset($userObj->orders) && count($userObj->orders) > 0) ? $userObj->orders[0]->created_at : null;

    $firstAndLastActivityArr['last_activity_date'] = ($last_reservation_activity && $last_order_activity) ? ((\Carbon\Carbon::parse($last_reservation_activity) > \Carbon\Carbon::parse($last_order_activity)) ? $last_reservation_activity : $last_order_activity) : ($last_reservation_activity ? $last_reservation_activity : ($last_order_activity ? $last_order_activity : null));

    $firstAndLastActivityArr['first_activity_date'] = ($first_reservation_activity && $first_order_activity) ? ((\Carbon\Carbon::parse($first_reservation_activity) < \Carbon\Carbon::parse($first_order_activity)) ? $first_reservation_activity : $first_order_activity) : ($first_reservation_activity ? $first_reservation_activity : ($first_order_activity ? $first_order_activity : $firstAndLastActivityArr['last_activity_date']));

    $order_activity_months = $reservation_activity_months = 1;
    if($last_order_activity && $first_order_activity){
        $order_activity_months = count(range(\Carbon\Carbon::parse($first_order_activity)->month, \Carbon\Carbon::parse($last_order_activity)->month));
    }
    if($first_reservation_activity && $last_reservation_activity){
        $reservation_activity_months = count(range(\Carbon\Carbon::parse($first_reservation_activity)->month, \Carbon\Carbon::parse($last_reservation_activity)->month));
    }
    $firstAndLastActivityArr['frequency_reservations'] = (isset($userObj->reservations) && count($userObj->reservations) > 0) ? count($userObj->reservations)/$reservation_activity_months:0;
    $firstAndLastActivityArr['frequency_orders'] = (isset($userObj->orders) && count($userObj->orders) > 0) ? count($userObj->orders)/$order_activity_months:0;

    return $firstAndLastActivityArr;
}

function checkEndTimeExceedFromRange($start_end_time_range_arr, $start_time, $end_time)
{
    $end_time_exceed_flag = true;
    $start_time_exist = false;
    $start_end_time_range_arr = array_unique($start_end_time_range_arr);
    foreach ($start_end_time_range_arr as $start_end_time_range_key => $start_end_time_range) {
        if (isInputTimeExistInTimeRange($start_end_time_range, $start_time)) {
            $start_time_exist = true;
            if (isInputTimeExistInTimeRange($start_end_time_range, $end_time)) {
                $end_time_exceed_flag = false;
            }
        }
    }
    if($start_time_exist){
        return $end_time_exceed_flag;
    }
    return false;
}

function hex2rgba($color, $opacity = false) {

    $default = 'rgb(0,0,0)';

    //Return default if no color provided
    if(empty($color))
        return $default;

    //Sanitize $color if "#" is provided
    if ($color[0] == '#' ) {
        $color = substr( $color, 1 );
    }

    //Check if color has 6 or 3 characters and get values
    if (strlen($color) == 6) {
        $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
    } elseif ( strlen( $color ) == 3 ) {
        $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
    } else {
        return $default;
    }

    //Convert hexadec to rgb
    $rgb =  array_map('hexdec', $hex);

    //Check if opacity is set(rgba or rgb)
    if($opacity){
        if(abs($opacity) > 1)
            $opacity = 1.0;
        $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
    } else {
        $output = 'rgb('.implode(",",$rgb).')';
    }

    //Return rgb(a) color string
    return $output;
}

function isBetween($from, $till, $input) {
    $fromTime = strtotime($from);
    $toTime = strtotime($till);
    $inputTime = strtotime($input);

    return($inputTime >= $fromTime and $inputTime <= $toTime);
}

function compareDeepValues($val1, $val2)
{
    return strcmp(serialize($val1), serialize($val2));
}