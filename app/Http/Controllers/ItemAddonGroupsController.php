<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\Models\ItemAddonGroups;
use App\Models\Restaurant;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use File;
use Config;

class ItemAddonGroupsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $inputData = [
            'restaurant_id' => 0,
            'group_name' => ''
        ];

        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();

        if ($restaurantData && count($restaurantData) == 1) {
            $inputData['restaurant_id'] = $restaurantData[0]['id'];
        }

        $rules = [
            'group_name' => 'sometimes|nullable|max:100',
            'restaurant_id' => 'sometimes|nullable'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $itemAddonGroupObj = ItemAddonGroups::orderBy('id', 'DESC');

            if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
                $inputData['restaurant_id'] = $request->input('restaurant_id');
                $itemAddonGroupObj->where('restaurant_id', $request->input('restaurant_id'));
            }
            if ($request->input('group_name') && is_string($request->input('group_name'))) {
                $inputData['group_name'] = $request->input('group_name');
                $itemAddonGroupObj->where('group_name', 'like', '%'. $request->input('group_name').'%');
            }

            $itemAddonGroups = $itemAddonGroupObj->paginate(20);
            return view('item_addon_groups.index', compact('itemAddonGroups', 'restaurantData'))->with('inputData', $inputData);
        }

        return redirect()->back()->withErrors($validator->errors())->with('inputData', $inputData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
        $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();

        return view('item_addon_groups.create', compact('restaurantData', 'languageData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = [
            'group_name' => 'required|max:100',
            'prompt' => 'sometimes|string|max:100',
            'restaurant_id' => 'required|numeric',
            'language_id' => 'required|numeric',
            'is_required' => 'required|numeric|min:0|max:1',
            'quantity' => 'required|numeric|min:1',
            'quantity_type' => 'required|string|in:Value,Range',
        ];

        $validator = Validator::make(Input::all(), $rules, [
                    'language_id.*' => 'Please select valid Language',
                    'restaurant_id.*' => 'Please select Restaurant',
        ]);

        if ($validator->passes()) {
            $itemAddonGroupObj = ItemAddonGroups::create(Input::all());
            return redirect()->back()->with('message', 'Item addon group added successfully');
        }
        
        return redirect()->back()->withErrors($validator->errors())->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (isset($id) && is_numeric($id)) {
            $itemAddonGroup = ItemAddonGroups::find($id);
            $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
            $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();
            
            return view('item_addon_groups.edit', compact('itemAddonGroup', 'restaurantData', 'languageData'));
        }
        return Redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (isset($id) && is_numeric($id)) {
            $rules = [
                'group_name' => 'required|max:100',
                'prompt' => 'sometimes|string|max:100',
                'restaurant_id' => 'required|numeric',
                'language_id' => 'required|numeric',
                'is_required' => 'required|numeric|min:0|max:1',
                'quantity' => 'required|numeric|min:1',
                'quantity_type' => 'required|string|in:Value,Range',
            ];

            $validator = Validator::make(Input::all(), $rules, [
                        'language_id.*' => 'Please select valid Language',
                        'restaurant_id.*' => 'Please select Restaurant',
            ]);

            if ($validator->passes()) {
                $inputArr = Input::except('_method', '_token', 'id');
                ItemAddonGroups::where('id', $id)->update($inputArr);
                return redirect()->back()->with('message', 'Item addon group updated successfully');
            } else {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
        }
        return Redirect::back()->with('message', 'Invalid Id');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {   $id = Input::get('id');
        if(isset($id) && is_numeric($id)) {
            $catgObj = ItemAddonGroups::find($id);
            if (!empty($catgObj)) {
                $catgObj->delete();
                return Redirect::back()->with('message', 'Item addon group deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg','Invalid Id');
    }
}
