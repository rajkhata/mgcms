<?php

namespace App\Http\Controllers;

use App\Models\MenuCategory;
use App\Models\MenuItem;
use App\Models\MenuMealType;
use App\Models\MenuMealTypePrice;
use App\Models\MenuSettingCategory;
use App\Models\MenuSettingItem;
use App\Models\MenuSubCategory;
use App\Models\Restaurant;
use App\Modesl\RestaurantPromotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Helpers\CommonFunctions;
use File;
use App\Models\Language;
use App\Models\VisibilityHours;


class MenuItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan view:clear');
        //exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan cache:clear');
    }

    public function index(Request $request)
    {
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        #$groupRestData = Restaurant::group();
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $menuItems = MenuItem::orderBy('menu_category_id', 'DESC')->whereIn('restaurant_id', $locations)->orderBy('id', 'DESC');
        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuItems->where('restaurant_id', $searchArr['restaurant_id']);
            //$menuItems = MenuItem::where('restaurant_id',$searchArr['restaurant_id'])->orderBy('category_id','DESC')->orderBy('id','DESC')->paginate(20);
        }
        if ($request->input('name') && strlen($request->input('name')) <= 100) {
            $searchArr['name'] = $request->input('name');
            $menuItems->where('name', 'like', $searchArr['name'] . '%');
        }

        $menuItems = $menuItems->where('product_type', 'food_item');// Get Product Type Food Item

        $menuItems = $menuItems->paginate(20);
        return view('menu_item.index', compact('menuItems', 'groupRestData', 'searchArr', 'selected_rest_id'));
    }

    public function product(Request $request)
    {
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        #$groupRestData = Restaurant::group();
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $menuItems = MenuItem::orderBy('menu_category_id', 'DESC')->whereIn('restaurant_id', $locations)->orderBy('id', 'DESC');
        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuItems->where('restaurant_id', $searchArr['restaurant_id']);
            //$menuItems = MenuItem::where('restaurant_id',$searchArr['restaurant_id'])->orderBy('category_id','DESC')->orderBy('id','DESC')->paginate(20);
        }
        if ($request->input('name') && strlen($request->input('name')) <= 100) {
            $searchArr['name'] = $request->input('name');
            $menuItems->where('name', 'like', $searchArr['name'] . '%');
        }

        $menuItems = $menuItems->where('product_type', 'product');// Get Product Type product

        $menuItems = $menuItems->paginate(20);
        return view('menu_item.index', compact('menuItems', 'groupRestData', 'searchArr', 'selected_rest_id'));
    }

    public function giftcard(Request $request)
    {
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        #$groupRestData = Restaurant::group();
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $menuItems = MenuItem::orderBy('menu_category_id', 'DESC')->whereIn('restaurant_id', $locations)->orderBy('id', 'DESC');
        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuItems->where('restaurant_id', $searchArr['restaurant_id']);
            //$menuItems = MenuItem::where('restaurant_id',$searchArr['restaurant_id'])->orderBy('category_id','DESC')->orderBy('id','DESC')->paginate(20);
        }
        if ($request->input('name') && strlen($request->input('name')) <= 100) {
            $searchArr['name'] = $request->input('name');
            $menuItems->where('name', 'like', $searchArr['name'] . '%');
        }

        $menuItems = $menuItems->where('product_type', 'gift_card');// Get Product Type giftcard

        $menuItems = $menuItems->paginate(20);
        return view('menu_item.index', compact('menuItems', 'groupRestData', 'searchArr', 'selected_rest_id'));
    }

    public function giftCardDetail($id)
    {
        return view('gift_cards.detail');
    }

    public function create()
    {
        #$groupRestData = Restaurant::group();
        $groupRestData = CommonFunctions::getRestaurantGroup();
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        $selected_rest_id = 0;
        if(count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if(count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        //echo $selected_rest_id; die;
        $categoryData = MenuCategory::whereIn('restaurant_id', $locations)->get();
        $product_types = config('constants.product_type');
        return view('menu_item.create', compact('groupRestData', 'selected_rest_id','product_types', 'languageData','categoryData'));
    }

    public function store(Request $request)
    {
        // $typeArr = array_values($request->input('type'));
        // $priceArr = array_values($request->input('price'));
        // $request->merge(array('type' => $typeArr));
        // $request->merge(array('price' => $priceArr));
        $rules = [
            'name' => 'required|max:100', 
            'product_type' => 'required',           
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'status' => 'required|in:0,1',
            //'image_caption' => 'required|max:100',
            'type' => 'required|array',
           // 'description' => 'required|max:250',
            //'image' => 'required',
            'size' => 'sometimes|nullable',
            'menu_category_id' => 'required|numeric|exists:menu_categories,id',
            //'sub_category_id' => 'numeric|exists:menu_sub_categories,id',
            //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:'.config('constants.image.file_size'),
        ];
        $sizeCount = 0;
        if($request->input('size') && is_Array($request->input('size')) )
        {
            $sizeCount = count($request->input('size'));
            $sizeObj = $request->input('size');
             //size validation commneted on 13-09-2018 By RG as we do not require size for some item
            // foreach ($sizeObj as $k=>$v)
            // {
            //     $rules['size.'.$k] = 'required|max:100';
            //     $rules['type.'.$k] = 'required|in:0,1';
            // }
        }

        if($request->input('type') && is_Array($request->input('type')) )
        {
            $typeObj = $request->input('type');
            foreach ($typeObj as $k=>$v)
            {
                if($v=='1')
                {
                    $rules['price_all.'.$k] = 'required|numeric';
                }
                else if($v=='0')
                {
                    $rules['meal_type.'.$k] = 'required|array';
                }
            }
        }

        if($request->input('meal_type') && is_Array($request->input('meal_type')) )
        {
            $mealTypeObj = $request->input('meal_type');
            foreach ($mealTypeObj as $k=>$v)
            {
                foreach($v as $k1=>$v1)
                {
                    $rules['price.'.$k.'.'.$k1] = 'required|numeric';
                }
            }
        }

        if($request->input('price'))
        {
            $priceObj = $request->input('price');
            foreach($priceObj as $k => $v){
                foreach($v as $k1 => $v1) {
                    if(!is_null($v1) && trim($v1)!='')
                        $rules['meal_type.' . $k.'.'.$k1] = 'required|numeric';
                }
            }
        }
        if($request->input('meal_type'))
        {
            $mealTypeObj = $request->input('meal_type');
            foreach($mealTypeObj as $k => $v){
                foreach($v as $k1 => $v1) {
                    $rules['price.' . $k.'.'.$k1] = 'required|numeric';
                }
            }
        }
        $rulesMsg = [
            'name.*' => 'Please enter valid name upto 100 chars',
            'product_type' => 'Please select Product Type',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status',
            'image_caption.*' => 'Please enter Image Caption upto 100 chars',
            'meal_type.*' => 'Please select Meal Type',
            'image.image' => 'Please select valid Image',
            'image.mimes' => 'Please select valid Image Type',
            'image.max' => 'Please select size up to'.config('constants.image.file_size'),
            'image.*' => 'Please select Image',
            'price.*' => 'Please enter Price',
            'price_all.*' => 'Please enter numeric Price',
            'type.*' => 'Please select Price Type',
            //'description.required' => 'Please enter Description',
           // 'description.max' => 'Please enter Description upto 250 chars',
            'size.distinct' => 'Sizes cannot be same',
            'size.*' => 'Please enter size upto 100 chars'
        ];

        $validator = Validator::make(Input::all(), $rules, $rulesMsg);
        if ($validator->passes())
        {
            $img_err = 0;
            $max_file_size = config('constants.image.file_size');
            $allowed_image_extension = array("png", "jpg", "jpeg", "gif","svg");
            $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
            if ($request->hasFile('image')) {
                $imageCaptionArr = $request->input('image_caption');
                foreach ($request->file('image') as $k => $image) {
                    if (!file_exists($image->getRealPath())) {
                        $validator->errors()->add('image', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('image', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('image', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
                    else if(!isset($imageCaptionArr[$k]))
                    {
                        $validator->errors()->add('image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(trim($imageCaptionArr[$k])=='' ){
                        $validator->errors()->add('image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(strlen(trim($imageCaptionArr[$k]))>100){
                        $validator->errors()->add('image_caption', 'Please add Image Caption upto 100 chars.');
                        $img_err = 1;
                    }
                }
            }
            if ($request->hasFile('web_image')) {
                $imageCaptionArr = $request->input('web_image_caption');
                foreach ($request->file('web_image') as $k => $image) {
                    if (!file_exists($image->getRealPath())) {
                        $validator->errors()->add('image', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('web_image_caption', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('web_image_caption', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
                    else if(!isset($imageCaptionArr[$k]))
                    {
                        $validator->errors()->add('web_image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(trim($imageCaptionArr[$k])=='' ){
                        $validator->errors()->add('web_image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(strlen(trim($imageCaptionArr[$k]))>100){
                        $validator->errors()->add('web_image_caption', 'Please add Image Caption upto 100 chars.');
                        $img_err = 1;
                    }
                }
            }
            if ($request->hasFile('mobile_web_image')) {
                $imageCaptionArr = $request->input('mobile_web_image_caption');
                foreach ($request->file('mobile_web_image') as $k => $image) {
                    if (!file_exists($image->getRealPath())) {
                        $validator->errors()->add('image', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('mobile_web_image_caption', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('mobile_web_image_caption', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
                    else if(!isset($imageCaptionArr[$k]))
                    {
                        $validator->errors()->add('mobile_web_image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(trim($imageCaptionArr[$k])=='' ){
                        $validator->errors()->add('mobile_web_image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(strlen(trim($imageCaptionArr[$k]))>100){
                        $validator->errors()->add('mobile_web_image_caption', 'Please add Image Caption upto 100 chars.');
                        $img_err = 1;
                    }
                }
            }
            if($img_err == 0) {
                $itemData = ['restaurant_id' => $request->input('restaurant_id'),'language_id' => $request->input('language_id'),
                    'menu_category_id' => $request->input('menu_category_id'),
                    'menu_sub_category_id' => $request->input('menu_sub_category_id'),
                    'name' => $request->input('name'),
                    'product_type' => $request->input('product_type'),
                    'status' => $request->input('status'),
                    'description' =>!empty($request->input('description'))?$request->input('description'):'',
                    'sub_description' =>!empty($request->input('sub_description'))?$request->input('sub_description'):'',
                    'slot_code' => $request->input('slot_code'),
                    'price_flag' => $request->input('type')
                ];

                $sizeObj = $request->input('size');
                $typeObj = $request->input('type');
                $priceObj = $request->input('price');
                $priceAllObj = $request->input('price_all');
                if($request->has('is_popular') && $request->input('is_popular') == 'on' ) {
                    $itemData['is_popular']  = 1;
                }else {
                    $itemData['is_popular']  = 0;
                }
                if($request->has('is_favourite') && $request->input('is_favourite') == 'on' ) {
                    $itemData['is_favourite']  = 1;
                }else {
                    $itemData['is_favourite']  = 0;
                }
                $cnt = 0;
                $sizePriceArr = array();
                foreach($sizeObj as $k=>$v)
                {
                    if($typeObj[$k]=="1")
                    {
                        $priceTmpArr = $availableTmpArr = array();
                        $sizePriceArr[$cnt]['size'] = $v;
                        $priceTmpArr[0] = $priceAllObj[$k];
                        //$sizePriceArr[$cnt]['price'] = $priceAllObj[$k];
                        //$sizePriceArr[$cnt]['menu_meal_type_id'] = 0;
                        //$cnt++;
                        if($request->has('carryout_all') && isset($request->input('carryout_all')[$k])) {
                            $availableTmpArr['is_carryout'] = 1;
                        }else {
                            $availableTmpArr['is_carryout'] = 0;
                        }

                        //set is_delivery and is_carrayout flag
                        if($request->has('delivery_all') && isset($request->input('delivery_all')[$k])) {
                            $availableTmpArr['is_delivery'] = 1;
                        }else {
                            $availableTmpArr['is_delivery'] = 0;
                        }
                    }
                    else
                    { 
                        $mealTypeObj = array();
                        if($request->input('meal_type'))
                        {
                            if(isset($request->input('meal_type')[$k]))
                                $mealTypeObj = $request->input('meal_type');
                        }
                        $sizePriceArr[$cnt]['size'] = $v;
                        $priceTmpArr = array(); //echo "<pre>";print_r($request->all());die;
                        foreach($mealTypeObj[$k] as $k1=>$v1)
                        {

                            $priceTmpArr[$v1] = $priceObj[$k][$k1];
                            //$sizePriceArr[$cnt]['price'] = $priceObj[$k][$k1];
                            //$sizePriceArr[$cnt]['menu_meal_type_id'] = $v1;
                           
                            if($request->has('carryout') && isset($request->input('carryout')[$k]) && array_key_exists($k1, $request->input('carryout')[$k])) {
                                $availableTmpArr[$v1]['is_carryout'] = 1;
                            }else {
                                $availableTmpArr[$v1]['is_carryout'] = 0;
                            }

                            if($request->has('delivery') && isset($request->input('delivery')[$k]) && array_key_exists($k1, $request->input('delivery')[$k])) {
                                 $availableTmpArr[$v1]['is_delivery'] = 1;
                            }else {
                                $availableTmpArr[$v1]['is_delivery'] = 0;
                            }
                        }
                    }
                    $sizePriceArr[$cnt]['price'] = (array) $priceTmpArr;
                    $sizePriceArr[$cnt]['available'] = (array)$availableTmpArr;
                    $cnt++;
                }
                // echo "<pre>";
                // print_r($sizePriceArr);
                // exit;
                $itemData['size_price'] = json_encode($sizePriceArr);

                $itemData['gift_wrapping_fees']=$request->input('gift_wrapping_fees')?$request->input('gift_wrapping_fees'):NULL;
                $itemData['gift_message']=$request->input('gift_message')?$request->input('gift_message'):NULL;
                $itemData['display_order']=$request->input('display_order')?$request->input('display_order'):0;

                
                $menuItemObj = MenuItem::create($itemData);



                if (isset($menuItemObj->id) && $request->input('type')) {                         

                    // image upload
                    $imageArr = [];
                    $imageJsonArray = [];
                    $imageRelPath = config('constants.image.rel_path.menu');
                    $imagePath = config('constants.image.path.menu');

                    $curr_restaurant_name=strtolower(str_replace(' ','-',$menuItemObj->Restaurant->restaurant_name));
                    $imagePath=$imagePath.DIRECTORY_SEPARATOR.$curr_restaurant_name;
                    $imageRelPath=$imageRelPath.$curr_restaurant_name. DIRECTORY_SEPARATOR;

                    (! File::exists($imagePath) ?File::makeDirectory($imagePath):'');
                    (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'thumb') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'thumb' ):'');

                    if ($request->hasFile('image')) {
                        
                        $mobile_app_images=[];
                        $i = 0;
                        $imageCaptionArr = $request->input('image_caption');
                     
                        foreach ($request->file('image') as $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                             $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);

                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);
                            
                            //$image->move($imagePath, $imageName);
                            $imageArr[] = [
                                'image' => $imageRelPath . $imageName,
                                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'image_caption' => $imageCaptionArr[$i],
                            ];

                            $mobile_app_images[]= [
                                'image' => $imageRelPath . $imageName,
                                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'image_caption' => $imageCaptionArr[$i],
                            ];
                                
                            /* Old field for saving mobile app images   
                            if (count($imageArr) > 0) {
                                MenuItem::where('id', $menuItemObj->id)
                                    ->update($imageArr[0]);
                            }
                            */
                            $i++;
                        }

                        $imageJsonArray['mobile_app_images']=$mobile_app_images;

                    }
                    // Desktop images
                    if ($request->hasFile('web_image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('web_image_caption');
                       $web_images=[];
                        foreach ($request->file('web_image') as $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                              $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();
                                  

                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                            //$image->move($imagePath, $imageName);
                            $imageWebArr[] = [
                                'web_image' => $imageRelPath . $imageName,
                                'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'web_thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'web_image_caption' => $imageCaptionArr[$i],
                            ];
                             $web_images[]= [
                                'web_image' => $imageRelPath . $imageName,
                                'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'web_thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'web_image_caption' => $imageCaptionArr[$i],
                            ];
                            /*Old field for saving web images 
                            if (count($imageWebArr) > 0) {
                                MenuItem::where('id', $menuItemObj->id)
                                    ->update($imageWebArr[0]);
                            }*/
                            $i++;
                        }

                        $imageJsonArray['desktop_web_images']=$web_images;

                    }
                    // mobile web
                    if ($request->hasFile('mobile_web_image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('mobile_web_image_caption');
                        $mobile_web_images=[];
                        foreach ($request->file('mobile_web_image') as $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                            $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);
                            
                            //$image->move($imagePath, $imageName);
                            $imageMobWebArr[] = [
                                'mobile_web_image' => $imageRelPath . $imageName,
                                'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'mobile_web_thumb_sml' =>$imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'mobile_web_image_caption' => $imageCaptionArr[$i],
                            ];
                             $mobile_web_images[]= [
                                'mobile_web_image' => $imageRelPath . $imageName,
                                'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'mobile_web_thumb_sml' =>$imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'mobile_web_image_caption' => $imageCaptionArr[$i],
                            ];
                            
                            /*Old field for saving mobile web images 
                            if (count($imageMobWebArr) > 0) {
                                MenuItem::where('id', $menuItemObj->id)
                                    ->update($imageMobWebArr[0]);
                            }
                            */
                            $i++;
                        }
                         $imageJsonArray['mobile_web_images']=$mobile_web_images;

                    }

                    MenuItem::where('id', $menuItemObj->id)
                                    ->update(['images'=>json_encode($imageJsonArray)]);
                    // CUSTOMIZABLE MENU DATA >>>
                    $menuItemArr = [];
                    $restId = $request->input('restaurant_id');
                    $catId = $request->input('menu_category_id');
                    $menuSetCat = MenuSettingCategory::select('id', 'name', 'is_multi_select','maximum_selection','minimum_selection','equal_selection')->where(['status' => 1, 'restaurant_id' => $restId,'menu_category_id' => $catId])->orderBy('display_order', 'ASC')->get()->toArray();

                    foreach($menuSetCat as $menuSet) {
                        $isMandatory = 0;
                        $menuSetName = str_replace(' ', '_', $menuSet['name']);
                        if(isset($_POST[$menuSet['id'].'_is_mandatory'])) {
                            $isMandatory = 1;
                        }

                        if(isset($_POST[$menuSetName]) && count($_POST[$menuSetName])) {
                            // get price of Customization setting item
                            $menuSetItemDBArr = [];
                            $res = MenuSettingItem::select('id','name','price')->whereIn('id', $_POST[$menuSetName])->orderBy('display_order', 'ASC')->get()->toArray();
                            array_walk($res, function($item) use (&$menuSetItemDBArr) { $menuSetItemDBArr[$item['id']] = $item; });

                            // check ids in POST menuSetName
                            foreach($request->input($menuSetName) as $menuSetItemId) {
                                $quantArr = [];
                                $sidesArr = [];
                                $defaultSelected = 0;
                                foreach(config('constants.sides') as $side) {
                                    if(isset($_POST[$menuSetItemId.'_'.$side])) {
                                        $sidesArr[] = ['label' => $side, 'is_selected' => 1];
                                    }
                                }
                                if(!count($sidesArr)) {
                                    $sidesArr = null;
                                }
                                foreach(config('constants.quantity') as $quant) {
                                    if(isset($_POST[$menuSetItemId.'_'.$quant])) {
                                        $quantArr[] = ['label'=>$quant, 'is_selected' => 1];
                                    }
                                }
                                if(!count($quantArr)) {
                                    $quantArr = null;
                                }
                                if(isset($_POST[$menuSetItemId.'_default_selected'])) {
                                    $defaultSelected = 1;
                                }
                                $settingExistsIndex = array_search($menuSet['id'], array_column($menuItemArr, 'id'));
                                if($settingExistsIndex === false) {
                                    $menuItemArr[] = [
                                        'id'              => $menuSet['id'],
                                        'label'           => $menuSet['name'],
                                        'is_multi_select' => $menuSet['is_multi_select'],
                                        'maximum_selection' => $menuSet['maximum_selection'],
                                        'minimum_selection' => $menuSet['minimum_selection'],
                                        'equal_selection' => $menuSet['equal_selection'],
                                        'is_mandatory'    => $isMandatory,
                                        'items'           => [
                                            [
                                                'id'               => $menuSetItemId,
                                                'label'            => $menuSetItemDBArr[$menuSetItemId]['name'],
                                                'default_selected' => $defaultSelected,
                                                'quantity'         => $quantArr,
                                                'sides'            => $sidesArr,
                                                'price'            => $menuSetItemDBArr[$menuSetItemId]['price'],
                                            ],
                                        ],
                                    ];
                                } else {
                                    $menuItemArr[$settingExistsIndex]['items'][] = [
                                        'id'               => $menuSetItemId,
                                        'label'            => $menuSetItemDBArr[$menuSetItemId]['name'],
                                        'default_selected' => $defaultSelected,
                                        'quantity'         => $quantArr,
                                        'sides'            => $sidesArr,
                                        'price'            => $menuSetItemDBArr[$menuSetItemId]['price'],
                                    ];
                                }
                            }
                        }
                    }
                    $customizableData = [
                        'is_customizable'   => count($menuItemArr) ? 1 : 0,
                        'customizable_data' => json_encode($menuItemArr)
                    ];
                    MenuItem::where('id', $menuItemObj->id)
                        ->update($customizableData);

                    // <<< CUSTOMIZABLE MENU DATA

                    // menu meal type price
                   /* if($request->input('type')=="1")
                    {
                        $itemPriceData = ['menu_meal_type_id' => 0, 'menu_item_id' => $menuItemObj->id, 'price' => trim($request->input('price_all')),'all_flag'=>$request->input('type')];
                        MenuMealTypePrice::create($itemPriceData);
                    }
                    else {
                        $mealTypeArr = $request->input('meal_type');
                        $priceArr = $request->input('price');
                        foreach ($mealTypeArr as $k => $v) {
                            $itemPriceData = ['menu_meal_type_id' => $v, 'menu_item_id' => $menuItemObj->id, 'price' => $priceArr[$k],'all_flag'=>$request->input('type')];
                            MenuMealTypePrice::create($itemPriceData);
                        }
                    } */
                }
                return Redirect::back()->with('message', 'Menu Item added successfully');
            }
        }
        $categoryData = array();
        $menuTypeData = array();
        $availibilityData = array();
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $categoryData = MenuCategory::where('restaurant_id', '=',trim($request->input('restaurant_id')))->get();
            $menuTypeData = MenuMealType::where('restaurant_id', '=',trim($request->input('restaurant_id')))->get();
            $availibilityData = VisibilityHours::where('restaurant_id', '=',trim($request->input('restaurant_id')))->get();

        }

        //dd($sizeCount);
        return redirect()->back()->withErrors($validator->errors())->withInput()
            ->with('categoryData',$categoryData)
            ->with('menuTypeData',$menuTypeData)
            ->with('sizeCount',$sizeCount)
            ->with('visibilityData',$availibilityData);
    }

    public function edit(Request $request,$id)
    {
        if(isset($id) && is_numeric($id)) {
            $categoryData = $menuTypeData = $menuTypePriceData = $mealPriceData = array();
            #$groupRestData = Restaurant::group();
            $groupRestData = CommonFunctions::getRestaurantGroup();
            $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();

            $menuItem = MenuItem::find($id);
            $menuTypeData = $menuTypePriceData = $categoryData = array();
            if(!is_null($menuItem->size_price) && $menuItem->size_price!='') {
                $mealPriceData = json_decode($menuItem->size_price,true);
            }
            if(!is_null($menuItem->customizable_data) && $menuItem->customizable_data!='') {
                $menuItem->customizable_data = json_decode($menuItem->customizable_data,true);
            }
            if (!empty($menuItem)) {
                $categoryData = MenuCategory::where('restaurant_id', '=', $menuItem->restaurant_id)->orderBy('name', 'DESC')->get();
                $subCategoryData = MenuSubCategory::select('id','name', 'priority')
                                        ->where(['restaurant_id' => $menuItem->restaurant_id, 'menu_category_id' => $menuItem->menu_category_id, 'status' => 1])
                                        ->orderBy('priority')->get();

                $subcat_options= app('App\Http\Controllers\MenuSubcategoryController')->getSubCatHtml($menuItem->menu_category_id,$menuItem->restaurant_id,0,NULL,$menuItem->menu_sub_category_id);


                $menuTypeData = MenuMealType::where('restaurant_id', '=', $menuItem->restaurant_id)->orderBy('name')->get();
                $menuTypePriceData = MenuMealTypePrice::where('menu_item_id', '=', $menuItem->id)->orderBy('id', 'DESC')->get();
                /*foreach ($menuTypePriceData as $k => $v) {
                    $mealPriceData[$v->menu_meal_type_id] = $v->price;
                }*/

                $visibilityData = VisibilityHours::where('restaurant_id', '=', $menuItem->restaurant_id)->get();

            }

            // MENU CUSTOMISATION SETTING >>>
            $restId = $menuItem->restaurant_id;
            $menuSetCat = MenuSettingCategory::select('id', 'name', 'is_multi_select')->where(['status' => 1, 'restaurant_id' => $restId, 'menu_category_id' => $menuItem->menu_category_id])->orderBy('display_order', 'ASC')->get()->toArray();
            $menuSetCatIds = array_column($menuSetCat, 'id');
            $menuSetItems = MenuSettingItem::select('id', 'menu_category_id', 'menu_setting_category_id', 'name', 'side_flag', 'quantity_flag')
                                ->whereIn('menu_setting_category_id', $menuSetCatIds)->where(['restaurant_id' => $restId, 'status' => 1])->orderBy('display_order', 'ASC')->get()->toArray();
            $menuSettingData = [];
            foreach($menuSetCat as $menuCat) {
                $res = array_filter($menuSetItems, function ($i) use ($menuCat) {
                    if($i['menu_setting_category_id'] == $menuCat['id']) {
                        return true;
                    }
                    return false;
                });
                $menuCat['items'] = array_values($res);
                array_push($menuSettingData, $menuCat);
            }
            $hriSides = config('constants.sides');
            $hriQuant = config('constants.quantity');
            // <<< MENU CUSTOMISATION SETTING
         //   echo "<pre>";print_r($mealPriceData);die;
            $product_types = config('constants.product_type');
            return view('menu_item.edit', compact('groupRestData','menuItem','categoryData','subCategoryData','subcat_options','menuTypeData','menuTypePriceData','mealPriceData','menuSettingData','hriSides', 'hriQuant','product_types', 'languageData', 'visibilityData'));
            // return view('menu_item.edit')->with('restaurantData', $restaurantData)->with('menuItem', $menuItem)->with('categoryData', $categoryData)->with('menuTypeData', $menuTypeData)->with('menuTypePriceData', $menuTypePriceData)->with('mealPriceData', $mealPriceData)->with('menuSettingData', $menuSettingData);
        }
        return Redirect::back();
    }
    public function unsetDeleted($keys,$arr){
        $allkeys=array_keys($arr);       

        foreach($allkeys as $val){

            if (!in_array($val, $keys)){
                
                 unset($arr[$val]);
            } 
        }
      
     return array_values($arr);

    }
    public function update(Request $request,$id)
    { //echo "<pre>";print_r($request->all());die;
        // $typeArr = array_values($request->input('type'));
        // $priceArr = array_values($request->input('price'));
        // $request->merge(array('type' => $typeArr));
        // $request->merge(array('price' => $priceArr));

        if(isset($id) && is_numeric($id)) {
            $rules = [
                'name' => 'required|max:100',
                'product_type' => 'required',
                'restaurant_id' => 'required|exists:restaurants,id',
                'language_id' => 'required|exists:languages,id',
                'status' => 'required|in:0,1',
                //'image_caption' => 'required|max:100',
                'type' => 'required|array',
               // 'description' => 'required|max:250',
                'size' => 'sometimes|nullable',
                'menu_category_id' => 'required|numeric|exists:menu_categories,id',
                //'sub_category_id' => 'numeric|exists:menu_sub_categories,id',
                'image_delete' => 'sometimes|nullable|in:1'
                //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:'.config('constants.image.file_size'),
            ];

            $sizeCount = 0;
            if ($request->input('size') && is_Array($request->input('size'))) {
                $sizeCount = count($request->input('size'));
                $sizeObj = $request->input('size');
                //size validation commneted on 13-09-2018 By RG as we do not require size for some item
                // foreach ($sizeObj as $k => $v) {
                //     $rules['size.' . $k] = 'required|max:100';
                //     $rules['type.' . $k] = 'required|in:0,1';
                // }
            }

            if ($request->input('meal_type') && is_Array($request->input('meal_type'))) {
                $mealTypeObj = $request->input('meal_type');
                foreach ($mealTypeObj as $k => $v) {
                    foreach ($v as $k1 => $v1) {
                        $rules['price.' . $k . '.' . $k1] = 'required|numeric';
                    }
                }
            }

            if ($request->input('price')) {
                $priceObj = $request->input('price');
                foreach ($priceObj as $k => $v) {
                    foreach ($v as $k1 => $v1) {
                        if (!is_null($v1) && trim($v1) != '')
                            $rules['meal_type.' . $k . '.' . $k1] = 'required|numeric';
                    }
                }
            }

            if ($request->input('type') && is_Array($request->input('type'))) {
                $typeObj = $request->input('type');
                foreach ($typeObj as $k => $v) {
                    if ($v == '1') {
                        $rules['price_all.' . $k] = 'required|numeric';
                    } else if ($v == '0') {
                        $rules['meal_type.' . $k] = 'required|array';
                    }
                }
            }
            if($request->file('image'))
            {
                foreach ($request->file('image') as $k => $v)
                {
                    $rules['image_caption.' . $k] = 'required|max:100';
                }
            }
            $rulesMsg = [
                'name.*' => 'Please enter valid name upto 100 chars',
                'product_type' => 'Please select Product Type',
                'restaurant_id.*' => 'Please select Restaurant',
                'language_id.*' => 'Please select valid Language',
                'status.*' => 'Please select Status',
                'image_caption.*' => 'Please enter Image Caption upto 100 chars',
                'meal_type.*' => 'Please select Meal Type',
                'image.image' => 'Please select valid Image',
                'image.mimes' => 'Please select valid Image Type',
                'image.max' => 'Please select size up to' . config('constants.image.file_size'),
                'image.*' => 'Please select Image',
                'price.*' => 'Please enter Price',
                'price_all.*' => 'Please enter numeric Price',
                'type.*' => 'Please select Price Type',
              //  'description.required' => 'Please enter Description',
                //'description.max' => 'Please enter Description upto 250 chars',
                'size.distinct' => 'Sizes cannot be same',
                'size.*' => 'Please enter size upto 100 chars'
            ];
            $validator = Validator::make(Input::all(), $rules, $rulesMsg);
            if ($validator->passes()) {
                $img_err = 0;
                if ($request->hasFile('image')) {
                    $max_file_size = config('constants.image.file_size');
                    $allowed_image_extension = array("png", "jpg", "jpeg", "gif");
                    $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
                    foreach ($request->file('image') as $image) {
                        if (!file_exists($image->getRealPath())) {
                            $validator->errors()->add('image', 'Please select Image.');
                            $img_err = 1;
                        } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types))  {
                            $validator->errors()->add('image', 'Please select valid Image.');
                            $img_err = 1;
                        } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('image', 'Please upload image up to ' . $max_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                }
                if ($request->hasFile('web_image')) {
                    $max_file_size = config('constants.image.file_size');
                    $allowed_image_extension = array("png", "jpg", "jpeg", "gif");
                    $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
                    foreach ($request->file('web_image') as $image) {
                        if (!file_exists($image->getRealPath())) {
                            $validator->errors()->add('image', 'Please select Image.');
                            $img_err = 1;
                        } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types))  {
                            $validator->errors()->add('image', 'Please select valid Image.');
                            $img_err = 1;
                        } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('image', 'Please upload image up to ' . $max_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                }
                if ($request->hasFile('mobile_web_image')) {
                    $max_file_size = config('constants.image.file_size');
                    $allowed_image_extension = array("png", "jpg", "jpeg", "gif");
                    $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
                    foreach ($request->file('mobile_web_image') as $image) {
                        if (!file_exists($image->getRealPath())) {
                            $validator->errors()->add('image', 'Please select Image.');
                            $img_err = 1;
                        } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types))  {
                            $validator->errors()->add('image', 'Please select valid Image.');
                            $img_err = 1;
                        } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('image', 'Please upload image up to ' . $max_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                }
                if ($img_err == 0) {
                    $sizeObj = $request->input('size');
                    $typeObj = $request->input('type');
                    $priceObj = $request->input('price');
                    $priceAllObj = $request->input('price_all');
                    $cnt = 0;
                    $sizePriceArr = array();
                    foreach ($sizeObj as $k => $v) {
                        $priceTmpArr = $availableTmpArr = array();
                        if ($typeObj[$k] == "1") {
                            $sizePriceArr[$cnt]['size'] = $v;
                            $priceTmpArr[0] = $priceAllObj[$k];
                            //$sizePriceArr[$cnt]['price'] = $priceAllObj[$k];
                            //$sizePriceArr[$cnt]['menu_meal_type_id'] = 0;
                            //$cnt++;
                            //set is_delivery and is_carrayout flag
                            if($request->has('carryout_all') && isset($request->input('carryout_all')[$k])) {
                                $availableTmpArr['is_carryout'] = 1;
                            }else {
                                $availableTmpArr['is_carryout'] = 0;
                            }

                            //set is_delivery and is_carrayout flag
                            if($request->has('delivery_all') && isset($request->input('delivery_all')[$k])) {
                                $availableTmpArr['is_delivery'] = 1;
                            }else {
                                $availableTmpArr['is_delivery'] = 0;
                            }
                        } else {
                            $mealTypeObj = array();
                            if ($request->input('meal_type')) {
                                if (isset($request->input('meal_type')[$k]))
                                    $mealTypeObj = $request->input('meal_type');
                            }
                            $sizePriceArr[$cnt]['size'] = $v;
                            foreach ($mealTypeObj[$k] as $k1 => $v1) {

                                $priceTmpArr[$v1] = $priceObj[$k][$k1];
                                //$sizePriceArr[$cnt]['price'] = $priceObj[$k][$k1];
                                //$sizePriceArr[$cnt]['menu_meal_type_id'] = $v1;
                                //set available
                                if($request->has('carryout') && isset($request->input('carryout')[$k]) && array_key_exists($k1, $request->input('carryout')[$k])) {
                                    $availableTmpArr[$v1]['is_carryout'] = 1;
                                }else {
                                    $availableTmpArr[$v1]['is_carryout'] = 0;
                                }

                                 if($request->has('delivery') && isset($request->input('delivery')[$k]) && array_key_exists($k1, $request->input('delivery')[$k])) {
                                    $availableTmpArr[$v1]['is_delivery'] = 1;
                                }else {
                                    $availableTmpArr[$v1]['is_delivery'] = 0;
                                }


                            }
                           
                        }
                        $sizePriceArr[$cnt]['price'] = (array)$priceTmpArr;
                        $sizePriceArr[$cnt]['available'] = (array)$availableTmpArr;
                        $cnt++;
                    }

                    // CUSTOMIZABLE MENU DATA >>>
                    $menuItemArr = [];
                    $restId = $request->input('restaurant_id');
                    $catId = $request->input('menu_category_id');
                    $menuSetCat = MenuSettingCategory::select('id', 'name', 'is_multi_select','maximum_selection','minimum_selection','equal_selection')->where(['status' => 1, 'restaurant_id' => $restId,'menu_category_id' => $catId])->orderBy('display_order', 'ASC')->get()->toArray();

                    foreach($menuSetCat as $menuSet) {
                        $isMandatory = 0;
                        $menuSetName = str_replace(' ', '_', $menuSet['name']);
                        if(isset($_POST[$menuSet['id'].'_is_mandatory'])) {
                            $isMandatory = 1;
                        }

                        if(isset($_POST[$menuSetName]) && count($_POST[$menuSetName])) {
                            // get price of Customization setting item
                            $menuSetItemDBArr = [];
                            $res = MenuSettingItem::select('id','name','price')->whereIn('id', $_POST[$menuSetName])->orderBy('display_order', 'ASC')->get()->toArray();
                            array_walk($res, function($item) use (&$menuSetItemDBArr) { $menuSetItemDBArr[$item['id']] = $item; });

                            // check ids in POST menuSetName
                            foreach($request->input($menuSetName) as $menuSetItemId) {
                                $quantArr = [];
                                $sidesArr = [];
                                $defaultSelected = 0;
                                foreach(config('constants.sides') as $side) {
                                    if(isset($_POST[$menuSetItemId.'_'.$side])) {
                                        $sidesArr[] = ['label' => $side, 'is_selected' => 1];
                                    }
                                }
                                if(!count($sidesArr)) {
                                    $sidesArr = null;
                                }
                                foreach(config('constants.quantity') as $quant) {
                                    if(isset($_POST[$menuSetItemId.'_'.$quant])) {
                                        $quantArr[] = ['label'=>$quant, 'is_selected' => 1];
                                    }
                                }
                                if(!count($quantArr)) {
                                    $quantArr = null;
                                }
                                if(isset($_POST[$menuSetItemId.'_default_selected'])) {
                                    $defaultSelected = 1;
                                }
                                $settingExistsIndex = array_search($menuSet['id'], array_column($menuItemArr, 'id'));
                                if($settingExistsIndex === false) {
                                    $menuItemArr[] = [
                                        'id'              => $menuSet['id'],
                                        'label'           => $menuSet['name'],
                                        'is_multi_select' => $menuSet['is_multi_select'],
                                        'maximum_selection' => $menuSet['maximum_selection'],
                                        'minimum_selection' => $menuSet['minimum_selection'],
                                        'equal_selection' => $menuSet['equal_selection'],
                                        'is_mandatory'    => $isMandatory,
                                        'items'           => [
                                            [
                                                'id'               => $menuSetItemId,
                                                'label'            => $menuSetItemDBArr[$menuSetItemId]['name'],
                                                'default_selected' => $defaultSelected,
                                                'quantity'         => $quantArr,
                                                'sides'            => $sidesArr,
                                                'price'            => $menuSetItemDBArr[$menuSetItemId]['price'],
                                            ],
                                        ],
                                    ];
                                } else {
                                    $menuItemArr[$settingExistsIndex]['items'][] = [
                                        'id'               => $menuSetItemId,
                                        'label'            => $menuSetItemDBArr[$menuSetItemId]['name'],
                                        'default_selected' => $defaultSelected,
                                        'quantity'         => $quantArr,
                                        'sides'            => $sidesArr,
                                        'price'            => $menuSetItemDBArr[$menuSetItemId]['price'],
                                    ];
                                }
                            }
                        }
                    }
                    // <<< CUSTOMIZABLE MENU DATA
                    $imageCaption = !empty(Input::get('image_caption'))?Input::get('image_caption'):null;
                    //dd($sizePriceArr,json_encode($sizePriceArr));
                    $menuItemObj                       = MenuItem::find($id);
                    $menuItemObj->name                 = Input::get('name');
                    $menuItemObj->product_type          = Input::get('product_type');
                    $menuItemObj->size_price           = json_encode($sizePriceArr);
                    $menuItemObj->restaurant_id        = Input::get('restaurant_id');
                    $menuItemObj->language_id        = Input::get('language_id');
                    $menuItemObj->is_customizable      = count($menuItemArr) ? 1 : 0;
                    $menuItemObj->customizable_data    = json_encode($menuItemArr);
                    $menuItemObj->status               = Input::get('status');
                    $menuItemObj->image_caption        = !empty($imageCaption)?current(Input::get('image_caption')): NULL;
                    $menuItemObj->menu_category_id     = Input::get('menu_category_id');
                    $menuItemObj->menu_sub_category_id = Input::get('menu_sub_category_id');
                    $menuItemObj->description          = !empty( Input::get('description'))? Input::get('description'):'';
                    $menuItemObj->sub_description          = !empty( Input::get('sub_description'))? Input::get('sub_description'):'';
                    $menuItemObj->slot_code = Input::get('slot_code');
                    $menuItemObj->gift_wrapping_fees =$request->input('gift_wrapping_fees')?$request->input('gift_wrapping_fees'):NULL;
                    $menuItemObj->gift_message =$request->input('gift_message')?$request->input('gift_message'):NULL;

                    if($request->has('is_popular') && $request->input('is_popular') == 'on' ) {
                        $menuItemObj->is_popular  = 1;
                    }else {
                        $menuItemObj->is_popular  = 0;
                    }
                    if($request->has('is_favourite') && $request->input('is_favourite') == 'on' ) {
                        $menuItemObj->is_favourite  = 1;
                    }else {
                        $menuItemObj->is_favourite  = 0;
                    }

                    if($request->input('image_delete') && $request->input('image_delete')=='1')
                    {
                        $menuItemObj->image = NULL;
                        $menuItemObj->thumb_image_med = NULL;
                        $menuItemObj->image_caption = NULL;
                    }
                    if($request->input('web_image_delete') && $request->input('web_image_delete')=='1')
                    {
                        $menuItemObj->web_image = NULL;
                        $menuItemObj->web_thumb_med = NULL;
                        $menuItemObj->web_image_caption = NULL;
                    }
                     if($request->input('mobile_web_image_delete') && $request->input('mobile_web_image_delete')=='1')
                    {
                        $menuItemObj->mobile_web_image = NULL;
                        $menuItemObj->mobile_web_thumb_med = NULL;
                        $menuItemObj->mobile_web_image_caption = NULL;
                    }
                    $menuItemObj->display_order = !empty(Input::get('display_order'))?Input::get('display_order'):0;
                    $menuItemObj->save();

                    /*if($request->input('type')=="1")
                    {
                        MenuMealTypePrice::where('menu_item_id', '=', $id)->delete();
                        $itemPriceData = ['menu_meal_type_id' => 0, 'menu_item_id' => $menuItemObj->id, 'price' => trim($request->input('price_all')),'all_flag'=>$request->input('type')];
                        MenuMealTypePrice::create($itemPriceData);
                    }
                    else {
                        if (isset($menuItemObj->id) && $request->input('meal_type')) {
                            MenuMealTypePrice::where('menu_item_id', '=', $id)->delete();
                            $mealTypeArr = $request->input('meal_type');
                            $priceArr = $request->input('price');
                            foreach ($mealTypeArr as $k => $v) {
                                $itemPriceData = ['menu_meal_type_id' => $v, 'menu_item_id' => $menuItemObj->id, 'price' => $priceArr[$k]];
                                MenuMealTypePrice::create($itemPriceData);
                            }
                        }
                    }*/
                   

                   
                    $imageRelPath = config('constants.image.rel_path.menu');
                    $imagePath = config('constants.image.path.menu');

                    $curr_restaurant_name=strtolower(str_replace(' ','-',$menuItemObj->Restaurant->restaurant_name));
                    $imagePath=$imagePath.DIRECTORY_SEPARATOR.$curr_restaurant_name;
                    $imageRelPath=$imageRelPath.$curr_restaurant_name. DIRECTORY_SEPARATOR;

                    (! File::exists($imagePath) ?File::makeDirectory($imagePath):'');
                    (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'thumb') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'thumb' ):'');

                    $allimages=json_decode($menuItemObj->images,true);
                    $imageJsonArray = $allimages;
                    $mobile_app_images=isset($allimages['mobile_app_images'])?$allimages['mobile_app_images']:[];
                    $web_images=isset($allimages['desktop_web_images'])?$allimages['desktop_web_images']:[];
                    $mobile_web_images=isset($allimages['mobile_web_images'])?$allimages['mobile_web_images']:[];

                    $imageHidden = !empty($request->input('image_hidden'))?$request->input('image_hidden'):[];
                    $mobile_app_images=$this->unsetDeleted($imageHidden,$mobile_app_images);
                    $imageJsonArray['mobile_app_images']=$mobile_app_images;

                     $webImageHidden = !empty($request->input('web_image_hidden'))?$request->input('web_image_hidden'):[];
                    $web_images=$this->unsetDeleted($webImageHidden,$web_images);
                    $imageJsonArray['desktop_web_images']=$web_images;

                    $mobileWebImageHidden = !empty($request->input('mobile_web_image_hidden'))?$request->input('mobile_web_image_hidden'):[];    
                    $mobile_web_images=$this->unsetDeleted($mobileWebImageHidden,$mobile_web_images);
                    $imageJsonArray['mobile_web_images']=$mobile_web_images;
    
                    /* Mobile app Image save*/
                    if ($request->hasFile('image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('image_caption');
                        foreach ($request->file('image') as $k => $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                            $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);

                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);
                            
                            //$image->move($imagePath, $imageName);
                            $imageArr[] = [
                                'image' => $imageRelPath . $imageName,
                                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'image_caption' => $imageCaptionArr[$k],
                            ];

                            $mobile_app_images[]= [
                                'image' => $imageRelPath . $imageName,
                                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'image_caption' => $imageCaptionArr[$k],
                            ];
                            /* comment old image entry   
                            if (count($imageArr) > 0) {
                                MenuItem::where('id', $menuItemObj->id)
                                    ->update($imageArr[0]);
                            }
                            */
                            $i++;
                        }
                        $mobile_app_images = array_values($mobile_app_images);
                        $imageJsonArray['mobile_app_images']=$mobile_app_images;
                    }
                    /*Desktop Image save*/
                    if ($request->hasFile('web_image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('web_image_caption');
                          

                        foreach ($request->file('web_image') as $k => $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                            $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();        
                            $image = Image::make($image->getRealPath());
                            $width = $image->width();

                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);
                            //$image->move($imagePath, $imageName);
                            $imageWebArr[] = [
                                'web_image' => $imageRelPath . $imageName,
                                'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'web_thumb_sml' => $imageCaptionArr[$k],
                            ];
                           $web_images[]= [
                                'web_image' => $imageRelPath . $imageName,
                                'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'web_thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'web_image_caption' => $imageCaptionArr[$k],
                            ];
                             /* comment old image entry  
                            if (count($imageWebArr) > 0) {
                                MenuItem::where('id', $menuItemObj->id)
                                    ->update($imageWebArr[0]);
                            }
                            */
                            $i++;
                        }
                        $web_images = array_values($web_images);
                         $imageJsonArray['desktop_web_images']=$web_images;
                    }
                      /*Mobile web Image save*/
                    if ($request->hasFile('mobile_web_image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('mobile_web_image_caption');

                        foreach ($request->file('mobile_web_image') as $k => $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                             $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();   
                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                            
                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                            //$image->move($imagePath, $imageName);
                            $imageMobWebArr[] = [
                                'mobile_web_image' => $imageRelPath . $imageName,
                                'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'mobile_web_thumb_sml' => $imageCaptionArr[$k],
                            ];
                             $mobile_web_images[$k]= [
                                'mobile_web_image' => $imageRelPath . $imageName,
                                'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'mobile_web_thumb_sml' =>$imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'mobile_web_image_caption' => $imageCaptionArr[$k],
                            ];
                            /*
                            if (count($imageMobWebArr) > 0) {
                                MenuItem::where('id', $menuItemObj->id)
                                    ->update($imageMobWebArr[0]);
                            } 
                            */
                            $i++;
                        }
                        $mobile_web_images = array_values($mobile_web_images);
                         $imageJsonArray['mobile_web_images']=$mobile_web_images;

                    }
                   //  if ($request->hasFile('image')|| $request->hasFile('web_image') || $request->hasFile('mobile_web_image')) {
                       MenuItem::where('id', $menuItemObj->id)->update(['images'=>json_encode($imageJsonArray)]);
                      //}
                    return Redirect::back()->with('message', 'Menu Item updated successfully');
                }
            }
            return redirect()->back()->withErrors($validator->errors())->withInput()->with('sizeCount', $sizeCount);
        }
        return Redirect::back()->with('message', 'Invalid Id');
    }

    public function getCategoryAndType(Request $request)
    {
        $rid = $request->input('rid');
        if(!empty($rid) && is_numeric($rid)) {
            $result['cat'] = MenuCategory::where('restaurant_id', '=', $rid)->get();
            $result['type'] = MenuMealType::where('restaurant_id', '=', $rid)->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }

    public function getAvailibility(Request $request)
    {
        $rid = $request->input('rid');
        if(!empty($rid) && is_numeric($rid)) {
            $result['cat'] = VisibilityHours::where('restaurant_id', '=', $rid)->get();
            //$result['type'] = MenuMealType::where('restaurant_id', '=', $rid)->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }

    public function destroy($id)
    {
        $menuItemObj = MenuItem::find($id);
        if(!empty($menuItemObj))
        {
            $menuItemObj->delete();
            return Redirect::back()->with('message','Menu Item deleted successfully');
        }
        else
            return Redirect::back()->with('err_msg','Invalid Item');
    }

    public function getCustomizationOptions(Request $request)
    {

       

        $restId = $request->input('rid');
        $catId  = $request->input('cid');
        if (!empty($restId) && is_numeric($restId) && !empty($catId) && is_numeric($catId)) {
            // MENU CUSTOMISATION SETTING >>>
            $restId        = $restId;
            $menuSetCat    = MenuSettingCategory::select('id', 'name', 'is_multi_select')->where(['status' => 1, 'restaurant_id' => $restId, 'menu_category_id' => $catId])->orderBy('display_order', 'ASC')->get()->toArray();
            $menuSetCatIds = array_column($menuSetCat, 'id');
            $menuSetItems  = MenuSettingItem::select('id', 'menu_category_id', 'menu_setting_category_id', 'name', 'side_flag', 'quantity_flag')
                            ->whereIn('menu_setting_category_id', $menuSetCatIds)->where(['restaurant_id' => $restId, 'status' => 1])->orderBy('display_order', 'ASC')->get()->toArray();
            $subCategories = MenuSubCategory::select('id', 'name')->where(['restaurant_id' => $restId, 'menu_category_id' => $catId, 'status' => 1])->orderBy('priority')->get();

            $subcat_options= app('App\Http\Controllers\MenuSubcategoryController')->getSubCatHtml($catId,$restId,0,NULL,0);

            $menuSettingData = [];
            foreach ($menuSetCat as $menuCat) {
                $res              = array_filter($menuSetItems, function ($i) use ($menuCat) {
                    if ($i['menu_setting_category_id'] == $menuCat['id']) {
                        return true;
                    }
                    return false;
                });
                $menuCat['items'] = array_values($res);
                array_push($menuSettingData, $menuCat);
            }

            $result = [
                'menu_settings_data' => $menuSettingData,
                'sub_categories'     => $subCategories,
                'subcat_options'     => $subcat_options,
                'hri_sides'          => config('constants.sides'),
                'hri_quantity'       => config('constants.quantity'),
            ];

            return response()->json(array('dataObj' => $result), 200);
        }

        return response()->json(array('err' => 'Invalid Data'), 200);
    }
}
