<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $inputData = array('country_name'=>'');
        $countryObj = Country::orderBy('id', 'DESC');

        if($request->input('country_name')) {
            $countryObj->where('country_name', 'like', '%' . $request->input('country_name') . '%');
            $inputData['country_name'] =  $request->input('country_name');
        }

        $countries = $countryObj->paginate(6);


        return view('countries.index', compact('countries'))->with('inputData',$inputData);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(isset($id) && is_numeric($id)) {
            $countries = Country::find($id);
            if( $countries) {
                return view('countries.edit', compact('countries'));
            }
        }
        return Redirect::back()->with('message', 'Invalid Request. Please try again!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { echo $id;die;
        if(isset($id) && is_numeric($id)) {
            $countries = Country::find($id);dd($countries);die;
            if( $countries) {
                return view('countries.edit', compact('countries'));
            }
        }
        return Redirect::back()->with('message', 'Invalid Request. Please try again!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
