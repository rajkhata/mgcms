<?php

namespace App\Http\Controllers;

use App\Models\MailTemplate;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;

class MailTemplateController extends Controller
{
    /*
     * Middleware(auth) : Validate user to access the application
     */
    public function __construct()
    {
        $this->middleware('auth');       
    }
    /**
     * Mail Template listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
         if(isset($request->name)){
            $template = MailTemplate::select('id','template_name', 'content', 'subject', 'status', 'language_id', 'restaurant_id')->where('template_name','like', '%'.$request->name.'%')->paginate(10);
         }else{
            $template = MailTemplate::select('id','template_name', 'content', 'subject', 'status', 'language_id', 'restaurant_id')->paginate(10);
         }
        
        return view('mailtemplate.index', compact('template'));
    }
    
    public function create()
    {
        $mailtemplate = "";
       
        return view('mailtemplate.create', compact('mailtemplate'));
    }
    
     public function addTemplate(Request $request)
    {

        $rules = [
            
            'template_name'     => 'required|max:255',
            'content' => 'required',
            'status'     => 'required|in:0,1',            
        ];
        
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->passes()) {
            if($request->template_id){
                MailTemplate::where('id',$request->template_id)->update(['template_name'=>$request->template_name,'content'=>$request->content,'status'=>$request->status]);
                return redirect()->back()->with('message','Template updated successfully.');
            }
            $templateObj = MailTemplate::create(Input::all());
            if(isset($templateObj->id))
            {                
                $templateObj->save();
            }
            return redirect()->back()->with('message','Template added successfully');
        }
        return redirect()->back()->withErrors($validator->errors())->withInput();
    }
    private  function _group_by($array, $key) {
        $return = array();
        foreach($array as $val) {
            $return[$val[$key]][] = $val;
        }
        return $return;
    }
    public function editTemplate(Request $request,$id){
       
        $mailtemplate = MailTemplate::find($id);
        if($mailtemplate){
            $default_template='';
            if($request->has('default_template')){
               $default_template=$request->input('default_template');
                $default_template = DB::table('default_mail_templates')->where('template_name',$default_template)->first(['subject','content']);

            }

            $default_templates = DB::table('default_mail_templates')->get(['name','template_name','template_group']);
            if($default_templates){
                $default_templates= $default_templates->toArray();

                $default_templates = array_map(function ($value) {
                    return (array)$value;
                }, $default_templates);
            }else{
                $default_templates=[];
            }
            $default_templates=$this->_group_by($default_templates,'template_group');
            return view('mailtemplate.create',compact('mailtemplate','default_templates','default_template'));
        }else{
           return redirect()->back()->with('message','Mail template not exist.'); 
        }
    }
    
    public function destroy($id)
    {
        if(isset($id) && is_numeric($id)) {
            $templateObj = MailTemplate::find($id);
            if (!empty($templateObj)) {
                $templateObj->delete();
                return Redirect::back()->with('message', 'Template has been deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg','Invalid Id');
    }

    /**
     * Create user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function create()
//    {
//        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();
//        return view('user.create', compact('restaurantData'));
//    }
//
//    public function store(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'restaurant_id' => 'required|exists:restaurants,id',
//            'fname'         => 'required|max:255',
//            'lname'         => 'required|max:255',
//            'email'         => 'required|string|email|max:255|unique:users',
//            'password'      => 'required|alpha_num|min:8|confirmed',
//            'status'        => 'required|in:0,1',
//        ]);
//        if (!$validator->fails()) {
//            $data = [
//                'restaurant_id' => $request->input('restaurant_id'),
//                'fname'         => strtolower($request->input('fname')),
//                'lname'         => strtolower($request->input('lname')),
//                'email'         => $request->input('email'),
//                'password'      => Hash::make($request->input('password')),
//                'status'        => $request->input('status'),
//            ];
//            //dd($data);
//            $user = User::create($data);
//            return Redirect::back()->with('message', 'User added successfully');
//        } else {
//
//            return Redirect::back()->withErrors($validator->errors())->withInput();
//        }
//
//    }
//    
    /**
     * User edit
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function edit($id)
//    {
//        $template          = MailTemplate::find($id);
//        $mailTemplate = mailTemplate::select('id', 'template_name','content','subject','status','language_id')->orderBy('id', 'DESC')->get();
//
//        return view('template.edit', compact('template', 'mailTemplate'));
//    }

    /**
     * User edit update
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\JsonResponse
     */
//    public function update(Request $request, $id)
//    {
//        if(isset($id) && is_numeric($id)) {
//            $user = User::find($id);
//            $oldEmail = $user->email;
//            if($oldEmail === $request->get('email')) {
//                // no change, validation - exists
//                $emailValidation = 'exists';
//            } else {
//                // change, validation - unique
//                $emailValidation = 'unique';
//            }
//            $validator = Validator::make($request->all(), [
//                'restaurant_id' => 'required|exists:restaurants,id',
//                'fname'         => 'required|max:255',
//                'lname'         => 'required|max:255',
//                'email'         => 'required|string|email|max:255|' . $emailValidation . ':users',
//                'status'        => 'required|in:0,1',
//                //'image_delete'  => 'sometimes|nullable|in:1',
//            ]);
//
//            if(!$validator->fails()) {
//                $user->restaurant_id = $request->input('restaurant_id');
//                $user->fname = $request->input('fname');
//                $user->lname = $request->input('lname');
//                $user->email = $request->input('email');
//                $user->status = $request->input('status');
//                $user->save();
//
//                return Redirect::back()->with('message', 'User updated successfully');
//
//            } else {
//
//                return Redirect::back()->withErrors($validator->errors())->withInput();
//            }
//        }
//
//        return Redirect::back()->with('message', 'Invalid Id');
//    }
}
