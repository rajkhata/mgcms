<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\MenuMealType;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Input;
use App\Helpers\CommonFunctions;
use DB;
use App\Models\Language;


class MenuMealTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        /*exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan view:clear');*/
    }

    public function index(Request $request)
    {
        $restaurant_id = 0;
        #$restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();        
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));        
        $restaurantData = Restaurant::select('id','restaurant_name')->whereIn('id',$locations)->orderBy('id','DESC')->get();
        if($restaurantData && count($restaurantData) == 1) { 
           $restaurant_id = $restaurantData[0]['id'];
        }
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $restaurant_id = $request->input('restaurant_id');
            $menuTypes = MenuMealType::where('restaurant_id',$restaurant_id)->orderBy('id','DESC')->paginate(20);
        }
        else
            $menuTypes = MenuMealType::orderBy('id','DESC')->whereIn('restaurant_id',$locations)->paginate(20);

        return view('menu_meal_type.index', compact('menuTypes'))->with('restaurantData',$restaurantData)->with('restaurant_id',$restaurant_id);
    }

    public function create()
    {
        //$restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();
        //$restaurantData = Restaurant::group();
        $restaurantData = CommonFunctions::getRestaurantGroup();
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        $selected_rest_id = 0;
        if(count($restaurantData) == 1) {
            $first_ind_array = current($restaurantData);
            if(count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        }   
        return view('menu_meal_type.create', compact('restaurantData', 'selected_rest_id', 'languageData'));
    }

    public function store(Request $request)
    {
        $request->merge(array('from_time' => str_replace(" ","",$request->input('from_time'))));
        $request->merge(array('to_time' => str_replace(" ","",$request->input('to_time'))));

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:100|unique:menu_meal_types,name,NULL,id,restaurant_id,'.$request->input('restaurant_id'),
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'from_time' => 'required|date_format:H:i:s',
            'to_time' => 'required|date_format:H:i:s',
            'status' => 'required|in:0,1'
        ], [
            'name.required' => 'Please enter valid name upto 100 chars',
            'name.max' => 'Please enter valid name upto 100 chars',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status'
        ]);

        if($validator->passes())
        {
            $err_count =0;
            if($request->input('from_time') && $request->input('to_time'))
            {
                if(strtotime($request->input('from_time')) >= strtotime($request->input('to_time')))
                {
                    $validator->errors()->add('from_time', 'From-time cannot be greater or equal to To-time');
                    $err_count++;
                }
            }

            $fTime = $request->input('from_time');
            $tTime = $request->input('to_time');
            $restId = $request->input('restaurant_id');
            $result = array();
            $result = DB::table('menu_meal_types')->select('id')
                    ->where('restaurant_id',$restId)
                    ->Where(function ($query) use ($fTime, $tTime) {
                        $query->Where('to_time','>=',$fTime)
                            ->Where('to_time','<=',$tTime)
                            ->orWhere(function ($query) use ($fTime, $tTime) {
                                $query->Where('from_time','>=',$fTime)
                                    ->where('from_time','<=',$tTime)
                                    ->orWhere(function ($query) use ($fTime, $tTime){
                                        $query->Where('from_time','>=',$fTime)
                                            ->where('to_time','<=',$tTime)
                                            ->orWhere(function ($query) use ($fTime, $tTime) {
                                                $query->Where('from_time','<=',$fTime)
                                                    ->where('to_time','>=',$tTime);
                                            })
                                        ;
                                    })
                                ;
                            })
                        ;
                    })
                    ->get();
            if(count($result)>0)
            {
                $validator->errors()->add('to_time', 'Slot already exists');
                $err_count++;
            }
            if($err_count == 0)
            {
                $menuTypeArr = ['restaurant_id'=>$request->input('restaurant_id'),
                    'language_id'=>$request->input('language_id'),
                    'name'=>$request->input('name'),
                    'status'=>$request->input('status'),
                    'from_time'=>$request->input('from_time'),
                    'to_time'=>$request->input('to_time')
                ];
                $menuTypeObj = MenuMealType::create($menuTypeArr);
                return Redirect::back()->with('message','Meal Type added successfully');
            }
        }
        return redirect()->back()->withErrors($validator->errors())->withInput();
    }

    public function edit($id)
    {
        //$restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();
        #$restaurantData = Restaurant::group();
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        $restaurantData = CommonFunctions::getRestaurantGroup();
        $menuType = MenuMealType::find($id);
        return view('menu_meal_type.edit', compact('restaurantData'),compact('menuType', 'languageData'));
    }

    public function update(Request $request,$id)
    {
        if(!isset($id) || !is_numeric($id))
        {
            return redirect()->back()->with('err_msg','Invalid Id');
        }
        $menuTypeObj = MenuMealType::find($id);
        if(empty($menuTypeObj))
        {
            return redirect()->back()->with('err_msg','Invalid Id');
        }

        $request->merge(array('from_time' => str_replace(" ","",$request->input('from_time'))));
        $request->merge(array('to_time' => str_replace(" ","",$request->input('to_time'))));
        $rules = [
            'name' => 'required|max:100',
            'language_id' => 'required|exists:languages,id',
            'restaurant_id' => 'required|exists:restaurants,id|unique:menu_meal_types,name,'.$id.',id,restaurant_id,'.$request->input('restaurant_id'),
            'from_time' => 'required|date_format:H:i:s',
            'to_time' => 'required|date_format:H:i:s',
            'status' => 'required|in:0,1'
        ];
        $rulesMsg = [
            'name.*' => 'Please enter valid name upto 100 chars',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status'
        ];
        $validator = Validator::make(Input::all(), $rules, $rulesMsg);
        if($validator->passes())
        {
            $err_count =0;
            if($request->input('from_time') && $request->input('to_time'))
            {
                if(strtotime($request->input('from_time')) >= strtotime($request->input('to_time')))
                {
                    $validator->errors()->add('from_time', 'From time cannot be greater than To time');
                    $err_count++;
                }
            }
            $fTime = $request->input('from_time');
            $tTime = $request->input('to_time');
            $restId = $request->input('restaurant_id');

            $result = array();
            $result = DB::table('menu_meal_types')->select('id')
                ->where('restaurant_id',$restId)
                ->whereNotIn('id', [$id])
                ->Where(function ($query) use ($fTime, $tTime) {
                    $query->Where('to_time','>=',$fTime)
                        ->Where('to_time','<=',$tTime)
                        ->orWhere(function ($query) use ($fTime, $tTime) {
                            $query->Where('from_time','>=',$fTime)
                                ->where('from_time','<=',$tTime)
                                ->orWhere(function ($query) use ($fTime, $tTime){
                                    $query->Where('from_time','>=',$fTime)
                                        ->where('to_time','<=',$tTime)
                                        ->orWhere(function ($query) use ($fTime, $tTime) {
                                            $query->Where('from_time','<=',$fTime)
                                                ->where('to_time','>=',$tTime);
                                        })
                                    ;
                                })
                            ;
                        })
                    ;
                })
                ->get();
            if(count($result)>0)
            {
                $validator->errors()->add('to_time', 'Slot already exists');
                $err_count++;
            }

            if($err_count == 0)
            {
                $menuTypeObj->name = Input::get('name');
                $menuTypeObj->restaurant_id      = Input::get('restaurant_id');
                $menuTypeObj->language_id      = Input::get('language_id');
                $menuTypeObj->from_time = Input::get('from_time');
                $menuTypeObj->to_time = Input::get('to_time');
                $menuTypeObj->status = Input::get('status');
                $menuTypeObj->save();

                return Redirect::back()->with('message','Meal Type updated successfully');
            }
        }
        return redirect()->back()->withErrors($validator->errors())->withInput();
    }

    public function show($id)
    {

    }

    public function destroy($id)
    {
        $menuTypeObj = MenuMealType::find($id);
        if(!empty($menuTypeObj))
        {
            $menuTypeObj->delete();
            return Redirect::back()->with('message','Menu Type deleted successfully');
        }
        else
            return Redirect::back()->with('err_msg','Invalid Type');
    }
}
