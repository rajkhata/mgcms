<?php

namespace App\Http\Controllers;
use App\Models\Language;
use App\Models\Restaurant;
use App\Models\RestaurantTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RestaurantTranslationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $restaurant_id = 0;
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();
        if($request->input('restaurants_id') && is_numeric($request->input('restaurants_id')))
        {
            $restaurant_id = $request->input('restaurants_id');
            $restTrans = RestaurantTranslation::where('restaurants_id',$restaurant_id)->orderBy('restaurant_name','ASC')->paginate(20);
        }
        else
            $restTrans = RestaurantTranslation::orderBy('restaurant_name','ASC')->paginate(20);

        return view('restaurant_translation.index', compact('restTrans'))->with('restaurantData',$restaurantData)->with('restaurant_id',$restaurant_id);
    }

    public function create()
    {
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('restaurant_name','ASC')->get();
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        return view('restaurant_translation.create', compact('restaurantData'), compact('languageData'));
    }

    public function store(Request $request)
    {
        $validationRules = [
            'restaurant_name' => 'required|max:32',
            'restaurants_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:language,id',
            'description' => 'required',
            'address' => 'required|max:255',
            'street' => 'required|max:200',
            'borough' => 'sometimes|nullable|max:50',
            'source_url' => 'sometimes|nullable|max:100|url',
            'delivery_desc' => 'sometimes|nullable|max:255',
            'notable_chef_desc' => 'sometimes|nullable|max:255',
            'parking_desc' => 'sometimes|nullable|max:255'
        ];

        if($request->input('restaurants_id') && !empty($request->input('restaurants_id')))
        {
            $validationRules['language_id'] = 'required|exists:language,id|unique:restaurant_translations,language_id,NULL,id,restaurants_id,'.$request->input('restaurants_id');
        }
        else
        {
            $validationRules['language_id'] = 'required|exists:language,id';
        }

        $validator = Validator::make($request->all(),$validationRules, [
            'restaurant_name.*' => 'Please enter valid name upto 32 chars',
            'restaurants_id.*' => 'Please select Restaurant',
            'language_id.required' => 'Please select Language',
            'language_id.exists' => 'Please select valid Language',
            'language_id.unique' => 'Language Translation already exist for Restaurant',
            'description.*' => 'Please enter Description',
            'address.*' => 'Please enter valid Address upto 255 chars',
            'street.*' => 'Please enter Street upto 200 chars',
            'borough.*' => 'Please enter Borough upto 50 chars',
            'delivery_desc.*' => 'Please enter Delivery Description upto 255 chars',
            'notable_chef_desc.*' => 'Please enter Notable Chef Description upto 255 chars',
            'parking_desc.*' => 'Please enter Parking Description upto 255 chars',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
            $restTransArr = [
                'restaurants_id' => $request->input('restaurants_id'),
                'language_id' => $request->input('language_id'),
                'restaurant_name' => $request->input('restaurant_name'),
                'description' => $request->input('description'),
                'address' => $request->input('address'),
                'street' => $request->input('street'),
                'borough' => $request->input('borough'),
                'source_url' => $request->input('source_url'),
                'delivery_desc' => $request->input('delivery_desc'),
                'notable_chef_desc' => $request->input('notable_chef_desc'),
                'parking_desc' => $request->input('parking_desc'),
            ];
            $restTransObj = RestaurantTranslation::create($restTransArr);
            return Redirect::back()->with('message', 'Data added successfully');
        }
    }

    public function edit($id)
    {
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('restaurant_name','ASC')->get();
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        $restTran = RestaurantTranslation::find($id);
        return view('restaurant_translation.edit', compact('restaurantData','restTran','languageData'));
    }

    public function update(Request $request, $id)
    {
        if(!isset($id) || !is_numeric($id))
        {
            return redirect()->back()->with('err_msg','Invalid Id');
        }
        $restTranObj = RestaurantTranslation::where('id',$id)->first();
        if(empty($restTranObj))
        {
            return redirect()->back()->with('err_msg','Invalid Id');
        }

        $validationRules = [
            'restaurant_name' => 'required|max:32',
            'restaurants_id' => 'required|exists:restaurants,id',
            'description' => 'required',
            'address' => 'required|max:255',
            'street' => 'required|max:200',
            'borough' => 'sometimes|nullable|max:50',
            'source_url' => 'sometimes|nullable|max:100|url',
            'delivery_desc' => 'sometimes|nullable|max:255',
            'notable_chef_desc' => 'sometimes|nullable|max:255',
            'parking_desc' => 'sometimes|nullable|max:255'
        ];

        if($request->input('restaurants_id') && !empty($request->input('restaurants_id')))
        {
            $validationRules['language_id'] = 'required|exists:language,id|unique:restaurant_translations,language_id,'.$id.',id,restaurants_id,'.$request->input('restaurants_id');
        }
        else
        {
            $validationRules['language_id'] = 'required|exists:language,id';
        }

        $validator = Validator::make($request->all(),$validationRules, [
            'restaurant_name.*' => 'Please enter valid name upto 32 chars',
            'restaurants_id.*' => 'Please select Restaurant',
            'language_id.required' => 'Please select Language',
            'language_id.exists' => 'Please select valid Language',
            'language_id.unique' => 'Language Translation already exist for Restaurant',
            'description.*' => 'Please enter Description',
            'address.*' => 'Please enter valid Address upto 255 chars',
            'street.*' => 'Please enter Street upto 200 chars',
            'borough.*' => 'Please enter Borough upto 50 chars',
            'source_url.*' => 'Please enter valid Source Url upto 100 chars',
            'delivery_desc.*' => 'Please enter Delivery Description upto 255 chars',
            'notable_chef_desc.*' => 'Please enter Notable Chef Description upto 255 chars',
            'parking_desc.*' => 'Please enter Parking Description upto 255 chars',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
            //$restTranObj = RestaurantTranslation::where('id',$id)->first();
            //if(!empty($restTranObj))
            //{
                $restTranObj->restaurant_name = Input::get('restaurant_name');
                $restTranObj->restaurants_id = Input::get('restaurants_id');
                $restTranObj->language_id = Input::get('language_id');
                $restTranObj->description = Input::get('description');
                $restTranObj->address = Input::get('address');
                $restTranObj->street = Input::get('street');
                $restTranObj->source_url = Input::get('source_url');
                $restTranObj->borough = Input::get('borough');
                $restTranObj->delivery_desc = Input::get('delivery_desc');
                $restTranObj->notable_chef_desc = Input::get('notable_chef_desc');
                $restTranObj->parking_desc = Input::get('parking_desc');

                $restTranObj->save();
                return Redirect::back()->with('message', 'Data updated successfully');
           //}
        }
        return redirect()->back()->with('err_msg','Invalid Id');
    }

    public function destroy($id)
    {
        if(isset($id) && is_numeric($id)) {
            $restTransObj = RestaurantTranslation::find($id);
            if (!empty($restTransObj)) {
                $restTransObj->delete();
                return Redirect::back()->with('message', 'Data deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg','Invalid Id');
    }
}
