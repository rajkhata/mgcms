<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MenuCategory;
use App\Models\MenuSubCategory;
use App\Models\Restaurant;
use Modules\MenuManagement\Entities\MenuItemModel;
use Illuminate\Support\Facades\Auth;
use App\Helpers\CommonFunctions;
use App\Models\NewMenuItems;
use App\Models\ItemAddonGroups;
use App\Models\ItemModifierGroup;
use App\Models\ItemModifierGroupLanguage;
use App\Models\ItemModifier;
use App\Models\ItemModifierLanguage;
use App\Models\ItemModifierOptionGroup;
use App\Models\MenuItemLabels;
use App\Models\Labels;
use Illuminate\Support\Facades\DB;
use App\Models\NewMenuItemsLanguage;
use App\Models\ItemAddonGroupsLanguage;
use App\Models\ItemAddons;

class CompareController extends Controller
{

	
    /**
     * Active orders listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * food_item for all general products
     */
    public function index(Request $request)
    { 
         $cmsUser = Auth::user();
         if($cmsUser->restaurant_id){
             $restaurantData = Restaurant::select('parent_restaurant_id')->where('id',$cmsUser->restaurant_id)->get()->toArray();
         }
        $branchData = [];
        $parentRestaurant = [];
        $branchId = [];
       
        if(isset($restaurantData[0]['parent_restaurant_id']) && $restaurantData[0]['parent_restaurant_id']==0){
            $branchData = Restaurant::select('id','parent_restaurant_id','restaurant_name','address','street','zipcode')->where('parent_restaurant_id',$cmsUser->restaurant_id)->get()->toArray();
            $parentRestaurant = $restaurantData;
            
        }elseif(isset($restaurantData[0]['parent_restaurant_id']) && $restaurantData[0]['parent_restaurant_id']>0){
            $branchData = Restaurant::select('id','parent_restaurant_id','restaurant_name','address','street','zipcode')->where('parent_restaurant_id',$restaurantData[0]['parent_restaurant_id'])->get()->toArray();
            if($branchData)
                $parentRestaurant = Restaurant::select('id','parent_restaurant_id','restaurant_name','address','street','zipcode')->where('id',$branchData[0]['parent_restaurant_id'])->get()->toArray();
        }

       return response()->view('compare.index', compact('parentRestaurant','branchData','branchId'));
    }
    
      public function getData(Request $request) {    
        
        $data = [];
        
        $branchId=$request->input('comparebranch');
        
        if(isset($branchId) && count($branchId)>1){
            
            $data ['branchId'] = $branchId;
            $catSubCatList = MenuCategory::whereIn('restaurant_id', $branchId)
            ->select('id', 'name','restaurant_id')
            ->with(['menuSubCategory' => function($query) {
                $query->select('id', 'name', 'menu_category_id', 'parent')
                    ->with('childrenRecursive')->where('parent', 0);
            }])
            ->orderBy('priority')->get()->toArray();
            $categories = [];
            
            if(!empty($catSubCatList)){
            foreach($branchId as $key =>$value){
                foreach($catSubCatList as $ckey=>$val){
                    if($val['restaurant_id']==$value)
                    $categories[$value][] = $val;
                }
            }
            }
    
            
            $data['category'] = $categories;
            $data['success'] = true;
            
        }else {
            $data = [
                'message' => 'Records not found.',
                'success' => false
            ];
        }

        return $data;   
        
    }
    
    
    public function getDataV2(Request $request) {    
        
        $data = [
                'message' => 'Records not found.',
                'success' => false
            ];
        
        $branchIds=$request->input('comparebranch');
    
        if(isset($branchIds) && !empty($branchIds)){
            if($request->input('compareWith') =='m'){
                return $this->getMenu($branchIds);
            }elseif($request->input('compareWith') =='s'){
                return $this->getService($branchIds);
            }elseif($request->input('compareWith') =='p'){
                return $this->getProfile($branchIds);
            }
        }       
        return $data;      
    }
    
    public function getMenu($branchIds){        
        foreach($branchIds as $key =>$branchId){
            $categories[$branchId][] = $this->menuData($branchId);                
        }            

        $data['category'] = $categories;
        $data['success'] = true;
            
        
        return $data;   
    }
    
    public function getService($branchIds){  
       
        $services=[];
        foreach($branchIds as $key =>$branchId){
            $services[$branchId] = $this->serviceData($branchId);                
        }            

        $data['service'] = $services;
        $data['success'] = true;
            
        
        return $data;   
    }
    
     public function getProfile($branchIds){        
        foreach($branchIds as $key =>$branchId){
            $profile[$branchId] = $this->profileData($branchId);                
        }            

        $data['profile'] = $profile;
        $data['success'] = true;
            
        
        return $data;   
    }
    
    public function serviceData($branchId){
        $service = Restaurant::select([
            'restaurants.delivery', 
            'restaurants.takeout',
            'restaurants.delivery_custom_message',
            'restaurants.takeout_custom_message',
            'restaurants.reservation',
            'restaurants.reservation_type',
            'restaurants.delivery_area',
            'restaurants.delivery_charge',
            'restaurants.free_delivery',
            'restaurants.cash',
            'restaurants.serve_alcohol',
            'restaurants.tip',
            'restaurants.city_tax',
            'restaurants.reservation_fee',
            'restaurants.tax',
            'restaurants.service_tax',
            'cities.state_id as cstate_id', 'cities.country_id as ccountry_id', 'pcities.state_id as pstate_id', 'pcities.country_id as pcountry_id'])
            ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
            ->leftJoin('cities as pcities', 'restaurants.city_id', '=', 'pcities.id')
            ->where('restaurants.id', $branchId)->orderBy('restaurants.id', 'ASC')->get();
        
        return $service;
    }
    
    public function profileData($branchId){
        
        $profile = Restaurant::select([
            'restaurants.restaurant_name', 
            'restaurants.tagline',
            'restaurants.slug',
            'restaurants.email',
            'restaurants.title',
            'restaurants.meta_tags',
            'restaurants.description',
            'restaurants.currency_code',
            'restaurants.address',
            'restaurants.address2',
            'restaurants.street',
            'restaurants.zipcode',
            'restaurants.phone',            
            'cities.state_id as cstate_id', 'cities.country_id as ccountry_id', 'pcities.state_id as pstate_id', 'pcities.country_id as pcountry_id'])
            ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
            ->leftJoin('cities as pcities', 'restaurants.city_id', '=', 'pcities.id')
            ->where('restaurants.id', $branchId)->orderBy('restaurants.id', 'ASC')->get()->toArray();
        
        return $profile;

    }
     
    public function getMenuOfSubcategory($id,$restaurant_id){
        $menuItems = [];
        if(isset($id) && $request->has('restaurant_id')){
            $request_params = [
               'menu_sub_category_id' => $id,
               'restaurant_id'=>$request->input('restaurant_id')
            ];
            $menuItemsData = MenuItemModel::select('*')
                ->where($request_params)            
                ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
            $menuItems['items']=$menuItemsData;
            $menuItems['success']=true;
        }else{
             $menuItems=['error'=>"Records not found.","success"=>false];
        }
            return $menuItems;
    }
    
    public function getMenuOfCategory(Request $request, $id){
        $menuItems = [];
        
        if(isset($id) && $request->has('restaurant_id')){
            
            $request_params = [
                'menu_category_id' => $id,   
                'restaurant_id'=> $request->input('restaurant_id')           
            ];
            $menuItemsData = MenuItemModel::select('*')
                ->where($request_params)
                ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
            $menuItems['items']=$menuItemsData;
            $menuItems['success']=true;
        }else{
            $menuItems=['error'=>"Records not found.","success"=>false];
        }
        return $menuItems;
    }
    
    
    
    ####################################################################################################################33
    public function menuData($restaurantID) {               
        
        if($restaurantID) {
            
            $menuCat = $menuItems = array();

            $menuCats = MenuCategory::where(['restaurant_id' => $restaurantID, 'status' => 1])->select('id','slug','name','status')->get();
           
            if ($menuCats) {

                foreach($menuCats as &$menuCat){
                    $category=$menuCat->slug;                 
                    
                    $subcats = $this->getItemsWithSubcatV2($menuCat->id, $restaurantID, $category);
                    $parent_cat_items = $this->getDirectParentCatOnlyV2($menuCat->id, $restaurantID, $category);
                    
                    $menuCat->subcats=$subcats;
                    $menuCat->menu_items_list=$parent_cat_items;

                }


            }
            
            return $menuCats;

        } else {
            
            return [];
        }
    }
    
    
    public function getItemsWithSubcatV2($cat_id, $locId, $category, $parent = 0) {

        $categoryData = MenuSubCategory::where('menu_category_id', '=', $cat_id)
            ->select('id','name',
                    'language_id',
                    'menu_category_id',
                    'name','pos_id',
                    'status','priority',
                    'description',
                    'sub_description',
                    'is_popular',
                    'is_favourite')
            ->where('status', '=', 1)
            ->where('parent', '=', $parent);

        $categoryData = $categoryData->orderBy('name', 'DESC')->get();
       
        if (count($categoryData)) {
            foreach ($categoryData as &$cat) {
                $cat->subcats = $this->getItemsWithSubcatV2($cat_id, $locId,  $category, $cat->id);
                $menuItems = $this->getMenuItemListV2($cat_id, $cat->id, $locId, $category);
                $cat->menu_items_list = $menuItems;
            }
        }
        
        return $categoryData;
    }
    
     public function getDirectParentCatOnlyV2($cat_id, $locId, $category) {
        return $this->getMenuItemListV2($cat_id, null, $locId, $category);
    }
    
    public function getMenuItemListV2($cat_id, $sub_cat_id = null, $locId,$category) {

        $localization_id = "en";

        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $language_id = $language_id->id;

        $request_params = [
            'new_menu_items.restaurant_id' => $locId,
            'new_menu_items.menu_category_id' => $cat_id,
            'new_menu_items.menu_sub_category_id' => $sub_cat_id,
            'new_menu_items.status' => 1,
        ];

        $menuItems = array();

        $menuItems = NewMenuItems::leftJoin('menu_sub_categories', 'new_menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
            ->where($request_params)
            ->select('new_menu_items.id','new_menu_items.name',
                    'new_menu_items.sub_description',
                    'new_menu_items.size_price',
                    'new_menu_items.size_weight',
                    'new_menu_items.status',
                    'new_menu_items.description',
                    'new_menu_items.delivery',
                    'new_menu_items.takeout',
                    'new_menu_items.menu_category_id',
                    'new_menu_items.menu_sub_category_id',
                    'new_menu_items.customizable_data',
                    'new_menu_items.image',                                
                    'menu_sub_categories.name as sub_category_name')
            ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
        foreach ($menuItems as $index => $menu) {
            $adonsDetails = $this->getItemsAdonsWithAddonsGroup($menu['id'], $language_id, $locId);
            $menuItems[$index]['adons'] = $adonsDetails;
            $modifierDetails = $this->getModifiersGroupV2($menu['id'], $language_id);
            $menuItems[$index]['modifier'] = $modifierDetails[0];
	    $menuItems[$index]['custom_dropdown'] = $modifierDetails[1];	
	    //if(isset())
            $menuItems[$index]['labels'] = $this->labelDetails($menu['id']);

        }
        // Remove menu items already in cart
        
        $temp = [];
        $defaultImage = null;
        if (strtolower($category) == 'pizza') {
            $defaultImage = '/images/menu/dummy_pizza_image.png';
        }
        
        $deleteFlagNIndex = 0;
        for ($i = 0; $i < count($menuItems); $i++) {
            // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
            if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 && strtolower($category) != 'pizza') {
                $menuItems[$i]['to_delete'] = $i;
                $deleteFlagNIndex = $i - 1;
            }
            #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
            $size_prices = json_decode($menuItems[$i]['size_price'], true);
            $this->getSizePrice($size_prices, $locId);

            $menuItems[$i]['size_price'] = $size_prices;
            $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
            //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);

//            $menuItems[$i]['img'] = [
//                "image" => $menuItems[$i]['image'] ?? $defaultImage,
//                "thumb_med" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
//                "thumb_sml" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
//                "image_caption" => $menuItems[$i]['image_caption'] ?? '',
//                "web_image" => $menuItems[$i]['web_image'] ?? $defaultImage,
//                "web_thumb_med" => $menuItems[$i]['web_thumb_med'] ?? $defaultImage,
//                "web_thumb_sml" => $menuItems[$i]['web_thumb_sml'] ?? $defaultImage,
//                "web_image_caption" => $menuItems[$i]['web_image_caption'] ?? '',
//                "mobile_web_image" => $menuItems[$i]['mobile_web_image'] ?? $defaultImage,
//                "mobile_web_thumb_med" => $menuItems[$i]['mobile_web_thumb_med'] ?? $defaultImage,
//                "mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_thumb_sml'] ?? $defaultImage,
//                "mobile_web_image_caption" => $menuItems[$i]['mobile_web_image_caption'] ?? '',
//                "banner_image" => $menuItems[$i]['banner_image'],
//                "thumb_banner_image" => $menuItems[$i]['banner_image'],
//            ];

           
            
//            unset($menuItems[$i]['customizable_data_old']);
//            unset($menuItems[$i]['thumb_image_med']);
//            unset($menuItems[$i]['banner_image']);
//            unset($menuItems[$i]['thumb_banner_image_med']);



            // set language description to item fields
            $newMenuItemsLanguage = NewMenuItemsLanguage::where([
                'new_menu_items_id' => $menuItems[$i]['id'],
                'language_id' => $language_id,
            ])->select('item_other_info','gift_message','name','description','sub_description')->first();

            if($newMenuItemsLanguage !== null) {
                $menuItems[$i]['item_other_info'] = $newMenuItemsLanguage->item_other_info;
                $menuItems[$i]['gift_message'] = $newMenuItemsLanguage->gift_message;
                $menuItems[$i]['name'] = $newMenuItemsLanguage->name;
                $menuItems[$i]['description'] = $newMenuItemsLanguage->description;
                $menuItems[$i]['sub_description'] = $newMenuItemsLanguage->sub_description;
            }
        }
        // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
        if ($deleteFlagNIndex) {
            //Commented on 07-09-2018 By RG as This was code done by Amit as I did not discussed with Amit
            //array_splice($menuItems, $deleteFlagNIndex, 1);
        }

        return $menuItems;

    }
    
    
    public function getModifiersGroupV2($menu_id, $language) {
        $modifierGroupData = ItemModifierGroup::where('menu_item_id', '=', $menu_id)->where('show_as_dropdown', '=', 0)->where('status', '=', 1)->select('modifier_groups.*')->orderBy('sort_order', 'ASC')->get()->toArray();
	$dropDowonData =ItemModifierGroup::where('menu_item_id', '=', $menu_id)->where('status', '=', 1)->where('show_as_dropdown', '=', 1)->select('modifier_groups.*')->orderBy('sort_order', 'ASC')->get()->toArray();;
        //$modifierGroupData = null;
        if (count($modifierGroupData)) {
            foreach ($modifierGroupData as $index => $cat) {
                $itemModifierGroupLanguage = ItemModifierGroupLanguage::where(['language_id' => $language, 'modifier_group_id' => $cat['id']])->first();
                if ($itemModifierGroupLanguage !== null) {
                    $modifierGroupData[$index]['group_name'] = $itemModifierGroupLanguage->group_name;
                    $modifierGroupData[$index]['prompt'] = $itemModifierGroupLanguage->prompt;
                }
                $modifierItems = $this->getModifierItems($cat['id'], $language);
		$modifierGroupData[$index]['modifier_items_list'] = $modifierItems;
            }
        }
	if (count($dropDowonData)) {
            foreach ($dropDowonData as $index => $cat) {
                $itemModifierGroupLanguage = ItemModifierGroupLanguage::where(['language_id' => $language, 'modifier_group_id' => $cat['id']])->first();
                if ($itemModifierGroupLanguage !== null) {
                    $dropDowonData[$index]['group_name'] = $itemModifierGroupLanguage->group_name;
                    $dropDowonData[$index]['prompt'] = $itemModifierGroupLanguage->prompt;
                }
                $modifierItems = $this->getModifierItems($cat['id'], $language);
		$dropDowonData[$index]['modifier_items_list'] = $modifierItems;
            }
        }
        return [$modifierGroupData,$dropDowonData];
    }
    
    public function getItemsAdonsWithAddonsGroup($menu_id, $language, $locId) {
        $adonsGroupData = ItemAddonGroups::where('menu_item_id', '=', $menu_id)
                ->select('id','group_name','prompt','quantity_type','quantity')
                ->get()->toArray();

        if (count($adonsGroupData)) {
            foreach ($adonsGroupData as $index => $cat) {
                // implement language
                $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
                    'item_addon_group_id' => $cat['id'],
                    'language_id' => $language
                ])->select('id','item_addon_group_id','group_name','language_id','prompt')->first();
                if ($itemAddonGroupsLanguage !== null) {
                    $adonsGroupData[$index]['group_name'] = $itemAddonGroupsLanguage->group_name;
                    $adonsGroupData[$index]['prompt'] = $itemAddonGroupsLanguage->prompt;
                }
                $adonItems = $this->getAdonsItems($cat['id'], $language, $locId);

                $adonsGroupData[$index]['adons_items_list'] = $adonItems;
            }
        }
        return $adonsGroupData;
    }
    
    public function getModifierItems($group_id, $language) {
        $modifierData = ItemModifier::where('modifier_group_id', '=', $group_id)->where('status', '=', 1)
                ->select('modifier_items.id','modifier_items.modifier_name','modifier_items.modifier_group_id','modifier_items.modifier_category_id','modifier_items.price','modifier_items.special_price','modifier_items.start_date','modifier_items.start_date','modifier_items.is_selected')
                ->orderBy('sort_order', 'ASC')->get()->toArray();
        if (count($modifierData)) {
            foreach ($modifierData as $index => $cat) {
                $ItemModifierLanguage = ItemModifierLanguage::where(['language_id' => $language, 'modifier_item_id' => $cat['id']])
                        ->select('id','modifier_name')->first();
                if ($ItemModifierLanguage !== null) {
                    $modifierData[$index]['modifier_name'] = $ItemModifierLanguage->modifier_name;
                }
                $modifierData[$index]['isSelected']=$cat['is_selected'];
                $modifierData[$index]['price'] = $cat['price'];
                if(isset($cat['special_price']) && isset($cat['start_date']) && isset($cat['end_date'])){
                    $special_price=$cat['special_price'];
                    $special_price_start_date=$cat['start_date'];
                    $special_price_end_date=$cat['end_date'];
                    $location_id = $request->header('X-location');
                    $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $location_id, 'datetime' =>date('Y-m-d H:i:s')));
                    $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                    $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                    if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
                        $modifierData[$index]['price'] = $special_price;
                        $modifierData[$index]['cutout_price'] =$cat['price'];

                        unset($modifierData[$index]['special_price']);
                        unset($modifierData[$index]['start_date']);
                        unset($modifierData[$index]['end_date']);
                    }
                }
                $modifierItems = $this->getModifierItemsOptionGroup($cat['id'], $language);
                $modifierData[$index]['modifier_OptionGroup_list'] = $modifierItems;
            }
        }
        return $modifierData;
    }
    
    public function getModifierItemsOptionGroup($group_id, $language) {
        $modifierData = ItemModifierOptionGroup::where('modifier_item_id', '=', $group_id)
                ->select('modifier_item_option_groups.id','modifier_item_option_groups.group_name','modifier_item_option_groups.modifier_item_id','modifier_item_option_groups.quantity_type','modifier_item_option_groups.quantity')->get()->toArray();
        if (count($modifierData)) {
            foreach ($modifierData as $index => $cat) {
                $itemModifierOptionGroupLanguage = ItemModifierOptionGroupLanguage::where([
                    'language_id' => $language,
                    'modifier_option_group_id' => $cat['id']
                ])->first();
                if($itemModifierOptionGroupLanguage !== null) {
                    $modifierData[$index]['group_name'] = $itemModifierOptionGroupLanguage->group_name;
                    $modifierData[$index]['prompt'] = $itemModifierOptionGroupLanguage->prompt;
                }
                $modifierItems = $this->getModifierItemsOptionGroupItem($cat['id'], $language);
                $modifierData[$index]['modifier_OptionGroup_item_list'] = $modifierItems;
            }
        }
        return $modifierData;
    }
    
    public function getModifierItemsOptionGroupItem($group_id, $language, Request $request) {
        $modifierData = ItemModifierOptionGroupItem::where('modifier_option_group_id', '=', $group_id)
                ->select('modifier_item_options.id','modifier_item_options.group_name','modifier_item_options.quantity_type','modifier_item_options.quantity','modifier_item_options.modifier_item_id')->get()->toArray();

        if(count($modifierData)) {
            foreach ($modifierData as $index => $row) {
                $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where(['language_id' => $language, 'modifier_option_id' => $row['id']])->first();
                if ($itemOptionsModifierLanguage !== null) {
                    $modifierData[$index]['modifier_option_name'] = $itemOptionsModifierLanguage->modifier_option_name;
                }
            }
        }
        return $modifierData;
    }
    
    public function labelDetails($menu_id)
    {
        $labelIdRes      = MenuItemLabels::select('id','labels','size','item_id')
            ->where('item_id', $menu_id)->first();
        $labelNameData = [];
        $serving = [0,0];
        if($labelIdRes) {
            $labelData = $labelIdRes->toArray();
            $labelIds = strlen($labelData['labels']) ? explode(',', $labelData['labels']) : '';
            if(strpos($labelData['size'], ',') !== false ) {
                $serving  = explode(',', $labelData['size']);
            } else {
                $serving  = [1,1];
            }
            $labelNameData = Labels::select('id','name','type')->whereIn('id', $labelIds)->get();
            if($labelNameData) {
                $labelNameData = $labelNameData->toArray();
            }
        }
        $labelData = [
            'label' => $labelNameData,
            'serving_size_min'  => $serving[0] <= $serving[1] ? $serving[0] : $serving[1],
            'serving_size_max'  => $serving[0] > $serving[1] ? $serving[0] : $serving[1],
        ];
        return $labelData;
    }
    
     public function getSizePrice(&$size_price_list, $location_id, $extra = array()) {       
        $current_menu_meal = DB::table('menu_meal_types')
                ->where('restaurant_id', $location_id)
                ->where('status', 1)
                ->first();
        if ($size_price_list) {
            foreach ($size_price_list as $key => $size) {
                $has_price = false;
                if ((count($size['price']) > 1) || !isset($size['price']['0'])) {

                    // It is price based on menu meal
                    if ($current_menu_meal) {
                        if (isset($size['price'][$current_menu_meal->id])) {

                            $size_price_list[$key]['price'] = $size['price'][$current_menu_meal->id];
                            if (isset($size['available'])) {
                                $size_price_list[$key]['is_delivery'] = $size['available'][$current_menu_meal->id]['is_delivery'];
                                $size_price_list[$key]['is_carryout'] = $size['available'][$current_menu_meal->id]['is_carryout'];
                            } else {
                                // for old data in DB if any 
                                $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                            }
                            $has_price = true;
                        }
                    } else {
                        // unset this price item as This is not available in meal type @26-07-2018
                        unset($size_price_list[$key]);
                    }
                } elseif (count($size['price']) == 1) {
                    // It is all price

                 //   $size_price_list[$key]['price'] = $size['price']['0'];
                    $size_price_list[$key]['price'] = $size['price']['0'];
                    if(isset($size['special_price']) && isset($size['start_date']) && isset($size['end_date'])){
                        $special_price=$size['special_price'];
                        $special_price_start_date=$size['start_date'];
                        $special_price_end_date=$size['end_date'];

                        $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $location_id, 'datetime' =>date('Y-m-d H:i:s')));
                        $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                        $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                        if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
                            $size_price_list[$key]['price'] = $special_price;
                            $size_price_list[$key]['cutout_price'] = $size['price']['0'];

                            unset($size_price_list[$key]['special_price']);
                            unset($size_price_list[$key]['start_date']);
                            unset($size_price_list[$key]['end_date']);
                        }
                    }

                    if (isset($size['available'])) {
                        $size_price_list[$key]['is_delivery'] = $size['available']['is_delivery'];
                        $size_price_list[$key]['is_carryout'] = $size['available']['is_carryout'];
                    } else {
                        // for old data in DB if any 
                        $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                    }
                    $has_price = true;
                }
                //check if price does not exists then remove it from the array
                if (!$has_price) {
                    unset($size_price_list[$key]);
                }
                # 9-Aug-19 - As discussed with Deepak, Prakash, Nishant
                // check if is_delivery & is_carryout is 0, remove it from the array
                if(isset($size_price_list[$key]['is_delivery']) && isset($size_price_list[$key]['is_carryout']) && $size_price_list[$key]['is_delivery'] == 0 && $size_price_list[$key]['is_carryout'] == 0) {
                    unset($size_price_list[$key]);
                }
                unset($size_price_list[$key]['available']);
            }
            $size_price_list = array_values($size_price_list);
        }
        #check availabiltity of the menu Items @21-12-2018 RG
        if(isset($extra['slot_code']) && $extra['slot_code']) {
            $check_availablity = $this->check_menu_availablity($extra);
           
            foreach ($size_price_list as &$list) {
                if($list['is_delivery'] == 1) {
                    //means delivery is allowed for that Item
                    //Now check if that item is available in that time
                    $list['is_delivery'] = $check_availablity['is_delivery'];
                }
                if($list['is_carryout'] == 1) {
                    //means delivery is allowed for that Item
                    //Now check if that item is available in that time
                    $list['is_carryout'] = $check_availablity['is_carryout'];
                }
            }
        }
        # End visibility Time        
    }
    public function getAdonsItems($group_id, $language, $locId) {
        $adonsData = ItemAddons::where('addon_group_id', '=', $group_id)->select('item_addons.*')->get()->toArray();
        $item = null;
        if (count($adonsData)) {
            foreach ($adonsData as $index => $adonsInfo) {
                $item1 = $this->getItemInfoById($adonsInfo['item_id'], $locId, $language);

                if ($item1 !== null) {
                    $item1['adon_details'] = $adonsInfo;
                    $item[] = $item1;
                }

            }
        }
        return $item;
    }
    
    
    public function getItemInfoById($id, $locId, $language_id) {
        $menuItems1 = NewMenuItems::leftJoin('menu_sub_categories', 'new_menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
            ->where(['new_menu_items.id' => $id, 'new_menu_items.status' => 1])
            ->select('new_menu_items.*', 'menu_sub_categories.name as sub_category_name')
            ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
        $temp = [];
        $defaultImage = null;
        $menuItems = null;
        if (isset($menuItems1[0])) {
            $menuItems = $menuItems1[0];
           
            $deleteFlagNIndex = 0;
            //for ($i = 0; $i < count($menuItems); $i++) {

            #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
            if (!empty($menuItems['size_price'])) {
                $size_prices = json_decode($menuItems['size_price'][0], true);
                $this->getSizePrice($size_prices, $locId);
            }

            $menuItems['size_price'] = $size_prices;
            $menuItems['customizable_data'] = json_decode($menuItems['customizable_data'][0], true);

            $modifierDetails = $this->getModifiersGroupV2($menuItems['id'], $language_id);
            $menuItems['modifier'] = $modifierDetails;



            // set language items
            $newMenuItemsLanguage = NewMenuItemsLanguage::where([
                'new_menu_items_id' => $id,
                'language_id' => $language_id,
            ])->first();

            if($newMenuItemsLanguage !== null) {
                $menuItems['item_other_info'] = $newMenuItemsLanguage->item_other_info;
                $menuItems['gift_message'] = $newMenuItemsLanguage->gift_message;
                $menuItems['name'] = $newMenuItemsLanguage->name;
                $menuItems['description'] = $newMenuItemsLanguage->description;
                $menuItems['sub_description'] = $newMenuItemsLanguage->sub_description;
            }
            if(!empty($menuItems['images'])) {
                $menuItems['images']  = json_decode($menuItems['images'], true);
            }
            //}
        }
        return $menuItems;
    }

}