<?php

namespace App\Http\Controllers;

use App\Models\SmsContent;
use App\Models\MailTemplate;
use App\Mail\NotifyMail;
use App\Jobs\NotifySms;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Mail;
use Log;

class NotificationController extends Controller {

    public function test() {
        $this->sendMail('reservation_cancel', 1, ['to' => 'ashokbhati.php@gmail.com'], ['first_name' => 'Rashid Bhat']);
        //$this->sendSms('reservation_cancel',1,['to'=>9650719414],['first_name'=>'Rakesh Bisht']);
    }

    public function sendSms($template_name, $restaurant_id, array $config, array $data) {

        if (count($config) && isset($config['to']) && !empty($config['to']) && count($data) && !empty($template_name) && !empty($restaurant_id)) {
            $finaltemplate = $this->getTemplate('sms', $template_name, $restaurant_id, $data);
            if ($finaltemplate) {

                //$rsp=dispatch(new NotifySms($config['to'],$finaltemplate));

                $resp = NotifySms::dispatch($config['to'], $finaltemplate)->onQueue('processing');
            }
        }
    }

    public function sendMail($template_name, $restaurant_id, array $config, array $data) {


        if (count($config) && isset($config['to']) && !empty($config['to']) && count($data) && !empty($template_name) && !empty($restaurant_id)) {


            $finaltemplate = $this->getTemplate('email', $template_name, $restaurant_id, $data);

            $mail = Mail::to($config['to']);

            /* if(isset($config['from']) && !empty($config['from'])){

              $mail=$mail->from($config['from']);
              } */

            if (isset($config['cc']) && !empty($config['cc'])) {

                $mail = $mail->cc($config['cc']);
            }
            if (isset($config['bcc']) && !empty($config['bcc'])) {

                $mail = $mail->bcc($config['bcc']);
            }
            $data = [
                'content' => $finaltemplate, 'subject' => $config['subject']
            ];
            if (isset($config['from_address']) && isset($config['from_name']) && !empty($config['from_address']) && !empty($config['from_name'])) {

                $data['from_address'] = $config['from_address'];
                $data['from_name'] = $config['from_name'];
            }
            $resp = $mail->queue(new NotifyMail($data));
        } else {
            
        }
    }

    public function getHeaderTemplate($restaurant_id) {

        return MailTemplate::select(['id', 'subject', 'content'])->whereIn('restaurant_id', array( $restaurant_id))->where('template_name', 'header_layout')->where('status', '1')->orderBy('restaurant_id', 'DESC')->first();
    }

    public function getFooterTemplate($restaurant_id) {

        //return MailTemplate::select(['id','subject','content'])->where('restaurant_id',$restaurant_id)->where('template_name', 'footer_layout')->where('status', '1')->first();
        return MailTemplate::select(['id', 'subject', 'content'])->whereIn('restaurant_id', array( $restaurant_id))->where('template_name', 'footer_layout')->where('status', '1')->orderBy('restaurant_id', 'DESC')->first();
    }

    public function getEmailByTemplateId($template_name, $restaurant_id) {

        //return MailTemplate::select(['id','subject','content'])->where('restaurant_id',$restaurant_id)->where('template_name',$template_name)->where('status', '1')->first();
        return MailTemplate::select(['id', 'subject', 'content'])->whereIn('restaurant_id', array( $restaurant_id))->where('template_name', $template_name)->where('status', '1')->orderBy('restaurant_id', 'DESC')->first();
    }

    public function getSmsByTemplateId($sms_name) {

        return SmsContent::select(['id', 'content'])->where('sms_name', $sms_name)->where('status', '1')->first();
    }

    public function getFinalTemplate(array $data, $template_content) {

        if ($template_content) {

            $content = $template_content;
            preg_match_all("/\[(.*?)\]/", $content, $options_array);
            if (isset($options_array[1]) && count($options_array[1])) {
                foreach ($options_array[1] as $key) {
                    
                    if (array_key_exists(strtolower($key), $data)) {
                        $content = preg_replace('/\[' . strtolower($key) . '\]/', $data[strtolower($key)], $content);
                    } elseif (array_key_exists(strtoupper($key), $data)) {
                        $content = preg_replace('/\[' . strtoupper($key) . '\]/', $data[strtoupper($key)], $content);
                    }
                }
                return $content;
            }
        }
    }

    public function getTemplate($type = 'email', $template_name, $restaurant_id, array $data) {

        if ($type == 'email' && !empty($restaurant_id)) {
            $restaurant = Restaurant::select(['restaurant_name', 'address'])->where('id', $restaurant_id)->first();

            $template = $this->getEmailByTemplateId($template_name, $restaurant_id);
            
            $headerTmp = $this->getHeaderTemplate($restaurant_id);
            $footerTmp = $this->getFooterTemplate($restaurant_id);
            if(isset($template->content))
            $template_content = $headerTmp->content . $template->content . $footerTmp->content;
            else $template_content = $headerTmp->content . $template_name . $footerTmp->content;
            Log::info('EMAIL TESTING ');
            Log::info($data);
        } else if ($type == 'sms') {

            $template = $this->getSmsByTemplateId($template_name);
            $template_content = $template->content;
        }
        if ($template && !empty($restaurant_id) && count($data)) {

            return $this->getFinalTemplate($data, $template_content);
        }
    }

}
