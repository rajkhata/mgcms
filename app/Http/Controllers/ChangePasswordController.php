<?php

namespace App\Http\Controllers;
use App\Helpers\CommonFunctions;
use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Models\CmsUser;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Change-password');
    }

    public function index(Request $request)
    {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        //$locationInfo = Restaurant::select('*')->where(['restaurants.id' => $restArray[0]])->first();
        $id = $restArray[0];
        $cmsUser = CmsUSer::where('restaurant_id', '=',$restArray[0]);
        return view('profile_setup.change_password',compact('cmsUser','id'));
    }
    public function updatePassword(Request $request)
    {
        $request_data = $request->All();
        $rules = [
            'oldpassword'          =>  'required',
            'newpassword'          =>  'required|min:5',
            'confirmpassword'   =>  'required|same:newpassword',
        ];
    
        $messages = [
            'oldpassword.required'         => 'Old password cannot be blank',
            'newpassword.required'         => 'New password cannot be blank',
            'newpassword.min'              => 'New Password should be minimal 5 character',
            'confirmpassword.same'      => 'Confirm Password should be same as new password!',
        ];
    
        $validator = Validator::make($request_data, $rules, $messages);
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->with("error","Error!");;
        } else { 

            if (!(Hash::check($request->get('oldpassword'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->with("error","Your current password does not matches with the old password you provided. Please try again.");
            }
    
            if(strcmp($request->get('oldpassword'), $request->get('newpassword')) == 0){
                //Current password and new password are same
                return redirect()->back()->with("error","New Password cannot be same as your old password. Please choose a different password.");
            } 
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('newpassword'));
            $user->save();
            
            return redirect()->back()->with("message","Password changed successfully !");
        }
    }

    /*public function create()
    {
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','ASC')->get();
        $stateData = State::select('id','state')->orderBy('id','ASC')->get();
        $cityData = array();
        //$cityData = City::select('id','city_name')->orderBy('id','DESC')->get();
        return view('cms_user.create', compact('restaurantData','stateData','cityData'));
    }

    public function edit(Request $request,$id)
    {
        if(isset($id) && is_numeric($id)) {
            $cityData = $restaurantData = array();
            //$restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('restaurant_name', 'ASC')->get();
            $stateData = State::select('id', 'state')->orderBy('state', 'ASC')->get();
            $cmsUser = CmsUSer::find($id);
            if (!empty($cmsUser->state_id)) {
                $cityData = City::where('state_id', '=', $cmsUser->state_id)->orderBy('city_name', 'ASC')->get();
            }
            if (!empty($cmsUser->city_id)) {
                $restaurantData = Restaurant::where('city_id', '=', $cmsUser->city_id)->orderBy('restaurant_name', 'ASC')->get();
            }
            return view('cms_user.edit',compact('cmsUser','restaurantData','stateData','cityData'));
        }
        return Redirect::back();
    }

    public function update(Request $request, $id)
    {
        if(!isset($id) || !is_numeric($id))
        {
            return redirect()->back()->with('err_msg','Invalid Id');
        }
        $cmsUserObj = CmsUser::where('id',$id)->first();
        if(empty($cmsUserObj))
        {
            return redirect()->back()->with('err_msg','Invalid Id');
        }

        $rules = [
            'name' => 'required|max:100',
            'password' => 'sometimes|nullable|max:50|min:5',
            'restaurant_id' => 'required|exists:restaurants,id',
            'state_id'   => 'required|exists:states,id',
            'city_id'   => 'required|exists:cities,id',
            'email'   => 'required|email|unique:cms_users,email,' . $cmsUserObj->id,
            'mobile'   => 'required|digits:10|min:0|unique:cms_users,mobile,' . $cmsUserObj->id,
            'phone'   => 'sometimes|nullable|numeric',
            'status' => 'required|in:0,1',
            'memail' => 'sometimes|nullable|max:50'
        ];
        $errMsgs = ['memail_tmp.*'=>'Please enter (,) seperated valid Emails upto 50 chars',
            'mobile'=>'Please enter valid 10 digits mobile'
        ];
        $memail = NULL;
        if($request->memail) {
            $request->merge(array('memail_tmp' => preg_split('~, *~', $request->memail)));
            $rules['memail_tmp.*'] = 'sometimes|nullable|email';
            //$memail =  preg_replace('/\s+/', '', implode(', ', $request->memail));
        }

        $validator = Validator::make($request->all(),$rules,$errMsgs);
        if($validator->passes()) {
            $cmsUserObj->name = $request->input('name');
            $cmsUserObj->restaurant_id = $request->input('restaurant_id');

            if($request->input('password'))
                $cmsUserObj->password = Hash::make($request->input('password'));

            $cmsUserObj->state_id = $request->input('state_id');
            $cmsUserObj->city_id = $request->input('city_id');
            $cmsUserObj->email = $request->input('email');
            $cmsUserObj->mobile = $request->input('mobile');
            $cmsUserObj->phone = $request->input('phone');
            $cmsUserObj->status = $request->input('status');
            $cmsUserObj->memail = $request->input('memail');
            $cmsUserObj->save();
            return Redirect::back()->with('message', 'Data updated successfully');
        }
         
        $cityData = $restaurantData = array();
        if(Input::get('state_id') && is_numeric(Input::get('state_id')))
        {
            $cityData = City::where('state_id', '=', Input::get('state_id'))->orderBy('city_name', 'ASC')->get();
        }
        if(Input::get('city_id') && is_numeric(Input::get('city_id')))
        {
            $restaurantData = Restaurant::where('city_id', '=', Input::get('city_id'))->orderBy('restaurant_name', 'ASC')->get();
        }

        if($validator->errors())
        {
            $validatorErrs = $validator->errors()->toArray();
            foreach($validatorErrs as $k => $v) {
                if (strpos($k, 'memail_tmp') !== false) {
                    $validator->errors()->add('memail', $v[0]);
                    break;
                }
            }
        }
        return redirect()->back()->withErrors($validator->errors())->withInput()->with('newCityData',$cityData)->with('newRestaurantData',$restaurantData);
    }
    public function store(Request $request)
    {
        //print_r($request->all()); die;
        $rules = [
            'name' => 'required|max:100',
            'password' => 'required|max:50|min:5',
            'restaurant_id' => 'required|exists:restaurants,id',
            'state_id'   => 'required|exists:states,id',
            'city_id'   => 'required|exists:cities,id',
            'email'   => 'required|email|unique:cms_users,email',
            'mobile'   => 'required|digits:10|min:0|unique:cms_users,mobile',
            'phone'   => 'sometimes|nullable|numeric',
            'status' => 'required|in:0,1',
            'memail' => 'sometimes|nullable|max:50'
        ];
        $errMsgs = ['memail.*'=>'Please enter valid Emails',
            'mobile'=>'Please enter valid 10 digits mobile'
        ];
        $memail = NULL;
        if($request->memail) {
            $request->merge(array('memail' => preg_split('~, *~', $request->memail)));
            $rules['memail.*'] = 'sometimes|nullable|email';
            $memail =  preg_replace('/\s+/', '', implode(', ', $request->memail));
        }
        $validator = Validator::make($request->all(),$rules,$errMsgs);

        if($validator->passes()) {
            $cmsUserArr = [
                'restaurant_id' => $request->input('restaurant_id'),
                'state_id' => $request->input('state_id'),
                'city_id' => $request->input('city_id'),
                'name' => $request->input('name'),
                'password' => Hash::make($request->input('password')),
                'email' => $request->input('email'),
                'mobile' => $request->input('mobile'),
                'phone' => $request->input('phone'),
                'status' => $request->input('status'),
                'memail' => $memail,
                'api_token'=>Str::random(60)
            ];
            $cmsUserObj = CmsUser::create($cmsUserArr);
            return Redirect::back()->with('message', 'User added successfully');
        }
        if($request->memail) {
            $request->merge(array('memail' => implode(', ', $request->memail)));
        }
        $cityData = $restaurantData = array();
        if(Input::get('state_id') && is_numeric(Input::get('state_id')))
        {
            $cityData = City::where('state_id', '=', Input::get('state_id'))->orderBy('city_name', 'ASC')->get();
        }
        if(Input::get('city_id') && is_numeric(Input::get('city_id')))
        {
            $restaurantData = Restaurant::where('city_id', '=', Input::get('city_id'))->orderBy('restaurant_name', 'ASC')->get();
        }
        return redirect()->back()->withErrors($validator->errors())->withInput()->with('newCityData',$cityData)->with('newRestaurantData',$restaurantData);
    }

    public function get_cities_by_state(Request $request)
    {
        $sid = $request->input('state_id');
        $result = [];
        if(!empty($sid) && is_numeric($sid)) {
            $result = City::where('state_id', '=', $sid)->select('id','city_name')->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }

    public function get_restaurant_by_city(Request $request)
    {
        $cid = $request->input('city_id');
        if(!empty($cid) && is_numeric($cid)) {
            $result = Restaurant::where('city_id', '=', $cid)->select('id','restaurant_name','zipcode')->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }*/

}
