<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\Models\ModifierCategories;
use App\Models\Restaurant;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Helpers\CommonFunctions;
use File;
use Config;

 
class ModifierCategoriesController extends Controller {
 

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $inputData = [
            'restaurant_id' => 0,
            'category_name' => ''
        ];

        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();

        if ($restaurantData && count($restaurantData) == 1) {
            $inputData['restaurant_id'] = $restaurantData[0]['id'];
        }

        $rules = [
            'category_name' => 'sometimes|nullable|max:100',
            'restaurant_id' => 'sometimes|nullable'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $modifierCatgObj = ModifierCategories::orderBy('id', 'DESC');

            if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
                $inputData['restaurant_id'] = $request->input('restaurant_id');
                $modifierCatgObj->where('restaurant_id', $request->input('restaurant_id'));
            }
            if ($request->input('category_name') && is_string($request->input('category_name'))) {
                $inputData['category_name'] = $request->input('category_name');
                $modifierCatgObj->where('category_name', 'like', '%'. $request->input('category_name').'%');
            }

            $modifierCategories = $modifierCatgObj->paginate(20);
            return view('modifier_categories.index', compact('modifierCategories', 'restaurantData'))->with('inputData', $inputData);
        }

        return redirect()->back()->withErrors($validator->errors())->with('inputData', $inputData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
        $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();

        return view('modifier_categories.create', compact('restaurantData', 'languageData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = [
            'category_name' => 'required|max:100',
            'restaurant_id' => 'required|numeric',
        ];

        $validator = Validator::make(Input::all(), $rules, [
//                    'language_id.*' => 'Please select valid Language',
                    'restaurant_id.*' => 'Please select Restaurant',
        ]);

        if ($validator->passes()) {
            $modifierCatgObj = ModifierCategories::create(Input::all());
            return redirect()->back()->with('message', 'Modifier category added successfully');
        }
        return redirect()->back()->withErrors($validator->errors())->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (isset($id) && is_numeric($id)) {
            $modifierCatg = ModifierCategories::find($id);
            $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();

            return view('modifier_categories.edit', compact('modifierCatg', 'restaurantData'));
        }
        return Redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (isset($id) && is_numeric($id)) {
            $rules = [
                'category_name' => 'required|max:100',
                'restaurant_id' => 'required|numeric',
            ];

            $validator = Validator::make(Input::all(), $rules, [
//                    'language_id.*' => 'Please select valid Language',
                        'restaurant_id.*' => 'Please select Restaurant',
            ]);

            if ($validator->passes()) {
                $inputArr = Input::except('_method', '_token', 'id');
                ModifierCategories::where('id', $id)->update($inputArr);
                return redirect()->back()->with('message', 'Modifier category updated successfully');
            } else {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
        }
        return Redirect::back()->with('message', 'Invalid Id');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {   $id = Input::get('id');
        if(isset($id) && is_numeric($id)) {
            $catgObj = ModifierCategories::find($id);
            if (!empty($catgObj)) {
                $catgObj->delete();
                return Redirect::back()->with('message', 'Modifier category deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg','Invalid Id');
    }
}
