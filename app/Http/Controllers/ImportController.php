<?php

namespace App\Http\Controllers;


use App\Models\Restaurant;
use App\Models\MenuItem;
use App\Models\MenuCategory;
use App\Models\MenuSubCategory;
use App\Models\MenuMealType;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\CommonFunctions;

use Carbon\Carbon;
use DB;

class ImportController extends Controller
{
    
    public function index($restaurant_id)
    {

       return view('import.index');

    } 
    public function saveMenuItems($restaurant_id,$value){

         if(!empty($value[0]) && !empty($value[2]) && !empty($value[3]) &&  !empty($value[6])){                       
                        $itemData=array();
                        $itemData['restaurant_id']=$restaurant_id;
                        $itemData['product_type']=$value[2];
                        $itemData['menu_category_id']=$value[0];
                        $itemData['menu_sub_category_id']=$value[1]??NULL;
                        $itemData['name']=$value[3]??NULL;
                        $itemData['description']=$value[4]??NULL;
                        $itemData['sub_description']=$value[5]??NULL;
                        $itemData['status']=$value[6];
                        $Size1PriceType=$value[9];
                        $size_price=[];
                        if(!empty($Size1PriceType) && !empty($value[7])){
                            $size_price['size']=$value[7];
                            if(trim($Size1PriceType)=='All'){                                
                                $size_price['price']=[$value[8]];
                                $size_price['available']=['is_delivery'=>(int)$value[10],'is_carryout'=>(int)$value[11]];
                            }else{

                            }

                            $itemData['size_price'] = json_encode([$size_price]);

                        }   
                        //$size_price['meal_type']=$value[12];
                        //$itemData['size_price'] = json_encode($sizePriceArr);
                        // $itemData['is_tax_applicable']=;
                         $itemall[]=$itemData;
                         MenuItem::create($itemData);
                     }

    }
  
    public function saveMainCategories($restaurant_id,$value){

         if(!empty($value[0]) && !empty($value[1]) && !empty($value[4])){                       
                        $itemData=array();
                        $itemData['restaurant_id']=$restaurant_id;
                        $itemData['product_type']=$value[0];                     
                        $itemData['name']=$value[1]??NULL;
                        $itemData['slug']=strtolower(str_replace(' ', '_', $value[1]))??NULL;
                        $itemData['description']=$value[2]??NULL;
                        $itemData['sub_description']=$value[3]??NULL;
                        $itemData['status']=$value[4];
                        $itemData['priority']=$value[5]??NULL;
                        $itemData['is_delivery']=$value[6]??1;
                        $itemData['is_carryout']=$value[7]??1;                       
                        $itemData['image_class']=$value[8]??NULL;                       
                      
                         MenuCategory::create($itemData);
                     }

  
    }
       public function saveSubcats($restaurant_id,$value){

         if(!empty($value[0]) && !empty($value[2]) && !empty($value[5])){                       
                        $itemData=array();
                        $itemData['restaurant_id']=$restaurant_id;
                        $itemData['menu_category_id']=$value[0];                     
                        $itemData['parent']=$value[1]??0;                     
                        $itemData['name']=$value[2]??NULL;
                        $itemData['description']=$value[3]??NULL;
                        $itemData['sub_description']=$value[4]??NULL;
                        $itemData['status']=$value[5];
                        $itemData['priority']=$value[6]??NULL;                                         
                        $itemData['image_class']=$value[7]??NULL;                       
                      
                         MenuSubCategory::create($itemData);
                     }

  
    }
     public function saveMenuMealType($restaurant_id,$value){

         if(!empty($value[0]) && !empty($value[3])){                       
                        $itemData=array();
                        $itemData['restaurant_id']=$restaurant_id;
                        $itemData['name']=$value[0]??NULL;                    
                        $itemData['status']=$value[3]??1;

                        $itemData['from_time']=$value[1]??'00:00:00';
                        $itemData['to_time']=$value[2]??'00:00:00';
                                   
                      
                         MenuMealType::create($itemData);
                     }
  
    }
    

    public function save($restaurant_id,Request $request)
    {
       
        if($request->has('importtype') && $request->has('csv')){
            $importtype=$request->input('importtype');
           DB::beginTransaction();
            try {                    
                $tmpName = $request->file('csv');
                $csvAsArray = array_map('str_getcsv', file($tmpName));
                //echo $restaurant_id;
                
                $onlyheaders=array_shift($csvAsArray);
                //print_r($onlyheaders);
                //print_r($csvAsArray);
                $itemall=array();
               
                foreach ($csvAsArray as $key => $value) {
                    if($importtype=='menu_item'){                        
                        $this->saveMenuItems($restaurant_id,$value);   
                    }elseif($importtype=='menu_categories'){                        
                        $this->saveMainCategories($restaurant_id,$value);   
                    }elseif($importtype=='menu_meal_types'){                        
                        $this->saveMenuMealType($restaurant_id,$value);   
                    }elseif($importtype=='menu_sub_categories'){                        
                        $this->saveSubcats($restaurant_id,$value);   
                    }





                }
                //print_r($itemall);   
               // $menuItemObj = MenuItem::create($itemall[0]);
                DB::commit();
               return back()->withInput()->with(['status'=>'Imported successfully','importtype'=>$request->get('importtype')]);
             
            }catch (\Exception $e){                    
                //DB::rollback();
               return back()->withInput()->withErrors(array('message' =>'Invalid request'));
            } 

        }else{
            return back()->withInput()->withErrors(array('message' =>'Invalid request'));
       }

    }  


   

}
