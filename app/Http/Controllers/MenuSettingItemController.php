<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Models\MenuSettingCategory;
use App\Models\Restaurant;
use App\Models\MenuCategory;
use App\Models\MenuSettingItem;
use Illuminate\Support\Facades\Input;
use App\Models\Language;

class MenuSettingItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan view:clear');
        //exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan cache:clear');
    }

    public function create(Request $request)
    {
        $groupRestData = Restaurant::group();
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        return view('menu_setting_item.create', compact('groupRestData', 'languageData'));
    }

    public function edit(Request $request,$id)
    {

        if(isset($id) && is_numeric($id)) {
            $categoryData  = $settingCategoryData = array();
            $groupRestData = Restaurant::group();
            $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
            $menuSettingItem = MenuSettingItem::find($id);
            if (!empty($menuSettingItem->restaurant_id)) {
                $categoryData = MenuCategory::where('restaurant_id', '=', $menuSettingItem->restaurant_id)->orderBy('name', 'DESC')->get();
            }
            if (!empty($menuSettingItem->menu_category_id)) {
                $settingCategoryData = MenuSettingCategory::where('menu_category_id', '=', $menuSettingItem->menu_category_id)->orderBy('name', 'DESC')->get();
            }
            return view('menu_setting_item.edit')->with('groupRestData', $groupRestData)->with('menuSettingItem', $menuSettingItem)->with('categoryData', $categoryData)->with('menuSettingCategoryData',$settingCategoryData)->with('languageData',$languageData);
        }
        return Redirect::back();
    }

    public function update($id)
    {
        $rules = [
            'name' => 'required|max:100',
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'status' => 'required|in:0,1',
            'menu_category_id' => 'required|numeric|exists:menu_categories,id',
            'menu_setting_category_id' => 'required|numeric|exists:menu_setting_categories,id',
            'side_flag' => 'required|in:0,1',
            'quantity_flag' => 'required|in:0,1',
            'price' => 'required|numeric'
        ];
        $rulesMsg = [
            'name.*' => 'Please enter valid name upto 100 chars',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status',
            'menu_category_id.*' => 'Please Select Category',
            'menu_setting_category_id.*' => 'Please Select Menu Setting Category',
            'side_flag' => 'Please select sides type',
            'quantity_flag' => 'Please select quatity type',
            'price.*' => 'Please enter numeric price'
        ];
        $validator = Validator::make(Input::all(), $rules, $rulesMsg);
        if ($validator->fails()) {
            $newMenuSettingCategoryData = $newCategoryData = array();
            if(Input::get('restaurant_id') && is_numeric(Input::get('restaurant_id')))
            {
                $newCategoryData = MenuCategory::where('restaurant_id', '=', Input::get('restaurant_id'))->orderBy('name', 'DESC')->get();
            }
            if(Input::get('menu_category_id') && is_numeric(Input::get('menu_category_id'))){
                $newMenuSettingCategoryData = MenuSettingCategory::where('menu_category_id', '=', Input::get('menu_category_id'))->orderBy('name', 'DESC')->get();
            }
            return redirect()->back()->withErrors($validator->errors())->withInput()->with('newMenuSettingCategoryData',$newMenuSettingCategoryData)->with('newCategoryData',$newCategoryData);
        } else {
            $menuItemObj = MenuSettingItem::find($id);
            $menuItemObj->name       = Input::get('name');
            $menuItemObj->restaurant_id      = Input::get('restaurant_id');
            $menuItemObj->language_id      = Input::get('language_id');
            $menuItemObj->menu_category_id      = Input::get('menu_category_id');
            $menuItemObj->menu_setting_category_id      = Input::get('menu_setting_category_id');
            $menuItemObj->status = Input::get('status');
            $menuItemObj->side_flag = Input::get('side_flag');
	    $menuItemObj->display_order = Input::get('display_order');	
            $menuItemObj->quantity_flag = Input::get('quantity_flag');
            $menuItemObj->price = Input::get('price');
            $menuItemObj->description = Input::get('description');
            $menuItemObj->save();
            return Redirect::back()->with('message','Item updated successfully');
        }
    }

    public function store(Request $request)
    {
        //print_r($request->all()); die;
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:100',
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'status' => 'required|in:0,1',
            'menu_category_id' => 'required|numeric|exists:menu_categories,id',
            'menu_setting_category_id' => 'required|numeric|exists:menu_setting_categories,id',
            'side_flag' => 'required|in:0,1',
            'quantity_flag' => 'required|in:0,1',
            'price' => 'required|numeric'
        ], [
            'name.*' => 'Please enter valid name upto 100 chars',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status',
            'menu_category_id.*' => 'Please select Category',
            'menu_setting_category_id.*' => 'Please select Menu setting category',
            'side_flag' => 'Please select sides type',
            'quantity_flag' => 'Please select quatity type',
            'price.*' => 'Please enter numeric price'
        ]);
        if($validator->passes())
        {
            $menuItemArr = ['restaurant_id'=>$request->input('restaurant_id'),
                'language_id'=>$request->input('language_id'),
                'name'=>$request->input('name'),
                'status'=>$request->input('status'),
                'menu_category_id' => $request->input('menu_category_id'),
                'menu_setting_category_id' => $request->input('menu_setting_category_id'),
                'side_flag' => $request->input('side_flag'),
		'display_order' => $request->input('display_order'),
                'quantity_flag' => $request->input('quantity_flag'),
                'price' => $request->input('price'),
                'description' => $request->input('description')
            ];
            //print_r($menuItemArr); die;
            $menuItemObj = MenuSettingItem::create($menuItemArr);
            $menuItemObj->description = $request->input('description');
            $menuItemObj->save();

            return Redirect::back()->with('message','Item added successfully');
        }
        $categoryData = $settingCategoryData = array();
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $categoryData = MenuCategory::where('restaurant_id', '=',trim($request->input('restaurant_id')))->get();
        }
        if($request->input('menu_category_id') && is_numeric($request->input('menu_category_id')))
        {
            $settingCategoryData = MenuSettingCategory::where('menu_category_id', '=',trim($request->input('menu_category_id')))->get();
        }
        return redirect()->back()->withErrors($validator->errors())->withInput()
            ->with('categoryData',$categoryData)
            ->with('menuSettingCategoryData',$settingCategoryData);
    }

    public function index(Request $request)
    {
        $searchArr = array('restaurant_id'=>0,'menu_category_id'=>0,'menu_setting_category_id'=>0);
        $categoryData = $settingCategoryData = array();
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();
        $menuSettingItems  = MenuSettingItem::orderBy('id','DESC');
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $categoryData = MenuCategory::where('restaurant_id', '=',trim($request->input('restaurant_id') ))->get();
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuSettingItems->where('restaurant_id',$searchArr['restaurant_id']);
        }
        if($request->input('menu_category_id') && is_numeric($request->input('menu_category_id')))
        {
            $settingCategoryData = MenuSettingCategory::where('menu_category_id', '=',trim($request->input('menu_category_id')))->get();
            $searchArr['menu_category_id'] = $request->input('menu_category_id');
            $menuSettingItems->where('menu_category_id',$searchArr['menu_category_id']);
        }
        if($request->input('menu_setting_category_id') && is_numeric($request->input('menu_setting_category_id')))
        {
            $searchArr['menu_setting_category_id'] = $request->input('menu_setting_category_id');
            $menuSettingItems->where('menu_setting_category_id',$searchArr['menu_setting_category_id']);
        }
        $menuSettingItems = $menuSettingItems->paginate(20);
        return view('menu_setting_item.index', compact('menuSettingItems'))->with('restaurantData',$restaurantData)->with('searchArr',$searchArr)->with('categoryData',$categoryData)->with('settingCategoryData',$settingCategoryData);
    }

    public function destroy($id)
    {
        $menuSettingItemObj = MenuSettingItem::find($id);
        if(!empty($menuSettingItemObj))
        {
            $menuSettingItemObj->delete();
            return Redirect::back()->with('message','Item deleted successfully');
        }
        else
            return Redirect::back()->with('err_msg','Invalid Category');
    }

    public function getSettingCategory(Request $request)
    {
        $cid = $request->input('cid');
        if(!empty($cid) && is_numeric($cid)) {
            $result['cat'] = MenuSettingCategory::where('category_id', '=', $cid)->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }
}
