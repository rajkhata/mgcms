<?php

namespace App\Http\Controllers;

use App\Models\MenuAttribute;
use App\Models\MenuAttributeValue;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Helpers\CommonFunctions;
use App\Models\Language;
use DB;

class MenuAttributeController extends Controller {

    protected  $fieldtypes=['Text','Textarea','Editor','Date','Yes/No','Multiple','Dropdown','Price','Image'];

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $restaurant_id = 0;
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
        if ($restaurantData && count($restaurantData) == 1) {
            $restaurant_id = $restaurantData[0]['id'];
        }

        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $restaurant_id = $request->input('restaurant_id');
            $menuAttributes = MenuAttribute::where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->paginate(20);
        } else {
            $menuAttributes = MenuAttribute::whereIn('restaurant_id', $locations)->orderBy('id', 'DESC')->paginate(20);
        }

        return view('menu_attribute.index', compact('menuAttributes'))->with('restaurantData', $restaurantData)->with('restaurant_id', $restaurant_id);
    }

    public function create() {

        $groupRestData = CommonFunctions::getRestaurantGroupAll(); //Requirement by Pranav to show all restaurants
        $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();

        $selected_rest_id = 0;
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        }
        $fieldtypes=$this->fieldtypes;

        return view('menu_attribute.create', compact('groupRestData', 'selected_rest_id', 'languageData','fieldtypes'));
    }

    public function store(Request $request) {

        $dataarray = [
            'restaurant_id' => $request->input('restaurant_id'),
            'language_id' => $request->input('language_id'),
            'attribute_label' => $request->input('attribute_label'),
            'attribute_prompt' => $request->input('attribute_prompt'),
            'attribute_code' => $request->input('attribute_code'),
            'form_field_type' => $request->input('form_field_type'),
            'status' => $request->input('status'),
        ];
        if ($request->has('show_as_dropdown') && $request->input('show_as_dropdown') == 'on') {
            $dataarray['show_as_dropdown'] = 1;
        } else {
            $dataarray['show_as_dropdown'] = 0;
        }
        if($request->has('is_configurable') && $request->input('is_configurable') == 'on' ) {
            $dataarray['is_configurable']  = 1;
        }else {
            $dataarray['is_configurable']  = 0;
        }
        if($request->has('use_as_filter_option') && $request->input('use_as_filter_option') == 'on' ) {
            $dataarray['use_as_filter_option']  = 1;
        }else {
            $dataarray['use_as_filter_option']  = 0;
        }
        if ($request->has('use_in_search') && $request->input('use_in_search') == 'on') {
            $dataarray['use_in_search'] = 1;
        } else {
            $dataarray['use_in_search'] = 0;
        }
        if ($request->has('use_for_prmo_rule_conditions') && $request->input('use_for_prmo_rule_conditions') == 'on') {
            $dataarray['use_for_prmo_rule_conditions'] = 1;
        } else {
            $dataarray['use_for_prmo_rule_conditions'] = 0;
        }

        MenuAttribute::create($dataarray);
        return Redirect::back()->with('message', 'Menu Attribute added successfully');


    }



    public function edit($id, $languageId = 1) {
        $menuAttribute = MenuAttribute::find($id);
        $groupRestData = CommonFunctions::getRestaurantGroupAll(); //Requirement by Pranav to show all restaurants
        $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();

        $restLocations = Restaurant::select('id', 'restaurant_name')
            ->where('parent_restaurant_id', $menuAttribute->restaurant_id)->orderBy('id', 'DESC')->get();
        $fieldtypes=$this->fieldtypes;

        return view('menu_attribute.edit', compact( 'menuAttribute', 'restLocations', 'groupRestData', 'languageData', 'languageId','fieldtypes'));
    }

    public function update(Request $request, $id) {

        $dataarray = [
            'restaurant_id' => $request->input('restaurant_id'),
            'language_id' => $request->input('language_id'),
            'attribute_label' => $request->input('attribute_label'),
            'attribute_prompt' => $request->input('attribute_prompt'),
            'attribute_code' => $request->input('attribute_code'),
            'form_field_type' => $request->input('form_field_type'),
            'status' => $request->input('status'),
        ];
        if ($request->has('show_as_dropdown') && $request->input('show_as_dropdown') == 'on') {
            $dataarray['show_as_dropdown'] = 1;
        } else {
            $dataarray['show_as_dropdown'] = 0;
        }
        if($request->has('is_configurable') && $request->input('is_configurable') == 'on' ) {
            $dataarray['is_configurable']  = 1;
        }else {
            $dataarray['is_configurable']  = 0;
        }
        if($request->has('use_as_filter_option') && $request->input('use_as_filter_option') == 'on' ) {
            $dataarray['use_as_filter_option']  = 1;
        }else {
            $dataarray['use_as_filter_option']  = 0;
        }
        if ($request->has('use_in_search') && $request->input('use_in_search') == 'on') {
            $dataarray['use_in_search'] = 1;
        } else {
            $dataarray['use_in_search'] = 0;
        }
        if ($request->has('use_for_prmo_rule_conditions') && $request->input('use_for_prmo_rule_conditions') == 'on') {
            $dataarray['use_for_prmo_rule_conditions'] = 1;
        } else {
            $dataarray['use_for_prmo_rule_conditions'] = 0;
        }

        MenuAttribute::where('id', $id)->update($dataarray);

         return Redirect::back()->with('message', 'Menu Attribute updated successfully');

    }


    public function destroy($id) {
        $attrobj= MenuAttribute::find($id);
        if (!empty($attrobj)) {
            $attrobj->delete();
            return Redirect::back()->with('message', 'Menu Atrribute deleted successfully');
        } else
            return Redirect::back()->with('err_msg', 'Invalid Menu Atrribute');
    }


    public function saveAttributeValue(Request $request, $id) {

        if (!isset($id) || !is_numeric($id)) {
            return redirect()->back()->with('err_msg', 'Invalid Request');
        }
        $menu_item_id = $id;
        $ids = $request->input('id');
        DB::beginTransaction();
        try {
            if ($request->has('attribute_value')) {
                $ids = $request->input('id');

                $attribute_values = $request->input('attribute_value');
                $attribute_image = $request->input('attribute_image');
                $pos_code = $request->input('pos_code');
                $status = $request->input('status');
                $sort_order = $request->input('sort_order');

                $all_items=$request->all();
                $itemval=$all_items;

                    if ($request->has('id')) {
                        $imageRelPath = config('constants.image.rel_path.menu');
                        $imagePath = config('constants.image.path.menu');

                        $menu_attributes='menu_attributes';
                        $imagePath=$imagePath.DIRECTORY_SEPARATOR.$menu_attributes;
                        $imageRelPath=$imageRelPath.$menu_attributes. DIRECTORY_SEPARATOR;

                        (! File::exists($imagePath) ?File::makeDirectory($imagePath):'');

                        foreach ($attribute_values as $key=> $attrval) {

                            if (isset($ids[$key]) && !empty($ids[$key])) {


                                MenuAttributeValue::where('attribute_id', $id)->whereNotIn('id', $ids)->delete();

                                $item_data = [
                                    'attribute_value' => $attrval,
                                    //'attribute_image' => $attribute_image[$key],
                                    'pos_code' => $pos_code[$key],
                                    'status' => $status[$key],
                                    'sort_order' => $sort_order[$key],

                                ];
                                if(@$request->file('attribute_image')[$key]){
                                    $imageArr =CommonFunctions::ImageAsItisUpload($request->file('attribute_image')[$key], time(), $imagePath, $imageRelPath, false);
                                    $item_data['attribute_image']=$imageArr['image'];
                                }
                                MenuAttributeValue::where('attribute_id', $id)->where('id',$ids[$key])->update($item_data);

                            } else {

                                $item_data = [
                                    'attribute_id' => $id,
                                    'attribute_value' => $attrval,
                                   // 'attribute_image' => $attribute_image[$key],
                                    'pos_code' => $pos_code[$key],
                                    'status' => $status[$key],
                                    'sort_order' => $sort_order[$key],


                                ];
                                if(@$request->file('attribute_image')[$key]){
                                    $imageArr =CommonFunctions::ImageAsItisUpload($request->file('attribute_image')[$key], time(), $imagePath, $imageRelPath, false);
                                    $item_data['attribute_image']=$imageArr['image'];
                                }

                                MenuAttributeValue::create($item_data);

                            }
                        }
                    }

                     DB::commit();
                    return \Response::json(['success' => true, 'data' => '', 'message' => 'Attribute value updated successfully.'], 200);

            }else{
                MenuAttributeValue::where('attribute_id', $id)->delete();
                DB::commit();
                return \Response::json(['success' => true, 'data' => '', 'message' => 'Attribute value updated successfully.'], 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
            } else {
                return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
            }
        }
    }



}
