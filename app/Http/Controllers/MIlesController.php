<?php

namespace App\Http\Controllers;
use App\Models\UserOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Helpers\CommonFunctions;
use Illuminate\Support\Facades\Auth;
use App\Models\Restaurant;
use Session;


class MilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $restaurant_id = 0;
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        $milesrecord = UserOrder::select('user_orders.*','restaurants.restaurant_name')->whereIn('user_orders.restaurant_id',$locations)->where('miles_away','>' ,1.5)
           ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')->orderBy('user_orders.id','DESC')->get();
      return view('milesrecord.index', compact('milesrecord'))->with('groupRestData', $groupRestData)->with('restaurant_id', $restaurant_id);    

    }
    public function create(Request $request) {
        }

    public function store(Request $request) {

    }



    public function edit($id) {

    }

    public function update(Request $request,$id) {
    }

    public function show($id) {
      
    }
    public function destroy($id) {
    }

}
