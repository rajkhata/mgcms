<?php

namespace App\Http\Controllers;


use App\Models\StaticPages;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\MenuMealType;
use App\Models\Restaurant;
use App\Models\MenuCategory;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\StaticBlock;
use App\Models\Language;
use App\Helpers\CommonFunctions;
class StaticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        /*exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan view:clear');*/
    }

    public function index(Request $request)
    {
        $restaurant_id = 0;
	$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantData = Restaurant::select('id','restaurant_name')->whereIn('id', $locations)->orderBy('id','DESC')->get();
	//$restaurantData = CommonFunctions::getRestaurantGroup();
	
	//print_r($restaurantData);die;
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $restaurant_id = $request->input('restaurant_id');
            $staticPages = StaticPages::leftJoin('restaurants', 'restaurants.id', '=', 'static_pages.restaurant_id')
                ->select('static_pages.*', 'restaurants.restaurant_name')->whereIn('static_pages.restaurant_id', $locations)
                ->where('static_pages.restaurant_id', $restaurant_id )
                ->orderBy('static_pages.restaurant_id')
                ->orderBy('static_pages.priority', 'ASC')->paginate(20);
        }
        else
        {
            $staticPages = StaticPages::leftJoin('restaurants', 'restaurants.id', '=', 'static_pages.restaurant_id')
                ->select('static_pages.*', 'restaurants.restaurant_name')->whereIn('static_pages.restaurant_id', $locations)
                ->orderBy('static_pages.restaurant_id')
                ->orderBy('static_pages.priority', 'ASC')->paginate(20);
        }
        return view('static.index', compact('staticPages'))->with('restaurantData',$restaurantData)->with('restaurant_id',$restaurant_id);
    }

    public function edit($id)
    {
        if(isset($id) && is_numeric($id)) {
	    $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));	
	    $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();	
            $restaurant = Restaurant::select('id','restaurant_name')->whereIn('id', $locations)->orderBy('id','DESC')->get();
            $staticPage = StaticPages::find($id); 
            $staticBlock = StaticBlock::select('id', 'block_title')->where('restaurant_id', $staticPage->restaurant_id)->get();

            return view('static.edit')->with('restaurant', $restaurant)->with('staticPage', $staticPage)->with('staticBlock', $staticBlock)->with('languageData', $languageData);
        }
        return Redirect::back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
	$languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();	
        //$groupRestData = Restaurant::group()->whereIn('id', $locations);
	$restaurant = Restaurant::select('id','restaurant_name')->whereIn('id', $locations)->orderBy('id','DESC')->get();
        return view('static.create', compact('restaurant','languageData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $rules = [
            'restaurant_id'    => 'required|exists:restaurants,id',
	    'language_id'    => 'required|exists:languages,id',	
            'page_heading'     => 'required|min:4|max:255',
            'page_sub_heading' => 'required|max:255',
            'page_content'     => 'required' 
        ];
        $err_msg =[
             
        ];

         
            // STATIC BLOCK IDS
            $staticBlocksArr = $request->post('static_blocks');
            $staticBlocksIds = !empty($staticBlocksArr) ? implode(',', $staticBlocksArr) : null;

            $staticPageObj = StaticPages::find($id);
            $staticPageObj->restaurant_id = $request->input('restaurant_id');
            $staticPageObj->static_block_ids = $staticBlocksIds;
            $staticPageObj->page_type = strtolower(str_replace(' ', '_', $request->input('page_heading')));
            $staticPageObj->page_heading = $request->input('page_heading');
            $staticPageObj->page_sub_heading = $request->input('page_sub_heading');
            $staticPageObj->page_content = $request->input('page_content');
            $staticPageObj->priority = $request->input('priority');
	    $staticPageObj->language_id = $request->input('language_id');	
             

            if($staticPageObj->save()){
           // return Redirect::back()->with('message', 'Page updated successfully');
		return Redirect::to('/static/')->with('message', 'Page updated successfully');	
        
         }else return redirect()->back()->withErrors($validator->errors())->withInput()->with('Something went wrong.');
    }
    public function store(Request $request)
    {
        $rules = [
            'restaurant_id'    => 'required|exists:restaurants,id',
            'page_heading'     => 'required|min:4|max:255',
            'page_sub_heading' => 'required|max:255',
            'page_content'     => 'required'
            
        ];
         
	$err_msg = [];
         

        $validator = Validator::make(Input::all(), $rules, $err_msg);
        if ($validator->passes())
        {
            // STATIC BLOCK IDS
            $staticBlocksArr = $request->post('static_blocks');
            $staticBlocksIds = !empty($staticBlocksArr) ? implode(',', $staticBlocksArr) : null;

            $pageData = [
                'restaurant_id' => $request->input('restaurant_id'),
                'static_block_ids' => $staticBlocksIds,
                'page_type' => strtolower(str_replace(' ', '_', $request->input('page_heading'))),
                'page_heading' => $request->input('page_heading'),
                'page_sub_heading' => $request->input('page_sub_heading'),
                'page_content' => $request->input('page_content'),
		'language_id' => $request->input('language_id') 
            ];
            

            $pageExist = StaticPages::where(['restaurant_id' => $request->input('restaurant_id'),'page_type' => $pageData['page_type']])->get()->count();
            if($pageExist)
            {
                $validator->errors()->add('page_err','Page '.$pageData['page_heading'].' already Exists');
            }
            else
            {
                StaticPages::create($pageData);
                ///return redirect()->back()->with('message','Page created successfully');
		return Redirect::to('/static/')->with('message', 'Page created successfully');
            }
        }
        return redirect()->back()->withErrors($validator->errors())->withInput()->with('Something went wrong.');
    }

    public function createBlock(){
        $groupRestData = Restaurant::group();
       // print_r($groupRestData);
        return view('static.block', compact('groupRestData'));
        //echo "hellooo"; die;
    }

    public function saveBlock(Request $request){
       //$arr = $request->all();
       //echo "<pre>";
    $block_group = $request->input('block_group');

    if(isset($block_group)){
        $i = 0;
        $newArray = [];
        foreach($block_group as $value){
            $group_data = [];
            $group_data['id'] = $i;
            $group_data['title'] = $value['title'];
            $newLabels = array_values($value);
            foreach($newLabels as $data){
                if(is_array($data)){
                    $arrVal[] = array($data['label']=>$data['value']);
                }
            }
            $group_data['label'] = $arrVal;
            $newArray[] = $group_data;
        $i++;
        }
        $newArray = (object) $newArray;
        //echo json_encode($newArray);
        $blockData = [
            'restaurant_id' => $request->input('restaurant_id'),
            'block_title' => $request->input('block_title'),
            'block_data' => json_encode($newArray),
            'language_id' => 1
        ];
        StaticBlock::create($blockData);
        return redirect()->back()->with('message','Block created successfully');
    }
    else{
        return redirect()->back()->withErrors('Fields should not be empty.');
    }

    }

    public function saveEditBlock(Request $request){
        //$arr = $request->all();
        //echo "<pre>";
        $block_group = $request->input('block_group');
        if(isset($block_group)){
            $blockID = $request->input('block_id');
            $StaticBlock = StaticBlock::find($blockID);
            //print_r($StaticBlock); die;
            $i = 0;
            $newArray = [];
            foreach($block_group as $value){
                $group_data = [];
                $group_data['id'] = $i;
                $group_data['title'] = $value['title'];
                $newLabels = array_values($value);
                foreach($newLabels as $data){
                    if(is_array($data)){
                        $arrVal[] = array($data['label']=>$data['value']);
                    }
                }
                $group_data['label'] = $arrVal;
                $newArray[] = $group_data;
                $i++;
            }
            $newArray = (object) $newArray;
            //echo json_encode($newArray);

            $StaticBlock->restaurant_id = $request->input('restaurant_id');
            $StaticBlock->block_title = $request->input('block_title');
            $StaticBlock->block_data = json_encode($newArray);
            $StaticBlock->language_id = 1;
            $StaticBlock->save();
            //StaticBlock::create($blockData);
            return redirect()->back()->with('message','Block updated successfully');
            //print_r($blockData);
        }
        else{
            return redirect()->back()->withErrors('Fields should not be empty.');
        }

    }


    public function edit_block($id)
    {

        if(isset($id) && is_numeric($id)) {
            $groupRestData = Restaurant::group();
            $staticBlock = StaticBlock::where('id', $id)->first();
            //$staticBlock = StaticBlock::find($id)->first();
            //echo "<pre>";
            //echo $staticBlock->id;
            //print_r($staticBlock); die;
            return view('static.edit_block')->with('groupRestData', $groupRestData)->with('staticBlock', $staticBlock)->with('id', $id);
        }
        return Redirect::back();
    }

    public function getblock(Request $request){

        $restaurant_id = 0;
        $groupRestData = Restaurant::group();
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $restaurant_id = $request->input('restaurant_id');
            $staticBlocks = StaticBlock::leftJoin('restaurants', 'restaurants.id', '=', 'static_blocks.restaurant_id')
                ->select('static_blocks.*', 'restaurants.restaurant_name')
                ->where('static_blocks.restaurant_id', $restaurant_id )
                ->orderBy('static_blocks.restaurant_id')
                ->orderBy('static_blocks.id', 'ASC')->paginate(20);
        }
        else
        {
            $staticBlocks = StaticBlock::leftJoin('restaurants', 'restaurants.id', '=', 'static_blocks.restaurant_id')
                ->select('static_blocks.*', 'restaurants.restaurant_name')
                ->orderBy('static_blocks.restaurant_id')
                ->orderBy('static_blocks.id', 'ASC')->paginate(20);
        }
        return view('static.blocklist', compact('staticBlocks'))->with('groupRestData',$groupRestData)->with('restaurant_id',$restaurant_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return StaticPages::where('restaurant_id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(isset($id) && is_numeric($id)) {
            $staticPageObj = StaticPages::find($id);
            if (!empty($staticPageObj)) {
                $staticPageObj->delete();
                return Redirect::back()->with('message', 'Page deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg','Invalid Item');
    }

    public function deleteBlock($id)
    {
        die('ffffffdddddd');
        if(isset($id) && is_numeric($id)) {
            $staticBlockObj = StaticBlock::find($id);
            if (!empty($staticBlockObj)) {
                $staticBlockObj->delete();
                return Redirect::back()->with('message', 'Block deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg','Invalid Item');
    }

}
