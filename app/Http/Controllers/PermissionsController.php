<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class PermissionsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct() {
        $this->middleware('auth');
        $this->middleware('permission:permission-list');
        $this->middleware('permission:permission-create', ['only' => ['create','store']]);
        $this->middleware('permission:permission-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:permission-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
        $permission = Permission::orderBy('group_name', 'DESC')->get();       
        return view('permissions.index', compact('permission'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {        
        $permissionAll = Permission::orderBy('group_name', 'ASC')->get();//->toArray();
        $groups = Permission::select('group_name','guard_name')->groupBy('group_name')->orderBy('group_name', 'ASC')->get()->toArray();
       // $guards = Permission::select('guard_name')->groupBy('guard_name')->orderBy('guard_name', 'ASC')->get()->toArray();
        $onlyGroups = [];
        $onlyGuards = [];
        if($groups){           
            $onlyGroups = array_unique(array_column($groups, 'group_name'));
            $onlyGuards = array_unique(array_column($groups, 'guard_name'));  
            $onlyGroups = array_combine($onlyGroups, $onlyGroups);            
            $onlyGuards = array_combine($onlyGuards, $onlyGuards);
            
            $onlyGuards = array_map('ucwords', $onlyGuards);
            $onlyGroups = array_map('ucwords', $onlyGroups);
        }
         
        
        if($permissionAll){
            $permissionArray = $permissionAll->toArray();
            $permission = array();
            foreach ( $permissionArray as $value ) {
                $permission[$value['group_name']][] = $value;
            }

        }
        return view('permissions.create', compact('permission','onlyGroups','onlyGuards'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
            'guard_name' => 'required|string|min:3|max:100',
            'group_name' => 'required|string|min:3|max:100',
        ]);        
         
        $permission = Permission::create(['name' => strtolower($request->input('name')),'guard_name'=>$request->input('guard_name'),'group_name'=>$request->input('group_name')]);        
         ###################
        $roles = Role::where('name','=','super admin')->get();
        if($roles){
            $role = $roles->toArray();            
            $permissionAll = Permission::orderBy('group_name', 'ASC')->get();
         
            $permission = ['name'=>'super admin'];
            $superAdminId = false;
         
         
            foreach($permissionAll as $key =>$value){
                $permission['permission'][] = $value['id'];             
            }
            
            if(isset($role[0]['id'])){ 
                $role = Role::find($role[0]['id']);
                $role->name = $permission['name'];
                $role->save();


                $role->syncPermissions($permission['permission']);
            }
            
        }     
                
        ##################
       

        return redirect()->route('permissions.index')
            ->with('success', 'Permission created successfully');
    }

    public function show($id) {
        $permissions = Permission::find($id)->toArray();
        
        return view('permissions.show', compact('permissions'));
    }

    public function edit($id) {
        $permissions = Permission::find($id)->toArray();
       
        $groups = Permission::select('group_name','guard_name')->groupBy('group_name')->orderBy('group_name', 'ASC')->get()->toArray();
        $onlyGroups = [];
        $onlyGuards = [];
        if($groups){           
            $onlyGroups = array_unique(array_column($groups, 'group_name'));
            $onlyGuards = array_unique(array_column($groups, 'guard_name'));  
            $onlyGroups = array_combine($onlyGroups, $onlyGroups);            
            $onlyGuards = array_combine($onlyGuards, $onlyGuards);
        }
        
        return view('permissions.edit', compact('permissions','onlyGroups','onlyGuards'));
    }

    public function update(Request $request, $id) {   
        
        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
            'guard_name' => 'required|string|min:3|max:100',
            'group_name' => 'required|string|min:3|max:100',
        ]);


        $permission = Permission::find($id);
        $permission->name = $request->input('name');
        $permission->guard_name = $request->input('guard_name');
        $permission->group_name = $request->input('group_name');
        
        $permission->save();


        return redirect()->route('permissions.index')
            ->with('success', 'Permission updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table("permissions")->where('id', $id)->delete();
        DB::table("role_has_permissions")->where('permission_id', $id)->delete();
        return redirect()->route('permissions.index')
            ->with('success', 'Permission deleted successfully');
    }
   
}
