<?php

namespace App\Http\Controllers;


use App\Models\PaymentGateway;

use App\Models\BYW\Restaurants as Restaurant;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;

use App\Helpers\CommonFunctions;




use Illuminate\Support\Facades\Auth;
/*
 * Test Accounts:

User: amalakar@bravvura.in
Pass: Bravvura@123

Published Key: pk_test_Fi2zMw8MczsNRTGFIW2PLHDL
Secret Key: sk_test_OxrK6FHy2yuax9TB60fspW3S

pk_test_3ksHOaiyAisVKWE7xOa1Z99D
sk_test_OxrK6FHy2yuax9TB60fspW3S
 *
 */


class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $inputData = array('restaurant_id'=> 0 ,'rest_code'=>'');
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
//print_r($locations);die;
        $restaurantData = Restaurant::select('*')->whereIn('id',$locations)->orderBy('id','DESC')->get();
        $paymentgateway = PaymentGateway::select('*')->whereIn('restaurant_id',$locations)->orderBy('id','DESC')->get();
        //print_r($paymentgateway);die;

        return view('paymentgateway.index', compact('paymentgateway'));

    }
    public function create(Request $request) {
        $inputData = array('restaurant_id'=> 0 ,'rest_code'=>'');
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name')); //print_r($locations);die;
        //$restaurantData = Restaurant::select('*')->whereIn('id',$locations)->orderBy('id','DESC')->get();
        $paymentgateway = PaymentGateway::select('*')->whereIn('restaurant_id',$locations)->orderBy('id','DESC')->get();
        return view('paymentgateway.create', compact('paymentgateway'));
    }

    public function store(Request $request) {

        $request = $request->all();
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        //$paymentgateway = PaymentGateway::select('*')->whereIn('restaurant_id',$locations)->orderBy('id','DESC')->get();
        foreach($request['configk'] as $i=>$key){
            $request['config'][$i][$key]=$request['configv'][$i];
        }
        $request['restaurant_id'] = $locations[0];
        $request['config'] = json_encode($request['config']);
        unset($request['configk']);unset($request['configv']);unset($request['_token']);
        PaymentGateway::create($request);
        return redirect()->back()->with('message','Payment Gateway Info created successfully');

    }



    public function edit($id) {

        $paymentgateway = PaymentGateway::select('*')->where('id',$id)->get();

        return view('paymentgateway.edit', compact('paymentgateway'));
    }

    public function update(Request $request,$id) {
        $request = $request->all();
        $id = $request['id'];
        foreach($request['configk'] as $i=>$key){
            $request['config'][$i][$key]=$request['configv'][$i];
        }
        $request['config'] = json_encode($request['config']);
        unset($request['configk']);unset($request['configv']);unset($request['_token']);unset($request['id']);unset($request['_method']);
        //if($request['status']==1)PaymentGateway::where('restaurant_id', $request['restaurant_id'])->update(['status'=>0]);
        //print_r($request);die;
        PaymentGateway::where('id', $id)->update($request);
        return redirect()->back()->with('message','Payment Gateway Info updated successfully');
    }

    public function show($id) {
        $inputData = array('restaurant_id'=> 0 ,'rest_code'=>'');
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantData = Restaurant::select('*')->whereIn('id',$locations)->orderBy('id','DESC')->get();
        $paymentgateway = PaymentGateway::select('*')->whereIn('restaurant_id',$locations)->orderBy('id','DESC')->get();
        return view('paymentgateway.create', compact('paymentgateway'));
    }


    public function destroy($id) {
        if(isset($id) && is_numeric($id)) {
            $Obj = PaymentGateway::find($id);
            if (!empty($Obj)) {
                $Obj->delete();
                return Redirect::back()->with('message', 'Deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg','Invalid Id');
    }

}
