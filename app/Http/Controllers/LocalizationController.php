<?php

namespace App\Http\Controllers;


use App\Models\Localization;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LocalizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * User listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $localization =Localization::select('localization.*', 'languages.code', 'languages.language_name')
                        ->leftJoin('languages', 'languages.id', '=', 'localization.language_id')
                        ->orderBy('localization.id', 'DESC')->paginate(20);
        //dd($localization);

        return view('localization.index', compact('localization'));
    }

    /**
     * Create user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        $languages = Language::select('id','language_name')->get();
        //dd($languages);
        return view('localization.create', compact('languages'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'key'         => 'required|max:255',
            'value'         => 'required|max:255'
        ]);
        if (!$validator->fails()) {
            $data = [
                'language_id' => $request->input('lang_id'),
                'key'         => $request->input('key'),
                'value'       => $request->input('value'),
                'platform'    => $request->input('platform'),
                'updated_at'        => now(),
                'status'      => $request->input('status')
            ];

            Localization::create($data);
            return redirect('/localization')->with('message', 'Localization added successfully');
        } else {

            return Redirect::back()->withErrors($validator->errors())->withInput();
        }

    }
    
    /**
     * User edit
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $localize  = Localization::find($id);
        $languages = Language::select('id','language_name')->get();

        return view('localization.edit', compact('localize', 'languages'));
    }

    /**
     * User edit update
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if(isset($id) && is_numeric($id)) {
            $localize = Localization::find($id);

            $validator = Validator::make($request->all(), [
                'key'         => 'required|max:255',
                'value'         => 'required|max:255'
            ]);

            if(!$validator->fails()) {
                $localize->language_id = $request->input('lang_id');
                $localize->key = $request->input('key');
                $localize->value = $request->input('value');
                $localize->platform = $request->input('platform');
                $localize->updated_at = now();
                $localize->status = $request->input('status');
                $localize->save();
                return redirect('/localization')->with('message', 'Localization added successfully');
            } else {

                return Redirect::back()->withErsudorors($validator->errors())->withInput();
            }
        }

        return Redirect::back()->with('message', 'Invalid Id');
    }
}
