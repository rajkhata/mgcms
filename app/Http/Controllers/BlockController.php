<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\StaticBlock;
use App\Models\StaticPages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\CommonFunctions;
use App\Models\StaticBlockResponse;
use Carbon\Carbon;
use App\Models\Reservation;
use DB;
use App\Models\Language;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\NotificationController;
use App\Models\EnquirySendMailLog;
use Illuminate\Support\Facades\URL;


class BlockController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('permission:Contact', ['only' => ['getRequestList']]);
    }

    /**
     * Funtion to convert string to lower case and replace space with underscore
     *
     * @param $key
     * @return mixed
     */
    private function modifyString($key) {
        if (!is_array($key)) {
            $key = (string) $key;
            return str_replace(' ', '_', strtolower($key));
        } else
            return $key;
    }

    /**
     * Funtion to convert array values to lower case and replace space with underscore
     *
     * @param $key
     * @return mixed
     */
    private function modifyArray(&$key) {
        $key = str_replace(' ', '_', strtolower($key));
    }

    public function index(Request $request) {
        $restaurant_id = 0;
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        $staticBlocks = StaticBlock::leftJoin('restaurants', 'restaurants.id', '=', 'static_blocks.restaurant_id')->whereIn('static_blocks.restaurant_id', $locations)
                        ->select('static_blocks.*', 'restaurants.restaurant_name')
                        ->orderBy('static_blocks.restaurant_id')
                        ->orderBy('static_blocks.id', 'ASC')->paginate(20);
        return view('block.index', compact('staticBlocks'))->with('groupRestData', $groupRestData)->with('restaurant_id', $restaurant_id);
    }

    public function edit($id) {
        if (isset($id) && is_numeric($id)) {
            $searchArr = array('restaurant_id' => 0, 'name' => NULL);
            $groupRestData = CommonFunctions::getRestaurantGroup();
            if (count($groupRestData) == 1) {
                $first_ind_array = current($groupRestData);
                if (count($first_ind_array['branches']) == 1) {
                    $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
                }
            }
            $staticBlock = StaticBlock::where('id', $id)->first();
            $fields_types = config('constants.static_block_fields_type');
            $block_type = config('constants.static_block_type');
            $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();
            return view('block.edit', compact('fields_types', 'block_type', 'languageData'))->with('groupRestData', $groupRestData)->with('staticBlock', $staticBlock)->with('id', $id);
        }
        return Redirect::back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $fields_types = config('constants.static_block_fields_type');
        $block_type = config('constants.static_block_type');
        return view('block.create', compact('groupRestData', 'fields_types', 'block_type', 'languageData'));
    }

    public function uploadImage($imgObj, $language, $prefixName, $rest_id) {
        if ($imgObj) {
            $image = $imgObj;
            $restLocations = Restaurant::select('id', 'restaurant_name')->where('id', $rest_id)->orderBy('id', 'DESC')->get()->toArray();
            //print_r($restLocations);die;//$restaurantName,
            if (strtolower($imgObj->getClientOriginalExtension()) == "jpeg" || strtolower($imgObj->getClientOriginalExtension()) == "jpg" || ($imgObj->getClientOriginalExtension()) == "png") {
                $restName = str_replace(" ", "", $restLocations[0]['restaurant_name']) . "-" . $restLocations[0]['id'];
                $imageRelPath = config('constants.image.rel_path.static_block') . $restName . "/";
                $imagePath1 = config('constants.image.path.static_block');
                if (!File::exists($imagePath1)) {
                    File::makeDirectory($imagePath1, 0777, true, true);
                }
                $imagePath = $imagePath1 . "/" . $restName;
                if (!File::exists($imagePath)) {
                    File::makeDirectory($imagePath, 0777, true, true);
                }
                // $realPath = $request->file('cat_attachment')->getRealPath();
                $uniqId = uniqid(mt_rand());

                $filname = $imgObj->getClientOriginalName();
                $uniqId = uniqid(mt_rand());
                $filnameNew = str_replace(" ", "", $restLocations[0]['restaurant_name'] . "-" . $restLocations[0]['id']) . '_' . pathinfo($filname, PATHINFO_FILENAME)
                        . '_' . $uniqId . '.' . $imgObj->getClientOriginalExtension();

                $filnameNew = str_replace(" ", "", $filnameNew);
                $uploaded_file_path = $imageRelPath . $filnameNew;
                if ($imgObj->move($imagePath, $filnameNew))
                    return $uploaded_file_path;
                else
                    return redirect()->back()->with('error', 'Image must be jpg or png format!');
            }else {
                return redirect()->back()->with('error', 'Image must be jpg or png format!');
            }
        } else {
            return redirect()->back()->with('error', 'Image missing!!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $request->validate([
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'block_title' => 'required|max:500',
        ]);
        //$staticBlock                = StaticBlock::find($id);
        //$restaurant=CommonFunctions::getRestaurant($request->post('restaurant_id'));  
        $language_id = $request->post('language_id') ?? 1;
        $array['restaurant_id'] = $request->post('restaurant_id');
        $array['normal_field_type'] = $request->post('normal_field_type');
        $array['normal_field_key'] = $request->post('normal_field_key');
        $array['normal_field_value'] = $request->post('normal_field_value');
        $array['normal_field_label'] = $request->post('normal_field_label');
        $array['file_normal_field_value'] = $request->file('file_normal_field_value');
        $array['file_group_field'] = $request->file('file_group_field');
        $array['file_group_field_hidden'] = $request->post('file_group_field_hidden');
        $array['file_normal_field_hidden'] = $request->post('file_normal_field_hidden');
        $array['normal_field_placeholder'] = $request->post('normal_field_placeholder');
        $array['block_title'] = $request->post('block_title');
        $array['group_title'] = $request->post('group_title');
        $array['group_field'] = $request->post('group_field');
        $block['normal'] = [];
        $keyArray = $block['group'] = [];
        echo"<pre>";
        //print_r($request->all());  die;
        if (isset($array['normal_field_type'])) {
            foreach ($array['normal_field_type'] as $i => $val) {
                if (isset($array['normal_field_type'][$i]) && isset($array['normal_field_type'][$i])) {
                    $blockKey = isset($array['normal_field_key'][$i]) ? $array['normal_field_key'][$i] : '0';
                    $blockVal = isset($array['normal_field_value'][$i]) ? $array['normal_field_value'][$i] : '';
                    $blockType = isset($array['normal_field_type'][$i]) ? $array['normal_field_type'][$i] : '';
                    $blockLabel = isset($array['normal_field_label'][$i]) ? $array['normal_field_label'][$i] : '';
                    $blockPlaceholder = isset($array['normal_field_placeholder'][$i]) ? $array['normal_field_placeholder'][$i] : '';
                    if (isset($array['file_normal_field_value'][$i])) {
                        $imgObj = isset($array['file_normal_field_value'][$i]) ? $array['file_normal_field_value'][$i] : null;
                        $blockVal = $this->uploadImage($imgObj, $language_id, 'normal-', $array['restaurant_id']);
                    } elseif (isset($array['file_normal_field_hidden'][$i])) {
                        $blockVal = $array['file_normal_field_hidden'][$i];
                    }
                    $block1['normal'][$blockKey] = ['key' => $blockKey, 'type' => $blockType, 'value' => $blockVal, 'label' => $blockLabel, 'placeholder' => $blockPlaceholder];
                }
            }
        }
        if (isset($array['group_title'])) {

            foreach ($array['group_field'] as $i => $val) {
                $groupName = $array['group_title'][$i];
                foreach ($val as $j => $val1) {
                    if ($j == "type") {
                        foreach ($val1 as $l => $val3) {
                            if ($val3 == "file") {
                                if (isset($array['file_group_field'][$i]['value'][$l]) && !isset($block['group'][$groupName][$l]['value'])) {

                                    $imgObj1 = isset($array['file_group_field'][$i]['value'][$l]) ? $array['file_group_field'][$i]['value'][$l] : null;
                                    $block['group'][$groupName][$l]['value'] = $this->uploadImage($imgObj1, $language_id, 'normal-', $array['restaurant_id']);
                                    $block['group'][$groupName][$l][$j] = $val3;
                                } elseif (isset($array['file_group_field_hidden'][$i]['value'][$l])) {
                                    $block['group'][$groupName][$l]['value'] = $array['file_group_field_hidden'][$i]['value'][$l];
                                    $block['group'][$groupName][$l][$j] = $val3;
                                } else {
                                    $block['group'][$groupName][$l][$j] = $val3;
                                }
                            } else {
                                $block['group'][$groupName][$l][$j] = $val3;
                            }
                        }
                    } else {
                        foreach ($val1 as $k => $val2) {
                            $block['group'][$groupName][$k][$j] = $val2;
                        }
                    }
                }
            }
            foreach ($block['group'] as $groupName => $a) {
                foreach ($a as $indx => $a1) {
                    $j = $a1['key'];
                    $groupIndex = $this->groupIndex($keyArray, $groupName, $j);
                    $keyArray[$groupName][] = $j;
                    $block1['group'][$groupName][$groupIndex][$j] = $a1;
                }
            }
            //}
        } //print_r($block1); die;
        $array1['restaurant_id'] = $array['restaurant_id'];
        $array1['block_title'] = $array['block_title'];
        $array1['block_data'] = json_encode($block1);
        //print_r(json_encode($array1)); die;

        $block_type = $request->post('block_type');

        $staticBlock = StaticBlock::find($id);
        $staticBlock->restaurant_id = $request->post('restaurant_id');
        $staticBlock->block_title = $array1['block_title'];
        $staticBlock->block_data = $array1['block_data'];
        $staticBlock->block_type = $block_type;
        $staticBlock->language_id = $language_id;
        $staticBlock->save();
        return Redirect::to('/block/')->with('message', 'Block updated successfully');
        //return redirect()->back()->with('message', 'Block updated successfully');
    }

    private function groupIndex($keyArray, $groupName, $j) {
        $index = 0;
        if (isset($keyArray[$groupName])) {
            foreach ($keyArray[$groupName] as $k => $v) {
                if ($v == $j)
                    $index = $index + 1;
            }
        }
        return $index;
    }

    public function store(Request $request) {
        $request->validate([
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'block_title' => 'required|max:500',
        ]);
        $restaurant = CommonFunctions::getRestaurant($request->post('restaurant_id'));

        $array['restaurant_id'] = $request->post('restaurant_id');
        $array['normal_field_type'] = $request->post('normal_field_type');
        $array['normal_field_key'] = $request->post('normal_field_key');
        $array['normal_field_value'] = $request->post('normal_field_value');
        $array['normal_field_label'] = $request->post('normal_field_label');
        $array['normal_field_placeholder'] = $request->post('normal_field_placeholder');
        $array['block_title'] = $request->post('block_title');
        $array['group_title'] = $request->post('group_title');
        $array['group_field'] = $request->post('group_field');
        $array['file_normal_field_value'] = $request->file('file_normal_field_value');
        $array['file_group_field'] = $request->file('file_group_field');
        $array['file_group_field_hidden'] = $request->post('file_group_field_hidden');
        //$array['language_id']=$request->post('language_id');
        //$array['sort_order']=$request->post('sort_order');
        //$array['status']=$request->post('status');
        //$block['data']['title']=$request->post('title');
        $block['normal'] = [];
        $keyArray = $block['group'] = [];

        if (isset($array['normal_field_type'])) {
            foreach ($array['normal_field_type'] as $i => $val) {
                if (isset($array['normal_field_key'][$i]) && isset($array['normal_field_value'][$i])) {
                    $blockKey = isset($array['normal_field_key'][$i]) ? $array['normal_field_key'][$i] : '0';
                    $blockVal = isset($array['normal_field_value'][$i]) ? $array['normal_field_value'][$i] : '';
                    $blockType = isset($array['normal_field_type'][$i]) ? $array['normal_field_type'][$i] : '';
                    $blockLabel = isset($array['normal_field_label'][$i]) ? $array['normal_field_label'][$i] : '';
                    $blockPlaceholder = isset($array['normal_field_placeholder'][$i]) ? $array['normal_field_placeholder'][$i] : '';
                    if (isset($array['file_normal_field_value'][$i])) {
                        $imgObj = isset($array['file_normal_field_value'][$i]) ? $array['file_normal_field_value'][$i] : null;
                        $blockVal = $this->uploadImage($imgObj, 'restaurantname', 'en', 'normal-');
                    }
                    $block1['normal'][$blockKey] = ['key' => $blockKey, 'type' => $blockType, 'value' => $blockVal, 'label' => $blockLabel, 'placeholder' => $blockPlaceholder];
                }
            }
        }
        
        if (isset($array['group_title'])) {

            foreach ($array['group_field'] as $i => $val) {
                $groupName = $array['group_title'][$i];
                foreach ($val as $j => $val1) {
                    if ($j == "type") {
                        foreach ($val1 as $l => $val3) {
                            if ($val3 == "file") {
                                if (isset($array['file_group_field'][$i]['value'][$l]) && !isset($block['group'][$groupName][$l]['value'])) {

                                    $imgObj1 = isset($array['file_group_field'][$i]['value'][$l]) ? $array['file_group_field'][$i]['value'][$l] : null;
                                    $block['group'][$groupName][$l]['value'] = $this->uploadImage($imgObj1, 'restaurantname', 'en', 'group-');
                                    $block['group'][$groupName][$l][$j] = $val3;
                                } elseif (isset($array['file_group_field_hidden'][$i]['value'][$l])) {
                                    $block['group'][$groupName][$l]['value'] = $array['file_group_field_hidden'][$i]['value'][$l];
                                    $block['group'][$groupName][$l][$j] = $val3;
                                } else {
                                    $block['group'][$groupName][$l][$j] = $val3;
                                }
                            } else {
                                $block['group'][$groupName][$l][$j] = $val3;
                            }
                        }
                    } else {
                        foreach ($val1 as $k => $val2) {
                            $block['group'][$groupName][$k][$j] = $val2;
                        }
                    }
                }
            }
            foreach ($block['group'] as $groupName => $a) {
                foreach ($a as $indx => $a1) {
                    $j = $a1['key'];
                    $groupIndex = $this->groupIndex($keyArray, $groupName, $j);
                    $keyArray[$groupName][] = $j;
                    $block1['group'][$groupName][$groupIndex][$j] = $a1;
                }
            }
            //}
        }
        $array1['restaurant_id'] = $array['restaurant_id'];
        $array1['block_title'] = $array['block_title'];
        $array1['block_data'] = json_encode($block1);
        //print_r(json_encode($array1)); die;
        $language_id = $request->post('language_id') ?? 1;
        $block_type = $request->post('block_type');
        $data = [
            'restaurant_id' => $request->post('restaurant_id'),
            'block_title' => $array1['block_title'],
            'block_type' => $block_type,
            'block_data' => $array1['block_data'],
            'language_id' => $language_id,
        ];

        StaticBlock::create($data);
        return Redirect::to('/block/')->with('message', 'Block created successfully');
        //return redirect()->back()->with('message', 'Block created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return StaticPages::where('restaurant_id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        dd('delete');
        if (isset($id) && is_numeric($id)) {
            $staticPageObj = StaticPages::find($id);
            if (!empty($staticPageObj)) {
                $staticPageObj->delete();
                return Redirect::back()->with('message', 'Page deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg', 'Invalid Item');
    }

    public function getStaticBlocks(Request $request) {
        $restId = $request->input('rid');
        $langId = $request->input('lid') ?? 1;
        $staticBlocks = StaticBlock::select('id', 'block_title')->where(['restaurant_id' => $restId, 'language_id' => $langId])->get()->toArray();

        if ($staticBlocks) {
            return response()->json(array('dataObj' => $staticBlocks), 200);
        }

        return response()->json(array('err' => 'Invalid Data'), 400);
    }

    public function getRequestList(Request $request, $enquiry_type, $enctype = null) {
        $reservations = array();
        $user = Auth::user();
         
        $unreadEnquiry =[];        
        $prestdetails = Restaurant::Where('id',$user->restaurant_id)->select('parent_restaurant_id')->get()->toArray();
        $prestId = $prestdetails[0]['parent_restaurant_id'];
        
        if($request->has('restaurant_id') && $request->get('restaurant_id')!="All"){
            $restArray = array($request->get('restaurant_id'));
        }else{
            $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        }
        $page = 100000000;
        $keyword = $request->get('keyword') ? trim($request->get('keyword')) : '';
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        ##change type
        
        $name = '';
        if ($enquiry_type == 'career') {
            $enquiry_type = 'career';
            $name = 'Career';
        }
        if ($enquiry_type == 'events') {
            $enquiry_type = 'private_event';
            $name = 'Events';
        }
        if ($enquiry_type == 'reservations') {
            $enquiry_type = 'reservation';
            $name = 'Reservations';
        }
        if ($enquiry_type == 'catering') {
            $enquiry_type = 'catering';
            $name = 'Catering';
        }
        
        if ($enquiry_type == 'contact') {
            $enquiry_type = 'contact';
            $name = 'Partner Bar Enquiry';
        }
    if ($enquiry_type == 'subscriptions') {
            $enquiry_type = 'subscriptions';
            $name = 'Newsletter Subscriptions';
        }

	   if ($enquiry_type == 'support') {
            $enquiry_type = 'support';
            $name = 'Support';
        }
        if ($enquiry_type == 'deliverynotfound') {
            $enquiry_type = 'deliverynotfound';
            $name = 'Postcode Unavailable';
        }
    $archive = 0;
        $total_covers = 0;
        if ($enquiry_type =='support' || $enquiry_type == 'career' || $enquiry_type == 'subscriptions' || $enquiry_type == 'contact' || $enquiry_type == 'deliverynotfound' || $enquiry_type == 'reservation') {
            $reservations = StaticBlockResponse::whereIn('restaurant_id', $restArray)
                    ->with(['restaurant' => function($query) {
                            $query->select('id', 'restaurant_name');
                        }])
                    ->where('enquiry_type', '=', trim($enquiry_type));

            
            if ($start_date || $end_date) {
                //$start_date = ($start_date) ? date('Y-m-d', strtotime($start_date)) : '';
                //$end_date = ($end_date) ? date('Y-m-d', strtotime($end_date)) : '';
                if ($start_date) {
                    $start_date = Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d');
                    $reservations = $reservations->where(DB::raw('date(created_at)'), '>=', $start_date);
                }
                if ($end_date) {
                    $end_date = Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d');
                    $reservations = $reservations->where(DB::raw('date(created_at)'), '<=', $end_date);
                }
            }
            #Archive Reservations
            $archive = 0;
            if ($enctype == 'archive') {
                $archive = 1;
                $current_date = date('Y-m-d');
                $reservations = $reservations->where(DB::raw('date(created_at)'), '<', $current_date);
            } elseif ($enquiry_type == 'reservation') {
                $current_date = date('Y-m-d');
                $reservations = $reservations->where(DB::raw('date(created_at)'), '>=', $current_date);
            }
            if (!empty($keyword)) {
                $reservations = $reservations->where(function ($query) use ($keyword) {
                    $query->where('response', 'like', '%' . $keyword . '%');
                });
            } 
            if ($enquiry_type == 'career') {
                $view = 'block.career_render';
            } elseif ($enquiry_type == 'catering') {
                $view = 'block.request_render';
            } elseif ($enquiry_type == 'deliverynotfound') {  
                $view = 'block.contact_render';
            }elseif ($enquiry_type == 'subscriptions') {
                $view = 'block.contact_render';
            }elseif ($enquiry_type == 'support') {
                $view = 'block.support_render';
            }else {
                $view = 'block.contact_render';
            }
            $reservations = $reservations->orderBy('created_at', 'desc')->paginate($page);

            if ($request->ajax()) {
                return view($view, compact('reservations', 'total_covers'));
                exit;
            }
        }
        
        return view('block.request', compact('reservations','name', 'total_covers', 'archive','prestId','enquiry_type'));
    }

    /* Catering Details */

    public function requestDetails(Request $request,$id) {
        $send_data = array();
        
        $requestDetails = StaticBlockResponse::find($id);  
        $user = Auth::user();
        
        $replyObj = EnquirySendMailLog::where('enquiry_id', $id)->orderBy('id', 'DESC')->get();
        //$restLocations = Restaurant::select('id', 'restaurant_name')->where('id', $rest_id)->orderBy('id', 'DESC')->get()->toArray();
        $replyArray['response'] = "fail";
        if ($replyObj) {
            $user = Auth::user();
            $replyDatas = $replyObj->toArray();
            foreach ($replyDatas as $key => $replyData) {
                $cmsuser = \App\Models\CmsUser::where('id', $replyData['cms_user'])->get()->toArray();
                $replydate = $replyData['created_at'];

                $created_date = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($requestDetails->parent_restaurant_id, $replydate)->format('D, M d h:i A');
                $replyData1 ['Sent To'] = $replyData['sent_to'];
                $replyData1['Sent On'] = $created_date;
                $replyData1 ['From Email'] = $cmsuser[0]['email'];
                $replyData1 ['From Name'] = $cmsuser[0]['name'];
                $replyData1 ['Message'] = $replyData['message'];
                
                
                $replyArray['records'][] = $replyData1;
            }
            $replyArray['response'] = "success";
        }
        
        


        if ($requestDetails) {
            $response = json_decode($requestDetails->response, 1);
            
            StaticBlockResponse::where('id',$id)->update(['stage' => 1]);
            $createdDate = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($requestDetails->parent_restaurant_id, $requestDetails->created_at)->format('D, M d h:i A');
            
            $eventDate = false;
            if(isset($response['date_of_occasion'])){
                $eventDate = date('m-d-Y', strtotime($response['date_of_occasion']));
            }elseif(isset($response['date_of_event'])){
                $eventDate = date('m-d-Y', strtotime($response['date_of_event']));
            }
            
            $resume = false;
            if (isset($response['resume']) && file_exists($response['resume'])) {

                $resume = URL::to("/")."/".$response['resume'];
                $ext = explode(".", $response['resume']);
                if($ext[1]=="png" || $ext[1]=="jpg" || $ext[1]=="gif" || $ext[1]=="jpeg"){
                   $icon = '<img src="'.URL::to("/").'/images/resume-img.png" alt="Reply" style="max-width: 40px; margin-top: 5px; vertical-align: top;" class="mCS_img_loaded">';
                }elseif($ext[1]=="docx" || $ext[1]=="doc"){
                   $icon = '<img src="'.URL::to("/").'/images/resume-doc.png" alt="Reply" style="max-width: 40px; margin-top: 5px; vertical-align: top;" class="mCS_img_loaded">';
                }elseif($ext[1]=="pdf"){
                   $icon = '<img src="'.URL::to("/").'/images/resume-pdf.png" alt="Reply" style="max-width: 40px; margin-top: 5px; vertical-align: top;" class="mCS_img_loaded">';
                }else{
                   $icon = '<img src="'.URL::to("/").'/images/resume-img.png" alt="Reply" style="max-width: 40px; margin-top: 5px; vertical-align: top;" class="mCS_img_loaded">';
                }               
               
                $response['resume'] = '<a href=\''.URL::to("/")."/".$response['resume'].'\' target="_blank">'.$icon.'</a>';
            }
            
            $message = false;
            if(isset($response['message']) && !empty($response['message'])){
                $message = nl2br(@$response['message']);
            }
            
            $registerUserId = false;
	    if(isset($response['email']))	
            $registerUser = \App\Models\User::select('id')->where('email',$response['email'])->get()->toArray();
            
            if(isset($registerUser)){
                $registerUserId = $registerUser[0]['id'];
            }
            $send_data = array(
                'id' => $requestDetails->id,
                'first_name' => isset($response['first_name']) ? ucfirst($response['first_name']) : '',
                'last_name' => isset($response['last_name']) ? ucfirst($response['last_name']) : '',
                'email' => isset($response['email'])?$response['email']:'',
                'phone' => isset($response['phone'])?$response['phone']:''                              
            );
            
            if($eventDate){
                $send_data['reservation_date'] = $eventDate;
            }
            
            if($resume){
                $send_data['resume'] = $resume;
            }
            
            if($message){
                $send_data['message'] = $message;
            }
            
            if (isset($response['no_of_guest'])) {
                $guest = $response['no_of_guest'];
                $send_data['reserved_seat'] = $guest;
                unset($response['no_of_guest']);
            }
            
            if (isset($response['no_of_people'])) {
                $guest = $response['no_of_people'];
                $send_data['reserved_seat'] = $guest;
                unset($response['no_of_people']);
            }     
            
            if ($response['enquiry_type'] == 'deliverynotfound') $response['enquiry_type']  ="Postcode Not Found";
	     if ($response['enquiry_type'] == 'contact') $response['enquiry_type']  ="Partner Bar";	
	     $send_data['enquiry_type'] = $response['enquiry_type'];

            $send_data['created_date'] = $createdDate;
	    if(isset($response['zipcode']))$response['postcode'] = $response['zipcode'];
            unset($response['first_name'], $response['last_name'], $response['email'], $response['phone'], $response['reservation_date'],$response['zipcode']);
            
            $send_data['extra_fields'] = $response;
          
            $loginUser = array('name'=>$user->name,'email'=>$user->email);
            return json_encode(array("reservation_detail" => $send_data, 'replylog' => $replyArray,'loginuser'=>$loginUser,'user_id'=>$registerUserId));
        } else {
            return json_encode(array("reservation_detail" => []));
        }
    }

   
    public function enquirySendMail(Request $request) {
        
        $email = $request->input('email');
        $id = $request->input('id');
        $replytype = $request->input('replytype');
        $message = $request->input('message');
        $requestDetails = StaticBlockResponse::find($id)->toArray();      
        $title = "We recieve your message.";
        
          
        $created_date = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($requestDetails['parent_restaurant_id'], date("Y/m/d H:i"))->format('D, M d h:i A');
        $str = "";
        $emailArray = [];
        $sentType = 0;
        $sentto = $email;
        if($replytype=="Forward"){
            $this->mailStr($requestDetails,$message);
            $sentto = $email;
            $email = explode(",", $email);
            $sentType = 1;
            $title = "We have recieved a message.";
                    
        }
               
        
        $enquirytype = $request->input('type');
        $username = $request->input('user_name');

        if ($request->has('email') && !empty($email) && $request->has('message')) {
            $user = Auth::user();
            $mailTemplate = "enquiryreplymail_user";
            $restaurantId = $user->restaurant_id;
            $fromName = $user->name;
            $fromEmail = $user->email;

            $data = [
                'restaurant_id' => $restaurantId,
                'cms_user' => $user->id,
                'enquiry_id' => $id,
                'message' => $message,
                'sent_type'=>$sentType,
                'sent_to'=>$sentto
            ];

            $cmsuser = \App\Models\CmsUser::where('id', $user->id)->get()->toArray();

            $replyData1 ['Sent On'] = $created_date;
            $replyData1 ['From Email'] = $cmsuser[0]['email'];
            $replyData1 ['From Name'] = $cmsuser[0]['name'];
            $replyData1 ['Message'] = $message;
            $replyData1 ['Sent to'] = $sentto;
            $replyData1 ['response'] = 'success';

            $mailFrom = Restaurant::select('restaurant_name', 'custom_from', 'support_from','restaurant_logo_name','source_url','address','facebook_url','twitter_url','pinterest_url','instagram_url','phone','street','zipcode')
                    ->where('id', $user->restaurant_id)->get()->toArray();
            $controller = new NotificationController;
            
            $mailKeywords['ADDRESS'] =  $mailFrom[0]['address'];
            $mailKeywords['TITLE'] = $title;
            $url = URL::to("/");

            $logo = '<img class="logo" src="'.$url.$mailFrom[0]['restaurant_logo_name'].'" alt="logo" style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:150px;outline:0;text-decoration:none;width:100%">';
            $mailKeywords['LOGO'] = $logo;
            
            $response = json_decode($requestDetails['response'], 1);
            $first_name = isset($response['first_name']) ? ucfirst($response['first_name']) : '';
            $last_name = isset($response['last_name']) ? " ".ucfirst($response['last_name']) : '';
            $username = ($replytype=="Forward")?'Hey <strong>'.$first_name.$last_name.',</strong>':'Dear <strong>'. $first_name.$last_name.",</strong>";
            
            $mailKeywords['FACEBOOK_URL']=$mailFrom[0]['facebook_url'];
            $mailKeywords['TWITTER_URL']=$mailFrom[0]['twitter_url'];
            $mailKeywords['INSTAGRAM_URL']=$mailFrom[0]['instagram_url'];
            $mailKeywords['PHONE']=$mailFrom[0]['phone'];
            
            $mailKeywords['STREET']=$mailFrom[0]['street'];
            $mailKeywords['ZIP']=$mailFrom[0]['zipcode'];
        
            $mailKeywords['USER_NAME'] = $username;

            $mailKeywords['MESSAGE'] = $message;
                $mailKeywords['RESTAURANT_NAME']=$mailFrom[0]['restaurant_name'];
            $config['to'] = $email;
            $config['subject'] = "We will contact soon";
            $config['from_name'] = $mailFrom[0]['restaurant_name'];
            $config['from_address'] = $mailFrom[0]['support_from'];
            
            $stage = ($replytype=="Forward")?3:2;//0=new,1=read,2=replied,3=forword
            StaticBlockResponse::where('id',$id)->update(['stage' => $stage]);

            $controller->sendMail($mailTemplate, 1, $config, $mailKeywords);
            
            
            EnquirySendMailLog::create($data);                

            return json_encode($replyData1);
        }
    }
    
    public function mailStr($requestDetails,&$message){
        $response = json_decode($requestDetails['response'], 1);
        $first_name = isset($response['first_name']) ? ucfirst($response['first_name']) : '';
        $last_name = isset($response['last_name']) ? ucfirst($response['last_name']) : '';
        $email = $response['email'];
        $phone = $response['phone'];        
       
        $createdDate = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($requestDetails['parent_restaurant_id'], date("Y/m/d H:i"))->format('D, M d h:i A');
            
        $eventDate = false;
        $date_of_occasion = false;
        if(isset($response['date_of_occasion'])){
            $date_of_occasion = date('F d, Y', strtotime($response['date_of_occasion']));
        }
        
        if(isset($response['date_of_event'])){
            $eventDate = date('F d, Y', strtotime($response['date_of_event']));
        }
        $additional_info = false;
        if (isset($response['additional_info']) && file_exists($response['additional_info'])) {
            $additional_info = $response['additional_info'];
        }
        
        $resume = false;
        if (isset($response['resume']) && file_exists($response['resume'])) {

            $ext = explode(".", $response['resume']);
            if($ext[1]=="png" || $ext[1]=="jpg" || $ext[1]=="gif" || $ext[1]=="jpeg"){
               $icon = '<img src="'.URL::to("/").'/images/resume-img.png" alt="Reply" style="max-width: 40px; margin-top: 5px; vertical-align: top;" class="mCS_img_loaded">';
            }elseif($ext[1]=="docx" || $ext[1]=="doc"){
               $icon = '<img src="'.URL::to("/").'/images/resume-doc.png" alt="Reply" style="max-width: 40px; margin-top: 5px; vertical-align: top;" class="mCS_img_loaded">';
            }elseif($ext[1]=="pdf"){
               $icon = '<img src="'.URL::to("/").'/images/resume-pdf.png" alt="Reply" style="max-width: 40px; margin-top: 5px; vertical-align: top;" class="mCS_img_loaded">';
            }else{
               $icon = '<img src="'.URL::to("/").'/images/resume-img.png" alt="Reply" style="max-width: 40px; margin-top: 5px; vertical-align: top;" class="mCS_img_loaded">';
            }
            $resume = '<a href=\''.URL::to("/")."/".$response['resume'].'\' target="_blank">'.$icon.'</a>';
        }

        $usermessage = false;
        if(isset($response['message']) && !empty($response['message'])){
            $usermessage = nl2br(@$response['message']);
        }
        $comment = false;
        if(isset($tesponse['comments']) && !empty($response['comments'])){
            $comment = nl2br(@$response['comments']);
        }           
        
        $mailStr = "";
        

        $mailStr .= "<div  style='display: block; min-height: 100px;font-family: Helvetica,Arial,sans-serif;color: #0a0a0a;'>".$message."<br/><br/></div>";
        
        
        $mailStr .= "<div style='font-weight: bold;font-family: Helvetica,Arial,sans-serif;color: #0a0a0a;'>---------- Forwarded message --------- </div>";
        $mailStr .= "<div style='font-weight: bold;font-family: Helvetica,Arial,sans-serif;color: #0a0a0a;'>From: ".$email."</div>";
        $mailStr .= "<div style='font-weight: bold;font-family: Helvetica,Arial,sans-serif;color: #0a0a0a;'>Date: ".$createdDate."</div>";

        
        $mailStr .="<div style='position: relative; margin-top: 40px;font-family: Helvetica,Arial,sans-serif;'>"

                . "<div style='font-size: 16px;font-weight: 500; color:#ff8700;line-height: 14px;float: left;font-family: Helvetica,Arial,sans-serif;margin-bottom: 10px'>".$first_name." ".$last_name."</div>"
                . "<div style='float: right;text-align: right;'>"
                . "<div  style='display: block;font-size: 16px;font-weight:normal;font-family: Helvetica,Arial,sans-serif;text-align: right;'><strong>Ph.:</strong> ".$phone."</div>"
                . "<div  style='display: block;font-size: 16px;font-weight:normal;font-family: Helvetica,Arial,sans-serif;text-align: right;'><strong>Email:</strong> ".$email."</div>"
                . "</div> <div style='clear: both;'></div></div>";

        $mailStr .= "<div style='border-bottom: 1px solid #ccc;margin-bottom: 25px; width: 100%; padding-top: 10px; display: block'></div>
<div  style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Enquiry type</span> ".$response['enquiry_type']."</div>";
        
        if(isset($response['occasion']) && !empty($response['occasion'])){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Occasion</span> ".$response['occasion']."</div>";
        }
        
        if($date_of_occasion){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Date Of Occasion</span> ".$date_of_occasion."</div>";

        }
        if($eventDate){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Date Of Event</span> ".$eventDate."</div>";

        }
        
        if (isset($response['no_of_guest'])) {
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>No. Of People</span> ".$response['no_of_guest']."</div>";

        }
        
        if(isset($response['address'])){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Address</span> ".$response['address']."</div>";

        }
        
        if(isset($response['Floor/Building/Suite No'])){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Floor/Building/Suite_No</span> <span>:</span> ".$response['Floor/Building/Suite No']."</div>";

        }
        
        if(isset($response['state'])){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>State</span> ".$response['state']."</div>";

        }
        if(isset($response['city'])){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>City</span> ".$response['city']."</div>";

        }
        if(isset($response['zipcode'])){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Zipcode</span> ".$response['zipcode']."</div>";

        }       
            
        if($resume){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Resume Link</span> ".$eventDate."</div>";

        }  
        
        if($additional_info){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 50%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Additional Info</span> ".$additional_info."</div>";

        }

        if($usermessage){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 100%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Comments</span> ".$usermessage."</div>";

        }
        if($comment){
            $mailStr .= "<div style='display: block; line-height: 30px;font-family: Helvetica,Arial,sans-serif;width: 100%;min-width: 230px;float: left;margin-bottom: 15px;color: #0a0a0a;'><span style='color: #ff8700; display: block;font-size: 12px;font-weight: 500;line-height: 6px;'>Comments</span> ".$comment."</div>";

        }

        $mailStr .= "<div style='clear: both;'></div>";

        $message =$mailStr;
    }   
    
    private function getUnreadEnquiry($request,$prestId,$user,&$unreadEnquiry){
    
        if($request->has('restaurant_id') && $request->get('restaurant_id')!="All"){
            $unreadEnquiryObj = StaticBlockResponse::groupBy('enquiry_type')
                ->where('restaurant_id', $request->get('restaurant_id'))->where('stage',0)
                ->select('enquiry_type', DB::raw('count(*) as total'))->get();
        }else{
            if($prestId == 0){
                $prestId = $user->restaurant_id;
            }
            $unreadEnquiryObj = StaticBlockResponse::groupBy('enquiry_type')
                ->where('parent_restaurant_id', $prestId)->where('stage',0)
                ->select('enquiry_type', DB::raw('count(*) as total'))->get();
        }
        if($unreadEnquiryObj){
            $unreadEnquiry = $unreadEnquiryObj->toArray();
        }
        

    }
    
}


//ALTER TABLE `static_block_responses`  ADD `stage` TINYINT NOT NULL DEFAULT '0' COMMENT '0=new,1=read,2=replied,3=forword'  AFTER `response`;
//ALTER TABLE `enquiry_sendmail_log`  ADD `sent_type` TINYINT NOT NULL DEFAULT '0' COMMENT '0=reply,1=forword'  AFTER `message`;
//ALTER TABLE `enquiry_sendmail_log`  ADD `sent_to` TEXT NULL DEFAULT NULL  AFTER `message`;
//ALTER TABLE `enquiry_sendmail_log` CHANGE `sent_type` `sent_type` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0=reply,1=forward';
//<img src="../images/resume-img.png" alt="Reply" style="max-width: 16px; vertical-align: top;" class="mCS_img_loaded">
//<img src="../images/resume-pdf.png" alt="Reply" style="max-width: 16px; vertical-align: top;" class="mCS_img_loaded">
//<img src="../images/resume-doc.png" alt="Reply" style="max-width: 16px; vertical-align: top;" class="mCS_img_loaded">
