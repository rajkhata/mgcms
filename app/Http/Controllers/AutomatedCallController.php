<?php

namespace App\Http\Controllers;

use App\Models\UserOrder;
use Illuminate\Http\Request;

//use DB;
class AutomatedCallController extends Controller
{

    public function sentCall($orderid)
    {
        $orderData = UserOrder::select('user_orders.id','user_orders.restaurant_id','user_orders.created_at','restaurants.phone','restaurants.call_interval')
        ->where('user_orders.status', 'placed')->where('restaurants.sent_call',1)->where('user_orders.is_call_sent',0)
        ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')->get()->toArray();                     
        if(isset($orderData[0]) && !empty($orderData[0])){
            
            foreach($orderData as $key => $value){
                $currentDate = \App\Helpers\CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $value['restaurant_id']));
                
                $currentTimestamp =  strtotime($currentDate->format('Y-m-d H:i:s'));
                $orderTimestamp = strtotime($value['created_at']);
                $interval = $value['call_interval']*60;
               
                $sentCallTime = $orderTimestamp+$interval;
               
                if($sentCallTime <= $currentTimestamp){  
                    $data['phone_number'] = $value['phone'];
                    $response = $this->automatedCall($data); 
                    
                    if(isset($response['error'])) {                       
                        echo $response['error'];
                    }else{
                        $uptArray['is_call_sent']=1;
                        UserOrder::where('id', $value['id'])->update($uptArray);
                        
                        echo $response['message'];
                    }                    
                   
                }else{
                    echo "Wait for time";
                }
            }
        }
            
        die;

    }
    
    private function automatedCall($data){
        $auth_id = "MAMMNHY2QYZDFLNTRMY2";
        $auth_token = "MzVjNDU0NjAwYzNkNWYxMjRkNjhmMDFkYzMzM2M2";
        $data = json_encode([
            'to'=>$data['phone_number'],
            'from'=>"1111111111",
            'answer_url'=>"https://s3.amazonaws.com/static.plivo.com/answer.xml",
            'answer_method'=>'GET'
            ]);

        $url= "https://api.plivo.com/v1/Account/".$auth_id."/Call/";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $auth_id . ':' . $auth_token);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $response = json_decode($result,1);
        if (isset($response['error'])) {
           return $response;
        }
        curl_close($ch);

        return $response; 
    }
    
       
}