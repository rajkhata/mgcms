<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Helpers\CommonFunctions;
use App\Models\DeliveryService;
use File;
use Config;
use Intervention\Image\Facades\Image;
use DB;
use Illuminate\Support\Facades\Auth;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        /*exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan view:clear');*/
    }

    public function index(Request $request)
    {
        $inputData = array('restaurant_id'=> 0 ,'rest_code'=>'');
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));        
        $restaurantData = Restaurant::select('id','restaurant_name')->whereIn('id',$locations)->orderBy('id','DESC')->get();

        if($restaurantData && count($restaurantData) == 1) {
           $inputData['restaurant_id'] = $restaurantData[0]['id'];
        }

      
        $rules = [
            'restaurant_name' => 'sometimes|nullable|max:100',
            'rest_code' => 'sometimes|nullable|max:20'
        ];
        $validator = Validator::make(Input::all(), $rules);
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        // parent restaurant id added in listing
        $parentRestId = Auth::user()->restaurant_id;
        if(!in_array($parentRestId,$locations)) {
            array_push($locations, $parentRestId);
        }
        if ($validator->passes()) {
            $restaurantObj = Restaurant::orderBy('id','DESC')->whereIn('id',$locations);

            if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
                $restaurantObj->where('id', $request->input('restaurant_id'));
                $inputData['restaurant_id'] = $request->input('restaurant_id');
            }

            if($request->input('rest_code')) {
                $restaurantObj->where('rest_code', '=', $request->input('rest_code'));
                $inputData['rest_code'] =  $request->input('rest_code');
            }

            $restaurant = $restaurantObj->paginate(20);
            return view('restaurant.index', compact('restaurant' ,'restaurantData'))->with('inputData',$inputData);
        }
        return redirect()->back()->withErrors($validator->errors())->with('inputData',$inputData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        // $restaurant = Restaurant::select('id', 'restaurant_name')
        //     ->Where(function ($query) {
        //         $query->Where('parent_restaurant_id', '0')
        //             ->orWhereNull('parent_restaurant_id');
        //     })
        //     ->orderBy('id', 'DESC')
        //     ->get();
        $restaurant = CommonFunctions::getParentRestaurant();
        $countryObj = Country::select('id','country_name')->orderBy('id', 'DESC')->get();
        $states = State::select('id', 'state')->where('country_id', 1)->get();

        $deliveryService = DeliveryService::select('id','short_name')->orderBy('id', 'DESC')->get();
        $reservationTypes = Restaurant::getPossibleEnumValues('reservation_type');


        //print_R($deliveryService); die;
        //print_r($countryObj); die;
        return view('restaurant.create', compact('restaurant','languageData', 'countryObj' ,'states', 'deliveryService','reservationTypes'));
    }

    public function getStates(Request $request){

        $cid = $request->input('country_id');
        $result = [];
        if(!empty($cid) && is_numeric($cid)) {
            $result = State::where('country_id', '=', $cid)->select('id','state')->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//'parent_restaurant_id' => 'sometimes|nullable|numeric|exists:restaurants,id',
        //print_r(Input::all()); die;
        $rules = [
            'parent_restaurant_id' => 'sometimes|nullable|numeric',
            'language_id' => 'required|exists:languages,id',
            'restaurant_name'      => 'required|max:30',
            'slug'      => 'required|max:100',
            'email'                => 'required',
            //'description'          => 'required|max:255',
            'address'              => 'required|max:255',
            'lat'                  => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],//|required_with:parent_restaurant_id|numeric
            'lng'                  =>['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],//|required_with:parent_restaurant_id|numeric
            //'delivery_geo'         => 'required',
            //'street'               => 'required|max:100',
            'zipcode'              => 'required|max:10',
            'city_id'              => 'required',
            'mobile'               => 'required|min:0|digits:10',
            'phone'                => 'sometimes|nullable|max:15',
            'status'               => 'required|in:0,1',
            //'accept_cc'            => 'required|in:0,1',
            //'accept_dc'            => 'required|in:0,1',
            'delivery'             => 'sometimes|nullable|in:0,1',
            'takeout'              => 'sometimes|nullable|in:0,1',
            'dinning'              => 'sometimes|nullable|in:0,1',
            'reservation'          => 'sometimes|nullable|in:0,1',
            'menu_available'       => 'sometimes|nullable|in:0,1',
            //'total_seats'          => 'required|numeric',
            'facebook_url'         => 'sometimes|nullable|url',
            'twitter_url'          => 'sometimes|nullable|url',
            'gmail_url'            => 'sometimes|nullable|url',
            'pinterest_url'        => 'sometimes|nullable|url',
            'instagram_url'        => 'sometimes|nullable|url',
            'yelp_url'             => 'sometimes|nullable|url',
            'tripadvisor_url'      => 'sometimes|nullable|url',
            'enquiry_prefix'        => 'sometimes|nullable',
            'foursquare_url'       => 'sometimes|nullable|url',
            'twitch_url'           => 'sometimes|nullable|url',
            'youtube_url'          => 'sometimes|nullable|url',
            'city_tax'             => 'required|in:0,1',
            'tax'                  =>  'numeric',
            'delivery_service_charge' => 'numeric'
        ];

        $validator = Validator::make(Input::all(), $rules,[
            'language_id.*' => 'Please select valid Language',
            'lat.regex' => 'Latitude value appears to be incorrect format.',
            'lng.regex' => 'Longitude value appears to be incorrect format.'
        ]);
        //$validator->setAttributeNames(['accept_cc'=>'Credit Card', 'accept_dc'=>'Debit Card','total_seats'=>'Seats','lat'=>'Latitude','lng'=>'Longitude' ]);
        $validator->setAttributeNames(['accept_cc'=>'Credit Card', 'accept_dc'=>'Debit Card','lat'=>'Latitude','lng'=>'Longitude' ]);

        $supportedLanguages = !empty($request->input('supported_languages')) ? implode(',', $request->input('supported_languages')) : '';
        Input::merge(['supported_languages' => $supportedLanguages]);

        $imageRelPath = '';
        if ($validator->passes()) {
            $max_file_size = config('constants.image.file_size');
            $max_video_file_size = config('constants.video.file_size');
            $allowed_image_extension = array("png", "jpg", "jpeg", "gif","svg");
            $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
            $allowed_video_extension = array("mp4", "mov", "3gp");
            $allowed_video_mime_types = array("video/mp4", "video/mov","video/3pg");

            $inputArr                         = Input::except('_method', '_token', 'country_id', 'state_id','image_hidden','image','update_days','update_hour','update_min');
            $inputArr['parent_restaurant_id'] = $inputArr['parent_restaurant_id'] ?? 0;
            $inputArr['min_partysize']        = $inputArr['min_partysize'] ?? 0;
            $inputArr['sentiments']           = $inputArr['sentiments'] ?? 0;
            $inputArr['order_pass_through']   = $inputArr['order_pass_through'] ?? 0;
            $inputArr['featured']             = $inputArr['featured'] ?? 0;
            $inputArr['pre_paid_enable']      = $inputArr['pre_paid_enable'] ?? 0;
            $inputArr['additional_charge_delivery']    = $inputArr['additional_charge_delivery'] ?? 0;
            $inputArr['service_tax']          = $inputArr['service_tax'] ?? 0;
            $inputArr['cod']                  = $inputArr['cod'] ?? 0;
            $inputArr['delivery_provider_id'] = $inputArr['delivery_provider_id'] ?? 0;
            $inputArr['delivery_provider_apikey'] = $inputArr['delivery_provider_apikey'] ?? "";
            $inputArr['producer_key'] = $inputArr['producer_key'] ?? "";
            $inputArr['delivery_charge'] = $inputArr['delivery_charge'] ?? 0.00;
            $inputArr['free_delivery'] = $inputArr['free_delivery'] ?? 0.00;
            $inputArr['minimum_delivery'] = $inputArr['minimum_delivery'] ?? 0.00;
            $inputArr['supported_languages'] = !empty($inputArr['supported_languages']) ? $inputArr['supported_languages'] : '';
            $inputArr['kpt_calender'] = $request->input('update_days').'-'.$request->input('update_hour').'-'.$request->input('update_min');
            $inputArr['kpt'] = ($request->input('update_days') * 24 * 60) + ($request->input('update_hour') * 60) + ($request->input('update_min')) ;

            $inputArr['meta_tags_type'] = array_filter($inputArr['meta_tags_type']);
            if(isset($inputArr['meta_tags_type']) && isset($inputArr['meta_tags_label']) && isset($inputArr['meta_tags_content'])
               && count($inputArr['meta_tags_type'])==count($inputArr['meta_tags_label']) && count($inputArr['meta_tags_type'])==count($inputArr['meta_tags_content'])) {
                $metaArr = [];
                for($i=0; $i<count($inputArr['meta_tags_type']); $i++) {
                    $metaArr[] = [
                        'type' => $inputArr['meta_tags_type'][$i],
                        'label' => $inputArr['meta_tags_label'][$i],
                        'content' => $inputArr['meta_tags_content'][$i],
                    ];
                }
                $inputArr['meta_tags'] = json_encode($metaArr);
            }
            unset($inputArr['meta_tags_type']);
            unset($inputArr['meta_tags_label']);
            unset($inputArr['meta_tags_content']);
            $inputArr['email_type'] = array_filter($inputArr['email_type']);
            if(isset($inputArr['email_type'])) {
                #$metaKeywords = array_combine($inputArr['meta_tags_key'], $inputArr['meta_tags_val']);
                $emailArr = [];
                for($i=0; $i<count($inputArr['email_type']); $i++) {
                    $emailArr[$inputArr['email_type'][$i]] = [
                        'from_name'     => $inputArr['from_name'][$i],
                        'from_email'    => $inputArr['from_email'][$i],
                        'to_name'       => $inputArr['to_name'][$i],
                        'to_email'      => $inputArr['to_email'][$i],
                    ];
                }
                $inputArr['email_data'] = json_encode($emailArr);
            }
            unset($inputArr['email_type']);
            unset($inputArr['from_name']);
            unset($inputArr['from_email']);
            unset($inputArr['to_name']);
            unset($inputArr['to_email']);

            if ($request->hasFile('restaurant_image_name')) {
                $restImage = $request->file('restaurant_image_name');
                if (!file_exists($restImage->getRealPath())) {
                    $validator->errors()->add('restaurant_image_name', 'Please select Image.');
                    $img_err = 1;
                } else if (!in_array($restImage->getClientOriginalExtension(), $allowed_image_extension) || !in_array($restImage->getMimeType(), $allowed_mime_types)) {
                    $validator->errors()->add('restaurant_image_name', 'Please select valid Image.');
                    $img_err = 1;
                } else if (($restImage->getClientSize() / 1024) > config('constants.image.file_size')) {
                    $validator->errors()->add('restaurant_image_name', 'Please upload image up to ' . $max_file_size . ' KB.');
                    $img_err = 1;
                }
            }
            if ($request->hasFile('restaurant_video_name')) {
                $restVideo = $request->file('restaurant_video_name');
                if (!file_exists($restVideo->getRealPath())) {
                    $validator->errors()->add('restaurant_video_name', 'Please select Video.');
                    $img_err = 1;
                } else if (!in_array($restVideo->getClientOriginalExtension(), $allowed_video_extension) || !in_array($restVideo->getMimeType(), $allowed_video_mime_types)) {
                    $validator->errors()->add('restaurant_video_name', 'Please select valid Video.');
                    $img_err = 1;
                } else if (($restVideo->getClientSize() / 1024) > config('constants.image.file_size')) {
                    $validator->errors()->add('restaurant_video_name', 'Please upload video up to ' . $max_video_file_size . ' KB.');
                    $img_err = 1;
                }
            }
            if ($request->hasFile('restaurant_logo_name')) {
                $restLogo = $request->file('restaurant_logo_name');
                if (!file_exists($restLogo->getRealPath())) {
                    $validator->errors()->add('restaurant_logo_name', 'Please select Image.');
                    $img_err = 1;
                } else if (!in_array($restLogo->getClientOriginalExtension(), $allowed_image_extension) || !in_array($restLogo->getMimeType(), $allowed_mime_types)) {
                    $validator->errors()->add('restaurant_logo_name', 'Please select valid Image.');
                    $img_err = 1;
                } else if (($restLogo->getClientSize() / 1024) > config('constants.image.file_size')) {
                    $validator->errors()->add('restaurant_logo_name', 'Please upload image up to ' . $max_file_size . ' KB.');
                    $img_err = 1;
                }
            }
            // TODO: image, video validation
            // upload image, video
            // RESTAURANT IMAGE
            if ($request->hasFile('restaurant_image_name')) {
                $imageRelPath = config('constants.image.rel_path.restaurant');
                $imagePath = config('constants.image.path.restaurant');
                // create restaurant folder
                if (!file_exists($imagePath) && !@mkdir($imagePath, 0777, true)) {
                    dd('unable to create directory ');
                } else {
                    $restLogo = $request->file('restaurant_image_name');
                    $uniqId = uniqid(mt_rand());
                    $restImageName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                    $imageName = $restImageName . '_image_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();

                    $image = Image::make($restLogo->getRealPath());
                    $width = $image->width();
                    $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                    $restImagePath = $imageRelPath . $imageName;
                    $inputArr['restaurant_image_name'] = $restImagePath;
                }
            }
            // RESTAURANT VIDEO
            if ($request->hasFile('restaurant_video_name')) {
                $videoRelPath = config('constants.video.rel_path.restaurant');
                $videoPath = config('constants.video.path.restaurant');
                // create restaurant folder
                if (!file_exists($videoPath) && !@mkdir($videoPath, 0777, true)) {
                    dd('unable to create directory');
                } else {
                    $restLogo = $request->file('restaurant_video_name');
                    $uniqId = uniqid(mt_rand());
                    $restVideoName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                    $videoName = $restVideoName . '_video_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();

                    $restLogo->move($videoPath, $videoName);
                    $restVideoPath = $videoRelPath . $videoName;
                    $inputArr['restaurant_video_name'] = $restVideoPath;
                }
            }
            // RESTAURANT LOGO
            if ($request->hasFile('restaurant_logo_name')) {
                $imageRelPath = config('constants.image.rel_path.restaurant');
                $imagePath = config('constants.image.path.restaurant');
                // create restaurant folder
                if (!file_exists($imagePath) && !@mkdir($imagePath, 0777, true)) {
                    dd('unable to create directory');
                } else {
                    $restLogo = $request->file('restaurant_logo_name');
                    $uniqId = uniqid(mt_rand());
                    $restImageName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                    $imageName = $restImageName . '_logo_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();

                    $image = Image::make($restLogo->getRealPath());
                    $width = $image->width();
                    $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                    $restLogoPath = $imageRelPath . $imageName;
                    $inputArr['restaurant_logo_name'] = $restLogoPath;
                }
            }

            $restObj = Restaurant::create($inputArr);
            if(isset($restObj->id))
            {
                $restObj->rest_code = 'REST' . str_pad($restObj->id,10,0,STR_PAD_LEFT);
                $restObj->restaurant_image_name = $imageRelPath;
                $restObj->save();
            }
            return redirect()->back()->with('message','Restaurant added successfully');
        }
        return redirect()->back()->withErrors($validator->errors())->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(isset($id) && is_numeric($id)) {
            $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
            $restaurantData = Restaurant::find($id);
            $restaurant = CommonFunctions::getParentRestaurant();

            // VALIDATE RESTAURANT EDIT
            $parentRestaurantId = $restaurantData['parent_restaurant_id'] ? $restaurantData['parent_restaurant_id'] : $restaurantData['id'];
            if(isset($restaurant[0]) && isset($restaurant[0]->id)){
                if($restaurant[0]->id != $parentRestaurantId) {
                    abort(404);
                }
            }

            // $restaurant = Restaurant::select('id', 'restaurant_name')
            //     ->where('id','<>',$id)
            //     ->Where(function ($query) {
            //         $query->Where('parent_restaurant_id', '0')
            //             ->orWhereNull('parent_restaurant_id');
            //     })
            //     ->orderBy('id', 'DESC')
            //     ->get();//echo "<pre>";print_r($restaurant);die;
          //echo "<pre>";
          //echo $restaurantData->delivery_provider_id;
            //print_r($restaurantData); die;
            $city_id =  $restaurantData->city_id;
            $countryID = 1;
            $statesdata = City::select('state_id', 'country_id')->where('id', $city_id)->first();
            $stateID = $statesdata->state_id;
            $countryID = $statesdata->country_id;
            $states = State::select('id', 'state')->where('country_id', $countryID)->get();

            $countryObj = Country::select('id','country_name')->orderBy('id', 'DESC')->get();
            $citydata = City::where('state_id', $stateID)->select('id','city_name')->get();
            $deliveryService = DeliveryService::select('id','short_name')->orderBy('id', 'DESC')->get();
            $reservationTypes = Restaurant::getPossibleEnumValues('reservation_type');
            // contact city, state, country
            $contactCityId = $restaurantData->contact_city_id;
            $contactStateId = $restaurantData->contact_state_id ?? '';
            $contactCountryId = $restaurantData->contact_country_id ?? '';
           // echo "<pre>";print_r($restaurantData);die;
            //print_r($citydata); die;

            return view('restaurant.edit', compact('restaurant','languageData','restaurantData', 'states', 'reservationTypes',
                'stateID', 'countryID', 'countryObj', 'citydata', 'city_id', 'deliveryService', 'contactCityId', 'contactStateId', 'contactCountryId'));
        }
        return Redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($id) && is_numeric($id)) {
            $rules = [
                'parent_restaurant_id'    => 'sometimes|nullable|numeric|exists:restaurants,id',
                'restaurant_name'     => 'required|max:30',
                'slug'     => 'required|max:100',
                'email'    => 'required',
                'language_id' => 'required|exists:languages,id',
                //'description' => 'required|max:255',
                'city_id'   =>  'required',
                'address'     => 'required|max:255',
                'lat'     => 'sometimes|nullable|required_with:parent_restaurant_id|numeric',
                'lng'     => 'sometimes|nullable|required_with:parent_restaurant_id|numeric',
                //'delivery_geo' => 'required',
                //'street'         => 'required|max:100',
                'zipcode'         => 'required|max:10',
                'mobile'         => 'required|min:0|digits:10',
                'phone'         => 'sometimes|nullable|max:15',
                'status'         => 'required|in:0,1',
                //'accept_cc'         => 'required|in:0,1',
                //'accept_dc'         => 'required|in:0,1',
                'delivery'         => 'sometimes|nullable|in:0,1',
                'takeout'         => 'sometimes|nullable|in:0,1',
                'dinning'         => 'sometimes|nullable|in:0,1',
                'reservation'       => 'sometimes|nullable|in:0,1',
                'menu_available'    => 'sometimes|nullable|in:0,1',
                //'total_seats'         => 'required|numeric',
                'facebook_url'         => 'sometimes|nullable|url',
                'twitter_url'         => 'sometimes|nullable|url',
                'gmail_url'         => 'sometimes|nullable|url',
                'pinterest_url'         => 'sometimes|nullable|url',
                'instagram_url'         => 'sometimes|nullable|url',
                'yelp_url'         => 'sometimes|nullable|url',
                'tripadvisor_url'         => 'sometimes|nullable|url',
                'foursquare_url'         => 'sometimes|nullable|url',
                'twitch_url'         => 'sometimes|nullable|url',
                'youtube_url'         => 'sometimes|nullable|url',
                'tip'                    => 'sometimes|nullable',
                'reservation_fee'        => 'sometimes|nullable',
                'enquiry_prefix'        => 'sometimes|nullable',
                'reservation_w_payment'  => 'sometimes|in:0,1',
                'reservation_wo_payment' => 'sometimes|in:0,1',
                'reservation_w_dinein'   => 'sometimes|in:0,1',
                'delivery_from'          => 'sometimes|nullable',
                'delivery_to'            => 'sometimes|nullable',
                'carryout_from'          => 'sometimes|nullable',
                'carryout_to'            => 'sometimes|nullable',
                'reservation_from'       => 'sometimes|nullable',
                'reservation_to'         => 'sometimes|nullable',
                'custom_from'            => 'sometimes|nullable',
                'support_from'           => 'sometimes|nullable',
                'city_tax'             => 'required|in:0,1',
                'tax'                  =>  'numeric',
                'delivery_service_charge' => 'numeric',
            ];
            $validator = Validator::make(Input::all(), $rules,[
                'language_id.*' => 'Please select valid Language',
                'image.image' => 'Please select valid Image',
                'image.mimes' => 'Please select valid Image Type',
                'image.max' => 'Please select size up to'.config('constants.image.file_size'),
                'image.*' => 'Please select Image',
            ]);
            //$validator->setAttributeNames(['accept_cc'=>'Credit Card', 'accept_dc'=>'Debit Card','total_seats'=>'Seats','lat'=>'Latitude','lng'=>'Longitude' ]);
            $validator->setAttributeNames(['accept_cc'=>'Credit Card', 'accept_dc'=>'Debit Card','lat'=>'Latitude','lng'=>'Longitude' ]);


            if ($validator->passes()) {
                $restaurantObj = Restaurant::find($id);

                $img_err = 0;
                $max_file_size = config('constants.image.file_size');
                $max_video_file_size = config('constants.video.file_size');
                $allowed_image_extension = array("png", "jpg", "jpeg", "gif","svg");
                $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
                $allowed_video_extension = array("mp4", "mov", "3gp");
                $allowed_video_mime_types = array("video/mp4", "video/mov","video/3pg");
                if (!empty($restaurantObj)) {
                    $inputArr                         = Input::except('_method', '_token', 'country_id', 'state_id','image_hidden','image','update_days','update_hour','update_min');
                    //dd($inputArr);
                    $inputArr['parent_restaurant_id'] = $inputArr['parent_restaurant_id'] ?? 0;
                    $inputArr['min_partysize']        = $inputArr['min_partysize'] ?? 0;
                    $inputArr['sentiments']           = $inputArr['sentiments'] ?? 0;
                    $inputArr['order_pass_through']   = $inputArr['order_pass_through'] ?? 0;
                    $inputArr['featured']             = $inputArr['featured'] ?? 0;
                    $inputArr['pre_paid_enable']      = $inputArr['pre_paid_enable'] ?? 0;
                    $inputArr['cod']                  = $inputArr['cod'] ?? 0;
                    $inputArr['delivery_provider_id'] = $inputArr['delivery_provider_id'] ?? 0;
                    $inputArr['delivery_provider_apikey'] = $inputArr['delivery_provider_apikey'] ?? "";
                    $inputArr['producer_key'] = $inputArr['producer_key'] ?? "";
                    $inputArr['delivery_charge'] = $inputArr['delivery_charge'] ?? 0.00;
                    $inputArr['free_delivery'] = $inputArr['free_delivery'] ?? 0.00;
                    $inputArr['minimum_delivery'] = $inputArr['minimum_delivery'] ?? 0.00;
                    $inputArr['supported_languages'] = !empty($inputArr['supported_languages']) ? implode(',', $inputArr['supported_languages']) : '';

                    $inputArr['meta_tags_type'] = array_filter($inputArr['meta_tags_type']);
                    if(isset($inputArr['meta_tags_type']) && isset($inputArr['meta_tags_label']) && isset($inputArr['meta_tags_content'])
                       && count($inputArr['meta_tags_type'])==count($inputArr['meta_tags_label']) && count($inputArr['meta_tags_type'])==count($inputArr['meta_tags_content'])) {
                        #$metaKeywords = array_combine($inputArr['meta_tags_key'], $inputArr['meta_tags_val']);
                        $metaArr = [];
                        for($i=0; $i<count($inputArr['meta_tags_type']); $i++) {
                            $metaArr[] = [
                                'type' => $inputArr['meta_tags_type'][$i],
                                'label' => $inputArr['meta_tags_label'][$i],
                                'content' => $inputArr['meta_tags_content'][$i],
                            ];
                        }
                        $inputArr['meta_tags'] = json_encode($metaArr);
                    }
                    unset($inputArr['meta_tags_type']);
                    unset($inputArr['meta_tags_label']);
                    unset($inputArr['meta_tags_content']);

                    $inputArr['email_type'] = array_filter($inputArr['email_type']);
                    if(isset($inputArr['email_type'])) {
                        #$metaKeywords = array_combine($inputArr['meta_tags_key'], $inputArr['meta_tags_val']);
                        $emailArr = [];
                        for($i=0; $i<count($inputArr['email_type']); $i++) {
                            $emailArr[$inputArr['email_type'][$i]] = [
                                'from_name'     => $inputArr['from_name'][$i],
                                'from_email'    => $inputArr['from_email'][$i],
                                'to_name'       => $inputArr['to_name'][$i],
                                'to_email'      => $inputArr['to_email'][$i],
                            ];
                        }
                        $inputArr['email_data'] = json_encode($emailArr);
                    }
                    unset($inputArr['email_type']);
                    unset($inputArr['from_name']);
                    unset($inputArr['from_email']);
                    unset($inputArr['to_name']);
                    unset($inputArr['to_email']);
                    //dump($inputArr['email_data']); dd($inputArr);

                    if($request->input('contact_address')) {
                       $inputArr['contact_address'] = $request->input('contact_address');
                    }
                    /*if(!$request->input('image_hidden')){
                       $inputArr['restaurant_image_name'] = NULL;
                    }*/
                    if ($request->hasFile('restaurant_image_name')) {
                        $restImage = $request->file('restaurant_image_name');
                        if (!file_exists($restImage->getRealPath())) {
                            $validator->errors()->add('restaurant_image_name', 'Please select Image.');
                            $img_err = 1;
                        } else if (!in_array($restImage->getClientOriginalExtension(), $allowed_image_extension) || !in_array($restImage->getMimeType(), $allowed_mime_types)) {
                            $validator->errors()->add('restaurant_image_name', 'Please select valid Image.');
                            $img_err = 1;
                        } else if (($restImage->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('restaurant_image_name', 'Please upload image up to ' . $max_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                    if ($request->hasFile('restaurant_video_name')) {
                        $restVideo = $request->file('restaurant_video_name');
                        if (!file_exists($restVideo->getRealPath())) {
                            $validator->errors()->add('restaurant_video_name', 'Please select Video.');
                            $img_err = 1;
                        } else if (!in_array($restVideo->getClientOriginalExtension(), $allowed_video_extension) || !in_array($restVideo->getMimeType(), $allowed_video_mime_types)) {
                            $validator->errors()->add('restaurant_video_name', 'Please select valid Video.');
                            $img_err = 1;
                        } else if (($restVideo->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('restaurant_video_name', 'Please upload video up to ' . $max_video_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                    if ($request->hasFile('restaurant_logo_name')) {
                        $restLogo = $request->file('restaurant_logo_name');
                        if (!file_exists($restLogo->getRealPath())) {
                            $validator->errors()->add('restaurant_logo_name', 'Please select Image.');
                            $img_err = 1;
                        } else if (!in_array($restLogo->getClientOriginalExtension(), $allowed_image_extension) || !in_array($restLogo->getMimeType(), $allowed_mime_types)) {
                            $validator->errors()->add('restaurant_logo_name', 'Please select valid Image.');
                            $img_err = 1;
                        } else if (($restLogo->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('restaurant_logo_name', 'Please upload image up to ' . $max_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                    // TODO: image, video validation
                    // upload image, video
                    // RESTAURANT IMAGE
                    if ($request->hasFile('restaurant_image_name')) {
                        $imageRelPath = config('constants.image.rel_path.restaurant');
                        $imagePath = config('constants.image.path.restaurant');
                        // create restaurant folder
                        if (!file_exists($imagePath) && !@mkdir($imagePath, 0777, true)) {
                            dd('unable to create directory ');
                        } else {
                            $restLogo = $request->file('restaurant_image_name');
                            $uniqId = uniqid(mt_rand());
                            $restImageName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                            $imageName = $restImageName . '_image_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();

                            $image = Image::make($restLogo->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                            $restImagePath = $imageRelPath . $imageName;
                            $inputArr['restaurant_image_name'] = $restImagePath;
                        }
                    }
                    // RESTAURANT VIDEO
                    if ($request->hasFile('restaurant_video_name')) {
                        $videoRelPath = config('constants.video.rel_path.restaurant');
                        $videoPath = config('constants.video.path.restaurant');
                        // create restaurant folder
                        if (!file_exists($videoPath) && !@mkdir($videoPath, 0777, true)) {
                            dd('unable to create directory');
                        } else {
                            $restLogo = $request->file('restaurant_video_name');
                            $uniqId = uniqid(mt_rand());
                            $restVideoName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                            $videoName = $restVideoName . '_video_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();

                            $restLogo->move($videoPath, $videoName);
                            $restVideoPath = $videoRelPath . $videoName;
                            $inputArr['restaurant_video_name'] = $restVideoPath;
                        }
                    }
                    // RESTAURANT LOGO
                    if ($request->hasFile('restaurant_logo_name')) {
                        $imageRelPath = config('constants.image.rel_path.restaurant');
                        $imagePath = config('constants.image.path.restaurant');
                        // create restaurant folder
                        if (!file_exists($imagePath) && !@mkdir($imagePath, 0777, true)) {
                            dd('unable to create directory');
                        } else {
                            $restLogo = $request->file('restaurant_logo_name');
                            $uniqId = uniqid(mt_rand());
                            $restImageName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                            $imageName = $restImageName . '_logo_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();

                            $image = Image::make($restLogo->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                            $restLogoPath = $imageRelPath . $imageName;
                            $inputArr['restaurant_logo_name'] = $restLogoPath;
                        }
                    }

                    $inputArr['kpt_calender'] = $request->input('update_days').'-'.$request->input('update_hour').'-'.$request->input('update_min');

                    $inputArr['kpt'] = ($request->input('update_days') * 24 * 60) + ($request->input('update_hour') * 60) + ($request->input('update_min')) ;
                    Restaurant::where('id', $id)->update($inputArr);

                    return Redirect::back()->with('message', 'Restaurant updated successfully');
                }
            }
            else
                return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        return Redirect::back()->with('message', 'Invalid Id');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(isset($id) && is_numeric($id)) {
            $restaurantObj = Restaurant::find($id);
            if (!empty($restaurantObj)) {
                $restaurantObj->delete();
                return Redirect::back()->with('message', 'Restaurant deleted successfully');
            }
        }
        return Redirect::back()->with('err_msg','Invalid Id');
    }

    /**    
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @Date: 29-01-2019
     */

    public function manageServices()
    {    
        $restInfo = array();
        $selected_restaurant_id = ''; 
        $groupRestData = CommonFunctions::getRestaurantGroup();
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $all_service_message = "Online ordering is not set up for your website currently.";
        $globalSetting = DB::table('global_settings')->whereNotNull('contact_mobile')->where('contact_mobile', '<>','')->first();
        if($globalSetting) {
            $all_service_message .= "  Please call us at $globalSetting->contact_mobile to discuss how you can start accepting orders online";
        }
        if($restArray && count($restArray) == 1) {
            $selected_restaurant_id = $restArray['0'];
            $restInfo = CommonFunctions::getRestaurantInfo($selected_restaurant_id, false, array('id', 'current_version', 'delivery', 'takeout', 'reservation', 'reservation_type', 'delivery', 'takeout', 'reservation', 'reservation_type', 'is_merchandise_allowed', 'is_catering_allowed', 'is_contact_allowed', 'is_event_allowed', 'is_career_allowed','merchandise_takeout_allowed','merchandise_delivery_allowed','food_ordering_delivery_allowed','food_ordering_takeout_allowed','pause_service_message','is_reservation_allowed','is_food_order_allowed','is_gift_card_allowed'));

        }
        return view('services.manage_services', compact('groupRestData', 'restArray' ,'selected_restaurant_id', 'restInfo','all_service_message'));
    }

    public function restaurantsettings(Request $request)
    { 
        if(!$request->has('restaurant_id') || $request->input('restaurant_id') == ''){
            return back()->withInput()->withErrors(array('message' => ' No restaurant found'));
        }else{
            $restaurant = Restaurant::find($request->input('restaurant_id'));
           #echo "<pre>";print_r($request->all());die;
            DB::beginTransaction();
            try {    
                $current_action = $request->input('action');
                if($current_action == 'delivery') {
                    if( $request->input('delivery') == 0) {
                        $activity_type = 'Off';
                        $success_msg = "Your website is currently not accepting new delivery orders."; 
                    }else {
                        $success_msg = "Your website is now accepting new delivery orders. Congratulations!"; 
                        $activity_type = 'On';
                    }   
                    $service_type = 1;                 
                }  
                if($current_action == 'takeout') {
                    if( $request->input('takeout') == 0) {
                        $activity_type = 'Off';
                        $success_msg = "Your website is currently not accepting new takeout orders. "; 
                    }else {
                        $success_msg = "Your website is now accepting new takeout orders. Congratulations!"; 
                        $activity_type = 'On';
                    }     
                    $service_type = 2;               
                }  
                if($current_action == 'reservation') {
                    if( $request->input('reservation') == 0) {
                        $activity_type = 'Off';
                        $success_msg = "Your website is currently not accepting new takeout reservations. "; 
                    }else {
                        $success_msg = "Your website is now accepting new takeout reservations. Congratulations!"; 
                        $activity_type = 'On';
                    }  
                    $service_type = 3;                  
                }    
                $restaurant->reservation= $request->input('reservation'); 
                $restaurant->delivery= $request->input('delivery'); 
                $restaurant->takeout= $request->input('takeout'); 
                if($request->has('pause_service_message')) {
                    #empty the messgae when every service is ON
                    #count how many services are allowed
                    $services = 0;
                    if($restaurant->is_food_order_allowed) {
                        if($restaurant->food_ordering_delivery_allowed) {
                           $services += 1; 
                        }
                        if($restaurant->food_ordering_takeout_allowed) {
                           $services += 1; 
                        }                            
                    } 
                    if($restaurant->is_reservation_allowed && $restaurant->reservation_type == 'full') {
                       $services += 1; 
                    }
                   
                    $current_services = $restaurant->reservation + $restaurant->delivery + $restaurant->takeout;
                   
                    if($services == $current_services && $services > 0) {
                        $restaurant->pause_service_message = '';
                    }else {
                        $restaurant->pause_service_message = $request->input('pause_service_message');
                    }
                }
                $restaurant->save();  
                
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $restaurant->id));   
                if(is_object($currentDateTimeObj)) {                        
                    $current_datetime = $currentDateTimeObj->format('Y-m-d H:i:s');
                }else {                        
                    $current_datetime = date('Y-m-d H:i:s');
                }
                $cms_user_id = \Auth::user()->id;
                $logData = [
                    'cms_user_id' => $cms_user_id,
                    'effective_date' => $current_datetime,          
                    'service_type' => $service_type,
                    'activity_type' => $activity_type,
                    'restaurant_id' => $restaurant->id,
                    'reason' => $request->has('pause_service_message') && $activity_type == 'Off' ? $request->input('pause_service_message')  : "",
                   
                    'ipaddress' => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
                ]; 
                DB::table('dashboard_services_logs')->insert($logData);

                DB::commit();
                return back()->withInput()->with(['status'=> $success_msg]);
           }catch (\Exception $e){                    
                DB::rollback();
                return back()->withInput()->withErrors(array('message' =>'Unable to update the settings'));
            }
        }          
    }

    /**
     * @param Params : token
     * @author : Rahul Gupta
     * @Date Created : 19-01-2019
     * @return Send logs
     */

    public function pauseResumeServiceHistory(Request $request) { 
        //print_r($request->all); die;

        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $logs = DB::table('dashboard_services_logs')->whereIn('dashboard_services_logs.restaurant_id', $restArray);
        if(isset($requestData['to']) && $requestData['from']) {
            $logs = $logs->whereDate('effective_date', '>=', $request->input('from'))->whereDate('effective_date', '<=', $request->input('to'));
        }
        $log_data = $logs->select('id','effective_date', 'service_type', 'activity_type', 'reason', DB::raw('(CASE WHEN service_type = 1 THEN "Delivery" ELSE (CASE WHEN service_type = 2 THEN "Takeout" ELSE (CASE WHEN service_type = 3 THEN "Reservation" END) END ) END) AS service_type') )->orderBy('created_at', 'DESC')->paginate(10);      
       
        #echo "<pre>";print_r($log_data);die;
        return response()->view('services.history', compact('log_data'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    }
}
