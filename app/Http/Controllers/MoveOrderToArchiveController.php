<?php

namespace App\Http\Controllers;


use App\Models\UserOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\CommonFunctions;
use Carbon\Carbon;

class MoveOrderToArchiveController extends Controller
{
   ### if Order is place stage in case of order already has been delivered or takeout
    public function index()
    {      
        
        $statusArray = array("archived", "cancelled", "arrived", "sent", "rejected", "refunded");
        $uptArray['manual_update']=0;
        $uptArray['order_state']=3;
        $updatedRecords = [];    
        $response = UserOrder::withoutTimestamps()->whereIn('user_orders.status',$statusArray)
                ->orwhereRaw('DATE_ADD(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL '.$_ENV['MOVE_ORDER_TO_ARCHIVE_TIME'].' HOUR)<=CONVERT_TZ(now(),"+00:00", cities.timezone_value)')
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
                ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                ->update($uptArray);                
        $updatedRecords[] = "updated successfully Order ID =>".$response;
        return json_encode($updatedRecords);
        }
        
      
}
