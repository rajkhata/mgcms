<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Models\MenuSettingCategory;
use App\Models\Restaurant;
use App\Models\MenuCategory;
use Illuminate\Support\Facades\Input;
use App\Models\Language;

class MenuSettingCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan view:clear');
        //exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan cache:clear');
    }

    public function create(Request $request)
    {
        $groupRestData = Restaurant::group();
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        return view('menu_setting_category.create', compact('groupRestData', 'languageData'));
    }

    public function edit($id)
    {
        if(isset($id) && is_numeric($id)) {
            $categoryData  = array();
            $groupRestData = Restaurant::group();
            $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
            $menuSettingCategory = MenuSettingCategory::find($id);
            if (!empty($menuSettingCategory)) {
                $categoryData = MenuCategory::where('restaurant_id', '=', $menuSettingCategory->restaurant_id)->orderBy('name', 'DESC')->get();
            }
            return view('menu_setting_category.edit')->with('groupRestData', $groupRestData)->with('menuSettingCategory', $menuSettingCategory)->with('categoryData', $categoryData)->with('languageData', $languageData);
        }
        return Redirect::back();
    }

    public function update($id)
    {
        $rules = [
            'name' => 'required|max:100',
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'status' => 'required|in:0,1',
            'menu_category_id' => 'required|numeric|exists:menu_categories,id',
            'is_multi_select' => 'required|in:0,1',
            'is_mandatory' => 'required|in:0,1'
        ];
        $rulesMsg = [
            'name.*' => 'Please enter valid name upto 100 chars',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status',
            'menu_category_id.*' => 'Please Select Category',
            'is_multi_select.*' => 'Please select selection type',
            'is_mandatory.*' => 'Please select mandatory or not'
        ];
        $validator = Validator::make(Input::all(), $rules, $rulesMsg);
        if ($validator->fails()) {
            $categoryData = array();
            if(Input::get('restaurant_id') && is_numeric(Input::get('restaurant_id')))
            {
                $categoryData = MenuCategory::where('restaurant_id', '=',trim(Input::get('restaurant_id')))->get();
            }
            return redirect()->back()->withErrors($validator->errors())->withInput()->with('newCategoryData',$categoryData);
        } else {
            $menuCategoryObj = MenuSettingCategory::find($id);
            $menuCategoryObj->name       = Input::get('name');
            $menuCategoryObj->restaurant_id      = Input::get('restaurant_id');
            $menuCategoryObj->language_id      = Input::get('language_id');
            $menuCategoryObj->menu_category_id      = Input::get('menu_category_id');
	    $menuCategoryObj->display_order      = Input::get('display_order');	
            $menuCategoryObj->status = Input::get('status');
            $menuCategoryObj->is_multi_select = Input::get('is_multi_select');
            $menuCategoryObj->is_mandatory = Input::get('is_mandatory');
            $menuCategoryObj->maximum_selection =!empty(Input::get('maximum_selection'))? Input::get('maximum_selection'):NULL;
            $menuCategoryObj->minimum_selection =!empty(Input::get('minimum_selection'))? Input::get('minimum_selection'):NULL;
            $menuCategoryObj->equal_selection =!empty(Input::get('equal_selection'))? Input::get('equal_selection'):NULL;

            //$menuCategoryObj->priority = Input::get('priority');
            $menuCategoryObj->save();
            return Redirect::back()->with('message','Category updated successfully');
        }
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:100',
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'status' => 'required|in:0,1',
            'menu_category_id' => 'required|numeric|exists:menu_categories,id',
            'is_multi_select' => 'required|in:0,1',
            'is_mandatory' => 'required|in:0,1'
        ], [
            'name.*' => 'Please enter valid name upto 100 chars',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status',
            'menu_category_id.*' => 'Please select Category',
            'is_multi_select.*' => 'Please select selection type',
            'is_mandatory.*' => 'Please select mandatory or not'
        ]);
        if($validator->passes())
        {
            $menuCategoryArr = ['restaurant_id'=>$request->input('restaurant_id'),
                'language_id'=>$request->input('language_id'),
                'name'=>$request->input('name'),
                'status'=>$request->input('status'),
                'menu_category_id' => $request->input('menu_category_id'),
                'is_multi_select' => $request->input('is_multi_select'),
		'display_order' => $request->input('display_order'),
                'is_mandatory' => $request->input('is_mandatory'),
                 'maximum_selection' =>!empty($request->input('maximum_selection'))?$request->input('maximum_selection'):NULL,
                 'minimum_selection' =>!empty($request->input('minimum_selection'))?$request->input('minimum_selection'):NULL,
                 'equal_selection' =>!empty($request->input('equal_selection'))?$request->input('equal_selection'):NULL,

            ];
            $menuCategoryObj = MenuSettingCategory::create($menuCategoryArr);
            return Redirect::back()->with('message','Category added successfully');
        }
        $categoryData = array();
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $categoryData = MenuCategory::where('restaurant_id', '=',trim($request->input('restaurant_id')))->get();
        }
        return redirect()->back()->withErrors($validator->errors())->withInput()
            ->with('categoryData',$categoryData);
    }

    public function index(Request $request)
    {
        $searchArr = array('restaurant_id'=>0,'menu_category_id'=>0,'menu_setting_category_id'=>0);
        $categoryData = array();
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();
        $menuSettingCategories = MenuSettingCategory::orderBy('id','DESC');
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $categoryData = MenuCategory::where('restaurant_id', '=',trim($request->input('restaurant_id') ))->get();
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuSettingCategories->where('restaurant_id',$searchArr['restaurant_id']);
        }
        if($request->input('menu_category_id') && is_numeric($request->input('menu_category_id')))
        {
            $searchArr['menu_category_id'] = $request->input('menu_category_id');
            $menuSettingCategories->where('menu_category_id',$searchArr['menu_category_id']);
        }
        $menuSettingCategories = $menuSettingCategories->paginate(20);
        return view('menu_setting_category.index', compact('menuSettingCategories'))->with('restaurantData',$restaurantData)->with('searchArr',$searchArr)->with('categoryData',$categoryData);
    }

    public function destroy($id)
    {
        $menuSettingCategoryObj = MenuSettingCategory::find($id);
        if(!empty($menuSettingCategoryObj))
        {
            $menuSettingCategoryObj->delete();
            return Redirect::back()->with('message','Category deleted successfully');
        }
        else
            return Redirect::back()->with('err_msg','Invalid Category');
    }

    public function getSettingCategory(Request $request)
    {
        $cid = $request->input('cid');
        if(!empty($cid) && is_numeric($cid)) {
            $result['cat'] = MenuSettingCategory::where('menu_category_id', '=', $cid)->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }
}
