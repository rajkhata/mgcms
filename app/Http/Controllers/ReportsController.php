<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Helpers\CommonFunctions;
use App\Models\Restaurant;
use Session;
//use Request;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Orders',['only'=>['orders']]);
        //$this->middleware('permission:Locations',['only'=>['locations']]);
        //$this->middleware('permission:Reservations',['only'=>['reservations']]);
        //$this->middleware('permission:Site Visitors',['only'=>['visitors']]);
        //$this->middleware('permission:Marketing Campaign',['only'=>['marketing']]);
        //$this->middleware('permission:Social',['only'=>['social']]);
        
    }

    /**
     * User listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
         $data=config("constants.report_api_configs");
        $restaurant_id=\Auth::user()->restaurant_id;
        $restaurant_name=($restaurant_id==24 || $restaurant_id==25)?'Katana':$data['restaurant_name'];
        $data['restaurant_name']=$restaurant_name;
         return view('reports.index',compact('data'));
    }
    public function remoteFileExists($url) {
            $curl = curl_init($url);

            //don't fetch the actual page, you only want to check the connection is ok
            curl_setopt($curl, CURLOPT_NOBODY, true);

            //do request
            $result = curl_exec($curl);

            $ret = false;

            //if request did not fail
            if ($result !== false) {
                //if request was ok, check response code
                $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  

                if ($statusCode == 200) {
                    $ret = true;   
                }
            }

            curl_close($curl);

            return $ret;
}

        public function download(Request $request){

            $data=config("constants.report_api_configs");
            $year=$request->get('year');
            $deal_id=$request->get('deal_id');
            $month=$request->get('month');
            $restaurant_id=\Auth::user()->restaurant_id;
            $restaurant_name=($restaurant_id==24 || $restaurant_id==25)?'Katana':$request->get('restaurant_name');
            if(!empty($year) && !empty($year) && !empty($year) && !empty($year)){
                $file_name = rawurlencode($restaurant_name).'.pdf';
                $file_url=$data['report_url'].$year.'/'.$deal_id.$data['deal_id_separator'].'/'.strtolower($month).'/'.$file_name ;
           if ($this->remoteFileExists($file_url)) {

                header('Content-Type: application/octet-stream');
                header("Content-Transfer-Encoding: Binary"); 
                header("Content-disposition: attachment; filename=\"".$file_name."\""); 
                readfile($file_url);
                exit;
            }else{

                return Redirect::back()->withErrors(['Report not found']);

            }

            }

           
    }

    public function getHtmlOfGrowthRate($getGrowthRate){
        if($getGrowthRate < 0){
            $growthRateHtml = '<div class="text-color-red font-weight-600">';
            $growthRateHtml.= '<span class="icon-rise-down margin-right-10 font-size-12"></span>';
            $growthRateHtml.= $getGrowthRate;
            $growthRateHtml.= '%</div>';
        } else if($getGrowthRate == 0){
            $growthRateHtml = '&nbsp;';
        } else {
            $growthRateHtml = '<div class="text-color-green font-weight-600">';
            $growthRateHtml.= '<span class="icon-rise-up margin-right-10 font-size-12"></span>+';
            $growthRateHtml.= $getGrowthRate;
            $growthRateHtml.= '%</div>';
        }

        return $growthRateHtml;
    }

   
    public function orders(Request $request)
    {
        //$restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        //$restDetails = Restaurant::select('*')->where(['restaurants.id' => $restArray[0]])->first();
        $role = $conditions = $restaurantId = $requestData = '';
        $role = \Auth::user()->role;
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantId = $restArray[0];
        if((Session::has('restaurant_id')) && ($role=="admin")){
            $restaurantId = Session::get('restaurant_id');
            if(\Request::has('restaurant_id')){
                $restaurantId = \Request::input('restaurant_id');
            }
            if($restaurantId=="All"){
                $requestData = $restaurantId;
                $restaurantId = $restArray[0];
            }
        }
        $restDetails = Restaurant::select('*')->where(['restaurants.id' => $restaurantId])->first();

        $menuItemsRes = $avgPerOrderOverviewResArray = $orderOverviewResArray = $revenueVsOrdersResArray = "";
        $deliveryVsTakeoutPieResArray = $deliveryVsTakeoutPieResArray = $getDashboardDaySort = $phoneCallOrdersOverviewResArray = $newVSReturnPieResArray = "";
        $htmlOnlineOrdersGrowthRate = $htmlRevenueGrowthRate = $currencySymbol = $orderRevenue = $date_interval = "";

        if(!is_null($restDetails->deal_id)) {
                $dealId = $restDetails->deal_id;
                $currencySymbol = $restDetails['currency_symbol'];
                if($request->has('dashboardDaySort') && $request->input('dashboardDaySort') != "") {
                    $getDashboardDaySort = $request->input('dashboardDaySort');
                    Session::put('sort_by', $getDashboardDaySort);
                } else {
                    $getDashboardDaySort = 30;
                }

                if (!$request->has('dashboardDaySort') && $request->input('dashboardDaySort') == "" && Session::has('sort_by')) {
                    $getDashboardDaySort = Session::get('sort_by');
                }
                $data_status = " and uo.status in ('archived','completed') ";
                $date_interval = " and uo.created_at BETWEEN DATE_SUB(NOW(), INTERVAL ".$getDashboardDaySort." DAY) AND NOW()";
            if($role=="admin" && !Session::has('restaurant_id') && $requestData!="All"){
                $conditions = 'restaurant_id ='.$restDetails->id.$date_interval;
                $revenueconditions ='restaurant_id ='.$restDetails->id.$data_status.$date_interval;
                $orderconditions = '1=1'.$date_interval;
                $cockatailconditions = '1=1'.$date_interval;
                $ageordercondition = ' and 1=1'.$date_interval;
                $agecocktailscondition = ' and 1=1'.$date_interval;
                $totalspirintcondition = ' 1=1'.$date_interval;
                $totalcoctailsordercondition = ' 1=1'.$date_interval;
                $cancelsordercondition = ' 1=1'.$date_interval;
            }
            else if($role=="admin" && Session::has('restaurant_id') && $requestData=="All"){
                $conditions = '1=1'.$date_interval;
                $revenueconditions ='1=1'.$data_status.$date_interval;
                $orderconditions = '1=1'.$date_interval;
                $cockatailconditions = '1=1'.$date_interval;
                $ageordercondition = ' and 1=1'.$date_interval;
                $agecocktailscondition = ' and 1=1'.$date_interval;
                $totalspirintcondition = ' 1=1'.$date_interval;
                $totalcoctailsordercondition = ' 1=1'.$date_interval;
                $cancelsordercondition = ' 1=1'.$date_interval;
            }
            else if($role=="manager"){
                $conditions = 'restaurant_id ='.$restDetails->id.$data_status.$date_interval;
                $revenueconditions ='restaurant_id ='.$restDetails->id.$data_status.$date_interval;
                $orderconditions = 'r.id ='.$restDetails->id.$date_interval;
                $cockatailconditions = 'uo.restaurant_id ='.$restDetails->id.$date_interval;
                $ageordercondition = ' and uo.restaurant_id ='.$restDetails->id.$date_interval;
                $agecocktailscondition = ' and uo.restaurant_id ='.$restDetails->id.$date_interval;
                $totalspirintcondition = ' uo.restaurant_id ='.$restDetails->id.$date_interval;
                $totalcoctailsordercondition = ' uo.restaurant_id ='.$restDetails->id.$date_interval;
                $cancelsordercondition = ' uo.restaurant_id ='.$restDetails->id.$date_interval;
            }
            else if($role=="admin" && Session::has('restaurant_id') && $requestData!="All"){
                $conditions = 'restaurant_id ='.$restDetails->id.$date_interval;
                $revenueconditions ='1=1'.$data_status.$date_interval;
                $orderconditions = 'r.id ='.$restDetails->id.$date_interval;
                $cockatailconditions = 'uo.restaurant_id ='.$restDetails->id.$date_interval;
                $ageordercondition = ' and uo.restaurant_id ='.$restDetails->id.$date_interval;
                $agecocktailscondition = ' and uo.restaurant_id ='.$restDetails->id.$date_interval;
                $totalspirintcondition = ' uo.restaurant_id ='.$restDetails->id.$date_interval;
                $totalcoctailsordercondition = ' uo.restaurant_id ='.$restDetails->id.$date_interval;
                $cancelsordercondition = ' uo.restaurant_id ='.$restDetails->id.$date_interval;
            }

            try {
                
                $orderOverview = "select count(uo.id) as orders_ct, round(sum(uo.total_amount),2) as order_revenue, round(round(sum(uo.total_amount),2)/ count(uo.id), 2) as order_avgvalue from user_orders uo where $conditions";

                $orderRevenue = "select round(sum(uo.total_amount),2) as orderrevenue from user_orders uo where $revenueconditions";

                $totalorderOverview = "select count(uo.id) as order_count, r.restaurant_name,round(sum(total_amount),2) as order_revenue FROM user_orders uo inner join restaurants r on uo.restaurant_id = r.id where $orderconditions GROUP BY r.id";

                $totalcoctailsOverview = "select SUM(uod.quantity) as cnt_qty, nmi.name as menu_name FROM user_order_details uod INNER JOIN user_orders uo on uo.id = uod.user_order_id inner join new_menu_items nmi on nmi.id = uod.menu_id where $cockatailconditions group by nmi.name";

                $totalageorderOverview = "select count(uo.id) as ord_count, year(curdate())-year(u.dob) count_age from users u inner JOIN user_orders uo ON u.id = uo.user_id where  year(curdate())-year(u.dob) > 17  $ageordercondition group by count_age";

                $totalagecocktailsOverview = "select nmi.name as menu_name, year(curdate())-year(u.dob) count_age, CONCAT('#',LPAD(CONV(ROUND(RAND()*16777215),10,16),6,0)) AS color_code from users u inner JOIN user_orders uo ON u.id = uo.user_id inner join user_order_details uod on uo.id = uod.user_order_id inner join new_menu_items nmi on nmi.id = uod.menu_id where  year(curdate())-year(u.dob) > 17  $agecocktailscondition group by nmi.name, count_age";

                $totalspirintOverview = "select count(uod.id) as spirint_cnt, TRIM(BOTH '\"' FROM JSON_EXTRACT(uod.menu_json, '$.modifier_items[0].modifier_name')) AS modifier FROM user_order_details uod inner JOIN user_orders uo on uo.id = uod.user_order_id where $totalspirintcondition and TRIM(BOTH '\"' FROM JSON_EXTRACT(uod.menu_json, '$.modifier_items[0].modifier_name'))<>'NULL' group by modifier" ;

                $totalcoctailsorderOverview = "select COUNT(uod.quantity) as cnt_quantity FROM user_order_details uod INNER JOIN user_orders uo on uo.id = uod.user_order_id 
                    where $totalcoctailsordercondition GROUP BY uo.parent_restaurant_id";
                
                $cancelledOrderOverview = "select r.restaurant_name, (select count(id) from user_orders where $cancelsordercondition and status in ('cancelled', 'rejected') and restaurant_id = uo.restaurant_id group by restaurant_id) as cancelled_ord, (select count(id) from user_orders where $cancelsordercondition and status = 'refunded' and restaurant_id = uo.restaurant_id group by restaurant_id) as refunded_ord FROM user_orders uo inner join restaurants r on uo.restaurant_id = r.id $date_interval group by r.id";
                
                

                $orderOverviewRes = DB::connection('mysql')->select($orderOverview);
                $orderRevenueRes = DB::connection('mysql')->select($orderRevenue);
                $totalorderOverviewRes = DB::connection('mysql')->select($totalorderOverview);
                $totalcoctailsOverviewRes = DB::connection('mysql')->select($totalcoctailsOverview);
                $totalageorderOverviewRes = DB::connection('mysql')->select($totalageorderOverview);
                $totalagecocktailsOverviewRes = DB::connection('mysql')->select($totalagecocktailsOverview);
                $totalspirintOverviewRes = DB::connection('mysql')->select($totalspirintOverview);
                $totalcoctailsorderOverviewRes = DB::connection('mysql')->select($totalcoctailsorderOverview);
                $cancelledOrderOverviewRes = DB::connection('mysql')->select($cancelledOrderOverview);
                

                
                $orderOverviewResArray = json_encode($orderOverviewRes);
                $orderRevenueResArray = json_encode($orderRevenueRes);
                $totalorderOverviewResArray = json_encode($totalorderOverviewRes);
                $totalcoctailsOverviewResArray = json_encode($totalcoctailsOverviewRes);
                $totalageorderOverviewResArray = json_encode($totalageorderOverviewRes);
                $totalagecocktailsOverviewResArray = json_encode($totalagecocktailsOverviewRes);
                $totalspirintOverviewResArray = json_encode($totalspirintOverviewRes);
                $totalcoctailsorderOverviewResArray = json_encode($totalcoctailsorderOverviewRes);
                $cancelledOrderOverviewResArray = json_encode($cancelledOrderOverviewRes);
                

            } catch (Exception $e){

            }
        }

        //dd('orders');
        return view('reports.orders', compact('menuItemsRes', 'avgPerOrderOverviewResArray', 'orderOverviewResArray', 'orderRevenueResArray', 'totalorderOverviewResArray',
            'totalcoctailsOverviewResArray', 'totalageorderOverviewResArray', 'totalagecocktailsOverviewResArray', 'totalspirintOverviewResArray', 'totalcoctailsorderOverviewResArray', 'cancelledOrderOverviewResArray',
            'phoneCallOrdersOverviewResArray', 'newVSReturnPieResArray','getDashboardDaySort',
            'htmlOnlineOrdersGrowthRate', 'htmlRevenueGrowthRate', 'currencySymbol','dealId'));
    }
    /*public function reservations(Request $request)
    {

        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restDetails = Restaurant::select('*')->where(['restaurants.id' => $restArray[0]])->first();

        $reservationsOverviewResArray = $reservationVsRevenueResArray = $reservationStatusResArray = "";
        $avgCoverPerReservationOverviewResArray = $reservationTimeSlotResArray = $getDashboardDaySort = $newVSReturningResvResArray = "";
        $phoneCallReservationOverviewResArray = $htmlOnlineReservationsGrowthRate = $htmlRevenueGrowthRate = $currencySymbol = "";

        if(!is_null($restDetails->deal_id)) {
            $dealId = $restDetails->deal_id; //"251235081"; //$restDetails->deal_id;
            try {

                $getDateFormatForAllDays = "'%b-%y'";
                $getAllData = "group by year(date),month(date) order by year(date),month(date)";
                $currencySymbol = $restDetails['currency_symbol'];

                if($request->has('dashboardDaySort') && $request->input('dashboardDaySort') != "") {
                    $getDashboardDaySort = $request->input('dashboardDaySort');

                    //Changes for 30 Days
                    if($getDashboardDaySort == "30"){
                        $getDateFormatForAllDays = "'%e-%b'";
                        $getAllData = "group by year(date),month(date),day(date) order by year(date),month(date),day(date)";
                    }

                } elseif($dealId=='100000001') {
                    $getDashboardDaySort = 30;
                }else{
                    $getDashboardDaySort = 2000;
                }

                $reservationsOverview = "select orders_ct , order_revenue , order_avgvalue from tb_db4_reservationsOverview_".$getDashboardDaySort." where deal_id = ".$dealId;//"select sum(transaction) as `orders_ct`, sum(revenue) as `order_revenue`, (sum(revenue)/sum(transaction) ) as `order_avgvalue` from transaction_revenue_dealID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() and type != 'ORDER'";
                $avgCoverPerReservationOverview = "select avg_cover from tb_db4_avg_cover_".$getDashboardDaySort." where deal_id = ".$dealId;//"select round(sum(party_size)/count(*),0) as avg_cover from reporting_dashboard.tb_reservation_customer_details_vw where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE()";
                $reservationVsRevenue = "select day , orders_ct , order_revenue from tb_db4_reservationVsRevenue_".$getDashboardDaySort." where deal_id = ".$dealId;//"select distinct  DATE_FORMAT(date, {$getDateFormatForAllDays}) AS `day`, sum(transaction) as `orders_ct`, sum(revenue) as `order_revenue` from transaction_revenue_dealID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() and type != 'ORDER' {$getAllData}";
                $reservationStatus = "select status , ct_status from tb_db4_reservationStatus_".$getDashboardDaySort." where deal_id = ".$dealId;//"select distinct status , count(status) as ct_status from tb_reservation_customer_details_vw where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() and total_amount <> 0 group by status order by status" ;
                $newVSReturningResv = "select distinct User_Type , count(User_Type ) as ct_usertype from reporting_dashboard.customer_reservation_type_{$getDashboardDaySort}_vw where deal_id = {$dealId} group by User_Type";
                $reservationTimeSlot = "select time_slot , ct_date from tb_db4_reservationTimeSlot_".$getDashboardDaySort." where deal_id = ".$dealId;//"select distinct DATE_FORMAT(DATE_ADD(date, INTERVAL 30 MINUTE),'%H:00') as `time_slot`, count(date) as ct_date from tb_reservation_customer_details_vw where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() and total_amount <> 0 group by DATE_FORMAT(DATE_ADD(date, INTERVAL 30 MINUTE),'%H:00') order by DATE_FORMAT(DATE_ADD(date, INTERVAL 30 MINUTE),'%H:00')";
                $phoneCallReservationOverview = "call  sp_PhoneCalls({$getDashboardDaySort},{$dealId})";

                $growthRateOnlineReservationsOverview = "call sp_growth_rate_type('transaction','transaction_revenue_dealID','date','type','RESERVATION',{$getDashboardDaySort},{$dealId})";
                $growthRateRevenueOverview = "call sp_growth_rate_type('revenue','transaction_revenue_dealID','date','type','RESERVATION',{$getDashboardDaySort},{$dealId})";

                //dd($reservationTimeSlot);

                $reservationsOverviewRes = DB::connection('mysql2')->select($reservationsOverview);
                $avgCoverPerReservationOverviewRes = DB::connection('mysql2')->select($avgCoverPerReservationOverview);
                $phoneCallReservationOverviewRes = DB::connection('mysql2')->select($phoneCallReservationOverview);
                $reservationVsRevenueRes = DB::connection('mysql2')->select($reservationVsRevenue);
                $reservationStatusRes = DB::connection('mysql2')->select($reservationStatus);
                $newVSReturningResvRes = DB::connection('mysql2')->select($newVSReturningResv);
                $reservationTimeSlotRes = DB::connection('mysql2')->select($reservationTimeSlot);

                $growthRateOnlineReservationsOverviewRes = DB::connection('mysql2')->select($growthRateOnlineReservationsOverview);
                $growthRateRevenueOverviewRes = DB::connection('mysql2')->select($growthRateRevenueOverview);

                $reservationsOverviewResArray = json_encode($reservationsOverviewRes);
                $avgCoverPerReservationOverviewResArray = json_encode($avgCoverPerReservationOverviewRes);
                $phoneCallReservationOverviewResArray = json_encode($phoneCallReservationOverviewRes);
                $reservationVsRevenueResArray = json_encode($reservationVsRevenueRes);
                $reservationStatusResArray = json_encode($reservationStatusRes);
                $newVSReturningResvResArray = json_encode($newVSReturningResvRes);
                $reservationTimeSlotResArray = json_encode($reservationTimeSlotRes);
                $growthRateOnlineReservationsOverviewResArray = json_encode($growthRateOnlineReservationsOverviewRes);
                $growthRateRevenueOverviewResArray = json_encode($growthRateRevenueOverviewRes);

                $getOnlineReservationsGrowthRateRes = isset(json_decode($growthRateOnlineReservationsOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateOnlineReservationsOverviewResArray, true)[0]['@growth']),0) : 0;
                $getRevenueGrowthRateRes = isset(json_decode($growthRateRevenueOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateRevenueOverviewResArray, true)[0]['@growth']),0) : 0;

                $htmlOnlineReservationsGrowthRate = self::getHtmlOfGrowthRate($getOnlineReservationsGrowthRateRes);
                $htmlRevenueGrowthRate = self::getHtmlOfGrowthRate($getRevenueGrowthRateRes);

                //dd($reservationStatusResArray);

            }catch (Exception $e){

            }
        }

        //dd('reservations');
        return view('reports.reservations', compact('reservationsOverviewResArray', 'reservationVsRevenueResArray', 'reservationStatusResArray',
            'avgCoverPerReservationOverviewResArray', 'reservationTimeSlotResArray', 'getDashboardDaySort', 'newVSReturningResvResArray', 'phoneCallReservationOverviewResArray',
            'htmlOnlineReservationsGrowthRate', 'htmlRevenueGrowthRate', 'currencySymbol','dealId'));
    }
    public function visitors(Request $request)
    {
        //$restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        //$restDetails = Restaurant::select('*')->where(['restaurants.id' => $restArray[0]])->first();
        $role = $conditions = $restaurantId = $requestData = '';
        $role = \Auth::user()->role;
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantId = $restArray[0];
        if((Session::has('restaurant_id')) && ($role=="admin")){
            $restaurantId = Session::get('restaurant_id');
            if(\Request::has('restaurant_id')){
                $restaurantId = \Request::input('restaurant_id');
            }
            if($restaurantId=="All"){
                $requestData = $restaurantId;
                $restaurantId = $restArray[0];
            }
        }
        $restDetails = Restaurant::select('*')->where(['restaurants.id' => $restaurantId])->first();
        $siteVisitorsOverviewResArray = $newVisitorsOverviewResArray = $returnVisitorsOverviewResArray = "";
        $trafficChannelPieResArray = $trafficTypePieResArray = $trafficDevicePieResArray = $getDashboardDaySort = $avgTimeSpentOverviewResArray = "";
        $customerJourneyFunnelResArray = $userPerSessionOverviewResArray = $newVisitorChartResArray = $oldVisitorChartResArray = $htmlVisitorsGrowthRate = "";
        $htmlNewVisitorsGrowthRate = $htmlReturnVisitorsGrowthRate = $currencySymbol = "";
        $restaurantCondition = '';
        if(!is_null($restDetails->deal_id)) {
            $dealId = $restDetails->deal_id; //"251235081"; //$restDetails->deal_id;
            $clientId = $restDetails->client_id;
            if($role=="admin" && !Session::has('restaurant_id') && $requestData!="All"){
                $conditions = 'client_id ='.$restDetails->client_id;
            }
            else if($role=="admin" && Session::has('restaurant_id') && $requestData=="All"){
                $conditions = 'client_id ='.$restDetails->client_id;
            }
            else if($role=="manager"){
                $conditions = 'client_id ='.$restDetails->client_id.' and deal_id ='.$restDetails->deal_id;
                $restaurantCondition = "and rest_id in ($restaurantId,'NA')";
            }
            else if($role=="admin" && Session::has('restaurant_id') && $requestData!="All"){
                $conditions = 'client_id ='.$restDetails->client_id.' and deal_id ='.$restDetails->deal_id;
                $restaurantCondition = "and rest_id in ($restaurantId,'NA')";
            }
            $flag = '';
            $checkFlag = "select multi_site from test_new.GA_BASE_DTLS_DEALID where $conditions limit 1;";
            $dataArray = $siteVisitorsOverviewRes = DB::connection('mysql2')->select($checkFlag);

            if(!empty($dataArray)){
                $flag = $dataArray['0']->multi_site;
                if ($flag == 1) {
                    $conditions = 'client_id =' . $restDetails->client_id . ' and deal_id =' . $restDetails->deal_id;
                } else if ($flag == 0){
                    $conditions = 'client_id =' . $restDetails->client_id;
                }
            }else {
                $conditions = 'client_id =' . $restDetails->client_id;
            }

            try {

                $getDateFormatForAllDays = "'%b-%y'";
                $getAllData = "group by year(date),month(date) order by year(date),month(date)";
                $currencySymbol = $restDetails['currency_symbol'];

                if($request->has('dashboardDaySort') && $request->input('dashboardDaySort') != "") {
                    $getDashboardDaySort = $request->input('dashboardDaySort');
                    Session::put('sort_by', $getDashboardDaySort);
                    //Changes for 30 Days
                    if($getDashboardDaySort === 30){
                        $getDateFormatForAllDays = "'%e-%b'";
                        $getAllData = "group by year(date),month(date),day(date) order by year(date),month(date),day(date)";
                    }

                } elseif($dealId=='100000001') {
                    $getDashboardDaySort = 30;
                }else{
                    $getDashboardDaySort = 30;
                }

                if (!$request->has('dashboardDaySort') && $request->input('dashboardDaySort') == "" && Session::has('sort_by')) {
                    $getDashboardDaySort = Session::get('sort_by');
                }

                $siteVisitorsOverview = "select  sum(Visitors) as Visitors  from test_new.tb_db1_overview_visitors_".$getDashboardDaySort." where $conditions;";

                //$newVisitorsOverview = "select sum(new_visitors) as new_visitors from tb_db5_typeVisitorOverview_2000 where userType = 'New Visitor' and $conditions;";
                $newVisitorsOverview = "select sum(new_visitors) as new_visitors from tb_db5_typeVisitorOverview_".$getDashboardDaySort." where userType = 'New Visitor' and $conditions;";

                //$returnVisitorsOverview = "select * from tb_db5_typeVisitorOverview_".$getDashboardDaySort." where userType = 'Returning Visitor' and deal_id = ".$dealId;//"select sum(users) as `new_visitors` from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and userType = 'Returning Visitor' and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE()";
                //$returnVisitorsOverview = "select * from tb_db5_typeVisitorOverview_".$getDashboardDaySort." where userType = 'Returning Visitor' and client_id = ".$clientId;//"select sum(users) as `new_visitors` from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and userType = 'Returning Visitor' and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE()";
                $returnVisitorsOverview = "select sum(new_visitors) as new_visitors from tb_db5_typeVisitorOverview_".$getDashboardDaySort." where userType = 'Returning Visitor' and $conditions;";

                //$avgTimeSpentOverview = "select avgtimespent from tb_db1_overview_visitors_".$getDashboardDaySort." where deal_id = ".$dealId;//"select SEC_TO_TIME(round(sum(sessionDuration)/sum(sessions),0)) as avgtimespent from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE()";
                //$avgTimeSpentOverview = "select avgtimespent from tb_db1_overview_visitors_".$getDashboardDaySort." where client_id = ".$clientId;//"select SEC_TO_TIME(round(sum(sessionDuration)/sum(sessions),0)) as avgtimespent from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE()";
                $avgTimeSpentOverview = "select avgtimespent from tb_db1_overview_visitors_".$getDashboardDaySort." where $conditions;";

                //$userPerSessionOverview = "select userpersession from tb_db1_overview_visitors_".$getDashboardDaySort." where deal_id = ".$dealId;//"select (round(sum(sessions)/sum(users),2)) as userpersession from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE();";
                //$userPerSessionOverview = "select userpersession from tb_db1_overview_visitors_".$getDashboardDaySort." where client_id = ".$clientId;//"select (round(sum(sessions)/sum(users),2)) as userpersession from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE();";
                $userPerSessionOverview = "select userpersession from tb_db1_overview_visitors_".$getDashboardDaySort." where $conditions;";

                //$newVisitorChartQuery = "select day , NV from tb_db5_Visitor_Type_".$getDashboardDaySort." where userType = 'New Visitor' and deal_id = ".$dealId;//"select distinct deal_id , DATE_FORMAT(date, {$getDateFormatForAllDays}) AS `day` , sum(users) as NV from  GA_BASE_DTLS_DEALID where deal_id = {$dealId} and userType = 'New Visitor' and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() {$getAllData}";
                //$newVisitorChartQuery = "select day , NV from tb_db5_Visitor_Type_".$getDashboardDaySort." where userType = 'New Visitor' and client_id = ".$clientId;//"select distinct deal_id , DATE_FORMAT(date, {$getDateFormatForAllDays}) AS `day` , sum(users) as NV from  GA_BASE_DTLS_DEALID where deal_id = {$dealId} and userType = 'New Visitor' and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() {$getAllData}";
                $newVisitorChartQuery = "select distinct day , sum(NV) as NV from tb_db5_Visitor_Type_".$getDashboardDaySort." where userType = 'New Visitor' and $conditions group by client_id , day order by date asc;";

                //$oldVisitorChartQuery = "select day , NV from tb_db5_Visitor_Type_".$getDashboardDaySort." where userType = 'Returning Visitor' and deal_id = ".$dealId;//"select distinct deal_id , DATE_FORMAT(date, {$getDateFormatForAllDays}) AS `day` , sum(users) as NV from  GA_BASE_DTLS_DEALID where deal_id = {$dealId} and userType != 'New Visitor' and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() {$getAllData}";
                //$oldVisitorChartQuery = "select day , NV from tb_db5_Visitor_Type_".$getDashboardDaySort." where userType = 'Returning Visitor' and client_id = ".$clientId;//"select distinct deal_id , DATE_FORMAT(date, {$getDateFormatForAllDays}) AS `day` , sum(users) as NV from  GA_BASE_DTLS_DEALID where deal_id = {$dealId} and userType != 'New Visitor' and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() {$getAllData}";
                $oldVisitorChartQuery = "select distinct day , sum(NV) as NV from tb_db5_Visitor_Type_".$getDashboardDaySort." where userType = 'Returning Visitor' and $conditions group by client_id , day order by date asc;";

                //$trafficChannelPieQery = "select channel , num_visitors from tb_db5_channel_".$getDashboardDaySort." where deal_id = ".$dealId;//"select distinct channel , sum(sessions) as `num_visitors` from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by channel order by sum(users) desc";
                //$trafficChannelPieQery = "select channel , num_visitors from tb_db5_channel_".$getDashboardDaySort." where client_id = ".$clientId;//"select distinct channel , sum(sessions) as `num_visitors` from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by channel order by sum(users) desc";
                $trafficChannelPieQery = "select distinct channel , sum(num_visitors) as num_visitors from tb_db5_channel_".$getDashboardDaySort." where $conditions group by channel;";

                //$
                // = "select userType , num_visitors from tb_db5_userType_".$getDashboardDaySort." where deal_id = ".$dealId;//"select  distinct userType  , sum(users) as `num_visitors` from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by userType";
                //$trafficTypePieQery = "select userType , num_visitors from tb_db5_userType_".$getDashboardDaySort." where client_id = ".$clientId;//"select  distinct userType  , sum(users) as `num_visitors` from GA_BASE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by userType";
                $trafficTypePieQery = "select distinct userType , sum(num_visitors) as num_visitors from tb_db5_userType_".$getDashboardDaySort." where $conditions group by userType;";

                //$customerJourneyFunnelQery = "select Label , clicks_num from tb_db5_customerJourneyFunnel_".$getDashboardDaySort." where deal_id = ".$dealId;//"select distinct Label , sum(clicks_num) as `clicks_num` from tb_customer_funnel_vw where deal_id = {$dealId} and day BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by Label ORDER BY sum(clicks_num) desc";
                //$customerJourneyFunnelQery = "select distinct Label , sum(clicks_num) as clicks_num from tb_db5_customerJourneyFunnel_".$getDashboardDaySort." where client_id =".$clientId." and rest_id in ('".$restArray[0]."','NA') group by Label";
                $customerJourneyFunnelQery = "select distinct Label , sum(clicks_num) as clicks_num from tb_db5_customerJourneyFunnel_".$getDashboardDaySort." where client_id =".$clientId." ".$restaurantCondition." group by  Label;";

                //$trafficDevicePieQery = "select deviceCategory , num_visitors from tb_db5_deviceCategory_".$getDashboardDaySort." where deal_id = ".$dealId;//"select  distinct deviceCategory  , sum(sessions) as `num_visitors` from GA_DEVICE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by deviceCategory ";
                //$trafficDevicePieQery = "select deviceCategory , num_visitors from tb_db5_deviceCategory_".$getDashboardDaySort." where client_id = ".$clientId;//"select  distinct deviceCategory  , sum(sessions) as `num_visitors` from GA_DEVICE_DTLS_DEALID where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by deviceCategory ";
                $trafficDevicePieQery = "select distinct deviceCategory , sum(num_visitors) as num_visitors from tb_db5_deviceCategory_".$getDashboardDaySort." where $conditions group by deviceCategory;";

                $growthRateVisitorsOverview = "call  sp_growth_rate('users','GA_BASE_DTLS_DEALID','date',{$getDashboardDaySort},{$dealId})";
                $growthRateNewVisitorsOverview = "call sp_growth_rate_type('users','GA_BASE_DTLS_DEALID','date','userType','New Visitor',{$getDashboardDaySort},{$dealId})";
                $growthRateReturnVisitorsOverview = "call sp_growth_rate_type('users','GA_BASE_DTLS_DEALID','date','userType','Returning Visitor',{$getDashboardDaySort},{$dealId})";

                $siteVisitorsOverviewRes = DB::connection('mysql2')->select($siteVisitorsOverview);
                $newVisitorsOverviewRes = DB::connection('mysql2')->select($newVisitorsOverview);
                $returnVisitorsOverviewRes = DB::connection('mysql2')->select($returnVisitorsOverview);
                $avgTimeSpentOverviewRes = DB::connection('mysql2')->select($avgTimeSpentOverview);
                $userPerSessionOverviewRes = DB::connection('mysql2')->select($userPerSessionOverview);
                $newVisitorChartRes = DB::connection('mysql2')->select($newVisitorChartQuery);
                $oldVisitorChartRes = DB::connection('mysql2')->select($oldVisitorChartQuery);
                $trafficChannelPieRes = DB::connection('mysql2')->select($trafficChannelPieQery);
                $trafficTypePieRes = DB::connection('mysql2')->select($trafficTypePieQery);
                $trafficDevicePieRes = DB::connection('mysql2')->select($trafficDevicePieQery);
                $customerJourneyFunnelRes = DB::connection('mysql2')->select($customerJourneyFunnelQery);
                $growthRateVisitorsOverviewRes = DB::connection('mysql2')->select($growthRateVisitorsOverview);
                $growthRateNewVisitorsOverviewRes = DB::connection('mysql2')->select($growthRateNewVisitorsOverview);
                $growthRateReturnVisitorsOverviewRes = DB::connection('mysql2')->select($growthRateReturnVisitorsOverview);

                $siteVisitorsOverviewResArray = json_encode($siteVisitorsOverviewRes);
                $newVisitorsOverviewResArray = json_encode($newVisitorsOverviewRes);
                $returnVisitorsOverviewResArray = json_encode($returnVisitorsOverviewRes);
                $avgTimeSpentOverviewResArray = json_encode($avgTimeSpentOverviewRes);
                $userPerSessionOverviewResArray = json_encode($userPerSessionOverviewRes);
                $newVisitorChartResArray = json_encode($newVisitorChartRes);
                $oldVisitorChartResArray = json_encode($oldVisitorChartRes);
                $trafficChannelPieResArray = json_encode($trafficChannelPieRes);
                $trafficTypePieResArray = json_encode($trafficTypePieRes);
                $trafficDevicePieResArray = json_encode($trafficDevicePieRes);
                $customerJourneyFunnelResArray = json_encode($customerJourneyFunnelRes);
                $growthRateVisitorsOverviewResArray = json_encode($growthRateVisitorsOverviewRes);
                $growthRateNewVisitorsOverviewResArray = json_encode($growthRateNewVisitorsOverviewRes);
                $growthRateReturnVisitorsOverviewResArray = json_encode($growthRateReturnVisitorsOverviewRes);

                $getVisitorsGrowthRateRes = isset(json_decode($growthRateVisitorsOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateVisitorsOverviewResArray, true)[0]['@growth']),0) : 0;
                $getNewVisitorsGrowthRateRes = isset(json_decode($growthRateNewVisitorsOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateNewVisitorsOverviewResArray, true)[0]['@growth']),0) : 0;
                $getReturnVisitorsGrowthRateRes = isset(json_decode($growthRateReturnVisitorsOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateReturnVisitorsOverviewResArray, true)[0]['@growth']),0) : 0;

                $htmlVisitorsGrowthRate = self::getHtmlOfGrowthRate($getVisitorsGrowthRateRes);
                $htmlNewVisitorsGrowthRate = self::getHtmlOfGrowthRate($getNewVisitorsGrowthRateRes);
                $htmlReturnVisitorsGrowthRate = self::getHtmlOfGrowthRate($getReturnVisitorsGrowthRateRes);

                //dd($growthRateNewVisitorsOverviewResArray);

            }catch (Exception $e){

            }
        }

        //dd('visitors');
        return view('reports.visitors', compact('siteVisitorsOverviewResArray', 'newVisitorsOverviewResArray', 'returnVisitorsOverviewResArray',
            'trafficChannelPieResArray', 'trafficTypePieResArray', 'trafficDevicePieResArray', 'getDashboardDaySort', 'avgTimeSpentOverviewResArray',
            'customerJourneyFunnelResArray', 'userPerSessionOverviewResArray', 'newVisitorChartResArray', 'oldVisitorChartResArray', 'htmlVisitorsGrowthRate',
            'htmlNewVisitorsGrowthRate','htmlReturnVisitorsGrowthRate', 'currencySymbol','dealId'));
    }
    public function marketing(Request $request)
    {
        //$restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        //$restDetails = Restaurant::select('*')->where(['restaurants.id' => $restArray[0]])->first();
        $role = $conditions = $restaurantId = $requestData = '';
        $role = \Auth::user()->role;
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantId = $restArray[0];
        if((Session::has('restaurant_id')) && ($role=="admin")){
            $restaurantId = Session::get('restaurant_id');
            if(\Request::has('restaurant_id')){
                $restaurantId = \Request::input('restaurant_id');
            }
            if($restaurantId=="All"){
                $requestData = $restaurantId;
                $restaurantId = $restArray[0];
            }
        }
        $restDetails = Restaurant::select('*')->where(['restaurants.id' => $restaurantId])->first();
        $marketingOverviewResArray = $getDashboardDaySort = $impConversionsAnalysisResArray = $spendAvgSpendResArray =  $campaignReportRes = "";
        $newVisitorsVSRemarkingResArray = $brandingVSConversionResArray = $phoneCallOverviewResArray = $desktopVSDeviceResArray = $htmlSpendGrowthRate = "";
        $htmlViewsGrowthRate = $htmlClicksGrowthRate = $htmlConversionsGrowthRate = $currencySymbol = "";
        $currencySymbol = $restDetails['currency_symbol'];
        $currencyRate = $restDetails['currency_rate'];
        if(!is_null($restDetails->deal_id)) {
            $dealId = $restDetails->deal_id; //"251235081"; //"249901889"; //$restDetails->deal_id;
            if($role=="admin" && !Session::has('restaurant_id') && $requestData!="All"){
                $conditions = 'client_id ='.$restDetails->client_id;
            }
            else if($role=="admin" && Session::has('restaurant_id') && $requestData=="All"){
                $conditions = 'client_id ='.$restDetails->client_id;
            }
            else if($role=="manager"){
                $conditions = 'client_id ='.$restDetails->client_id.' and deal_id ='.$restDetails->deal_id;
            }
            else if($role=="admin" && Session::has('restaurant_id') && $requestData!="All"){
                $conditions = 'client_id ='.$restDetails->client_id.' and deal_id ='.$restDetails->deal_id;
            }

            try {

                $getDateFormatForAllDays = "'%b-%y'";
                $getAllData = "group by year(day),month(day) order by year(day),month(day)";
                

                if ($request->has('dashboardDaySort') && $request->input('dashboardDaySort') != "") {
                    $getDashboardDaySort = $request->input('dashboardDaySort');
                    Session::put('sort_by', $getDashboardDaySort);
                    //Changes for 30 Days
                    if ($getDashboardDaySort == "30") {
                        $getDateFormatForAllDays = "'%e-%b'";
                        $getAllData = "group by year(day),month(day),day(day) order by year(day),month(day),day(day)";
                    }

                } elseif($dealId=='100000001') {
                    $getDashboardDaySort = 30;
                     $getDateFormatForAllDays = "'%e-%b'";
                     $getAllData = "group by year(day),month(day),day(day) order by year(day),month(day),day(day)";                     
                }else{
                    $getDashboardDaySort = 30;
                }

                if (!$request->has('dashboardDaySort') && $request->input('dashboardDaySort') == "" && Session::has('sort_by')) {
                    $getDashboardDaySort = Session::get('sort_by');
                }

                $marketingOverview = "select sum(spend) as spend,sum(views) as views,sum(clicks) as clicks,sum(Conversions) as transactions,sum(revenue) as revenue from tb_db1_overview_".$getDashboardDaySort." where $conditions group by client_id;";

                //$marketingPhoneOverview = "select sum(conversions) as `PhoneCalls` from reporting_dashboard.adword_data_custom_dealid where ClickType = 'Phone calls' and deal_id = {$dealId} and Day BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE()";
                $phoneCallOverview = "call  sp_PhoneCalls({$getDashboardDaySort},{$dealId})";

                //$impConversionsAnalysisQuery = "select day , Conversions , reach as Impressions from tb_db1_rev_reach_".$getDashboardDaySort." where deal_id = ".$dealId;//"SELECT DISTINCT DATE_FORMAT(`day`, {$getDateFormatForAllDays}) AS `day`,SUM(`orders`) AS `Conversions`,sum(impressions) AS `Impressions` FROM `monthly_new_reports` where deal_id = {$dealId} and day BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() {$getAllData}";
                $impConversionsAnalysisQuery = "select  distinct(day) as day , sum(Conversions) as Conversions, sum(reach) as Impressions from tb_db1_rev_reach_".$getDashboardDaySort." where $conditions group by client_id , day order by date asc;";

                //$spendAvgSpendQuery = "select day , round(Spend,2) as Spend , AvgSpend from tb_db1_rev_reach_".$getDashboardDaySort." where deal_id = ".$dealId;
                $spendAvgSpendQuery = "select distinct day , round(sum(Spend),2) as Spend , sum(AvgSpend) as AvgSpend from tb_db1_rev_reach_".$getDashboardDaySort." where $conditions group by client_id , day order by date asc;";

                //$campaignReportQuery = "select Campaing , channel , start_date , clicks , impressions from tb_db6_campaignReport_".$getDashboardDaySort." where deal_id =".$dealId;
                $campaignReportQuery = "select distinct Campaing , channel , min(start_date) as start_date , sum(clicks) as clicks , sum(impressions) as impressions from tb_db6_campaignReport_".$getDashboardDaySort." where $conditions group by Campaing, channel";

                //$newVisitorsVSRemarkingQuery = "select VisitorType , ct_count from tb_db6_VisitorsVSRemarking_".$getDashboardDaySort."  where deal_id = ".$dealId;//"select distinct VisitorType , count(VisitorType) as ct_count from reporting_dashboard.campaign_dealid_vw where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() and CampaingFormat <> 'NA' group by VisitorType;";
                $newVisitorsVSRemarkingQuery = "select distinct VisitorType , sum(ct_count) as ct_count  from tb_db6_VisitorsVSRemarking_".$getDashboardDaySort." where $conditions group by VisitorType;";

                //$brandingVSConversionQuery = "select CampaignType , ct_CampaingType from tb_db6_brandingVSConversion_".$getDashboardDaySort."  where deal_id = ".$dealId;//"select distinct CampaignType , count(distinct CampaingFormat)  as ct_CampaingType from reporting_dashboard.campaign_dealid_vw where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() and CampaignType  in ('Branding','Conversion') group by CampaignType";
                $brandingVSConversionQuery = "select distinct CampaignType , ct_CampaingType  from tb_db6_brandingVSConversion_".$getDashboardDaySort." where $conditions group by  CampaignType;";

                $desktopVSDeviceQuery = "select DeviceType , Impressions from tb_db6_desktopVSDevice_".$getDashboardDaySort." where deal_id = ".$dealId;//"select case when Device = 'Computers' then 'Desktop' when Device = 'Mobile devices with full browsers' then 'Mobile' when Device = 'Tablets with full browsers' then 'Tablet' else 'Others' end as DeviceType ,sum(Impressions) as Impressions from reporting_dashboard.adword_data_custom_dealid where deal_id = {$dealId} and Day BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by DeviceType order by sum(Impressions) desc";
                $growthRateSpendOverview = "call  sp_growth_rate('spend','monthly_new_reports','day',{$getDashboardDaySort},{$dealId})";
                $growthRateConversionsOverview = "call  sp_growth_rate('orders','monthly_new_reports','day',{$getDashboardDaySort},{$dealId})";
                $growthRateViewsOverview = "call  sp_growth_rate('impressions','monthly_new_reports','day',{$getDashboardDaySort},{$dealId})";
                $growthRateClicksOverview = "call  sp_growth_rate('clicks','monthly_new_reports','day',{$getDashboardDaySort},{$dealId})";

                $growthRateSpendOverviewRes = DB::connection('mysql2')->select($growthRateSpendOverview);
                $growthRateConversionsOverviewRes = DB::connection('mysql2')->select($growthRateConversionsOverview);
                $growthRateViewsOverviewRes = DB::connection('mysql2')->select($growthRateViewsOverview);
                $growthRateClicksOverviewRes = DB::connection('mysql2')->select($growthRateClicksOverview);

                $marketingOverviewRes = DB::connection('mysql2')->select($marketingOverview);
                $phoneCallOverviewRes = DB::connection('mysql2')->select($phoneCallOverview);
                $impConversionsAnalysisRes = DB::connection('mysql2')->select($impConversionsAnalysisQuery);
                $spendAvgSpendRes = DB::connection('mysql2')->select($spendAvgSpendQuery);
                $campaignReportRes = DB::connection('mysql2')->select($campaignReportQuery);
                $newVisitorsVSRemarkingRes = DB::connection('mysql2')->select($newVisitorsVSRemarkingQuery);
                $brandingVSConversionRes = DB::connection('mysql2')->select($brandingVSConversionQuery);
                $desktopVSDeviceRes = DB::connection('mysql2')->select($desktopVSDeviceQuery);


                $marketingOverviewResArray = json_encode($marketingOverviewRes);
                $phoneCallOverviewResArray = json_encode($phoneCallOverviewRes);
                $impConversionsAnalysisResArray = json_encode($impConversionsAnalysisRes);
                $spendAvgSpendResArray = json_encode($spendAvgSpendRes);
                //$campaignReportResArray = json_encode($campaignReportRes);
                $newVisitorsVSRemarkingResArray = json_encode($newVisitorsVSRemarkingRes);
                $brandingVSConversionResArray = json_encode($brandingVSConversionRes);
                $desktopVSDeviceResArray = json_encode($desktopVSDeviceRes);

                $growthRateSpendOverviewResArray = json_encode($growthRateSpendOverviewRes);
                $growthRateConversionsOverviewResArray = json_encode($growthRateConversionsOverviewRes);
                $growthRateViewsOverviewResArray = json_encode($growthRateViewsOverviewRes);
                $growthRateClicksOverviewResArray = json_encode($growthRateClicksOverviewRes);

                $getSpendGrowthRateRes = isset(json_decode($growthRateSpendOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateSpendOverviewResArray, true)[0]['@growth']), 0) : 0;
                $getViewsGrowthRateRes = isset(json_decode($growthRateViewsOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateViewsOverviewResArray, true)[0]['@growth']), 0) : 0;
                $getClicksGrowthRateRes = isset(json_decode($growthRateClicksOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateClicksOverviewResArray, true)[0]['@growth']), 0) : 0;
                $getConversionsGrowthRateRes = isset(json_decode($growthRateConversionsOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateConversionsOverviewResArray, true)[0]['@growth']), 0) : 0;

                $htmlSpendGrowthRate = self::getHtmlOfGrowthRate($getSpendGrowthRateRes);
                $htmlViewsGrowthRate = self::getHtmlOfGrowthRate($getViewsGrowthRateRes);
                $htmlClicksGrowthRate = self::getHtmlOfGrowthRate($getClicksGrowthRateRes);
                $htmlConversionsGrowthRate = self::getHtmlOfGrowthRate($getConversionsGrowthRateRes);


                //dd($phoneCallOverviewResArray);

            } catch (Exception $e) {

            }
        }
        return view('reports.marketing', compact('marketingOverviewResArray', 'getDashboardDaySort', 'impConversionsAnalysisResArray',
            'spendAvgSpendResArray', 'campaignReportRes', 'newVisitorsVSRemarkingResArray', 'brandingVSConversionResArray', 'phoneCallOverviewResArray', 'desktopVSDeviceResArray', 'htmlSpendGrowthRate',
            'htmlViewsGrowthRate', 'htmlClicksGrowthRate', 'htmlConversionsGrowthRate', 'currencySymbol','currencyRate','dealId'));
    }
    
    public function social(Request $request)
    {
        $role = $conditions = $restaurantId = $requestData = '';
        $role = \Auth::user()->role;
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantId = $restArray[0];
        if((Session::has('restaurant_id')) && ($role=="admin")){
            $restaurantId = Session::get('restaurant_id');
            if(\Request::has('restaurant_id')){
                $restaurantId = \Request::input('restaurant_id');
            }
            if($restaurantId=="All"){
                $requestData = $restaurantId;
                $restaurantId = $restArray[0];
            }
        }
        $restDetails = Restaurant::select('*')->where(['restaurants.id' => $restaurantId])->first();
        $getDashboardDaySort = $socialMediaOverviewResArray = $likeTrendsOverviewResArray = $currencySymbol = "";
        $currencySymbol = $restDetails['currency_symbol'];
        $currencyRate = $restDetails['currency_rate'];
       
        if(!is_null($restDetails->deal_id)) {
            $dealId = $restDetails->deal_id; //"251235081"; //"249901889"; //$restDetails->deal_id;
            //$conditions = CommonFunctions::getQueryCondition($restDetails, $requestData, $role);
            if($role=="admin" && !Session::has('restaurant_id') && $requestData!="All"){
                //echo 'ad';
                $conditions = 'client_id ='.$restDetails->client_id;
            }
            else if($role=="admin" && Session::has('restaurant_id') && $requestData=="All"){
                //echo 'adAllll';
                $conditions = 'client_id ='.$restDetails->client_id;
            }
            else if($role=="manager"){
                //echo 'man';
                $conditions = 'client_id ='.$restDetails->client_id.' and deal_id ='.$restDetails->deal_id;
            }
            else if($role=="admin" && Session::has('restaurant_id') && $requestData!="All"){
                //echo 'ad-loc';
                $conditions = 'client_id ='.$restDetails->client_id.' and deal_id ='.$restDetails->deal_id;
            }

            try {

                $getDateFormatForAllDays = "'%b-%y'";
                $getAllData = "group by year(date_start),month(date_start) order by year(date_start),month(date_start)";
                

                if ($request->has('dashboardDaySort') && $request->input('dashboardDaySort') != "") {
                    $getDashboardDaySort = $request->input('dashboardDaySort');
                    Session::put('sort_by', $getDashboardDaySort);
                    //Changes for 30 Days
                    if ($getDashboardDaySort == "30") {
                        $getDateFormatForAllDays = "'%e-%b'";
                        $getAllData = "group by year(date_start),month(date_start),day(date_start) order by year(date_start),month(date_start),day(date_start)";
                    }

                } elseif($dealId=='100000001') {
                    $getDashboardDaySort = 30;
                    $getDateFormatForAllDays = "'%e-%b'";
                    $getAllData = "group by year(date_start),month(date_start),day(date_start) order by year(date_start),month(date_start),day(date_start)";
                    
                }else{
                    $getDashboardDaySort = 30;
                }
                if (!$request->has('dashboardDaySort') && $request->input('dashboardDaySort') == "" && Session::has('sort_by')) {
                  $getDashboardDaySort = Session::get('sort_by');
                }
                //echo $getDashboardDaySort; die;
                 //$socialMediaOverview = "select sum(reach) as `reach`, sum(page_like) as `like`, sum(impressions) as `impressions`, sum(clicks) as `clicks`, sum(spend) as `spend` from facebook_campaign_data_dealid where date_start BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() and deal_id = {$dealId}";
                 $socialMediaOverview = "select  sum(reach) as reach , sum(`like`) as `like` , sum(impressions) as impressions, sum(clicks) as clicks, sum(spend) as spend from tb_db7_facebookOverview_".$getDashboardDaySort." where $conditions";
                //$likeTrendsOverview = "select distinct DATE_FORMAT(date_start, {$getDateFormatForAllDays}) AS `day`,sum(page_like) as page_like from facebook_campaign_data_dealid where deal_id = {$dealId} and date_start BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() {$getAllData}";
                $likeTrendsOverview = "select distinct day , sum(page_like) as page_like from tb_db7_pagelike_trend_".$getDashboardDaySort." where $conditions group by day order by date_start asc";

                $socialMediaOverviewRes = DB::connection('mysql2')->select($socialMediaOverview);
                $likeTrendsOverviewRes = DB::connection('mysql2')->select($likeTrendsOverview);

                $socialMediaOverviewResArray = json_encode($socialMediaOverviewRes);
                $likeTrendsOverviewResArray = json_encode($likeTrendsOverviewRes);
             
                //dd($likeTrendsOverviewResArray);


            } catch (Exception $e) {

            }

        }


        //dd('social');
        return view('reports.social', compact('getDashboardDaySort','socialMediaOverviewResArray','likeTrendsOverviewResArray', 'currencySymbol','currencyRate','dealId'));
    }*/

}