<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
 	
    /**
     * User listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::orderBy('id', 'DESC')->active()->paginate(6);

        return view('user.index', compact('users'));
    }

    /**
     * Create user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();
        return view('user.create', compact('restaurantData'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'restaurant_id' => 'required|exists:restaurants,id',
            'fname'         => 'required|max:255',
            'lname'         => 'required|max:255',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|min:8|max:64|confirmed',
            'status'        => 'required|in:0,1',
        ]);
        if (!$validator->fails()) {
            $data = [
                'restaurant_id' => $request->input('restaurant_id'),
                'fname'         => strtolower($request->input('fname')),
                'lname'         => strtolower($request->input('lname')),
                'email'         => $request->input('email'),
                'password'      => Hash::make($request->input('password')),
                'status'        => $request->input('status'),
            ];
            //dd($data);
            $user = User::create($data);
            return Redirect::back()->with('message', 'User added successfully');
        } else {

            return Redirect::back()->withErrors($validator->errors())->withInput();
        }

    }
    
    /**
     * User edit
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user           = User::find($id);
        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();

        return view('user.edit', compact('user', 'restaurantData'));
    }

    /**
     * User edit update
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if(isset($id) && is_numeric($id)) {
            $user = User::find($id);
            $oldEmail = $user->email;
            if($oldEmail === $request->get('email')) {
                // no change, validation - exists
                $emailValidation = 'exists';
            } else {
                // change, validation - unique
                $emailValidation = 'unique';
            }
            $validator = Validator::make($request->all(), [
                'restaurant_id' => 'required|exists:restaurants,id',
                'fname'         => 'required|max:255',
                'lname'         => 'required|max:255',
                'email'         => 'required|string|email|max:255|' . $emailValidation . ':users',
                'status'        => 'required|in:0,1',
                //'image_delete'  => 'sometimes|nullable|in:1',
            ]);

            if(!$validator->fails()) {
                $user->restaurant_id = $request->input('restaurant_id');
                $user->fname = $request->input('fname');
                $user->lname = $request->input('lname');
                $user->email = $request->input('email');
                $user->status = $request->input('status');
                $user->save();

                return Redirect::back()->with('message', 'User updated successfully');

            } else {

                return Redirect::back()->withErrors($validator->errors())->withInput();
            }
        }

        return Redirect::back()->with('message', 'Invalid Id');
    }
}
