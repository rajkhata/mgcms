<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Models\UserAuth;
use App\Helpers\CommonFunctions;
use App\Helpers\LocationTimeSlot;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;


class TimeslotController extends Controller {

    /**
     * Function to return can restaurant deliver on particular address 
     * @param restauraid : $id
     * @param lat : lat
     * @param long : long
     * @return \Illuminate\Http\JsonResponse
     * vertex,inside,outside,boundary
     */
  
    public function get(Request $request, $id) {    
        $ms = microtime(true);
        $error = '';
        $platform = config('app.userAgent');
        $langId = config('app.language')['id'];
        $result = [];
        $currentTimezone = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $id));
        
        if(!empty(CommonFunctions::$timezoneError)){
            $me = microtime(true) - $ms;
            return response()->json(['timeslots' => $result, 'error' => CommonFunctions::$timezoneError['message'], 'xtime' => $me]);
        }
        
        $currentDate = $currentTimezone->format('Y-m-d h:i');
        $validDate = $currentTimezone->format('Y-m-d');
       
        if(strtotime($request->date) < strtotime($validDate)){
            $me = microtime(true) - $ms;
            return response()->json(['timeslots' => $result, 'error' => "Date is not valid", 'xtime' => $me]);
        }
        
        $orderType = ($request->has('type'))?strtolower($request->type):"delivery";
        $date = ($request->has('date'))?date("Y-m-d",strtotime($request->date)):$currentDate;
               
        if ($orderType=="delivery") {          
           $result = LocationTimeSlot::getDeliveryTimeSlot($id,$date);
           
        }elseif($orderType=="carryout"){
           $result = LocationTimeSlot::getCarryoutTimeSlot($id,$date);
        }else{
            $error = "Order type not found";
        }
        $me = microtime(true) - $ms;
        return response()->json(['timeslots' => $result, 'error' => $error, 'xtime' => $me]);
    }

    /*
        Headers 
        X-location = Outlet Id 
        X-restaurant = Parent Restaurant ID

    */

    public function getRestTime(Request $request, $locationId) {       
        $userAuth = $response = array();        
        $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');
        $ms = microtime(true);
        $status = Config('constants.status_code.BAD_REQUEST');
        if($request->has('date') && $request->has('type') && in_array(strtolower($request->query('type')), array('carryout', 'delivery'))) {
            $bearerToken = $request->bearerToken();
            if ($bearerToken) {
                $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            } else {
                if($request->header('Authorization')) {
                    $tokenInfo    = explode(' ', $request->header('Authorization'));
                    if($tokenInfo && isset($tokenInfo['1'])) {
                        $token = $tokenInfo['1'];
                        $userAuth = UserAuth::where(['guest_token' => $token])->first();
                    }
                }          
            }
            if($userAuth) {
                if($locationId){
                    $rest = Restaurant::where(['id' => $locationId])->select('id')->active()->toArray();
                    if($rest) {
                        $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                        // only for testing purpose It will be removed once tested with all cases
                        $restcurrentDateTime['current_day'] = strtolower(date('D', strtotime($request->date)));
                        if($request->has('current_date')  && $request->has('current_time')) {
                            $restcurrentDateTime['current_date'] = $request->query('current_date') ;
                            $restcurrentDateTime['current_time'] = $request->query('current_time') ;
                        }else {
                            if(is_object($currentDateTimeObj)) {
                                $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                                $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i');
                            }else { 
                                $restcurrentDateTime['current_date'] = date('Y-m-d');
                                $restcurrentDateTime['current_time'] = date('H:i');
                            }  
                        }                      
                        // check if current data is greater Than OR equals to the requested date
                        if(strtotime($request->date) < strtotime($restcurrentDateTime['current_date'])){
                            $error = "Invalid Date. Please try again with valid date.";                           
                        }else {
                            $status = Config('constants.status_code.STATUS_SUCCESS');
                            
                            // Delivery Time slots
                            if(strtolower($request->query('type')) == 'delivery') {
                                $slots = $this->get_delivery_slots($locationId, $request->date, $restcurrentDateTime);
                            }else {
                                $slots = $this->get_carryout_slots($locationId, $request->date, $restcurrentDateTime);
                            }
                        }
                    } 
                } 
            }else {
                $status = Config('constants.status_code.UNAUTHORIZED_REQUEST');
            }
            // success response
            $me = microtime(true) - $ms;
            if($status == 200) {
                return response()->json(['success_msg' => $success_msg ,'data' => $slots, 'xtime' => $me], $status);
            }
        }
        $me = microtime(true) - $ms;
        // Failure response
        return response()->json(['error' => $error, 'xtime' => $me], $status);
    }

    /* $location_id = Branch_id*/
    public function get_delivery_slots($location_id, $date, $current_datetime) {
        $slots = array();
        $sorted_time_slots = array();
        // check custom date if any exists
        $custom_date = DB::table('cms_restaurant_delivery_custom_time')->where('calendar_date->start_date', '=', $date)->where('restaurant_id', $location_id)->orderBy('id', 'DESC')->get()->first();
        if($custom_date && $custom_date->slot_detail) {
            $time_slots =  json_decode($custom_date->slot_detail, true);
            $sorted_time_slots = array_values(array_sort($time_slots, function ($value) {
                return $value['open_time'];
            }));
            
        }
        // if not found in custom timeslot then check in week time
        if(empty($sorted_time_slots)) {
            $calendar_day = 
            $week_date = DB::table('cms_restaurant_delivery_week_time')->where('calendar_day', $current_datetime['current_day'])->where('restaurant_id', $location_id)->orderBy('id', 'DESC')->get()->first();
            if($week_date && $week_date->slot_detail) {
                $time_slots =  json_decode($week_date->slot_detail, true);
                $sorted_time_slots = array_values(array_sort($time_slots, function ($value) {
                    return $value['open_time'];
                }));
                
            }
        } 
        // if any time slots are available
        $get_interval_slots = array(); 
        if($sorted_time_slots) {
            foreach ($sorted_time_slots as $key => $available_time) {
                $interval_gap = 30;
                $order_gap =  1;
                $asap_gap = 30;
                // check in menu meal type if any slots is available then take interval and order gap otherwise 30 minute default
               
                $check_menu_meal = DB::table('menu_meal_types')
                ->whereTime('to_time' ,' >= ' , $available_time['close_time'])
                ->whereTime('from_time' ,'<= ' , $available_time['open_time'])
                ->where('restaurant_id', $location_id)
                ->orderBy('id', 'DESC')
                ->get()
                ->toArray();
                $available_time['open_time'] = $date.' '.$available_time['open_time'];
                $available_time['close_time'] = $date.' '.$available_time['close_time'];
                if($check_menu_meal) { 
                    foreach ($check_menu_meal as $key => $menu_meal_type) {
                        $interval_gap = 30;
                        $order_gap =  1;
                        $asap_gap = 30;
                        if($menu_meal_type->delivery_interval) {
                            $interval_gap = $menu_meal_type->delivery_interval;
                        }
                        if($menu_meal_type->delivery_order_gap > 0 ) {
                           $order_gap = $menu_meal_type->delivery_order_gap;
                        }
                        if($menu_meal_type->asap_gap > 0 ) {
                           $asap_gap = $menu_meal_type->asap_gap;
                        }
                        $get_interval = $this->interval_slots( $available_time['open_time'], $available_time['close_time'], $interval_gap, $order_gap, $current_datetime,  $asap_gap);
                       
                        $get_interval_slots = array_merge($get_interval_slots, $get_interval);                   
                    }
                }else { 
                    // Menu meal does not exists then default 30 min will work 
                    $get_interval = $this->interval_slots( $available_time['open_time'], $available_time['close_time'], $interval_gap, $order_gap, $current_datetime, $asap_gap);
                    $get_interval_slots = array_merge($get_interval_slots, $get_interval);
                }
            }
        }
        if($get_interval_slots) { 
            asort($get_interval_slots);
            $k = 0;//echo "<pre>";print_r($get_interval_slots);die;
            foreach ($get_interval_slots as $key => $value) {
                //ASAP if current time and first time slot have difference of 1 hr then only
                $odr_gap = explode('-', $key)[1] * 60;
                $asap_gap = explode('-', $key)[2] * 60;
                

                if(strtotime($current_datetime['current_time']) <  ($value - $asap_gap)) {
                    $slots[] = array( 'key' => date('H:i', $value), 'value' => date('h:i A', $value), 'order_gap' => $odr_gap, 'asap_gap' => $asap_gap);
                }else {
                    if(strtotime($current_datetime['current_time']) + $odr_gap <= $value) {
                        $slots[] = array( 'key' => date('H:i', $value), 'value' => 'ASAP', 'order_gap' => $odr_gap, 'asap_gap' => $asap_gap);
                    }else {
                        $slots[] = array( 'key' => date('H:i', $value), 'value' => date('h:i A', $value), 'order_gap' => $odr_gap, 'asap_gap' => $asap_gap);
                    }
                }

                $k++;
            }
        }
        return $slots;
    }

    public function interval_slots($open_time, $close_time, $interval_gap, $order_gap, $current_datetime, $asap_gap) {

        $open_timestamp = strtotime($open_time);
        $close_timestamp = strtotime($close_time);
        $interval_timestamp = $interval_gap * 60;
        $ordergap_timestamp = $order_gap * 60;
        $asap_gap_timestamp = $asap_gap * 60;
        $current_datetime['current_time'] = $current_datetime['current_date'].' '.$current_datetime['current_time'];
        $times = array();       
        for ($i = $open_timestamp; $i <= ($close_timestamp); $i= $i+ $interval_timestamp) {           

            if($i >= (strtotime($current_datetime['current_time']) + $ordergap_timestamp) ) {
                // show only near by time slots
                $times[$i.'-'.$order_gap.'-'.$asap_gap] = $i;
            }
            //$times[] = array('key' =>  date('H:i', $i), 'value' => date('h:i A', $i));
        }
        return $times;
    }

    /**
    * $location_id = Branch_id
    * $date = Request date from api
    * $current_datetime = Array of current rest Date and time
    * @return timeslots array
    */

    public function get_carryout_slots($location_id, $date, $current_datetime) {
        $slots = array();
        $sorted_time_slots = array();
        // check custom date if any exists
        $custom_date = DB::table('cms_restaurant_carryout_custom_time')->where('calendar_date->start_date', '=', $date)->where('restaurant_id', $location_id)->orderBy('id', 'DESC')->get()->first();
        if($custom_date && $custom_date->slot_detail) {
            $time_slots =  json_decode($custom_date->slot_detail, true);
            $sorted_time_slots = array_values(array_sort($time_slots, function ($value) {
                return $value['open_time'];
            }));
            
        }
        // if not found in custom timeslot then check in week time
        if(empty($sorted_time_slots)) {
            $calendar_day = 
            $week_date = DB::table('cms_restaurant_carryout_week_time')->where('calendar_day', $current_datetime['current_day'])->where('restaurant_id', $location_id)->orderBy('id', 'DESC')->get()->first();
            if($week_date && $week_date->slot_detail) {
                $time_slots =  json_decode($week_date->slot_detail, true);
                $sorted_time_slots = array_values(array_sort($time_slots, function ($value) {
                    return $value['open_time'];
                }));
                
            }
        }
        // if any time slots are available
        $get_interval_slots = array(); 
        if($sorted_time_slots) {
            foreach ($sorted_time_slots as $key => $available_time) {
                $interval_gap = 30;
                $order_gap =  1;
                $asap_gap = 30;
                // check in menu meal type if any slots is available then take interval and order gap otherwise 30 minute default
               
                $check_menu_meal = DB::table('menu_meal_types')
                ->whereTime('to_time' ,' >= ' , $available_time['close_time'])
                ->whereTime('from_time' ,'<= ' , $available_time['open_time'])
                ->where('restaurant_id', $location_id)
                ->orderBy('id', 'DESC')
                ->get()
                ->toArray();

                $available_time['open_time'] = $date.' '.$available_time['open_time'];
                $available_time['close_time'] = $date.' '.$available_time['close_time'];

                if($check_menu_meal) { 
                    foreach ($check_menu_meal as $key => $menu_meal_type) {
                        $interval_gap = 30;
                        $order_gap =  1;
                        $asap_gap = 30;

                        if($menu_meal_type->carryout_interval) {
                            $interval_gap = $menu_meal_type->carryout_interval;
                        }
                        if($menu_meal_type->carryout_order_gap > 0 ) {
                           $order_gap = $menu_meal_type->carryout_order_gap;
                        }
                        if($menu_meal_type->asap_gap > 0 ) {
                           $asap_gap = $menu_meal_type->carryout_asap_gap;
                        }
                        $get_interval = $this->interval_slots( $available_time['open_time'], $available_time['close_time'], $interval_gap, $order_gap, $current_datetime, $asap_gap);
                       
                        $get_interval_slots = array_merge($get_interval_slots, $get_interval);                   
                    }
                }else { 
                    // Menu meal does not exists then default 30 min will work 
                    $get_interval = $this->interval_slots( $available_time['open_time'], $available_time['close_time'], $interval_gap, $order_gap, $current_datetime, $asap_gap);
                    $get_interval_slots = array_merge($get_interval_slots, $get_interval);
                }
            }
        }
        if($get_interval_slots) { 
            asort($get_interval_slots);
            $k = 0;//echo "<pre>";print_r($get_interval_slots);die;
            foreach ($get_interval_slots as $key => $value) {

                //ASAP if current time and first time slot have difference of 1 hr then only
                $odr_gap = explode('-', $key)[1] * 60;
                $asap_gap = explode('-', $key)[2] * 60;

                if(strtotime($current_datetime['current_time']) <  ($value - $asap_gap)) {
                    $slots[] = array( 'key' => date('H:i', $value), 'value' => date('h:i A', $value), 'order_gap' => $odr_gap, 'asap_gap' => $asap_gap);
                }else {
                    if(strtotime($current_datetime['current_time']) + $odr_gap <= $value) {
                        $slots[] = array( 'key' => date('H:i', $value), 'value' => 'ASAP', 'order_gap' => $odr_gap, 'asap_gap' => $asap_gap);
                    }else {
                        $slots[] = array( 'key' => date('H:i', $value), 'value' => date('h:i A', $value), 'order_gap' => $odr_gap, 'asap_gap' => $asap_gap);
                    }
                }
                
                $k++;
            
            }
        }
        return $slots;
    }

    public function validateRestaurantTime($locationId, $order_type, $date) {
        $result = array('status' => 0, 'error' => 'Invalid Request. Please try again!');
        $slots = array();
        if($locationId){
            $rest = Restaurant::where(['id' => $locationId])->select('id')->active()->toArray();
            if($rest) { 
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                $restcurrentDateTime['current_day'] = strtolower(date('D', strtotime($date)));
                if(is_object($currentDateTimeObj)) {
                    $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                    $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i');
                }else { 
                    $restcurrentDateTime['current_date'] = date('Y-m-d');
                    $restcurrentDateTime['current_time'] = date('H:i');
                }
                // check if current data is greater Than OR equals to the requested date
                if(strtotime($date) < strtotime($restcurrentDateTime['current_date'])){ 
                    $result['error'] = "Invalid Date. Please try again with valid date.";                           
                }else {
                    // Delivery Time slots
                    if(strtolower($order_type) == 'delivery') {
                        $slots = $this->get_delivery_slots($locationId, $date, $restcurrentDateTime);
                    }else {
                        $slots = $this->get_carryout_slots($locationId, $date, $restcurrentDateTime);
                    } 
                    $result = array('status' => 1, 'slot' => $slots);
                }
            } 
        } 
        return $result;
    }
}