<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Helpers\BYW\Godaddy;
use App\Models\BYW\Restaurants;
use App\Helpers\BYW\CloneRestaurant;

//use App\Models\BYW\RestaurantRegister;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Models\BYW\CmsUser;
use App\Models\BYW\Cusines;
use App\Models\BYW\MenuCategories;
use App\Models\BYW\MenuCategoryLanguage;
use App\Models\BYW\MenuSubCategories;
use App\Models\BYW\MenuSubCategoryLanguage;
use App\Models\BYW\MenuItems;
use App\Models\BYW\MenuMealType;
use App\Models\BYW\MenuMealTypePrice;
use App\Models\BYW\MenuSettingCategory;
use App\Models\BYW\MenuSettingItem;
use App\Models\BYW\Localization;
use App\Models\BYW\MailTemplate;
use App\Models\BYW\BYWDomains;
use App\Models\BYW\SBPages;
use App\Models\BYW\CmsRestaurantOperationWeekTime;
use App\Models\BYW\CmsRestaurantOperationCustomTime;
use App\Models\BYW\RestaurantDeliveryCustomTime;
use App\Models\BYW\RestaurantDeliveryWeekTime;
use App\Models\BYW\RestaurantCarryoutCustomeTime;
use App\Models\BYW\RestaurantCarryoutWeekTime;
use App\Models\BYW\RestaurantVisiblityCustomTime;
use App\Models\BYW\RestaurantVisiblityWeekTime;

use App\Models\BYW\NewMenu\MenuItemModel;
use App\Models\BYW\NewMenu\MenuItemsLanguage;
use App\Models\BYW\NewMenu\ItemAddonGroups;
use App\Models\BYW\NewMenu\ItemAddonGroupsLanguage;
use App\Models\BYW\NewMenu\ItemAddons;
use App\Models\BYW\NewMenu\ItemLabels;
use App\Models\BYW\NewMenu\ItemModifierGroupLanguage;
use App\Models\BYW\NewMenu\ItemModifierGroup;
use App\Models\BYW\NewMenu\ItemModifierLanguage;
use App\Models\BYW\NewMenu\ItemModifierOptionGroupLanguage;
use App\Models\BYW\NewMenu\ItemModifierOptionGroup;
use App\Models\BYW\NewMenu\ItemModifier;
use App\Models\BYW\NewMenu\ItemOptionsModifierLanguage;
use App\Models\BYW\NewMenu\ItemOptionsModifier;
use App\Helpers\CommonFunctions;
use App\Models\BYW\NewMenu\MenuItemsVisibility;
use App\Models\BYW\NewMenu\ModifierCategories;
use App\Models\BYW\NewMenu\VisibilityHours;
//use Assurrussa\GridView\Support\Column; 

class OnboardingController extends Controller
{
    protected $date,$itmelimit,$catlimit,$message;
    public $folderPath ="/var/www/html/cms-qc-byw/public/deploy/";
    public $domainFolderPath="/var/www/html/cms-qc-byw/domainconfig/";
    public $deployedPath ='/var/www/html/website/byw-compiled/template';		
    	
   
    private $registeredRestaurant;
    public $domain = null;
    public $qc = 0;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {//RestaurantRegister $RestaurantRegister
        $this->date = date("Y-m-d H:i:s");
        $this->itmelimit = 1000;
        $this->catlimit = 500;
        $this->registeredRestaurant = null;//$RestaurantRegister;
        $this->message = "Restaurant creation start...";
        $this->qc = 0;
    }
    
    public function index()
    { 
        
        $title = 'Bravvura Restaurant List';
        $data = $this->_getGridView()->getSimple();
            if (request()->ajax() || request()->wantsJson()) {
                return $data->toHtml();
            }
         return view('restaurant.indexgrid', compact('title', 'data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
	 
        $Cusines = Cusines::select('id','name')->orderBy('name','ASC')->get();
        return view('BYWRestaurant.create', compact('Cusines'));
    }
     
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	 	
	    $this->registerNewRestaurant($request);
	 
	 
    }	
    public function checkMobileNo(Request $request){
	$phoneno = trim($request->input('phoneno'));
	if($phoneno!=""){
		return true;
	}else return false; 
    }
    public function checkDealId(Request $request){
	$deal_id = trim($request->input('deal_id'));
	if($deal_id!=""){
		$restaurantArray = Restaurants::select('id')->where('deal_id',$deal_id)->get()->toArray();  
		if(isset($restaurantArray[0]['id']))return false;
		else return true;
	}else return false; 
    }	
    public function checkEmailId(Request $request){
	$email = trim($request->input('email'));
	if($email!=""){
		$restaurantArray = Restaurants::select('id')->where('email',$email)->get()->toArray();
		if(count($restaurantArray)>0)return false;
		else return true;
	}else return false;
    }
    public function checkSubDomainAvailable($subdomain){
	//$email = trim($request->input('email'));
	if($subdomain!=""){
		$domain = BYWDomains::select('id')->where('domain',$subdomain)->get()->toArray();
		if(count($domain)>0)return false;
		else return true;
	}else return false;
    }	
    public function generateSlug($slug){
	$newstr = preg_replace('/[^a-zA-Z0-9\']/', '-', $slug);
	$newstr = str_replace("'", '', $newstr);
	return trim($newstr);
    }
    public function filterLatLong($latLong){
	if($latLong!=""){
		$newstr = str_replace(")", '', $latLong); 
		$newstr = str_replace("(", '', $latLong);
		return explode(",",$newstr);
	}else return [0,0];
    }
    public function getCityInfo($name){
	if($name!=""){
		$cityArray = City::select('*')->where('city_name',ucwords($name))->get()->toArray();
		if(count($cityArray)>0)return $cityArray;
		else return null;
	}else return null;
    }			 		
    public function registerNewRestaurant(Request $request)
    { //print_r($request->all());die;
	 
        if($this->qc==1){
            $this->domain=config('constants.setup_restaurant.qcdomain');
            $restaurant_id = 74 ;
        }
        else{
            $restaurant_id = 1 ;
            $this->domain=config('constants.setup_restaurant.domain');;
        }
        $restaurantName = str_replace(" ","-",$request->input('restaurant_name'));
        $subdomanName  = $restaurantName;
        $allowedServices = $request->input('allowedServices');
        //if($this->checkDealId($request)){
            if($this->checkEmailId($request)){
                if($this->checkMobileNo($request)){
                    if($this->checkSubDomainAvailable($subdomanName)){
                    $contactAddressCityInfo = $this->getCityInfo($request->input('locality'));
                    $pickupAddressCityInfo = $this->getCityInfo($request->input('locality1'));
                    $latLongArray = $this->filterLatLong($request->input('latlong'));
                    $additionalInfo = ['food_ordering_takeout_allowed'=>(isset($allowedServices[0]) && $allowedServices[0]=="Takeout" 
					|| isset($allowedServices[1]) && $allowedServices[1]=="Takeout")?1:0,'food_ordering_delivery_allowed'=>(isset($allowedServices[0]) 
					&& $allowedServices[0]=="Delivery")?1:0,
                        'is_food_order_allowed'=>(isset($allowedServices[0]) && ($allowedServices[0]=="Delivery" || $allowedServices[0]=="Takeout" || isset($allowedServices[1])))?1:0,
                        'name'=>$request->input('restaurant_name'),
                        'cusine'=>$request->input('cusine'),
                        'slug'=>$this->generateSlug($request->input('restaurant_name')),
                        'email'=>$request->input('email'),
			'feature'=>$request->input('feature'),
		        'amemail'=>$request->input('amemail'),
                        'tagline'=>$request->input('tagline'),
                        'amname'=>$request->input('amname'),
                        'type'=>$request->input('type'),
                        'deal_id'=>$request->input('deal_id'),
                        'sales_tax'=>$contactAddressCityInfo[0]['sales_tax'],
                        'timezone_value'=>$contactAddressCityInfo[0]['timezone_value'],
                        'phone'=>$request->input('phoneno'),
                        'address'=>$request->input('address1'),
	   		'address2'=>$request->input('route1'),
                        'source_url'=>"https://".$subdomanName.".".$this->domain,
                        'street'=>$request->input('street_number1'),
                        'zipcode'=>$request->input('postal_code1'),
                        'pickup_contact_person'=>$request->input('pickup_contact_person1'),
                        'pickup_contact_email'=>$request->input('pickup_contact_email1'),
			'pickup_contact_person_title'=>$request->input('title1'),
                        'zipcode'=>$request->input('postal_code1'),
                        'country_id'=>$request->input('country1'),
                        'state_id'=>$request->input('administrative_area_level_11'),
                        'city_id'=>isset($pickupAddressCityInfo[0])?$pickupAddressCityInfo[0]['id']:0,
                        'contact_person_title'=>$request->input('title'),
			'contact_address'=>$request->input('address'),
			'contact_address2'=>$request->input('route'),
                        'contact_street'=>$request->input('street_number'),
                        'contact_zipcode'=>$request->input('postal_code'),
                        'contact_person'=>$request->input('contact_person'),
 			'contact_country_id'=>$request->input('country'),
                        'contact_state_id'=>$request->input('administrative_area_level_1'),
                        'contact_city_id'=>isset($contactAddressCityInfo[0])?$contactAddressCityInfo[0]['id']:0,
                        'meta_tags'=>json_encode([["type"=>"Meta","label"=>"Title","content"=>$request->input('restaurant_name') ." - ".$request->input('cusine')." in ".$request->input('locality').", ". $request->input('administrative_area_level_1') . " ". $request->input('postal_code') . " | ". $request->input('is_allowed_services')],
                            ["type"=>"Meta","label"=>"keyword","content"=>$request->input('restaurant_name') .", ".$request->input('cusine')." restaurant, ".$request->input('cusine')." Restaurant Near Me, best ".$request->input('cusine')." ".$request->input('locality').", ".$request->input('cusine')." near me, > ".$request->input('locality')." ".$request->input('cusine')],
                            ["type"=>"Meta","label"=>"Description","content"=> $request->input('is_allowed_services') ." in ".$request->input('locality').", ". $request->input('administrative_area_level_1') . " ". $request->input('postal_code')." from  ".$request->input('restaurant_name')],
                            ["type"=>"Meta","label"=>"geo.region","content"=>$request->input('locality')],
                            ["type"=>"Meta","label"=>"geo.placename","content"=>$request->input('locality')],
                            ["type"=>"Meta","label"=>"geo.position","content"=>$latLongArray[0].";". $latLongArray[0]],
                            ["type"=>"Meta","label"=>"ICBM","content"=>$latLongArray[0].",". $latLongArray[0]],
                        ]),
 			'title'=>$request->input('restaurant_name') . " ". $request->input('locality') .
                        " ". $request->input('administrative_area_level_1') . " ". $request->input('postal_code') . " ". $request->input('is_allowed_services'),
                        'description'=>$request->input('restaurant_name') . " ". $request->input('locality') .
                        " ". $request->input('administrative_area_level_1') . " ". $request->input('postal_code') . " ". $request->input('is_allowed_services'),
                        'lat'=>$latLongArray[0],
                        'lng'=>$latLongArray[1],
                        ] ;

                    // clone parent restaurant info

			//$this->coneNewMenuCategoryData(15, 2, $additionalInfo);die;

                    $pid = 0;
		    //echo $restaurant_id;	
                    $new_p_restaurant_id  = $this->coneRestaurnatData($restaurant_id,$pid,$additionalInfo,0);
                    $this->cloneSBPages($restaurant_id,$new_p_restaurant_id);
                    // clone branch restaurant info
                    if($new_p_restaurant_id>0) {

                        $new_restaurant_id = 1000;
			$clone = new CloneRestaurant();
                        $restaurantObj = Restaurants::select('*')->where('parent_restaurant_id', $restaurant_id)->where('is_default_outlet', 1)->limit(1)->get()->toArray();
			foreach ($restaurantObj as $array) {
			   
			     $new_restaurant_id = $this->coneRestaurnatData($array['id'], $new_p_restaurant_id, $additionalInfo, 1);
                             $old_restaurant_id = $array['id'];
                            //$d = $this->coneNewMenuCategoryData($new_restaurant_id, $old_restaurant_id, $additionalInfo);
			    //$d = $clone->coneNewMenuCategoryData($new_restaurant_id, $old_restaurant_id); 

                        }
                        $m = $this->createSubDomain($subdomanName, $new_restaurant_id);
                        if($m['error']==false) 
                        CloneRestaurant::sendmailtoclient($request->input('amemail'),$request->input('email'),$request->input('amname'));
			else json_encode([ 'error' => 4 ,'msg' =>  "Something went wrong!!!" ]); die;
                    }else{
                        // Mobile no required
                        echo  json_encode([ 'error' => 4 ,'msg' =>  "Something went wrong!!" ]); die;
                    }
                }else{
                    // Mobile no required
                    echo  json_encode([ 'error' => 4 ,'msg' =>  "Domain already exist!" ]); die;
                }
                }else{
                    // Mobile no required
                    echo  json_encode([ 'error' => 1 ,'msg' =>  "Mobile Number required!" ]); die;
                }
            }else{
                // Email already exist
                echo json_encode([ 'error' => 2 ,'msg' => "Duplicate Email id!" ]  );die;
            }
        /*}else{
            // Deal Id already exist
            echo json_encode([ 'error' => 3,'msg' => "Duplicate Deal id!" ] );die;
        }*/
        echo json_encode([ 'error' => 0 ,'msg' => "Restaurant created successfully!"]  );die;
	 
    }

    public function createSubDomain($subdomanName,$new_restaurant_id){
		if($this->setrestauranttheme(3,$new_restaurant_id,$subdomanName)){
			$Godaddy = new Godaddy(null,$subdomanName,1);
			$m = $Godaddy->createNewSubDomain($this->qc,$this->domain);
			return $m;
		}return ["error"=>true];
		
    }		

    public function setrestauranttheme($themeId,$restId,$subdomanName){

	  
	 $restDetails = Restaurants::select('*')->where(['restaurants.id' => $restId])->get()->toArray(); 
	 $restDetails =$restDetails[0];
	 if($restDetails['parent_restaurant_id']>0){	
		$inputArr['template_id'] = $themeId;
		Restaurants::where('id', $restId)->update($inputArr);
         	$this->cloneSubDomain($subdomanName,$inputArr['template_id'],$restDetails['parent_restaurant_id']);
		$domain = BYWDomains::select('*')->where('restaurant_id',$restDetails['parent_restaurant_id'])->get()->toArray(); 
		if(isset($domain[0]['domain']))
		$this->deployCodeBase($domain[0]['domain'],$domain[0]['vhostinfo'],$inputArr['template_id'],$restDetails['parent_restaurant_id']);
	 }
	 return true; 
    }			
    public function cloneSubDomain($domainName,$templateid,$new_restaurant_id){
        if($this->qc==1) {
            //$this->domain = 'munchado.it';
            $ssl = "SSLCertificateFile /etc/apache2/munchadoitssl/certificate.crt
            SSLCertificateKeyFile /etc/apache2/munchadoitssl/private.key
            SSLCACertificateFile /etc/apache2/munchadoitssl/ca_bundle.crt";
	    $subDomainContent='<VirtualHost *:80>
            ServerName '.$domainName.'.'.$this->domain.'
             
            ServerAdmin ashokbhati.php@gmail.com
            Redirect permanent / https://'.$domainName.$this->domain.'/
            DocumentRoot '.$this->deployedPath.$templateid.'/
		<Directory '.$this->deployedPath.$templateid.'/>
		            DirectoryIndex index.html index.cgi index.pl index.php
		            Options MultiViews FollowSymLinks
		            Require all granted
		            AllowOverride All
		            Order allow,deny
		            Allow from all
		    </Directory>
		    SetEnv APPLICATION_ENV production
		</VirtualHost>
		
		<VirtualHost *:443>
		    ServerName $domainName.$this->domain
		     
		    ServerAdmin ashokbhati.php@gmail.com
		    SSLEngine on
		    $ssl
	            DocumentRoot '.$this->deployedPath.$templateid.'/
		<Directory '.$this->deployedPath.$templateid.'/>
		            DirectoryIndex  index.php index.html index.cgi index.pl
		            Options MultiViews FollowSymLinks
		            Require all granted
		            AllowOverride All
		    AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript image/jpeg image/jpg image/png
		</Directory>
		</VirtualHost>';	
        }else {
             
             $subDomainContent="server {
			listen 80 ;
			server_name ".$domainName.".".$this->domain."
			root /var/www/html/website/byw-compiled/template3;
			return 301 https://".$domainName.".".$this->domain."\$request_uri;
		}server {
			listen   443 ssl http2;
			client_max_body_size 1G;
			ssl_session_cache    shared:SSL:30m;
			ssl_session_timeout  30m;
			ssl_ciphers ALL:!kEDH!ADH:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP;
			ssl_prefer_server_ciphers on;
		 	ssl    on;
			ssl_certificate   /etc/nginx/munchadossl/bundle.crt;     #/etc/ssl/your_domain_name.pem; (or bundle.crt)
			ssl_certificate_key  /etc/nginx/munchadossl/private.key;  #/etc/ssl/your_domain_name.key;
		        index index.php index.htm index.nginx-debian.html;
			 
			root ".$this->deployedPath.$templateid.";
			server_name ".$domainName.".".$this->domain.";
			access_log /var/log/nginx/nkdaccess.log;
			error_log /var/log/nginx/nkderror.log;
			try_files \$uri \$uri/ /index.php?\$args;
			location ~ \.php$ {
		 		include snippets/fastcgi-php.conf;
		 		fastcgi_pass unix:/run/php/php7.2-fpm.sock;
			   	include fastcgi_params;
		    	}

		 	location ~ /\.ht {
				deny all;
			}
					
		}";
	}
        
        $d['domain']=$domainName;
        $d['restaurant_id']=$new_restaurant_id;
        $d['vhostinfo']= $subDomainContent;
        $m = BYWDomains::create($d);
    }
    function createFile($fileName,$data){
       	$my_file = $fileName;
	 
	if(file_exists($my_file)){
	   unlink($my_file);
	}	
	$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
	fwrite($handle, $data, strlen($data));
	fclose($handle); 
	return true;
    }	
    public function deployCodeBase($subdomanName,$vhost,$templateid,$restid){
 	$domainconfigFile = $this->domainFolderPath.$subdomanName.'__'.$templateid.'__'.$restid.'__config.conf';
        $this->createFile($domainconfigFile,$vhost);
        $runShFile = $this->folderPath.'run.txt';
        $this->createFile($runShFile,"testinggg");
         return true;     
    }
	 				
    public function coneRestaurnatData($old_rest_id,$parent_id=0,$otherInfo=[],$is_default_outlet=0)	{
        $restaurantArray = Restaurants::select('*')->where('id',$old_rest_id)->limit(1)->get()->toArray();//print_r($restaurantArray);die;
        $new_id = 0;
        foreach($restaurantArray as $k=>$d){

            $d['parent_restaurant_id']=$parent_id;
            $d['restaurant_name']=$otherInfo['name'];
            $d['template_id']=0;
            $d['phone']=$otherInfo['phone'];
            $d['mobile']=$otherInfo['phone'];
            $d['email']=($parent_id==0)?$otherInfo['email']:'1'.$otherInfo['email'];
            $d['plan_type']=$otherInfo['type'];
	    $d['is_default_outlet'] = 1;
            $d['amemail']=$otherInfo['amemail'];
            $d['amname']=$otherInfo['amname'];
	    $d['feature']=$otherInfo['feature'];
            $d['tagline']=$otherInfo['tagline'];	
            $d['city_tax']=isset($otherInfo['sales_tax'])?$otherInfo['sales_tax']:0;
            $d['tax']=isset($otherInfo['sales_tax'])?$otherInfo['sales_tax']:0;
            $d['takeout']=$d['food_ordering_takeout_allowed']=isset($otherInfo['food_ordering_takeout_allowed'])?1:0;
            $d['delivery']=$d['food_ordering_delivery_allowed']=isset($otherInfo['food_ordering_delivery_allowed'])?1:0;
            $d['is_food_order_allowed']=isset($otherInfo['is_food_order_allowed'])?$otherInfo['is_food_order_allowed']:0;
            $d['slug']=$otherInfo['slug'];
            $d['address']=$otherInfo['address'];
            $d['address2']=$otherInfo['address2'];
            $d['source_url']=$otherInfo['source_url'];
            $d['street']=$otherInfo['street'];
            $d['deal_id']=$otherInfo['deal_id'];
            $d['zipcode']=$otherInfo['zipcode'];
            $d['contact_country']=$otherInfo['contact_country_id'];
            $d['contact_state']=$otherInfo['contact_state_id'];

            $d['is_default_outlet']=$is_default_outlet;
            $d['city_id']=$otherInfo['city_id'];
            $d['contact_address']=$otherInfo['contact_address'];
            $d['contact_address2']=$otherInfo['contact_address2'];
            $d['contact_street']=$otherInfo['contact_street'];
            $d['cusine']=$otherInfo['cusine'];
            $d['contact_zipcode']=$otherInfo['contact_zipcode'];
            $d['contact_person']=$otherInfo['contact_person'];
	    $d['contact_person_title']=$otherInfo['contact_person_title'];
	    $d['pickup_contact_person_title']=$otherInfo['pickup_contact_person_title'];
            $d['pickup_contact_person']=$otherInfo['pickup_contact_person'];
            $d['pickup_contact_email']=$otherInfo['pickup_contact_email'];
            $d['pickup_country']=$otherInfo['country_id'];
            $d['pickup_state']=$otherInfo['state_id'];
            $d['meta_tags']=$otherInfo['meta_tags'];
            $d['meta_keyword']=$otherInfo['meta_tags'];
            $d['title']=$otherInfo['title'];
            $d['description']=$otherInfo['description'];
            $d['lat']=$otherInfo['lat'];
            $d['lng']=$otherInfo['lng'];
            $d['created_at']=$this->date;
            $d['updated_at']=$this->date; 
            $m = Restaurants::create($d);
            $new_id = $m->id;
            if($new_id>0)$this->cloneCmsUser($old_rest_id,$m->id,$otherInfo,$parent_id);
        }
        return $new_id;
	
     }
     public function cloneCmsUser($rest_id,$new_rest_id=0,$otherInfo=[],$parent_id=0)	{
        $cmsArray = CmsUser::select('*')->where('restaurant_id',$rest_id)->limit(1)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;
            $d['name']=$otherInfo['name'];
            $d['email']=($parent_id==0)?$otherInfo['email']:$new_rest_id.$otherInfo['email'];
            $d['created_at']=$this->date;
            $d['updated_at']=$this->date;
            $m = CmsUser::create($d);
            $new_id = $m->id;
        }
        return $new_id;

      }

      public function cloneRestaurantVisiblityCustomTime($rest_id,$new_rest_id=0)	{
	$cmsArray = RestaurantVisiblityCustomTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
	$new_id = 0;
	foreach($cmsArray as $k=>$d){ 
		$d['restaurant_id']=$new_rest_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$m = RestaurantVisiblityCustomTime::create($d); 
		$new_id = $m->id;
	}
	return $new_id;
	
    }
    public function cloneRestaurantVisiblityWeekTime($rest_id,$new_rest_id=0)	{
	$cmsArray = RestaurantVisiblityWeekTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
	$new_id = 0;
	foreach($cmsArray as $k=>$d){ 
		$d['restaurant_id']=$new_rest_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$m = RestaurantVisiblityWeekTime::create($d); 
		$new_id = $m->id;
	}
	return $new_id;
	
    }	
	
     public function cloneRestaurantOperationWeekTime($rest_id,$new_rest_id=0)	{
	$cmsArray = CmsRestaurantOperationWeekTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
	$new_id = 0;
	foreach($cmsArray as $k=>$d){ 
		$d['restaurant_id']=$new_rest_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$m = CmsRestaurantOperationWeekTime::create($d); 
		$new_id = $m->id;
	}
	return $new_id;
	
    }
    public function cloneRestaurantOperationCustomTime($rest_id,$new_rest_id=0)	{
	$cmsArray = CmsRestaurantOperationCustomTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
	$new_id = 0;
	foreach($cmsArray as $k=>$d){ 
		$d['restaurant_id']=$new_rest_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$m = CmsRestaurantOperationCustomTime::create($d); 
		$new_id = $m->id;
	}
	return $new_id;
	
    }	
    		 
    public function cloneRestaurantCarryoutCustomeTime($rest_id,$new_rest_id=0)	{
	$cmsArray = RestaurantCarryoutCustomeTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
	$new_id = 0;
	foreach($cmsArray as $k=>$d){ 
		$d['restaurant_id']=$new_rest_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$m = RestaurantCarryoutCustomeTime::create($d); 
		$new_id = $m->id;
	}
	return $new_id;
	
    }
    public function cloneRestaurantCarryoutWeekTime($rest_id,$new_rest_id=0)	{
	$cmsArray = RestaurantCarryoutWeekTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
	$new_id = 0;
	foreach($cmsArray as $k=>$d){ 
		$d['restaurant_id']=$new_rest_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$m = RestaurantCarryoutWeekTime::create($d); 
		$new_id = $m->id;
	}
	return $new_id;
	
    }	

    public function cloneRestaurantDeliveryWeekTime($rest_id,$new_rest_id=0)	{
	$cmsArray = RestaurantDeliveryWeekTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
	$new_id = 0;
	foreach($cmsArray as $k=>$d){ 
		$d['restaurant_id']=$new_rest_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$m = RestaurantDeliveryWeekTime::create($d); 
		$new_id = $m->id;
	}
	return $new_id;
	
    }
    public function cloneRestaurantDeliveryCustomTime($rest_id,$new_rest_id=0)	{
	$cmsArray = RestaurantDeliveryCustomTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
	$new_id = 0;
	foreach($cmsArray as $k=>$d){ 
		$d['restaurant_id']=$new_rest_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$m = RestaurantDeliveryCustomTime::create($d); 
		$new_id = $m->id;
	}
	return $new_id;
	
    }
    public function cloneSBPages($rest_id,$new_rest_id=0)	{
       //$rest_id = 74;
        $cmsArray = SBPages::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;
            $d['created_at']=$this->date;
            $d['updated_at']=$this->date;
            $m = SBPages::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }
    public function coneNewMenuCategoryData($new_restaurant_id,$restaurnat_id)	{ 
	//first copy modifier category data based on restaurnat id
	$CategoryArray1 = null;
	$CategoryArray1['modifierCategory'] = $this->coneNewMenuModfierCategoryData($CategoryArray1,$new_restaurant_id,$restaurnat_id); 
	// copy all hour data
	$CategoryArray1['VisiblityCustom'] = $this->cloneRestaurantVisiblityCustomTime($restaurnat_id,$new_restaurant_id);
	$CategoryArray1['VisiblityWeek'] = $this->cloneRestaurantVisiblityWeekTime($restaurnat_id,$new_restaurant_id);	
	$CategoryArray1['OperationWeek'] = $this->cloneRestaurantOperationWeekTime($restaurnat_id,$new_restaurant_id);
	$CategoryArray1['OperationCustom'] = $this->cloneRestaurantOperationCustomTime($restaurnat_id,$new_restaurant_id);
	$CategoryArray1['CarryoutCustom'] = $this->cloneRestaurantCarryoutCustomeTime($restaurnat_id,$new_restaurant_id);
	$CategoryArray1['CarryoutWeek'] = $this->cloneRestaurantCarryoutWeekTime($restaurnat_id,$new_restaurant_id);	
	$CategoryArray1['DeliveryWeek'] = $this->cloneRestaurantDeliveryWeekTime($restaurnat_id,$new_restaurant_id);
	$CategoryArray1['DeliveryCustom'] = $this->cloneRestaurantDeliveryCustomTime($restaurnat_id,$new_restaurant_id);
	 
        // start coneing category and its realted data
	$CategoryArray = MenuCategories::select('*')->where('restaurant_id',$restaurnat_id)->where('status',1)->limit($this->catlimit)->get()->toArray();
	  
	
	foreach($CategoryArray as $k=>$d){
		
		$d['restaurant_id'] =$new_restaurant_id;
		$d['created_at']=$this->date;
		$d['updated_at']=$this->date;
		$CategoryArray1['menuCategory'][$k]['id'] = $d['id'];
		unset($d['id']);
		$m = MenuCategories::create($d);
		$d['old_restaurant_id'] = $d['restaurant_id'];
		$CategoryLanguageArray = MenuCategoryLanguage::select('*')->where('menu_categories_id',$CategoryArray1['menuCategory'][$k]['id'])->get()->toArray();
		foreach($CategoryLanguageArray as $languageD){
			$languageD['menu_categories_id']=$m->id;
			$languageDd['created_at']=$this->date;
			$languageD['updated_at']=$this->date;
			$m1 = MenuCategoryLanguage::create($languageD);
		}
		$CategoryArray1['menuCategory'][$k]['restaurant_id'] =$new_restaurant_id;
		$CategoryArray1['menuCategory'][$k]['new_cat_id'] = $m->id;
	} 
         
	$CategoryArray1 = $this->coneNewMenuSubCategoryData($CategoryArray1); 
	 
	return $this->coneNewMenuItemData($CategoryArray1);
	 
    }
    public function coneNewMenuSubCategoryData($CategoryArray)	{
	if(is_array($CategoryArray['menuCategory'])){
		foreach($CategoryArray['menuCategory'] as $i=>$d){  
			$a = null;
			$SubCategoryArray = MenuSubCategories::select('*')->where('menu_category_id',$d['id'])->where('status',1)->get()->toArray();
			foreach($SubCategoryArray as $k=>$subD){
				$subD['restaurant_id'] = $d['restaurant_id'];
				$subD['menu_category_id'] = $d['new_cat_id'];
				$subD['created_at']=$this->date;
				$subD['updated_at']=$this->date;
				$a[$k]['id'] = $subD['id'];
				unset($subD['id']);
				$m = MenuSubCategories::create($subD);
				$CategoryLanguageArray = MenuSubCategoryLanguage::select('*')->where('menu_sub_categories_id',$a[$k]['id'])->get()->toArray();
				foreach($CategoryLanguageArray as $languageD){
					$languageD['menu_sub_categories_id']=$m->id;
					$languageDd['created_at']=$this->date;
					$languageD['updated_at']=$this->date;
					$m1 = MenuSubCategoryLanguage::create($languageD);
				}
				$a[$k]['new_sub_cat_id'] = $m->id;
			}
			$CategoryArray['menuCategory'][$i]['subcategory'] = $a;
			
		}
	}
	 
	return $CategoryArray;
    }				
    public function coneNewMenuItemData($CategoryArray){ 
	if(is_array($CategoryArray['menuCategory'])){
		foreach($CategoryArray['menuCategory'] as $i=>$cat){
			if(is_array($cat['subcategory'])){
				foreach($cat['subcategory'] as $j=>$sub){
					$a = null;
					$menuItemArrayBySubCategory = MenuItemModel::select('*')->where('menu_sub_category_id',$sub['id'])->where('status',1)->limit($this->itmelimit)->get()->toArray();
					foreach($menuItemArrayBySubCategory as $k=>$menuItem){
						$menuItem['restaurant_id'] = $cat['restaurant_id'];
						$menuItem['menu_category_id'] = $cat['new_cat_id'];
						$menuItem['menu_sub_category_id'] = $sub['new_sub_cat_id'];
						$menuItem['created_at']=$this->date;
						$menuItem['updated_at']=$this->date;
						$a[$k]['id'] = $menuItem['id'];	
						unset($menuItem['id']);
						$m = MenuItemModel::create($menuItem);
						$menuItemLanguageArray = MenuItemsLanguage::select('*')->where('new_menu_items_id',$a[$k]['id'])->get()->toArray();
						foreach($menuItemLanguageArray as $menulanguageD){
							$menulanguageD['new_menu_items_id']=$m->id;
							$menulanguageD['created_at']=$this->date;
							$menulanguageD['updated_at']=$this->date;
							$m1 = MenuItemsLanguage::create($menulanguageD);
						}
						$a[$k]['new_menu_id'] = $m->id;
					}
					$CategoryArray['menuCategory'][$i]['menuItems'] = $a;
				}
			}else{ //menu item without any sub category mapping 
				$a = null;
				$menuItemArrayByCategory = MenuItemModel::select('*')->where('menu_category_id',$cat['id'])->where('status',1)->limit($this->itmelimit)->get()->toArray();
				foreach($menuItemArrayByCategory as $k=>$menuItem){
					$menuItem['restaurant_id'] = $cat['restaurant_id'];
					$menuItem['menu_category_id'] = $cat['new_cat_id'];
					$menuItem['menu_sub_category_id'] = NULL;
					$menuItem['created_at']=$this->date;
					$menuItem['updated_at']=$this->date;
					$a[$k]['id'] = $menuItem['id'];
					unset($menuItem['id']);
					$m = MenuItemModel::create($menuItem);
					$menuItemLanguageArray = MenuItemsLanguage::select('*')->where('new_menu_items_id',$a[$k]['id'])->get()->toArray();
					foreach($menuItemLanguageArray as $menulanguageD){
							$menulanguageD['new_menu_items_id']=$m->id;
							$menulanguageD['created_at']=$this->date;
							$menulanguageD['updated_at']=$this->date;
							$m1 = MenuItemsLanguage::create($menulanguageD);
					}
					$a[$k]['new_menu_id'] = $m->id;
					$CategoryArray['menuCategory'][$i]['menuItems'] = $a;
					
				}
				
			} 
		}
	}
	//create modifier group and realted data
	$CategoryArray = $this->coneNewMenuModfierGroupData($CategoryArray);
	//create adon group and realted data	
	$CategoryArray = $this->coneNewMenuItemAdonsGroupData($CategoryArray);
	return $CategoryArray; 
    }
    public function coneNewMenuItemAdonsGroupData($CategoryArray)	{  
	if(is_array($CategoryArray['menuCategory'])){  
		foreach($CategoryArray['menuCategory'] as $i=>$d){   
			$a = null;
			if(isset($d['menuItems'])){
			foreach($d['menuItems'] as $j=>$menuItem){ 
				$mCategoryArray = ItemAddonGroups::select('*')->where('menu_item_id',$menuItem['id'])->get()->toArray();
				foreach($mCategoryArray as $k=>$catD){
					$catD['restaurant_id'] = $d['restaurant_id'];
					$catD['menu_item_id'] = $menuItem['new_menu_id'];
					$catD['created_at']=$this->date;
					$catD['updated_at']=$this->date;
					$a[$k]['id'] = $catD['id'];
					unset($catD['id']);
					$m = ItemAddonGroups::create($catD);
					$menuItemModGrpLanguageArray = ItemAddonGroupsLanguage::select('*')->where('item_addon_group_id',$a[$k]['id'])->get()->toArray();
					foreach($menuItemModGrpLanguageArray as $menulanguageD){
						$menulanguageD['item_addon_group_id']=$m->id;
						$menulanguageD['created_at']=$this->date;
						$menulanguageD['updated_at']=$this->date;
						$m1 = ItemAddonGroupsLanguage::create($menulanguageD);
					}
					$a[$k]['new_adon_grp_id'] = $m->id;
					$CategoryArray['menuCategory'][$i]['menuItems'][$j]['adonGroup'] = $a;
				}
				
			}}
		}
	}
	//create modifier group and realted data
	$CategoryArray = $this->coneNewMenuAdonGroupItemData($CategoryArray);
	 return $CategoryArray;
    }
    public function coneNewMenuAdonGroupItemData($CategoryArray)	{ 
	if(is_array($CategoryArray['menuCategory'])){ 
		foreach($CategoryArray['menuCategory'] as $i=>$d){   //categoryData $d
			if(isset($d['menuItems'])){
			foreach($d['menuItems'] as $j=>$menuItem){ //menuItemData $d1
				if(isset($menuItem['adonGroup'])){
				foreach($menuItem['adonGroup'] as $k=>$adonGroup){  //adonGroupData $d2
					$a = null;
					$adonArray = ItemAddons::select('*')->where('addon_group_id',$adonGroup['id'])->get()->toArray();
					foreach($adonArray as $l=>$adonItemArray){  
						$adonItemArray['addon_group_id'] = $adonGroup['new_adon_grp_id'];
						$adonItemArray['restaurant_id'] = $d['restaurant_id'];
						$adonItemArray['item_id'] = $menuItem['new_menu_id'];
						$adonItemArray['created_at']=$this->date;
						$adonItemArray['updated_at']=$this->date; 
						$a[$k]['id'] = $adonItemArray['id'];
						unset($adonItemArray['id']);
						$m = ItemAddons::create($adonItemArray);
						 
						$a[$k]['new_adon_item_id'] = $m->id;
					}$CategoryArray['menuCategory'][$i]['menuItems'][$j]['adonGroup'][$k] = $a;
				}
				}//if exist modifierGroup Array
			}}
			
			
		}
	} 
	return $CategoryArray;
    }		
    public function coneNewMenuModfierGroupData($CategoryArray)	{  
	if(is_array($CategoryArray['menuCategory'])){  
		foreach($CategoryArray['menuCategory'] as $i=>$d){   
			$a = null;
			if(isset($d['menuItems'])){
			foreach($d['menuItems'] as $j=>$menuItem){ 
				$mCategoryArray = ItemModifierGroup::select('*')->where('menu_item_id',$menuItem['id'])->get()->toArray();
				foreach($mCategoryArray as $k=>$catD){
					$catD['restaurant_id'] = $d['restaurant_id'];
					$catD['menu_item_id'] = $menuItem['new_menu_id'];
					$catD['created_at']=$this->date;
					$catD['updated_at']=$this->date;
					$a[$k]['id'] = $catD['id'];
					unset($catD['id']);
					$m = ItemModifierGroup::create($catD);
					$menuItemModGrpLanguageArray = ItemModifierGroupLanguage::select('*')->where('modifier_group_id',$a[$k]['id'])->get()->toArray();
					foreach($menuItemModGrpLanguageArray as $menulanguageD){
						$menulanguageD['modifier_group_id']=$m->id;
						$menulanguageD['created_at']=$this->date;
						$menulanguageD['updated_at']=$this->date;
						$m1 = ItemModifierGroupLanguage::create($menulanguageD);
					}
					$a[$k]['new_mod_grp_id'] = $m->id;
					$CategoryArray['menuCategory'][$i]['menuItems'][$j]['modifierGroup'] = $a;
				}
				
			}}
		}
	}
	//create modifier group and realted data
	$CategoryArray = $this->coneNewMenuModfierGroupItemData($CategoryArray);
	//print_r($CategoryArray); 
	return $CategoryArray;
    }
    public function coneNewMenuModfierGroupItemData($CategoryArray)	{ 
	if(is_array($CategoryArray['menuCategory'])){$a =$newRestaurantId = $newMenuId = $OldMenuId = 0;
		foreach($CategoryArray['menuCategory'] as $i=>$d){   //categoryData $d
			if(isset($d['menuItems'])){
			foreach($d['menuItems'] as $j=>$menuItem){ //menuItemData $d1
				if(isset($menuItem['modifierGroup'])){
				foreach($menuItem['modifierGroup'] as $k=>$modifierGroup){  //modifierGroupData $d2
					$a = null;
					$modifierArray = ItemModifier::select('*')->where('modifier_group_id',$modifierGroup['id'])->get()->toArray();
					foreach($modifierArray as $l=>$modifierItemD){  
						$modifierItemD['modifier_group_id'] = $modifierGroup['new_mod_grp_id'];
						$mod_cat_key_indx = $modifierItemD['modifier_category_id'];
						$modifierItemD['modifier_category_id'] = $CategoryArray['modifierCategory'][$mod_cat_key_indx]??0;
						$modifierItemD['created_at']=$this->date;
						$modifierItemD['updated_at']=$this->date; 
						$a[$k]['id'] = $modifierItemD['id'];
						unset($modifierItemD['id']);
						$m = ItemModifier::create($modifierItemD);
						$menuItemModGrpLanguageArray = ItemModifierLanguage::select('*')->where('modifier_item_id',$a[$k]['id'])->get()->toArray();
						foreach($menuItemModGrpLanguageArray as $menulanguageD){
							$menulanguageD['modifier_item_id']=$m->id;
							$menulanguageD['created_at']=$this->date;
							$menulanguageD['updated_at']=$this->date;
							$m1 = ItemModifierLanguage::create($menulanguageD);
						}
						$a[$k]['new_modifier_item_id'] = $m->id;
					}$CategoryArray['menuCategory'][$i]['menuItems'][$j]['modifierGroup'][$k] = $a;
				}
				}//if exist modifierGroup Array
			}}
			//$CategoryArray[$i][$newMenuId]['modifierGroup'] = $a;
			
		}
	} 
	return $CategoryArray;
    }		
    public function coneNewMenuModfierCategoryData($CategoryArray,$new_restaurant_id,$restaurnat_id)	{
			$a = null;
			$mCategoryArray = ModifierCategories::select('*')->where('restaurant_id',$restaurnat_id)->get()->toArray();
			foreach($mCategoryArray as $k=>$catD){
				$catD['restaurant_id'] = $new_restaurant_id;
				$id = $catD['id'];
				unset($catD['id']);
				$m = ModifierCategories::create($catD);
				$a[$id] = $m->id;
			}
			$CategoryArray = $a;
			
	return $CategoryArray;
    }
    /**
     * @return \Assurrussa\GridView\GridView|\Illuminate\Foundation\Application|mixed
     */
    public function _getGridView()
    {
	// tutorial https://github.com/assurrussa/grid-view-table-app/
      
        /** @var \Assurrussa\GridView\GridView $gridView */
        $query = $this->registeredRestaurant->newQuery(); 
             
            
        $gridView = app(\Assurrussa\GridView\GridView::NAME);
        $gridView->setQuery($query)
            ->setSearchInput(true)
            ->setSortName('id')
            ->setOrderByDesc();
        // classes for every tr string
        $gridView->column()->setClassForString(function ($data) {
            /** @var \App\Post $data */
            return $data->id % 2 ? 'text-success' : '';
        });
        // export setting
        $gridView->setExport(true)
            ->setFieldsForExport([
                'ID'         => 'id',
                'Name'         => 'name',
		'Restaurant ID'         => 'restaurant_id',
                'Contact Address'    => 'contact_address',
                'Created At' => 'created_at',
            ]);
        $gridView->button()->setButtonExport();
        // columns
        $gridView->column('id', '#')->setSort(true)->setFilterString('byId', '', '', 'width:60px');
        //$gridView->column()->setCheckbox();
        $gridView->column('name', 'Restaruant')->setFilterString('byTitleLike', '', '', 'width:160px')->setSort(true);
	$gridView->column('domain', 'Domain')->setFilterString('byUrlLike', '', '', 'width:160px')->setSort(true);
	$gridView->column('git', 'Git')->setFilterString('byGitLike', '', '', 'width:160px')->setSort(false);
	$gridView->column('gitbranch', 'Branch')->setFilterString('byBranchLike', '', '', 'width:160px')->setSort(false);
	 /** @var \App\Post $data 
        $gridView->column('preview', 'preview')->setScreening(true)->setHandler(function ($data) {
           
            return '<img src="' . $data->preview . '" alt="' . $data->title . '" widht="60" height="60">';
        });*/
        //$gridView->column('type', 'type')->setFilterSelect('byType', \App\Post::$types)->setSort(false);
        //$gridView->column('user.country.name', 'country')->setSort(false)->setScreening(false)->setFilterSelectNotAjax('byCountryId', $listCountry, $listCountrySelected);
	 /** @var \App\Post $data 
        $gridView->column('city_id', 'city')->setSort(false)->setScreening(false)->setFilterSelectAjax($nameFilterCity, [], $listCitySelected, route('city.search'))
            ->setHandler(function ($data) {
               
                return $data->user->city->name;
            });*/
	/** @var \App\Post $data 
        $gridView->column('user.name', 'author')->setSort(false)->setScreening(false)->setFilterString('byUserName')
            ->setHandler(function ($data) {
                
                return $data->user->name . ' (id#' . $data->user->id . ')';
            });*/
        //$gridView->column('published_at', 'Published At')->setDateActive(true)->setFilterDateRange('byPublishedAtRange', '', true, 'Y-m-d H:i')->setFilterFormat('DD MMM YY');
        $gridView->column('created_at', 'Created At')->setDateActive(true)
            ->setFilterDate('byCreatedAt', '', true, 'Y-m-d H:i')
            ->setFilterFormat('DD MMM YY');
        // column actions
        $gridView->columnActions(function ($data, $columns) {
            /**
             * @var \App\Post                           $data
             * @var \Assurrussa\GridView\Support\Column $columns
             */
            $columns->addButton()->setActionShow('/create-new-subdomain', [$data->id])
                ->setClass('btn btn-info btn-sm')
                ->setOptions(['target' => '_blank'])
                ->setHandler(function ($data) {
                    /** @var \App\Post $data */
                    return $data->id % 2;
                });
            $columns->addButton()->setActionEdit('/create-new-subdomain', [$data->id], 'Edit')
                ->setClass('btn btn-outline-primary btn-sm')
                ->setOptions(['target' => '_blank'])
                ->setHandler(function ($data) {
                    /** @var \App\Post $data */
                    return $data->id % 2;
                });
            $columns->addButton()->setActionDelete('/create-new-subdomain', [$data->id], '')
                ->setHandler(function ($data) {
                    /** @var \App\Post $data */
                    return $data->id % 2 && !$data->deleted_at;
                });
            $columns->addButton()->setActionRestore('/create-new-subdomain', [$data->id])
                ->setMethod('PUT')->setHandler(function ($data) {
                    /** @var \App\Post $data */
                    return $data->deleted_at;
                });
        });
 
        // create button
        $gridView->button()->setButtonCreate(route('create-new-subdomain'));
        // create custom button
        //$gridView->button()->setButtonCheckboxAction(route('home'), '?custom=');
        return $gridView;
    }	
    /**
     * @return \Assurrussa\GridView\GridView
     */
    private function _getGridView1()
    {
	
        /** @var \Assurrussa\GridView\GridView $gridView */
        $query = $this->registeredRestaurant->newQuery();
        $gridView = app('amiGrid');
	//$gridView = $this->app->make(\Assurrussa\GridView\GridView::NAME);
        $gridView->setQuery($query);;//->setSearchInput(true)
        $column = new Column();
	        $column->setKey('id')
            ->setValue('id')
            ->setSort(true)
            ->setScreening(true)
            ->setHandler(function ($data) {
                return $data == null;
            })
            ->setFilter('id', ['ID' => '1']);
        
        $gridView->column('id', 'ID')->setSort(true);
	
        $gridView->column('name', 'Restaurant')->setSort(true)->setHandler(function ($data) {
            return $data->name ;//. ' ' . $data->name
        });
	$gridView->column('restaurant_id', 'Restaurant ID')->setSort(true);
	$gridView->column('created_at', 'Registartion Date')->setSort(true);
        $gridView->columnActions(function ($data) use ($gridView) {
	    $buttons = []; 	
            /** @var \Assurrussa\GridView\Models\Model $data */
            //$buttons[] = $gridView->columnAction()->setActionShow('Show');
            //$buttons[] = $gridView->columnAction()->setActionEdit('Edit');
            //$buttons[] = $gridView->columnAction()->setActionDelete('Delete');
	 return $buttons;
        });
        $gridView->button()->setButtonCreate('/create-new-subdomain');
        //$gridView->button()->setButtonExport('/export');
        //$gridView->button()->setButtonCheckboxAction('create');
         
        return $gridView;
    }
    public function oldindex(Request $request)
    { 
        //$restaurant = RestaurantRegister::orderBy('id', 'DESC')->paginate(20);
	//return view('restaurant.index', compact('restaurant'));
    }							
    //********************************************************************************************************************************************
    ///old menu clone under development
    public function coneMenuCategoryData($new_restaurant_id,$restaurnat_id,$limit=1000)	{ 
	$CategoryArray = MenuCategories::select('*')->where('restaurant_id',$restaurnat_id)->limit($limit)->get()->toArray();
	 
	$CategoryArray1 = null;
	foreach($CategoryArray as $k=>$d){
		$d['restaurant_id'] =$new_restaurant_id;
		$m = MenuCategories::create($d);
		$CategoryArray1[$k]['id'] = $d['id'];
		$CategoryArray1[$k]['restaurant_id'] =$new_restaurant_id;
		$CategoryArray1[$k]['new_cat_id'] = $m->id;
	} 
        print_r($CategoryArray1); 
	$CategoryArray1 = $this->coneMenuSubCategoryData($CategoryArray1,2);
	if(is_array($CategoryArray))$this->coneMenuItemData($CategoryArray1,2);
	else $this->coneMenuItemData($CategoryArray,10);
    }
    public function coneMenuSubCategoryData($CategoryArray,$limit=1000)	{
	if(is_array($CategoryArray)){
		foreach($CategoryArray as $i=>$d){
			$a = null;
			$SubCategoryArray = MenuSubCategories::select('*')->where('menu_category_id',$d['id'])->limit($limit)->get()->toArray();
			foreach($SubCategoryArray as $k=>$subD){
				$subD['restaurant_id'] = $d['restaurant_id'];
				$subD['menu_category_id'] = $d['new_cat_id'];
				$m = MenuSubCategories::create($subD);
				$a[$k]['id'] = $subD['id'];
				$a[$k]['new_sub_cat_id'] = $m->id;
				 
			}
			$CategoryArray[$i]['subcategory'] = $a;
			
		}
	} 
	return $CategoryArray;
    }
    	
    public function coneMenuItemData($CategoryArray,$limit=1000){ 
	if(is_array($CategoryArray)){
		foreach($CategoryArray as $i=>$cat){
			if(is_array($cat['subcategory'])){
				foreach($cat['subcategory'] as $j=>$sub){
					$a = null;
					$menuItemArrayBySubCategory = MenuItems::select('*')->where('menu_sub_category_id',$sub['id'])->limit($limit)->get()->toArray();
					foreach($menuItemArrayBySubCategory as $k=>$menuItem){
						$menuItem['restaurant_id'] = $cat['restaurant_id'];
						$menuItem['menu_category_id'] = $cat['new_id'];
						//$m = MenuItems::create($menuItem);
						$a[$k]['id'] = $menuItem['id'];
						$a[$k]['new_id'] = $m->id;
						 
					}
					$CategoryArray[$k]['subcategory'] = $a;
				}
			}else{ //menu item without any sub category mapping 
				$a = null;
				$menuItemArrayByCategory = MenuItems::select('*')->where('menu_category_id',$cat['id'])->limit($limit)->get()->toArray();
				foreach($menuItemArrayByCategory as $k=>$menuItem){
					$menuItem['restaurant_id'] = $cat['restaurant_id'];
					$menuItem['menu_category_id'] = $cat['new_cat_id'];
					$menuItem['menu_sub_category_id'] = 0; 
					$m = MenuItems::create($menuItem);
					$a[$k]['id'] = $menuItem['id'];
					$a[$k]['new_menu_id'] = $m->id;
				}
				$CategoryArray[$i]['menuItems'] = $a;
			} 
		}
	} 
    }	

}
