<?php

namespace App\Http\Controllers;

use App\Models\BuildYourOwnPizza;


use App\Models\MenuSettingItems;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use ImageOptimizer;

class PizzaExpItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pizzaSettings = BuildYourOwnPizza::select('build_your_own_pizzas.*', 'restaurants.restaurant_name')
            ->join('restaurants', 'restaurants.id','=','build_your_own_pizzas.restaurant_id')->paginate(20);

        $groupRestData = Restaurant::group();
        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();

        //dd(json_decode($pizzaSettings[0]['setting'], true));
        return view('pizzaexp_item.index', compact('pizzaSettings', 'restaurantData','groupRestData'));


        //$pizzaSetting = config('constants.pizza_setting');
        $pizzaSettings = MenuSettingItems::select('menu_setting_items.*', 'restaurants.restaurant_name')
            ->join('restaurants', 'restaurants.id','=','menu_setting_items.restaurant_id');
        //->whereRaw('json_contains(build_your_own_pizzas.crust, \'{"name": "simple"}\')');
        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
        $pizzaSettings = $pizzaSettings->paginate(20);
        $groupRestData = Restaurant::group();
        return view('pizzaexp_item.index', compact('pizzaSettings', 'restaurantData','groupRestData'));


        $pizzaSetting = config('constants.pizza_setting');
        $pizzaExp = MenuSettingItems::select('menu_setting_items.*', 'restaurants.restaurant_name')
            ->join('restaurants', 'restaurants.id','=','menu_setting_items.restaurant_id')
            //->whereRaw('json_contains(build_your_own_pizzas.crust, \'{"name": "simple"}\')')
            ->get();

        return view('pizzaexp_item.index', compact('pizzaExp', 'pizzaSetting','groupRestData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //$setting        = Input::get('setting');
        //$allSettings    = config('constants.pizza_setting');
        //$correctSetting = $this->findSimilarMatch($setting, $allSettings);
        /*if ($setting !== $correctSetting) {
            return redirect('pizzaexp/create?setting=' . $correctSetting);
        }*/

        // Get all restaurant_ids
        $allRests = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
        $groupRestData = Restaurant::group();
        return view('pizzaexp_item.create', compact('allRests','groupRestData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //print_r($request->all());die;
        if ($request->isMethod('post')) {

            // static content validation
            $request->validate([
                'restaurant_id'   => 'required|exists:restaurants,id',
                'setting'         => 'required',
                'label'           => 'required|max:255',
                'status'          => 'sometimes|in:0,1',
                'is_selected'     => 'sometimes|in:0,1',
                'is_customizable' => 'sometimes|in:0,1',
                'is_preference'   => 'sometimes|in:0,1',
                'image'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'base_image'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'web_image'       => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'mobile_image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'light_left'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'light_whole'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'light_right'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'regular_left'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'regular_whole'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'regular_right'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'extra_left'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'extra_whole'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'extra_right'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $restId    = $request->input('restaurant_id');
            $settingId = $request->input('setting');
            $label     = strtolower($request->input('label'));

            $pizzaSetItem = [
                "label"         => $label,
                "price"         => number_format($request->input('price'), 2), //(float)$request->input('price'),
                "is_selected"   => (integer)$request->input('is_selected'),
                "is_spreadable" => (integer)$request->input('is_spreadable'),
                "is_preference" => (integer)$request->input('is_preference'),
                "status"        => (integer)$request->input('status'),
                "priority"      => (integer)$request->input('priority'),
            ];

            // check if specific restaurant setting exists
            $pizzaExp      = BuildYourOwnPizza::select('id', 'setting')->where(['restaurant_id' => $restId, 'id' => $settingId])->first();
            if($pizzaExp) {
                $oldSetting = $pizzaExp->setting;
                $imagePath     = config('constants.image.path.pizzaexp');
                $imageRelPath  = config('constants.image.rel_path.pizzaexp');
                $imageNameBase = $restId  . '_' .time(). '_' . str_replace(' ', '_', $label);

                $finalImageArr = [];
                $imageArr = [];
                // Mobile image
                if ($request->hasFile('mobile_image')) {
                    $imageName = $imageNameBase.'_mob';
                    $imageArr                   = $this->imageUpload($request, 'mobile_image', $imageName, $imagePath, $imageRelPath, true);
                    $finalImageArr['image']     = $imageArr['image'];
                    $finalImageArr['thumb_med'] = $imageArr['thumb_med'];
                    $finalImageArr['thumb_sml'] = $imageArr['thumb_sml'];
                } else {
                    $finalImageArr['image']     = null;
                    $finalImageArr['thumb_med'] = null;
                    $finalImageArr['thumb_sml'] = null;
                }
                // Web image
                if ($request->hasFile('web_image')) {
                    $imageName = $imageNameBase.'_web';
                    $imageArr                       = $this->imageUpload($request, 'web_image', $imageName, $imagePath, $imageRelPath, true);
                    $finalImageArr['web_image']     = $imageArr['image'];
                    $finalImageArr['web_thumb_med'] = $imageArr['thumb_med'];
                    $finalImageArr['web_thumb_sml'] = $imageArr['thumb_sml'];
                } else {
                    $finalImageArr['web_image']     = null;
                    $finalImageArr['web_thumb_med'] = null;
                    $finalImageArr['web_thumb_sml'] = null;
                }
                // Base image
                if ($request->hasFile('base_image')) {
                    $imageName                   = $imageNameBase . '_base';
                    $imageArr                    = $this->imageUpload($request, 'base_image', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['base_image'] = $imageArr['image'];
                } else {
                    $finalImageArr['base_image'] = null;
                }

                // 9 Images Combinations
                if ($request->hasFile('light_left')) {
                    $imageName                   = $imageNameBase . '_light_left';
                    $imageArr                    = $this->imageUpload($request, 'light_left', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['light_left'] = $imageArr['image'];
                } else {
                    $finalImageArr['light_left'] = null;
                }
                if ($request->hasFile('light_whole')) {
                    $imageName                    = $imageNameBase . '_light_whole';
                    $imageArr                     = $this->imageUpload($request, 'light_whole', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['light_whole'] = $imageArr['image'];
                } else {
                    $finalImageArr['light_whole'] = null;
                }
                if ($request->hasFile('light_right')) {
                    $imageName                    = $imageNameBase . '_light_right';
                    $imageArr                     = $this->imageUpload($request, 'light_right', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['light_right'] = $imageArr['image'];
                } else {
                    $finalImageArr['light_right'] = null;
                }
                if ($request->hasFile('regular_left')) {
                    $imageName                     = $imageNameBase . '_regular_left';
                    $imageArr                      = $this->imageUpload($request, 'regular_left', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['regular_left'] = $imageArr['image'];
                } else {
                    $finalImageArr['regular_left'] = null;
                }
                if ($request->hasFile('regular_whole')) {
                    $imageName                      = $imageNameBase . '_regular_whole';
                    $imageArr                       = $this->imageUpload($request, 'regular_whole', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['regular_whole'] = $imageArr['image'];
                } else {
                    $finalImageArr['regular_whole'] = null;
                }
                if ($request->hasFile('regular_right')) {
                    $imageName                      = $imageNameBase . '_regular_right';
                    $imageArr                       = $this->imageUpload($request, 'regular_right', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['regular_right'] = $imageArr['image'];
                } else {
                    $finalImageArr['regular_right'] = null;
                }
                if ($request->hasFile('extra_left')) {
                    $imageName                   = $imageNameBase . '_extra_left';
                    $imageArr                    = $this->imageUpload($request, 'extra_left', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['extra_left'] = $imageArr['image'];
                } else {
                    $finalImageArr['extra_left'] = null;
                }
                if ($request->hasFile('extra_whole')) {
                    $imageName                    = $imageNameBase . '_extra_whole';
                    $imageArr                     = $this->imageUpload($request, 'extra_whole', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['extra_whole'] = $imageArr['image'];
                } else {
                    $finalImageArr['extra_whole'] = null;
                }
                if ($request->hasFile('extra_right')) {
                    $imageName                    = $imageNameBase . '_extra_right';
                    $imageArr                     = $this->imageUpload($request, 'extra_right', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['extra_right'] = $imageArr['image'];
                } else {
                    $finalImageArr['extra_right'] = null;
                }

                $pizzaSetItem['image'] = $finalImageArr;
                if($request->has('size')){
                    $pizzaSetItem['size']        =$request->input('size')??'';

                }

                if($request->has('crust')){
                    $pizzaSetItem['crust']        =$request->input('crust')??'';

                }


                if($request->has('position_type')){

                    $position_type=$request->input('position_type');
                    $pizzaSetItem['positions']=[];

                    foreach($position_type as $key=> $position){
                        if(!empty($position)) {
                            $pos = $request->input('position')[$key] ?? '';
                            $position_price = $request->input('position_price')[$key] ?? '';
                            $is_selected =(isset( $request->input('is_selected_position')[$key]) && $request->input('is_selected_position')[$key]) ? 1:0;
                            $is_selected_type =(isset( $request->input('is_selected_type')[$key]) && $request->input('is_selected_type')[$key]) ? 1:0;

                            if (isset($request->file('position_image')[$key])) {
                                $imageName = $imageNameBase . '_posname';
                                $imageArr = $this->fileUpload($request->file('position_image')[$key], $imageName, $imagePath, $imageRelPath, false);
                                $position_image = $imageArr['image'];
                            } else {
                                $position_image = NULL;
                            }

                            $pizzaSetItem['positions'][] = ['id' => ($key + 1), 'type' => $position,'is_selected_type'=>$is_selected_type, 'position' => $pos, 'price' => $position_price,'is_selected'=>$is_selected, 'image' => $position_image];
                        }

                    }
                }

                // Create or Update setting item
                $pizzaExp = BuildYourOwnPizza::find($pizzaExp->id);
                if(is_null($oldSetting)) {
                    $pizzaExp->setting = json_encode([$pizzaSetItem]);
                } else {
                    $setting = json_decode($pizzaExp->setting, true);
                    array_push($setting, $pizzaSetItem);
                    $pizzaExp->setting = json_encode($setting);
                }
                $pizzaExp->save();
            }

            return Redirect::back()->with('message', 'Pizza Setting Item added successfully');

            /*$pizzaExpSetting = BuildYourOwnPizza::where('restaurant_id', $restId)
                ->select('id', $settingType)
                ->whereRaw('json_contains(' . $settingType . ', \'{"label": "' . $label . '"}\')')->first();

            $oldSetting = json_decode($pizzaExp->{$settingType}, true);
            //dd($pizzaExpSetting,$settingType,$newSetting,$oldSetting);
            if($oldSetting) {
                $index      = array_search(strtolower($label), array_map('strtolower', array_column($oldSetting, 'label')));
                if ($index !== false) {
                    // setting available, update setting values
                    $oldSetting[$index] = $newSetting;
                } else {
                    // Update setting with new array of data
                    array_push($oldSetting, $newSetting);
                }
            } else {
                // Update setting with new array of data
                $oldSetting = [$newSetting];
            }
            // Update settings
            $pizzaExp                 = BuildYourOwnPizza::find($pizzaExp->id);
            $pizzaExp->{$settingType} = json_encode($oldSetting);
            $pizzaExp->save();*/

        }

        return redirect('pizzaexpitem');
    }

    public function fileUpload($image, $imageName, $imagePath, $imageRelPath, $thumb = false)
    {
        $imageArr = [];
        //Image::configure(array('driver' => 'imagick'));


        $ext             = $image->getClientOriginalExtension();

       /* if($thumb) {
            $medImageName    = $imageName . '_med' . '@2x.' . $ext;
            $smlImageName    = $imageName . '_sml' . '@1x.' . $ext;
        }*/
        $imageName=$imageName.$image->getClientOriginalName();
        $smlImageName= $medImageName= $imageName       = $imageName . '@3x.' . $ext;
        $image->move($imagePath, $imageName);


        /*  $image           = Image::make($image->getRealPath());

          $width = $image->width();
          $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);*/
        //ImageOptimizer::optimize($pathToImage);
        if($thumb) {
        /*    $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);*/

            $imageArr = [
                'image'     => $imageRelPath . $imageName,
                'thumb_med'     => $imageRelPath . $imageName,
                'thumb_sml'     => $imageRelPath . $imageName,
              //  'thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
               // 'thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
            ];

        } else {
            $imageArr['image'] = $imageRelPath . $imageName;
        }

        return $imageArr;

    }


    public function imageUpload($request, $imageField, $imageName, $imagePath, $imageRelPath, $thumb = false)
    {
        //Image::configure(array('driver' => 'imagick'));
        $imageArr = [];
        if ($request->hasFile($imageField)) {

            $image        = $request->file($imageField);
            $ext             = $image->getClientOriginalExtension();

            $imageName=$imageName.$image->getClientOriginalName();
            $smlImageName= $medImageName= $imageName       = $imageName . '@3x.' . $ext;
            $image->move($imagePath, $imageName);


            /*if($thumb) {
                $medImageName    = $imageName . '_med' . '@2x.' . $ext;
                $smlImageName    = $imageName . '_sml' . '@1x.' . $ext;
            }*/
            /*$imageName       = $imageName . '@3x.' . $ext;
            $image           = Image::make($image->getRealPath());

            $width = $image->width();
            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);*/
            //ImageOptimizer::optimize($pathToImage);
            if($thumb) {
               /* $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);*/

                $imageArr = [
                    'image'     => $imageRelPath . $imageName,
                    'thumb_med'     => $imageRelPath . $imageName,
                    'thumb_sml'     => $imageRelPath . $imageName,
                   // 'thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                    //'thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                ];

            } else {
                $imageArr['image'] = $imageRelPath . $imageName;
            }

            return $imageArr;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $item = $request->input('item');

        // Pizza setting
        $pizzaSetting = BuildYourOwnPizza::find($id);
        $pizzaSetting->setting = json_decode($pizzaSetting->setting, true);

        $editItem = $pizzaSetting->setting[$item-1];

        // All restaurant data
        $allRests = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();

        // Get all Restaurant Pizza settings
        $parentSettingArr = BuildYourOwnPizza::select('id', 'label','setting')->where(['restaurant_id' => $pizzaSetting->restaurant_id, 'status' => 1])->get();

        //return response()->json([$item, $editItem]);
        return view('pizzaexp_item.edit', compact('allRests', 'pizzaSetting', 'parentSettingArr', 'editItem', 'item'));
    }
     /**
     * Show the form for destroy the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */	
    public function delete(Request $request,$id,$item)
    {
        //$item = $request->input('item');

        // Pizza setting
        $pizzaSetting = BuildYourOwnPizza::find($id);
        $pizzaSetting->setting = json_decode($pizzaSetting->setting, true);

        $setting = $pizzaSetting->setting;
	if(isset($setting[$item-1]))unset($setting[$item-1]);
	foreach($setting as $s){
		$setting1[]=$s;
	}	
	$pizzaSetting->setting = json_encode($setting1);
        $pizzaSetting->save();
 	return Redirect::back()->with('message', 'Pizza Setting Item deleted successfully');
         
    }
    /**

     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($id) && is_numeric($id)) {

            // static content validation
            $request->validate([
                'restaurant_id'   => 'required|exists:restaurants,id',
                'setting'         => 'required',
                'label'           => 'required|max:255',
                'status'          => 'sometimes|in:0,1',
                'is_selected'     => 'sometimes|in:0,1',
                'is_customizable' => 'sometimes|in:0,1',
                'is_preference'   => 'sometimes|in:0,1',
                'image'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'base_image'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'web_image'       => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'mobile_image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'light_left'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'light_whole'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'light_right'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'regular_left'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'regular_whole'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'regular_right'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'extra_left'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'extra_whole'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'extra_right'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            // setting item index, -1 for array
            $item = $request->input('item') - 1;
            $isImg = 0;
            // Update setting item
            $pizzaExp       = BuildYourOwnPizza::find($id);
            $oldSettingJson = json_decode($pizzaExp->setting, true);
            if (isset($oldSettingJson[$item])) {
                $label         = strtolower($request->input('label'));
                $restId        = $request->input('restaurant_id');
                $imagePath     = config('constants.image.path.pizzaexp');
                $imageRelPath  = config('constants.image.rel_path.pizzaexp');
                //$imageNameBase = $restId . '_' . str_replace(' ', '_', $label);
	        $imageNameBase = $restId  . '_' .time(). '_' . str_replace(' ', '_', $label);

                $finalImageArr =$oldSettingJson[$item]['image'];
                $imageArr      = [];

                // DELETE images
                if($request->input('base_image_delete') && $request->input('base_image_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['base_image']) && !empty($oldSettingJson[$item]['image']['base_image']) && is_file(public_path($oldSettingJson[$item]['image']['base_image']))){
                        unlink(public_path($oldSettingJson[$item]['image']['base_image']));
                    }

                    $finalImageArr['base_image'] = null;

                    $isImg=1;
                }
                if($request->input('web_image_delete') && $request->input('web_image_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['web_image']) && !empty($oldSettingJson[$item]['image']['web_image']) && is_file(public_path($oldSettingJson[$item]['image']['web_image']))){
                        unlink(public_path($oldSettingJson[$item]['image']['web_image']));
                    }

                    //unlink(public_path($oldSettingJson[$item]['image']['web_image']));
                    $finalImageArr['web_image']     = null;
                    $finalImageArr['web_thumb_med'] = null;
                    $finalImageArr['web_thumb_sml'] = null;

                    $isImg=1;
                }
                if($request->input('mobile_image_delete') && $request->input('mobile_image_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['mobile_image']) && !empty($oldSettingJson[$item]['image']['mobile_image']) && is_file(public_path($oldSettingJson[$item]['image']['mobile_image']))){
                        unlink(public_path($oldSettingJson[$item]['image']['mobile_image']));
                    }
                    //unlink(public_path($oldSettingJson[$item]['image']['mobile_image']));
                    $finalImageArr['image']     = null;
                    $finalImageArr['thumb_med'] = null;
                    $finalImageArr['thumb_sml'] = null;

                    $isImg=1;

                }
                if($request->input('light_left_delete') && $request->input('light_left_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['light_left']) && !empty($oldSettingJson[$item]['image']['light_left']) && is_file(public_path($oldSettingJson[$item]['image']['light_left']))){
                        unlink(public_path($oldSettingJson[$item]['image']['light_left']));
                    }
                    // unlink(public_path($oldSettingJson[$item]['image']['light_left']));
                    $finalImageArr['light_left'] = null;

                    $isImg=1;
                }
                if($request->input('light_whole_delete') && $request->input('light_whole_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['light_whole']) && !empty($oldSettingJson[$item]['image']['light_whole']) && is_file(public_path($oldSettingJson[$item]['image']['light_whole']))){
                        unlink(public_path($oldSettingJson[$item]['image']['light_whole']));
                    }
                    //unlink(public_path($oldSettingJson[$item]['image']['light_whole']));
                    $finalImageArr['light_whole'] = null;

                    $isImg=1;
                }
                if($request->input('light_right_delete') && $request->input('light_right_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['light_right']) && !empty($oldSettingJson[$item]['image']['light_right']) && is_file(public_path($oldSettingJson[$item]['image']['light_right']))){
                        unlink(public_path($oldSettingJson[$item]['image']['light_right']));
                    }
                    //unlink(public_path($oldSettingJson[$item]['image']['light_right']));
                    $finalImageArr['light_right'] = null;

                    $isImg=1;
                }
                if($request->input('regular_left_delete') && $request->input('regular_left_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['regular_left']) && !empty($oldSettingJson[$item]['image']['regular_left']) && is_file(public_path($oldSettingJson[$item]['image']['regular_left']))){
                        unlink(public_path($oldSettingJson[$item]['image']['regular_left']));
                    }
                    //unlink(public_path($oldSettingJson[$item]['image']['regular_left']));
                    $finalImageArr['regular_left'] = null;

                    $isImg=1;
                }
                if($request->input('regular_whole_delete') && $request->input('regular_whole_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['regular_whole']) && !empty($oldSettingJson[$item]['image']['regular_whole']) && is_file(public_path($oldSettingJson[$item]['image']['regular_whole']))){
                        unlink(public_path($oldSettingJson[$item]['image']['regular_whole']));
                    }
                    //unlink(public_path($oldSettingJson[$item]['image']['regular_whole']));
                    $finalImageArr['regular_whole'] = null;

                    $isImg=1;
                }
                if($request->input('regular_right_delete') && $request->input('regular_right_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['regular_right']) && !empty($oldSettingJson[$item]['image']['regular_right']) && is_file(public_path($oldSettingJson[$item]['image']['regular_right']))){
                        unlink(public_path($oldSettingJson[$item]['image']['regular_right']));
                    }
                    //unlink(public_path($oldSettingJson[$item]['image']['regular_right']));
                    $finalImageArr['regular_right'] = null;

                    $isImg=1;
                }
                if($request->input('extra_left_delete') && $request->input('extra_left_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['extra_left']) && !empty($oldSettingJson[$item]['image']['extra_left']) && is_file(public_path($oldSettingJson[$item]['image']['extra_left']))){
                        unlink(public_path($oldSettingJson[$item]['image']['extra_left']));
                    }
                    // unlink(public_path($oldSettingJson[$item]['image']['extra_left']));
                    $finalImageArr['extra_left'] = null;
                    $isImg=1;
                }
                if($request->input('extra_whole_delete') && $request->input('extra_whole_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['extra_whole']) && !empty($oldSettingJson[$item]['image']['extra_whole']) && is_file(public_path($oldSettingJson[$item]['image']['extra_whole']))){
                        unlink(public_path($oldSettingJson[$item]['image']['extra_whole']));
                    }
                    //unlink(public_path($oldSettingJson[$item]['image']['extra_whole']));
                    $finalImageArr['extra_whole'] = null;

                    $isImg=1;
                }
                if($request->input('extra_right_delete') && $request->input('extra_right_delete')=='1') {
                    if( isset($oldSettingJson[$item]['image']['extra_right']) && !empty($oldSettingJson[$item]['image']['extra_right']) && is_file(public_path($oldSettingJson[$item]['image']['extra_right']))){
                        unlink(public_path($oldSettingJson[$item]['image']['extra_right']));
                    }
                    // unlink(public_path($oldSettingJson[$item]['image']['extra_right']));
                    $finalImageArr['extra_right'] = null;
                    $isImg=1;
                }


                // Mobile image
                if ($request->hasFile('mobile_image')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['mobile_image']))) { unlink(public_path($oldSettingJson[$item]['image']['mobile_image'])); }
                    $imageName                  = $imageNameBase . '_mob';
                    $imageArr                   = $this->imageUpload($request, 'mobile_image', $imageName, $imagePath, $imageRelPath, true);
                    $finalImageArr['image']     = $imageArr['image'];
                    $finalImageArr['thumb_med'] = $imageArr['thumb_med'];
                    $finalImageArr['thumb_sml'] = $imageArr['thumb_sml'];
                    $isImg=1;
                }
                // Web image
                if ($request->hasFile('web_image')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['web_image']))) { unlink(public_path($oldSettingJson[$item]['image']['web_image'])); }
                    $imageName                      = $imageNameBase . '_web';
                    $imageArr                       = $this->imageUpload($request, 'web_image', $imageName, $imagePath, $imageRelPath, true);
                    $finalImageArr['web_image']     = $imageArr['image'];
                    $finalImageArr['web_thumb_med'] = $imageArr['thumb_med'];
                    $finalImageArr['web_thumb_sml'] = $imageArr['thumb_sml'];
                    $isImg=1;
                }
                // Base image
                if ($request->hasFile('base_image')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['base_image']))) { unlink(public_path($oldSettingJson[$item]['image']['base_image'])); }
                    $imageName                   = $imageNameBase . '_base';
                    $imageArr                    = $this->imageUpload($request, 'base_image', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['base_image'] = $imageArr['image'];
                    $isImg=1;
                }
                // 9 Images Combinations
                if ($request->hasFile('light_left')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['light_left']))) { unlink(public_path($oldSettingJson[$item]['image']['light_left'])); }
                    $imageName                   = $imageNameBase . '_light_left';
                    $imageArr                    = $this->imageUpload($request, 'light_left', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['light_left'] = $imageArr['image'];
                    $isImg=1;
                }
                if ($request->hasFile('light_whole')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['light_whole']))) { unlink(public_path($oldSettingJson[$item]['image']['light_whole'])); }
                    $imageName                    = $imageNameBase . '_light_whole';
                    $imageArr                     = $this->imageUpload($request, 'light_whole', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['light_whole'] = $imageArr['image'];
                    $isImg=1;
                }
                if ($request->hasFile('light_right')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['light_right']))) { unlink(public_path($oldSettingJson[$item]['image']['light_right'])); }
                    $imageName                    = $imageNameBase . '_light_right';
                    $imageArr                     = $this->imageUpload($request, 'light_right', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['light_right'] = $imageArr['image'];
                    $isImg=1;
                }
                if ($request->hasFile('regular_left')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['regular_left']))) { unlink(public_path($oldSettingJson[$item]['image']['regular_left'])); }
                    $imageName                     = $imageNameBase . '_regular_left';
                    $imageArr                      = $this->imageUpload($request, 'regular_left', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['regular_left'] = $imageArr['image'];
                    $isImg=1;
                }
                if ($request->hasFile('regular_whole')) {
                    // if(file_exists(public_path($oldSettingJson[$item]['image']['regular_whole']))) { unlink(public_path($oldSettingJson[$item]['image']['regular_whole'])); }
                    $imageName                      = $imageNameBase . '_regular_whole';
                    $imageArr                       = $this->imageUpload($request, 'regular_whole', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['regular_whole'] = $imageArr['image'];
                    $isImg=1;

                }
                if ($request->hasFile('regular_right')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['regular_right']))) { unlink(public_path($oldSettingJson[$item]['image']['regular_right'])); }
                    $imageName                      = $imageNameBase . '_regular_right';
                    $imageArr                       = $this->imageUpload($request, 'regular_right', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['regular_right'] = $imageArr['image'];
                    $isImg=1;
                }
                if ($request->hasFile('extra_left')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['extra_left']))) { unlink(public_path($oldSettingJson[$item]['image']['extra_left'])); }
                    $imageName                   = $imageNameBase . '_extra_left';
                    $imageArr                    = $this->imageUpload($request, 'extra_left', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['extra_left'] = $imageArr['image'];
                    $isImg=1;
                }
                if ($request->hasFile('extra_whole')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['extra_whole']))) { unlink(public_path($oldSettingJson[$item]['image']['extra_whole'])); }
                    $imageName                    = $imageNameBase . '_extra_whole';
                    $imageArr                     = $this->imageUpload($request, 'extra_whole', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['extra_whole'] = $imageArr['image']; $isImg=1;
                }
                if ($request->hasFile('extra_right')) {
                    //if(file_exists(public_path($oldSettingJson[$item]['image']['extra_right']))) { unlink(public_path($oldSettingJson[$item]['image']['extra_right'])); }
                    $imageName                    = $imageNameBase . '_extra_right';
                    $imageArr                     = $this->imageUpload($request, 'extra_right', $imageName, $imagePath, $imageRelPath, false);
                    $finalImageArr['extra_right'] = $imageArr['image']; $isImg=1;
                }

                $oldSettingJson[$item]['label']         = $label;
                $oldSettingJson[$item]['price']         = number_format($request->input('price'), 2); //(float)$request->input('price');
                $oldSettingJson[$item]['is_selected']   = (integer)$request->input('is_selected');
                $oldSettingJson[$item]['is_spreadable'] = (integer)$request->input('is_spreadable');
                $oldSettingJson[$item]['is_preference'] = (integer)$request->input('is_preference');
                $oldSettingJson[$item]['status']        = (integer)$request->input('status');
                $oldSettingJson[$item]['priority']      = (integer)$request->input('priority');
                if( $isImg==1)$oldSettingJson[$item]['image']         = $finalImageArr;

                if($request->has('size')){
                    $oldSettingJson[$item]['size']        =$request->input('size')??'';
                }
                if($request->has('pos_id')){
                    $oldSettingJson[$item]['pos_id']        =$request->input('pos_id')??'';
                }

                if($request->has('crust')){
                    $oldSettingJson[$item]['crust']        =$request->input('crust')??'';
                }
                if(!$request->has('position_type')){
                    $oldSettingJson[$item]['positions']=[];
                }else{

                    if(!isset($oldSettingJson[$item]['positions']) || (isset($oldSettingJson[$item]['positions']) && !count($oldSettingJson[$item]['positions']))){

                        if($request->has('position_type')){

                            $position_type=$request->input('position_type');
                            $pizzaSetItem['positions']=[];

                            foreach($position_type as $key=> $position) {
                                if(!empty($position)) {
                                    $pos = $request->input('position')[$key] ?? '';
                                    $position_price = $request->input('position_price')[$key] ?? '';
                                    $is_selected =(isset( $request->input('is_selected_position')[$key]) && $request->input('is_selected_position')[$key]) ? 1:0;
                                    $is_selected_type =(isset( $request->input('is_selected_type')[$key]) && $request->input('is_selected_type')[$key]) ? 1:0;


                                    if (@$request->file('position_image')[$key]) {
                                        $imageName = $imageNameBase . '_posname';
                                        $imageArr = $this->fileUpload($request->file('position_image')[$key], $imageName, $imagePath, $imageRelPath, false);
                                        $position_image = $imageArr['image'];
                                    } else {
                                        $position_image = NULL;
                                    }
                                    if (!empty($pos) && !empty($position_price)) {
                                        $oldSettingJson[$item]['positions'][] = ['id' => $key, 'type' => $position,'is_selected_type'=>$is_selected_type, 'position' => $pos, 'price' => $position_price,'is_selected'=>$is_selected, 'image' => $position_image];

                                    }
                                }

                            }
                        }
                    }else{
                        // dd($oldSettingJson);
                        $old_positions=$oldSettingJson[$item]['positions'];
                        /**********************Delete OLd data**************************/
                        $newpositions=$request->input('position_type_id');
                        $old_id = array_filter($newpositions);
                        $updated_positions=[];
                        $last_id='';
                        if(count($old_positions)){
                            foreach ($old_positions as $pos){
                                $last_id=(int)$pos['id'];
                                if (in_array($pos['id'], $old_id)){
                                    //  unset($pos);
                                    $updated_positions[]=$pos;

                                }
                            }
                        }

                        $newPosItem =$updated_positions;


                        /************************************************/

                        /**********************Add new data**************************/

                        $position_type=$request->input('position_type');
                        $new_additions=[];

                        foreach($position_type as $key=> $position){

                            if(empty($request->input('position_type_id')[$key])){

                                if(!empty($position)) {
                                    // print('hi1');

                                    $pos = $request->input('position')[$key] ?? '';
                                    $position_price = $request->input('position_price')[$key] ?? '';
                                    $is_selected =(isset( $request->input('is_selected_position')[$key]) && $request->input('is_selected_position')[$key]) ? 1:0;
                                    $is_selected_type =(isset( $request->input('is_selected_type')[$key]) && $request->input('is_selected_type')[$key]) ? 1:0;
                                    $pos_id = $request->input('position_pos_id')[$key] ?? '';


                                    if (@$request->file('position_image')[$key]) {
                                        $imageName = $imageNameBase . '_posname';
                                        $imageArr = $this->fileUpload($request->file('position_image')[$key], $imageName, $imagePath, $imageRelPath, false);
                                        $position_image = $imageArr['image'];
                                    } else {
                                        $position_image = NULL;
                                    }

                                    if (!empty($pos) && !empty($position_price)) {
                                        $last_id = $last_id + 1;
                                        $new_additions[] = ['id' => $last_id,'pos_id'=>$pos_id, 'type' => $position,'is_selected_type'=>$is_selected_type, 'position' => $pos, 'price' => $position_price,'is_selected'=>$is_selected, 'image' => $position_image];
                                    }

                                }

                            }else{

                                foreach ($updated_positions as $upkey=>$posup){
                                    // print('hi2-');
                                    //dump($posup);
                                    if(trim($posup['id'])==trim($request->input('position_type_id')[$key])) {
                                        $newmatched=[];
                                        //dump(trim($posup['id']));
                                        $newmatched['id']=$posup['id'];
                                        $newmatched['type'] = $request->input('position_type')[$key] ?? '';
                                        $newmatched['pos_id'] = $request->input('position_pos_id')[$key] ?? '';
                                        $newmatched['position'] = $request->input('position')[$key] ?? '';
                                        $newmatched['price'] = $request->input('position_price')[$key] ?? '';
                                        $newmatched['is_selected'] = (isset( $request->input('is_selected_position')[$key]) && $request->input('is_selected_position')[$key]) ? 1:0;
                                        $newmatched['is_selected_type'] = (isset( $request->input('is_selected_type')[$key]) && $request->input('is_selected_type')[$key]) ? 1:0;
                                        if (@$request->file('position_image')[$key]) {
                                            //   dump($posup['image']);
                                            //  dump('---'.trim($request->input('position_type_id')[$key]));
                                            $imageName = $imageNameBase . '_posname';
                                            $imageArr = $this->fileUpload($request->file('position_image')[$key], $imageName, $imagePath, $imageRelPath, false);
                                            $newmatched['image'] = $imageArr['image'];
                                        } else {

                                            $newmatched['image'] = $posup['image'];
                                        }

                                        $newPosItem[$upkey]=$newmatched;


                                    }

                                }

                            }

                        }

                        if(count($new_additions)){
                            $newPosItem=array_merge($newPosItem,$new_additions);
                        }

                        /************************************************/
                        $oldSettingJson[$item]['positions']=$newPosItem;
                    }

                }

            }


            $pizzaExp->setting = json_encode($oldSettingJson);
            $pizzaExp->save();



            return Redirect::back()->with('message', 'Pizza Setting Item updated successfully');
        }

        return redirect('pizzaexpitem');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function findSimilarMatch($input, $haystackArr) {
        // no shortest distance found, yet
        $shortest = -1;
        // loop through words to find the closest
        foreach ($haystackArr as $word) {

            // calculate the distance between the input word,
            // and the current word
            $lev = levenshtein($input, $word);

            // check for an exact match
            if ($lev == 0) {

                // closest word is this one (exact match)
                $closest  = $word;
                $shortest = 0;

                // break out of the loop; we've found an exact match
                break;
            }

            // if this distance is less than the next found shortest
            // distance, OR if a next shortest word has not yet been found
            if ($lev <= $shortest || $shortest < 0) {
                // set the closest match, and shortest distance
                $closest  = $word;
                $shortest = $lev;
            }
        }

        return $closest;
    }
}
