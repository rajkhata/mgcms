<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserOrder;
use App\Models\CmsUser;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;
use Modules\Reservation\Entities\Reservation;
use Modules\Reservation\Entities\ReservationStatus;
use App\Helpers\CommonFunctions;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth; 
use App\Models\Restaurant;
use App\Models\StaticBlockResponse;
use App\Models\BYW\RestaurantTemplates;
use App\Models\BYW\BYWDomains;
use Session;

class HomeController extends Controller
{
    public $folderPath ="/var/www/html/cms-qc-byw/public/deploy/";
    public $domainFolderPath="/var/www/html/cms-qc-byw/domainconfig/";
    //public $domainFolderPath="/var/www/html/BYWRestaurantSetup1/domainconfig/";
    public $deployedPath ='/var/www/html/website/byw-compiled/template';
    public $domain = null;
    public $qc = 1;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:dashboard');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function getHtmlOfGrowthRate($getGrowthRate){
        if($getGrowthRate < 0){
            $growthRateHtml = '<div class="text-color-red font-weight-600 growth-sign">';
            $growthRateHtml.= '<span class="icon-rise-down margin-right-10 font-size-12 growth-sign"></span>';
            $growthRateHtml.= $getGrowthRate;
            $growthRateHtml.= '%</div>';
        } else if($getGrowthRate == 0){
            $growthRateHtml = '<div style="height:25px"></div>';
        } else {
            $growthRateHtml = '<div class="text-color-green font-weight-600 growth-sign">';
            $growthRateHtml.= '<span class="icon-rise-up margin-right-10 font-size-12 growth-sign"></span>+';
            $growthRateHtml.= $getGrowthRate;
            $growthRateHtml.= '%</div>';
        }

        return $growthRateHtml;
    }

    public function index(Request $request)
    {
        return Redirect::to('/mng_order/1/food_item');

        //$restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        //$restDetails = Restaurant::select('*')->where(['restaurants.id' => $restArray[0]])->first();
        $role = $conditions = $restaurantId = $requestData = '';
        $getGrowthRateDaySort = 30;
        $role = \Auth::user()->role;
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantId = $restArray[0];
        if((Session::has('restaurant_id')) && ($role=="admin")){
            $restaurantId = Session::get('restaurant_id');
            if(\Request::has('restaurant_id')){
                $restaurantId = \Request::input('restaurant_id');
            }
            if($restaurantId=="All"){
                $requestData = $restaurantId;
                $restaurantId = $restArray[0];
            }
        }
        $restDetails = Restaurant::select('*')->where(['restaurants.id' => $restaurantId])->first();
//echo $restaurantId; die;
        //if(Route::currentRouteName() != 'dashboard'){
            if($restDetails['template_id']<1)
                return Redirect::to('/template-setup');
            /*if($restDetails['template_id']>0 && $restDetails['template_state']<1)
                return Redirect::to('/profile-setup');*/
        //}
        $accountOverviewResArray = $revenueRes = $revenuetrends = $reachRes = $menuItemsRes = $accountOverviewVisitorsResArray = '';
        $visitorsRes = $accountOverviewGrowthRateResArray = $htmlSpendGrowthRate = $htmlReachGrowthRate = '';
        $htmlConversionsGrowthRate = $htmlRevenueGrowthRate = $htmlVisitorsGrowthRate = '';
        $getDashboardDaySort = $socialReachResArray = $socialReachQuery = $visitorChartResArray = $paidVisitorChartResArray = '';

        $currencySymbol = $restDetails['currency_symbol'];
        $currencyRate = $restDetails['currency_rate'];
        if(!is_null($restDetails->deal_id)) {
            $dealId = $restDetails->deal_id; //"251235081"; //"249901828";
            $clientId = $restDetails->client_id;
            $dealJoin = '';
            //$conditions = CommonFunctions::getQueryCondition($restDetails, $requestData, $role);
            try {

                $getDateFormatForAllDays = "'%b-%y'";
                $getAllDataDate = "group by year(date),month(date) order by year(date),month(date)";
                $getAllDataDay = "group by year(day),month(day) order by year(day),month(day)";
                $getAllDataDateStart = "group by year(date_start),month(date_start) order by year(date_start),month(date_start)";

                if($request->has('dashboardDaySort') && $request->input('dashboardDaySort') != "") {
                    $getDashboardDaySort = $request->input('dashboardDaySort');
                    Session::put('sort_by', $getDashboardDaySort);
                    //Changes for 30 Days
                    if($getDashboardDaySort == "30"){
                        $getDateFormatForAllDays = "'%e-%b'";
                        $getAllDataDate = "group by year(date),month(date),day(date) order by year(date),month(date),day(date)";
                        $getAllDataDay = "group by year(day),month(day),day(day) order by year(day),month(day),day(day)";
                        $getAllDataDateStart = "group by year(date_start),month(date_start),day(date_start) order by year(date_start),month(date_start),day(date_start)";
                    }

                } elseif($dealId=='100000001') {
                    $getDashboardDaySort = 30;
                    $getDateFormatForAllDays = "'%e-%b'";
                    $getAllDataDate = "group by year(date),month(date),day(date) order by year(date),month(date),day(date)";
                    $getAllDataDay = "group by year(day),month(day),day(day) order by year(day),month(day),day(day)";
                    $getAllDataDateStart = "group by year(date_start),month(date_start),day(date_start) order by year(date_start),month(date_start),day(date_start)";
                }else{
                    $getDashboardDaySort = 30;
                }
                if (!$request->has('dashboardDaySort') && $request->input('dashboardDaySort') == "" && Session::has('sort_by')) {
                     $getDashboardDaySort = Session::get('sort_by');
                }

                if($role=="admin" && !Session::has('restaurant_id') && $requestData!="All"){
                $conditions = '';//'client_id ='.$restDetails->client_id;
                $dealJoin = "select distinct a.client_id, a.name , a.email , a.phone, sum(a.total_amount) as `total_amount` , min(a.date) as `first_activity` , max(a.date) as `last_activity`, 
                ifnull(b.ct_type,0) as ct_orders , ifnull(ct_reservation,0) as ct_reservation from tb_customer_details a left join vw_orders_".$getDashboardDaySort."_customer_details b on a.client_id = b.client_id and a.email = b.email
                left join vw_reservation_".$getDashboardDaySort."_customer_details c on a.client_id = c.client_id and a.email = c.email where a.client_id = $restDetails->client_id and a.date BETWEEN CURDATE() - INTERVAL ".$getDashboardDaySort." DAY AND CURDATE() group by a.client_id , a.email order by ct_type, total_amount desc limit 5;";
                }
                else if($role=="admin" && Session::has('restaurant_id') && $requestData=="All"){
                $conditions = 'client_id ='.$restDetails->client_id;
                $dealJoin = "select distinct a.client_id, a.name , a.email , a.phone , 
                sum(a.total_amount) as `total_amount` , min(a.date) as `first_activity` , max(a.date) as `last_activity`, 
                ifnull(b.ct_type,0) as ct_orders , ifnull(ct_reservation,0) as ct_reservation 
                from tb_customer_details a 
                left join vw_orders_".$getDashboardDaySort."_customer_details b 
                on a.client_id = b.client_id and a.email = b.email
                left join vw_reservation_".$getDashboardDaySort."_customer_details c 
                on a.client_id = c.client_id and a.email = c.email
                where  a.date BETWEEN CURDATE() - INTERVAL ".$getDashboardDaySort." DAY AND CURDATE() 
                group by a.client_id , a.email order by ct_type desc, total_amount desc limit 5;";
                }
                else if($role=="manager"){
                    $dealJoin = "select distinct a.client_id, a.deal_id , a.name , a.email , a.phone , 
                sum(a.total_amount) as `total_amount` , min(a.date) as `first_activity` , max(a.date) as `last_activity`, 
                ifnull(b.ct_type,0) as ct_orders , ifnull(ct_reservation,0) as ct_reservation 
                from tb_customer_details a 
                left join vw_orders_".$getDashboardDaySort."_customer_details b 
                on a.client_id = b.client_id and a.deal_id = b.deal_id and a.email = b.email
                left join vw_reservation_".$getDashboardDaySort."_customer_details c 
                on a.client_id = c.client_id and a.deal_id = c.deal_id and a.email = c.email
                where  a.deal_id = $restDetails->deal_id
                and a.date BETWEEN CURDATE() - INTERVAL ".$getDashboardDaySort." DAY AND CURDATE() 
                group by a.client_id , a.deal_id , a.email order by ct_type desc, total_amount desc limit 5;";
                    $conditions = ' deal_id ='.$restDetails->deal_id;
                }
                else if($role=="admin" && Session::has('restaurant_id') && $requestData!="All"){
                    $conditions = ' deal_id ='.$restDetails->deal_id;
                    $dealJoin = "select distinct a.client_id, a.deal_id , a.name , a.email , a.phone , 
                sum(a.total_amount) as `total_amount` , min(a.date) as `first_activity` , max(a.date) as `last_activity`, 
                ifnull(b.ct_type,0) as ct_orders , ifnull(ct_reservation,0) as ct_reservation 
                from tb_customer_details a 
                left join vw_orders_".$getDashboardDaySort."_customer_details b 
                on a.client_id = b.client_id and a.deal_id = b.deal_id and a.email = b.email
                left join vw_reservation_".$getDashboardDaySort."_customer_details c 
                on a.client_id = c.client_id and a.deal_id = c.deal_id and a.email = c.email
                where   a.deal_id = $restDetails->deal_id
                and a.date BETWEEN CURDATE() - INTERVAL ".$getDashboardDaySort." DAY AND CURDATE() 
                group by a.client_id , a.deal_id , a.email order by ct_type desc, total_amount desc limit 5;";
                }

                $flag = '';
		if($conditions!="")$conditions =" where" . $conditions;	
                $checkFlag = "select  multi_site from GA_BASE_DTLS_DEALID $conditions limit 1;";
                $dataArray = $siteVisitorsOverviewRes = DB::connection('mysql2')->select($checkFlag);
                if(!empty($dataArray)) {
                    $flag = $dataArray['0']->multi_site;
                    if ($flag != '') {
                        $flag = $dataArray['0']->multi_site;
                    }
                }
		$clientId= 10000;
		if($conditions!="")$conditions = "where $conditions";
                //$accountOverview = "select spend , views as reach, clicks ,  Conversions, revenue from tb_db1_overview_".$getDashboardDaySort." where deal_id = ".$dealId ;//"SELECT tb_dash01_tile.`deal_id` AS `deal_id`, ROUND(SUM(`tb_dash01_tile`.`spend`), 2) AS `spend`, SUM(`tb_dash01_tile`.`impressions`) AS `reach`, SUM(`tb_dash01_tile`.`orders`) AS `Conversions`, ROUND(SUM(`tb_dash01_tile`.`revenue`), 2) AS `revenue` FROM reporting_dashboard.tb_dash01_tile where deal_id = {$dealId} and day BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() GROUP BY `tb_dash01_tile`.`deal_id`";
                $accountOverview = "select sum(spend) as spend,sum(views) as reach,sum(clicks) as clicks,sum(Conversions) as Conversions,sum(revenue) as revenue from tb_db1_overview_".$getDashboardDaySort." $conditions group by client_id";
                //$accountOverviewVisitors = "select Visitors from tb_db1_overview_visitors_".$getDashboardDaySort." where deal_id =".$dealId ;
                //$accountOverviewVisitors = "select Visitors from tb_db1_overview_visitors_".$getDashboardDaySort." where client_id =".$clientId;
                if($flag==1){
                    $accountOverviewVisitors = "select  Visitors from tb_db1_overview_visitors_".$getDashboardDaySort." where  deal_id = $dealId";
                    //$visitorsChartQery = "select distinct day , sum(Visitors) as Visitors from tb_db1_visitor_".$getDashboardDaySort." where $conditions group by client_id , day order by date asc";
                    $visitorsChartQery = "select  distinct(day) as day , sum(Visitors) as Visitors from reporting_dashboard.tb_db1_channel_visitor_".$getDashboardDaySort." where $conditions and SourceChannel = 'Organic' group by client_id , date;";
                    $visitorsPaidChartQery = "select  distinct(day) as day , sum(Visitors) as Visitors from reporting_dashboard.tb_db1_channel_visitor_".$getDashboardDaySort." where $conditions and SourceChannel = 'Paid' group by client_id , date;";

                } else{
                    $accountOverviewVisitors = "select  Visitors from tb_db1_overview_visitors_".$getDashboardDaySort." where client_id =".$clientId;
                    //$visitorsChartQery = "select distinct day , sum(Visitors) as Visitors from tb_db1_visitor_".$getDashboardDaySort." where client_id =".$clientId." group by client_id , day order by date asc";
                    $visitorsChartQery = "select  distinct(day) as day , sum(Visitors) as Visitors from reporting_dashboard.tb_db1_channel_visitor_".$getDashboardDaySort." where client_id =".$clientId." and SourceChannel = 'Organic' group by client_id , date;";
                    $visitorsPaidChartQery = "select  distinct(day) as day , sum(Visitors) as Visitors from reporting_dashboard.tb_db1_channel_visitor_".$getDashboardDaySort." where client_id =".$clientId." and SourceChannel = 'Paid' group by client_id , date;";
                }
                //"SELECT SUM(`GA_BASE_DTLS_DEALID`.`users`) AS `Visitors` FROM `GA_BASE_DTLS_DEALID` where deal_id = {$dealId} and date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() GROUP BY `GA_BASE_DTLS_DEALID`.`deal_id`;";
                $accountOverviewGrowthRate = "select spend_growth , reach_growth , Conversions_growth , revenue_growth from tb_dash_01_growth_vw_{$getDashboardDaySort} where deal_id = {$dealId}";
                //$accountOverviewVisitorsGrowthRate = "select vistior_growth  from reporting_dashboard.tb_dash_01_vistior_growth_vw_{$getDashboardDaySort} where deal_id = {$dealId}";
                $growthRateSpendOverview = "call  sp_growth_rate('spend','monthly_new_reports','day',{$getGrowthRateDaySort},{$dealId})";
                $growthRateConversionsOverview = "call  sp_growth_rate('orders','monthly_new_reports','day',{$getGrowthRateDaySort},{$dealId})";

                $growthRateReachOverview = "call  sp_growth_rate('impressions','monthly_new_reports','day',{$getGrowthRateDaySort},{$dealId})";
                //$growthRateReachOverview = "call sp_growth_rate('reach','facebook_campaign_data_dealid','date_start',{$getGrowthRateDaySort},{$dealId})";

                $growthRateVisitorsOverview = "call  sp_growth_rate('users','GA_BASE_DTLS_DEALID','date',{$getGrowthRateDaySort},{$dealId})";
                $growthRateRevenueOverview = "call  sp_growth_rate('revenue','monthly_new_reports','day',{$getGrowthRateDaySort},{$dealId})";

                //$revenueChartQery = "Select day , revenue from tb_db1_rev_reach_".$getDashboardDaySort." where deal_id =".$dealId;//"SELECT DISTINCT DATE_FORMAT(`day`, {$getDateFormatForAllDays}) AS `day`, ROUND(SUM(`revenue`), 2) AS `revenue` FROM `monthly_new_reports` where deal_id = {$dealId} and day BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() {$getAllDataDay}";
                //$revenueChartQery = "select  distinct(day) as day , sum(revenue) as revenue from tb_db1_rev_reach_".$getDashboardDaySort." where $conditions group by client_id , day order by date asc;";
                $revenueChartQery = "select distinct User_Type , round(sum(revenue),2) as revenue from reporting_dashboard.customer_type_last_".$getDashboardDaySort."_day where $conditions group by User_Type;";

                $revenueTrendChartQery = "select  distinct(day) as day, round(sum(revenue),2) as revenuetrend from tb_db1_rev_reach_".$getDashboardDaySort." where $conditions group by client_id , day order by date asc;";

                //$reachChartQery = "Select day , reach from tb_db1_rev_reach_".$getDashboardDaySort." where deal_id = ".$dealId;//"SELECT DISTINCT DATE_FORMAT(`day`, {$getDateFormatForAllDays}) AS `day`, SUM(`impressions`) AS `reach` FROM `monthly_new_reports` where deal_id = {$dealId} and day BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() {$getAllDataDay}";
                //$reachChartQery = "select  distinct(day) as day , sum(reach) as reach from tb_db1_rev_reach_".$getDashboardDaySort." where $conditions group by client_id , day order by date asc";
                $reachChartQery = "select distinct(day) as day , round(sum(reach)) as impression , sum(spend) as spend from reporting_dashboard.tb_db1_rev_reach_".$getDashboardDaySort." where $conditions group by client_id , day order by date;";

                //$mostPopularItemQery = "select item , ct_item from tb_db1_mostpopitem_".$getDashboardDaySort." where deal_id = ".$dealId." limit 10";//"select item , ct_item from tb_db1_mostpopitem_".$getDashboardDaySort." where deal_id = ".$dealId." limit 10";//"select distinct item , sum(quantity) as `ct_item` from reporting_dashboard.menu_item_dealid_vw where deal_id = {$dealId} and order_date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by item order by sum(quantity) desc limit 10";
                $mostPopularItemQery = "select  distinct item , sum(ct_item) as ct_item from tb_db1_mostpopitem_".$getDashboardDaySort." where $conditions group by item order by ct_item desc limit 10";

                $socialReachQuery = "select distinct day , sum(social_engagment) as fb_reach from tb_db1_social_reach_".$getDashboardDaySort." where $conditions group by client_id , day order by date_start asc;";
                /*select distinct a.deal_id as deal_id , concat(a.fname,' ',a.lname) as `name` , a.email as email , a.phone as phone, sum(a.total_amount) as `total_amount` , min(a.date) as `first_activity` , max(a.date) as `last_activity`, b.ct_type as ct_orders , ifnull(ct_reservation,0) as ct_reservation from tb_customer_details a left join vw_orders_{$getDashboardDaySort}_customer_details b on a.deal_id = b.deal_id and concat(a.fname,' ',a.lname) = b.name left join vw_reservation_{$getDashboardDaySort}_customer_details c on a.deal_id = c.deal_id and concat(a.fname,' ',a.lname) = c.name where a.deal_id = {$dealId} and fname <> '0' and a.date BETWEEN CURDATE() - INTERVAL {$getDashboardDaySort} DAY AND CURDATE() group by a.deal_id , a.phone order by ct_type desc, total_amount desc limit 5*/
                $mostLoyalCustomerQuery = $dealJoin;

                $accountOverviewRes = null;//DB::connection('mysql2')->select($accountOverview);
                $accountOverviewVisitorsRes =  null;//DB::connection('mysql2')->select($accountOverviewVisitors);
                $accountOverviewGrowthRateRes = null;// DB::connection('mysql2')->select($accountOverviewGrowthRate);
                //$accountOverviewVisitorsGrowthRateRes =  null;//DB::connection('mysql2')->select($accountOverviewVisitorsGrowthRate);
                $mostLoyalCustomerQueryRes = null;//DB::connection('mysql2')->select($mostLoyalCustomerQuery);
                $socialReachRes =  null;//DB::connection('mysql2')->select($socialReachQuery);

                $growthRateSpendOverviewRes =  null;//DB::connection('mysql2')->select($growthRateSpendOverview);
                $growthRateConversionsOverviewRes =  null;//DB::connection('mysql2')->select($growthRateConversionsOverview);
                $growthRateReachOverviewRes =  null;//DB::connection('mysql2')->select($growthRateReachOverview);
                $growthRateVisitorsOverviewRes = null;// DB::connection('mysql2')->select($growthRateVisitorsOverview);
                $growthRateRevenueOverviewRes = null;// DB::connection('mysql2')->select($growthRateRevenueOverview);

                $revenueRes =  null;//DB::connection('mysql2')->select($revenueChartQery);
                $revenuetrends =  null;//DB::connection('mysql2')->select($revenueTrendChartQery);
                //$visitorsRes = DB::connection('mysql2')->select($visitorsChartQery);

                $visitorsRes =  null;//DB::connection('mysql2')->select($visitorsChartQery);
                $paidVisitorChartRes =  null;//DB::connection('mysql2')->select($visitorsPaidChartQery);


                $reachRes =  null;//DB::connection('mysql2')->select($reachChartQery);

                //print_r(json_encode($reachRes)); die;
                $menuItemsRes =  null;//DB::connection('mysql2')->select($mostPopularItemQery);
                $accountOverviewResArray = json_encode($accountOverviewRes);
                $accountOverviewVisitorsResArray = json_encode($accountOverviewVisitorsRes);
                $accountOverviewGrowthRateResArray = json_encode($accountOverviewGrowthRateRes);
                $growthRateSpendOverviewResArray = json_encode($growthRateSpendOverviewRes);
                $growthRateConversionsOverviewResArray = json_encode($growthRateConversionsOverviewRes);
                $growthRateReachOverviewResArray = json_encode($growthRateReachOverviewRes);
                $growthRateVisitorsOverviewResArray = json_encode($growthRateVisitorsOverviewRes);
                $growthRateRevenueOverviewResArray = json_encode($growthRateRevenueOverviewRes);
                //$accountOverviewVisitorsGrowthRateResArray = json_encode($accountOverviewVisitorsGrowthRateRes);
                $socialReachResArray = json_encode($socialReachRes);

                $visitorChartResArray = json_encode($visitorsRes);
                $paidVisitorChartResArray = json_encode($paidVisitorChartRes);

                //print_r($visitorChartResArray);
                //print_r($paidVisitorChartResArray);
                //die;
                //$mostLoyalCustomerQueryResArray = json_encode($mostLoyalCustomerQueryRes);

                //dd($growthRateReachOverviewResArray);

                $getSpendGrowthRateRes = isset(json_decode($growthRateSpendOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateSpendOverviewResArray, true)[0]['@growth']),0) : 0;
                $getReachGrowthRateRes = isset(json_decode($growthRateReachOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateReachOverviewResArray, true)[0]['@growth']),0) : 0;
                $getVisitorsGrowthRateRes = isset(json_decode($growthRateVisitorsOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateVisitorsOverviewResArray, true)[0]['@growth']),0) : 0;
                $getConversionsGrowthRateRes = isset(json_decode($growthRateConversionsOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateConversionsOverviewResArray, true)[0]['@growth']),0) : 0;
                $getRevenueGrowthRateRes = isset(json_decode($growthRateRevenueOverviewResArray, true)[0]) ? number_format(round(json_decode($growthRateRevenueOverviewResArray, true)[0]['@growth']),0) : 0;

                $htmlSpendGrowthRate = self::getHtmlOfGrowthRate($getSpendGrowthRateRes);
                $htmlReachGrowthRate = self::getHtmlOfGrowthRate($getReachGrowthRateRes);
                $htmlVisitorsGrowthRate = self::getHtmlOfGrowthRate($getVisitorsGrowthRateRes);
                $htmlConversionsGrowthRate = self::getHtmlOfGrowthRate($getConversionsGrowthRateRes);
                $htmlRevenueGrowthRate = self::getHtmlOfGrowthRate($getRevenueGrowthRateRes);

            }catch (Exception $e){

            }
        }
        //echo"<pre>";print_r($restDetails);die;

        $statusArray = array("placed", "confirmed", "ready");

        // $orderData = UserOrder::select('id','order_type','total_amount','created_at','delivery_time','delivery_date','status')
        //     ->whereIn('restaurant_id', $restArray)
        //     ->whereIn('status', $statusArray)->orderBy('id', 'DESC')
        //     ->paginate(3);

        # Food Oders total
        $orderData = UserOrder::select('order_type', 'total_amount', 'delivery_date','delivery_time', 'restaurant_id','created_at')
            ->whereIn('restaurant_id', $restArray)
            ->where('product_type','food_item')
            ->whereIn('status', $statusArray)->orderBy('id', 'DESC')
            //->groupBy('delivery_date', 'order_type')
            ->orderBy('delivery_date', 'desc')
            ->get();

        $total_order_count = count($orderData);
        // if($orderData) {
        //     foreach ($orderData as $odr) {
        //         $total_order_count = $total_order_count + $odr['order_count'];
        //       }  
        // }

         # Merchandise Oders 
            
        $merchandiseOrderData = UserOrder::select('order_type', 'total_amount', 'delivery_date', 'restaurant_id', 'user_orders.created_at', 'user_order_details.item',DB::raw('COUNT(user_order_details.id) AS num_item'))
            ->whereIn('restaurant_id', $restArray)
            ->where('product_type','product')
             ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC')
            //->groupBy(DB::raw('Date(created_at)'))
            //->groupBy('order_type')
            ->orderBy('user_orders.created_at', 'desc')
             ->groupBy('user_orders.id')
            ->get();
        //echo "<pre>";print_r($orderData->toArray());die;
        //echo "<pre>";print_r($merchandiseOrderData->toArray());die;

        $total_merchandise_count =  count($merchandiseOrderData);
        // if($merchandiseOrderData) {
        //     foreach ($merchandiseOrderData as $odr) {
        //         $total_merchandise_count = $total_merchandise_count + $odr['order_count'];
        //     }  
        // }

        #total Reservation

        $reserveData = Reservation::select('id','start_time','created_at','reserved_seat')
            ->whereIn('restaurant_id', $restArray)
            ->where('start_time', '>', Carbon::now())
            //->groupBy(DB::raw('Date(start_time)'))
            ->orderBy('start_time', 'desc')
            ->get();


        $total_resr_count = count($reserveData);
        // if($reserveData) {
        //     foreach ($reserveData as $reservation) {
        //         $total_resr_count = $total_resr_count + $reservation['reservation_count'];
        //       }  
        // }
       
        ## Statictics ##

        // As discussed with Adit, Rahul Prabhakar, Praksh, Pragun and Deepak Chandna and : Total Orders are the orders which have been given to restaurant will include all order status except pending means credit card not charged @22-11-2018
        #email subject: Confirmation on the changes for new Web Dashboard 25-10-2018
        $allstatusArray = array("pending");
        $allOrders = UserOrder::select('id', DB::raw('count(id) as total_orders'))->whereIn('restaurant_id', $restArray)->whereNotIn('status', $allstatusArray)->get();
        $totalOrder = $allOrders['0']->total_orders;

        #Revenuue will be for only full-filled orders
        #closed = Fully Refunded status @22-11-2018
        
        $allstatusArray = array("pending", "cancelled", "rejected", "closed");

        $allOrdersum = UserOrder::select('id', DB::raw('sum(total_amount) as total_amount'))->whereIn('restaurant_id', $restArray)->whereNotIn('status', $allstatusArray)->get();
        $orderAmount = $allOrdersum['0']->total_amount ? $allOrdersum['0']->total_amount : 0;
       
        #New Customers: User who combine done reservation and orders means sum == 1
        $user = Auth::user();
        $assigned_rest_id =  $user->restaurant_id;

        $restaurantData = Restaurant::where('id', $assigned_rest_id)->first(['id','parent_restaurant_id'])->toArray();
        if($restaurantData) {
            if($restaurantData['parent_restaurant_id']) {
                $parent_id = $restaurantData['parent_restaurant_id'];
                $restaurant_array = $assigned_rest_id;
            }else {
                $parent_id = $restaurantData['id']; 
                $restaurant_array = implode(',', $restArray);
            }
            $parents = "users.restaurant_id = $parent_id";
        }else {
            $parents = 1;
        }
        
        ##NEW CUSTOMER QUERY

        #$newCustomer = DB::SELECT("SELECT count(d.new_user_count) as new_customers from (SELECT users.id as new_user_count FROM `users` left join user_orders on users.id = user_orders.user_id left join user_restaurant_resv on users.id = user_restaurant_resv.user_id  WHERE $parents and (user_orders.restaurant_id IN ($restaurant_array) OR  user_restaurant_resv.restaurant_id IN ($restaurant_array))  and  ( (user_orders.user_id IS NOT NULL) OR  (user_restaurant_resv.user_id IS NOT NULL) ) group by users.id having(count(users.id) = 1) order by users.id DESC) d");

        $newCustomer = DB::SELECT("SELECT count(d.new_user_count) as new_customers from (SELECT users.id as new_user_count FROM `users` left join user_orders on users.id = user_orders.user_id left join user_restaurant_resv on users.id = user_restaurant_resv.user_id  WHERE $parents   group by users.id having(count(users.id) = 1) order by users.id DESC) d");

        if($newCustomer && isset($newCustomer['0']) &&  $newCustomer['0']->new_customers) {
            $customerNew = $newCustomer['0']->new_customers;
        }else {
            $customerNew = 0;
        }


        ## Repeated CUSTOMER QUERY

        #$repeatCustomer = DB::SELECT("SELECT count(d.new_user_count) as new_customers from (SELECT users.id as new_user_count FROM `users` left join user_orders on users.id = user_orders.user_id left join user_restaurant_resv on users.id = user_restaurant_resv.user_id  WHERE $parents and (user_orders.restaurant_id IN ($restaurant_array) OR  user_restaurant_resv.restaurant_id IN ($restaurant_array))  and  ( (user_orders.user_id IS NOT NULL) OR  (user_restaurant_resv.user_id IS NOT NULL) ) group by users.id having(count(users.id) > 1) order by users.id DESC) d");

        $repeatCustomer = DB::SELECT("SELECT count(d.new_user_count) as new_customers from (SELECT users.id as new_user_count FROM `users` left join user_orders on users.id = user_orders.user_id left join user_restaurant_resv on users.id = user_restaurant_resv.user_id  WHERE $parents  group by users.id having(count(users.id) > 1) order by users.id DESC) d");

        if($repeatCustomer && isset($repeatCustomer['0']) &&  $repeatCustomer['0']->new_customers) {
            $repeated = $repeatCustomer['0']->new_customers;
        }else {
            $repeated = 0;
        }

        $totalReserve = Reservation::whereIn('restaurant_id', $restArray)->get();
        $reservedSheet = $totalReserve->sum('reserved_seat') ;

        #check Reservation allowed
        $reservation_locations = Restaurant::whereIn('id', $restArray)->select(DB::raw('sum(reservation) as reservation_count'), DB::raw('sum(is_merchandise_allowed) as merchandise_count') )->get()->toArray();
        $enquiry = StaticBlockResponse::whereIn('restaurant_id', $restArray)
                            ->where('status', 1)->limit(6)->orderBy('created_at', 'DESC')->get();

        //echo "<pre>";print_r($reservation_locations);die;                 

        // Classic cards option commented and not coded for it due to requirement & "tonight" deadline
        $statusKey = '';
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $statusArray = array("placed", "confirmed", "ready");
        $giftOrderData = UserOrder::select('user_orders.product_type','user_orders.total_amount','user_orders.created_at', 'user_orders.restaurant_id')
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC')
            ->where('user_orders.product_type','=',"gift_card")->orderBy('user_orders.id', 'desc')->limit(5);
        $giftOrderDataTotal = $giftOrderData->count();
        $giftOrderData = $giftOrderData->get();

        // Added null variable to stop "undefined variable" error msg
        //$mostLoyalCustomerQueryRes = null;

        return view('home', compact('orderData', 'total_order_count', 'merchandiseOrderData', 'total_merchandise_count' ,
            'giftOrderData', 'giftOrderDataTotal', 'reserveData', 'total_resr_count', 'totalOrder','orderAmount', 'customerNew',
            'repeated','reservedSheet', 'reservation_locations', 'enquiry', 'accountOverviewResArray', 'revenueRes', 'revenuetrends', 'reachRes', 'menuItemsRes',
            'accountOverviewVisitorsResArray', 'visitorChartResArray', 'paidVisitorChartResArray', 'accountOverviewGrowthRateResArray','htmlSpendGrowthRate', 'htmlReachGrowthRate',
            'htmlConversionsGrowthRate', 'htmlRevenueGrowthRate', 'htmlVisitorsGrowthRate', 'getDashboardDaySort', 'mostLoyalCustomerQueryRes', 'socialReachResArray', 'currencySymbol','currencyRate','dealId'));

    }

    public function setLanguage(Request $request)
    {
        $setLanguage = $request->input('selected_language');
        session()->put('lang', $setLanguage);

        return Redirect::back()->withSuccess('Language set successfully!');
    }
    public function templatesetup(Request $request){
	 if($request->input('id') != null){
		$restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
		 $restDetails = Restaurant::select('*')->where(['restaurants.id' => $restArray[0]])->first();
		 
		 if($restDetails['parent_restaurant_id']>0){	
			 $inputArr['template_id'] = $request->input('id') ?? 0;
			Restaurant::where('id', $restArray[0])->update($inputArr);
		 }return Redirect::to('/profile-setup');
	 }else{
	 	$templateDetails = RestaurantTemplates::select('*')->where(['status' => 1])->get();
	 	return view('BYWRestaurant.template-setup',compact('templateDetails'));
	}
    }
    public function setrestauranttheme(Request $request){
	 $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
	 $restDetails = Restaurant::select('*')->where(['restaurants.id' => $restArray[0]])->first();
	 if($restDetails['parent_restaurant_id']>0){	
		$inputArr['template_id'] = $request->input('id') ?? 0;
		Restaurant::where('id', $restArray[0])->update($inputArr);
         $subdomanName = str_replace(" ","-",$restDetails['restaurant_name']);

         $this->cloneSubDomain($subdomanName,$inputArr['template_id'],$restDetails['parent_restaurant_id']);
		$domain = BYWDomains::select('*')->where('restaurant_id',$restDetails['parent_restaurant_id'])->get()->toArray();
		if(isset($domain[0]['domain']))
		$this->deployCodeBase($domain[0]['domain'],$domain[0]['vhostinfo'],$inputArr['template_id'],$restDetails['parent_restaurant_id']);
	 }
	 return Redirect::to('/profile-setup');
    }
    function createFile($fileName,$data){
       	$my_file = $fileName;
	 
	if(file_exists($my_file)){
	   unlink($my_file);
	}	
	$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
	fwrite($handle, $data, strlen($data));
	fclose($handle); 
	return true;
    }	
    public function deployCodeBase($subdomanName,$vhost,$templateid,$restid){

        //$fileName = $this->folderPath.'restaurantName.txt';

        $domainconfigFile = $this->domainFolderPath.$subdomanName.'__'.$templateid.'__'.$restid.'__config.conf';
        $this->createFile($domainconfigFile,$vhost);
        $runShFile = $this->folderPath.'run.txt';
        $this->createFile($runShFile,"testinggg");
        //$frontendConfFileName = "root-config.ts";
        //$configContent = $this->createFrontEndConfigFile($p_rest_id,$location_id);
        //$configContent = $this->cloneSubDomain($restaurantName);
        return true;     
    }


    public function cloneSubDomain($domainName,$templateid,$new_restaurant_id){
        if($this->qc==1) {
            $this->domain = 'munchado.it';
            $ssl = "SSLCertificateFile /etc/apache2/munchadoitssl/certificate.crt
            SSLCertificateKeyFile /etc/apache2/munchadoitssl/private.key
            SSLCACertificateFile /etc/apache2/munchadoitssl/ca_bundle.crt";
        }else {
            $ssl = "SSLCertificateFile /etc/apache2/munchadoshowcase.biz/certificate.crt
            SSLCertificateKeyFile /etc/apache2/munchadoshowcase.biz/private.key
            SSLCACertificateFile /etc/apache2/munchadoshowcase.biz/ca_bundle.crt";
            $this->domain = 'munchadoshowcase.biz';
        }
        $subDomainContent="<VirtualHost *:80>
            ServerName $domainName.munchadoshowcase.biz
             
            ServerAdmin ashokbhati.php@gmail.com
            Redirect permanent / https://$domainName.$this->domain/
            DocumentRoot ".$this->deployedPath.$templateid."/
        <Directory ".$this->deployedPath.$templateid."/>
                    DirectoryIndex index.html index.cgi index.pl index.php
                    Options MultiViews FollowSymLinks
                    Require all granted
                    AllowOverride All
                    Order allow,deny
                    Allow from all
            </Directory>
            SetEnv APPLICATION_ENV production
        </VirtualHost>
        
        <VirtualHost *:443>
            ServerName $domainName.$this->domain
             
            ServerAdmin ashokbhati.php@gmail.com
            SSLEngine on
            $ssl
\           DocumentRoot ".$this->deployedPath.$templateid."/
        <Directory ".$this->deployedPath.$templateid."/>
                    DirectoryIndex  index.php index.html index.cgi index.pl
                    Options MultiViews FollowSymLinks
                    Require all granted
                    AllowOverride All
            AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript image/jpeg image/jpg image/png
        </Directory>
        ##SetEnv APPLICATION_ENV demo
        

        </VirtualHost>";
        $d['domain']=$domainName;
        $d['restaurant_id']=$new_restaurant_id;
        $d['vhostinfo']= $subDomainContent;
        $m = BYWDomains::create($d);
    }
}
