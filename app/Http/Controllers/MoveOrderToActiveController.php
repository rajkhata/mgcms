<?php

namespace App\Http\Controllers;


use App\Models\Restaurant;
use App\Models\UserOrder;
use App\Models\FinancialRecord;
use App\Models\UserOrderDetail;
use App\Models\UserOrderAddons;
use App\Models\OrderStatuses;
use Illuminate\Http\Request;
use App\Http\Controllers\NotificationController;
//use Illuminate\Support\Facades\DB;
use App\Helpers\CommonFunctions;
use Carbon\Carbon;    

use DB;
use App\Models\PaymentGatewayLog;
use App\Helpers\ApiPayment;
use App\Models\DeliveryServicesLogs;
use App\Models\CmsUserLog; 
use App\Helpers\Payment\StripeGateway;
use App\Helpers\Stuarts;
use App\Models\StuartOrders;
use App\Models\StuartOrdersLog; 
use App\Models\PaymentGateway;
use App\Models\DeliveryService;
 

class MoveOrderToActiveController extends Controller
{
   ### if Order is place stage in case of order already has been delivered or takeout
    public function asapOrderRoute(){
	$statusArray = array("placed");
	//asap order ---case
	$oData = UserOrder::select('user_orders.payment_receipt','user_orders.delivery_time','user_orders.delivery_date','user_orders.restaurant_id','user_orders.latitude','user_orders.longitude',
		'user_orders.restaurant_id','restaurants.parent_restaurant_id','user_orders.routed_restaurant_id','user_orders.id')
		->whereIn('user_orders.status',$statusArray)->whereIn('order_state',[1,0])->where('is_urgent',0)->where('is_asap_order',1)
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
		->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id') 
		->whereRaw('DATE_ADD(user_orders.created_at , INTERVAL 4 MINUTE)<= CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
		 
	 
	$oData = $oData->get()->toArray();
	 echo "--asapOrderRoute--"; print_r($oData);//die;
	$nearestRestId[0]=1;
	foreach($oData as $order){
		$restIdNotIn[]= $order['restaurant_id'];
		$nearestRestId[$order['id']] = $this->reAllocateNearestBar($order,$restIdNotIn);
		$nearestRestId[$order['id']]['delivery_time'] = $order['delivery_time'];
		$nearestRestId[$order['id']]['delivery_date'] = $order['delivery_date'];
		$nearestRestId[$order['id']]['payment_receipt'] = $order['payment_receipt'];
		$nearestRestId[$order['id']]['routed_from_restaurant_id'] = $order['restaurant_id'];
		$nearestRestId[$order['id']]['routed_restaurant_id'] = $order['routed_restaurant_id'];
	} 
	$orderList =null; 
	if(isset($nearestRestId)){
	 //add restaturant routing to orders
	  foreach($nearestRestId as $oId=>$data){
		if($data!=null && isset($data['id'])){//restaurnt in 2.5 mile
		   $f1 = FinancialRecord::where("restaurant_id",$data['id'])->where("ma_order_id",$oId)
		 	->where("order_type",'Routed')->first();
		   $process =1;
		   $f2 = FinancialRecord::where("ma_order_id",$oId)->where("order_type",'Routed')->orderBy('id','desc')->first();
		   if(isset($f2->order_process_time)){
			$process =(time() > ($f2->order_process_time + 240))?1:0;
		   }	
		   if(!isset($f1->id) && $process==1){
			$orderList[] =$oId; 
			 echo "--asapOrderRoute-loop--"; print_r($data);//die;
		  		
			$newDeliveryTime=date("Y-m-d H:i",(strtotime($data['delivery_date']." ".$data['delivery_time']) + 240)); // 4 mins added
			$newDeliveryTime = explode(" ",$newDeliveryTime);
			UserOrder::whereIn('id', [$oId])->update(['is_urgent' => 1, 'routed_restaurant_id' => $data['id'],'delivery_time'=>$newDeliveryTime[1],'delivery_date'=>$newDeliveryTime[0]]);
			//SELECT * FROM `user_orders` WHERE `created_at` BETWEEN '2020-09-15 00:00:00.000000' AND '2020-09-15 23:59:55.000000' ORDER BY `id` DESC 
			$p = FinancialRecord::whereBetween('created_at', [date('Y-m-d')." 00:00:00", date('Y-m-d')." 23:59:59"])
				->where("routed_from_restaurant_id",$data['routed_from_restaurant_id'])
				->where("order_type",'Routed')->get()->toArray();
			$total_amount=5;			
			if(isset($p) && count($p)<2)
			$total_amount = 5;
			if(isset($p) && count($p)==2)
			$total_amount = 10;
			if(isset($p) && count($p)>2)
			$total_amount = 15;
			FinancialRecord::create(["order_process_time"=>time(),"restaurant_id"=>$data['routed_from_restaurant_id'],'routed_from_restaurant_id'=>$data['routed_from_restaurant_id'],
			"ma_order_id"=>$oId,"order_id"=>$data['payment_receipt'],"amount"=>$total_amount,"order_type"=>'Routed',"payment_type"=>"Debit","status"=>1]);
			$this->orderRouteMail($oId,$data['id']);
		   }
		} 
	}}
	$this->asapOrderCancel($orderList); 
	
	unset($nearestRestId);
    }
    public function asapOrderCancel($orderList){
	$statusArray = array("placed");

	//cancel loop asap
	//asap order ---case
	$oData = UserOrder::select('user_orders.payment_receipt','user_orders.delivery_time','user_orders.delivery_date','user_orders.restaurant_id','user_orders.latitude','user_orders.longitude',
		'user_orders.restaurant_id','restaurants.parent_restaurant_id','user_orders.routed_restaurant_id','user_orders.id')
		->whereIn('order_state',[1,0])->where('is_urgent',0)->where('is_asap_order',1)->whereIn('user_orders.status',$statusArray)
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
		->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id') 
		->whereRaw('DATE_ADD(user_orders.created_at , INTERVAL 5 MINUTE)<= CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
		//->whereRaw('DATE_ADD(DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE) , INTERVAL 4 MINUTE)< CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
 		//->whereRaw('DATE_ADD(concat(user_orders.delivery_date," ",user_orders.delivery_time) , INTERVAL 5 MINUTE)< CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
	 	//->whereIn('user_orders.status',$statusArray)->where('user_orders.id',715)
	if($orderList!=null)$oData =$oData->whereNotIn('user_orders.id',$orderList);
	$oData = $oData->get()->toArray();
	
	  echo "--asapOrderCancel --"; print_r($oData);//die;
	if(isset($oData)){
		//add restaturant routing to orders
		foreach($oData as  $data){
		   
		   //cancel and refund order
		   $process =1;
		   $f2 = FinancialRecord::where("ma_order_id",$data['id'])->where("order_type",'Routed')->orderBy('id','desc')->first();
		   if(isset($f2->order_process_time)){
			$process =(time() > ($f2->order_process_time + 240))?1:0;
		   }	
		   if($process==1){	 echo "--asapOrderCancel-loop--";print_r($data);
			 	$orderList[] = $data['id'];
				$this->changeOrderStatuscancel($data['id'], "Your order has been cancelled, a refund has been initiated for your order as no bar is available for delivery at the moment.", 'Cancelled',
				 $data['restaurant_id'], 0, 0);
			     	 $rdata = ['refund_mode'=>'fully','refund_flat_or_percentage'=>'flat','refund_fp_value'=>"",'refund_type'=>'F','oid'=>$data['id'],'refund_value'=>'',
				'host_name'=>'Ginie Martini','reason'=>'A refund has been initiated for your order as no bar is available for delivery at the moment.','reason_other'=>''];	
				$s=$this->releaseOrderRefund($rdata,$data['restaurant_id']);
				$total_amount=5;
				$p = FinancialRecord::whereBetween('created_at', [date('Y-m-d')." 00:00:00", date('Y-m-d')." 23:59:59"])
				->where("restaurant_id",$data['restaurant_id'])
				->where("order_type",'Cancelled')->get()->toArray();			
				if(isset($p) && count($p)<2)
				$total_amount = 5;
				if(isset($p) && count($p)==2)
				$total_amount = 10;
				if(isset($p) && count($p)>2)
				$total_amount = 15;			
				FinancialRecord::create(["order_process_time"=>time(),"restaurant_id"=>$data['restaurant_id'],'routed_from_restaurant_id'=>$data['restaurant_id'],
				"ma_order_id"=>$data['id'],"order_id"=>$data['payment_receipt'],"amount"=>$total_amount,"order_type"=>'Cancelled',"payment_type"=>"Debit","status"=>1]);
		       }
		}
	 } 
	$this->activeOrderRoute($orderList);
	
    }	
    public function activeOrderRoute($orderList){	
	    $statusArray = array("placed");
	//schedule order---case
        $oData = UserOrder::select('user_orders.payment_receipt','user_orders.delivery_time','user_orders.delivery_date','user_orders.restaurant_id','user_orders.latitude','user_orders.longitude',
		'user_orders.restaurant_id','restaurants.parent_restaurant_id','user_orders.routed_restaurant_id','user_orders.id')
		->whereIn('user_orders.status',$statusArray)->whereIn('order_state',[1,0])->where('is_urgent',0)->where('is_asap_order',0)  
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
		->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id') 
 		->whereRaw('DATE_ADD(DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE) , INTERVAL 4 MINUTE)<= CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
	 
	if($orderList!=null)$oData =$oData->whereNotIn('user_orders.id',$orderList);
	$oData = $oData->get()->toArray();$nearestRestId[0]=1;
	  echo "--activeOrderRoute --"; print_r($oData);//die;
	foreach($oData as $order){
		$nearestRestId[$order['id']] = $this->reAllocateNearestBar($order);
		$nearestRestId[$order['id']]['delivery_time'] = $order['delivery_time'];
		$nearestRestId[$order['id']]['delivery_date'] = $order['delivery_date'];
		$nearestRestId[$order['id']]['payment_receipt'] = $order['payment_receipt'];
		$nearestRestId[$order['id']]['routed_from_restaurant_id'] = $order['restaurant_id'];
		$nearestRestId[$order['id']]['routed_restaurant_id'] = $order['routed_restaurant_id'];
	}print_r($nearestRestId);
	if(isset($nearestRestId)){
	 //add restaturant routing to orders
	  foreach($nearestRestId as $oId=>$data){
		if($data!=null && isset($data['id'])){//restaurnt in 2.5 mile
		   $f1 = FinancialRecord::where("restaurant_id",$data['id'])->where("ma_order_id",$oId)
		 	->where("order_type",'Routed')->first();
		   $process =1;
		   $f2 = FinancialRecord::where("ma_order_id",$oId)->where("order_type",'Routed')->orderBy('id','desc')->first();
		   if(isset($f2->order_process_time)){
			$process =(time() > ($f2->order_process_time + 240))?1:0;
		   }	
		   if(!isset($f1->id) && $process==1){
			$orderList[] = $oId;
		 	 echo "--activeOrderRoute -loop--"; print_r($data);	
			$newDeliveryTime=date("Y-m-d H:i",(strtotime($data['delivery_date']." ".$data['delivery_time']) + 240)); // 4 mins added
			$newDeliveryTime = explode(" ",$newDeliveryTime);
			UserOrder::whereIn('id', [$oId])->update(['is_urgent' => 1, 'routed_restaurant_id' => $data['id'],'delivery_time'=>$newDeliveryTime[1],'delivery_date'=>$newDeliveryTime[0]]);
			//SELECT * FROM `user_orders` WHERE `created_at` BETWEEN '2020-09-15 00:00:00.000000' AND '2020-09-15 23:59:55.000000' ORDER BY `id` DESC 
			$p = FinancialRecord::whereBetween('created_at', [date('Y-m-d')." 00:00:00", date('Y-m-d')." 23:59:59"])
				->where("routed_from_restaurant_id",$data['routed_from_restaurant_id'])
				->where("order_type",'Routed')->get()->toArray();
			$total_amount=5;			
			if(isset($p) && count($p)<2)
			$total_amount = 5;
			if(isset($p) && count($p)==2)
			$total_amount = 10;
			if(isset($p) && count($p)>2)
			$total_amount = 15;
			FinancialRecord::create(["order_process_time"=>time(),"restaurant_id"=>$data['routed_from_restaurant_id'],'routed_from_restaurant_id'=>$data['routed_from_restaurant_id'],
			"ma_order_id"=>$oId,"order_id"=>$data['payment_receipt'],"amount"=>$total_amount,"order_type"=>'Routed',"payment_type"=>"Debit","status"=>1]);
			$this->orderRouteMail($oId,$data['id']);
		   }
		} 
	}}
	$this->activeOrderCancel($orderList);  
	
	unset($nearestRestId);
    }	
    public function activeOrderCancel($orderList){
	    $statusArray = array("placed");
	$oData = UserOrder::select('user_orders.payment_receipt','user_orders.delivery_time','user_orders.delivery_date', 'user_orders.latitude','user_orders.longitude',
		'user_orders.restaurant_id','restaurants.parent_restaurant_id','user_orders.routed_restaurant_id','user_orders.id')
		->whereIn('user_orders.status',$statusArray)->whereIn('order_state',[1,0])->where('is_urgent',0)->where('is_asap_order',0)  
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
		->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id') 
 		->whereRaw('DATE_ADD(DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE) , INTERVAL 5 MINUTE)<= CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
	if($orderList!=null)$oData =$oData->whereNotIn('user_orders.id',$orderList);
	$oData = $oData->get()->toArray();
	//cancel loop
	 echo "--activeOrderCancel --"; print_r($oData);	
	if(isset($oData)){
		//add restaturant routing to orders
		foreach($oData as  $data){
		  
		        //cancel and refund order
			 $process =1;
		   	$f2 = FinancialRecord::where("ma_order_id",$data['id'])->where("order_type",'Routed')->orderBy('id','desc')->first();
		   	if(isset($f2->order_process_time)){
				$process =(time() > ($f2->order_process_time + 240))?1:0;
		   	}	
		   	if($process==1){  echo "--activeOrderCancel -loop--"; print_r($oData);	
			 	$orderList[] = $data['id'];
				$this->changeOrderStatuscancel($data['id'], "Your order has been cancelled, a refund has been initiated for your order as no bar is available for delivery at the moment.", 'Cancelled',
				 $data['restaurant_id'], 0, 0);
			     	 $rdata = ['refund_mode'=>'fully','refund_flat_or_percentage'=>'flat','refund_fp_value'=>"",'refund_type'=>'F','oid'=>$data['id'],'refund_value'=>'',
				'host_name'=>'Ginie Martini','reason'=>'A refund has been initiated for your order as no bar is available for delivery at the moment.','reason_other'=>''];	
				$s=$this->releaseOrderRefund($rdata,$data['restaurant_id']);
				$total_amount=5;
				$p = FinancialRecord::whereBetween('created_at', [date('Y-m-d')." 00:00:00", date('Y-m-d')." 23:59:59"])
				->where("restaurant_id",$data['restaurant_id'])
				->where("order_type",'Cancelled')->get()->toArray();			
				  if(isset($p) && count($p)<2)
				 $total_amount = 5;
				 if(isset($p) && count($p)==2)
					$total_amount = 10;
					if(isset($p) && count($p)>2)
					$total_amount = 15;		
				FinancialRecord::create(["order_process_time"=>time(),"restaurant_id"=>$data['restaurant_id'],'routed_from_restaurant_id'=>$data['restaurant_id'],
				"ma_order_id"=>$data['id'],"order_id"=>$data['payment_receipt'],"amount"=>$total_amount,"order_type"=>'Cancelled',"payment_type"=>"Debit","status"=>1]);
		       }
		}
	 } $this->reroutedOrderReoute($orderList);
	 
    }
    public function reroutedOrderReoute($orderList){
	 $statusArray = array("placed");
	// re-routed orders
	$oDataRe = UserOrder::select('user_orders.payment_receipt','user_orders.delivery_time','user_orders.delivery_date','user_orders.restaurant_id','user_orders.latitude','user_orders.longitude',
		 'restaurants.parent_restaurant_id','user_orders.routed_restaurant_id','user_orders.id') 
		->whereIn('user_orders.status',$statusArray)->whereIn('order_state',[1,0])  
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.routed_restaurant_id')->where('is_urgent',1) 
		->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id') 
 		->whereRaw('DATE_ADD(DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE) , INTERVAL 4 MINUTE)< CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
		if($orderList!=null)$oDataRe =$oDataRe->whereNotIn('user_orders.id',$orderList);
		$oDataRe = $oDataRe->get()->toArray();
	 	//print_r($oDataRe);//die;
		$nearestRestId[0]=1;
		 echo "--activeOrderRoute-Route --"; print_r($oDataRe);	
		foreach($oDataRe as $order){
			$idNotIn[] =$order['restaurant_id'];
			if(isset($order['routed_restaurant_id']) && $order['routed_restaurant_id']>0)$idNotIn[] =$order['routed_restaurant_id'];
			$financialRecord = FinancialRecord::select('restaurant_id')->where('ma_order_id',$order['id'])->whereIn('order_type',['Routed'])->get()->toArray();
			 
	    		if(isset($financialRecord)){
				foreach($financialRecord as $id){
					$idNotIn[] = $id['restaurant_id'];
				}
	    		}  
			$nearestRestId[$order['id']] = $this->reAllocateNearestBar($order,$idNotIn);
			$nearestRestId[$order['id']]['delivery_time'] = $order['delivery_time'];
			$nearestRestId[$order['id']]['delivery_date'] = $order['delivery_date'];
			$nearestRestId[$order['id']]['payment_receipt'] = $order['payment_receipt'];
			$nearestRestId[$order['id']]['routed_from_restaurant_id'] = $order['restaurant_id'];
			//$nearestRestId[$order['id']]['re_routed_from_restaurant_id'] = ($order['routed_restaurant_id']>0)?$order['routed_restaurant_id']:$order['restaurant_id'];
			$nearestRestId[$order['id']]['routed_restaurant_id'] =  $order['routed_restaurant_id'];
		} 
		
	if(isset($nearestRestId)){
		//add restaturant routing to orders
		foreach($nearestRestId as $oId=>$data){
			if($data!=null && isset($data['id'])){//restaurnt in 2.5 mile
		   $f1 = FinancialRecord::where("restaurant_id",$data['id'])->where("ma_order_id",$oId)
		 	->where("order_type",'Routed')->first();
		   $process =1;
		   $f2 = FinancialRecord::where("ma_order_id",$oId)->where("order_type",'Routed')->orderBy('id','desc')->first();
		   if(isset($f2->order_process_time)){
			$process =(time() > ($f2->order_process_time + 240))?1:0;
		   }	
		   if(!isset($f1->id) && $process==1){
			$orderList[] =  $oId;
		   	echo "--activeOrderRoute-Route-loop --"; print_r($data);	
			$newDeliveryTime=date("Y-m-d H:i",(strtotime($data['delivery_date']." ".$data['delivery_time']) + 240)); // 4 mins added
			$newDeliveryTime = explode(" ",$newDeliveryTime);//->whereNotIn('restaurant_id', [$data['id']])
			$uOrder = UserOrder::whereIn('id', [$oId])->update(['is_urgent' => 1, 'routed_restaurant_id' => $data['id'],'delivery_time'=>$newDeliveryTime[1],'delivery_date'=>$newDeliveryTime[0]]);
			$p = FinancialRecord::whereBetween('created_at', [date('Y-m-d')." 00:00:00", date('Y-m-d')." 23:59:59"])
				->where("restaurant_id",$data['routed_restaurant_id'])
				->where("order_type",'Routed')->get()->toArray();
			$total_amount=5;			
			if(isset($p) && count($p)<2)
			$total_amount = 5;
			if(isset($p) && count($p)==2)
			$total_amount = 10;
			if(isset($p) && count($p)>2)
			$total_amount = 15;
			  FinancialRecord::create(["order_process_time"=>time(),"restaurant_id"=>$data['routed_restaurant_id'],'routed_from_restaurant_id'=>$data['routed_from_restaurant_id'],
			"ma_order_id"=>$oId,"order_id"=>$data['payment_receipt'],"amount"=>$total_amount,"order_type"=>'Routed',"payment_type"=>"Debit","status"=>1]);
			 $this->orderRouteMail($oId,$data['id']);
		   }
		}
	} }$this->reroutedOrderCancel($orderList);
	unset($nearestRestId);
    }
    public function reroutedOrderCancel($orderList){
	$statusArray = array("placed");
	//cancel loop
	$oDataRe = UserOrder::select('user_orders.payment_receipt','user_orders.delivery_time','user_orders.delivery_date','user_orders.restaurant_id','user_orders.latitude','user_orders.longitude',
		'user_orders.restaurant_id as rsid','restaurants.parent_restaurant_id','user_orders.routed_restaurant_id','user_orders.id')
		->whereIn('user_orders.status',$statusArray)->whereIn('order_state',[1,0])->where('is_urgent',1) 
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.routed_restaurant_id')
		->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id') 
 		->whereRaw('DATE_ADD(DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE) , INTERVAL 5 MINUTE)<= CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
		if($orderList!=null)$oDataRe =$oDataRe->whereNotIn('user_orders.id',$orderList);
		$oDataRe = $oDataRe->get()->toArray();
		echo "--activeOrderRoute-RouteCanel  --"; print_r($oDataRe);	

	   if(isset($oDataRe)){
		//add restaturant routing to orders
		foreach($oDataRe as  $data){
		   	
		        //cancel and refund order
			  $process =1;
		   	  $f2 = FinancialRecord::where("ma_order_id",$data['id'])->where("order_type",'Routed')->orderBy('id','desc')->first();
		   	  if(isset($f2->order_process_time)){
					$process =(time() > ($f2->order_process_time + 240))?1:0;
		   		}	
		   	if($process==1){ echo "--activeOrderRoute-RouteCanel-loop  --"; print_r($data);
				$this->changeOrderStatuscancel($data['id'], "Your order has been cancelled, a refund has been initiated for your order as no bar is available for delivery at the moment.", 'Cancelled',
				 $data['restaurant_id'], 0, 0);
			     	 $rdata = ['refund_mode'=>'fully','refund_flat_or_percentage'=>'flat','refund_fp_value'=>"",'refund_type'=>'F',
					'oid'=>$data['id'],'refund_value'=>'', 'host_name'=>'Ginie Martini','reason'=>'A refund has been initiated for your order as no bar is available for delivery at the moment.','reason_other'=>''];	
				$s = $this->releaseOrderRefund($rdata,$data['restaurant_id']);
				$total_amount=5;
				$p = FinancialRecord::whereBetween('created_at', [date('Y-m-d')." 00:00:00", date('Y-m-d')." 23:59:59"])
				->where("restaurant_id",$data['routed_restaurant_id'])
				->where("order_type",'Cancelled')->get()->toArray();			
				  if(isset($p) && count($p)<2)
				  $total_amount = 5;
			          if(isset($p) && count($p)==2)
			          $total_amount = 10;
			         if(isset($p) && count($p)>2)
			         $total_amount = 15;		
				FinancialRecord::create(["order_process_time"=>time(),"restaurant_id"=>$data['routed_restaurant_id'],'routed_from_restaurant_id'=>$data['restaurant_id'],
				"ma_order_id"=>$data['id'],"order_id"=>$data['payment_receipt'],"amount"=>$total_amount,"order_type"=>'Cancelled',"payment_type"=>"Debit","status"=>1]);
		       }
		}
	 } 
		
	  
	   
    }
   
    public function index()
    {      
        
        $statusArray = array("placed");
        $uptArray['manual_update']=0;
        $uptArray['order_state']=1;
	//$uptArray['delivery_time_left_hour']=1;   
         $updatedRecords = []; 
         $response = UserOrder::withoutTimestamps()->whereIn('user_orders.status',$statusArray)->where('order_state',2)
                ->whereRaw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE)<= CONVERT_TZ(now(),"+00:00", cities.timezone_value)')
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
                ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                ->update($uptArray); 
        //return true;
        //$updatedRecords[] = "updated successfully Order ID =>".$response;
	
 	$this->asapOrderRoute();
	 
        
     }
     public function reAllocateNearestBar($order,$restIdNotIn=null){
	    CommonFunctions::$lat1 = $order['latitude'];
            CommonFunctions::$lon1 = $order['longitude'];  
            CommonFunctions::$unit="M";
	    //$restIdNotIn = null;
		//print_r($restIdNotIn);	
	    $branchSelectFields = ['id','lat','lng','delivery'];
	    $restaurantData = Restaurant::select($branchSelectFields)->where(['parent_restaurant_id' => $order['parent_restaurant_id'] , 'restaurants.status' => 1]); 
	    //$financialRecord = FinancialRecord::select('restaurant_id')->where('ma_order_id',$order['id'])->whereIn('order_type',['Routed'])->get()->toArray();
	    if(isset($restIdNotIn)){
		foreach($restIdNotIn as $id){
			$idNotIn[] = $id;
		}
	    }  
	    $idNotIn[] = $order['restaurant_id'];
	    $restaurantData->whereNotIn('id',$idNotIn);	 
	    $restaurantData =$restaurantData->get()->toArray();   
	    $result = $this->chkNearestDistance($restaurantData,$order['id'],$order['restaurant_id']); 
	    return $result;	 
     }
     private function chkNearestDistance($restaurantData,$orderId,$orderResId ) {
	$result = null;
        if(count($restaurantData)>0){
          foreach ($restaurantData as $key => $rest) {
            CommonFunctions::$lat2 = $rest['lat'];
            CommonFunctions::$lon2 = $rest['lng'];
            //$result[$key]['id'] = $rest['id'];
            $distance = CommonFunctions::distanceBetweenTwoPointOnEarth();
	    $orderCount= $this->countOrderInPreparingState($rest['id']);
	    $is_delivery_on = $rest['delivery'];	
            if($distance<=2.5 && $is_delivery_on==1 ){//$distance<=2.5 &&  
		
			$result[$key]['sortby_distance'] = $distance;
		        $result[$key]['id'] = $rest['id'];
			$result[$key]['orderCount'] = $orderCount;
			$result[$key]['distance'] = $distance;
			$result[$key]['orderId']  = $orderId;
			$result[$key]['orderResId']  = $orderResId;
		 
            } 
          }
	} 
        if($result!=null){ 
	     usort($result, function ($item1, $item2) {
                 return $item1['sortby_distance'] <=> $item2['sortby_distance'];
             });
	     return $result[0];	
        }else return null; 
        
        
    }
    private function countOrderInPreparingState($restaurant_id){
	   $order = DB::table('user_orders')->where('restaurant_id', $restaurant_id)->where('status', 'preparing')->get()->toArray();
	   return count($order); 
    } 
    public function orderRouteMail($id,$restaurant_id){ echo "routing mail";
	    $orderData = UserOrder::find($id);
	    if($restaurant_id!='')$restaurant_id=$orderData->restaurant_id;
	   
            $cardInfo = $orderData->card_type . ' (' . $orderData->card_number . ')';
            $orderDetail = new UserOrderDetail();
            $orderDetailData = $orderDetail->getOrderDetail($id);
            $oDetail = new UserOrderDetail();
	    $restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
	    $oldrestaurantData = Restaurant::where(array('id'=>$restaurant_id))->first();
	    $prestaurantData = Restaurant::where(array('id'=>$oldrestaurantData->parent_restaurant_id))->first();
	     $orderItem = '';
	     $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $restaurantData->restaurant_id, 'datetime' => $orderData->created_at)));	
	    $oDetail = new UserOrderDetail();
	    		        $oDetailData = $oDetail->getOrderDetail($orderData->id);
				$orderDetailData = $oDetail->where('user_order_id',  $orderData->id)->get(); 
                                foreach($orderDetailData as $data){
                                    $label = "";
                                    $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                                    $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                                    $addons_data = [];
                                    $bypData = json_decode($data->menu_json, true );
                                    //print_r($bypData); die;
                                    if (!is_null($bypData)) {
                                        $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                                    }
                                    $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody> 
                            <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                            <tr style="padding:0;text-align:left;vertical-align:top">
                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                            <b>'.$data->quantity.' '.$data->item.'</b>';
                                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                                     
                                    $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                                    $label .= $addon_modifier_html;
                                     
                                    $instruct = '';
                                    if(!empty($data->special_instruction)){
                                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                                    }
                                    $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span> </span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                                }
$total_refunded_amount=0;	
 	    $mailKeywords = array(
                        'RESTAURANT_NAME'     => $prestaurantData->restaurant_name,
                        'ORDER_DETAILS'         => $orderItem,
                        'RESTAURANT_ADD' => $restaurantData->address,
                        'ORDER_NUMBER' => $orderData->payment_receipt,
                        'USER_NAME' => ucfirst($orderData->fname),
                        'ORDER_TYPE' => ucfirst($orderData->order_type),
                        'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                        'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                        'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                        'SUB_TOTAL' =>  $orderData->order_amount,
                           'DELIVERY_CHARGE'	  =>  $orderData->delivery_charge,
			'OLD_REST_NAME'=>$oldrestaurantData->restaurant_name,
                        'GRAND_TOTAL'	  =>  $orderData->total_amount,
                        'TIP'	 	  =>  $orderData->tip_amount,
                        'TAX'	 	  =>  $orderData->tax,  
                        'TOT_AMOUNT' =>  $orderData->total_amount,
                        'REFUND_AMOUNT' => number_format($total_refunded_amount,2),
                        'CARD_NUMBER' => $cardInfo,
                        'REASON' => '',
                        'ITEM_DETAIL' => $orderItem,
                        'SITE_URL' => $restaurantData->source_url,
                        'title' => "Order Moved to other Partner Bar"
             );
             /*************************************************************************************************/
                    $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,'');
            /**************************************************************************************************/
             $tipAmount = $orderData->tip_amount;
                    
                    $mailKeywords['DISPLAY_REFUND'] = 'none';
                    if($total_refunded_amount > 0 ) {
                        $mailKeywords['DISPLAY_REFUND'] = 'table';
                    }
                    if($orderData->user_comments!=""){
                        $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                    }
                    else {
                        $mailKeywords['SPECIAL_INFO'] = '';
                    }
                     
                        $address = $orderData->address;
                        if ($orderData->address2) {
                            $address = $address . ' ' . $orderData->address2;
                        }
                        $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                        //echo $address;
                        $mailKeywords['DELIVERY_CHARGE'] =  $orderData->delivery_charge;
                        $mailKeywords['USER_ADDRESS'] = $address;
                        $mailTemplate = 'delivery-route';
                     
                    $mailKeywords = array(
                        'header' => array(),
                        'footer' => array(),
                        'data' => $mailKeywords
                    );
                    $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                    $mailData['subject']        = "Your Martini Genie Order Update";
                    $mailData['body']           = $mail_html;
                    $mailData['receiver_email'] =  $orderData->email;
                    $mailData['receiver_name']  = ucfirst($orderData->fname);
                    $mailData['MAIL_FROM_NAME'] =  $prestaurantData->restaurant_name;
                    $mailData['MAIL_FROM'] = $prestaurantData->custom_from;
                    CommonFunctions::sendMail($mailData);
                    $response['result'] = true;
                    $response['message'] = 'Success';
                    $response['data'] = array(
                        'message' => true,
                        'status' => 'cancelled',
                        'order_id' => $orderData->id,
                        'orders' => 0,
                        'is_order_scheduled' => 0,
                    );
                    /******** SMS Send **** @18-10-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $orderData->id,
                        'sms_module' => 'order',
                        'action' => 'route',
                        'sms_to' => 'customer'
                    );
                    CommonFunctions::getAndSendSms($sms_keywords);  
		//print_r($mailKeywords);die;
    }
    public function changeOrderStatuscancel($id, $reason, $status, $restaurant_id, $user_id, $isTwinjet = 0)
    {
         
        if (isset($id) && is_numeric($id)) {
	    
            $orderData = UserOrder::find($id);
	    if($restaurant_id!='')$restaurant_id=$orderData->restaurant_id;
            $total_refunded_amount = DB::table('refund_histories')->where('order_id', $id)->sum('amount_refunded');
            /// print_r($total_refunded_amount);die;
            $previousData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
            $previous_action_data = json_encode($previousData);
	    $oldStatus = $orderData->status;		
            $orderData->updated_at = now();
            $orderData->status = $status;
            $orderData->restaurants_comments = $reason;
            
	     if($oldStatus!="placed"){
		$job = StuartOrders::where('ma_order_id',$id)->first();
		if(isset($job->job_id)){
			$cdata = Stuarts::cancelAJob($job->job_id);
			$cdata = json_decode($cdata,true);
			if($cdata['error']=='CANCELLATION_ERROR'){
				//return null;
			}}
		
	    }	$orderData->save();		
	    /****************************order_statuses**********************************************************/
            $orderStatus = new OrderStatuses();
            $orderStatus->order_id = $id;
            $orderStatus->status = $status;
            $orderStatus->user_id =0;
            $orderStatus->restaurant_id = $restaurant_id; 
            $orderStatus->created_at = now();
            $orderStatus->save();

            /*******************************************************************************************************/			
             

             
            $langId = 1;
            CommonFunctions::$langId = $langId;
            $restaurantData = Restaurant::where(array('id' => $restaurant_id))->first();
            $orderItem = $label = "";
 	     $orderItem = '';
				$oDetail = new UserOrderDetail();
	    		        $oDetailData = $oDetail->getOrderDetail($orderData->id);
				$orderDetailData = $oDetail->where('user_order_id',  $orderData->id)->get(); 
                                foreach($orderDetailData as $data){
                                    $label = "";
                                    $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                                    $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                                    $addons_data = [];
                                    $bypData = json_decode($data->menu_json, true );
                                    //print_r($bypData); die;
                                    if (!is_null($bypData)) {
                                        $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                                    }
                                    $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody> 
                            <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                            <tr style="padding:0;text-align:left;vertical-align:top">
                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                            <b>'.$data->quantity.' '.$data->item.'</b>';
                                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                                     
                                    $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                                    $label .= $addon_modifier_html;
                                     
                                    $instruct = '';
                                    if(!empty($data->special_instruction)){
                                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                                    }
                                    $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span> </span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                                }
            //$user_id = ;
            $updateData = array("restaurant_name" => $restaurantData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
            $action_data = json_encode($updateData);
            $moduleName = '';
            if ($orderData->product_type == 'product') {
                $moduleName = 'Merchandise Order';
            } else if ($orderData->product_type == 'food_item') {
                $moduleName = 'Food Order';
            }
            $logData = array(
                'user_id' => $user_id,
                'updated_id' => $id,
                'module_name' => $moduleName,
                'restaurant_id' => $restaurant_id,//Auth::user()->restaurant_id, //$orderData->restaurant_id,   // PE-3221 - restaurant id should be based who's logged in Branch/Brand
                'tablename' => 'user_orders',
                'action_type' => 'update',
                'previous_action_data' => $previous_action_data,
                'action_data' => $action_data);

            //return $logData;
	     $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
                        	$timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $restaurantData->restaurant_id, 'datetime' => $orderData->created_at)));
            $logs = CmsUserLog::create($logData);
            //$timestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');
            //$timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
            $cardInfo = $orderData->card_type . ' (' . $orderData->card_number . ')';
            $orderDetail = new UserOrderDetail();
            $orderDetailData = $orderDetail->getOrderDetail($id);
            $oDetail = new UserOrderDetail();
	    $prestaurantData = Restaurant::where(array('id'=>$restaurantData->parent_restaurant_id))->first();
  $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $restaurantData->restaurant_id, 'datetime' => $orderData->created_at)));
 	    $mailKeywords = array(
                        'RESTAURANT_NAME'     => $prestaurantData->restaurant_name,
                        'ORDER_DETAILS'         => $orderItem,
                        'RESTAURANT_ADD' => $restaurantData->address,
                        'ORDER_NUMBER' => $orderData->payment_receipt,
                        'USER_NAME' => ucfirst($orderData->fname),
                        'ORDER_TYPE' => ucfirst($orderData->order_type),
                        'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                        'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                        'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                        'SUB_TOTAL' =>  $orderData->order_amount,
                           'DELIVERY_CHARGE'	  =>  $orderData->delivery_charge,
                        'GRAND_TOTAL'	  =>  $orderData->total_amount,
                        'TIP'	 	  =>  $orderData->tip_amount,
                        'TAX'	 	  =>  $orderData->tax,  
                        'TOT_AMOUNT' =>  $orderData->total_amount,
                        'REFUND_AMOUNT' => number_format($total_refunded_amount,2),
                        'CARD_NUMBER' => $cardInfo,
                        'REASON' => $reason,
                        'ITEM_DETAIL' => $orderItem,
                        'SITE_URL' => $restaurantData->source_url,
                        'title' => "Cancel Order"
             );
             /*************************************************************************************************/
                    $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,'');
            /**************************************************************************************************/
             $tipAmount = $orderData->tip_amount;
                    
                    $mailKeywords['DISPLAY_REFUND'] = 'none';
                    if($total_refunded_amount > 0 ) {
                        $mailKeywords['DISPLAY_REFUND'] = 'table';
                    }
                    if($orderData->user_comments!=""){
                        $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                    }
                    else {
                        $mailKeywords['SPECIAL_INFO'] = '';
                    }
                     
                        $address = $orderData->address;
                        if ($orderData->address2) {
                            $address = $address . ' ' . $orderData->address2;
                        }
                        $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                        //echo $address;
                        $mailKeywords['DELIVERY_CHARGE'] =  $orderData->delivery_charge;
                        $mailKeywords['USER_ADDRESS'] = $address;
                        $mailTemplate = 'delivery-cancel';
                     
                    $mailKeywords = array(
                        'header' => array(),
                        'footer' => array(),
                        'data' => $mailKeywords
                    );
                    $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                    $mailData['subject']        = "Your order has been cancelled";
                    $mailData['body']           = $mail_html;
                    $mailData['receiver_email'] =  $orderData->email;
                    $mailData['receiver_name']  = ucfirst($orderData->fname);
                    $mailData['MAIL_FROM_NAME'] =  $prestaurantData->restaurant_name;
                    $mailData['MAIL_FROM'] = $prestaurantData->custom_from;
                    CommonFunctions::sendMail($mailData);
                    $response['result'] = true;
                    $response['message'] = 'Success';
                    $response['data'] = array(
                        'message' => true,
                        'status' => 'cancelled',
                        'order_id' => $orderData->id,
                        'orders' => 0,
                        'is_order_scheduled' => 0,
                    );
                    /******** SMS Send **** @18-10-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $orderData->id,
                        'sms_module' => 'order',
                        'action' => 'rejected',
                        'sms_to' => 'customer'
                    );
                    CommonFunctions::getAndSendSms($sms_keywords); 

              //return $orderData;
        }

        //return Redirect::back()->withSuccess('Language set successfully!');
    }	
    private function getPaymentGetwayInfo($restaurant_id)
    {
        $paymentGatewayConfig = null;
        $paymentGatewayInfo = PaymentGateway::select('*')->where("restaurant_id", $restaurant_id)->where("status", 1)->get()->toArray();
        //print_r($paymentGatewayInfo);die;
        if (isset($paymentGatewayInfo[0])) {
            $paymentGatewayConfig['ID'] = $paymentGatewayInfo[0]['id'];
            $config = json_decode($paymentGatewayInfo[0]['config']);
            $paymentGatewayConfig['GATEWAY'] = strtolower(trim($paymentGatewayInfo[0]['name']));
            foreach ($config as $val1) {
                foreach ($val1 as $key => $val) {
                    $paymentGatewayConfig[$key] = $val;
                }
            }
        } else {
            $paymentGatewayConfig['ID'] = 0;
            $paymentGatewayConfig['STRIPE_SECRET'] = config('constants.payment_gateway.stripe.secret');
            $paymentGatewayConfig['GATEWAY'] = "stripe";
        }
        return $paymentGatewayConfig;
    } 	  	 
    public function releaseOrderRefund($rdata,$restaurant_id=0)
    {
        $errors = array();
        $status = Config('constants.status_code.BAD_REQUEST');
        $success = "Refund completed successfully.";
        $product_type = '';
        $order_id = '';
        $refund_reason = $rdata['reason'];
	 
        if (empty($errors) && $rdata['oid'] && is_numeric($rdata['oid'])) {
            $orderData = UserOrder::where('id', $rdata['oid'])->whereNotIn('status', ['refunded', 'pending'])->first(); 
            if ($orderData) {
                $product_type = $orderData->product_type;
                $order_id = $rdata['oid'];
                $action = $orderData->status;
                if ($rdata['refund_mode'] && $rdata['refund_mode'] == 'fully') {
                    $action = 'refunded';
                    $refund_value = $orderData->total_amount;
                    $amount_refunded = $refund_value = $orderData->total_amount;

                } else {
                    if ($rdata['refund_type'] == 'F') {
                        $refund_value = $rdata['refund_value'];
                        $amount_refunded = $rdata['refund_value'];
                    } else {
                        $amount_refunded = ($rdata['refund_value'] * $orderData->total_amount) / 100;
                        $refund_value = $rdata['refund_value']; //100% is being send
                    }
                }
		
                if ($amount_refunded <= $orderData->total_amount) {
                    $globalSetting = DB::table('global_settings')->whereNotNull('bcc_email')->whereNotNull('bcc_name')->first();

	            $parentRestaurant = DB::table('restaurants')->where('id',$restaurant_id)->first();
                    $refundResponse = $this->refundInitatedOnPaymentGateway($orderData, $amount_refunded, $action,$parentRestaurant);
	 	    //echo "--->".$orderData->id;print_r($refundResponse['errorMessage']);
                    //if (!isset($refundResponse['errorMessage'])) {
		    if (1) {	
                        #Refund Data Saved

                        $refund_data = array(
                            'order_id' => $orderData->id,
                            'refund_type' => $rdata['refund_type'],
                            'refund_value' => $refund_value,
                            'amount_refunded' => $amount_refunded,
                            'charge_id' => isset($refundResponse->charge) ? $refundResponse->charge : $orderData->stripe_charge_id,
                            'refund_id' => isset($refundResponse->id) ? $refundResponse->id : 0,
                            'reason' => $refund_reason,
                            'host_name' => $rdata['host_name'],
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                        );

                        //RefundHistory::create($refund_data);

                        DB::table('refund_histories')->insert($refund_data);
			
                        $total_refunded_amount = DB::table('refund_histories')->where('order_id', $orderData->id)->sum('amount_refunded');

                        $previousData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
                        $previous_action_data = json_encode($previousData);
                        $oldOrderStatus = $orderData->status;
                        $orderData->updated_at = now();
                        $orderData->status = $action;
                        $orderData->total_amount = $orderData->total_amount - $amount_refunded;
                        if ($orderData->total_amount <= 0) {
                            $orderData->status = 'refunded';

                        }

                        $orderData->save();
			$job = StuartOrders::where('ma_order_id',$orderData->id)->first();
			if(isset($job->job_id)){
			$cdata = Stuarts::cancelAJob($job->job_id);
			$cdata = json_decode($cdata,true);
			if($cdata['error']=='CANCELLATION_ERROR'){
				//return null;
			}}
                        $restaurantData = Restaurant::where(array('id' => $restaurant_id))->first();
                        $restaurantData1 = $restaurantData->toArray();

                        $deliveryServicesData = DeliveryService::where(array('id' => $restaurantData->delivery_provider_id))->first();

                         
                        $langId = 1;//config('app.language')['id'];
                        CommonFunctions::$langId = $langId;

                        $orderItem = $label = "";

                        $user_id = 1;
                        $updateData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
                        $action_data = json_encode($updateData);
                        $moduleName = '';
                        if ($orderData->product_type == 'product') {
                            $moduleName = 'Merchandise Order';
                        } else if ($orderData->product_type == 'food_item') {
                            $moduleName = 'Food Order';
                        }
                        $logData = array(
                            'user_id' => $restaurant_id,
                            'updated_id' => $orderData->id,
                            'module_name' => $moduleName,
                            'restaurant_id' => $restaurant_id,   
                            'tablename' => 'user_orders',
                            'action_type' => 'update',
                            'previous_action_data' => $previous_action_data,
                            'action_data' => $action_data);

                       
                        $logs = CmsUserLog::create($logData);
                        if (1) {
                            ##-----------Refund Order Email and sms--- ###
                                $total_refunded_amount = DB::table('refund_histories')->where('order_id', $orderData->id)->sum('amount_refunded');
                                    // send email
                                    $orderItem = '';
				$oDetail = new UserOrderDetail();
	    		        $oDetailData = $oDetail->getOrderDetail($orderData->id);
				$orderDetailData = $oDetail->where('user_order_id',  $orderData->id)->get(); 
                                foreach($orderDetailData as $data){
                                    $label = "";
                                    $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                                    $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                                    $addons_data = [];
                                    $bypData = json_decode($data->menu_json, true );
                                    //print_r($bypData); die;
                                    if (!is_null($bypData)) {
                                        $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                                    }
                                    $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody> 
                            <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                            <tr style="padding:0;text-align:left;vertical-align:top">
                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                            <b>'.$data->quantity.' '.$data->item.'</b>';
                                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                                     
                                    $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                                    $label .= $addon_modifier_html;
                                     
                                    $instruct = '';
                                    if(!empty($data->special_instruction)){
                                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                                    }
                                    $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span> </span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                                }
				 $cardInfo = "<span style='color: #000;'>".$orderData->card_type."</span>".' ('.$orderData->card_number.')';
                            	 $cardInfoTxt = '<b>Credit Card:</b>';
                                $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
                        	$timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $restaurantData->restaurant_id, 'datetime' => $orderData->created_at)));
                                $prestaurantData = Restaurant::where(array('id'=>$restaurantData->parent_restaurant_id))->first();
  				$mailKeywords = array(
                                    'RESTAURANT_NAME'     => $prestaurantData->restaurant_name,
                                    'ORDER_DETAILS'         => $orderItem,
                                    'RESTAURANT_ADD' => $restaurantData->address,
                                    'ORDER_NUMBER' => $orderData->payment_receipt,
                                    'USER_NAME' => ucfirst($orderData->fname),
                                    'ORDER_TYPE' => ucfirst($orderData->order_type),
                                    'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                                    'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                                    'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                                    'SUB_TOTAL' =>  $orderData->order_amount,
                                        'DELIVERY_CHARGE'	  =>  $orderData->delivery_charge,
                                    'GRAND_TOTAL'	  =>  $orderData->total_amount,
                                    'TIP'	 	  =>  $orderData->tip_amount,
                                    'TAX'	 	  =>  $orderData->tax,      
                                    'TOT_AMOUNT' =>  number_format($orderData->total_amount,2),
                                    'REFUND_AMOUNT' =>  number_format($total_refunded_amount,2),
                                    'CARD_NUMBER' => $cardInfo,
                                    'REASON' => $refund_reason,
                                    'ITEM_DETAIL' => $orderItem,
                                    'SITE_URL' => $restaurantData->source_url,
                                    'title' => "Refund Order",
                                    'URL' => $restaurantData->source_url,
                                );
                                /*************************************************************************************************/
                                $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,'');
                                /**************************************************************************************************/
                                if($orderData->tax==0.00){
                                    $mailKeywords['TAX'] = '';
                                    $mailKeywords['TAX_TITLE'] = '';
                                    $mailKeywords['DISPLAY'] = 'none';
                                } else{
                                    $mailKeywords['TAX'] =  $orderData->tax;
                                    $mailKeywords['TAX_TITLE'] = "Taxes";
                                    $mailKeywords['DISPLAY'] = 'table';
                                }
                                $tipAmount = $orderData->tip_amount;
                                if($orderData->tip_amount==0.00){
                                    $mailKeywords['TIP_AMOUNT'] = '';
                                    $mailKeywords['TIP_TITLE'] = '';
                                    $mailKeywords['DISPLAY_TIP'] = 'none';
                                } else{
                                    $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                                    $mailKeywords['TIP_AMOUNT'] ='<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                                                <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left"><p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">'.$orderData->tip_percent.'<span>%</span></p>
                                                </th></tr></table></th>
                                                <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span> </span>'.$tipAmount.'</p></th></tr></table></th>
                                            </tr></tbody></table>';
                                    $mailKeywords['DISPLAY_TIP'] = 'table';
                                }
                                $mailKeywords['DISPLAY_REFUND'] = 'table';
                                if($orderData->user_comments!=""){
                                    $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                                }
                                else {
                                    $mailKeywords['SPECIAL_INFO'] = '';
                                }
                                if($orderData->order_type=="delivery") {
                                    $address = $orderData->address;
                                    if ($orderData->address2) {
                                        $address = $address . ' ' . $orderData->address2;
                                    }
                                    $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                                    //echo $address;
                                    $mailKeywords['DELIVERY_CHARGE'] =  $orderData->delivery_charge;
                                    $mailKeywords['USER_ADDRESS'] = $address;
                                    $mailTemplate = 'delivery_order_refund';
                                } else {
                                    $mailKeywords['USER_ADDRESS'] = '';
                                    $mailTemplate = 'carryout_order_refund';
                                }
                                $order_refund_date = date('M d, Y h:i A', strtotime($orderData->delivery_date.' '.$orderData->delivery_time.':00'));
                                if($orderData->status == 'refunded'){
                                    $mailKeywords['HEAD'] = "We've initiated a refund for your experience with us on $order_refund_date";
                                }else {
                                    $mailKeywords['HEAD'] = "We've initiated a partial refund for your experience with us on $order_refund_date";
                                }
                                $mailKeywords = array(
                                    'header' => array(),
                                    'footer' => array(),
                                    'data' => $mailKeywords
                                );
                                $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                                $mailData['subject']  = "Your Refund Is Being Processed";
                                $mailData['body']           = $mail_html;
                                $mailData['receiver_email'] =  $orderData->email;
                                $mailData['receiver_name']  = ucfirst($orderData->fname);
                                $mailData['MAIL_FROM_NAME'] =  $prestaurantData->restaurant_name;
                                $mailData['MAIL_FROM'] = $prestaurantData->custom_from;
                                if($globalSetting) {
                                    $mailData['bcc_email'] = $globalSetting->bcc_email;
                                    $mailData['bcc_name'] = $globalSetting->bcc_name;
                                }
                                CommonFunctions::sendMail($mailData);
                                /******** SMS Send **** @18-10-2018 by RG *****/
                                $sms_keywords = array(
                                    'order_id' => $orderData->id,
                                    'sms_module' => 'order',
                                    'action' => 'refund',
                                    'sms_to' => 'customer',
                                    'amount_refunded' => $total_refunded_amount
                                );
                                CommonFunctions::getAndSendSms($sms_keywords);
                                /************ END SMS *************/
                            }
                            $status  = Config('constants.status_code.STATUS_SUCCESS');
                            #$response['message'] = 'Refund initiated successfully!';
                            $response['result'] = true;
                            $response['message'] = 'Success';
                            $response['data'] = array(
                                'message' => true,
                                'status' => $action,
                                'order_id' => $orderData->id,
                                'orders' => 0
                            );
                        return true;
                        //$errors[] = "Refund completed successfully.";
                    } 
                }  
            }  
        }
        return  true;
        //return Redirect::back()->withSuccess('Language set successfully!');
    } 
    private function refundInitatedOnPaymentGateway($orderData, $amount_refunded, $status,$parentRestaurant)
    {
        if ($orderData->payment_gatway != "Card") {
            return true;
        }
        $refundResponse = null;
	
        $paymentGatewayConfig = $this->getPaymentGetwayInfo($parentRestaurant->parent_restaurant_id);
        // print_r($paymentGatewayConfig);die;
        if (isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY'])) == "stripe") {
            $apiObj = new StripeGateway($paymentGatewayConfig);
            $stripeRefundData = [
                'charge' => $orderData->stripe_charge_id,
                'amount' => ($amount_refunded * 100),
                'reason' => 'requested_by_customer',
            ];
            $refundResponse = $apiObj->createRefund($stripeRefundData);
            $orderLog = ['payment_gateway_order_id' => $orderData->stripe_charge_id,

                'order_id' => $orderData->id,
                'payment_gateway_id' => $paymentGatewayConfig['ID'],
                'order_status' => $status,
                'platform' => 'api',
                'cms_user_id' => 0,
                'request_data' => isset($stripeRefundData) ? json_encode($stripeRefundData) : '',
                'response_data' => isset($refundResponse) ? json_encode($refundResponse) : ''
            ];
            PaymentGatewayLog::create($orderLog);//die;
        }   
        return $refundResponse; 
    }	  
}
