<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\CustomDeliveryHours;
use Modules\Reservation\Entities\ReservationStatus;
use App\Models\DeliveryHours;
use DB;
use App\Http\Requests\CarryoutDeliveryHoursRequest;
use Modules\Reservation\Entities\Reservation;

class DeliveryHoursController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
       $calendar_days = config('reservation.calendar_days');
        return view('delivery_hours.index', compact('calendar_days'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('delivery_hours::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CarryoutDeliveryHoursRequest $request)
    {
        try {
            $type = $request->get('type');
            $inputData = $this->getModifiedInputArray($request);
            if($inputData){
                DB::beginTransaction();
                if($type == config('reservation.working_hour_type.day')){
                    if(isset($inputData['calendar_day']) && !empty($inputData['calendar_day'])){
                        DeliveryHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => $inputData['calendar_day']], $inputData);
                    }
                }else{
                    $schd_id = $request->has('schd_id') ? $request->get('schd_id'): 0;
                    $action = ($schd_id==0)?'add':'update';
                    if($this->subRangeExistInRange($inputData['calendar_date'], $action)){
                        return \Response::json(['success' => false, 'errors' => ['Slots are already exist in this date range, please select different range']], 419);
                    }
                    CustomDeliveryHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'id' => $schd_id], $inputData);
                }
                if($request->has('repeat_day') && $type == config('reservation.working_hour_type.day')){
                    $repeat_arr = array_keys($request->get('repeat_day'));
                    $input_calendar_day = $inputData['calendar_day'];
                    foreach($repeat_arr as $day){
                        if($input_calendar_day != $day){
                            $calendar_days = config('reservation.calendar_days');
                            $request->merge(['calendar_day' => array_search($day, $calendar_days)]);
                            $inputData = $this->getModifiedInputArray($request);
                            if($inputData && (isset($inputData['calendar_day']) && !empty($inputData['calendar_day']))){
                                DeliveryHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => $day], $inputData);
                            }
                        }
                    }
                }
                DB::commit();
                return \Response::json(['success' => true, 'message'=>'Hour setting has been updated'], 200);
            }
        }catch (\Exception $e){
            return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
            DB::rollback();
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('delivery_hours::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('delivery_hours::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CarryoutDeliveryHoursRequest $request)
    {
        //\Log::info($request->all());
        $type = $request->get('type');
        $schd_id = $request->get('schd_id');
        $inputData = $this->getModifiedInputArray($request);
        if($inputData){
            DB::beginTransaction();
            try {
                if($type == config('reservation.working_hour_type.day')){
                    DeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $inputData['calendar_day'])->update($inputData);
                    if($request->has('repeat_day')){
                        $repeat_arr = array_keys($request->get('repeat_day'));
                        $input_calendar_day = $inputData['calendar_day'];
                        foreach($repeat_arr as $day){
                            if($input_calendar_day != $day){
                                $calendar_days = config('reservation.calendar_days');
                                $request->merge(['calendar_day' => array_search($day, $calendar_days)]);
                                $request->merge(['repeat' => true]);
                                $inputData = $this->getModifiedInputArray($request);
                                if($inputData && (isset($inputData['calendar_day']) && !empty($inputData['calendar_day']))){
                                    DeliveryHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => $day], $inputData);
                                }
                            }
                        }
                    }
                }else{
                    CustomDeliveryHours::find($schd_id)->update($inputData);
                }
                DB::commit();
                return \Response::json(['success' => true], 200);
            }catch(\Exception $e){
                return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
                DB::rollback();
            }
        }
        return \Response::json(['success' => false, 'error' => 'Schedule setting could not be updated'], 500);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */

    public function destroy($id)
    {
        $type  = Input::get('type');
        $slot_id = Input::get('slot_id');
        $restaurant_id = \Auth::user()->restaurant_id;
        DB::beginTransaction();
        try {
            if($type == config('reservation.working_hour_type.day')) {
                $calendar_days = config('reservation.calendar_days');
                $calendar_day = $calendar_days[$id];
                $scheduleHours = DeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $calendar_day)->first();
            }else {
                $scheduleHours = CustomDeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('id', $id)->first();
            }
            if($scheduleHours){
                if($type == config('reservation.working_hour_type.day')) {
                    $dayOrDate = $scheduleHours->calendar_day;
                }else{
                    $dayOrDate = $scheduleHours->calendar_date;
                }
                if(empty($scheduleHours->slot_detail) || $scheduleHours->slot_detail==NULL || (empty($slot_id) || $slot_id<=0)) {
                    $scheduleHours->delete($id);
                    DB::commit();
                    return \Response::json(['success' => true], 200);
                }
                $slot_detail_db = json_decode($scheduleHours->slot_detail, true);
                $exist = array_search($slot_id, array_column($slot_detail_db, 'slot_id'));
                $time_range = $slot_detail_db[$exist]['open_time'].'-'.$slot_detail_db[$exist]['close_time'];
                \Log::info($slot_detail_db[$exist]['open_time']."****".$time_range);
                unset($slot_detail_db[$exist]);
                $slot_detail_db = array_values($slot_detail_db); // 'reindex' array
                //\Log::info($slot_detail_db);
                $slot_detail_db = !empty($slot_detail_db)?$slot_detail_db:0;
                $scheduleHours->slot_detail = json_encode($slot_detail_db);
                $scheduleHours->save();
                DB::commit();
                return \Response::json(['success' => true], 200);
            }
        }catch(\Exception $e){
            return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
            DB::rollback();
        }
    }

    public function getWorkingHoursSchedule()
    {
        $eventData = DeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->get()->where('calendar_day', '!=', '');
        $events = $date_range = [];
        $inc = 0;
        $calendar_days = config('reservation.calendar_days');
        $dayoff = DeliveryHours::where('restaurant_id', '=', \Auth::user()->restaurant_id)
            ->get(['is_dayoff', 'calendar_day'])
            ->groupBy('calendar_day')
            ->map(function($day) {
                return $day->pluck('is_dayoff')->take(1);
            })->toArray();
        foreach ($calendar_days as $day_num => $day) {
            $date_range[$day_num]['id'] = $day_num;
            $date_range[$day_num]['title'] = ucfirst($day);
            $date_range[$day_num]['dayoff'] = isset($dayoff[$day]) ? $dayoff[$day][0]: 0;
        }
        $calendar_days = array_flip($calendar_days);
        \Log::info($calendar_days);
        foreach($eventData as $event){
            if($event->is_dayoff){
                $events[$inc]['id'] = $inc;
                $events[$inc]['start'] = date( "Y-m-d 00:00:00");
                $events[$inc]['end'] = date( "Y-m-d 24:00:00");
                $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                $inc++;
            }elseif(empty($event->slot_detail)){
                $events[$inc]['id'] = $inc;
                \Log::info($event->calendar_day);
                $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                $inc++;
            }else{
                $slot_detail = json_decode($event->slot_detail, true);
                if($slot_detail){
                    foreach ($slot_detail as $slot){
                        $events[$inc]['id'] = $inc;
                        $events[$inc]['slot_id'] = $slot['slot_id'];
                        $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                        $events[$inc]['start'] = date( "Y-m-d H:i:s", strtotime( $slot['open_time']));
                        $events[$inc]['end'] = date( "Y-m-d H:i:s", strtotime( $slot['close_time']));
                        $events[$inc]['title'] = '';
                        $inc++;
                    }
                }
            }
        }
        $events = ($events) ?? '';
        return json_encode(array("events" => $events, 'date_range' => $date_range));
    }

    public function getCustomWorkingHoursSchedule()
    {
        $eventData = CustomDeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->get();
        $events = $date_range = [];
        $inc = 0;
        foreach($eventData as $key=>$event) {
            $resource_id = $event->id;
            $calendar_dates = json_decode($event->calendar_date, true);
            $title = date('dS M', strtotime($calendar_dates['start_date'])) . '-' . date('dS M', strtotime($calendar_dates['end_date']));
            $date_range[$key]['title'] = $title;
            $date_range[$key]['id'] = $resource_id;
            $date_range[$key]['dayoff'] = $event->is_dayoff;
            $slot_detail = json_decode($event->slot_detail, true);

            if($event->is_dayoff){
                $events[$inc]['id'] = $inc;
                $events[$inc]['start'] = date( "Y-m-d 00:00:00");
                $events[$inc]['end'] = date( "Y-m-d 24:00:00");
                $events[$inc]['resourceId'] = $resource_id;
                $inc++;
            }elseif(empty($event->slot_detail)){
                $events[$inc]['id'] = $inc;
                $events[$inc]['resourceId'] = $resource_id;
                $inc++;
            } else {
                foreach ($slot_detail as $slot) {
                    $events[$inc]['id'] = $inc;
                    $events[$inc]['resourceId'] = $resource_id;
                    $events[$inc]['slot_id'] = $slot['slot_id'];
                    $events[$inc]['start'] = date("Y-m-d H:i:s", strtotime($slot['open_time']));
                    $events[$inc]['end'] = date("Y-m-d H:i:s", strtotime($slot['close_time']));
                    $events[$inc]['title'] = '';
                    $inc++;
                }
            }
        }
        $events = ($events) ?? '';
        $date_range = ($date_range) ? array_values($date_range): '';
        return json_encode(array("events" => $events, 'date_range' => $date_range));
    }

    public function dayOffToggle()
    {
        $status = Input::get('status');
        $resourceId = Input::get('resourceId');
        $resourceId = ($resourceId)? str_replace('dayoff_', '', $resourceId) : 0;
        $type = Input::get('type');
        $status = ($status=='true')? 0:1;
        $start_date = Input::get('start_date');
        $end_date = Input::get('end_date');
        $inputData['restaurant_id'] = \Auth::user()->restaurant_id;
        $inputData['is_dayoff'] = $status;
        $inputData['slot_detail'] = NULL;
        $restaurant_id = \Auth::user()->restaurant_id;
        DB::beginTransaction();
        try {
            if($type == config('reservation.working_hour_type.day')){
                DeliveryHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => strtolower($resourceId)], $inputData);
            }else{
                if($resourceId){
                    $inputData['calendar_date'] = $this->getCustomStartEndDate($resourceId);
                }else{
                    $calendar_dates = ["start_date" => date('Y-m-d', strtotime($start_date)), "end_date" => date('Y-m-d', strtotime($end_date))];
                    $inputData['calendar_date'] = $calendar_dates ? json_encode($calendar_dates) : $calendar_dates;
                    if($this->subRangeExistInRange($inputData['calendar_date'], 'add')){
                        return \Response::json(['success' => false, 'errors' => ['Slots are already exist in this date range, please select different range']], 422);
                    }
                }
                CustomDeliveryHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'id' => $resourceId], $inputData);
            }
            DB::commit();
            return \Response::json(['success' => true], 200);
        }
        catch(\Exception $e){
            return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
            DB::rollback();
        }
    }

    private function getModifiedInputArray($request){
        $inputData = array();
        $type = $request->get('type');
        $available_table_count = NULL;
       
        $calendar_day = $request->get('calendar_day');
        if($type == config('reservation.working_hour_type.day')){
            $calendar_days = config('reservation.calendar_days');
            $calendar_day = $calendar_days[$calendar_day];
            $inputData['calendar_day'] = $calendar_day;
            $working_schedule = DeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $inputData['calendar_day'])->first();
        }else{
            if($request->has('schd_id')){
                $inputData['calendar_date'] = $this->getCustomStartEndDate($request->get('schd_id'));
            }else{
                $start_date = $request->has('start_date') ? date('Y-m-d', strtotime($request->get('start_date'))) : date('Y-m-d');
                $end_date = $request->has('end_date') ? date('Y-m-d', strtotime($request->get('end_date'))) : date('Y-m-d');
                $calendar_dates = ["end_date" => $end_date, "start_date" => $start_date];
                $inputData['calendar_date'] = $calendar_dates ? json_encode($calendar_dates) : $calendar_dates;
            }
            $working_schedule = CustomDeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_date', 'LIKE', $inputData['calendar_date'])->first();
        }

        $slot_name = $request->get('slot_name');
        
        $calendar_dates =  NULL;
        $isDayOff = $request->has('is_dayoff') ? 1 : 0;
        $inputData['restaurant_id'] = \Auth::user()->restaurant_id;
        $inputData['is_dayoff']= $isDayOff;
        $slot_detail["open_time"] = $request->get('open_time');
        $slot_detail["close_time"] = $request->get('close_time');


        $slot_detail['slot_name']= $slot_name;
        if($working_schedule && !empty($working_schedule->slot_detail)){
            \Log::info('hee');
            $slot_detail_db = json_decode($working_schedule->slot_detail, true);
            // for update existing slots
            $slotId = 0;
            
            if(($request->has('slot_id') && !empty($request->get('slot_id'))) || $slotId>0){
                $slot_id = ($slotId) ? $slotId : $request->get('slot_id');
                $exist = array_search($slot_id, array_column($slot_detail_db, 'slot_id'));
                $slot_detail["slot_id"] = $slot_id;
                $slot_detail_db[$exist] = $slot_detail;
            }else{
                //for insert multiple slots
                \Log::info('hererererere');
                if($this->isTimeSlotSettingExistAlready($slot_detail_db, $request->get('open_time'), $request->get('close_time'))){
                    \Log::info('setting override');
                    return [];

                }
                $slot_detail["slot_id"] = max(array_column($slot_detail_db, 'slot_id'))+1;
                array_push($slot_detail_db, $slot_detail);
            }
            $inputData['slot_detail'] = json_encode($slot_detail_db);
        }else{
            //for insert first time
            $slot_detail["slot_id"] = 1;
            $inputData['slot_detail'] = json_encode([$slot_detail]);
        }
        return $inputData;
    }

    public function getSlotDetail($schd_id){
        $type = Input::get('type');
        $slot_id = Input::get('slot_id');
        $slot_detail = array();
        if($type == config('reservation.working_hour_type.day')){
            $calendar_days = config('reservation.calendar_days');
            $calendar_day = $calendar_days[$schd_id];
            $inputData['calendar_day'] = $calendar_day;
            $slotData = DeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $calendar_day)->first();
        }else{
            $slotData = CustomDeliveryHours::find($schd_id);
        }
        if($slotData){
            $slot_details = json_decode($slotData->slot_detail, true);
            if($slot_details){
                $exist = array_search($slot_id, array_column($slot_details, 'slot_id'));
                $slot_detail = $slot_details[$exist];
                $slot_detail['schd_id'] = $slotData->id;
                $slot_detail['slot_id'] = $slot_detail['slot_id'];
                $slot_detail['slot_name'] = $slot_detail['slot_name'];
                $slot_detail['is_dayoff'] = $slotData->is_dayoff;
            }
        }
        return json_encode(array("slot_detail" => $slot_detail));
    }

    public function getCustomStartEndDate($slot_id){
        $custom = CustomDeliveryHours::find($slot_id);
        $custom_dates = ($custom)? $custom->calendar_date: NULL;
        return $custom_dates;
    }

    public function subRangeExistInRange($date_range, $action)
    {
        $date_range_arr = json_decode($date_range, true);
        $subRangeCount = CustomDeliveryHours::where('restaurant_id', \Auth::user()->restaurant_id)->where(function($query) use ($date_range_arr) {
            return $query->where('calendar_date->start_date', '<=', $date_range_arr['start_date'])
                ->where('calendar_date->end_date', '>=', $date_range_arr['start_date']);
        })->orWhere(function($query) use ($date_range_arr) {
            return $query->where('calendar_date->start_date', '<=', $date_range_arr['end_date'])
                ->where('calendar_date->end_date', '>=', $date_range_arr['end_date']);
        });

        if($action=='update'){
            return false;
        }
        $subRangeCount = $subRangeCount->count();
        if($subRangeCount){
            return true;
        }
        return false;
    }

    public function isTimeSlotSettingExistAlready($slot_detail, $start_time, $end_time)
    {
        if($slot_detail<=0){
            return false;
        }
        foreach ($slot_detail as $slot) {
            if(!rangesNotOverlapOpen($slot['open_time'], $slot['close_time'], $start_time, $end_time) || !rangesNotOverlapClosed($slot['open_time'], $slot['close_time'], $start_time, $end_time)){
                return true;
            }
        }
        return false;
    }

    private function getExistingSlotIdForDayRepeat($slot_detail_db, $open_time, $close_time)
    {
        foreach ($slot_detail_db as $slot_db){
            if($slot_db['open_time']==$open_time && $slot_db['close_time']==$close_time){
                return $slot_db['slot_id'];
            }
        }
        return 0;
    }

}