<?php

namespace App\Http\Controllers;


use App\Models\UserOrder;
use App\Models\Language;
use App\Models\Localization;
use App\Models\UserOrderDetail;
use App\Models\UserOrderAddons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Modules\Reservation\Entities\Reservation;
use App\Helpers\CommonFunctions;
use App\Models\Restaurant;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\TimeslotController;
use App\Helpers\LocationWorkingDay;
use App\Helpers\DeliveryServiceProvider;
use App\Models\DeliveryService;
use Illuminate\Support\Facades\Auth;
use App\Models\CmsUserLog;

class ManageOrderController extends Controller
{
    /**
     * Active orders listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $statusKey = '';
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $statusArray = array("placed", "confirmed", "ready");
        $orderStatus = array(""=>"Active Orders", "placed"=>"Placed", "confirmed"=>"Confirmed", "ready"=>"Ready", "sent"=>"Sent", "archived"=>"Archived","test"=>"Test");
        //print_r($orderStatus); die;

        if($request->input('order_status'))
        {
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }
        $groupRestData = CommonFunctions::getRestaurantGroup();
        $orderData = UserOrder::select('user_orders.id','user_orders.restaurant_id','user_orders.restaurant_name','user_orders.fname','user_orders.lname','user_orders.order_type','user_orders.total_amount','user_orders.created_at',
            'user_orders.delivery_time','user_orders.delivery_date','user_orders.payment_receipt','user_orders.stripe_charge_id','user_orders.manual_update','user_orders.status',DB::raw('COUNT(user_order_details.id) AS num_item'), 'user_order_details.item')
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->groupBy('user_orders.id','user_orders.order_type','user_orders.restaurant_id','user_orders.restaurant_name','user_orders.fname','user_orders.lname','user_orders.total_amount','user_orders.created_at',
                'user_orders.delivery_time','user_orders.delivery_date','user_orders.payment_receipt','user_orders.stripe_charge_id','user_orders.manual_update','user_orders.status');

        //$searchArr = array('restaurant_id'=>0,'name'=>NULL);
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $orderData = $orderData->where('user_orders.restaurant_id',$searchArr['restaurant_id']);
        }

        /*if($request->input('key_search'))
        {
            $keySearch = $request->input('key_search');
            //$orderData = $orderData->where('user_orders.payment_receipt', 'like', $keySearch)->orWhere('user_orders.stripe_charge_id', 'like', $keySearch)->orWhere('user_orders.id', $keySearch);
            $orderData = $orderData->where(DB::raw('concat(fname," ",lname)') , 'LIKE' , $keySearch)->orWhere('user_orders.payment_receipt', 'like', $keySearch)->orWhere('user_orders.id', $keySearch);
        }*/

        if($request->input('key_search'))
        {
            $keySearch = $request->input('key_search');
            $orderData = $orderData->where(function ($query) use ($keySearch){
                $query->where('user_orders.fname', 'like', $keySearch)
                    ->orWhere('user_orders.lname', 'like', $keySearch)
                    ->orWhere(DB::raw('concat(fname," ",lname)') , 'LIKE' , $keySearch)
                    ->orWhere('user_orders.payment_receipt', 'like', $keySearch)
                    ->orWhere('user_orders.id', $keySearch);
            });

        }

        if($request->input('from_date') && $request->input('to_date'))
        {
            $fromDate = $request->input('from_date');
            $toDate = $request->input('to_date');
            $fromDateNew = date("Y-m-d", strtotime($fromDate));
            $toDateNew = date("Y-m-d", strtotime($toDate));
            $orderData = $orderData->whereBetween('user_orders.created_at', [$fromDateNew, $toDateNew]);
        }

        $orderData = $orderData->paginate(10);
        echo "<pre>";
        print_r($orderData);die;

        return view('manage_order.index', compact('orderData','groupRestData', 'searchArr', 'statusKey', 'orderStatus'));
    }
    /**
     * Active orders listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function archiveOrder()
    {

        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $statusArray = array("archived","cancelled","arrived","sent","rejected");

        $orderData = UserOrder::select('user_orders.id','user_orders.restaurant_id','user_orders.restaurant_name','user_orders.fname','user_orders.lname','user_orders.order_type','user_orders.total_amount','user_orders.created_at',
            'user_orders.delivery_time','user_orders.delivery_date','user_orders.status',DB::raw('COUNT(user_order_details.id) AS num_item'))
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->groupBy('user_orders.id','user_orders.order_type','user_orders.restaurant_id','user_orders.restaurant_name','user_orders.fname','user_orders.lname','user_orders.total_amount','user_orders.created_at','user_orders.delivery_time','user_orders.delivery_date','user_orders.status')
            ->paginate(10);

        //print_r($orderData);die;
        return view('manage_order.archive', compact('orderData'));
    }

    /**
     *  order details
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function orderDetail($id)
    {

        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $orderData = UserOrder::where('id', $id)->whereIn('restaurant_id', $restArray)->first();
        $user_id = Auth::user();

        if (empty($orderData)) {
            return Redirect::back()->with('message', 'Invalid Id');
        } else {

            //$orderData = UserOrder::find($id);
            $action = "";
            $userId = $orderData->user_id;
            $emailId = $orderData->email;
            $locationId = $orderData->restaurant_id;
            $action = $orderData->product_type;
            $orderDetail = new UserOrderDetail();
            $UserOrderAddons = new UserOrderAddons();
            $userOrder = new UserOrder();
            $userReserve = new Reservation();
            $userOrders = $userOrder->where(['user_id' => $userId, 'restaurant_id' => $orderData->restaurant_id])->whereOr('email', $emailId)->get()->count();
            $status = $orderDate = "";
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
            $orderDate = date('l d M, h:i A', $timestamp2);

            $restaurantDetails = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);

            $result = [];
            $timeSlotData = '';
            $timeSlotData = CommonFunctions::getTime_Slot($orderData->delivery_time);
            $result = CommonFunctions::getDateSlot();

            $status = $orderData->status;

            if ($status == "placed")
                $status = "confirmed";

            else if ($status == "confirmed" && $orderData->order_type == "delivery")
                $status = "sent";

            else if ($status == "confirmed" && $orderData->order_type == "carryout")
                $status = "ready";

            else if ($status == "sent")
                $status = "archived";

            else if ($status == "ready")
                $status = "archived";

            else if ($status == "archived")
                $status = "archiv";

            else if ($status == "rejected")
                $status = "rejected";

            else if ($status == "cancelled")
                $status = "cancelled";

            $userReservation = $userReserve->getReservationByUser($userId, $emailId, $restArray);

            $orderDetailData = $orderDetail->getOrderDetail($id);
            $addOnsDetail = [];
            $i = 0;
            $addonsData = [];
            foreach ($orderDetailData as $data) {
                $addons_data = [];
                $bypData = json_decode($data->menu_json, true);
                //print_r($bypData); die;
                if (!is_null($bypData)) {
                    $addons_data = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                    $addonsData[$data->id] = $addons_data;
                    $orderDetailData[$i]['addOnsDetail'] = $addons_data;
                    $i++;
                }
            }

            $orderData->created_at = date('Y-m-d H:i:s', strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at))));
            $currentDate = date('Y-m-d H:i:s');
            $currentTimestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $currentDate)));
            $deliveryTimestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');

            // die;
            return view('manage_order.details', compact('orderData', 'orderDetailData', 'userOrders', 'userReservation', 'addOnsDetail', 'status', 'orderDate', 'timeSlotData', 'result', 'restaurantDetails', 'currentTimestamp', 'deliveryTimestamp', 'action'));

            //return view('manage_order.details', compact('orderData','orderDetailData','userOrders','userReservation'));
        }
    }



    /**
     * order edit update
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function updateOrderStatus( $id, $action)
    {
        if(isset($id) && is_numeric($id)) {
            $orderData = UserOrder::find($id);
            $langId = config('app.language')['id'];
            CommonFunctions::$langId = $langId;
            $restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
            $orderItem = $label = "";

            //$user_id = Auth::user()->id;
            $previousData = array("restaurant_name"=>$orderData->restaurant_name, "fname"=>$orderData->fname, "lname"=>$orderData->lname, "email"=>$orderData->email, "order_id"=>$orderData->id, "order_type"=>$orderData->order_type, "total_amount"=>$orderData->total_amount, "delivery_time"=>$orderData->delivery_time,"delivery_date"=>$orderData->delivery_date, "status"=>$orderData->status);
            $previous_action_data = json_encode($previousData);
            //die;
            //echo $restaurantData->delivery_provider_id; die;
            //print_r($restaurantData); die;
            if($restaurantData->delivery_provider_id > 0 && $action==="confirmed" && $orderData->order_type=="delivery" && $orderData->manual_update==0){

                $deliveryServiceObj = new DeliveryServiceProvider();

                $deliveryServicesData = DeliveryService::where(array('id'=>$restaurantData->delivery_provider_id))->first();

                $deliveryRelayDetails = array(
                    'id'=>$restaurantData->delivery_provider_id,
                    'producer_key'=>$restaurantData->producer_key,
                    'order_creation_url'=>$deliveryServicesData->order_creation_url,
                    'api_key'=>$restaurantData->delivery_provider_apikey
                );
                //print_r($deliveryRelayDetails); die;
                $ms = microtime(true);
                $deliveryServiceObj->deliveryRelayDetails = $deliveryRelayDetails;

                if(!$deliveryServiceObj->orderCreation($orderData)){

                    $response = $deliveryServiceObj->orderCreationSuccess;
                    //$response['message'] = $deliveryServiceObj->orderCreationSuccess['message'];
                    //print_r($deliveryServiceObj->orderCreationSuccess); die;
                    $me = microtime(true) - $ms;
                    $response['xtime'] = $me;
                    $orderData->updated_at = now();
                    $orderData->manual_update = 1;
                    $orderData->save();
                    /******** SMS Send **** @20-10-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $orderData->id,
                        'sms_module' => 'order',
                        'action' => 'confirmed',
                        'sms_to' => 'customer'
                    );
                    CommonFunctions::getAndSendSms($sms_keywords);
                    /************ END SMS *************/
                    //print_r($response); die;
                    if(isset($response['message'])){
                        return $response;
                    }
                    else{
                        $error['message'] = "Something went wrong";
                        return $error;
                    }

                    //$message = "Something went wrong";
                    //return $message;
                }

            }

            $orderData->updated_at = now();
            $orderData->status = $action;
            $orderData->save();

            $user_id = Auth::user()->id;
            $updateData = array("restaurant_name"=>$orderData->restaurant_name, "fname"=>$orderData->fname, "lname"=>$orderData->lname, "email"=>$orderData->email, "order_id"=>$orderData->id, "order_type"=>$orderData->order_type, "total_amount"=>$orderData->total_amount, "delivery_time"=>$orderData->delivery_time,"delivery_date"=>$orderData->delivery_date, "status"=>$orderData->status);
            $action_data = json_encode($updateData);
            $logData = array(
                'user_id' => $user_id,
                'module_name' => 'ManageOrder',
                'restaurant_id' => Auth::user()->restaurant_id, //$orderData->restaurant_id,   // PE-3221 - restaurant id should be based who's logged in Branch/Brand
                'tablename' => 'user_orders',
                'action_type' => 'update',
                'previous_action_data' => $previous_action_data,
                'action_data' => $action_data);

            //return $logData;
            $logs = CmsUserLog::create($logData);
            //print_r($logData); die;
            $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
            $cardInfo = $orderData->card_type.' ('.$orderData->card_number.')';
            $orderDetail = new UserOrderDetail();
            $orderDetailData = $orderDetail->getOrderDetail($id);
            $orderItem = '';

            foreach($orderDetailData as $data){
                $addons_data = [];
                $bypData = json_decode($data->menu_json, true );
                //print_r($bypData); die;
                if (!is_null($bypData)) {
                    $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                }

                $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
        <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
        <tr style="padding:0;text-align:left;vertical-align:top">
        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
        <b>'.$data->quantity.' '.$data->item.'</b>';

                $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                $label = strtolower($data->size) != 'full' ? $data->size .' ' : '';
                if($data->is_byp==1 && $result){
                    foreach($result as $key=>$data1){
                        $label .= $key;
                        $label .= '['.$data1.']>';
                    }

                }
                if($data->is_byp==0 && $result) {
                    foreach ($result as $data1) {
                        $label .= $data1['addons_name'];
                        $label .= '[' . $data1['addons_option'] . ']>';
                    }
                }
                $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
        <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>$</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
        <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
            }
            $url = $restaurantData->source_url;
            $resultData = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);

            $is_asap_order = "";
            if($orderData->is_asap_order===1)
                $is_asap_order = "(Possibly sooner!)";
            if(Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
            } else {
                $curSymbol = config('constants.currency');
            }
            $mailKeywords = array(
                'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                'ORDER_NUMBER' => $orderData->payment_receipt,
                'USER_NAME' => ucfirst($orderData->fname),
                'ORDER_TYPE' => ucfirst($orderData->order_type),
                'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp).' '.$is_asap_order,
                'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                'DELIVERY_ADD' => $orderData->address.' '.$orderData->city,
                'PHONE' => $orderData->phone,
                'SUB_TOTAL' => '<span>'.$curSymbol.'</span>'.$orderData->order_amount,
                'TOT_AMOUNT' => '<span>'.$curSymbol.'</span>'.$orderData->total_amount,
                'ITEM_DETAIL' => $orderItem,
                'CARD_NUMBER' => $cardInfo,
                'URL' => $restaurantData->source_url,
                'REST_NAME' => strtolower($restaurantData->restaurant_name),
                'SITE_URL' => $restaurantData->source_url,
                'title' => "Order Confirmation",
                'APP_BASE_URL'=>config('constants.mail_image_url'),
            );
            $tipAmount = $orderData->tip_amount;
            if($orderData->user_comments!=""){
                $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
            }
            else {
                $mailKeywords['SPECIAL_INFO'] = '';
            }

            if($orderData->tax==0.00){
                $mailKeywords['TAX'] = '';
                $mailKeywords['TAX_TITLE'] = '';
                $mailKeywords['DISPLAY'] = 'none';
            } else{
                $mailKeywords['TAX'] = '<span>'.$curSymbol.'</span>'.$orderData->tax;
                $mailKeywords['TAX_TITLE'] = "Taxes";
                $mailKeywords['DISPLAY'] = 'table';
            }

            if($orderData->tip_amount==0.00){
                $mailKeywords['TIP_AMOUNT'] = '';
                $mailKeywords['TIP_TITLE'] = '';
                $mailKeywords['DISPLAY_TIP'] = 'none';

            } else{
                $mailKeywords['TIP_AMOUNT'] ='<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                            <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
<p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">'.$orderData->tip_percent.'<span>%</span></p>
                            </th></tr></table></th>
                            <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$tipAmount.'</p></th></tr></table></th>
                        </tr></tbody></table>';
                $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                $mailKeywords['DISPLAY_TIP'] = 'table';
            }

            if($orderData->order_type=="delivery") {

                $address = $orderData->address;
                if ($orderData->address2) {
                    $address = $address . ' ' . $orderData->address2;
                }
                $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                //echo $address;

                $mailKeywords['DELIVERY_CHARGE'] = '<span>'.$curSymbol.'</span>'.$orderData->delivery_charge;
                $mailKeywords['USER_ADDRESS'] = $address;
                $mailTemplate = 'delivery_confirm_user';
            } else {
                $mailTemplate = 'carryout_confirm_user';
                $mailKeywords['USER_ADDRESS'] = '';
                $img = "";
                if($action=="ready"){
                    $heading = "We have finished preparing your takeout order and can't wait for you to enjoy it.";
                    $img = '<a href="#"><img src="https://dev-keki.bravvurashowcase.com/mailer-images/keki/get-direction.jpg" alt="" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></a>';
                }
                else{
                    $heading = "You've put together an amazing order.";
                    $img = '<a href="'.$url.'"><img src="https://dev-keki.bravvurashowcase.com/mailer-images/keki/manage-order.jpg" alt="" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></a>';
                }

                $mailKeywords['HEAD'] = $heading;
                $mailKeywords['IMG'] = $img;
                $mailKeywords['ACTION'] = ucfirst($action);

            }

            $config['to'] = $orderData->email;
            $config['subject'] = "Your order has been ".$action;
            $config['from_address'] = $resultData['custom_from'];
            $config['from_name'] = $resultData['restaurantParentName'];

            $controller = new NotificationController;
            if(($action=="archived") || ($action=="sent"))
            {
                return $orderData;
            }
            else
            {
                if($action == "confirmed"){
                    /******** SMS Send **** @18-10-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $orderData->id,
                        'sms_module' => 'order',
                        'action' => 'confirmed',
                        'sms_to' => 'customer'
                    );
                    CommonFunctions::getAndSendSms($sms_keywords);
                    /************ END SMS *************/
                }
                //$controller->sendMail($mailTemplate, $restaurantData->parent_restaurant_id, $config, $mailKeywords);
                return $orderData;
            }

        }
    }

    public function cancelOrder(Request $request)
    {
        $id = $request->input('oid');
        $reason = $request->input('reason');
        $status = $request->input('status');

        if(isset($id) && is_numeric($id)) {
            $orderData = UserOrder::find($id);

            $orderData->updated_at = now();
            $orderData->status = $status;
            $orderData->restaurants_comments = $reason;
            $orderData->save();

            $langId = config('app.language')['id'];
            CommonFunctions::$langId = $langId;
            $restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
            $orderItem = $label = "";

            $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
            $cardInfo = $orderData->card_type.' ('.$orderData->card_number.')';
            $orderDetail = new UserOrderDetail();
            $orderDetailData = $orderDetail->getOrderDetail($id);
            $orderItem = '';
            foreach($orderDetailData as $data){
                $addons_data = [];
                $bypData = json_decode($data->menu_json, true );

                if (!is_null($bypData)) {
                    $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                }

                $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
        <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
        <tr style="padding:0;text-align:left;vertical-align:top">
        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
        <b>'.$data->quantity.' '.$data->item.'</b>';

                $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                $label = strtolower($data->size) != 'full' ? $data->size .' ' : '';
                if($data->is_byp==1 && $result){
                    foreach($result as $key=>$data1){
                        $label .= $key;
                        $label .= '['.$data1.']>';
                    }

                }
                if($data->is_byp==0 && $result) {
                    foreach ($result as $data1) {
                        $label .= $data1['addons_name'];
                        $label .= '[' . $data1['addons_option'] . ']>';
                    }
                }
                $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
        <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>$</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
        <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
            }
            $is_asap_order = "";
            if($orderData->is_asap_order===1)
                $is_asap_order = "(Possibly sooner!)";
            $resultData = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);
            if(Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
            } else {
                $curSymbol = config('constants.currency');
            }
            $mailKeywords = array(
                'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                'RESTAURANT_ADD' => $restaurantData->address,
                'ORDER_NUMBER' => $orderData->payment_receipt,
                'USER_NAME' => ucfirst($orderData->fname),
                'ORDER_TYPE' => ucfirst($orderData->order_type),
                'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp).' '.$is_asap_order,
                'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                'SUB_TOTAL' => '<span>'.$curSymbol.'</span>'.$orderData->order_amount,
                'TOT_AMOUNT' => '<span>'.$curSymbol.'</span>'.$orderData->total_amount,
                'CARD_NUMBER' => $cardInfo,
                'REASON' => $reason,
                'ITEM_DETAIL' => $orderItem,
                'URL' => $restaurantData->source_url,
                'REST_NAME' => strtolower($restaurantData->restaurant_name),
                'SITE_URL' => $restaurantData->source_url,
                'title' => "Cancel Order",
                'APP_BASE_URL'=>config('constants.mail_image_url'),
            );
            $tipAmount = $orderData->tip_amount;
            if($orderData->tip_amount==0.00){
                $mailKeywords['TIP_AMOUNT'] = '';
                $mailKeywords['TIP_TITLE'] = '';
                $mailKeywords['DISPLAY_TIP'] = 'none';

            } else{
                $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                $mailKeywords['TIP_AMOUNT'] ='<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                            <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
<p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">'.$orderData->tip_percent.'<span>%</span></p>
                            </th></tr></table></th>
                            <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$tipAmount.'</p></th></tr></table></th>
                        </tr></tbody></table>';
                $mailKeywords['DISPLAY_TIP'] = 'table';
            }

            if($orderData->user_comments!=""){
                $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
            }
            else {
                $mailKeywords['SPECIAL_INFO'] = '';
            }

            if($orderData->tax==0.00){
                $mailKeywords['TAX'] = '';
                $mailKeywords['TAX_TITLE'] = '';
                $mailKeywords['DISPLAY'] = 'none';
            } else{
                $mailKeywords['TAX'] = '<span>'.$curSymbol.'</span>'.$orderData->tax;
                $mailKeywords['TAX_TITLE'] = "Taxes";
                $mailKeywords['DISPLAY'] = 'table';
            }

            if($orderData->order_type=="delivery") {

                $address = $orderData->address;
                if ($orderData->address2) {
                    $address = $address . ' ' . $orderData->address2;
                }
                $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                //echo $address;

                $mailKeywords['DELIVERY_CHARGE'] = '<span>'.$curSymbol.'</span>'.$orderData->delivery_charge;
                $mailKeywords['USER_ADDRESS'] = $address;
                $mailTemplate = 'delivery-cancel';
            } else {
                $mailTemplate = 'carryout_cancel';
                $mailKeywords['USER_ADDRESS'] = '';
            }

            $config['to'] = $orderData->email;
            $config['subject'] = "Your order has been cancelled";
            $config['from_address'] = $resultData['custom_from'];
            $config['from_name'] = $resultData['restaurantParentName'];
            $controller = new NotificationController;
            echo $status;die;
            if($status=="rejected"){
                /******** SMS Send **** @18-10-2018 by RG *****/
                $sms_keywords = array(
                    'order_id' => $orderData->id,
                    'sms_module' => 'order',
                    'action' => 'rejected',
                    'sms_to' => 'customer'
                );
                CommonFunctions::getAndSendSms($sms_keywords);
                /************ END SMS *************/
                $controller->sendMail($mailTemplate, $restaurantData->parent_restaurant_id, $config, $mailKeywords);
            }

            if($status=="cancelled"){
                /******** SMS Send **** @18-10-2018 by RG *****/
                $sms_keywords = array(
                    'order_id' => $orderData->id,
                    'sms_module' => 'order',
                    'action' => 'canceled',
                    'sms_to' => 'customer'
                );
                CommonFunctions::getAndSendSms($sms_keywords);
                /************ END SMS *************/
                $controller->sendMail($mailTemplate, $restaurantData->parent_restaurant_id, $config, $mailKeywords);
            }
            return $orderData;
        }

        //return Redirect::back()->withSuccess('Language set successfully!');
    }

    public function updateTimeSlot(Request $request)
    {
        $id = $request->input('oid');
        $sdate = $request->input('sdate');
        $stime = $request->input('stime');

        if (isset($id) && is_numeric($id)) {
            $orderData = UserOrder::find($id);
            $orderData->updated_at = now();
            $orderData->delivery_date = $sdate;
            $orderData->delivery_time = $stime;
            $orderData->save();

            return $orderData;
        }
    }
}
