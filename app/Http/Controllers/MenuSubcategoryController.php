<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Models\MenuSettingCategory;
use App\Models\Restaurant;
use App\Models\MenuCategory;
use App\Models\MenuSubCategory;
use Illuminate\Support\Facades\Input;
use App\Helpers\CommonFunctions;
use Intervention\Image\Facades\Image;
use File;
use App\Models\Language;
use App\Models\MenuSubCategoryLanguage;
use DB;

class MenuSubcategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan view:clear');
        //exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan cache:clear');
    }
    public function getDropDownOptionNlevel($data,$level,$default=0){
        
        $lvl=$level;
        $str='';
        if($level==1){

            $parentspace='';
        }else{
            $parentspace='--';
        }
        $parentspace=str_repeat($parentspace,$level);
       
        $childspace='----';
      //  $childspace=str_repeat($childspace,$level);
        foreach($data as $i=>$item){
            $parentclass='class="optionChild"';
             
            if($lvl==1){
                $parentclass='class="optionGroup"';                          
            }
            if($lvl!=1){
                $spc=$level>2?1:2; 
                $parentspace=str_repeat($parentspace,$spc);
            }
            $selec_cat=($default==$item->id)? 'selected="selected"':'';
            $str.= '<option value="'.$item->id.'"  '.$parentclass.'     '.$selec_cat.'>'.$parentspace. $item->name .'</option>' ;
            $level++;  
            if(count($item->subcats)){
                foreach($item->subcats as $i=>$item){
                    $parentclass='class="optionChild"';
                    if($lvl!=1){
                       
                      $childspace=str_repeat($parentspace,2);
                    }
                    $selec_cat=($default==$item->id)? 'selected="selected"':'';
                   $str.= '<option value="'.$item->id.'"  '.$parentclass.'    '.$selec_cat.'>'.$childspace. $item->name .'</option>' ;
                  if(count($item->subcats)){
                    $str.=$this->getDropDownOptionNlevel($item->subcats,$level,$default);
                  }

                }    
            }

        }

          return $str;
      }



      
    public function nthLevelSubCats($menu_category_id,$restaurant_id,$parent=0,$exclude=NULL){
        $categoryData = MenuSubCategory::where('restaurant_id', '=', $restaurant_id)
                        ->where('menu_category_id', '=', $menu_category_id)
                        ->where('parent', '=', $parent);
        if($exclude!=NULL){
            $categoryData = $categoryData->where('id', '!=', $exclude);
        }
        $categoryData = $categoryData->orderBy('name', 'DESC')->get();
        if(count($categoryData)){
            foreach($categoryData as  &$cat){
                $cat->subcats=$this->nthLevelSubCats($menu_category_id,$restaurant_id,$cat->id,$exclude);
            }
        }
        return  $categoryData;
    }
    public function getSubCatHtml($menu_category_id,$restaurant_id,$parent=0,$exclude=NULL,$default=0){
        $data =$this->nthLevelSubCats($menu_category_id,$restaurant_id,$parent,$exclude); 
       $catlist=$this->getDropDownOptionNlevel($data,1,$default);

       return $catlist=' <option value="">Select Parent Sub Category</option>'.$catlist;

    }
    public function getSubcatsGroup(Request $request){
        $restaurant_id = $request->get('rid');
        $menu_category_id = $request->get('menu_category_id');
        $result=[];        
        if(!empty($restaurant_id) && is_numeric($restaurant_id) && !empty($menu_category_id) && is_numeric($menu_category_id)) {
           $result =$this->getSubCatHtml($menu_category_id,$restaurant_id,0,NULL,0); 

         //$result =$this->nthLevelSubCats($menu_category_id,$restaurant_id,0); 
         //$result=$this->getDropDownOptionNlevel($result,1);           
        }
          
        return response()->json(array('data' =>$result ), 200);
      
    }
    public function create(Request $request)
    {

        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
       // $groupRestData = CommonFunctions::getRestaurantGroup();
        $groupRestData = CommonFunctions::getRestaurantGroupAll(); //Requirement by Pranav to show all restaurants
        $selected_rest_id = 0;
        if(count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if(count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        } //echo "<pre>";print_r( $groupRestData);die;
        return view('menu_subcategory.create', compact('groupRestData', 'selected_rest_id', 'languageData'));
    }

    public function edit(Request $request,$id, $languageId = 1)
    {
        if(isset($id) && is_numeric($id)) {
            $categoryData  = array();
            #$groupRestData = Restaurant::group();
           // $groupRestData = CommonFunctions::getRestaurantGroup();
            $groupRestData = CommonFunctions::getRestaurantGroupAll(); //Requirement by Pranav to show all restaurants
            $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
            $menuSubcategory = MenuSubCategory::find($id);
	    $menuSubCategoryLangObj = MenuSubCategoryLanguage::where([
                    'menu_sub_categories_id' => $menuSubcategory->id,
                    'language_id' => $languageId,
                ])->first();
            $restaurant_id     = \Auth::user()->restaurant_id;

            $user_groups = DB::table('user_groups')
                ->whereIn('restaurant_id', [0,$restaurant_id])
                ->where(['status'=>1])
                ->get();
	    if ($menuSubCategoryLangObj !== null) {
		    $menuSubcategory->name = $menuSubCategoryLangObj->name;
		    $menuSubcategory->description = $menuSubCategoryLangObj->description;
		    $menuSubcategory->sub_description = $menuSubCategoryLangObj->sub_description;
	    }
            if (!empty($menuSubcategory->restaurant_id)) {
                $categoryData = MenuCategory::where('restaurant_id', '=', $menuSubcategory->restaurant_id)->orderBy('name', 'DESC')->get();
            }
           // $result =$this->nthLevelSubCats($menuSubcategory->menu_category_id,$menuSubcategory->restaurant_id,0,$id); 
            //$sub_cats=$this->getDropDownOptionNlevel($result,1,$menuSubcategory->parent);

            $sub_cats =$this->getSubCatHtml($menuSubcategory->menu_category_id,$menuSubcategory->restaurant_id,0,$id,$menuSubcategory->parent); 

            return view('menu_subcategory.edit', compact('user_groups','groupRestData','menuSubcategory','categoryData','sub_cats','languageData'));
        }
        return Redirect::back();
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:100',
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'status' => 'required|in:0,1',
            'menu_category_id' => 'required|numeric|exists:menu_categories,id',
            'priority' => 'required|integer'
        ];
        $rulesMsg = [
            'name.*' => 'Please enter valid name upto 100 chars',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status',
            'menu_category_id.*' => 'Please Select Category',
            'priority' => 'Please enter Priority'
        ];
        $validator = Validator::make(Input::all(), $rules, $rulesMsg);
        if ($validator->fails()) {
            $newMenuSettingCategoryData = $newCategoryData = array();
            if(Input::get('restaurant_id') && is_numeric(Input::get('restaurant_id')))
            {
                $newCategoryData = MenuCategory::where('restaurant_id', '=', Input::get('restaurant_id'))->orderBy('name', 'DESC')->get();
            }
            return redirect()->back()->withErrors($validator->errors())->withInput()->with('newCategoryData',$newCategoryData);
        } else {
            $menuSubcategoryObj = MenuSubCategory::find($id);
            $menuSubcategoryObj->name       = Input::get('name');
            $menuSubcategoryObj->restaurant_id      = Input::get('restaurant_id');
            $menuSubcategoryObj->language_id      = Input::get('language_id');
            $menuSubcategoryObj->menu_category_id      = Input::get('menu_category_id');
            $menuSubcategoryObj->status = Input::get('status');
            $menuSubcategoryObj->priority = Input::get('priority');
            $menuSubcategoryObj->description = Input::get('description');
            $menuSubcategoryObj->sub_description = Input::get('sub_description');
            $menuSubcategoryObj->promotional_banner_url = $request->input('promotional_banner_url')??NULL;
            $menuSubcategoryObj->pos_id = $request->input('pos_id')??NULL;

            if(Input::has('parent') && !empty(Input::get('parent')) && is_numeric(Input::get('parent'))){
                $menuSubcategoryObj->parent=Input::get('parent');
            }else{
               $menuSubcategoryObj->parent=0;
            }
            if($request->has('is_popular') && $request->input('is_popular') == 'on' ) {
                $menuSubcategoryObj->is_popular  = 1;
            }else {
                $menuSubcategoryObj->is_popular  = 0;
            }
            if($request->has('is_favourite') && $request->input('is_favourite') == 'on' ) {
                $menuSubcategoryObj->is_favourite  = 1;
            }else {
                $menuSubcategoryObj->is_favourite  = 0;
            }
            if($request->input('image_svg_delete') && $request->input('image_svg_delete')=='1') {
                unlink(public_path($menuSubcategoryObj->image_svg));
                unlink(public_path($menuSubcategoryObj->thumb_image_med_svg));
                $menuSubcategoryObj->image_svg = NULL;
                $menuSubcategoryObj->thumb_image_med_svg = NULL;
            }
            if($request->input('image_png_delete') && $request->input('image_png_delete')=='1') {
                unlink(public_path($menuSubcategoryObj->image_png));
                unlink(public_path($menuSubcategoryObj->thumb_image_med_png));
                $menuSubcategoryObj->image_png = NULL;
                $menuSubcategoryObj->thumb_image_med_png = NULL;
            }
            if($request->input('image_png_selected_delete') && $request->input('image_png_selected_delete')=='1') {
                unlink(public_path($menuSubcategoryObj->image_png_selected));
                unlink(public_path($menuSubcategoryObj->thumb_image_med_png_selected));
                $menuSubcategoryObj->image_png_selected = NULL;
                $menuSubcategoryObj->thumb_image_med_png_selected = NULL;
            }

            if($request->input('promotional_banner_image_deleted') && $request->input('promotional_banner_image_deleted')=='1') {
                unlink(public_path($menuSubcategoryObj->promotional_banner_image));
                $menuSubcategoryObj->promotional_banner_image = NULL;
            }
            if($request->input('promotional_banner_image_mobile_deleted') && $request->input('promotional_banner_image_mobile_deleted')=='1') {
                unlink(public_path($menuSubcategoryObj->promotional_banner_image_mobile));
                $menuSubcategoryObj->promotional_banner_image_mobile = NULL;
            }
            //////

            $promo_banners=json_decode($menuSubcategoryObj->promo_banners,1);
            $old_positions=$promo_banners;
            $banners=NULL;
            if(!$request->has('Type')){
                $banners=NULL;
                $promo_banners=NULL;
            }else{

                $imageRelPath = config('constants.image.rel_path.subcategory');
                $imagePath = config('constants.image.path.subcategory');
                if(empty($menuSubcategoryObj->promo_banners)){
                    $promo_banners=[];
                    if($request->has('Type')){

                        $field_type=$request->input('Type');

                        foreach($field_type as $key=> $type) {

                            if(!empty($type)) {

                                $Display = $request->input('Display')[$key] ?? '';
                                $Position = $request->input('Position')[$key] ?? '';
                                $AltText = $request->input('AltText')[$key] ?? '';

                                $Link = $request->input('Link')[$key] ?? '';
                                $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                $Status = $request->input('Status')[$key] ?? 0;
                                $imageNameBase =  'promo_' . str_replace(' ', '_', $Position);

                                if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                    $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                    $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                    $Value_Mobile = $imageArr['image'];
                                } else {
                                    $Value_Mobile = $request->input('Value_Mobile')[$key] ?? '';
                                }
                                if ($type!='Text' && @$request->file('Value')[$key]) {
                                    $imageName = $imageNameBase .time(). '_Banner_';
                                    $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                    $Value = $imageArr['image'];
                                } else {
                                    $Value = $request->input('Value')[$key] ?? '';
                                }
                                if ((!empty($Value) || !empty($Value_Mobile)) && !empty($type)) {

                                    $promo_banners[] = [
                                        'Id' => $key,
                                        'Display' => $Display,
                                        'Position'=>$Position,
                                        'Type' => $type,
                                        'Value' => $Value,
                                        'Value_Mobile' => $Value_Mobile,
                                        'AltText'=>$AltText,
                                        'Link' => $Link,
                                        'UserGroup' => $UserGroup,
                                        'SortOrder' => $SortOrder,
                                        'Status' => $Status
                                    ];

                                }

                            }

                        }
                    }
                }else{
                    $old_id=[];
                    $alloldids=$request->input('ID');
                    foreach ($alloldids as $oldid){
                        if($oldid!=''){
                            $old_id[]=$oldid;
                        }

                    }
                    //$old_id = array_filter($request->input('ID'));
                    $updated_positions=[];
                    $last_id=0;
                    $last_ids=[];
                    if(count($old_positions)){
                        foreach ($old_positions as $pos){

                            if (in_array($pos['Id'], $old_id)){
                                $last_id=(int)$pos['Id'];
                                $last_ids[]=$last_id;
                                //  unset($pos);
                                $updated_positions[]=$pos;

                            }
                        }

                        $last_id=count($last_ids)?max($last_ids):0;
                    }

                    $newPosItem =$updated_positions;
                    $field_type=$request->input('Type');
                    $new_additions=[];

                    foreach($field_type as $key=> $type){

                        if(!isset($request->input('ID')[$key]) || $request->input('ID')[$key]==''){
                            //print_r("iff---");
                            if(!empty($type)) {

                                $Display = $request->input('Display')[$key] ?? '';
                                $Position = $request->input('Position')[$key] ?? '';
                                $AltText = $request->input('AltText')[$key] ?? '';

                                $Link = $request->input('Link')[$key] ?? '';
                                $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                $Status = $request->input('Status')[$key] ?? 0;
                                $imageNameBase = 'promo_' . str_replace(' ', '_', $Position);

                                if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                    $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                    $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                    $Value_Mobile = $imageArr['image'];
                                } else {
                                    $Value_Mobile = $request->input('Value_Mobile')[$key] ?? '';
                                }
                                if ($type!='Text' && @$request->file('Value')[$key]) {
                                    $imageName = $imageNameBase .time(). '_Banner_';
                                    $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                    $Value = $imageArr['image'];
                                } else {
                                    $Value = $request->input('Value')[$key] ?? '';
                                }
                                if ((!empty($Value) || !empty($Value_Mobile)) && !empty($type)) {
                                    $last_id = $last_id + 1;
                                    $new_additions[] = [
                                        'Id' => $last_id,
                                        'Display' => $Display,
                                        'Position'=>$Position,
                                        'Type' => $type,
                                        'Value' => $Value,
                                        'Value_Mobile' => $Value_Mobile,
                                        'AltText'=>$AltText,
                                        'Link' => $Link,
                                        'UserGroup' => $UserGroup,
                                        'SortOrder' => $SortOrder,
                                        'Status' => $Status
                                    ];
                                }

                            }

                        }else{

                            foreach ($updated_positions as $upkey=>$posup){
                                //print('hi2-');
                                //dump($posup);
                                if(trim($posup['Id'])==trim($request->input('ID')[$key])) {
                                    $Display = $request->input('Display')[$key] ?? '';
                                    $Position = $request->input('Position')[$key] ?? '';
                                    $AltText = $request->input('AltText')[$key] ?? '';

                                    $Link = $request->input('Link')[$key] ?? '';
                                    $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                    $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                    $Status = $request->input('Status')[$key] ?? 0;
                                    $imageNameBase = 'promo_' . str_replace(' ', '_', $Position);
                                    // $last_id = $last_id + 1;
                                    $newmatched=[
                                        'Id' => $posup['Id'],
                                        'Display' => $Display,
                                        'Position'=>$Position,
                                        'Type' => $type,
                                        'AltText'=>$AltText,
                                        'Link' => $Link,
                                        'UserGroup' => $UserGroup,
                                        'SortOrder' => $SortOrder,
                                        'Status' => $Status
                                    ];

                                    if ($type!='Text' && @$request->file('Value')[$key]) {
                                        $imageName = $imageNameBase.time() . '_Banner_';
                                        $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                        $newmatched['Value']= $imageArr['image'];
                                    } else {
                                        if($type=='Text'){
                                            $newmatched['Value'] = $request->input('Value')[$key] ?? '';

                                        }else{
                                            $newmatched['Value'] = $posup['Value'];
                                        }
                                    }
                                    //$newPosItem[$upkey]=$newmatched;
                                    if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                        $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                        $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                        $newmatched['Value_Mobile']= $imageArr['image'];
                                    } else {
                                        if($type=='Text'){
                                            $newmatched['Value_Mobile'] = $request->input('Value_Mobile')[$key] ?? '';

                                        }else{
                                            $newmatched['Value_Mobile'] = $posup['Value_Mobile'];
                                        }
                                    }
                                    $newPosItem[$upkey]=$newmatched;
                                }

                            }

                        }

                    }

                    if(count($new_additions)){
                        $newPosItem=array_merge($newPosItem,$new_additions);
                    }

                    $promo_banners=$newPosItem;
                }
            }
            //print_r($promo_banners);die;

            $menuSubcategoryObj->promo_banners =(is_array($promo_banners) && count($promo_banners))? json_encode($promo_banners):NULL;


            ///

            $menuSubcategoryObj->save();
	    $menuSubCategoryLangObj = MenuSubCategoryLanguage::where([
                        'menu_sub_categories_id' => $menuSubcategoryObj->id,
                        'language_id' => $request->input('language_id'),
                    ])->first();

            if ($menuSubCategoryLangObj !== null) {
                $menuSubCategoryLangArr = [
                    'description' => $request->input('description'),
                    'sub_description' => $request->input('sub_description'),
                    'name' => strtolower($request->input('name')),
                ];
                $menuSubCategoryLangObj->update($menuSubCategoryLangArr);
            } else {
                $menuSubCategoryLangArr = [
                    'language_id' => $request->input('language_id'),
                    'description' => $request->input('description'),
                    'sub_description' => $request->input('sub_description'),
                    'name' => strtolower($request->input('name')),
                    'menu_sub_categories_id' => $menuSubcategoryObj->id,
                ];
                MenuSubCategoryLanguage::create($menuSubCategoryLangArr);
            }
            $imageArr = [];
            if ($request->hasFile('image_svg')) {
                $image                           = $request->file('image_svg');
                $imageRes                        = $this->file_upload($image, $menuSubcategoryObj->id, $request->input('restaurant_id'));
                $imageArr['image_svg']           = $imageRes[0];
                $imageArr['thumb_image_med_svg'] = $imageRes[1];
            }
            if ($request->hasFile('image_png')) {  
                $image                           = $request->file('image_png');
                $imageRes                        = $this->file_upload($image, $menuSubcategoryObj->id, $request->input('restaurant_id'));

                
                $imageArr['image_png']           = $imageRes[0];
                $imageArr['thumb_image_med_png'] = $imageRes[1];
            }
            if ($request->hasFile('image_png_selected')) {
                $image                                    = $request->file('image_png_selected');
                $imageRes                                 = $this->file_upload($image, $menuSubcategoryObj->id, $request->input('restaurant_id'));
                $imageArr['image_png_selected']           = $imageRes[0];
                $imageArr['thumb_image_med_png_selected'] = $imageRes[1];
            }
            if ($request->hasFile('promotional_banner_image')) {
                $image = $request->file('promotional_banner_image');

                //echo 'asdasd';exit;
                $imageRes = $this->category_file_upload($image, $menuSubcategoryObj->id);
                $imageArr['promotional_banner_image'] = $imageRes[0];


            }
            if ($request->hasFile('promotional_banner_image_mobile')) {
                $image = $request->file('promotional_banner_image_mobile');

                //echo 'asdasd';exit;
                $imageRes = $this->category_file_upload($image, $menuSubcategoryObj->id);
                $imageArr['promotional_banner_image_mobile'] = $imageRes[0];


            }
            if (count($imageArr) > 0) {
                MenuSubCategory::where('id', $menuSubcategoryObj->id)->update($imageArr);
            }
            return Redirect::back()->with('message','Subcategory updated successfully');
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:100',
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            'status' => 'required|in:0,1',
            'menu_category_id' => 'required|numeric|exists:menu_categories,id',
            'priority' => 'required|integer',
        ], [
            'name.*' => 'Please enter valid name upto 100 chars',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            'status.*' => 'Please select Status',
            'menu_category_id.*' => 'Please select Category',
            'priority' => 'Please enter Priority'
        ]);
        if($validator->passes())
        {
	    $lanId = $request->input('language_id');
	    if($lanId==1){
		    $menuSubcategoryArr = ['restaurant_id'=>$request->input('restaurant_id'),
		        'language_id'=>$request->input('language_id'),
		        'name'=>$request->input('name'),
		        'status'=>$request->input('status'),
		        'menu_category_id' => $request->input('menu_category_id'),
		        'priority' => $request->input('priority'),
		        'description' => $request->input('description'),
		        'sub_description' => $request->input('sub_description'),
		        'image_class'   => $request->input('image_class'),
		    ];
	    }else{
		   $menuSubcategoryArr = ['restaurant_id'=>$request->input('restaurant_id'),
		        'language_id'=>$request->input('language_id'),
		         'status'=>$request->input('status'),
		        'menu_category_id' => $request->input('menu_category_id'),
		        'priority' => $request->input('priority'),
		        'image_class'   => $request->input('image_class'),
		    ]; 
	    } 
            if($request->has('parent') && !empty($request->input('parent')) && is_numeric($request->input('parent'))){
                $menuSubcategoryArr['parent']=$request->input('parent');
            }
            if($request->has('is_popular') && $request->input('is_popular') == 'on' ) {
                $menuSubcategoryArr['is_popular']  = 1;
            }else {
                $menuSubcategoryArr['is_popular']  = 0;
            }
            if($request->has('is_favourite') && $request->input('is_favourite') == 'on' ) {
                $menuSubcategoryArr['is_favourite']  = 1;
            }else {
                $menuSubcategoryArr['is_favourite']  = 0;
            }
            $menuSubcategoryArr['promotional_banner_url']=$request->input('promotional_banner_url')??NULL;
            $menuSubcategoryArr['pos_id']=$request->input('pos_id')??NULL;
            $menuSubcategoryObj = MenuSubCategory::create($menuSubcategoryArr);
            $menuSubCategoryLangArr = [
                'language_id' => $request->input('language_id'),
                'description' => $request->input('description'),
                'sub_description' => $request->input('sub_description'),
                'name' => strtolower($request->input('name')),
                'menu_sub_categories_id' => $menuSubcategoryObj->id,
            ];
            MenuSubCategoryLanguage::create($menuSubCategoryLangArr);	


            $imageArr = [];
            if ($request->hasFile('image_svg')) {
                $image                           = $request->file('image_svg');
                $imageRes                        = $this->file_upload($image, $menuSubcategoryObj->id, $request->input('restaurant_id'));
                $imageArr['image_svg']           = $imageRes[0];
                $imageArr['thumb_image_med_svg'] = $imageRes[1];
            }
            if ($request->hasFile('image_png')) {
                $image                           = $request->file('image_png');
                $imageRes                        = $this->file_upload($image, $menuSubcategoryObj->id, $request->input('restaurant_id'));
                $imageArr['image_png']           = $imageRes[0];
                $imageArr['thumb_image_med_png'] = $imageRes[1];
            }
            if ($request->hasFile('image_png_selected')) {
                $image                                    = $request->file('image_png_selected');
                $imageRes                                 = $this->file_upload($image, $menuSubcategoryObj->id, $request->input('restaurant_id'));
                $imageArr['image_png_selected']           = $imageRes[0];
                $imageArr['thumb_image_med_png_selected'] = $imageRes[1];
            }
            if ($request->hasFile('promotional_banner_image')) {
                $image = $request->file('promotional_banner_image');
                $imageRes = $this->category_file_upload($image, $menuSubcategoryObj->id);
                $imageArr['promotional_banner_image'] = $imageRes[0];

            }
            if ($request->hasFile('promotional_banner_image_mobile')) {
                $image = $request->file('promotional_banner_image_mobile');
                $imageRes = $this->category_file_upload($image, $menuSubcategoryObj->id);
                $imageArr['promotional_banner_image_mobile'] = $imageRes[0];

            }
           
            if (count($imageArr) > 0) {
                MenuSubCategory::where('id', $menuSubcategoryObj->id)->update($imageArr);
            }

            return Redirect::back()->with('message','Subcategory added successfully');
        }
        $categoryData = array();
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $categoryData = MenuCategory::where('restaurant_id', '=',trim($request->input('restaurant_id')))->get();
        }
        return redirect()->back()->withErrors($validator->errors())->withInput()
            ->with('categoryData',$categoryData);
    }

    public function index(Request $request)
    {
        $searchArr = array('restaurant_id'=>0,'menu_category_id'=>0,'name'=>NULL);
        $categoryData = array();
        #$restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get(); 
        $selected_rest_id = 0;       
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));        
        $restaurantData = Restaurant::select('id','restaurant_name')->whereIn('id',$locations)->orderBy('id','DESC')->get();
         if($restaurantData && count($restaurantData) == 1) { 
           $selected_rest_id = $restaurantData[0]['id'];
        }


        $menuSubcategories  = MenuSubCategory::orderBy('id','DESC')->whereIn('status',[0,1])->whereIn('restaurant_id',$locations);
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $categoryData = MenuCategory::where('restaurant_id', '=',trim($request->input('restaurant_id') ))->get();
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuSubcategories->where('restaurant_id',$searchArr['restaurant_id']);
        }
        if($request->input('menu_category_id') && is_numeric($request->input('menu_category_id')))
        {
            $searchArr['menu_category_id'] = $request->input('menu_category_id');
            $menuSubcategories->where('menu_category_id',$searchArr['menu_category_id']);
        }
        if($request->input('name') && strlen($request->input('name'))<=100)
        {
            $searchArr['name'] = $request->input('name');
            $menuSubcategories->where('name','like', $searchArr['name'].'%');
        }
        $menuSubcategories = $menuSubcategories->paginate(20);
        return view('menu_subcategory.index', compact('menuSubcategories', 'selected_rest_id'))->with('restaurantData',$restaurantData)->with('searchArr',$searchArr)->with('categoryData',$categoryData);
    }

    public function destroyOld($id)
    {
        $menuSubcategoryObj = MenuSubCategory::find($id);
        if(!empty($menuSubcategoryObj))
        {
            $menuSubcategoryObj->delete();
            return Redirect::back()->with('message','Subcategory deleted successfully');
        }
        else
            return Redirect::back()->with('err_msg','Invalid Category');
    }

    /**
     * File upload function
     * @param $image
     * @param $id
     * @return array
     */
    private function file_upload($image, $id, $restaurant_id)
    {
        $imageRelPath = config('constants.image.rel_path.subcategory');
        $imagePath    = config('constants.image.path.subcategory');
       
        $restaurantData = Restaurant::select('id','restaurant_name')->where('id', $restaurant_id)->first();
       
        $curr_restaurant_name=strtolower(str_replace(' ','-', $restaurantData->restaurant_name));

        $imagePath=$imagePath.DIRECTORY_SEPARATOR.$curr_restaurant_name;
        $imageRelPath=$imageRelPath.$curr_restaurant_name. DIRECTORY_SEPARATOR;
        
        (! File::exists($imagePath) ?File::makeDirectory($imagePath):'');
        (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'thumb') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'thumb' ):'');

        $uniqId       = uniqid(mt_rand());
        $imageName    = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '.' . $image->getClientOriginalExtension();
        $medImageName = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();

        if($image->getClientOriginalExtension() != 'svg'){
            $image = Image::make($image->getRealPath());
            $width = $image->width();
            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
        } else {
            // in case of SVGs upload files directly, as cur. ver. of Interverntion doesn't support SVG
            File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . $imageName);
            File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
        }

        return [
            $imageRelPath . $imageName,
            $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
        ];
    }
    public function destroy(Request $request,$id)
      {
        DB::beginTransaction();
        try {
	      
	    $menuItemObj =  MenuSubCategory::find($id);
	    $menuItemObj->status = 2;	
	    $menuItemObj->save();		
	     
           
            DB::commit();
 	    return Redirect::back()->with('message','Menu Item deleted successfully');
             

        }
        catch (\Exception $e){
            DB::rollback();
               return Redirect::back()->with('message','Please try again!!');
        }
    }		
}
