<?php

namespace App\Http\Controllers;

use App\Models\MenuCategory;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Helpers\CommonFunctions;
use App\Models\Language;
use App\Models\MenuCategoryLanguage;
use Modules\MenuManagement\Entities\MenuItemModel;
use DB;


class MenuCategoryController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        /* exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan view:clear'); */
    }

    public function index(Request $request) {
        $restaurant_id = 0;
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
        if ($restaurantData && count($restaurantData) == 1) {
            $restaurant_id = $restaurantData[0]['id'];
        }

        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $restaurant_id = $request->input('restaurant_id');
            $menuCategories = MenuCategory::where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->paginate(20);
        } else {
            $menuCategories = MenuCategory::whereIn('restaurant_id', $locations)->orderBy('id', 'DESC')->paginate(20);
        }
        $product_types = config('constants.product_type');

        return view('menu_category.index', compact('menuCategories', 'product_types'))->with('restaurantData', $restaurantData)->with('restaurant_id', $restaurant_id);
    }

    public function create() {
        //$groupRestData = Restaurant::group();
       // $groupRestData = CommonFunctions::getRestaurantGroup();
        $groupRestData = CommonFunctions::getRestaurantGroupAll(); //Requirement by Pranav to show all restaurants
        $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();

        $selected_rest_id = 0;
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        }
        $product_types = config('constants.product_type');

        return view('menu_category.create', compact('groupRestData', 'selected_rest_id', 'product_types', 'languageData'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|max:100',
                    'slug' => 'required|max:100',
                    'product_type' => 'required',
                    'restaurant_id' => 'required|exists:restaurants,id',
                    'language_id' => 'required|exists:languages,id',
                    //'location_id'   => 'required|exists:restaurants,id',
                    'status' => 'required|in:0,1'
                        //'priority' => 'required|Integer|min:0'
                        //|exists:menu_categories,priority,restaurant_id,NULL'
                        ], [
                    'name.*' => 'Please enter valid name upto 100 chars',
                    'slug.*' => 'Please enter valid slug upto 100 chars',
                    'product_type' => 'Please select product type',
                    'restaurant_id.*' => 'Please select Restaurant',
                    'language_id.*' => 'Please select valid Language',
                    'status.*' => 'Please select Status'
                        //'priority.required' => 'Please enter Priority',
                        //'priority.*' => 'Please enter valid Priority'
                        //,'priority.exists' => 'Priority already exists for Restaurant',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $menuCategoryArr = [
                'restaurant_id' => $request->input('restaurant_id'),
                'language_id' => $request->input('language_id'),
                'product_type' => $request->input('product_type'),
                'pos_id' => $request->input('pos_id'),
                'description' => $request->input('description'),
                'sub_description' => $request->input('sub_description'),
                'priority' => $request->input('priority'),
                'promotional_banner_url' => $request->input('promotional_banner_url')??NULL,
                //'location_id'   => $request->input('location_id'),
                'name' => strtolower($request->input('name')),
                'slug' => strtolower(str_replace(' ', '_', $request->input('slug'))),
                'status' => $request->input('status'),
                'image_class' => $request->input('image_class'),
            ];
            if ($request->has('is_delivery') && $request->input('is_delivery') == 'on') {
                $menuCategoryArr['is_delivery'] = 1;
            } else {
                $menuCategoryArr['is_delivery'] = 0;
            }
            if($request->has('is_popular') && $request->input('is_popular') == 'on' ) {
                $menuCategoryArr['is_popular']  = 1;
            }else {
                $menuCategoryArr['is_popular']  = 0;
            }
            if($request->has('is_favourite') && $request->input('is_favourite') == 'on' ) {
                $menuCategoryArr['is_favourite']  = 1;
            }else {
                $menuCategoryArr['is_favourite']  = 0;
            }
            if ($request->has('is_carryout') && $request->input('is_carryout') == 'on') {
                $menuCategoryArr['is_carryout'] = 1;
            } else {
                $menuCategoryArr['is_carryout'] = 0;
            }
            //dd($menuCategoryArr);
            //'priority'=>$request->input('priority')];
            $menuCategoryObj = MenuCategory::create($menuCategoryArr);
            $menuCategoryLangArr = [
                'language_id' => $request->input('language_id'),
                'description' => $request->input('description'),
                'sub_description' => $request->input('sub_description'),
                'name' => strtolower($request->input('name')),
                'menu_categories_id' => $menuCategoryObj->id,
            ];
            MenuCategoryLanguage::create($menuCategoryLangArr);
            
            // IMAGES
            $imageArr = [];
            if ($request->hasFile('image_svg')) {
                $image = $request->file('image_svg');
                $imageRes = $this->file_upload($image, $menuCategoryObj->id);
                $imageArr['image_svg'] = $imageRes[0];
                $imageArr['thumb_image_med_svg'] = $imageRes[1];
            }
            if ($request->hasFile('image_png')) {
                $image = $request->file('image_png');
                $imageRes = $this->file_upload($image, $menuCategoryObj->id);
                $imageArr['image_png'] = $imageRes[0];
                $imageArr['thumb_image_med_png'] = $imageRes[1];
            }
            if ($request->hasFile('image_png_selected')) {
                $image = $request->file('image_png_selected');
                $imageRes = $this->file_upload($image, $menuCategoryObj->id);
                $imageArr['image_png_selected'] = $imageRes[0];
                $imageArr['thumb_image_med_png_selected'] = $imageRes[1];
            }

            if ($request->hasFile('cat_attachment')) {
                $image = $request->file('cat_attachment');
                if($request->file('cat_attachment')->getClientOriginalExtension()=="pdf" || $request->file('cat_attachment')->getClientOriginalExtension()=="docx" || $request->file('cat_attachment')->getClientOriginalExtension()=="doc")
                {
                    $imageRelPath = config('constants.image.rel_path.category_attachment');
                   // $realPath = $request->file('cat_attachment')->getRealPath();

                    $imagePath = config('constants.image.path.category_attachment');
                    $filname = $request->file('cat_attachment')->getClientOriginalName();
                    $uniqId = uniqid(mt_rand());
                    $filnameNew = $menuCategoryObj->id . '_' . pathinfo($filname, PATHINFO_FILENAME)
                        . '_' . $uniqId . '.' . $request->file('cat_attachment')->getClientOriginalExtension();
                    $uploaded_file_path = $imageRelPath.$filnameNew;
                    $request->file('cat_attachment')->move($imagePath, $filnameNew);
                    $imageArr['cat_attachment'] = $uploaded_file_path;
                } else {
                        $imageRes = $this->category_file_upload($image, $menuCategoryObj->id);
                        $imageArr['cat_attachment'] = $imageRes[0];
                    }

            }

            if ($request->hasFile('promotional_banner_image')) {
                    $image = $request->file('promotional_banner_image');
                    $imageRes = $this->category_file_upload($image, $menuCategoryObj->id);
                    $imageArr['promotional_banner_image'] = $imageRes[0];

            }
            if ($request->hasFile('promotional_banner_image_mobile')) {
                $image = $request->file('promotional_banner_image_mobile');
                $imageRes = $this->category_file_upload($image, $menuCategoryObj->id);
                $imageArr['promotional_banner_image_mobile'] = $imageRes[0];

            }

            if (count($imageArr) > 0) {
                MenuCategory::where('id', $menuCategoryObj->id)->update($imageArr);
            }

            return Redirect::back()->with('message', 'Category added successfully');
        }
    }

    /**
     * File upload function
     * @param $image
     * @param $id
     * @return array
     */
    private function file_upload($image, $id) {
        $imageRelPath = config('constants.image.rel_path.category');
        $imagePath = config('constants.image.path.category');

        $uniqId = uniqid(mt_rand());
        $imageName = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();
        $medImageName = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();

        if($image->getClientOriginalExtension() != 'svg'){
            $image = Image::make($image->getRealPath());
            $width = $image->width();
            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
            $image->resize((int) ($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
        }
        else {
            // in case of SVGs upload files directly, as cur. ver. of Interverntion doesn't support SVG
            File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . $imageName);
            File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
        }

        return [
            $imageRelPath . $imageName,
            $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
        ];
    }

    public function edit($id, $languageId = 1) {
        $menuCategory = MenuCategory::find($id);
        $menuCategoryLangObj = MenuCategoryLanguage::where([
                    'menu_categories_id' => $menuCategory->id,
                    'language_id' => $languageId,
                ])->first();
        $restaurant_id     = \Auth::user()->restaurant_id;

        $user_groups = DB::table('user_groups')
            ->whereIn('restaurant_id', [0,$restaurant_id])
            ->where(['status'=>1])
            ->get();
        if ($menuCategoryLangObj !== null) {
            $menuCategory->name = $menuCategoryLangObj->name;
            $menuCategory->description = $menuCategoryLangObj->description;
            $menuCategory->sub_description = $menuCategoryLangObj->sub_description;
        }
        //$groupRestData = CommonFunctions::getRestaurantGroup();
        $groupRestData = CommonFunctions::getRestaurantGroupAll(); //Requirement by Pranav to show all restaurants
        $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();

        $restLocations = Restaurant::select('id', 'restaurant_name')
                        ->where('parent_restaurant_id', $menuCategory->restaurant_id)->orderBy('id', 'DESC')->get();
        $product_types = config('constants.product_type');

        return view('menu_category.edit', compact('user_groups', 'menuCategory', 'restLocations', 'groupRestData', 'product_types', 'languageData', 'languageId'));
    }

    public function update(Request $request, $id) {
        $rules = [
            'name' => 'required|max:100',
            'slug' => 'required|max:100',
            'product_type' => 'required',
            'restaurant_id' => 'required|exists:restaurants,id',
            'language_id' => 'required|exists:languages,id',
            //'location_id' => 'required|exists:restaurants,id',
            'status' => 'required|in:0,1'
                //'priority' => 'required|Integer|min:0'
                //|exists:menu_categories,priority,restaurant_id,NULL'
        ];
        $rulesMsg = [
            'name.*' => 'Please enter valid name upto 100 chars',
            'slug.*' => 'Please enter valid slug upto 100 chars',
            'product_type' => 'Please select product type',
            'restaurant_id.*' => 'Please select Restaurant',
            'language_id.*' => 'Please select valid Language',
            //'location_id.*' => 'Please select Location/Branch',
            'status.*' => 'Please select Status'
                //'priority.required' => 'Please enter Priority',
                //'priority.*' => 'Please enter valid numeric Priority'
                //,'priority.exists' => 'Priority already exists for Restaurant',
        ];
        $validator = Validator::make(Input::all(), $rules, $rulesMsg);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $menuCategoryObj = MenuCategory::where('id', $id)->first();
            $menuCategoryObj->name = $request->input('name');
            $menuCategoryObj->product_type = $request->input('product_type');
            $menuCategoryObj->restaurant_id = $request->input('restaurant_id');
            $menuCategoryObj->language_id = $request->input('language_id');
            $menuCategoryObj->pos_id = $request->input('pos_id') ?? '';
            //$menuCategoryObj->location_id   = $request->input('location_id');
            $menuCategoryObj->slug = strtolower(str_replace(' ', '_', $request->input('slug')));
            $menuCategoryObj->status = $request->input('status');
            $menuCategoryObj->image_class = $request->input('image_class');
            $menuCategoryObj->description = $request->input('description');
            $menuCategoryObj->sub_description = $request->input('sub_description');
            $menuCategoryObj->priority = $request->input('priority');
            $menuCategoryObj->promotional_banner_url = $request->input('promotional_banner_url')??NULL;

            if($request->has('is_popular') && $request->input('is_popular') == 'on' ) {
                $menuCategoryObj->is_popular  = 1;
            }else {
                $menuCategoryObj->is_popular  = 0;
            }
            if($request->has('is_favourite') && $request->input('is_favourite') == 'on' ) {
                $menuCategoryObj->is_favourite  = 1;
            }else {
                $menuCategoryObj->is_favourite  = 0;
            }
            if ($request->input('image_svg_delete') && $request->input('image_svg_delete') == '1') {
                unlink(public_path($menuCategoryObj->image_svg));
                unlink(public_path($menuCategoryObj->thumb_image_med_svg));
                $menuCategoryObj->image_svg = NULL;
                $menuCategoryObj->thumb_image_med_svg = NULL;
            }
            if ($request->input('image_png_delete') && $request->input('image_png_delete') == '1') {
                unlink(public_path($menuCategoryObj->image_png));
                unlink(public_path($menuCategoryObj->thumb_image_med_png));
                $menuCategoryObj->image_png = NULL;
                $menuCategoryObj->thumb_image_med_png = NULL;
            }
            if ($request->input('image_png_selected_delete') && $request->input('image_png_selected_delete') == '1') {
                unlink(public_path($menuCategoryObj->image_png_selected));
                unlink(public_path($menuCategoryObj->thumb_image_med_png_selected));
                $menuCategoryObj->image_png_selected = NULL;
                $menuCategoryObj->thumb_image_med_png_selected = NULL;
            }
            if($request->input('cat_attachment_deleted') && $request->input('cat_attachment_deleted')=='1') {
                unlink(public_path($menuCategoryObj->cat_attachment));
                $menuCategoryObj->cat_attachment = NULL;
            }

            if($request->input('promotional_banner_image_deleted') && $request->input('promotional_banner_image_deleted')=='1') {
                unlink(public_path($menuCategoryObj->promotional_banner_image));
                $menuCategoryObj->promotional_banner_image = NULL;
            }
            if($request->input('promotional_banner_image_mobile_deleted') && $request->input('promotional_banner_image_mobile_deleted')=='1') {
                unlink(public_path($menuCategoryObj->promotional_banner_image_mobile));
                $menuCategoryObj->promotional_banner_image_mobile = NULL;
            }
            //$menuCategoryObj->priority = Input::get('priority');
            if ($request->has('is_delivery') && $request->input('is_delivery') == 'on') {
                $menuCategoryObj->is_delivery = 1;
            } else {
                $menuCategoryObj->is_delivery = 0;
            }
            if ($request->has('is_carryout') && $request->input('is_carryout') == 'on') {
                $menuCategoryObj->is_carryout = 1;
            } else {
                $menuCategoryObj->is_carryout = 0;
            }

            //////

            $promo_banners=json_decode($menuCategoryObj->promo_banners,1);
            $old_positions=$promo_banners;
            $banners=NULL;
            if(!$request->has('Type')){
                $banners=NULL;
                $promo_banners=NULL;
            }else{

                $imageRelPath = config('constants.image.rel_path.category');
                $imagePath = config('constants.image.path.category');
                if(empty($menuCategoryObj->promo_banners)){
                    $promo_banners=[];
                    if($request->has('Type')){

                        $field_type=$request->input('Type');

                        foreach($field_type as $key=> $type) {

                            if(!empty($type)) {

                                $Display = $request->input('Display')[$key] ?? '';
                                $Position = $request->input('Position')[$key] ?? '';
                                $AltText = $request->input('AltText')[$key] ?? '';

                                $Link = $request->input('Link')[$key] ?? '';
                                $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                $Status = $request->input('Status')[$key] ?? 0;
                                $imageNameBase =  'promo_' . str_replace(' ', '_', $Position);

                                if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                    $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                    $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                    $Value_Mobile = $imageArr['image'];
                                } else {
                                    $Value_Mobile = $request->input('Value_Mobile')[$key] ?? '';
                                }
                                if ($type!='Text' && @$request->file('Value')[$key]) {
                                    $imageName = $imageNameBase .time(). '_Banner_';
                                    $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                    $Value = $imageArr['image'];
                                } else {
                                    $Value = $request->input('Value')[$key] ?? '';
                                }
                                if ((!empty($Value) || !empty($Value_Mobile)) && !empty($type)) {

                                    $promo_banners[] = [
                                        'Id' => $key,
                                        'Display' => $Display,
                                        'Position'=>$Position,
                                        'Type' => $type,
                                        'Value' => $Value,
                                        'Value_Mobile' => $Value_Mobile,
                                        'AltText'=>$AltText,
                                        'Link' => $Link,
                                        'UserGroup' => $UserGroup,
                                        'SortOrder' => $SortOrder,
                                        'Status' => $Status
                                    ];

                                }

                            }

                        }
                    }
                }else{
                    $old_id=[];
                    $alloldids=$request->input('ID');
                    foreach ($alloldids as $oldid){
                        if($oldid!=''){
                            $old_id[]=$oldid;
                        }

                    }
                    //$old_id = array_filter($request->input('ID'));
                    $updated_positions=[];
                    $last_id=0;
                    $last_ids=[];
                    if(count($old_positions)){
                        foreach ($old_positions as $pos){

                            if (in_array($pos['Id'], $old_id)){
                                $last_id=(int)$pos['Id'];
                                $last_ids[]=$last_id;
                                //  unset($pos);
                                $updated_positions[]=$pos;

                            }
                        }

                        $last_id=count($last_ids)?max($last_ids):0;
                    }

                    $newPosItem =$updated_positions;
                    $field_type=$request->input('Type');
                    $new_additions=[];

                    foreach($field_type as $key=> $type){

                        if(!isset($request->input('ID')[$key]) || $request->input('ID')[$key]==''){
                            //print_r("iff---");
                            if(!empty($type)) {

                                $Display = $request->input('Display')[$key] ?? '';
                                $Position = $request->input('Position')[$key] ?? '';
                                $AltText = $request->input('AltText')[$key] ?? '';

                                $Link = $request->input('Link')[$key] ?? '';
                                $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                $Status = $request->input('Status')[$key] ?? 0;
                                $imageNameBase = 'promo_' . str_replace(' ', '_', $Position);

                                if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                    $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                    $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                    $Value_Mobile = $imageArr['image'];
                                } else {
                                    $Value_Mobile = $request->input('Value_Mobile')[$key] ?? '';
                                }
                                if ($type!='Text' && @$request->file('Value')[$key]) {
                                    $imageName = $imageNameBase .time(). '_Banner_';
                                    $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                    $Value = $imageArr['image'];
                                } else {
                                    $Value = $request->input('Value')[$key] ?? '';
                                }
                                if ((!empty($Value) || !empty($Value_Mobile)) && !empty($type)) {
                                    $last_id = $last_id + 1;
                                    $new_additions[] = [
                                        'Id' => $last_id,
                                        'Display' => $Display,
                                        'Position'=>$Position,
                                        'Type' => $type,
                                        'Value' => $Value,
                                        'Value_Mobile' => $Value_Mobile,
                                        'AltText'=>$AltText,
                                        'Link' => $Link,
                                        'UserGroup' => $UserGroup,
                                        'SortOrder' => $SortOrder,
                                        'Status' => $Status
                                    ];
                                }

                            }

                        }else{

                            foreach ($updated_positions as $upkey=>$posup){
                                //print('hi2-');
                                //dump($posup);
                                if(trim($posup['Id'])==trim($request->input('ID')[$key])) {
                                    $Display = $request->input('Display')[$key] ?? '';
                                    $Position = $request->input('Position')[$key] ?? '';
                                    $AltText = $request->input('AltText')[$key] ?? '';

                                    $Link = $request->input('Link')[$key] ?? '';
                                    $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                    $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                    $Status = $request->input('Status')[$key] ?? 0;
                                    $imageNameBase = 'promo_' . str_replace(' ', '_', $Position);
                                    // $last_id = $last_id + 1;
                                    $newmatched=[
                                        'Id' => $posup['Id'],
                                        'Display' => $Display,
                                        'Position'=>$Position,
                                        'Type' => $type,
                                        'AltText'=>$AltText,
                                        'Link' => $Link,
                                        'UserGroup' => $UserGroup,
                                        'SortOrder' => $SortOrder,
                                        'Status' => $Status
                                    ];

                                    if ($type!='Text' && @$request->file('Value')[$key]) {
                                        $imageName = $imageNameBase.time() . '_Banner_';
                                        $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                        $newmatched['Value']= $imageArr['image'];
                                    } else {
                                        if($type=='Text'){
                                            $newmatched['Value'] = $request->input('Value')[$key] ?? '';

                                        }else{
                                            $newmatched['Value'] = $posup['Value'];
                                        }
                                    }
                                    //$newPosItem[$upkey]=$newmatched;
                                    if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                        $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                        $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                        $newmatched['Value_Mobile']= $imageArr['image'];
                                    } else {
                                        if($type=='Text'){
                                            $newmatched['Value_Mobile'] = $request->input('Value_Mobile')[$key] ?? '';

                                        }else{
                                            $newmatched['Value_Mobile'] = $posup['Value_Mobile'];
                                        }
                                    }
                                    $newPosItem[$upkey]=$newmatched;
                                }

                            }

                        }

                    }

                    if(count($new_additions)){
                        $newPosItem=array_merge($newPosItem,$new_additions);
                    }

                    $promo_banners=$newPosItem;
                }
            }
            //print_r($promo_banners);die;

            $menuCategoryObj->promo_banners =(is_array($promo_banners) && count($promo_banners))? json_encode($promo_banners):NULL;


            ///

            $menuCategoryObj->save();

            $menuCategoryLangObj = MenuCategoryLanguage::where([
                        'menu_categories_id' => $menuCategoryObj->id,
                        'language_id' => $request->input('language_id'),
                    ])->first();

            if ($menuCategoryLangObj !== null) {
                $menuCategoryLangArr = [
                    'description' => $request->input('description'),
                    'sub_description' => $request->input('sub_description'),
                    'name' => strtolower($request->input('name')),
                ];
                $menuCategoryLangObj->update($menuCategoryLangArr);
            } else {
                $menuCategoryLangArr = [
                    'language_id' => $request->input('language_id'),
                    'description' => $request->input('description'),
                    'sub_description' => $request->input('sub_description'),
                    'name' => strtolower($request->input('name')),
                    'menu_categories_id' => $menuCategoryObj->id,
                ];
                MenuCategoryLanguage::create($menuCategoryLangArr);
            }

            // IMAGES
            $imageArr = [];
            if ($request->hasFile('image_svg')) {
                $image = $request->file('image_svg');
                $imageRes = $this->file_upload($image, $menuCategoryObj->id);
                $imageArr['image_svg'] = $imageRes[0];
                $imageArr['thumb_image_med_svg'] = $imageRes[1];
            }
            if ($request->hasFile('image_png')) {
                $image = $request->file('image_png');
                $imageRes = $this->file_upload($image, $menuCategoryObj->id);
                $imageArr['image_png'] = $imageRes[0];
                $imageArr['thumb_image_med_png'] = $imageRes[1];
            }
            if ($request->hasFile('image_png_selected')) {
                $image = $request->file('image_png_selected');
                $imageRes = $this->file_upload($image, $menuCategoryObj->id);
                $imageArr['image_png_selected'] = $imageRes[0];
                $imageArr['thumb_image_med_png_selected'] = $imageRes[1];
            }

            if ($request->hasFile('cat_attachment')) {
                $image = $request->file('cat_attachment');

                if($request->file('cat_attachment')->getClientOriginalExtension()=="pdf" || $request->file('cat_attachment')->getClientOriginalExtension()=="docx" || $request->file('cat_attachment')->getClientOriginalExtension()=="doc")
                {
                    $imageRelPath = config('constants.image.rel_path.category_attachment');
                    // $realPath = $request->file('cat_attachment')->getRealPath();

                    $imagePath = config('constants.image.path.category_attachment');
                    $filname = $request->file('cat_attachment')->getClientOriginalName();
                    $uniqId = uniqid(mt_rand());
                    $filnameNew = $menuCategoryObj->id . '_' . pathinfo($filname, PATHINFO_FILENAME)
                        . '_' . $uniqId . '.' . $request->file('cat_attachment')->getClientOriginalExtension();
                    $uploaded_file_path = $imageRelPath.$filnameNew;
                    $request->file('cat_attachment')->move($imagePath, $filnameNew);
                    $imageArr['cat_attachment'] = $uploaded_file_path;
                } else {
                    //echo 'asdasd';exit;
                    $imageRes = $this->category_file_upload($image, $menuCategoryObj->id);
                    $imageArr['cat_attachment'] = $imageRes[0];
                }

            }

            if ($request->hasFile('promotional_banner_image')) {
                $image = $request->file('promotional_banner_image');

                    //echo 'asdasd';exit;
                    $imageRes = $this->category_file_upload($image, $menuCategoryObj->id);
                    $imageArr['promotional_banner_image'] = $imageRes[0];


            }
            if ($request->hasFile('promotional_banner_image_mobile')) {
                $image = $request->file('promotional_banner_image_mobile');

                //echo 'asdasd';exit;
                $imageRes = $this->category_file_upload($image, $menuCategoryObj->id);
                $imageArr['promotional_banner_image_mobile'] = $imageRes[0];


            }

            if (count($imageArr) > 0) {
                MenuCategory::where('id', $menuCategoryObj->id)->update($imageArr);
            }

            return Redirect::back()->with('message', 'Category updated successfully');
        }
    }

    public function show($id) {
        
    }

    public function destroy($id) {
        $menuCategoryObj = MenuCategory::find($id);
        if (!empty($menuCategoryObj)) {
            $menuCategoryObj->delete();
		
 
	    MenuItemModel::where('menu_category_id', $id)->delete();
            MenuCategoryLanguage::where('menu_categories_id', $menuCategoryObj->id)->delete();
	    MenuCategoryLanguage::where('menu_categories_id', $menuCategoryObj->id)->delete();	
            return Redirect::back()->with('message', 'Category deleted successfully');
        } else
            return Redirect::back()->with('err_msg', 'Invalid Category');
    }

    public function getCategory(Request $request) {
        $rid = $request->input('rid');
        if (!empty($rid) && is_numeric($rid)) {
            $result['cat'] = MenuCategory::where('restaurant_id', '=', $rid)->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }

    public function get_restaurant_locations(Request $request) {
        $restId = $request->input('rest_id');
        $restLocations = Restaurant::select('id', 'restaurant_name')->where(['parent_restaurant_id' => $restId, 'status' => 1])->get();

        if ($restLocations) {
            return response()->json(array('dataObj' => $restLocations), 200);
        }

        return response()->json(array('err' => 'Invalid Data'), 200);
    }

    /**
     * File upload function
     * @param $image
     * @param $id
     * @return array
     */
    private function category_file_upload($image, $id)
    {
        $imagePath    = config('constants.image.path.category_attachment');
        $imageRelPath = config('constants.image.rel_path.category_attachment');

        $uniqId       = uniqid(mt_rand());
        $imageName    = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
            . '_' . $uniqId . '.' . $image->getClientOriginalExtension();
        $medImageName = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
            . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();


            $image = Image::make($image->getRealPath());
            $width = $image->width();
            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
            //echo $imageRelPath.$imageName;exit;
        return [
            $imageRelPath.$imageName
        ];
    }
}
