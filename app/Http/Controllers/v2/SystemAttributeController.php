<?php
namespace App\Http\Controllers\v2;
use App\Http\Controllers\Controller;
use App\Models\v2menu\ProductType;
use App\Models\v2menu\MaAttribute;
use App\Models\v2menu\MaAttributeItem;
use App\Models\v2menu\MenuItemAttributeType;
use App\Models\v2menu\MaAttributeSet;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Helpers\CommonFunctions;
use App\Models\Language;
use Illuminate\Support\Facades\Auth;
use DB;
 
class SystemAttributeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $restaurant_id = 0;
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));  
        $restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
        if ($restaurantData && count($restaurantData) == 1) {
            $restaurant_id = $restaurantData[0]['id'];
        }

        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $restaurant_id = $request->input('restaurant_id');
            $menuAttributes = MaAttribute::where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->get();
        } else {
            $menuAttributes = MaAttribute::whereIn('restaurant_id', $locations)
	     		->orderBy('id', 'DESC')->get();
        }
	$language = Language::where('status', 1)->orderBy('id', 'DESC')->get();
	$product_type=ProductType::where('status', 1)->orderBy('name', 'Asc')->get(); 
        return view('v2menu.ma_attribute', compact('menuAttributes'))->with('language', $language)->with('product_type', $product_type);
    }
    private function getAttributeCode($id,$attributelabel){
	$attributelabel = str_replace(" ","_",$attributelabel);	
	return $attributelabel."_".$id;
    }	
    public function addAttribute(Request $request) {
		$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
		$dataarray=$request->all();  
		if(isset($dataarray['deletemyimage']))unset($dataarray['deletemyimage']);
		if(isset($dataarray['deleteimage']))unset($dataarray['deleteimage']);
		
		$dataarray['restaurant_id'] = $locations[0];
		//check if attribute code already exist attribute_code for restaurant
		$s  = MaAttribute::where('restaurant_id', $locations[0])->where('attribute_type',$dataarray['attribute_type'])
			->where('attribute_code', $dataarray['attribute_code'])->get()->toArray();
		if(!isset($s[0])){ //if same attribute code dosenot exist
			$s  = MaAttribute::where('restaurant_id', $locations[0])->where('attribute_type', $dataarray['attribute_type'])->min('sort_order'); 
			$s  = ($s==0)?99999:($s-1);
			$dataarray['sort_order'] = ($dataarray['sort_order']==99999)?$s:$dataarray['sort_order'];
			$d = MaAttribute::create($dataarray);
			if ($request->hasFile('attribute_image')) {
				 $image = $request->file('attribute_image'); 
				$imageRes = $this->file_upload($image, $d->id);
				$imageArr['image_svg'] = $imageRes[0];
				//$imageArr['thumb_image_med_svg'] = $imageRes[1];
				MaAttribute::where('id',$d->id)->update(['attribute_image' => $imageArr['image_svg']]);
			}
			//$dataarray['attribute_code'] = $this->getAttributeCode($d->id,$dataarray['attribute_code']);
			//MaAttribute::where('id',$d->id)->update(['attribute_code' => $dataarray['attribute_code']]); 
			$dataarray['id']=$d->id;
			 
		        return \Response::json(['data'=>$dataarray,'error'=>0,'message'=>'Menu Attribute added successfully'], 200);
		}else{
			return \Response::json(['data'=>$dataarray,'error'=>1,'message'=>'Attribute code ['.$dataarray['attribute_code'].'] already exist!!'], 200);
		}
    }
    public function updateAttribute(Request $request,$id){
		// print_r($request->all());die;
		$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
		$dataarray=$request->all(); 
		//$dataarray['product_type_id']=implode(",",$dataarray['product_type_id']); 
		//$dataarray['product_type_id'] = 0;
			//check if attribute code already exist attribute_code for restaurant
		$s  = MaAttribute::where('restaurant_id', $locations[0])->where('attribute_type',$dataarray['attribute_type'])
			->where('attribute_code', $dataarray['attribute_code'])->whereNotIn('id', [$id])->get()->toArray();
		if(!isset($s[0])){ //if same attribute code dosenot exist
			if(isset($dataarray['deletemyimage']) && $dataarray['deletemyimage']==0){
				if ($request->hasFile('attribute_image')) {
					$image = $request->file('attribute_image');
					$imageRes = $this->file_upload($image, $id);
					$dataarray['attribute_image'] = $imageRes[0];
					//print_r($dataarray);die;
				}
			}if(isset($dataarray['deletemyimage']) && $dataarray['deletemyimage']==1){
				$dataarray['attribute_image'] = '';
			}
			if(isset($dataarray['deletemyimage']))unset($dataarray['deletemyimage']);
			if(isset($dataarray['deleteimage']))unset($dataarray['deleteimage']);
			MaAttribute::where('id',$id)->update($dataarray);
			
			$d = MaAttribute::where('id', $id)->get()->toArray();	
			//$product_type=ProductType::where('id', $dataarray['product_type'])->orderBy('name', 'Asc')->get()->toArray();
			//$dataarray['name']=$product_type[0]['name']??'';
			//$dataarray['id']=$id;
			return \Response::json(['data'=>$d[0],'error'=>0,'message'=>'Menu Attribute added successfully'], 200);
		}else{
			return \Response::json(['data'=>null,'error'=>1,'message'=>'Duplicate Attribute code.'], 200);
		}
    }		
    
    public function getAttribute(Request $request,$id){
	$menuAttributes = MaAttribute::where('id', $id)->get()->toArray();
	return \Response::json(['data'=>$menuAttributes[0],'error'=>0,'message'=>'Menu Attribute added successfully'], 200);
    }

    public function updateAttributeSortOrder(Request $request,$id){
	//print_r($request->input('data'));
	$data = $request->input('data');
	if(isset($data)){
		foreach($data as $arr){
			$old = MaAttribute::find($arr[0]);
			$new = MaAttribute::find($arr[1]); 
			$oldA = ['sort_order'=>$new->sort_order]; 
			$newA = ['sort_order'=>$old->sort_order]; 
			MaAttribute::where('id',$arr[0])->update($oldA);	
			MaAttribute::where('id',$arr[1])->update($newA); 
		} 
	}
    }	

    public function deleteAttribute($id) {
        $attrobj= MaAttribute::find($id);
	$items = MaAttributeItem::where('attribute_id',$id)->get()->toArray();
	if(isset($items)){
		foreach($items as $item){
			$attrobj= MaAttributeItem::find($item['id']);
			$attrobj->delete();
		}
	}		
        $attrobj->delete();
    }


    
    

    public function listAttributeValue(Request $request,$attribute_id) {
        $restaurant_id = 0;
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
        if ($restaurantData && count($restaurantData) == 1) {
            $restaurant_id = $restaurantData[0]['id'];
        }

        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
           // $restaurant_id = $request->input('restaurant_id');
           // $menuAttributes = AttributeValue::where('attribute_id', $attribute_id)->orderBy('id', 'DESC')->paginate(20);
        } else {
            $menuAttributes = MaAttributeItem::select('v.attribute_item_name as parent_attribute_item_name','ma_attribute_item.attribute_item_parent_id','ma_attribute.attribute_label','ma_attribute_item.id','ma_attribute_item.attribute_item_name'
		,'ma_attribute_item.sort_order','ma_attribute_item.status')->where('ma_attribute_item.attribute_id', $attribute_id)->leftJoin('ma_attribute_item as v', 'v.id', '=', 'ma_attribute_item.attribute_item_parent_id')
	    ->leftJoin('ma_attribute', 'ma_attribute_item.attribute_id', '=', 'ma_attribute.id')->orderBy('ma_attribute_item.sort_order', 'Asc')->paginate(200000);
        }
	$menuAttributesList =  MaAttributeItem::where('attribute_id', $attribute_id)->where('attribute_item_parent_id',0)->orderBy('sort_order', 'DESC')->get();	
	$menuAttributesLable = MaAttribute::where('id', $attribute_id)->orderBy('sort_order', 'DESC')->get()->toArray();
	$attribute_label = $menuAttributesLable[0]['attribute_label'];
	//$attribute_code = $menuAttributesLable[0]['attribute_code'];
        return view('v2menu.ma_attribute_item', compact('menuAttributes','menuAttributesList'))->with('restaurant_id', $restaurant_id)->with('attribute_id', $attribute_id)->with('attribute_label', $attribute_label);
    }
    public function listAttributeItem(Request $request,$attribute_id) {
        $restaurant_id = 0;
        $menuAttributes = $this->getAttributeItems($attribute_id,0);
        return \Response::json(['data'=>$menuAttributes], 200);
    }
    public function getAttributeItems($attribute_id,$parent_id=0,$id=0) {
        $itemData = MaAttributeItem::select('v.attribute_item_name as parent_attribute_item_name','ma_attribute_item.attribute_item_parent_id','ma_attribute.attribute_label'
			,'ma_attribute_item.id','ma_attribute_item.attribute_item_name' ,'ma_attribute_item.sort_order','ma_attribute_item.status')
			 ->leftJoin('ma_attribute_item as v', 'v.id', '=', 'ma_attribute_item.attribute_item_parent_id')
	    		->leftJoin('ma_attribute', 'ma_attribute_item.attribute_id', '=', 'ma_attribute.id')->orderBy('ma_attribute_item.sort_order', 'Asc');
			if($id<1){
				if($attribute_id!=null)
				$itemData->where('ma_attribute_item.attribute_id', $attribute_id);
				$itemData->where('ma_attribute_item.attribute_item_parent_id', '=', $parent_id);
			}else{
				$itemData->where('ma_attribute_item.id', '=', $id);
			}
	$itemData =  $itemData->get()->toArray();
	$m = null; 
        if (count($itemData)) {
	    $i=0;	
            foreach ($itemData as $index => $grp) { 
			$m[$i] = $grp;	
			$m[$i]['children'] =  $this->getAttributeItems(null,$grp['id']);
			$i++;
		}  
		 
        } 
        return isset($m)?$m:null;
    } 
    public function updateAttributeValueSortOrder(Request $request,$id){
	//print_r($request->input('data'));
	$data = $request->input('data');
	if(isset($data)){
		foreach($data as $arr){
			$old = MaAttributeItem::find($arr[0]);
			$new = MaAttributeItem::find($arr[1]); 
			$oldA = ['sort_order'=>$new->sort_order]; 
			$newA = ['sort_order'=>$old->sort_order]; 
			MaAttributeItem::where('id',$arr[0])->update($oldA);	
			MaAttributeItem::where('id',$arr[1])->update($newA); 
		}
	} 
    }

    public function updateAttributeValue(Request $request,$id){
	        $dataarray=$request->all();  
		$pid = $request->input('attribute_item_parent_id');
		if($pid!=$id){
			$dataarray['attribute_item_parent_id']=$pid; 
		}else {
			$a = MaAttributeItem::find($id);
			$dataarray['attribute_item_parent_id'] = $a->attribute_item_parent_id;
		} 
		$checkIsDefaultIsAny = MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->get()->toArray();
		$checkIsDefault = MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->where('is_default', 'Yes')->get()->toArray();
		if(isset($checkIsDefault[0]) && $dataarray['is_default']=='Yes'){
				//update rest attribute as no
				MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->update(['is_default' =>'No']);
		}elseif(!isset($checkIsDefaultIsAny[0])){
				$dataarray['is_default']='Yes';
		} 
		MaAttributeItem::where('id',$id)->update($dataarray);
		if ($request->hasFile('attribute_item_image')) {
		        $image = $request->file('attribute_item_image');
		        $imageRes = $this->file_upload($image, $id);
		        $imageArr['image_svg'] = $imageRes[0];
		        //$imageArr['thumb_image_med_svg'] = $imageRes[1];
			MaAttributeItem::where('id',$id)->update(['attribute_item_image' => $imageArr['image_svg']]);
		}
		 
		$menuAttributes = MaAttribute::where('id', $request->input('attribute_id'))->orderBy('id', 'DESC')->get()->toArray();
		$parentAttributes = MaAttributeItem::select('attribute_item_name')->where('id', $dataarray['attribute_item_parent_id'])->orderBy('id', 'DESC')->get()->toArray();
		$dataarray['id']=$id;
		$dataarray['attribute_label']=$menuAttributes[0]['attribute_label'];
		$dataarray['parent_attribute_value']=$parentAttributes[0]['attribute_item_name']??"";
                return \Response::json(['data'=>$dataarray,'error'=>0,'message'=>'Menu Attribute added successfully'], 200);
    }



    public function getAttributeItemInfo(Request $request,$id){
	$menuAttributes =  MaAttributeItem::where('ma_attribute_item.id', $id)->orderBy('ma_attribute_item.sort_order', 'Asc')->paginate(200000);
	$menuAttributesParent =  MaAttributeItem::orderBy('ma_attribute_item.sort_order', 'Asc');
	if(isset($menuAttributes[0]['attribute_item_parent_id']) && $menuAttributes[0]['attribute_item_parent_id']>0)
	$menuAttributesParent = $menuAttributesParent->where('ma_attribute_item.id', $menuAttributes[0]['attribute_item_parent_id']);
	else $menuAttributesParent = $menuAttributesParent->where('ma_attribute_item.attribute_item_parent_id',0);
	$menuAttributesParent = $menuAttributesParent->where('ma_attribute_item.attribute_id', $menuAttributes[0]['attribute_id']);
	$menuAttributesParent = $menuAttributesParent->get();
	return \Response::json(['data'=>$menuAttributes[0],'pdata'=>$menuAttributesParent,'error'=>0], 200);
    }
    public function getAttributeItemByAttributeId(Request $request,$attribute_id,$parent_id){
	$menuAttributes =  MaAttributeItem::where('ma_attribute_item.attribute_id', $attribute_id)
			  ->where('ma_attribute_item.attribute_item_parent_id', $parent_id)->orderBy('ma_attribute_item.sort_order', 'Asc')->get()->toArray();
	return \Response::json(['data'=>$menuAttributes,'error'=>0], 200);
    }	

    public function getAttributeListByProductType(Request $request,$id,$attribute_set_id){
	$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
	$menuAttributes =  MaAttribute::whereIn('restaurant_id', $locations)->where('status', 1);
	$menuAttributeSet =  MaAttributeSet::select('general')->where('id', $attribute_set_id)->where('status', 1)->get()->toArray(); 
	if($menuAttributeSet[0]['general']){
		$ids = explode(',',$menuAttributeSet[0]['general']);
		$menuAttributes = $menuAttributes->whereIn('id', $ids);
	}
	if($id!=4 && $id!=5 && $id!=3){
		$menuAttributes = $menuAttributes->where('is_customizable', 'Yes');
	}else{
		$menuAttributes = $menuAttributes->where('is_customizable', 'No');
	}
	$menuAttributes= $menuAttributes->orderBy('sort_order', 'Asc')->get()->toArray();
		
	
	return \Response::json(['list'=>$menuAttributes], 200);
    }

	
    public function addAttributeValue(Request $request) {
		
		$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
		$dataarray=$request->all(); 
		// check attribute item already exist
		$exist = MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->where('attribute_item_name', $dataarray['attribute_item_name'])->get()->toArray();
		if(!isset($exist[0])){
			//$dataarray['sort_order'] = ( (MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->min('sort_order')) - 1); 
			$dataarray['sort_order'] = MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->min('sort_order');
			$dataarray['sort_order'] = ($dataarray['sort_order']==0)?99999:($dataarray['sort_order']-1);
			$checkIsDefaultIsAny = MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->get()->toArray();
			$checkIsDefault = MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->where('is_default', 'Yes')->get()->toArray();
			if(isset($checkIsDefault[0]) && $dataarray['is_default']=='Yes'){
				//update rest attribute as no
				MaAttributeItem::where('attribute_item_parent_id', $dataarray['attribute_item_parent_id'])->update(['is_default' =>'No']);
			}elseif(!isset($checkIsDefaultIsAny[0])){
				$dataarray['is_default']='Yes';
			} 
			$d = MaAttributeItem::create($dataarray);
			//print_r($_FILES);die;
			if ($request->hasFile('file')) {
				$image = $request->file('file');
				$imageRes = $this->file_upload($image, $d->id);
				$imageArr['image_svg'] = $imageRes[0];
				//$imageArr['thumb_image_med_svg'] = $imageRes[1];
				MaAttributeItem::where('id',$d->id)->update(['attribute_item_image' => $imageArr['image_svg']]);
			}
			 
			$menuAttributes = MaAttribute::where('id', $request->input('attribute_id'))->orderBy('id', 'DESC')->get()->toArray();
			$parentAttributes = MaAttributeItem::select('attribute_item_name')->where('id', $dataarray['attribute_item_parent_id'])->orderBy('id', 'DESC')->get()->toArray();
			$dataarray['id']=$d->id;
			$dataarray['parent_attribute_value']=$parentAttributes[0]['attribute_item_name']??"";
			$dataarray['attribute_label']=$menuAttributes[0]['attribute_label'];
		        //echo json_encode ['message','Menu Attribute added successfully'];
			return \Response::json(['data'=>$dataarray,'error'=>0,'message'=>'Menu Attribute added successfully'], 200);
		}else{
			return \Response::json(['data'=>null,'error'=>1,'message'=>'Duplicate Item '.$dataarray['attribute_item_name']], 200);
		}
    }
    /**
     * File upload function
     * @param $image
     * @param $id
     * @return array
     */
    private function file_upload($image, $id) {
        $imageRelPath = config('constants.image.rel_path.attribute');
        $imagePath = config('constants.image.path.attribute');

        $uniqId = uniqid(mt_rand());
        $imageName = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();
        $medImageName = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();

        if($image->getClientOriginalExtension() != 'svg'){
            $image = Image::make($image->getRealPath());
            $width = $image->width();
            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
            $image->resize((int) ($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
        }
        else {
            // in case of SVGs upload files directly, as cur. ver. of Interverntion doesn't support SVG
            File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . $imageName);
            File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
        }

        return [
            $imageRelPath . $imageName,
            $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
        ];
    }
	
    public function deleteAttributeValue($id) {
        $attrobj= MaAttributeItem::find($id);
        $attrobj->delete();
    }
    public function listAttributeSet(Request $request) {
        $restaurant_id = 0;
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
        if ($restaurantData && count($restaurantData) == 1) {
            $restaurant_id = $restaurantData[0]['id'];
        }

        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
           // $restaurant_id = $request->input('restaurant_id');
           // $menuAttributes = AttributeValue::where('attribute_id', $attribute_id)->orderBy('id', 'DESC')->paginate(20);
        } else {
            $menuAttributeSet = MaAttributeSet::select('*')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
        }
	 
        return view('v2menu.ma_attribute_set', compact('menuAttributeSet'))->with('restaurant_id', $restaurant_id);
    }
    public function createAttributeSet(Request $request) {
        $restaurant_id = 0;
	$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
	$user = Auth::user();  
	if($user->role=="manager"){
		 	$groupRestData =  Restaurant::select('id', 'restaurant_name')->whereIn('id',$locations)->orderBy('id', 'DESC')->get()->toArray(); 
	}else {
		 
			$groupRestData =  Restaurant::select('id', 'restaurant_name')->whereIn('id',$locations)->orderBy('id', 'DESC')->get()->toArray(); 
		 
	} 
	//'General','Modifier','System','SystemModifier','SystemModifierItem','SystemMeal','SystemMealItem','SystemRelated','SystemRelatedItem'
        
	$restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
        if ($restaurantData && count($restaurantData) == 1) {
            $restaurant_id = $restaurantData[0]['id'];
        }
	//'General','Modifier','System','SystemModifier','SystemModifierItem','SystemMeal','SystemMealItem','SystemAddon','SystemAddonItem'
	$menuAttributeAddon = MaAttribute::select('*')->where('attribute_type', 'SystemAddon')->whereIn('restaurant_id', $locations)->where('status', 1)->get(); 
	$menuAttributeAddonItem = MaAttribute::select('*')->where('attribute_type', 'SystemAddonItem')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
        $menuAttributeGeneral = MaAttribute::select('*')->where('attribute_type', 'General')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
        $menuAttributeSystem = MaAttribute::select('*')->where('attribute_type', 'System')->whereIn('restaurant_id', $locations)->where('status', 1)->get(); 
	$menuAttributeSystemMeal = MaAttribute::select('*')->where('attribute_type', 'SystemMeal')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
	$menuAttributeModifier = MaAttribute::select('*')->where('attribute_type', 'SystemModifier')->whereIn('restaurant_id', $locations)->where('status', 1)->get(); 
	$menuAttributeModifierItem = MaAttribute::select('*')->where('attribute_type', 'SystemModifierItem')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
	     
        return view('v2menu.ma_attribute_set_create', compact('menuAttributeGeneral','menuAttributeSystem','menuAttributeSystemMeal','menuAttributeModifier','menuAttributeModifierItem','groupRestData','menuAttributeAddon','menuAttributeAddonItem'))->with('restaurant_id', $restaurant_id);
    }
    public function editAttributeSet(Request $request,$id) {
        $restaurant_id = 0;
	$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
	$user = Auth::user();  
	if($user->role=="manager"){
		 	$groupRestData =  Restaurant::select('id', 'restaurant_name')->whereIn('id',$locations)->orderBy('id', 'DESC')->get()->toArray(); 
	}else {
		 
			$groupRestData =  Restaurant::select('id', 'restaurant_name')->whereIn('id',$locations)->orderBy('id', 'DESC')->get()->toArray(); 
		 
	} 
	//'General','Modifier','System','SystemModifier','SystemModifierItem','SystemMeal','SystemMealItem','SystemAddon','SystemAddonItem'
        
	$restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
        if ($restaurantData && count($restaurantData) == 1) {
            $restaurant_id = $restaurantData[0]['id'];
        }
	$menuAttributeAddon = MaAttribute::select('*')->where('attribute_type', 'SystemAddon')->whereIn('restaurant_id', $locations)->where('status', 1)->get(); 
	$menuAttributeAddonItem = MaAttribute::select('*')->where('attribute_type', 'SystemAddonItem')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
	$menuAttributeModifier = MaAttribute::select('*')->where('attribute_type', 'SystemModifier')->whereIn('restaurant_id', $locations)->where('status', 1)->get(); 
	$menuAttributeModifierItem = MaAttribute::select('*')->where('attribute_type', 'SystemModifierItem')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
	$menuAttributeSet = MaAttributeSet::select('*')->whereIn('restaurant_id', $locations)->where('id', $id)->get()->toArray();
        $menuAttributeGeneral = MaAttribute::select('*')->where('attribute_type', 'General')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
        $menuAttributeSystem = MaAttribute::select('*')->where('attribute_type', 'System')->whereIn('restaurant_id', $locations)->where('status', 1)->get(); 
	$menuAttributeSystemMeal = MaAttribute::select('*')->where('attribute_type', 'SystemMeal')->whereIn('restaurant_id', $locations)->where('status', 1)->get();  
        return view('v2menu.ma_attribute_set_edit', compact('menuAttributeGeneral','menuAttributeSystem','menuAttributeSystemMeal','menuAttributeSet','groupRestData','menuAttributeModifier','menuAttributeModifierItem','menuAttributeAddon','menuAttributeAddonItem'))->with('restaurant_id', $restaurant_id);
    }
    public function getAttributeList($rest){
	$menuAttributeModifier = MaAttribute::select('*')->where('attribute_type', 'SystemModifier')->whereIn('restaurant_id', $locations)->where('status', 1)->get(); 
	$menuAttributeModifierItem = MaAttribute::select('*')->where('attribute_type', 'SystemModifierItem')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
	$menuAttributeSet = MaAttributeSet::select('*')->whereIn('restaurant_id', $locations)->where('id', $id)->get()->toArray();
        $menuAttributeGeneral = MaAttribute::select('*')->where('attribute_type', 'General')->whereIn('restaurant_id', $locations)->where('status', 1)->get();
        $menuAttributeSystem = MaAttribute::select('*')->where('attribute_type', 'System')->whereIn('restaurant_id', $locations)->where('status', 1)->get(); 
	$menuAttributeSystemMeal = MaAttribute::select('*')->where('attribute_type', 'SystemMeal')->whereIn('restaurant_id', $locations)->where('status', 1)->get();  
	// return \Response::json(['data'=>$dataarray,'error'=>0,'message'=>'Menu Attribute added successfully'], 200);	
    }	
    public function addUpdateAttributeSet(Request $request){
	        $dataarray=$request->all(); 
		$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
		
                $data['name'] =  $dataarray['name'];
		
		$system_addon = $system_addon_item = $system_modifier = $system_modifier_item = $general_attribute_ids =  null;
		if(isset($dataarray['general_attribute_ids'])){
			foreach($dataarray['general_attribute_ids'] as $key=>$value)
			$general_attribute_ids[]=$key;
			$data['general'] = implode(',',$general_attribute_ids);
		}
                if(isset($dataarray['system_modifier'])){
			foreach($dataarray['system_modifier'] as $key=>$value)
			$system_modifier[]=$key;
			$data['system_modifier'] = implode(',',$system_modifier);
		}
		if(isset($dataarray['system_modifier_item'])){
			foreach($dataarray['system_modifier_item'] as $key=>$value)
			$system_modifier_item[]=$key;
			$data['system_modifier_item'] = implode(',',$system_modifier_item);
		}
		if(isset($dataarray['system_addon'])){
			foreach($dataarray['system_addon'] as $key=>$value)
			$system_addon[]=$key;
			$data['system_addon'] = implode(',',$system_addon);
		}
		if(isset($dataarray['system_addon_item'])){
			foreach($dataarray['system_addon_item'] as $key=>$value)
			$system_addon_item[]=$key;
			$data['system_addon_item'] = implode(',',$system_addon_item);
		}	
		$data['status'] =  isset($dataarray['status'])?$dataarray['status']:1;
		$data['restaurant_id']=  $dataarray['restaurant_id'];
		if(isset($dataarray['id'])){
			$msg = 'Attribute Set updated successfully!!!';
			MaAttributeSet::where('id',$dataarray['id'])->update($data);//print_r($data);die;
		}else {
			$d = MaAttributeSet::create($data);
			$msg = 'Attribute Set created successfully!!!';
		}
		return redirect('/v2/systemattributeset')->with('message', $msg);
    }
}
