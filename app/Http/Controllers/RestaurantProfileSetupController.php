<?php

namespace App\Http\Controllers;

use App\Helpers\BYW\CloneRestaurant;
use App\Models\BYW\Cusines;
use App\Models\BYW\DeliveryRates;
use App\Models\BYW\DeliveryConfig;
use App\Models\BYW\Restaurants as Restaurant;
use App\Models\BYW\RestaurantDeliveryCustomTime;
use App\Models\BYW\RestaurantVisiblityCustomTime;
use App\Models\BYW\CmsRestaurantOperationCustomTime;
use App\Models\BYW\RestaurantVisiblityWeekTime;
use App\Models\BYW\RestaurantDeliveryWeekTime;
 
use App\Models\BYW\CustomVisibilityHours;
use App\Models\BYW\RestaurantCarryoutWeekTime;
use App\Models\BYW\RestaurantCarryoutCustomeTime;
use App\Models\BYW\CmsRestaurantOperationWeekTime;
use App\Models\PromotionCoupon;


use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Helpers\CommonFunctions;
use App\Models\DeliveryService;

use File;
use Config;
use Intervention\Image\Facades\Image;
use DB;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\CsvImportRequest;
use URL;

class RestaurantProfileSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Website',['only'=>['website']]);
        $this->middleware('permission:General info',['only'=>['generalinfo']]);
        $this->middleware('permission:Update General Info', ['only' => ['updateprofile']]);
        $this->middleware('permission:Ordering', ['only' => ['onlineorderinginfo']]);
        $this->middleware('permission:Ordering Edit', ['only' => ['restaurantsettings']]);
        $this->middleware('permission:Promotions', ['only' => ['dealinfo']]);
        $this->middleware('permission:Add Promotions', ['only' => ['addPromotion']]);
        $this->middleware('permission:Add Coupon', ['only' => ['addCoupon']]);
        $this->middleware('permission:Social Media', ['only' => ['socialinfo']]);
    }
    public function index(Request $request)
    {
        $inputData = array('restaurant_id'=> 0 ,'rest_code'=>'');
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantData = Restaurant::select('*')->whereIn('id',$locations)->orderBy('id','DESC')->get();
        if($restaurantData && count($restaurantData) == 1) {
            $inputData['restaurant_id'] = $restaurantData[0]['id'];
        }
        
        return view('profile_setup.index', compact('restaurantData'));
         
    }
    public function generalinfo(Request $request)
    {

        $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $parentlocation = Restaurant::select('parent_restaurant_id')->where('id', $location[0])->get()->toArray();
        $parentlocation[0]['parent_restaurant_id'] = ($parentlocation[0]['parent_restaurant_id']==0)?$location[0]:$parentlocation[0]['parent_restaurant_id'];
        $parentRestaurantData = Restaurant::select(['restaurants.*','cities.state_id as cstate_id','cities.country_id as ccountry_id','pcities.state_id as pstate_id','pcities.country_id as pcountry_id'])
            ->leftJoin('cities', 'restaurants.contact_city_id', '=', 'cities.id')
            ->leftJoin('cities as pcities', 'restaurants.city_id', '=', 'pcities.id')
            ->where('restaurants.id',$parentlocation[0]['parent_restaurant_id'])->orderBy('restaurants.id','DESC')->get()->toArray();
        $parentRestaurantCityData = City::select('*')->where('id',$parentRestaurantData[0]['city_id'])->orderBy('id','DESC')->get()->toArray();
         $user = Auth::user(); //echo"<pre>"; print_r($user );die;
        if($user->role=="manager") {
            $branchRestaurantData = Restaurant::select(['restaurants.*', 'cities.state_id as cstate_id', 'cities.country_id as ccountry_id', 'pcities.state_id as pstate_id', 'pcities.country_id as pcountry_id'])
                ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                ->leftJoin('cities as pcities', 'restaurants.city_id', '=', 'pcities.id')
                ->where('restaurants.id', $user->restaurant_id)->orderBy('restaurants.id', 'ASC')->get();
        }else{
            $branchRestaurantData = Restaurant::select(['restaurants.*', 'cities.state_id as cstate_id', 'cities.country_id as ccountry_id', 'pcities.state_id as pstate_id', 'pcities.country_id as pcountry_id'])
                ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                ->leftJoin('cities as pcities', 'restaurants.city_id', '=', 'pcities.id')
                ->where('parent_restaurant_id', $parentlocation[0]['parent_restaurant_id'])->orderBy('restaurants.id', 'ASC')->get();
        }
        $Cusines = Cusines::select('id','name')->orderBy('name','ASC')->get();
        $cites = City::select('id','city_name')->where('status',1)->orderBy('city_name','ASC')->get();
        $state = State::select('id','state')->where('status',1)->orderBy('state','ASC')->get();
        $country = Country::select('id','country_name')->where('status',1)->orderBy('country_name','ASC')->get();
        $languageData = Language::select('id','language_name')->where('status',1)->orderBy('language_name','ASC')->get();
        return view('profile_setup.generalinfo', compact('languageData','user','country','cites','state','parentRestaurantData','branchRestaurantData','parentRestaurantCityData','Cusines'));


    }
    public function socialinfo(Request $request)
    {
        //$inputData = array('restaurant_id'=> 0 ,'rest_code'=>'');
        $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $locationInfo1 = Restaurant::select('parent_restaurant_id')->where('id', $location[0])->get()->toArray(); //echo"<pre>";print_r($locationInfo);die;
	//if($locationInfo1[0]['parent_restaurant_id']>0)
	//$locationInfo = Restaurant::select('*')->where('id', $locationInfo1[0]['parent_restaurant_id'])->get()->toArray();
	//else 
        $locationInfo = Restaurant::select('*')->where('id', $location[0])->get()->toArray();
	$resultsReferral = DB::select('select * from restaurant_referral_promotions where restaurant_id = :id', ['id' => $location[0]]);
	if(isset($resultsReferral[0])){
		$locationInfo[0]['referral_code_status'] = $resultsReferral[0]->referral_code_status;
		$locationInfo[0]['referral_code']= $resultsReferral[0]->referral_code;
		$locationInfo[0]['referral_code_amount']= $resultsReferral[0]->referral_code_amount;
	}
	//echo "<pre>";print_r($location[0]);print_r($locationInfo);die;
        return view('profile_setup.socialinfo', compact('locationInfo'));


    }
    public function dealinfo(Request $request)
    {
	 
        $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $promos  = DB::table('restaurant_promotions')
            ->where(['restaurant_id' => $location[0]])
            ->get();

        $restInfoArr = Restaurant::select('*')->where('id', $location[0])->get()->toArray();
        $couponArr = PromotionCoupon::select('*')->where('restaurant_id', $location[0])->get()->toArray();

        $user_groups = DB::table('user_groups')
            ->whereIn('restaurant_id', [0,$location[0]])
            ->where(['status'=>1])
            ->get();


	//print_r($restInfoArr[0]['flat_discount_start_date']);print_r($restInfoArr[0]['flat_discount_start_date']);print_r($couponArr);die;	
        return view('profile_setup.deal', compact('restInfoArr','couponArr','user_groups','promos'));


    }
    public function contact(Request $request)
    {
         $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $locationInfo = Restaurant::select('*')->where('id', $location[0])->get()->toArray(); //echo"<pre>";print_r($locationInfo);die;

        return view('profile_setup.contactus', compact('locationInfo'));


    }
    public function onlineorderinginfo(Request $request)
    {  
        //echo "<pre>";
        $restInfoArr = array();
        $selected_restaurant_id = '';
        $groupRestData = CommonFunctions::getRestaurantGroup();
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        $all_service_message = "Online ordering is not set up for your website currently.";
        $globalSetting = DB::table('global_settings')->whereNotNull('contact_mobile')->where('contact_mobile', '<>','')->first();
        if($globalSetting) {
            $all_service_message .= "  Please call us at $globalSetting->contact_mobile to discuss how you can start accepting orders online";
        }


        if($restArray && count($restArray) > 0) {
            $selected_restaurant_id = $restArray['0'];
            $restInfoArr = Restaurant::select('id','restaurant_name', 'current_version', 'delivery', 'takeout', 'reservation', 'reservation_type', 'delivery', 'takeout', 'reservation', 'reservation_type', 'is_merchandise_allowed', 'is_catering_allowed', 'is_contact_allowed', 'is_event_allowed', 'is_career_allowed','merchandise_takeout_allowed','merchandise_delivery_allowed','food_ordering_delivery_allowed','food_ordering_takeout_allowed','pause_service_message','is_reservation_allowed','is_food_order_allowed','is_gift_card_allowed')->whereIn('id',$restArray)->get()->toArray();
	  
            
        } $deliveryWeekTime = $carryoutWeekTime = null;
        $languageData = Language::select('id','language_name')->where('status',1)->orderBy('language_name','ASC')->get();
        return view('profile_setup.onlineordersetup', compact('languageData','groupRestData', 'restArray' ,'selected_restaurant_id', 'restInfoArr','all_service_message','carryoutWeekTime','deliveryWeekTime'));

    }
    public function onlineorderinglocationinfo(Request $request,$id)
    {
	//echo "<pre>";
        //$inputData = array('restaurant_id'=> 0 ,'rest_code'=>'');
        $restArray = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $restInfoArr = Restaurant::select('*')->where('id',$id)->get()->toArray();
        $restInfoArr1=DeliveryConfig::select('*')->where('restaurant_id',$id)->get()->toArray();
        $deliveryService = DeliveryService::select('id','short_name')->orderBy('id', 'DESC')->get();
        $resultsReferral = DB::select('select * from restaurant_referral_promotions where restaurant_id = :id', ['id' => $id]);
	//print_r($resultsReferral[0]->referral_code_status);die;
        $carryoutWeekTime = RestaurantCarryoutWeekTime::select('*')->where('restaurant_id',$id)->get()->toArray();
        $deliveryWeekTime = RestaurantDeliveryWeekTime::select('*')->where('restaurant_id',$id)->get()->toArray();
	$operationWeekTime = CmsRestaurantOperationWeekTime::select('*')->where('restaurant_id',$id)->get()->toArray();
        $days = array( "sun"=>"Sunday", "mon"=>"Monday", "tue"=> "Tuesday",  "wed"=>"Wednesday",  "thu"=>"Thursday",  "fri"=>"Friday",  "sat"=>"Saturday");
        foreach($carryoutWeekTime as $carryourArray){
            if($carryourArray['is_dayoff']==0) {
		    if(isset($days[$carryourArray['calendar_day']])){
		            $k = $days[$carryourArray['calendar_day']];
		            $slotArray = json_decode($carryourArray['slot_detail']); 
			    if(is_array($slotArray)){	 
				    foreach($slotArray as $index=>$val){	
				    $carryourArray1[$k][$index] = $val;//json_decode($carryourArray['slot_detail'])[0];
				    $carryourArray1[$k][$index]->open_time = date('h:i A', strtotime(date("Y-m-d " . $carryourArray1[$k][$index]->open_time)));
				    $carryourArray1[$k][$index]->close_time = date('h:i A', strtotime(date("Y-m-d " . $carryourArray1[$k][$index]->close_time)));
				    }
			    }
		    }else{
			$carryourArray1[$k][0]->open_time = 'Closed';
                    	$carryourArray1[$k][0]->close_time = 'Closed';
		    }
               //
            }

        }//print_r($operationWeekTime);die;
	foreach($operationWeekTime as $carryourArray){
            if($carryourArray['is_dayoff']==0) {
		    if(isset($days[$carryourArray['calendar_day']])){
		            $k = $days[$carryourArray['calendar_day']];
		            $slotArray = json_decode($carryourArray['slot_detail']);
			    if(is_array($slotArray)){		  
			    foreach($slotArray as $index=>$val){	
		            $operationArray1[$k][$index] = $val;//json_decode($carryourArray['slot_detail'])[0];
		            $operationArray1[$k][$index]->open_time = date('h:i A', strtotime(date("Y-m-d " . $operationArray1[$k][$index]->open_time)));
		            $operationArray1[$k][$index]->close_time = date('h:i A', strtotime(date("Y-m-d " . $operationArray1[$k][$index]->close_time)));
			    }}
		    }else{
			$operationArray1[$k][0]->open_time = 'Closed';
                    	$operationArray1[$k][0]->close_time = 'Closed';
		    }
               //
            }

        }//print_r($operationArray1);die;
        foreach($deliveryWeekTime as $deliveryArray){
            if($deliveryArray['is_dayoff']==0){
		if(isset($days[$deliveryArray['calendar_day']])){
                    $k = $days[$deliveryArray['calendar_day']];
		    $slotArray = json_decode($deliveryArray['slot_detail']);  	
		    if(is_array($slotArray)){	
                    foreach($slotArray as $index=>$val){	
                    $deliveryArray1[$k][$index] = $val;//json_decode($deliveryArray['slot_detail'])[0];
                    $deliveryArray1[$k][$index]->open_time = date('h:i A', strtotime(date("Y-m-d " . $deliveryArray1[$k][$index]->open_time)));
                    $deliveryArray1[$k][$index]->close_time = date('h:i A', strtotime(date("Y-m-d " . $deliveryArray1[$k][$index]->close_time)));
		    }}
                }else{
			$deliveryArray1[$k][0]->open_time = 'Closed';
                    	$deliveryArray1[$k][0]->close_time = 'Closed';
		}
            }


        }//print_r($deliveryArray1);print_r($carryourArray1);die;
	if(isset($resultsReferral[0])){
		$restInfoArr[0]['referral_code_status'] = $resultsReferral[0]->referral_code_status;
		$restInfoArr[0]['referral_code']= $resultsReferral[0]->referral_code;
		$restInfoArr[0]['referral_code_amount']= $resultsReferral[0]->referral_code_amount;
	}
        $languageData = Language::select('id','language_name')->orderBy('language_name','ASC')->get();
        //print_r($deliveryWeekTime);print_r($carryourArray);die;
        return view('profile_setup.onlineorderlocationsetup', compact('languageData','restInfoArr','restInfoArr1','deliveryArray1','carryourArray1','operationArray1','deliveryService'));


    }
    public function generateSlug($slug){
        $newstr = preg_replace('/[^a-zA-Z0-9\']/', '-', $slug);
        $newstr = str_replace("'", '', $newstr);
        return trim($newstr);
    }
    public function getCityInfo($name){
        if($name!=""){
            $cityArray = City::select('*')->where('city_name',ucwords($name))->get()->toArray();
            if(count($cityArray)>0)return $cityArray;else return null;
        }else return null;
    }
    public function getStates(Request $request){

        $cid = $request->input('country_id');
        $result = [];
        if(!empty($cid) && is_numeric($cid)) {
            $result = State::where('country_id', '=', $cid)->select('id','state')->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);

    }
    public function contactus(Request $request){
        $otherInfo = $request->all(); //print_r($otherInfo); die;


        $mailData['subject']= "Question asked by " . $otherInfo['name'];

        $mailData['body'] ="Name : ". $otherInfo['name'] ."<br>Mobile : ". $otherInfo['mobile'] . "<br>Email : ". $otherInfo['email']. "<br>Query : ".$otherInfo['query'];
        $mailData['receiver_email'] =  "pdutta@bravvura.com";
        $mailData['receiver_name']  = "pdutta@bravvura.com";
        $mailData['MAIL_FROM_NAME'] =  "NKD Pizza";
        $mailData['MAIL_FROM'] = "pdutta@bravvura.com";
        CommonFunctions::sendMail($mailData);
        //return Redirect::to('/social-account-setup');
        return redirect()->back()->with('message','Mail send to concerned department!');
    }

    public function updatesocialprofile(Request $request){
        $otherInfo = $request->all(); //print_r($otherInfo); die;
        $d['description']=$otherInfo['description'];
	$d['facebook_url']=$otherInfo['facebook_url'];
        $d['twitter_url']=$otherInfo['twitter_url'];
        $d['gmail_url']=$otherInfo['gmail_url'];
        $d['pinterest_url']=$otherInfo['pinterest_url'];
        $d['instagram_url']=$otherInfo['instagram_url'];
        $d['yelp_url']=$otherInfo['yelp_url'];
        $d['tripadvisor_url']=$otherInfo['tripadvisor_url'];
        $d['foursquare_url']=$otherInfo['foursquare_url'];

        $d['youtube_url']=$otherInfo['youtube_url'];
        $d['google_verification_code']=$otherInfo['google_verification_code'];
        $d['gtm_code']=$otherInfo['gtm_code'];
        $d['google_map_id']=$otherInfo['google_map_id'];
	    $d['header_script']=$otherInfo['header_script'];
	    $d['footer_script']=$otherInfo['footer_script'];
        $d['google_client_id']= $otherInfo['google_client_id'];
        $d['facebook_client_id']= $otherInfo['facebook_client_id'];
        $d['netcore_api_key']= $otherInfo['netcore_api_key'];
        if(isset($otherInfo['meta_tags_type'])){
            $otherInfo['meta_tags_type'] = array_filter($otherInfo['meta_tags_type']);
            if(isset($otherInfo['meta_tags_type']) && isset($otherInfo['meta_tags_label']) && isset($otherInfo['meta_tags_content'])
                && count($otherInfo['meta_tags_type'])==count($otherInfo['meta_tags_label']) && count($otherInfo['meta_tags_type'])==count($otherInfo['meta_tags_content'])) {
                #$metaKeywords = array_combine($inputArr['meta_tags_key'], $inputArr['meta_tags_val']);
                $metaArr = [];
                for($i=0; $i<count($otherInfo['meta_tags_type']); $i++) {
                    $metaArr[] = [
                        'slug' => isset($otherInfo['meta_tags_slug'][$i])?$otherInfo['meta_tags_slug'][$i]:'--',
                        'type' => $otherInfo['meta_tags_type'][$i],
                        'label' => $otherInfo['meta_tags_label'][$i],
                        'content' => $otherInfo['meta_tags_content'][$i],
                    ];
                }
                $d['meta_tags'] = json_encode($metaArr);
            }
        }else{
            $d['meta_tags'] = json_encode([]);
        }
        $otherInfo['email_type'] = array_filter($otherInfo['email_type']);
        if(isset($otherInfo['email_type'])) {
                #$metaKeywords = array_combine($inputArr['meta_tags_key'], $inputArr['meta_tags_val']);
                $emailArr = [];
                for($i=0; $i<count($otherInfo['email_type']); $i++) {
                    $emailArr[$otherInfo['email_type'][$i]] = [
                        'from_name'     => $otherInfo['from_name'][$i],
                        'from_email'    => $otherInfo['from_email'][$i],
                        'to_name'       => $otherInfo['to_name'][$i],
                        'to_email'      => $otherInfo['to_email'][$i],
                    ];
                }
                $d['email_data'] = json_encode($emailArr);
        }else{
            $d['email_data'] = json_encode([]);
        }
        unset($otherInfo['email_type']);
        unset($otherInfo['from_name']);
        unset($otherInfo['from_email']);
        unset($otherInfo['to_name']);
        unset($otherInfo['to_email']);
	unset($otherInfo['to_email']);

	
	Restaurant::where('id', $otherInfo['id'])->update($d);
        if($otherInfo['parent_restaurant_id']>0)
        Restaurant::where('id', $otherInfo['parent_restaurant_id'])->update($d);
	if($otherInfo['parent_restaurant_id']==0)
	{
		$d=null;
		//$otherInfo['parent_restaurant_id']=$otherInfo['restaurant_id'];
		$d['common_message']=isset($otherInfo['common_message'])?$otherInfo['common_message']:'';
		$d['common_message_status']=isset($otherInfo['common_message_status'])?$otherInfo['common_message_status']:0;
		$d['common_message_url']=isset($otherInfo['common_message_url'])?$otherInfo['common_message_url']:'';
		Restaurant::where('id', $otherInfo['id'])->update($d);
		/*$results = DB::select('select * from restaurant_referral_promotions where restaurant_id = :id', ['id' => $otherInfo['id']]);
		$d=null;
		$d=['referral_code' => isset($otherInfo['referral_code'])?$otherInfo['referral_code']:'',
		'referral_code_status' =>isset($otherInfo['referral_code_status'])?$otherInfo['referral_code_status']:0,
                'restaurant_id' =>$otherInfo['id'],
                'referral_code_amount' => isset($otherInfo['referral_code_amount'])?$otherInfo['referral_code_amount']:''
            	];	
		if(isset($results[0])){
			DB::table('restaurant_referral_promotions')->where('restaurant_id',$otherInfo['id']) ->update($d);
		}else{
		      //if($otherInfo['referral_code']!=""){	
		 	DB::insert('insert into restaurant_referral_promotions (referral_code, referral_code_status,restaurant_id,referral_code_amount) values (?, ?,?,?)', [$d['referral_code'], $d['referral_code_status'],$d['restaurant_id'],$d['referral_code_amount']]);
			//}
		}*/
        }
        return redirect()->back()->with('message','Restaurant Social Info updated successfully');
    }
    public function mediasettup(Request $request){
            $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
            $locationInfo = Restaurant::select('*')->where('id', $location[0])->get()->toArray(); 
        //echo"<pre>";print_r($locationInfo);die;
            return view('profile_setup.media', compact('locationInfo'));
        }
    public function removerestimage(Request $request){
        $id = $request->input('id');
            $inputArr = $request->all();
        $d[$inputArr['imgCol']]='';
        $d[$inputArr['imgColAlt']]='';
            Restaurant::where('id', $id)->update($d);
        return "success";
    }	
    public function updaterestaurantmedia(Request $request){
        $rules = [
            'id' => 'sometimes|nullable|numeric',
        ];

        $validator = Validator::make(Input::all(), $rules,[ ]);

        $id = $request->input('id');
        $inputArr = $request->all();
        //print_r($inputArr); die;

        $imageRelPath = '';
        if ($validator->passes()) {
            $max_file_size = config('constants.image.file_size');
            $max_video_file_size = config('constants.video.file_size');
            $allowed_image_extension = array("png", "jpg", "jpeg", "gif","svg");
            $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
            $allowed_video_extension = array("mp4", "mov", "3gp");
            $allowed_video_mime_types = array("video/mp4", "video/mov","video/3pg");
            if ($request->hasFile('restaurant_image_name')) {
                    $restImage = $request->file('restaurant_image_name');
                    if (!file_exists($restImage->getRealPath())) {
                        $validator->errors()->add('restaurant_image_name', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($restImage->getClientOriginalExtension(), $allowed_image_extension) || !in_array($restImage->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('restaurant_image_name', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($restImage->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('restaurant_image_name', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
            }if ($request->hasFile('restaurant_video_name')) {
                    $restVideo = $request->file('restaurant_video_name');
                    if (!file_exists($restVideo->getRealPath())) {
                        $validator->errors()->add('restaurant_video_name', 'Please select Video.');
                        $img_err = 1;
                    } else if (!in_array($restVideo->getClientOriginalExtension(), $allowed_video_extension) || !in_array($restVideo->getMimeType(), $allowed_video_mime_types)) {
                        $validator->errors()->add('restaurant_video_name', 'Please select valid Video.');
                        $img_err = 1;
                    } else if (($restVideo->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('restaurant_video_name', 'Please upload video up to ' . $max_video_file_size . ' KB.');
                        $img_err = 1;
                    }
             }if ($request->hasFile('restaurant_logo_name')) {
                    $restLogo = $request->file('restaurant_logo_name');
                    if (!file_exists($restLogo->getRealPath())) {
                        $validator->errors()->add('restaurant_logo_name', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($restLogo->getClientOriginalExtension(), $allowed_image_extension) || !in_array($restLogo->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('restaurant_logo_name', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($restLogo->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('restaurant_logo_name', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
                }
		if ($request->hasFile('restaurant_favicon')) {
                    $restLogo = $request->file('restaurant_favicon');
                    if (!file_exists($restLogo->getRealPath())) {
                        $validator->errors()->add('restaurant_favicon', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($restLogo->getClientOriginalExtension(), $allowed_image_extension) || !in_array($restLogo->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('restaurant_favicon', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($restLogo->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('restaurant_favicon', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
                }
            }
        $ismedia = 0;
            // TODO: image, video validation
            // upload image, video
            // RESTAURANT IMAGE
            $isDeleteRestaurantImage = isset($inputArr['delete_restaurant_image_name'])?'1':0;

            if ($request->hasFile('restaurant_image_name')) {

                $imageRelPath = config('constants.image.rel_path.restaurant');
                $imagePath = config('constants.image.path.restaurant');
                // create restaurant folder
                if (!file_exists($imagePath) && !@mkdir($imagePath, 0777, true)) {
                    dd('unable to create directory ');
                } else {
                    $restLogo = $request->file('restaurant_image_name');
                    $uniqId = uniqid(mt_rand());
                    $restImageName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                    $imageName = $restImageName . '_image_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();

                    $image = Image::make($restLogo->getRealPath());
                    $width = $image->width();
                    $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                    $ismedia = 1;

                    $restImagePath = $imageRelPath . $imageName;
                    $inputArr['restaurant_image_name'] = $restImagePath;
                }
            }elseif($isDeleteRestaurantImage==1){
                //$ismedia = 1;
                //$inputArr['restaurant_image_name'] = '';
            }
            // RESTAURANT VIDEO
            $isDeleteRestaurantImage = isset($inputArr['delete_restaurant_video_name'])?'1':0;

            if ($request->hasFile('restaurant_video_name')) {
                $videoRelPath = config('constants.video.rel_path.restaurant');
                $videoPath = config('constants.video.path.restaurant');
                // create restaurant folder
                if (!file_exists($videoPath) && !@mkdir($videoPath, 0777, true)) {
                    dd('unable to create directory');
                } else {
                    $restLogo = $request->file('restaurant_video_name');
                    $uniqId = uniqid(mt_rand());
                    $restVideoName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                    $videoName = $restVideoName . '_video_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();
                    $ismedia = 1;
                    $restLogo->move($videoPath, $videoName);
                    $restVideoPath = $videoRelPath . $videoName;
                    $inputArr['restaurant_video_name'] = $restVideoPath;
                }
            }elseif($isDeleteRestaurantImage==1){
               // $ismedia = 1;
                //$inputArr['restaurant_video_name'] = '';
            }
            $isDeleteRestaurantImage = isset($inputArr['delete_restaurant_logo_name'])?'1':0;
            // RESTAURANT LOGO
            if ($request->hasFile('restaurant_logo_name')) {
                $imageRelPath = config('constants.image.rel_path.restaurant');
                $imagePath = config('constants.image.path.restaurant');
                // create restaurant folder
                if (!file_exists($imagePath) && !@mkdir($imagePath, 0777, true)) {
                    dd('unable to create directory');
                } else {
                    $restLogo = $request->file('restaurant_logo_name');
                    $uniqId = uniqid(mt_rand());
                    $restImageName = strtolower(str_replace(' ', '_', $inputArr['restaurant_name']));
                    $imageName = $restImageName . '_logo_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();
                    $ismedia = 1;
                    $image = Image::make($restLogo->getRealPath());
                    $width = $image->width();
                    $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                    $restLogoPath = $imageRelPath . $imageName;
                    $inputArr['restaurant_logo_name'] = $restLogoPath;
                }
            }elseif($isDeleteRestaurantImage==1){
                //$ismedia = 1;
                //$inputArr['restaurant_logo_name'] = '';
            }
	    if ($request->hasFile('restaurant_favicon')) {
                $imageRelPath = config('constants.image.rel_path.restaurant');
                $imagePath = config('constants.image.path.restaurant');
                // create restaurant folder
                if (!file_exists($imagePath) && !@mkdir($imagePath, 0777, true)) {
                    dd('unable to create directory');
                } else {
                    $restLogo = $request->file('restaurant_favicon');
                    $uniqId = uniqid(mt_rand());
                    $restImageName = strtolower(str_replace(' ', '_', $inputArr['restaurant_favicon']));
                    $imageName = $restImageName . '_logo_' . $uniqId . '.' . $restLogo->getClientOriginalExtension();
                    $ismedia = 1;
                    $image = Image::make($restLogo->getRealPath());
                    $width = $image->width();
                    $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                    $restLogoPath = $imageRelPath . $imageName;
                    $inputArr['restaurant_favicon'] = $restLogoPath;
                }
            }elseif($isDeleteRestaurantImage==1){
                //$ismedia = 1;
                //$inputArr['restaurant_logo_name'] = '';
            }	

            //$inputArr['kpt_calender'] = $request->input('update_days').'-'.$request->input('update_hour').'-'.$request->input('update_min');
            unset($inputArr['delete_restaurant_logo_name']);unset($inputArr['delete_restaurant_video_name']);
            unset($inputArr['delete_restaurant_image_name']);
            unset($inputArr['_token']);unset($inputArr['id']);unset($inputArr['restaurant_name']);unset($inputArr['image_hidden']);

           // $inputArr['kpt'] = ($request->input('update_days') * 24 * 60) + ($request->input('update_hour') * 60) + ($request->input('update_min')) ;
         if($ismedia == 1)Restaurant::where('id', $id)->update($inputArr);
        return redirect()->back()->with('message','Restaurant Info updated successfully');

    }
    public function updateParentInfo($otherInfo){
        $otherInfo['parent_contact_city_id'] = (isset($otherInfo['parent_contact_city_id']))?$otherInfo['parent_contact_city_id']:0;
        $contactAddressCityInfo =$this->getCityInfo($otherInfo['parent_contact_city_id']);
        $d['tagline']=isset($otherInfo['tagline'])?$otherInfo['tagline']:'';
	//$d['contact_person_title']=isset($otherInfo['parent_contact_person_title'])?$otherInfo['parent_contact_person_title']:'';
        $d['cusine']=isset($otherInfo['restaurant_type'])?$otherInfo['restaurant_type']:'';
        $d['feature']=isset($otherInfo['feature'])?$otherInfo['feature']:'';
        $d['restaurant_name']=isset($otherInfo['parent_restaurant_name'])?$otherInfo['parent_restaurant_name']:'';
        $d['contact_person_title']=isset($otherInfo['parent_contact_person_title'])?$otherInfo['parent_contact_person_title']:'';
        $d['contact_person']=isset($otherInfo['parent_contact_person'])?$otherInfo['parent_contact_person']:'';
        $d['address']=isset($otherInfo['parent_address'])?$otherInfo['parent_address']:'';
        $d['address2']=isset($otherInfo['parent_address2'])?$otherInfo['parent_address2']:'';
        if(isset($otherInfo['parent_contact_city_id']))$d['contact_city_id']=  $otherInfo['parent_contact_city_id'];
        $d['street']=isset($otherInfo['parent_street'])?$otherInfo['parent_street']:'';
        $d['contact_state']=isset($otherInfo['parent_contact_state'])?$otherInfo['parent_contact_state']:'';
        $d['contact_country']=isset($otherInfo['parent_contact_country'])?$otherInfo['parent_contact_country']:'';
        $d['contact_city_id']=isset($otherInfo['parent_contact_city_id'])?$otherInfo['parent_contact_city_id']:0;
        $d['contact_zipcode']=isset($otherInfo['parent_contact_zipcode'])?$otherInfo['parent_contact_zipcode']:'';
        $d['phone']= isset($otherInfo['parent_phone'])?$otherInfo['parent_phone']:'';
        $d['email']= isset($otherInfo['parent_email'])?$otherInfo['parent_email']:'';
        $d['delivery_custom_message']= isset($otherInfo['delivery_custom_message'])?$otherInfo['delivery_custom_message']:'';
        $d['takeout_custom_message']= isset($otherInfo['takeout_custom_message'])?$otherInfo['takeout_custom_message']:'';
        $d['client_id']= isset($otherInfo['client_id'])?$otherInfo['client_id']:NULL;
        Restaurant::where('id', $otherInfo['parent_restaurant_id'])->update($d);
    }
    public function updateprofile(Request $request){
        //echo"<pre>";
        $otherInfo = $request->all();
         
        $user = Auth::user();
        
        if($user->role!="manager") {
            $this->updateParentInfo($otherInfo);
        }
        $countBranch = $request->input('id');
        $restaurantObj = Restaurant::select('*')->where('parent_restaurant_id', $otherInfo['parent_restaurant_id'])->orderBy('id', 'ASC')->limit(1)->get()->toArray();
        $restaurant_id = $restaurantObj[0]['id'];


        for($i = 0; $i<count($countBranch);$i++){
            $d[$i]['id']=$otherInfo['id'][$i];
            $otherInfo['contact_city_id'][$i] = (isset($otherInfo['contact_city_id'][$i]))?$otherInfo['contact_city_id'][$i]:0;
            $otherInfo['pickup_contact_city_id'][$i] = (isset($otherInfo['pickup_contact_city_id'][$i]))?$otherInfo['pickup_contact_city_id'][$i]:0;
            $contactAddressCityInfo =$this->getCityInfo($otherInfo['contact_city_id'][$i]);
            $pickupAddressCityInfo =$this->getCityInfo($otherInfo['pickup_contact_city_id'][$i]);
            $d[$i]['parent_restaurant_id']=$otherInfo['parent_restaurant_id'];
            $d[$i]['client_id']=isset($otherInfo['client_id'][$i])?$otherInfo['client_id']:NULL;           
            $d[$i]['restaurant_name']=isset($otherInfo['restaurant_name'][$i])?$otherInfo['restaurant_name'][$i]:'';            
            $d[$i]['contact_person_title']=isset($otherInfo['contact_person_title'][$i])?$otherInfo['contact_person_title'][$i]:'';
            if($user->role!="manager") {
		 $d[$i]['title']=isset($otherInfo['title'][$i])?$otherInfo['title'][$i]:'';
	      	$d[$i]['slug']=isset($otherInfo['slug'][$i])?$otherInfo['slug'][$i]:'';	
                $d[$i]['status']=isset($otherInfo['status'][$i])?$otherInfo['status'][$i]:'1';
                if (isset($otherInfo['is_default_outlet']) && ($otherInfo['id'][$i] == $otherInfo['is_default_outlet']))
                    $d[$i]['is_default_outlet'] = 1;
                else
                    $d[$i]['is_default_outlet'] = 0;
            }
            $d[$i]['contact_person']=isset($otherInfo['contact_person'][$i])?$otherInfo['contact_person'][$i]:'';
	    $supportedLanguages = !empty($otherInfo['supported_languages'][$i]) ? implode(',', $otherInfo['supported_languages'][$i]) : '1';	
	    $d[$i]['supported_languages']=$supportedLanguages; 
	    $d[$i]['language_id']=isset($otherInfo['language_id'][$i])?$otherInfo['language_id'][$i]:'1';
            $d[$i]['lat']=isset($otherInfo['lat'][$i])?$otherInfo['lat'][$i]:'';
            $d[$i]['lng']=isset($otherInfo['lng'][$i])?$otherInfo['lng'][$i]:'';
            ///$d[$i]['status']=isset($otherInfo['status'][$i])?$otherInfo['status'][$i]:1;
            $d[$i]['contact_address']=isset($otherInfo['contact_address'][$i])?$otherInfo['contact_address'][$i]:'';
            $d[$i]['contact_address2']=isset($otherInfo['contact_address2'][$i])?$otherInfo['contact_address2'][$i]:'';
            $d[$i]['contact_street']=isset($otherInfo['contact_street'][$i])?$otherInfo['contact_street'][$i]:'';
            //$d[$i]['contact_city_id']=isset($contactAddressCityInfo[0])?$contactAddressCityInfo[0]['id']:0;
            if(isset($otherInfo['contact_city_id'][$i]) && $otherInfo['contact_city_id'][$i]>0)
                $d[$i]['contact_city_id']=$otherInfo['contact_city_id'][$i];
            $d[$i]['contact_country']=isset($otherInfo['contact_country'][$i])?$otherInfo['contact_country'][$i]:'';
            $d[$i]['contact_state']=isset($otherInfo['contact_state'][$i])?$otherInfo['contact_state'][$i]:'';
            $d[$i]['contact_zipcode']=isset($otherInfo['contact_zipcode'][$i])?$otherInfo['contact_zipcode'][$i]:'';
            $d[$i]['contact_phone']=isset($otherInfo['contact_phone'][$i])?$otherInfo['contact_phone'][$i]:'';
            $d[$i]['contact_email']=isset($otherInfo['contact_email'][$i])?$otherInfo['contact_email'][$i]:'';

	    

            $d[$i]['template_state']=1;
            $d[$i]['support_from']=isset($otherInfo['support_from'][$i])?$otherInfo['support_from'][$i]:'';
            $d[$i]['source_url']=isset($otherInfo['source_url'][$i])?$otherInfo['source_url'][$i]:'';

            //$d[$i]['supported_languages']=isset($otherInfo['supported_languages'][$i])?json_encode($otherInfo['supported_languages'][$i]):'';
            $d[$i]['language_id']=isset($otherInfo['language_id'][$i])?$otherInfo['language_id'][$i]:'1';
            $d[$i]['updated_at']=date("Y_m-d:H:i");
           // $d[$i]['is_pickup_contact_address_same']=($otherInfo['same_branch_and_pickup_address'][$i]==$d[$i]['id'] && $otherInfo['same_branch_and_pickup_address'][$i]>0)?1:0;
            if(isset($otherInfo['same_branch_and_pickup_address'][$i]) && $otherInfo['same_branch_and_pickup_address'][$i]==$d[$i]['id']){
		$d[$i]['is_pickup_contact_address_same']= 1;
                $d[$i]['pickup_contact_person_title']=$d[$i]['contact_person_title'];
                $d[$i]['pickup_contact_person']=$d[$i]['contact_person'];
                $d[$i]['pickup_contact_email']=$d[$i]['contact_email'];
                $d[$i]['city_id']=$d[$i]['contact_city_id'];
                //$d[$i]['pickup_country']=isset($otherInfo['pickup_contact_country'][$i])?$otherInfo['pickup_contact_country'][$i]:'';
                //$d[$i]['pickup_state']=isset($otherInfo['pickup_contact_state'][$i])?$otherInfo['pickup_contact_state'][$i]:'';
                $d[$i]['zipcode']=$d[$i]['contact_zipcode'];
                $d[$i]['address']=$d[$i]['contact_address'];
                $d[$i]['address2']=$d[$i]['contact_address2'];
                $d[$i]['street']=$d[$i]['contact_street'];
                $d[$i]['phone']=$d[$i]['contact_phone'];
                $d[$i]['email']=$d[$i]['contact_email'];

            }else{
		$d[$i]['is_pickup_contact_address_same']= 0;
                $d[$i]['pickup_contact_person_title']=isset($otherInfo['pickup_person_title'][$i])?$otherInfo['pickup_person_title'][$i]:'';
                $d[$i]['pickup_contact_person']=isset($otherInfo['pickup_contact_person'][$i])?$otherInfo['pickup_contact_person'][$i]:'';
                $d[$i]['pickup_contact_email']=isset($otherInfo['pickup_contact_email'][$i])?$otherInfo['pickup_contact_email'][$i]:'';
                if(isset($otherInfo['pickup_contact_city_id'][$i]) && $otherInfo['pickup_contact_city_id'][$i]>0)$d[$i]['city_id']=$otherInfo['pickup_contact_city_id'][$i];
                $d[$i]['pickup_country']=isset($otherInfo['pickup_contact_country'][$i])?$otherInfo['pickup_contact_country'][$i]:'';
                $d[$i]['pickup_state']=isset($otherInfo['pickup_contact_state'][$i])?$otherInfo['pickup_contact_state'][$i]:'';
                $d[$i]['zipcode']=isset($otherInfo['pickup_contact_zipcode'][$i])?$otherInfo['pickup_contact_zipcode'][$i]:'';
                $d[$i]['address']=isset($otherInfo['pickup_contact_address'][$i])?$otherInfo['pickup_contact_address'][$i]:'';
                $d[$i]['address2']=isset($otherInfo['pickup_contact_address2'][$i])?$otherInfo['pickup_contact_address2'][$i]:'';
                $d[$i]['street']=isset($otherInfo['pickup_contact_street'][$i])?$otherInfo['pickup_contact_street'][$i]:'';
                $d[$i]['phone']=isset($otherInfo['pickup_contact_phone'][$i])?$otherInfo['pickup_contact_phone'][$i]:'';
                $d[$i]['email']=isset($otherInfo['pickup_contact_email'][$i])?$otherInfo['pickup_contact_email'][$i]:'';
            }
	    //echo "<pre>";print_r($d);	
            if($d[$i]['id']==0 && isset($d[$i]['restaurant_name']) && $d[$i]['restaurant_name']!='' && isset($d[$i]['contact_email']) && $d[$i]['contact_email']!=''){//clone restaurant
                //DB::beginTransaction();
                //DB::rollback(); DB::commit();
                $clone = new CloneRestaurant();
                $d[$i]['is_default_outlet']=0;
                $new_restaurant_id = $clone->coneRestaurnatData($restaurant_id, $otherInfo['parent_restaurant_id'], $d[$i], 0);

                $old_restaurant_id = $restaurant_id;
                $d1 = $clone->coneNewMenuCategoryData($new_restaurant_id, $old_restaurant_id, $d);
                if(isset($d[$i]['contact_email']))CloneRestaurant::sendmailtoclient($d[$i]['contact_email'],$d[$i]['contact_email'],$d[$i]['contact_person']);
            }else{
                //update restaurant
                //$restaurantObj = Restaurant::find($id);
                Restaurant::where('id', $d[$i]['id'])->update($d[$i]);
            }
        }  return redirect()->back()->with('message','Restaurant Info updated successfully');
        //return Redirect::to('/profile-setup')->with('message','Restaurant Info updated successfully');
        //print_r($d);die;
    }

    public function restaurantsettingsupdate(CsvImportRequest $request)
    {
	
        $otherInfo = $request->all();
	 
	//print_r($otherInfo);die;
        $d['delivery_geo']=isset($otherInfo['delivery_geo'])?$otherInfo['delivery_geo']:'';
        $d['delivery_zipcode']=isset($otherInfo['delivery_zipcode'])?$otherInfo['delivery_zipcode']:'';
        $d['is_delivery_geo']= isset($otherInfo['is_delivery_geo'])?$otherInfo['is_delivery_geo']:0;
        $d['minimum_delivery']=isset($otherInfo['minimum_order_amount'])?$otherInfo['minimum_order_amount']:'0';
        $d['free_delivery']=isset($otherInfo['free_delivery'])?$otherInfo['free_delivery']:'0';
        $d['delivery_asap_gap']=isset($otherInfo['delivery_asap_gap'])?$otherInfo['delivery_asap_gap']:'';
        $d['carryout_asap_gap']=isset($otherInfo['carryout_asap_gap'])?$otherInfo['carryout_asap_gap']:'';
        $d['delivery_interval']=isset($otherInfo['delivery_interval'])?$otherInfo['delivery_interval']:'';
        $d['carryout_interval']=isset($otherInfo['carryout_interval'])?$otherInfo['carryout_interval']:'';
        $d['delivery_order_gap']=isset($otherInfo['delivery_order_gap'])?$otherInfo['delivery_order_gap']:'';
        $d['carryout_order_gap']=isset($otherInfo['carryout_order_gap'])?$otherInfo['carryout_order_gap']:'';
        //$d['status']=isset($otherInfo['status'])?$otherInfo['status']:'0';
        $d['tax']=isset($otherInfo['tax'])?$otherInfo['tax']:'0';
        $d['lat']=isset($otherInfo['lat'])?$otherInfo['lat']:'0';
        $d['lng']=isset($otherInfo['lng'])?$otherInfo['lng']:'0';
        $d['service_tax']= isset($otherInfo['service_tax'])?$otherInfo['service_tax']:'0';

        $d['delivery_charge']=isset($otherInfo['delivery_charge'])?$otherInfo['delivery_charge']:0;
	$d['preorder_delivery_accept_in_days']= isset($otherInfo['preorder_delivery_accept_in_days'])?$otherInfo['preorder_delivery_accept_in_days']:'1';
	$d['preorder_takeout_accept_in_days']=isset($otherInfo['preorder_takeout_accept_in_days'])?$otherInfo['preorder_takeout_accept_in_days']:'1';
	/*
        $d['tip']=isset($otherInfo['tip'])?$otherInfo['tip']:'';
        $d['is_preorder_accept']=isset($otherInfo['is_preorder_accept'])?$otherInfo['is_preorder_accept']:0;
        
        $d['additional_charge_config']= isset($otherInfo['additional_charge_config'])?$otherInfo['additional_charge_config']:0;
        $d['additional_charge_takeout']= isset($otherInfo['additional_charge_takeout'])?$otherInfo['additional_charge_takeout']:0;

        $d['additional_charge_name']=$otherInfo['additional_charge_name'];

        $d['additional_charge_delivery_type']=  isset($otherInfo['additional_charge_delivery_type'])?$otherInfo['additional_charge_delivery_type']:1;
        $d['additional_charge_takeout_type']=  isset($otherInfo['additional_charge_takeout_type'])?$otherInfo['additional_charge_takeout_type']:1;
        $d['delivery_charge_type']=  isset($otherInfo['delivery_charge_type'])?$otherInfo['delivery_charge_type']:1;
        $d['delivery_charge']=  isset($otherInfo['delivery_charge'])?$otherInfo['delivery_charge']:0;
        $d['additional_charge_delivery']=  isset($otherInfo['additional_charge_delivery'])?$otherInfo['additional_charge_delivery']:0;


        $d['kpt_calender'] = $otherInfo['update_days'] .'-'.$otherInfo['update_hour'].'-'.$otherInfo['update_min'];
        $d['kpt'] = ($otherInfo['update_days']  * 24 * 60) + ($otherInfo['update_hour']   * 60) + ($otherInfo['update_min']) ;
        $d['is_tip_online']=  isset($otherInfo['is_tip_online'])?$otherInfo['is_tip_online']:0;
        
	*/
	$d['kpt_calender'] = $otherInfo['update_days'] .'-'.$otherInfo['update_hour'].'-'.$otherInfo['update_min'];
        $d['kpt'] = ($otherInfo['update_days']  * 24 * 60) + ($otherInfo['update_hour']   * 60) + ($otherInfo['update_min']) ;
        $d['is_tip_online']=  isset($otherInfo['is_tip_online'])?$otherInfo['is_tip_online']:0;
	
        Restaurant::where('id', $otherInfo['restaurant_id'])->update($d);
	 
	$this->addUpdateReferralCode($request, $otherInfo['restaurant_id']);
	/*$d=null;
	if($otherInfo['parent_restaurant_id']==0)$otherInfo['parent_restaurant_id']=$otherInfo['restaurant_id'];
	$d['common_message']=isset($otherInfo['common_message'])?$otherInfo['common_message']:'';
	$d['common_message_status']=isset($otherInfo['common_message_status'])?$otherInfo['common_message_status']:0;
	$d['common_message_url']=isset($otherInfo['common_message_url'])?$otherInfo['common_message_url']:'';
	Restaurant::where('id', $otherInfo['parent_restaurant_id'])->update($d);*/
        //$this->updateDeliveryConfig($otherInfo);

        $this->parseImport($request,$otherInfo['restaurant_id']);
        return redirect()->back()->with('message','Restaurant Info updated successfully');
        //return Redirect::to('/profile-setup')->with('message','Restaurant Info updated successfully');
    }
    public function addUpdateReferralCode(Request $request,$rest_id){
	 $otherInfo = $request->all();	
	 //DB::beginTransaction();
         $data=['referral_code' => isset($otherInfo['referral_code'])?$otherInfo['referral_code']:'',
		'referral_code_status' =>isset($otherInfo['referral_code_status'])?$otherInfo['referral_code_status']:0,
                'restaurant_id' =>isset($rest_id)?$rest_id:0,
                'referral_code_amount' => isset($otherInfo['referral_code_amount'])?$otherInfo['referral_code_amount']:''
            ];
	 $results = DB::select('select * from restaurant_referral_promotions where restaurant_id = :id', ['id' => $rest_id]);
	 if(isset($results[0])){
		DB::table('restaurant_referral_promotions')->where('restaurant_id',$data['restaurant_id']) ->update($data);
	 }else{
	      //if($otherInfo['referral_code']!=""){	
	 	DB::insert('insert into restaurant_referral_promotions (referral_code, referral_code_status,restaurant_id,referral_code_amount) values (?, ?,?,?)', [$data['referral_code'], $data['referral_code_status'],$data['restaurant_id'],$data['referral_code_amount']]);
		//}
	 }
         
            //DB::commit();
    }
    public function updatediscount(Request $request){
        //echo"<pre>";
        $otherInfo = $request->all();
        /// echo"<pre>";print_r($otherInfo);die;
        $d['flat_discount']=isset($otherInfo['flat_discount'])?$otherInfo['flat_discount']:0;
        $d['flat_discount_type']=isset($otherInfo['flat_discount_type'])?$otherInfo['flat_discount_type']:1;
        $d['flat_discount_min']=isset($otherInfo['flat_discount_min'])?$otherInfo['flat_discount_min']:0;
        $d['flat_discount_start_date']=isset($otherInfo['flat_discount_start_date'])?$otherInfo['flat_discount_start_date']:'';
        $d['flat_discount_end_date']=isset($otherInfo['flat_discount_end_date'])?$otherInfo['flat_discount_end_date']:0;
	$d['flat_discount_start_date'] =  $this->reformatDate($d['flat_discount_start_date']);
	$d['flat_discount_end_date'] =  $this->reformatDate($d['flat_discount_end_date']);
        Restaurant::where('id', $otherInfo['id'])->update($d);
        return redirect()->back()->with('message','Discount updated successfully');
    }

    public function addPromotion(Request $request)
    {

                DB::beginTransaction();
                try {
                    $otherInfo = $request->all();
                    $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));

                    DB::table('restaurant_promotions')->insert(
                        [
                            'restaurant_id' =>$location[0],
                            'name' => isset($otherInfo['promotion_name'])?$otherInfo['promotion_name']:'',
                            'amount_or_percent' =>isset($otherInfo['amount_or_percent'])?$otherInfo['amount_or_percent']:0,
                            'condition' => isset($otherInfo['condition'])?$otherInfo['condition']:'',
                            'condition_amount' => isset($otherInfo['condition_amount'])?$otherInfo['condition_amount']:'',
                            'discount' =>isset($otherInfo['discount'])?$otherInfo['discount']:'',
                            'start_date' => isset($otherInfo['start_date'])?$otherInfo['start_date']:'',
                            'end_date' =>isset($otherInfo['end_date'])?$otherInfo['end_date']:'',
                            'user_group_id' => isset($otherInfo['user_group_id'])?implode(",",$otherInfo['user_group_id']):'',
                            'delivery_type' => isset($otherInfo['delivery_type'])?$otherInfo['delivery_type']:'',
                            'status' => isset($otherInfo['status'])?$otherInfo['status']:'',


                        ]
                    );
                    DB::commit();
                    return \Response::json(['success' => true,'data'=>'', 'message' =>'Promotion created successfully.'], 200);

                    // return redirect('floor')->withInput()->with('status','Floor added');


                }catch (\Exception $e){
                    DB::rollback();
                    return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
                }


    }
    public function updatePromotion(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $otherInfo = $request->all();
            $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
            $data=[
                'name' => isset($otherInfo['promotion_name'])?$otherInfo['promotion_name']:'',
                'amount_or_percent' =>isset($otherInfo['amount_or_percent'])?$otherInfo['amount_or_percent']:0,
                'condition' => isset($otherInfo['condition'])?$otherInfo['condition']:'',
                'condition_amount' => isset($otherInfo['condition_amount'])?$otherInfo['condition_amount']:'',
                'discount' =>isset($otherInfo['discount'])?$otherInfo['discount']:'',
                'start_date' => isset($otherInfo['start_date'])?$otherInfo['start_date']:'',
                'end_date' =>isset($otherInfo['end_date'])?$otherInfo['end_date']:'',
                'user_group_id' => isset($otherInfo['user_group_id'])?implode(",",$otherInfo['user_group_id']):'',
                'delivery_type' => isset($otherInfo['delivery_type'])?$otherInfo['delivery_type']:'',
                'status' => isset($otherInfo['status'])?$otherInfo['status']:'',


            ];

            DB::table('restaurant_promotions')
                ->where('id',$id)->where('restaurant_id',$location[0])
                ->update($data);
            DB::commit();
            return \Response::json(['success' => true,'data'=>'', 'message' =>'Promotion updated successfully.'], 200);

            // return redirect('floor')->withInput()->with('status','Floor added');


        }catch (\Exception $e){
            DB::rollback();
            return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
        }


    }
    public function addCoupon(Request $request)
    {

        DB::beginTransaction();
        try {
            $otherInfo = $request->all();
            $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));

            DB::table('promotion_coupon')->insert(
                [
                    'restaurant_id' =>$location[0],
                    'coupon_code' => isset($otherInfo['coupon_code'])?$otherInfo['coupon_code']:'',
                    'discount_type' =>isset($otherInfo['discount_type'])?$otherInfo['discount_type']:0,
                    'uses_per_coupon' => isset($otherInfo['uses_per_coupon'])?$otherInfo['uses_per_coupon']:'',
                    'uses_per_customer' => isset($otherInfo['uses_per_customer'])?$otherInfo['uses_per_customer']:'',
                    'minimum_amount' =>isset($otherInfo['minimum_amount'])?$otherInfo['minimum_amount']:'',
                    'discount' => isset($otherInfo['discount'])?$otherInfo['discount']:'',
                    'start_date' => isset($otherInfo['start_date'])?$otherInfo['start_date']:'',
                    'end_date' =>isset($otherInfo['end_date'])?$otherInfo['end_date']:'',
                    'user_group_id' => isset($otherInfo['user_group_id'])?implode(",",$otherInfo['user_group_id']):'',
                   // 'delivery_type' => isset($otherInfo['delivery_type'])?$otherInfo['delivery_type']:'',
                    'status' => isset($otherInfo['status'])?$otherInfo['status']:'',


                ]
            );
            DB::commit();
            return \Response::json(['success' => true,'data'=>'', 'message' =>'Coupon created successfully.'], 200);

            // return redirect('floor')->withInput()->with('status','Floor added');


        }catch (\Exception $e){
            DB::rollback();
            return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
        }


    }

    public function updateCoupon(Request $request,$id)
    {

        DB::beginTransaction();
        try {
            $otherInfo = $request->all();
            $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
            $data= [
                'coupon_code' => isset($otherInfo['coupon_code'])?$otherInfo['coupon_code']:'',
                'discount_type' =>isset($otherInfo['discount_type'])?$otherInfo['discount_type']:0,
                'uses_per_coupon' => isset($otherInfo['uses_per_coupon'])?$otherInfo['uses_per_coupon']:'',
                'uses_per_customer' => isset($otherInfo['uses_per_customer'])?$otherInfo['uses_per_customer']:'',
                'minimum_amount' =>isset($otherInfo['minimum_amount'])?$otherInfo['minimum_amount']:'',
                'discount' => isset($otherInfo['discount'])?$otherInfo['discount']:'',
                'start_date' => isset($otherInfo['start_date'])?$otherInfo['start_date']:'',
                'end_date' =>isset($otherInfo['end_date'])?$otherInfo['end_date']:'',
                'user_group_id' => isset($otherInfo['user_group_id'])?implode(",",$otherInfo['user_group_id']):'',
                // 'delivery_type' => isset($otherInfo['delivery_type'])?$otherInfo['delivery_type']:'',
                'status' => isset($otherInfo['status'])?$otherInfo['status']:'',


            ];

            DB::table('promotion_coupon')
                ->where('id',$id)->where('restaurant_id',$location[0])
                ->update($data);
            DB::commit();
            return \Response::json(['success' => true,'data'=>'', 'message' =>'Coupon updated successfully.'], 200);

            // return redirect('floor')->withInput()->with('status','Floor added');


        }catch (\Exception $e){
            DB::rollback();
            return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
        }


    }

    public function deleteCoupon($id)
    {
        DB::beginTransaction();
        try {

            $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));

            DB::table('promotion_coupon')->where('restaurant_id', $location[0])->where('id', $id)->delete();


            DB::commit();
            return \Response::json(['title' => 'Coupon Deleted', 'message' => 'Coupon has been deleted successfully'], 200);
        }
        catch (\Exception $e){
            DB::rollback();
            return \Response::json([ 'errors'=>['Unable to delete the Coupon']], 422);
        }
    }

    public function deletePromotion($id)
    {
        DB::beginTransaction();
        try {
            $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));

            DB::table('restaurant_promotions')->where('restaurant_id', $location[0])->where('id', $id)->delete();

            DB::commit();
            return \Response::json(['title' => 'Promotion Deleted', 'message' => 'Promotion has been deleted successfully'], 200);
        }
        catch (\Exception $e){
            DB::rollback();
            return \Response::json([ 'errors'=>['Unable to delete the Promotion']], 422);
        }
    }



    public function updatedeal(Request $request){
        //echo"<pre>";
        $otherInfo = $request->all();
        //print_r($otherInfo);die;

        $countCoupon = $otherInfo['id'];
        unset($otherInfo['_token']);
        for($i = 0; $i<count($countCoupon);$i++){
            $id = $otherInfo['id'][$i];
             unset($otherInfo['_token'][$i]);
            $d['coupon_code']=isset($otherInfo['coupon_code'][$i])?$otherInfo['coupon_code'][$i]:'';
            $d['discount_type']=isset($otherInfo['discount_type'][$i])?$otherInfo['discount_type'][$i]:1;
            $d['discount']=isset($otherInfo['discount'][$i])?$otherInfo['discount'][$i]:0;
	    $d['minimum_amount']=isset($otherInfo['minimum_amount'][$i])?$otherInfo['minimum_amount'][$i]:0;	
            $d['start_date']=isset($otherInfo['start_date'][$i])?$otherInfo['start_date'][$i]:'';
            $d['end_date']=isset($otherInfo['end_date'][$i])?$otherInfo['end_date'][$i]:'';
            $d['status']=isset($otherInfo['status'][$i])?$otherInfo['status'][$i]:0;
            $d['restaurant_id']=$otherInfo['restaurant_id'];
	    $d['start_date'] =  $this->reformatDate($d['start_date']);	
	    $d['end_date']= $this->reformatDate($d['end_date']);	
            unset($otherInfo['id'][$i]);
            if($id==0){
                PromotionCoupon::create($d);
            }else{

                PromotionCoupon::where('id', $id)->update($d);
            }
        }

        return redirect()->back()->with('message','Deals updated successfully');
    }
    private function reformatDate($d){
		if(isset($d)){
			$explode = explode(" ",$d);
			$dataArr = explode("-",$explode[0]);
			$d=  $dataArr[2]."-".$dataArr[1]."-".$dataArr[0]." ".$explode[1];
		    //print_r($dataArr);print_r($explode); echo"===>".$d."=="; 
		}
	        return $d;
    }
    private function reformatDateUi($d){
		if(isset($d)){
			$explode = explode(" ",$d);
			$dataArr = explode("-",$explode[0]);
			$d= $dataArr[2]."-".$dataArr[1]."-".$dataArr[0]." ".$explode[1];
			//print_r($dataArr);print_r($explode); echo"===>".$d."=="; 
		}
	        return $d;
    }
    private function updateDeliveryConfig($otherInfo){
        /*
        $d1['minimum_order_amount']=isset($otherInfo['minimum_order_amount'])?$otherInfo['minimum_order_amount']:'';
        $d1['additional_charge_delivery_type']=  isset($otherInfo['additional_charge_delivery_type'])?$otherInfo['additional_charge_delivery_type']:'';
        $d1['additional_charge_delivery_order']= isset($otherInfo['additional_charge_delivery_order'])?$otherInfo['additional_charge_delivery_order']:'';
        $d1['additional_charge_takeout_type']= isset($otherInfo['additional_charge_takeout_type'])?$otherInfo['additional_charge_takeout_type']:'';
        $d1['additional_charge_takeout_order']= isset($otherInfo['additional_charge_takeout_order'])?$otherInfo['additional_charge_takeout_order']:'';
        $d1['additional_charge_description']= isset($otherInfo['additional_charge_description'])?$otherInfo['additional_charge_description']:'';
        $d1['sort_order']=isset($otherInfo['sort_order'])?$otherInfo['sort_order']:'';
        $d1['rate_type']=isset($otherInfo['rate_type'])?$otherInfo['rate_type']:'';
        $d1['tax']=isset($otherInfo['tax'])?$otherInfo['tax']:'';
        $d1['service_tax']=isset($otherInfo['service_tax'])?$otherInfo['service_tax']:'';
        $d1['tip']= isset($otherInfo['tip'])?$otherInfo['tip']:'';
        $d1['free_delivery_above_sub_total']= isset($otherInfo['free_delivery_above_sub_total'])?$otherInfo['free_delivery_above_sub_total']:'';
        $d1['delivery_condition']=isset($otherInfo['delivery_condition'])?$otherInfo['delivery_condition']:'';
        $d1['restaurant_id']=isset($otherInfo['restaurant_id'])?$otherInfo['restaurant_id']:'';

        $d1['filtering_rule']=isset($otherInfo['filtering_rule'])?$otherInfo['filtering_rule']:'';
        $d1['delivery_service_id']=isset($otherInfo['delivery_service_id'])?$otherInfo['delivery_service_id']:0;
        $d1['status']=1;
        if(isset($otherInfo['delivery_config_id']) && $otherInfo['delivery_config_id']>0)
            DeliveryConfig::where('id', $otherInfo['delivery_config_id'])->update($d1);
        else  DeliveryConfig::create($d1);*/
    }

    public function restaurantsettings(Request $request)
    {   //print_r($request->all());die;
        $resid = $request->input('restaurant_id');
        if(!$request->has('restaurant_id') || $request->input('restaurant_id') == ''){
            return back()->withInput()->withErrors(array('message' => ' No restaurant found'));
        }else{
            $restaurant = Restaurant::find($request->input('restaurant_id'));
           //echo "<pre>";print_r($restaurant);die;
            DB::beginTransaction();
            try {    
                $current_action = $request->input('action');
                if($current_action == 'delivery') {
                    if( $request->input('delivery') == 0) {
                        $activity_type = 'Off';
                        $success_msg = "Your website is currently not accepting new delivery orders.";
                        $title="Online Ordering Delivery Off.";
                    }else {
                        $success_msg = "Your website is now accepting new delivery orders. Congratulations!"; 
                        $activity_type = 'On';
                        $title="Online Ordering Delivery On.";
                    }   
                    $service_type = 1;                 
                }  
                if($current_action == 'takeout') {
                    if( $request->input('takeout') == 0) {
                        $activity_type = 'Off';
                        $success_msg = "Your website is currently not accepting new takeout orders. "; 
                        $title="Online Ordering Takeout Off.";
                    }else {
                        $success_msg = "Your website is now accepting new takeout orders. Congratulations!"; 
                        $activity_type = 'On';
                        $title="Online Ordering Takeout On.";
                    }     
                    $service_type = 2;               
                }  
                if($current_action == 'reservation') {
                    if( $request->input('reservation') == 0) {
                        $activity_type = 'Off';
                        $success_msg = "Your website is currently not accepting new takeout reservations. "; 
                        $title="Online Reservation Off.";
                    }else {
                        $success_msg = "Your website is now accepting new takeout reservations. Congratulations!"; 
                        $activity_type = 'On';
                        $title="Online Reservation On.";
                    }  
                    $service_type = 3;                  
                }    
                //$restaurant->reservation= $request->input('reservation');
                $restaurant->delivery= $request->input('delivery'.$resid);
                $restaurant->takeout= $request->input('takeout'.$resid);
                if($request->has('pause_service_message'.$resid)) {
                    #empty the messgae when every service is ON
                    #count how many services are allowed
                    $services = 0;
                    if($restaurant->is_food_order_allowed) {
                        if($restaurant->food_ordering_delivery_allowed) {
                           $services += 1; 
                        }
                        if($restaurant->food_ordering_takeout_allowed) {
                           $services += 1; 
                        }                            
                    } 
                    if($restaurant->is_reservation_allowed && $restaurant->reservation_type == 'full') {
                       $services += 1; 
                    }
                   
                    $current_services = $restaurant->reservation + $restaurant->delivery + $restaurant->takeout;
                    //echo $restaurant->pause_service_message = $request->input('pause_service_message'.$resid); die;
                    if($services == $current_services && $services > 0) {
                        $restaurant->pause_service_message = '';
                    }else {
                        $restaurant->pause_service_message = $request->input('pause_service_message'.$resid);
                    }
                }
                $restaurant->save(); 
                $mailData = ['restaurant_id'=>$resid,'message'=>$success_msg,'title'=>$title];
                //$this->restaurantSettingMail($mailData);
                
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $restaurant->id));   
                if(is_object($currentDateTimeObj)) {                        
                    $current_datetime = $currentDateTimeObj->format('Y-m-d H:i:s');
                }else {                        
                    $current_datetime = date('Y-m-d H:i:s');
                }
                $cms_user_id = \Auth::user()->id;
                $logData = [
                    'cms_user_id' => $cms_user_id,
                    'effective_date' => $current_datetime,          
                    'service_type' => $service_type,
                    'activity_type' => $activity_type,
                    'restaurant_id' => $restaurant->id,
                    'reason' => $request->has('pause_service_message') && $activity_type == 'Off' ? $request->input('pause_service_message')  : "",
                   
                    'ipaddress' => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
                ]; 
                DB::table('dashboard_services_logs')->insert($logData);

                DB::commit();
                return back()->withInput()->with(['status'=> $success_msg]);
                //return Redirect::to('/profile-setup')->with('message',$success_msg);
           }catch (\Exception $e){                    
                DB::rollback();
                return back()->withInput()->withErrors(array('message' =>'Unable to update the settings'));
            }
        }          
    }

    /**
     * @param Params : token
     * @author : Rahul Gupta
     * @Date Created : 19-01-2019
     * @return Send logs
     */

    public function pauseResumeServiceHistory(Request $request) { 
        //print_r($request->all); die;

        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $logs = DB::table('dashboard_services_logs')->whereIn('dashboard_services_logs.restaurant_id', $restArray);
        if(isset($requestData['to']) && $requestData['from']) {
            $logs = $logs->whereDate('effective_date', '>=', $request->input('from'))->whereDate('effective_date', '<=', $request->input('to'));
        }
        $log_data = $logs->select('id','effective_date', 'service_type', 'activity_type', 'reason', DB::raw('(CASE WHEN service_type = 1 THEN "Delivery" ELSE (CASE WHEN service_type = 2 THEN "Takeout" ELSE (CASE WHEN service_type = 3 THEN "Reservation" END) END ) END) AS service_type') )->orderBy('created_at', 'DESC')->paginate(10);      
       
        #echo "<pre>";print_r($log_data);die;
        return response()->view('services.history', compact('log_data'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    }
    //composer require "maatwebsite/excel:~2.1.0"
    //https://quickadminpanel.com/blog/how-to-import-csv-in-laravel-and-choose-matching-fields/
    public function parseImport(CsvImportRequest $request,$restaurant_id)
    {
        if($request->hasFile('csv_file')) {
            $path = $request->file('csv_file')->getRealPath();

            if ($request->has('header')) {
                $data = Excel::load($path, function ($reader) {
                })->get()->toArray();
            } else {
                $data = array_map('str_getcsv', file($path));
            }
            //echo"<pre>";print_r($data);die;
            $restId = 0;
            if (count($data) > 0) {
                foreach ($data as $k => $row) {
                    $restId = $row['restaurant_id'];
                }
                if ($restId == $restaurant_id) DeliveryRates::where('restaurant_id', $restId)->update(['status' => 0]);
                foreach ($data as $k => $row) {
                    if ($row['restaurant_id'] == $restaurant_id)
                        $m = DeliveryRates::create($row);
                }

            } else {
                // return redirect()->back();
            }
        }
        //return redirect()->back();


    }
    public function processImport(Request $request)
    {

    }
    /**
     * @param array $columnNames
     * @param array $rows
     * @param string $fileName
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public static function getCsv($columnNames, $rows, $fileName = 'restaurantDeliveryServiceRate.csv') {
        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=" . $fileName,
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];
        $callback = function() use ($columnNames, $rows ) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columnNames);
            foreach ($rows as $row) {
                fputcsv($file, $row);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    public function downloadServiceRates(Request $request,$id) {
        $d = new DeliveryRates();
        $columnNames =$d->getTableColumns();
        unset($columnNames[0]);
        // print_r($columnNames);die;
        $rows = DeliveryRates::select($columnNames)->where('restaurant_id', $id)->where('status', 1)->orderBy('id', 'ASC')->get()->toArray();
        return self::getCsv($columnNames, $rows);
    }
    
    public function website(){
        die;
    }
    
    public function restaurantSettingMail($data){       
        $mailTemplate = "enquiryreplymail_user";              
               
        $mailFrom = Restaurant::select('restaurant_name', 'custom_from', 'support_from','restaurant_logo_name','source_url','address','facebook_url','twitter_url','pinterest_url','instagram_url','phone','street','zipcode')
                    ->where('id', $data['restaurant_id'])->get()->toArray();
        $controller = new NotificationController;

        $mailKeywords['ADDRESS'] =  $mailFrom[0]['address'];
        $mailKeywords['TITLE'] = $data['title'];
        $url = URL::to("/");

        $logo = '<img class="logo" src="'.$url.$mailFrom[0]['restaurant_logo_name'].'" alt="logo" style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:150px;outline:0;text-decoration:none;width:100%">';
        $mailKeywords['LOGO'] = $logo;       
        
        $username = 'Dear <strong>'.$mailFrom[0]['restaurant_name'].",</strong>";

        $mailKeywords['FACEBOOK_URL']=$mailFrom[0]['facebook_url'];
        $mailKeywords['TWITTER_URL']=$mailFrom[0]['twitter_url'];
        $mailKeywords['INSTAGRAM_URL']=$mailFrom[0]['instagram_url'];
        $mailKeywords['PHONE']=$mailFrom[0]['phone'];

        $mailKeywords['STREET']=$mailFrom[0]['street'];
        $mailKeywords['ZIP']=$mailFrom[0]['zipcode'];

        $mailKeywords['USER_NAME'] = $username;

        $mailKeywords['MESSAGE'] = $data['message'];
        $mailKeywords['RESTAURANT_NAME']=$mailFrom[0]['restaurant_name'];
        $config['to'] = $orderData->email;
        
        $config['subject'] = $data['title'];
        $config['from_name'] = $mailFrom[0]['restaurant_name'];
        $config['from_address'] = $mailFrom[0]['support_from'];        
        $controller->sendMail($mailTemplate, 1, $config, $mailKeywords);
        return true;
    }
}
