<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LanguagesController extends Controller
{
    /**
     * User listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $languages = Language::orderBy('id', 'DESC')->paginate(6);

        return view('languages.index', compact('languages'));
    }

    /**
     * Create Languages
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        return view('languages.create');
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'language_name' => 'required|max:255',
            'code'         => 'required|max:255'
        ]);
        if (!$validator->fails()) {
            $data = [
                'language_name' => strtolower($request->input('language_name')),
                'code'         => strtolower($request->input('code')),
                'status'        => $request->input('status'),
                'updated_at'        => now()
            ];
            //dd([$request->all(),2]);
            Language::create($data);
            return redirect('/languages')->with('message', 'Language added successfully');

        } else {
            //dd([$request->all(), '1', $validator->errors()]);

            return Redirect::back()->withErrors($validator->errors())->withInput();
        }

    }
    
    /**
     * User edit
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)

    {
        $languages = Language::find($id);
        return view('languages.edit', compact('languages'));
    }

    /**
     * User edit update
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if(isset($id) && is_numeric($id)) {
            $languages = Language::find($id);
            $validator = Validator::make($request->all(), [
                'language_name'         => 'required|max:255',
                'code'           => 'required|max:255',
            ]);

            if(!$validator->fails()) {

                $languages->language_name = $request->input('language_name');
                $languages->code = $request->input('code');
                $languages->updated_at = now();
                $languages->status = $request->input('status');
                $languages->save();

                return redirect('/languages')->with('message', 'Language updated successfully');

            } else {

                return Redirect::back()->withErrors($validator->errors())->withInput();
            }
        }

        return Redirect::back()->with('message', 'Invalid Id');
    }
}
