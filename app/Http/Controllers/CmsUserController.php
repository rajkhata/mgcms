<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Models\Language;
use App\Models\CmsUser;
use App\Models\State;
use App\Models\City;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class CmsUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:user list',['only'=>['allOrder']]);
        $this->middleware('permission:user edit', ['only' => ['releaseOrderRefund']]);
        $this->middleware('permission:user add', ['only' => ['mngorderDetail']]);
        $this->middleware('permission:user delete', ['only' => ['mngorderDetail']]);
    }

    public function index(Request $request)
    {
        $searchArr = array('restaurant_id'=>0,'email'=>NULL);
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','DESC')->get();

        $rules = ['email' => 'sometimes|nullable|email'];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $cmsUsers  = CmsUser::orderBy('id','DESC');
            if($request->input('email'))
            {
                $searchArr['email'] = $request->input('email');
                $cmsUsers->where('email',$searchArr['email']);
            }
            if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
            {
                $searchArr['restaurant_id'] = $request->input('restaurant_id');
                $cmsUsers->where('restaurant_id',$searchArr['restaurant_id']);
            }
            $cmsUsers = $cmsUsers->paginate(20);
            return view('cms_user.index', compact('cmsUsers','restaurantData','searchArr'));
        }
        return redirect()->back()->withErrors($validator->errors())->withInput();
    }

    public function create()
    {   $roles = Role::where('name', '!=', 'admin')->pluck('name','name')->all();
        $restaurantData = Restaurant::select('id','restaurant_name')->orderBy('id','ASC')->get();
        $stateData = State::select('id','state')->orderBy('id','ASC')->get();
        $cityData = array();
        //$cityData = City::select('id','city_name')->orderBy('id','DESC')->get();
        return view('cms_user.create', compact('restaurantData','stateData','cityData', 'roles'));
    }

    public function edit(Request $request,$id)
    {
        if(isset($id) && is_numeric($id)) {
            $cityData = $restaurantData = array();
            //$restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('restaurant_name', 'ASC')->get();
            $stateData = State::select('id', 'state')->orderBy('state', 'ASC')->get();
            $cmsUser = CmsUSer::find($id);
            $roles = Role::where('name', '!=', 'admin')->pluck('name','name')->all();
            $userRole = $cmsUser->roles->pluck('name','name')->all();
            if (!empty($cmsUser->state_id)) {
                $cityData = City::where('state_id', '=', $cmsUser->state_id)->orderBy('city_name', 'ASC')->get();
            }
           
            $restaurantData = Restaurant::orderBy('restaurant_name', 'ASC')->get();
          
            return view('cms_user.edit',compact('cmsUser','restaurantData','stateData','cityData', 'roles', 'userRole'));
        }
        return Redirect::back();
    }

    public function update(Request $request, $id)
    {
        if(!isset($id) || !is_numeric($id))
        {
            return redirect()->back()->with('err_msg','Invalid Id');
        }
        $cmsUserObj = CmsUser::where('id',$id)->first();
        if(empty($cmsUserObj))
        {
            return redirect()->back()->with('err_msg','Invalid Id');
        }

        $rules = [
            'name' => 'required|max:100',
            'password' => 'sometimes|nullable|max:50|min:5',
            'restaurant_id' => 'required|exists:restaurants,id',               
            'email'   => 'required|email|unique:cms_users,email,' . $cmsUserObj->id,            
            'status' => 'required|in:0,1',           
            'roles' => 'required'
        ];
        $errMsgs = [];

        $memail = NULL;

        $validator = Validator::make($request->all(),$rules,$errMsgs);
        if($validator->passes()) {           
            $cmsUserObj->name = $request->input('name');
            $cmsUserObj->restaurant_id = $request->input('restaurant_id');
            if($request->input('password'))
            $cmsUserObj->password = Hash::make($request->input('password'));
            $cmsUserObj->email = $request->input('email');
            $cmsUserObj->status = $request->input('status');
            $cmsUserObj->save();
            
            $inputRoles = $request->input('roles');
            $userRoles = [];
            foreach ($inputRoles as $inputRole) {
                if ($inputRole == 'admin') {
                    continue;
                }
                $userRoles[] = $inputRole;
            }

            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $cmsUserObj->assignRole($userRoles);
            return Redirect::back()->with('message', 'Data updated successfully');
        }

        $cityData = $restaurantData = array();
        
        $restaurantData = Restaurant::orderBy('restaurant_name', 'ASC')->get();


        if($validator->errors())
        {
            $validatorErrs = $validator->errors()->toArray();
            foreach($validatorErrs as $k => $v) {
                if (strpos($k, 'memail_tmp') !== false) {
                    $validator->errors()->add('memail', $v[0]);
                    break;
                }
            }
        }
        return redirect()->back()->withErrors($validator->errors())->withInput()->with('newCityData',$cityData)->with('newRestaurantData',$restaurantData);
    }
    public function store(Request $request)
    {
        //print_r($request->all()); die;
        $rules = [
            'name' => 'required|max:100',
            'password' => 'required|max:50|min:5',
            'restaurant_id' => 'required|exists:restaurants,id',
//            'state_id'   => 'required|exists:states,id',
//            'city_id'   => 'required|exists:cities,id',
            'email'   => 'required|email|unique:cms_users,email',
//            'mobile'   => 'required|digits:10|min:0|unique:cms_users,mobile',
//            'phone'   => 'sometimes|nullable|numeric',
            'status' => 'required|in:0,1',
//            'memail' => 'sometimes|nullable|max:50',
            'roles' => 'required'
        ];
        $errMsgs = [];
//         ['memail.*'=>'Please enter valid Emails',
//            'mobile'=>'Please enter valid 10 digits mobile'
//        ];
//        $memail = NULL;
//        if($request->memail) {
//            $request->merge(array('memail' => preg_split('~, *~', $request->memail)));
//            $rules['memail.*'] = 'sometimes|nullable|email';
//            $memail =  preg_replace('/\s+/', '', implode(', ', $request->memail));
//        }
        $validator = Validator::make($request->all(),$rules,$errMsgs);

        if($validator->passes()) {
            $cmsUserArr = [
                'restaurant_id' => $request->input('restaurant_id'),                
                'name' => $request->input('name'),
                'password' => Hash::make($request->input('password')),
                'email' => $request->input('email'),                
                'status' => $request->input('status'),                
                'api_token'=>Str::random(60)
            ];
            $cmsUserObj = CmsUser::create($cmsUserArr);
            $inputRoles = $request->input('roles');
            $userRoles = [];
            foreach ($inputRoles as $inputRole) {
                if ($inputRole == 'admin') {
                    continue;
                }
                $userRoles[] = $inputRole;
            }

            $cmsUserObj->assignRole($userRoles);
            return Redirect::back()->with('message', 'User added successfully');
        }
        if($request->memail) {
            $request->merge(array('memail' => implode(', ', $request->memail)));
        }
        $cityData = $restaurantData = array();
        if(Input::get('state_id') && is_numeric(Input::get('state_id')))
        {
            $cityData = City::where('state_id', '=', Input::get('state_id'))->orderBy('city_name', 'ASC')->get();
        }
        if(Input::get('city_id') && is_numeric(Input::get('city_id')))
        {
            $restaurantData = Restaurant::where('city_id', '=', Input::get('city_id'))->orderBy('restaurant_name', 'ASC')->get();
        }
        return redirect()->back()->withErrors($validator->errors())->withInput()->with('newCityData',$cityData)->with('newRestaurantData',$restaurantData);
    }

    public function get_cities_by_state(Request $request)
    {
        $sid = $request->input('state_id');
        $result = [];
        if(!empty($sid) && is_numeric($sid)) {
            $result = City::where('state_id', '=', $sid)->select('id','city_name')->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }

    public function get_restaurant_by_city(Request $request)
    {
        $cid = $request->input('city_id');
        if(!empty($cid) && is_numeric($cid)) {
            $result = Restaurant::where('city_id', '=', $cid)->select('id','restaurant_name','zipcode')->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }
    public function destroy($id) {
        DB::table("cms_users")->where('id', $id)->delete();
        return redirect()->route('cms_user.index')
            ->with('success', 'CMS User deleted successfully');
    }

}
