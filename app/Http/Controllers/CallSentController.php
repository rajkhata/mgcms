<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserOrder;


class CallSentController extends Controller
{
    
     public function sentCall()
    { 
       
        $orderData = [];
        
        #########
        #   Table : restaurants
        #   sent_call
        #   sent_fax
        #   call_interval
        #   
        #   Table : user_orders
        #   is_call_sent
        #   is_fax_sent
       
        
        
        #########
        #   ALTER TABLE `restaurants`  ADD `sent_call` TINYINT NOT NULL DEFAULT '0' COMMENT 'automated call'  AFTER `updated_at`,  ADD `sent_fax` TINYINT NOT NULL DEFAULT '0' COMMENT 'efax'  AFTER `sent_call`,  ADD `call_interval` INT NOT NULL DEFAULT '0' COMMENT 'automated call interval'  AFTER `sent_fax`;
        
        #########
        #   ALTER TABLE `user_orders`  ADD `is_call_sent` TINYINT NOT NULL DEFAULT '0'  AFTER `pos_status`,  ADD `is_fax_sent` TINYINT NOT NULL DEFAULT '0'  AFTER `is_call_sent`;
        
        $orderData = UserOrder::select('user_orders.id','user_orders.restaurant_id','user_orders.created_at','restaurants.phone','restaurants.call_interval')
        ->where('user_orders.status', 'placed')->where('restaurants.sent_call',1)->where('user_orders.is_call_sent',0)
        ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')->get()->toArray();                     
        if(isset($orderData[0]) && !empty($orderData[0])){
            
            foreach($orderData as $key => $value){
                $currentDate = \App\Helpers\CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $value['restaurant_id']));
                
                $currentTimestamp =  strtotime($currentDate->format('Y-m-d H:i:s'));
                $orderTimestamp = strtotime($value['created_at']);
                $interval = $value['call_interval']*60;
               
                $sentCallTime = $orderTimestamp+$interval;
               
                if($sentCallTime <= $currentTimestamp){  
                    $data['phone_number'] = $value['phone'];
                    $response = $this->sendCall($data); 
                    $search = "SUCCESS";
                    if(preg_match("/{$search}/i", $response)) {
                       $this->updateCallSent($value['id']);
                       echo $response;
                    }else{
                        echo "Call not sent : Please contact the administrator";
                    }
                    
                    die;
                }else{
                    echo "Wait for time";
                }
            }
        }
            
        die;

    }
    
    public function sendCall($data){
        //print_r($data['phone_number']);
       // http://173.234.25.90/munchado/munchado_api.php?key=873279875SDHG65765214&phone_code=1&phone_number=3474535994        
        //http://173.234.25.90/munchado/munchado_api.php?key=873279875SDHG65765214&phone_code=1&phone_number=3474535994&source=test&user=146738&pass=test1q2w3e&function=add_lead&list_id=7545689075
        $key = env('CALL_SENT_KEY', '873279875SDHG65765214');
        $url= "http://173.234.25.90/munchado/munchado_api.php?key=".$key;
        $data = "&phone_number=".$data['phone_number']."&phone_code=1&list_id=7545689075";
        //echo $url.$data;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.$data ); //Url together with parameters
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($ch, CURLOPT_HEADER, 0);    
        $result = curl_exec($ch);
        curl_close($ch);       
        return $result;
        
    }
    
    public function updateCallSent($orderId){
        UserOrder::where('id', $orderId)->update(array("is_call_sent"=>1));
        echo "OrderId".$orderId."######";
        return true;
    }
}