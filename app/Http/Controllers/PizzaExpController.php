<?php

namespace App\Http\Controllers;

use App\Models\BuildYourOwnPizza;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;

class PizzaExpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$pizzaSetting = config('constants.pizza_setting');
        $pizzaSettings = BuildYourOwnPizza::select('build_your_own_pizzas.*', 'restaurants.restaurant_name')
                        ->join('restaurants', 'restaurants.id','=','build_your_own_pizzas.restaurant_id');
                        //->whereRaw('json_contains(build_your_own_pizzas.crust, \'{"name": "simple"}\')');
        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
        $pizzaSettings = $pizzaSettings->paginate(20);

        return view('pizzaexp.index', compact('pizzaSettings', 'restaurantData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*$setting        = Input::get('setting');
        $allSettings    = config('constants.pizza_setting');
        $correctSetting = $this->findSimilarMatch($setting, $allSettings);
        if ($setting !== $correctSetting) {
            return redirect('pizzaexp/create?setting=' . $correctSetting);
        }*/

        // Get all restaurant_ids
        $allRests = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
	$groupRestData = Restaurant::group();
        return view('pizzaexp.create', compact('allRests', 'groupRestData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post')) {

            // static content validation
            $request->validate([
                'restaurant_id'     => 'required|exists:restaurants,id',
                'label'             => 'required|max:255',
                'priority'          => 'required|numeric',
                'is_multiselect'    => 'sometimes|in:0,1',
                'is_customizable'   => 'sometimes|in:0,1',
                'is_preference'     => 'sometimes|in:0,1',
                'status'            => 'required|in:0,1'
            ]);

            $pizzaSetting = [
                'restaurant_id'     => $request->input('restaurant_id'),
                'parent_id'         => $request->input('parent_setting') ?? 0,
                'label'             => strtolower($request->input('label')),
                'status'            => $request->input('status'),
                'priority'          => $request->input('priority'),
                'is_multiselect'    => $request->input('is_multiselect') ?? 0,
                'is_customizable'   => $request->input('is_customizable') ?? 0,
                'is_preference'     => $request->input('is_preference') ?? 0
            ];
            //dd([$request->all(), $pizzaSetting]);

            $pizzaExp                 = BuildYourOwnPizza::create($pizzaSetting);
            return Redirect::back()->with('message', 'Pizza Setting added successfully');
        }

        return redirect('pizzaexp');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get PizzaExp setting
        $pizzaSetting = BuildYourOwnPizza::where('id',$id)->first();

        // Get all restaurant_ids
        $allRests = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();

        // Get all Restaurant Pizza settings
        $parentSettingArr = BuildYourOwnPizza::select('id', 'label')->where(['restaurant_id' => $pizzaSetting->restaurant_id, 'status' => 1])->get();
	$groupRestData = Restaurant::group();
        return view('pizzaexp.edit', compact('pizzaSetting','allRests', 'parentSettingArr', 'groupRestData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($id) && is_numeric($id)) {

            // static content validation
            $request->validate([
                'restaurant_id'     => 'required|exists:restaurants,id',
                'label'             => 'required|max:255',
                'priority'          => 'required|numeric',
                'is_multiselect'    => 'sometimes|in:0,1',
                'is_customizable'   => 'sometimes|in:0,1',
                'is_preference'     => 'sometimes|in:0,1',
                'status'            => 'required|in:0,1'
            ]);

            $pizzaExp                   = BuildYourOwnPizza::find($id);
            $pizzaExp->restaurant_id    = $request->input('restaurant_id');
            $pizzaExp->parent_id        = $request->input('parent_setting') ?? 0;
            $pizzaExp->label            = strtolower($request->input('label'));
            $pizzaExp->status           = $request->input('status');
            $pizzaExp->priority         = $request->input('priority');
            $pizzaExp->is_multiselect   = $request->input('is_multiselect') ?? 0;
            $pizzaExp->is_customizable  = $request->input('is_customizable') ?? 0;
            $pizzaExp->is_preference    = $request->input('is_preference') ?? 0;
            $pizzaExp->save();

            return Redirect::back()->with('message', 'Pizza Setting updated successfully');
        }

        return redirect('pizzaexp');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pizzaSetting = BuildYourOwnPizza::find($id);

        if(!empty($pizzaSetting)) {
            $pizzaSetting->delete();
            return Redirect::back()->with('message', 'Menu Item deleted successfully');
        }
        else
            return Redirect::back()->with('err_msg', 'Invalid Item');
    }

    private function findSimilarMatch($input, $haystackArr) {
        // no shortest distance found, yet
        $shortest = -1;
        // loop through words to find the closest
        foreach ($haystackArr as $word) {

            // calculate the distance between the input word,
            // and the current word
            $lev = levenshtein($input, $word);

            // check for an exact match
            if ($lev == 0) {

                // closest word is this one (exact match)
                $closest  = $word;
                $shortest = 0;

                // break out of the loop; we've found an exact match
                break;
            }

            // if this distance is less than the next found shortest
            // distance, OR if a next shortest word has not yet been found
            if ($lev <= $shortest || $shortest < 0) {
                // set the closest match, and shortest distance
                $closest  = $word;
                $shortest = $lev;
            }
        }

        return $closest;
    }

    public function getRestaurantSettings(Request $request)
    {
        $restId = $request->input('rid');
        $pizzaSettings = BuildYourOwnPizza::select('id', 'label','setting')->where(['restaurant_id' => $restId, 'status' => 1])->get()->toArray();

        if($pizzaSettings){
            return response()->json(array('dataObj' => $pizzaSettings), 200);
        }

        return response()->json(array('err' => 'Invalid Data'), 200);
    }
}
