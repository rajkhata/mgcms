<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use App\Helpers\CommonFunctions;

class CmsActivityLogsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct() {
        $this->middleware('auth');
        $this->middleware('permission:cms user logs');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $logs = DB::table("log_cmsuser_action")->whereIn('cms_user_id',$restArray)->get()->toArray();   
        $cmsuser = [];
        $action=[];
        if($logs){
            foreach($logs as $key => $log){
              
                $cmsuser = ['user_id'=>$log->cms_user_id,'name'=>$log->cms_user_name,'email'=>$log->cms_user_email];
                $action[][$log->created_at] = [
                    'action'=>$log->action,
                    'action_name'=>$log->action_name,
                    'method'=>$log->method,
                    'ip_address'=>$log->ip_address,
                    'user_agent'=>json_decode($log->user_agent,1), 
                    'activity'=>json_decode($log->activity,1)
                    ];
               
            }
        }
       
        return view('cms_activity_logs.show', compact('cmsuser','action'));
    }

   


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table("log_cmsuser_action")->where('cms_user_id', $id)->delete();
        
        return redirect()->route('user_action_log.index')
            ->with('success', 'Logs deleted successfully');
    }
   
}
