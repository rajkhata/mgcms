<?php

namespace App\Http\Controllers;
use URL;
use App\Models\GiftcardCoupons;
use App\Models\PaymentGatewayLog;
use App\Models\UserOrder;
use App\Models\Language;
use App\Models\Localization;
use App\Models\UserOrderDetail;
use App\Models\UserOrderAddons;
use App\Models\OrderStatuses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Helpers\CommonFunctions;
use App\Models\Restaurant;
use App\Http\Controllers\NotificationController;
use Modules\Reservation\Entities\Reservation;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\TimeslotController;
use App\Helpers\LocationWorkingDay;
use App\Helpers\DeliveryServiceProvider;
use App\Models\DeliveryService;
use App\Models\MenuItem;
use App\Models\CmsUserLog;
use Stripe\Order;
use App\Helpers\ApiPayment;
use App\Models\DeliveryServicesLogs;
use App\Helpers\Twinjet;
use App\Helpers\Payment\StripeGateway;
use App\Helpers\Payment\BraintressGateway;
use App\Models\PaymentGateway;
use App\Models\EnquirySendMailLog;
use App\Helpers\Stuarts;
use App\Models\StuartOrders;
use App\Models\StuartOrdersLog;

class UserOrderController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('auth', ['except' => ['orderUpdateArchive']]);
        $this->middleware('permission:order-list', ['only' => ['allOrder']]);
      //  $this->middleware('permission:mng_orderdetails', ['only' => ['mng_orderdetails']]);
        $this->middleware('permission:refund', ['only' => ['releaseOrderRefund']]);
        $this->middleware('permission:order-view', ['only' => ['mngorderDetail']]);
    }

    /**
     * Active orders listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * food_item for all general products
     */
    public function index(Request $request)
    {
        //print_r($request->all); die;

        $orderData = [];
        $statusKey = '';
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        //print_r($restArray); die;
        $statusArray = array("placed", "confirmed", "ready");
        $orderStatus = array("" => "Active Orders", "placed" => "Placed", "confirmed" => "Confirmed", "ready" => "Ready");
        $search_type = array("delivery_datetime" => "Delivery Time", "order_datetime" => "Order Time", 'confirmed' => 'Confirmed', 'placed' => 'Unconfirmed');
        if ($request->input('order_status')) {
            $statusArray = [];
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }


        $orderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.email', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at',
            'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update', 'user_orders.status', 'user_orders.product_type', 'restaurants.restaurant_name', 'restaurants.phone as rphone', 'restaurants.rest_code', 'restaurants.address as raddress', 'restaurants.zipcode as rzipcode', 'restaurants.street as rstreet', 'user_orders.address', 'is_guest', 'user_orders.address2', 'user_orders.state', 'user_orders.zipcode', 'address_label', DB::raw('COUNT(user_order_details.id) AS num_item'), DB::raw('CONCAT(user_orders.delivery_date, " ", user_orders.delivery_time) AS delivery_datetime'), DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE) as order_delivery_fdatetime'))
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
            ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
            ->groupBy('user_orders.id', 'user_orders.user_id', 'user_orders.order_type', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.total_amount', 'user_orders.created_at',
                'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update', 'user_orders.status', 'user_orders.product_type');
        $orderData = $orderData->where('user_orders.product_type', '=', "food_item");
        if ($request->has('key_search')) {
            $keySearch = $request->input('key_search');
            $orderData = $orderData->where(function ($query) use ($keySearch) {
                $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.email', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(user_orders.fname," ",user_orders.lname)'), 'LIKE', '%' . $keySearch . '%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%')
                    ->orWhere('users.email', 'like', '%' . $keySearch . '%')
                    ->orWhere('users.mobile', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(users.fname," ",users.lname)'), 'LIKE', '%' . $keySearch . '%');
                if (is_numeric($keySearch)) {
                    $query->orWhere('user_orders.id', $keySearch);
                }
            });
            $statusArray = array("placed", "confirmed", "ready", "archived", "cancelled", "arrived", "sent", "rejected", "refunded");
        } else {
            $orderData = $orderData->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)'), '>=', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)'));
        }
        $orderData = $orderData->whereIn('user_orders.status', $statusArray);

        $default_sort_order = 'delivery_datetime';
        if ($request->has('sortorder') && $request->input('sortorder') && $request->input('sortorder') != '') {
            if ($request->input('sortorder') == 'order_datetime') {
                $default_sort_order = 'order_datetime';
            } elseif (is_numeric($request->input('sortorder'))) {
                $orderData = $orderData->where('user_orders.restaurant_id', $request->input('sortorder'));
            } else {

                $default_sort_order = $request->input('sortorder');
            }
        }
        /*************************************************delivery time + 12 hour <=current time****************************************************************************************/
        // $orderData = $orderData->whereRaw('DATE_ADD(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL 12 HOUR)>CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
        //Remove logic as per Deepak sir
        /*************************************************delivery time + 12 hour <=current time****************************************************************************************/

        if ($default_sort_order == 'delivery_datetime') {
            $orderData = $orderData->orderBy('delivery_datetime', 'ASC');
        } else if ($default_sort_order == 'confirmed' || $default_sort_order == 'placed') {
            $orderData = $orderData->orderByRaw("FIELD(user_orders.status, '" . $default_sort_order . "') ASC");

        } else {
            $orderData = $orderData->orderBy('user_orders.created_at', 'ASC');
        }


        $orderData = $orderData->paginate(10);

        $groupRestData = CommonFunctions::getRestaurantGroup();
        $default_sort_order = $request->input('sortorder');

        // echo "<pre>";
        // print_r($groupRestData);  print_r($search_type); die;

        return response()->view('user_order.index', compact('orderData', 'orderStatus', 'statusKey', 'search_type', 'default_sort_order', 'groupRestData'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    }

    /**
     * Active orders listing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function archiveOrder(Request $request)
    {
        $orderData = [];
        $statusKey = '';
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $statusArray = array("archived", "cancelled", "arrived", "sent", "rejected", "refunded");
        $orderStatus = array("" => "All Archived", "archived" => "Archived", "cancelled" => "Cancelled", "rejected" => "Rejected", "refunded" => "Refunded");
        if ($request->input('order_status')) {
            $statusArray = [];
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }

        $search_type = array("delivery_datetime" => "Delivery Time", "order_datetime" => "Order Time");
        $orderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.email', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at', 'restaurants.restaurant_name', 'restaurants.phone as rphone', 'restaurants.rest_code', 'restaurants.address as raddress', 'restaurants.zipcode as rzipcode', 'restaurants.street as rstreet', 'user_orders.address', 'is_guest', 'user_orders.address2', 'user_orders.state', 'user_orders.zipcode', 'address_label',
            'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type', DB::raw('COUNT(user_order_details.id) AS num_item'), DB::raw('CONCAT(user_orders.delivery_date, " ", user_orders.delivery_time) AS delivery_datetime'), DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE) as order_delivery_fdatetime'))
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
            ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
            ->groupBy('user_orders.id', 'user_orders.user_id', 'user_orders.order_type', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.total_amount', 'user_orders.created_at', 'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type');
        $orderData = $orderData->where('user_orders.product_type', '=', "food_item");

        if ($request->has('key_search')) {
            $keySearch = $request->input('key_search');
            $orderData = $orderData->where(function ($query) use ($keySearch) {
                $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.email', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(user_orders.fname," ",user_orders.lname)'), 'LIKE', '%' . $keySearch . '%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%')
                    ->orWhere('users.email', 'like', '%' . $keySearch . '%')
                    ->orWhere('users.mobile', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(users.fname," ",users.lname)'), 'LIKE', '%' . $keySearch . '%');
                if (is_numeric($keySearch)) {
                    $query->orWhere('user_orders.id', $keySearch);
                }
            });
            $statusArray = array("placed", "confirmed", "ready", "archived", "cancelled", "arrived", "sent", "rejected", "refunded");
        }
        $orderData = $orderData->whereIn('user_orders.status', $statusArray);

        /*************************************************delivery time + 12 hour <=current time****************************************************************************************/
        /* $orderData = $orderData->where(function ($query)use($statusArray) {
                    $query->whereIn('user_orders.status', $statusArray)
                        ->orWhereRaw('DATE_ADD(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL 12 HOUR)<=CONVERT_TZ(now(),"+00:00", cities.timezone_value)');
                });*/
        //Remove logic as per Deepak sir

        // $orderData = $orderData->orWhereRaw('DATE_ADD(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL 12 HOUR)<=NOW()');
        /*************************************************delivery time + 12 hour <=current time****************************************************************************************/

        $default_sort_order = 'updated_at';
        if ($request->has('sortorder') && $request->input('sortorder') && $request->input('sortorder') != '') {
            if ($request->input('sortorder') == 'order_datetime') {
                $default_sort_order = 'order_datetime';
            } elseif (is_numeric($request->input('sortorder'))) {
                $orderData = $orderData->where('user_orders.restaurant_id', $request->input('sortorder'));
            }
        }
        if ($default_sort_order == 'updated_at') {
            $orderData = $orderData->orderBy('user_orders.updated_at', 'DESC');
        } else {
            $orderData = $orderData->orderBy('user_orders.id', 'DESC');
        }
        $orderData = $orderData->paginate(10);
        $groupRestData = CommonFunctions::getRestaurantGroup();
        $default_sort_order = $request->input('sortorder');
        return view('user_order.archive', compact('orderData', 'orderStatus', 'statusKey', 'search_type', 'default_sort_order', 'groupRestData'));
    }

    /* Title: Products order archive list
    Date: 20-09-2018
*/
    public function productArchiveOrder(Request $request)
    {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $statusArray = array("archived", "cancelled", "arrived", "sent", "rejected", "refunded");
        $orderStatus = array("" => "All Archived", "archived" => "Archived", "cancelled" => "Cancelled", "rejected" => "Rejected", "refunded" => "Refunded");
        $statusKey = '';
        if ($request->input('order_status')) {
            $statusArray = [];
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }

        $productArchiveOrderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.address', 'user_orders.phone', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at',
            'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type', DB::raw('COUNT(user_order_details.id) AS num_item'), 'user_order_details.item')
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->groupBy('user_orders.id', 'user_orders.user_id', 'user_orders.address', 'user_orders.phone', 'user_orders.order_type', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.total_amount', 'user_orders.created_at', 'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type');

        $productArchiveOrderData = $productArchiveOrderData->where('user_orders.product_type', '=', "product");
        if ($request->input('key_search')) {
            $keySearch = $request->input('key_search');
            $productArchiveOrderData = $productArchiveOrderData->where(function ($query) use ($keySearch) {
                $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.lname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(fname," ",lname)'), 'LIKE', '%' . $keySearch . '%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%');
                if (is_numeric($keySearch)) {
                    $query->orWhere('user_orders.id', $keySearch);
                }
            });

        }
        $productArchiveOrderData = $productArchiveOrderData->paginate(10);
        return view('user_order.product-archive', compact('productArchiveOrderData', 'orderStatus', 'statusKey'));

    }

    /* Product orders list
     * Date: 18-09-2018
     * */

    public function productOrders(Request $request)
    {

        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $statusArray = array("placed", "confirmed", "ready");
        $statusKey = '';
        $orderStatus = array("" => "Active Orders", "placed" => "Placed", "confirmed" => "Confirmed", "refunded" => "Refunded");
        if ($request->input('order_status')) {
            $statusArray = [];
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }

        $keySearch = $request->input('key_search');

        $productOrderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.address', 'user_orders.phone', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at',
            'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update', 'user_orders.status', 'user_orders.product_type', DB::raw('COUNT(user_order_details.id) AS num_item'), 'user_order_details.item',
            'users.fname AS user_fname', 'users.lname AS user_lname', 'users.mobile AS user_mobile', 'users.phone AS user_phone')
            ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->groupBy('user_orders.id', 'user_orders.user_id', 'user_orders.address', 'user_orders.phone', 'user_orders.order_type', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.total_amount', 'user_orders.created_at',
                'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update', 'user_orders.status', 'user_orders.product_type');

        //$productOrderData = $productOrderData->whereNotNull('user_order_details.item_other_info');
        $productOrderData = $productOrderData->where('user_orders.product_type', '=', "product");
        if (!empty($keySearch)) {
            $productOrderData = $productOrderData
                ->when('user_orders.user_id', function ($query, $sortBy) use ($keySearch) {
                    return $query->where('users.fname', 'like', '%' . $keySearch . '%')
                        ->orWhere('users.lname', 'like', '%' . $keySearch . '%')
                        ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                        ->orWhere(DB::raw('concat(users.fname," ",users.lname)'), 'LIKE', '%' . $keySearch . '%')
                        ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%')
                        ->orWhere('user_orders.id', '%' . $keySearch . '%');
                }, function ($query) use ($keySearch) {
                    return $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                        ->orWhere('user_orders.lname', 'like', '%' . $keySearch . '%')
                        ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                        ->orWhere(DB::raw('concat(user_orders.fname," ",user_orders.lname)'), 'LIKE', '%' . $keySearch . '%')
                        ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%')
                        ->orWhere('user_orders.id', '%' . $keySearch . '%');
                });

            /*$productOrderData = $productOrderData->where(function ($query) use ($keySearch){
                $query->where('users.fname', 'like', '%'.$keySearch.'%')
                    ->orWhere('users.lname', 'like', '%'.$keySearch.'%')
                    ->orWhere('user_orders.phone', 'like', '%'.$keySearch.'%')
                    ->orWhere(DB::raw('concat(users.fname," ",users.lname)') , 'LIKE' , '%'.$keySearch.'%')
                    ->orWhere('user_orders.fname', 'like', '%'.$keySearch.'%')
                    ->orWhere('user_orders.lname', 'like', '%'.$keySearch.'%')
                    ->orWhere('user_orders.phone', 'like', '%'.$keySearch.'%')
                    ->orWhere(DB::raw('concat(user_orders.fname," ",user_orders.lname)') , 'LIKE' , '%'.$keySearch.'%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%'.$keySearch.'%')
                ->orWhere('user_orders.id', '%'.$keySearch.'%');
            });*/

        }
        // dd($productOrderData->get()->toArray());
        $productOrderData = $productOrderData->paginate(10);

        // echo "<pre>";
        // print_r($productOrderData->toArray()); die;
        return view('user_order.product-order', compact('productOrderData', 'orderStatus', 'statusKey'));


    }

    /**
     *  order details
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function orderDetail($id, $action)
    {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $orderData = UserOrder::where('id', $id)->with('user')->whereIn('restaurant_id', $restArray)->where('product_type', '=', $action)->first();

        //$orderData = $orderData->where('product_type', '=', $action);
        if (empty($orderData)) {
            return Redirect::back()->with('message', 'Invalid Id');
        } else {
            //update is_order_view=1 as per new design @18-03-2019 RG
            UserOrder::where('id', $id)->update(['is_order_viewed' => 1]);

            $total_refunded_amount = 0;
            $refund_reason = array();
            $refunded_amount_data = DB::table('refund_histories')->where('order_id', $id)->select('amount_refunded', 'reason')->get();
            if ($refunded_amount_data) {
                foreach ($refunded_amount_data as $key => $amount_data) {
                    $total_refunded_amount = $total_refunded_amount + $amount_data->amount_refunded;
                    $refund_reason[$key] = array('amount' => $amount_data->amount_refunded, 'reason' => $amount_data->reason);
                }
            }

            $userId = $orderData->user_id;
            $emailId = $orderData->email;
            $locationId = $orderData->restaurant_id;

            $orderDetail = new UserOrderDetail();
            $UserOrderAddons = new UserOrderAddons();
            $userOrder = new UserOrder();
            $userReserve = new Reservation();

            #@23-11-18 RG if user_id is there then check with user_id else with email id if Guest User and if not registered
            $userOrders = $userOrder->whereIn('restaurant_id', $restArray)->whereNotIn('status', ['pending']);
            if ($userId) {
                $userOrders = $userOrders->where('user_id', $userId);
            } else {
                $userOrders = $userOrders->where('email', $emailId);
            }
            $userOrders = $userOrders->get()->count();

            $status = $orderDate = "";
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
            $orderDate = date('l d M, h:i A', $timestamp2);

            $restaurantDetails = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);

            $result = [];
            $timeSlotData = '';
            $timeSlotData = CommonFunctions::getTime_Slot($orderData->delivery_time);
            $result = CommonFunctions::getDateSlot();

            $status = $orderData->status;

            if ($status == "placed")
                $status = "confirmed";

            else if ($status == "confirmed" && $orderData->order_type == "delivery")
                $status = "sent";

            else if ($status == "confirmed" && $orderData->order_type == "carryout")
                $status = "ready";

            else if ($status == "sent")
                $status = "archived";

            else if ($status == "ready")
                $status = "archived";

            else if ($status == "archived")
                $status = "archiv";

            else if ($status == "rejected")
                $status = "rejected";

            else if ($status == "cancelled")
                $status = "cancelled";

            $userReservation = $userReserve->getReservationByUser($userId, $emailId, $restArray);

            $orderDetailData = $orderDetail->getOrderDetail($id);
            $addOnsDetail = [];

            $i = 0;
            $addonsData = [];
            foreach ($orderDetailData as $key => $data) {
                $addon_modifier = CommonFunctions::getAddonsAndModifiers($data);
                $orderDetailData[$key]['addon_modifier'] = $addon_modifier;
                $addons_data = [];
                $bypData = json_decode($data->menu_json, true);
                //echo $data->is_byp;print_r($bypData); die;
                //@change By RG 29-10-2018 PE-2670
                if (!is_null($bypData)) {
                    $addons_data = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                    //print_r($addons_data); die;
                    //\Log::info($addons_data);
                    $addonsData[$data->id] = $addons_data;
                    $orderDetailData[$key]['addOnsDetail'] = $addons_data;

                } else {
                    //@change By RG 29-10-2018 PE-2670
                    // $addons_data[] = [
                    //     'addons_name' => $data->item,
                    //     'price' => $data->unit_price,
                    // ];
                    // $orderDetailData[$key]['addOnsDetail'] = $addons_data;
                }
            }

            $orderData->created_at = date('Y-m-d H:i:s', strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at))));

            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $orderData->restaurant_id));

            if (is_object($currentDateTimeObj)) {
                $currentTimestamp = strtotime($currentDateTimeObj->format('Y-m-d H:i:s'));
                $currentTime = $currentDateTimeObj->format('H:i');
                $currentDate = $currentDateTimeObj->format('l, M d, Y');
            } else {
                $currentTimestamp = strtotime(date('Y-m-d H:i:s'));
                $currentTime = date('H:i');
                $currentDate = date('l, M d, Y');
            }
            $deliveryTimestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');

            $order_del_time = ($deliveryTimestamp - $restaurantDetails['kpt'] * 60);
            $is_order_scheduled = 0;
            if ($currentTimestamp < $order_del_time) {
                $is_order_scheduled = 1;
            }

            //@RG 08-10-2018 Show Log button when Order is in archived:
            $show_log_btn = false;
            if (in_array($orderData->status, array("archived", "cancelled", "arrived", "sent", "rejected", "refunded"))) {
                $show_log_btn = true;
            }

            //echo "<pre>";print_r($orderDetailData->toArray());die;
            if (Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
            } else {
                $curSymbol = config('constants.currency');
            }
            $order_other_details = $data = CommonFunctions::getOrderCharges($orderData, $curSymbol);
          //  return  json_encode(array('orderData'=>$orderData, 'orderDetailData'=>$orderDetailData, 'userOrders'=>$userOrders, 'userReservation'=>$userReservation, 'addOnsDetail'=>$addOnsDetail, 'status'=>$status, 'orderDate'=>$orderDate, 'timeSlotData'=>$timeSlotData, 'result'=>$result, 'restaurantDetails'=>$restaurantDetails, 'currentTimestamp'=>$currentTimestamp, 'deliveryTimestamp'=>$deliveryTimestamp, 'action'=>$action, 'show_log_btn'=>$show_log_btn, 'currentTime'=>$currentTime, 'currentDate'=>$currentDate, 'total_refunded_amount'=>$total_refunded_amount, 'refund_reason'=>$refund_reason, 'is_order_scheduled'=>$is_order_scheduled, 'order_other_details'=>$order_other_details));
           return view('user_order.details', compact('orderData', 'orderDetailData', 'userOrders', 'userReservation', 'addOnsDetail', 'status', 'orderDate', 'timeSlotData', 'result', 'restaurantDetails', 'currentTimestamp', 'deliveryTimestamp', 'action', 'show_log_btn', 'currentTime', 'currentDate', 'total_refunded_amount', 'refund_reason', 'is_order_scheduled', 'order_other_details'));
        }
    }

    /**
     * Create user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function create()
    {

        $languages = Language::select('id', 'language_name')->get();
        //dd($languages);
        return view('localization.create', compact('languages'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required|max:255',
            'value' => 'required|max:255'
        ]);
        if (!$validator->fails()) {
            $data = [
                'lang_id' => $request->input('lang_id'),
                'key' => $request->input('key'),
                'value' => $request->input('value'),
                'platform' => $request->input('platform'),
                'updated_at' => now(),
                'status' => $request->input('status')
            ];

            Localization::create($data);
            return redirect('/localization')->with('message', 'Localization added successfully');
        } else {

            return Redirect::back()->withErrors($validator->errors())->withInput();
        }

    }

    /**
     * User edit
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $localize = Localization::find($id);
        $languages = Language::select('id', 'language_name')->get();

        return view('localization.edit', compact('localize', 'languages'));
    }

    public function submitOrderToVendor($orderData, $restaurantData1, $isCreate = 1, $provider_id)
    {
        $res = null;
        try {
            if (isset($restaurantData1['delivery_provider_apikey'])) {
                $deliverServiceProvider = new Twinjet(['apikey' => $restaurantData1['delivery_provider_apikey']]);

                if ($isCreate == 1) { //create job
                    $city = DB::table('cities')->where('id', $restaurantData1['city_id'])->first();
                    if ($city && $city->time_zone != '') {
                        $restaurantData1['city'] = $city->city_name;
                        $restaurantData1['state'] = $city->state_code;
                        $restaurantData1['time_zone'] = $city->time_zone;
                        $res = $deliverServiceProvider->createOrderTwinjet($orderData, $restaurantData1);
                        $deliveryServiceLogObj = new DeliveryServicesLogs();
                        $deliveryServiceLogObj->provider_id = $provider_id;
                        $deliveryServiceLogObj->order_id = $orderData['id'];
                        $deliveryServiceLogObj->order_source_id = 0;
                        if (isset($res['response_data']['request_id']))
                            $deliveryServiceLogObj->order_receipt = $res['response_data']['request_id'];
                        $deliveryServiceLogObj->restaurant_id = $restaurantData1['id'];
                        $deliveryServiceLogObj->order_key = $orderData['id'];
                        $deliveryServiceLogObj->response_data = (string)json_encode($res['response_data']);
                        $deliveryServiceLogObj->request_data = (string)$res['request_data'];
                        $deliveryServiceLogObj->updated_at = date("Y-m-d H:i:s");
                        $deliveryServiceLogObj->created_at = date("Y-m-d H:i:s");
                        $deliveryServiceLogObj->save();
                    }
                } elseif ($isCreate == 2) { //delete job
                    $log = DB::table('delivery_services_logs')->where('order_id', $orderData['id'])->orderBy('delivery_services_logs.id', 'DESC')->whereNotNull('order_receipt')->first();
                    if ($log) {
                        $deliveryServiceLogObj = DeliveryServicesLogs::find($log->id);
                        $res = $deliverServiceProvider->canceledOrderTwinjet($log->order_receipt);
                        $deliveryServiceLogObj = new DeliveryServicesLogs();
                        $deliveryServiceLogObj->provider_id = $provider_id;
                        $deliveryServiceLogObj->order_id = $orderData['id'];
                        $deliveryServiceLogObj->order_source_id = 0;
                        if (isset($res['response_data']['request_id']))
                            $deliveryServiceLogObj->order_receipt = $res['response_data']['request_id'] . "- Cancel";
                        $deliveryServiceLogObj->restaurant_id = $restaurantData1['id'];
                        $deliveryServiceLogObj->order_key = $orderData['id'];
                        $deliveryServiceLogObj->response_data = (string)json_encode($res['response_data']);
                        $deliveryServiceLogObj->request_data = (string)json_encode($res['request_data']);
                        $deliveryServiceLogObj->updated_at = date("Y-m-d H:i:s");
                        $deliveryServiceLogObj->created_at = date("Y-m-d H:i:s");
                        $deliveryServiceLogObj->save();
                    }
                } else {    //edit job
                    $city = DB::table('cities')->where('id', $restaurantData1['city_id'])->first();
                    if ($city && $city->time_zone != '') {
                        $restaurantData1['city'] = $city->city_name;
                        $restaurantData1['state'] = $city->state_code;
                        $restaurantData1['time_zone'] = $city->time_zone;

                        $log = DB::table('delivery_services_logs')->where('order_id', $orderData['id'])->orderBy('delivery_services_logs.id', 'DESC')->whereNotNull('order_receipt')->first();
                        if ($log) {
                            $deliveryServiceLogObj = DeliveryServicesLogs::find($log->id);
                            $res = $deliverServiceProvider->editOrderTwinjet($orderData, $restaurantData1, $log->order_receipt);
                            $deliveryServiceLogObj->provider_id = $provider_id;
                            $deliveryServiceLogObj->order_id = $orderData['id'];
                            $deliveryServiceLogObj->order_source_id = 0;
                            if (isset($res['response_data']['request_id']))
                                $deliveryServiceLogObj->order_receipt = $res['response_data']['request_id'];
                            $deliveryServiceLogObj->restaurant_id = $restaurantData1['id'];
                            $deliveryServiceLogObj->order_key = $orderData['id'];
                            $deliveryServiceLogObj->response_data = (string)json_encode($res['response_data']);
                            $deliveryServiceLogObj->request_data = (string)$res['request_data'];
                            $deliveryServiceLogObj->updated_at = date("Y-m-d H:i:s");
                            $deliveryServiceLogObj->created_at = date("Y-m-d H:i:s");
                            $deliveryServiceLogObj->save();

                        } else {
                            $orderData['item_list'][] = array();

                            $this->submitOrderToVendor($orderData, $restaurantData1, 1, $provider_id);
                        }

                    }
                }
            }
        } catch (Excption $e) {
            $mailKeywords = array(
                'header' => array(),
                'footer' => array(),
                'data' => $mailKeywords
            );

            $mailData['subject'] = "Twinjet API error order->id" . $orderData['id'];
            $mailData['body'] = "Twinjet Error";
            $mailData['receiver_email'] = 'ashokbhati.php@gmail.com';
            $mailData['receiver_name'] = "Twinjet Error";
            $mailData['MAIL_FROM_NAME'] = "Bar Cargo";
            $mailData['MAIL_FROM'] = "ashokbhati.php@gmail.com";
            CommonFunctions::sendMail($mailData);
        }
        return $res;
    }

    public function syncTwinJetOrderStatus()
    {
        $statusArray = array("confirmed", "ready");
        //$Restaurant = Restaurant::where('delivery_provider_id' ,'=',5)->get();
        $deliveryServicesData = DeliveryService::where('provider_name', '=', 'twinjet')->get();
        foreach ($deliveryServicesData as $deliveryServices) {

            $restaurantData = Restaurant::where('delivery_provider_id', '=', $deliveryServices->id)->where('parent_restaurant_id', '>', 0)->first();
            $rest = $restaurantData->toArray();
            $deliverServiceProvider = new Twinjet(['apikey' => $rest['delivery_provider_apikey']]);
            //$restArray = [53];
            $orderData = UserOrder::where('user_orders.restaurant_id', '=', $rest['id'])->where('user_orders.order_type', '=', 'delivery')
                ->whereIn('user_orders.status', $statusArray)->get();


            foreach ($orderData as $order) {
                try {
                    $deliveryServiceLogObj = DeliveryServicesLogs::where('order_id', '=', $order->id)->get()->toArray();
                    //print_r($deliveryServiceLogObj );
                    if (isset($deliveryServiceLogObj[0]['order_receipt'])) {
                        $statusData = $deliverServiceProvider->getOrderStatus($deliveryServiceLogObj[0]['order_receipt']);

                        /*if(isset($statusData['response_data']['current_status']['status_code']) && $statusData['response_data']['current_status']['status_code']==52){ //cancel
					$this->changeOrderStatuscancel($order->id,"Order was cancelled as we can't deliver the order in the expected timeframe.",'cancelled',$order->restaurant_id,0,1);
				}
				elseif(isset($statusData['response_data']['current_status']['status_code']) && $statusData['response_data']['current_status']['status_code']==63){//cancel
					$this->changeOrderStatuscancel($order->id,"Order was cancelled as we can't deliver the order in the expected timeframe.",$order->restaurant_id,0,1);
				}
				else*/
                        if (isset($statusData['response_data']['current_status']['status_code']) && $statusData['response_data']['current_status']['status_code'] == 62) {//delivered
                            $this->changeOrderStatus($order->id, 'delivered', $order->restaurant_id, 0, 0, 0, 0, 0);
                        }

                    }
                } catch (Exception $e) {
                    \Log::info($e);
                }

            }
        }
    }

    /**
     * order edit update
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOrderStatus($id, $action, Request $request)
    {
        $giftOrderFlag = (int)$request->input('gift_order') ?? 0;
        $giftCardOrderId = (int)$request->input('oid') ?? 0;
        $giftCouponId = (int)$request->input('id') ?? 0;
        $giftCardSendAll = (int)$request->input('send_all') ?? 0;
        $byUser = Auth::user()->id;
        $restaurant_id = Auth::user()->restaurant_id;
        return $this->changeOrderStatus($id, $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
    }

    private function changeOrderStatus($id, $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser = 0)
    {
        
        $service_provider_error = '';
        $ordermsg = "";
         

        if ((isset($id) && is_numeric($id)) || ($giftOrderFlag && $giftCardOrderId)) {
            $orderData = UserOrder::find($id);

            $langId = 1;//config('app.language')['id'];
            CommonFunctions::$langId = $langId;
            $restaurantData = Restaurant::where(array('id' => $orderData->restaurant_id))->first();

            $orderItem = $label = "";
            $previousData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
            $previous_action_data = json_encode($previousData); 
            //echo $restaurantData->delivery_provider_id; die;
            //print_r($restaurantData); die;fname
	
            if ($action === "preparing" && $orderData->order_type == "delivery" && $orderData->manual_update == 0) {
		if($action === "preparing"){
		         $dtTime = strtotime($orderData->delivery_date." " .$orderData->delivery_time);
			 $ctTime = strtotime(date("Y-m-d H:i"));
			if(($dtTime - $ctTime) < (60*90)){
             //if($orderData->order_state == 1){    
			 $dataArray = ["job"=>[
				 //"pickup_at"=>str_replace(" ","T",$orderData->created_at),
				 //"dropoff_at"=>$orderData->delivery_date."T".$orderData->delivery_time,
				 "assignment_code"=> "#R2".$orderData->payment_receipt,
				 "pickups"=> [[ "address"=>$restaurantData->address. " ".$restaurantData->zipcode,
				                "comment"=> "Delivery Date ".$orderData->delivery_date . " #Delivery Time ".$orderData->delivery_time,
				                "contact"=> [
				                  "firstname"=> $restaurantData->pickup_contact_person,
				                  "lastname"=> '',
				                  "phone"=>"+".str_replace("+","",$restaurantData->phone),
						   
				                  "email"=> $restaurantData->pickup_contact_email,
				                  "company"=> $orderData->restaurant_name
				                ]
				              ]
				],
				"dropoffs"=> [
				              [
				                "package_type"=> "small",
				                "package_description"=> "Food Item",
				                "client_reference"=> "Order_ID##R2".$orderData->payment_receipt,
						        "address"=>$orderData->address. " ".$orderData->address2. " ".$orderData->city. " ".$orderData->state. " ".$orderData->zipcode,
				                "address2"=> $orderData->address. " ".$orderData->zipcode,
				                "comment"=> $orderData->address_label,
				                "contact"=> [ 
				                  "firstname"=>  $orderData->fname,
				                  "lastname"=> $orderData->lname,
				                  "phone"=>"+".str_replace("+","",trim($orderData->phone)),
				                   
				                  "email"=> $orderData->email,
				                  "company"=> ""
				                ]
				              ]
		       
		       
				]
			    ]
			   ];
	 		}else{
				$dataArray = ["job"=>[
				 //"pickup_at"=>$orderData->created_at,
				 "dropoff_at"=>$orderData->delivery_date."T".$orderData->delivery_time,
				 "assignment_code"=> "#R2".$orderData->payment_receipt,
				 "pickups"=> [[ "address"=>$restaurantData->address. " ".$restaurantData->zipcode,
				                "comment"=> "Delivery Date ".$orderData->delivery_date . " #Delivery Time ".$orderData->delivery_time,
				                "contact"=> [
				                  "firstname"=> $restaurantData->pickup_contact_person,
				                  "lastname"=> '',
				                  "phone"=> "+".str_replace("+","",$restaurantData->phone),
						  "email"=> $restaurantData->pickup_contact_email,
				                  "company"=> $orderData->restaurant_name
				                ]
				              ]
				],
				"dropoffs"=> [
				              [
				                "package_type"=> "small",
				                "package_description"=> "Food Item",
				                "client_reference"=> "Order_ID##R2".$orderData->payment_receipt,
						        "address"=>$orderData->address. " ".$orderData->address2. " ".$orderData->city. " ".$orderData->state. " ".$orderData->zipcode,
				                "address2"=> $orderData->address. " ".$orderData->zipcode,
				                "comment"=> $orderData->address_label,
				                "contact"=> [ 
				                  "firstname"=>  $orderData->fname,
				                  "lastname"=> $orderData->lname,
				                  "phone"=>"+".str_replace("+","",trim($orderData->phone)),
				                   
				                  "email"=> $orderData->email,
				                  "company"=> ""
				                ]
				              ]
		       
		       
				]
			    ]
			   ];
			}
			$response = Stuarts::createJobStuart($dataArray);
			$response = json_decode($response,true);
			if(isset($response['id'])){
				$orderInsert['ma_order_id']=$orderData->id;
				$orderInsert['ma_restaurant_id']=$orderData->restaurant_id;
				$orderInsert['job_id']=$response['id'];
				$orderInsert['status']=$response['status'];
				$orderInsert['assignment_code']=$response['assignment_code'];
				$orderInsert['pickup_at']=isset($response['deliveries']['pickup_at'])?$response['deliveries']['pickup_at']:null;
				$orderInsert['dropoff_at']=isset($response['deliveries']['pickup_at'])?$response['deliveries']['pickup_at']:null;
				$orderInsert['ended_at']=isset($response['ended_at'])?$response['ended_at']:null;
				$orderInsert['distance']=isset($response['distance'])?$response['distance']:null;
				$orderInsert['duration']=isset($response['duration'])?$response['duration']:null;
				$orderInsert['traveled_time']=isset($response['traveled_time'])?$response['traveled_time']:null;
				$orderInsert['traveled_distance']=isset($response['traveled_distance'])?$response['traveled_distance']:null;
				$orderInsert['driver']=isset($response['driver']['display_name'])?$response['driver']['display_name']." [".$response['driver']['display_name']."]":'N/A'  ;
				$orderInsert['comment']=isset($response['comment'])?$response['comment']:null;
				$orderInsert['request_obj']=json_encode($dataArray,true);
				$orderInsert['response_obj']=json_encode($response,true);
				StuartOrders::create($orderInsert);
			 }else{
				$action = "Placed";
                if(isset($response['error']))$msg = 'Stuart rejected this order! '.($response['error']."-".$response['message']);
			    else $msg = 'Stuart rejected this order! ';
				unset($orderInsert);
				$orderInsert['ma_order_id']=$orderData->id;
				$orderInsert['ma_restaurant_id']=$orderData->restaurant_id;
				$orderInsert['request_obj']=json_encode($dataArray,true);
				$orderInsert['response_obj']=json_encode($response,true);
				StuartOrdersLog::create($orderInsert);
				return response()->json(['data' => null, 'message' => $msg, 'errors' => 1]);
			 }
			}
		 
                 }
            $orderData->updated_at = now();
            if ($giftOrderFlag != 1) {
                $orderData->status = $action;
            }

            $orderData->save();
            /****************************order_statuses**********************************************************/
            $orderStatus = new OrderStatuses();
            $orderStatus->order_id = $id;
            $orderStatus->status = $action;
            $orderStatus->user_id = $byUser;
            $orderStatus->restaurant_id = $restaurant_id;
            $orderStatus->created_at = now();
            $orderStatus->save();
            //if($action === "preparing")
            /*******************************************************************************************************/
            $ordermsg = "Order has been " . $action . " successfully.";
            $user_id = $byUser;//Auth::user()->id;
            $updateData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
            $action_data = json_encode($updateData);
            $moduleName = '';
            if ($orderData->product_type == 'product') {
                $moduleName = 'Merchandise Order';
            } else if ($orderData->product_type == 'food_item') {
                $moduleName = 'Food Order';
            }
            $logData = array(
                'user_id' => $user_id,
                'updated_id' => $id,
                'module_name' => $moduleName,
                'restaurant_id' => $restaurant_id,///Auth::user()->restaurant_id, //$orderData->restaurant_id,   // PE-3221 - restaurant id should be based who's logged in Branch/Brand
                'tablename' => 'user_orders',
                'action_type' => 'update',
                'previous_action_data' => $previous_action_data,
                'action_data' => $action_data);

            //return $logData;
            $logs = CmsUserLog::create($logData);

            $timestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
            $cardInfo = '<span style="color: #000;">' . $orderData->card_type . '</span>(' . $orderData->card_number . ')';
            $orderDetail = new UserOrderDetail();
            $orderDetailData = $orderDetail->getOrderDetail($id);
            $orderItem = '';
            $total_refunded_amount = DB::table('refund_histories')->where('order_id', $orderData->id)->sum('amount_refunded');
            if (Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
            } else {
                $curSymbol = config('constants.currency');
            }
            if ($orderData->product_type != 'gift_card') {
                foreach ($orderDetailData as $data) {

                    $addon_modifier = CommonFunctions::getAddonsAndModifiers($data);
                    $addon_modifier_html = CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                    $instruct = '';

                    $addons_data = [];
                    $bypData = json_decode($data->menu_json, true);
                    //print_r($bypData); die;
                    if (!is_null($bypData)) {
                        $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                    }

                    $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
                    <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <b>' . $data->quantity . ' ' . $data->item . '</b>';

                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                    if ($orderData->product_type == 'product') {
                        if (!is_null($data->item_other_info)) {

                            if (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) {
                                $label = $data->item_size . ' ';
                            } else {
                                $label = "";
                            }
                            $item_other_info = json_decode($data->item_other_info);
                            $value3 = '';
                            $value = $item_other_info->is_gift_wrapping == 1 ? 'Yes' : 'No';
                            $value2 = 'Gift Wrapping: ' . $value;
                            if (isset($item_other_info->gift_wrapping_message) && !empty($item_other_info->gift_wrapping_message) && $item_other_info->gift_wrapping_message != 'null') {
                                $value3 = 'Message: ' . $item_other_info->gift_wrapping_message;
                            }
                            $label .= $value2 . '<br/>' . $value3;
                            $label .= $addon_modifier_html;
                        }
                    } else {
                        //$label = strtolower($data->size) != 'full' ? $data->size . ' ' : '';
                        if (strtolower($data->item_size) != 'full' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) {
                            $label = $data->item_size . ' ';
                        } else {
                            $label = "";
                        }
                        
                        if ($data->is_byp) {
                            $label = '';
                            $label = CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                        } else {
                            $label .= $addon_modifier_html;
                        }


                        if (!empty($data->special_instruction)) {
                            $instruct = '<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">' . $data->special_instruction . '</p>';
                        }
                    }
                    $orderItem .= '<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">' . $label . '</p>' . $instruct . '</th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>' . $curSymbol . '</span>' . $data->total_item_amt . '</p></th></tr></table></th></tr></tbody></table>
                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                }
            } else {
                $orderItem = CommonFunctions::getProductList($id);
            }
            $url = isset($restaurantData->source_url)?$restaurantData->source_url:null;
            $resultData = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);

            $is_asap_order = "";
            if ($orderData->is_asap_order === 1)
                $is_asap_order = "(Possibly sooner!)";
	        $prestaurantData = Restaurant::where(array('id'=>$restaurantData->parent_restaurant_id))->first();
                $mailKeywords = array(
                'BUYER_NAME' => ucfirst($orderData->fname),
                'RESTAURANT_NAME' => $prestaurantData->restaurant_name,
		          'BRANCH_RESTAURANT_NAME'=>$restaurantData->restaurant_name,
                'ORDER_NUMBER' => $orderData->payment_receipt,
                'USER_NAME' => ucfirst($orderData->fname),
                'ORDER_TYPE' => ucfirst($orderData->order_type),
                'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp) . ' ' . $is_asap_order,
                'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                'DELIVERY_ADD' => $orderData->address . ' ' . $orderData->city,
                'PHONE' => $orderData->phone,
                'SUB_TOTAL' =>   $orderData->order_amount,
                'TOT_AMOUNT' =>  number_format($orderData->total_amount, 2),
		      'DELIVERY_CHARGE'	  =>  $orderData->delivery_charge,
		      'GRAND_TOTAL'	  =>  number_format($orderData->total_amount, 2),
		      'TIP'	 	  =>  $orderData->tip_amount,
		      'TAX'	 	  =>  $orderData->tax,
                
                'REFUND_AMOUNT' => '<span>' . $curSymbol . '</span>' . number_format($total_refunded_amount, 2),
                'ITEM_DETAIL' => $orderItem,
                'CARD_NUMBER' => $cardInfo,
                'URL' => $prestaurantData->source_url,
                'REST_NAME' => strtolower($restaurantData->restaurant_name),
                'SITE_URL' => $restaurantData->source_url,
                'title' => "Order Confirmation",
                'PRNAME' => ucfirst($resultData['restaurantParentName']),
                'APP_BASE_URL' => config('constants.mail_image_url'),
            );
            /*************************************************************************************************/
            $mailKeywords = CommonFunctions::getAllChargesForMailer($mailKeywords, $orderData, $curSymbol);
             /**************************************************************************************************/
            $tipAmount = $orderData->tip_amount;
            if ($orderData->user_comments != "") {
                $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>' . $orderData->user_comments . '</i>';
            } else {
                $mailKeywords['SPECIAL_INFO'] = '';
            }
	
            if ($orderData->tax == 0.00) {
                $mailKeywords['TAX'] = '';
                $mailKeywords['TAX_TITLE'] = '';
                $mailKeywords['DISPLAY'] = 'none';
            } else {
                $mailKeywords['TAX'] = '<span>' . $curSymbol . '</span>' . $orderData->tax;
                $mailKeywords['TAX_TITLE'] = "Taxes";
                $mailKeywords['DISPLAY'] = 'table';
            }
            $mailKeywords['DISPLAY_REFUND'] = 'none';
            if ($total_refunded_amount > 0) {
                $mailKeywords['DISPLAY_REFUND'] = 'table';
            }
            if ($orderData->tip_amount == 0.00) {
                $mailKeywords['TIP_AMOUNT'] = '';
                $mailKeywords['TIP_TITLE'] = '';
                $mailKeywords['DISPLAY_TIP'] = 'none';

            } else {
                $mailKeywords['TIP_AMOUNT'] = '<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                            <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
<p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">' . $orderData->tip_percent . '<span>%</span></p>
                            </th></tr></table></th>
                            <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>' . $curSymbol . '</span>' . $tipAmount . '</p></th></tr></table></th>
                        </tr></tbody></table>';
                $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                $mailKeywords['DISPLAY_TIP'] = 'table';
            }
	    /**************************************************************************************************/
	    $orderDetail = new UserOrderDetail();
	    $orderDetailData = $orderDetail->getOrderDetail($id);
	       
	    $mailKeywords['ORDER_DETAILS'] = $this->getItemHtmlTemplate($orderDetailData); ;	
	    $mailKeywords['DELIVERY_CHARGE'] = $orderData->delivery_charge;			
	    /*************************************************************************************************/
            if ($orderData->order_type == "delivery") {

                $address = $orderData->address;
                if ($orderData->address2) {
                    $address = $address . ' ' . $orderData->address2;
                }
                $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                //echo $address;

                $mailKeywords['DELIVERY_CHARGE'] = '<span>' . $curSymbol . '</span>' . $orderData->delivery_charge;
                $mailKeywords['USER_ADDRESS'] = $address;
		 
                if ($orderData->product_type != 'gift_card') {
                    $mailTemplate = 'delivery_confirm_user';
                } else {
                    $mailTemplate = 'giftcard_buyer_confirmation_sent';
                }
                if ($orderData->product_type == 'product') {
                    $mailKeywords['DTIME'] = 'style="display:none"';
                } else {
                    $mailKeywords['DTIME'] = '';
                }
		if($action == "delivered"){
			 $mailKeywords['subject'] ="Bottoms up!";
			$mailTemplate = 'user_order_delivered';
		}else $mailKeywords['subject'] = "They're on it!";
            } else {
                $mailTemplate = 'carryout_confirm_user';
                $mailKeywords['USER_ADDRESS'] = '';
                $img = "";
                if ($action == "ready") {
                    $heading = "We have finished preparing your takeout order and can't wait for you to enjoy it.";
                    $img = '<a href="#"><img src="https://dashboard.believerssoftware.com/mailer-images/keki/get-direction.jpg" alt="" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></a>';
                    $mailKeywords['DISPLAY_IMG'] = 'none';
                } else {
                    $heading = "You've put together an amazing order.";
                    $img = '<a href="' . $url . '"><img src="https://dashboard.believerssoftware.com/mailer-images/keki/manage-order.jpg" alt="" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></a>';
                    $mailKeywords['DISPLAY_IMG'] = 'table';
                }

                $mailKeywords['HEAD'] = $heading;
                $mailKeywords['IMG'] = $img;
                $mailKeywords['ACTION'] = ucfirst($action);

            }

            $config['to'] = $orderData->email;
            if ($orderData->product_type == 'gift_card' && $giftCardSendAll == 1) {
                $config['subject'] = "Your Recipients Just Got Your Gift Card";
                $mailKeywords['RECIPIENT'] = "recipients";
            } else {
                $config['subject'] = "Your order has been " . $action;
            } 
	    if($action == "delivered") $config['subject'] =  "Bottoms up!";
	    else $config['subject'] =  "They're on it!";
            $config['from_address'] = $resultData['custom_from'];
            //$config['from_name'] = $resultData['restaurantParentName'];
            $config['from_name'] = ucfirst($prestaurantData->restaurant_name);

            $controller = new NotificationController;
            if (($action == "archived") || ($action == "sent")) {
                return $orderData;
            } else {
                if ($action == "outfordelivery") {
                    /******** SMS Send **** @18-10-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $orderData->id,
                        'sms_module' => 'order',
                        'action' => 'ready',
                        'sms_to' => 'customer'
                    );
                    CommonFunctions::getAndSendSms($sms_keywords);
                    /************ END SMS *************/
                } elseif ($action == "preparing"  && $orderData->product_type != 'gift_card') {
                    /******** SMS Send **** @18-10-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $orderData->id,
                        'sms_module' => 'order',
                        'action' => 'confirmed',
                        'sms_to' => 'customer'
                    );
                    CommonFunctions::getAndSendSms($sms_keywords);
                    /************ END SMS *************/
                }
                //if ($orderData->product_type != 'gift_card') {
                    $controller->sendMail($mailTemplate, $restaurantData->id, $config, $mailKeywords);
                //}
                // PROCESS GIFT CARD ORDERS
                //$byUser = Auth::user()->id;
                
            }
            if ($service_provider_error) {
                return response()->json(['data' => 1, 'message' => $service_provider_error, 'errors' => null]);
            }
            //  return $orderData;

            return response()->json(['errors' => null, 'success' => $ordermsg, 'message' => 'success']);

        }
    }
    public function getItemHtmlTemplate($orderDetailData){
	$temp ="";$modifierData = "";
	foreach($orderDetailData as $oData){
		$addon_modifier=json_decode($oData->menu_json,true);
		
		$i=0; $modifierData=$groupname = $addonDataArrary = null;
        if(isset($addon_modifier['modifier_items'])){
            $modifier_data_records = $addon_modifier['modifier_items'];
		foreach($modifier_data_records as $modifier_data){  
			  $groupname =   "<b>".$modifier_data['group_prompt']."</b>". $modifier_data['modifier_name'];
			  
		 
			$modifierData ='<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; position: relative; text-align: left; vertical-align: top; width: 100%; padding: 0;"> <tbody> <tr style="vertical-align: top; padding: 0;" align="left"> <th class="small-12 large-12 columns first last" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 100%; margin: 0 auto; padding: 0 0 8px;" align="left"> <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;"> <tbody> <tr style="vertical-align: top; padding: 0;" align="left"> <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left"> <p class="addon" style="color: #848484; font-family: Helvetica,Arial,sans-serif; font-size: 13px; font-weight: 700; line-height: 1.3; margin: 0; padding: 0;" align="left">'.$groupname.'</p> </th> <th class="expander" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; visibility: hidden; width: 0; margin: 0; padding: 0;" align="left"></th> </tr> </tbody> </table> </th> </tr> </tbody> </table>';
		
		} 
    }
	$temp .='<tr style="vertical-align: top; padding: 0;" align="left">
                <th class="small-6 large-6 columns first" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 282px; margin: 0 auto; padding: 0 4px 8px 8px;" align="left">
                <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
<tbody>
<tr style="vertical-align: top; padding: 0;" align="left">
<th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left">
<table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
<tbody>
<tr style="vertical-align: top; padding: 0;" align="left">
<td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 1.3; word-wrap: break-word; margin: 0; padding: 0;" valign="top" height="15" align="left">&nbsp;</td>
</tr>
</tbody>
</table>
<p class="item" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 700; line-height: 1.3; margin: 0; padding: 0;" align="left">'.$oData->item.'</p>
<table class="spacer" style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
<tbody>
<tr style="vertical-align: top; padding: 0;" align="left">
<td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 6px; font-weight: 400; hyphens: auto; line-height: 6px; mso-line-height-rule: exactly; word-wrap: break-word; margin: 0; padding: 0;" valign="top" height="6px" align="left">&nbsp;</td>
</tr>
</tbody>
</table>
<!--Customisation Row Starts-->
<table class="row customisation" style="border-collapse: collapse; border-spacing: 0; display: table; position: relative; text-align: left; vertical-align: top; width: 100%; padding: 0;">
<tbody>
<tr style="vertical-align: top; padding: 0;" align="left">
<th class="nopadding small-12 large-12 columns first last" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 100%; margin: 0 auto; padding: 0 0 8px;" width="12" align="left">
<table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
    <tbody>
    <tr style="vertical-align: top; padding: 0;" align="left">
    <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left">
    <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;" width="100%" cellspacing="0" cellpadding="0">
        <tr e="vertical-align: toadding: 0;" align="left">
                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 1.3; word-wrap: break-word; margin: 0; padding: 0;" valign="top" align="left">
                    <!--addon start-->										
                    '.$modifierData.'
                    <!--addon end--> 
                </td>
                    </tr>
                        </tbody>
                                </th>
                                    </tr>
                                     </tbody>
                                  </table>
                               </th>
                            </tr>
                         </tbody>
                      </table>
                   </th>
                </tr>
             </tbody>
          </table>
       </th>

       <th class="small-3 large-3 columns" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 137px; margin: 0 auto; padding: 0 4px 8px;" valign="top" align="left">
          <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
             <tbody>
                <tr style="vertical-align: top; padding: 0;" align="left">
                   <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left">
                      <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                         <tbody>
                            <tr style="vertical-align: top; padding: 0;" align="left">
                               <td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 1.3; word-wrap: break-word; margin: 0; padding: 0;" valign="top" height="15" align="left">&nbsp;</td>
                            </tr>
                         </tbody>
                        </table>
                      <p style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 100; line-height: 1.3; margin: 0 0 auto; padding: 0;" class="item text-center" align="center">'.$oData->quantity.'</p>
                   </th>
                </tr>
             </tbody>
          </table>
       </th>
       <th class="small-3 large-3 columns last" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 137px; margin: 0 auto; padding: 0 8px 8px 4px;" valign="top" align="left">
          <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
             <tbody>
                <tr style="vertical-align: top; padding: 0;" align="left">
                   <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left">
                      <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                         <tbody>
                            <tr style="vertical-align: top; padding: 0;" align="left">
                               <td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 1.3; word-wrap: break-word; margin: 0; padding: 0;" valign="top" height="15" align="left">&nbsp;</td>
                            </tr>
                         </tbody>
                      </table>
                      <p style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 700; line-height: 1.3; margin: 0 0 auto; padding: 0;" class="item text-right" align="right">£<span>'.$oData->total_item_amt.'</span></p>
                   </th>
                </tr>
             </tbody>
          </table>
       </th>
    </tr>';
	}
	return $temp;
    } 
    public function fetchOrderOrderStatusFromStuart()
    {
	 
         
	$jobData = StuartOrders::whereIn('status',['in_progress','searching','pending','picking','almost_picking','waiting_at_pickup','delivering','almost_delivering','waiting_at_dropoff','delivering'])->where(['is_porcess'=>0])->where(['is_cron'=>0])->get()->toArray();  
	foreach($jobData as $job){
        StuartOrders::where('id', $job['id'])->update(['is_cron' => 1]); 
		$response = Stuarts::getStuartJobStatus($job['job_id']);
		$response = json_decode($response,true);
		if(isset($response['id'])){
                //$response['status'] = isset($response['deliveries'][0]['status'])?$response['deliveries'][0]['status']:$response['status'];
                $orderInsert['ma_order_id']= $job['ma_order_id'];
                $orderInsert['ma_restaurant_id']=$job['ma_restaurant_id'];
                $orderInsert['job_id']=$response['id'];
                $orderInsert['status']=$response['status'];
                $orderInsert['assignment_code']=$response['assignment_code'];
                $orderInsert['pickup_at']=isset($response['deliveries']['pickup_at'])?$response['deliveries']['pickup_at']:null;
				$orderInsert['dropoff_at']=isset($response['deliveries']['pickup_at'])?$response['deliveries']['pickup_at']:null;
				$orderInsert['ended_at']=isset($response['ended_at'])?$response['ended_at']:null;
				$orderInsert['distance']=isset($response['distance'])?$response['distance']:null;
				$orderInsert['duration']=isset($response['duration'])?$response['duration']:null;
				$orderInsert['traveled_time']=isset($response['traveled_time'])?$response['traveled_time']:null;
				$orderInsert['traveled_distance']=isset($response['traveled_distance'])?$response['traveled_distance']:null;
				$orderInsert['driver']=isset($response['driver']['display_name'])?$response['driver']['display_name']." [".$response['driver']['display_name']."]":'N/A'  ;
                $orderInsert['tracking_url']=isset($response['deliveries'][0]['tracking_url'])?$response['deliveries'][0]['tracking_url']:'N/A'  ;
                
				$orderInsert['comment']=isset($response['comment'])?$response['comment']:null;
                $orderInsert['request_obj']=json_encode($job['job_id'],true);
                $orderInsert['response_obj']=json_encode($response,true);
                $orderInsert['is_porcess']=0;
            if(isset($response['deliveries'][0]['status']) && $response['deliveries'][0]['status']=="delivering"){
				$action="outfordelivery";
                $orderInsert['status']=$response['deliveries'][0]['status'];
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
                $stl = StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status', $orderInsert['status'])->get()->toArray();
                echo count($stl);
                if(count($stl)<1){ 
				    $this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				    StuartOrders::where('ma_order_id', $job['ma_order_id'])->update(['is_porcess' => 1]);
                    StuartOrders::create($orderInsert);
                }
			}else if($response['status']=="finished"){
                $action="completed";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
                 StuartOrders::where('ma_order_id', $job['ma_order_id'])->update(['is_porcess' => 1]);
			    $orderInsert['is_porcess']=1;     
				StuartOrders::create($orderInsert);
				
				//$this->changeOrderStatus($job['ma_order_id'], "outfordelivery", $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				//StuartOrders::where('ma_order_id', $job['ma_order_id'])->update(['is_porcess' => 1]);
			}
			else if($response['status']=="delivered"){
				$action="delivered";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
					
			        
				StuartOrders::create($orderInsert);
				//$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				StuartOrders::where('ma_order_id', $job['ma_order_id'])->update(['is_porcess' => 1]);
                
                $action="completed";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
					
			        
				StuartOrders::create($orderInsert);
				//$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				StuartOrders::where('ma_order_id', $job['ma_order_id'])->update(['is_porcess' => 1]);
                
			}
			else if($response['status']=="pending"){
				
				StuartOrders::create($orderInsert);
				StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status', '!=' , $response['status'])->update(['is_porcess' => 1]);
			}else if($response['status']=="picking"){
				$action="ready";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
				$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				StuartOrders::create($orderInsert);
				StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status', '!=' , $response['status'])->update(['is_porcess' => 1]);
			}else if($response['status']=="almost_picking"){
				$action="ready";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
				//$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				StuartOrders::create($orderInsert);
				StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status', '!=' , $response['status'])->update(['is_porcess' => 1]);
			}else if($response['status']=="waiting_at_pickup"){
				$action="ready";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
				//$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				StuartOrders::create($orderInsert);
				StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status', '!=' , $response['status'])->update(['is_porcess' => 1]);
			}else if($response['status']=="delivering"){
				$action="outfordelivery";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
                 $stl = StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status',  $response['status'])->get()->toArray();
                if(count($stl)<1){ 
				$restaurant_id = $job['ma_restaurant_id'];
				$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
                 StuartOrders::where('ma_order_id', $job['ma_order_id'])->update(['is_porcess' => 1]);    
				 StuartOrders::create($orderInsert);
				
                }
			} else if($response['status']=="almost_delivering"){
				$action="sent";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
				//$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				StuartOrders::create($orderInsert);
				StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status', '!=' , $response['status'])->update(['is_porcess' => 1]);
			}else if($response['status']=="waiting_at_dropoff"){
				$action="sent";
				$giftOrderFlag = 0;
				$giftCardOrderId =  0;
				$giftCouponId = $job['ma_order_id'];
				$giftCardSendAll =   0;
				$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
				//$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
				StuartOrders::create($orderInsert);
				StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status', '!=' , $response['status'])->update(['is_porcess' => 1]);
			}else if($response['status']=="canceled"){
				$response['status']="cancelled";
				$message = "Stuart cancelled this order";
				if(isset($response['deliveries']['cancellation']['canceled_by'])){
					$message .= ", Cancelled By ".ucfirst(str_replace("_"," ",$response['deliveries']['cancellation']['canceled_by']));
				       if(isset($response['deliveries']['cancellation']['reason_key'])) 
					$message .= ", Reason- ".str_replace("_"," ",$response['deliveries']['cancellation']['reason_key']);
				}
				$orderInsert['is_porcess']=1;
				$s = StuartOrders::find($job['id']);
				$s->is_porcess = 1;
				$s->save();
				StuartOrders::create($orderInsert);
				$this->changeOrderStatuscancel($job['ma_order_id'], $message , "cancelled", $job['ma_restaurant_id'], 0, 0);
			}else{
                //$action="sent";
				//$giftOrderFlag = 0;
				//$giftCardOrderId =  0;
				//$giftCouponId = $job['ma_order_id'];
				//$giftCardSendAll =   0;
				//$byUser = 0;
				$restaurant_id = $job['ma_restaurant_id'];
				//$this->changeOrderStatus($job['ma_order_id'], $action, $restaurant_id, $giftOrderFlag, $giftCardOrderId, $giftCouponId, $giftCardSendAll, $byUser);
                $stl = StuartOrders::where('ma_order_id', $job['ma_order_id'])->where('status',  $response['status'])->get()->toArray();
                if(count($stl)<1){ 
				    StuartOrders::where('ma_order_id', $job['ma_order_id'])->update(['is_porcess' => 1]);
                    StuartOrders::create($orderInsert);
                }
            }
		}else{
			unset($orderInsert);
			$orderInsert['ma_order_id']= $job['ma_order_id'];
			$orderInsert['ma_restaurant_id']=$job['ma_restaurant_id'];
			$orderInsert['request_obj']=json_encode($job['job_id'],true);
			$orderInsert['response_obj']=json_encode($response,true);
			StuartOrdersLog::create($orderInsert);
			//return response()->json(['data' => 1, 'message' => "Stuart recject this order", 'errors' => null]);
		}
        StuartOrders::where('id', $job['id'])->update(['is_cron' => 0]); 
       }
    }
    public function cancelOrder(Request $request)
    {
        $id = $request->input('oid');
        $reason = $request->input('reason');
        $status = $request->input('status');
        $restaurant_id = Auth::user()->restaurant_id;
        $user_id = Auth::user()->id;
        return $this->changeOrderStatuscancel($id, $reason, $status, $restaurant_id, $user_id, 0);
    }

    public function changeOrderStatuscancel($id, $reason, $status, $restaurant_id, $user_id, $isTwinjet = 0)
    {
         
        if (isset($id) && is_numeric($id)) {
            $orderData = UserOrder::find($id);
            $total_refunded_amount = DB::table('refund_histories')->where('order_id', $id)->sum('amount_refunded');
            /// print_r($total_refunded_amount);die;
            $previousData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
            $previous_action_data = json_encode($previousData);
	    $oldStatus = $orderData->status;		
            $orderData->updated_at = now();
            $orderData->status = $status;
            $orderData->restaurants_comments = $reason;
            
	     if($oldStatus!="placed"){
		$job = StuartOrders::where('ma_order_id',$id)->first();
		if(isset($job->job_id)){
			$cdata = Stuarts::cancelAJob($job->job_id);
			$cdata = json_decode($cdata,true);
			if($cdata['error']=='CANCELLATION_ERROR'){
				//return null;
			}}
		
	    }	$orderData->save();		
	    /****************************order_statuses**********************************************************/
            $orderStatus = new OrderStatuses();
            $orderStatus->order_id = $id;
            $orderStatus->status = $status;
            $orderStatus->user_id =0;
            $orderStatus->restaurant_id = $orderData->restaurant_id;
            $orderStatus->created_at = now();
            $orderStatus->save();

            /*******************************************************************************************************/			
            $restaurantData = Restaurant::where(array('id' => $orderData->restaurant_id))->first();
            $restaurantData1 = $restaurantData->toArray();

            $deliveryServicesData = DeliveryService::where(array('id' => $restaurantData->delivery_provider_id))->first();
            if (isset($deliveryServicesData) && $deliveryServicesData && $deliveryServicesData->provider_name == "twinjet") {
                if ($orderData->order_type == "delivery" && $status == "cancelled" && $isTwinjet == 0) {
                    $orderDataArray = $orderData->toArray();
                    $this->submitOrderToVendor($orderDataArray, $restaurantData1, 2, $restaurantData->delivery_provider_id);
                }
            }//twinjet
            $langId = config('app.language')['id'];
            CommonFunctions::$langId = $langId;
            $restaurantData = Restaurant::where(array('id' => $orderData->restaurant_id))->first();
            $orderItem = $label = "";

            //$user_id = ;
            $updateData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
            $action_data = json_encode($updateData);
            $moduleName = '';
            if ($orderData->product_type == 'product') {
                $moduleName = 'Merchandise Order';
            } else if ($orderData->product_type == 'food_item') {
                $moduleName = 'Food Order';
            }
            $logData = array(
                'user_id' => $user_id,
                'updated_id' => $id,
                'module_name' => $moduleName,
                'restaurant_id' => $restaurant_id,//Auth::user()->restaurant_id, //$orderData->restaurant_id,   // PE-3221 - restaurant id should be based who's logged in Branch/Brand
                'tablename' => 'user_orders',
                'action_type' => 'update',
                'previous_action_data' => $previous_action_data,
                'action_data' => $action_data);

            //return $logData;
            $logs = CmsUserLog::create($logData);
            $timestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
            $cardInfo = $orderData->card_type . ' (' . $orderData->card_number . ')';
            $orderDetail = new UserOrderDetail();
            $orderDetailData = $orderDetail->getOrderDetail($id);
            $orderItem = '';
            if (Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
            } else {
                $curSymbol = config('constants.currency');
            }
            foreach ($orderDetailData as $data) {

                $addon_modifier = CommonFunctions::getAddonsAndModifiers($data);
                $addon_modifier_html = CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                $instruct = '';

                $addons_data = [];
                $bypData = json_decode($data->menu_json, true);

                if (!is_null($bypData)) {
                    $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                }

                $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
        <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
        <tr style="padding:0;text-align:left;vertical-align:top">
        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
        <b>' . $data->quantity . ' ' . $data->item . '</b>';

                $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                if ($orderData->product_type == 'product') {
                    if (!is_null($data->item_other_info)) {

                        if (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) {
                            $label = $data->item_size . ' ';
                        } else {
                            $label = "";
                        }
                        $item_other_info = json_decode($data->item_other_info);
                        $value3 = '';
                        $value = $item_other_info->is_gift_wrapping == 1 ? 'Yes' : 'No';
                        $value2 = 'Gift Wrapping: ' . $value;
                        if (isset($item_other_info->gift_wrapping_message) && !empty($item_other_info->gift_wrapping_message) && $item_other_info->gift_wrapping_message != 'null') {
                            $value3 = 'Message: ' . $item_other_info->gift_wrapping_message;
                        }
                        $label .= $value2 . '<br/>' . $value3;
                        $label .= $addon_modifier_html;
                    }
                } else {
                    //$label = strtolower($data->size) != 'full' ? $data->size . ' ' : '';
                    if (strtolower($data->item_size) != 'full' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) {
                        $label = $data->item_size . ' ';
                    } else {
                        $label = "";
                    }
                     
                    if ($data->is_byp) {
                        $label = '';
                        $label = CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                    } else {
                        $label .= $addon_modifier_html;
                    }


                    if (!empty($data->special_instruction)) {
                        $instruct = '<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">' . $data->special_instruction . '</p>';
                    }
                }
                $orderItem .= '<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">' . $label . '</p>' . $instruct . '</th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
        <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>' . $curSymbol . '</span>' . $data->total_item_amt . '</p></th></tr></table></th></tr></tbody></table>
        <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
            }
            $is_asap_order = "";
            if ($orderData->is_asap_order === 1)
                $is_asap_order = "(Possibly sooner!)";
            $resultData = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);

            $mailKeywords = array(
                'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                'RESTAURANT_ADD' => $restaurantData->address,
                'ORDER_NUMBER' => $orderData->payment_receipt,
                'USER_NAME' => ucfirst($orderData->fname),
                'ORDER_TYPE' => ucfirst($orderData->order_type),
                'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp) . ' ' . $is_asap_order,
                'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                'SUB_TOTAL' => '<span>' . $curSymbol . '</span>' . $orderData->order_amount,
                'TOT_AMOUNT' => '<span>' . $curSymbol . '</span>' . number_format($orderData->total_amount, 2),
                'REFUND_AMOUNT' => '<span>' . $curSymbol . '</span>' . number_format($total_refunded_amount, 2),
                'CARD_NUMBER' => $cardInfo,
                'REASON' => $reason,
                'ITEM_DETAIL' => $orderItem,
                'URL' => $restaurantData->source_url,
                'REST_NAME' => strtolower($restaurantData->restaurant_name),
                'SITE_URL' => $restaurantData->source_url,
                'title' => "Cancel Order",
                'APP_BASE_URL' => config('constants.mail_image_url'),
            );

            /*************************************************************************************************/
            $mailKeywords = CommonFunctions::getAllChargesForMailer($mailKeywords, $orderData, $curSymbol);
            /**************************************************************************************************/


            $tipAmount = $orderData->tip_amount;
            $mailKeywords['DISPLAY_REFUND'] = 'none';
            if ($total_refunded_amount > 0) {
                $mailKeywords['DISPLAY_REFUND'] = 'table';
            }
             

            if ($orderData->user_comments != "") {
                $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>' . $orderData->user_comments . '</i>';
            } else {
                $mailKeywords['SPECIAL_INFO'] = '';
            }

             
            if ($orderData->order_type == "delivery") {

                $address = $orderData->address;
                if ($orderData->address2) {
                    $address = $address . ' ' . $orderData->address2;
                }
                $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;

                $mailKeywords['DELIVERY_CHARGE'] = '<span>' . $curSymbol . '</span>' . $orderData->delivery_charge;
                $mailKeywords['USER_ADDRESS'] = $address;
                $mailTemplate = 'delivery-cancel';
                if ($orderData->product_type == 'product') {
                    $mailKeywords['DTIME'] = 'style="display:none"';
                } else {
                    $mailKeywords['DTIME'] = '';
                }
            } else {
                $mailTemplate = 'carryout_cancel';
                $mailKeywords['USER_ADDRESS'] = '';
            }

            $config['to'] = $orderData->email;
            $config['subject'] = ucfirst($restaurantData->restaurant_name) . " Order Cancellation";
            $config['from_address'] = $resultData['custom_from'];
            //$config['from_name'] = $resultData['restaurantParentName'];
            $config['from_name'] = ucfirst($restaurantData->restaurant_name);
            $controller = new NotificationController;
            if ($status == "rejected") {
                /******** SMS Send **** @18-10-2018 by RG *****/
                $sms_keywords = array(
                    'order_id' => $orderData->id,
                    'sms_module' => 'order',
                    'action' => 'rejected',
                    'sms_to' => 'customer'
                );
                CommonFunctions::getAndSendSms($sms_keywords);
                /************ END SMS *************/
            }
            if ($status == "cancelled") {
                /******** SMS Send **** @18-10-2018 by RG *****/
                $sms_keywords = array(
                    'order_id' => $orderData->id,
                    'sms_module' => 'order',
                    'action' => 'canceled',
                    'sms_to' => 'customer'
                );
                CommonFunctions::getAndSendSms($sms_keywords);
                /************ END SMS *************/
            }
	   
	    
            #PE-3241 By RG 04-Dec-18
            $controller->sendMail($mailTemplate, $restaurantData->parent_restaurant_id, $config, $mailKeywords);

            return $orderData;
        }

        //return Redirect::back()->withSuccess('Language set successfully!');
    }

    public function updateTimeSlot(Request $request)
    {

        $id = $request->input('oid');
        $sdate = $request->input('sdate');
        $stime = $request->input('stime');
        $quantity = $request->input('quantity');
        $user_id = Auth::user()->id;
        if (isset($id) && is_numeric($id)) {
            $orderData = UserOrder::find($id);
            $previousData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->delivery_date . ' ' . date('h:i A', strtotime($orderData->delivery_time)));
            $previous_action_data = json_encode($previousData);
            $moduleName = '';
            if ($orderData->product_type == 'product') {
                $moduleName = 'Merchandise Order';
            } else if ($orderData->product_type == 'food_item') {
                $moduleName = 'Food Order';
            }

            /********************update time by quantity value************************/
            $minutes_to_add = $quantity;

            $quantity_date = date('Y-m-d', strtotime($orderData->delivery_date));
            $quantity_time = date('H:i', strtotime($orderData->delivery_time));

            $date_time = $quantity_date . " " . $quantity_time;
            $time = new \DateTime($date_time);
            $time->add(new \DateInterval('PT' . $minutes_to_add . 'M'));
            $stamp = $time->format('Y-m-d H:i');
            $quantity_date_time = explode(" ", $stamp);


            $orderData->delivery_date = $quantity_date_time[0];
            $orderData->delivery_time = $quantity_date_time[1];

            /***************************************************************************/
            $updateData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => date('H:i', strtotime($quantity_date_time[1])), "delivery_date" => date('Y-m-d', strtotime($quantity_date_time[0])), "status" => date('Y-m-d', strtotime($quantity_date_time[0])) . ' ' . date('h:i A',
                    strtotime($quantity_date_time[1])));
            $action_data = json_encode($updateData);

            $logData = array(
                'user_id' => $user_id,
                'updated_id' => $id,
                'module_name' => $moduleName,
                'restaurant_id' => Auth::user()->restaurant_id, //$orderData->restaurant_id,   // PE-3221 - restaurant id should be based who's logged in Branch/Brand
                'tablename' => 'user_orders',
                'action_type' => 'update',
                'previous_action_data' => $previous_action_data,
                'action_data' => $action_data);

            //return $logData;
            $logs = CmsUserLog::create($logData);

            $orderData->updated_at = now();


            $orderData->save();
            $restaurantData = Restaurant::where(array('id' => $orderData->restaurant_id))->first();
            //send email to end customer and sms @26-03-2019
            $restaurant_name = htmlentities($restaurantData->restaurant_name, ENT_QUOTES, "UTF-8");
	    $prestaurantData = Restaurant::where(array('id'=>$restaurantData->parent_restaurant_id))->first();
            if ($orderData->email && filter_var($orderData->email, FILTER_VALIDATE_EMAIL)) {
                $mailKeywords = array(
                    'RESTAURANT_NAME' => $prestaurantData->restaurant_name,
                    'RESTAURANT_ADD' => $restaurantData->address,
                    'ORDER_NUMBER' => $orderData->payment_receipt,
                    'USER_NAME' => ucfirst($orderData->fname),
                    'ORDER_TYPE' => ucfirst($orderData->order_type),
                    'ORDER_DELIVERY_TIME' => date('l d M, h:i A', strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00')),
                    'HEAD' => 'Order Time Update',

                    'URL' => $restaurantData->source_url,
                    'REST_NAME' => $restaurant_name,
                    'SITE_URL' => $restaurantData->source_url,
                    'title' => "Timeslot update",
                    'APP_BASE_URL' => config('constants.mail_image_url'),
                );
                $mailTemplate = 'order_time_update';
                $config['to'] = $orderData->email;
                $config['subject'] = "Your Order Update";
                $config['from_address'] = $restaurantData->custom_from;
                //$config['from_name'] = $resultData['restaurantParentName'];
                $config['from_name'] = $prestaurantData->restaurant_name;
                $controller = new NotificationController;
                $controller->sendMail($mailTemplate, $restaurantData->parent_restaurant_id, $config, $mailKeywords);
            }
            /******** SMS Send **** @26-03-2019 by RG *****/
            $sms_keywords = array(
                'order_id' => $orderData->id,
                'sms_module' => 'order',
                'action' => 'timeupdate',
                'sms_to' => 'customer',
                'time' => $mailKeywords['ORDER_DELIVERY_TIME']
            );
            CommonFunctions::getAndSendSms($sms_keywords);
            /************ END SMS *************/

            $deliveryServicesData = DeliveryService::where(array('id' => $restaurantData->delivery_provider_id))->first();
            /*Bug fixes by jawed PE-4149*/
            if (isset($deliveryServicesData) && $deliveryServicesData->provider_name == "twinjet" && $orderData->order_type == "delivery") { //trigger twinjet
                $orderDataArray = $orderData->toArray();
                $restaurantDataArray = $restaurantData->toArray();
                $orderDetail = new UserOrderDetail();
                $orderDetailsData = $orderDetail->getOrderDetail($orderData->id);
                $orderDetailsData1 = $orderDetailsData->toArray();

                $orderDataArray['item_list'][] = $orderDetailsData1;
                $responseData = $this->submitOrderToVendor($orderDataArray, $restaurantDataArray, 0, $deliveryServicesData->id);


            }
            return $orderData;
        }
    }

    /*  public function getTimeSlots(Request $request)
    {
        $order_type = $request->input('order_type');
        $restaurant_id = $request->input('restaurant_id');
        $delivery_date = $request->input('delivery_date');

        $time = new TimeslotController;
        //echo $orderData->delivery_date;
        //echo $locationId;
        $get_time_slot = '';
        if($order_type == 'delivery') {
            // Get delivery slot
            $get_time_slot = $time->validateRestaurantTime($restaurant_id, 'delivery', $delivery_date);
            //$result = LocationWorkingDay::getDeliveryWorkingDay($locationId);
        }else {
            $get_time_slot = $time->validateRestaurantTime($restaurant_id, 'carryout', $delivery_date);
            //$result = LocationWorkingDay::getTakeoutWorkingDay($locationId);
        }

        $timeSlotStatus = 0;
        $timeSlotData = "";
        if(!empty($get_time_slot)) {
            $timeSlotStatus = $get_time_slot['status'];
            if ($timeSlotStatus == 1) {
                $timeSlotData = $get_time_slot['slot'];
            } else {
                $timeSlotData = array("error" => 'Slot not found');
            }
        }
        return $timeSlotData;
    } */

    public function orderUpdateArchive()
    {
        //die('cronUpdate');
        ///$statusArray = array("sent", "ready");
        $statusArray = array("sent");
        $restArray = [];
        $orderUpdate = [];
        $orderUpdate = UserOrder::whereIn('status', $statusArray)->where('cronUpdate', 0)
            ->update([
                'status' => 'archived',
                'cronUpdate' => 1]);

        if (!empty($orderUpdate))
            $msg = "updated";
        else
            $msg = "Record not found.";
    }

    public function orderLogs($id, $action)
    {
        $orderLogData = [];
        //echo $id;
        if (isset($id) && is_numeric($id)) {
            if ($action == 'gift_card') {
                $orderData = UserOrder::where('id', $id)
                    ->select('id', 'fname', 'lname', 'restaurant_id')
                    ->with([
                        'details' => function ($query) {
                            $query->select('id', 'user_order_id', 'menu_json');
                        },
                        'details.giftdetails' => function ($query) {
                            $query->select('id', 'order_detail_id', 'status', 'by_user', 'bonus_for', 'card_type', 'name', 'gift_unique_code', 'updated_at')->where('status', 1);
                        },
                        'details.giftdetails.user' => function ($query) {
                            $query->select('id', 'name');
                        },
                        'restaurant' => function ($query) {
                            $query->select('id', 'restaurant_name');
                        }

                    ])->first();
                $orderLogData = [];
                if (isset($orderData->details)) {
                    $resultData = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);
                    $parent_restaurant_name = isset($resultData['restaurantParentName']) ? $resultData['restaurantParentName'] : $orderData->restaurant->restaurant_name;
                    /*$fname = $orderData->fname;
                    $lname = $orderData->lname;*/
                    foreach ($orderData->details as $detail) {
                        if (isset($detail->giftdetails)) {
                            foreach ($detail->giftdetails as $giftCard) {
                                /*if ($giftCard->card_type == 'bonus' && $giftCard->bonus_for == 'myself') {
                                    $name = ucwords($fname . ' ' . $lname);
                                } else {
                                    $name = ucwords($giftCard->name);
                                }*/
                                $orderLogData[] = [
                                    'id' => $giftCard->id,
                                    'restaurant_id' => $orderData->restaurant_id,
                                    'gift_unique_code' => $giftCard->gift_unique_code,
                                    'updated_at' => $giftCard->updated_at,
                                    'name' => $parent_restaurant_name, //$giftCard->user->name,
                                    'status' => $giftCard->status ? 'Confirmed' : '',
                                ];
                            }
                        }
                    }
                }
            } else {
                $orderLogData = CmsUserLog::where('updated_id', $id)->paginate(20);
            }
            //dd($orderLogData);
            return view('user_order.logdetails', compact('orderLogData', 'action'));

        } else {
            return Redirect::back()->with('message', 'Invalid Order Id');
        }

    }

    /* Export Orders Both Mechandise OR Food Active / Archive Orders
    /?key_search=121&order_status=active&order_type=food&status=active
    */

    public function export(Request $request)
    {
        $statusKey = '';
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        if ($request->input('order_status') != 'undefined' && $request->input('export_type') == 'filtered') {
            /* Active Food Orders / Merchanside */
            if ($request->input('status') == 'active') {
                $statusArray = array("confirmed", "ready", "placed");
                /* Archive Food / Merchanside Orders */
            } elseif ($request->input('status') == 'archive') {
                $statusArray = array("archived", "cancelled", "arrived", "sent", "rejected", "refunded");
            }

            if ($request->input('order_status')) {
                $statusKey = $request->input('order_status');
                $statusArray = array($statusKey);
            }
        } else {
            $statusArray = array("confirmed", "ready", "archived", "cancelled", "arrived", "sent", "rejected", "refunded", "placed");
        }

        $orderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.email', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.order_type', 'user_orders.total_amount', 'promocode_discount', 'deal_discount', 'tip_amount', 'order_amount', 'stripe_charge_id', 'payment_gatway', 'remarks', 'user_comments', 'redeem_point', 'pay_via_point', 'user_ip', 'user_orders.created_at',
            'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update', 'user_orders.status', 'user_orders.product_type', 'restaurant_name', 'restaurants_comments', 'delivery_charge', 'zipcode', 'user_orders.city', 'user_orders.state', 'user_orders.tax', 'user_orders.address_label', 'user_orders.is_guest')
            ->with(['restaurant' => function ($query) {
                $query->leftJoin('delivery_services', 'delivery_services.id', '=', 'restaurants.delivery_provider_id')
                    ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                    ->select('cities.id', 'city_name', 'restaurants.id', 'delivery_provider_id', 'delivery_services.provider_name', 'restaurants.restaurant_name', 'restaurants.address', 'restaurants.street', 'restaurants.zipcode');
            }, 'user' => function ($query) {
                $query->select('users.id', 'users.fname', 'users.lname', 'users.email', 'users.mobile');
            }])
            ->whereDate('created_at', '>=', $request->input('from_date'))
            ->whereDate('created_at', '<=', $request->input('to_date'))
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC');

        if ($request->input('order_status') != 'undefined') {
            $orderData = $orderData->where('user_orders.product_type', '=', $request->input('order_type'));
        }

        if ($request->input('key_search') && $request->input('export_type') == 'filtered') {
            $keySearch = $request->input('key_search');
            $orderData = $orderData->where(function ($query) use ($keySearch) {
                $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.lname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(fname," ",lname)'), 'LIKE', '%' . $keySearch . '%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%');
                if (is_numeric($keySearch)) {
                    $query->orWhere('user_orders.id', $keySearch);
                }
            });

        }

        $orderData = $orderData->get();

        // echo "<pre>";
        // print_r($orderData->toArray()); die;

        $filename = 'Report_' . time() . '.csv';
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment;filename="' . $filename . '";');
        $file = fopen('php://output', 'w');

        $columns = array(
            "Transaction #",
            "Order Category",
            "Customer Name",
            "Order Date",
            "Restaurant Ordered From",
            "Location Ordered From",
            "Payment Receipt",
            "Phone",
            "Email",
            "Order Type",
            "Total $ Amount of Transaction AFTER Discount",
            "Coupon or Discount Amount ($)",
            "deal_discount",
            "Tip $ Amount",
            "Delivery $ Charges",
            "Tax $ Amount",
            "Pay Via Point",
            "Order $ Refunded Amount",
            "Order $ Amount Final",
            "STRIPE Charge Id",
            "Payment Gatway",
            "Remarks",
            "user_comments",
            "Redeem Point",
            "User Ip",
            "Delivery / Carryout Date",
            "Delivery / Carryout Time",
            "Manual Update",
            "Status",
            "Delivery Provider",
            "City",
            "State"
        );

        fputcsv($file, $columns);

        foreach ($orderData as $guest) {

            $resultData = CommonFunctions::getRestaurantDetailsById($guest->restaurant_id);
            $parent_restaurant_name = isset($resultData['restaurantParentName']) ? $resultData['restaurantParentName'] : $orderData->restaurant->restaurant_name;

            if ($guest->order_type == 'delivery') {
                $address = $guest->address;
                if ($guest->address2) {
                    $address = $address . ' ' . $guest->address2;
                }
                $address = $address . ' ' . $guest->city . ' ' . $guest->state . ' ' . $guest->zipcode;
                if ($guest->address_label) {
                    $address = $guest->address_label . ' ' . $address;
                }
                $city = $guest->city;
            } else {
                $city = $guest->restaurant->city_name;
                $address = $guest->restaurant->restaurant_name . ' ' . $guest->restaurant->address . ' ' . $guest->restaurant->street . ' ' . $guest->restaurant->zipcode;
            }

            if ($guest->is_guest) {
                $name = $guest->fname . ' ' . $guest->lname;
                $email = $guest->email;
                $phone = $guest->phone;
            } else {
                if (isset($guest->user->fname)) {
                    $name = $guest->user->fname . ' ' . $guest->user->lname;
                    $email = $guest->user->email;
                    $phone = $guest->user->mobile;
                } else {
                    $name = $guest->fname . ' ' . $guest->lname;
                    $email = $guest->email;
                    $phone = $guest->phone;
                }
            }

            $refunded_amount = DB::table('refund_histories')->where('order_id', $guest->id)->sum('amount_refunded');

            $data = array(
                $guest->id,
                $guest->product_type,
                ucwords($name),
                explode(' ', $guest->created_at)['0'],
                $parent_restaurant_name,
                $guest->restaurant->restaurant_name,
                $guest->payment_receipt,
                $phone,
                $email,
                ucfirst($guest->order_type),
                $guest->order_amount,
                $guest->promocode_discount,
                $guest->deal_discount,
                $guest->tip_amount,
                $guest->delivery_charge,
                $guest->tax,
                $guest->pay_via_point,
                $refunded_amount ? number_format($refunded_amount, 2) : 'NA',
                $guest->total_amount,
                $guest->stripe_charge_id,
                $guest->payment_gatway,
                $guest->remarks,
                $guest->user_comments,
                $guest->redeem_point,
                $guest->user_ip,
                $guest->delivery_date,
                $guest->delivery_time,
                $guest->manual_update,
                ucfirst($guest->status),
                ucfirst($guest->restaurant->provider_name),
                ucfirst($city),
                ucfirst($guest->state),
            );
            fputcsv($file, $data);
        }
        fclose($file);

        exit;
    }

    public function getGiftCardListing(Request $request)
    {
        // Classic cards option commented and not coded for it due to requirement & "tonight" deadline
        $statusKey = '';
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $statusArray = array("placed", "confirmed", "ready");
        if ($request->input('order_status')) {
            $statusArray = [];
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }
        $giftOrderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.fname',
            'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.order_type', 'user_orders.total_amount',
            'user_orders.created_at', 'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update',
            'user_orders.status', 'user_orders.product_type', DB::raw('COUNT(user_order_details.id) AS num_item'), DB::raw('SUM(user_order_details.quantity) AS quantity'))
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->groupBy('user_orders.id', 'user_orders.user_id', 'user_orders.order_type', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.total_amount', 'user_orders.created_at',
                'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update', 'user_orders.status', 'user_orders.product_type')
            ->where('user_orders.product_type', '=', "gift_card")//->where('restaurant_id', \Auth::user()->restaurant_id)
            ->with('details');

        if ($request->input('key_search')) {
            $keySearch = $request->input('key_search');
            $giftOrderData = $giftOrderData->where(function ($query) use ($keySearch) {
                $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.lname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(fname," ",lname)'), 'LIKE', '%' . $keySearch . '%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%');
                if (is_numeric($keySearch)) {
                    $query->orWhere('user_orders.id', $keySearch);
                }
            });

        }
        //dd($giftOrderData->get());
        $giftOrderData = $giftOrderData->paginate(10);
        return view('gift_cards.index', compact('giftOrderData', 'statusKey'));
    }

    public function getGiftCardDetail($id)
    {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        // Classic cards option commented and not coded for it due to requirement & "tonight" deadline
        $userOrder = UserOrder::where('id', $id)->with('details')->first();

        $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $userOrder->restaurant_id, 'datetime' => $userOrder->created_at)));
        $orderDate = date('l d M, h:i A', $timestamp2);

        $giftCardCouponDetails = [];
        if ($userOrder->details) {
            $giftDetails = $userOrder->details->toArray();
            $orderDetailsIds = array_filter(array_column($giftDetails, 'id'));

            $giftCardCouponDetails = UserOrderDetail::select('gift_card_coupons.id', 'user_order_id', 'order_detail_id', 'gift_unique_code', 'menu_json', 'name',
                'gift_card_coupons.email', 'gift_card_coupons.phone', 'floor_building', 'gift_card_coupons.city', 'gift_card_coupons.state', 'zip', 'unit_price',
                'user_order_details.created_at', 'gift_card_coupons.type', 'gift_card_coupons.amount', 'gift_card_coupons.status', 'gift_card_coupons.amount',
                'gift_card_coupons.card_type', 'gift_card_coupons.bonus_for', 'user_orders.fname', 'user_orders.lname', 'user_orders.email AS email_address')
                ->leftJoin('gift_card_coupons', 'gift_card_coupons.order_detail_id', '=', 'user_order_details.id')
                ->leftJoin('user_orders', 'user_orders.id', '=', 'user_order_details.user_order_id')
                ->whereIn('user_order_details.id', $orderDetailsIds)
                ->get();

            $allGiftCards = [];
            $allGiftStatus = 1;
            foreach ($giftCardCouponDetails as $giftCard) {
                $menuJson = !empty($giftCard->menu_json) ? json_decode($giftCard->menu_json) : '';
                $userComment = isset($menuJson->comment) ? $menuJson->comment : '';
                if ($giftCard->card_type == 'bonus' && $giftCard->bonus_for == 'myself') {
                    $name = ucwords($giftCard->fname . ' ' . $giftCard->lname);
                    $email = $giftCard->email_address;
                } else {
                    $name = ucwords($giftCard->name);
                    $email = $giftCard->email;
                }
                if ($giftCard->status == 0) {
                    $allGiftStatus = 0;
                }
                $allGiftCards[] = [
                    'id' => $giftCard->id,
                    'user_order_id' => $giftCard->user_order_id,
                    'order_detail_id' => $giftCard->order_detail_id,
                    'gift_unique_code' => $giftCard->gift_unique_code,
                    // as suggested by Pravish & Rahul P.
                    'card_type' => $giftCard->card_type == 'bonus' ? 'eGift Card - Bonus' : 'eGift Card',
                    'status' => $giftCard->status,
                    'amount' => $giftCard->amount,
                    'user_comment' => $userComment,
                    'name' => $name,
                    'email' => $email,
                    'phone' => $giftCard->phone,
                    'floor_building' => $giftCard->floor_building,
                    'city' => $giftCard->city,
                    'state' => $giftCard->state,
                    'address' => $giftCard->address,
                    'zip' => $giftCard->zip,
                    'unit_price' => $giftCard->amount,
                    'created_at' => $giftCard->created_at,
                    'type' => $giftCard->type,
                ];
            }
        }
        $userId = $userOrder->user_id;
        $userEmail = $userOrder->email;

        #@23-11-18 RG if user_id is there then check with user_id else with email id if Guest User and if not registered
        $userOrderObj = new UserOrder();
        $userOrders = $userOrderObj->whereIn('restaurant_id', $restArray)->whereNotIn('status', ['pending']);
        if ($userId) {
            $userOrders = $userOrders->where('user_id', $userId);
        } else {
            $userOrders = $userOrders->where('email', $userEmail);
        }
        $userOrderCount = $userOrders->get()->count();

        $resvObj = new Reservation();
        $userResvCount = count($resvObj->getReservationByUser($userId, $userEmail, $restArray));

        //dd($giftCardCouponDetails->toArray());
        //dd($allGiftCards);
        $orderData = $userOrder;
        $action = 'gift_card';
        $total_refunded_amount = 0;
        $refund_reason = array();
        $refunded_amount_data = DB::table('refund_histories')->where('order_id', $id)->select('amount_refunded', 'reason')->get();
        if ($refunded_amount_data) {
            foreach ($refunded_amount_data as $key => $amount_data) {
                $total_refunded_amount = $total_refunded_amount + $amount_data->amount_refunded;
                $refund_reason[$key] = array('amount' => $amount_data->amount_refunded, 'reason' => $amount_data->reason);
            }
        }
        return view('gift_cards.detail', compact('userOrder', 'allGiftCards', 'userOrderCount', 'userResvCount', 'allGiftStatus', 'orderDate', 'orderData', 'action', 'total_refunded_amount', 'refund_reason'));
    }

    public function getGiftCardArchiveDetail($id)
    {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        // Classic cards option commented and not coded for it due to requirement & "tonight" deadline
        $userOrder = UserOrder::where('id', $id)->with('details')->first();
        $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $userOrder->restaurant_id, 'datetime' => $userOrder->created_at)));
        $orderDate = date('l d M, h:i A', $timestamp2);

        $giftCardCouponDetails = [];
        if ($userOrder->details) {
            $giftDetails = $userOrder->details->toArray();
            $orderDetailsIds = array_filter(array_column($giftDetails, 'id'));

            $giftCardCouponDetails = UserOrderDetail::select('gift_card_coupons.id', 'user_order_id', 'order_detail_id', 'gift_unique_code', 'menu_json', 'name',
                'gift_card_coupons.email', 'gift_card_coupons.phone', 'floor_building', 'gift_card_coupons.city', 'gift_card_coupons.state', 'zip', 'unit_price',
                'user_order_details.created_at', 'gift_card_coupons.type', 'gift_card_coupons.amount', 'gift_card_coupons.status', 'gift_card_coupons.amount',
                'gift_card_coupons.card_type', 'gift_card_coupons.bonus_for', 'user_orders.fname', 'user_orders.lname', 'user_orders.email AS email_address')
                ->leftJoin('gift_card_coupons', 'gift_card_coupons.order_detail_id', '=', 'user_order_details.id')
                ->leftJoin('user_orders', 'user_orders.id', '=', 'user_order_details.user_order_id')
                ->whereIn('user_order_details.id', $orderDetailsIds)
                ->get();

            $allGiftCards = [];
            $allGiftStatus = 1;
            foreach ($giftCardCouponDetails as $giftCard) {
                $menuJson = !empty($giftCard->menu_json) ? json_decode($giftCard->menu_json) : '';
                $userComment = isset($menuJson->comment) ? $menuJson->comment : '';
                if ($giftCard->card_type == 'bonus' && $giftCard->bonus_for == 'myself') {
                    $name = ucwords($giftCard->fname . ' ' . $giftCard->lname);
                    $email = $giftCard->email_address;
                } else {
                    $name = ucwords($giftCard->name);
                    $email = $giftCard->email;
                }
                if ($giftCard->status == 0) {
                    $allGiftStatus = 0;
                }
                $allGiftCards[] = [
                    'id' => $giftCard->id,
                    'user_order_id' => $giftCard->user_order_id,
                    'order_detail_id' => $giftCard->order_detail_id,
                    // as suggested by Pravish & Rahul P.
                    'card_type' => $giftCard->card_type == 'bonus' ? 'eGift Card - Bonus' : 'eGift Card',
                    'gift_unique_code' => $giftCard->gift_unique_code,
                    'status' => $giftCard->status,
                    'amount' => $giftCard->amount,
                    'user_comment' => $userComment,
                    'name' => $name,
                    'email' => $email,
                    'phone' => $giftCard->phone,
                    'floor_building' => $giftCard->floor_building,
                    'city' => $giftCard->city,
                    'state' => $giftCard->state,
                    'address' => $giftCard->address,
                    'zip' => $giftCard->zip,
                    'unit_price' => $giftCard->amount,
                    'created_at' => $giftCard->created_at,
                    'type' => $giftCard->type,
                ];
            }
        }
        $userId = $userOrder->user_id;
        $userEmail = $userOrder->email;

        #@23-11-18 RG if user_id is there then check with user_id else with email id if Guest User and if not registered
        $userOrderObj = new UserOrder();
        $userOrders = $userOrderObj->whereIn('restaurant_id', $restArray)->whereNotIn('status', ['pending']);
        if ($userId) {
            $userOrders = $userOrders->where('user_id', $userId);
        } else {
            $userOrders = $userOrders->where('email', $userEmail);
        }
        $userOrderCount = $userOrders->get()->count();

        $resvObj = new Reservation();
        $userResvCount = count($resvObj->getReservationByUser($userId, $userEmail, $restArray));
        $orderData = $userOrder;
        $total_refunded_amount = 0;
        $refund_reason = array();
        $refunded_amount_data = DB::table('refund_histories')->where('order_id', $id)->select('amount_refunded', 'reason')->get();
        if ($refunded_amount_data) {
            foreach ($refunded_amount_data as $key => $amount_data) {
                $total_refunded_amount = $total_refunded_amount + $amount_data->amount_refunded;
                $refund_reason[$key] = array('amount' => $amount_data->amount_refunded, 'reason' => $amount_data->reason);
            }
        }
        $action = 'gift_card';
        return view('gift_cards.detail', compact('userOrder', 'allGiftCards', 'userOrderCount', 'userResvCount', 'allGiftStatus', 'orderDate', 'total_refunded_amount', 'orderData', 'action', 'refund_reason'));
    }

    public function giftCardArchiveOrder(Request $request)
    {
        // Archive Gift Card Orders
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $statusArray = array("archived", "cancelled", "arrived", "sent", "rejected", "refunded"); // as discussed with Rahul/Pravish
        $orderStatus = array("" => "All Archived", "archived" => "Archived", "cancelled" => "Cancelled", "rejected" => "Rejected", "refunded" => "Refunded");
        if ($request->input('order_status')) {
            $statusArray = [];
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }
        $statusKey = '';
        $giftOrderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.address', 'user_orders.phone', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at',
            'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type', DB::raw('COUNT(user_order_details.id) AS num_item'), DB::raw('SUM(user_order_details.quantity) AS quantity'), 'user_order_details.item')
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.id', 'DESC')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->groupBy('user_orders.id', 'user_orders.user_id', 'user_orders.address', 'user_orders.phone', 'user_orders.order_type', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.total_amount', 'user_orders.created_at', 'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type');

        $giftOrderData = $giftOrderData->where('user_orders.product_type', '=', "gift_card");
        if ($request->input('key_search')) {
            $keySearch = $request->input('key_search');
            $giftOrderData = $giftOrderData->where(function ($query) use ($keySearch) {
                $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.lname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(fname," ",lname)'), 'LIKE', '%' . $keySearch . '%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%');
                if (is_numeric($keySearch)) {
                    $query->orWhere('user_orders.id', $keySearch);
                }
            });

        }
        $giftOrderData = $giftOrderData->paginate(10);

        return view('gift_cards.product-archive', compact('giftOrderData', 'orderStatus', 'statusKey'));
    }

    public function giftExport(Request $request)
    {
        $statusKey = '';
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        /* Active Food Orders / Merchanside */
        if ($request->input('gift_type') == 'gift-card') {
            $statusArray = array("placed", "confirmed", "ready");
            /* Archive Food / Merchanside Orders */
        } elseif ($request->input('gift_type') == 'gift-archive') {
            $statusArray = array("archived", "cancelled", "arrived", "sent", "rejected", "refunded");
        }

        /*if($request->input('order_status')){
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }*/

        $orderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.created_at', 'user_orders.product_type')
            ->with(['restaurant' => function ($query) {
                $query->leftJoin('delivery_services', 'delivery_services.id', '=', 'restaurants.delivery_provider_id')->select('restaurants.id', 'delivery_provider_id', 'delivery_services.provider_name');
            }])
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->where('user_orders.product_type', '=', 'gift_card')
            ->whereIn('user_orders.status', $statusArray)
            ->orderBy('user_orders.id', 'DESC')
            ->with('details');
        $giftType = $request->input('gift_type');
        if ($giftType == 'gift-archive') {
            $orderData->where('status', config('constants.order_status.archived'));
        }

        if ($request->input('key_search')) {
            $keySearch = $request->input('key_search');
            $orderData = $orderData->where(function ($query) use ($keySearch) {
                $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.lname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(fname," ",lname)'), 'LIKE', '%' . $keySearch . '%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%');
                if (is_numeric($keySearch)) {
                    $query->orWhere('user_orders.id', $keySearch);
                }
            });

        }
        $orderData = $orderData->get();

        // echo "<pre>";
        // print_r($orderData->toArray()); die;

        $filename = 'Report_' . time() . '.csv';
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment;filename="' . $filename . '";');
        $file = fopen('php://output', 'w');

        $columns = array(
            "Gift Card ID",
            "Expires",
            "Balance",
            "Points",
            "Date Created",
        );

        fputcsv($file, $columns);
        $allGiftCards = [];
        foreach ($orderData as $order) {
            if ($order->details) {
                $giftDetails = $order->details->toArray();
                $orderDetailsIds = array_column($giftDetails, 'id');

                $giftCardCouponDetails = UserOrderDetail::whereIn('user_order_details.id', $orderDetailsIds)
                    ->select('id')
                    ->with(['giftdetails' => function ($query) {
                        $query->select('id', 'order_detail_id', 'gift_unique_code', 'amount', 'created_at');
                    }])
                    ->get();
                foreach ($giftCardCouponDetails as $giftCard) {
                    if (isset($giftCard->giftdetails) && count($giftCard->giftdetails)) {
                        foreach ($giftCard->giftdetails as $gift) {
                            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $order->restaurant_id, 'datetime' => $gift->created_at)));
                            $giftDate = date('Y-m-d h:i:s', $timestamp2);

                            $allGiftCards = [
                                'gift_unique_code' => "\t{$gift->gift_unique_code}" ?? '',
                                'expires' => '',
                                'balance' => $gift->amount,
                                'points' => '',
                                'created_at' => $giftDate ?? '',
                            ];
                            fputcsv($file, $allGiftCards);
                        }
                    }
                }
            }
        }
        fclose($file);

        exit;
    }

    public function giftCardOrderStatusUpdate($giftCardOrderId, $giftCouponId, $giftCardSendAll)
    {
        $resultArr = [];
        $cardType = '';
        if ($giftCardSendAll == 1) {
            // Confirming all gift cards at once
            $orderData = UserOrder::where('id', $giftCardOrderId)
                ->select('id')
                ->with([
                    'details' => function ($query) {
                        $query->select('id', 'user_order_id');
                    },
                    'details.giftdetails' => function ($query) {
                        $query->select('id', 'order_detail_id', 'status');
                    }
                ])->first();
            $giftCardAllIdsArr = [];
            if (isset($orderData->details)) {
                foreach ($orderData->details as $detail) {
                    if (isset($detail->giftdetails)) {
                        foreach ($detail->giftdetails as $gift) {
                            if (isset($gift->status)) { // && $gift->status==0
                                $giftCardAllIdsArr[] = $gift->id;
                            }
                        }
                    }
                }
            }
            // update giftCardAllIdsArr with confirmed status
            if (count($giftCardAllIdsArr)) {
                GiftcardCoupons::whereIn('id', $giftCardAllIdsArr)->update(['status' => 1]);
                // update the main order status as confirmed
                $userOrder = UserOrder::where('id', $giftCardOrderId)->update(['status' => config('constants.order_status.archived')]);
            }
        } elseif (is_numeric($giftCouponId) && $giftCouponId > 0) {
            // Confirming single gift card
            $giftCard = GiftcardCoupons::where('id', $giftCouponId)->first();
            $cardType = isset($giftCard->card_type) ? $giftCard->card_type : '';
            $giftCard->status = 1;
            $giftCard->save();
        }

        // check if all gift cards for this order has be confirmed, mark the order confirmed
        $orderData = UserOrder::where('id', $giftCardOrderId)
            ->select('id')
            ->with([
                'details' => function ($query) {
                    $query->select('id', 'user_order_id');
                },
                'details.giftdetails' => function ($query) {
                    $query->select('id', 'order_detail_id', 'status');
                }
            ])->first();

        $allStatusConfirmedFlag = 1;
        if (isset($orderData->details)) {
            foreach ($orderData->details as $detail) {
                if (isset($detail->giftdetails)) {
                    foreach ($detail->giftdetails as $gift) {
                        if (isset($gift->status) && $gift->status == 0) {
                            $allStatusConfirmedFlag = 0;
                        }
                    }
                }
            }
        }
        $resultArr = [
            'all_confirmed' => $allStatusConfirmedFlag,
            'card_type' => $cardType,
        ];

        return $resultArr;
    }


    /**
     * Get a particular customer
     * @param $charge , amount, reason, refund_application_fee
     * @return Customer
     * 19-Dec-2018 by RG
     */

    public function refundOrder($id, $action)
    {
        $orderData = UserOrder::where('id', $id)->with('user')->where('product_type', '=', $action)->first();
        if (empty($orderData)) {
            return Redirect::back()->with('message', 'Invalid Id');
        } else {
            $refunded_amount = DB::table('refund_histories')->where('order_id', $id)->sum('amount_refunded');
            return view('user_order.refund', compact('orderData', 'action', 'refunded_amount'));
        }
    }

    /**
     * Get a particular customer
     * @param order_id
     * refund_mode = partially / fully
     * refund_type = F / P    (Flat OR Percentage) For Now you can send F
     * refund_value = 10   If flat then 10 means total $10 will be refunded and if P then 10% of order_total will be refunded
     * reason = XYZ
     * host_name =
     * @return Refund success OR fail
     * 19-Dec-2018 by RG
     */

    public function releaseOrderRefund(Request $request)
    {
        $errors = array();
        $status = Config('constants.status_code.BAD_REQUEST');
        $success = "Refund completed successfully.";
        $product_type = '';
        $order_id = '';
        #return response()->json(['data' => $userOrder, 'errors' => $errors, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        if ($request->input('refund_mode') == 'partially') {
            if (!$request->has('refund_value')) {
                $errors[] = 'Partial amount is mandatory.';
            } else {
                if ($request->input('refund_value') == '') {
                    $errors[] = 'Partial amount is mandatory.';
                } else {
                    if ($request->input('refund_value') < 1) {
                        $errors[] = 'Please enter the amount greater than $1.';
                    }
                }
            }
        }

        if ($request->input('host_name') == '') {
            $errors[] = 'Host name is mandatory.';
        }

        if ($request->input('reason') == '') {
            $errors[] = 'Reason is mandatory.';
        } else {
            if ($request->input('reason') == 'other') {
                if (!$request->has('reason_other')) {
                    $errors[] = 'Reason is mandatory.';
                } else {
                    if ($request->input('reason_other') == '') {
                        $errors[] = 'Reason is mandatory.';
                    } else {
                        $refund_reason = $request->input('reason_other');
                    }
                }

            } else {
                $refund_reason = $request->input('reason');
            }
        }

        if (empty($errors) && $request->has('oid') && is_numeric($request->input('oid'))) {
            $orderData = UserOrder::where('id', $request->input('oid'))->whereNotIn('status', ['refunded', 'pending'])->first();
            if ($orderData) {
                $product_type = $orderData->product_type;
                $order_id = $request->input('oid');
                $action = $orderData->status;
                if ($request->input('refund_mode') && $request->input('refund_mode') == 'fully') {
                    $action = 'refunded';
                    $refund_value = $orderData->total_amount;
                    $amount_refunded = $refund_value = $orderData->total_amount;

                } else {
                    if ($request->input('refund_type') == 'F') {
                        $refund_value = $request->input('refund_value');
                        $amount_refunded = $request->input('refund_value');
                    } else {
                        $amount_refunded = ($request->input('refund_value') * $orderData->total_amount) / 100;
                        $refund_value = $request->input('refund_value'); //100% is being send
                    }
                }

                if ($amount_refunded <= $orderData->total_amount) {
                    $globalSetting = DB::table('global_settings')->whereNotNull('bcc_email')->whereNotNull('bcc_name')->first();

	            $parentRestaurant = DB::table('restaurants')->where('id',$orderData->restaurant_id)->first();
                    $refundResponse = $this->refundInitatedOnPaymentGateway($orderData, $amount_refunded, $action,$parentRestaurant);
                    if (!isset($refundResponse['errorMessage'])) {
                        #Refund Data Saved

                        $refund_data = array(
                            'order_id' => $orderData->id,
                            'refund_type' => $request->input('refund_type'),
                            'refund_value' => $refund_value,
                            'amount_refunded' => $amount_refunded,
                            'charge_id' => isset($refundResponse->charge) ? $refundResponse->charge : $orderData->stripe_charge_id,
                            'refund_id' => isset($refundResponse->id) ? $refundResponse->id : 0,
                            'reason' => $refund_reason,
                            'host_name' => $request->input('host_name'),
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                        );

                        //RefundHistory::create($refund_data);

                        DB::table('refund_histories')->insert($refund_data);

                        $total_refunded_amount = DB::table('refund_histories')->where('order_id', $orderData->id)->sum('amount_refunded');

                        $previousData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
                        $previous_action_data = json_encode($previousData);
                        $oldOrderStatus = $orderData->status;
                        $orderData->updated_at = now();
                        $orderData->status = $action;
                        $orderData->total_amount = $orderData->total_amount - $amount_refunded;
                        if ($orderData->total_amount <= 0) {
                            $orderData->status = 'refunded';

                        }

                        $orderData->save();

                        $restaurantData = Restaurant::where(array('id' => $orderData->restaurant_id))->first();
                        $restaurantData1 = $restaurantData->toArray();

                        $deliveryServicesData = DeliveryService::where(array('id' => $restaurantData->delivery_provider_id))->first();

                        if ($deliveryServicesData && $deliveryServicesData->provider_name == "twinjet") {
                            $orderDataArray = $orderData->toArray();

                            // if($oldOrderStatus == 'placed'){
                            //     $orderDetail = new UserOrderDetail();
                            //     $orderDetailsData = $orderDetail->getOrderDetail($orderData->id);
                            //     $orderDetailsData1 = $orderDetailsData->toArray();

                            //     $orderDataArray['item_list'][] = $orderDetailsData1;
                            //     $responseData = $this->submitOrderToVendor($orderDataArray,$restaurantData1,1,$restaurantData->delivery_provider_id);
                            // }else{

                            if ($request->input('refund_mode') != 'partially') {
                                if ($orderData->order_type == "delivery" && ($oldOrderStatus != "delivered" && $oldOrderStatus != "archived")) {
                                    $responseData = $this->submitOrderToVendor($orderDataArray, $restaurantData1, 2, $restaurantData->delivery_provider_id);
                                }
                            } else {
                                if ($orderData->total_amount == 0 && $orderData->order_type == "delivery" && ($oldOrderStatus != "delivered" && $oldOrderStatus != "archived")) {

                                    $this->submitOrderToVendor($orderDataArray, $restaurantData1, 2, $restaurantData->delivery_provider_id);
                                } else {
                                    //$deliveryServicesData = DeliveryService::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                                    if ($orderData->order_type == "delivery") { //trigger twinjet
                                        $orderDetail = new UserOrderDetail();
                                        $orderDetailsData = $orderDetail->getOrderDetail($orderData->id);
                                        $orderDetailsData1 = $orderDetailsData->toArray();
                                        $orderDataArray['item_list'][] = $orderDetailsData1;
                                        $responseData = $this->submitOrderToVendor($orderDataArray, $restaurantData1, 0, $deliveryServicesData->id);
                                    }
                                }
                            }
                            // }
                        }//end twinjet
                        $langId = config('app.language')['id'];
                        CommonFunctions::$langId = $langId;

                        $orderItem = $label = "";

                        $user_id = Auth::user()->id;
                        $updateData = array("restaurant_name" => $orderData->restaurant_name, "fname" => $orderData->fname, "lname" => $orderData->lname, "email" => $orderData->email, "order_id" => $orderData->id, "order_type" => $orderData->order_type, "total_amount" => $orderData->total_amount, "delivery_time" => $orderData->delivery_time, "delivery_date" => $orderData->delivery_date, "status" => $orderData->status);
                        $action_data = json_encode($updateData);
                        $moduleName = '';
                        if ($orderData->product_type == 'product') {
                            $moduleName = 'Merchandise Order';
                        } else if ($orderData->product_type == 'food_item') {
                            $moduleName = 'Food Order';
                        }
                        $logData = array(
                            'user_id' => $user_id,
                            'updated_id' => $orderData->id,
                            'module_name' => $moduleName,
                            'restaurant_id' => Auth::user()->restaurant_id, //$orderData->restaurant_id,   // PE-3221 - restaurant id should be based who's logged in Branch/Brand
                            'tablename' => 'user_orders',
                            'action_type' => 'update',
                            'previous_action_data' => $previous_action_data,
                            'action_data' => $action_data);

                        //return $logData;
                        $logs = CmsUserLog::create($logData);
                        if (1) {
                            $resultData = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);
                            $config['to'] = $orderData->email;
                            $config['subject'] = "Your order has been cancelled";
                            $config['from_address'] = $resultData['custom_from'];
                            //$config['from_name'] = $resultData['restaurantParentName'];
                            $config['from_name'] = ucfirst($restaurantData->restaurant_name);

                            if ($globalSetting) {

                                $config['bcc'] = $globalSetting->bcc_email;
                            }

                            $controller = new NotificationController;

                            $is_asap_order = "";
                            if ($orderData->is_asap_order === 1)
                                $is_asap_order = "(Possibly sooner!)";

                            $timestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');
                            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));

                            $cardInfo = "<br /> Credit Card <br />" . $orderData->card_type . ' (' . $orderData->card_number . ')';
                            if ($orderData->payment_gatway == 'Card') {
                                $cardInfo = "<br /> Credit Card <br />" . $orderData->card_type . ' (' . $orderData->card_number . ')';
                            } elseif ($orderData->payment_gatway == 'Cash') {
                                $cardInfo = "Cash";
                            } else {
                                $cardInfo = $orderData->payment_gatway;
                            }
                            if (Auth::user()) {
                                $curSymbol = Auth::user()->restaurant->currency_symbol;
                            } else {
                                $curSymbol = config('constants.currency');
                            }
                            //$cardInfo = ($orderData->payment_gateway == "Card")?"<b>Credit Card:</b> <br />".$cardInfo:$orderData->payment_gateway;
                            $mailKeywords = array(
                                'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                                'RESTAURANT_ADD' => $restaurantData->address,
                                'ORDER_NUMBER' => $orderData->payment_receipt,
                                'USER_NAME' => ucfirst($orderData->fname),
                                'ORDER_TYPE' => ucfirst($orderData->order_type),
                                'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                                'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp) . ' ' . $is_asap_order,
                                'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                                'SUB_TOTAL' => '<span>' . $curSymbol . '</span>' . $orderData->order_amount,
                                'TOT_AMOUNT' => '<span>' . $curSymbol . '</span>' . number_format($orderData->total_amount, 2),
                                'REFUND_AMOUNT' => '<span>' . $curSymbol . '</span>' . number_format($total_refunded_amount, 2),
                                'PAYMEN_INFO' => $cardInfo,
                                'CARD_NUMBER' => $cardInfo,
                                'REASON' => $refund_reason,
                                'URL' => $restaurantData->source_url,
                                'REST_NAME' => strtolower($restaurantData->restaurant_name),
                                'SITE_URL' => $restaurantData->source_url,
                                'title' => "Cancel Order",
                                'APP_BASE_URL' => config('constants.mail_image_url'),
                            );

                            /*************************************************************************************************/
                            $mailKeywords = CommonFunctions::getAllChargesForMailer($mailKeywords, $orderData, $curSymbol);
                            /**************************************************************************************************/
                            $mailKeywords['DISPLAY_REFUND'] = 'none';
                            if ($total_refunded_amount > 0) {
                                $mailKeywords['DISPLAY_REFUND'] = 'table';
                            }

                            if ($orderData->user_comments != "") {
                                $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>' . $orderData->user_comments . '</i>';
                            } else {
                                $mailKeywords['SPECIAL_INFO'] = '';
                            }
                            if ($orderData->status == 'refunded') {
                                $mailKeywords['HEAD'] = "We've initiated a refund for your experience with us on ";
                            } else {
                                $mailKeywords['HEAD'] = "We've initiated a partial refund for your experience with us on ";
                            }
                            if ($orderData->product_type != 'gift_card') {

                                $order_refund_date = date('M d, Y h:i A', $timestamp);
                                $orderDetail = new UserOrderDetail();
                                $orderDetailData = $orderDetail->getOrderDetail($orderData->id);

                                foreach ($orderDetailData as $data) {
                                    $addon_modifier = CommonFunctions::getAddonsAndModifiers($data);
                                    $addon_modifier_html = CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);

                                    $instruct = '';

                                    $addons_data = [];
                                    $bypData = json_decode($data->menu_json, true);

                                    if (!is_null($bypData)) {
                                        $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                                    }

                                    $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody> 
                                <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                <b>' . $data->quantity . ' ' . $data->item . '</b>';

                                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                                    if ($orderData->product_type == 'product') {
                                        if (!is_null($data->item_other_info)) {

                                            if (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) {
                                                $label = $data->item_size . ' ';
                                            } else {
                                                $label = "";
                                            }
                                            $item_other_info = json_decode($data->item_other_info);
                                            $value3 = '';
                                            $value = $item_other_info->is_gift_wrapping == 1 ? 'Yes' : 'No';
                                            $value2 = 'Gift Wrapping: ' . $value;
                                            if (isset($item_other_info->gift_wrapping_message) && !empty($item_other_info->gift_wrapping_message) && $item_other_info->gift_wrapping_message != 'null') {
                                                $value3 = 'Message: ' . $item_other_info->gift_wrapping_message;
                                            }
                                            $label .= $value2 . '<br/>' . $value3;
                                        }
                                        if ($data->is_byp) {
                                            $label = '';
                                            $label = CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                                        } else {
                                            $label .= $addon_modifier_html;
                                        }

                                    } else {
                                        //$label = strtolower($data->size) != 'full' ? $data->size . ' ' : '';
                                        if (strtolower($data->item_size) != 'full' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) {
                                            $label = $data->item_size . ' ';
                                        } else {
                                            $label = "";
                                        }
                                        // if ($data->is_byp == 1 && $result) {
                                        //     foreach ($result as $key => $data1) {
                                        //         $label .= $key;
                                        //         if($data1!="")
                                        //         $label .= '[' . $data1 . ']>';
                                        //     }

                                        // }
                                        // if ($data->is_byp == 0 && $result) {
                                        //     foreach ($result as $data1) {
                                        //         $label .= $data1['addons_name'];
                                        //         if($data1['addons_option']!="")
                                        //         $label .= '[' . $data1['addons_option'] . ']>';
                                        //     }
                                        // }

                                        if ($data->is_byp) {
                                            $label = '';
                                            $label = CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                                        } else {
                                            $label .= $addon_modifier_html;
                                        }

                                        if (!empty($data->special_instruction)) {
                                            $instruct = '<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">' . $data->special_instruction . '</p>';
                                        }
                                    }


                                    $orderItem .= '<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">' . $label . '</p>' . $instruct . '</th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>' . $curSymbol . '</span>' . $data->total_item_amt . '</p></th></tr></table></th></tr></tbody></table>
                                <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                                }


                                $tipAmount = $orderData->tip_amount;
                                if ($orderData->tip_amount == 0.00) {
                                    $mailKeywords['TIP_AMOUNT'] = '';
                                    $mailKeywords['TIP_TITLE'] = '';
                                    $mailKeywords['DISPLAY_TIP'] = 'none';

                                } else {
                                    $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                                    $mailKeywords['TIP_AMOUNT'] = '<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                                                    <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                        <p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">' . $orderData->tip_percent . '<span>%</span></p>
                                                    </th></tr></table></th>
                                                    <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                                        <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>' . $curSymbol . '</span>' . $tipAmount . '</p></th></tr></table></th>
                                                </tr></tbody></table>';
                                    $mailKeywords['DISPLAY_TIP'] = 'table';
                                }


                                if ($orderData->tax == 0.00) {
                                    $mailKeywords['TAX'] = '';
                                    $mailKeywords['TAX_TITLE'] = '';
                                    $mailKeywords['DISPLAY'] = 'none';
                                } else {
                                    $mailKeywords['TAX'] = '<span>' . $curSymbol . '</span>' . $orderData->tax;
                                    $mailKeywords['TAX_TITLE'] = "Taxes";
                                    $mailKeywords['DISPLAY'] = 'table';
                                }

                                if ($orderData->order_type == "delivery") {

                                    $address = $orderData->address;
                                    if ($orderData->address2) {
                                        $address = $address . ' ' . $orderData->address2;
                                    }
                                    $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;

                                    $mailKeywords['DELIVERY_CHARGE'] = '<span>' . $curSymbol . '</span>' . $orderData->delivery_charge;
                                    $mailKeywords['USER_ADDRESS'] = $address;
                                    $mailTemplate = 'delivery_order_refund';
                                    if ($orderData->product_type == 'product') {
                                        $mailKeywords['DTIME'] = 'style="display:none"';
                                    } else {
                                        $mailKeywords['DTIME'] = '';
                                    }
                                } else {
                                    $mailKeywords['USER_ADDRESS'] = '';
                                    $mailTemplate = 'carryout_order_refund';
                                }
                                $mailKeywords['ITEM_DETAIL'] = $orderItem;
                                $config['subject'] = "Your Refund Is Being Processed";
                                $mailKeywords['HEAD'] = $mailKeywords['HEAD'] . $order_refund_date;
                                #PE-3241 By RG 04-Dec-18
                                $controller->sendMail($mailTemplate, $restaurantData->parent_restaurant_id, $config, $mailKeywords);
                            } elseif ($product_type == "gift_card" && $orderData->email) {
                                $order_refund_date = date('M d, Y h:i A', $timestamp2);
                                $mailKeywords['HEAD'] = $mailKeywords['HEAD'] . $order_refund_date;
                                $parent_restaurant_name = isset($resultData['restaurantParentName']) ? $resultData['restaurantParentName'] : $restaurantData->restaurant_name;

                                $config['subject'] = "Your Refund Is Being Processed";
                                $mailKeywords['REST_NAME'] = ucfirst($parent_restaurant_name);

                                $orderItem = CommonFunctions::getProductList($orderData->id, 'buyer');

                                $mailKeywords['ITEM_DETAIL'] = $orderItem;
                                $mailKeywords['BUYER_NAME'] = $orderData->fname;

                                $mailTemplate = 'gift_card_refund';
                                $controller->sendMail($mailTemplate, $restaurantData->parent_restaurant_id, $config, $mailKeywords);

                            }
                            /******** SMS Send **** @18-12-2018 by RG *****/
                            $sms_keywords = array(
                                'order_id' => $orderData->id,
                                'sms_module' => 'order',
                                'action' => 'refund',
                                'sms_to' => 'customer',
                                'amount_refunded' => $total_refunded_amount
                            );
                            CommonFunctions::getAndSendSms($sms_keywords);
                            /************ END SMS *************/
                        }
                        $status = Config('constants.status_code.STATUS_SUCCESS');
                        $success = "Refund completed successfully.";
                        $errors = [];
                        return response()->json(['errors' => $errors, 'success' => $success, 'order_id' => $order_id, 'product_type' => $product_type], $status);
                        //$errors[] = "Refund completed successfully.";
                    } else {
                        $errors[] = $refundResponse['errorMessage'];
                    }
                } else {
                    $errors[] = 'Sorry, This order does not have sufficient funds to transfer.';
                }
            } else {
                $errors[] = 'Sorry, Either this order is fully refunded. OR Invalid Order ID.';
            }
        }
        return response()->json(['errors' => $errors, 'success' => $success, 'order_id' => $order_id, 'product_type' => $product_type], $status);
        //return Redirect::back()->withSuccess('Language set successfully!');
    }

    public function scheduled_order(Request $request)
    {

        //print_r($request->all); die;

        $orderData = [];
        $statusKey = '';
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        //print_r($restArray); die;
        $statusArray = array("placed", "confirmed", "ready");
        $orderStatus = array("" => "Active Orders", "placed" => "Placed", "confirmed" => "Confirmed", "ready" => "Ready");
        if ($request->input('order_status')) {
            $statusArray = [];
            $statusKey = $request->input('order_status');
            $statusArray = array($statusKey);
        }

        $search_type = array("delivery_datetime" => "Delivery Time", "order_datetime" => "Order Time");

        $orderData = UserOrder::select('user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.email', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at', 'restaurants.restaurant_name', 'restaurants.phone as rphone', 'restaurants.rest_code', 'restaurants.address as raddress', 'restaurants.zipcode as rzipcode', 'restaurants.street as rstreet', 'user_orders.address', 'is_guest', 'user_orders.address2', 'user_orders.state', 'user_orders.zipcode', 'address_label', 'user_orders.is_order_viewed',
            'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update', 'user_orders.status', 'user_orders.product_type', DB::raw('COUNT(user_order_details.id) AS num_item'), 'user_orders.created_at as odat', DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value) AS rest_current_date'), 'cities.id as city_id', DB::raw('concat(user_orders.delivery_date," ",user_orders.delivery_time) as order_delivery_datetime'), DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE) as order_delivery_fdatetime'), 'kpt', DB::raw('CONCAT(user_orders.delivery_date, " ", user_orders.delivery_time) AS delivery_datetime'))
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
            ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
            ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
            ->groupBy('user_orders.id', 'user_orders.user_id', 'user_orders.order_type', 'user_orders.restaurant_id', 'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.total_amount', 'user_orders.created_at',
                'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.manual_update', 'user_orders.status', 'user_orders.product_type');
        $orderData = $orderData->where('user_orders.product_type', '=', "food_item");

        if ($request->has('key_search')) {
            $keySearch = $request->input('key_search');
            $orderData = $orderData->where(function ($query) use ($keySearch) {
                $query->where('user_orders.fname', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.email', 'like', '%' . $keySearch . '%')
                    ->orWhere('user_orders.phone', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(user_orders.fname," ",user_orders.lname)'), 'LIKE', '%' . $keySearch . '%')
                    ->orWhere('user_orders.payment_receipt', 'like', '%' . $keySearch . '%')
                    ->orWhere('users.email', 'like', '%' . $keySearch . '%')
                    ->orWhere('users.mobile', 'like', '%' . $keySearch . '%')
                    ->orWhere(DB::raw('concat(users.fname," ",users.lname)'), 'LIKE', '%' . $keySearch . '%');
                if (is_numeric($keySearch)) {
                    $query->orWhere('user_orders.id', $keySearch);
                }
            });
            $statusArray = array("placed", "confirmed", "ready", "archived", "cancelled", "arrived", "sent", "rejected", "refunded");

        } else {
            $orderData = $orderData->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)'), '<', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)'));
        }
        $orderData = $orderData->whereIn('user_orders.status', $statusArray);
        $default_sort_order = 'delivery_datetime';
        if ($request->has('sortorder') && $request->input('sortorder') && $request->input('sortorder') != '') {
            if ($request->input('sortorder') == 'order_datetime') {
                $default_sort_order = 'order_datetime';
            } elseif (is_numeric($request->input('sortorder'))) {
                $orderData = $orderData->where('user_orders.restaurant_id', $request->input('sortorder'));
            }
        }
        if ($default_sort_order == 'delivery_datetime') {
            $orderData = $orderData->orderBy('delivery_datetime', 'ASC');
        } else {
            $orderData = $orderData->orderBy('user_orders.created_at', 'ASC');
        }
        $orderData = $orderData->paginate(10);
        $groupRestData = CommonFunctions::getRestaurantGroup();
        $default_sort_order = $request->input('sortorder');

        // echo "<pre>";
        // print_r($orderData->toArray()); die;

        return response()->view('user_order.scheduled_order', compact('orderData', 'orderStatus', 'statusKey', 'default_sort_order', 'search_type', 'groupRestData'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    }

    //@27-03-2019 RG
    public function updateprint(Request $request)
    {
        $id = $request->input('oid');
        if (isset($id) && is_numeric($id)) {
            $orderData = UserOrder::find($id);
            $orderData->is_order_printed = 1;
            $orderData->save();
            return response()->json(['success' => true, 'order_id' => $id], 200);
        } else {
            return response()->json(['success' => false, 'order_id' => $id], 200);
        }
    }

    //@27-03-2019 RG
    public function move_to_archive(Request $request)
    {
        $id = $request->input('oid');
        if (isset($id) && is_numeric($id)) {
            $orderData = UserOrder::find($id);
            if ($orderData->order_type == 'delivery') {
                $orderData->status = 'sent';
            } else {
                $orderData->status = 'archived';
            }
            $orderData->save();
            return response()->json(['success' => true, 'order_id' => $id], 200);
        } else {
            return response()->json(['success' => false, 'order_id' => $id], 200);
        }
    }

    private function getPaymentGetwayInfo($restaurant_id)
    {
        $paymentGatewayConfig = null;
        $paymentGatewayInfo = PaymentGateway::select('*')->where("restaurant_id", $restaurant_id)->where("status", 1)->get()->toArray();
        //print_r($paymentGatewayInfo);die;
        if (isset($paymentGatewayInfo[0])) {
            $paymentGatewayConfig['ID'] = $paymentGatewayInfo[0]['id'];
            $config = json_decode($paymentGatewayInfo[0]['config']);
            $paymentGatewayConfig['GATEWAY'] = strtolower(trim($paymentGatewayInfo[0]['name']));
            foreach ($config as $val1) {
                foreach ($val1 as $key => $val) {
                    $paymentGatewayConfig[$key] = $val;
                }
            }
        } else {
            $paymentGatewayConfig['ID'] = 0;
            $paymentGatewayConfig['STRIPE_SECRET'] = config('constants.payment_gateway.stripe.secret');
            $paymentGatewayConfig['GATEWAY'] = "stripe";
        }
        return $paymentGatewayConfig;
    }

    private function refundInitatedOnPaymentGateway($orderData, $amount_refunded, $status,$parentRestaurant)
    {
        if ($orderData->payment_gatway != "Card") {
            return true;
        }
        $refundResponse = null;
	
        $paymentGatewayConfig = $this->getPaymentGetwayInfo($parentRestaurant->parent_restaurant_id);
        // print_r($paymentGatewayConfig);die;
        if (isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY'])) == "stripe") {
            $apiObj = new StripeGateway($paymentGatewayConfig);
            $stripeRefundData = [
                'charge' => $orderData->stripe_charge_id,
                'amount' => ($amount_refunded * 100),
                'reason' => 'requested_by_customer',
            ];
            $refundResponse = $apiObj->createRefund($stripeRefundData);
            $orderLog = ['payment_gateway_order_id' => $orderData->stripe_charge_id,

                'order_id' => $orderData->id,
                'payment_gateway_id' => $paymentGatewayConfig['ID'],
                'order_status' => $status,
                'platform' => 'api',
                'cms_user_id' => 0,
                'request_data' => isset($stripeRefundData) ? json_encode($stripeRefundData) : '',
                'response_data' => isset($refundResponse) ? json_encode($refundResponse) : ''
            ];
            PaymentGatewayLog::create($orderLog);//die;
        } elseif (isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY'])) == "braintress") {
            $apiObj = new BraintressGateway($paymentGatewayConfig);
            $stripeRefundData = [
                'transactionId' => $orderData->stripe_charge_id,
                'amount' => ($amount_refunded),

            ];
            $refundResponse = $apiObj->createRefund($stripeRefundData);//print_r($refundResponse);die;
            $orderLog = ['payment_gateway_order_id' => $orderData->stripe_charge_id,

                'order_id' => $orderData->id,
                'payment_gateway_id' => $paymentGatewayConfig['ID'],
                'order_status' => $status,
                'platform' => 'api',
                'cms_user_id' => 0,
                'request_data' => isset($stripeRefundData) ? json_encode($stripeRefundData) : '',
                'response_data' => isset($refundResponse) ? json_encode($refundResponse) : ''
            ];
            PaymentGatewayLog::create($orderLog);//die;
        } else {
            /*
             * *https://developer.authorize.ne====>nkdpizza1=>Reset@123,,,API Login Id =>7F4h6QCpqkXX,,,, Transation Id=>5v3v2S4Z9J69rhZr,,,key=>Simon
             * https://sandbox.authorize.net/
             */
        }
        return $refundResponse;
    }

    function orderNotification(Request $request)
    {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        /*return $notificationordercounts =UserOrder::select('user_orders.id')
            ->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', array("placed"))
            ->get()->count();	*/
        /**    $notificationorderId =UserOrder::select(['id','product_type','payment_receipt','order_type'])->whereIn('user_orders.restaurant_id', $restArray)
         * ->whereIn('user_orders.status', array("placed"))->orderBy('id', 'DESC')->limit(1)
         * ->get()->toArray();**/
        $notificationorderId = UserOrder::select(['id', 'product_type', 'payment_receipt', 'order_type'])->whereIn('user_orders.restaurant_id', $restArray)
            ->whereIn('user_orders.status', array("placed"))->orderBy('id', 'DESC')->limit(1)
            ->get()->toArray();

        return response()->json(['success' => false, 'order_type' => $notificationorderId[0]['order_type'], 'id' => $notificationorderId[0]['id'], 'product_type' => $notificationorderId[0]['product_type'], 'payment_receipt' => $notificationorderId[0]['payment_receipt']], 200);
    }

    /**
     * Mark Test order from Order details page
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function testOrderUpdate(Request $request)
    {
        $orderId = $request->input('order_id');
        $testFlag = $request->input('test_order');
        try {
            $res = UserOrder::where('id', $orderId)->update(['test_order' => $testFlag]);
        } catch (Exception $e) {
            $res = $e;
        }
        return response()->json($res);
    }

    public function allOrder($id, $action,Request $request)
    {


     $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        $statusArray = array("placed", "confirmed", "ready", "completed", "cancelled", "rejected", "refunded", "archived", "preparing", "outfordelivery");
        $custormename = "";
       if ($action == "food_item") {

            $orderData = UserOrder::select('routed.restaurant_name as routed_restaurant_name','user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.order_amount', 'user_orders.tip_amount','user_orders.surname',
                'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.promocode_discount', 'user_orders.flat_discount', 'user_orders.promocode',
                'user_orders.email', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at',
                'restaurants.restaurant_name', 'restaurants.phone as rphone', 'restaurants.rest_code',
                'restaurants.address as raddress', 'restaurants.zipcode as rzipcode', 'restaurants.street as rstreet',
                'user_orders.address', 'is_guest', 'user_orders.address2', 'user_orders.state', 'user_orders.zipcode',
                'address_label',
                'user_orders.city', 'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type', DB::raw('CONCAT(user_orders.delivery_date, " ", user_orders.delivery_time) AS delivery_datetime'), DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE) as order_delivery_fdatetime'))
                ->whereIn('user_orders.restaurant_id', $restArray)
		 
                ->whereIn('user_orders.status', $statusArray)
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')->leftJoin('restaurants as routed', 'routed.id', '=', 'user_orders.routed_restaurant_id');
            $orderData = $orderData->orderBy('user_orders.id', 'DESC');
            $orderData = $orderData->where('user_orders.product_type', '=', "food_item")->get()->toArray();

      } else {

            $orderData = UserOrder::select('routed.restaurant_name as routed_restaurant_name','user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.order_amount', 'user_orders.promocode_discount','user_orders.surname', 'user_orders.flat_discount', 'user_orders.promocode', 'user_orders.tip_amount',
                'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.email', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at', 'restaurants.restaurant_name', 'restaurants.phone as rphone', 'restaurants.rest_code', 'restaurants.address as raddress', 'restaurants.zipcode as rzipcode', 'restaurants.street as rstreet', 'user_orders.address', 'is_guest', 'user_orders.address2', 'user_orders.state', 'user_orders.zipcode', 'address_label',
                'user_orders.city', 'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type', DB::raw('CONCAT(user_orders.delivery_date, " ", user_orders.delivery_time) AS delivery_datetime'), DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE) as order_delivery_fdatetime'))
                ->whereIn('user_orders.restaurant_id', $restArray)
                ->where('user_orders.user_id', $id)
                ->whereIn('user_orders.status', $statusArray)
                ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
		->leftJoin('restaurants as routed', 'routed.id', '=', 'user_orders.routed_restaurant_id');
            $orderData = $orderData->orderBy('user_orders.id', 'DESC');
            $orderData = $orderData->where('user_orders.product_type', '=', "food_item")->get()->toArray();
	    if(isset($orderData[0]))
            $custormename = $orderData[0]['fname'] . " " . $orderData[0]['lname'];
	    else $custormename = null;
        }
       $totalData =  count($orderData);
        if($totalData==0)
        {
            return view('user_order.allorders');
        }
        $totalFiltered = $totalData;
        if ($request['val']=='getEMP') {
            $sqlquery ="";
            $requestData = $request;

            $columns = array(
                0 => "user_orders.order_type",
                1 => "user_orders.payment_receipt",
                2 => "user_orders.created_at",
                3 => "user_orders.delivery_date",
                4 => "user_orders.promocode",
                5 => "user_orders.total_amount",
                6 => "user_orders.phone",
                7 => "user_orders.zipcode",
                8 => "user_orders.status",
                9 => "user_orders.order_type",
		10 => "restaurants.restaurant_name",
		11 => "routed_restaurant_name"
            );

            $orderData = UserOrder::select('routed.restaurant_name as routed_restaurant_name','user_orders.id', 'user_orders.user_id', 'user_orders.restaurant_id', 'user_orders.order_amount', 'user_orders.tip_amount','user_orders.surname',
                'user_orders.fname', 'user_orders.lname', 'user_orders.payment_receipt', 'user_orders.phone', 'user_orders.promocode_discount', 'user_orders.flat_discount', 'user_orders.promocode',
                'user_orders.email', 'user_orders.order_type', 'user_orders.total_amount', 'user_orders.created_at',
                'restaurants.restaurant_name', 'restaurants.phone as rphone', 'restaurants.rest_code',
                'restaurants.address as raddress', 'restaurants.zipcode as rzipcode', 'restaurants.street as rstreet',
                'user_orders.address', 'is_guest', 'user_orders.address2', 'user_orders.state', 'user_orders.zipcode',
                'address_label',
                'user_orders.city', 'user_orders.delivery_time', 'user_orders.delivery_date', 'user_orders.status', 'user_orders.product_type', DB::raw('CONCAT(user_orders.delivery_date, " ", user_orders.delivery_time) AS delivery_datetime'), DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL restaurants.kpt MINUTE) as order_delivery_fdatetime'))
                ->whereIn('user_orders.restaurant_id', $restArray)

                ->whereIn('user_orders.status', $statusArray);
                if(trim(strtolower($action))=='usrord'){
                    $orderData->where('user_orders.user_id', $id);
                }

                $orderData->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')->leftJoin('restaurants as routed', 'routed.id', '=', 'user_orders.routed_restaurant_id');


         if($requestData['type']=='orderdate'&& empty($requestData['searchval']))
            {

                if($requestData['startdate']!="" && $requestData['endate']!="")
                {
                    $startdate  = date("Y-m-d", strtotime($requestData['startdate']));
                    $endate  = date("Y-m-d", strtotime($requestData['endate'].' +1 day'));

                        $orderData->whereBetween('user_orders.created_at', [ $startdate.' 00:00:00' ,$endate.' 00:00:00']);


                }
                elseif($requestData['startdate']!=""&& $requestData['endate']=="")
                {
                    $startdate  = date("Y-m-d", strtotime($requestData['startdate']));
                    $orderData->where('user_orders.created_at', '>=',  $startdate.' 00:00:00');
                }
                elseif($requestData['startdate']==""&& $requestData['endate']!="")
                {
                    $endate  = date("Y-m-d", strtotime($requestData['endate']));
                    $orderData->where('user_orders.created_at', '<=',  $endate.' 00:00:00');
                }
            }
            else if($requestData['type']=='Fulfillment' && empty($requestData['searchval']))
            {
                if($requestData['startdate']!="" && $requestData['endate']!="")
                {
                    $startdate  = date("Y-m-d", strtotime($requestData['startdate']));
                    $endate  = date("Y-m-d", strtotime($requestData['endate']));
                    $orderData->whereBetween('user_orders.delivery_date', [ $startdate ,$endate]);
                }
                else if($requestData['startdate']!="" && $requestData['endate']=="")
                {
                    $startdate  = date("Y-m-d", strtotime($requestData['startdate']));
                    $orderData->where('user_orders.delivery_date', '>=',  $startdate);
                }
                else if($requestData['startdate']=="" && $requestData['endate']!="")
                {
                    $endate  = date("Y-m-d", strtotime($requestData['endate']));
                    $orderData->where('user_orders.delivery_date', '<=',  $endate);
                }
            }

           if (!empty($requestData['search']['value']) && $requestData['type']=='no')
            {
                $orderData->where('user_orders.payment_receipt', 'like',  '%' .$requestData['search']['value'] .'%' );
                $orderData->orWhere('user_orders.fname', 'like',  '%' .$requestData['search']['value'] .'%' );
                $orderData->orWhere('user_orders.lname', 'like',  '%' .$requestData['search']['value'] .'%' );
                $orderData->orWhere('user_orders.phone', 'like',  '%' .$requestData['search']['value'] .'%' );
            }
            elseif(!empty($requestData['searchval']) && $requestData['type']=='no')
            {
                $orderData->where('user_orders.payment_receipt', 'like',  '%' .$requestData['searchval'].'%' );
                $orderData->orWhere('user_orders.fname', 'like',  '%' .$requestData['searchval'].'%' );
                $orderData->orWhere('user_orders.lname', 'like',  '%' .$requestData['searchval'].'%' );
                $orderData->orWhere('user_orders.phone', 'like',  '%' .$requestData['searchval'].'%' );
            }
          if(!empty($requestData['search']['value']) && $requestData['type']=='orderdate')
           {

                  if($requestData['startdate']!="" && $requestData['endate']!="") {
                   $startdate = date("Y-m-d", strtotime($requestData['startdate']));
                   $endate = date("Y-m-d", strtotime($requestData['endate'] . ' +1 day'));
                   if ($orderData->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%')->get()->count())
                   {
                       $datequery = $orderData->whereBetween('user_orders.created_at', [$startdate . ' 00:00:00', $endate . ' 00:00:00'])->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%');
                    }
                   else if ($orderData->Where('user_orders.fname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->whereBetween('user_orders.created_at', [$startdate . ' 00:00:00', $endate . ' 00:00:00'])->where('user_orders.fname', 'like', '%' . $requestData['search']['value'] . '%');
                   }
                   else if ($orderData->Where('user_orders.lname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->whereBetween('user_orders.created_at', [$startdate . ' 00:00:00', $endate . ' 00:00:00'])->where('user_orders.lname', 'like', '%' . $requestData['search']['value'] . '%');
                   }
                   else if ($orderData->Where('user_orders.phone', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->whereBetween('user_orders.created_at', [$startdate . ' 00:00:00', $endate . ' 00:00:00'])->where('user_orders.phone', 'like', '%' . $requestData['search']['value'] . '%');
                   }
               }
               elseif($requestData['startdate']!=""&& $requestData['endate']=="")
               {
                   $startdate  = date("Y-m-d", strtotime($requestData['startdate']));
                  // $orderData->where('user_orders.created_at', '>=',  $startdate.' 00:00:00');

                   if ($orderData->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%')->get()->count())
                   {
                       $datequery = $orderData->where('user_orders.created_at', '>=',  $startdate.' 00:00:00')->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%');
                   }
                   else if ($orderData->Where('user_orders.fname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->where('user_orders.created_at', '>=',  $startdate.' 00:00:00')->where('user_orders.fname', 'like', '%' . $requestData['search']['value'] . '%');
                   }
                   else if ($orderData->Where('user_orders.lname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->where('user_orders.created_at', '>=',  $startdate.' 00:00:00')->where('user_orders.lname', 'like', '%' . $requestData['search']['value'] . '%');
                   }
                   else if ($orderData->Where('user_orders.phone', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->where('user_orders.created_at', '>=',  $startdate.' 00:00:00')->where('user_orders.phone', 'like', '%' . $requestData['search']['value'] . '%');
                   }
               }
               elseif($requestData['startdate']==""&& $requestData['endate']!="")
               {
                   $endate  = date("Y-m-d", strtotime($requestData['endate']));
                   $orderData->where('user_orders.created_at', '<=',  $endate.' 00:00:00');
                   if ($orderData->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%')->get()->count())
                   {
                       $datequery = $orderData->where('user_orders.created_at', '<=',  $endate.' 00:00:00')->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%');
                   }
                   else if ($orderData->where('user_orders.fname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->where('user_orders.created_at', '<=',  $endate.' 00:00:00')->where('user_orders.fname', 'like', '%' . $requestData['search']['value'] . '%');
                   }
                   else if ($orderData->Where('user_orders.lname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->where('user_orders.created_at', '<=',  $endate.' 00:00:00')->where('user_orders.lname', 'like', '%' . $requestData['search']['value'] . '%');
                   }
                   else if ($orderData->Where('user_orders.phone', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                   {
                       $datequery = $orderData->where('user_orders.created_at', '<=',  $endate.' 00:00:00')->where('user_orders.phone', 'like', '%' . $requestData['search']['value'] . '%');
                   }
               }
           }
          else if(!empty($requestData['search']['value']) && $requestData['type']=='Fulfillment')
          {
              if($requestData['startdate']!="" && $requestData['endate']!="") {
                  $startdate = date("Y-m-d", strtotime($requestData['startdate']));
                  $endate = date("Y-m-d", strtotime($requestData['endate'] . ' +1 day'));
                  if ($orderData->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%')->get()->count())
                  {
                      $datequery = $orderData->whereBetween('user_orders.delivery_date', [ $startdate ,$endate])->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.fname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->whereBetween('user_orders.delivery_date', [ $startdate ,$endate])->where('user_orders.fname', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.lname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->whereBetween('user_orders.delivery_date', [ $startdate ,$endate])->where('user_orders.lname', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.phone', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->whereBetween('user_orders.delivery_date', [ $startdate ,$endate])->where('user_orders.phone', 'like', '%' . $requestData['search']['value'] . '%');
                  }
              }
              elseif($requestData['startdate']!=""&& $requestData['endate']=="")
              {
                  $startdate  = date("Y-m-d", strtotime($requestData['startdate']));
                  // $orderData->where('user_orders.created_at', '>=',  $startdate.' 00:00:00');

                  if ($orderData->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%')->get()->count())
                  {
                      $datequery = $orderData->where('user_orders.delivery_date', '>=',  $startdate)->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.fname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->where('user_orders.delivery_date', '>=',  $startdate)->where('user_orders.fname', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.lname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->where('user_orders.delivery_date', '>=',  $startdate)->where('user_orders.lname', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.phone', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->where('user_orders.delivery_date', '>=',  $startdate)->where('user_orders.phone', 'like', '%' . $requestData['search']['value'] . '%');
                  }
              }
              elseif($requestData['startdate']==""&& $requestData['endate']!="")
              {
                  $endate  = date("Y-m-d", strtotime($requestData['endate']));
               //   $orderData->where('user_orders.created_at', '<=',  $endate.' 00:00:00');
                  if ($orderData->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%')->get()->count())
                  {
                      $datequery = $orderData->where('user_orders.delivery_date', '<=',  $endate)->where('user_orders.payment_receipt', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.fname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->where('user_orders.delivery_date', '<=',  $endate)->where('user_orders.fname', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.lname', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->where('user_orders.delivery_date', '<=',  $endate)->where('user_orders.lname', 'like', '%' . $requestData['search']['value'] . '%');
                  }
                  else if ($orderData->where('user_orders.phone', 'like',  '%' .$requestData['search']['value'] .'%' )->get()->count())
                  {
                      $datequery = $orderData->where('user_orders.delivery_date', '<=',  $endate)->where('user_orders.phone', 'like', '%' . $requestData['search']['value'] . '%');
                  }
              }
          }

            $orderData = $orderData->where('user_orders.product_type', '=', "food_item")->orderBy($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])->offset($requestData['start'])->limit($requestData['length'])->get()->toArray();
            $totalData1 =  count($orderData);
            $totalFiltered1 = count($orderData);
            $data = array();


            foreach($orderData as $oData)
            {

                if(Auth::user()) {
                    $curSymbol = Auth::user()->restaurant->currency_symbol;
                } else {
                    $curSymbol = config('constants.currency');
                }
                $url=  "'".URL::to('user_order/' . $oData['id'] . '/mngdetails/'.$oData['product_type'])."'";
                $link = '<span  onclick="getOrderDetail('.$url.')" >';
             $nestedData = array();
                if($oData['status']=='placed'){
                    $status_color='#00af3a';
                }else if($oData['status']=='cancelled'){
                    $status_color='#ff0000';
                }else if($oData['status']=='rejected'){
                    $status_color='#ff0000';
                }else if($oData['status']=='refunded'){
                    $status_color='#ffc741';
                }
                else if($oData['status']=='completed'){
                    $status_color='#00af3a';
                }
                else if($oData['status']=='sent'){
                    $status_color='#00af3a'; //
                }else if($oData['status']=='archived'){
                    $status_color='#ff8207'; //
                }else{
                    $status_color='#ff8207';
                }
             if($oData['order_type']=="delivery") {
              $type=    $link.'<div title = "Delivery" class="del icon-delivery-order" style = "color: '.$status_color.'" ><span class="hide" > Delivery</span ></div ></span>';
              }
             else
             {
                 $type=  $link.'<div title="Takeout" class="tak icon-takeout-order" style="color:'.$status_color.'"><span class="hide">Takeout</span></div></span>';
             }
             $nestedData[] = $type;
            $nestedData[] =  $link.$oData["payment_receipt"].'</span>';
         //   $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData['restaurant_id'], 'datetime' => $oData['created_at'])));
             $orderDate = date('d M, Y, h:i A', (strtotime($oData['created_at']) ));
	    //$orderDate = (new DateTime($oData['created_at']))->format('l d M, h:i A');
            $nestedData[] =$orderDate;
            $deliveryDate = date("d M, Y, h:i A", strtotime($oData['delivery_datetime']));
            $nestedData[] = $deliveryDate;
            $nestedData[] =   $oData['promocode'];
            $nestedData[] =      $curSymbol.$oData['total_amount'];
            $custormename = '<strong style="text-transform:capitalize">'.$oData['surname'] . " " .$oData['fname'] . " " . $oData['lname'].'</strong>';
            $nestedData[] =    $custormename.'<br> <a href="tel:'.$oData['phone'].'">'.$oData['phone'].'</a>';
            $nestedData[] =   $oData['zipcode'];
            $nestedData[] =      '<div style="color:'.$status_color.'">'.ucfirst($oData['status']).'</div>';
	       $nestedData[] = $oData['restaurant_name'];
	       //$nestedData[] = $oData['routed_restaurant_name'];
            $start= "'".$requestData['startdate']."'";
            $end =    "'".$requestData['endate']."'";
            $type =  "'".$requestData['type']."'";
            if (!empty($requestData['search']['value']))
            {
                $search_val    =  "'".$requestData['search']['value']."'";
            }else{
                $search_val    =  "'".$requestData['searchval']."'";
            }

            $nestedData[] =  '<button  onclick="getOrderDetail('.$url.','.$start.','.$end.','.$type.','.$search_val.')" class="btn_ btn__primary font-weight-600 box-shadow" style="font-size:14px;">View</button>';
                $data[] = $nestedData;
            }

            $json_data = array(
                "draw" => intval($requestData['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalData),  // total number of records
                //"recordsFiltered" => intval($totalFiltered1), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data  ,
		"recordsFiltered" => intval($totalData), 	
                'start' => $requestData['start'],
                'length' => $requestData['length']
            );

   	echo  json_encode($json_data);  // send data as json format
        //  return view('user_order.allorders');


     } else {
           // CommonFunctions::createJobStuart(null);
	  //CommonFunctions::callStuartsApi('46 Boulevard Barbès, 75018 Paris','156 rue de Charonne, 75011 Paris');
	  //$groupRestData = $default_sort_order = $search_type= $statusKey= $orderStatus = null;
	  //$orderStatus = array("placed", "confirmed", "ready", "completed", "cancelled", "rejected", "refunded");
	  //$statusKey = $search_type =  $default_sort_order = $groupRestData = $action = $custormename = $id= null;
          return view('user_order.allorders', compact('orderData', 'orderStatus', 'statusKey', 'search_type', 'default_sort_order', 'groupRestData', "action","id" ,"custormename"));
      }
    }

    public function mngorderDetail($id, $action)
    {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name','r2.lat','r2.lng'));


        $orderData = UserOrder::where('user_orders.id', $id)->with('user')->whereIn('restaurant_id', $restArray)->where('product_type', '=', $action)->first();

        //$orderData = $orderData->where('product_type', '=', $action);
        if (empty($orderData)) {
            return Redirect::back()->with('message', 'Invalid Id');
        } else {
            //update is_order_view=1 as per new design @18-03-2019 RG
            UserOrder::where('id', $id)->update(['is_order_viewed' => 1]);
	    
            $total_refunded_amount = 0;
            $refund_reason = array();
            $refunded_amount_data = DB::table('refund_histories')->where('order_id', $id)->select('amount_refunded', 'reason')->get();
            if ($refunded_amount_data) {
                foreach ($refunded_amount_data as $key => $amount_data) {
                    $total_refunded_amount = $total_refunded_amount + $amount_data->amount_refunded;
                    $refund_reason[$key] = array('amount' => $amount_data->amount_refunded, 'reason' => $amount_data->reason);
                }
            }
 
            $userId = $orderData->user_id;
            $emailId = $orderData->email;
            $locationId = $orderData->restaurant_id;
            $restlatlng = Restaurant::select('lat','lng')->where('id', $orderData->restaurant_id)->get()->toArray();
	    $restlatName = Restaurant::select('restaurant_name')->where('id', $orderData->restaurant_id)->get()->toArray();	
	    $restlatName = $restlatName[0]['restaurant_name'];	
            $restlat_lng =$restlatlng[0];
           // print_r($restlatlng);
            $lat =$restlat_lng['lat'];
            $lng =  $restlat_lng['lng'];

            $orderDetail = new UserOrderDetail();
            $UserOrderAddons = new UserOrderAddons();
            $userOrder = new UserOrder();
            $userReserve = new Reservation();

            #@23-11-18 RG if user_id is there then check with user_id else with email id if Guest User and if not registered
            $userOrders = $userOrder->whereIn('restaurant_id', $restArray)->whereNotIn('status', ['pending']);
	    
            if ($userId) {
                $userOrders = $userOrders->where('user_id', $userId);
            } else {
                $userOrders = $userOrders->where('email', $emailId);
            }
            $userOrders = $userOrders->get()->count();

            $status = $orderDate = "";
            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
            $orderDate = date('l d M, h:i A', strtotime($orderData->created_at));

            $restaurantDetails = CommonFunctions::getRestaurantDetailsById($orderData->restaurant_id);

            $result = [];
            $timeSlotData = '';
            $timeSlotData = CommonFunctions::getTime_Slot($orderData->delivery_time);
            $result = CommonFunctions::getDateSlot();

            $status = $orderData->status;
            $button_name = "";
            if ($status == "placed") {
                $status = "preparing";
                $button_name = "Confirm";
            } else if ($status == "preparing") {
                $button_name = "Sent";
                $status = "outfordelivery";
            } else if ($status == "outfordelivery" && $orderData->order_type == "delivery") {
                $button_name = "Delivered";
                $status = "completed";
            } else if ($status == "confirmed" && $orderData->order_type == "carryout") {
                $button_name = "Ready";
                $status = "ready";
            } else if ($status == "ready") {
                $button_name = "Picked up";
                $status = "completed";
            } else if ($status == "rejected")
                $status = "rejected";

            else if ($status == "cancelled")
                $status = "cancelled";

            $userReservation = $userReserve->getReservationByUser($userId, $emailId, $restArray);

            $orderDetailData = $orderDetail->getOrderDetail($id);
            $addOnsDetail = [];

            $i = 0;
            $addonsData = [];
            foreach ($orderDetailData as $key => $data) {
                $addon_modifier = CommonFunctions::getAddonsAndModifiers($data);
                $orderDetailData[$key]['addon_modifier'] = $addon_modifier;
                $addons_data = [];
                $bypData = json_decode($data->menu_json, true);
                //echo $data->is_byp;print_r($bypData); die;
                //@change By RG 29-10-2018 PE-2670
                if (!is_null($bypData)) {
                    $addons_data = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                    //print_r($addons_data); die;
                    //\Log::info($addons_data);
                    $addonsData[$data->id] = $addons_data;
                    $orderDetailData[$key]['addOnsDetail'] = $addons_data;

                } else {
                    //@change By RG 29-10-2018 PE-2670
                    // $addons_data[] = [
                    //     'addons_name' => $data->item,
                    //     'price' => $data->unit_price,
                    // ];
                    // $orderDetailData[$key]['addOnsDetail'] = $addons_data;
                }
            }

            //$orderData->created_at = date('Y-m-d H:i:s', strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at))));

            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $orderData->restaurant_id));

            if (is_object($currentDateTimeObj)) {
                $currentTimestamp = strtotime($currentDateTimeObj->format('Y-m-d H:i:s'));
                $currentTime = $currentDateTimeObj->format('H:i');
                $currentDate = $currentDateTimeObj->format('l, M d, Y');
            } else {
                $currentTimestamp = strtotime(date('Y-m-d H:i:s'));
                $currentTime = date('H:i');
                $currentDate = date('l, M d, Y');
            }

            $deliveryTimestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');

            $order_del_time = ($deliveryTimestamp - $restaurantDetails['kpt'] * 60);
            $is_order_scheduled = 0;
            if ($currentTimestamp < $order_del_time) {
                $is_order_scheduled = 1;
            }

            //@RG 08-10-2018 Show Log button when Order is in archived:
            $show_log_btn = false;
            if (in_array($orderData->status, array("archived", "cancelled", "arrived", "sent", "rejected", "refunded"))) {
                $show_log_btn = true;
            }

            //echo "<pre>";print_r($orderDetailData->toArray());die;
            if (Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
            } else {
                $curSymbol = config('constants.currency');
            }
            $order_other_details = $data = CommonFunctions::getOrderCharges($orderData, $curSymbol);
	    $stuartOrdersDetails = StuartOrders::where('ma_order_id',$id)->get()->toArray();
	    $stuartOrdersLogDetails = StuartOrdersLog::where('ma_order_id',$id)->get()->toArray(); 		
            return view('user_order.mngdetails', compact('userId', 'orderData','lat' ,'lng','orderDetailData', 'userOrders', 'userReservation', 'addOnsDetail', 'status', 'orderDate', 'timeSlotData', 'result', 'restaurantDetails', 'currentTimestamp', 'deliveryTimestamp', 'action', 'show_log_btn', 'currentTime', 'currentDate', 'total_refunded_amount', 'refund_reason', 'is_order_scheduled', 'order_other_details', 'button_name','stuartOrdersDetails','stuartOrdersLogDetails','restlatName'));
        }
    }
    
    public function orderReplyMail(Request $request){        
        $id = $request->input('id');
        $message = $request->input('message');
        $replytype = "order";
        $user = Auth::user();        
        $orderData = UserOrder::find($id);
        
        $sentto = $orderData->email;
        $title = "We recieve your order.";
        $cmsuser = \App\Models\CmsUser::where('id', $user->id)->get()->toArray();
        $created_date = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($orderData->restaurant_id, date("Y/m/d H:i"))->format('D, M d h:i A');
        $replyData1 ['Sent On'] = $created_date;
        $replyData1 ['From Email'] = $cmsuser[0]['email'];
        $replyData1 ['From Name'] = $cmsuser[0]['name'];
        $replyData1 ['Message'] = $message;
        $replyData1 ['Sent to'] = $sentto;
        $replyData1 ['response'] = 'success';        
        
        $mailTemplate = "enquiryreplymail_user";
        $restaurantId = $user->restaurant_id;
        $fromName = $user->name;
        $fromEmail = $user->email;

        $reply = json_encode(['reply_message'=>$message,'reply_from'=>$user->id,'reply_to'=>$sentto],1);
        $data = [
            'restaurant_id' => $restaurantId,            
            'order_id' => $id,
            'user_id'=>$orderData->user_id,
            'comments' => $reply,
            'status'=>$orderData->status            
        ];
        ##########################
        
        $mailFrom = Restaurant::select('restaurant_name', 'custom_from', 'support_from','restaurant_logo_name','source_url','address','facebook_url','twitter_url','pinterest_url','instagram_url','phone','street','zipcode')
                    ->where('id', $user->restaurant_id)->get()->toArray();
        
        $controller = new NotificationController;
        
        $mailKeywords['ADDRESS'] =  $mailFrom[0]['address'];
        $mailKeywords['TITLE'] = $title;
        $url = URL::to("/");

        $logo = '<img class="logo" src="'.$url.$mailFrom[0]['restaurant_logo_name'].'" alt="logo" style="-ms-interpolation-mode:bicubic;clear:both;display:block;max-width:150px;outline:0;text-decoration:none;width:100%">';
        $mailKeywords['LOGO'] = $logo;

        
        $first_name = isset($orderData->fname) ? ucfirst($orderData->fname) : '';
        $last_name = isset($orderData->lname) ? " ".ucfirst($orderData->lname) : '';
        $username = 'Dear <strong>'. $first_name.$last_name.",</strong>";

        $mailKeywords['FACEBOOK_URL']=$mailFrom[0]['facebook_url'];
        $mailKeywords['TWITTER_URL']=$mailFrom[0]['twitter_url'];
        $mailKeywords['INSTAGRAM_URL']=$mailFrom[0]['instagram_url'];
        $mailKeywords['PHONE']=$mailFrom[0]['phone'];

        $mailKeywords['STREET']=$mailFrom[0]['street'];
        $mailKeywords['ZIP']=$mailFrom[0]['zipcode'];

        $mailKeywords['USER_NAME'] = $username;

        $mailKeywords['MESSAGE'] = $message;
        $mailKeywords['RESTAURANT_NAME']=$mailFrom[0]['restaurant_name'];
        $config['to'] = $orderData->email;
        $config['subject'] = "We will contact soon";
        $config['from_name'] = $mailFrom[0]['restaurant_name'];
        $config['from_address'] = $mailFrom[0]['support_from'];
        
        $controller->sendMail($mailTemplate, 1, $config, $mailKeywords);
        
        OrderStatuses::create($data);                

        return json_encode($replyData1);
    }
}


//ALTER TABLE `order_statuses`  ADD `comments` JSON NULL DEFAULT NULL  AFTER `restaurant_id`;


