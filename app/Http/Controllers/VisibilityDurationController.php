<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\CustomVisibilityHours;
use Modules\Reservation\Entities\ReservationStatus;
use Modules\Reservation\Entities\TableType;
use App\Models\VisibilityHours;
use DB;
use App\Http\Requests\OperationHoursRequest;

use Modules\Reservation\Entities\Reservation;

class VisibilityDurationController extends Controller
{
   
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $visibilityData = VisibilityHours::join('restaurants', 'cms_restaurant_visibility_duration_week_time.restaurant_id','=','restaurants.id')
            ->groupBy('slot_name')
            ->select('cms_restaurant_visibility_duration_week_time.id',
                'cms_restaurant_visibility_duration_week_time.slot_name as slot_name',
                'restaurants.restaurant_name',
                'cms_restaurant_visibility_duration_week_time.created_at',
                'cms_restaurant_visibility_duration_week_time.updated_at'
            )
            ->orderBy('updated_at', 'desc')
            ->get();
        return view('visibility_duration/visibility_all')->with(['visibilityData' => $visibilityData]);
       /*$calendar_days = config('reservation.calendar_days');
        return view('visibility_duration.index', compact('calendar_days'));*/
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {   $calendar_days = config('reservation.calendar_days');
        $actionName =  $request->route()->getActionMethod();
        $edit_id = '';
        $slot_name = '';
        return view('visibility_duration.index', compact('calendar_days', 'actionName', 'edit_id', 'slot_name'));
        //return view('operation_hours::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(OperationHoursRequest $request)
    {
        try {
            $type = $request->get('type');
            $inputData = $this->getModifiedInputArray($request);
            if($inputData){
                DB::beginTransaction();
                if($type == config('reservation.working_hour_type.day')){
                    if(isset($inputData['calendar_day']) && !empty($inputData['calendar_day'])){
                        $updateID = VisibilityHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'slot_name' => $inputData['slot_name'], 'calendar_day' => $inputData['calendar_day']], $inputData);
                        /*$saveData = new VisibilityHours();
                        $saveData->restaurant_id = \Auth::user()->restaurant_id;
                        $saveData->slot_name = $inputData['slot_name'];
                        $saveData->calendar_day = $inputData['calendar_day'];
                        $saveData->slot_detail = $inputData['slot_detail'];
                        $saveData->is_dayoff = $inputData['is_dayoff'];
                        $saveData->save();*/
                    }
                }else{
                    $schd_id = $request->has('schd_id') ? $request->get('schd_id'): 0;
                    $action = ($schd_id==0)?'add':'update';
                    if($this->subRangeExistInRange($inputData['calendar_date'], $action)){
                        return \Response::json(['success' => false, 'errors' => ['Slots are already exist in this date range, please select different range']], 419);
                    }
                    $updateID = CustomVisibilityHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'id' => $schd_id], $inputData);
                }
                if($request->has('repeat_day') && $type == config('reservation.working_hour_type.day')){
                    $repeat_arr = array_keys($request->get('repeat_day'));
                    $input_calendar_day = $inputData['calendar_day'];
                    foreach($repeat_arr as $day){
                        if($input_calendar_day != $day){
                            $calendar_days = config('reservation.calendar_days');
                            $request->merge(['calendar_day' => array_search($day, $calendar_days)]);
                            $inputData = $this->getModifiedInputArray($request);
                            if($inputData && (isset($inputData['calendar_day']) && !empty($inputData['calendar_day']))){
                                $updateID = VisibilityHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => $day], $inputData);
                            }
                        }
                    }
                }
                DB::commit();
                return \Response::json(['success' => true, 'message'=>'Hour setting has been updated', 'insertedId' => $updateID->id, 'slot_name' => $inputData['slot_name']], 200);
            }
        }catch (\Exception $e){
            return \Response::json(['success' => false, 'error' => $e->getMessage(), 'insertedId' => $updateID->id,'slot_name' => $inputData['slot_name']], 500);
            DB::rollback();
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(VisibilityHours $request)
    {
        $visibilityData = VisibilityHours::join('restaurants', 'cms_restaurant_visibility_duration_week_time.restaurant_id','=','restaurants.id')
            ->groupBy('slot_name')
            ->select('cms_restaurant_visibility_duration_week_time.id',
                'cms_restaurant_visibility_duration_week_time.slot_name as slot_name',
                'restaurants.restaurant_name',
            'cms_restaurant_visibility_duration_week_time.created_at',
            'cms_restaurant_visibility_duration_week_time.updated_at'
            )
            ->get();
        return view('visibility_duration/visibility_all')->with(['visibilityData' => $visibilityData]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $calendar_days = config('reservation.calendar_days');
        $actionName =  $request->route()->getActionMethod();
        $edit_id = $id;
        $slotNameData = VisibilityHours::where('id', '=', $id)->get();
        $slot_name = $slotNameData[0]->slot_name;
        return view('visibility_duration.index', compact('calendar_days', 'actionName', 'edit_id', 'slot_name'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(VisibilityHoursRequest $request)
    {
        //\Log::info($request->all());
        $type = $request->get('type');
        $schd_id = $request->get('schd_id');
        $inputData = $this->getModifiedInputArray($request);
        if($inputData){
            DB::beginTransaction();
            try {
                if($type == config('reservation.working_hour_type.day')){
                    VisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)
                        ->where('calendar_day', $inputData['calendar_day'])->update($inputData);
                    if($request->has('repeat_day')){
                        $repeat_arr = array_keys($request->get('repeat_day'));
                        $input_calendar_day = $inputData['calendar_day'];
                        foreach($repeat_arr as $day){
                            if($input_calendar_day != $day){
                                $calendar_days = config('reservation.calendar_days');
                                $request->merge(['calendar_day' => array_search($day, $calendar_days)]);
                                $request->merge(['repeat' => true]);
                                $inputData = $this->getModifiedInputArray($request);
                                if($inputData && (isset($inputData['calendar_day']) && !empty($inputData['calendar_day']))){
                                    VisibilityHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => $day], $inputData);
                                }
                            }
                        }
                    }
                }else{
                    CustomVisibilityHours::find($schd_id)->update($inputData);
                }
                DB::commit();
                return \Response::json(['success' => true], 200);
            }catch(\Exception $e){
                return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
                DB::rollback();
            }
        }
        return \Response::json(['success' => false, 'error' => 'Schedule setting could not be updated'], 500);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */

    public function destroy($id)
    {
        //is the day of the weeek, 0 for sun, 1 for monday and so on
        $type  = Input::get('type');
        $slot_id = Input::get('slot_id');
        $slot_name = Input::get('slot_name');
        $calendar_days = config('reservation.calendar_days');
        $restaurant_id = \Auth::user()->restaurant_id;
        $calendar_day = $calendar_days[$id];
        DB::beginTransaction();
        try {
            if($type == config('reservation.working_hour_type.day')) {
                $scheduleHours = VisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)
                    ->where('calendar_day', $calendar_day)
                    ->where('slot_name', $slot_name)->first();
            }else {
                $scheduleHours = CustomVisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)
                    ->where('id', $id)
                    ->where('slot_name', $slot_name)->first();
            }
            if($scheduleHours){
                if($type == config('reservation.working_hour_type.day')) {
                    $dayOrDate = $scheduleHours->calendar_day;
                }else{
                    $dayOrDate = $scheduleHours->calendar_date;
                }
                if(empty($scheduleHours->slot_detail) || $scheduleHours->slot_detail==NULL || (empty($slot_id) || $slot_id<=0)) {echo 'asdasd';exit;
                    $scheduleHours->where('calendar_day', $calendar_day)->where('slot_name', $slot_name)->delete();
                    DB::commit();
                    return \Response::json(['success' => true], 200);
                }
                $slot_detail_db = json_decode($scheduleHours->slot_detail, true);
                $exist = array_search($slot_id, array_column($slot_detail_db, 'slot_id'));
                $time_range = $slot_detail_db[$exist]['open_time'].'-'.$slot_detail_db[$exist]['close_time'];
                \Log::info($slot_detail_db[$exist]['open_time']."****".$time_range);
                unset($slot_detail_db[$exist]);
                $slot_detail_db = array_values($slot_detail_db); // 'reindex' array
                //\Log::info($slot_detail_db);
                $slot_detail_db = !empty($slot_detail_db)?$slot_detail_db:0;
                $scheduleHours->slot_detail = json_encode($slot_detail_db);
                $scheduleHours->save();
                DB::commit();
                return \Response::json(['success' => true], 200);
            }
        }catch(\Exception $e){
            return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
            DB::rollback();
        }
    }

    public function getVisibilityHoursSchedule(Request $request)
    {
        $url = $request->get('url');
        $edit_id = $request->get('edit_id');
        $edit_id = $request->get('edit_id');
        $slot_name = $request->get('slot_name');
        if(!empty($edit_id) and !empty($slot_name)){
            //$eventData = VisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)->get()->where('calendar_day', '!=', '');
            $eventData = VisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)
                ->where('calendar_day', '!=', '')
                ->where('slot_name', '=', $slot_name)
                ->get();
        }else{
            $eventData = [];
        }

        $events = $date_range = [];
        $inc = 0;
        $calendar_days = config('reservation.calendar_days');
        $dayoff = VisibilityHours::where('restaurant_id', '=', \Auth::user()->restaurant_id)
            ->get(['is_dayoff', 'calendar_day'])
            ->groupBy('calendar_day')
            ->map(function($day) {
                return $day->pluck('is_dayoff')->take(1);
            })->toArray();
        foreach ($calendar_days as $day_num => $day) {
            $date_range[$day_num]['id'] = $day_num;
            $date_range[$day_num]['title'] = ucfirst($day);
            $date_range[$day_num]['dayoff'] = isset($dayoff[$day]) ? $dayoff[$day][0]: 0;
        }
        $calendar_days = array_flip($calendar_days);
        \Log::info($calendar_days);
        foreach($eventData as $event){
            if($event->is_dayoff){
                $events[$inc]['id'] = $inc;
                $events[$inc]['start'] = date( "Y-m-d 00:00:00");
                $events[$inc]['end'] = date( "Y-m-d 24:00:00");
                $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                $inc++;
            }elseif(empty($event->slot_detail)){
                $events[$inc]['id'] = $inc;
                \Log::info($event->calendar_day);
                $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                $inc++;
            }else{
                $slot_detail = json_decode($event->slot_detail, true);
                if($slot_detail){
                    foreach ($slot_detail as $slot){
                        $events[$inc]['id'] = $inc;
                        $events[$inc]['slot_id'] = $slot['slot_id'];
                        $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                        $events[$inc]['start'] = date( "Y-m-d H:i:s", strtotime( $slot['open_time']));
                        $events[$inc]['end'] = date( "Y-m-d H:i:s", strtotime( $slot['close_time']));
                        $events[$inc]['title'] = '';
                        $inc++;
                    }
                }
            }
        }
        $events = ($events) ?? '';
        return json_encode(array("events" => $events, 'date_range' => $date_range));
    }

    public function getCustomVisibilityHoursSchedule()
    {
        $eventData = CustomVisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)->get();
        $events = $date_range = [];
        $inc = 0;
        foreach($eventData as $key=>$event) {
            $resource_id = $event->id;
            $calendar_dates = json_decode($event->calendar_date, true);
            $title = date('dS M', strtotime($calendar_dates['start_date'])) . '-' . date('dS M', strtotime($calendar_dates['end_date']));
            $date_range[$key]['title'] = $title;
            $date_range[$key]['id'] = $resource_id;
            $date_range[$key]['dayoff'] = $event->is_dayoff;
            $slot_detail = json_decode($event->slot_detail, true);

            if($event->is_dayoff){
                $events[$inc]['id'] = $inc;
                $events[$inc]['start'] = date( "Y-m-d 00:00:00");
                $events[$inc]['end'] = date( "Y-m-d 24:00:00");
                $events[$inc]['resourceId'] = $resource_id;
                $inc++;
            }elseif(empty($event->slot_detail)){
                $events[$inc]['id'] = $inc;
                $events[$inc]['resourceId'] = $resource_id;
                $inc++;
            } else {
                foreach ($slot_detail as $slot) {
                    $events[$inc]['id'] = $inc;
                    $events[$inc]['resourceId'] = $resource_id;
                    $events[$inc]['slot_id'] = $slot['slot_id'];
                    $events[$inc]['start'] = date("Y-m-d H:i:s", strtotime($slot['open_time']));
                    $events[$inc]['end'] = date("Y-m-d H:i:s", strtotime($slot['close_time']));
                    $events[$inc]['title'] = '';
                    $inc++;
                }
            }
        }
        $events = ($events) ?? '';
        $date_range = ($date_range) ? array_values($date_range): '';
        return json_encode(array("events" => $events, 'date_range' => $date_range));
    }

    public function dayOffToggle()
    {
        $status = Input::get('status');
        $resourceId = Input::get('resourceId');
        $resourceId = ($resourceId)? str_replace('dayoff_', '', $resourceId) : 0;
        $type = Input::get('type');
        $status = ($status=='true')? 0:1;
        $start_date = Input::get('start_date');
        $end_date = Input::get('end_date');
        $inputData['restaurant_id'] = \Auth::user()->restaurant_id;
        $inputData['slot_name'] = Input::get('slot_name');
        $inputData['slot_code'] = preg_replace('/\s+/', '', Input::get('slot_name'));
        $inputData['is_dayoff'] = $status;
        $inputData['slot_detail'] = NULL;
        $restaurant_id = \Auth::user()->restaurant_id;
        DB::beginTransaction();
        try {
            if($type == config('reservation.working_hour_type.day')){
                $updateID = VisibilityHours::updateOrCreate(['restaurant_id' => $restaurant_id, 'slot_name' => $inputData['slot_name'],'calendar_day' => strtolower($resourceId)], $inputData);
            }else{
                $updateID = '';
                if($resourceId){
                    $inputData['calendar_date'] = $this->getCustomStartEndDate($resourceId);
                }else{
                    $calendar_dates = ["start_date" => date('Y-m-d', strtotime($start_date)), "end_date" => date('Y-m-d', strtotime($end_date))];
                    $inputData['calendar_date'] = $calendar_dates ? json_encode($calendar_dates) : $calendar_dates;
                    if($this->subRangeExistInRange($inputData['calendar_date'], 'add')){
                        return \Response::json(['success' => false, 'errors' => ['Slots are already exist in this date range, please select different range'], 'insertedId' => $updateID->id, 'slot_name' => $inputData['slot_name']], 422);
                    }
                }
                $updateID = CustomVisibilityHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'id' => $resourceId], $inputData);
            }
            DB::commit();
            return \Response::json(['success' => true, 'insertedId' => $updateID->id, 'slot_name' => $inputData['slot_name']], 200);
        }
        catch(\Exception $e){
            return \Response::json(['success' => false, 'error' => $e->getMessage(), 'insertedId' => $updateID->id, 'slot_name' => $inputData['slot_name']], 500);
            DB::rollback();
        }
    }

    private function getModifiedInputArray($request){
        $inputData = array();
        $type = $request->get('type');
        $slot_name = $request->get('slot_name');
        $slot_code = preg_replace('/\s+/', '', $request->get('slot_name'));
        $available_table_count = NULL;
       
        $calendar_day = $request->get('calendar_day');
        if($type == config('reservation.working_hour_type.day')){
            $calendar_days = config('reservation.calendar_days');
            $calendar_day = $calendar_days[$calendar_day];
            $inputData['calendar_day'] = $calendar_day;
            $working_schedule = VisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)
                ->where('calendar_day', $inputData['calendar_day'])
                ->where('slot_name' , '=', $slot_name )->first();
        }else{
            if($request->has('schd_id')){
                $inputData['calendar_date'] = $this->getCustomStartEndDate($request->get('schd_id'));
            }else{
                $start_date = $request->has('start_date') ? date('Y-m-d', strtotime($request->get('start_date'))) : date('Y-m-d');
                $end_date = $request->has('end_date') ? date('Y-m-d', strtotime($request->get('end_date'))) : date('Y-m-d');
                $calendar_dates = ["end_date" => $end_date, "start_date" => $start_date];
                $inputData['calendar_date'] = $calendar_dates ? json_encode($calendar_dates) : $calendar_dates;
            }
            $working_schedule = CustomVisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_date', 'LIKE', $inputData['calendar_date'])->first();
        }


        $calendar_dates =  NULL;
        $isDayOff = $request->has('is_dayoff') ? 1 : 0;
        $inputData['restaurant_id'] = \Auth::user()->restaurant_id;
        $inputData['is_dayoff']= $isDayOff;
        $slot_detail["open_time"] = $request->get('open_time');
        $slot_detail["close_time"] = $request->get('close_time');


        $inputData['slot_name']= $slot_name;
        $inputData['slot_code']= $slot_code;
        if($working_schedule && !empty($working_schedule->slot_detail)){
            \Log::info('hee');
            $slot_detail_db = json_decode($working_schedule->slot_detail, true);
            // for update existing slots
            $slotId = 0;
            
            if(($request->has('slot_id') && !empty($request->get('slot_id'))) || $slotId>0){
                $slot_id = ($slotId) ? $slotId : $request->get('slot_id');
                $exist = array_search($slot_id, array_column($slot_detail_db, 'slot_id'));
                $slot_detail["slot_id"] = $slot_id;
                $slot_detail_db[$exist] = $slot_detail;
            }else{
                //for insert multiple slots
                \Log::info('hererererere');
                if($this->isTimeSlotSettingExistAlready($slot_detail_db, $request->get('open_time'), $request->get('close_time'))){
                    \Log::info('setting override');
                    return [];

                }
                $slot_detail["slot_id"] = max(array_column($slot_detail_db, 'slot_id'))+1;
                array_push($slot_detail_db, $slot_detail);
            }
            $inputData['slot_detail'] = json_encode($slot_detail_db);
        }else{
            //for insert first time
            $slot_detail["slot_id"] = 1;
            $inputData['slot_detail'] = json_encode([$slot_detail]);
        }
        return $inputData;
    }

    public function getSlotDetail($schd_id){
        //$schd_id is the day of the weeek, 0 for sun, 1 for monday and so on
        $type = Input::get('type');
        $slot_id = Input::get('slot_id');
        $slot_name = Input::get('slot_name');
        $slot_detail = array();
        if($type == config('reservation.working_hour_type.day')){
            $calendar_days = config('reservation.calendar_days');
            $calendar_day = $calendar_days[$schd_id];
            $inputData['calendar_day'] = $calendar_day;
            $slotData = VisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)
                ->where('calendar_day', $calendar_day)
                ->where('slot_name', $slot_name)
                ->first();
        }else{
            $slotData = CustomVisibilityHours::find($schd_id);
        }
        if($slotData){
            $slot_details = json_decode($slotData->slot_detail, true);
            if($slot_details){
                $exist = array_search($slot_id, array_column($slot_details, 'slot_id'));
                $slot_detail = $slot_details[$exist];
                $slot_detail['schd_id'] = $schd_id;
                $slot_detail['pk_id'] = $slotData->id;
                $slot_detail['slot_name'] = $slotData->slot_name;
                $slot_detail['is_dayoff'] = $slotData->is_dayoff;
            }
        }
        return json_encode(array("slot_detail" => $slot_detail));
    }

    public function getCustomStartEndDate($slot_id){
        $custom = CustomVisibilityHours::find($slot_id);
        $custom_dates = ($custom)? $custom->calendar_date: NULL;
        return $custom_dates;
    }

    public function subRangeExistInRange($date_range, $action)
    {
        $date_range_arr = json_decode($date_range, true);
        $subRangeCount = CustomVisibilityHours::where('restaurant_id', \Auth::user()->restaurant_id)->where(function($query) use ($date_range_arr) {
            return $query->where('calendar_date->start_date', '<=', $date_range_arr['start_date'])
                ->where('calendar_date->end_date', '>=', $date_range_arr['start_date']);
        })->orWhere(function($query) use ($date_range_arr) {
            return $query->where('calendar_date->start_date', '<=', $date_range_arr['end_date'])
                ->where('calendar_date->end_date', '>=', $date_range_arr['end_date']);
        });

        if($action=='update'){
            return false;
        }
        $subRangeCount = $subRangeCount->count();
        if($subRangeCount){
            return true;
        }
        return false;
    }

    public function isTimeSlotSettingExistAlready($slot_detail, $start_time, $end_time)
    {
        if($slot_detail<=0){
            return false;
        }
        foreach ($slot_detail as $slot) {
            if(!rangesNotOverlapOpen($slot['open_time'], $slot['close_time'], $start_time, $end_time) || !rangesNotOverlapClosed($slot['open_time'], $slot['close_time'], $start_time, $end_time)){
                return true;
            }
        }
        return false;
    }

    private function getExistingSlotIdForDayRepeat($slot_detail_db, $open_time, $close_time)
    {
        foreach ($slot_detail_db as $slot_db){
            if($slot_db['open_time']==$open_time && $slot_db['close_time']==$close_time){
                return $slot_db['slot_id'];
            }
        }
        return 0;
    }

    public function getVisibilityDuration(){
        return view('visibility_duration/visibility_all');
    }

    public function fetchSlotDetail(Request $request){

        if($request->ajax()){
            $slotName = base64_decode($request->input('slot_name'));

            $slotData = VisibilityHours::where('slot_name', '=', $slotName)->get();
            $responseData = '<table class="table">
                                <thead>
                                <tr><h4 class="modal-title">Slot Detail for '.$slotName.'</h4></tr>
                                <tr>
                                    <th scope="col">Day</th>
                                    <th scope="col">Duration</th>
                                </tr>
                                </thead>
                                <tbody>';
            foreach ($slotData as $data){
                $responseData .= '<tr>
                                    <td><b>'.ucfirst($data->calendar_day).'<b></td>
                                    <td><table border="1px #efefef dotted" style="width: 95%;">';
                        if($data->slot_detail != ''){
                            $jslotDetail = json_decode($data->slot_detail);
                            foreach ($jslotDetail as $jslotData){
                                $responseData .= '<tr><td><b>Slot No</b></td><td><b>'.$jslotData->slot_id.'</b></td></tr>';
                                $responseData .= '<tr><td>Open Time</td><td>'.$jslotData->open_time.'</td></tr>';
                                $responseData .= '<tr><td>Close Time</td><td>'.$jslotData->close_time.'</td></tr>';
                            }
                        }else{
                            $responseData .= 'No Slots selected.';
                        }
                $responseData .= '</table></td>';
                $responseData .= '</tr>';
            }

            $responseData .= '</tbody>
                            </table>';
            return $responseData;

        }
    }
}