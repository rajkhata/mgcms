<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\UserOrderController;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

      /*  $schedule->command('queue:work')
                   ->everyMinute();
        $schedule->command('queue:work --tries=3')
        ->name('queue processing')
        ->withoutOverlapping();*/
	 $schedule->call(function () {
             app('App\Http\Controllers\UserOrderController')->fetchOrderOrderStatusFromStuart();
         })->everyMinute();
	 $schedule->call(function () {
            app('App\Http\Controllers\MoveOrderToActiveController')->index();
         })->everyMinute();
	/*$schedule->call(function () {
            app('app\Http\Controllers\UserOrderController')->fetchOrderOrderStatusFromStuart();
        })->everyMinute();
       /* $schedule->call(function () {
            app('Modules\Reservation\Http\Controllers\ReservationController')->releaseReservationAmountCron();
        })->dailyAt('1:00'); 
	///$schedule->call('app\Http\Controllers\UserOrderController@syncTwinJetOrderStatus')->everyMinute();
	    //$schedule->call(join('@', [ UserOrderController::class, 'syncTwinJetOrderStatus' ]))->everyFiveMinutes();
        //Refund amount when customer attempted reservation successfully
        
//        $schedule->call(function () {
//            app('App\Http\Controllers\MoveOrderToArchiveController')->index();
//        })->hourly();
//        
//        $schedule->call(function () {
//            app('App\Http\Controllers\MoveOrderToActiveController')->index();
//        })->everyTwoMinute();
        
        $schedule->call(function () {
            app('Modules\Reservation\Http\Controllers\ReservationController')->releaseReservationAmountCron();
        })->dailyAt('1:00');

        //Expire payment link after 15 minute of payment link generation if payment not done
        $schedule->call(function () {
            app('Modules\Reservation\Http\Controllers\ReservationController')->paymentLinkExpireCron();
        })->everyMinute();
        
//        $schedule->call(function () {
//            app('app\Http\Controllers\CallSentController')->sentCall();
//        })->everyMinute(); */
        

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
