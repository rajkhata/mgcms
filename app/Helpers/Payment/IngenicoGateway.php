<?php


use Illuminate\Http\Request;
use Ingenico\Connect\Sdk\ApiException;
use Ingenico\Connect\Sdk\Client;
use Ingenico\Connect\Sdk\Communicator;
use Ingenico\Connect\Sdk\CommunicatorConfiguration;
use Ingenico\Connect\Sdk\DeclinedPaymentException;
use Ingenico\Connect\Sdk\DefaultConnection;
use Ingenico\Connect\Sdk\Domain\Definitions\Address;
use Ingenico\Connect\Sdk\Domain\Definitions\AmountOfMoney;
use Ingenico\Connect\Sdk\Domain\Definitions\Card;
use Ingenico\Connect\Sdk\Domain\Definitions\CompanyInformation;
use Ingenico\Connect\Sdk\Domain\Payment\CreatePaymentRequest;
use Ingenico\Connect\Sdk\Domain\Payment\CreatePaymentResponse;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\AddressPersonal;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\CardPaymentMethodSpecificInput;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\ContactDetails;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\Customer;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\LineItem;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\LineItemInvoiceData;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\Order;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\OrderInvoiceData;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\OrderReferences;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\PersonalInformation;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\PersonalName;
use Ingenico\Connect\Sdk\ValidationException;



use DateTime;
use Ingenico\Connect\Sdk\CallContext;
use Ingenico\Connect\Sdk\ClientTestCase;
use Ingenico\Connect\Sdk\Domain\Payment\ApprovePaymentRequest;
use Ingenico\Connect\Sdk\Domain\Payment\CancelApprovalPaymentResponse;
use Ingenico\Connect\Sdk\Domain\Payment\CancelPaymentResponse;
use Ingenico\Connect\Sdk\Domain\Payment\PaymentApprovalResponse;
use Ingenico\Connect\Sdk\Domain\Payment\PaymentResponse;
use Ingenico\Connect\Sdk\Domain\Payment\TokenizePaymentRequest;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\ApprovePaymentNonSepaDirectDebitPaymentMethodSpecificInput;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\OrderApprovePayment;
use Ingenico\Connect\Sdk\Domain\Payment\Definitions\OrderReferencesApprovePayment;
use Ingenico\Connect\Sdk\Domain\Token\CreateTokenResponse;


Class IngenicoGateway{

    public function placeOrder($data) {
        $client = $this->getClient();
        $merchantId = 1056;
        $createPaymentRequest = new CreatePaymentRequest();
        $order = new Order();
        $amountOfMoney = new AmountOfMoney();
        $amountOfMoney->amount = 2980;
        $amountOfMoney->currencyCode = "EUR";
        $order->amountOfMoney = $amountOfMoney;
        $customer = new Customer();
        $customer->merchantCustomerId = "1234";
        $customer->locale = "en_GB";
        //$customer->vatNumber = "1234AB5678CD";
        $personalInformation = new PersonalInformation();
        $personalName = new PersonalName();
        $personalName->title = "Mr.";
        $personalName->firstName = "Wile";
        $personalName->surnamePrefix = "E.";
        $personalName->surname = "Coyote";
        $personalInformation->name = $personalName;
        $personalInformation->gender = "male";
        $personalInformation->dateOfBirth = "19490917";
        $customer->personalInformation = $personalInformation;
        $companyInformation = new CompanyInformation();
        $companyInformation->name = "Acme Labs";
        $customer->companyInformation = $companyInformation;
        $billingAddress = new Address();
        $billingAddress->street = "Desertroad";
        $billingAddress->houseNumber = "13";
        $billingAddress->additionalInfo = "b";
        $billingAddress->zip = "84536";
        $billingAddress->city = "Monument Valley";
        $billingAddress->state = "Utah";
        $billingAddress->countryCode = "US";
        $customer->billingAddress = $billingAddress;
        $shippingAddress = new AddressPersonal();
        $shippingName = new PersonalName();
        $shippingName->title = "Miss";
        $shippingName->firstName = "Road";
        $shippingName->surname = "Runner";
        $shippingAddress->name = $shippingName;
        $shippingAddress->street = "Desertroad";
        $shippingAddress->houseNumber = "1";
        $shippingAddress->additionalInfo = "Suite II";
        $shippingAddress->zip = "84536";
        $shippingAddress->city = "Monument Valley";
        $shippingAddress->state = "Utah";
        $shippingAddress->countryCode = "US";
        $customer->shippingAddress = null;//$shippingAddress;
        $contactDetails = new ContactDetails();
        $contactDetails->emailAddress = "wile.e.coyote@acmelabs.com";
        $contactDetails->emailMessageType = "html";
        $contactDetails->phoneNumber = "+123456";
        $contactDetails->faxNumber = "+12345";
        $customer->contactDetails = $contactDetails;
        $order->customer = $customer;
        $references = new OrderReferences();
        $references->merchantOrderId = 123456;
        $references->merchantReference = "AcmeOrder0001";
        $references->descriptor = "Fast and Furry-ous";
        $invoiceData = new OrderInvoiceData();
        $invoiceData->invoiceNumber = "0000001232";
        $invoiceData->invoiceDate = "20140306191503";
        $references->invoiceData = $invoiceData;
        $order->references = $references;
        $lineItem1 = new LineItem();
        $itemAmountOfMoney1 = new AmountOfMoney();
        $itemAmountOfMoney1->amount = 2500;
        $itemAmountOfMoney1->currencyCode = "EUR";
        $lineItem1->amountOfMoney = $itemAmountOfMoney1;
        $lineItemInvoiceData1 = new LineItemInvoiceData();
        $lineItemInvoiceData1->nrOfItems = "1";
        $lineItemInvoiceData1->description = "ACME Super Outfit";
        $lineItemInvoiceData1->pricePerItem = 2500;
        $lineItem1->invoiceData = $lineItemInvoiceData1;
        $lineItem2 = new LineItem();
        $itemAmountOfMoney2 = new AmountOfMoney();
        $itemAmountOfMoney2->currencyCode = "EUR";
        $itemAmountOfMoney2->amount = 480;
        $lineItem2->amountOfMoney = $itemAmountOfMoney2;
        $lineItemInvoiceData2 = new LineItemInvoiceData();
        $lineItemInvoiceData2->nrOfItems = "12";
        $lineItemInvoiceData2->description = "Aspirin";
        $lineItemInvoiceData2->pricePerItem = 40;
        $lineItem2->invoiceData = $lineItemInvoiceData2;
        $order->items = array($lineItem1, $lineItem2);
        $createPaymentRequest->order = $order;
        $cardPaymentMethodSpecificInput = new CardPaymentMethodSpecificInput();
        $cardPaymentMethodSpecificInput->paymentProductId = 4;
        $cardPaymentMethodSpecificInput->skipAuthentication = false;
        $card = new Card();
        $card->cvv = "123";
        //$card->cardNumber = "4567350000427977";
        $card->cardNumber = "4111111111111111";
        $card->expiryDate = "1220";
        $card->cardholderName = "Wile E. Coyote";
        $cardPaymentMethodSpecificInput->card = $card;
        $createPaymentRequest->cardPaymentMethodSpecificInput = $cardPaymentMethodSpecificInput;
        /** @var CreatePaymentResponse $createPaymentResponse */
        //$createPaymentResponse = $client->merchant($merchantId)->payments()->create($createPaymentRequest);
        try {
            $createPaymentResponse = $client->merchant($merchantId)->payments()->create($createPaymentRequest);
        } catch (DeclinedPaymentException $e) {
            print_r($e->getPaymentResult());die;$this->handleDeclinedPayment($e->getPaymentResult());
        } catch (ValidationException $e) {
            print_r($e->getErrors());die;$this->handleDeclinedPayment($e->getPaymentResult());
        }  catch (ApiException $e) {
            print_r($e->getErrors());die;$this->handleApiErrors($e->getErrors());
        }
        //print_r($createPaymentResponse);die;
        return $createPaymentResponse->payment->id;
    }
    protected function getClient()
    {
        if (is_null($this->client)) {
            $connection = new DefaultConnection();
            $communicatorConfiguration = $this->getCommunicatorConfiguration();
            $communicator = new Communicator($connection, $communicatorConfiguration);
            $this->client = new Client($communicator);
        }
        return $this->client;
    }
    /**
     * @return CommunicatorConfiguration
     */
    protected function getCommunicatorConfiguration()
    {
        if (is_null($this->communicatorConfiguration)) {
            $this->communicatorConfiguration = new CommunicatorConfiguration(
                '3d984f50a2a021fb',
                'AvrYcmjlJnl9pkErarP5LoK1kYfOYmuHM5h01P47oR8=',
                'https://eu.sandbox.api-ingenico.com',
                'Ingenico'
            );
        }
        return $this->communicatorConfiguration;
    }
}
