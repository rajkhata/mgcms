<?php
//sudo composer require braintree/braintree_php
//https://developers.braintreepayments.com/start/hello-server/php
//https://developers.braintreepayments.com/start/go-live/php
//https://hotexamples.com/examples/-/Braintree_Transaction/refund/php-braintree_transaction-refund-method-examples.html
/*
 * Braintress
https://sandbox.braintreegateway.com/
pdutta@munchado.biz
Reset123
Merchant ID: q7x7ccv67zrs7s99
Public Key: v77yy8rvxr96ftsf
Secret Key: ebec53c0e21423b2d49754752fd2d47e
 */
namespace App\Helpers\Payment;
use Braintree\Gateway;
use Braintree\Transaction;
use Braintree\Configuration;
use Braintree\Customer;

Class BraintressGateway{

    //You can create a transaction using an $amount and the $nonceFromTheClient you received in the previous step:
    public function __construct($config) {
        Configuration::environment($config['Environment']); // Change to production
        Configuration::merchantId($config['MerchantID']);
        Configuration::publicKey($config['PublicKey']);
        Configuration::privateKey($config['SecretKey']);
    }
    public function createRefund($requestData) {
        $result = Transaction::refund($requestData['transactionId'],$requestData['amount']);//echo "111==========>";//Transaction::refund($transactionId, '150.00')
        //print_r($result);die;
        if ($result->success) {
            if ($result->transaction->id) {
                $d['id'] = $result->transaction->id;
                $d['response'] = $result;
                $d['errorMesg'] = null;
            }
        }else {
            $msg = null;
            foreach ($result->errors->deepAll() as $error) {
                $msg .= ($error->code . ": " . $error->message . "<br />");
            }
            $d['errorMesg'] = $msg;
        }

        return $d;
    }

    public function pleaceOrderByCard($requestData) {

        $result = Transaction::sale($requestData);//echo "111==========>";//
        //print_r($result);die;
        if ($result->success) {
            if ($result->transaction->id) {
                $d['id']  = $result->transaction->id;
                $d['last4']  = $result->transaction->creditCardDetails->last4;
                $d['cardType']  = $result->transaction->creditCardDetails->cardType;
                $d['errorMesg'] = null;
            }
        }else {
            $msg = null;
            foreach ($result->errors->deepAll() as $error) {
                $msg .= ($error->code . ": " . $error->message . "<br />");
            }
            $d['errorMesg'] = $msg;
        }

        return $d;
    }
    function createCustomer($data){

        /* First we create a new user using the BT API */
        $result = Customer::create(array(
            'firstName' => $data['first_name'],
            'lastName' => $data['last_name'],
            //'company' => $data['company'],
            'email' => $data['email'],
            'phone' => $data['phone'],

            // we can create a credit card at the same time
            'creditCard' => array(
                'cardholderName' => $data['name'],
                'number' => $data['number'],
                'expirationMonth' => $data['exp_month'],
                'expirationYear' => $data['exp_year'],
                'cvv' => $data['cvv'],
                'billingAddress' => array(
                    'firstName' => $data['first_name'],
                    'lastName' => $data['last_name'])
                    /*Optional Information you can supply
                     'company' => $data['company'],
                     'streetAddress' => $data['user_address'],
                     'locality' => $data['user_city'],
                     'region' => $data['user_state'], 
                     //'postalCode' => $data['zip_code'],
                     'countryCodeAlpha2' => $data['user_country'])
                    */
                )
            )
        );
        if ($result->success) {
            //Do your stuff
            $d['creditCardToken'] = $creditCardToken = $result->customer->creditCards[0]->token;
            $d['id'] = $result->customer->id;
            $d['errorMesg'] = null;
            //echo("Customer ID: " . $result->customer->id . "<br />");
            //echo("Credit card ID: " . $result->customer->creditCards[0]->token . "<br />");
        } else {
            $msg = null;
            foreach ($result->errors->deepAll() as $error) {
               $msg .= ($error->code . ": " . $error->message . "<br />");
            }
            $d['errorMesg'] = $msg;
        }
        return $d;
    }
}
