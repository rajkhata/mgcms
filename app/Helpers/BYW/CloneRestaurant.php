<?php


namespace App\Helpers\BYW;


use App\Helpers\CommonFunctions;
use App\Models\BYW\CmsRestaurantOperationCustomTime;
use App\Models\BYW\CmsRestaurantOperationWeekTime;
use App\Models\BYW\CmsUser;
use App\Models\BYW\MenuCategories;
use App\Models\BYW\MenuCategoryLanguage;
use App\Models\BYW\MenuSubCategories;
use App\Models\BYW\MenuSubCategoryLanguage;
use App\Models\BYW\NewMenu\ItemAddonGroups;
use App\Models\BYW\NewMenu\ItemAddonGroupsLanguage;
use App\Models\BYW\NewMenu\ItemAddons;
use App\Models\BYW\NewMenu\ItemModifier;
use App\Models\BYW\NewMenu\ItemModifierGroup;
use App\Models\BYW\NewMenu\ItemModifierGroupLanguage;
use App\Models\BYW\NewMenu\ItemModifierLanguage;
use App\Models\BYW\NewMenu\MenuItemModel;
use App\Models\BYW\NewMenu\MenuItemsLanguage;
use App\Models\BYW\NewMenu\ModifierCategories;
use App\Models\BYW\RestaurantCarryoutCustomeTime;
use App\Models\BYW\RestaurantCarryoutWeekTime;
use App\Models\BYW\RestaurantDeliveryCustomTime;
use App\Models\BYW\RestaurantDeliveryWeekTime;
use App\Models\BYW\Restaurants;
use App\Models\BYW\RestaurantVisiblityCustomTime;
use App\Models\BYW\RestaurantVisiblityWeekTime;
use App\Models\BYW\SBPages;

class CloneRestaurant
{
    public $date,$catlimit,$itmelimit;

    public function __construct()
    {
        $this->date = date("Y-m-d H:i");
        $this->itmelimit = $this->catlimit = 1000;
    }

    public function coneRestaurnatData($old_rest_id,$parent_id=0,$otherInfo=[],$is_default_outlet=0)	{
        $restaurantArray = Restaurants::select('*')->where('id',$old_rest_id)->limit(1)->get()->toArray();
        $new_id = 0;
        foreach($restaurantArray as $k=>$d){
            $d['restaurant_name']=isset($otherInfo['restaurant_name'])?$otherInfo['restaurant_name']:'';
            $d['contact_person_title']=isset($otherInfo['contact_person_title'])?$otherInfo['contact_person_title']:'';

            $d['contact_person']=isset($otherInfo['contact_person'])?$otherInfo['contact_person']:'';
            $d['contact_address']=isset($otherInfo['contact_address'])?$otherInfo['contact_address']:'';
            $d['contact_address2']=isset($otherInfo['contact_address2'])?$otherInfo['contact_address2']:'';
            $d['contact_street']=isset($otherInfo['contact_street'])?$otherInfo['contact_street']:'';
            $d['contact_city_id']=isset($otherInfo['contact_city_id'])?$otherInfo['contact_city_id']:0;
            $d['contact_country']=isset($otherInfo['contact_country'])?$otherInfo['contact_country']:'';
            $d['contact_state']=isset($otherInfo['contact_state'])?$otherInfo['contact_state']:'';
            $d['contact_zipcode']=isset($otherInfo['contact_zipcode'])?$otherInfo['contact_zipcode']:'';
            $d['contact_phone']=isset($otherInfo['contact_phone'])?$otherInfo['contact_phone']:'';
            $d['contact_email']=isset($otherInfo['contact_email'])?$otherInfo['contact_email']:'';
            $d['slug']=$this->generateSlug($otherInfo['restaurant_name']);
            $d['pickup_contact_person_title']=isset($otherInfo['pickup_contact_person_title'])?$otherInfo['pickup_contact_person_title']:'';
            $d['pickup_contact_person']=isset($otherInfo['pickup_contact_person'])?$otherInfo['pickup_contact_person']:'';
            $d['pickup_contact_email']=isset($otherInfo['pickup_contact_email'])?$otherInfo['pickup_contact_email']:'';
            $d['address']=isset($otherInfo['address'])?$otherInfo['address']:'';
            $d['address2']=isset($otherInfo['address2'])?$otherInfo['address2']:'';
            $d['street']=isset($otherInfo['street'])?$otherInfo['street']:'';
            $d['city_id']=isset($otherInfo['city_id'])?$otherInfo['city_id']:0;
            $d['pickup_country']=isset($otherInfo['pickup_country'])?$otherInfo['pickup_country']:'';
            $d['pickup_state']=isset($otherInfo['pickup_state'])?$otherInfo['pickup_state']:'';
            $d['zipcode']=isset($otherInfo['zipcode'])?$otherInfo['zipcode']:'';
            $d['phone']=isset($otherInfo['phone'])?$otherInfo['phone']:'';
            $d['email']=isset($otherInfo['email'])?$otherInfo['email']:'';
            $d['is_default_outlet']=isset($otherInfo['is_default_outlet'])?$otherInfo['is_default_outlet']:0;
            unset($d['meta_keyword']);unset($d['meta_tags']);
            //$d['updated_at ']=date("Y_m-d:H:i");
            $m = Restaurants::create($d);
            $new_id = $m->id;
            $this->cloneCmsUser($old_rest_id,$new_id,$otherInfo,$parent_id);
        }
        return $new_id;

    }
    public function generateSlug($slug){
        $newstr = preg_replace('/[^a-zA-Z0-9\']/', '-', $slug);
        $newstr = str_replace("'", '', $newstr);
        return trim($newstr);
    }
    public function cloneCmsUser($rest_id,$new_rest_id=0,$otherInfo=[],$parent_id=0)	{
        $cmsArray = CmsUser::select('*')->where('restaurant_id',$rest_id)->limit(1)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;
            $d['name']=$otherInfo['contact_person'];
            $d['email']=$otherInfo['contact_email'];
            $d['salt'] = null;
            $d['status'] = 1;
            //$d['created_at']=$this->date;
            //$d['updated_at ']=$this->date;
            $m = CmsUser::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }

    public function cloneRestaurantVisiblityCustomTime($rest_id,$new_rest_id=0)	{
        $cmsArray = RestaurantVisiblityCustomTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;
            $d['created_at']=$this->date;
            $d['updated_at']=$this->date;
            $m = RestaurantVisiblityCustomTime::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }
    public function cloneRestaurantVisiblityWeekTime($rest_id,$new_rest_id=0)	{
        $cmsArray = RestaurantVisiblityWeekTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;
            $d['created_at']=$this->date;
            $d['updated_at']=$this->date;
            $m = RestaurantVisiblityWeekTime::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }

    public function cloneRestaurantOperationWeekTime($rest_id,$new_rest_id=0)	{
        $cmsArray = CmsRestaurantOperationWeekTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;

            $m = CmsRestaurantOperationWeekTime::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }
    public function cloneRestaurantOperationCustomTime($rest_id,$new_rest_id=0)	{
        $cmsArray = CmsRestaurantOperationCustomTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;

            $m = CmsRestaurantOperationCustomTime::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }

    public function cloneRestaurantCarryoutCustomeTime($rest_id,$new_rest_id=0)	{
        $cmsArray = RestaurantCarryoutCustomeTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;

            $m = RestaurantCarryoutCustomeTime::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }
    public function cloneRestaurantCarryoutWeekTime($rest_id,$new_rest_id=0)	{
        $cmsArray = RestaurantCarryoutWeekTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;

            $m = RestaurantCarryoutWeekTime::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }

    public function cloneRestaurantDeliveryWeekTime($rest_id,$new_rest_id=0)	{
        $cmsArray = RestaurantDeliveryWeekTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;

            $m = RestaurantDeliveryWeekTime::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }
    public function cloneRestaurantDeliveryCustomTime($rest_id,$new_rest_id=0)	{
        $cmsArray = RestaurantDeliveryCustomTime::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;

            $m = RestaurantDeliveryCustomTime::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }
    public function cloneSBPages($rest_id,$new_rest_id=0)	{
        $rest_id = 74;
        $cmsArray = SBPages::select('*')->where('restaurant_id',$rest_id)->get()->toArray();
        $new_id = 0;
        foreach($cmsArray as $k=>$d){
            $d['restaurant_id']=$new_rest_id;

            $m = SBPages::create($d);
            $new_id = $m->id;
        }
        return $new_id;

    }
    public function coneNewMenuCategoryData($new_restaurant_id,$restaurnat_id)	{
        //first copy modifier category data based on restaurnat id
        $CategoryArray1 = null;
        $CategoryArray1['modifierCategory'] = $this->coneNewMenuModfierCategoryData($CategoryArray1,$new_restaurant_id,$restaurnat_id);
        // copy all hour data
        $CategoryArray1['VisiblityCustom'] = $this->cloneRestaurantVisiblityCustomTime($restaurnat_id,$new_restaurant_id);
        $CategoryArray1['VisiblityWeek'] = $this->cloneRestaurantVisiblityWeekTime($restaurnat_id,$new_restaurant_id);
        $CategoryArray1['OperationWeek'] = $this->cloneRestaurantOperationWeekTime($restaurnat_id,$new_restaurant_id);
        $CategoryArray1['OperationCustom'] = $this->cloneRestaurantOperationCustomTime($restaurnat_id,$new_restaurant_id);
        $CategoryArray1['CarryoutCustom'] = $this->cloneRestaurantCarryoutCustomeTime($restaurnat_id,$new_restaurant_id);
        $CategoryArray1['CarryoutWeek'] = $this->cloneRestaurantCarryoutWeekTime($restaurnat_id,$new_restaurant_id);
        $CategoryArray1['DeliveryWeek'] = $this->cloneRestaurantDeliveryWeekTime($restaurnat_id,$new_restaurant_id);
        $CategoryArray1['DeliveryCustom'] = $this->cloneRestaurantDeliveryCustomTime($restaurnat_id,$new_restaurant_id);
        //$CategoryArray1['SBPages'] = $this->cloneSBPages($restaurnat_id,$new_restaurant_id);
        // start coneing category and its realted data
        $CategoryArray = MenuCategories::select('*')->where('restaurant_id',$restaurnat_id)->where('status',1)->limit($this->catlimit)->get()->toArray();


        foreach($CategoryArray as $k=>$d){

            $d['restaurant_id'] =$new_restaurant_id;

            $CategoryArray1['menuCategory'][$k]['id'] = $d['id'];
            unset($d['id']);
            $m = MenuCategories::create($d);
            $d['old_restaurant_id'] = $d['restaurant_id'];
            $CategoryLanguageArray = MenuCategoryLanguage::select('*')->where('menu_categories_id',$CategoryArray1['menuCategory'][$k]['id'])->get()->toArray();
            foreach($CategoryLanguageArray as $languageD){
                $languageD['menu_categories_id']=$m->id;

                $m1 = MenuCategoryLanguage::create($languageD);
            }
            $CategoryArray1['menuCategory'][$k]['restaurant_id'] =$new_restaurant_id;
            $CategoryArray1['menuCategory'][$k]['new_cat_id'] = $m->id;
        }

        $CategoryArray1 = $this->coneNewMenuSubCategoryData($CategoryArray1);

        return $this->coneNewMenuItemData($CategoryArray1);

    }
    public function coneNewMenuSubCategoryData($CategoryArray)	{
        if(is_array($CategoryArray['menuCategory'])){
            foreach($CategoryArray['menuCategory'] as $i=>$d){
                $a = null;
                $SubCategoryArray = MenuSubCategories::select('*')->where('menu_category_id',$d['id'])->where('status',1)->get()->toArray();
                foreach($SubCategoryArray as $k=>$subD){
                    $subD['restaurant_id'] = $d['restaurant_id'];
                    $subD['menu_category_id'] = $d['new_cat_id'];

                    $a[$k]['id'] = $subD['id'];
                    unset($subD['id']);
                    $m = MenuSubCategories::create($subD);
                    $CategoryLanguageArray = MenuSubCategoryLanguage::select('*')->where('menu_sub_categories_id',$a[$k]['id'])->get()->toArray();
                    foreach($CategoryLanguageArray as $languageD){
                        $languageD['menu_sub_categories_id']=$m->id;

                        $m1 = MenuSubCategoryLanguage::create($languageD);
                    }
                    $a[$k]['new_sub_cat_id'] = $m->id;
                }
                $CategoryArray['menuCategory'][$i]['subcategory'] = $a;

            }
        }

        return $CategoryArray;
    }
    public function coneNewMenuItemData($CategoryArray){
        if(is_array($CategoryArray['menuCategory'])){
            foreach($CategoryArray['menuCategory'] as $i=>$cat){
                if(is_array($cat['subcategory'])){
                    foreach($cat['subcategory'] as $j=>$sub){
                        $a = null;
                        $menuItemArrayBySubCategory = MenuItemModel::select('*')->where('menu_sub_category_id',$sub['id'])->where('status',1)->limit($this->itmelimit)->get()->toArray();
                        foreach($menuItemArrayBySubCategory as $k=>$menuItem){
                            $menuItem['restaurant_id'] = $cat['restaurant_id'];
                            $menuItem['menu_category_id'] = $cat['new_cat_id'];
                            $menuItem['menu_sub_category_id'] = $sub['new_sub_cat_id'];

                            $a[$k]['id'] = $menuItem['id'];
                            unset($menuItem['id']);
                            $m = MenuItemModel::create($menuItem);
                            $menuItemLanguageArray = MenuItemsLanguage::select('*')->where('new_menu_items_id',$a[$k]['id'])->get()->toArray();
                            foreach($menuItemLanguageArray as $menulanguageD){
                                $menulanguageD['new_menu_items_id']=$m->id;

                                $m1 = MenuItemsLanguage::create($menulanguageD);
                            }
                            $a[$k]['new_menu_id'] = $m->id;
                        }
                        $CategoryArray['menuCategory'][$i]['menuItems'] = $a;
                    }
                }else{ //menu item without any sub category mapping
                    $a = null;
                    $menuItemArrayByCategory = MenuItemModel::select('*')->where('menu_category_id',$cat['id'])->where('status',1)->limit($this->itmelimit)->get()->toArray();
                    foreach($menuItemArrayByCategory as $k=>$menuItem){
                        $menuItem['restaurant_id'] = $cat['restaurant_id'];
                        $menuItem['menu_category_id'] = $cat['new_cat_id'];
                        $menuItem['menu_sub_category_id'] = NULL;

                        $a[$k]['id'] = $menuItem['id'];
                        unset($menuItem['id']);
                        $m = MenuItemModel::create($menuItem);
                        $menuItemLanguageArray = MenuItemsLanguage::select('*')->where('new_menu_items_id',$a[$k]['id'])->get()->toArray();
                        foreach($menuItemLanguageArray as $menulanguageD){
                            $menulanguageD['new_menu_items_id']=$m->id;

                            $m1 = MenuItemsLanguage::create($menulanguageD);
                        }
                        $a[$k]['new_menu_id'] = $m->id;
                        $CategoryArray['menuCategory'][$i]['menuItems'] = $a;

                    }

                }
            }
        }
        //create modifier group and realted data
        $CategoryArray = $this->coneNewMenuModfierGroupData($CategoryArray);
        //create adon group and realted data
        $CategoryArray = $this->coneNewMenuItemAdonsGroupData($CategoryArray);
        return $CategoryArray;
    }
    public function coneNewMenuItemAdonsGroupData($CategoryArray)	{
        if(is_array($CategoryArray['menuCategory'])){
            foreach($CategoryArray['menuCategory'] as $i=>$d){
                $a = null;
                if(isset($d['menuItems'])){
                    foreach($d['menuItems'] as $j=>$menuItem){
                        $mCategoryArray = ItemAddonGroups::select('*')->where('menu_item_id',$menuItem['id'])->get()->toArray();
                        foreach($mCategoryArray as $k=>$catD){
                            $catD['restaurant_id'] = $d['restaurant_id'];
                            $catD['menu_item_id'] = $menuItem['new_menu_id'];

                            $a[$k]['id'] = $catD['id'];
                            unset($catD['id']);
                            $m = ItemAddonGroups::create($catD);
                            $menuItemModGrpLanguageArray = ItemAddonGroupsLanguage::select('*')->where('item_addon_group_id',$a[$k]['id'])->get()->toArray();
                            foreach($menuItemModGrpLanguageArray as $menulanguageD){
                                $menulanguageD['item_addon_group_id']=$m->id;

                                $m1 = ItemAddonGroupsLanguage::create($menulanguageD);
                            }
                            $a[$k]['new_adon_grp_id'] = $m->id;
                            $CategoryArray['menuCategory'][$i]['menuItems'][$j]['adonGroup'] = $a;
                        }

                    }}
            }
        }
        //create modifier group and realted data
        $CategoryArray = $this->coneNewMenuAdonGroupItemData($CategoryArray);
        return $CategoryArray;
    }
    public function coneNewMenuAdonGroupItemData($CategoryArray)	{
        if(is_array($CategoryArray['menuCategory'])){
            foreach($CategoryArray['menuCategory'] as $i=>$d){   //categoryData $d
                if(isset($d['menuItems'])){
                    foreach($d['menuItems'] as $j=>$menuItem){ //menuItemData $d1
                        if(isset($menuItem['adonGroup'])){
                            foreach($menuItem['adonGroup'] as $k=>$adonGroup){  //adonGroupData $d2
                                $a = null;
                                $adonArray = ItemAddons::select('*')->where('addon_group_id',$adonGroup['id'])->get()->toArray();
                                foreach($adonArray as $l=>$adonItemArray){
                                    $adonItemArray['addon_group_id'] = $adonGroup['new_adon_grp_id'];
                                    $adonItemArray['restaurant_id'] = $d['restaurant_id'];
                                    $adonItemArray['item_id'] = $menuItem['new_menu_id'];

                                    $a[$k]['id'] = $adonItemArray['id'];
                                    unset($adonItemArray['id']);
                                    $m = ItemAddons::create($adonItemArray);

                                    $a[$k]['new_adon_item_id'] = $m->id;
                                }$CategoryArray['menuCategory'][$i]['menuItems'][$j]['adonGroup'][$k] = $a;
                            }
                        }//if exist modifierGroup Array
                    }}


            }
        }
        return $CategoryArray;
    }
    public function coneNewMenuModfierGroupData($CategoryArray)	{
        if(is_array($CategoryArray['menuCategory'])){
            foreach($CategoryArray['menuCategory'] as $i=>$d){
                $a = null;
                if(isset($d['menuItems'])){
                    foreach($d['menuItems'] as $j=>$menuItem){
                        $mCategoryArray = ItemModifierGroup::select('*')->where('menu_item_id',$menuItem['id'])->get()->toArray();
                        foreach($mCategoryArray as $k=>$catD){
                            $catD['restaurant_id'] = $d['restaurant_id'];
                            $catD['menu_item_id'] = $menuItem['new_menu_id'];

                            $a[$k]['id'] = $catD['id'];
                            unset($catD['id']);
                            $m = ItemModifierGroup::create($catD);
                            $menuItemModGrpLanguageArray = ItemModifierGroupLanguage::select('*')->where('modifier_group_id',$a[$k]['id'])->get()->toArray();
                            foreach($menuItemModGrpLanguageArray as $menulanguageD){
                                $menulanguageD['modifier_group_id']=$m->id;

                                $m1 = ItemModifierGroupLanguage::create($menulanguageD);
                            }
                            $a[$k]['new_mod_grp_id'] = $m->id;
                            $CategoryArray['menuCategory'][$i]['menuItems'][$j]['modifierGroup'] = $a;
                        }

                    }}
            }
        }
        //create modifier group and realted data
        $CategoryArray = $this->coneNewMenuModfierGroupItemData($CategoryArray);
        //print_r($CategoryArray);
        return $CategoryArray;
    }
    public function coneNewMenuModfierGroupItemData($CategoryArray)	{
        if(is_array($CategoryArray['menuCategory'])){$a =$newRestaurantId = $newMenuId = $OldMenuId = 0;
            foreach($CategoryArray['menuCategory'] as $i=>$d){   //categoryData $d
                if(isset($d['menuItems'])){
                    foreach($d['menuItems'] as $j=>$menuItem){ //menuItemData $d1
                        if(isset($menuItem['modifierGroup'])){
                            foreach($menuItem['modifierGroup'] as $k=>$modifierGroup){  //modifierGroupData $d2
                                $a = null;
                                $modifierArray = ItemModifier::select('*')->where('modifier_group_id',$modifierGroup['id'])->get()->toArray();
                                foreach($modifierArray as $l=>$modifierItemD){
                                    $modifierItemD['modifier_group_id'] = $modifierGroup['new_mod_grp_id'];
                                    $mod_cat_key_indx = $modifierItemD['modifier_category_id'];
                                    $modifierItemD['modifier_category_id'] = $CategoryArray['modifierCategory'][$mod_cat_key_indx]??0;

                                    $a[$k]['id'] = $modifierItemD['id'];
                                    unset($modifierItemD['id']);
                                    $m = ItemModifier::create($modifierItemD);
                                    $menuItemModGrpLanguageArray = ItemModifierLanguage::select('*')->where('modifier_item_id',$a[$k]['id'])->get()->toArray();
                                    foreach($menuItemModGrpLanguageArray as $menulanguageD){
                                        $menulanguageD['modifier_item_id']=$m->id;

                                        $m1 = ItemModifierLanguage::create($menulanguageD);
                                    }
                                    $a[$k]['new_modifier_item_id'] = $m->id;
                                }$CategoryArray['menuCategory'][$i]['menuItems'][$j]['modifierGroup'][$k] = $a;
                            }
                        }//if exist modifierGroup Array
                    }}
                //$CategoryArray[$i][$newMenuId]['modifierGroup'] = $a;

            }
        }
        return $CategoryArray;
    }
    public function coneNewMenuModfierCategoryData($CategoryArray,$new_restaurant_id,$restaurnat_id)	{
        $a = null;
        $mCategoryArray = ModifierCategories::select('*')->where('restaurant_id',$restaurnat_id)->get()->toArray();
        foreach($mCategoryArray as $k=>$catD){
            $catD['restaurant_id'] = $new_restaurant_id;
            $id = $catD['id'];
            unset($catD['id']);
            $m = ModifierCategories::create($catD);
            $a[$id] = $m->id;
        }
        $CategoryArray = $a;

        return $CategoryArray;
    }
    public static function sendmailtoclient($toEmail,$cmsUser,$toName){
        $mailBodyTemplate = self::getMailTemplate();

        $mailData['subject']= "Your Munch Ado Website is Ready to Be Personalized!";
        $SUB_HEAD =  "<b>Get Started Today</b><br><br>The power of the internet is now in your hands.<br>Use the username and password below to start creating your website.<br><br>Email:- ".$cmsUser."<br>Password:- admin@123<br><br>Login Here:- https://cms-nkd.munchadoshowcase.biz<br><br>We can't wait to see the experiences you're able to create for your customers online with Munch Ado.";
        $mailBodyTemplate  =  str_replace("[SUB_HEAD]",$SUB_HEAD,$mailBodyTemplate);
        $mailBodyTemplate  =  str_replace("[CUST_NAME]",$toName,$mailBodyTemplate);
        $mailBodyTemplate  =  str_replace("[REST_NAME]","NKD Pizza",$mailBodyTemplate);
        $mailData['body'] =$mailBodyTemplate;
        $mailData['receiver_email'] =  $toEmail;
        $mailData['receiver_name']  =  $toName;
        $mailData['MAIL_FROM_NAME'] =  "NKD Pizza";
        $mailData['MAIL_FROM'] = $toEmail;
        CommonFunctions::sendMail($mailData);
    }
    public static function getMailTemplate(){
        return  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background:#fafafa!important">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>My Email Templates</title>
    <style>@media only screen{html{min-height:100%;background:#fff}}@media only screen and (max-width:654px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important}.small-text-center{text-align:center!important}.small-text-left{text-align:left!important}.small-text-right{text-align:right!important}}@media only screen and (max-width:654px){.hide-for-large{display:block!important;width:auto!important;overflow:visible!important;max-height:none!important;font-size:inherit!important;line-height:inherit!important}}@media only screen and (max-width:654px){table.body table.container .hide-for-large{display:table!important;width:100%!important}}@media only screen and (max-width:654px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width:654px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:24px!important;padding-right:24px!important}table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .columns{padding-left:0!important;padding-right:0!important}th.small-3{display:inline-block!important;width:25%!important}th.small-5{display:inline-block!important;width:41.66667%!important}th.small-6{display:inline-block!important;width:50%!important}th.small-7{display:inline-block!important;width:58.33333%!important}th.small-9{display:inline-block!important;width:75%!important}th.small-12{display:inline-block!important;width:100%!important}.columns th.small-12{display:block!important;width:100%!important}table.menu{width:100%!important}table.menu td,table.menu th{width:auto!important;display:inline-block!important}table.menu.small-vertical td,table.menu.small-vertical th,table.menu.vertical td,table.menu.vertical th{display:block!important}table.menu[align=center]{width:auto!important}}</style>
</head>
<body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#fafafa!important;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important">
<span class="preheader" style="color:#fff;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden"></span>
<table class="body" style="Margin:0;background:#fafafa!important;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%">
    <tr style="padding:0;text-align:left;vertical-align:top">
        <td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
            <center data-parsed="" style="min-width:630px;width:100%">


                <table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="24px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:24px;font-weight:400;hyphens:auto;line-height:24px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table>

                <table align="center" class="container float-center" style="Margin:0 auto;background:0 0;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:630px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                    <table valign="middle" class="wrapper header" align="center" style="background:#fff;border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><td class="wrapper-inner" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:10px;text-align:left;vertical-align:top;word-wrap:break-word">
                        <table valign="middle" align="center" class="container" style="Margin:0 auto;background:0 0;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:630px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                            <table valign="middle" class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:center;vertical-align:top">
                                <th class="zero-padding-bottom small-12 large-12 columns first" valign="middle" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0!important;padding-left:8px;padding-right:12px;text-align:center;width:291px">
                               
                                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:center;vertical-align:top;width:100%"><tr style="padding:0;text-align:center;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:center">
                                            <center>
                                                   <a href="https://kekinyc.com/" style="Margin:0;color:#c10c27;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:center;text-decoration:none">
                                                <!-- LOGO -->
                                                <img class="small-float-center" src="https://cms-nkd.munchadoshowcase.biz/images/restaurant/nkd_pizza_logo_1447743065cf93484edc5d.png" alt="" style="-ms-interpolation-mode:bicubic;border:none;clear:both;max-width:100%;outline:0;text-decoration:none;width:auto;min-width:70px;">
                                            </a>
                                        </center>
                                        </th></tr></table>
                                   
                            
                            
                            </th>
                                
                            </tr></tbody></table>
                        </td></tr></tbody></table>
                    </td></tr></table> 
<!--Middle part --> 
<table class="row body-wrap" style="background:#ffffff;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="small-12 large-12 columns first last" style="color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:24px;padding-right:24px;text-align:left">
                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                            <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td height="40px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:40px;font-weight:400;hyphens:auto;line-height:40px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="wrapper main-body" align="center" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <td class="wrapper-inner" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;hyphens:auto;line-height:1.5;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                        <!-- START main body -->
                                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:0;margin-bottom:25px;padding:0;text-align:left">
                                            Hey <b>[CUST_NAME]</b>,
                                        </p>
                                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:0;margin-bottom:25px;padding:0;text-align:left">[SUB_HEAD]</p>
                                        
                                       <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                            <tbody>
                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                    <td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                      
                                        Best,<br/>
                                        [REST_NAME] Management
                                        <br/><br/>
                                        <!-- END main body -->
                                    </td>
                                </tr>
                            </table>
                            <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </table>
            </th>
        </tr>
    </tbody>
</table>
            <!-- end middle part --> 

<table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table>
<table class="row stores" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">

</tr></tbody></table>
<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
    <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:24px;padding-right:24px;text-align:left;width:606px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top">
        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th>
</tr></tbody></table>
<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
    <th class="small-12 large-7 columns first" style="Margin:0 auto;color:#ffffff;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:24px;padding-right:12px;text-align:left;width:343.5px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#ffffff;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:center">
        <p class="title text-center" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center">
            NKD HEADQUARTERS
        </p>
        <a href="" class="" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center;text-decoration:none; cursor:default;">
            <!-- HRI HEADQUARTERS ADDRESS -->
            6-8 Morningside Drive, EH10 5LY, London, UK<br/> 
        </a>
		<a href="tel:6468632094" style="Margin:0;Margin-bottom:10px;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center;text-decoration:none; cursor:default;">Phone:(646) 863-2094</a>
    </th></tr></table></th>

</tr></tbody></table>
<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
    <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:24px;padding-right:24px;text-align:left;width:606px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
        <center data-parsed="" style="min-width:558px;width:100%">
            <table align="center" class="menu main-menu float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:auto!important"><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top">
              <!--   <th class="menu-item float-center" style="Margin:0 auto;color:#0a0a0a;float:none;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0 14px;padding-right:5px;text-align:center"><span style="Margin:0;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">
                    
                    Terms
                </span></th>
				-->
            </tr></table></td></tr></table>
        </center>
    </th>
        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th>
</tr></tbody></table>
<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
    <hr style="border-bottom:1px solid rgba(119,119,119,.35);border-left:1px solid #1a191a;border-right:1px solid #1a191a;border-top:1px solid #1a191a;display:block;height:1px;margin:1em 0;padding:0">
</tr></tbody></table>
<table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="8px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:8px;font-weight:400;hyphens:auto;line-height:8px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table>
<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
    <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:24px;padding-right:24px;text-align:left;width:606px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
        <center data-parsed="" style="min-width:558px;width:100%">
            <table align="center" class="menu main-menu small-menu float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:auto!important"><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top">
                <th class="menu-item float-center" style="Margin:0 auto;color:#0a0a0a;float:none;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0 14px;padding-right:5px;text-align:center"><span style="Margin:0;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">
                    <!-- CONTACT EMAIL  -->
                    <span style="color:#8a8a8a">Feel Free to contact us at</span><a href="mailto:nkd@munchadoshowcase.biz" style="Margin:0;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none"> nkd@munchadoshowcase.biz </a></span></th>

            </tr></table></td></tr></table>
        </center>
    </th>
        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th>
</tr></tbody></table>
<!-- END FOOTER -->
</td></tr></tbody></table>
</td></tr></tbody></table>

<table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table>

</center>
</td>
</tr>
</table>
<!-- prevent Gmail on iOS font size manipulation -->
<div style="display:none;white-space:nowrap;font:15px courier;line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>';
    }
}