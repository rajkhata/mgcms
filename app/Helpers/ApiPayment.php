<?php
/**
     * Created by PhpStorm.
     * User: Amit Malakar
     * Date: 2/5/18
     * Time: 11:03 AM
     */

namespace App\Helpers;

use Stripe\Charge;
use Stripe\Customer;
use Stripe\Refund;
use Stripe\Stripe;
use Stripe\Token;

class ApiPayment
{
    protected $apiPay;
    
    public function __construct()
    {
        $stripeSecretKey = config('reservation.payment_gateway.stripe.secret');
        $this->apiPay    = Stripe::setApiKey($stripeSecretKey);
    }

    /**========================================
     * CUSTOMER
    ========================================*/

    /**
     * Get a particular customer
     * @param $customerId
     * @return Customer
     */
    public function getCustomer($customerId)
    {
        $customer = Customer::retrieve($customerId);

        return $customer;
    }

    /**
     * Get all customer records
     * @return \Stripe\Collection
     */
    public function getAllCustomers()
    {
        $limit = ['limit' => 10];
        return $customers = Customer::all($limit);
    }

    /**
     * Create customer
     * @param $data
     * @return Customer
     */
    public function createCustomer($data)
    {
        // email, source, description, livemode
        $customer    = Customer::create($data);

        return $customer;
    }

    /**
     * Update customer record
     * @param $customerId
     * @param $data
     * @return Customer
     */
    public function updateCustomer($customerId, $data)
    {
        $customer              = Customer::retrieve($customerId);
        $customer->description = '';
        $customer->email       = '';
        $customer->source      = 'tok_visa';
        $customer->save();

        return $customer;
    }

    /**
     * Delete customer record
     * @param $customerId
     * @return Customer
     */
    public function deleteCustomer($customerId)
    {
        $customer = Customer::retrieve($customerId);
        $customer->delete();

        return $customer;
    }


    /**========================================
     * TOKEN
     ========================================*/
    /**
     * Create token for card
     * @param $data
     * @return Token
     */
    public function createToken($data, $type='card')
    {
        $token = '';

            if ($type == 'card') {
                try {
                    $token = Token::create([
                        'card' => [
                            'number' => $data['number'],
                            'exp_month' => $data['exp_month'],
                            'exp_year' => $data['exp_year'],
                            'cvc' => $data['cvc'] ?? '123',
                            'name' => $data['name'],
                        ],
                    ]);
                    return $token;
                } catch(\Stripe\Error\Card $e) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $error['errorMessage'] = $err['message'];
                    return $error;
                }catch (\Stripe\Error\RateLimit $e) {
                      // Too many requests made to the API too quickly
                     $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $error['errorMessage'] = $err['message'];
                    return $error;
                } catch (\Stripe\Error\InvalidRequest $e) {
                     $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $error['errorMessage'] = $err['message'];
                    return $error;
                      // Invalid parameters were supplied to Stripe's API
                } catch (\Stripe\Error\Authentication $e) {
                     $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $error['errorMessage'] = $err['message'];
                    return $error;
                      // Authentication with Stripe's API failed
                      // (maybe you changed API keys recently)
                } catch (\Stripe\Error\ApiConnection $e) {
                     $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $error['errorMessage'] = $err['message'];
                    return $error;
                      // Network communication with Stripe failed
                } catch (\Stripe\Error\Base $e) {
                     $body = $e->getJsonBody();
                    $err  = $body['error'];
                    $error['errorMessage'] = $err['message'];
                    return $error;
                      // Display a very generic error to the user, and maybe send
                      // yourself an email
                } catch (\Exception $ex) {
                    $error = [];
                    $error['errorMessage'] = $ex->getMessage();
                    return $error;
                 }
            } else {
                $error['errorMessage'] = 'Invalid card type';
                return $error;
            }
    }

    /**
     * Get particular token detail
     * @param $tokenId
     * @return Token
     */
    public function getToken($tokenId)
    {
        $token = Token::retrieve($tokenId);

        return $token;
    }

    /**========================================
     * CHARGE
    ========================================*/
    
    /**
     * Charge customer with amount
     * @param        $customerCode
     * @param        $amount
     * @param string $currency
     * @return Charge
     */
    public function createCharge($data, $capture=true, $currency='usd')
    {
        // amount, currency, description, source, application_fee
        // $tokenData   = $this->createToken($data);
        $charge = Charge::create([
            'amount'   => $data['amount'],
            'currency' => $currency,
            'capture'  => $capture,
            'source'   => $data['card_id'],
            'customer' => $data['customer'],
            'description' => $data['description'],
            'metadata[order_id]' => $data['metadata']
            /*'source'   => $tokenData['id'],
            'metadata' => ['stripe_customer_id'=>$data['customer'], 'user_id' => 3],*/
        ]);

        return $charge;
    }

    /**
     * Charge customer with amount
     * @param        $customerCode
     * @param        $amount
     * @param string $currency
     * @return Charge
     */
    public function captureCharge($data, $currency='usd')
    {
        // amount, currency, description, source, application_fee
        // $tokenData   = $this->createToken($data);
        \Log::info($data);
        $charge = Charge::create([
            'amount'   => $data['amount']*100,
            'currency' => $currency,
            'capture'  => false,
            'source'   => $data['card_id'],
            'customer' => $data['customer'],
            'description' => $data['description'],
            'metadata[reservation_id]' => $data['metadata']
        ]);
        return $charge;
    }

    public function retriveCharge($chargeId)
    {
        $charge = Charge::retrieve($chargeId);
        $charge->capture();
        return $charge;
    }

    public function releaseCharge($chargeId)
    {
        $refund = Refund::create(['charge'=>$chargeId]);
        return $refund;
    }

    /**
     * Get particular charge details
     * @param $chargeId
     * @return Charge
     */
    public function getCharge($chargeId)
    {
        $charge = Charge::retrieve($chargeId);

        return $charge;
    }

    /**
     * Get all charge details
     * @return \Stripe\Collection
     */
    public function getAllCharges()
    {
        $limit = ['limit' => 10];
        return Charge::all($limit);
    }

    /**========================================
     * CARDS
    ========================================*/

    public function createCustomerCard($customerId, $cardData)
    {
        $tokenData = $this->createToken($cardData, 'card');
        if(!isset($tokenData['errorMessage'])) {
            $customer = Customer::retrieve($customerId);
            //echo $tokenData['card']['id'];
            //@17-08-2018 BY RG handle stripe card error
            try {
                $customer->sources->create(['source' => $tokenData['id']]);
                $customer = Customer::retrieve($customerId);
                $card = $customer->sources->retrieve($tokenData['card']['id']);
                return $card;
            }catch(\Stripe\Error\Card $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $error ['errorMesg'] = $err['message'];
                return $error;
            }catch (Exception $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $error ['errorMesg'] = $err['message'];
                return $error;
            }
        }
        else{
            $error = [];
            $error ['errorMesg'] = $tokenData['errorMessage'];
            return $error;
        }
    }

    /**
     * Get all Customer Cards
     * @param $customerId
     * @return array|\Stripe\StripeObject
     */
    public function getCustomerCards($customerId)
    {
        $customerCards = Customer::retrieve($customerId)->sources->all(['limit' => 3, 'object' => 'card']);

        return $customerCards;
    }

    public function defaultCustomerCard($customerId, $cardId)
    {
        $customer = Customer::retrieve($customerId);
        $customer->default_source = $cardId;
        $customer->save();

        return $customer;
    }

    /**
     * Delete particular Card of a Customer
     * @param $customerId
     * @param $cardId
     */
    public function deleteCustomerCard($customerId, $cardId)
    {
        $customerCard = Customer::retrieve($customerId)->sources->retrieve($cardId)->delete();

        return $customerCard;
    }


     /**
     * Get a particular customer
     * @param $charge, amount, reason, refund_application_fee
     * @return Customer
     * 19-Dec-2018 by RG
     */
    public function createRefund($data)
    {
        try {
            $refund = Refund::create($data);
            return $refund;
        } catch(\Stripe\Error\Card $e) { 
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $error['errorMessage'] = $err['message'];
            return $error;
        }catch (\Stripe\Error\RateLimit $e) { 
              // Too many requests made to the API too quickly
             $body = $e->getJsonBody();
            $err  = $body['error'];
            $error['errorMessage'] = $err['message'];
            return $error;
        } catch (\Stripe\Error\InvalidRequest $e) { 
             $body = $e->getJsonBody();
            $err  = $body['error'];
            $error['errorMessage'] = $err['message'];
            return $error;
              // Invalid parameters were supplied to Stripe's API
        } catch (\Stripe\Error\Authentication $e) {
             $body = $e->getJsonBody();
            $err  = $body['error'];
            $error['errorMessage'] = $err['message'];
            return $error;
              // Authentication with Stripe's API failed
              // (maybe you changed API keys recently)
        } catch (\Stripe\Error\ApiConnection $e) {
             $body = $e->getJsonBody();
            $err  = $body['error'];
            $error['errorMessage'] = $err['message'];
            return $error;
              // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
             $body = $e->getJsonBody();
            $err  = $body['error'];
            $error['errorMessage'] = $err['message'];
            return $error;
              // Display a very generic error to the user, and maybe send
              // yourself an email
        } catch (\Exception $ex) {
            $error = [];
            $error['errorMessage'] = $ex->getMessage();
            return $error;
        }            

    }

}