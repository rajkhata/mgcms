<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * common function has been written for used in overall application where required to use
 * Function list
 * [sendMail,sendSMS, netcore]
 */

namespace App\Helpers;

use App\Models\User;
use Carbon\Carbon;
use Modules\Reservation\Entities\Reservation;
use Modules\Reservation\Entities\ReservationStatus;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Models\MailTemplate;
use App\Models\SmsContent;
use App\Models\Restaurant;
use App\Models\City;
use App\Models\MenuSettingItem;
use App\Models\MenuItems;
use App\Models\OrderStatuses;
use Illuminate\Support\Facades\Auth;
use App\Models\CmsUserLog;
use App\Jobs\NotifySms;
use App\Models\UserOrder;
use App\Models\Language;
use Modules\MenuManagement\Entities\OrderDetailOptions;
use Modules\MenuManagement\Entities\MybagOptions;
use Modules\MenuManagement\Entities\ItemAddons;
use App\Models\UserAuth;
use Illuminate\Support\Facades\DB;
use PubNub\PNConfiguration;
use PubNub\PubNub;
use Intervention\Image\Facades\Image;
use ImageOptimizer;
use App\Models\Configuration;
use File;
class CommonFunctions {
    /*
     * sendMail is used to any mail accept bellow data
     * $amilData[receiver_email,cc_emial,bcc_emial,attachment,subject,body,AltBody]
     */

    public static $langId = 1;
    private static $_cache = array();
    public static $city_id;
    protected static $_date_time_zone;
    public static $lat1;
    public static $lon1;
    public static $lat2;
    public static $lon2;
    public static $unit;
    public static $timezoneError=[];
    
    public static function dashboardAnnouncement(){
        return config('constants.dashboard_announcement');
    }

    public static function sendMail($mailData) {

        $mail = new PHPMailer(true);
	self::setEnvConfiguration(1);
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output2
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $_ENV['MAIL_HOST'];                     // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $_ENV['MAIL_USERNAME'];             // SMTP username
            $mail->Password = $_ENV['MAIL_PASSWORD'];             // SMTP password
            $mail->SMTPSecure = $_ENV['MAIL_ENCRYPTION'];         // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $_ENV['MAIL_PORT'];                     // TCP port to connect to
            //Recipients
            $mail->setFrom($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);
            
            if(is_array($mailData['receiver_email']) && !empty($mailData['receiver_email'])){
                foreach($mailData['receiver_email'] as $key =>$sentTo){
                    if(is_array($mailData['receiver_name']) && !empty($mailData['receiver_name'])){
                        foreach($mailData['receiver_name'] as $rkey => $receivername){
                            if(!empty($receivername)){
                                $mail->addAddress($sentTo, $receivername);  
                            }else{
                                $mail->addAddress($sentTo);  
                            }
                            $mail->addReplyTo($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);
                            if (isset($mailData['cc_email']) && !empty($mailData['cc_email'])) {
                                $mail->addCC($mailData['cc_email'],$mailData['cc_name']);
                            }
                            if (isset($mailData['bcc_email']) && !empty($mailData['bcc_email'])) {
                                $mail->addBCC($mailData['bcc_email']);
                            }

                            //Attachments
                            if (isset($mailData['attachment']) && !empty($mailData['attachment'])) {
                                $mail->addAttachment($mailData['attachment']);                            // Add attachments
                                //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');                       // Optional name
                            }

                            //Content
                            $mail->isHTML(true);                                                           // Set email format to HTML
                            $mail->Subject = isset($mailData['subject']) ? $mailData['subject'] : "";
                            $mail->Body = isset($mailData['body']) ? $mailData['body'] : "";
                            $mail->AltBody = isset($mailData['AltBody']) ? $mailData['AltBody'] : "";
                            $mail->send();
                            $mail->clearAddresses();
                        }
                    }
                    
                }
                
            }else{
            
                $mail->addAddress($mailData['receiver_email'], $mailData['receiver_name']);     // Add a recipient
                //$mail->addAddress('sudhanshuk@bravvura.in');                                  // Name is optional
                $mail->addReplyTo($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);

                if (isset($mailData['cc_email']) && !empty($mailData['cc_email'])) {
                    $mail->addCC($mailData['cc_email'],$mailData['cc_name']);
                }
                if (isset($mailData['bcc_email']) && !empty($mailData['bcc_email'])) {
                    $mail->addBCC($mailData['bcc_email']);
                }

                //Attachments
                if (isset($mailData['attachment']) && !empty($mailData['attachment'])) {
                    $mail->addAttachment($mailData['attachment']);                            // Add attachments
                    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');                       // Optional name
                }

                //Content
                $mail->isHTML(true);                                                           // Set email format to HTML
                $mail->Subject = isset($mailData['subject']) ? $mailData['subject'] : "";
                $mail->Body = isset($mailData['body']) ? $mailData['body'] : "";
                $mail->AltBody = isset($mailData['AltBody']) ? $mailData['AltBody'] : "";
                $mail->send();
                $mail->clearAddresses();
                
            }
            return "Message has been sent";
        } catch (Exception $e) {
            return 'Message could not be sent. Mailer Error';
        }
    }

   public static function getRestaurant($id){
      return  Restaurant::select(['id','restaurant_name'])->where('id',$id)->first();
   }
    public static function uploadFile($image,$type,$restaurant_name,$specific_uniid=''){

        $imageRelPath = config("constants.image.rel_path.{$type}");
        $imagePath = config("constants.image.path.{$type}");

        $curr_restaurant_name=strtolower(str_replace(' ','-',$restaurant_name));
        $imagePath=$imagePath.DIRECTORY_SEPARATOR.$curr_restaurant_name;
        $imageRelPath=$imageRelPath.$curr_restaurant_name. DIRECTORY_SEPARATOR;

        (! File::exists($imagePath) ?File::makeDirectory($imagePath,0755, false,true):'');
        (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'thumb') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'thumb' ,0755, false,true):'');
        $uniqId = uniqid(mt_rand());
       $imageName = $specific_uniid. '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

        $resp= $image->move($imagePath, $imageName );
        if($resp){
             return [
                    'image' => $imageRelPath . $imageName,
                   // 'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                  //  'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                  //  'image_caption' => $imageCaptionArr[$i],
                ];

        }else{
             return [];

        }

    }

    /*
     * sendSms is used for sms accept array variable that containg below data
     * array[message,user_mob_no]
     */

    public static function sendSms($smsData) {
        //print_r($_ENV);    
	self::setEnvConfiguration(1);           
        $userMobNo = preg_replace('/\s+/', '', str_replace(" ","",$smsData['user_mob_no']));
        $SmsText = $smsData['message'];
        $clickatellConfig = [
            'cat_auth_url' => $_ENV['SMS_AUTH_URL'],
            'cat_ac_username' => $_ENV['SMS_AC_USERNAME'],
            'cat_ac_password' => $_ENV['SMS_AC_PASSWORD'],
            'cat_api_id' => $_ENV['SMS_API_ID'],
            'cat_sendmsg_url' => $_ENV['SMS_SEND_URL'],
            'cat_from' => $_ENV['SMS_FROM'],
        ];
        
        self::clickATellSms($clickatellConfig, $userMobNo, $SmsText);
        return "Message sent";
    }

    private static function clickATellSms($clickatellConfig, $userMobNo = false, $SmsText = false) {
        $smsresponse = false;

        if ($userMobNo && $SmsText) {
            $SmsText = urlencode($SmsText);
            //auth call
            $url_auth = $clickatellConfig['cat_auth_url'] . "?user=" . $clickatellConfig['cat_ac_username'] . "&password=" . $clickatellConfig['cat_ac_password'] . "&api_id=" . $clickatellConfig['cat_api_id'];
            $ret_auth = self::sendRequest($url_auth, 'get'); // do auth call
            $sess = explode(":", $ret_auth); // explode Auth response. 
           
            if ($sess[0] == 1) {                
                $sess_id = trim($sess[0]); // remove any whitespace
                $url_send = $clickatellConfig['cat_sendmsg_url'] . "?session_id=" . $sess_id . "&to=" . $userMobNo . "&text=" . $SmsText . "&mo=1&from=" . $clickatellConfig['cat_from'];
                $ret_send = self::sendRequest($url_send, 'get'); // do sendmsg call               
                $send = explode(":", $ret_send);  
                
                if ($send[0]) {
                    $smsresponse = true;
                }
            }
           
        }
        return $smsresponse;
    }

    public static function netcoreEvent($data) {
        $postDataArr = array(
            "EMAIL" => $data['email'],
            "FIRST_NAME" => isset($data['first_name']) ? $data['first_name'] : "",
            "MOBILE" => isset($data['mobile']) ? (string) $data['mobile'] : "",
            "IS_REGISTER" => isset($data['is_register']) ? (string) $data['is_register'] : "",
            "LAST_NAME" => isset($data['last_name']) ? (string) $data['last_name'] : "",
            "REGISTRATION_DATE" => isset($data['registration_date']) ? $data['registration_date'] : "",
            "HOST_URL" => isset($data['host_url']) ? $data['host_url'] : ""
        );


        $postData = json_encode($postDataArr);

        $url = $_ENV['NETCORE_URL'] . "/" . $_ENV['NETCORE_API_VERSION_APIV2'] . "?" . "type=contact&activity=add&listid=1&apikey=" . $_ENV['NETCORE_API_KEY'];
        $url .= "&data=" . urlencode($postData);
        return self::sendRequest($url, 'post');
    }

    private static function sendRequest($url, $method = false, $postData = array(), $htmlBuildQuery = false) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == "post") {
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($htmlBuildQuery && !empty($postData)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
            } elseif (!empty($postData)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            }
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        return true;
    }

    public static function mailTemplate($mailFor, $mailKeywords = array()) {
        if ($mailFor) {
            $templateContent = MailTemplate::where(['template_name' => $mailFor, 'language_id' => self::$langId])->get()->toArray();
            $header = self::getHeaderLayout()[0]['content'];
            $content = $templateContent[0]['content'];
            $footer = self::getFooterLayout()[0]['content'];
            $mailContent = $header . $content . $footer;

            $pattern = '[%s]';
            foreach ($mailKeywords as $key => $val) {
                $keywordMap[sprintf($pattern, $key)] = $val;
            }
//            print_R($keywordMap);
//            die;
            $mailTemplate = strtr($mailContent, $keywordMap);
           
            return $mailTemplate;
        }
        return false;
    }

    private static function getHeaderLayout() {
        return MailTemplate::where(['template_name' => 'header_layout', 'language_id' => self::$langId])->get()->toArray();
    }

    private static function getFooterLayout() {
        return MailTemplate::where(['template_name' => 'footer_layout', 'language_id' => self::$langId])->get()->toArray();
    }

    public static function getSMSContent($smsFor) {
        return SmsContent::where(['sms_name' => $smsFor, 'language_id' => self::$langId])->get()->toArray();
    }

    /*
     * format the restarant polygon data into array
     */

    public static function formatDeliveryGeo($deliveryGeo = false) {
        $result = [];
        if (!empty($deliveryGeo)) {
            $polygonData = str_replace('POLYGON((', '', $deliveryGeo);
            $polygonData = str_replace('))', '', $polygonData);
            $polygonData = trim($polygonData);
            $polygonData = explode(',', $polygonData);

            foreach ($polygonData as $key => $polyvalue) {
                $polLatLng = explode(" ", trim($polyvalue));
                $result[] = $polLatLng[0] . " " . $polLatLng[1];
            }
        }
        return $result;
    }

    public static function getRelativeCityDateTime(array $options = array(), $dateTime = 'now', $format = '') {
       
        $cityTimeZone = static::getTimeZoneMapped($options);        
        if(isset($cityTimeZone['error']) && $cityTimeZone['error']){            
            self::$timezoneError = $cityTimeZone;
            return true;  
        }
       
        if (strtolower($dateTime) == 'now' && $cityTimeZone) {
            $dateTime = new \DateTime();
            return $dateTime->setTimezone(new \DateTimeZone($cityTimeZone));
        }

        $dateTimeObject = \DateTime::createFromFormat($format, $dateTime, static::getDateTimeZone());
        return $dateTimeObject->setTimezone(new \DateTimeZone($cityTimeZone));
    }

    /**
     * Get time zone mapping according to restaurant_id, state_code, state_timezone
     *
     * @param array $options            
     * @throws \Exception
     * @return string TimeZoneText
     */
    public static function getTimeZoneMapped(array $options = array()) {
        $city = new City();
        if (isset($options['state_timezone']) && $options['state_timezone']) {
            return $options['state_timezone'];
        } elseif (isset($options['state_code']) && $options['state_code']) {
            if (isset(static::$_cache['state_code_timezone']) && static::$_cache['state_code_timezone']) {
                return static::$_cache['state_code_timezone'];
            }

            // Get time zoen with state code

            $timeZoneResult = $city::Where(['state_code' => $options['state_code']])->get()->toArray();

            if ($timeZoneResult) {
                static::$_cache['state_code_timezone'] = $timeZoneResult['time_zone'];
                return static::$_cache['state_code_timezone'];
            }
        } elseif (isset($options['restaurant_id']) && $options['restaurant_id']) {
            if (isset(static::$_cache['restaurant_id_timezone']) && static::$_cache['restaurant_id_timezone']) {
                return static::$_cache['restaurant_id_timezone'];
            }
            $resturantModel = new Restaurant();
            $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')
                ->where(['restaurants.id' => $options['restaurant_id']])->get()->toArray();

            if (isset($timeZoneResult[0]['time_zone']) && !empty($timeZoneResult[0]['time_zone'])) {
                static::$city_id = $timeZoneResult[0]['city_id'];
                static::$_cache['restaurant_id_timezone'] = $timeZoneResult[0]['time_zone'];
                return static::$_cache['restaurant_id_timezone'];
            }else{
                return array('message'=>'Invalid Options for DateTime Please provide restaurant_id or state_code or state_timezone','error'=>true);
            }
        }
        return array('message'=>'Invalid Options for DateTime Please provide restaurant_id or state_code or state_timezone','error'=>true);
    }

    public static function getDateTimeZone() {
        if (!static::$_date_time_zone) {
            static::setDateTimeZone();
        }
        return static::$_date_time_zone;
    }

    public static function setDateTimeZone($dateTimeZoneText = null) {
        $defaultDateTimeZoneText = date_default_timezone_get();
        if (!$dateTimeZoneText || !is_string($dateTimeZoneText)) {
            $dateTimeZoneText = $defaultDateTimeZoneText;
        }
        try {
            $dateTimeZone = new \DateTimeZone($dateTimeZoneText);
        } catch (\Exception $ex) {
            $dateTimeZone = new \DateTimeZone($defaultDateTimeZoneText);
        }
        static::$_date_time_zone = $dateTimeZone;
    }

    public static function distanceBetweenTwoPointOnEarth() {
        $miles = "";
        $theta = self::$lon1 - self::$lon2;
        $dist = sin(deg2rad(self::$lat1)) * sin(deg2rad(self::$lat2)) + cos(deg2rad(self::$lat1)) * cos(deg2rad(self::$lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper(self::$unit);
        return number_format((float)$miles, 2, '.', '');


    }

    public static function getAddonsAndModifiersForMailer($addon_modifier){
        $data='';

        if(isset($addon_modifier['modifier_data']) && count($addon_modifier['modifier_data'])){

            $modifier_data_records=$addon_modifier['modifier_data'];
            // $data.='<div class="food_order_item padding-left-25"> Modifiers :-</div>';
            $addonDataArrary = array();
            $i = 0;
            foreach($modifier_data_records as $modifier_data){
                $groupname='';
                $price=0;
                $quantity=1;

                if(isset($modifier_data['quantity']) && !empty($modifier_data['quantity'])){
                    $quantity=$modifier_data['quantity'];
                }
                if(isset($modifier_data['price']) && !empty($modifier_data['price'])){
                    $price=$modifier_data['price'];
                }
                if(isset($modifier_data['group_prompt']) && !empty($modifier_data['group_prompt'])){
                    $groupname = $modifier_data['group_prompt'] ;
                }

                $addonDataArrary[$groupname][$i]['name'] = $modifier_data['modifier_name'];
                $addonDataArrary[$groupname][$i]['quantity'] = $quantity;
                $addonDataArrary[$groupname][$i]['price'] = $price;
                $i++;

                // $data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$modifier_data['modifier_name']." <b>".$groupname."</b> </div> <div class=\"col-md-2 col-xs-2 text-center\">".$quantity."</div> <div class=\"col-md-2 col-xs-2 text-right mb0\"> ".'<span>$</span>'. @number_format($price,2) ." </div> <div class=\"col-md-2 col-xs-2 text-right\"> ".'<span>$</span>'. number_format($price * $quantity,2) ." </div> <div class=\"col-md-2 col-xs-2 text-right\"></div> </div>";


                //$data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$modifier_data['modifier_name']. $groupname . "- <span style='float: right;'> ".$quantity." * ".number_format($price,2)."</span> </div> </div>";

            }
            foreach($addonDataArrary as $key => $value){
                $abc = '';
                $pArr = array();
                $abc = '<b>'.$key.': </b>';
                foreach($value as $innerArr){
                    if($innerArr['quantity'] > 1){
                        $pArr[] = $innerArr['name'].' X'.$innerArr['quantity'];
                    }else{
                        $pArr[] = $innerArr['name'];
                    }
                }
                $data .= '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" >'.$abc.implode(', ', $pArr).'</td></tr>';
            }

        }elseif(isset($addon_modifier['addons_data']) && count($addon_modifier['addons_data'])){

            $addons_data_records=$addon_modifier['addons_data'];
            //  $data.='<div class="food_order_item padding-left-25"> Addons :-</div>';
            $addonDataArrary = array();
            $i = 0;
            foreach($addons_data_records as $addons_data){
                $groupname='';
                if(isset($addons_data['addongroup']) && !empty($addons_data['addongroup'])){
                    $groupname = $addons_data['addongroup']['prompt'] ;
                }

                $addonDataArrary[$groupname][$i]['name'] = $addons_data['option_name'];
                $addonDataArrary[$groupname][$i]['quantity'] = $addons_data['quantity'];
                $addonDataArrary[$groupname][$i]['price'] = $addons_data['price'];
                $i++;

                // $data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$addons_data['option_name'] ." <b>".$groupname."</b> </div> <div class=\"col-md-2 col-xs-2 text-center\">".$addons_data['quantity']."</div> <div class=\"col-md-2 col-xs-2 text-right mb0\">".'<span>$</span>'. @number_format($addons_data['price'],2) ." </div> <div class=\"col-md-2 col-xs-2 text-right\">".'<span>$</span>'. number_format($addons_data['price'] * $addons_data['quantity'],2)." </div> <div class=\"col-md-2 col-xs-2 text-right\"></div> </div>";

                //$data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$addons_data['option_name']." ".$groupname." - <span style='float: right;'>".$addons_data['quantity']." * ".number_format($addons_data['price'],2)."</span> </div> </div>";
            }
            foreach($addonDataArrary as $key => $value){
                $abc = '';
                $pArr = array();
                $abc = '<b>'.$key.': </b>';
                foreach($value as $innerArr){
                    if($innerArr['quantity'] > 1){
                        $pArr[] = $innerArr['name'].' X'.$innerArr['quantity'];
                    }else{
                        $pArr[] = $innerArr['name'];
                    }
                }
                $data .= '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" >'.$abc.implode(', ', $pArr).'</td></tr>';
            }
        }

        return $data;
    }
    /* Function to Convert my bag json data in array format */
    public static function getAddonsAndModifiers($order_data,$type='order'){

        $is_byp=$order_data->is_byp;
        $is_pot=$order_data->is_pot;

        $data=[];
        if(!empty($order_data->menu_json) ){
            if(!is_array($order_data->menu_json))
                $jsonData = json_decode($order_data->menu_json, true);
            else $jsonData = $order_data->menu_json;
            if(isset($jsonData['modifier_items'])){
                $data['modifier_data']= $jsonData['modifier_items'];
                //modifer category,modifer option group ,modifier option need to discuss
            }
        }
        if($type=='order'){
            $addon_options= OrderDetailOptions::where('user_order_detail_id',$order_data->id)->get();

        }else{

            $addon_options= MybagOptions::where('bag_id',$order_data->id)->get();

        }

        if( $addon_options){
            $option_data=[];
            foreach ($addon_options as $addon_menu) {
                $addongroup='';
                $addonInfo = ItemAddons::where('id', $addon_menu->option_id)->with('addongroup')->first();
                if ($addonInfo) {
                    $addongroup= $addonInfo->addongroup;
                }
                $option_data[]=[
                    'option_name'=>$addon_menu->option_name,
                    'option_id'=>$addon_menu->option_id,
                    'price'=>$addon_menu->price,
                    'quantity'=>$addon_menu->quantity,
                    'user_order_detail_id'=>$order_data->id,
                    'total_amount'=>$addon_menu->total_amount,
                    'addongroup'=>$addongroup,
                    'addongroup_id'=>$addongroup->id,

                ];


            }

            $data['addons_data']=$option_data;
        }

        return  $data;
    }

    /* Function to Convert my bag json data in array format */

    public static function changeMyBagJsonData($isBYP, $jsonData){
        //echo $isBYP; die;
        $addons_data = [];
        if($isBYP==0) {
            if(!is_null($jsonData)) {
                $bagData = $jsonData;
                //print_r($bagData); die;
                /*************************************************/
                if(!empty($bagData)) {
                    foreach ($bagData as $val) {
                        if (isset($val['items'])) {
                            $item_id = $val['id'];
                            $addons_label = $val['label'];
                            foreach ($val['items'] as $data) {
                                if ($data['default_selected'] == 0) {
                                    continue;
                                }
                                $label_addons = [];
                                $addons_option = '';
                                $menu_addons_option_id = $data['id'];
                                $MenuSettingItem = MenuSettingItem::select(['menu_setting_category_id', 'name', 'price'])->where('id', $menu_addons_option_id)->first();
                                $menu_addons_id = @$MenuSettingItem->menu_setting_category_id;
                                $addons_name = $data['label'];
                                $price = $data['price'];
                                $addons_option = '';
                                if (!is_null($data['quantity'])) {
                                    if (!empty($data['quantity'])) {
                                        //print_r([$menu_addons_option_id, $menu_addons_id]);
                                        foreach ($data['quantity'] as $qty_items) {
                                            if (isset($qty_items['is_checked'])) {
                                                $label_addons [] = $qty_items['label'];
                                            }
                                        }
                                    }
                                    if (!empty($data['sides'])) {
                                        foreach ($data['sides'] as $qty_sides) {
                                            if (isset($qty_sides['is_checked'])) {
                                                $label_addons [] = $qty_sides['label'];
                                            }
                                        }
                                    }
                                    $addons_option = implode(',', $label_addons);
                                }

                                $addons_data [] = [
                                    'menu_addons_id' => $menu_addons_id,
                                    'menu_addons_option_id' => $menu_addons_option_id,
                                    'addons_label' => $addons_label,
                                    'addons_name' => $addons_name,
                                    'addons_option' => $addons_option,
                                    'price' => $price,
                                    'quantity' => 1,
                                    'selection_type' => 0, //Not sure of the values yet,
                                    'was_free' => 0,   //Not sure of the values yet,
                                    'priority' => 1,   //Not sure of the values yet,
                                ];

                            }
                        }
                    }
                }
                /***************************************************************/
            }

        }
        else if($isBYP==1){
            //print_r($jsonData);
            $addoncData = [];
            $label = $addonVal = "";
            foreach($jsonData as $key=>$value){
                if($key=='label'){
                  continue;
                }
                $label = $key;
                if(is_array($value)){
                    $arrData = [];
                    $str_head='';
                    foreach($value as $data) {
                        $arr = [];
                        $addons_option = "";
                        if (is_array($data)) {
                            $mod_val='';
                            $headkey='';
                            foreach($data as $k=>$val){
                                if($k==$label) {
                                    $label = $label;
                                    $headkey=$val;
                                }
                                else{
                                    $arr[]= ucfirst($val);
                                }

                                $mod_val='';
                            }
                            $addons_option = implode(',', $arr);
                            $str_head.=ucwords($headkey).'- ['.ucwords($addons_option).']'."\n";
                            //$addonVal = $addons_option;
                        }
                        else{
                            $addonVal = $data;
                        }
                    }
                    if(!empty($str_head)){
                        $addonVal=nl2br($str_head);
                    }
                }
                else {
                    $addonVal =  $value;
                }

                $addons_data[ucwords($label)] = $addonVal;
            }
            //print_r($addons_data);
        }
        return $addons_data;
    }

    /*End function */

    /**
    * @param Menu_item_id
    * @author Rahul Gupta
    * @date 27-07-2018
    * Static is defined inorder to call from ClassName only
    */

    public static function get_menu_item_images($menu_item_id) {
        $images = array();
        $menuItem = MenuItems::where('id', $menu_item_id)->select('id', 'name','description','images')->first();
        if($menuItem && $menuItem->images) {
            $images = json_decode($menuItem->images, true);
        }
        // $images = array(
        //     'desktop_web_images' => array(
        //         '0' => array(
        //             'web_image' => '/images/menu/3_heart-pizza_18268214325b361de436063.png',
        //             'web_thumb_med' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'web_thumb_sml' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'web_image_caption' => 'Pizza'
        //         ),
        //         '1' => array(
        //             'web_image' => '/images/menu/4_VodkaRigatoni_680_3315085655afc4e6191059.png',
        //             'web_thumb_med' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.png',
        //             'web_thumb_sml' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.pngg',
        //             'web_image_caption' => 'Pasta'
        //         ),
        //     ),
        //     'mobile_web_images' => array(
        //         '0' => array(
        //             'mobile_web_image' => '/images/menu/3_heart-pizza_18268214325b361de436063.png',
        //             'mobile_web_thumb_med' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'mobile_web_thumb_sml' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'mobile_web_image_caption' => 'Pizza'
        //         ),
        //         '1' => array(
        //             'mobile_web_image' => '/images/menu/4_VodkaRigatoni_680_3315085655afc4e6191059.png',
        //             'mobile_web_thumb_med' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.png',
        //             'mobile_web_thumb_sml' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.pngg',
        //             'mobile_web_image_caption' => 'Pasta'
        //         ),
        //     ),
        //     'mobile_app_images' => array(
        //         '0' => array(
        //             'image' => '/images/menu/3_heart-pizza_18268214325b361de436063.png',
        //             'thumb_image_med' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'thumb_image_sml' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'image_caption' => 'Pizza'
        //         ),
        //         '1' => array(
        //             'image' => '/images/menu/4_VodkaRigatoni_680_3315085655afc4e6191059.png',
        //             'thumb_image_med' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.png',
        //             'thumb_image_sml' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.pngg',
        //             'image_caption' => 'Pasta'
        //         ),
        //     ),
        // );       
        return $images;
    }

    /**
     * @param Menu_item_id
     * @author Rahul Gupta
     * @date 31-07-2018
     * Static is defined inorder to call from ClassName only
     */

    public static function getLocalTimeBased($options = array()) {
        $return_datatime =  $options['datetime'];
        $resturantModel = new Restaurant();
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $options['restaurant_id']])->first();
        if($timeZoneResult && $timeZoneResult->time_zone && $options['datetime']) {
            $datetime = new \DateTime($options['datetime']);
            $la_time = new \DateTimeZone($timeZoneResult->time_zone);
            $datetime->setTimezone($la_time);
            $return_datatime =  $datetime->format('Y-m-d H:i:s');
        }
        return $return_datatime;
    }

    /**
    * @param Parent rest ID
    * @author Rahul Gupta
    * @date 08-08-2018
    * It will send list of all child restaurants
    */

    public static function getRestaurantDetails($fields = array()) {
        $user = Auth::user();
        $restList = array();

        // Manager id for Single Outlet of the parent Restautant  
        if($user->role == "manager") {
           $restList = array($user->restaurant_id);
        } else if($user->role == "admin") {
            // Admin is of parent Restaurant ID
            $resturantModel = new Restaurant();
            $restList = Restaurant::Join('restaurants AS r2', 'restaurants.id', 'r2.parent_restaurant_id')->select($fields)->where(['restaurants.id' => $user->restaurant_id])->get();

            if($restList) {
                $restList = $restList->toArray();
                $restList = array_pluck($restList, 'id');

            }         
        } else {
            // super admin, can see all restaturant and all menu
            $resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select($fields)->where('r2.parent_restaurant_id', '<>', 0)->get();  
            if($restList) {  
                $restList = $restList->toArray();
                $restList = array_pluck($restList, 'id');

            }         
        } 
       
        return $restList;
    }

    /**
    * @param Parent rest ID
    * @author Rahul Gupta
    * @date 08-08-2018
    * It will fetch all the restaurant based on the 
    */

    public static function getRestaurantGroup() {
        $user = Auth::user();
        $restList = array();
        $groupRestData = [];
        // Manager id for Single Outlet of the parent Restautant  
        if($user->role == "manager") { 
            $resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select('id', 'restaurant_name', 'parent_restaurant_id')->where('r2.id', '=', $user->restaurant_id)->first();  
            if($restList) {  
                $restList = $restList->toArray();
                $restList = array($restList['id'], $restList['parent_restaurant_id']);                
            }  

        } else if($user->role == "admin") {
            // Admin is of parent Restaurant ID
            $resturantModel = new Restaurant();
            $restList = Restaurant::Join('restaurants AS r2', 'restaurants.id', 'r2.parent_restaurant_id')->select('restaurants.id', 'r2.id', 'restaurants.restaurant_name')->where(['restaurants.id' => $user->restaurant_id])->get();  
            if($restList) {  
                $restList = $restList->toArray();
                $restList = array_pluck($restList, 'id');
                array_push($restList, $user->restaurant_id);
            }      
        } else {
            // super admin, can see all restaturant and all menu
            $resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select('id', 'restaurant_name')->where('status', 1)->get();  
            if($restList) {  
                $restList = $restList->toArray();
                $restList = array_pluck($restList, 'id');
            }         
        }  
        if($restList) {
            $allRestaurants = Restaurant::select('id', 'restaurant_name', 'parent_restaurant_id')
                    ->where('status', 1)
                    ->whereIn('id', $restList)
                    ->get();                    
            
            foreach($allRestaurants as $res) {
                if(isset($groupRestData[$res['parent_restaurant_id']])) {
                    $groupRestData[$res['parent_restaurant_id']]['branches'][] = [
                        'id'    => $res['id'],
                        'restaurant_name' => $res['restaurant_name'],
                    ];
                } else {
                    $groupRestData[$res['id']] = [
                        'id'    => $res['id'],
                        'restaurant_name' => $res['restaurant_name'],
                        'branches' => []
                    ];
                }
            }
        }       
        return $groupRestData;
    }

     /**
    * @param Parent rest ID
    * @author Rahul Gupta
    * @date 10-08-2018
    * It will fetch all the parent restaurant based on the Role 
    */

    public static function getParentRestaurant() {
        $user = Auth::user();
        $restList = array();
        $groupRestData = [];
        // Manager id for Single Outlet of the parent Restautant  
        if($user->role == "manager") { 
            $resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select('id', 'restaurant_name', 'parent_restaurant_id')->where('r2.id', '=', $user->restaurant_id)->first();  
            
            if($restList) {  
                $restList = $restList->toArray(); 
                $restList = array($restList['parent_restaurant_id']); 
            } 

        } else if($user->role == "admin") {
            // Admin is of parent Restaurant ID
            $restList = array($user->restaurant_id);    
        } else {
            // super admin, can see all restaturant and all menu
            $resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select('id', 'restaurant_name')->where('status', 1)->where('parent_restaurant_id', 0)->get();  
            if($restList) {  
                $restList = $restList->toArray();
                $restList = array_pluck($restList, 'id');
            }         
        }  
        if($restList) {
            $groupRestData = Restaurant::select('id', 'restaurant_name', 'parent_restaurant_id')
                    ->where('status', 1)
                    ->whereIn('id', $restList)
                    ->get(); 
           
        } 
        return $groupRestData;
    }

    public static function getRestaurantDetailsById($id)
    {
        //$user = Auth::user();
        $restList = array();
        $groupRestData = [];
        $parentName = "";
        // Manager id for Single Outlet of the parent Restautant
        if(isset($id) && is_numeric($id)) {
            //$resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select('id', 'restaurant_name', 'parent_restaurant_id', 'email', 'address','street','zipcode','kpt','restaurant_image_name')->where('r2.id', '=', $id)->first();
            //print_r($restList); die;
            if ($restList) {
                $restList = $restList->toArray();
                if(isset($restList['parent_restaurant_id'])) {
                    $parentData = Restaurant::select('restaurant_name','custom_from', 'support_from')
                        ->where('status', 1)
                        ->where('id', $restList['parent_restaurant_id'])
                        ->first();
                    $parentName = $parentData->restaurant_name;
                    $custom_from = $parentData->custom_from;
                    $support_from = $parentData->support_from;
                }
                $restList['restaurantParentName'] = $parentName;
                $restList['custom_from'] = $custom_from;
                $restList['support_from'] = $support_from;
                //print_r($restList); die;
                return $restList;
                //$restList = array($restList['id'], $restList['parent_restaurant_id']);
            }

        }
    }

    public static function getTime_Slot($deliveryTime){

        $r = array("00","01","02","03","04","05","06","07","08","09",10,11,12,13,14,15,16,17,18,19,20,21,22,23);
        $m = array("00",15,30,45);
        $timeSlotData = [];
        foreach ($r as $hour)
        {
            foreach($m as $min)
            {

                $start_time = strtotime($hour.':'.$min);
                $time = $hour.':'.$min;

                if( !empty($deliveryTime) && $deliveryTime!="null" && $time>$deliveryTime){
                    $timeSlotData[$time]= date("H:i", $start_time);
                }else{
                    $timeSlotData[$time]= $time;

                }


            }
        }
        return $timeSlotData;
    }

    public static function getDateSlot(){

        $cdate = strtotime(date('Y-m-d'));
        $cdate2 = strtotime("+7 day", $cdate);

        for ($date = strtotime(date('Y-m-d')); $date < $cdate2; $date = strtotime("+1 day", $date)) {
            $d = date("Y-m-d", $date);
            $result [$d] = date("l d M, Y", $date);
        }
        return $result;
    }


        /*
    * send SMS of based on 
    * RG 18-10-2018
    * sms_module = order/ reservation_enquiry, event_enquiry, contact, catering
    * sms_to = manager / customer
    * restaurant_id = optional 
    * order_id = optional 
    * action = accept / reject / placed / confirmed / ready 
    * mobile_no = array() : Optional if have then Get it from this key otherwise from Order's phone column
    * US No. 6465957373 Neela Mohan No.
    * $options = array(
        'order_id', 'restaurant_id', 'sms_to' ,'sms_module', 'mobile_no'
    );
    */   
    
    public static function getAndSendSms($options = array()) {
        $sms_template = '';
        if(isset($options['mobile_no']) && $options['mobile_no']) {
            $mobile_no = $options['mobile_no'];
        }

        switch ($options['sms_module']) {
            case 'order':
                /************************* ORDER ************ Module */
                $orderInfo =  UserOrder::select('user_orders.id', 'order_type', 'user_orders.status', 'delivery_date', 'delivery_time', 'user_orders.created_at', 'restaurant_id', 'is_asap_order', 'is_guest', 'restaurants.restaurant_name', 'user_orders.phone', 'user_id', 'product_type', 'payment_receipt', 'user_orders.updated_at')
                    ->leftjoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
                    ->where('user_orders.id', $options['order_id'])
                    ->first();

                if($orderInfo) {
                    if($orderInfo->phone) {
                        $mobile_no = array($orderInfo->phone);
                    }
                    #$restaurant_name = $orderInfo->restaurant_name;
                    $restaurant_name = htmlentities($orderInfo->restaurant_name, ENT_QUOTES, "UTF-8");
                    // check the Order type : It is either  Food or Merchandise
                    #$restOrderLocalTimestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderInfo->restaurant_id, 'datetime' => $orderInfo->created_at)));
                    if($orderInfo->delivery_date) {
                        $order_requested_date = date('l, F d, Y', strtotime($orderInfo->delivery_date));
                    }
                    if($orderInfo->delivery_time) {
                        $order_requested_time = date('h:i A', strtotime($orderInfo->delivery_time));
                    }
                    if($orderInfo->product_type == 'food_item') {
                        // check Order is delivery or Carryout
                        if(strtolower($orderInfo->order_type) == 'carryout') {
                            # check if Order is ASAP
                            if($orderInfo->is_asap_order) {
                                switch ($options['action']) {
                                    case 'placed':
                                        $sms_template = 7;
                                        break;
                                    case 'confirmed':
                                        $sms_template = 9;
                                        break;
                                    case 'rejected':
                                        $sms_template = 12;
                                        break;

                                    default:
                                        # code...
                                        break;
                                }

                            }else {
                                switch ($options['action']) {
                                    case 'placed':
                                        $sms_template = 8;
                                        break;
                                    case 'confirmed':
                                        $sms_template = 10;
                                        break;
                                    case 'rejected':
                                        $sms_template = 13;
                                        break;

                                    default:
                                        # code...
                                        break;
                                }
                            }
                            # Ready State
                            if($options['action'] == 'ready') {
                                $sms_template = 11;
                            }
                            
                        }elseif(strtolower($orderInfo->order_type) == 'delivery') {
                            # check if Order is ASAP
                            if($orderInfo->is_asap_order) {
                                switch ($options['action']) {
                                    case 'placed':
                                        $sms_template = 1;
                                        break;
                                    case 'confirmed':
                                        $sms_template = 3;
                                        break;
                                    case 'rejected':
                                        $sms_template = 5;
                                        break;
                                    case 'completed':
                                        $sms_template = 27;
                                        break;    
                                    default:
                                        # code...
                                        break;
                                }

                            }else {
                                switch ($options['action']) {
                                    case 'placed':
                                        $sms_template = 2;
                                        break;
                                    case 'confirmed':
                                        $sms_template = 4;
                                        break;
                                    case 'rejected':
                                        $sms_template = 6;
                                        break;
                                    case 'completed':
                                        $sms_template = 27;
                                        break;
                                    default:
                                        # code...
                                        break;
                                }
                            }

                        }                        
                    }elseif($orderInfo->product_type == 'product') {
                        // merchandise Product
                        switch ($options['action']) {
                            case 'placed':
                                $sms_template = 16;
                                break;
                            case 'confirmed':
                                $sms_template = 17;
                                break;
                            case 'rejected':
                                # Restaurant manager Rejected
                                $sms_template =18;
                                break;
                            case 'Canceled':
                                #customer cancelled by their own
                                $sms_template =19;
                                break;

                            default:
                                # code...
                                break;
                        }
                    }elseif($orderInfo->product_type == 'gift_card') {
                        // Gift Type
                        // merchandise Product
                        $parent_restaurant_info = CommonFunctions::getRestaurantDetailsById($orderInfo->restaurant_id);
                        $parent_restaurant_name = isset($parent_restaurant_info['restaurantParentName']) ? htmlentities($parent_restaurant_info['restaurantParentName'], ENT_QUOTES, "UTF-8") : $restaurant_name;

                        switch ($options['action']) {
                            case 'placed':
                                $sms_template = 22;
                                break;
                            #Buyer will get confirmation Gift Card Sent to Recipient
                            case 'confirmed':
                                $sms_template = 23;
                                $recipient_name = 'Guest';
                                if(isset($options['recipient_name']) && $options['recipient_name']) {
                                    $recipient_name = $options['recipient_name'];
                                }
                                break;

                            default:
                                # code...
                                break;
                        }
                    }
                    # Refund Order
                    if($options['action'] == 'refund') {
                        $amount_refunded = number_format($options['amount_refunded'],2);
                        $sms_template = 25;
                        $receipt_id = $orderInfo->payment_receipt;

                        #$timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderInfo->restaurant_id, 'datetime' => $orderInfo->updated_at)));
                        $order_refund_date = date('M d, Y h:i A', strtotime($orderInfo->delivery_date.' '.$orderInfo->delivery_time.':00'));
                        
                        if($orderInfo->product_type == 'gift_card') {
                            $parent_restaurant_info = CommonFunctions::getRestaurantDetailsById($orderInfo->restaurant_id);
                            $restaurant_name = isset($parent_restaurant_info['restaurantParentName']) ? htmlentities($parent_restaurant_info['restaurantParentName'], ENT_QUOTES, "UTF-8") : $restaurant_name;
                            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderInfo->restaurant_id, 'datetime' => $orderInfo->created_at)));
                            $order_refund_date = date('M d, Y h:i A', $timestamp2);

                        }
                    }
                    if($options['action'] == 'timeupdate') {
                        $sms_template = 26;
                        $receipt_id = $orderInfo->payment_receipt;
                        $order_time = $options['time'];
                    }
                }
                break;
            case 'reservation_enquiry':
                /************************* Reservation Enquiry ************ Module */
                $event = StaticBlockResponse::find($options['order_id']);
                if($event) {
                    $restInfo = CommonFunctions::getRestaurantDetails($event->restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name'));
                    #$restaurant_name =  $restInfo['restaurant_name'];
                    $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");
                    $response = json_decode($event->response);
                    if(isset($response->no_of_guest)) {
                        $guest = $response->no_of_guest;
                    }elseif(isset($response->no_of_people)) {
                        $guest = $response->no_of_people;
                    }
                    $date_of_event = date('M d, Y', strtotime($response->date_of_event));
                    $time_of_event = date('h:i A', strtotime($response->time_of_event));

                    switch ($options['action']) {
                        case 'placed':
                            $sms_template = 14;
                            break;

                        default:
                            # code...
                            break;
                    }
                }
                break;
            case 'event_enquiry':
                /************************* Event Enquiry ************ Module */
                $event = StaticBlockResponse::find($options['order_id']);
                if($event) {
                    $restInfo = CommonFunctions::getRestaurantDetails($event->restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name'));
                    #$restaurant_name =  $restInfo['restaurant_name'];
                    $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");
                    $response = json_decode($event->response);
                    if(isset($response->no_of_guest)) {
                        $guest = $response->no_of_guest;
                    }elseif(isset($response->no_of_people)) {
                        $guest = $response->no_of_people;
                    }
                    $date_of_event = date('M d, Y', strtotime($response->date_of_event));
                    $time_of_event = date('h:i A', strtotime($response->time_of_event));

                    switch ($options['action']) {
                        case 'placed':
                            $type_of_event = 'event';
                            $sms_template = 21;
                            if(isset($response->type_of_event)) {
                                $type_of_event = $response->type_of_event;
                            }
                            break;

                        default:
                            # code...
                            break;
                    }
                }
                break;
            case 'catering':
                /************************* Catering Enquiry ************ Module */
                $event = StaticBlockResponse::find($options['order_id']);
                if($event) {
                    $restInfo = CommonFunctions::getRestaurantDetails($event->restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name'));
                    #$restaurant_name =  $restInfo['restaurant_name'];
                    $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");
                    $response = json_decode($event->response);
                    #$date_of_event = date('l, F d, Y', strtotime($response->date_of_occasion));
                    #$date_of_event = date('M d, Y', strtotime($response->date_of_occasion));
                    if(isset($response->date_of_occasion)) {
                        $date_of_event = date('M d, Y', strtotime($response->date_of_occasion));
                    }else if(isset($response->date_of_event)) {
                        $date_of_event = date('M d, Y', strtotime($response->date_of_event));
                    }
                    switch ($options['action']) {
                        case 'placed':
                            $room_requested = '';
                            if(isset($response->room)) {
                                $room_requested = $response->room;
                            }
                            $sms_template = 20;
                            break;

                        default:
                            # code...
                            break;
                    }
                }
            break;
            case 'forgot_password':
                /************************* Catering Enquiry ************ Module */
                $event = UserAuth::find($options['order_id']);
                if($event) {
                    $restInfo = CommonFunctions::getRestaurantDetails($event->parent_restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name'));
                    #$restaurant_name =  $restInfo['restaurant_name'];
                    $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");


                    switch ($options['action']) {
                        case 'forgot':
                            $sms_template = 24;
                            $otp = $event->otp;
                            break;

                        default:
                            # code...
                            break;
                    }
                }
            break;
            default:
                # code...
                break;
        }

        $smsText = '';
        switch ($sms_template) {
            case '1':
                #Order ASAP Delivery
                $smsText = " Hey, it's $restaurant_name, we got your delivery order! We will update you once your order is confirmed.";
                break;
            case '2':
                # Future Delivery (same day or not same day)
                $smsText = "Hey, it's $restaurant_name, we got your delivery pre-order for $order_requested_date at  $order_requested_time! See you then!";
                break;
            case '3':
                #ASAP Delivery Confirmed
                $smsText = "$restaurant_name is preparing your order.";
                break;
            case '4':
                #Future Delivery Confirmed (same day and not same day)
                $smsText = "$restaurant_name Martini Genie received your order for $order_requested_date at $order_requested_time.";
                break;
            case '5':
                #ASAP delivery rejected by restaurant
                $smsText = "You canceled your Martini Genie order.";
                break;
            case '6':
                #Delivery Pre-order Rejected By Restaurant
                $smsText = "You canceled your Martini Genie order.";
                break;
             case '27':
                #Delivery Completed By Restaurant
                $smsText = "Your Martini Genie  order has been delivered. Cheers!";
                break;
                
            case '7':
                #Order ASAP Takeout
                $smsText = "Hey, it's $restaurant_name, we got your takeout order! We will update you once your order is confirmed.";
                break;
            case '8':
                #Ordered Future Takeout (same day or not same day)
                $smsText = "Hey, it's $restaurant_name, we got your takeout pre-order for $order_requested_date at $order_requested_time! See you then!";
                break;
            case '9':
                #ASAP Takeout Confirmed
                $smsText = "$restaurant_name Staff: We've confirmed your order. We'll have your food ready as soon as possible.";
                break;
            case '10':
                #Future Takeout Confirmed (same day and not same day)
                $smsText = "$restaurant_name Staff: We've confirmed your takeout pre-order for $order_requested_date at $order_requested_time. See you then!";
                break;
            case '11':
                #Takeout Ready
                $smsText = "$restaurant_name Staff: Your takeout order is ready! Check your inbox for more details.";
                break;
            case '12':
                #ASAP takeout rejected by restaurant
                $smsText = "$restaurant_name Staff: We're sorry, but we could not accept your takeout order. Check your inbox for more details.";
                break;
            case '13':
                #Takeout Pre-order Rejected By Restaurant
                $smsText = "$restaurant_name Staff: We're sorry, but we could not accept your takeout pre-order for $order_requested_date at $order_requested_time. Check your inbox for more details.";

                break;
            case '14':
                #Placed a Reservations
                $smsText = "Hey, it's $restaurant_name! We received your reservation request for $guest people on $date_of_event at $time_of_event.";

                break;
            case '15':
                $smsText = "Don't want to receive SMS notifications? Reply “Unsubscribe” and we'll keep communications to email only.";
                break;
            case '16':
                #Placed a Merchandise Order
                $smsText = "Hey, it's $restaurant_name, we got your merchandise order! We will update you once your order is confirmed.";
                break;
            case '17':
                #Merchandise Order Confirmed
                $smsText = "$restaurant_name Staff: We've confirmed your merchandise order. We'll have your gift delivered as soon as possible.";
                break;
            case '18':
                #Merchandise Order Rejected by Restaurant
                $smsText = "$restaurant_name Staff: We're sorry, but we could not accept your merchandise order. Check your inbox for more details.";
                break;
            case '19':
                #Merchandise Order Canceled by User
                $smsText = "$restaurant_name Staff: We've successfully undone your order for you. Bummer.";
                break;
            case '20':
                #Placed a Catering Request
                #$smsText = "Hey, it's $restaurant_name, we got your catering request for $occassion on $date_of_event.";
               $smsText =  "Hey, it's $restaurant_name, we got your catering request for $date_of_event! We will let you know once your request is confirmed.";

                break;
            case '21':
                #Placed an Events Request
                #$smsText =  "Hey, it's $restaurant_name, we got your booking request for a private event on $date_of_event at $time_of_event for $guest people! We will book the same once your request is confirmed.";
                $smsText = "Hey, it's $restaurant_name, we got your event request for $date_of_event at $time_of_event for $guest people! We will let you know once your request is confirmed.";
                break;
            case '22':
                #Placed a Gift Card Order
                $smsText =  "Hey, it's $parent_restaurant_name, we got your gift card order! Check your inbox for details.";
                break;
            case '23':
                #Gift Card Sent to Recipient
                $smsText =  "$parent_restaurant_name Staff: We've sent your gift card to $recipient_name. You're a great gift giver, we're sure they're going to love it.";
                break;
             case '24':
                #Gift Card Sent to Recipient
                $smsText =  "$otp is your OTP for $restaurant_name. Use it to sign in and update your password.";
                break;
            case '25':
                #Ordered Future Takeout (same day or not same day)
                #$smsText = "Hey, it's $restaurant_name, we have refunded $".$amount_refunded." for your Order ID: $receipt_id on $order_refund_date!";
                $smsText = "$restaurant_name Staff: We've issued you a refund for your experience with us on $order_refund_date. Check your inbox for more details.";
                         
            break;
            case '26':
                #Ordered Future Takeout (same day or not same day)
                #$smsText = "Hey, it's $restaurant_name, we have refunded $".$amount_refunded." for your Order ID: $receipt_id on $order_refund_date!";
                $smsText = "$restaurant_name Staff: We've have changed your order time to $order_time for your Order ID: $receipt_id. Check your inbox for more details.";
                         
            break;
            default:
                # code...
                break;
        }
        if($smsText && $mobile_no) {
            $resp = NotifySms::dispatch($mobile_no['0'] ,$smsText);
            #CommonFunctions::sendSms(array('message' => $smsText, 'user_mob_no' => $mobile_no['0']));
        }
    }

    public static function changeUserCategory($user_id)
    {
        $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
        $user_category = User::where('restaurant_id', $parent_restaurant_id)->where('id', $user_id)->value('category');
        if ($user_category!=config('reservation.guest_category.VIP')) {
            $reservation_count = Reservation::where('user_id', $user_id)->whereNotIn('status_id', [ReservationStatus::getCancelledStatusId(), ReservationStatus::getNoShowStatusId()])->count();
            $order_count = UserOrder::where('user_id', $user_id)->whereNotIn('status', ['cancelled', 'rejected', 'pending'])->count();
            $total_transaction_count = $reservation_count+$order_count;
            if ($total_transaction_count > 1 && $user_category==config('reservation.guest_category.New')) {
                User::where('id', $user_id)->update(['category' => config('reservation.guest_category.Repeat')]);
            }elseif ($total_transaction_count <= 1 && $user_category==config('reservation.guest_category.Repeat')){
                User::where('id', $user_id)->update(['category' => config('reservation.guest_category.New')]);
            }
        }
    }

    # Common Email Templates for all

    public static function getProductList($order_id){
        $item_list_html = '';
        $orderInfo =  UserOrder::select('user_orders.id', 'order_type', 'user_orders.status', 'delivery_date', 'delivery_time', 'user_orders.created_at', 'restaurant_id', 'is_asap_order', 'is_guest', 'user_orders.phone', 'user_id', 'product_type','is_byp', 'order_amount', 'tax', 'tip_amount', 'tip_percent', 'user_comments', 'total_amount','delivery_charge')
            ->where('user_orders.id', $order_id)
            ->with([
                'details' =>function ($query) {
                    $query->with('giftdetails');
                }
            ])
            ->first();

        if($orderInfo) {
            $item_list_html = '<table class="callout" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="callout-inner summry secondary" style="Margin:0;background:#f3f3f3;border:0;color:#0a0a0a;font-family: Montserrat;font-size:16px;font-weight:400;line-height:30px;margin:25px 0;padding: 0 25px 25px;text-align:left;width:100%;">';
            $orderInfo = $orderInfo->toArray();
            // Restaurant currency symbol
            $restData = Restaurant::find($orderInfo['restaurant_id'])->select('currency_symbol','currency_code')->first();
            if($restData) {
                $curSymbol = $restData->currency_symbol;
                $curCode = $restData->currency_code;
            } else {
                $curSymbol = config('constants.currency');
                $curCode = config('constants.currency_code');
            }
            //echo "<pre>";print_r($orderInfo); die;
            switch ($orderInfo['product_type']) {
                case 'gift_card':
                    #FOR GIFT CARD ITEM It has 2 varidant with Physical card and without physical cards
                    foreach ($orderInfo['details'] as  $items) {
                        $item_list_html .=
                            '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;border-bottom:1px #eaeaea solid">
                            <tbody>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <td class="small-9 large-9 columns first" height="20" colspan="3"></td>
                                </tr>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <td class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height: 1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width: 40%;"><b>' . $items['item'] . '</b></td>
                                    <td valign="middle" class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height: 1.5;margin:0 auto;padding:0;text-align:center;width: 10%;"><b>' . $items['quantity'] . '</b></td>
                                    <td valign="middle" class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height: 1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;text-align: right;width: 25%;"><b><span>'.$curSymbol.'</span>' . number_format($items['unit_price'], 2) . '</b></td>
                                    <td valign="middle" class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height: 1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align: right;width: 25%;"><b><span>'.$curSymbol.'</span>' . number_format($items['total_item_amt'], 2) . '</b></td>
                                </tr>
                                <tr style="padding:0;text-align:left;vertical-align:top;color: #757575;font-family: Helvetica,Arial,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.5;/* font-style: italic; */">
                                    <td colspan="4" class="small-9 large-9 columns first" height="30" style="
                                        /* width: 40%; */
                                        ">
                                        <table style="
                                            width: 100%;
                                            ">
                                            <tbody>';
                        #promotion Applied DIV
                        $giftotherInfo = json_decode($items['item_other_info'], true);
                        $qty = '';
                        if($items['quantity']>1){
                            $qty = $items['quantity'].'X';
                        }

                        if ($items['giftdetails'] && isset($items['giftdetails']['0']) && $items['giftdetails']['0']['amount']) {
                            foreach ($items['giftdetails'] as $gval){
                                $bonusAmt = "";
                                if ($gval['card_type'] == "bonus" && $gval['bonus_for'] == "recipient" && $gval['recipient'] == "single") {
                                    $bonusAmt = '<b style="color: #000;font-family: Helvetica,Arial,sans-serif;">Offer:</b> Your Recipient Has Earned <span style="color: gray;">'.$qty.$curSymbol.'</span><span style="color: gray;">' .$gval['amount'] . ' Bonus Card </span>';
                                }
                                else if ($gval['card_type'] == "bonus" && $gval['bonus_for'] == "recipient" && $gval['recipient'] == "multiple") {
                                    $bonusAmt = '<b style="color: #000;font-family: Helvetica,Arial,sans-serif;">Offer:</b> Your Recipients Have Earned <span style="color: gray;">'.$curSymbol.'</span><span style="color: gray;">' .$gval['amount'] . ' Bonus Card</span>';
                                }
                                else if ($gval['card_type'] == "bonus" && $gval['bonus_for'] == "myself") {
                                    $bonusAmt = '<b style="color: #000;font-family: Helvetica,Arial,sans-serif;">Offer:</b> You\'ve Earned <span style="color: gray;">'.$curSymbol.'</span><span style="color: gray;">' .$gval['amount'] . ' Bonus Card</span>';
                                }
                            }
                            if($bonusAmt!="") {
                                $item_list_html .= '<tr>
                                                        <td colspan="2" style="
                                                            font-size: 11px;
                                                            font-style: italic;
                                                            font-weight: 600;
                                                            font-family: Helvetica,Arial,sans-serif;
                                                            ">' . $bonusAmt . '</td>
                                                    </tr>';
                            }

                        }
                        $item_list_html .=  '<tr style="
                                                    height: 5px;
                                                    ">
                                                    <td colspan="2"></td>
                                                </tr>
                                                <tr>
                                                
                                                    <td colspan="2" style="
                                                        word-break: break-all;
                                                        ">
                                                        <table style="width: 100%;">
                                                        <tbody>
                                                        <tr>
                                                        <td colspan="2">
                                                            <b style="font-weight:600;color:#000;font-family: Helvetica,Arial,sans-serif;">To&nbsp;</b></td>
                                                            </tr>';
                        if($giftotherInfo['gift_type'] == 'egiftcard') {
                            foreach ($items['giftdetails'] as $key => $details) {
                                if(($details) && ($details['card_type']=="normal")) {
                                    $item_list_html .= '<tr><td style="width: 18px;"></td>
<td><p  style="margin:0;font-family: Helvetica,Arial,sans-serif;">'.$details['name'].'<br> <a href="mailto:'.$details['email'].'" target="_blank" style="color: gray !important;">('.$details['email'].')</a></p></td>
			 </tr>';
                                    if($details['recipient']=="single"){
                                        break;
                                    }


                                }
                            }
                        }else {
                            foreach ($items['giftdetails'] as $key => $details) {
                                if(($details) && ($details['card_type']=="normal")) {
                                    $item_list_html .= '<tr><td style="width: 18px;"></td>
<td><p style="margin: 0;font-family: Helvetica,Arial,sans-serif;">'.$details['name'].'('.$details['email'].')</div><div>'.$details['address'].'</p></td>
			 </tr>';
                                }
                            }
                        }
                        $item_list_html .=  '</tbody></table></td>
                                                </tr>';
                        if($giftotherInfo['gift_wrapping_message']!='null') {
                            $item_list_html .= '<tr style="
                                                    height: 5px;
                                                    ">
                                                    <td colspan="2"></td>
                                                </tr><tr>
                                                    <td colspan="2">
                                                    <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                    <td colspan="2">
                                                        <b style="font-weight:600;color:#000">Message&nbsp;</b></td>
                                                        </tr>
                                                        <tr><td style="width: 18px;"></td><td>
                                                            <p style="margin: 0;"> ' . $giftotherInfo['gift_wrapping_message'] . '</p>
                                                            </td></tr></tbody></table>
                                                    </td>
                                                </tr>';
                        }
                        $item_list_html .='</tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <td class="small-9 large-9 columns first" height="30" colspan="5"></td>
                                </tr>
                            </tbody>
                        </table>';
                    }
                    break;

                case 'product':
                    break;
                case 'food_item':
                    #BYP
                    #BYP = 0 and with Customisation
                    #BYP = 0 without customisation
                    break;
                default:
                    # code...
                    break;
            }
            if(Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
            } else {
                $curSymbol = config('constants.currency');
            }
            if($orderInfo["order_amount"] > 0) {
                $item_list_html .= '<br>
                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th height="40" valign="middle" class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%;">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;">
                                            <b>Subtotal</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:right;">
                                            <b><span>'.$curSymbol.'</span>'.number_format($orderInfo["order_amount"], 2).'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>';
            }
            if($orderInfo["tax"] > 0) {
                $item_list_html .=
                    '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;font-size: 15px;font-weight: 400;">
                                           
                                            <b>Tax</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:right;">
                                            <b> <span>'.$curSymbol.'</span>'.number_format($orderInfo["tax"],2) .'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>';
            }
            if($orderInfo["tip_amount"] > 0) {
                $item_list_html .=
                    '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-5 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;">
                                           
                                            <b>Tip Amount</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-7 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:150px">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:2;margin:0;padding:0;padding-bottom:0;text-align:right;border:1px #eaeaea solid;padding-right:5px;">
                                            <b>'.$orderInfo['tip_percent'].'%'.number_format($orderInfo['tip_amount'], 2).'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>
                <br>';
            }
            if($orderInfo["delivery_charge"] > 0) {
                $item_list_html .=
                    '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;font-size: 15px;font-weight: 400;">
                                           
                                            <b>Delivery Charge</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:right;">
                                            <b> <span>'.$curSymbol.'</span>'.number_format($orderInfo["delivery_charge"],2) .'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>';
            }
            $refunded_amount = DB::table('refund_histories')->where('order_id', $orderInfo["id"])->sum('amount_refunded');  
            if($refunded_amount > 0) {
                $item_list_html .=
                    '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;font-size: 15px;font-weight: 400;">
                                           
                                            <b>Refund</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:right;">
                                            <b> <span>'.$curSymbol.'</span>'.number_format($refunded_amount,2) .'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>';
            }
            $item_list_html .=  '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%;vertical-align:middle">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 16px;font-weight:400;line-height:2;margin:0;padding:0;padding-bottom:0;text-align:left;padding-top:3px;">
                                            
                                            <b>Total</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-6 large-3 columns last" valign="bottom" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th style="Margin:0;color: #000;font-family:Helvetica,Arial,sans-serif;font-size: 16px;font-weight:700;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align: right;">
                                            
                                            <b><span>'.$curSymbol.'</span>'.number_format($orderInfo['total_amount'], 2).'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
            </table>
            </th></tr></tbody></table>';
        }
        return $item_list_html;
    }

    public static function getAvailableLanguages() {
        $languageObj = Language::where('status', 1)
               ->orderBy('language_name', 'asc')
               ->get();
        if (count($languageObj)) {
            return $languageObj;
        } else {
            return [];
        }
    }

    /* End here */
    public static function getRestaurantCurrentLocalTime($restaurant_id, $format='now') {
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $restaurant_id])->first();
        if($timeZoneResult && $timeZoneResult->time_zone) {
            $timezone = $timeZoneResult->time_zone;
        }else{
            $timezone = 'UTC';
        }
        if($format=='now'){
            $localDateTime = Carbon::now($timezone);
        }else{
            $localDateTime = Carbon::now($timezone)->format($format);
        }
        return $localDateTime;
    }

    public static function convertRestaurantDateTimeInUTC($restaurant_id, $userSuppliedDateTime)
    {
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $restaurant_id])->first();
        if($timeZoneResult && $timeZoneResult->time_zone) {
            $timezone = $timeZoneResult->time_zone;
        }else{
            $timezone = 'UTC';
        }
        return Carbon::parse($userSuppliedDateTime, $timezone)->setTimezone('UTC');
    }

    public static function convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $userSuppliedDateTime)
    {
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $restaurant_id])->first();
        if($timeZoneResult && $timeZoneResult->time_zone) {
            $timezone = $timeZoneResult->time_zone;
        }else{
            $timezone = 'UTC';
        }
        return Carbon::parse($userSuppliedDateTime, 'UTC')->setTimezone($timezone);
    }

    public static function getRestaurantTimeZone($restaurant_id) {
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $restaurant_id])->first();
        if($timeZoneResult && $timeZoneResult->time_zone) {
            $timezone = $timeZoneResult->time_zone;
        }else{
            $timezone = 'UTC';
        }
        return $timezone;
    }


    /*Rahul Gupta 08-01-2019 */


    public static function getOldAddons($label, $isbyp, $result) {
        if($isbyp ==1 && $result){
            foreach($result as $key=>$data){
                 $label .= '<div class="row item_addons" style="font-size:12px;line-height:18px;">      
                    <div class="col-sm-4">
                    <div class="text-capitalize "><b>'.
                        str_replace('-', ' ',
                    str_replace('_', ' ', $key)).'</b></div>

                    <div>&nbsp;&nbsp;'.$data.'</div></div>
                    <div class="col-md-2 col-xs-2 text-center"></div>
                </div>';
            }
        }
        if($isbyp==0 && $result) {
            $is_label_present = array();
            $total_addons = count($result);
            foreach ($result as $key => $data) {
                if(isset($data['addons_label']) && !in_array($data['addons_label'], $is_label_present)){
                    $is_label_present[] = $data['addons_label'];
                    $label .= "</br><b style='color: #0a0a0a;'>".$data['addons_label'].'</b></br>';
                }
                if($data['addons_name']!=""){
                    $label .= $data['addons_name'];
                    if(($key + 1) != $total_addons) {
                        $label .= ', ';
                    }
                }
            }
        }
        return $label;
    }


     /**
     * @param Parent Rest ID/ Location ID, fetch_parent_rest, Fields
     * @author Rahul Gupta
     * @date 30-01-2019
     * It will send restaurant Details based on the fetch_parent_rest = true / false
     */
    public static function getRestaurantInfo($rest_id, $fetch_parent_rest = false, $fields = array()) {
        $restInfo = $restDetails = array();
        $resturantModel = new Restaurant();
        if($fetch_parent_rest) {
            // Get the details of parent Rest
            $restDetails = Restaurant::Join('restaurants AS r2', 'restaurants.id', 'r2.parent_restaurant_id')->select($fields)->where(['restaurants.id' => $rest_id])->first();
        }else {
            // Get detail of child restaurants
            $restDetails = Restaurant::select($fields)->where(['restaurants.id' => $rest_id])->first();
        }
        if($restDetails) {
            $restInfo = $restDetails->toArray();
        }
        return $restInfo;
    }

    public function dashboard_pause_service() {
        $msg = '';
        if (Auth::check() && !empty(\Auth::user()->restaurant_id)) {

            $rest_id =  \Auth::user()->restaurant_id;

            $restInfo = CommonFunctions::getRestaurantInfo($rest_id, false, array('id', 'current_version', 'delivery', 'takeout', 'reservation', 'reservation_type', 'delivery', 'takeout', 'reservation', 'reservation_type', 'is_merchandise_allowed', 'is_catering_allowed', 'is_contact_allowed', 'is_event_allowed', 'is_career_allowed','merchandise_takeout_allowed','merchandise_delivery_allowed','food_ordering_delivery_allowed','food_ordering_takeout_allowed','pause_service_message','is_reservation_allowed','is_food_order_allowed','is_gift_card_allowed', 'parent_restaurant_id'));
            $err_msg = array();
            if( $restInfo['parent_restaurant_id'] != 0) {
                $services = 0;
                if($restInfo['is_food_order_allowed']) {
                    if($restInfo['food_ordering_delivery_allowed']) {
                       $services += 1;
                    }
                    if($restInfo['food_ordering_takeout_allowed']) {
                       $services += 1;
                    }
                    if($restInfo['food_ordering_delivery_allowed'] == 1 && $restInfo['delivery'] == 0) {
                        $err_msg['0'] = 'new delivery orders';
                    }
                    if($restInfo['food_ordering_takeout_allowed'] == 1 && $restInfo['takeout'] == 0) {
                        $err_msg['0'] = 'new takeout orders';
                    }
                    if($restInfo['food_ordering_delivery_allowed'] == 1 && $restInfo['food_ordering_takeout_allowed'] == 1  && $restInfo['delivery'] == 0 &&  $restInfo['takeout'] == 0) {
                        $err_msg['0'] = 'online orders';
                    }
                }
                if($restInfo['is_reservation_allowed'] && $restInfo['reservation_type'] == 'full') {
                   $services += 1;
                    if($restInfo['reservation'] == 0) {
                        $err_msg[] = 'new reservations';
                    }
                }

                if($services > 0 && count($err_msg) > 0 ) {
                    if(count($err_msg) == 2) {
                        $key = $err_msg['0'].' / '.$err_msg['1'];
                    }else {
                        $key = $err_msg['0'];
                    }
                    $msg = "Your website is currently not accepting $key. ";
                }
            }
        }
        return $msg ;
    }
    public static function getRestaurantWithBranches($city_id=NULL,$defaultid=NULL) { //NEW ACL CAN BE IMPLEMENTED HERE
        $parentRestaurants = Restaurant::select('id', 'restaurant_name', 'parent_restaurant_id')
            ->where('status', 1);
        if($city_id!=NULL){
            $parentRestaurants =$parentRestaurants->where('city_id',$city_id);
        }
        $parentRestaurants =$parentRestaurants->where('parent_restaurant_id',0)->get();
        $select='';
        if(count($parentRestaurants)){
            // $select='<select name="restaurant_groups" id="restaurant_groups" class="form-control ">';
            foreach($parentRestaurants as $rest){
                $selected='';
                if($defaultid!=NULL && $defaultid==$rest->id){
                    $selected='selected="selected"';
                }
                $select.='<option value="'.$rest->id.'" class="optionGroup" '.$selected.'>'.$rest->restaurant_name.'</option>';
                $locationRestaurants = Restaurant::select('id', 'restaurant_name', 'parent_restaurant_id')
                    ->where('status', 1)
                    ->where('parent_restaurant_id',$rest->id)
                    ->get();
                if(count($locationRestaurants)){
                    foreach($locationRestaurants as $locrest){
                        $selected='';
                        if($defaultid!=NULL && $defaultid==$locrest->id){
                            $selected='selected="selected"';
                        }
                        $select.='<option value="'.$locrest->id.'" class="optionChild" '.$selected.'>-----'.$locrest->restaurant_name.'</option>';
                    }
                }
            }
            //$select.='</select>';
        }
        return $select;
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public static function getRestaurantParentInfo($id){
        return  Restaurant::select(['id','restaurant_name','parent_restaurant_id','source_url'])->where('id',$id)->first();
    }

    public static function getCustomerOrders($order) { 
        $order = $order->toArray();
        if($order['user_id']) {
            $total_user_odr_counts =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $order['restaurant_id'])->where('user_orders.user_id', $order['user_id'])->orderBy('user_orders.id', 'DESC')->whereNotIn('status', ['pending'])->count();
        }else {
           $total_user_odr_counts =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $order['restaurant_id'])->where('user_orders.email', $order['email'])->orderBy('user_orders.id', 'DESC')->whereNotIn('status', ['pending'])->count();
        }
        
        return $total_user_odr_counts;
    }
    
    public static function pubnubPushNotification($data){
        $publishKey = $_ENV['DASHBOARD_PUBNUB_PUB'];
        $subscriberKey = $_ENV['DASHBOARD_PUBNUB_SUB'];
        $pnConfiguration = new PNConfiguration();
        $pnConfiguration->setSubscribeKey($subscriberKey);
        $pnConfiguration->setPublishKey($publishKey);
        $pnConfiguration->setSecure(false);

        $pubnub = new PubNub($pnConfiguration);
        $user_current_notification['aps'] = array('alert' => $data['msg'], 'badge' => 1);
        $user_current_notification['restaurant_id'] = $data['restaurant_id'];
        $user_current_notification['order_id']=$data['order_id'];
        $user_current_notification['type'] = $data['type'];
        try{
            ##### Push notification ####   

            $result = $pubnub->publish()->channel($data['channel'])->message(
                [
                    $user_current_notification,
                    "msg"=>$data['msg'],
                    "type"=>$data['type'],
                    "channel"=>$data['channel'],
                    "curDate"=>$data['curDate'],
                    'is_friend'=>$data['is_friend'],
                    'restaurant_id'=>$data['restaurant_id'],
                    'username'=>$data['username'],
                    'order_id'=>$data['order_id'],
                    'order_status'=>$data['order_status']
                ]
            )->shouldStore(true)->usePost(true)->sync();

            return $result;
            ##### End of Push Notification #####

            ##### Start Log #####
//            $log = $pubnub->getLogger()->pushHandler(new ErrorLogHandler());
//            print_r($log);
//            print_r("### End of log ###");
            ##### End of log #####

            #### Start Subscription ####   
//            $subscribeCallback = new PubnubSubscribeCallback(); 
//            $pubnub->addListener($subscribeCallback);
//            $subscribe = $pubnub->subscribe()->channels($data['channel'])->execute();            
//            print_r($subscribe);
//            print_r("### End of Subscription ###");
            #### End of subscription ####

            // some time later
//            $pubnub->removeListener($subscribeCallback);

        }catch(PubNubException $error){
            return false;
        }
    }

    public static function  getAllChargesForMailer($mailKeywords,$userOrder,$curSymbol){

        $data=self::getOrderCharges($userOrder,$curSymbol);
        $mailhtml='';
        if(count($data)){
            foreach ($data as $val){
                $mailhtml.='<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;display:'.$val['display'].';"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left"><p class="light-text bold" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left">'.$val['name'].'</p></th></tr></table></th><th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left"><p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"> '.$val['value'].'</p></th></tr></table></th></tr></tbody></table>';
            }
        }

        $mailKeywords['ALL_CHARGES']=$mailhtml;
        return $mailKeywords;

    }
    public static function  getOrderCharges($userOrder,$curSymbol){
        $data=[];
        if($userOrder->flat_discount==0.00){

        } else{
            $data[]=['name'=>'Discount','value'=>'-<span>'.$curSymbol.'</span>'.number_format($userOrder->flat_discount, 2),'display'=>'table'];
        }

        if($userOrder->promocode_discount==0.00){

        } else{
            $data[]=['name'=>'Coupon Discount','value'=>'-<span>'.$curSymbol.'</span>'.number_format($userOrder->promocode_discount, 2),'display'=>'table'];
        }

        if($userOrder->additional_charge==0.00){

        } else{
            $data[]=['name'=>$userOrder->additional_charge_name,'value'=>'<span>'.$curSymbol.'</span>'.number_format($userOrder->additional_charge, 2),'display'=>'table'];
        }

        if($userOrder->order_type=="delivery") {
            if ($userOrder->delivery_charge == 0.00) {
                $data[]=['name'=>'Delivery Charge','value'=>'Free','display'=>'table'];

            } else {
                $data[]=['name'=>'Delivery Charge','value'=>'<span>' . $curSymbol . '</span>' . number_format($userOrder->delivery_charge, 2),'display'=>'table'];
            }
        }

        if($userOrder->service_tax==0.00){

        } else{
            $data[]=['name'=>'Service Tax','value'=>'<span>'.$curSymbol.'</span>'.number_format($userOrder->service_tax, 2),'display'=>'table'];

        }
        if($userOrder->tax==0.00){

        } else{
            $data[]=['name'=>'Tax','value'=>'<span>'.$curSymbol.'</span>'.number_format($userOrder->tax, 2),'display'=>'table'];

        }

        if($userOrder->tip_amount==0.00){

        } else{
            $data[]=['name'=>'Tip Amount','value'=>'<span>'.$curSymbol.'</span>'.number_format($userOrder->tip_amount, 2),'display'=>'table'];

        }


        return $data;
    }

    public static function fileUpload($image, $imageName, $imagePath, $imageRelPath, $thumb = false)
    {
        $imageArr = [];


        $ext             = $image->getClientOriginalExtension();

        if($thumb) {
            $medImageName    = $imageName . '_med' . '@2x.' . $ext;
            $smlImageName    = $imageName . '_sml' . '@1x.' . $ext;
        }
        $imageName=$imageName.$image->getClientOriginalName();
        $imageName       = $imageName . '@3x.' . $ext;
        $image           = Image::make($image->getRealPath());

        $width = $image->width();
        $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
        //ImageOptimizer::optimize($pathToImage);
        if($thumb) {
            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

            $imageArr = [
                'image'     => $imageRelPath . $imageName,
                'thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                'thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
            ];

        } else {
            $imageArr['image'] = $imageRelPath . $imageName;
        }

        return $imageArr;

    }
    public static function setEnvConfiguration($locationId=1){
	$configuration = Configuration::select(["config_key","config_value"])->where("restaurant_id",$locationId)->get()->toArray();
	if(is_array($configuration)){
		foreach($configuration as $row){
			$key = $row['config_key'];
			$_ENV[$key]=$row['config_value'];
		}
	}
   }
    public static function getRestaurantGroupAll() {
        $user = Auth::user();
        $restList = array();
        $groupRestData = [];
        // Manager id for Single Outlet of the parent Restautant
        if($user->role == "manager") {
            $resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select('id', 'restaurant_name', 'parent_restaurant_id')->where('r2.id', '=', $user->restaurant_id)->first();
            if($restList) {
                $restList = $restList->toArray();
                $restList = array($restList['id'], $restList['parent_restaurant_id']);
            }

        } else if($user->role == "admin") {
            // Admin is of parent Restaurant ID
            $resturantModel = new Restaurant();
            $restList = Restaurant::Join('restaurants AS r2', 'restaurants.id', 'r2.parent_restaurant_id')->select('restaurants.id', 'r2.id', 'restaurants.restaurant_name')->where(['restaurants.id' => $user->restaurant_id])->get();
            if($restList) {
                $restList = $restList->toArray();
                $restList = array_pluck($restList, 'id');
                array_push($restList, $user->restaurant_id);
            }
        } else {
            // super admin, can see all restaturant and all menu
            $resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select('id', 'restaurant_name')->get();
            if($restList) {
                $restList = $restList->toArray();
                $restList = array_pluck($restList, 'id');
            }
        }
        if($restList) {
            $allRestaurants = Restaurant::select('id', 'restaurant_name', 'parent_restaurant_id')
                ->whereIn('id', $restList)
                ->get();

            foreach($allRestaurants as $res) {
                if(isset($groupRestData[$res['parent_restaurant_id']])) {
                    $groupRestData[$res['parent_restaurant_id']]['branches'][] = [
                        'id'    => $res['id'],
                        'restaurant_name' => $res['restaurant_name'],
                    ];
                } else {
                    $groupRestData[$res['id']] = [
                        'id'    => $res['id'],
                        'restaurant_name' => $res['restaurant_name'],
                        'branches' => []
                    ];
                }
            }
        }
        return $groupRestData;
    }

    public static function ImageAsItisUpload($image, $imageName, $imagePath, $imageRelPath, $thumb = false)
    {
        $imageArr = [];
        $ext             = $image->getClientOriginalExtension();

        $smlImageName= $medImageName= $imageName       = $imageName . '@3x.' . $ext;
        $image->move($imagePath, $imageName);

        if($thumb) {

            $imageArr = [
                'image'     => $imageRelPath . $imageName,
                'thumb_med'     => $imageRelPath . $imageName,
                'thumb_sml'     => $imageRelPath . $imageName,

            ];

        } else {
            $imageArr['image'] = $imageRelPath . $imageName;
        }

        return $imageArr;

    }

public static function number_format_short( $n ) {
        $n_format = null;
        $suffix = null;
        if ($n > 0 && $n < 1000) {
            // 1 - 999
            $n_format = floor($n); $suffix = '';
        } else if ($n >= 1000 && $n < 1000000) {

            $n_format = floor($n / 1000); $suffix = 'K+';
        } else if ($n >= 1000000 && $n < 1000000000) {

            $n_format = floor($n / 1000000); $suffix = 'M+'; }
        else if ($n >= 1000000000 && $n < 1000000000000) {

            // 1b-999b
            $n_format = floor($n / 1000000000); $suffix = 'B+'; }
        else if ($n >= 1000000000000) {
            // 1t+
            $n_format = floor($n / 1000000000000); $suffix = 'T+';
        } return !empty($n_format . $suffix) ? $n_format . $suffix : 0;
    }
    public static function getLanguageInfo($code){
     return $reservations = DB::table('languages')
            ->select(DB::raw("*"))
            ->where(['code' => $code])
            ->first();
    //DB::raw("count(id) as total_reservations")
    }

    public static function imageUploader($axis,$image,$type='restaurant',$restaurant_name,$sectionname='',$specific_uniid='', $thumb = false,$as_it_is=false){

        $imageRelPath = config("constants.image.rel_path.{$type}");
        $imagePath = config("constants.image.path.{$type}");
        $imagesize=config('deal.images_size');


        $curr_restaurant_name=strtolower(str_replace(' ','-',$restaurant_name));
        $imagePath=$imagePath.DIRECTORY_SEPARATOR.$curr_restaurant_name;
        $imageRelPath=$imageRelPath.$curr_restaurant_name. DIRECTORY_SEPARATOR;

        (! File::exists($imagePath) ?File::makeDirectory($imagePath,0777, false,true):'');

        if($sectionname!=''){
            $imagePath=$imagePath.DIRECTORY_SEPARATOR.$sectionname;
            $imageRelPath=$imageRelPath.$sectionname.DIRECTORY_SEPARATOR;
            (! File::exists($imagePath) ?File::makeDirectory($imagePath,0777, false,true):'');
        }

        (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'image') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'image' ,0777, false,true):'');
        (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'small') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'small' ,0777, false,true):'');
        (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'thumbnail') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'thumbnail' ,0777, false,true):'');

        $uniqId = uniqid(mt_rand());
        $imageName = $specific_uniid. '_' . str_replace(' ', '_', pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME))
            . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

        if($as_it_is){

            $resp= $image->move($imagePath, $imageName );
            $medImageName    = $imageName;
            $smlImageName    = $imageName;
        }else{

            $image           = Image::make($image->getRealPath());

            $medImageName    = 'med_'.$imageName;
            $smlImageName    = 'sm_'.$imageName;
            if($axis){
                $axis=explode(',',$axis);
                $x1=$axis[0];
                $y1=$axis[1];
                $w=$axis[2];
                $h=$axis[3];
                //  $image->crop($w, $h, $x1, $y1);
            }

            $image->resize($imagesize['image']['width'], $imagesize['image']['height'], function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath  . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $imageName);

            //$width = $image->width();

            $image->resize($imagesize['small']['width'], $imagesize['small']['height'], function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'small' . DIRECTORY_SEPARATOR . $medImageName);

            $image->resize($imagesize['thumbnail']['width'], $imagesize['thumbnail']['height'], function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . $smlImageName);

        }
        if($thumb){
            return [
                'image' => $imageRelPath . 'image' . DIRECTORY_SEPARATOR .  $imageName,
                'small' => $imageRelPath . 'small' . DIRECTORY_SEPARATOR . $medImageName,
                'thumbnail' => $imageRelPath . 'thumbnail' . DIRECTORY_SEPARATOR . $smlImageName,
            ];

        }else{
            return [
                'image' => $imageRelPath . $imageName,
                'small' => $imageRelPath . $imageName,
                'thumbnail' => $imageRelPath . $imageName,
            ];

        }

    }
public static function user_order_statuslog($id)
    {

        $userData = OrderStatuses::select('order_statuses.created_at','order_statuses.status','order_statuses.user_id','cms_users.name')->where('order_id',$id)->whereNull('order_statuses.comments')->leftJoin('cms_users', 'cms_users.id', '=', 'order_statuses.user_id')->orderBy('order_statuses.id','DESC')->get();
        return  $userInfo = $userData->toArray();
    }
    
public static function user_order_replylog($id)
    {

        $orderReplyData = OrderStatuses::select('order_statuses.created_at','order_statuses.status','order_statuses.user_id','cms_users.name','order_statuses.comments')->where('order_id',$id)->whereNotNull('order_statuses.comments')->leftJoin('cms_users', 'cms_users.id', '=', 'order_statuses.user_id')->orderBy('order_statuses.id','DESC')->get();
        if($orderReplyData){
            return  $orderReplyData->toArray();
        }else{
            return false;
        }
    }
    public static function callStuartsApi($pickupAddress,$dropAddress){
	/*$environment = \Stuart\Infrastructure\Environment::SANDBOX;
	$api_client_id = '9850da35f3ed171188e535502bf2b9ecff0b9809eeefcde8ed0f549bfe66e7af'; // can be found here: https://admin-sandbox.stuart.com/client/api
	$api_client_secret = '9c5997ef47652a7386bd01a373ee911bcb36060e6bd4b34a98cfbd2bbdf26319'; // can be found here: https://admin-sandbox.stuart.com/client/api
	$authenticator = new \Stuart\Infrastructure\Authenticator($environment, $api_client_id, $api_client_secret);
	$httpClient = new \Stuart\Infrastructure\HttpClient($authenticator);

	$client = new \Stuart\Client($httpClient);
        ///Create a Job
	//https://github.com/StuartApp/stuart-client-php
	//With scheduling at pickup
	//Validate a Job
	//Before creating a Job you can validate it (control delivery area & address format). Validating a Job is optional and does not prevent you from creating a Job.
	$job = new \Stuart\Job();

	$job->addPickup($pickupAddress);//'46 Boulevard Barbès, 75018 Paris'

	$job->addDropOff($dropAddress)->setPackageType('small');//'156 rue de Charonne, 75011 Paris'
	    
	$result = $client->validateJob($job);
	//Get a pricing

	//Before creating a Job you can ask for a pricing. Asking for a pricing is optional and does not prevent you from creating a Job.	
	//Get a job ETA to pickup

	//Before creating a Job you can ask for an estimated time of arrival at the pickup location (expressed in seconds). Asking for ETA is optional and does not prevent you from creating a job.
	if($result){	
		$job = new \Stuart\Job();

		$job->addPickup('46 Boulevard Barbès, 75018 Paris');

		$job->addDropOff('156 rue de Charonne, 75011 Paris')
		    ->setPackageType('small');
		    
		$eta = $client->getEta($job);
		$client->createJob($job);
		echo $eta->eta;die;
	}*/
    }
    public static function getAccessToken(){
        $url = "https://sandbox-api.stuart.com/oauth/token"; 
        $api_client_id = '9850da35f3ed171188e535502bf2b9ecff0b9809eeefcde8ed0f549bfe66e7af'; // can be found here: https://admin-sandbox.stuart.com/client/api
	    $api_client_secret = '9c5997ef47652a7386bd01a373ee911bcb36060e6bd4b34a98cfbd2bbdf26319'; // can be found here: https://admin-sandbox.stuart.com/client/api    
        $curl = curl_init();
            
             curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "client_id=$api_client_id&client_secret=$api_client_secret&scope=api&grant_type=client_credentials",
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
                //stuart_api_auth
                // DB::table('refund_histories')->where('order_id', $orderInfo["id"])->sum('amount_refunded');  
              echo $response;
            }
    }
    public static function createJobStuart($ordeData){
        //echo $access_token = self::getAccessToken(); die;
        $access_token = "eyJhbGciOiJFUzI1NiJ9.eyJpc3MiOiJzdHVhcnQtYXBpIiwiaWF0IjoxNTk1NDA5ODI1LCJqdGkiOiI5MDMxZmQ3Ny1mYTA1LTRlMzQtOTU3ZC1jOTc4OWRjOTdkY2MiLCJzcnQ6ZW52aXJvbm1lbnRzIjpbInNhbmRib3giXX0.2XUKbPA2PvNI9rwREY4qrZUWnP4f5YP-lkPlnQvYjg8ZQxxyJF9obIsyuvXIPG6_LTTxFp__NrbFSQ3VWj9NvQ";
        $url = "https://sandbox-api.stuart.com/v2/jobs";
        $curl = curl_init();
        $dataArray = ["job"=>[
                 "assignment_code"=> "ACC861MM",
                 "pickups"=> [
                              [
                                "address"=> "12 rue rivoli, 75001 Paris",
                                "comment"=> "Ask Bobby",
                                "contact"=> [
                                  "firstname"=> "Pranav",
                                  "lastname"=> "Mishra",
                                  "phone"=> "+33610101010",
                                  "email"=> "pranav.mishra@bravvura.com",
                                  "company"=> "Bravvura Pizza Hut"
                                ]
                              ]
                ],
                "dropoffs"=> [
                              [
                                "package_type"=> "small",
                                "package_description"=> "The blue one.",
                                "client_reference"=> "Order_ID#".time(),
                                "address"=> "42 rue rivoli, 75001 Paris",
                                "comment"=> "2nd floor on the left",
                                "contact"=> [ 
                                  "firstname"=> "Chitrasen",
                                  "lastname"=> "Prabvakar",
                                  "phone"=>"+33611112222",
                                  "email"=> "cprabvakar@bravvura.com",
                                  "company"=> "Bravvura Inc."
                                ]
                              ]
       
       
                ]
            ]
           ];
           curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($dataArray),
              CURLOPT_HTTPHEADER => array(
                "authorization: Bearer $access_token",
                "content-type: application/json"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }
            /*
            "{\"job\":{\"assignment_code\":\"ACC861MM\",\"pickups\":[{\"address\":\"12 rue rivoli, 75001 Paris\",\"comment\":\"Ask Bobby\",\"contact\":{\"firstname\":\"Bobby\",\"lastname\":\"Brown\",\"phone\":\"+33610101010\",\"email\":\"bobby.brown@pizzahut.com\",\"company\":\"Pizza Hut\"}}],\"dropoffs\":[{\"package_type\":\"small\",\"package_description\":\"The blue one.\",\"client_reference\":\"Order_ID#1234_1\",\"address\":\"42 rue rivoli, 75001 Paris\",\"comment\":\"2nd floor on the left\",\"contact\":{\"firstname\":\"Dany\",\"lastname\":\"Dan\",\"phone\":\"+33611112222\",\"email\":\"client1@email.com\",\"company\":\"Sample Company Inc.\"}},{\"package_type\":\"large\",\"package_description\":\"The red one.\",\"client_reference\":\"Order_ID#1234_2\",\"address\":\"6 Place des Vosges, 75004 Paris\",\"contact\":{\"firstname\":\"John\",\"lastname\":\"Doe\",\"phone\":\"+33611111111\",\"email\":\"client2@email.com\",\"company\":\"Sample Company Inc.\"}},{\"package_type\":\"xlarge\",\"package_description\":\"The yellow one.\",\"client_reference\":\"Order_ID#1234_3\",\"address\":\"6 Passage Thiéré, 75011 Paris\",\"comment\":\"3nd floor on the right\",\"end_customer_time_window_start\":\"2020-05-22T11:00:00.000+02:00\",\"end_customer_time_window_end\":\"2020-05-22T13:00:00.000+02:00\",\"contact\":{\"firstname\":\"Julia\",\"lastname\":\"Moore\",\"phone\":\"+33712222222\",\"email\":\"client3@email.com\",\"company\":\"Sample Company Inc.\"}}]}}"
            */
    } 
    /**
     * @param get Restaurnat mail template
     * @author Rahul Gupta
     * @date 14-08-2018
     * It will filter data
     */
    public static function restMailTemplate($mailFor, $restaurantId, $mailKeywords = array()) {
        if ($mailFor) {
            $header = MailTemplate::where(['template_name' => 'header_layout', 'language_id' => self::$langId])->whereIn('restaurant_id', array(0, $restaurantId))->orderBy('restaurant_id', 'DESC')->first();
            if($header) {
                $header = $header->toArray();
                // format header with keys
                $header =  $header['content'];
            }
            $templateContent = MailTemplate::where(['template_name' => $mailFor, 'language_id' => self::$langId])->whereIn('restaurant_id', array(0, $restaurantId))->orderBy('restaurant_id', 'DESC')->first();
            if($templateContent) {
                $templateContent = $templateContent->toArray();
                // format header with keys
                $templateContent =  self::getFinalTemplate($mailKeywords['data'], $templateContent['content']);
            }
            $footer = MailTemplate::where(['template_name' => 'footer_layout', 'language_id' => self::$langId])->whereIn('restaurant_id', array(0, $restaurantId))->orderBy('restaurant_id', 'DESC')->first();
            if($footer) {
                $footer = $footer->toArray();
                // format header with keys
                $footer =  $footer['content'];
            }
            $mailContent = $header . $templateContent . $footer;
            return $mailContent;
        }
        return false;
    }
    /**
     * @param Data array and email tenplate content
     * @author Rahul Gupta
     * @date 14-08-2018
     * It will send replaced content with data
     */
    public static function getFinalTemplate(array $data,$template_content){
        if($template_content){

            $content = $template_content;
            preg_match_all("/\[(.*?)\]/",$content, $options_array);

            if(isset($options_array[1]) && count($options_array[1])) {
                foreach($options_array[1] as $key){
                    if (array_key_exists(strtolower($key),$data))
                    {
                        $content=preg_replace('/\['.strtolower($key).'\]/',$data[strtolower($key)],$content);
                    }elseif (array_key_exists(strtoupper($key),$data)){
                        $content=preg_replace('/\['.strtoupper($key).'\]/', $data[strtoupper($key)],$content);
                    }
                }
                return $content;
            }
        }
    }	
}

