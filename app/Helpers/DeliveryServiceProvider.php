<?php

namespace App\Helpers;

use App\Models\DeliveryServicesLogs;

class DeliveryServiceProvider {

    public $errorMessage;
    public $deliveryRelayDetails = [];
    public $message;
    public $logDetails;
    public $orderCreationSuccess = false;
    public $orderStatus = false;
    public $orderId = false;
    public $orderComment = "";
    public $dsOrderId = false;
    public $orderScheduledAt;
    public $orderCancelledReason = "";
    public $updateComment = "";
    public $deliveredby;

    public function orderCreation($orderDetails) {
        $this->deliveredby = "RELAY";       
        return $this->createOrderRELAY($orderDetails);
    }

    private function createOrderRELAY($orderDetails) {
        $producerKey = explode(",", $this->deliveryRelayDetails['producer_key']);
        if (isset($orderDetails->total_amount)) {
            $order = [
                'order' => [
                    'externalId' => $orderDetails->payment_receipt,
                    'specialInstructions' => $orderDetails->user_comments,
                    'producer' => [
                        'producerLocationKey' => $producerKey[0]
                    ],
                    'consumer' => [
                        'name' => $orderDetails->fname,
                        'phone' => $orderDetails->phone,
                        'location' => [
                            'address1' => $orderDetails->address,
                            'apartment' => $orderDetails->address2,
                            'city' => $orderDetails->city,
                            'state' => $orderDetails->state_code,
                            'zip' => $orderDetails->zipcode
                        ]
                    ],
                    'price' => [
                        'subTotal' => $orderDetails->order_amount,
                        'tax' => $orderDetails->tax,
                        'tip' => $orderDetails->tip_amount
                    ]
                ]
            ];

            $orderData = json_encode($order);
            $maCurl = curl_init();           
            curl_setopt($maCurl, CURLOPT_URL, $this->deliveryRelayDetails['order_creation_url']);
            curl_setopt($maCurl, CURLOPT_POST, 1);
            curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($maCurl, CURLOPT_HTTPHEADER, array('content-type:application/json', "x-relay-auth:" . $this->deliveryRelayDetails['api_key']));
            curl_setopt($maCurl, CURLOPT_POSTFIELDS, $orderData);
            curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
            $maData = curl_exec($maCurl);
            
            $response = json_decode($maData, true);            

            if (isset($response['success']) && !$response['success']) {
                $this->orderCreationSuccess = $response;
                $deliveryServiceLogData = [
                    'order_receipt' => isset($response['order']['externalId'])?$response['order']['externalId']:"",
                    'order_source_id' => isset($response['order']['source']['orderSourceId'])?$response['order']['source']['orderSourceId']:0,
                    'order_key' => isset($response['order']['orderKey'])?$response['order']['orderKey']:"",
                    'provider_id' => $this->deliveryRelayDetails['id'],
                    'order_id' => $orderDetails->id,
                    'restaurant_id' => $orderDetails->restaurant_id,
                    'request_data' => "order_" . $orderData,
                    'response_data' => $maData
                ];
               
                DeliveryServicesLogs::create($deliveryServiceLogData);
                return false;
            }
           
            if (isset($response['isValid']) && $response['isValid']) {
                $deliveryServiceLogData = [
                    'order_receipt' => $response['order']['externalId'],
                    'order_source_id' => $response['order']['source']['orderSourceId'],
                    'order_key' => $response['order']['orderKey'],
                    'provider_id' => $this->deliveryRelayDetails['id'],
                    'order_id' => $orderDetails->id,
                    'restaurant_id' => $orderDetails->restaurant_id,
                    'request_data' => "order_" . $orderData,
                    'response_data' => $maData
                ];

                DeliveryServicesLogs::create($deliveryServiceLogData);
            }

            if (!curl_errno($maCurl)) {
                $info = curl_getinfo($maCurl);
                return true;
            } else {
                $this->orderCreationSuccess = curl_error($maCurl);
                return false;
            }
        }
    }

    public function updateOrder($orderData) {
        $isOrderCancelled = false;
        $orderCancelledBy = "";
        $updateComment = "";
        $orderAction = "UPDATE";
        if ($this->orderStatus == "cancelled") {
            $isOrderCancelled = true;
            $orderCancelledBy = "REST";
            $orderAction = "CANCEL";
        }

        if ($this->orderStatus == "confirmed") {
            $orderAction = "CONFIRM";
        }

        $updateComment = $this->updateComment;
        if (!$this->updateComment) {
            $updateComment = "";
        }
        if ($orderAction == "CONFIRM") {
            $updateOreder = [
                "orderID" => $this->dsOrderId,
                "updateAction" => $orderAction,
                "isOrderCancelled" => $isOrderCancelled,
                "updateComment" => $updateComment,
                "orderScheduledAt" => $orderData['delivery_date'],
                "orderCancelledBy" => $orderCancelledBy,
                "orderCancelledReason" => $this->orderCancelledReason,
            ];
        } else {
            $updateOreder = [
                "orderID" => $this->dsOrderId,
                "updateAction" => $orderAction,
                "isOrderCancelled" => $isOrderCancelled,
                "updateComment" => $updateComment,
                "orderCancelledBy" => $orderCancelledBy,
                "orderCancelledReason" => $this->orderCancelledReason,
            ];
        }
        $dmgOrderData = json_encode($updateOreder);
        //print_r($dmgOrderData);
        //print_r($this->deliveryRelayDetails['order_ready_url']."?authkey=".$this->deliveryRelayDetails['api_key']);

        $maCurl = curl_init();
        curl_setopt($maCurl, CURLOPT_URL, $this->deliveryRelayDetails['order_ready_url'] . "?authkey=" . $this->deliveryRelayDetails['api_key']);
        curl_setopt($maCurl, CURLOPT_POST, 1);
        curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($maCurl, CURLOPT_HTTPHEADER, array('content-type:application/json', "x-relay-auth:" . $this->deliveryRelayDetails['api_key']));
        curl_setopt($maCurl, CURLOPT_POSTFIELDS, $dmgOrderData);
        curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
        $maData = curl_exec($maCurl);
        $response = json_decode($maData, true);
        //print_r($response);
        //print_r($this->deliveryRelayDetails['order_ready_url']."?authkey=".$this->deliveryRelayDetails['api_key']);
        if (isset($response['code']) && $response['code'] == 200) {
            $deliveryServiceLog = new Model\DeliveryServiceLog();
            $deliveryServiceLog->order_receipt = $orderData['payment_receipt'];
            $deliveryServiceLog->order_source_id = "";
            $deliveryServiceLog->order_key = "";
            $deliveryServiceLog->provider_id = $this->deliveryRelayDetails['id'];
            $deliveryServiceLog->order_id = $orderData['id'];
            $deliveryServiceLog->restaurant_id = $orderData['restaurant_id'];
            $deliveryServiceLog->request_data = $dmgOrderData;
            $deliveryServiceLog->response_data = $maData;
            $deliveryServiceLog->created_date = $response['datetime'];
            $deliveryServiceLog->requested_by = "updateOrder";
            $deliveryServiceLog->insert();
        } else {
            $this->orderCreationSuccess = "An error has occurred on DMG during update order";
            return false;
        }

        if (!curl_errno($maCurl)) {
            $info = curl_getinfo($maCurl);
            return true;
        } else {
            $this->orderCreationSuccess = curl_error($maCurl);
            return false;
        }
    }

}
