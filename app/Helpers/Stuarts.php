<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * common function has been written for used in overall application where required to use
 * Function list
 * [sendMail,sendSMS, netcore]
 */

namespace App\Helpers;

use App\Models\StuartToken;
use App\Models\StuartOrders;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
 

class Stuarts {
     
    public static function callStuartsApi($pickupAddress,$dropAddress){
	/*$environment = \Stuart\Infrastructure\Environment::SANDBOX;
	$api_client_id = '9850da35f3ed171188e535502bf2b9ecff0b9809eeefcde8ed0f549bfe66e7af'; // can be found here: https://admin-sandbox.stuart.com/client/api
	$api_client_secret = '9c5997ef47652a7386bd01a373ee911bcb36060e6bd4b34a98cfbd2bbdf26319'; // can be found here: https://admin-sandbox.stuart.com/client/api
	$authenticator = new \Stuart\Infrastructure\Authenticator($environment, $api_client_id, $api_client_secret);
	$httpClient = new \Stuart\Infrastructure\HttpClient($authenticator);

	$client = new \Stuart\Client($httpClient);
        ///Create a Job
	//https://github.com/StuartApp/stuart-client-php
	//With scheduling at pickup
	//Validate a Job
	//Before creating a Job you can validate it (control delivery area & address format). Validating a Job is optional and does not prevent you from creating a Job.
	$job = new \Stuart\Job();

	$job->addPickup($pickupAddress);//'46 Boulevard Barbès, 75018 Paris'

	$job->addDropOff($dropAddress)->setPackageType('small');//'156 rue de Charonne, 75011 Paris'
	    
	$result = $client->validateJob($job);
	//Get a pricing

	//Before creating a Job you can ask for a pricing. Asking for a pricing is optional and does not prevent you from creating a Job.	
	//Get a job ETA to pickup

	//Before creating a Job you can ask for an estimated time of arrival at the pickup location (expressed in seconds). Asking for ETA is optional and does not prevent you from creating a job.
	if($result){	
		$job = new \Stuart\Job();

		$job->addPickup('46 Boulevard Barbès, 75018 Paris');

		$job->addDropOff('156 rue de Charonne, 75011 Paris')
		    ->setPackageType('small');
		    
		$eta = $client->getEta($job);
		$client->createJob($job);
		echo $eta->eta;die;
	}*/
    }
    public static function getAccessToken(){
	$token = StuartToken::orderBy('id','desc')->first();
	if($token->id){
		$smallestTimestamp = $token->expires_in; 
		$biggestTimestamp = time();
		$numDays = abs($smallestTimestamp - $biggestTimestamp)/60/60/24;
		if($numDays<25)return $token->access_token; 
		$data = DB::table('stuarts_config')->get()->toArray(); 
		if(isset($data[0])){
			$url = $data[0]->url ."/oauth/token"; 
			$api_client_id = $data[0]->api_client_id; 
			$api_client_secret = $data[0]->api_secret ;    
		}else{
			$url = "https://sandbox-api.stuart.com"; 
			$api_client_id = '9850da35f3ed171188e535502bf2b9ecff0b9809eeefcde8ed0f549bfe66e7af'; // can be found here: https://admin-sandbox.stuart.com/client/api
			$api_client_secret = '9c5997ef47652a7386bd01a373ee911bcb36060e6bd4b34a98cfbd2bbdf26319'; // can be found here: https://admin-sandbox.stuart.com/client/api   
		} 
	}else{
			$url = "https://sandbox-api.stuart.com/oauth/token"; 
			$api_client_id = '9850da35f3ed171188e535502bf2b9ecff0b9809eeefcde8ed0f549bfe66e7af'; // can be found here: https://admin-sandbox.stuart.com/client/api
			$api_client_secret = '9c5997ef47652a7386bd01a373ee911bcb36060e6bd4b34a98cfbd2bbdf26319'; // can be found here: https://admin-sandbox.stuart.com/client/api 
	}
        $curl = curl_init();
            
             curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "client_id=$api_client_id&client_secret=$api_client_secret&scope=api&grant_type=client_credentials",
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
		$response = json_decode($response, true);
		//$addon_options= OrderDetailOptions::where('user_order_detail_id',$order_data->id)->get();
		unset($response['scope']);
		unset($response['created_at']);
                //stuart_api_auth
                // DB::table('refund_histories')->where('order_id', $orderInfo["id"])->sum('amount_refunded');  
		 StuartToken::where('id', $token->id)->delete();
		$d = StuartToken::create($response);
              return $response['access_token'];
            }
    }
    public static function createJobStuart($dataArray){
        $access_token = self::getAccessToken();  
        $data = DB::table('stuarts_config')->get()->toArray(); 
		if(isset($data[0])){
			$url = $data[0]->url  ."/v2/jobs"; 
			$api_client_id = $data[0]->api_client_id ; 
			$api_client_secret = $data[0]->api_secret;    
		}else  
         	$url = "https://sandbox-api.stuart.com/v2/jobs";
        $curl = curl_init();
         
           curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($dataArray),
              CURLOPT_HTTPHEADER => array(
                "authorization: Bearer $access_token",
                "content-type: application/json"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              return $response;
            }
             
    }
    //Once you successfully created a Job using endpoint, you will be able to get the Job details at any time by using the job_id.
    public static function getStuartJobStatus($jobId){
	$access_token = self::getAccessToken();
	$data = DB::table('stuarts_config')->get()->toArray(); 
		if(isset($data[0])){
			$url = $data[0]->url ."/v2/jobs/$jobId"; 
			$api_client_id = $data[0]->api_client_id ; 
			$api_client_secret = $data[0]->api_secret;    
		}else  
        $url = "https://sandbox-api.stuart.com/v2/jobs/$jobId";
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_POSTFIELDS => "{}",
	  CURLOPT_HTTPHEADER => array(
                "authorization: Bearer $access_token",
                "content-type: application/json"
              ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  return $response;
	}
    }
    public static function requestJobETA($dataArray){
	 
	 $access_token = self::getAccessToken(); 
	$data = DB::table('stuarts_config')->get()->toArray(); 
		if(isset($data[0])){
			$url = $data[0]->url."/jobs/eta"; 
			$api_client_id = $data[0]->api_client_id; 
			$api_client_secret = $data[0]->api_secret;    
		}else
        $url = "https://sandbox-api.stuart.com/v2/jobs/eta";
        $curl = curl_init();
         
           curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($dataArray),
              CURLOPT_HTTPHEADER => array(
                "authorization: Bearer $access_token",
                "content-type: application/json"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              return $response;
            } 
    }	
    public static function cancelAJob($jobId){
	$curl = curl_init();
	$data = DB::table('stuarts_config')->get()->toArray(); 
		if(isset($data[0])){
			$url = $data[0]->url ."/v2/jobs/"; 
			$api_client_id = $data[0]->api_client_id; 
			$api_client_secret = $data[0]->api_client_id;    
		}else $url = "https://sandbox-api.stuart.com/v2/jobs/";
	$url = $url."$jobId/cancel";
	$access_token = self::getAccessToken(); 
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{}",
	  CURLOPT_HTTPHEADER => array(
	    "authorization: Bearer $access_token"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  return null;
	} else {
	  return $response;
	}
    }
    public function cancelDelivery(){
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://sandbox-api.stuart.com/v2/deliveries/%7Bdelivery_id%7D/cancel",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{}",
	  CURLOPT_HTTPHEADER => array(
	    "authorization: Bearer <<access_token>>"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}
    }	
}
