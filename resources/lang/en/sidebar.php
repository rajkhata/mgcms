<?php

return [

    /*
    |--------------------------------------------------------------------------
    | sidebar menu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'working_hours' => 'Working Hours',
    'floor' => 'FLOOR',
    'reservation' => 'Reservation',
    'vouchers' => 'Vouchers',
    'restaurant' => 'Restaurant',
    'options' => 'Options',
    'configure' => 'Configure',
    'turnover_time' => 'Turnover Time',
    'hours' => 'Hours',
    'archive' => 'Archive',
    'users' => 'Rest. Users',
    'restaurant_list' => 'Rest. list',
    'pizzaexp' => 'Pizza Exp:',
    'pizzaexplist' => 'PE list',
    'pizzaexpsetting' => 'PE Setting',
    'pizzaexpsettingitems' => 'PE Setting Items',
    
    
    'normalmenu' => 'Normal Menu',
    'manage_produts' => 'Manage Products',
    'categories' => 'Categories',
    'subcategories' => 'Subcategories',
    'meal_type' => 'Meal Types',
    'menu_item' => 'Food Items',
    'menu_products' => 'Products',
    'menu_giftcards' => 'Gift Cards',    
    'menu_setting' => 'Menu Settings',
    'menu_setting_category' => 'Menu Setting Category',
    'menu_setting_item' => 'Menu Setting Item',
    'language_setting' => 'Language Settings',
    'languages' => 'Languages',
    'localization' => 'Localization',
    'static_pages' => 'Static Pages',
    'static_pages_list' => 'Pages list',
    'static_block_list' => 'Blocks list',
    'templates' => 'Templates',
    'mailtemplate' => 'Mail Templates',
    'manage_sms' => 'Mansge SMS',
    'sms' => 'SMS',
    'server_list' => 'Servers',
    'server_assign' => 'Server Assign',
    'server' => 'Server',
    'manage_countries' => 'Manage Countries',
    'country' => 'Country',
    'states' => 'States',
    'cities' => 'Cities',
    'delivery-hours' => 'Delivery Hours',
    'carryout-hours' => 'Carryout Hours',
    'operational-hours' => 'Operational Hours',
    'global_settings' => 'Global Settings',
    'guest_tags' => 'Guest Tags',
	'static_request_list' => 'Request List',
	'manage_services' => 'Manage Services',
    'menu' => 'Menu',
];
