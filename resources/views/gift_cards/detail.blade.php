@extends('layouts.app')
@section('content')
    <div class="main__container margin-lr-auto giftcard-detail-page" id="giftcard-container">
        <div>
            <div class="order-detail-page popup_orderdetail row">
                <div class="t-scroll1 res-bott-space col-md-10 footeractive col-md-offset-1">
                    <div class="row margin-bottom-30">
                        <div class="col-sm-1 text-left no-padding-left no-padding-right padding-top-bottom-10">
                            <a href="javascript:goBack()" class="font-weight-600"><i class="fa fa-arrow-left margin-right-10" aria-hidden="true"></i>Back</a>
                        </div>
                        <div class="col-sm-11 print__button text-right no-padding-right">
                            @if($allGiftStatus && $userOrder->status !='refunded')
                                <a href="javascript:void(0)" data-order-id="<?php echo $userOrder->id;?>" disabled="disabled" class="send_all_btn btn btn__primary margin-left-10 font-weight-700">Send All Gift Cards</a>
                            @else
                                @if($orderData->status !='refunded')
                                <a href="javascript:void(0)" send_all = "1" data-order-id="<?php echo $userOrder->id;?>"  class="send_all_btn btn btn__primary margin-left-10 font-weight-700 send-email-gift-coupon" id = "0" >Send All Gift Cards</a>
                                @endif
                            @endif
                             @if($orderData->status !='refunded')
                                    <button type="button" data-toggle="modal" data-target="#refundOrder" class="btn btn__holo action_btn min-width-180 min-sm-width-150 font-weight-600 margin-left-10">
                                        {{--<button type="button" class="btn btn__primary action_btn font-weight-600" onclick="window.location='{{ URL::to('user_order/' . $orderData->id .'/refund/'. $action) }}'">--}}
                                        Refund Order
                                    </button>
                                @endif
                            <button type="button" class="btn btn__primary action_btn font-weight-700 margin-left-10" onclick="window.location='{{ URL::to('user_order/'.$userOrder->id.'/gift_card/logs') }}'">
                                Log
                            </button>
                        </div>
                    </div>

                    <div class="row dis-flex res-dis-block od__container tablet-box-shadow border-box">
                        <div class="col-lg-6 col-lg-offset-0 padding-bottom-20 col-sm-offset-0 col-sm-6 dis-flex-1 od__details desktop-box-shadow">

                            <div class="txt_order_des order_del_border custom-width  ">
                                <div class="order-title fullWidth o-head">
                                    <div class="col-sm-12">
                                        <b>ORDER DETAILS</b></div>
                                </div>
                                <div class="row fullWidth food_order_item">
                                    <div class="col-lg-5 col-xs-12 order-text-align">Receipt No. </div>
                                    <div class="col-lg-1 col-xs-1 text-center hidden-sm hidden-xs hidden-md"><b>:</b></div>
                                    <div class="detail__ans col-lg-6 col-xs-12" style="text-transform: capitalize;"><span>{{ $userOrder->payment_receipt }}</span>
                                    </div>
                                </div>
                                <div class="row fullWidth food_order_item">
                                    <div class=" col-lg-5 col-xs-12 order-text-align">Time of Order </div>
                                    <div class="col-md-1 col-xs-1 text-center hidden-sm hidden-xs hidden-md"><b>:</b></div>
                                    <div class="detail__ans col-lg-6 col-xs-12"><span>{{ $orderDate }}</span>{{--Wednesday 17 Oct, 04:19 AM--}}
                                    </div>
                                </div>
                                <!-- address end -->
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-offset-0 padding-bottom-20 no-margin-right col-sm-offset-0 col-sm-6 _pr0 dis-flex-1 od__details desktop-box-shadow">
                                <div class=" full_height  custom-width">

                                    <div class="txt_profile order_del_border black-color">
                                        <div class="order-title fullWidth o-head">
                                            <div class="col-sm-12">
                                                <b>CUSTOMER DETAILS</b>
                                            </div>
                                        </div>
                                        <div class="row fullWidth food_order_item">
                                            <div class="col-lg-5 col-xs-12 col-sm-12 order-text-align">Name</div>
                                            <div class="col-lg-1 col-xs-1 text-center hidden-sm hidden-xs hidden-md"><b>:</b></div>
                                            <div class="detail__ans col-lg-6 col-xs-12 text-capitalize"><span>{{ ucwords($userOrder->fname.' '.$userOrder->lname) }}</span></div>
                                        </div>
                                        <div class="row fullWidth food_order_item">
                                            <div class="col-lg-5 col-xs-12 order-text-align">Email </div>
                                            <div class="col-lg-1 col-xs-1 text-center hidden-sm hidden-xs hidden-md"><b>:</b></div>
                                            <div class="detail__ans col-lg-6 col-xs-12"><span><a href="mailto:{{ $userOrder->email }}" target="_top">{{ $userOrder->email }}</a></span>
                                            </div>
                                        </div>
                                        <div class="row fullWidth food_order_item">
                                            <div class="col-lg-5 col-xs-12 order-text-align">Telephone </div>
                                            <div class="col-lg-1 col-xs-1 text-center hidden-sm hidden-xs hidden-md"><b>:</b></div>
                                            <div class="detail__ans col-lg-6 col-xs-6"><span>{{ $userOrder->phone }}</span>
                                            </div>
                                        </div>
                                        <div class="row fullWidth food_order_item">
                                            <div class="col-lg-5 col-xs-12 order-text-align"> Activities  </div>
                                            <div class="col-lg-1 col-xs-1 text-center hidden-sm hidden-xs hidden-md"><b>:</b></div>
                                            <div class="detail__ans col-lg-6 col-xs-12 text-capitalize">

                                            <span class="custom__od">
                                                <span>{{ $userOrderCount }} Orders,</span><br>
                                                <span>{{ $userResvCount }} Reservations</span>
                                            </span>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                    </div>

                    <div class="row order__item-container margin-top-20 order-summary-section border-box">
                        <div class="col-lg-12 col-lg-offset-0 col-sm-12 od__details box-shadow">

                            <div class="col-sm-12">
                                <div class="col-sm-12 no-padding-left no-padding-right order-title o-head text-center padding-bottom-20 "><b>ORDER SUMMARY</b></div>

                                    @foreach($allGiftCards as $cDetail)
                                        <div class="col-sm-12 padding-top-bottom-20 border-dash no-padding-left no-padding-right product__data__row">
                                            <div class="col-sm-3 font-weight-700">
                                                {{-- as suggested by Pravish & Rahul P.--}}
                                                <span class="font-weight-800">{{ $cDetail['card_type'] }}</span><br/>
                                                {{ $cDetail['gift_unique_code'] }}
                                                <input type="hidden" name="gift_coupon" class="gift_coupon" value="{{ $cDetail['id'] }}" />
                                            </div>
                                            <div class="col-sm-4 font-weight-700">
                                                <div class="fullWidth">
                                                    <div class="pull-left">Name: </div>
                                                    <div class="pull-left font-weight-800 padding-left-5">{{ $cDetail['name'] }}</div>
                                                </div>
                                                <div class="fullWidth">
                                                    <div class="pull-left">Email: </div>
                                                    <div class="pull-left font-weight-800 padding-left-5">{{ $cDetail['email'] }}</div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2 text-right font-weight-800">
                                                {{ $curSymbol .' '. $cDetail['amount'] }}
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="col-sm-10 col-md-12 col-lg-9 pull-right no-padding gift-action-button">
                                                    {{--Only email option is there, as Classic card is not in use--}}
                                                        @if($orderData->status !='refunded')
                                                        <a href="javascript:void(0)" data-order-id="<?php echo $cDetail['user_order_id'];?>" data-order-detail-id="<?php echo $cDetail['order_detail_id']; ?>"
                                                           send_all="0" <?php if($cDetail['status']==1) { echo 'disabled="disabled"'; } ?>
                                                           class="btn btn__primary fullWidth font-weight-700 <?php if($cDetail['status']==0) { echo 'send-email-gift-coupon'; }?>" id="{{ $cDetail['id'] }}">Email eGift Card</a>
                                                        @endif
                                                </div>
                                            </div>


                                            <div class="col-sm-12 font-weight-800 text-center padding-top-30">
                                                <i class="text-color-grey">{{ $cDetail['user_comment'] }}</i>
                                            </div>
                                        </div>
                                    @endforeach

                                <div class="col-sm-12 no-padding-left no-padding-right padding-top-20 padding-bottom-10 special__instructions__section">
                                    <div class="col-sm-12">
                                        <div class="col-sm-12 no-padding text-color-grey">Special Instructions</div>
                                        <div class="col-sm-12 no-padding font-weight-700 special__instructions__text"><?php echo nl2br($userOrder->user_comments); ?></div>
                                    </div>
                                </div>
                                @php
                                    if(Auth::user()) {
                                        $curSymbol = Auth::user()->restaurant->currency_symbol;
                                    } else {
                                        $curSymbol = config('constants.currency');
                                    }
                                @endphp
                                <div class="col-sm-12 no-padding-left no-padding-right panel_order_subtotal">
                                    <div class="row margin-bottom-10 font-weight-600">
                                        <div class="col-xs-6">Subtotal</div>
                                        <div class="col-xs-6 text-right">{{ $curSymbol .' '. $userOrder->order_amount }}</div>
                                    </div>
                                    {{--<div class="row margin-bottom-10 font-weight-600">
                                        <div class="col-xs-6">Delivery</div>
                                        <div class="col-xs-6 text-right">{{ $curSymbol . number_format($userOrder->delivery_charge,2) }}</div>
                                    </div>--}}
                                    <div class="row margin-bottom-10 font-weight-600">
                                        <div class="col-xs-6">Tax</div>
                                        <div class="col-xs-6 text-right">{{ $curSymbol .' '. number_format($userOrder->tax,2) }}</div>
                                    </div>
                                    {{--<div class="row margin-bottom-10 font-weight-600">
                                        <div class="col-xs-6">Discount</div>
                                        <div class="col-xs-6 text-right">ToDo</div>
                                    </div>--}}
                                    @if($total_refunded_amount > 0 )
                                        <div class="row margin-bottom-10 font-weight-600">
                                            <div class="col-xs-6">Refund</div>
                                            <div class="col-xs-6 text-right">{{ $curSymbol . ' '. number_format($total_refunded_amount,2) }}</div>
                                        </div>
                                    @endif
                                    <div class="row margin-bottom-10">
                                        <div class="col-xs-6 font-weight-800">Total</div>
                                        <div class="col-xs-6 text-right font-weight-800">{{ $curSymbol .' '. number_format($userOrder->total_amount,2) }}</div>
                                    </div>
                                </div>
                                 @if($refund_reason) 
                                <div class="cancel-preview padding-left-right-15 panel_order_subtotal padding-top-bottom-20">
                                    <p class="font-weight-800 color-black text-uppercase">Refund Reason</p>
                                    <?php foreach($refund_reason as $refund){?>
                                        <div class="col-xs-6">{{$refund['reason']}}</div>
                                        <div class="col-xs-6 text-right">{{ $curSymbol .' '. number_format($refund['amount'],2) }}</div>
                                    <?php } ?>


                                </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection