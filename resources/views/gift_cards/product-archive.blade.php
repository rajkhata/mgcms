<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')
@section('content')
    <div class="main__container container__custom giftcard-listing-page">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <div class="reservation__atto flex-align-item-start">
            <div class="reservation__title margin-top-5">
                <h1>{{$giftOrderData->total()}} Archive <br class="hidden-md hidden-lg"/> Gift Card Orders</h1>
            </div>

            <div class="search__manage-order">

                <div class="fullWidth text-right hidden-lg hidden-md show-tablet margin-bottom-10">
                    <a href="{{ URL::to('order-gift') }}" class="btn btn__holo font-weight-600">Active Gift Card Orders</a>
                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10" ><i class="fa icon-export" aria-hidden="true"></i> Export</a>
                </div>

                {!! Form::open(['method'=>'get']) !!}
                <div class="fullWidth form_field__container flex-justify-content-end">

                    <div class="guestbook__sec2">
                        <div class="search-icon no-margin-right">
                            <i class="fa fa-search"></i>
                            <input type="text" name="key_search" autocomplete="off" placeholder="Name and Receipt no." id="key_search" value="{{ $_GET['key_search']??'' }}">
                        </div>
                    </div>

                    <!--<div class="selct-picker-plain margin-left-20 filter-by-status">
                    <select class="selectpicker" data-style="no-background-with-buttonline margin-top-5 no-padding-left  no-padding-top" data-width="150" name="order_status" id="order_status">
                        @foreach($orderStatus as $key=>$value)
                    <option value="{{$key}}" <?php if($statusKey==$key) echo "selected";?>>{{ $value }}</option>
                        @endforeach
                        </select>

                    </div>-->

                    <div class="margin-left-10 no-margin-right">
                        <button class="btn btn__primary font-weight-600" type="submit">Search</button>
                    </div>


                    <div class="hidden-xs hidden-sm hidden-tablet margin-left-10"><a href="{{ URL::to('order-gift') }}" class="btn btn__holo font-weight-600">Active Gift Card Orders</a></div>
                    <div class="hidden-xs hidden-sm hidden-tablet"><a href="javascript:void(0)" rel="gift-archive" id="gift_export" class="btn btn__holo font-weight-600" ><i class="fa icon-export" aria-hidden="true"></i> Export</a></div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <div class="guestbook__container fullWidth box-shadow">

            <div class="guestbook__table-header fullWidth">
                <div role="button" class="col-xs-4 col-sm-4 col-lg-5 no-padding-left">
                    <div class="col-xs-6 col-sm-6 show-tablet-50 col-lg-4 no-padding-left">Receipt No.</div>
                    <div class="col-xs-6 col-sm-6 show-tablet-50 col-lg-5">Name</div>
                    <div class="col-xs-3 col-sm-3 hidden-sm hidden-tablet no-padding-left">Amount</div>
                </div>
                <div class="col-xs-2 col-sm-2 padding-left-25 no-padding-right">Type</div>
                <div class="col-xs-2 col-sm-2 col-md-2">Quantity</div>
                <div class="col-xs-2 col-sm-2 no-padding">Order In</div>
                <div class="col-xs-2 col-sm-2 col-lg-1"></div>
            </div>

                @if(isset($giftOrderData) && count($giftOrderData)>0)
                    @foreach($giftOrderData as $giftOrder)
                        @php
                            $cardTypeArr = [];
                            if(count($giftOrder->details)) {
                                foreach($giftOrder->details as $detail) {
                                    $menuJson   = !empty($detail->menu_json) ? json_decode($detail->menu_json) : '';
                                    $detailType = isset($menuJson->type) ? $menuJson->type : '';
                                    if(!empty($detailType) && !in_array($detailType, $cardTypeArr)) {
                                        $detailType = ($detailType == 'egiftcard') ? 'egiftcard' : 'classiccard';
                                        $cardTypeArr[] = $detailType;
                                    }
                                }
                            }
                            $cardType = array_map(function($v) use($cardTypeArr) {
                                if($v=='egiftcard') return 'eGift Card'; else return 'Classic Card';
                            }, $cardTypeArr);
                            if(Auth::user()) {
                                $curSymbol = Auth::user()->restaurant->currency_symbol;
                            } else {
                                $curSymbol = config('constants.currency');
                            }
                        @endphp
                        <div class="guestbook__cutomer-details-wrapper">
                            <div class="row guestbook__customer-details-content fullWidth text-color-black font-weight-700">
                                <div role="button" onclick="window.location='{{ URL::to('/order-gift-archive/' . $giftOrder->id) }}'" class="col-xs-4 col-sm-4 col-lg-5 no-padding-left font-weight-700">
                                    <div class="col-xs-6 col-sm-6 show-tablet-50 col-lg-4 no-padding-left" >{{ $giftOrder->payment_receipt }}</div>
                                    <div class="col-xs-6 col-sm-6 show-tablet-50 col-lg-5 text-ellipsis" >{{ ucwords($giftOrder->fname . ' ' . $giftOrder->lname) }}</div>
                                    <div class="col-xs-3 col-sm-3 show-tablet-50 no-padding-left">{{ $curSymbol. (float)$giftOrder->total_amount + (float)$giftOrder->delivery_time }}</div>
                                </div>

                                <div role="button" onclick="window.location='{{ URL::to('/order-gift-archive/' . $giftOrder->id) }}'" class="col-xs-2 col-sm-2 font-weight-700 padding-left-25 no-padding-right">{{ implode(' + ', $cardType) }}</div>
                                <div role="button" onclick="window.location='{{ URL::to('/order-gift-archive/' . $giftOrder->id) }}'" class="col-xs-2 col-sm-2 col-md-2 font-weight-700">{{ $giftOrder->quantity }}</div>
                                <div role="button" onclick="window.location='{{ URL::to('/order-gift-archive/' . $giftOrder->id) }}'" class="col-xs-2 col-sm-2 font-weight-700 no-padding">
                                    @php
                                        $timestamp2 = '';
                                        $currentDate = date('Y-m-d H:i:s');
                                        $currentTimestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $giftOrder->restaurant_id, 'datetime' => $currentDate)));
                                        $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $giftOrder->restaurant_id, 'datetime' => $giftOrder->created_at)));
                                        //$createdAt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $giftOrder->created_at); @endphp
                                    <strong>{{ date('D, M d', $timestamp2) }}</strong> <br>{{--Monday, Oct 15--}}
                                    <div class="text-color-grey">{{ date('h:i A', $timestamp2) }}</div>{{--01:10 PM--}}
                                </div>

                                <div class="col-xs-2 col-sm-2 col-lg-1 no-padding-right">
                                    <div class="guest_btn fullWidth">
                                        <a href="{{ URL::to('/order-gift-archive/' . $giftOrder->id) }}" class="btn btn__primary font-weight-700">
                                            View
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-center col-xs-12 col-sm-12 margin-top-bottom-40" >
                        <strong>No Record Found</strong>
                    </div>
                @endif

            </div>
            @if(isset($giftOrderData) && count($giftOrderData)>0)
                <div class="text-center" >
                    {{ $giftOrderData->appends($_GET)->links()}}
                </div>
            @endif
        </div>

        {{--<div class="addition__functionality hide">
            {!! Form::open(['method'=>'get']) !!}
            <div class="row functionality__wrapper">
                <div class="row form-group col-md-3">
                    <select name="restaurant_id" id="restaurant_id"
                            class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}">
                        <option value="">Select Restaurant</option>
                        @foreach($groupRestData as $rest)
                            <optgroup label="{{ $rest['restaurant_name'] }}">
                                @foreach($rest['branches'] as $branch)
                                    <option value="{{ $branch['id'] }}" {{ (isset($searchArr['restaurant_id']) && $searchArr['restaurant_id']==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
                <div class="row form-group col-md-1">
                    &nbsp;
                </div>
                <div class="row form-group col-md-3">
                    <input class="form-control" placeholder="Name" type="text" name="name"
                           value="{{isset($searchArr['name']) ? $searchArr['name'] : ''}}">
                </div>
                <div class="row form-group col-md-3">
                    <button class="btn btn__primary" style="margin-left: 5px;" type="submit">Search</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="hide">
            <div class="active__order__container">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif

                <div class="guestbook__container box-shadow">
                    <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
                        <div class="col-md-10 row">
                            <div class="col-md-4" style="font-weight: bold;">Name</div>
                            <div class="col-md-4" style="font-weight: bold;">Restaurant</div>
                            <div class="col-md-2" style="font-weight: bold;">Category</div>
                            <div class="col-md-2" style="font-weight: bold;">Status</div>
                        </div>
                        <div class="col-md-2 row">
                            <div class="col-md-12" style="font-weight: bold;">Action</div>
                        </div>
                    </div>
                    @if(isset($menuItems) && count($menuItems)>0)
                        @foreach($menuItems as $menuItem)
                            <div class="guestbook__customer-details-content rest__row">
                                <div class="col-md-10 order__row">
                                    <div class="col-md-4" style="margin: auto 0px;">{{ $menuItem->name}}</div>
                                    <div class="col-md-4"
                                         style="margin: auto 0px;">{{ $menuItem->Restaurant->restaurant_name }}</div>
                                    <div class="col-md-2"
                                         style="margin: auto 0px;">{{ $menuItem->MenuCategory->name }}</div>
                                    <div class="col-md-2"
                                         style="margin: auto 0px;">{{ ($menuItem->status==1)?'Active':($menuItem->status==0?'Inactive':'') }}</div>
                                </div>
                                <div class="row col-md-2" style="margin: auto 0px;">
                                    <div class="col-md-12">
                                        <a style="width:100%;margin:10px 0;" href="{{ URL::to('menu_item/' . $menuItem->id . '/edit') }}"
                                           class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
                                    </div>
                                    <div class="col-md-12">
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['menu_item.destroy', $menuItem->id]]) !!}
                                        @csrf
                                        <button style="width:100% ;margin:10px 0;" class="btn btn__cancel" type="submit"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="row"
                             style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                            No Record Found
                        </div>
                    @endif
                </div>

                @if(isset($menuItems) && count($menuItems)>0)
                    <div style="margin: 0px auto;text-align:center;">
                        {{ $menuItems->appends($_GET)->links()}}
                    </div>
                @endif
            </div>
        </div>--}}

    </div>

    <script type="text/javascript">
        setTimeout(function(){ // Requirement by Rahul Prabhakar
            location.reload();
        }, 60000);
    </script>
@endsection