@extends('layouts.app')
@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">

            <div class="card">
                <div class="card-header" style="padding: 8px;">
                    Pizza Settings
                    <span style="float: right;margin:0px 5px;">({{ $pizzaSettings->currentPage() .'/' . $pizzaSettings->lastPage() }})</span>&emsp;
                    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('pizzaexp/create') }}" class="btn btn-primary">Add</a></span>
                </div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'get']) !!}
                {{--<div class="row" style="margin:10px;justify-content: center;">
                    <div class="row form-group col-md-2">&nbsp;</div>
                    <div class="row form-group col-md-3">
                        <select name="restaurant_id" id="restaurant_id" class="form-control" >
                            <option value="">Select Restaurant</option>
                            @foreach($restaurantData as $rest)
                                <option value="{{ $rest->id }}" {{ (isset($searchArr['restaurant_id']) && $searchArr['restaurant_id']==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row form-group col-md-1">
                        &nbsp;
                    </div>
                    <div class="row form-group col-md-3">
                        <input class="form-control" placeholder="Name" type="text" name="name" value="{{isset($searchArr['name']) ? $searchArr['name'] : ''}}">
                    </div>
                    <div class="row form-group col-md-3">
                        <button class="btn btn-info" style="margin-left: 5px;" type="submit" >Search</button>
                    </div>
                </div>--}}
                {!! Form::close() !!}
                <div class="card-body" style="padding-top:5px;">
                    <div class="row panel-heading" style="background-color: rgba(0,0,0,.03);padding:8px 0px;margin-bottom: 5px;">
                        <div class="col-md-3" style="font-weight: bold;">Name</div>
                        <div class="col-md-2" style="font-weight: bold;">Restaurant</div>
                        <div class="col-md-2" style="font-weight: bold;">Parent</div>
                        <div class="col-md-1" style="font-weight: bold;">Multi Select</div>
                        <div class="col-md-1" style="font-weight: bold;">Customizable</div>
                        <div class="col-md-1" style="font-weight: bold;">Status</div>
                        <div class="col-md-2" style="font-weight: bold;">Action</div>
                    </div>
                    @if(isset($pizzaSettings) && count($pizzaSettings)>0)
                        @foreach($pizzaSettings as $pizzaSet)
                            <div class="row" style="padding-bottom: 6px;">
                                <div class="col-md-3" style="margin: auto 0px;">{{ ucfirst($pizzaSet->label) }}</div>
                                <div class="col-md-2" style="margin: auto 0px;">{{ $pizzaSet->Restaurant->restaurant_name }}</div>
                                <div class="col-md-2" style="margin: auto 0px;">{{ isset($pizzaSet->parent->label) ? ucfirst($pizzaSet->parent->label) : '' }}</div>
                                <div class="col-md-1" style="margin: auto 0px;">{{ ($pizzaSet->is_multiselect==1)?'Yes':($pizzaSet->is_multiselect==0?'No':'') }}</div>
                                <div class="col-md-1" style="margin: auto 0px;">{{ ($pizzaSet->is_customizable==1)?'Yes':($pizzaSet->is_customizable==0?'No':'') }}</div>
                                <div class="col-md-1" style="margin: auto 0px;">{{ ($pizzaSet->status==1)?'Active':($pizzaSet->status==0?'Inactive':'') }}</div>
                                <div class="row col-md-2" style="margin: auto 0px;">
                                    <div class="col-md-5">
                                        <a href="{{ URL::to('pizzaexp/' . $pizzaSet->id . '/edit') }}" class="btn btn-info">Edit</a>
                                    </div>
                                    <div class="col-md-7">
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['pizzaexp.destroy', $pizzaSet->id]]) !!}
                                        @csrf
                                        <button class="btn btn-default" type="submit">Delete</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                            No Record Found
                        </div>
                    @endif
                </div>

                @if(isset($pizzaSettings) && count($pizzaSettings)>0)
                    <div style="margin: 0px auto;">
                        {{ $pizzaSettings->appends($_GET)->links()}}
                    </div>
                @endif
            </div>

        </div>
    </div>
@endsection
