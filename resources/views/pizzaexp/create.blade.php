@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Pizza Setting - Create') }}</h1>

    </div>

  </div>
  <div>

    <div class="card form__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit">
          <form method="POST" action="{{ route('pizzaexp.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row form_field__container">
              <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
              <div class="col-md-6">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                  required autofocus >
                  <option value="">{{ __('Select restaurant') }}</option>
                  @foreach($allRests as $rests)
                  <option value="{{ $rests->id }}">{{ $rests->restaurant_name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="parent_setting" class="col-md-4 col-form-label text-md-right">{{ __('Parent Setting') }}</label>
              <div class="col-md-6">
                <select name="parent_setting" id="parent_setting" class="form-control{{ $errors->has('parent_setting') ? ' is-invalid' : '' }}"
                  autofocus>
                  <option value="">{{ __('Select parent setting') }}</option>
                </select>
                @if ($errors->has('parent_setting'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('parent_setting') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="label" class="col-md-4 col-form-label text-md-right">{{ __('Setting Label') }}</label>
              <div class="col-md-6">
                <div class="input__field">
                  <input id="label" type="text" class="{{ $errors->has('label') ? ' is-invalid' : '' }}" name="label" value="" required autocomplete="off">
                </div>
                @if ($errors->has('label'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('label') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-4" style="padding-top: 10px;">
                <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1') ? 'checked' :'' }}> Active
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }}> Inactive
                @if ($errors->has('status'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="is_multiselect" class="col-md-4 col-form-label text-md-right">{{ __('Multiple Select') }}</label>
              <div class="col-md-4" style="padding-top: 10px;">
                <input type="radio" name="is_multiselect" value="1" {{ (old('is_multiselect')=='1') ? 'checked' :'' }}> Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="is_multiselect" value="0" {{ (old('is_multiselect')=='0') ? 'checked' :'' }}> No
                @if ($errors->has('is_multiselect'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('is_multiselect') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="is_customizable" class="col-md-4 col-form-label text-md-right">{{ __('Customizable') }}</label>
              <div class="col-md-4" style="padding-top: 10px;">
                <input type="radio" name="is_customizable" value="1" {{ (old('is_customizable')=='1') ? 'checked' :'' }}> Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="is_customizable" value="0" {{ (old('is_customizable')=='0') ? 'checked' :'' }}> No
                @if ($errors->has('is_customizable'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('is_customizable') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="is_preference" class="col-md-4 col-form-label text-md-right">{{ __('Preference') }}</label>
              <div class="col-md-4" style="padding-top: 10px;">
                <input type="radio" name="is_preference" value="1" {{ (old('is_preference')=='1') ? 'checked' :'' }}> Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="is_preference" value="0" {{ (old('is_preference')=='0') ? 'checked' :'' }}> No
                @if ($errors->has('is_preference'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('is_preference') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Priority') }}</label>
              <div class="col-md-6">
                <div class="input__field">
                  <input id="priority" type="text" class="{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                  name="priority" value="{{ old('priority') }}" autocomplete="off" required>
                </div>
                @if ($errors->has('priority'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('priority') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row text-center mb-0">
              <div class="text-center col-md-12">
                <button type="submit" class="btn btn__primary">{{ __('Save Settings') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>


    </div>
  </div>
</div>

<script type="text/javascript">
$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $("#restaurant_id").change(function () {
    var rest_id = '';
    rest_id = $('option:selected').val();
    if (rest_id != '') {
      $('#category_id').find('option').not(':first').remove();
      $.ajax({
        type: 'POST',
        url: '<?php echo URL::to('/') . "/get_restaurant_settings"; ?>',
        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
        success: function (data) {
          if ($.type(data.dataObj) !== 'undefined') {
            if ($.type(data.dataObj) !== 'undefined') {
              $.each(data.dataObj, function (i, item) {
                console.log(item.id + ' ' + item.label);
                $('#parent_setting').append('<option value="' + item.id + '">' + item.label + '</option>');
              });
            }
          }
        }
      });
    }
  });

});
</script>

@endsection
