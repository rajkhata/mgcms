@extends('layouts.app')
@section('content')

    <div class="main__container container__custom">

        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>{{ __('CMS Users - Edit') }}</h1>
            </div>
        </div>

        <div>

            <div class="card form__container no-margin-top">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    <div style="text-align:left">
                        <a class="btn" href="{{ route('cms_user.index') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    </div>
                    <div class="form__user__edit" style="width:100%">
                        {{ Form::model($cmsUser, array('route' => array('cms_user.update', $cmsUser->id), 'method' => 'PUT')) }}
                        @csrf
<!--                        <div class="form-group row form_field__container">
                            <label for="category_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>
                            <div class="col-md-4">
                                <select name="state_id" id="state_id"
                                        class="form-control {{ $errors->has('state_id') ? ' is-invalid' : '' }}">
                                    <option value="">Select State</option>
                                    @if(isset($stateData))
                                        @foreach($stateData as $state)
                                            <option value="{{ $state->id }}" {{ ((old('state_id')==$state->id) || (count(old())==0 && isset($cmsUser->state_id) && $cmsUser->state_id==$state->id)) ? 'selected' :'' }}>{{ $state->state }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('state_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('state_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>-->

<!--                        <div class="form-group row form_field__container">
                            <label for="city_id" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
                            <div class="col-md-4">
                                <select name="city_id" id="city_id"
                                        class="form-control {{ $errors->has('city_id') ? ' is-invalid' : '' }}">
                                    <option value="">Select City</option>
                                    @php
                                        if(session()->has('newCityData')){
                                            $cityData = session()->get('newCityData');
                                        }
                                    @endphp
                                    @if(isset($cityData))
                                        @foreach($cityData as $city)
                                            <option value="{{ $city->id }}" {{ ((old('city_id')==$city->id) || (count(old())==0 && isset($cmsUser->city_id) && $cmsUser->city_id==$city->id)) ? 'selected' :'' }}>{{ $city->city_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('city_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>-->

                        <div class="row">
                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <label for="restaurant_id"
                                    class="col-form-label text-md-right">{{ __('Restaurant') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="restaurant_id" id="restaurant_id"
                                            class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Restaurant</option>
                                        @php
                                            if(session()->has('newRestaurantData')){
                                                $restaurantData = session()->get('newRestaurantData');
                                            }
                                        @endphp
                                        @if(isset($restaurantData))
                                            @foreach($restaurantData as $rest)
                                                <option value="{{ $rest->id }}" {{ ((old('restaurant_id')==$rest->id) || (count(old())==0 && isset($cmsUser->restaurant_id) && $cmsUser->restaurant_id==$rest->id)) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('restaurant_id'))
                                        <span class="invalid-feedback">
                                                <strong>{{ $errors->first('restaurant_id') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <label for="roles"
                                    class="col-form-label text-md-right">{{ __('Role') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple', 'data-style' => 'no-background-with-buttonline', 'no-padding-left', 'margin-top-5', 'no-padding-top')) !!}
                                    @if ($errors->has('roles'))
                                        <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('roles') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="">
                                    <div class="input__field">
                                        <input id="name" type="text"
                                            class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            name="name"
                                            value="{{ old('name') ? old('name') : (isset($cmsUser->name) && count(old()) == 0 ?$cmsUser->name:'') }}"
                                            required>
                                            <span for="name">Name</span>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="">
                                    <div class="input__field">
                                        <input id="email" type="email"
                                            class="{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email"
                                            value="{{ old('email') ? old('email') : (isset($cmsUser->email) && count(old()) == 0 ?$cmsUser->email:'') }}"
                                            required>
                                            <span for="email">Email</span>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- <div class="form-group row form_field__container">
                            <label for="password"
                                   class="col-sm-4 col-md-4 col-lg-4 col-xs-12 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                <div class="input__field">
                                    <input id="password" type="password"
                                           class="{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" value="{{ old('password')}}">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->

<!--                        <div class="form-group row form_field__container">
                            <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="mobile" type="text"
                                           class="{{ $errors->has('mobile') ? ' is-invalid' : '' }}"
                                           name="mobile"
                                           value="{{ old('mobile') ? old('mobile') : (isset($cmsUser->mobile) && count(old()) == 0 ?$cmsUser->mobile:'') }}"
                                           required>
                                </div>
                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="phone" type="text"
                                           class="{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                           name="phone"
                                           value="{{ old('phone') ? old('phone') : (isset($cmsUser->phone) && count(old()) == 0 ?$cmsUser->phone:'') }}">
                                </div>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>-->

<!--                        <div class="form-group row form_field__container">
                            <label for="memail"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Other Emails') }}</label>
                            <div class="col-md-4">
                                <div class="custom__message-textContainer">
                                <textarea maxlength="500" rows="4" id="memail" name="memail"
                                          class="{{ $errors->has('memail') ? ' is-invalid' : '' }}">{{ old('memail') ? old('memail') : (isset($cmsUser->memail) && count(old()) == 0 ? $cmsUser->memail : '') }}</textarea>
                                </div>
                                @if ($errors->has('memail'))
                                    <span class="invalid-feedback" style="display: block">
                                        <strong>{{ $errors->first('memail') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="">
                                    <div class="input__field">
                                        <input id="password" type="password"
                                            class="{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            name="password" value="{{ old('password')}}">
                                            <span for="password">Password</span>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="row">
                                    <label for="status" class="col-xs-12 col-sm-2 col-form-label text-md-right no-padding-left">{{ __('Status') }}</label>
                                    <div class="col-xs-12 col-sm-10" style="padding-top: 0px;">
                                        <input type="radio" id="status1" name="status"
                                            value="1" {{ ((isset($cmsUser->status) && $cmsUser->status=='1' && count(old())==0) || old('status')=='1') ? 'checked' :'' }}>
                                        Active
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" id="status0" name="status"
                                            value="0" {{ ((isset($cmsUser->status) && $cmsUser->status=='0' && count(old())==0) || old('status')=='0') ? 'checked' :'' }}>
                                        Inactive
                                        @if ($errors->has('status'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('status') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row form_field__container margin-top-30">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-align-center">
                                <button type="submit" class="btn btn__primary">{{ __('Edit User') }}</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#state_id').on('change', function () {
                var ajaxurl = '/get_cities_by_state';
                $('#city_id').find('option').not(':first').remove();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/get_cities_by_state"; ?>',
                    data: '_token = <?php echo csrf_token() ?>&state_id=' + this.value,
                    success: function (data) {
                        if ($.type(data.dataObj) !== 'undefined') {
                            $.each(data.dataObj, function (i, item) {
                                $('#city_id').append('<option value="' + item.id + '">' + item.city_name + '</option>');
                            });
                        }
                    }
                });
            });

            $('#city_id').on('change', function () {
                var ajaxurl = '/get_restaurant_by_city';
                $('#restaurant_id').find('option').not(':first').remove();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/get_restaurant_by_city"; ?>',
                    data: '_token = <?php echo csrf_token() ?>&city_id=' + this.value,
                    success: function (data) {
                        if ($.type(data.dataObj) !== 'undefined') {
                            $.each(data.dataObj, function (i, item) {
                                $('#restaurant_id').append('<option value="' + item.id + '">' + item.restaurant_name + '</option>');
                            });
                        }
                    }
                });
            });
            $(function () {
                $('.form-control').selectpicker();
            });
        });
    </script>

@endsection
