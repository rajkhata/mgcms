@extends('layouts.app')

@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>CMS Users</h1>
                        <!-- <h1>CMS Users<span style="margin:0px 5px;">({{ $cmsUsers->currentPage() .'/' . $cmsUsers->lastPage() }}
                        )</span>&emsp;</h1> -->
            </div>
            @can('user add')
            <!-- <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('cms_user/create') }}" class="btn btn__primary">Add</a></span> -->
            @endcan
        </div>
<!--        <div class="addition__functionality">
            {!! Form::open(['method'=>'get']) !!}
            <div class="row functionality__wrapper">
                <div class="row form-group col-md-3">
                    <select name="restaurant_id" id="restaurant_id" class="form-control">
                        <option value="">Select Restaurant</option>
                        @foreach($restaurantData as $rest)
                            <option value="{{ $rest->id }}" {{ (isset($restaurant_id) && $restaurant_id==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row form-group col-md-3">
                    <div class="input__field">
                        <input class="" placeholder="" type="text" name="email"
                               value="{{isset($searchArr['email']) ? $searchArr['email'] : ''}}">
                        <label>Email</label>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" style="display: block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>
                <div class="row form-group col-md-3">
                    <button class="btn btn__primary" style="margin-left: 5px;" type="submit">Search</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>-->

        <div>

            <div class="active__order__container" style="padding:10px!important;">

                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif


                <table id="myTable2" class="responsive ui cell-border hover" style="margin-bottom:10px;" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Restaurant</th>           
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                  <tbody>
                    @if(isset($cmsUsers) && count($cmsUsers)>0)
                        @foreach($cmsUsers as $cmsUser)
                            <tr>
                              <td>{{ $cmsUser->name}}</td>
                              <td>{{ isset($cmsUser->Restaurant)?$cmsUser->Restaurant->restaurant_name:''}}</td>
<!--                              <td>{{ isset($cmsUser->City)?$cmsUser->City->city_name:''}}</td>-->
<!--                              <td>{{ $cmsUser->mobile }}</td>-->
                              <td>{{ $cmsUser->email}}</td>
                              <td>{{ ($cmsUser->status==1)?'Active':($cmsUser->status==0?'Inactive':'') }}</td>
                              <td nowrap>
                                  @can('user edit')
                                    <a href="{{ URL::to('cms_user/' . $cmsUser->id . '/edit') }}"
                                       class="btn btn__primary">Edit</a> 
                                   @endcan    
                                 
                                 @can('user delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['cms_user.destroy', $cmsUser->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn__primary']) !!}
                                {!! Form::close() !!}
                                @endcan

                                   
                               </td>
                            </tr>
                          
                        @endforeach
                        </tbody>
            </table>
                    @else
                    
                        <div class="row"
                             style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                            No Record Found
                        </div>
                    @endif
                </div>
                @if(isset($cmsUsers) && count($cmsUsers)>0)
                    <div style="margin: 0px auto;">
                        {{ $cmsUsers->appends($_GET)->links()}}
                    </div>
                @endif
            </div>

        </div>
    </div>
    
    <script>
    $(document).ready(function() {
        $('#myTable2').DataTable( { 
            dom:'<"top"fiB><"bottom"trlp><"clear">',
            "order": [[0, "asc" ]],
            buttons: [{
                text: 'Add',
                action: function () {location.href = "cms_user/create";}
            }],
    //        buttons: [
    //            {extend: 'excelHtml5'}, 
    //            {extend: 'pdfHtml5'},   
    //            {               
    //                extend: 'print',
    //                exportOptions: {
    //                    columns: ':visible'
    //                }
    //            },
    //            'colvis'
    //        ],
    //        columnDefs: [ {
    //            targets: -1,
    //            visible: true
    //        }],
        language: {
            search: "",
            searchPlaceholder: "Search"
        },
        responsive: true
        
        } );
        //$('#myTable').show();
        //$('#myTable').removeClass('hidden');
    });
</script>
@endsection