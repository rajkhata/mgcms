@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
    <form method="POST" action="" enctype="multipart/form-data">
    <div class="reservation__atto">
        <div class="reservation__title">
            <h1>Import</h1>
        </div>
          <div class="flex-box flex-direction-row flex-justify-content-end">
                <div class="margin-left-right-5"><a href="javascript:goBack()" class="btn btn__primary font-weight-600">Back</a></div>
              

            </div>
    </div>
    <div>
       

    @if ($errors->any())
        <div class="row messageblock">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{ $error }}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
         
    @endif
    @if (session('status'))
        <div class="row messageblock">
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session('status') }}
            </div>
        </div>
    @endif

        <div class="card form__container ">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            <div class="card-body">
                <div class="form__user__edit">                   
                        @csrf

                         <div class="form-group row form_field__container">
                            <label for="restaurant_id"
                                class="col-md-4 col-form-label text-md-right">{{ __('Import Type') }}</label>
                            <div class="col-md-4">
                                
                               <select class="form-control" name='importtype' required="">
                                   <option value="">--Select--</option>
                                   <option value="menu_item">Menu Item</option>
                                   <option value="menu_categories">Menu Main Categories</option>
                                   <option value="menu_sub_categories">Sub Categories</option>
                                   <option value="menu_meal_types">Meal Type</option>
                               </select>
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                            <label for="restaurant_id"
                                class="col-md-4 col-form-label text-md-right">{{ __('CSV File') }}</label>
                            <div class="col-md-4">
                                <input id="csv" type="file" accept=".csv"  required class="form-control" name="csv">
                               
                            </div>
                        </div>
                        
                </div>
             
               
     
        <div id="menu_settings_data_div"></div>
        <div style="margin-top: 50px;" class="row mb-0">
        <div class="col-md-12 text-center">
        <button type="submit" class="btn btn__primary">{{ __('Import') }}</button>
        </div>
        </div>       
    </div>
</div>
</div>
</div>
 </form>
</div>
<script type="text/javascript">
 function goBack() {
            window.history.go(-1)
        }
</script>
@endsection