@extends('layouts.app')
@section('content')
<link href="{{asset('css/multiple-emails.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/multiple-emails.js')}}" type="text/javascript" charset="utf-8"></script>
<div class="main__container notificationcontainer margin-lr-auto">
    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
            @include('reservation::common.message')
            <ul class="nav nav-pills globel_tabs">
                <li class="working__tab active">
                    <a href="#ordering_setting" data-toggle="tab" aria-expanded="false">Manage Services</a>
                </li>
                <li class="working__tab active">
                    <a href="/services/history">History</a>
                </li>
            </ul>
            <div class="tab-content clearfix working-hours">
                <div class="tab-pane active" id="ordering_setting">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 card--head no-padding">
                            <form  method="post" id = "submit_pause_service_form" action="/configure/restaurantsettings" >
                                @csrf
                                <?php 
                                $pause_service_message = (isset($restInfo) && $restInfo && isset($restInfo['pause_service_message']) && $restInfo['pause_service_message']) ? $restInfo['pause_service_message'] : "We are currently not accepting online orders. We'll be back soon"; 

                                if( ($restInfo && $restInfo['is_food_order_allowed'] == 1 && ($restInfo['food_ordering_takeout_allowed'] == 1 || $restInfo['food_ordering_delivery_allowed'] == 1) ) ||  ($restInfo && $restInfo['is_reservation_allowed'] == 1)){ 

                                ?>
                                <div class="box-shadow res--sett-content">
                                    <?php if(isset($selected_restaurant_id232) && $selected_restaurant_id =='') { ?>
                                    <div class="row form-group col-md-3">
                                        <select name="restaurant_id" id="restaurant_id"
                                        class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}">    
                                         <option value="">Select Restaurant</option>
                                        @foreach($groupRestData as $rest)
                                            <optgroup label="{{ $rest['restaurant_name'] }}">

                                                @foreach($rest['branches'] as $branch)
                                                    <option value="{{ $branch['id'] }}" {{ (isset($selected_restaurant_id) && $selected_restaurant_id ==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}
                                                    </option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                        </select>
                                    </div>
                                    <?php }
                                        $del_class = (isset($restInfo) && isset($restInfo['food_ordering_delivery_allowed']) && $restInfo['food_ordering_delivery_allowed'] == 1 && $restInfo['is_food_order_allowed'] == 1) ? 'block' :'none'; 
                                        $del_active = (isset($restInfo) && isset($restInfo['delivery']) && $restInfo['delivery'] == 1) ? 'active' :''; 
                                        $del_value = (isset($restInfo) && isset($restInfo['delivery']) && $restInfo['delivery'] == 1) ? 1 : 0; 

                                       
                                    ?>

                                
                                    <input type="hidden" value ="<?php echo (isset($restInfo) && isset($restInfo['id']) && $restInfo['id'] ) ? $restInfo['id'] : '' ;?>" name = "restaurant_id">

                                    <input type="hidden" value ="" name = "action" id= "current_action">

                                    <input type="hidden" value ="<?php echo (isset($restInfo) && isset($restInfo['pause_service_message']) && $restInfo['pause_service_message'] ) ? $restInfo['pause_service_message'] : '' ;?>" name = "pause_service_message" id = "pause_service_message">

                                    <div class="row" style="display: <?php echo $del_class; ?> ">
                                        <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Accept Deilvery Orders</div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right">
                                            <div class="waitList__otp no-padding">
                                                <div class="toggle__container">
                                                    <button type="button" id="enable_deilvery_orders_button" class="btn btn-xs btn-secondary btn-toggle change_setting <?php echo $del_active;?>" data-toggle="button" aria-pressed="true" autocomplete="off" rel="delivery">
                                                        <div class="handle"></div>
                                                    </button>
                                                    <input type="hidden" class="enable_deilvery_orders_button" value ="<?php echo $del_value;?>" name = "delivery">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                        $take_class = (isset($restInfo) && isset($restInfo['food_ordering_takeout_allowed']) && $restInfo['food_ordering_takeout_allowed'] == 1 && $restInfo['is_food_order_allowed'] == 1) ? 'block' :'none'; 
                                        $take_active = (isset($restInfo) && isset($restInfo['takeout']) && $restInfo['takeout'] == 1) ? 'active' :''; 
                                        $take_value = (isset($restInfo) && isset($restInfo['takeout']) && $restInfo['takeout'] == 1) ? 1 : 0; 
                                    ?>
                                    <div class="row" style="display: <?php echo $take_class; ?> " >
                                        <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Accept Takeout Orders</div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right">
                                            <div class="waitList__otp no-padding">
                                                <div class="toggle__container">
                                                    <button type="button" id="enable_takeout_orders_button" class="btn btn-xs btn-secondary change_setting btn-toggle <?php echo  $take_active;?>" data-toggle="button" aria-pressed="true" autocomplete="off" rel="takeout">
                                                        <div class="handle"></div>
                                                    </button>
                                                    <input type="hidden" class="enable_takeout_orders_button" value ="<?php echo $take_value;?>" name = "takeout">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <?php 
                                        $res_class = (isset($restInfo) && isset($restInfo['is_reservation_allowed']) && $restInfo['is_reservation_allowed'] == 1  && $restInfo['reservation_type'] == 'full' ) ? 'block' :'none'; 
                                        $res_active = (isset($restInfo) && isset($restInfo['reservation']) && $restInfo['reservation'] == 1) ? 'active' :''; 
                                        $res_value = (isset($restInfo) && isset($restInfo['reservation']) && $restInfo['reservation'] == 1) ? 1 : 0; 
                                    ?>
                                    <div class="row" style="display: <?php echo $res_class; ?> " >
                                        <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Accept Reservation</div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right">
                                            <div class="waitList__otp no-padding">
                                                <div class="toggle__container">
                                                    <button type="button" id="enable_reservation_button" class="btn btn-xs btn-secondary btn-toggle  change_setting <?php echo $res_active;?>" data-toggle="button" aria-pressed="true" autocomplete="off" rel="reservation">
                                                        <div class="handle"></div>
                                                    </button>
                                                    <input type="hidden" class="enable_reservation_button" value ="<?php echo $res_value;?>" name = "reservation">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  <!--   <div class="row setting-save-btn">
                                        <button type='submit' class="form_save">Save</button>
                                    </div> -->
                                </div>
                            <?php } else { ?>
                                <P><?php echo $all_service_message;?></p>
                            <?php }?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Reason Poup-->

<div class="modal fade manage-stop-popup" id="pdf_popup" role="dialog">
    <div class="modal-dialog vertical-center">
        <form id = "modal_text_form">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">


                <h5 class="modal-title">REASON FOR STOPPING SERVICES</h5>
            </div>
            <div class="modal-body">
               
              
                <div class="row text-left edit-res-pop">
                    <div class="col-xs-offset-1 col-xs-12 margin-top-10">
                    <div class="col-xs-12 no-padding">
                    <div class="custom__message-textContainer margin-top-10 no-margin-bottom">
                        <textarea placeholder=" Add service off reason" class="no-padding required" maxlength="100" rows="2" style="height: 100px" id="other_reason" name="text_data"></textarea>
                    </div> </div></div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <div class="col-lg-6 col-xs-6 text-left">
                    <a href="javascript:void(0)" class="btn btn-block btn__primary font-weight-600 submit_reason_btn "  id="cmd">Submit</a>
                </div>
                <div class="col-lg-6 col-xs-6 text-right ">
                    <a href="javascript:void(0)" data-dismiss="modal" class="btn btn-block btn__holo font-weight-600 down-btn " >Cancel</a>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>


<script> 

    var current_slide_class = '';
    var current_action = '';
    $(document.body).on('click','.change_setting',function() {
       current_action = $(this).attr('rel');
        $('#current_action').val(current_action);
        var selected = $(this).hasClass('active');
        if(selected == true) {
            current_slide_class = $(this).attr('id');   
            $('#pdf_popup').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#other_reason').val("<?php echo $pause_service_message;?>");
            // $('.setting-save-btn').hide();
        }else {
           $('#pdf_popup').modal('hide');
           $('#submit_pause_service_form').submit();
           // $('.setting-save-btn').show();
        }
    });

    //BS modal close
    $('#pdf_popup').on('hidden.bs.modal', function () {
        $('#'+current_slide_class).addClass('active');
        $('.'+current_slide_class).val(1);
        // $('.setting-save-btn').show();
    });

    //submit the form   
    $(document.body).on('click','.submit_reason_btn',function() {
        
        if(!$('#modal_text_form').valid()){
            return false;
        }
        var other_reason = $('#other_reason').val();
        $('#pause_service_message').val(other_reason);
        $('#submit_pause_service_form').submit();
    });


    if($(".row.messageblock .alert").length){
        setTimeout(() => {
            $(".row.messageblock .alert .close").trigger('click')    
        }, 5000);        
    }

</script>
@endsection