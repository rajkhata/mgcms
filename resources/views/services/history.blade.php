<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            //setInterval("location.reload(true)", 120000 );
        });
    </script>
    
    <div class="main__container container__custom order-page">
    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <div class="reservation__atto flex-align-item-start">
            <div class="reservation__title margin-top-5">
                <h1>{{$log_data->total()}} Services Logs</h1>
            </div>
            <div class="search__manage-order col-sm-9 col-md-9 no-padding-right">

                <div class="fullWidth text-right hidden-lg hidden-md show-tablet margin-bottom-10">
                    <a href="{{ URL::to('archive_order') }}" class="btn btn__holo font-weight-600">Archive</a>
                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10 export_button" rel = "food_item-active" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a>
                </div>

            {!! Form::open(['method'=>'get']) !!}
            <div class="fullWidth form_field__container flex-justify-content-end">
                <div class="hidden-xs hidden-sm hidden-tablet">
                    <!-- <a href="javascript:void(0)" class="btn btn__holo font-weight-600 export_order" rel = "food_item-active"><i class="fa icon-export" aria-hidden="true"></i> Export</a> -->

                   <!--  <a href="javascript:void(0)" class="btn btn__holo font-weight-600 export_button" rel = "food_item-active" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a> -->

                    <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#pdf_pop">Exportss</button> -->

                </div>


            </div>
            {!! Form::close() !!}
        </div>

        </div>
        <div class="guestbook__container fullWidth box-shadow order-listing">

            <div class="guestbook__table-header fullWidth">
                <div class="row no-padding-left">
                    <div class="col-xs-3 col-sm-3 col-md-3 show-tablet-50 type_of_order no-padding">
                        Orders <!-- Type (Receipt No.) -->
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 show-tablet-50 no-padding">Name</div>
                    <div class="col-xs-3 ol-sm-3 col-md-3 hidden-sm hidden-tablet no-padding">Phone</div>
                    
                </div>
                
                <div class="col-xs-2 col-sm-2 col-lg-1"></div>
            </div>


                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'get']) !!}

                {!! Form::close() !!}


                    @if(isset($log_data) && count($log_data)>0)
                        @foreach($log_data as $oData)
                        <div class="guestbook__cutomer-details-wrapper">

                            <div class="row manage__container guestbook__customer-details-content font-weight-700 text-color-black fullWidth" id="restaurant-order-{{ $oData->id }}">
                                <div class="col-xs-3 col-sm-3 col-md-3 show-tablet-50 type_of_order no-padding-left" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->service_type) }}'"><strong>{{ $oData->service_type}}</strong> <br><div class="text-color-grey font-size-13 font-weight-600">({{ date('Y-m-d',strtotime($oData->effective_date))}})</div></div>
                               
                                <div class="col-xs-6 col-sm-6 col-md-6 show-tablet-50 no-padding-left" >{{ $oData->reason }}
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 show-tablet-50 no-padding-left" >{{ $oData->activity_type }}
                                </div>
                                
                               
                                
                                  
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="text-center col-xs-12 col-sm-12 margin-top-bottom-40" >
                            <strong>No Record Found</strong>
                        </div>
                    @endif


        </div>


    @if(isset($log_data) && count($log_data)>0)
        <div class="text-center" >
            {{ $log_data->appends($_GET)->links()}}
        </div>
    @endif

    </div>
    
@endsection
