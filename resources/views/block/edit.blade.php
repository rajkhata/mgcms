@extends('layouts.app')

@section('content')
<style>
.form__container{max-width: 100%;}
.is_form_type,.block_manager_parent,.create_block_btn{
    display:block;
}
</style>
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
            <h1>{{ __('Edit Static Page Block') }}</h1>
 	</div>

    </div>
    <div>
            <?php 
             $staticBlockArray = $staticBlock->toArray();
	     $id = $staticBlockArray['id'];	
	     $blockData = isset($staticBlockArray['block_data'])?json_decode($staticBlockArray['block_data']):null;	
           //echo"<pre>"; print_r($blockData ); die;
            ?>

            <div class="card form__container">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif ($errors->has('page_err'))
                    <div class="alert alert-danger">
                        {{ $errors->first('page_err') }}
                    </div>
                @endif
                <div class="card-body">
                         {{ Form::model($staticBlock, array('route' => array('block.update', $id), 'method' => 'PUT', 'files' => true)) }}
                            @csrf

                            <div class="form-group row form_field__container">
				<label  class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
				  <div class="col-md-6">
				      <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
				          <option value="">Select Language</option>
				          @foreach($languageData as $lang)
				              <option value="{{ $lang->id }}" {{ ($staticBlock['language_id']==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
				          @endforeach
				      </select>
				      @if ($errors->has('language_id'))
				          <span class="invalid-feedback">
				  		<strong>{{ $errors->first('language_id') }}</strong>
					</span>
				      @endif
				  </div>
                                <label for="restaurant_id"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                                <div class="col-md-4">
                                    <select name="restaurant_id" id="restaurant_id"
                                            class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                                            required1>
                                        <option value="">Select Restaurant</option>
                                        @foreach($groupRestData as $rest)
                                            <optgroup label="{{ $rest['restaurant_name'] }}">
                                                @foreach($rest['branches'] as $branch)
                                                    <option value="{{ $branch['id'] }}" {{ ($staticBlock['restaurant_id']==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('restaurant_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                    <label for="block_type"
                                       class="col-md-2 col-form-label text-md-right">Block Type</label>
                                    <div  class="col-md-4">
                                            
                                     <select class="form-control block_type" id="block_type" name="block_type" required1="">
                                             <option value="">Select Block Type</option>
                                        @foreach($block_type as $key=>$val)
                                             <option value="{{$key}}" @if($staticBlock['block_type']==$key) selected @endif>{{$val}}</option>    
                                        @endforeach  
                                        
                                         </select>
                                    </div>
                            </div>
                            <div class="block_manager_parent" >

                            <div class="form-group row">
                                <div class="col-xs-12 form__wrapper">
                                    <div class="block block__header well">
                                        <div class="row">
                                            <div class="col-xs-6 form-inline">
                                                <div class="form-group ">
                                                    <label for="block_title">Block Title</label>
                                                    <input type="text" name="block_title"
                                                           class="form-control block__title" id="block_title"
                                                           placeholder="Title" required1="" value="{{ $staticBlock['block_title'] }}">
                                                    	@if ($errors->has('block_title'))
                                                        	<span class="invalid-feedback"><strong>{{ $errors->first('block_title') }}</strong></span>
                                                    	@endif
                                                </div>
                                            

                                               
                                            </div>

                                            <div class="col-xs-3 pull-right">
                                                <div class="btn-group blockBtn__container pull-right" role="group" aria-label="...">
                                                    <button type="button" class="btn btn__primary" id="normal_add_field"> Add Field </button>
                                                    <button type="button" class="btn btn__primary" id="add_group">Add Group </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="normal_field_type_block">
						@if (isset($blockData->normal))

                                 		<table class="table table-stripped table-bordered fields_table" id="normal_fields_table"> 
						<thead> <tr> <th>Type</th> <th>Value</th> <th>Attributes</th> <th>Action</th> </tr> </thead> 
						<tbody> <?php $nFi= 0;?>
						@foreach($blockData->normal as $normalField)
						<tr> 
									<td> 
									<select class="form-control field_type" name="normal_field_type[{{$nFi}}]">  
									@foreach($fields_types as $key=>$val) 
										<option value="{{$key}}" @if($normalField->type==$key) selected @endif>{{$val}}</option>
									@endforeach 
									</select> 
									</td>
									@if($normalField->type=="editor")		                                     
									<td>
									 <textarea required1="" name="normal_field_value[{{$nFi}}]"  class="form-control field_type_val"
									 placeholder="Value" id="normal_field_value[{{$nFi}}]" aria-hidden="false"> 
									 @if(isset($normalField->value)) {{$normalField->value}} @endif 
									 </textarea>
									 
									</td> 
									@elseif($normalField->type=="file")		                                     
									<td> 
									 	<input type="file" required1="" value="" name="file_normal_field_value[{{$nFi}}]" 
										class="form-control field_type_val" placeholder="Value"><br/><br/>
										<input type="hidden" name="file_normal_field_hidden[{{$nFi}}]" class="form-control " 
									placeholder="Value" value="@if(isset($normalField->value)) {{$normalField->value}} @endif"> 
									</td> 
									@elseif($normalField->type=="textarea" || $normalField->type=='select' || $normalField->type=='multi-select' 
									|| $normalField->type=='radio' || $normalField->type=='checkbox')    <td> 
										<textarea required1="" name="normal_field_value[{{$nFi}}]" class="form-control field_type_val" placeholder="Value">
										@if(isset($normalField->value)) {{$normalField->value}} @endif
										</textarea>
									</td> 
									@else	                                     
									<td><input type="text" name="normal_field_value[{{$nFi}}]" class="form-control field_type_val" 
									placeholder="Value" value="@if(isset($normalField->value)) {{$normalField->value}} @endif">
									</td>  
									@endif

							  
									<td> <p> <input type="text" required1="" value="{{$normalField->key}}" name="normal_field_key[{{$nFi}}]" 
										class="form-control key" placeholder="Key"> </p>
									<div class="is_form_type" style="display:@if(!empty($normalField->placeholder)) block @else none @endif">  <p> 
									<input type="text" name="normal_field_label[{{$nFi}}]" class="form-control label"  
									value="{{$normalField->placeholder}}" placeholder="Label"> </p> 
									<p> <input type="text" name="normal_field_placeholder[{{$nFi}}]" value="{{$normalField->placeholder}}" 
									class="form-control placeholder" placeholder="Placeholder"> </p> 
									<p> <input type="checkbox" name="normal_field_is_required[{{$nFi}}]" class="is_required1" value="1" 
									placeholder="Is Required">Is Required </p> 
									</div></td>   
									<td> <button type="button" class="btn btn-danger field_delete">Delete</button> </td> 
						</tr> <?php $nFi++;?>
						@endforeach
						</tbody></table> 
						
						@endif
                                    </div>

                                    <div class="full_group__container" > <?php $i=0;?> 
						@if (isset($blockData->group))
						       @foreach($blockData->group as $title=>$groupArray) 
							<div class="block well group__container" id="group_{{$i}}">                     
							<span class="glyphicon glyphicon-remove text-danger pull-right delete_group_btn"></span>
							<div class="group_fields" id="group_{{$i}}_element_{{($i+1)}}">                         
								<div class="col-xs-6 form-inline">                             
								   <div class="form-group block">                                 
									<label for="block_title">Group Title</label>                                 
									<input type="text" name="group_title[{{$i}}]" class="form-control block__title group_title" 
									placeholder="Title" required1="" value="{{$title}}">                             
								   </div>                         
								</div>
 				                            								                         
								<div class="group__toolbar">                             
								  <div style="margin-bottom:20px" class="btn-group blockBtn__container pull-right" role="group"
									 aria-label="...">                                 
									<button type="button" class="btn btn__primary group_add_field">Add Field</button>                                 
									<!--button type="button" class="btn btn__primary group_repeat">Repeat Elements</button-->                             
								  </div>                         
								</div> 
								<div class="block group__fields-wrapper">                             
								  <table class="table table-stripped table-bordered bm0 fields_table group_fields_table">                                 
								  <thead><tr><th>Type</th><th>Value</th><th>Attributes</th><th>Action</th></tr></thead>                                 
								  <tbody>
						                @foreach($groupArray as $index=>$groupArrayData1)    
								 <?php $j=0;?> 
								 
								@foreach($groupArrayData1 as $keyIndex=>$groupArrayData)            
								                                
									<tr><td>
										<select class="form-control group_field_type" name="group_field[{{$i}}][type][{{$j}}]">  
									  		@foreach($fields_types as $key=>$val) 
											 <option value="{{$key}}" @if(isset($groupArrayData->type) && $groupArrayData->type==$key) selected @endif>{{$val}}</option>
											@endforeach 
										</select>
									</td>
									@if(isset($groupArrayData->type) && $groupArrayData->type=="editor")		                                     
									<td>
									<textarea required1="" name="group_field[{{$i}}][value][{{$j}}]" class="form-control group_field_type_val editor"
									 placeholder="Value" id="group_field[{{$i}}][value][{{$j}}]" aria-hidden="false"> 
									@if(isset($groupArrayData->value)) {{$groupArrayData->value}} @endif 
									</textarea>
									</td> 
									@elseif(isset($groupArrayData->type) && $groupArrayData->type=="file")		                                     
									<td> 
									 <input type="file" required1="" value="" name="file_group_field[{{$i}}][value][{{$j}}]" 
										class="form-control group_field_type_val " placeholder="Value"><br/><br/>
										<input type="hidden" name="file_group_field_hidden[{{$i}}][value][{{$j}}]" class="form-control " 
									placeholder="Value" value="@if(isset($groupArrayData->value)) {{$groupArrayData->value}} @endif">
									</td>
									 
									 
									@elseif(isset($groupArrayData->type) && ($groupArrayData->type=="textarea" || $groupArrayData->type=='select' || $groupArrayData->type=='multi-select' || $groupArrayData->type=='radio' || $groupArrayData->type=='checkbox'))    <td> 
									<textarea required1="" name="group_field[{{$i}}][value][{{$j}}]" class="form-control group_field_type_val " placeholder="Value">
									@if(isset($groupArrayData->value)) {{$groupArrayData->value}} @endif
									</textarea>
									</td> 
									@else	                                     
									<td><input type="text" name="group_field[{{$i}}][value][{{$j}}]" class="form-control group_field_type_val" 
									placeholder="Value" value="@if(isset($groupArrayData->value)) {{$groupArrayData->value}} @endif">
									</td>  
									@endif
								                                           
									<td><p><input type="text" name="group_field[{{$i}}][key][{{$j}}]" class="form-control key" placeholder="Key" 
									value="@if(isset($groupArrayData->key)) {{$groupArrayData->key}} @endif"></p>
									<div class="is_form_type" style="display:@if(!empty($groupArrayData->placeholder) && $groupArrayData->placeholder!='') block @else none @endif">  
									<p> <input type="text" value="@if(isset($groupArrayData->label))  {{$groupArrayData->label}} @endif" name="group_field[{{$i}}][label][{{$j}}]" 
									class="form-control label" placeholder="Label"> </p> 
									<p> <input type="text" name="group_field[{{$i}}][placeholder][{{$j}}]" class="form-control placeholder" 
									value="@if(isset($groupArrayData->label)) {{$groupArrayData->placeholder}} @endif"placeholder="Placeholder"> </p> 
									<p> <input type="checkbox" name="group_field[{{$i}}][is_required][{{$j}}]" placeholder="Is Required" class="is_required1" value="1">
									Is Required </p> 
									</div></td>                                     
									<td><button type="button" class="btn btn-danger field_delete">Delete</button></td>                                 
									</tr>                                 
								  
							  	<?php $j++;?>@endforeach 
								<?php $i++;?> @endforeach    
								</tbody>                             
								  </table>                         
								</div>	 
								                 
							</div>                 
							</div>
						@endforeach
						@endif
                                    </div>

                                </div>

                            </div>

                            <div class="form-group row mb-0 create_block_btn" >
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn__primary">{{ __('Update Block') }}</button>
                                </div>
                            </div>

                           </div>
                        </form>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
       
        $(function () {
            function submitBtn(){
                if($('#normal_fields_table').length || $('.group__container').length){
                    $('.create_block_btn').show();
                    //alert('show');
                }else{
                    $('.create_block_btn').hide();
                }
                
            }

            $(document).on('change','#restaurant_id,#block_type', function () {
                var restaurant_id=$("#restaurant_id").val();
                var block_type=$("#block_type").val();
                if(restaurant_id!='' && block_type !=''){
                    $('.block_manager_parent').show();
                }else{
                    $('.block_manager_parent').hide();
                }
                if(block_type=='Form'){
                   $('.is_form_type').show(); 
                }else{
                    $('.is_form_type').hide(); 
                }
               // $('.create_block_btn').hide(); 
               submitBtn();
            });

            $('#normal_add_field').on('click', function () {

                var block_type=$("#block_type").val();
                var  formblocktype='style="display:block"'; 
             
                if(block_type!='Form'){                    
                    formblocktype='style="display:none"';                   
                }
                //console.log(formblocktype);

                if($('.normal_field_type_block').find('#normal_fields_table').length){
                    var field_type_vallen=  $('#normal_fields_table').find('.field_type_val').length + 1;
                    var normal_field_value=(field_type_vallen)+'_normal_field_value';
                    var field_row = ' <tr> <td> <select class="form-control field_type" name="normal_field_type['+field_type_vallen+']"> @foreach($fields_types as $key=>$val) <option value="{{$key}}">{{$val}}</option> @endforeach </select> </td> <td> <input type="text" required1="" name="normal_field_value['+field_type_vallen+']" class="form-control field_type_val" placeholder="Value"> </td> <td> <p> <input type="text" required1 name="normal_field_key['+field_type_vallen+']" class="form-control key" placeholder="Key"> </p><input type="hidden" name="normal_formblocktype['+field_type_vallen+']" value="'+formblocktype+'"> <div class="is_form_type"  '+formblocktype+'><p> <input type="text" name="normal_field_label[]" class="form-control label" placeholder="Label"> </p> <p> <input type="text" name="normal_field_placeholder['+field_type_vallen+']" class="form-control placeholder" placeholder="Placeholder"> </p> <p> <input type="checkbox" name="normal_field_is_required1['+field_type_vallen+']" class="is_required1" value="1" placeholder="Is Required">Is Required </p> </div>  </td> <td> <button type="button"  class="btn btn-danger field_delete">Delete </button> </td> </tr>';

                    $(field_row).appendTo('#normal_fields_table tbody');

                }else{
		    var normal_field_type_index=  $('#normal_fields_table').find('.field_type_val').length;	
                    var tablenormal = '<table class="table table-stripped table-bordered fields_table" id="normal_fields_table"> <thead> <tr> <th>Type</th> <th>Value</th> <th>Attributes</th> <th>Action</th> </tr> </thead> <tbody> <tr> <td> <select class="form-control field_type" name="normal_field_type['+normal_field_type_index+']"> @foreach($fields_types as $key=>$val) <option value="{{$key}}">{{$val}}</option> @endforeach </select> </td> <td> <input type="text" required1="" name="normal_field_value['+normal_field_type_index+']" class="form-control field_type_val" placeholder="Value"> </td> <td> <p> <input type="text" required1 name="normal_field_key['+normal_field_type_index+']" class="form-control key" placeholder="Key"> <input type="hidden" name="normal_formblocktype['+normal_field_type_index+']" value="'+formblocktype+'"></p><div class="is_form_type"  '+formblocktype+'>  <p> <input type="text" name="normal_field_label['+normal_field_type_index+']" class="form-control label" placeholder="Label"> </p> <p> <input type="text" name="normal_field_placeholder['+normal_field_type_index+']" class="form-control placeholder" placeholder="Placeholder"> </p> <p> <input type="checkbox" name="normal_field_is_required1['+normal_field_type_index+']"  class="is_required1" value="1" placeholder="Is Required">Is Required </p> </div> </td>  <td> <button type="button"  class="btn btn-danger field_delete">Delete </button> </td> </tr> </tbody> </table>';

                    $('.normal_field_type_block').html(tablenormal);

                }
                submitBtn();

            });
            $('#add_group').on('click', function () {
                var block_type=$("#block_type").val();
                var  formblocktype='style="display:block"'; 
             
                if(block_type!='Form'){                    
                    formblocktype='style="display:none"';                   
                }
                var group_block_index = $('div.group__container').length + 1;
		var group_field_value_index = $('div.group__container').length + 1;
		var group_type_vallen=  $('.group__fields-wrapper').find('.group_field_type_val').length + 1;
		if(group_block_index==1)group_block_index=0;	
		if(group_type_vallen==1)group_type_vallen=0;
                //var group_field_type_val=(group_block_index)+'_group_field_val_1_0';
                var new_group_html = '<div class="block well group__container" id="group_' + group_block_index + '">' +
                    '                     <span class="glyphicon glyphicon-remove text-danger pull-right delete_group_btn"></span><div class="group_fields" id="group_' + group_block_index + '_element_'+ (group_block_index + 1) +'">' +
                    '                         <div class="col-xs-6 form-inline">' +
                    '                             <div class="form-group block">' +
                    '                                 <label for="block_title">Group Title</label>' +
                    '                                 <input type="text" name="group_title['+ group_block_index +']" class="form-control block__title group_title" placeholder="Title" required1>' +
                    '                             </div>' +
                    '                         </div>' +
                    '                         <div class="group__toolbar">' +
                    '                             <div style="margin-bottom:20px" class="btn-group blockBtn__container pull-right" role="group" aria-label="...">' +
                    '                                 <button type="button" class="btn btn__primary group_add_field">Add Field</button>' +
                    '                                 <!--button type="button" class="btn btn__primary group_repeat">Repeat Elements</button-->' +
                    '                             </div>' +
                    '                         </div>' +
                    '                         <div class="block group__fields-wrapper">' +
                    '                             <table class="table table-stripped table-bordered bm0 fields_table group_fields_table">' +
                    '                                 <thead><tr><th>Type</th><th>Value</th><th>Attributes</th><th>Action</th></tr></thead>' +
                    '                                 <tbody>' +
                    '                                 <tr>' +
                    '                                     <td><select class="form-control group_field_type" name="group_field[' + group_block_index + '][type]['+group_type_vallen+']"> @foreach($fields_types as $key=>$val) <option value="{{$key}}">{{$val}}</option> @endforeach </select></td>' +
                       '                                     <td><input type="text" name="group_field[' + group_block_index + '][value]['+group_type_vallen+']" class="form-control group_field_type_val" placeholder="Value"></td>' +

                    '                                     <td><p><input type="text" name="group_field[' + group_block_index + '][key]['+group_type_vallen+']" class="form-control key" placeholder="Key"></p><div class="is_form_type" '+formblocktype+'>  <p> <input type="hidden" name="group_formblocktype['+group_type_vallen+']" value="'+formblocktype+'"> <input type="text" name="group_field[' + group_block_index + '][label]['+group_type_vallen+']" class="form-control label" placeholder="Label"> </p> <p> <input type="text" name="group_field[' + group_block_index + '][placeholder]['+group_type_vallen+']"class="form-control placeholder" placeholder="Placeholder"> </p> <p> <input type="checkbox" name="group_field[' + group_block_index + '][is_required11]['+group_type_vallen+']" placeholder="Is Required" class="is_required1" value="1">Is Required </p> </div></td>' +
                    '                                     <td><button type="button" class="btn btn-danger field_delete" >Delete</button></td>' +
                    '                                 </tr>' +
                    '                                 </tbody>' +
                    '                             </table>' +
                    '                         </div>' +
                    '                     </div>' +
                    '                 </div>';
                    $('.full_group__container').find('.group__container').length? $('.full_group__container').find('.group__container:last').after(new_group_html):$('.full_group__container').html(new_group_html);
                    submitBtn();
            });

            $('div.form__wrapper').on('click', '.group_add_field', function () {
                var block_type=$("#block_type").val();
                var  formblocktype='style="display:block"'; 
             
                if(block_type!='Form'){                    
                    formblocktype='style="display:none"';                   
                }

                //console.log('add field clicked');
                var group_block_index = $(this).parents('div.group__container').attr('id').split('_').pop();
                var group_element_index = $(this).parents('div.group_fields').attr('id').split('_').pop();
                var closest_tbody = $(this).parents('div.group_fields').find('table.group_fields_table tbody');console.log(closest_tbody);
                var trCount = $(closest_tbody).find('tr').length;
                
		   
		var group_field_value_index = $('div.group__container').length + 1;
		var group_type_vallen=  $('.group__fields-wrapper').find('.group_field_type_val').length + 1;
		 	
		if(group_type_vallen==1)group_type_vallen=0;
                var field_row = '<tr><td>' +
                    '<select class="form-control group_field_type" name="group_field[' + group_block_index + '][type]['+group_type_vallen+']"> @foreach($fields_types as $key=>$val) <option value="{{$key}}">{{$val}}</option> @endforeach </select>' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="group_field[' + group_block_index + '][value]['+group_type_vallen+']" class="form-control group_field_type_val" placeholder="Value">' +
                    '</td>' +
                    '<td>' +
                    '<p><input type="text" name="group_field[' + group_block_index + '][key]['+group_type_vallen+']" class="form-control key" placeholder="Key"></p><div class="is_form_type" '+formblocktype+' >  <p> <input type="text" name="group_field[' + group_block_index + '][label]['+group_type_vallen+']" class="form-control label" placeholder="Label"> </p> <p> <input type="text" name="group_field[' + group_block_index + '][placeholder]['+group_type_vallen+']"class="form-control placeholder" placeholder="Placeholder"> </p> <p> <input type="checkbox" name="group_field[' + group_block_index + '][is_required1]['+group_type_vallen+']" placeholder="Is Required" class="is_required1"  value="1">Is Required </p> </div>' +
                    '</td>' +
                    '<td>' +
                    '<button type="button" class="btn btn-danger field_delete" >Delete</button>' +
                    '</td></tr>';

                //$(field_row).appendTo('.group_fields_table tbody');
                $(closest_tbody).append(field_row);
                submitBtn();
            });    
            
                 // repeat group elements
            $('div.form__wrapper').on('click', '.group_repeat', function () { /*
               
                var block_type=$("#block_type").val();
                var  formblocktype='style="display:block"'; 
             
                if(block_type!='Form'){                    
                    formblocktype='style="display:none"';                   
                }
                var group_block_id = $(this).parents('div.group__container').attr('id');
                var group_block_index = group_block_id.split('_').pop();
                var group_element_index = $('#' + group_block_id).find('.group_fields').length + 1;
                var currentparentgroup='group_' + group_block_index + '_element_' + group_element_index;


                var group_html = '<div class="group_fields" id="group_' + group_block_index + '_element_' + group_element_index + '">' +
                    '    <div class="col-xs-6 form-inline">' +
                    '    </div>' +
                    '    <div class="block group__fields-wrapper">' +
                    '        <table class="table table-stripped table-bordered bm0 fields_table">' +
                    '            <thead><tr><th>Type</th><th>Value</th><th>Attributes</th><th>Action</th></tr></thead>' +
                    '            <tbody>';
                var field_type_vallen=  $(this).parents('.group__container').find('.group_field_type_val').length;

                $('#' + group_block_id + '_element_1').find('.group_fields_table').find('tbody tr').each(function (k,v) {   
                    var fieldtypeoptions = $(this).find('select > option');
                    var option='';
                    var currentselect_val='';
                    $(fieldtypeoptions).each(function(key,element){
                        var selected=element.selected?'selected':'';
                        currentselect_val=element.selected?element.value:'';
                        option+='<option value="'+element.value+'"   '+selected+'>'+element.text+'</option>';;
                         
                    })
                    var keyval = $(this).find('.key').val();
                    var label='';
                    var placeholder='';
                    var is_required1='';
                    if(block_type=='Form'){  
                          label=$(this).find('.label').val();
                          placeholder=$(this).find('.placeholder').val();
                          if($(this).find('.is_required1').is(":checked")){
                            is_required1='checked';
                          }
                          
                    }


                  var elementtype=$(this).find('select').val();
                    var group_key_value = $(this).find('td:first-child input').val();
                    var group_field_type_val=(group_block_index)+'_group_field_val_'+group_element_index+'_'+(k);

                    var  elementhtml=setElementType($(this).find('.group_field_type'),'group_field_type_val',elementtype,group_field_type_val);

                    group_html += '<tr>' +
                        '                <td><select class="form-control group_field_type" name="group_field[' + group_block_index + '][type][]">'+option+'</select></td>' +

                        '                <td>'+elementhtml+'</td>' +
                        '                <td><p><input type="text" name="group_field[' + group_block_index + '][key][]" class="form-control key" placeholder="Key" value="' + keyval + '"></p><div class="is_form_type" '+formblocktype+' >  <p><input type="hidden" name="normal_formblocktype[]" value="'+formblocktype+'">  <input value="'+label+'" type="text" name="group_field[' + group_block_index + '][label][]" class="form-control label" placeholder="Label"> </p> <p> <input value="'+placeholder+'"  type="text" name="group_field[' + group_block_index + '][placeholder][]"class="form-control placeholder" placeholder="Placeholder"> </p> <p> <input type="checkbox" name="group_field[' + group_block_index + '][is_required1][]" placeholder="Is Required" class="is_required1"  '+is_required1+'   value="1">Is Required </p> </div></td>' +
                        '                <td><button type="button" class="btn btn-danger field_delete" >Delete</button></td>' +
                        '              </tr>';
                });

                group_html += '</tbody>' +
                    '        </table>' +
                    '    </div>' +
                    '</div>';

                $('#' + group_block_id).append(group_html);
               //$('#' + currentparentgroup).find('select').val(fieldtype);
              // alert( $('#' + currentparentgroup).find('select'));*/
            });

            $(document).on('click','.delete_group_btn', function () {
                $(this).parents('.group__container').remove();
                submitBtn();
            });


            function setElementType(curel,elementclass,type,nameelement){
		//alert(type)

               // var placeholder=curel.parents('tr').find('.'+elementclass).attr('placeholder');
                var placeholder='Value';
                var name=curel.parents('tr').find('.'+elementclass).attr('name');
                var classes=curel.parents('tr').find('.'+elementclass).attr('class');
                var val=curel.parents('tr').find('.'+elementclass).val();
                console.error(val);

                var parentofit= curel.parents('tr').find('.'+elementclass).parent('td');
                var elementhtml='';
                classes=classes.split('editor')[0];
                if(type=='textarea'){
                    elementhtml='<textarea  required1 name="'+name+'" class="'+classes+'" placeholder="'+placeholder+'">'+val+'</textarea>';
                }else if(type=='editor'){
                    elementhtml='<textarea  required1 name="'+name+'" class="'+classes+' editor" placeholder="'+placeholder+'">'+val+'</textarea>';

                }else if(type=='select' || type=='multi-select' || type=='radio' || type=='checkbox'){
                    placeholder='key:val,key:val';
                    elementhtml='<textarea  required1 name="'+name+'" class="'+classes+'" placeholder="'+placeholder+'">'+val+'</textarea>';

                }
                else if(type=='hidden' || type=='price' ){
                    elementhtml='<input type="text" required1="" value="'+val+'" name="'+name+'" class="'+classes+'" placeholder="'+placeholder+'">';
                    //curel.parents('tr').find('.'+elementclass).attr('type','text'); 

                }else if(type=='quantity'){
                    //curel.parents('tr').find('.'+elementclass).attr('type','number');  
                    elementhtml='<input type="number" required1=""  value="'+val+'" name="'+name+'" class="'+classes+'" placeholder="'+placeholder+'">';

                }else{
                   // curel.parents('tr').find('.'+elementclass).attr('type',type);  file
                    elementhtml='<input type="'+type+'" required1=""  value="'+val+'" name="file_'+name+'" class="'+classes+'" placeholder="'+placeholder+'">';
                }
                if(nameelement!=undefined){
                  return elementhtml
                }else{
                    parentofit.html(elementhtml);  
                }
                        
            
            }
      

            $(document).on('change','.field_type', function () {
                var type=$(this).val().toLowerCase();                
                setElementType($(this),'field_type_val',type);
                getEditor();     
            });

            $(document).on('change','.group_field_type', function () {
                var type=$(this).val().toLowerCase();
                //$(this).parents('tr').find('.group_field_type_val').attr('type',type);
                setElementType($(this),'group_field_type_val',type);
                getEditor();               
            });

            // add group fields, key value
           
            
            $(document).on('click', '.field_delete', function () {

               var trcount= $(this).parents('tbody').find('tr').length;
               if(trcount==1){
                var repeaterlen=$(this).parents('.group__container').find('.group_fields').length;
                $(this).parents('.group_fields').remove();
               }
                $(this).parents('tr').remove();
            });
             

        });

       
    </script>
<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
getEditor();
    function getEditor(){
        tinymce.init({
        selector: 'textarea.editor',
        height: 320,
        theme: 'modern',
        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],

        });

    }

</script>
@endsection

