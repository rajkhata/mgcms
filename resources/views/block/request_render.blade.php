<div class="booking_container tab-content clearfix box-shadow margin-top-0">
    <div class="booking_content tab-pane active no-margin">
       
        <table id="table_id" class="responsive ui cell-border hover"  style=" margin-bottom:10px;" width="100%">
            @if($reservations->count())
            <thead>
                <tr>
                    <th>Status</th>
                    @if($prestId == 0)
                    <th class="hide">Restaurant</th>
                    @endif
                    <th>Received On</th> 

		    @if($enquiry_type == 'contact')<th>Bar Name</th>@endif
                    @if($enquiry_type != 'subscriptions' && $enquiry_type != 'deliverynotfound'  && $enquiry_type != 'contact') <th>Name</th>@endif
                    @if($enquiry_type != 'contact')<th>Email</th>@endif
		    @if($enquiry_type == 'contact')<th>Address</th>@endif
                    @if($enquiry_type != 'subscriptions' && $enquiry_type != 'deliverynotfound' && $enquiry_type != 'contact')<th>Phone</th>@endif
		    @if($enquiry_type == 'deliverynotfound')<th>Postcode</th>@endif
		    @if($enquiry_type == 'contact')<th>Contact Name</th>@endif
        </tr>
            </thead>
            @endif
            <tbody>

                @foreach($reservations as $reser)
                <?php $reservation = json_decode($reser->response); ?>
                <tr class="catering_detail_enquiry" data-toggle="modal" data-target="#request_detail" id="resv_{{$reser->id}}">
                    <td>

                        @if($reser->stage==0)
                            <span title="New" class="newMSG icon-enquiry-new"><span class="hide">New</span></span></span>
                        @elseif($reser->stage==1)
                        <span title="Read" class="readMSG icon-enquiry-read"><span class="hide">Read</span></span>
                        @elseif($reser->stage==2)
                        <span title="Reply" class="repliedMSG icon-enquiry-reply"><span class="hide">Reply</span></span>
                        @else
                        <span title="Forward" class="forwardMSG icon-enquiry-forward"><span class="hide">Forward</span></span>
                        @endif

                    </td>

                    @if($prestId == 0)
                    <td class="hide">{{ isset($reser->restaurant->restaurant_name) ? $reser->restaurant->restaurant_name : 'N/A' }}</td>
                    @endif

                    <td data-order="{{ strtotime($reser->created_at)}}">{{ date('D, M d', strtotime($reser->created_at)) }}</td>

                    <!--<td data-order="{{ strtotime($reser->created_at)}}">
                    <?php if (isset($reservation->date_of_occasion)) { ?>
                                <strong>{{date('D, M d', strtotime($reservation->date_of_occasion))}}</strong>
                    <?php } else if (isset($reservation->date_of_event)) { ?>
                                <strong>{{date('D, M d', strtotime($reservation->date_of_event))}}</strong>
                    <?php } ?>
                    </td>-->
		@if($enquiry_type != 'subscriptions' && $enquiry_type != 'deliverynotfound')

                    <td>
                        <?php
                        $name = '';
                        if (isset($reservation->fName)) {
                            $name = $reservation->fName;
                            $name = isset($reservation->lname) ? $name . ' ' . $reservation->lname : $name;
                        } elseif (isset($reservation->name)) {
                            $name = $reservation->name;
                        } elseif (isset($reservation->first_name)) {
                            $name = $reservation->first_name;
                            $name = isset($reservation->last_name) ? $name . ' ' . $reservation->last_name : $name;
                        }
                       echo $name;
                        

                        
                        ?>

                    </td> @endif
		@if( $enquiry_type != 'contact')	
                    <td>
                        <?php
                        if (!empty($reservation->email)) {
                            echo "<i class='fa fa-envelope margin-right-5'></i><span>" . $reservation->email . "</span>";
                        }
                        ?>
                    </td> @endif

 			@if( $enquiry_type == 'contact')
			<td>
			<?php  echo isset($reservation->address)?$reservation->address:'';?>
			</td> @endif
			@if($enquiry_type != 'subscriptions' && $enquiry_type != 'deliverynotfound' && $enquiry_type != 'contact')

                    <td>
                        <?php
                       if (!empty($reservation->phone)) {
                            echo "<span class='phico'>" . $reservation->phone . "</span>";
                        } 
                        ?>
                    </td> @endif
                      @if( $enquiry_type == 'deliverynotfound')
			<td>
			<?php  echo isset($reservation->zipcode)?$reservation->zipcode:'';?>
			</td> @endif
		      @if( $enquiry_type == 'contact')
			<td>
			<?php  echo isset($reservation->contact_name)?$reservation->contact_name:'';?>
			</td> @endif
                </tr>		

                @endforeach
                @if($reservations->count()==0)
                <tr>
                    <td colspan="4" class="text-center col-sm-12 margin-top-bottom-40">
                        <strong>No Record Found</strong>
                    </td> </tr>
                @endif
            </tbody>
        </table> 

    </div>
</div>
<div class="pagination__wrapper paginationstyle">
    {{--{{ $reservations->links() }}--}}
    {{ $reservations->links('pagination.default') }}
</div>


<script type="text/javascript">
    $('div[data-toggle="popover"]').popover();
    $(document).off("click.fb-start", "[data-trigger]");
    $(document).ready(function () {
        $('#table_id').DataTable({
            dom: '<"top"fiB><"bottom"trlp><"clear">',
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye-slash"></i>',
                    titleAttr: 'Column Visibility'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    exportOptions: {
                        columns: ':visible',
                    },
                }
            ],

            columnDefs: [
                //{ targets:[-1], visible: false },
                { targets:[0,1,2,3,4,5], className: "desktop" },
                { targets:[0,1,2,3,5], className: "tablet" },
                { targets:[0,1], className: "mobile" }
            ],

            language: {
                search: "",
                searchPlaceholder: "Search"
            },
            responsive: true
        });
    });
</script>
