<div class="modal right autoListingPopup fade" id="request_detail" tabindex="-1" role="dialog"
     aria-labelledby="reservation-enquiry-detailsModalLabel" aria-hidden="true" data-backdrop="static"  style="z-index:49!important">
    <style type="text/css">
     .modal-backdrop { z-index:48!important}
    </style>
    <div class="modal-dialog" role="document">

        <div class="modal-content scroller full-content cateringformdetails">
            <div class="modal-header">
                <div class="closeDiv padding-left-right-0">
                    <a href="javascript:;" class="slideClose" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="right--popup-wrapper">

                <div class="reservation-details__header">

                    <div class="row clearfix">
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 nopad">
                            <section class="delBlockqoute width-auto">
                                <div class="delBlockqoute-top">
                                    <!-- <a href="javascript:;" class="showmob slideClose closeCta" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-close" aria-hidden="true"></i> Close
                                    </a> -->
                                    <span></span>
                                </div>
                               
                            </section>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 nopad">
                            <section class="delBlockqoute-right">                                
                                <p><strong>Received On:</strong> <span id="recevedOn" class=""></span></p>
                            </section>
                        </div>
                    </div>

                    
                    <input type="hidden" id="enquiry_resv_id_title" class="enquiry_resv_id_title" value="">


                <!-- edit reservation details -->
                <div class="border-sepration fullWidth"></div>
                <div id="userContectDetail">
                    <div class="form_field__container fullWidth margin-top-20 padding-left-right-0">

                    <div class="row clearfix">
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 norel nopad">
                            <section class="addressBlockqoute">

                                <div class="enquiry_fname"></div>


                                <div><strong style="color:#9a9a9a" class=""></strong></div>

                            </section>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 norel nopad">
                            <section class="addressBlockqoute tar">
                                {{--<div class="ordernum"><strong><a href="/mng_order/109/usrord" target="_blank">17 Order(s)</a></strong></div>--}}
                                <div class="enquiry_mobile">

                                </div>
                                <div>
                                    <span id="emailActionReply">
                                        <i class="fa fa-envelope margin-right-5" aria-hidden="true"></i>
                                        <span class="enquiry_email"></span>
                                    </span>
                                </div>
                            </section>
                        </div>
                    </div>



                    </div>
                    <div class="border-sepration fullWidth"></div>
                </div>

                    <div class="form_field__container fullWidth">
                    <!--extra field-->
                        <input type="hidden" id="user_id" value="" >
                        <div id="extra_field" class="fullWidth margin-top-20 margin-bottom-30"></div>

                    </div>


                <div class="border-sepration fullWidth"></div>

                  <div class="form_field__container fullWidth  margin-top-20  margin-bottom-30">
                      <button type="button" name="btnreply" id="reply" class="btn btn__primary "><img src="../images/reply-arrow-icon.png" alt="Reply" style="max-width: 16px; vertical-align: top;"> Reply</button>
                      <button type="button" name="btnforward" id="forward" class="btn btn__primary margin-left-15">Forward <img src="../images/forward-arrow-icon.png" alt="Forward" style="max-width: 16px; vertical-align: top;"></button>

                      <div id="enquiry_error" class="margin-top-20"></div>
                      <div id="successsend" class="margin-top-20"></div>
                  </div>

                  <div class="form_field__container fullWidth  margin-top-20 emailToggleBox">
                      <input type="hidden" value="" id="repFor">
                          
                          <input type="hidden" value="" id="enquirytype">
                          <input type="hidden" value="" id="loginusername">
                          <input type="hidden" value="" id="loginuseremail">
                      <div class="input__field fullWidth relative" id="from">                          

                          <span class="position-relative">From :</span>  <span class="position-relative" id="loginuser"></span>                          
                      </div>
                      <div class="input__field fullWidth relative">
                          
                          <label for="email_message" class="position-relative">To :</label>
                          <input type="text" name="email_message" id="email_message">
                      </div>

                      <div class="input__field fullWidth relative">
                          <label for="Message" class="position-relative">Message :</label>
                          <textarea id="messageAll" name="messageAll" palceholder="Message" style="boder:none;"></textarea>
                          <div id= "userdata">
                              <div contenteditable="true" id= "userdataEdit"> </div>
                              <div id="userdataForward">

                              </div>
                          </div>
                         
                      </div>

                      


                      <input type="hidden" name="replytype" value="" id="replytype">
                      <button name="btnreply" id="btnreply" class="btn btn__primary">Send</button>
                      <button name="btncancel" id="btncancel" class="btn btn__primary margin-left-15">Cancel</button>

                  </div>


                {{--<div class="border-sepration fullWidth"></div>--}}

                <div id = "replyrecord" class="form_field__container fullWidth margin-top-20">
                    
                </div>
                
                
                
            </div>



        </div>

    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
