@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.timepicker.css')}}" />
<div class="main__container container__custom">
    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <!--Type of Customer-->
    <div class="reservation__atto">
        <div class="reservation__title">
            <h1>Enquiry > {{$name}}</h1>
        </div>

        <div class="search__manage-order">

            <div class="form_field__container">

                @if($prestId == 0)

                <div class="selct-picker-plain margin-left-20 filter-by-status hide">
                    <label for="order_status" class="no-margin hide">Filter By Status</label>
                    <select class="selectpicker" data-style="no-background-with-buttonline no-padding-left  no-padding-top" data-width="150" name="order_status" id="order_status">
                        <option value="">Active Orders</option>
                    </select>
                </div>

                @endif

                <div class="hide">
                    <button class="btn btn__primary font-weight-600" type="submit">Search</button>
                </div>

                <div class="hidden-xs hidden-sm hidden-tablet hide"><a href="" class="btn btn__holo font-weight-600">Archive</a></div>
            </div>

        </div>
    </div>

    <!--Bookings-->
    <div class="booking">
        <div class="booking-nav archive__page_nav hide">

            <div class="heading_filter">
                {{-- <div class="table__heading" >All</div>--}}
                <div class="archive__page_navtab">

                    <div class="archive__page_item">
                        <i class="fa fa-calendar" aria-hidden="true"></i> Select Date
                    </div>


                    <div class="archive__page_item">
                        <input class="datepicker__tab start_date" title="Start Date" type="text" autocomplete="off" id="start_date_all" name="start_date_all">
                        <label>From</label>
                    </div>

                    <div class="archive__page_item">
                        <input type="text" class="datepicker__tab end_date" title="End Date" autocomplete="off" id="end_date_all" name="end_date_all">
                        <label>To</label>
                    </div>
                    <div class="archive__page_item text-center">
                        <button type="button" class="reset btn btn__primary" id="reset_filter">Reset</button>
                    </div>
                </div>
            </div>
            <!-- search bar start-->
            <div class="reservation__searchbar all">
                <i class="fa fa-search fa-fw" aria-hidden="true"></i> 
                <input type="text" id="reservation_search" onkeyup="" placeholder="Search" title="">
            </div>
            <!-- search bar end -->
        </div>


        <div class="archive_reservation">
            @include('block.request_render')
        </div>

    </div>
</div>
</div>
@include('block.request_detail')

<script src="{{URL::asset('js/schedular/moment.min.js')}}"></script>
<script src="{{URL::asset('js/schedular/fullcalendar.min.js')}}"></script>
<script src="{{URL::asset('js/reservation.js')}}"></script>
<script src="{{URL::asset('js/turnovertime.js')}}"></script>
<script src="{{URL::asset('js/jquery-ui-1.12.1.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.js')}}"></script>

<link href="{{asset('css/reservation/tags/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/reservation/tags/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/tags/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>
<style> .ui-front { z-index: 10000!important;}</style>
@endsection
