@extends('layouts.app')

@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>{{ __('Static Blocks') }}<span
                            style="margin:0px 5px;">({{ $staticBlocks->currentPage() .'/' . $staticBlocks->lastPage() }}
                        )</span>&emsp;</h1>
            </div>
            <span style="float: right;"><a href="{{ URL::to('block/create') }}" class="btn btn__primary">Add</a></span>
        </div>
        <div class="addition__functionality">
            {!! Form::open(['method'=>'get']) !!}
            <div class="row functionality__wrapper">
                <div class="row form-group col-md-3">
                    <select name="restaurant_id" id="restaurant_id"
                            class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>
                        <option value="">Select Restaurant</option>
                        @foreach($groupRestData as $rest)
                            <optgroup label="{{ $rest['restaurant_name'] }}">
                                @foreach($rest['branches'] as $branch)
                                    <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
                <div class="row form-group col-md-5">
                    <button class="btn btn__primary" style="margin-left: 5px;" type="submit">Search</button>
                </div>
            </div>
        </div>


        <div>
            <div class="active__order__container">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif

                <div class="guestbook__container box-shadow">
                    <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
                        <div class="col-md-10">
                            <div class="col-md-2" style="font-weight: bold;">Id</div>
                            <div class="col-md-5" style="font-weight: bold;">Restaurant</div>
                            <div class="col-md-5" style="font-weight: bold;">Block Heading</div>
                        </div>
                        {{--<div class="col-md-3" style="font-weight: bold;">Image</div>--}}
                        <div class="col-md-2">
                            <div class="col-md-12" style="font-weight: bold;">Action</div>
                        </div>
                    </div>

                    @if(isset($staticBlocks) && count($staticBlocks)>0)
                        @foreach($staticBlocks as $sp)
                            @php
                                $images = '';
                                if($sp->image)
                                    $images = implode(" | ", array_column(json_decode($sp->image, true), 'image'));
                            @endphp
                            <div class="guestbook__customer-details-content rest__row">
                                <div class="col-md-10">
                                    <div class="col-md-2">{{ $sp->id }}</div>
                                    <div class="col-md-5">{{ $sp->restaurant_name}}</div>
                                    <div class="col-md-5">{{ $sp->block_title}}</div>
                                </div>
                                {{--<div class="col-md-3">{{ $images }}</div>--}}
                                <div class="row col-md-2">
                                    <div class="col-md-12">
                                        <a style="margin:10px 0; width:100%;" href="{{ URL::to('block/' . $sp->id . '/edit') }}"
                                           class="btn btn__primary">Edit</a>
                                        <!--<a href="" class="btn btn-info">Edit</a>-->
                                    </div>
                                    <div class="col-md-12">
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['static.destroy', $sp->id]]) !!}
                                        @csrf
                                        <button style="margin:10px 0; width:100%;" class="btn btn__cancel" type="submit">Delete</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="row"
                             style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                            No Record Found
                        </div>
                    @endif
                </div>
                @if(isset($staticPages) && count($staticPages)>0)
                    <div style="margin: 0px auto;">
                        {{ $staticPages->appends($_GET)->links()}}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection