@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Menu Attribute - Add') }}</h1>

    </div>
      <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('menu_attribute') }}" class="btn btn__primary">List</a></span>

  </div>

  <div>

    <div class="card form__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit">
          <form method="POST" action="{{ route('menu_attribute.store') }}" enctype="multipart/form-data">
            @csrf

            {{--<div class="form-group row form_field__container">
              <label for="parent_restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant *') }}</label>
              <div class="col-md-4">
                <select name="parent_restaurant_id" id="parent_restaurant_id" class="form-control{{ $errors->has('parent_restaurant_id') ? ' is-invalid' : '' }}" required>
                  <option value="">Select Restaurant</option>
                  @foreach($restaurantData as $rest)
                  <option value="{{ $rest->id }}" {{ (old('parent_restaurant_id')==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('parent_restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('parent_restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>--}}
            <div class="form-group row form_field__container">
                  <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
                  <div class="col-md-4">
                      <select name="language_id" required id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                          <option value="">Select Language</option>
                          @foreach($languageData as $lang)
                              <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                          @endforeach
                      </select>
                      @if ($errors->has('language_id'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('language_id') }}</strong>
                </span>
                      @endif
                  </div>
              </div>
            
            <div class="form-group row form_field__container">
              <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant Branch *') }}</label>
              <div class="col-md-4">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>
                  <option value="">Select Restaurant</option>
                  @foreach($groupRestData as $rest)
                  <optgroup label="{{ $rest['restaurant_name'] }}">
                    @foreach($rest['branches'] as $branch)
                    <option value="{{ $branch['id'] }}" {{ (old('restaurant_id') == $branch['id']) ? 'selected' : (isset($selected_rest_id) && $selected_rest_id == $branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                    @endforeach
                  </optgroup>
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>

          {{--    <div class="form-group row form_field__container">
                                <label for="product_type"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Product Type') }}</label>
                                <div class="col-md-4">
                                    <select required name="product_type" id="product_type"
                                            class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }}">
                                        <option value="">Select Product Type</option>
                                       
                                            @foreach($product_types as $key=>$val)
                                                <option value="{{ $key }}" {{ (old('product_type')==$key) ? 'selected' :'' }}>{{ $val }}</option>
                                            @endforeach
                                    
                                    </select>
                                    @if ($errors->has('product_type'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>--}}

            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Attribute Label *') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="attribute_label" type="text" class="{{ $errors->has('attribute_label') ? ' is-invalid' : '' }}"
                  name="attribute_label" value="{{ old('attribute_label') }}" required>
                </div>
                @if ($errors->has('attribute_label'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('attribute_label') }}</strong>
                </span>
                @endif
              </div>
            </div>
              <div class="form-group row form_field__container">
                  <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Attribute Prompt *') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="attribute_prompt" type="text" class="{{ $errors->has('attribute_prompt') ? ' is-invalid' : '' }}"
                                 name="attribute_prompt" value="{{ old('attribute_prompt') }}" required>
                      </div>
                      @if ($errors->has('attribute_prompt'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('attribute_prompt') }}</strong>
                </span>
                      @endif
                  </div>
              </div>

            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Attribute Code *') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="attribute_code" type="text" class="{{ $errors->has('attribute_code') ? ' is-invalid' : '' }}"
                  name="attribute_code" value="{{ old('attribute_code') }}" required>
                </div>
                @if ($errors->has('attribute_code'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('attribute_code') }}</strong>
                </span>
                @endif
              </div>
            </div>

              <div class="form-group row form_field__container">
                  <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Form Field Type') }}</label>
                  <div class="col-md-4">
                      <select name="form_field_type" id="form_field_type" class="form-control{{ $errors->has('form_field_type') ? ' is-invalid' : '' }}"  required>
                          <option value="">Select </option>
                          @foreach($fieldtypes as $type)
                              <option value="{{ $type}}" {{ (old('form_field_type')==$type) ? 'selected' :'' }}>{{ isset($type)?ucfirst($type):'' }}</option>
                          @endforeach
                      </select>
                      @if ($errors->has('form_field_type'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('form_field_type') }}</strong>
                </span>
                      @endif
                  </div>
              </div>


              <div class="form-group row form_field__container">
                  <label for="is_popular" class="col-md-4 col-form-label text-md-right">Show as dropdown</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                          <input id="show_as_dropdown" type="checkbox" class="" name="show_as_dropdown">
                      </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="is_configurable" class="col-md-4 col-form-label text-md-right">Is configurable</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                      <input id="is_configurable" type="checkbox" class="" name="is_configurable">
                  </div>
              </div>


              <div class="form-group row form_field__container">
                  <label for="use_as_filter_option" class="col-md-4 col-form-label text-md-right">Use as filter option</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                      <input id="use_as_filter_option" type="checkbox" class="" name="use_as_filter_option">
                  </div>
              </div>
              <div class="form-group row form_field__container">
                  <label for="use_in_search" class="col-md-4 col-form-label text-md-right">Use in search</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                      <input id="use_in_search" type="checkbox" class="" name="use_in_search">
                  </div>
              </div>
              <div class="form-group row form_field__container">
                  <label for="use_for_prmo_rule_conditions" class="col-md-4 col-form-label text-md-right">Use for Prmo Rule Condition</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                      <input id="use_for_prmo_rule_conditions" type="checkbox" class="" name="use_for_prmo_rule_conditions">
                  </div>
              </div>

            <!--end here -->


            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-4" style="padding-top: 10px;">
                <input type="radio" id="status1" name="status" value="1" checked> Active
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="status0" name="status" value="0" > Inactive
                @if ($errors->has('status'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>
            </div>


            <br /><br />
            <div class="row mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
<script>

    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    }

    $(document).on('focus','#attribute_code',function(){

        var attrcode=$('#attribute_label').val();
        $(this).val(convertToSlug(attrcode));

    });
</script>

@endsection
