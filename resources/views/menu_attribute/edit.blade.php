@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Menu Attribute - Edit') }}</h1>

    </div>
      <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('menu_attribute') }}" class="btn btn__primary">List</a></span>

  </div>

  <div>

    <div class="card ">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
          <ul class="nav nav-tabs">
              <li class="<?php if(!isset($_GET['tab']) || (isset($_GET['tab']) && $_GET['tab']==1)){ echo 'active'; } ?>"><a data-toggle="tab" href="#general">General</a></li>
              <li class="<?php if((isset($_GET['tab']) && $_GET['tab']==2)){ echo 'active'; } ?>"><a data-toggle="tab" href="#attributesvalues">Attribute Values</a></li>

          </ul>
          <div class="tab-content">
              <div id="general" class="tab-pane  <?php if(!isset($_GET['tab']) || (isset($_GET['tab']) && $_GET['tab']==1)){ echo 'active in'; } ?>">
                  <div class="form__user__edit1">
          {{ Form::model($menuAttribute, array('route' => array('menu_attribute.update', $menuAttribute->id), 'method' => 'PUT',  'enctype' => 'multipart/form-data')) }}

          @csrf
          
           <div class="form-group row form_field__container">
                  <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
                  <div class="col-md-4">
                      <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                          <option value="">Select Language</option>
                          @foreach($languageData as $lang)
                              <option value="{{ $lang->id }}" {{ ($menuAttribute->language_id==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                          @endforeach
                      </select>
                      @if ($errors->has('language_id'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('language_id') }}</strong>
                </span>
                      @endif
                  </div>
              </div>
          
          <div class="form-group row form_field__container">
            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant Branch *') }}</label>
            <div class="col-md-4">
              <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>                
                @foreach($groupRestData as $rest)
                <optgroup label="{{ $rest['restaurant_name'] }}">
                  @foreach($rest['branches'] as $branch)
                  <option value="{{ $branch['id'] }}" {{ ((old('restaurant_id')==$branch['id']) || (isset($menuAttribute->restaurant_id) && $menuAttribute->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                  @endforeach
                </optgroup>
                @endforeach
              </select>
              @if ($errors->has('restaurant_id'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('restaurant_id') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="attribute_label" class="col-md-4 col-form-label text-md-right">{{ __('Attribute Label *') }}</label>
            <div class="col-md-4">
              <div class="input__field">
                <input id="attribute_label" type="text" class="{{ $errors->has('attribute_label') ? ' is-invalid' : '' }}"
                name="attribute_label" value="{{ isset($menuAttribute->attribute_label)?$menuAttribute->attribute_label:old('attribute_label') }}" required>
              </div>
              @if ($errors->has('attribute_label'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('attribute_label') }}</strong>
              </span>
              @endif
            </div>
          </div>

            <div class="form-group row form_field__container">
                <label for="attribute_prompt" class="col-md-4 col-form-label text-md-right">{{ __('Attribute Prompt *') }}</label>
                <div class="col-md-4">
                    <div class="input__field">
                        <input id="attribute_prompt" type="text" class="{{ $errors->has('attribute_prompt') ? ' is-invalid' : '' }}"
                               name="attribute_prompt" value="{{ isset($menuAttribute->attribute_prompt)?$menuAttribute->attribute_prompt:old('attribute_prompt') }}" required>
                    </div>
                    @if ($errors->has('attribute_prompt'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('attribute_prompt') }}</strong>
              </span>
                    @endif
                </div>
            </div>
            <div class="form-group row form_field__container">
                <label for="attribute_code" class="col-md-4 col-form-label text-md-right">{{ __('Attribute Code *') }}</label>
                <div class="col-md-4">
                    <div class="input__field">
                        <input id="attribute_code" type="text" class="{{ $errors->has('attribute_code') ? ' is-invalid' : '' }}"
                               name="attribute_code" value="{{ isset($menuAttribute->attribute_code)?$menuAttribute->attribute_code:old('attribute_code') }}" required>
                    </div>
                    @if ($errors->has('attribute_code'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('attribute_code') }}</strong>
              </span>
                    @endif
                </div>
            </div>
            <div class="form-group row form_field__container">
                <label for="attribute_label" class="col-md-4 col-form-label text-md-right">{{ __('Form Field Type *') }}</label>
                <div class="col-md-4">
                    <select name="form_field_type" id="form_field_type" class="form-control{{ $errors->has('form_field_type') ? ' is-invalid' : '' }}"  required>
                        <option value="">Select </option>
                        @foreach($fieldtypes as $type)
                            <option value="{{ $type}}" {{ ($menuAttribute->form_field_type==$type) ? 'selected' :'' }}>{{ isset($type)?ucfirst($type):'' }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('form_field_type'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('form_field_type') }}</strong>
              </span>
                    @endif
                </div>
            </div>



          <div class="form-group row form_field__container">
            <label for="show_as_dropdown" class="col-md-4 col-form-label text-md-right">Show as dropdown</label>
            <div class="col-md-4" style="padding-top: 10px;">
              <input id="show_as_dropdown" type="checkbox" class="" name="show_as_dropdown" {{ ((isset($menuAttribute->show_as_dropdown) && $menuAttribute->show_as_dropdown=='1') || old('show_as_dropdown')=='1') ? 'checked' :'' }}>
            </div>
          </div>
            <div class="form-group row form_field__container">
                <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is configurable</label>
                <div class="col-md-4" style="padding-top: 10px;">
                    <input id="is_configurable" type="checkbox" class="" name="is_configurable" {{ ((isset($menuAttribute->is_configurable) && $menuAttribute->is_configurable=='1') || old('is_configurable')=='1') ? 'checked' :'' }}>
                </div>
            </div>
            <div class="form-group row form_field__container">
                <label for="use_as_filter_option" class="col-md-4 col-form-label text-md-right">Use as filter option</label>
                <div class="col-md-4" style="padding-top: 10px;">
                    <input id="use_as_filter_option" type="checkbox" class="" name="use_as_filter_option" {{ ((isset($menuAttribute->use_as_filter_option) && $menuAttribute->use_as_filter_option=='1') || old('use_as_filter_option')=='1') ? 'checked' :'' }}>
                </div>
            </div>
            <div class="form-group row form_field__container">
                <label for="use_in_search" class="col-md-4 col-form-label text-md-right">Use in search</label>
                <div class="col-md-4" style="padding-top: 10px;">
                    <input id="use_in_search" type="checkbox" class="" name="use_in_search" {{ ((isset($menuAttribute->use_in_search) && $menuAttribute->use_in_search=='1') || old('use_in_search')=='1') ? 'checked' :'' }}>
                </div>
            </div>
            <div class="form-group row form_field__container">
                <label for="use_for_prmo_rule_conditions" class="col-md-4 col-form-label text-md-right">Use for Prmo Rule Condition</label>
                <div class="col-md-4" style="padding-top: 10px;">
                    <input id="use_for_prmo_rule_conditions" type="checkbox" class="" name="use_for_prmo_rule_conditions" {{ ((isset($menuAttribute->use_for_prmo_rule_conditions) && $menuAttribute->use_for_prmo_rule_conditions=='1') || old('use_for_prmo_rule_conditions')=='1') ? 'checked' :'' }}>
                </div>
            </div>


          <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
            <div class="col-md-4" style="padding-top: 10px;">
              <input type="radio" id="status1" name="status" value="1" {{ ((isset($menuAttribute->status) && $menuAttribute->status=='1') || old('status')=='1') ? 'checked' :'' }}> Active
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="status0" name="status" value="0" {{ ((isset($menuAttribute->status) && $menuAttribute->status=='0') || old('status')=='0') ? 'checked' :'' }}> Inactive
              @if ($errors->has('status'))
              <span class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>
          </div>


          <br /><br />
          <div class=" row  mb-0">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
            </div>
          </div>
        </form>
      </div>
              </div>
              <div id="attributesvalues" class="tab-pane <?php if((isset($_GET['tab']) && $_GET['tab']==2)){ echo 'active in'; } ?>">
                  <button type="button" class="btn btn__primary add_attribute_value">{{ __('Add New [+]') }}</button>
                  <form name="attributevalueform" id="attributevalueform" method="post" action="/save/attribute/value/{{$menuAttribute->id}}" enctype="multipart/form-data">


                  <table class="table table-striped">
                      <thead>
                      <tr>
                          <th>Attribute Value</th>
                          <th>Attribute Image</th>
                          <th>POS Code</th>
                          <th>Sort Order</th>
                          <th>Status</th>
                          <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>


                      <?php
                      if(count($menuAttribute->attributeValues)){
                          $i=0;
                          foreach ($menuAttribute->attributeValues as $val){
                      ?>
                      <tr>
                          <input type="hidden" name="id[{{$i}}]"  value="{{$val->id}}">
                          <td>
                              <input type="text" name="attribute_value[{{$i}}]"  value="{{$val->attribute_value}}">

                          </td>
                          <td>
                              <input type="file" name="attribute_image[{{$i}}]"  value="{{$val->attribute_image}}">

                          @if(isset($val->attribute_image))
                                  <div class="">
                                      <a data-fancybox="gallery" href="{{url('/').'/..'.$val->attribute_image}}"><img  class="img-thumbnail" src="{{ url('/').'/..'.$val->attribute_image }}"></a>&emsp;
                                  </div>
                               @else

                               @endif
                          </td>
                          <td>
                              <input type="text" name="pos_code[{{$i}}]"  value="{{$val->pos_code}}">
                          </td>
                          <td>
                              <input type="text" name="sort_order[{{$i}}]"  value="{{$val->sort_order}}">

                          </td>
                          <td>

                              <select name="status[{{$i}}]" id="status" class="form-control"  required>
                                  <option value="">--Select-- </option>
                                  <option value="1" {{$val->status==1?'selected':''}}>Active </option>
                                  <option value="0" {{$val->status==0?'selected':''}}>Inactive </option>
                              </select>

                          </td>
                          <td>
                              <button  class="glyphicon glyphicon-trash delete_attr_vals"  ></button>



                          </td>

                      </tr>
                      <?php
                      $i++;
                          }
                          }else{ ?>
            <tr>
                <td colspan="4">No Record</td>
            </tr>

                         <?php } ?>



                      </tbody>
                  </table>

                  <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                  </form>

              </div>
              </div>
    </div>
  </div>

</div>
</div>
    <script>
        function convertToSlug(Text)
        {
            return Text
                .toLowerCase()
                .replace(/[^\w ]+/g,'')
                .replace(/ +/g,'-')
                ;
        }

        $(document).on('focus','#attribute_code',function(){

            var attrcode=$('#attribute_label').val();
            $(this).val(convertToSlug(attrcode));

        });
        $(document).on('click','.delete_attr_vals',function(){

            $(this).parents('tr').remove();

        });

      $(document).on('click','.add_attribute_value',function(){

          var attrib=$('#attributesvalues');
          var item_count=attrib.find('tbody tr').length;
          if(attrib.find('tbody tr').length){
                var data='<tr> <input type="hidden" name="id['+item_count+']" value=""> <td> <input type="text" name="attribute_value['+item_count+']" value=""> </td> <td> <input type="file" name="attribute_image['+item_count+']" value=""> </td> <td> <input type="text" name="pos_code['+item_count+']" value=""> </td><td> <input type="text" name="sort_order['+item_count+']" value=""> </td> <td> <select name="status['+item_count+']" id="status" class="form-control" required=""> <option value="">--Select-- </option> <option value="1" selected="">Active </option> <option value="0">Inactive </option> </select> </td> <td> <button class="glyphicon glyphicon-trash delete_attr_vals"></button> </td></tr>';
                attrib.find('tbody tr:last').after(data);

          }else{
              // insertBefore
              var data='<tr> <input type="hidden" name="id[0]" value=""> <td> <input type="text" name="attribute_value[0]" value=""> </td> <td> <input type="file" name="attribute_image[0]" value=""> </td> <td> <input type="text" name="pos_code[0]" value=""> </td> <td> <input type="text" name="sort_order[0]" value=""> </td><td> <select name="status[0]" id="status" class="form-control" required=""> <option value="">--Select-- </option> <option value="1" selected="">Active </option> <option value="0">Inactive </option> </select> </td> <td> <button class="glyphicon glyphicon-trash delete_attr_vals"></button> </td></tr>';
              attrib.find('tbody').html( data);

          }

      })


        $("#attributevalueform").submit(function(e){

            e.preventDefault();


            console.log(formData);

            var inputData = $(this).serialize();
            var url=$(this).attr('action');

            var attrib=$('#attributesvalues');
            var item_count=attrib.find('tbody tr').length;
            if(item_count){
                var formData = new FormData(this);

            }else{

                var formData = $('#attributevalueform').serialize();

            }
            var senddata={
                type: 'POST',
                url:url,
                data: formData,
                success: function (data) {

                    alertbox('Success',data.message, function(modal){
                        setTimeout(function() {
                            window.location.href="/menu_attribute/<?php echo $menuAttribute->id;?>/edit?tab=2";
                            modal.modal('hide');
                        }, 2000);

                    });

                },
                error: function (data, textStatus, errorThrown) {

                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){

                        setTimeout(function() {
                            // $('#add-tabletype-modal').modal('hide');
                            // location.reload();
                        }, 2000);

                    });
                },
                complete: function(data) {
                },
                //cache: false,
               // contentType: false,
                //processData: false
            };
            if(item_count){
                senddata.cache=false;
                senddata.contentType=false;
                senddata.processData=false;
            }
            $.ajax(senddata);
        });

    </script>

@endsection
