@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Menu Attributes<span style="margin:0px 5px;">({{ $menuAttributes->currentPage() .'/' . $menuAttributes->lastPage() }})</span>&emsp;</h1>
    </div>
    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('menu_attribute/create') }}" class="btn btn__primary">Add</a></span>
  </div>

  <div class="addition__functionality">

    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">
      <div class="row form-group col-md-3">
        <select name="restaurant_id" id="restaurant_id" class="form-control" >
          <option value="">Select Restaurant</option>
          @foreach($restaurantData as $rest)
          <option value="{{ $rest->id }}" {{ (isset($restaurant_id) && $restaurant_id==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
          @endforeach
        </select>
      </div>
      <div class="row form-group col-md-5">
        <button class="btn btn__primary" style="margin-left: 5px;" type="submit" >Search</button>
      </div>
    </div>
    {!! Form::close() !!}

  </div>

  <div>

    <div class="active__order__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif



      <div class="guestbook__container box-shadow">
        <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
          <div class="row col-md-10">
            <div class="col-md-2" style="font-weight: bold;">Attribute Label</div>
            <div class="col-md-2" style="font-weight: bold;">Attribute Prompt</div>
            <div class="col-md-2" style="font-weight: bold;">Attribute Code</div>
            <div class="col-md-2" style="font-weight: bold;">Restaurant</div>
            <div class="col-md-2" style="font-weight: bold;">Status</div>
          </div>

          <div class="row col-md-2">
            <div class="col-md-12" style="font-weight: bold;">Action</div>
          </div>


        </div>
        @if(isset($menuAttributes) && count($menuAttributes)>0)
        @foreach($menuAttributes as $menuattribute)
        <div class="guestbook__customer-details-content rest__row">
          <div class="row col-md-10 order__row">
            <div class="col-md-2" style="margin: auto 0px;">{{ isset($menuattribute->attribute_label) ? $menuattribute->attribute_label : '' }}</div>
            <div class="col-md-2" style="margin: auto 0px;">{{ isset($menuattribute->attribute_prompt) ? $menuattribute->attribute_prompt : '' }}</div>
            <div class="col-md-2" style="margin: auto 0px;">{{ isset($menuattribute->attribute_code) ? $menuattribute->attribute_code : '' }}</div>
            <div class="col-md-2" style="margin: auto 0px;">{{ isset($menuattribute->Restaurant->restaurant_name) ? $menuattribute->Restaurant->restaurant_name : '' }}</div>
            <div class="col-md-2" style="margin: auto 0px;">{{ ($menuattribute->status==1)?'Active':($menuattribute->status==0?'Inactive':'') }}</div>

          </div>

          <div class="row col-md-2">
            <div class="col-md-12">
              <a style="width:100%;margin:10px 0;" href="{{ URL::to('menu_attribute/' . $menuattribute->id . '/edit') }}" class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
            </div>
            <div class="col-md-12">
              {!! Form::open(['method' => 'DELETE', 'route' => ['menu_attribute.destroy', $menuattribute->id]]) !!}
              @csrf
              <button style="width:100%;margin:10px 0;" class="btn btn__cancel" type="submit"><span class="glyphicon glyphicon-trash"></span> Delete</button>
              {!! Form::close() !!}
            </div>

          </div>
        </div>
        @endforeach
        @else
        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
          No Record Found
        </div>
        @endif
      </div>
      @if(isset($menuAttributes) && count($menuAttributes)>0)
      <div style="margin: 0px auto;text-align: center;">
        {{ $menuAttributes->appends($_GET)->links()}}
      </div>
      @endif
    </div>

  </div>
</div>

  {{--<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<style href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></style>
  <script>
    $(document).ready(function() {
      $('#attributes').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "../server_side/scripts/server_processing.php"
      } );
    } );

  </script>--}}
@endsection
