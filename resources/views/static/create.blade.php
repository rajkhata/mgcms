@extends('layouts.app')

@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">

            <div class="card">
                <div class="card-header">{{ __('Restaurant Static Pages') }}</div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif ($errors->has('page_err'))
                    <div class="alert alert-danger">
                        {{ $errors->first('page_err') }}
                    </div>
                @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('static.store') }}" enctype="multipart/form-data">
                        @csrf
			 <div class="form-group row">
                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                            <div class="col-md-6">
                                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" >
                                    <option value="">Select restaurant</option>
                                    @foreach($restaurant as $rest)
                                        <option value="{{ $rest->id }}" >{{ $rest->restaurant_name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('restaurant_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
			<div class="form-group row ">
		          <label  class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
		          <div class="col-md-6">
		              <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
		                  <option value="">Select Language</option>
		                  @foreach($languageData as $lang)
		                      <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
		                  @endforeach
		              </select>
		              @if ($errors->has('language_id'))
		                  <span class="invalid-feedback">
		          		<strong>{{ $errors->first('language_id') }}</strong>
		        	</span>
		              @endif
		          </div>
		      </div>
                        <div class="form-group row">
                            <label for="parent_id" class="col-md-4 col-form-label text-md-right">{{ __('Static Blocks') }}</label>
                            <div class="col-md-6" id="static_block_div">
                                {{--@php $savedStaticBlockIds = explode(',', $staticPage['static_block_ids']); @endphp
                                @foreach($staticBlock as $block)
                                    <input type="checkbox" name="static_blocks[]" value="{{ $block['id'] }}" @if(in_array($block['id'], $savedStaticBlockIds)) {{ 'checked="checked"' }} @endif>{{ $block['block_title'] }}<br />
                                @endforeach--}}
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="page_heading" class="col-md-4 col-form-label text-md-right">{{ __('Page Heading') }}</label>
                            <div class="col-md-6">
                                <input id="page_heading" type="text" class="form-control{{ $errors->has('page_heading') ? ' is-invalid' : '' }}"
                                       value="{{ old('page_heading') }}" name="page_heading" required>
                                @if ($errors->has('page_heading'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('page_heading') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="page_sub_heading" class="col-md-4 col-form-label text-md-right">{{ __('Page Sub Heading') }}</label>
                            <div class="col-md-6">
                                <input id="page_sub_heading" type="text" class="form-control{{ $errors->has('page_sub_heading') ? ' is-invalid' : '' }}"
                                       value="{{ old('page_sub_heading') }}" name="page_sub_heading">
                                @if ($errors->has('page_sub_heading'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('page_sub_heading') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="page_content" class="col-md-4 col-form-label text-md-right">{{ __('Page Content') }}</label>
                            <div class="col-md-6">
                                <textarea name="page_content" id="page_content" class="form-control{{ $errors->has('page_content') ? ' is-invalid' : '' }}"
                                          cols="30" rows="10" class="form-control{{ $errors->has('page_content') ? ' is-invalid' : '' }}editor" required>{{ old('page_content') }}</textarea>
                                @if ($errors->has('page_content'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('page_content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Create Page') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function(){
            $("#restaurant_id").change(function() {
                var rest_id = '';
		var lang_id = '';
                rest_id = $('#restaurant_id').find(":selected").val();
		lang_id = $('#language_id').find(":selected").val();
                if(rest_id!='' && lang_id!='') {
                      	getBlockData(rest_id,lang_id);
                }else{
			alert('Please selected langauage.');
		}
            });
	    $("#language_id").change(function() {
                var rest_id = '';
		var lang_id = '';
		lang_id = $('#language_id').find(":selected").val();
                rest_id = $('#restaurant_id').find(":selected").val();
                if(rest_id!='' && lang_id!='') {  
                        getBlockData(rest_id,lang_id);
                }else{
			alert('Please selected restaurant.');
		}
            });

        });
	function getBlockData(rest_id,lang_id){
		$.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/')."/get_static_blocks"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid='+rest_id+'&lid='+lang_id,
                        success: function (data) {
                            if($.type(data.dataObj)!=='undefined')
                            {
                                var html_checkbox = '';
                                $.each(data.dataObj, function(i, item) {
                                    html_checkbox += '<input type="checkbox" name="static_blocks[]" value="'+item.id+'">'+ item.block_title + '<br />';
                                    //console.log(item);
                                });
                                $('#static_block_div').html(html_checkbox)
                            }
                        }
                    });
	}
        function addFileDiv(sel) {
            var type = $(sel).hasClass('image-caption-button') ? 'image' : 'video';
            if(type=='image') {
                var divHtml = $('#' + type + '_div_0').html();
            }
            else
                var divHtml = $('.source-'+type+'-div').html(); // $('.image-caption-div')[0].outerHTML;
            $('.'+type+'-caption-div').last().after('<div class="form-group row '+type+'-caption-div" style="margin-top: 20px;">'+divHtml+'</div>');
        }
        function removeFileDiv(sel) {
            var type = $(sel).hasClass('image-caption-button') ? 'image' : 'video';
            var elementCnt = $('.'+type+'-caption-div').length;
            //if(elementCnt>1) {
            $('.' + type + '-caption-div').last().remove();
            /*if(type=='image')
                imgCnt--;
            else if(type=='video')
                videoCnt--;*/
            //}
        }
    </script>
    <!--script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
        tinymce.init({
            selector: 'textarea.editor',
            menubar:false,
            height: 320,
            theme: 'modern',
            plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });
    });
</script-->
@endsection
