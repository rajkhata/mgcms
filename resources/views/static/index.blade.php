@extends('layouts.app')

@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">
            <div class="card">
                <div class="card-header">
                    {{ __('Static Pages') }}
                    <span style="float: right;margin:0px 5px;">({{ $staticPages->currentPage() .'/' . $staticPages->lastPage() }})</span>&emsp;
                    <span style="float: right;"><a href="{{ URL::to('static/create') }}" class="btn btn-primary">Add</a></span>
                    <!--<span style="float: right;">()</span>-->
                </div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'get']) !!}
                <div class="row" style="margin:10px;justify-content: center;">
                    <div class="row form-group col-md-4">&nbsp;</div>
                    <div class="row form-group col-md-3">
                        <select name="restaurant_id" id="restaurant_id" class="form-control" >
                            <option value="">Select Restaurant</option>
                            @foreach($restaurantData as $rest)
                                <option value="{{ $rest->id }}" {{ (isset($restaurant_id) && $restaurant_id==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                            @endforeach  /static/create
                        </select>
                    </div>
                    <div class="row form-group col-md-5">
                        <button class="btn btn-info" style="margin-left: 5px;" type="submit" >Search</button>
                    </div>
                </div>
                <div class="card-body">

                    <div class="row panel-heading" style="background-color: rgba(0,0,0,.03);padding:8px 0px;margin-bottom: 5px;">
                        <div class="col-md-1" style="font-weight: bold;">Id</div>
                        <div class="col-md-3" style="font-weight: bold;">Restaurant</div>
                        <div class="col-md-2" style="font-weight: bold;">Page Heading</div>
                        <div class="col-md-2" style="font-weight: bold;">Page Subheading</div>
                        <div class="col-md-1" style="font-weight: bold;">Priority</div>
                        {{--<div class="col-md-3" style="font-weight: bold;">Image</div>--}}
                        <div class="col-md-2" style="font-weight: bold;">Action</div>
                    </div>

                    @if(isset($staticPages) && count($staticPages)>0)
                        @foreach($staticPages as $sp)
                            @php
                                $images = '';
                                if($sp->image)
                                    $images = implode(" | ", array_column(json_decode($sp->image, true), 'image'));
                            @endphp
                            <div class="row" style="padding: 6px 0px;border-bottom: 1px solid #f5f8fa;">
                                <div class="col-md-1">{{ $sp->id }}</div>
                                <div class="col-md-3">{{ $sp->restaurant_name}}</div>
                                <div class="col-md-2">{{ $sp->page_heading}}</div>
                                <div class="col-md-2">{{ $sp->page_sub_heading}}</div>
                                <div class="col-md-1">{{ $sp->priority }}</div>
                                {{--<div class="col-md-3">{{ $images }}</div>--}}
                                <div class="row col-md-2">
                                    <div class="col-md-5">
                                        <a href="{{ URL::to('static/' . $sp->id . '/edit') }}" class="btn btn-info">Edit</a>
                                    </div>
                                    <div class="col-md-7">
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['static.destroy', $sp->id]]) !!}
                                        @csrf
                                        <button class="btn btn-default" type="submit">Delete</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                            No Record Found
                        </div>
                    @endif
                </div>
                @if(isset($staticPages) && count($staticPages)>0)
                    <div style="margin: 0px auto;">
                        {{ $staticPages->appends($_GET)->links()}}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection