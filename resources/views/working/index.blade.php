@extends('layouts.app')
@section('content')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
  <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
  <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />



  <div class="main__container container__custom_working">

    <div id="exTab1" class=" ">
      <ul  class="nav nav-pills">
        <li class="active working__tab">
          <a href="#working_hours" data-toggle="tab">Week</a>
        </li>
        <li class="working__tab">
          <a href="#custom_working_hours" data-toggle="tab">Custom</a>
        </li>    
     
      </ul>
      
          <div class="working__tab hide">
         <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#workingHours">Working hours</button>

        </div>

      <div class="tab-content clearfix working-hours">
        <div class="tab-pane active box-shadow" id="working_hours"  >
          <h5 class="tab-head">Working Hours</h5>
          <div class="workinghrs__calendar">
            <div id='calendar'></div>
          </div>
        </div>
        <div class="tab-pane customhrs__calendar" id="custom_working_hours">
          <h5 class="tab-head">Create a custom rule for a specific day or range of days that overrides the standard setting for every week.</h5>

          <div class="custom__setting-btn box-shadow text-align-center">
            <div  class="btn_ btn btn__holo font-weight-600" data-toggle="modal" data-target="#customSettingModal">
              Add a Custom Setting
            </div>
          </div>

          <div id='custom_calendar_view' class="box-shadow"></div>

        </div>
      </div>
    </div>
    <input type="hidden" id="type" name="type" value="day" />

    <!--modal custom working hours-->
    <div class="modal fade" id="customSettingModal" tabindex="-1" role="dialog" aria-labelledby="customSettingModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg customHrs__modal">
        <div class="modal-content">
          <div class="workinghrs__header">
            <h4>Add a Custom Working Hour</h4>
          </div>
          <div class="modal-body">
            <div class="custom_hrsModal__container">
              <div class="alert alert-danger" style="display:none"></div>
              <form id="custom_hours" ethod="post" action="{{url('configure/delivery-working-hours-dayoff')}}">
                <div class="form-group">
                  <label for="table-available" class="col-form-label datePicker__head">Pick start date and end date</label>

                  <div class="input__date-container">
                    <div class="fa fa-calendar" aria-hidden="true"></div>
                    <input class="input__date start__date" type="text" id="start_date" name="start_date" readonly="readonly" placeholder="Start date" required>
                  </div>

                  <div class="input__date-container">
                    <div class="fa fa-calendar" aria-hidden="true"></div>
                    <input class="input__date end__date" type="text" id="end_date" name="end_date" placeholder="End date"  readonly="readonly" required>
                  </div>

                  <div class="toggle__container">

                    <button type="button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom active" title="Open" data-toggle="button" aria-pressed="false" autocomplete="off">
                      <div class="handle"></div>
                    </button>

                  </div>
                  <div class="handle"></div>
                  <input type="hidden" id="schd_id" name="schd_id" value="0">
                  <input type="hidden" id="slot_id" name="slot_id" value="0">
                </div>
              </form>
              <div class="form-group">
                <div class="custom_modalHrs">
                  <div id='custom_calendar'></div>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <div type="button" class="btn_ btn__primary custom_done hide">Save</div>
          </div>
        </div>
      </div>
    </div>
    <!--modal-->
    <div class="modal fade" id="availabilityModal" tabindex="-1" role="dialog" aria-labelledby="availabilityModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form id="week_hours" action="{{url('configure/delivery-working-hours')}}">
            <div class="modal-head">
            </div>
            <div class="setting-gear">
              <img src="{{asset('images/gear-icon.png')}}" alt="gear" />
            </div>

            <div class="modal-body pt0">
              <div class="alert alert-danger" style="display:none"></div>
              <div class="form-group step1_div">
                <!--Step1-->
                <div class="step1 __step">

                  <div class='form-field'>
                    <label for="table-available" class="col-form-label">Slot Time:</label>
                    <div>
                      <input type="text" class="form-control" id="slot_time" name="slot_time" readonly>
                    </div>
                  </div>

                  <div class='form-field'>
                    <label for="table-available" class="col-form-label">Slot name<span class="mandatory-field">*</span> :</label>
                    <div>
                      <input type="text" class="form-control" id="slot_name" name="slot_name" maxlength="20" required>
                    </div>
                  </div>

               


                </div>


              
              </div>
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
              <input type="hidden" name="total_table" id="total_table" value="0">
              <input type="hidden" name="total_cover" id="total_cover" value="0">
            </div>
            <div class="modal-footer editable_footer">
              <button type="button" class="btn done__btn done"><span class="glyphicon glyphicon-send"></span> &nbsp;Done</button>
            </div>
            <div class="modal-footer non_editable_footer" style="display: none;">
              <button type="button" class="btn btn__primary edit"><span class="fa fa-pencil"></span>&nbsp;Edit</button>
              <button type="button" class="btn btn__primary deleteworkingslot deleteslot" ><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--modal-->
    
    <!-- Working Hour Modal -->
	<div class="modal  fade working--hours" id="workingHours" tabindex="-1" role="dialog" aria-labelledby="workingHours">
		<div class="modal-dialog" role="document">
			<div class="modal-content   full-content">

				<div class="modal-header">
				<a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
				
				</div>

				<div class="modal-body ">
					 <!--Popup Container-->
					 
                <div class="row border-row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <label for="">Shift Time</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">

                        <div class="input__fullWidth col-lg-4 col-md-4 col-sm-4 pull-right">
                            <input type="text" class="edit__field timepicker reservation-book" value="" name="reservation_time" id="book-a-table-tp" readonly\>
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </div>

                        <div class=" col-lg-3 col-md-3 col-sm-3 pull-right">
                            <div class="to-text">To</div>
                        </div>

                        <div class="input__fullWidth col-lg-4 col-md-4 col-sm-4 pull-right">
                            <input type="text" class="edit__field timepicker reservation-book" value="" name="reservation_time" id="book-a-table-tp" readonly\>
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </div>

                    </div>
                </div>
                
                 <div class="row border-row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <label for="">Shift Name</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">

                      <div class="input__field fullWidth">
                                <input class="shift_name" type="text" name="text" id="text">
                           
                            </div>

                      

                    </div>
                </div>
                
                <div class="row border-row">
                <div class="accordion" id="accordionExample">
              <div class="card">
    <div id="headingOne" class="card-header plus-btn collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
      <h5 class="mb-0">
        <button class="btn btn-link " type="button" >
          Modify Table Availability fo this shift
        </button>

                        <button type="button" class=" plus-btn collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseTwo" >
                      <i class="fa fa-plus" aria-hidden="true"></i>
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                
      </h5>
    </div>

    <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
  <div class="card-body">
      

<table class="modify-tbl-avail">
    <thead>
        <tr>
            <th width="25%">Table Type</th>
            <th width="25%">No of Table for Online Reservation</th>
            <th width="25%">Cover</th>
            <th width="25%">Turnover Time</th>
            
        </tr>
    </thead>

           <tbody>
                <tr>
                    <td>Reguler Tables <div> (2-4)</div></td>
                    <td>         <div class="selct-picker-plain">
                            <select class="selectpicker reservation-book" name="floor_id" id="floors">
                                <option value="">3</option>
                                <option value="">5</option>
                            </select>
                        </div></td>
                    <td><span>15</span></td>
                    <td>
                        <div class="edit__field">
                            <!--Value Box-->
                            <div class="number__box">
                                <div class="input-group">
                    <span class="input-group-btn">
                      <button data-type="minus" data-field="party_size" type="button" class="btn btn-danger btn-number minus-btn">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    </span>
                                    <input type="text" name="party_size" class="form-control input-number party_size reservation-book" min="2" max="10" value="2">
                                    <span class="input-group-btn">
                        <button type="button" class="btn btn-success btn-number plus-btn" data-type="plus" data-field="party_size">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    </span>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                   <tr>
                       <td>Large Tables <div>(6-8)</div></td>
                    <td>         <div class="selct-picker-plain">
                            <select class="selectpicker reservation-book" name="floor_id" id="floors">
                                <option value="">3</option>
                                <option value="">5</option>
                            </select>
                        </div></td>
                    <td><span>20</span></td>
                    <td>
                        <div class="edit__field">
                            <!--Value Box-->
                            <div class="number__box">
                                <div class="input-group">
                    <span class="input-group-btn">
                      <button data-type="minus" data-field="party_size" type="button" class="btn btn-danger btn-number minus-btn">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    </span>
                                    <input type="text" name="party_size" class="form-control input-number party_size reservation-book" min="2" max="10" value="2">
                                    <span class="input-group-btn">
                        <button type="button" class="btn btn-success btn-number plus-btn" data-type="plus" data-field="party_size">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    </span>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
            
            <tfoot>
                <tr>
                    <th>Total</th>
                    <th>8</th>
                    <th>35</th>
                    <th></th>
                </tr>
                
            </tfoot>
     </table>
     
     <div class="max-cover">
         Specify maximum cover for this shift <div class="input__field ">
                                <input type="text" name="text" id="text" value="35">
                           
                            </div>
         
     </div>
     
      </div>
    </div>
  </div>
            </div>
                </div>
                
                <div class="row border-row">
                <div class="accordion" id="accordionExample1">
              <div class="card">
    <div class="card-header plus-btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link " >
          Adjust Pacing
        </button>

                        <button type="button" class=" plus-btn collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                      <i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                
      </h5>
    </div>

    <div id="collapseTwo" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample1">
      <div class="card-body">
      
      <p>Modify Table availability for this shift </p>
      
      <div class="scroller">
<table class="modify-tbl-avail adjust--pacing">
    <thead>
        <tr>
            <th width="33.33%">Time</th>
            <th width="33.33%"> Reservation</th>
            <th width="33.33%">Cover</th>
         
            
        </tr>
    </thead>

           <tbody>
                <tr>
                    <td>
                          <div>
    <input class="styled-checkbox" id="equal-pacing" type="checkbox" value="value1" checked>
    <label for="equal-pacing">Equal Pacing</label>
  </div>
                    </td>
                    <td class="">        
                    
                      <div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div>
                    </td>
                    <td class="">
                        
                        
                      <div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div>
                    </td>
                </tr>
                   <tr class="hide">
                    <td>00:30</td>
                    <td>       <div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div></td>
                        
                    <td><div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div></td>
                </tr>
                   <tr class="hide">
                    <td>00:45</td>
                    <td>       <div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div></td>
                        
                    <td><div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div></td>
                </tr>
                   <tr class="hide">
                    <td>00:60</td>
                    <td>         <div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div></td>
                        
                    <td><div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div></td>
                </tr>
                   <tr class="hide">
                    <td>00:75</td>
                    <td>         <div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div></td>
                        
                    <td><div class="input__field fullWidth">
                                <input type="text" name="text" id="text">
                           
                            </div></td>
                </tr>
            </tbody>
            
    
     </table>
          </div>
      </div>
    </div>
  </div>
            </div>
                </div>
                
                <div class="row border-row">
                      <div class="col-lg-6 col-md-6 col-sm-6">
                        <label for="">Available Floors</label>
                    </div>
                       <div class="col-lg-6 col-md-6 col-sm-6">
                     <ul class="unstyled centered pull-right">
  <li>
    <input class="styled-checkbox" id="styled-checkbox-11" type="checkbox" value="value11">
    <label for="styled-checkbox-11">Main Floor</label>
  </li>
  <li>
    <input class="styled-checkbox" id="styled-checkbox-22" type="checkbox" value="value22" checked="">
    <label for="styled-checkbox-22">Bar</label>
  </li>
  <li>
    <input class="styled-checkbox" id="styled-checkbox-33" type="checkbox" value="value33" >
    <label for="styled-checkbox-33">Patio</label>
  </li>

</ul>
                        
                    </div>
                    </div>
                      <div class="row border-row">
                      <div class="col-lg-3 col-md-3 col-sm-3 text-left">
                        <label for="">Repeat for</label>
                    </div>
                       <div class="col-lg-9 col-md-9 col-sm-9 padd-zero">
                    <ul class="unstyled centered">
  <li>
    <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
    <label for="styled-checkbox-1">Sun</label>
  </li>
  <li>
    <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" checked="">
    <label for="styled-checkbox-2">Mon</label>
  </li>
  <li>
    <input class="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value3" >
    <label for="styled-checkbox-3">Tue</label>
  </li>
  <li>
    <input class="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value4">
    <label for="styled-checkbox-4">Wed</label>
  </li>
   <li>
    <input class="styled-checkbox" id="styled-checkbox-5" type="checkbox" value="value5">
    <label for="styled-checkbox-5">Thu</label>
  </li>
     <li>
    <input class="styled-checkbox" id="styled-checkbox-6" type="checkbox" value="value6">
    <label for="styled-checkbox-6">Fri</label>
  </li>
     <li>
    <input class="styled-checkbox" id="styled-checkbox-7" type="checkbox" value="value7">
    <label for="styled-checkbox-7">Sat</label>
  </li>
</ul>
                        
                    </div>
                    </div>
                    
                    <div class="form_field__container newWaitlist__btn-container rem-btn-border text-center">
                <button type="button" class="btn_ btn__primary save__btn " id="" button-name="Check Availability">
                    Done
                </button>

                </div>
                
				</div>

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->
    
    <div class="modal fade modal-popup" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
          </div>
          <div class="modal-body text-center desc">
            <p>You are about to delete a custom setting. Do you want to proceed?</p>
          </div>
          <div class="modal-footer">
            <a class="btn btn__primary btn-ok">Yes</a>
            <button type="button" class="btn btn__holo" data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>


    <!--modal-->
    <div class="modal fade modal-popup" id="slot-overlap-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Slot Overlap</h4>
          </div>
          <div class="modal-body text-center">
            <p>Please select the different time slot.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>

    <!--modal-->
    <div class="modal fade modal-popup" id="slot-overwrite-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Overlap Slot</h4>
          </div>
          <div class="modal-body text-center">
            <p>Please select the different time slot.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>

    <!--modal-->
    <div class="modal fade modal-popup" id="confirm-turnover-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Turnover Time Alert</h4>
          </div>
          <div class="modal-body text-center">
            <p>Kindly select a slot time greater than or equal to <span id="minTurnoverTime"></span> minutes.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="{{asset('js/jquery-ui-1.12.1.js')}}"></script>
  <script src="{{asset('js/schedular/moment.min.js')}}"></script>
  <script src="{{asset('js/schedular/fullcalendar.min.js')}}"></script>
  <script src="{{asset('js/schedular/scheduler.min.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.timepicker.css')}}" />
 <script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.js')}}"></script>
  <script src="{{asset('js/delivery_hours.js')}}"></script>
  
  
@endsection