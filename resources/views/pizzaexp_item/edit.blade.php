@extends('layouts.app')

@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">
            <div class="card">
                <div class="card-header">{{ __('Pizza Setting Item - Edit') }}</div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    {{ Form::model($pizzaSetting, array('route' => array('pizzaexpitem.update', $pizzaSetting->id), 'method' => 'PUT', 'files' => true)) }}
                    @csrf
                    <input type="hidden" name="item" value="{{ $item }}" />
                    <div class="form-group row">
                        <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                        <div class="col-md-6">
                            <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                                    required autofocus >
                                <option value="" >{{ __('Select restaurant') }}</option>
                                @foreach($allRests as $rests)
                                    <option value="{{ $rests->id }}" {{ (old('restaurant_id')==$rests->id || (count(old())==0 && $pizzaSetting->restaurant_id==$rests->id)) ? 'selected' :'' }}>{{ $rests->restaurant_name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('restaurant_id'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="setting" class="col-md-4 col-form-label text-md-right">{{ __('Setting Label') }}</label>
                        <div class="col-md-6">
                            <select name="setting" id="setting" class="form-control{{ $errors->has('setting') ? ' is-invalid' : '' }}"
                                    autofocus>
                                <option value="">{{ __('Select setting') }}</option>
                                <?php
                                $sizeCurst=[];
                                ?>
                                @foreach($parentSettingArr as $setArr)
                                    <?php
                                    if($setArr->label=='size'){
                                        $sizeCurst['size']=json_decode($setArr->setting);
                                    }
                                    elseif($setArr->label=='crust'){
                                        $sizeCurst['crust']=json_decode($setArr->setting);
                                    }

                                    ?>
                                    <option value="{{ $setArr->id }}" {{ (old('setting')==$rests->id || (count(old())==0 && $pizzaSetting->id==$setArr->id)) ? 'selected' :'' }}>{{ $setArr->label }}</option>
                                @endforeach

                            </select>
                            @if ($errors->has('setting'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('setting') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="label" class="col-md-4 col-form-label text-md-right">{{ __('Setting Item Label') }}</label>
                        <div class="col-md-6">
                            <input id="label" type="text" class="form-control{{ $errors->has('label') ? ' is-invalid' : '' }}" name="label" value="{{ count(old())==0 ? $editItem['label'] : old('label') }}" required autocomplete="off">
                            @if ($errors->has('label'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('label') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="setting" class="col-md-4 col-form-label text-md-right">{{ __('Size') }}</label>
                        <div class="col-md-6">
                            <select name="size" id="size" class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}"
                                    autofocus>
                                <option value="">{{ __('Select Size') }}</option>
                                <?php
                                /* $config=[];
                                 if(!empty($pizzaSetting->config)){
                                  $config=json_decode($pizzaSetting->config);

                                 }*/

                                if(count($sizeCurst) && isset($sizeCurst['size'])){

                                    foreach ($sizeCurst['size'] as $size){
                                        if( isset($editItem['size']) && $editItem['size']==$size->label){
                                            $selected='selected';
                                        }else{
                                            $selected='';

                                        }
                                        echo "<option  ".$selected." value='".$size->label."'>".$size->label."</option>";
                                    }

                                }

                                ?>
                            </select>
                            @if ($errors->has('size'))
                                <span class="invalid-feedback">
                            <strong>{{ $errors->first('size') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="setting" class="col-md-4 col-form-label text-md-right">{{ __('Crust') }}</label>
                        <div class="col-md-6">
                            <select name="crust" id="crust" class="form-control{{ $errors->has('crust') ? ' is-invalid' : '' }}"
                                    autofocus>
                                <option value="">{{ __('Select Crust') }}</option>

                                <?php
                                if(count($sizeCurst) && isset($sizeCurst['crust'])){

                                    foreach ($sizeCurst['crust'] as $crust){
                                        if(isset($editItem['crust']) && $editItem['crust']==$crust->label){
                                            $selected='selected';
                                        }else{
                                            $selected='';

                                        }
                                        echo "<option  ".$selected." value='".$crust->label."'>".$crust->label."</option>";
                                    }

                                }
                                ?>
                            </select>
                            @if ($errors->has('crust'))
                                <span class="invalid-feedback">
                            <strong>{{ $errors->first('crust') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
                        <div class="col-md-6">
                            <input id="price" type="text"
                                   class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price"
                                   value="{{ count(old())==0 ? $editItem['price'] : old('price') }}" autocomplete="off" required>
                            @if ($errors->has('price'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="pos_id" class="col-md-4 col-form-label text-md-right">{{ __('POS ID') }}</label>
                        <div class="col-md-6">
                            <input id="pos_id" type="text"
                                   class="form-control{{ $errors->has('pos_id') ? ' is-invalid' : '' }}" name="pos_id"
                                   value="{{ count(old())==0 ? @$editItem['pos_id'] : old('pos_id') }}"  >
                            @if ($errors->has('pos_id'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('pos_id') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Priority') }}</label>
                        <div class="col-md-6">
                            <input id="priority" type="text" class="form-control{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                                   name="priority" value="{{ count(old())==0 ? $editItem['priority'] : old('priority') }}" autocomplete="off" required>
                            @if ($errors->has('priority'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('priority') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                        <div class="col-md-4" style="padding-top: 10px;">
                            <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1' || (count(old())==0 && $editItem['status']=='1')) ? 'checked' :'' }}> Active
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0' || (count(old())==0 && $editItem['status']=='0')) ? 'checked' :'' }}> Inactive
                            @if ($errors->has('status'))
                                <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="is_selected" class="col-md-4 col-form-label text-md-right">{{ __('Selectable') }}</label>
                        <div class="col-md-4" style="padding-top: 10px;">
                            <input type="radio" name="is_selected" value="1" {{ (old('is_selected')=='1' || (count(old())==0 && $editItem['is_selected']=='1')) ? 'checked' :'' }}> Yes
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="is_selected" value="0" {{ (old('is_selected')=='0' || (count(old())==0 && $editItem['is_selected']=='0')) ? 'checked' :'' }}> No
                            @if ($errors->has('is_selected'))
                                <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('is_selected') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="is_spreadable" class="col-md-4 col-form-label text-md-right">{{ __('Spreadable') }}</label>
                        <div class="col-md-4" style="padding-top: 10px;">
                            <input type="radio" name="is_spreadable" value="1" {{ (old('is_spreadable')=='1' || (count(old())==0 && $editItem['is_spreadable']=='1')) ? 'checked' :'' }}> Yes
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="is_spreadable" value="0" {{ (old('is_spreadable')=='0' || (count(old())==0 && $editItem['is_spreadable']=='0')) ? 'checked' :'' }}> No
                            @if ($errors->has('is_spreadable'))
                                <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('is_spreadable') }}</strong>
                                        </span>
                            @endif
                        </div>
                    </div>
                    <br /><br />
                    <div class="form-group row">
                        <label for="base_image" class="col-md-4 col-form-label text-md-right">{{ __('Base/Bowl Image') }}</label>
                        <div class="col-md-4">
                            <input id="base_image" type="file" class="form-control{{ $errors->has('base_image') ? ' is-invalid' : '' }}"
                                   value="{{ old('base_image') }}" name="base_image" multiple>
                            @if ($errors->has('base_image'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('base_image') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['base_image']) && $editItem['image']['base_image']!='')
                            <div class="col-md-2">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['base_image']}}"><img src="{{ url('/').'/..'.$editItem['image']['base_image'] }}" style="width:100px;"></a>&emsp;
                                <input type="checkbox" name="base_image_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>

                    <div class="form-group row">
                        <label for="web_image" class="col-md-4 col-form-label text-md-right">{{ __('Web Image') }}</label>
                        <div class="col-md-4">
                            <input id="web_image" type="file" class="form-control{{ $errors->has('web_image') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="web_image" multiple>
                            @if ($errors->has('web_image'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('web_image') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['web_image']) && $editItem['image']['web_image']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['web_image']}}"><img src="{{ url('/').'/..'.$editItem['image']['web_image'] }}"></a>&emsp;
                                <input type="checkbox" name="web_image_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="mobile_image" class="col-md-4 col-form-label text-md-right">{{ __('Mobile Image') }}</label>
                        <div class="col-md-4">
                            <input id="mobile_image" type="file" class="form-control{{ $errors->has('mobile_image') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="mobile_image" multiple>
                            @if ($errors->has('mobile_image'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('mobile_image') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['image']) && $editItem['image']['image']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['image']}}">
					<img src="{{ url('/').'/..'.$editItem['image']['image'] }}"></a>&emsp;
                                <input type="checkbox" name="mobile_image_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>

                    {{-- 9 images left/whole/right & light/regular/extra combinations --}}
                    <br /><br />
                    <div class="form-group row">
                        <label for="light_left" class="col-md-4 col-form-label text-md-right">{{ __('Light Left Image') }}</label>
                        <div class="col-md-4">
                            <input id="light_left" type="file" class="form-control{{ $errors->has('light_left') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="light_left" multiple>
                            @if ($errors->has('light_left'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('light_left') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['light_left']) && $editItem['image']['light_left']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['light_left']}}"><img src="{{ url('/').'/..'.$editItem['image']['light_left'] }}"></a>&emsp;
                                <input type="checkbox" name="light_left_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="light_whole" class="col-md-4 col-form-label text-md-right">{{ __('Light Whole Image') }}</label>
                        <div class="col-md-4">
                            <input id="light_whole" type="file" class="form-control{{ $errors->has('light_whole') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="light_whole" multiple>
                            @if ($errors->has('light_whole'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('light_whole') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['light_whole']) && $editItem['image']['light_whole']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['light_whole']}}"><img src="{{ url('/').'/..'.$editItem['image']['light_whole'] }}"></a>&emsp;
                                <input type="checkbox" name="light_whole_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="light_right" class="col-md-4 col-form-label text-md-right">{{ __('Light Right Image') }}</label>
                        <div class="col-md-4">
                            <input id="light_right" type="file" class="form-control{{ $errors->has('light_right') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="light_right" multiple>
                            @if ($errors->has('light_right'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('light_right') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['light_right']) && $editItem['image']['light_right']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['light_right']}}"><img src="{{ url('/').'/..'.$editItem['image']['light_right'] }}"></a>&emsp;
                                <input type="checkbox" name="light_right_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="regular_left" class="col-md-4 col-form-label text-md-right">{{ __('Regular Left Image') }}</label>
                        <div class="col-md-4">
                            <input id="regular_left" type="file" class="form-control{{ $errors->has('regular_left') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="regular_left" multiple>
                            @if ($errors->has('regular_left'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('regular_left') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['regular_left']) && $editItem['image']['regular_left']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['regular_left']}}"><img src="{{ url('/').'/..'.$editItem['image']['regular_left'] }}"></a>&emsp;
                                <input type="checkbox" name="regular_left_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="regular_whole" class="col-md-4 col-form-label text-md-right">{{ __('Regular Whole Image') }}</label>
                        <div class="col-md-4">
                            <input id="regular_whole" type="file" class="form-control{{ $errors->has('regular_whole') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="regular_whole" multiple>
                            @if ($errors->has('regular_whole'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('regular_whole') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['regular_whole']) && $editItem['image']['regular_whole']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['regular_whole']}}"><img src="{{ url('/').'/..'.$editItem['image']['regular_whole'] }}"></a>&emsp;
                                <input type="checkbox" name="regular_whole_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="regular_right" class="col-md-4 col-form-label text-md-right">{{ __('Regular Right Image') }}</label>
                        <div class="col-md-4">
                            <input id="regular_right" type="file" class="form-control{{ $errors->has('regular_right') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="regular_right" multiple>
                            @if ($errors->has('regular_right'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('regular_right') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['regular_right']) && $editItem['image']['regular_right']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['regular_right']}}"><img src="{{ url('/').'/..'.$editItem['image']['regular_right'] }}"></a>&emsp;
                                <input type="checkbox" name="regular_right_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="extra_left" class="col-md-4 col-form-label text-md-right">{{ __('Extra Left Image') }}</label>
                        <div class="col-md-4">
                            <input id="extra_left" type="file" class="form-control{{ $errors->has('extra_left') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="extra_left" multiple>
                            @if ($errors->has('extra_left'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('extra_left') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['extra_left']) && $editItem['image']['extra_left']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['extra_left']}}"><img src="{{ url('/').'/..'.$editItem['image']['extra_left'] }}"></a>&emsp;
                                <input type="checkbox" name="extra_left_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="extra_whole" class="col-md-4 col-form-label text-md-right">{{ __('Extra Whole Image') }}</label>
                        <div class="col-md-4">
                            <input id="extra_whole" type="file" class="form-control{{ $errors->has('extra_whole') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="extra_whole" multiple>
                            @if ($errors->has('extra_whole'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('extra_whole') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['extra_whole']) && $editItem['image']['extra_whole']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['extra_whole']}}"><img src="{{ url('/').'/..'.$editItem['image']['extra_whole'] }}"></a>&emsp;
                                <input type="checkbox" name="extra_whole_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label for="extra_right" class="col-md-4 col-form-label text-md-right">{{ __('Extra Right Image') }}</label>
                        <div class="col-md-4">
                            <input id="extra_right" type="file" class="form-control{{ $errors->has('extra_right') ? ' is-invalid' : '' }}"
                                   value="{{ old('file') }}" name="extra_right" multiple>
                            @if ($errors->has('extra_right'))
                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('extra_right') }}</strong>
                                        </span>
                            @endif
                        </div>
                        @if(isset($editItem['image']['extra_right']) && $editItem['image']['extra_right']!='')
                            <div class="col-md-4">
                                <a data-fancybox="gallery" href="{{url('/').'/..'.$editItem['image']['extra_right']}}"><img src="{{ url('/').'/..'.$editItem['image']['extra_right'] }}"></a>&emsp;
                                <input type="checkbox" name="extra_right_delete" value="1" style="margin-left:20px;">Delete
                            </div>
                        @endif
                    </div>

                    <fieldset>

                        <legend>Position Images <a href="#" class="position_plus pull-right"><i class="fa fa-plus"></i></a></legend>
                        <div id="position_container">
                            <?php if(isset($editItem['positions'])){
                            foreach ($editItem['positions'] as $key=>$pos){
                            ?>
                            <div class="form-group row form_field__container position_container">
                                <input type="hidden" class="input__field" name="position_type_id[{{$key}}]"  value="<?php echo @$pos['id'];?>" placeholder="Type">
                                <div class="col-md-1">
                                    <div class=""><input type="text" class="input__field" name="position_pos_id[{{$key}}]"  value="<?php echo @$pos['pos_id'];?>" placeholder="POS ID"></div>
                                </div>
                                <div class="col-md-1">
                                    <div class=""><input type="text" class="input__field" name="position_type[{{$key}}]"  value="<?php echo $pos['type'];?>" placeholder="Type"></div>
                                </div>
                                <div class="col-md-1">
                                    <div class=""><input type="checkbox" class="" name="is_selected_type[{{$key}}]"  <?php if(isset($pos['is_selected_type']) && $pos['is_selected_type']){echo 'checked';}?> value="1">Default Type</div>
                                </div>

                                <div class="col-md-2">
                                    <div class=""><input type="text" class="" name="position[{{$key}}]" value="<?php echo $pos['position'];?>"  placeholder="position"></div>
                                </div>
                                <div class="col-md-2">
                                    <div class=""><input type="text" class="" name="position_price[{{$key}}]" value="<?php echo $pos['price'];?>"  placeholder="price"></div>
                                </div>
                                <div class="col-md-1">
                                    <div class=""><input type="checkbox" class="" name="is_selected_position[{{$key}}]"  <?php if(isset($pos['is_selected']) && $pos['is_selected']){echo 'checked';}?> value="1">Default Position</div>
                                </div>
                                <div class="col-md-2">
                                    <div class=""><input type="file" class="" name="position_image[{{$key}}]"  >
                                        @if(isset($pos['image']) && $pos['image']!='')
                                            <div class="">
                                                <a data-fancybox="gallery" href="{{url('/').'/..'.$pos['image']}}"><img  class="img-thumbnail" src="{{ url('/').'/..'.$pos['image'] }}"></a>&emsp;
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    {{--  <a href="#" class="position_plus"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;--}}

                                    <a href="#" class="position_minus"><i class="fa fa-minus"></i></a>

                                </div>
                            </div>
                            <?php
                            }
                            }
                            ?>

                        </div>
                    </fieldset>

                    <br /><br />
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">{{ __('Save Settings') }}</button>
                        </div>
                    </div>
                    </form>
                </div>


            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on('click','.position_plus',function(e){
                var lenofbox=$('.position_container').length;
                e.preventDefault();

                // $("#position_container").append('<div class="form-group row form_field__container position_container"> <div class="col-md-2"> <div class=""><input type="text" class="input__field" name="position_type['+lenofbox+']" placeholder="Type"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position['+lenofbox+']" placeholder="position"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position_price['+lenofbox+']" placeholder="price"></div> </div><div class="col-md-2"> <div class=""><input type="checkbox" class="" name="is_selected_position['+lenofbox+']" value="1">Default Position</div> </div> <div class="col-md-2"> <div class=""><input type="file" class="" name="position_image['+lenofbox+']"></div> </div> <div class="col-md-2"> &nbsp;&nbsp; <a href="#" class="position_minus"><i class="fa fa-minus"></i></a> </div> </div>');
                $("#position_container").append('<div class="form-group row form_field__container position_container"><div class="col-md-1"> <div class=""><input type="text" class="input__field" name="position_pos_id['+lenofbox+']" placeholder="POS ID"></div> </div> <div class="col-md-1"> <div class=""><input type="text" class="input__field" name="position_type['+lenofbox+']" placeholder="Type"></div> </div><div class="col-md-1"> <div class=""><input type="checkbox" class="" name="is_selected_type['+lenofbox+']" value="1">Default Type</div> </div>  <div class="col-md-2"> <div class=""><input type="text" class="" name="position['+lenofbox+']" placeholder="position"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position_price['+lenofbox+']" placeholder="price"></div> </div><div class="col-md-1"> <div class=""><input type="checkbox" class="" name="is_selected_position['+lenofbox+']" value="1">Default Position</div> </div> <div class="col-md-2"> <div class=""><input type="file" class="" name="position_image['+lenofbox+']"></div> </div> <div class="col-md-2"> &nbsp;&nbsp; <a href="#" class="position_minus"><i class="fa fa-minus"></i></a> </div> </div>');

            });

            $(document).on('click','.position_minus',function(e){

                e.preventDefault();
                $(this).parents('.position_container').empty().remove();
            });


            function getCrustAndSize(sizeCrust){
                console.error(sizeCrust);

                var sizes=sizeCrust.size;
                var crust=sizeCrust.crust;

                $.each(sizes, function (i, item) {
                    $('#size').append('<option value='+item.label+'>' + item.label + '</option>');

                });
                $.each(crust, function (i, item) {
                    $('#crust').append('<option value='+item.label+'>' + item.label + '</option>');

                });

            }

            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $('option:selected').val();
                if (rest_id != '') {
                    $('#parent_setting').find('option').not(':first').remove();
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_restaurant_settings"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
                        success: function (data) {
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj) !== 'undefined') {
                                    var sizeCrust={};
                                    $.each(data.dataObj, function (i, item) {
                                        console.log(item.id + ' ' + item.label);
                                        if(item.label=='crust'){
                                            sizeCrust.crust=JSON.parse(item.setting);
                                        }
                                        else if(item.label=='size'){
                                            sizeCrust.size=JSON.parse(item.setting);
                                        }
                                        $('#parent_setting').append('<option value="' + item.id + '">' + item.label + '</option>');
                                    });
                                    getCrustAndSize(sizeCrust);
                                }
                            }else{
                                $('#size').html('<option value="">Select Size</option>');
                                $('#crust').html('<option value="">Select Crust</option>');


                            }
                        }
                    });
                }
            });

        });
    </script>

@endsection


