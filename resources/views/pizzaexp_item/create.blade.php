@extends('layouts.app')

@section('content')
  <div class="main__container container__custom">
    <div class="reservation__atto">
      <div class="reservation__title">
        <h1>{{ __('Pizza Setting Item - Create') }}</h1>

      </div>

    </div>
    <div>

      <div class="card form__container1">
        @if(session()->has('message'))
          <div class="alert alert-success">
            {{ session()->get('message') }}
          </div>
        @endif
        <div class="card-body">
          <div class="form__user__edit">
            <form method="POST" action="{{ route('pizzaexpitem.store') }}" enctype="multipart/form-data">
              @csrf

              <div class="form-group row form_field__container">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                <div class="col-md-6">
                  <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>
                    <option value="">Select Restaurant</option>
                    @foreach($groupRestData as $rest)
                      <optgroup label="{{ $rest['restaurant_name'] }}">
                        @foreach($rest['branches'] as $branch)
                          <option value="{{ $branch['id'] }}" {{ ((old('restaurant_id')==$branch['id']) || (isset($menuCategory->restaurant_id) && $menuCategory->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                        @endforeach
                      </optgroup>
                    @endforeach
                  </select>
                  @if ($errors->has('restaurant_id'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="setting" class="col-md-4 col-form-label text-md-right">{{ __('Setting Label') }}</label>
                <div class="col-md-6">
                  <select name="setting" id="setting" class="form-control{{ $errors->has('setting') ? ' is-invalid' : '' }}"
                          autofocus>
                    <option value="">{{ __('Select setting') }}</option>
                  </select>
                  @if ($errors->has('setting'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('setting') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="label" class="col-md-4 col-form-label text-md-right">{{ __('Setting Item Label') }}</label>
                <div class="col-md-6">
                  <div class="input__field">
                    <input id="label" type="text" class="{{ $errors->has('label') ? ' is-invalid' : '' }}" name="label" value="" required autocomplete="off">
                  </div>
                  @if ($errors->has('label'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('label') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="setting" class="col-md-4 col-form-label text-md-right">{{ __('Size') }}</label>
                <div class="col-md-6">
                  <select name="size" id="size" class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}"
                          autofocus>
                    <option value="">{{ __('Select Size') }}</option>
                  </select>
                  @if ($errors->has('size'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('size') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="setting" class="col-md-4 col-form-label text-md-right">{{ __('Crust') }}</label>
                <div class="col-md-6">
                  <select name="crust" id="crust" class="form-control{{ $errors->has('crust') ? ' is-invalid' : '' }}"
                          autofocus>
                    <option value="">{{ __('Select Crust') }}</option>
                  </select>
                  @if ($errors->has('crust'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('crust') }}</strong>
                </span>
                  @endif
                </div>
              </div>

              <div class="form-group row form_field__container">
                <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
                <div class="col-md-6">
                  <div class="input__field">
                    <input id="price" type="text"
                           class="{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price"
                           value="1.00" required>
                  </div>
                  @if ($errors->has('price'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('price') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Priority') }}</label>
                <div class="col-md-6">
                  <div class="input__field">
                    <input id="priority" type="text" class="{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                           name="priority" value="{{ old('priority') }}" autocomplete="off" required>
                  </div>
                  @if ($errors->has('priority'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('priority') }}</strong>
                </span>
                  @endif
                </div>
              </div>

              <div class="form-group row form_field__container">
                <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                <div class="col-md-4" style="padding-top: 10px;">
                  <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1') ? 'checked' :'' }}> Active
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }}> Inactive
                  @if ($errors->has('status'))
                    <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="is_selected" class="col-md-4 col-form-label text-md-right">{{ __('Selectable') }}</label>
                <div class="col-md-4" style="padding-top: 10px;">
                  <input type="radio" name="is_selected" value="1" {{ (old('is_selected')=='1') ? 'checked' :'' }}> Yes
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" name="is_selected" value="0" {{ (old('is_selected')=='0') ? 'checked' :'' }}> No
                  @if ($errors->has('is_selected'))
                    <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('is_selected') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="is_spreadable" class="col-md-4 col-form-label text-md-right">{{ __('Spreadable') }}</label>
                <div class="col-md-4" style="padding-top: 10px;">
                  <input type="radio" name="is_spreadable" value="1" {{ (old('is_spreadable')=='1') ? 'checked' :'' }}> Yes
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" name="is_spreadable" value="0" {{ (old('is_spreadable')=='0') ? 'checked' :'' }}> No
                  @if ($errors->has('is_spreadable'))
                    <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('is_spreadable') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <br /><br />
              <div class="form-group row form_field__container">
                <label for="base_image" class="col-md-4 col-form-label text-md-right">{{ __('Base/Bowl Image') }}</label>
                <div class="col-md-6">
                  <input id="base_image" type="file" class="form-control{{ $errors->has('base_image') ? ' is-invalid' : '' }}"
                         value="{{ old('base_image') }}" name="base_image" multiple>
                  @if ($errors->has('base_image'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('base_image') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="web_image" class="col-md-4 col-form-label text-md-right">{{ __('Web Image') }}</label>
                <div class="col-md-6">
                  <input id="web_image" type="file" class="form-control{{ $errors->has('web_image') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="web_image" multiple>
                  @if ($errors->has('web_image'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('web_image') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="mobile_image" class="col-md-4 col-form-label text-md-right">{{ __('Mobile Image') }}</label>
                <div class="col-md-6">
                  <input id="mobile_image" type="file" class="form-control{{ $errors->has('mobile_image') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="mobile_image" multiple>
                  @if ($errors->has('mobile_image'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('mobile_image') }}</strong>
                </span>
                  @endif
                </div>
              </div>

              {{-- 9 images left/whole/right & light/regular/extra combinations --}}
              <br /><br />
              <div class="form-group row form_field__container">
                <label for="light_left" class="col-md-4 col-form-label text-md-right">{{ __('Light Left Image') }}</label>
                <div class="col-md-6">
                  <input id="light_left" type="file" class="form-control{{ $errors->has('light_left') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="light_left" multiple>
                  @if ($errors->has('light_left'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('light_left') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="light_whole" class="col-md-4 col-form-label text-md-right">{{ __('Light Whole Image') }}</label>
                <div class="col-md-6">
                  <input id="light_whole" type="file" class="form-control{{ $errors->has('light_whole') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="light_whole" multiple>
                  @if ($errors->has('light_whole'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('light_whole') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="light_right" class="col-md-4 col-form-label text-md-right">{{ __('Light Right Image') }}</label>
                <div class="col-md-6">
                  <input id="light_right" type="file" class="form-control{{ $errors->has('light_right') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="light_right" multiple>
                  @if ($errors->has('light_right'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('light_right') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="regular_left" class="col-md-4 col-form-label text-md-right">{{ __('Regular Left Image') }}</label>
                <div class="col-md-6">
                  <input id="regular_left" type="file" class="form-control{{ $errors->has('regular_left') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="regular_left" multiple>
                  @if ($errors->has('regular_left'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('regular_left') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="regular_whole" class="col-md-4 col-form-label text-md-right">{{ __('Regular Whole Image') }}</label>
                <div class="col-md-6">
                  <input id="regular_whole" type="file" class="form-control{{ $errors->has('regular_whole') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="regular_whole" multiple>
                  @if ($errors->has('regular_whole'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('regular_whole') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="regular_right" class="col-md-4 col-form-label text-md-right">{{ __('Regular Right Image') }}</label>
                <div class="col-md-6">
                  <input id="regular_right" type="file" class="form-control{{ $errors->has('regular_right') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="regular_right" multiple>
                  @if ($errors->has('regular_right'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('regular_right') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="extra_left" class="col-md-4 col-form-label text-md-right">{{ __('Extra Left Image') }}</label>
                <div class="col-md-6">
                  <input id="extra_left" type="file" class="form-control{{ $errors->has('extra_left') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="extra_left" multiple>
                  @if ($errors->has('extra_left'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('extra_left') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="extra_whole" class="col-md-4 col-form-label text-md-right">{{ __('Extra Whole Image') }}</label>
                <div class="col-md-6">
                  <input id="extra_whole" type="file" class="form-control{{ $errors->has('extra_whole') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="extra_whole" multiple>
                  @if ($errors->has('extra_whole'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('extra_whole') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="form-group row form_field__container">
                <label for="extra_right" class="col-md-4 col-form-label text-md-right">{{ __('Extra Right Image') }}</label>
                <div class="col-md-6">
                  <input id="extra_right" type="file" class="form-control{{ $errors->has('extra_right') ? ' is-invalid' : '' }}"
                         value="{{ old('file') }}" name="extra_right" multiple>
                  @if ($errors->has('extra_right'))
                    <span class="invalid-feedback">
                  <strong>{{ $errors->first('extra_right') }}</strong>
                </span>
                  @endif
                </div>
              </div>

              <fieldset>
                <legend>Position Images</legend>
                <div id="position_container">
                  <div class="form-group row form_field__container position_container">

                    <div class="col-md-2">
                      <div class=""><input type="text" class="input__field" name="position_type[0]"  placeholder="Type"></div>
                    </div>
                    <div class="col-md-1">
                      <div class=""><input type="checkbox" class="" name="is_selected_type[0]"  value="1"></br>Default Type</div>
                    </div>

                    <div class="col-md-2">
                      <div class="">

                        <input type="text" class="" name="position[0]"  placeholder="position">


                        {{--Make dropdown with Left,Right,WHole--}}

                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class=""><input type="text" class="" name="position_price[0]"  placeholder="price"></div>
                    </div>
                    <div class="col-md-1">
                      <div class=""><input type="checkbox" class="" name="is_selected_position[0]"  value="1"></br>Default Position</div>
                    </div>
                    <div class="col-md-2">
                      <div class=""><input type="file" class="" name="position_image[0]"  ></div>
                    </div>
                    <div class="col-md-2">
                      <a href="#" class="position_plus"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;
                      {{--
                                          <a href="#" class="position_tags_minus"><i class="fa fa-minus"></i></a>
                      --}}
                    </div>
                  </div>

                </div>
              </fieldset>

              <br /><br />
              <div class="row text-center mb-0">
                <div class="col-md-12 text-center">
                  <button type="submit" class="btn btn__primary">{{ __('Add Setting Item') }}</button>
                </div>
              </div>
            </form>
          </div>
        </div>


      </div>
    </div>
  </div>

  <script type="text/javascript">
    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $(document).on('click','.position_plus',function(e){
        var lenofbox=$('.position_container').length;
        e.preventDefault();
        $("#position_container").append('<div class="form-group row form_field__container position_container"> <div class="col-md-2"> <div class=""><input type="text" class="input__field" name="position_type['+lenofbox+']" placeholder="Type"></div> </div><div class="col-md-1"> <div class=""><input type="checkbox" class="" name="is_selected_type['+lenofbox+']" value="1">Default Type</div> </div>  <div class="col-md-2"> <div class=""><input type="text" class="" name="position['+lenofbox+']" placeholder="position"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position_price['+lenofbox+']" placeholder="price"></div> </div><div class="col-md-1"> <div class=""><input type="checkbox" class="" name="is_selected_position['+lenofbox+']" value="1">Default Position</div> </div> <div class="col-md-2"> <div class=""><input type="file" class="" name="position_image['+lenofbox+']"></div> </div> <div class="col-md-2"> &nbsp;&nbsp; <a href="#" class="position_minus"><i class="fa fa-minus"></i></a> </div> </div>');

      });

      $(document).on('click','.position_minus',function(e){

        e.preventDefault();
        $(this).parents('.position_container').empty().remove();
      });
      function getCrustAndSize(sizeCrust){
        console.error(sizeCrust);

        var sizes=sizeCrust.size;
        var crust=sizeCrust.crust;

        $.each(sizes, function (i, item) {
          $('#size').append('<option value=' + item.label + '>' + item.label + '</option>');

        });
        $.each(crust, function (i, item) {
          $('#crust').append('<option value=' + item.label + '>' + item.label + '</option>');

        });

      }

      $("#restaurant_id").change(function () {
        var rest_id = '';
        rest_id = $('option:selected').val();
        if (rest_id != '') {
          $('#setting').find('option').not(':first').remove();
          $.ajax({
            type: 'POST',
            url: '<?php echo URL::to('/') . "/get_restaurant_settings"; ?>',
            data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
            success: function (data) {
              if ($.type(data.dataObj) !== 'undefined') {
                if ($.type(data.dataObj) !== 'undefined') {
                  var sizeCrust={};
                  $.each(data.dataObj, function (i, item) {
                    console.log(item.id + ' ' + item.label);
                    if(item.label=='crust'){
                      sizeCrust.crust=JSON.parse(item.setting);
                    }
                    else if(item.label=='size'){
                      sizeCrust.size=JSON.parse(item.setting);
                    }
                    $('#setting').append('<option value="' + item.id + '">' + item.label + '</option>');
                  });

                  getCrustAndSize(sizeCrust);

                }
              }else{
                $('#size').html('<option value="">Select Size</option>');
                $('#crust').html('<option value="">Select Crust</option>');


              }
            }
          });
        }
      });

    });
  </script>

@endsection
