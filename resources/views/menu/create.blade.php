@extends('layouts.app')

@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">

            <div class="card">
                <div class="card-header">{{ __('Restaurant Menus') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('static.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                            <div class="col-md-6">
                                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" >
                                    <option value="">Select restaurant</option>
                                    @foreach($restaurantData as $rest)
                                        <option value="{{ $rest->id }}">{{ $rest->restaurant_name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('restaurant_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="menu_categories" class="col-md-4 col-form-label text-md-right">{{ __('Menu Categories') }}</label>
                            <div class="col-md-6">
                                <select name="menu_categories" id="menu_categories" class="form-control{{ $errors->has('menu_categories') ? ' is-invalid' : '' }}" onchange="getPageData(this);">
                                    <option value="">Select page type</option>
                                    {{--@foreach ($staticPageType as $sp)
                                        <option value="{{ $sp }}">{{ $sp }}</option>
                                    @endforeach--}}
                                </select>
                                @if ($errors->has('menu_categories'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('menu_categories') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="page_heading" class="col-md-4 col-form-label text-md-right">{{ __('Page Heading') }}</label>
                            <div class="col-md-6">
                                <input id="page_heading" type="text" class="form-control{{ $errors->has('page_heading') ? ' is-invalid' : '' }}"
                                       value="{{ old('page_heading') }}" name="page_heading" required>
                                @if ($errors->has('page_heading'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('page_heading') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="page_sub_heading" class="col-md-4 col-form-label text-md-right">{{ __('Page Sub Heading') }}</label>
                            <div class="col-md-6">
                                <input id="page_sub_heading" type="text" class="form-control{{ $errors->has('page_sub_heading') ? ' is-invalid' : '' }}"
                                       value="{{ old('page_sub_heading') }}" name="page_sub_heading">
                                @if ($errors->has('page_sub_heading'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('page_sub_heading') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="page_content" class="col-md-4 col-form-label text-md-right">{{ __('Page Content') }}</label>
                            <div class="col-md-6">
                                <textarea name="page_content" id="page_content" class="form-control{{ $errors->has('page_content') ? ' is-invalid' : '' }}"
                                          cols="30" rows="10" class="form-control{{ $errors->has('page_content') ? ' is-invalid' : '' }}" required>{{ old('page_content') }}</textarea>
                                @if ($errors->has('page_content'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('page_content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Priority') }}</label>
                            <div class="col-md-6">
                                <input id="priority" type="text" class="form-control{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                                       name="priority" value="{{ old('priority') }}" required>
                                @if ($errors->has('priority'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('priority') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <button type="button" class="image-caption-button btn btn-primary" onclick="addFileDiv(this);">{{ __('+') }}</button>
                        <button type="button" class="image-caption-button btn btn-primary" onclick="removeFileDiv(this);">{{ __('-') }}</button>
                        <div class="form-group row source-image-div image-caption-div">
                            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>
                            <div class="col-md-5">
                                <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                                       value="{{ old('file') }}" name="image[]">
                                @if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                                <input id="image_caption" type="text" class="form-control{{ $errors->has('image_caption') ? ' is-invalid' : '' }}"
                                       name="image_caption[]" value="{{ old('image_caption') }}" placeholder="Image Caption" required>
                                @if ($errors->has('image_caption'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image_caption') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <button type="button" class="video-caption-button btn btn-primary" onclick="addFileDiv(this);">{{ __('+') }}</button>
                        <button type="button" class="video-caption-button btn btn-primary" onclick="removeFileDiv(this);">{{ __('-') }}</button>
                        <div class="form-group row source-video-div video-caption-div">
                            <label for="video" class="col-md-4 col-form-label text-md-right">{{ __('Video') }}</label>
                            <div class="col-md-5">
                                <input id="video" type="file" class="form-control{{ $errors->has('video') ? ' is-invalid' : '' }}"
                                       value="{{ old('file') }}" name="video[]">
                                @if ($errors->has('video'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
                                @endif
                                <input id="video_caption" type="text" class="form-control{{ $errors->has('video_caption') ? ' is-invalid' : '' }}"
                                       name="video_caption[]" value="{{ old('video_caption') }}" placeholder="Video Caption" required>
                                @if ($errors->has('video_caption'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('video_caption') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <br /><br />
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Create Page') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        var restPageData = '';
        function getRestaurantMenus(sel) {
            var id = 'restaurant/'+sel.value;
            var ajaxurl = id;
            $("#page_heading").val('');
            $("#page_sub_heading").val('');
            $("#page_content").val('');
            $("#priority").val('');
            $("#image").val('');
            $.ajax({
                url: ajaxurl,
                type: "GET",
                success: function(data){
                    console.log(data);
                    restPageData = data;
                    var optionHtml = '<option value="">Select page type</option>';
                    data.forEach(function(dt) {
                        optionHtml += '<option value="' + dt.page_type + '">' + dt.page_type + '</option>';
                    });
                    console.log(optionHtml);
                    $("#page_type").empty().append(optionHtml);
                }
            });
        }
        function getPageData(sel) {
            console.log(restPageData);
            restPageData.forEach(function(dt) {
                breakOut = false;
                if(dt.page_type == sel.value) {
                    console.log('ma' + dt.page_type + ' - ' + sel.value);
                    $("#page_heading").val(dt.page_heading);
                    $("#page_sub_heading").val(dt.page_sub_heading);
                    $("#page_content").val(dt.page_content);
                    $("#priority").val(dt.priority);
                    breakOut = true;
                } else {
                    console.log('nm' + dt.page_type + ' - ' + sel.value);
                }
                if(breakOut) return false;
            });
        }
        function addFileDiv(sel) {
            var type = $(sel).hasClass('image-caption-button') ? 'image' : 'video';

            var divHtml = $('.source-'+type+'-div').html(); // $('.image-caption-div')[0].outerHTML;
            $('.'+type+'-caption-div').last().after('<div class="form-group row '+type+'-caption-div">'+divHtml+'</div>');
        }
        function removeFileDiv(sel) {
            var type = $(sel).hasClass('image-caption-button') ? 'image' : 'video';

            $('.'+type+'-caption-div').last().remove();
        }
    </script>

@endsection
