@extends('layouts.app')

@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">
            <div class="card">
                <div class="card-header">
                    {{ __('Restaurant Menus') }}
                    <span style="float: right;">()</span>
                </div>

                <div class="card-body">
                    <table>
                        <tr>
                            <th style="width: 3%">ID</th>
                            <th style="width: 16.66%">Restaurant</th>
                            <th style="width: 10%">Menu</th>
                            <th style="width: 3%">Priority</th>
                            <th style="width: 16.66%">Image</th>
                        </tr>
                        @if($menuData)
                            @foreach($menuData as $menu)
                                @php
                                    $images = '';
                                    if($sp->image)
                                        $images = implode(" | ", array_column(json_decode($sp->image, true), 'image'));
                                @endphp
                                <tr>
                                    <td style="width: 3%">{{ $sp->id }}</td>
                                    <td style="width: 16.66%">{{ $sp->restaurant_name }}</td>
                                    <td style="width: 10%">{{ $sp->page_heading }}</td>
                                    <td style="width: 3%">{{ $sp->priority }}</td>
                                    <td style="width: 16.66%">{{ $images }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection