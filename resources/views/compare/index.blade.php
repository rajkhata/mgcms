@extends('layouts.app')


@section('content')
<div class="main__container container__custom clearfix">
    <div class="inner__container">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1 class="module__title">Branch Compare</h1>
            </div>
            <!-- <div>
                <a class="btn btn__primary" href="{{ route('permissions.index') }}"> Back</a>
            </div> -->
        </div>

        <div class="text-align-right">
            <a class="btn btn__primary" href="{{ route('permissions.index') }}"> Back</a>
        </div>
        <div class="_block" style="margin-top:20px;border:1px solid #999999; width:100%;background:#ffffff">
            @if(isset($parentRestaurant) && !empty($parentRestaurant[0]))
            <div class="form-group" style="border-bottom: 1px solid #999999;height:40px;text-align:center;padding-top:10px; background-color:#cccccc">   
                <strong>Brand:</strong>
                <span style='margin: 10px;'>{{ ucfirst($parentRestaurant[0]['restaurant_name']) }}</span>
                <span >( {{$parentRestaurant[0]['address']}} </span>
                <span >, {{$parentRestaurant[0]['street']}} </span>
                <span >, {{$parentRestaurant[0]['zipcode']}} )</span>
            </div>




            {!! Form::model($parentRestaurant, ['method' => 'PATCH','route' => ['compare.update', $parentRestaurant[0]['id']]]) !!}
            <div class="guestbook__container fullWidth box-shadow margin-top-20 margin-bottom-20" style="padding:10px!important;">
                @if(isset($branchData) && !empty($branchData))

                <div class="_block">
                    <div id="pannelBox" class="panel panel-default">
                        
                        <div class="panel-heading permission__title">
                             <div class="option__wrapper">
                                <label class="custom_checkbox relative padding-left-25 margin-left-5 font-weight-600">
                                    {{ Form::radio('globalRadiobox', 's', true, array('class' => 'globalRadiobox','id'=>'service_1')) }}
                                    Service
                                    
                                </label>
                                {!! Form::close() !!}
                            </div>
                            
                            <div class="option__wrapper">
                                <label class="custom_checkbox relative padding-left-25 margin-left-5 font-weight-600">
                                    {{ Form::radio('globalRadiobox', 'm', false, array('class' => 'globalRadiobox','id'=>'service_2')) }}
                                    Menu
                                   
                                </label>
                                {!! Form::close() !!}
                            </div>
                            
                            
                            
                            <div class="option__wrapper">
                                <label class="custom_checkbox relative padding-left-25 margin-left-5 font-weight-600">
                                    {{ Form::radio('globalRadiobox', 'p', false, array('class' => 'globalRadiobox','id'=>'service_3')) }}
                                    Profile
                                   
                                </label>
                                {!! Form::close() !!}
                            </div>
                            
                            
                        </div>
                        
                        <div class="panel-heading permission__title">
                            <strong>Branches</strong>

                        </div>
                        <div class="panel-body">  
                            
<!--                            <div class="option__wrapper">
                                <label class="custom_checkbox relative padding-left-25 margin-left-5 font-weight-600">
                                    {{ Form::checkbox('globalCheckbox', 'all', '', array('class' => 'hide globalCheckbox')) }}
                                    All
                                    <span class="control_indicator"></span>
                                </label>
                                {!! Form::close() !!}
                            </div>-->

                            @foreach($branchData as $key =>$val)
                            <div class="option__wrapper">
                                <label class="custom_checkbox relative padding-left-25 margin-left-5 font-weight-600">
                                    {{ Form::checkbox('comparebranch[]', $val['id'], in_array($val['id'], $branchId) ? true : false, array('class' => 'hide permission compare_active', 'id'=>'branch_'.$val['restaurant_name'],'branch_name'=> $val['restaurant_name'])) }}
                                    {{ $val['restaurant_name'] }} 
                                    <span class="control_indicator"></span>
                                </label>
                            </div>

                            @endforeach
                        </div>
                    </div>
<!--                    <div class="_block text-center padding-bottom-10">
                        <button type="button" class="btn btn__primary compare_active">Submit</button>
                    </div>-->
                </div>          

                @endif
                {!! Form::close() !!}


                @else 
                <div class="form-group" style="height:30px;text-align:center;padding-top:10px;">
                    <strong>Record not found</strong>                    
                </div>
                @endif
            </div>

            <div id="menu_data">
                <div id ="loader" class="hide"></div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {

//        if ($(".permission").length == $(".permission:checked").length) {
//            $(".globalCheckbox").prop("checked", true);
//        } else {
//            $(".globalCheckbox").prop("checked", false);
//        }
//
//        $('.globalCheckbox').click(function(){
//            if ($(this).prop("checked")) {
//                $(".permission").prop("checked", true);
//            } else {
//                $(".permission").prop("checked", false);
//            }
//        });
//        $('.permission').click(function(){
//            if ($(".permission").length == $(".permission:checked").length) {
//                $(".globalCheckbox").prop("checked", true);
//            } else {
//                $(".globalCheckbox").prop("checked", false);
//            }
//        });
        $('.compare_active').on('click', function() {
            
            var branch_id = [];
             branch_id.push($(this).val());
             branch_name = $(this).attr('branch_name');
            
//            $.each($(".permission:checked"), function(){
//                branch_id.push($(this).val());
//            });
        
        
        
        if(branch_id.length == 1){
            var compareService = $("input[name='globalRadiobox']:checked"). val();
           //if($('#branch_'+branch_name).is(":checked")){
           if(compareService=='m'){
            var request = $.ajax({
                url: "compare/category",
                type: "POST",
                data: { comparebranch : branch_id,compareWith:compareService },
                success: function(response)
                {
                    var str = "";
                    
                    if(compareService=='m'){
                    $.each(response.category, function(ckey, cdata){
                        
                       
                        str += "<div style='width:300px; float:left;' id='"+branch_name+"'>";
                        str += "<div style='width:300px;border-bottom:2px solid #999999;'><h2>"+branch_name+"</h2></div>";
                        $.each(cdata, function(dkey, ddata){
                            $.each(ddata, function(ddkey, dddata){
                                str += "<div style='color:#ff8700; font-size: 24px;'><strong>-" + dddata.name + "</strong></div>";
                                if (!$.isEmptyObject(dddata.subcats)){
                                    
                                    $.each(dddata.subcats, function(subkey, subval){
                                        str += "<div style='margin-left:15px;font-size: 12px;color:red'><strong>-" + subval.name + "</strong></span></div>";
                                         if(!$.isEmptyObject(subval.menu_items_list)){
                                             $.each(subval.menu_items_list, function(sikey, sival){
                                             //console.log(sival);
                                             
                                              str += "<div style='margin-left:25px;font-size: 10px;'>" + sival.name + "</span></div>";
                                              if(!$.isEmptyObject(sival.modifier)){
                                                    $.each(sival.modifier, function(imkey, imval){
                                                    str += "<div style='margin-left:35px;font-size: 10px;color:green'>" + imval.group_name + "</span></div>";
                                                     str += "<div style='margin-left:35px;font-size: 10px;color:green'>" + imval.prompt + "</span></div>";
                                                });
                                               }
                                            
                                            });  
                                         }
                                    });
                                }else if(!$.isEmptyObject(dddata.menu_items_list)){                                    
                                      $.each(dddata.menu_items_list, function(ikey, ival){
                                        str += "<div style='margin-left:15px;font-size: 10px;'>" + ival.name + "</span></div>";
                                        if(!$.isEmptyObject(ival.modifier)){
                                            $.each(ival.modifier, function(imkey, imval){
                                                str += "<div style='margin-left:35px;font-size: 10px;color:green'>" + imval.group_name + "</span></div>";
                                                str += "<div style='margin-left:35px;font-size: 10px;color:green'>" + imval.prompt + "</span></div>";
                                            });
                                        }
                                         
                                    });   
                                }else{
                                    str += "<div style='margin-left:15px;'>Items details not available.</div>";
                                }
                            });
                        });
                        str += "</div>";
                        
                    });
                    
                    }else if(compareService=='p'){
                        $.each(response.profile, function(pkey, profiles){
                            if($('#branch_'+branch_name).is(":checked")){  
                            str += "<div style='width:300px; border:2px solid #000000;float:left;margin-left:30px; ' id='"+branch_name+"'>";
                            str += "<div style='width:298px;border-bottom:1px solid red;text-align:center;height:50px;'><h2>"+branch_name+"</h2></div>";
                            str += "<div style='width:298px;border-bottom:1px solid red;margin-top:1px;'></div>";
                             $.each(profiles, function(key, data){
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Title :</strong> " + data.title + "</div>"; 
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Restaurant Name :</strong> " + data.restaurant_name + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Email : </strong>" + data.email + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Phone : </strong>" + data.phone + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Address : </strong>" + data.address + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Zip Code : </strong>" + data.zipcode + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Description : </strong>" + data.description + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Currency Code : </strong>" + data.currency_code + "</div>";
                                 
                                
                                 
                             });
                             str += "</div>";
                             }else{
                                $( "div" ).remove( "#"+branch_name );
                                return false;
                             }
                        });
                    }else{
                        $.each(response.service, function(skey, services){ 
                        
                            if($('#branch_'+branch_name).is(":checked")){                              
                            
                            str += "<div style='width:300px; border:2px solid #000000;float:left;margin-left:30px; ' id='"+branch_name+"'>";
                            str += "<div style='width:298px;border-bottom:1px solid red;text-align:center;height:50px;'><h2>"+branch_name+"</h2></div>";
                            str += "<div style='width:298px;border-bottom:1px solid red;margin-top:1px;'></div>";
                             $.each(services, function(key, data){
                                  delivery = (data.delivery==1)?"Yes":"No";
                                  takeout = (data.takeout==1)?"Yes":"No";
                                  reservation = (data.reservation==1)?"Yes":"No";
                                  dmessage = (data.delivery_custom_message==null)?"No":data.delivery_custom_message;
                                  tmessage = (data.takeout_custom_message==null)?"No":data.takeout_custom_message;
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Delivery :</strong> " + delivery + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Takeout : </strong>" + takeout + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Reservation : </strong>" + reservation + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Reservation : </strong>" + reservation + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Delivery Message : </strong>" + dmessage + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Takeout Message : </strong>" + tmessage + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Tax : </strong>" + data.tax + "</div>";
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom: 2px solid #999999;'> <strong>Service Tax : </strong>" + data.service_tax + "</div>";
                                 
                                 str += "<div style='padding:20px 0px 0px 40px;border-bottom:1px'> <strong>Tip : </strong>" + data.tip + "</div>";
                                
                                 
                             });
                             str += "</div>";
                             }else{                    
                               
                                $( "div" ).remove( "#"+branch_name );
                                return false;
                            
                             }
                        });
                        
                    }
                    //jQuery('#menu_data').html('');
                    $("#menu_data").append(str);

                },
                    dataType: "json"//set to JSON    
            });
            
        }else{
            alert("Please select two branches for compare.","defaultText");
            return false;
        }
        });
        });

        //Get Category Items
        $(document).on("click", '[id^="cat_"]', function(){
            var cat_id = $(this).attr('id').split("_")[1];
            var restaurant_id = $(this).attr('id').split("_")[2];
            var category_name = $(this).text().split(" ")[1];
            
            var request = $.ajax({
                url: "compare/menu/" + cat_id,
                type: "GET",
                data: { restaurant_id : restaurant_id },
                success: function(response)
                {
                   
                    var str = "";
                    
                    //str += "<div><span id='minus_cat_" + cat_id + "_" + restaurant_id + "' data-val='" + cat_id + "_" + restaurant_id + "' style='margin:10px;cursor:pointer'>- " + category_name + "</span></div>";
                    $.each(response.items, function(mkey, mdata){                   
                        console.log(mdata.name);
                        str += "<div style='margin-left:25px;>" + mdata.name + "</div>";
                       
                    });
                    str += "</div>";
                    
                   $(str).insertAfter("#cat_"+cat_id+"_"+restaurant_id);
                   //$("#cat_"+cat_id+"_"+restaurant_id).remove();
                
                },
                dataType: "json"//set to JSON    
            });
           
            
        });
        
        //Get Subcategory Items
        $(document).on("click", '[id^="sub_"]', function(){
            var sub_cat_id = $(this).attr('id').split("_")[1];
            var restaurant_id = $(this).attr('id').split("_")[2];
        });
        
        
        
        
        $(document).on("click", '[id^="minus_cat_"]', function(){
            var cat_id = $(this).attr('id').split("_")[1];
            var restaurant_id = $(this).attr('id').split("_")[2];
            var category_name = $(this).text().split(" ")[1];
            alert("hello sud");
            $(this).text("+ "+category_name);
            $(this).siblings().css( "display", "none" );
            //$("<div><span id='cat_" + cat_id + "_" + restaurant_id + "' data-val='" + cat_id + "_" + restaurant_id + "' style='margin:10px;cursor:pointer'> " + category_name + "</span></div>").insertAfter("#minus_cat_"+cat_id+"_"+restaurant_id);
            
        });
        
        $(document).on("click", '[id^="service_"]', function(){
            $('#menu_data').html('');
            $(".compare_active").prop("checked", false);
        });
    
    </script>
    @endsection

