@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

    <div class="main__container container__custom_working">

        <div class="row">
            <!--Header-->
            <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
            <div class="reservation__atto flex-direction-row">
                <div class="reservation__title margin-top-5">
                    <h1>Website</h1>
                </div>

                <div class="selct-picker-plain margin-left-30 width-120">
                    <select class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                        <option value="0" selected>Last 30 Days</option>
                        <option value="1">Yesterday</option>
                        <option value="2">Today</option>
                    </select>
                </div>

            </div>

      <div class="row margin-top-30">
          @if(session()->has('message'))
              <div class="alert alert-success margin-top-30">
                  {{ session()->get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
          @endif
                <div class="menuItem__db__stepper">

                    <ul class="stepperContainer flex-box overflow-visible flex-wrap">

                        <li class="step active flex-box flex-justify-center white-box box-shadow">
                            <div class="step__wrapper flex-justify-space-between">
                                <div class="stepContainer">
                                    <i class="icon-tick-mark" aria-hidden="true"></i>
                                </div>
                                <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                    <p>
                                       <span class="icon-add-general-Info"></span>
                                        Add General Info<br/>About Your Business
                                    </p>
                                    <a href="/general-info-setup"  class="btn btn__holo width-120">Add Info</a>
                                </div>
                            </div>
                        </li>

                    <!--li class="step active flex-box flex-justify-center white-box box-shadow">
                        <div class="step__wrapper flex-justify-space-between">
                            <div class="stepContainer">
                                <i class="icon-tick-mark" aria-hidden="true"></i>
                            </div>
                            <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                <p>
                                    <span class="icon-edit-website"></span>
                                    Edit Website Design
                                </p>
                                <a href="#" class="btn btn__holo width-120">Edit</a>
                            </div>
                        </div>
                    </li-->

                        <li class="step flex-box flex-justify-center white-box box-shadow">
                            <div class="step__wrapper flex-justify-space-between">
                                <div class="stepContainer">
                                    <i class="icon-tick-mark" aria-hidden="true"></i>
                                </div>
                                <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                    <p>
                                        <span class="icon-edit-website-content"></span>
                                        Edit Website<br/>Content Page
                                    </p>
                                    <a href="/content/pages" class="btn btn__primary width-120">Edit</a>
                                </div>
                            </div>
                        </li>

                        <!--li class="step active flex-box flex-justify-center white-box box-shadow">
                            <div class="step__wrapper flex-justify-space-between">
                                <div class="stepContainer">
                                    <i class="icon-tick-mark" aria-hidden="true"></i>
                                </div>
                                <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                    <p>
                                        <span class="icon-create-menu"></span>
                                        Create Your Menu
                                    </p>
                                    <a href="/menu/item" class="btn btn__holo width-120">Create Menu</a>
                                </div>
                            </div>
                        </li-->

                        <li class="step flex-box flex-justify-center white-box box-shadow">
                            <div class="step__wrapper flex-justify-space-between">
                                <div class="stepContainer">
                                    <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                </div>
                                <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                    <p>
                                        <span class="icon-setup-online-order"></span>
                                        Set Up Online Ordering
                                    </p>
                                    <a href="/online-ordering-setup" class="btn btn__primary width-120">Set Up</a>
                                </div>
                            </div>
                        </li>

                        <li class="step flex-box flex-justify-center white-box box-shadow">
                            <div class="step__wrapper flex-justify-space-between">
                                <div class="stepContainer">
                                    <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                </div>
                                <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                    <p>
                                        <span class="icon-link-your-social-account"></span>
                                        line Your<br/>Social Accounts
                                    </p>
                                    <a href="/social-account-setup" class="btn btn__primary width-120">Link</a>
                                </div>
                            </div>
                        </li>

                        <li class="step active flex-box flex-justify-center white-box box-shadow">
                            <div class="step__wrapper flex-justify-space-between">
                                <div class="stepContainer">
                                    <i class="icon-tick-mark" aria-hidden="true"></i>
                                </div>
                                <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                    <p>
                                        <span class="icon-connect-to-your-domain"></span>
                                        Connect To<br/>Your Domain
                                    </p>
                                    <a href="/contact"   class="btn btn__holo width-120">Connect</a>
                                </div>
                            </div>
                        </li>{{----}}

                    </ul>

                </div>
            </div>
        </div>
    </div>
@endsection
