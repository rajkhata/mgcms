@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/social-media.css')}}" rel='stylesheet' />

    <div class="main__container social-media-links container__custom_working new-design-labels">

        <form method="POST" action="{{ route('updatesocialprofile') }}" enctype="multipart/form-data" id="restaurantform">@csrf

        <div id="cms-accordion" class="row">
            <div class="reservation__atto flex-direction-row">
                <div class="reservation__title margin-top-5">
                    <h1>Social</h1>
                </div>
                <div class="guestbook__sec2">
                    <div class="margin-left-10">

                    </div>

                </div>
            </div>
            @if(session()->has('message'))
                <div class="alert alert-success margin-top-20">
                    {{ session()->get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
            @endif
            <div class="row white-box box-shadow margin-top-0 social-media-list">
                <div class="col-xs-12 text-uppercase font-weight-700 padding-left-25 padding-top-20 padding-bottom-20 accordion-header">Social Media <span class="fa fa-plus"></span></div>
                <div class="col-xs-12 padding-left-25 padding-right-20 margin-top-10 margin-bottom-30 content">
                    <div class="fullWidth flex-box flex-align-item-center padding-top-10 padding-bottom-15">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-facebook font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">Facebook</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="facebook_url" type="text"
                                   class="{{ $errors->has('facebook_url') ? ' is-invalid' : '' }}" name="facebook_url"
                                   value="{{  $locationInfo[0]['facebook_url'] }}" placeholder="www." autofocus>
                            @if ($errors->has('facebook_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('facebook_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['facebook_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>

                    <div class="fullWidth flex-box flex-align-item-center padding-bottom-15">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-twitter font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">Twitter</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="twitter_url" type="text"
                                   class="{{ $errors->has('twitter_url') ? ' is-invalid' : '' }}"
                                   name="twitter_url"
                                   placeholder="www."
                                   value="{{  $locationInfo[0]['twitter_url'] }}">

                            @if ($errors->has('twitter_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('twitter_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['twitter_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>

                    <div class="fullWidth flex-box flex-align-item-center padding-bottom-15 ">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-google font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">Google</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="gmail_url" type="text"
                                   class="{{ $errors->has('gmail_url') ? ' is-invalid' : '' }}"
                                   name="gmail_url"
                                   placeholder="www."
                                   value="{{  $locationInfo[0]['gmail_url'] }}">

                            @if ($errors->has('gmail_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('gmail_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['gmail_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>

                    <div class="fullWidth flex-box flex-align-item-center padding-bottom-15">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-pinterest-p font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">Pinterest</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="pinterest_url" type="text"
                                   class="{{ $errors->has('pinterest_url') ? ' is-invalid' : '' }}"
                                   name="pinterest_url"
                                   placeholder="www."
                                   value="{{  $locationInfo[0]['pinterest_url'] }}">
                            @if ($errors->has('pinterest_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('pinterest_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['pinterest_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>

                    <div class="fullWidth flex-box flex-align-item-center padding-bottom-15">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-instagram font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">Instagram</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="instagram_url" type="text"
                                   class="{{ $errors->has('instagram_url') ? ' is-invalid' : '' }}"
                                   name="instagram_url"
                                   placeholder="www."
                                   value="{{  $locationInfo[0]['instagram_url'] }}">
                            @if ($errors->has('instagram_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('instagram_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['instagram_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>

                    <div class="fullWidth flex-box flex-align-item-center padding-bottom-15">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-yelp font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">Yelp</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="yelp_url" type="text"
                                   class="{{ $errors->has('yelp_url') ? ' is-invalid' : '' }}"
                                   name="yelp_url"
                                   placeholder="www."
                                   value="{{  $locationInfo[0]['yelp_url'] }}">
                            @if ($errors->has('yelp_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('yelp_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['yelp_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>

                    <div class="fullWidth flex-box flex-align-item-center padding-bottom-15 ">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-tripadvisor font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">TripAdvisor</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="tripadvisor_url" type="text"
                                   class="{{ $errors->has('tripadvisor_url') ? ' is-invalid' : '' }}"
                                   name="tripadvisor_url"
                                   placeholder="www."
                                   value="{{  $locationInfo[0]['tripadvisor_url'] }}">

                            @if ($errors->has('tripadvisor_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('tripadvisor_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['tripadvisor_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>

                    <div class="fullWidth flex-box flex-align-item-center padding-bottom-15 ">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-foursquare font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">Four Square</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="foursquare_url" type="text"
                                   class="{{ $errors->has('foursquare_url') ? ' is-invalid' : '' }}"
                                   name="foursquare_url"
                                   placeholder="www."
                                   value="{{  $locationInfo[0]['foursquare_url'] }}">
                            @if ($errors->has('foursquare_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('foursquare_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['foursquare_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>

                    <div class="fullWidth flex-box flex-align-item-center padding-bottom-15">
                        <div class="col-xs-1 col-md-2 flex-box flex-direction-row no-padding-left">
                            <span class="fa fa-youtube font-size-16"></span>
                            <span class="font-weight-600 margin-left-15 icotxt">Youtube</span>
                        </div>
                        <div class="col-xs-11 col-md-10 input__field no-padding-right">
                            <input id="youtube_url" type="text"
                                   class="{{ $errors->has('youtube_url') ? ' is-invalid' : '' }}"
                                   name="youtube_url"
                                   placeholder="www."
                                   value="{{  $locationInfo[0]['youtube_url'] }}">
                            @if ($errors->has('youtube_url'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('youtube_url') }}</strong></span>
                            @endif
                        </div>
                        {{--<a href="{{  $locationInfo[0]['youtube_url'] }}" target="_blank" class="open-close-eye"><span class="icon-eye-open"></span></a>--}}
                    </div>
                </div>
            </div>

            <div class="row white-box box-shadow margin-top-30 meta-tags">
                <div class="col-xs-12 text-uppercase font-weight-700 padding-left-25 padding-top-20 padding-bottom-20 accordion-header">Meta tags <span class="fa fa-plus"></span></div>
                <div class="col-xs-12 padding-left-25 padding-right-25 margin-top-20 margin-bottom-30 content">
                    <div class="fullWidth">
                        <div id="keyword_container">
                            @php
                                $metaKeywords = !empty($locationInfo[0]['meta_tags']) ? json_decode($locationInfo[0]['meta_tags'], true) : '';
                            @endphp
                            @if(is_array($metaKeywords) && count($metaKeywords))
                                @foreach($metaKeywords as $metaVal)
                                    <div class="fullWidth margin-bottom-20 meta-tags-row flex-box flex-direction-row overflow-visible padding-right-20">
                                        <div class="col-xs-12 no-padding-left">
                                            <div class="col-xs-12 col-sm-3 no-mob-pad">
                                                <div class="input__field">
                                                    <input type="text" class="input__field" name="meta_tags_slug[]" value="{{ isset($metaVal['slug'])?$metaVal['slug']:'--' }}" />
                                                    <span>Slug</span>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-3 no-mob-pad">
                                                <div class="input__field">
                                                    <input type="text" name="meta_tags_type[]" value="{{ $metaVal['type'] }}" />
                                                    <span>Type</span>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-3 no-mob-pad">
                                                <div class="input__field">
                                                    <input type="text" name="meta_tags_label[]" value="{{ $metaVal['label'] }}" />
                                                    <span>Key</span>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-3 no-mob-pad">
                                                <div class="input__field">
                                                    <input type="text" class="" name="meta_tags_content[]" value="{{ $metaVal['content'] }}" />
                                                    <span>Value</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="no-padding">
                                            {{--<a href="javascript:void(0)" class="meta_tags_plus"><i class="fa fa-plus"></i></a>--}}
                                            <a href="javascript:void(0)" class="meta_tags_minus margin-left-10"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="fullWidth meta-tags-row flex-box flex-direction-row overflow-visible padding-right-20">
                                    <div class="col-xs-12 no-padding-left">
                                        <div class="col-xs-12 col-sm-3 no-mob-pad">
                                            <div class="input__field">
                                                <input type="text" name="meta_tags_slug[]" />
                                                <span>Slug</span>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-3 no-mob-pad">
                                            <div class="input__field">
                                                <input type="text" name="meta_tags_type[]" />
                                                <span>Type</span>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-3 no-mob-pad">
                                            <div class="input__field">
                                                <input type="text" name="meta_tags_label[]" />
                                                <span>Key</span>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-3 no-mob-pad">
                                            <div class="input__field">
                                                <input type="text" name="meta_tags_content[]" />
                                                <span>Value</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="no-padding">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        {{--<a href="javascript:void(0)" class="meta_tags_plus"><i class="fa fa-plus"></i></a>
                                        <a href="javascript:void(0)" class="meta_tags_minus margin-left-10"><i class="fa fa-minus"></i></a>--}}
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center margin-top-30 margin-bottom-20">
                                {{--<a href="javascript:void(0)" class="meta_tags_plus"><i class="fa fa-plus"></i></a>--}}
                                <button type="button" class="meta_tags_plus btn_ btn__holo font-weight-600 box-shadow">Add New Meta Tags</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    <div class="row white-box box-shadow margin-top-30 meta-tags">
                <div class="col-xs-12 text-uppercase font-weight-700 padding-left-25 padding-top-20 padding-bottom-20 accordion-header">Emails <span class="fa fa-plus"></span></div>
                <div class="col-xs-12 padding-left-25 padding-right-25 margin-top-20 margin-bottom-30 content">
                    <div class="fullWidth">
                        <div id="keyword_container11">
                            @php
                                $email_data = !empty($locationInfo[0]['email_data']) ? json_decode($locationInfo[0]['email_data'], true) : '';
                            @endphp
                            @if(is_array($email_data) && count($email_data))
                                @foreach($email_data as $key => $emailVal)
                                    <div class="fullWidth margin-bottom-20 meta-tags-row flex-box flex-direction-row overflow-visible padding-right-20">
                                        <div class="col-xs-12 no-padding-left">
                                            <div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">
                                                <div class="">
                                                    <select name="email_type[]" id="email_type" class="form-control" >
							                            <option value="">Select Type</option>
							                            <option value="contact" {{ (isset($key) && $key=='contact')? 'selected' :'' }}>Contact</option>
							                            <option value="careers" {{ (isset($key) && $key=='careers')? 'selected' :'' }}>Career</option>
							                            <option value="catering" {{ (isset($key) && $key=='catering')? 'selected' :'' }}>Catering</option>
							                            <option value="events" {{ (isset($key) && $key=='events')? 'selected' :'' }}>Events</option>
							                            <option value="enquiry" {{ (isset($key) && $key=='enquiry')? 'selected' :'' }}>Enquiry</option>
							                            <option value="reservation" {{ (isset($key) && $key=='reservation')? 'selected' :'' }}>Reservation</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">
                                                <div class="input__field">
                                                    <input type="text" name="from_name[]" value="{{ $emailVal['from_name'] }}" />
                                                    <span>From Name</span>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">
                                                <div class="input__field">
                                                    <input type="text" name="to_name[]" value="{{ $emailVal['to_name'] }}" />
                                                    <span>To Name</span>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-3 no-mob-pad2 padding-bottom-10">
                                                <div class="input__field">
                                                    <input type="text" class="" name="from_email[]" value="{{ $emailVal['from_email'] }}" />
                                                    <span>From Email</span>
                                                </div>
                                            </div>
					                        <div class="col-xs-12 col-sm-3 no-mob-pad2 padding-bottom-10">
                                                <div class="input__field">
                                                    <input type="text" class="" name="to_email[]" value="{{ $emailVal['to_email'] }}" />
                                                    <span>To Email</span>
                                                </div>
                                            </div>	
                                        </div>

                                        <div class="no-padding">
                                            {{--<a href="javascript:void(0)" class="meta_tags_plus"><i class="fa fa-plus"></i></a>--}}
                                            <a href="javascript:void(0)" class="emails_minus margin-left-10"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="fullWidth meta-tags-row flex-box flex-direction-row overflow-visible padding-right-20 margin-bottom-20">
                                    <div class="col-xs-12 no-padding-left no-padding-right">
                                        <div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">
                                            <div class="position-relative">
                                                <select name="email_type[]" id="email_type" class="form-control" >
                                                    <option value="" selected>Select Type</option>
                                                    <option value="contact">Contact</option>
                                                    <option value="careers">Career</option>
                                                    <option value="catering">Catering</option>
                                                    <option value="events" >Events</option>
                                                    <option value="enquiry">Enquiry</option>
                                                    <option value="reservation">Reservation</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">
                                            <div class="input__field">
                                                <input type="text" name="from_name[]" />
                                                <span>From Name</span>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">
                                            <div class="input__field">
                                                <input type="text" name="to_name[]" />
                                                <span>To Name</span>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-3 no-mob-pad2 padding-bottom-10">
                                            <div class="input__field">
                                                <input type="text" name="from_email[]" />
                                                <span>From Email</span>
                                            </div>
                                        </div>
					                    <div class="col-xs-12 col-sm-3 no-mob-pad2 padding-bottom-10">
                                            <div class="input__field">
                                                <input type="text" name="to_email[]" />
                                                <span>To Email</span>
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center margin-top-30 margin-bottom-20">
                                 
                                <button type="button" class="emails_plus btn_ btn__holo font-weight-600 box-shadow margin-left-15">Add New Email</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
            
            <div class="row white-box box-shadow margin-top-30 social-media-apis">
                
                <div class="col-xs-12 text-uppercase font-weight-700 padding-left-25 padding-top-20 padding-bottom-20 accordion-header">API Keys <span class="fa fa-plus"></span></div>
                <div class="col-xs-12 padding-left-25 padding-right-25 margin-top-10 margin-bottom-30 content">
                    <div class="col-xs-12 no-mob-pad3">
                        {{--<div class="font-weight-700">Google Verification Code</div>--}}
                        <div class="input__field api-key">
                            <textarea    maxlength="5000" rows="1" name="google_verification_code" id="google_verification_code" class="apiCode {{ $errors->has('google_verification_code') ? ' is-invalid' : '' }}">{{  $locationInfo[0]['google_verification_code'] }}</textarea>
                            <span>Google Verification Code</span>
                        </div>
                        @if ($errors->has('google_verification_code'))
                            <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('google_verification_code') }}</strong></span>
                        @endif
                    </div>

                    <div class="col-xs-12 no-mob-pad3 margin-top-20">
                        <div class="input__field api-key">
                            <input    id="gtm_code" type="text"
                                   class="{{ $errors->has('gtm_code') ? ' is-invalid' : '' }}"
                                   name="gtm_code"
                                   value="{{  $locationInfo[0]['gtm_code'] }}">
                            <span for="gtm_code">GTM Code</span>
                            @if ($errors->has('gtm_code'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('gtm_code') }}</strong></span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 no-mob-pad3 margin-top-20">
                        {{--<div class="font-weight-700">Google Map Id</div>--}}
                        <div class="input__field api-key">
                            <textarea    maxlength="5000" rows="1" name="google_map_id" id="google_map_id" class="apiCode {{ $errors->has('google_map_id') ? ' is-invalid' : '' }}" >{{  $locationInfo[0]['google_map_id'] }}</textarea>
                            <span for="google_map_id">Google Map Id</span>
                            @if ($errors->has('google_map_id'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('google_map_id') }}</strong></span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 no-mob-pad3 margin-top-20">
                        {{--<div class="font-weight-700">Google Client Id</div>--}}
                        <div class="input__field api-key">
                            <textarea    maxlength="5000" rows="1" name="google_client_id" id="google_client_id" class="apiCode {{ $errors->has('google_client_id') ? ' is-invalid' : '' }}">{{  $locationInfo[0]['google_client_id'] }}</textarea>
                            <span for="google_client_id">Google Client Id</span>
                            @if ($errors->has('google_client_id'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('google_client_id') }}</strong></span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 no-mob-pad3 margin-top-20">
                        {{--<div class="font-weight-700">Facebook Client Id</div>--}}
                        <div class="input__field api-key">
                            <textarea    maxlength="5000" rows="1" name="facebook_client_id" id="facebook_client_id" class="apiCode {{ $errors->has('facebook_client_id') ? ' is-invalid' : '' }}">{{$locationInfo[0]['facebook_client_id']  }}</textarea>
                            <span for="facebook_client_id">Facebook Client Id</span>
                            @if ($errors->has('facebook_client_id'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('facebook_client_id') }}</strong></span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 no-mob-pad3 margin-top-20">
                        {{--<div class="font-weight-700">Netcore API Key</div>--}}
                        <div class="input__field api-key">
                            <textarea    maxlength="5000" rows="1" name="netcore_api_key" id="netcore_api_key" class="apiCode {{ $errors->has('netcore_api_key') ? ' is-invalid' : '' }}">{{  $locationInfo[0]['netcore_api_key'] }}</textarea>
                            <span for="netcore_api_key">Netcore API Key</span>
                            @if ($errors->has('netcore_api_key'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('netcore_api_key') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    
                </div>
            </div>
	    <div class="row white-box box-shadow margin-top-30">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-25 padding-top-20 padding-bottom-20 accordion-header">Script <span class="fa fa-plus"></span></div>

                        <div class="col-xs-12 padding-left-25 padding-right-25 margin-bottom-30 content">
                            <div class="col-xs-12 no-mob-pad3 text-color-grey font-weight-700">This script will be placed in website header and footer.</div>

                            <div class="col-xs-12 no-mob-pad3 col-sm-6 margin-top-30">
                                <div class="input__field">
				                    <textarea id="header_script" type="text" rows="5" cols="5" name="header_script">{{$locationInfo[0]['header_script']}}</textarea>
                                     
                                    <span for="header_script" class="hasVal">Header</span>
                                    <!-- <span for="header_script" class="{{($locationInfo[0]['header_script']!='')?'hasVal':''}}" >Header</span> -->
                                </div>
                            </div>

                            <div class="col-xs-12 no-mob-pad3 col-sm-6 margin-top-30">
                                <div class="input__field">
				                    <textarea id="footer_script" type="text" rows="5" cols="5" name="footer_script">{{$locationInfo[0]['footer_script']}}</textarea>	
                                    
                                    <span for="footer_script" class="hasVal">Footer</span>
                                    <!-- <span for="footer_script" class="{{($locationInfo[0]['footer_script']!='')?'hasVal':''}} ">Footer</span> -->
                                </div>
                            </div>

                        </div>
                    </div>			
		 <div class="row white-box box-shadow margin-top-30">
                        <div class="col-xs-12 padding-left-25 padding-right-25 margin-bottom-30 content">
                            <div class="col-xs-12 no-mob-pad3 text-color-grey font-weight-700">Restaurant Description.</div>

                            <div class="col-xs-12 no-mob-pad3 col-sm-6 margin-top-30">
                                <div class="input__field">
				                    <textarea id="description" type="text" rows="5" cols="5" name="description">{{$locationInfo[0]['description']}}</textarea>
                                     
                                    <span for="description" class="hasVal"></span>
                                     
                                </div>
                            </div>

                             

                        </div>
                    </div>
		<?php if($locationInfo[0]['parent_restaurant_id']<1){?>
		 
		   <div class="row white-box box-shadow margin-top-30">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header no-mob-pad6">Promotional Message <span class="fa fa-plus"></span></div>
			     <div class="col-xs-12 padding-left-40 padding-right-40 margin-top-10 margin-bottom-30 content no-mob-pad6">
				<div class="col-xs-12  padding-right-40 margin-top-10 margin-bottom-30 content no-mob-pad6">
                                <div class="fullWidth">
                                   Status <span role="button" class="icon-information text-color-grey text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Message Status."></span>
                                </div>
                                <div class="input__field common_message textarea__input"  >
                                        <select id="common_message_status" name="common_message_status" class="selectpicker123" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="">Select</option>
                                            <option value="1" {{(isset($locationInfo[0]['common_message_status']) && $locationInfo[0]['common_message_status']==1)?"selected":''}}  >
                                                Active
                                            </option>
                                            <option value="0" {{(isset($locationInfo[0]['common_message_status']) && $locationInfo[0]['common_message_status']==0)?"selected":''}}  >
                                                Inactive
                                            </option>

                                        </select>
                                    </div>
                                
                            </div>
			  <div class="col-xs-12  padding-right-40 margin-top-10 margin-bottom-30 content no-mob-pad6">
                                <div class="fullWidth">
                                    Url Link <span role="button" class="icon-information text-color-grey text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Enter refferal amout for the code."></span>
                                </div>
                                <div class="input__field common_message textarea__input"  >
                                        <input type="text"  class="fullWidth" name="common_message_url" id="common_message_url" value="{{(isset($locationInfo[0]['common_message_url']))?$locationInfo[0]['common_message_url']:''}}"  />
                                    
                                </div>
                            </div>
                        <div class="col-xs-12  padding-right-40 margin-top-10 margin-bottom-30 content no-mob-pad6">
                            <div class="fullWidth">
Message <span role="button" class="icon-information text-color-grey text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Enter refferal amout for the code."></span>
				    <div class="input__field common_message textarea__input"  >
		                        <textarea  id="common_message"  name="common_message"   class="fullWidth" autocomplete="off">{{(isset($locationInfo[0]['common_message']))?$locationInfo[0]['common_message']:''}}</textarea>
		                         
		                    </div>
                                     
                                </div>
                             </div>
			</div>
		    </div>
		<?php }?>
            <div class="col-xs-12 text-center margin-top-30">
                <input type="submit" class="btn btn__primary font-weight-600  width-120 box-shadow" value="Save" >
                <input id="id" type="hidden" value="{{ $locationInfo[0]['id']}}" name="id" ><input id="parent_restaurant_id" type="hidden" value="{{ $locationInfo[0]['parent_restaurant_id']}}" name="parent_restaurant_id" >
            </div>

        </div>



        </form>
    </div>

    <script>

        $(".social-media-apis .input__field textarea").each(function(key, value) {
            if($(this).val()){
                $(this).next().addClass('hasVal')
            } else {
                $(this).next().removeClass('hasVal')
            }
        });

        setTimeout(function(){
            $(".social-media-apis .input__field input, .meta-tags-row .input__field input").each(function(key, value) {
                if($(this).val() != ""){
                    $(this).next().addClass('hasVal')
                } else {
                    $(this).next().removeClass('hasVal')
                }
            });
        },400)

        $(document).on('click', '.meta_tags_plus', function() {
            var html = '<div class="fullWidth margin-top-20 meta-tags-row flex-box flex-direction-row overflow-visible padding-right-20">\n' +
                '                                   <div class="col-xs-12 no-padding-left">\n'+
                '                                    <div class="col-xs-12 col-sm-3 no-mob-pad">\n' +
                '                                        <div class="input__field">\n' +
                '                                            <input type="text" name="meta_tags_slug[]" />\n' +
                '                                            <span>Slug</span>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                    <div class="col-xs-12 col-sm-3 no-mob-pad">\n' +
                '                                        <div class="input__field">\n' +
                '                                            <input type="text" name="meta_tags_type[]" />\n' +
                '                                            <span>Type</span>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                    <div class="col-xs-12 col-sm-3 no-mob-pad">\n' +
                '                                        <div class="input__field">\n' +
                '                                            <input type="text" name="meta_tags_label[]" />\n' +
                '                                            <span>Key</span>\n' +
                '                                        </div>\n' +
                '                                    </div>\n'+
                '                                    <div class="col-xs-12 col-sm-3 no-mob-pad">\n' +
                '                                        <div class="input__field">\n' +
                '                                            <input type="text" name="meta_tags_content[]" />\n' +
                '                                            <span>Value</span>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                    </div>\n'+
                '                                    <div class="no-padding">\n' +
                //'                                        <a href="javascript:void(0)" class="meta_tags_plus"><i class="fa fa-plus"></i></a>\n' +
                '                                        <a href="javascript:void(0)" class="meta_tags_minus"><i class="icon-cancelled text-color-black font-size-16"></i></a>\n' +
                '                                    </div>\n' +
                '</div>';
            $('#keyword_container').append(html);
        });
        $('#keyword_container').on('click', 'a.meta_tags_minus', function() {
            $(this).parents('.meta-tags-row').remove();
        });

	$(document).on('click', '.emails_plus', function() {
            var html = '<div class="fullWidth meta-tags-row flex-box flex-direction-row overflow-visible padding-right-20 margin-bottom-20">\n'+
            '<div class="col-xs-12 no-padding-left">\n'+
            '<div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">\n'+
            '<div class="">\n'+
            '<select name="email_type[]" id="email_type" class="form-control" >\n'+
                '<option value="" selected>Select Type</option>\n'+
                '<option value="contact"  >Contact</option>\n'+
                '<option value="career"  >Career</option>\n'+
            '</select>\n'+
            '</div>\n'+
            '</div>\n'+

            '<div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">\n'+
            '<div class="input__field">\n'+
            '<input type="text" name="from_name[]" />\n'+
            '<span>From Name</span>\n'+
            '</div>\n'+
            '</div>\n'+

            '<div class="col-xs-12 col-sm-2 no-mob-pad2 padding-bottom-10">\n'+
            '<div class="input__field">\n'+
            '<input type="text" name="to_name[]" />\n'+
            '<span>To Name</span>\n'+
            '</div>\n'+
            '</div>\n'+

            '<div class="col-xs-12 col-sm-3 no-mob-pad2 padding-bottom-10">\n'+
            '<div class="input__field">\n'+
            '<input type="text" name="from_email[]" />\n'+
            '<span>From Email</span>\n'+
            '</div>\n'+
            '</div>\n'+
            '<div class="col-xs-12 col-sm-3 no-mob-pad2 padding-bottom-10">\n'+
            '<div class="input__field">\n'+
            '<input type="text" name="to_email[]" />\n'+
            '<span>To Email</span>\n'+
            '</div>\n'+
            '</div>\n'+
            '</div>\n'+
            '<div class="no-padding">\n' +
            '<a href="javascript:void(0)" class="emails_minus"><i class="icon-cancelled text-color-black font-size-16"></i></a>\n' +
            '</div>\n' +
            '</div>';
            $('#keyword_container11').append(html);
        });
        $('#keyword_container11').on('click', 'a.emails_minus', function() {
            $(this).parents('.meta-tags-row').remove();
        });
    </script>
@endsection
