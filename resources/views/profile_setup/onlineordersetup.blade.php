@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

        <div class="main__container container__custom_working new-design-labels">

            <div class="row">
                <form  method="post" id = "submit_pause_service_form" action="/online-ordering-restaurantsettings" > @csrf
                <div class="reservation__atto flex-direction-row">
                    <div class="reservation__title margin-top-5">
                        <h1>Online Ordering</h1>
                    </div>
                    <!-- This will hide once we make dynamic -->
                    <!--div class="guestbook__sec2">
                        <div class="margin-left-10" style="display:none;">
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#stop-service" class="btn btn__primary box-shadow margin-right-15">Stop Service Popup</a>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#copy-settings-for-locations" class="btn btn__primary box-shadow">Copy Setting Locations Popup</a>
                        </div>
                    </div-->
                    <!-- End - This will hide once we make dynamic -->
                </div>
                @include('reservation::common.message')
                <input type="hidden" value ="" name = "action" id= "current_action">
                <input type="hidden" value ="0" name = "restaurant_id" id= "restaurant_id">
                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-0 location-list">
                <?php
                    foreach($restInfoArr as $restInfo){
                    $pause_service_message = (isset($restInfo) && $restInfo && isset($restInfo['pause_service_message']) && $restInfo['pause_service_message']) ? $restInfo['pause_service_message'] : "We are currently not accepting online orders. We'll be back soon.";

                    $del_class = (isset($restInfo) && isset($restInfo['food_ordering_delivery_allowed']) && $restInfo['food_ordering_delivery_allowed'] == 1 && $restInfo['is_food_order_allowed'] == 1) ? 'block' :'none';
                    $del_active = (isset($restInfo) && isset($restInfo['delivery']) && $restInfo['delivery'] == 1) ? 'active' :'';
                    $del_value = (isset($restInfo) && isset($restInfo['delivery']) && $restInfo['delivery'] == 1) ? 1 : 0;
                    $take_class = (isset($restInfo) && isset($restInfo['food_ordering_takeout_allowed']) && $restInfo['food_ordering_takeout_allowed'] == 1 && $restInfo['is_food_order_allowed'] == 1) ? 'block' :'none';
                    $take_active = (isset($restInfo) && isset($restInfo['takeout']) && $restInfo['takeout'] == 1) ? 'active' :'';
                    $take_value = (isset($restInfo) && isset($restInfo['takeout']) && $restInfo['takeout'] == 1) ? 1 : 0;
                    ?>
                    <div class="col-xs-12 padding-top-20 padding-bottom-15 padding-left-40 padding-right-40 no-mob-pad4">
                        <div class="col-xs-12 col-sm-3 no-padding-left font-weight-700 padding-top-10 pad-bot-20">
                            {{$restInfo['restaurant_name']}}
                            <input type="hidden" value ="<?php echo (isset($restInfo) && isset($restInfo['id']) && $restInfo['id'] ) ? $restInfo['id'] : '' ;?>" name = "restaurant[{{$restInfo['id']}}]" id = "restaurant_id{{$restInfo['id']}}">
                            <input type="hidden" value ="<?php echo (isset($restInfo) && isset($restInfo['pause_service_message']) && $restInfo['pause_service_message'] ) ? $restInfo['pause_service_message'] : '' ;?>" name = "pause_service_message{{$restInfo['id']}}" id = "pause_service_message{{$restInfo['id']}}">

                        </div>
                        <div class="col-xs-12 col-sm-6 pad-bot-20 hide">
                            <div class="col-xs-6 flex-box flex-justify-center flex-align-item-center no-padding overflow-visible">
                                <div class="toggle__container pull-left">
                                    <button type="button" id="delivery_button{{$restInfo['id']}}" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox change_setting dayoff_custom <?php echo $del_active;?>" data-toggle="button" aria-pressed="true" autocomplete="off" rel="delivery" relid="{{$restInfo['id']}}" relmsg="{{$pause_service_message}}">
                                        <div class="handle"></div>
                                    </button>
                                    <input type="hidden" class="enable_deilvery_orders_button delivery_button{{$restInfo['id']}}" value ="<?php echo $del_value;?>" name = "delivery[{{$restInfo['id']}}]">
                                </div>
                                <div class="pull-left font-weight-700 margin-left-10">Delivery</div>
                            </div>

                            <div class=" hide col-xs-6 flex-box flex-justify-center flex-align-item-center no-padding overflow-visible">
                                <div class="toggle__container">
                                    <button type="button" id="takeout_button{{$restInfo['id']}}" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox change_setting dayoff_custom <?php echo $take_active;?>" data-toggle="button" aria-pressed="true" autocomplete="off" rel="takeout" relid="{{$restInfo['id']}}" relmsg="{{$pause_service_message}}">
                                        <div class="handle"></div>
                                    </button>
                                    <input type="hidden" class="enable_takeout_orders_button takeout_button{{$restInfo['id']}}" value ="<?php echo $take_value;?>" name = "takeout{{$restInfo['id']}}">
                                </div>
                                <div class="pull-left font-weight-700 margin-left-10">Takeout</div>
                            </div>
                        </div>
                        @can('Ordering Edit')
                        <div class="col-xs12 col-sm-9 no-padding-right text-right tac">
                            <a href="/online-ordering-location-setup/{{$restInfo['id']}}" class="btn btn__primary width-90">Edit</a>
                        </div>
                        @endcan
                    </div>
                    <?php }?>
                </form>
                </div>
            </div>
        </div>


<!-- Stop Service Model -->
<div class="modal modal-popup fade" id="stop-service" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" data-backdrop="static">
    <div class="modal-dialog vertical-center" role="document">

        <div class="modal-content white-smoke">
            <div class="modal-body">
               <div class="row">
                  <div class="col-lg-12 text-center margin-top-20">
                      <p class="margin-bottom-20 font-weight-800" >Reason for Stopping Service</p>
                  </div>
               </div>
                <div class="row">
                    <div class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 no-padding">
                        <div class="custom__message-textContainer no-margin col-lg-12">
                            <textarea class="border-radius-none" placeholder="We are currently not accepting online orders. We'll be back soon." maxlength="300" rows="5" id="other_reason"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row margin-bottom-30 text-center">

                <button type="button" class="btn btn__primary width-120 font-weight-600 margin-bottom-30 submit_reason_btn">Save</button>
                <button type="button" class="btn btn__holo width-120 margin-left-15 font-weight-600 margin-bottom-30 closebtn" >Cancel</button>

            </div>

        </div>
    </div>
</div>
<!-- end -->

    <script>
        //$('span[data-toggle="popover"]').popover();
        var current_slide_class = '';
        var current_action = '';
        var relid = '';
        var relmsg = '';
            $(document.body).on('click','.change_setting',function() {
            current_action = $(this).attr('rel');
             relid = $(this).attr('relid');
             relmsg = $(this).attr('relmsg');
            $('#current_action').val(current_action);
            var selected = $(this).hasClass('active');
            if(selected == true) {
                current_slide_class = $(this).attr('id');
                $('#stop-service').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#restaurant_id').val(relid);
                $('#other_reason').val(relmsg);
                // $('.setting-save-btn').hide();
            }else {
                $('#restaurant_id').val(relid);
                $('#other_reason').val(relmsg);
                $('#stop-service').modal('hide');
                $('#submit_pause_service_form').submit();
                // $('.setting-save-btn').show();
            }
        });



        //submit the form
        $(document.body).on('click','.submit_reason_btn',function() {


            var other_reason = $('#other_reason').val();
            $('#pause_service_message' + relid).val(other_reason);
            $('#submit_pause_service_form').submit();
        });


        if($(".row.messageblock .alert").length){
            setTimeout(() => {
                $(".row.messageblock .alert .close").trigger('click')
            }, 5000);
        }
        //BS modal close

        $("#stop-service .closebtn").click(function () {
            //alert(12)
            $('#'+current_slide_class).addClass('active');
            $('.'+current_slide_class).val(1);
            // $('.setting-save-btn').show();
            $("#stop-service").modal('hide')
        })


    </script>






<!-- Stop Service Model -->
<div class="modal modal-popup fade" id="copy-settings-for-locations" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" data-backdrop="static">
    <div class="modal-dialog vertical-center" role="document">

        <div class="modal-content white-smoke">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 text-center margin-top-20">
                        <p class="margin-bottom-20 font-weight-800">Copy Settings to Other Locations</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10">
                        <p class="text-color-grey font-weight-600">
                            Select location for which these settings have to be copied.<br/>
                            Note: All settings except delivery areas will be copied.
                        </p>
                    </div>

                    <div class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 padding-top-bottom-20">
                        <div class="selct-picker-plain">
                            <select multiple class="selectpicker" data-dropup-auto="false" title="Select The Locations" data-size="5" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                <option value="1">Location 1</option>
                                <option value="2">Location 2</option>
                                <option value="3">Location 3</option>
                                <option value="4">Location 4</option>
                                <option value="5">Location 5</option>
                                <option value="6">Location 6</option>
                                <option value="7">Location 7</option>
                                <option value="8">Location 8</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10">
                        <p class="text-color-grey font-weight-600">
                            This will override the existing setting on selected locations. Do you want to continue.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row margin-bottom-30 text-center">

                <button type="button" class="btn btn__primary width-120 font-weight-600 margin-bottom-30">Yes</button>
                <button type="button" class="btn btn__holo width-120 margin-left-15 font-weight-600 margin-bottom-30" data-dismiss="modal">No</button>

            </div>

        </div>
    </div>
</div>
<!-- end -->
@endsection
