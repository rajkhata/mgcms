@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/social-media.css')}}" rel='stylesheet' />

    <div class="main__container social-media-links container__custom_working new-design-labels">

        <form method="POST" action="{{ route('contactus') }}" enctype="multipart/form-data" id="restaurantform">@csrf

            <div class="row margin-top-30">
                <div class="reservation__atto flex-direction-row">
                    <div class="reservation__title margin-top-5">
                        <h1>Connect</h1>
                    </div>
                    <div class="guestbook__sec2">
                        <div class="margin-left-10">
                            <input type="submit" class="btn btn__primary font-weight-600  width-120 box-shadow" value="Send" >
                            <input id="id" type="hidden" value="{{ $locationInfo[0]['id']}}" name="id" >
                        </div>

                    </div>
                </div>
                @if(session()->has('message'))
                    <div class="alert alert-success margin-top-20">
                        {{ session()->get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif


                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-20 social-media-apis">

                    <div class="col-xs-12 padding-left-25 padding-right-25 margin-top-20">
                        <div class="col-xs-12">

                            <div class="input__field api-key">
                                <input id="name" type="text"
                                       class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name"
                                       value="">
<span for="name">Name</span>

                            </div>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('name') }}</strong></span>
                            @endif
                        </div>

                        <div class="col-xs-12 margin-top-20">
                            <div class="input__field api-key">
                                <input id="mobile" type="text"
                                       class="{{ $errors->has('') ? ' is-invalid' : '' }}"
                                       name="mobile"
                                       value="">
                                <span for="gtm_code">Contact Number</span>
                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('mobile') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 margin-top-20">
                            <div class="input__field api-key">
                                <input id="email type="text"
                                       class="{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email"
                                       value="">
                                <span for="gtm_code">Email</span>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 margin-top-20">
                            {{--<div class="font-weight-700">Google Map Id</div>--}}
                            <div class="input__field api-key">
                                <textarea maxlength="5000" rows="5" name="query" id="query" class="apiCode {{ $errors->has('query') ? ' is-invalid' : '' }}"></textarea>
                                <span for="google_map_id">Domain and Server Details</span>
                                @if ($errors->has('query'))
                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('query') }}</strong></span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>



        </form>
    </div>

    <script>

        $(".social-media-apis .input__field textarea").each(function(key, value) {
            if($(this).val()){
                $(this).next().addClass('hasVal')
            } else {
                $(this).next().removeClass('hasVal')
            }
        });
        setTimeout(function(){
            $(".social-media-apis .input__field input").each(function(key, value) {
                if($(this).val()){
                    $(this).next().addClass('hasVal')
                } else {
                    $(this).next().removeClass('hasVal')
                }
            });
        },200)

    </script>
@endsection
