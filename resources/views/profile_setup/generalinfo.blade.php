@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet'/>
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print'/>
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet'/>
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet'/>
    <php print_r($restaurantData);die; ?>
        <div class="main__container info-restaurant container__custom_working new-design-labels">


            <form method="POST" action="{{ route('updateprofile') }}" enctype="multipart/form-data"
                  id="restaurantform">@csrf
                <div id="cms-accordion" class="row">
                    <!--Header-->
                    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
                    <div class="reservation__atto flex-direction-row">
                        <div class="reservation__title margin-top-5">
                            <h1>General Info</h1>
                            <input id="parent_restaurant_id" type="hidden"
                                   value="{{ $parentRestaurantData[0]['id']}}"
                                   name="parent_restaurant_id">
                        </div>

                    </div>
                    @if(session()->has('message'))
                        <div class="alert alert-success margin-top-20 no-margin-bottom">
                            {{ session()->get('message') }}
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        </div>
                    @endif
                    @if($user->role!="manager")
                    <div class="row white-box box-shadow margin-top-20">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header">Basic Information <span class="fa fa-minus"></span></div>
                        <div class="col-xs-12 margin-bottom-30 padding-left-25 padding-right-25 content">
                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="input__field">
                                    <input id="parent_restaurant_name"
                                           value="{{$parentRestaurantData[0]['restaurant_name']}}" type="text"
                                           name="parent_restaurant_name" autocomplete="off">
                                    <span for="restaurantName">Restaurant Name</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="input__field">
                                    <input id="restaurantDes" value="{{$parentRestaurantData[0]['tagline']}}"
                                           type="text" name="tagline" autocomplete="off">
                                    <span for="restaurantDes">Description or Tagline</span>
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="selct-picker-plain position-relative">

                                    <select id="restaurant_type" name="restaurant_type" data-size="8"
                                            class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                        <option value="">Select Type</option>
                                        @foreach($Cusines as $lang)
                                            <option value="{{ $lang->name }}" {{ ($parentRestaurantData[0]['cusine']==$lang->name) ? 'selected' :'' }}>
                                                {{ isset($lang->name)?ucfirst($lang->name):'' }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span for="restaurantDes" class="{{($parentRestaurantData[0]['cusine']==$lang->name) ? 'hasVal' :'' }} label-field">Select Type</span>

                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="input__field">
                                    <input id="restaurantFeature" value="{{$parentRestaurantData[0]['feature']}}"
                                           type="text" name="feature" autocomplete="off">
                                    <span for="restaurantFeature">Restaurant Features</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                     <div class="row white-box box-shadow margin-top-20">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header">Restaurant Identification Number <span class="fa fa-plus"></span></div>
                        <div class="col-xs-12 margin-bottom-30 padding-left-25 padding-right-25 content">   
                        <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="input__field">
                                    <input id="client_id"
                                           value="{{$parentRestaurantData[0]['client_id']}}" type="text"
                                           name="client_id" autocomplete="off">
                                    <span for="clientId">Client Id</span>
                                </div>
                            </div>
                        </div>
                       
                    </div>

                    <div class="row white-box box-shadow margin-top-30">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header">Custom Message <span class="fa fa-plus"></span></div>

                        <div class="col-xs-12 padding-left-25 padding-right-25 margin-bottom-30 content">
                            <div class="col-xs-12 text-color-grey font-weight-700">This message will be shown to customers on your website while placing an order.</div>

                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="input__field">
                                    <input id="delivery_custom_message" type="text" name="delivery_custom_message" autocomplete="off" value="{{$parentRestaurantData[0]['delivery_custom_message']}}">
                                    <span for="delivery_custom_message" class="{{($parentRestaurantData[0]['delivery_custom_message']!='')?'hasVal':''}}" >Delivery</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="input__field">
                                    <input id="takeout_custom_message" type="text" name="takeout_custom_message" autocomplete="off" value="{{$parentRestaurantData[0]['takeout_custom_message']}}">
                                    <span for="takeoutmessage" class="{{($parentRestaurantData[0]['takeout_custom_message']!='')?'hasVal':''}} ">Takeout</span>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row white-box box-shadow margin-top-30">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header">Brand Information <span class="fa fa-plus"></span></div>
                        <div class="col-xs-12 padding-left-25 padding-right-25 margin-bottom-30 content">
                            
                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="selct-picker-plain position-relative">
                                    <select name="parent_contact_person_title" id="parent_contact_person_title"
                                            class="selectpicker"
                                            data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                        <option value="">Title</option>
                                        <option value="Owner" {{ ($parentRestaurantData[0]['contact_person_title']=="Owner") ? 'selected' :'' }}>
                                            Owner
                                        </option>
                                        <option value="Manager" {{ ($parentRestaurantData[0]['contact_person_title']=="Manager") ? 'selected' :'' }}>
                                            Manager
                                        </option>
                                    </select>
                                    <span for="parent_contact_person_title" class="{{ ($parentRestaurantData[0]['contact_person_title'] != '') ? 'hasVal' :'' }} label-field" >Title</span>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="input__field">
                                    <input id="parent_contact_person" type="text"
                                           value="{{$parentRestaurantData[0]['contact_person']}}"
                                           name="parent_contact_person" autocomplete="off">
                                    <span for="parent_contact_person">Contact Person</span>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="input__field">
                                    <input id="parent_address" value="{{$parentRestaurantData[0]['address']}}"
                                           type="text" name="parent_address" autocomplete="off">
                                    <span for="parent_address">Business Address</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="input__field">
                                    <input id="parent_address2" type="text" name="parent_address2"
                                           value="{{$parentRestaurantData[0]['address2']}}" autocomplete="off">
                                    <span for="parent_address2">Address 2</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="input__field">
                                    <input id="parent_street" type="text" value="{{$parentRestaurantData[0]['street']}}"
                                           name="parent_street" autocomplete="off">
                                    <span for="parent_street">Street</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30  pull-right">
                                <div class="selct-picker-plain position-relative">
                                    <select name="parent_contact_city_id" id="parent_contact_city_id"
                                            class="selectpicker"
                                            data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                        <option value="0">City</option>
                                        @foreach($cites as $lang)
                                            <option value="{{ $lang->id }}" {{ ($parentRestaurantData[0]['contact_city_id']==$lang->id) ? 'selected' :'' }}>
                                                {{ isset($lang->city_name)?ucfirst($lang->city_name):'' }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span for="parent_contact_city_id" class="{{($parentRestaurantData[0]['contact_city_id']==$lang->id) ? 'hasVal' :'' }} label-field">City</span>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="selct-picker-plain position-relative">
                                    <select name="cstate_id" id="cstate_id"
                                            class="selectpicker"
                                            data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                        <option value=""></option>
                                        @foreach($state as $lang)
                                            <option value="{{ $lang->id }}" {{($parentRestaurantData[0]['cstate_id']==$lang->id) ? 'selected' :'' }}>
                                                {{ isset($lang->state)?ucfirst($lang->state):'' }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span for="cstate_id" class="{{($parentRestaurantData[0]['cstate_id']==$lang->id) ? 'hasVal' :'' }} label-field">State</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30  pull-right">
                                <div class="selct-picker-plain position-relative">
                                    <select name="parent_contact_person_country" id="parent_contact_person_country"
                                            class="selectpicker"
                                            data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                        <option value=""></option>
                                        @foreach($country as $lang)
                                            <option value="{{ $lang->id }}" {{($parentRestaurantData[0]['ccountry_id']==$lang->id) ? 'selected' :'' }}>
                                                {{ isset($lang->country_name)?ucfirst($lang->country_name):'' }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span for="parent_contact_person_country" class="{{($parentRestaurantData[0]['ccountry_id']==$lang->id) ? 'hasVal' :''}} label-field">Country</span>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="input__field">
                                    <input id="parent_contact_zipcode" type="text" name="parent_contact_zipcode"
                                           value="{{$parentRestaurantData[0]['contact_zipcode']}}" autocomplete="off">
                                    <span for="parent_contact_zipcode">Zip Code</span>
                                </div>
                            </div>
                            {{--<div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="input__field">
                                    <input id="parent_contact_person" type="text" value="{{$parentRestaurantData[0]['contact_person']}}"  name="parent_contact_person" autocomplete="off">
                                    <span for="parent_contact_person">Contact Person</span>
                                </div>
                            </div>
--}}
                            <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                <div class="input__field">
                                    <input id="parent_phone" type="text" name="parent_phone"
                                           value="{{$parentRestaurantData[0]['phone']}}" onkeypress="insertNumberOnly(event)" autocomplete="off">
                                    <span for="parent_phone">Phone</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 margin-top-30">
                                <div class="input__field">
                                    <input id="parent_email" type="text" name="parent_email"
                                           value="{{$parentRestaurantData[0]['email']}}" autocomplete="off">
                                    <span for="parent_email">Email</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div id="locationfrom" class="row">
                        <?php $default_restaurant_id = $i=0;
                        $role= $user->role;
                        ?>
                        @foreach($branchRestaurantData as $branch)
                            <div class="row white-box box-shadow {{($role != "manager")?'margin-top-30':'margin-top-0'}} locationBox">
                                <input id="id[]" type="hidden" value="{{ $branch->id}}" name="id[]">
                                <!-- <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 {{($role != "manager")?'accordion-header':''}}">{{ $branch->restaurant_name}} <span class="fa fa-plus {{($role == "manager")?'hidden':''}}"></span></div> -->
                                <div class="col-xs-12 font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 topTil">Basic Information</div>
                                <div class="col-xs-12 padding-left-25 padding-right-25 {{($role != "manager")?'content':''}}">
                                <div class="fullWidth padding-bottom-20">
                                    <?php if($branch->is_default_outlet==1)$default_restaurant_id=$branch->id; ?>
                                    @if($role != "manager")
                                        <div id="defaultLocation{{$i}}" class="col-xs-12 margin-top-10 margin-bottom-10">
                                            <div class="fullWidth defaultLocation flex-box flex-align-item-center padding-top-bottom-10">
                                                <div class="col-xs-6 no-padding-left flex-box flex-align-item-center">
                                                    <span class="font-size-14 font-weight-600" for="is_default_outlet{{ $branch->id}}">
                                                        Default Location
                                                    </span>
                                                </div>
                                                <div class="col-xs-6 text-right no-padding-right">
                                                    <div class="toggle__container padding-top-10">
                                                        <button type="button" id="is_default_outlet{{$i}}" class="btn btn-xs btn-secondary btn-toggle change_setting btn-defaultLocation {{ ($branch->is_default_outlet==1) ? 'active' : '' }}" data-toggle="button" aria-pressed="true" autocomplete="off">
                                                            <div class="handle"></div>
                                                        </button>
                                                        <input type="radio" class="hidden" {{ ($branch->is_default_outlet==1) ? 'checked' : '' }} value="{{ $branch->id}}" name="is_default_location">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="selct-picker-plain position-relative">
                                            <select id="language_id{{$i}}" name="language_id[]"
                                                    data-size="8" class="selectpicker ignore"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700 {{($role == "manager")?'pointer-none':''}}">
                                                <option value="">Select Language</option>
                                                @foreach($languageData as $lang)
                                                    <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id || (count(old())==0 && isset($branch->language_id) && $branch->language_id==$lang->id)) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                                                @endforeach
                                            </select>
                                            <span for="language_id{{$i}}" class="{{ (isset($branch->language_id)) ? 'hasVal' :'' }} label-field" >Default Language</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="selct-picker-plain position-relative">
                                            <select name="supported_languages[{{$i}}][]"
                                                    id="supported_languages{{$i}}"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700 {{($role == "manager")?'pointer-none':''}}"
                                                    multiple
                                                    class="selectpicker form-control{{ $errors->has('supported_languages') ? ' is-invalid' : '' }}">
                                                <option value="">Supported Languages</option>
                                                <?php
                                                $supportedLanguageArr = [];
                                                if ($branch->supported_languages != '') {
                                                    $supportedLanguageArr = explode(',', $branch->supported_languages);
                                                }
                                                foreach($languageData as $lang) {
                                                    echo '<option value="'.$lang->id.'"' . (in_array($lang->id, $supportedLanguageArr) ? 'selected' :'' ).'>'.ucfirst($lang->language_name) .'</option>';
                                                }
                                                ?>
                                            </select>
                                            <span for="supported_languages{{$i}}" class="hasVal label-field">Supported Languages</span>
                                        </div>
                                    </div>
                                        <div class="clearfix"></div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="restaurant_name{{$i}}" type="text"
                                                   value="{{ $branch->restaurant_name}}" name="restaurant_name[]"
                                                   autocomplete="off" {{($role == "manager")?'readonly':''}} />
                                            <span for="restaurant_name{{$i}}">Restaurant/Location Name</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="input__field">
                                            <input id="source_url{{$i}}" type="text"
                                                   value="{{ $branch->source_url}}" name="source_url[]"
                                                   autocomplete="off" {{($role == "manager")?'readonly':''}} />
                                            <span for="source_url{{$i}}">Website Url</span>
                                        </div>
                                    </div>

                                    <?php if($role != "manager") { ?>
				    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="title{{$i}}" type="text"
                                                   value="{{ $branch->title}}" name="title[]"
                                                   autocomplete="off">
                                            <span for="title{{$i}}">Title</span>
                                        </div>
                                    </div>
				   <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="input__field">
                                            <input id="slug{{$i}}" type="text"
                                                   value="{{ $branch->slug}}" name="slug[]"
                                                   autocomplete="off">
                                            <span for="slug{{$i}}">Slug</span>
                                        </div>
                                    </div>	
                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="support_from{{$i}}" type="text"
                                                   value="{{ $branch->support_from}}" name="support_from[]"
                                                   autocomplete="off">
                                            <span for="support_from{{$i}}">Support Email From</span>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="selct-picker-plain position-relative">
                                            <select id="status{{$i}}" name="status[]" data-size="28" class="selectpicker"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value="">Select </option>
                                                <option value="1" {{ ($branch->status==1) ? 'selected' :'' }}>Active </option>
                                                <option value="0" {{ ($branch->status==0) ? 'selected' :'' }}>In-Active</option>
                                            </select>
                                            <span for="status{{$i}}" class="hasVal label-field" >Status</span>
                                        </div>
                                    </div>

                                    <?php } ?>

                                    {{--<div class="col-xs-12">
                                        <div class="fullWidth margin-bottom-20 margin-top-20 separator"></div>
                                    </div>--}}

                                    <div class="fullWidth margin-top-30 padding-top-10 margin-bottom-10 topTil">
                                        <!-- <label class="col-xs-12">Branch Address</label> -->
                                        <label class="col-xs-12">Contact Information</label>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-20">
                                        <div class="selct-picker-plain position-relative">
                                            <select name="contact_person_title[]" id="contact_person_title{{$i}}"
                                                    class="selectpicker"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value=""></option>
                                                <option value="Owner" {{ ($branch->contact_person_title=="Owner") ? 'selected' :'' }}>
                                                    Owner
                                                </option>
                                                <option value="Manager" {{ ($branch->contact_person_title=="Manager") ? 'selected' :'' }}>
                                                    Manager
                                                </option>
                                            </select>
                                            <span for="contact_person_title{{$i}}" class="{{ ($branch->contact_person_title!="") ? 'hasVal' :'' }} label-field">Title</span>
                                        </div>
                                        {{--<div class="input__field">
                                            <input id="title" type="text" value="{{$branch->contact_person_title}}" name="contact_person_title[]" autocomplete="off">
                                            <span for="title">Title</span>
                                        </div>--}}
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="input__field">
                                            <input id="id_ct0{{$i}}" type="text" name="contact_person[]"
                                                   value="{{ $branch->contact_person}}" autocomplete="off">
                                            <span for="id_ct0{{$i}}">Contact Person</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="locationEmail{{$i}}" type="text" name="contact_email[]"
                                                   value="{{ $branch->contact_email}}" autocomplete="off">
                                            <span for="locationEmail{{$i}}">Email</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right {{($user->role = "manager")?'pull-right66':''}}">
                                        <div class="input__field">
                                            <input id="locationPhone{{$i}}" type="text" name="contact_phone[]"
                                                   value="{{ $branch->contact_phone}}"   autocomplete="off">
                                            <span for="locationPhone{{$i}}">Phone</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="contact_address{{$i}}" type="text" value="{{ $branch->contact_address}}"
                                                   name="contact_address[]" autocomplete="off">
                                            <span for="contact_address{{$i}}">Location Address</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="input__field">
                                            <input id="locationAddress2{{$i}}" type="text"
                                                   value="{{ $branch->contact_address2}}" name="contact_address2[]"
                                                   autocomplete="off">
                                            <span for="locationAddress2{{$i}}">Address 2</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="locationStreet{{$i}}" type="text" name="contact_street[]"
                                                   value="{{ $branch->contact_street}}" autocomplete="off">
                                            <span for="locationStreet{{$i}}">Street</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="selct-picker-plain position-relative">
                                            <select name="contact_city_id[]" id="contact_city_id{{$i}}" class="selectpicker"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value="0">City</option>
                                                @foreach($cites as $lang)
                                                    <option value="{{ $lang->id }}" {{ ($branch->contact_city_id==$lang->id) ? 'selected' :'' }}>
                                                        {{ isset($lang->city_name)?ucfirst($lang->city_name):'' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span for="contact_city_id{{$i}}" class="{{ ($branch->contact_city_id!="") ? 'hasVal' :'' }} label-field">City</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="selct-picker-plain position-relative">
                                            <select name="contact_state_id[]" id="contact_state_id{{$i}}" class="selectpicker"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value="0">State</option>
                                                @foreach($state as $lang)
                                                    <option value="{{ $lang->id }}" {{ ($branch->contact_state==$lang->id) ? 'selected' :'' }}>
                                                        {{ isset($lang->state)?ucfirst($lang->state):'' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span for="contact_state_id{{$i}}" class="{{ ($branch->cstate_id==$lang->id) ? 'hasVal' :'' }} label-field">State</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="selct-picker-plain position-relative">
                                            <select name="contact_country_id[]" id="contact_country_id{{$i}}"
                                                    class="selectpicker"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value=""></option>
                                                @foreach($country as $lang)
                                                    <option value="{{ $lang->id }}" {{ ($branch->ccountry_id==$lang->id) ? 'selected' :'' }}>
                                                        {{ isset($lang->country_name)?ucfirst($lang->country_name):'' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span for="contact_country_id{{$i}}" class="{{ ($branch->ccountry_id==$lang->id) ? 'hasVal' :'' }} label-field">Country</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="lat{{$i}}" type="text" name="lat[]"
                                                   value="{{ $branch->lat}}" autocomplete="off">
                                            <span for="lat{{$i}}">Lat</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="input__field">
                                            <input id="lng{{$i}}" type="text" name="lng[]"
                                                   value="{{ $branch->lng}}" autocomplete="off">
                                            <span for="lng{{$i}}">Long</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="locationZipcode{{$i}}" type="text" name="contact_zipcode[]"
                                                   value="{{ $branch->contact_zipcode}}" autocomplete="off">
                                            <span for="locationZipcode{{$i}}">Zipcode</span>
                                        </div>
                                    </div>

                                    <div class="fullWidth margin-top-40">
                                        <label role="button" class="custom_checkbox relative padding-left-40 margin-left-5 font-weight-600" style="padding-top:3px">
                                            <!-- Use similar address for branch and pickup -->
                                            Use Same Address for Pickup
                                            <input id="same_branch_and_pickup_address{{$i}}" {{($branch->is_pickup_contact_address_same==1)?"checked=true":""}} class="hide sameBranchAndPickupAddress" onclick="bothAddress1('pickupAddress{{$i}}','same_branch_and_pickup_address{{$i}}',{{$branch->id}})" type="checkbox" name="same_branch_and_pickup_address[{{$i}}]" value="0">
                                            <span class="control_indicator"></span>
                                        </label>
                                    </div>

                                </div>

                                <div id="pickupAddress{{$i}}" class="fullWidth margin-bottom-30 {{($branch->is_pickup_contact_address_same==1)?"hidden":""}}">

                                    <div class="fullWidth margin-top-20 padding-top-10">
                                        <div class="tbl-radio-btn margin-left-15 topTil">
                                            <label>Pickup Address </label>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="selct-picker-plain position-relative">
                                            <select name="pickup_person_title[]" id="pickup_person_title{{$i}}"
                                                    class="selectpicker"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value="">Title</option>
                                                <option value="Owner" {{ ($branch->pickup_contact_person_title=="Owner") ? 'selected' :'' }}>
                                                    Owner
                                                </option>
                                                <option value="Manager" {{ ($branch->pickup_contact_person_title=="Manager") ? 'selected' :'' }}>
                                                    Manager
                                                </option>
                                            </select>
                                            <span for="pickup_person_title{{$i}}" class="{{ ($branch->pickup_contact_person_title!="") ? 'hasVal' :'' }} label-field">Title</span>
                                        </div>
                                        {{--<div class="input__field">
                                            <input id="title" type="text" value="{{$branch->pickup_contact_person_title}}" name="pickup_person_title[]" autocomplete="off">
                                            <span for="title">Title</span>
                                        </div>--}}
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="input__field">
                                            <input id="pickupLocationContactPerson{{$i}}" type="text"
                                                   name="pickup_contact_person[]"
                                                   value="{{ $branch->pickup_contact_person}}" autocomplete="off">
                                            <span for="pickupLocationContactPerson{{$i}}">Contact Person</span>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30 ">
                                        <div class="input__field">
                                            <input id="pickupLocationEmail{{$i}}" type="text" name="pickup_contact_email[]"
                                                   value="{{ $branch->pickup_contact_email}}" autocomplete="off">
                                            <span for="pickupLocationEmail{{$i}}">Email</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="input__field">
                                            <input id="pickupLocationPhone{{$i}}" type="text" name="pickup_contact_phone[]"
                                                   value="{{ $branch->phone}}"  autocomplete="off">
                                            <span for="pickupLocationPhone{{$i}}">Phone</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="pickup_contact_address{{$i}}" type="text" value="{{ $branch->address}}"
                                                   name="pickup_contact_address[]" autocomplete="off">
                                            <span for="pickup_contact_address{{$i}}">Location Address</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                                        <div class="input__field">
                                            <input id="pickupLocationAddress2{{$i}}" type="text"
                                                   value="{{ $branch->address2}}" name="pickup_contact_address2[]"
                                                   autocomplete="off">
                                            <span for="pickupLocationAddress2{{$i}}">Address 2</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="pickupLocationStreet{{$i}}" type="text" name="pickup_contact_street[]"
                                                   value="{{ $branch->street}}" autocomplete="off">
                                            <span for="pickupLocationStreet{{$i}}">Street</span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 margin-top-30  pull-right">
                                        <div class="selct-picker-plain position-relative">
                                            <select id="pickup_contact_city_id{{$i}}" name="pickup_contact_city_id[]"
                                                    data-size="8" class="selectpicker ignore"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value="">City</option>
                                                @foreach($cites as $lang)
                                                    <option value="{{ $lang->id }}" {{ ($branch->city_id==$lang->id) ? 'selected' :'' }}>
                                                        {{ isset($lang->city_name)?ucfirst($lang->city_name):'' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span for="pickup_contact_city_id{{$i}}" class="{{ ($branch->city_id!="") ? 'hasVal' :'' }} label-field">City</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="selct-picker-plain position-relative">
                                            <select id="pickup_contact_state{{$i}}[]" name="pickup_contact_state[]"
                                                    data-size="8" class="selectpicker ignore"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value="">State</option>
                                                @foreach($state as $lang)
                                                    <option value="{{ $lang->id }}" {{ ($branch->pickup_state==$lang->id) ? 'selected' :'' }}>
                                                        {{ isset($lang->state)?ucfirst($lang->state):'' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span for="pickup_contact_state{{$i}}[]" class="{{ ($branch->pstate_id==$lang->id) ? 'hasVal' :'' }} label-field">State</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30  pull-right">
                                        <div class="selct-picker-plain position-relative ">
                                            <select id="pickup_contact_country{{$i}}" name="pickup_contact_country[]"
                                                    data-size="8" class="selectpicker ignore"
                                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                <option value=""></option>
                                                @foreach($country as $lang)
                                                    <option value="{{ $lang->id }}" {{ ($branch->pcountry_id==$lang->id) ? 'selected' :'' }}>
                                                        {{ isset($lang->country_name)?ucfirst($lang->country_name):'' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span for="pickup_contact_country{{$i}}"  class="{{ ($branch->pcountry_id==$lang->id) ? 'hasVal' :'' }} label-field">Country</span>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-5 margin-top-30">
                                        <div class="input__field">
                                            <input id="pickupLocationZipcode{{$i}}" type="text"
                                                   name="pickup_contact_zipcode[]" value="{{ $branch->zipcode}}"
                                                   autocomplete="off">
                                            <span for="pickupLocationZipcode{{$i}}">Zipcode</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="brand_image{{$i}}" class="fullWidth margin-bottom-30">

                                    <div class="fullWidth margin-top-20 padding-top-10">
                                        <div class="tbl-radio-btn margin-left-15 topTil">
                                            <label>Other Info </label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-5 margin-top-30"> Background Image
                                        <div class="input__field">
                                            
                                            <input type="file" name="restaurant_image_name[{{$branch->id}}]" accept="image/*"/>
                                        </div>
                                        <div class="">
                                            <?php if($branch->restaurant_image_name!=''){ ?>
                                                <img src="/{{$branch->restaurant_image_name}}" width="100" height="100" >
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 margin-top-30"> Logo
                                        <div class="input__field">
                                            
                                            <input type="file" name="brand_log[{{$branch->id}}]" accept="image/*"/>
                                        </div>
                                        <div class="">
                                            <?php if($branch->brand_log!=''){ ?>
                                                <img src="/{{$branch->brand_log}}" width="100" height="100" >
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div><?php $i++;?>
                        @endforeach

                            <input type="hidden" value="{{ $default_restaurant_id}}" name="is_default_outlet">
                    </div>
                    @can('Update General Info')
                    <div class="row">
                        <input type="hidden" id="i" name="i" value="{{$i}}" />
                        <div class="col-xs-12 text-center margin-top-30">
                            <input type="submit" class="btn_ btn__primary font-weight-600 box-shadow" value="Save"/>
                            {{-- <button onclick="return addNewLocationForm(i++)" class="button btn_ btn__holo font-weight-600 box-shadow margin-left-15">Add New Location</button>--}}
                            @if($role != "manager")

                            <button class="button btn_ btn__holo addNewFormTrigger font-weight-600 box-shadow margin-left-15">Add New Location</button>

                            {{--<input type="button" class="btn_ addNewFormTrigger btn__primary font-weight-600 box-shadow"
                                   value="Add New Location">--}}
                                @endif
                        </div>
                    </div>
                    @endcan
                </div>
            </form>
        </div>




        <script type="text/javascript">

	    function bothAddress1(id,checkboxId,restId){
                if($("#"+checkboxId).is(':checked') == true){
                    $("#"+id).addClass('hidden');
		     $("#"+checkboxId).val(restId);	
                } else {
                    $("#"+id).removeClass('hidden');
			$("#"+checkboxId).val(0);
                }
            }	
            function bothAddress(id,checkboxId){
                if($("#"+checkboxId).is(':checked') == true){
                    $("#"+id).addClass('hidden');
                } else {
                    $("#"+id).removeClass('hidden');
                }
            }


            $('.addNewFormTrigger').on('click', function () {
                var i = $('#i').val();
                $('#i').val((parseInt(i)+ 1)) ;
               // alert(i)
                var htmlForm = '<div id="locationBox'+ i +'" class="row white-box box-shadow margin-top-30 locationBox addMoreLocationBox">\n' +
                    '                                <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-right-40 padding-top-20 padding-bottom-20 accordion-header">Add New Location <a href="javascript:void(0)" onclick="removeLocationBox(\'locationBox'+ i +'\')" class="icon-close font-size-18 gray pull-right"></a></div>\n' +
                    '                                <input id="id[]" type="hidden" value="0" name="id[]" >\n' +
                    '                                <div class="col-xs-12 padding-left-25 padding-right-25 margin-bottom-20 content">\n' +
                    '                                    <div class="fullWidth margin-top-30 margin-bottom-10 padding-top-10">\n' +
                    '                                        <label class="col-xs-12">Branch Address</label>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                     <div class="selct-picker-plain position-relative">\n' +
                    '                                       <select name="contact_person_title[]" id="contact_person_title'+ i +'" class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                    '                                           <option value="">Title</option>\n' +
                    '                                           <option value="Owner">Owner</option>\n' +
                    '                                           <option value="Manager">Manager</option>\n' +
                    '                                       </select>\n' +
                    '                                       <span for="contact_person_title'+ i +'" >Title</span>\n' +
                    '                                       </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="locationContactPerson' + i + '" type="text" name="contact_person[]" value="" autocomplete="off">\n' +
                    '                                            <span for="locationContactPerson' + i + '">Contact Person</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                   <div class="clearfix"></div><div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="locationEmail' + i + '" type="text" name="contact_email[]" value="" autocomplete="off">\n' +
                    '                                            <span for="locationEmail' + i + '">Email</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="locationPhone' + i + '" type="text" name="contact_phone[]"  autocomplete="off">\n' +
                    '                                            <span for="locationPhone' + i + '">Phone</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="restaurant_name' + i + '" type="text" value="" name="restaurant_name[]" autocomplete="off">\n' +
                    '                                            <span for="restaurant_name' + i + '">Location Name</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="contact_address' + i + '" type="text" value="" name="contact_address[]" autocomplete="off">\n' +
                    '                                            <span for="contact_address' + i + '"></span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="locationAddress2' + i + '" type="text" value="" name="contact_address2[]" autocomplete="off">\n' +
                    '                                            <span for="locationAddress2' + i + '">Address 2</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="locationStreet' + i + '" type="text" name="contact_street[]" value="" autocomplete="off">\n' +
                    '                                            <span for="locationStreet' + i + '">Street</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                   <div class="input__field">\n' +
                    '                                   <input id="lat" type="text" name="lat[]"\n' +
                    '                               value="" autocomplete="off">\n' +
                     '                                   <span for="pickupLocationStreet">Lat</span>\n' +
                     '                                   </div>\n' +
                      '                                 </div>\n' +
                       '                                 <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                        '                                <div class="input__field">\n' +
                         '                               <input id="lng" type="text" name="lng[]"\n' +
                          '                          value="" autocomplete="off">\n' +
                          '                              <span for="pickupLocationStreet">Long</span>\n' +
                           '                             </div>\n' +
                            '                            </div>\n' +
                    '                                     <div class="col-xs-12 col-sm-5 margin-top-30  pull-right">\n' +
                                                        '<div class="selct-picker-plain position-relative">\n' +
                                                        '<select id="contact_city_id'+ i +'[]" name="contact_city_id[]" data-size="8" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                                                        ' <option value=""></option>\n' +
                                                            @foreach($cites as $lang)

                                                                '<option value="{{ $lang->id }}">\n' +
                                                        '{{ isset($lang->city_name)?ucfirst($lang->city_name):'' }}\n' +
                                                        '</option>\n' +
                                                            @endforeach
                                                                '</select>\n' +
                                                        '<span for="contact_city_id'+ i +'[]" >City</span>\n'+


                                                        '</div>\n' +
                                                        '</div>\n' +
                                                        '<div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                                                        '<div class="selct-picker-plain position-relative">\n' +
                                                        '<select id="contact_state_id'+ i +'[]" name="contact_state_id[]" data-size="8" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                                                        ' <option value=""></option>\n' +
                                                            @foreach($state as $lang)
                                                                '<option value="{{ $lang->id }}">\n' +
                                                        '{{ isset($lang->state)?ucfirst($lang->state):'' }}\n' +
                                                        '</option>\n' +
                                                        @endforeach
                                                        '</select>\n' +
                                                        '<span for="contact_state_id' + i + '[]">State</span> \n' +
                                                        '</div>\n' +
                                                        '</div>\n' +
                                                        '<div class="col-xs-12 col-sm-5 margin-top-30  pull-right">\n' +
                                                        '<div class="selct-picker-plain position-relative ">\n' +
                                                        '<select id="contact_country_id'+ i +'[]" name="contact_country_id[]" data-size="8" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                                                        ' <option value=""></option>\n' +
                                                            @foreach($country as $lang)
                                                                '<option value="{{ $lang->id }}">\n' +
                                                            '{{ isset($lang->country_name)?ucfirst($lang->country_name):'' }}\n' +
                                                                '</option>\n' +
                                                            @endforeach
                                                                '</select> ' +
                    '                                   <span class="contact_country_id'+ i +'[]">Country</span>'+
                    '                                   </div>\n' +
                    '                                   </div>\n' +
                    '                                       <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="locationZipcode' + i + '" type="text" name="contact_zipcode[]" value="" autocomplete="off">\n' +
                    '                                            <span for="locationZipcode' + i + '">Zipcode</span>\n' +
                    '                                      </div>\n' +
                    '                                    </div>\n' +
                    //'                                </div>\n' +
                     '                               <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                        <div class="selct-picker-plain position-relative">\n' +

                    '                                   <select id="status'+ i +'" name="status[]" data-size="28" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                    '                                   <option value="">Select </option>\n' +
                    '                                   <option value="1"  >Active </option>\n' +
                    '                                   <option value="0"  >In-Active</option>\n' +
                    '                                   </select>\n' +
                    '                                   <span for="status'+ i +'">Status</span>\n' +
                    '                                   </div>\n' +
                    '                                   </div>\n' +
                    '                                    <div class="fullWidth margin-top-40">\n' +
                    '                                        <label role="button" class="custom_checkbox relative padding-left-40 margin-left-5 font-weight-600">\n' +
                    '                                            Use similar address for branch and pickup\n' +
                    '                                            <input id="same_branch_and_pickup_address{{$i}}" checked class="hide sameBranchAndPickupAddress" onclick="bothAddress(\'pickupAddress{{$i}}\',\'same_branch_and_pickup_address{{$i}}\')" type="checkbox" name="same_branch_and_pickup_address[{{$i}}]" value="{{$i}}">\n' +
                    '                                            <span class="control_indicator"></span>\n' +
                    '                                        </label>\n' +
                    '                                    </div>\n'+
                    '                                <div id="pickupAddress' + i +'" class="fullWidth margin-bottom-10 hidden">\n' +
                    '                                   <div class="fullWidth margin-top-40 pull-right">\n' +
                    '                                    <div class="tbl-radio-btn margin-left-15">\n' +
                    '                                        <label>Pickup Address</label>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                     <div class="selct-picker-plain position-relative">\n' +
                    '                                       <select name="pickup_person_title[]" id="pickup_person_title' + i +'" class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                    '                                           <option value="">&nbsp;</option>\n' +
                    '                                           <option value="Owner">Owner</option>\n' +
                    '                                           <option value="Manager">Manager</option>\n' +
                    '                                       </select>\n' +
                    '                                       <span for="pickup_person_title' + i +'">Title</span>\n' +
                    '                                       </div>\n' +
                    //'                                        <div class="input__field">\n' +
                    //'                                            <input id="title" type="text" value="" name="pickup_person_title[]" autocomplete="off">\n' +
                    //'                                            <span for="title">Title</span>\n' +
                    //'                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="pickupLocationContactPerson' + i + '" type="text" name="pickup_contact_person[]" value="" autocomplete="off">\n' +
                    '                                            <span for="pickupLocationContactPerson' + i + '">Contact Person</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div><div class="clearfix"></div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="pickupLocationEmail' + i + '" type="text" name="pickup_contact_email[]" value="" autocomplete="off">\n' +
                    '                                            <span for="pickupLocationEmail' + i + '">Email</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="pickupLocationPhone' + i + '" type="text" name="pickup_contact_phone[]"   autocomplete="off">\n' +
                    '                                            <span for="pickupLocationPhone' + i + '">Phone</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="pickup_contact_address' + i + '" type="text" value="" name="pickup_contact_address[]" autocomplete="off">\n' +
                    '                                            <span for="pickup_contact_address' + i + '"></span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="pickupLocationAddress2' + i + '" type="text" value="" name="pickup_contact_address2[]" autocomplete="off">\n' +
                    '                                            <span for="pickupLocationAddress2' + i + '">Address 2</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="pickupLocationStreet' + i + '" type="text" name="pickup_contact_street[]" value="" autocomplete="off">\n' +
                    '                                            <span for="pickupLocationStreet' + i + '">Street</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                     <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                       <div class="selct-picker-plain position-relative">\n' +
                    '                                           <select id="pickup_contact_city_id" name="pickup_contact_city_id[]" data-size="8" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                    '                                               <option value="" selected></option>\n' +
                                                                        @foreach($cites as $lang)
                    '                                                       <option value="{{ $lang->id }}">\n' +
                    '                                                           {{ isset($lang->city_name)?ucfirst($lang->city_name):'' }}\n' +
                    '                                                       </option>\n' +
                                                                        @endforeach
                    '                                           </select>\n' +
                    '                                           <span for="pickup_contact_city_id">City</span>\n' +
                    '                                       </div>\n' +
                    '                                   </div>\n' +
                    '                                   <div class="clearfix"></div><div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                       <div class="selct-picker-plain position-relative">\n' +
                    '                                           <select id="pickup_contact_state'+ i +'[]" name="pickup_contact_state[]" data-size="8" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                    '                                               <option value="" selected></option>\n' +
                                                                        @foreach($state as $lang)
                    '                                                       <option value="{{ $lang->id }}">\n' +
                    '                                                           {{ isset($lang->state)?ucfirst($lang->state):'' }}\n' +
                    '                                                       </option>\n' +
                                                                        @endforeach
                    '                                           </select>\n' +
                    '                                           <span for="pickup_contact_state'+ i +'[]">State</span>\n'+
                    '                                       </div>\n' +
                    '                                    </div>\n' +
                    '<div class="col-xs-12 col-sm-5 margin-top-30  pull-right">\n' +
                    '<div class="selct-picker-plain position-relative ">\n' +
                    '<select id="pickup_contact_'+ i +'country[]" name="pickup_contact_country[]" data-size="8" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">\n' +
                    ' <option value="" selected></option>\n' +
                        @foreach($country as $lang)
                            '<option value="{{ $lang->id }}">\n' +
                        '{{ isset($lang->country_name)?ucfirst($lang->country_name):'' }}\n' +
                            '</option>\n' +
                        @endforeach
                            '</select> ' +
                        '<span for="pickup_contact_'+ i +'country[]">Country</span>\n'+


                    '</div>\n' +
                    '</div>\n' +
                    '                                    <div class="col-xs-12 col-sm-5 margin-top-30">\n' +
                    '                                        <div class="input__field">\n' +
                    '                                            <input id="pickupLocationZipcode' + i + '" type="text" name="pickup_contact_zipcode[]" value="" autocomplete="off">\n' +
                    '                                            <span for="pickupLocationZipcode' + i + '">Zipcode</span>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                            </div>';
                $("#locationfrom").append(htmlForm);
                $(".selectpicker").selectpicker("refresh");
                $('#cms-accordion').accordion("refresh");
                initAutocompleteNew(i);

                $(".addMoreLocationBox .input__field input").each(function (key, value) {
                    //console.log($(this).val())

                    if ($(this).val()) {
                        $(this).next().addClass('hasVal')
                    } else {
                        $(this).next().removeClass('hasVal')
                    }
                });

                return false;
            })

            window.removeLocationBox = function(boxId){
                $("#"+boxId).remove();
            }

            $(".btn-defaultLocation").click(function(){
                var getResID = $(this).parents('.toggle__container').find('input').val();

                $(".btn-defaultLocation").removeClass("active");
                $(".btn-defaultLocation").attr("aria-pressed","false");
                $(".btn-defaultLocation").parents('.toggle__container').find('input').removeAttr("checked")
                $(this).parents('.toggle__container').find('input').click();console.log("getResID===>" + getResID)
                $("input[name='is_default_outlet']").val(getResID);
            })

            $('span[data-toggle="popover"]').popover();

	        $(document).ready(function(){
	            setTimeout(function () {
                    $("#restaurantform .input__field input").each(function (key, value) {
                        //console.log($(this).val())
                        if ($(this).val()) {
                            $(this).next().addClass('hasVal')
                        } else {
                            $(this).next().removeClass('hasVal')
                        }
                    });

                    $(".selct-picker-plain select").each(function (key, value) {
                        //console.log($(this).val())
                        if ($(this).val()){
                            $(this).parents(".selct-picker-plain").find("span.label-field").addClass('hasVal')
                        } else {
                            $(this).parents(".selct-picker-plain").find("span.label-field").removeClass('hasVal')
                        }
                    });

                }, 1000);
            })


            window.localStorage.removeItem("prevSelectOpt");

            $(document).on("shown.bs.select", ".selectpicker", (
                function () {
                    window.localStorage.setItem('prevSelectOpt', $(this).val());
                })
            );

            $(document).on("change", ".selectpicker", (
                function () {
                    if ($(this).val() != "") {
                        $(this).parents(".bootstrap-select").next().addClass("hasVal")
                    }
                })
            )

            $(document).on("hidden.bs.select", ".selectpicker", (
                function () {
                    if (window.localStorage.getItem('prevSelectOpt') == $(this).val()) {
                        //$(this).selectpicker('val','0')
                        //$(this).parents("").find(".filter-option").text("")
                        //$(this).parents(".bootstrap-select").next().removeClass("hasVal")
                    }
                })
            );

        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC55ZKtM00wLqY0sG1_YGo52LhfZMg3u6w&libraries=places&callback=initAutocomplete" async></script>
        <script>
            // This sample uses the Autocomplete widget to help the user select a
            // place, then it retrieves the address components associated with that
            // place, and then it populates the form fields with those details.
            // This sample requires the Places library. Include the libraries=places
            // parameter when you first load the API. For example:
            // <script
            // src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

            //var  autocomplete, autocomplete1;
            var autocomplet = [];
            var autocomplet1 = [];
            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                country: 'long_name',
                postal_code: 'short_name'
            };
            function initAutocompleteNew(i) {
                autocomplet[i] = new google.maps.places.Autocomplete(
                    document.getElementById('contact_address' + i), {types: ['geocode']});
                autocomplet1[i] = new google.maps.places.Autocomplete(
                    document.getElementById('pickup_contact_address'+ i), {types: ['geocode']});
                 
                autocomplet[i].setFields(['address_component']);
                autocomplet1[i].setFields(['address_component']);
		
            }
            function initAutocomplete() {
                // Create the autocomplete object, restricting the search predictions to
                // geographical location types.
                var j = document.getElementById('i').value;
                for(var i=0;i<j;i++) {
                    autocomplet[i] = new google.maps.places.Autocomplete(
                        document.getElementById('contact_address' + i), {types: ['geocode']});
                        autocomplet1[i] = new google.maps.places.Autocomplete(
                        document.getElementById('pickup_contact_address'+ i), {types: ['geocode']});
                    
                    autocomplet[i].setFields(['address_component']);
                    autocomplet1[i].setFields(['address_component']);
                    
                }//codeAddress(j)
            }
            function initAutocomplete2() {
                // Create the autocomplete object, restricting the search predictions to
                // geographical location types.
                autocomplete = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete'), {types: ['geocode']});
                autocomplete1 = new google.maps.places.Autocomplete(
                    document.getElementById('autocomplete1'), {types: ['geocode']});
                // Avoid paying for data that you don't need by restricting the set of
                // place fields that are returned to just the address components.
                autocomplete.setFields(['address_component']);
                autocomplete1.setFields(['address_component']);
                // When the user selects an address from the drop-down, populate the
                // address fields in the form.
                //autocomplete.addListener('place_changed', fillInAddress);
                //autocomplete1.addListener('place_changed', fillInAddress1);
            }

             

            // Bias the autocomplete object to the user's geographical location,
            // as supplied by the browser's 'navigator.geolocation' object.
            function geolocate(id) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        var circle = new google.maps.Circle(
                            {center: geolocation, radius: position.coords.accuracy});
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }


            function codeAddress(address) {
                var geocoder = new google.maps.Geocoder;
                //var address = document.getElementById(('contact_address'+i)).value;
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == 'OK') {
                        var results = results.map(function (item) {
                            item.geometry.locationString = item.geometry.location.toString();
                            //document.getElementById('latlong'+i).value = item.geometry.locationString;
				console.log(item.geometry.locationString);
                            return item;

                        });
                    }
                });
            }

        </script>

         
        <script>

            function returnCheckboxStatus(ele){
                var getCheckedStatus = $("#"+ele.id).parents(".locationBox").find(".sameBranchAndPickupAddress").prop("checked");

                if(getCheckedStatus){
                    return false
                } else {
                    return true
                }
            }

            $("#restaurantform").validate({
                ignore: [],
                rules: {
                    /*'feature': {
                        required: true
                    },
                    'restaurant_type': {
                        required: true
                    },
                    'delivery_custom_message': {
                        required: true
                    },
                    'takeout_custom_message': {
                        required: true
                    },*/
                    'parent_restaurant_name': {
                        required: true
                    },
                    'cusine': {
                        required: true
                    },
                    'parent_contact_person': {
                        required: true
                    },
                    'parent_address': {
                        required: true
                    },
                    /*'parent_street': {
                        required: true
                    },*/
                    'parent_contact_city_id': {
                        required: true
                    },
                    'parent_contact_state': {
                        required: true
                    },
                    'parent_contact_country': {
                        required: true
                    },
                    'parent_contact_zipcode': {
                        required: true
                    },
                    'parent_phone': {
                        required: true
                    },
                    'parent_email': {
                        required: true,
                        email: true
                    },
                    'contact_person[]': {
                        required: true
                    },
                    'restaurant_name[]': {
                        required: true
                    },
                    'source_url[]': {
                        required: true,
                        url: true
                    },
                    'contact_address[]': {
                        required: true
                    },
                    'contact_address2[]': {
                        required: false
                    },
                    /*'contact_street[]': {
                        required: true
                    },*/
                    'contact_zipcode[]': {
                        required: true
                    },
                    'contact_email[]': {
                        required: true,
                        email: true
                    },
                    'contact_phone[]': {
                        required: true
                    },
                    'contact_state_id[]': {
                        required: true
                    },
                    'contact_country_id[]': {
                        required: true
                    },
                    'contact_city_id[]': {
                        required: true
                    },
                    'lat[]': {
                        required: true
                    },
                    'lng[]': {
                        required: true
                    },
                    'status[]': {
                        required: true
                    },
                    'pickup_contact_person[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        }
                    },
                    /*'pickup_contact_address[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        }
                    },
                    'pickup_contact_street[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        }
                    },*/
                    'pickup_contact_city_id[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        }
                    },
                    /*'pickup_contact_state[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        }
                    },
                    'pickup_contact_country[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        }
                    },*/
                    'pickup_contact_zipcode[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        }
                    },
                    'pickup_contact_email[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        },
                        email: true
                    },
                    'pickup_contact_phone[]': {
                        required: function(ele){
                            return returnCheckboxStatus(ele);
                        }
                    }
                },
                messages: {
                    feature: "Please enter the restaurant features",
                    restaurant_type: "Please select the restaurant type",
                    delivery_custom_message: "Please enter the delivery message",
                    takeout_custom_message: "Please enter the takeout message",
                    parent_restaurant_name: "Please enter the restaurant name",
                    cusine: "Please enter the cusine",
                    parent_contact_person: "Please enter the contact person name",
                    parent_address: "Please enter the contact address",
                    //parent_street: "Please enter the street address",
                    parent_contact_city_id: "Please enter the city",
                    parent_contact_state: "Please enter the state",
                    parent_contact_country: "Please enter the country",
                    parent_contact_zipcode: "Please enter the zip code",
                    parent_phone: "Please enter the valid phone number",
                    parent_email: "Please enter the valid email/admin user Id",
                    'contact_person[]': "Please enter the contact person name",
                    'restaurant_name[]': "Please enter the restaurant name",
                    'source_url[]': "Please enter the valid website url",
                    'contact_address[]': "Please enter the contact address",
                    //'contact_street[]': "Please enter the street",
                    'contact_zipcode[]': "Please enter the zipcode",
                    'contact_email[]': "Please enter the valid email",
                    'contact_phone[]': "Please enter the valid phone number",
                    'contact_state_id[]': "Please select the state",
                    'contact_country_id[]': "Please select the country",
                    'contact_city_id[]': "Please enter the city",
                    'lat[]': "Please enter the latitude",
                    'lng[]': "Please enter the longitude",
                    'status[]': "Please select the status",
                    'pickup_contact_person[]': "Please enter the contact person name",
                    //'pickup_contact_address[]': "Please enter the contact address",
                    'pickup_contact_city_id[]': "Please enter the city",
                    //'pickup_contact_country[]': "Please enter the country",
                    //'pickup_contact_street[]': "Please enter the street",
                    'pickup_contact_zipcode[]': "Please enter the zipcode",
                    'pickup_contact_email[]': "Please enter the valid email",
                    'pickup_contact_phone[]': "Please enter the valid phone number"
                },
                errorPlacement: function (error, element) {
                    if (element.is(".delivery__radio")) {
                        error.appendTo(element.parents('.allowedServices'));
                    } else { // This is the default behavior
                        error.insertAfter(element);
                    }
                },
                checkForm: function () {
                    //ert(1223)
                    this.prepareForm();
                    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
                        this.check(elements[i]);
                    }
                    return this.valid();
                }
            });
        </script>


@endsection
