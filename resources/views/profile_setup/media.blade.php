@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/info-restaurant.css')}}" rel='stylesheet' />

    <div class="main__container restaurant-media-page container__custom_working new-design-labels info-restaurant">

        <form method="POST" action="{{ route('updaterestaurantmedia') }}" enctype="multipart/form-data" id="restaurantMediaform">@csrf

            <div class="row margin-top-30">
                <div class="reservation__atto flex-direction-row">
                    <div class="reservation__title margin-top-5">
                        <h1>Media</h1>
                    </div>

                </div>
                @if(session()->has('message'))
                    <div class="alert alert-success margin-top-20">
                        {{ session()->get('message') }}
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </div>
                @endif

                <div class="row white-box box-shadow margin-top-20 social-media-apis">
                    <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-right-40 padding-top-20 padding-bottom-20">Advanced Setting</div>
                    <div class="col-xs-12 padding-left-10 padding-right-10 padding-bottom-20 uploadCSV">

                        <div class="col-xs-12 col-md-5 uploadCSV_field margin-top-20 margin-bottom-30">
                            <div class="col-xs-12 font-weight-700 margin-bottom-15 margin-bottom-25">
                                <div class="section-heading padding-bottom-10">Favicon</div>
                            </div>
                            <div class="col-xs-8">
                                <div class="input__field fullWidth uploadinput__field">
                                    <span class="uploadFileLabel" for="upload_csv_fileName">Image Url</span>
                                    <input class="uploadFileName font-size-14"  type="text" value="" class="font-size-14" readonly />
                                    <a href="javascript:void(0)" class="font-weight-700 uploadFile" >Upload</a>
                                </div>
                                <div class="input__field fullWidth">
                                    <input id="restaurant_favicon" type="file" class="hidden" value="" name="restaurant_favicon" />
                                </div>

                            </div>
                             @if(!empty($locationInfo[0]['restaurant_favicon'])) 
                            <div class="col-xs-4 flex-box flex-direction-row flex-align-item-center">
                                <img width="70%" class="restaurant_favicon" src="{{$locationInfo[0]['restaurant_favicon']}}" />
                                <a href="#" onclick="deleteImage('restaurant_favicon','restaurant_favicon_alt')" class="margin-left-15"><span class="icon-cancelled text-color-black font-size-16"></span></a>
                            </div>
                             @endif 

                        </div>

                        <div class="col-xs-12 col-md-5 uploadCSV_field margin-top-20 margin-bottom-30 pull-right">
                            <div class="col-xs-12 font-weight-700 margin-bottom-15 margin-bottom-25">
                                <div class="section-heading padding-bottom-10">Restaurant Logo</div>
                            </div>
                            <div class="fullWidth flex-box">
                                <div class="col-xs-8">
                                    <div class="input__field fullWidth uploadinput__field">
                                        <span class="uploadFileLabel" for="upload_csv_fileName">Image Url</span>
                                        <input class="uploadFileName font-size-14" type="text" value="{{ old('restaurant_logo_name') }}" readonly />
                                        <a href="javascript:void(0)" class="font-weight-700 uploadFile" >Upload</a>
                                    </div>
                                    <div class="input__field fullWidth">
                                        <input id="restaurant_logo_name" type="file" class="hidden" value="{{ old('restaurant_logo_name') }}" name="restaurant_logo_name" />
                                    </div>

                                    <div class="input__field fullWidth margin-top-20">
                                        <input type="text" name="restaurant_image_logo_alt" value="{{$locationInfo[0]['restaurant_image_logo_alt']}}" class="font-size-14 restaurant_image_logo_alt" />
                                        <span class="{{($locationInfo[0]['restaurant_image_logo_alt']!="")?'hasVal':''}}">Alt Text</span>
                                    </div>
                                </div>
                                @if ($errors->has('restaurant_logo_name'))
                                    <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('restaurant_logo_name') }}</strong></span>
                                @endif
                                 @if(!empty($locationInfo[0]['restaurant_logo_name'])) 
                                <div class="col-xs-4 flex-box flex-direction-row flex-align-item-center">
                                    <img width="70%" class="restaurant_logo_name" src="{{ ($locationInfo[0]['restaurant_logo_name'] != "")? url('/').@$locationInfo[0]['restaurant_logo_name'] : asset('images/image-placeholder-127x85.jpg') }}">
                                    <a href="#" onclick="deleteImage('restaurant_logo_name','restaurant_image_logo_alt')"  class="margin-left-15"><span class="icon-cancelled text-color-black font-size-16"></span></a>
                                </div>
                                 @endif 
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="col-xs-12 col-md-5 uploadCSV_field margin-top-20 margin-bottom-30">
                            <div class="col-xs-12 font-weight-700 margin-bottom-15 margin-bottom-25">
                                <div class="section-heading padding-bottom-10">Location Image</div>
                            </div>
                            <div class="fullWidth flex-box">
                                <div class="col-xs-8">
                                    <div class="input__field fullWidth uploadinput__field">
                                        <span class="uploadFileLabel" for="upload_csv_fileName">Image Url</span>
                                        <input class="uploadFileName font-size-14" type="text" value="{{ old('restaurant_logo_name') }}" readonly />
                                        <a href="javascript:void(0)" class="font-weight-700 uploadFile" >Upload</a>
                                    </div>

                                    <div class="input__field fullWidth">
                                        <input id="restaurant_image_name" type="file" class="hidden" value="{{ old('restaurant_image_name') }}" name="restaurant_image_name" >
                                    </div>

                                    <div class="input__field fullWidth margin-top-20">
                                        <input type="text" name="restaurant_image_name_alt" value="{{$locationInfo[0]['restaurant_image_name_alt']}}" class="font-size-14 restaurant_image_name_alt" />
                                        <span class="{{($locationInfo[0]['restaurant_image_name_alt'] != "")?'hasVal':''}}">Alt Text</span>
                                    </div>
                                </div>
                                @if ($errors->has('restaurant_image_name'))
                                    <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('restaurant_image_name') }}</strong></span>
                                @endif
                                <input type="hidden" name="image_hidden" value="1" >
                                 @if(!empty($locationInfo[0]['restaurant_image_name'])) 
                                <div class="col-xs-4 flex-box flex-direction-row flex-align-item-center">
                                    <img width="70%" class="restaurant_image_name" src="{{ ($locationInfo[0]['restaurant_image_name'] != "") ? url('/').@$locationInfo[0]['restaurant_image_name'] : asset('images/image-placeholder-127x85.jpg') }}" />
                                    <a href="#" onclick="deleteImage('restaurant_image_name','restaurant_image_name_alt')" class="margin-left-15"><span class="icon-cancelled text-color-black font-size-16"></span></a>
                                </div>
                                 @endif 
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-5 uploadCSV_field pull-right margin-top-20 margin-bottom-30">
                            <div class="col-xs-12 font-weight-700 margin-bottom-25">
                                <div class="section-heading padding-bottom-10">Restaurant Video</div>
                            </div>
                            <div class="fullWidth flex-box">
                                <div class="col-xs-8">
                                    <div class="input__field fullWidth uploadinput__field">
                                        <span class="uploadFileLabel" for="upload_csv_fileName">Video Url</span>
                                        <input class="uploadFileName font-size-14" type="text" value="{{ old('restaurant_logo_name') }}" readonly />
                                        <a href="javascript:void(0)" class="font-weight-700 uploadFile" >Upload</a>
                                    </div>

                                    <div class="input__field fullWidth">
                                        <input id="restaurant_video_name" type="file" class="hidden" value="{{ old('restaurant_video_name') }}" name="restaurant_video_name" />
                                    </div>

                                    <div class="input__field fullWidth margin-top-20">
                                        <input type="text" name="restaurant_video_name_alt" value="{{$locationInfo[0]['restaurant_video_name_alt']}}" class="font-size-14 restaurant_video_name_alt" />
                                        <span class="{{($locationInfo[0]['restaurant_video_name_alt'] != "")?'hasVal':''}}">Alt Text</span>
                                    </div>
                                </div>
                                @if ($errors->has('restaurant_video_name'))
                                        <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('restaurant_video_name') }}</strong></span>
                                @endif
                                 @if(!empty($locationInfo[0]['restaurant_video_name'])) 
                                <div class="col-xs-4 flex-box flex-direction-row flex-align-item-center">
                                    <img width="70%"  class="restaurant_video_name" src="{{($locationInfo[0]['restaurant_video_name'] != "") ? url('/').@$locationInfo[0]['restaurant_video_name'] : asset('images/image-placeholder-127x85.jpg') }}" />
                                    <a href="#" onclick="deleteImage('restaurant_video_name','restaurant_video_name_alt')" class="margin-left-15"><span class="icon-cancelled text-color-black font-size-16"></span></a>
                                </div>
                                  @endif 
                            </div>

                        </div>



                    </div>
                </div>
            </div>

            <div class="row margin-top-30 text-center">
                <input type="submit" class="btn btn__primary font-weight-600  width-120 box-shadow" value="Save" >
                <input id="id" type="hidden" value="{{ $locationInfo[0]['id']}}" name="id" >
                <input id="restaurant_name" type="hidden" value="{{ $locationInfo[0]['restaurant_name']}}" name="restaurant_name" >
            </div>

        </form>
    </div>

    <script>

        /*$("#restaurantMediaform").submit(function(){
            if(!$(this).valid()){
                return false;
            }
        })*/

        $.validator.addMethod('filesize', function (value, element, arg) {
            var minsize=1000; // min 1kb

            if((element.files[0].size>minsize)&&(element.files[0].size<=arg)){
                return true;
            }else{
                return false;
            }
        });

        $("#restaurantMediaform").validate({
            ignore: "",
            /*rules: {
                restaurant_favicon: {
                    required: true,
                    extension: "png|ico",
                    filesize: 51200 //50Kb
                },
                restaurant_logo_name: {
                    required: true,
                    extension: "png|jpe?g",
                    filesize: 512000
                },
                restaurant_image_name: {
                    required: true,
                    extension: "png|jpe?g",
                    filesize: 512000
                },
                restaurant_video_name: {
                    required: true,
                    extension: "mp4",
                    filesize: 51200000
                }
            },
            messages:{
                restaurant_favicon: {
                    required: "Please upload the favicon image",
                    extension: "We accept only PNG and ICON",
                    filesize: "Upload image less then 50KB"
                },
                restaurant_logo_name: {
                    required: "Please upload the logo",
                    extension: "We accept only PNG and JPEG",
                    filesize: "Upload image less then 500KB"
                },
                restaurant_image_name: {
                    required: "Please upload the location image",
                    extension: "We accept only PNG and JPEG",
                    filesize: "Upload image less then 500KB"
                },
                restaurant_video_name: {
                    required: "Please upload the video",
                    extension: "We accept only mp4",
                    filesize: "Upload image less then 50MB"
                }
            },*/
            submitHandler: function(form) {
                form.submit();
            }
        })

        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        };
	function deleteImage(imgCol,imgColAlt){
	  	$.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/removerestimage"; ?>',
                    data: '_token = <?php echo csrf_token() ?>&imgCol=' + imgCol + '&imgColAlt='+ imgColAlt+ '&id='+ {{ $locationInfo[0]['id']}},
                    success: function (data) {
                         $('.'+imgColAlt).val('')
			$('.'+imgCol).attr('src','{{ asset('images/image-placeholder-127x85.jpg') }}');
                    }
                });
            }
        //alert(bytesToSize(1024*50000))

    </script>


@endsection
