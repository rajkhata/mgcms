@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />


        <div class="main__container container__custom_working new-design-labels info-restaurant">
            <div class="row">
                <div class="reservation__atto flex-direction-row">
                    <div class="reservation__title margin-top-5">
                        <h1>Online Ordering <i class="fa fa-angle-right margin-left-right-5" aria-hidden="true"></i> {{$restInfoArr[0]['restaurant_name']}}</h1>
                    </div>

                </div>
                @if(session()->has('message'))
                    <div class="alert alert-success margin-top-20">
                        {{ session()->get('message') }}
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </div>
                @endif
                @include('reservation::common.message')

                <div id="cms-accordion" class="row">

                    <form  method="post" id = "submit_pause_service_form" action="/online-ordering-restaurantsettings" > @csrf
                        <input type="hidden" value ="" name = "action" id= "current_action">
                        <input type="hidden" value ="0" name = "restaurant_id" id= "restaurant_id">
                    <?php
                    foreach($restInfoArr as $restInfo){
                    $pause_service_message = (isset($restInfo) && $restInfo && isset($restInfo['pause_service_message']) && $restInfo['pause_service_message']) ? $restInfo['pause_service_message'] : "We are currently not accepting online orders. We'll be back soon.";

                    $del_class = (isset($restInfo) && isset($restInfo['food_ordering_delivery_allowed']) && $restInfo['food_ordering_delivery_allowed'] == 1 && $restInfo['is_food_order_allowed'] == 1) ? 'block' :'none';
                    $del_active = (isset($restInfo) && isset($restInfo['delivery']) && $restInfo['delivery'] == 1) ? 'active' :'';
                    $del_value = (isset($restInfo) && isset($restInfo['delivery']) && $restInfo['delivery'] == 1) ? 1 : 0;
                    $take_class = (isset($restInfo) && isset($restInfo['food_ordering_takeout_allowed']) && $restInfo['food_ordering_takeout_allowed'] == 1 && $restInfo['is_food_order_allowed'] == 1) ? 'block' :'none';
                    $take_active = (isset($restInfo) && isset($restInfo['takeout']) && $restInfo['takeout'] == 1) ? 'active' :'';
                    $take_value = (isset($restInfo) && isset($restInfo['takeout']) && $restInfo['takeout'] == 1) ? 1 : 0;
                    ?>

                            <input type="hidden" value ="<?php echo (isset($restInfo) && isset($restInfo['id']) && $restInfo['id'] ) ? $restInfo['id'] : '' ;?>" name = "restaurant_id{{$restInfo['id']}}" id = "restaurant_id{{$restInfo['id']}}">
                            <input type="hidden" value ="<?php echo (isset($restInfo) && isset($restInfo['pause_service_message']) && $restInfo['pause_service_message'] ) ? $restInfo['pause_service_message'] : '' ;?>" name = "pause_service_message{{$restInfo['id']}}" id = "pause_service_message{{$restInfo['id']}}">


                <div class="row white-box box-shadow margin-top-0 hide">
                    <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header no-mob-pad6">Turn Service On/Off <span class="fa fa-minus"></span></div>
                    <div class="col-xs-12 padding-left-40 padding-right-40 margin-top-10 margin-bottom-30 content no-mob-pad6">
                        <div class="col-xs-12 col-sm-4 col-md-4 flex-box flex-align-item-center no-padding-left overflow-visible">
                            <div class="toggle__container pull-left">
                                <button type="button" id="delivery_button{{$restInfo['id']}}" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox change_setting dayoff_custom <?php echo $del_active;?>" data-toggle="button" aria-pressed="true" autocomplete="off" rel="delivery" relid="{{$restInfo['id']}}" relmsg="{{$pause_service_message}}">
                                    <div class="handle"></div>
                                </button>
                                <input type="hidden" class="enable_deilvery_orders_button delivery_button{{$restInfo['id']}}" value ="<?php echo $del_value;?>" name = "delivery{{$restInfo['id']}}">
                            </div>
                            <div class="pull-left font-weight-700 margin-left-10">Delivery Order</div>
                        </div>
                        <div class="hide col-xs-12 col-sm-4 col-md-4 flex-box flex-align-item-center overflow-visible zero">
                            <div class="toggle__container pull-left">
                                <button type="button" id="takeout_button{{$restInfo['id']}}" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox change_setting dayoff_custom <?php echo $take_active;?>" data-toggle="button" aria-pressed="true" autocomplete="off" rel="takeout" relid="{{$restInfo['id']}}" relmsg="{{$pause_service_message}}">>
                                    <div class="handle"></div>
                                </button>
                                <input type="hidden" class="enable_takeout_orders_button takeout_button{{$restInfo['id']}}" value ="<?php echo $take_value;?>" name = "takeout{{$restInfo['id']}}">
                            </div>
                            <div class="pull-left font-weight-700 margin-left-10">Takeout Order</div>
                        </div>
                        <!--div class="col-xs-4 no-padding-right text-right margin-top-7">
                            <a href="#" class="font-weight-700">View Service History</a>
                        </div-->
                    </div>
                </div>

                 <?php }?>
                    </form>
                <form  method="POST" action="{{ route('online-ordering-update') }}" enctype="multipart/form-data" id = "submit_location_setting_form"  > @csrf
                <input type="hidden" value ="{{$restInfoArr[0]['id']}}" name = "restaurant_id" id= "restaurant_id">


                <div class="row white-box box-shadow margin-top-30 addvanced-setting">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header no-mob-pad6">Advanced Settings <span class="fa fa-plus"></span></div>

                        <div class="col-xs-12 padding-left-40 padding-right-40 margin-top-10 margin-bottom-30 content no-mob-pad6">
                            <?php
                            list($days,$hours,$minute) = explode('-', $restInfoArr[0]['kpt_calender']);
                            ?>

                                <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center overflow-visible">
                                    <div class="col-xs-5 col-sm-4 col-md-5 col-lg-6 no-padding-left font-weight-600">
                                        When will the order be ready? <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Interval + Minimum Prepatation Time=KPT"></span>
                                    </div>
                                    <div class="col-xs-7 col-sm-8 col-md-7 col-lg-6 no-padding-right text-right">
                                        <div class="col-xs-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                            <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker update_days" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left font-weight-600" name="update_days" id="update_days">
                                                    <option value="">Select Days</option>
                                                    @for ($i = 0; $i <= 30; $i++)
                                                        <option value="{{ $i }}" {{ $i == $days ? 'selected' : '' }} >{{ $i }} days</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                            <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker update_hour" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left font-weight-600" name="update_hour" id="update_hour">
                                                    <option value="">Select Hours</option>
                                                    @for ($i = 0; $i <= 24; $i++)
                                                        <option value="{{ $i }}" {{ $i == $hours ? 'selected' : '' }} >{{ $i }} hrs</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                            <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker update_min" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left font-weight-600" name="update_min" id="update_min">
                                                    <option value="">Select Minutes</option>
                                                    @for ($i = 0; $i <= 59; $i++)
                                                        <option value="{{ $i }}" {{ $i == $minute ? 'selected' : '' }}>{{ $i }} {{$i > 1 ?'mins':'min'}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center overflow-visible disblck">
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 no-padding-left font-weight-600 pad-bot-10">
                                        Minimum Preparation Time <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Minimum prep time for an order."></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6 no-padding-right flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible no-mob-pad3">
                                        <div class="col-xs-6 col-sm-6 col-md-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                            <div class="pull-right font-weight-700">Delivery</div>
                                            <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker" name="delivery_asap_gap" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                    @for ($i = 0; $i <= 60; $i+=5)
                                                        <option value="{{$i}}" {{ ( $restInfoArr[0]['delivery_asap_gap']==$i) ? 'selected' :'' }}>{{$i}} {{$i > 1 ?'mins':'min'}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <!--<div class="col-xs-6 col-sm-6 col-md-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                             <div class="pull-right font-weight-700">Takeout</div>
                                            <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker" name="carryout_asap_gap" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                    @for ($i = 0; $i <= 60; $i+=5)
                                                        <option value="{{$i}}" {{ ( $restInfoArr[0]['carryout_asap_gap']==$i) ? 'selected' :'' }}>{{$i}} {{$i > 1 ?'mins':'min'}}</option>
                                                    @endfor
                                                </select>
                                            </div> 
                                        </div> -->
                                    </div>
                                </div>

                                <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center overflow-visible disblck">
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 no-padding-left font-weight-600 pad-bot-10">
                                        Interval <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Time slots to be communicated to users for orders."></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6 no-padding-right flex-box flex-align-item-center flex-justify-content-end overflow-visible no-mob-pad3">
                                        <div class="col-xs-6 col-sm-6 col-md-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                            <div class="pull-right font-weight-700">Delivery</div>
                                            <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker" name="delivery_interval" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                    @for ($i = 0; $i <= 60; $i+=5)
                                                        <option value="{{$i}}" {{ ( $restInfoArr[0]['delivery_interval']==$i) ? 'selected' :'' }}>{{$i}} {{$i > 1 ?'mins':'min'}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <!--<div class="col-xs-6 col-sm-6 col-md-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                            <div class="pull-right font-weight-700">Takeout</div>
                                             <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker" name="carryout_interval" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                    @for ($i = 0; $i <= 60; $i+=5)
                                                        <option value="{{$i}}" {{ ( $restInfoArr[0]['carryout_interval']==$i) ? 'selected' :'' }}>{{$i}} {{$i > 1 ?'mins':'min'}}</option>
                                                    @endfor
                                                </select>
                                            </div> 
                                        </div> -->
                                    </div>
                                </div>

                                <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center overflow-visible disblck">
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6 no-padding-left font-weight-600 pad-bot-10">
                                        Order Gap <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Interval at which the system assigns slots. Current time + interval = rounded up to next available."></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6 no-padding-right flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible no-mob-pad3">
                                        <div class="col-xs-6 col-sm-6 col-md-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                            <div class="pull-right font-weight-700">Delivery</div>
                                            <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker" name="delivery_order_gap" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                    @for ($i = 0; $i <= 60; $i+=5)
                                                        <option value="{{$i}}" {{ ( $restInfoArr[0]['delivery_order_gap']==$i) ? 'selected' :'' }}>{{$i}} {{$i > 1 ?'mins':'min'}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="col-xs-6 col-sm-6 col-md-4 flex-box flex-align-item-center flex-justify-content-end no-padding-right overflow-visible">
                                            <div class="pull-right font-weight-700">Takeout</div>
                                            <div class="selct-picker-plain width-80 margin-left-15">
                                                <select class="selectpicker" name="carryout_order_gap" data-size="5" data-style="no-background-with-buttonline width-80 no-padding-left margin-top-5 no-padding-top font-weight-700">
                                                    @for ($i = 0; $i <= 60; $i+=5)
                                                        <option value="{{$i}}" {{ ( $restInfoArr[0]['carryout_order_gap']==$i) ? 'selected' :'' }}>{{$i}} {{$i > 1 ?'mins':'min'}}</option>
                                                    @endfor
                                                </select>
                                            </div> 
                                        </div> -->
                                    </div>
                                </div>


                                
                                <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                    <div class="col-xs-10 no-padding-left font-weight-600">
                                        Tax (%)
                                    </div>
                                    <div class="col-xs-2 no-padding-right">
                                        <div class="simple_input_field width-80">
                                            <input type="text" maxlength="3" class="form-control fullWidth text-right font-weight-600 color-black" value="{{(isset($restInfoArr[0]['tax']))?$restInfoArr[0]['tax']:''}}" name="tax" id="tax" />
                                        </div>
                                    </div>
                                </div>

                                <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                    <div class="col-xs-10 no-padding-left font-weight-600">
                                        Service Tax (%) <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Local service tax for this location/state (applied to subtotal)"></span>
                                    </div>
                                    <div class="col-xs-2 no-padding-right">
                                        <div class="simple_input_field width-80">
                                            <input type="text" maxlength="3" class="form-control fullWidth font-weight-600 text-right color-black" name="service_tax" id="service_tax" value="{{(isset($restInfoArr[0]['service_tax']))?$restInfoArr[0]['service_tax']:''}}"   />
                                        </div>
                                    </div>
                                </div>
				<div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center no-border">
                                    <div class="col-xs-10 no-padding-left font-weight-600">
                                        Accept Pre-Orders <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Allow customers to place orders for later in the day, the next day, and the future."></span>
                                    </div>
                                    <div class="col-xs-2 no-padding-right text-right">
                                        <div class="toggle__container pull-right">
                                            <button type="button" id="delivery_button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom {{($restInfoArr[0]['is_preorder_accept']==1)?'active':''}}" data-toggle="button" aria-pressed="true" autocomplete="off">
                                                <div class="handle"></div>
                                            </button>
                                            <input type="hidden" id="is_preorder_accept" name="is_preorder_accept" value="{{$restInfoArr[0]['is_preorder_accept']}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="fullWidth padding-bottom-10 flex-box flex-align-item-center overflow-visible">
                                    <div class="col-xs-10 no-padding-left font-weight-600">
                                        How far in days advance can customers place online delivery order?
                                    </div>
                                    <div class="col-xs-2 no-padding-right">
                                        <div class="simple_input_field width-80">
                                            <input type="text" class="form-control fullWidth font-weight-600 text-right color-black" name="preorder_delivery_accept_in_days" id="preorder_delivery_accept_in_days" value="{{$restInfoArr[0]['preorder_delivery_accept_in_days']}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="hide fullWidth padding-bottom-10 flex-box flex-align-item-center overflow-visible">
                                    <div class="col-xs-10 no-padding-left font-weight-600">
                                        How far in days advance can customers place online takeout order?
                                    </div>
                                    <div class="col-xs-2 no-padding-right">
                                        <div class="simple_input_field width-80">
                                            <input type="text" class="form-control fullWidth font-weight-600 text-right color-black" name="preorder_takeout_accept_in_days" id="preorder_takeout_accept_in_days" value="{{$restInfoArr[0]['preorder_takeout_accept_in_days']}}" />
                                        </div>
                                    </div>
                                </div>

                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-9 no-padding-left font-weight-600">
                                    Lat
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <div class="simple_input_field width-120">
                                        <input type="text" class="form-control font-weight-600 text-right color-black fullWidth" name="lat" id="lat" value="{{$restInfoArr[0]['lat']}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-9 no-padding-left font-weight-600">
                                    Long
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <div class="simple_input_field width-120">
                                        <input type="text" class="form-control font-weight-600 text-right color-black fullWidth" name="lng" id="lng" value="{{$restInfoArr[0]['lng']}}" />
                                    </div>
                                </div>
                            </div>
                            <!--div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-10 no-padding-left font-weight-600">
                                    Status
                                </div>
                                <div class="col-xs-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="status" name="status" data-size="28" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="">Select </option>
                                            <option value="1" {{ ($restInfoArr[0]['status']==1) ? 'selected' :'' }}>Active </option>
                                            <option value="0" {{ ($restInfoArr[0]['status']==0) ? 'selected' :'' }}>In-Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div-->

                        </div>
                </div>

                <div class="row white-box box-shadow margin-top-30 addvanced-setting ">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header">Delivery  Configuration <span class="fa fa-plus"></span></div>
                        <div class="col-xs-12 padding-left-40 padding-right-40 margin-top-10 margin-bottom-30 content">
                            <input type="hidden" name="delivery_config_id" value="{{(isset($restInfoArr1[0]['id']))?$restInfoArr1[0]['id']:0}}" />
                            <!--div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-10 no-padding-left font-weight-600">
                                    Delivery Provider
                                </div>
                                <div class="col-xs-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="delivery_provider_id " name="delivery_provider_id " data-size="28" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="">Select</option>
                                            @foreach($deliveryService as $dataval)
                                                <option value="{{ $dataval->id }}" {{ (isset($restInfoArr[0]['delivery_provider_id ']) && $restInfoArr[0]['delivery_provider_id ']==$dataval->id) ? 'selected' :'' }}>{{ isset($dataval->short_name)?ucfirst($dataval->short_name):'' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div-->

                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center hidden">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Tip Accepted <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Allow customers to tip on orders."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="is_tip_online" name="is_tip_online" class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="0" >None</option>
                                            <option value="1" selected='true'>
                                                Delivery
                                            </option>
                                            <option  value="2"  >
                                                Takeout
                                            </option>
                                            <option   value="3"  >
                                                Delivery & Takeout Both
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="fullWidth padding-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Tip (%) <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Default tip amount (customer adjustable)."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="simple_input_field fullWidth text-right">
                                        <input type="text" class="form-control font-weight-600 color-black fullWidth text-right"  value="{{(isset($restInfoArr[0]['tip']))?$restInfoArr[0]['tip']:''}}"   name="tip" id="tip" />
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                    <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                        Free Delivery Order <span role="button" class="icon-information text-color-grey text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Set the value at which an order qualifies for free delivery."></span>
                                    </div>
                                    <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                        <div class="input-group position-relative">
                                            <span class="input-group-addon currencySymbol font-weight-600">{{$restInfoArr[0]['currency_symbol']}}</span>
                                            <input type="text" class="form-control fullWidth fullWidth font-weight-600 text-right padding-left-15 color-black" name="free_delivery" id="free_delivery" value="{{(isset($restInfoArr[0]['free_delivery']))?$restInfoArr[0]['free_delivery']:''}}"  />
                                        </div>
                                    </div>
                                </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Minimum Delivery Amount <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Set the minimum order value to quality for delivery."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="input-group position-relative">
                                        <span class="input-group-addon currencySymbol font-weight-600">{{$restInfoArr[0]['currency_symbol']}}</span>
                                        <input type="text" class="form-control fullWidth font-weight-600 text-right padding-left-15 color-black" name="minimum_order_amount" id="minimum_order_amount" value="{{(isset($restInfoArr[0]['minimum_delivery']))?$restInfoArr[0]['minimum_delivery']:''}}"   />
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Delivery Charge Type <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Charge for delivery as a flat amount or a percentage of the subtotal."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="delivery_charge_type" name="delivery_charge_type" class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="">Select</option>
                                            <option value="1" {{(isset($restInfoArr[0]['delivery_charge_type']) && $restInfoArr[0]['delivery_charge_type']==1)?"selected='true'":''}}  >
                                                Flat
                                            </option>
                                            <option value="2" {{(isset($restInfoArr[0]['delivery_charge_type']) && $restInfoArr[0]['delivery_charge_type']==2)?"selected='true'":''}}  >
                                                Percentage
                                            </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Delivery Charge <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Set the charge for delivery based on delivery type."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="input-group position-relative">
                                        <span class="input-group-addon currencySymbol font-weight-600">{{$restInfoArr[0]['currency_symbol']}}</span>
                                        <input type="text" class="form-control fullWidth font-weight-600 text-right padding-left-15 color-black" name="delivery_charge" id="delivery_charge" value="{{(isset($restInfoArr[0]['delivery_charge']))?$restInfoArr[0]['delivery_charge']:''}}"   />
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Additional Charge Name
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="input__field">
                                        <input id="additional_charge_name" type="text" name="additional_charge_name" autocomplete="off" value="{{(isset($restInfoArr[0]['additional_charge_name']))?$restInfoArr[0]['additional_charge_name']:''}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Additional Delivery Charge Config <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Include an additional charge to deliver per order/item."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="additional_charge_config" name="additional_charge_config" class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700" >
                                            <option value="">Select</option>
                                            <option value="1" {{(isset($restInfoArr[0]['additional_charge_config']) && $restInfoArr[0]['additional_charge_config']==1)?"selected='true'":''}} >
                                                Per Order
                                            </option>
                                            <option value="2" {{(isset($restInfoArr[0]['additional_charge_config']) && $restInfoArr[0]['additional_charge_config']==2)?"selected='true'":''}} >
                                                Per Item
                                            </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-10 no-padding-left font-weight-600">
                                    Delivery Charge <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Lorem Ipsum text will come here Lorem Ipsum text will come here"></span>
                                </div>
                                <div class="col-xs-2 no-padding-right">
                                    <div class="input-group">
                                        <span class="input-group-addon font-weight-600">£</span>
                                        <input type="text" class="form-control font-weight-600 color-black" name="delivery_charge" id="delivery_charge" value="{{$restInfoArr[0]['delivery_charge']}}" />
                                    </div>
                                </div>
                            </div-->


                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Additional Delivery Charge Type <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Apply additional delivery charge as flat amount or a percentage of the subtotal."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="additional_charge_delivery_type" name="additional_charge_delivery_type" class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="">Select</option>
                                            <option value="1" {{(isset($restInfoArr[0]['additional_charge_delivery_type']) && $restInfoArr[0]['additional_charge_delivery_type']==1)?"selected":''}}  >
                                                Flat
                                            </option>
                                            <option value="2" {{(isset($restInfoArr[0]['additional_charge_delivery_type']) && $restInfoArr[0]['additional_charge_delivery_type']==2)?"selected":''}}  >
                                                Percentage
                                            </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Additional Charge for Delivery Order <span role="button" class="icon-information text-color-grey text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Additional delivery charge amount/percentage."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="input-group position-relative">
                                        <span class="input-group-addon currencySymbol font-weight-600">{{$restInfoArr[0]['currency_symbol']}}</span>
                                        <input type="text" class="form-control fullWidth font-weight-600 text-right padding-left-15 color-black" name="additional_charge_delivery" id="additional_charge_delivery" value="{{(isset($restInfoArr[0]['additional_charge_delivery']))?$restInfoArr[0]['additional_charge_delivery']:''}}"   />
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center hidden">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Additional Takeout Charge Type <span role="button" class="icon-information text-color-grey text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Apply additional takeout charge as flat amount or a percentage of the subtotal."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="additional_charge_takeout_type" name="additional_charge_takeout_type" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700" >
                                            <option value="">Select</option>
                                            <option value="1" {{(isset($restInfoArr[0]['additional_charge_takeout_type']) && $restInfoArr[0]['additional_charge_takeout_type']==1)?"selected":''}}  >
                                                Flat
                                            </option>
                                            <option value="2" {{(isset($restInfoArr[0]['additional_charge_takeout_type']) && $restInfoArr[0]['additional_charge_takeout_type']==2)?"selected":''}}   >
                                                Percentage
                                            </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center no-border hidden">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Additional Charge for Takeout Order <span role="button" class="icon-information text-color-grey text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Additional takeout charge amount/percentage."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="input-group position-relative">
                                        <span class="input-group-addon currencySymbol font-weight-600">{{$restInfoArr[0]['currency_symbol']}}</span>
                                        <input type="text" class="form-control fullWidth font-weight-600 text-right padding-left-15 color-black" name="additional_charge_takeout" id="additional_charge_takeout" value="{{(isset($restInfoArr[0]['additional_charge_takeout']))?$restInfoArr[0]['additional_charge_takeout']:''}}"  />
                                    </div>
                                </div>
                            </div>
                            <!-- nothing -->







                            <!--div class="fullWidth padding-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-10 no-padding-left font-weight-600">
                                    Sort Order
                                </div>
                                <div class="col-xs-2 no-padding-right">
                                    <div class="simple_input_field">
                                        <input type="text" class="form-control font-weight-600 color-black"  value="{{(isset($restInfoArr1[0]['sort_order']))?$restInfoArr1[0]['sort_order']:''}}" name="sort_order" id="sort_order" />

                                    </div>
                                </div>
                            </div-->

                            <!--div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-10 no-padding-left font-weight-600">
                                    Delivery Conditions
                                </div>
                                <div class="col-xs-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="delivery_condition" name="delivery_condition" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="">Select</option>
                                            <option value="1" {{(isset($restInfoArr1[0]['delivery_condition']) && $restInfoArr1[0]['delivery_condition']==1)?"selected='true'":''}} >
                                                Price Vs Destinations
                                            </option>
                                            <option value="2" {{(isset($restInfoArr1[0]['delivery_condition']) && $restInfoArr1[0]['delivery_condition']==2)?"selected='true'":''}} >
                                                Weight Vs Destinations
                                            </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-10 no-padding-left font-weight-600">
                                    Delivery Conditions
                                </div>
                                <div class="col-xs-2 no-padding-right">
                                    <div class="selct-picker-plain">
                                        <select id="filtering_rule" name="filtering_rule" class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="">Select</option>
                                            <option value="1" {{(isset($restInfoArr1[0]['filtering_rule']) && $restInfoArr1[0]['filtering_rule']==1)?"selected='true'":''}} >
                                                Standard
                                            </option>
                                            <option value="2" {{(isset($restInfoArr1[0]['filtering_rule']) && $restInfoArr1[0]['filtering_rule']==2)?"selected='true'":''}} >
                                                Advanced
                                            </option>

                                        </select>
                                    </div>
                                </div>
                            </div-->
                             

                        </div>


                    </div>
                    <?php if($restInfoArr[0]['is_delivery_geo']=='1'){
                        $disp = "block";
                        $disp2 = "none";$disp3 = "none"; }
                    elseif($restInfoArr[0]['is_delivery_geo']=='2'){
                        $disp = "none";
                        $disp2 = "none";
                        $disp3 = "block";
                    }
                    else{
                        $disp3 = "none";
                        $disp = "none";
                        $disp2 = "block"; }?>
                    <div class="hide row white-box box-shadow margin-top-30">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header no-mob-pad6">Delivery Area <span class="fa fa-plus"></span></div>

                        <div class="col-xs-12 padding-left-40 padding-right-40 margin-top-10 margin-bottom-30 content no-mob-pad6">
                            <div class="fullWidth">
                            <div class="pull-left font-weight-700 toprow">Delivery Coverage:</div>
                            <div class="pull-left tbl-radio-btn margin-left-30 margin-left-0">
                                <input type="radio"  id="default_yes" class="delivery__radio" name="is_delivery_geo" value="1"  {{($restInfoArr[0]['is_delivery_geo']==1)? 'checked="true"' :''}}    >
                                <label for="default_yes">Geo JSON</label>
                            </div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="default_no" class="delivery__radio" name="is_delivery_geo" value="0"  {{($restInfoArr[0]['is_delivery_geo']==0)? 'checked="true"' :''}} >
                            <label for="default_no">Zipcodes</label>
                        </div>
                        <!--div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="default_d" class="delivery__radio" name="is_delivery_geo" value="2"  {{($restInfoArr[0]['is_delivery_geo']==2)? 'checked="true"' :''}} >
                            <label for="default_d">Advanced Zipcodes</label>
                        </div-->
                        </div>

                        <div class="fullWidth margin-top-30 delivery-area">
                            <div class="input__field delivery_geo textarea__input textarea_field" style="display:{{$disp}};">
                                <textarea id="deliverymessage"  name="delivery_geo"  autocomplete="off" class="fullWidth">{{($restInfoArr[0]['is_delivery_geo']==1)? $restInfoArr[0]['delivery_geo']: ''}}</textarea>
                                <span for="deliverymessage" class="{{!empty($restInfoArr[0]['is_delivery_geo'])? 'hasVal': ''}}">Delivery Polygon</span>
                            </div>
                            <div class="input__field delivery_zipcode textarea__input" style="display:{{$disp2}};">
                                <textarea id="takeoutmessage" id="delivery_zipcode"  name="delivery_zipcode" class="fullWidth" autocomplete="off">{{($restInfoArr[0]['is_delivery_geo']==1)? '': $restInfoArr[0]['delivery_zipcode']}}</textarea>
                                <span for="takeoutmessage" class="{{!empty($restInfoArr[0]['delivery_zipcode'])? 'hasVal': ''}}">Delivery Zipcode</span>
                            </div>
                            <div class="delivery_ad_zipcode uploadCSV" style="display:{{$disp3}};">

                                <div class="fullWidth">
                                    <div class="input__field uploadCSV_field">
                                        <label for="upload_csv_fileName">CSV file to import</label>
                                        <input id="upload_csv_fileName" type="text" readonly />
                                        <a href="javascript:void(0)" id="uploadCVSFile" class="font-weight-700" >Upload CSV</a>
                                    </div>
                                    <input id="csv_file" type="file" class="form-control hidden" name="csv_file" >

                                    @if ($errors->has('csv_file'))
                                        <span class="help-block"><strong>{{ $errors->first('csv_file') }}</strong></span>
                                    @endif
                                </div>
                                <div class="fullWidth margin-top-5">
                                    <div class="col-xs-6 no-padding-left">
                                        <label class="custom_checkbox relative margin-top-5 readonly">
                                            <input class="hide" type="checkbox" readonly checked name="header" value="1">File contains header row?
                                            <span class="control_indicator"></span>
                                        </label>
                                    </div>
                                    <div class="col-xs-6 text-right no-padding-right">
                                        <a href="/download_delivery_service_rates/{{$restInfoArr[0]['id']}}">Download</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
			
                    <?php if($restInfoArr[0]['parent_restaurant_id']>0){?>
		<div class="row white-box box-shadow margin-top-30">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header no-mob-pad6">Referral Promotions<span class="fa fa-plus"></span></div>

                        <div class="col-xs-12 padding-left-40 padding-right-40 margin-top-10 margin-bottom-30 content no-mob-pad6">
			    
			     <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Referral Code Status <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Enable Referral Promotions."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="selct-picker-plain123">
                                        <select id="referral_code_status" name="referral_code_status" class="selectpicker123" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                            <option value="">Select</option>
                                            <option value="1" {{(isset($restInfoArr[0]['referral_code_status']) && $restInfoArr[0]['referral_code_status']==1)?"selected":''}}  >
                                                Active
                                            </option>
                                            <option value="0" {{(isset($restInfoArr[0]['referral_code_status']) && $restInfoArr[0]['referral_code_status']==0)?"selected":''}}  >
                                                Inactive
                                            </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
			     <div class="fullWidth padding-bottom-10 flex-box flex-align-item-center">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Referral Code <span role="button" class="icon-information text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Enter refferal code."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="simple_input_field fullWidth text-right">
                                        <input type="text" class="form-control font-weight-600 color-black fullWidth text-right"  value="{{(isset($restInfoArr[0]['referral_code']))?$restInfoArr[0]['referral_code']:''}}"   name="referral_code" id="referral_code" />
                                    </div>
                                </div>
                            </div>
					
                              
			    <div class="fullWidth padding-top-bottom-10 flex-box flex-align-item-center no-border">
                                <div class="col-xs-7 col-sm-7 col-md-9 col-lg-10 no-padding-left font-weight-600">
                                    Referral Code Amount : <span role="button" class="icon-information text-color-grey text-color-grey margin-left-5" aria-hidden="true" data-toggle="popover" data-trigger="hover" data-placement="top" onclick='return false;' data-content="Enter refferal amout for the code."></span>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-2 no-padding-right">
                                    <div class="input-group position-relative">
                                        <span class="input-group-addon font-weight-600">{{$restInfoArr[0]['currency_symbol']}}</span>
                                        <input type="text" class="form-control fullWidth font-weight-600 text-right padding-left-15 color-black" name="referral_code_amount" id="referral_code_amount" value="{{(isset($restInfoArr[0]['referral_code_amount']))?$restInfoArr[0]['referral_code_amount']:''}}"  />
                                    </div>
                                </div>
                            </div>  
		          </div>


				
                    </div>
		    
		<?php }?>
                    <div class="row white-box box-shadow margin-top-30 location-availability hidden">
                        <div class="col-xs-12 text-uppercase font-weight-700 padding-left-40 padding-top-20 padding-bottom-20 accordion-header">Availability <span class="fa fa-plus"></span></div>
                        <div class="col-xs-12 padding-left-10 padding-right-10 margin-bottom-30 content">
                            <div class="col-xs-12 col-md-6 margin-top-10">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 text-center font-weight-700 padding-top-bottom-10 heading-availability margin-bottom-15">Delivery&nbsp;&nbsp; <a href="/configure/delivery-hours" class="pull-right"><i class="fa fa-pencil" aria-hidden="true"></i></a></div>
				    <?php $i=0;  $html='';$html1='';
					 if(isset($deliveryArray1)){
					 foreach($deliveryArray1 as $dayName=>$arr){
						   $css = ($i>1)?' margin-top-10':''	;
						   if($i%2==0){
						   $html .='<div class="fullWidth '.$css.'">
		                                    <div class="font-weight-700">'.$dayName.'</div>';
						     foreach($arr as $arr){	
		                                    	$html .='<div class="text-color-grey">'.$arr->open_time." - ".$arr->close_time.'</div>';
						     }
		                                   $html .='</div>';
						   }else{
							$html1 .='<div class="fullWidth '.$css.'">
		                                     <div class="font-weight-700">'.$dayName.'</div>'; 
						     foreach($arr as $arr){	
		                                    	$html1 .='<div class="text-color-grey">'.$arr->open_time." - ".$arr->close_time.'</div>';
						     }
		                                   $html1 .='</div>';
					           }							
					  $i++; 
					 } }	?>
                                    <div class="col-xs-12 col-sm-6 font-weight-600">
					
                                         <?php echo $html;?>		

                                    </div>
                                    <div class="col-xs-12 col-sm-6 font-weight-600">
                                         
                                         <?php echo $html1;?>
                                         
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 margin-top-10 ">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 hide text-center font-weight-700 padding-top-bottom-10 heading-availability margin-bottom-15">Takeout &nbsp;&nbsp; <a href="/configure/carryout-hours" class="pull-right"><i class="fa fa-pencil" aria-hidden="true"></i></a></div>
					<?php $i=0;  $html='';$html1='';
					if(isset($carryourArray1)){
					 foreach($carryourArray1 as $dayName=>$arr){
						   $css = ($i>1)?' margin-top-10':''	;
						   if($i%2==0){
						     $html .='<div class="fullWidth '.$css.'">
		                                      <div class="font-weight-700">'.$dayName.'</div>'; 
						     foreach($arr as $arr){	
		                                    	$html .='<div class="text-color-grey">'.$arr->open_time." - ".$arr->close_time.'</div>';
						     }
		                                   $html .='</div>';
						   }else{
							$html1 .='<div class="fullWidth '.$css.'">
		                                       <div class="font-weight-700">'.$dayName.'</div>'; 
						     foreach($arr as $arr){	
		                                    	$html1 .='<div class="text-color-grey">'.$arr->open_time." - ".$arr->close_time.'</div>';
						     }
		                                   $html1 .='</div>';
					           }							
					  $i++; 
					 } }	?>
				    <div class="col-xs-12 col-sm-6 font-weight-600">
					
                                        <?php echo $html;?>	

                                    </div>
                                    <div class="col-xs-12 col-sm-6 font-weight-600">
                                         
                                        <?php echo $html1;?>
                                         
                                    </div>
                                     
                                    
                                </div>
                            </div>
			    <div class="col-xs-12 col-md-6 margin-top-10">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 text-center font-weight-700 padding-top-bottom-10 heading-availability margin-bottom-15">Operations &nbsp;&nbsp; <a href="/configure/operation-hours" class="pull-right"><i class="fa fa-pencil" aria-hidden="true"></i></a></div>
					<?php $i=0;  $html='';$html1='';
					if(isset($operationArray1)){
					 foreach($operationArray1 as $dayName=>$arr){
						   $css = ($i>1)?' margin-top-10':''	;
						   if($i%2==0){
						     $html .='<div class="fullWidth '.$css.'">
		                                      <div class="font-weight-700">'.$dayName.'</div>'; 
						     foreach($arr as $arr){	
		                                    	$html .='<div class="text-color-grey">'.$arr->open_time." - ".$arr->close_time.'</div>';
						     }
		                                   $html .='</div>';
						   }else{
							$html1 .='<div class="fullWidth '.$css.'">
		                                       <div class="font-weight-700">'.$dayName.'</div>'; 
						     foreach($arr as $arr){	
		                                    	$html1 .='<div class="text-color-grey">'.$arr->open_time." - ".$arr->close_time.'</div>';
						     }
		                                   $html1 .='</div>';
					           }							
					  $i++; 
					 }}	?>
				    <div class="col-xs-12 col-sm-6 font-weight-600">
					
                                        <?php echo $html;?>	

                                    </div>
                                    <div class="col-xs-12 col-sm-6 font-weight-600">
                                         
                                        <?php echo $html1;?>
                                         
                                    </div>
                                     
                                    
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row text-center margin-top-30">
                        <input type="submit" value="Save" class="btn  btn__primary font-weight-600 width-120 box-shadow">
                        <a href="javascript:void(0)" class="btn btn__white width-120 margin-left-10 box-shadow discard">Discard</a>
                    </div>
                </form>
            <?php /*?> <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                <div class="col-xs-12 text-center text-uppercase margin-bottom-10 font-weight-700">Delivery Service Rate </div>
                <div class="col-xs-12 text-center text-color-grey font-weight-700"> </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('import_parse') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <a href="/download_delivery_service_rates/{{$restInfoArr[0]['id']}}">Download</a>
                        <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                            <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

                            <div class="col-md-6">
                                <input id="csv_file" type="file" class="form-control" name="csv_file" required>

                                @if ($errors->has('csv_file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="header" checked> File contains header row?
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Upload CSV
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div><?php */?>


                </div>
        </div>

        <script>
            $('span[data-toggle="popover"]').popover();
        </script>
            <!-- Stop Service Model -->
            <div class="modal modal-popup fade" id="stop-service" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" data-backdrop="static">
                <div class="modal-dialog vertical-center" role="document">

                    <div class="modal-content white-smoke">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12 text-center margin-top-20">
                                    <p class="margin-bottom-20 font-weight-800" >Reason for Stopping Service</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 no-padding">
                                    <div class="custom__message-textContainer no-margin col-lg-12">
                                        <textarea class="border-radius-none" placeholder="We are currently not accepting online orders. We'll be back soon." maxlength="300" rows="5" id="other_reason"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row margin-bottom-30 text-center">

                            <button type="button" class="btn btn__primary width-120 font-weight-600 margin-bottom-30 submit_reason_btn">Save</button>
                            <button type="button" class="btn btn__holo width-120 margin-left-15 font-weight-600 margin-bottom-30 closebtn" >Cancel</button>

                        </div>

                    </div>
                </div>
            </div>
            <!-- end -->

            <script>
                //$('span[data-toggle="popover"]').popover();
                var current_slide_class = '';
                var current_action = '';
                var relid = '';
                var relmsg = '';
                $(document.body).on('click','.change_setting',function() {
                    current_action = $(this).attr('rel');
                    relid = $(this).attr('relid');
                    relmsg = $(this).attr('relmsg');
                    $('#current_action').val(current_action);
                    var selected = $(this).hasClass('active');
                    if(selected == true) {
                        current_slide_class = $(this).attr('id');
                        $('#stop-service').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#restaurant_id').val(relid);
                        $('#other_reason').val(relmsg);
                        // $('.setting-save-btn').hide();
                    }else {
                        $('#restaurant_id').val(relid);
                        $('#other_reason').val(relmsg);
                        $('#stop-service').modal('hide');
                        $('#submit_pause_service_form').submit();
                        // $('.setting-save-btn').show();
                    }
                });


                $(".discard").click(function(){
                    $('#submit_location_setting_form').trigger("reset");
                })

                //submit the form
                $(document.body).on('click','.submit_reason_btn',function() {


                    var other_reason = $('#other_reason').val();
                    $('#pause_service_message' + relid).val(other_reason);
                    $('#submit_pause_service_form').submit();
                });


                if($(".row.messageblock .alert").length){
                    setTimeout(() => {
                        $(".row.messageblock .alert .close").trigger('click')
                    }, 5000);
                }
                //BS modal close

                $("#stop-service .closebtn").click(function () {
                    //alert(12)
                    $('#'+current_slide_class).addClass('active');
                    $('.'+current_slide_class).val(1);
                    // $('.setting-save-btn').show();
                    $("#stop-service").modal('hide')
                })
                $('.delivery__radio').on('click', function () {
                    //alert('--' + this.value)
                    if (this.value == 1) {
                        $(".delivery_geo").show();
                        $(".delivery_zipcode").hide();
                        $(".delivery_ad_zipcode").hide();

                        //$(".delivery_geo textarea").blur();

                    }
                    if (this.value == 2) {
                        $(".delivery_geo").hide();
                        $(".delivery_zipcode").hide();
                        $(".delivery_ad_zipcode").show();

                    }

                    if (this.value == 0) { //alert('-0-' + this.value)
                        $(".delivery_zipcode").show();
                        $(".delivery_geo").hide();
                        $(".delivery_ad_zipcode").hide();
                        //$(".delivery_zipcode textarea").blur();
                    }
                });

                if($(".delivery-area").find('textarea').val() != ""){
                    $(".delivery-area textarea").blur();
                }



                $("#uploadCVSFile").click(function () {
                    $("#csv_file").trigger('click');
                });

                $("#csv_file").change(function (e) {
                    var fileName = e.target.files[0].name
                    $("#upload_csv_fileName").val(fileName);
                    $("label[for='upload_csv_fileName']").addClass('hasVal');
                })




            </script>



        <!-- Stop Service Model -->
        <div class="modal modal-popup fade" id="copy-settings-for-locations" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" data-backdrop="static">
            <div class="modal-dialog vertical-center" role="document">

                <div class="modal-content white-smoke">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 text-center margin-top-20">
                                <p class="margin-bottom-20 font-weight-800">Copy Settings to Other Locations</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10">
                                <p class="text-color-grey font-weight-600">
                                    Select location for which these settings have to be copied.<br/>
                                    Note: All settings except delivery areas will be copied.
                                </p>
                            </div>

                            <div class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10 padding-top-bottom-20">
                                <div class="selct-picker-plain">
                                    <select multiple class="selectpicker" data-dropup-auto="false" title="Select The Locations" data-size="5" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                        <option value="1">Location 1</option>
                                        <option value="2">Location 2</option>
                                        <option value="3">Location 3</option>
                                        <option value="4">Location 4</option>
                                        <option value="5">Location 5</option>
                                        <option value="6">Location 6</option>
                                        <option value="7">Location 7</option>
                                        <option value="8">Location 8</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-offset-0 col-xs-12 col-sm-offset-1 col-sm-10">
                                <p class="text-color-grey font-weight-600">
                                    This will override the existing setting on selected locations. Do you want to continue.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row margin-bottom-30 text-center">

                        <button type="button" class="btn btn__primary width-120 font-weight-600 margin-bottom-30">Yes</button>
                        <button type="button" class="btn btn__holo width-120 margin-left-15 font-weight-600 margin-bottom-30" data-dismiss="modal">No</button>

                    </div>

                </div>
            </div>
        </div>
        <!-- end -->
        <script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.datetimepicker.full.js')}}"></script>

        <script>
            /*$('#flat_discount_start_date').datetimepicker({
                showOn: "button",
                showSecond: true,
                dateFormat: "yyyy-mm-dd",
                timeFormat: "HH:mm:ss",
                onSelect: function () {
                    $(this).trigger('change');
                    $(this).addClass('addDate');

                }
            }).attr('readonly', 'readonly');
            $('#flat_discount_end_date').datetimepicker({
                showOn: "button",
                showSecond: true,
                dateFormat: "yyyy-mm-dd",
                timeFormat: "HH:mm:ss",
                onSelect: function () {
                    $(this).trigger('change');
                    $(this).addClass('addDate');

                }
            }).attr('readonly', 'readonly');*/

            /*$("#flat_discount_end_date").datepicker({
                showAnim: "slideDown",
                dateFormat: 'yy-mm-dd HH:mm:ss',
                maxDate: 0,
                onSelect: function () {
                    $(this).trigger('change');
                    $(this).addClass('addDate');

                }
            })
            $("#flat_discount_start_date").datepicker({
                showAnim: "slideDown",
                dateFormat: 'yy-mm-dd HH:mm:ss',
                maxDate: 0,
                onSelect: function () {
                    $(this).trigger('change');
                    $(this).addClass('addDate');

                }
            })*/

            $('#flat_discount_end_date, #flat_discount_start_date').datetimepicker({
                startDate:	new Date(),
                format: 'Y-m-d h:i',
                formatTime: 'h:i a'
            });

            setTimeout(function () {
                $(".input__field input, .selct-picker-plain select").each(function (key, value) {

                    //console.log($(this).val());

                    if ($(this).val()) {
                        $(this).next().addClass('hasVal')
                    } else {
                        $(this).next().removeClass('hasVal')
                    }
                });
            }, 500);


            window.localStorage.removeItem("prevSelectOpt");

            $(document).on("shown.bs.select", ".selectpicker", (
                function () {
                    window.localStorage.setItem('prevSelectOpt', $(this).val());
                })
            );

            $(document).on("change", ".selectpicker", (
                function () {
                    if ($(this).val() != "") {
                        $(this).parents(".bootstrap-select").next().addClass("hasVal")
                    }
                })
            )

            $(document).on("hidden.bs.select", ".selectpicker", (
                function () {
                    if (window.localStorage.getItem('prevSelectOpt') == $(this).val()) {
                        //$(this).selectpicker('val','0')
                        //$(this).parents("").find(".filter-option").text("")
                        //$(this).parents(".bootstrap-select").next().removeClass("hasVal")
                    }
                })
            );

        </script>
@endsection
