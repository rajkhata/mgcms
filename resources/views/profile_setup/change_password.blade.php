@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />

    <div class="main__container deal-discounts-page container__custom_working new-design-labels prl20">

    <div class=""><!-- main__container social-media-links container__custom_working new-design-labels -->

        <form method="POST" action="{{ route('update-password') }}" enctype="multipart/form-data" id="restaurantform">@csrf

            <div class="row"><!-- margin-top-30 -->
                <div class="reservation__atto flex-direction-row">
                    <div class="reservation__title margin-top-5">
                        <h1>Change Password</h1>
                    </div>
                    <div class="guestbook__sec2">
                        

                    </div>
                </div>
                @if(session()->has('message'))
                    <div class="alert alert-success ">
                        {{ session()->get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger margin-top-20">
                        {{ session()->get('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif


                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-0 social-media-apis">
                    <div class="col-xs-12 padding-left-25 padding-right-25 no-mob-pad5">
                        <div class="col-xs-12">
                            <div class="input__field api-key">
                                <input id="oldpassword" type="text"
                                       class="{{ $errors->has('oldpassword') ? ' is-invalid' : '' }}"
                                       name="oldpassword"
                                       value="">
                                       @if ($errors->has('oldpassword'))
                                       @else
                                       <span for="oldpassword">Old Password</span>
                                       @endif

                            </div>
                            @if ($errors->has('oldpassword'))
                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('oldpassword') }}</strong></span>
                            @endif
                        </div>

                        <div class="col-xs-12 margin-top-20">
                            <div class="input__field api-key">
                                <input id="newpassword" type="text"
                                       class="{{ $errors->has('') ? ' is-invalid' : '' }}"
                                       name="newpassword"
                                       value="">
                                       @if ($errors->has('newpassword'))
                                       @else
                                        <span for="gtm_code">New Password</span>
                                        @endif
                                @if ($errors->has('newpassword'))
                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('newpassword') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 margin-top-20">
                            <div class="input__field api-key">
                                <input id="confirmpassword" type="text"
                                       class="{{ $errors->has('') ? ' is-invalid' : '' }}"
                                       name="confirmpassword"
                                       value="">
                                       @if ($errors->has('confirmpassword'))
                                       @else
                                        <span for="gtm_code">Confirm New Password</span>
                                       @endif
                                
                                @if ($errors->has('confirmpassword'))
                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('confirmpassword') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 margin-top-20">
                            <div class="">
                                <input type="submit" class="btn btn__primary font-weight-600  width-120 box-shadow" value="Update" >
                                <input id="id" type="hidden" value="{{ $id}}" name="id" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
</div>
@endsection