@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />

    <div class="main__container deal-discounts-page container__custom_working new-design-labels prl20">

        <div id="cms-accordion" class="row">
            <div class="reservation__atto flex-direction-row">
                <div class="reservation__title margin-top-5">
                    <h1>Promotions</h1>
                </div>

            </div>
            @if(session()->has('message'))
                <div class="alert alert-success margin-top-20">
                    {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="row white-box box-shadow margin-top-0 row-design" style="border-bottom:none">
                <div class="col-xs-12 text-uppercase deal1">
                    <!-- Flat Discount Settings <span class="fa fa-plus"></span> --><!--  accordion-header -->
                    <div class="row" style="border-bottom:none">
                        <div class="col-xs-5 col-sm-6 col-md-6 col-lg-6">
                            Flat Discount Settings
                        </div>
                        <div class="col-xs-7 col-sm-6 col-md-6 col-lg-6 text-align-right">
                            @can('Add Promotions')
                            <button type="button" class="btn_ btn__holo font-weight-600 box-shadow" data-toggle="modal" data-target="#addpromotion">Add New Promotion</button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 padding-left-20 padding-right-20 margin-top-10 margin-bottom-30 content prl">
                    <div class="" id="zeroforphone" style="border-bottom:none">
                        <table id="table_id1" class="responsive ui cell-border hover" style="margin-bottom:10px; width:100%;">
                            <thead>
                                <tr>
                                    <th>User Group</th>
                                    <th>Delivery Type</th>
                                    <th>Promotion Name</th>
                                    <th>Type</th>
                                    <th>Condition</th>
                                    <th>Conditional Amount</th>
                                    <th>Discount</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th class = "filterhead">Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($promos))
                                @foreach($promos as $promo)
                                    <tr>
                                        <td>
                                            <?php foreach ($user_groups as $group){
                                            $user_group_id=explode(',',$promo->user_group_id);
                                            if(!in_array($group->id, $user_group_id)){
                                                continue;
                                            }
                                            ?>
                                            <option value="<?php echo $group->id;?>" ><?php echo $group->user_group;?> </option>
                                            <?php } ?>
                                        </td>
                                        <td>{{ isset($promo->delivery_type)?ucfirst($promo->delivery_type):'' }}</td>
                                        <td>{{ isset($promo->name)?$promo->name:'' }}</td>
                                        <td>{{ ($promo->amount_or_percent==0) ? 'Flat' :'Percentage' }}</td>
                                        <td>{{$promo->condition}}</td>
                                        <td>{{$promo->condition_amount}}</td>
                                        <td>{{ isset($promo->discount)?$promo->discount:'' }}</td>
                                        <td data-order="{{ strtotime($promo->start_date)}}">{{date("d/m/y H:i", strtotime($promo->start_date))}}</td>
                                        <td data-order="{{ strtotime($promo->end_date)}}">{{date("d/m/y H:i", strtotime($promo->end_date))}}</td>
                                        <td>{{ ($promo->status==1) ? 'Active' :'In Active' }}</td>
                                        <td>
                                            <!-- <button type="button" class="btn btn__primary width-90 editpromo" data-source='<?php echo json_encode($promo);?>'>Edit</button> -->
                                            <!-- <button  type="button" class="btn btn__holo width-90 font-weight-600" onclick="deletePromotion({{$promo->id}})">Delete</button> -->
                                            <a href="javascript:void(0);"  data-source='<?php echo json_encode($promo);?>' class="glyphicon glyphicon-edit editpromo"></a>&nbsp;
                                            <a href="javascript:void(0);" onclick="deletePromotion({{$promo->id}})"
                                               class="glyphicon glyphicon-trash"></a>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr><td colspan="11">No Promotion</td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- @can('Add Promotions')
                    <div class="fullWidth margin-top-20 padding-top-bottom-10 margin-bottom-10 text-center no-border">
                        <button type="button" class="btn_ btn__holo font-weight-600 box-shadow margin-left-15" data-toggle="modal" data-target="#addpromotion">Add New Promotion</button>
                    </div>
                    @endcan -->

                </div>
            </div>

                <div class="row white-box box-shadow margin-top-30 row-design" style="border-bottom:none">
                    <div class="col-xs-12 text-uppercase deal1">
                        <div class="row" style="border-bottom:none">
                            <div class="col-xs-5 col-sm-6 col-md-6 col-lg-6">
                                Coupons
                            </div>
                            <div class="col-xs-7 col-sm-6 col-md-6 col-lg-6 text-align-right">
                                @can('Add Coupon')
                                <button type="button" class="btn_ btn__holo font-weight-600 box-shadow" data-toggle="modal" data-target="#addcouponcode">Add New Coupon</button>
                                <input id="restaurant_id" type="hidden" value="{{ $restInfoArr[0]['id']}}" name="restaurant_id">
                                @endcan
                            </div>
                        </div>
                        <!--<span class="fa fa-plus"></span>-->
                    </div><!-- accordion-header -->

                    <div class="col-xs-12 padding-left-20 padding-right-20 margin-top-10 margin-bottom-30 content prl">
                        <div class="" style="border-bottom:none">
                            <table class="responsive ui cell-border hover" style="margin-bottom:10px; width:100%;" id="table_id">
                                <thead>
                                <tr>
                                    <th>User Group</th>
                                    <th>Coupon</th>
                                    <th>Type</th>
                                    <th>Usage Per Coupon</th>
                                    <th>Usage Per Customer</th>
                                    <th>Min Amount</th>
                                    <th>Discount</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th class = "filterhead">Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(is_array($couponArr) && count($couponArr))
                                    @foreach($couponArr as $coupon)
                                <tr>
                                    <td>
                                        <?php foreach ($user_groups as $group){
                                           $user_group_id=explode(',',$coupon['user_group_id']);
                                            if(!in_array($group->id, $user_group_id)){
                                             continue;
                                            }
                                            ?>
                                        <option value="<?php echo $group->id;?>" ><?php echo $group->user_group;?> </option>
                                        <?php } ?>
                                    </td>
                                    <td>{{ isset($coupon['coupon_code'])?$coupon['coupon_code']:'' }}</td>
                                    <td>{{ ($coupon['discount_type']==1) ? 'Flat' :'Percentage' }}</td>
                                    <td>{{$coupon['uses_per_coupon']}}</td>
                                    <td>{{$coupon['uses_per_customer']}}</td>
                                    <td>{{ isset($coupon['minimum_amount'])?$coupon['minimum_amount']:'' }}</td>
                                    <td>{{ isset($coupon['discount'])?$coupon['discount']:'' }}</td>
                                    <td data-search="{{date('d/m/y H:i', strtotime($coupon['start_date']))}}" data-order="{{ strtotime($coupon['start_date'])}}">{{date("d/m/y H:i", strtotime($coupon['start_date']))}}</td>
                                    <td data-search="{{date('d/m/y H:i', strtotime($coupon['end_date']))}}" data-order="{{ strtotime($coupon['end_date'])}}">{{date("d/m/y H:i", strtotime($coupon['end_date']))}}</td>
                                    <td data-order="{{ $coupon['status']}}">{{ ($coupon['status']==1) ? 'Active' :'In Active' }}</td>
                                    <td>
                                        <!-- <button type="button" data-toggle="modal" data-target="#editcouponcode" class="editcoupon glyphicon glyphicon-edit" data-source='<?php echo json_encode($coupon);?>'>Edit</button> -->
                                        <!-- <button type="button" class="btn btn__holo width-90 font-weight-600" onclick="deleteCoupon({{$coupon['id']}})" >Delete</button> -->

                                        <a href="javascript:void(0);" onclick ="editCoupons({{$coupon['id']}})" data-source='<?php echo json_encode($coupon);?>' class="glyphicon glyphicon-edit editcoupon coupons_{{$coupon['id']}}"></a>&nbsp;
                                        <a href="javascript:void(0);" onclick="deleteCoupon({{$coupon['id']}})" class="glyphicon glyphicon-trash"></a>
                                    </td>
                                </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="11">No Coupon</td></tr>
                                @endif

                                </tbody>
                            </table>
                        </div>
                        <!-- <div id="keyword_container" class="fullWidth">

                        </div> -->
                        
                        @can('Add Coupon')
                        <div class="fullWidth text-center no-border">
{{--
                            <input type="submit" class="btn_ btn__primary font-weight-600 box-shadow" value="Save">
--}}
                            <!-- <button type="button" class="btn_ btn__holo font-weight-600 box-shadow margin-left-15" data-toggle="modal" data-target="#addcouponcode">Add New Coupon</button>
                            <input id="restaurant_id" type="hidden" value="{{ $restInfoArr[0]['id']}}" name="restaurant_id"> -->
                        </div>
                        @endcan



                </div>

        </div>




    </div>

    <script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>
    <script>

        setTimeout(function () {
            checkHasVal("#restaurantform .input__field input, .selct-picker-plain select");
            $('.dateTime_start_date').datetimepicker(commonOptions);
            $('.dateTime_end_date').datetimepicker(commonOptions);
        }, 400);


        window.localStorage.removeItem("prevSelectOpt");

        $(document).on("shown.bs.select", ".selectpicker", (
            function () {
                window.localStorage.setItem('prevSelectOpt', $(this).val());
            })
        );

        $(document).on("change", ".selectpicker", (
            function () {
                if ($(this).val() != "") {
                    $(this).parents(".bootstrap-select").next().addClass("hasVal")
                }
            })
        )

        $(document).on("hidden.bs.select", ".selectpicker", (
            function () {
                if (window.localStorage.getItem('prevSelectOpt') == $(this).val()) {
                    //$(this).selectpicker('val','0')
                    //$(this).parents("").find(".filter-option").text("")
                    //$(this).parents(".bootstrap-select").next().removeClass("hasVal")
                }
            })
        );

        var startDate;
        var today = new Date();
        var date = ("0" + today.getDate()).slice(-2)+'-'+("0" + (today.getMonth()+1)).slice(-2)+'-'+today.getFullYear();
        var time = today.getHours() + ":" + today.getMinutes() // + ":" + today.getSeconds();
        var currentDateTime = date+' '+time;

        var commonOptions = {
            i18n:{
                en: {
                    dayOfWeekShort: [
                        "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"
                    ]
                }
            },
            format: 'Y-m-d H:i:s',
            formatTime: 'H:i:s'
        }


           


        /*$('.dateTime_start_date').datetimepicker(commonOptions,{
            onChangeDateTime: function(dp,$input){
                startDate = $input[0].value;
            },
            onShow:function( ct ){
                console.log(ct)

                this.setOptions({
                    maxDate:jQuery('.dateTime_end_date').val()?jQuery('.dateTime_end_date').val():false
                })
            }
        });*/

        $(function(){
            jQuery('#date_timepicker_start').datetimepicker({
                onShow:function( ct ){
                    this.setOptions({
                        maxDate:(jQuery('#date_timepicker_end').val()).split(" ",2)[0]?(jQuery('#date_timepicker_end').val()).split(" ",2)[0]:false
                    })
                }
            });
            jQuery('#date_timepicker_end').datetimepicker({
                //startDate: $('#date_timepicker_end').val()?(jQuery('#date_timepicker_start').val()).split(" ",2)[0]:false,
                onShow:function( ct ){
                    this.setOptions({
                        minDate:(jQuery('#date_timepicker_start').val()).split(" ",2)[0]?(jQuery('#date_timepicker_start').val()).split(" ",2)[0]:false
                    })
                }
            });
        });



        /*$('.dateTime_end_date').datetimepicker(commonOptions, {
            startDate:  $('.dateTime_start_date').val(),//new Date(),
            onShow: function(ct){
                console.log(ct)

                this.setOptions({
                    minDate:jQuery('.dateTime_start_date').val()?jQuery('.dateTime_start_date').val():false
                })
            }/!*,
            onClose: function(current_time, $input) {
                var endDate = $input[0].value;

                console.log("startDate"+ startDate)

                if (startDate > endDate) {
                    console.log('Please select correct date');
                }
            }*!/
        });

        $(".social-media-apis .input__field textarea").each(function(key, value) {
            if($(this).val()){
                $(this).next().addClass('hasVal')
            } else {
                $(this).next().removeClass('hasVal')
            }
        });*/
        setTimeout(function(){
            $(".social-media-apis .input__field input").each(function(key, value) {
                if($(this).val()){
                    $(this).next().addClass('hasVal')
                } else {
                    $(this).next().removeClass('hasVal')
                }
            });
        },200)


        $('#keyword_container').on('click', 'a.meta_tags_minus', function() {
            $(this).parents('.coupon-row').remove();
        });


    </script>
     <div id="addcouponcode" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Coupon</h4>
                </div>
                <div class="modal-body">
                    <form id="couponform" name="couponform">
                        <div class="row clearfix">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-10">
                                <label for="usergroup">User Group   :</label>
                                <select id="user_group_id" required multiple name="user_group_id[]" data-size="8" class="selectpicker ignore"
                                        data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                    <option value=""></option>
                                    <?php foreach ($user_groups as $group){ ?>
                                    <option value="<?php echo $group->id;?>" ><?php echo $group->user_group;?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-10">
                                <label for="usergroup">Coupon   :</label>
                                <div class="input__field"><input type="text"  required class=" color-black fullWidth" name="coupon_code"  placeholder="Coupon"/></div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                <label for="usergroup">Discount Type    :</label>
                                <select id="discount_type" required name="discount_type" data-size="8" class="selectpicker ignore"
                                        data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                    <option value=""></option>
                                    <option value="1" >Flat</option>
                                    <option value="2" >Percentage</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">Usage per Coupon :</label>
                                <div class="input__field"><input type="text" required class="" id="usergroup" placeholder="Usage per Coupon" name="uses_per_coupon"></div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">Usage per Customer   :</label>
                                <div class="input__field"><input type="text" required class="" id="usergroup" placeholder="Usage per Customer" name="uses_per_customer"></div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">Minimum Amount   :</label>
                                <div class="input__field"><input type="text" required class=" color-black fullWidth" name="minimum_amount" id="minimum_amount"  placeholder="Minimum Amount"/></div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">Discount :</label>
                                <div class="input__field"><input type="text" required class=" color-black fullWidth" name="discount" id="discount" placeholder="Discount" /></div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">Start Date       :</label>
                                <div class="input__field"><input type="text" required class=" color-black fullWidth dateTime_start_date icocal" name="start_date" id="start_date" placeholder="Start Date" /></div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">End Date     :</label>

                                <div class="input__field"><input type="text" required class=" color-black fullWidth dateTime_end_date icocal" name="end_date" id="end_date" placeholder="End Date"/></div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                <label for="usergroup">Status       :</label>
                                <select id="status" required name="status" data-size="5" class="selectpicker ignore"
                                        data-style="no-background-with-buttonline no-padding-left margin-top-5 padding-bottom-3 no-padding-top">
                                    <option value=""></option>
                                    <option value="1">Active</option>
                                    <option value="0">In Active</option>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn_ btn__holo font-weight-600 box-shadow margin-left-15 margin-top-30 margin-bottom-10">Submit</button>


                    </form>
                </div>

            </div>

        </div>
     </div>

     <div id="editcouponcode" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Coupon</h4>
                    </div>
                    <div class="modal-body">
                        <form id="editcouponform" name="editcouponform">
                            <input type="hidden" name="coupon_id" id="coupon_id">

                            <div class="row clearfix">
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-10">
                                    <label for="usergroup">User Group   :</label>
                                    <select id="user_group_id" required multiple name="user_group_id[]" data-size="8" class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                        <option value=""></option>
                                        <?php foreach ($user_groups as $group){ ?>
                                        <option value="<?php echo $group->id;?>"><?php echo $group->user_group;?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-10">
                                    <label for="usergroup">Coupon   :</label>
                                    <div class="input__field"><input type="text" id="coupon_code" required class=" color-black fullWidth" name="coupon_code"  placeholder="Coupon"/></div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                    <label for="usergroup">Discount Type    :</label>
                                    <select id="discount_type" required name="discount_type" data-size="8" class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                        <option value=""></option>
                                        <option value="1" >Flat</option>
                                        <option value="2" >Percentage</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="usergroup">Usage per Coupon :</label>
                                    <div class="input__field"><input type="text" required class="" id="uses_per_coupon" placeholder="Usage per Coupon" name="uses_per_coupon"></div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="usergroup">Usage per Customer   :</label>
                                    <div class="input__field"><input type="text" required class="" id="uses_per_customer" placeholder="Usage per Customer" name="uses_per_customer"></div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="usergroup">Minimum Amount   :</label>
                                    <div class="input__field"><input type="text" required class=" color-black fullWidth" name="minimum_amount" id="minimum_amount"  placeholder="Minimum Amount"/></div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="usergroup">Discount :</label>
                                    <div class="input__field"><input type="text" required class=" color-black fullWidth" name="discount" id="discount" placeholder="Discount" /></div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="usergroup">Start Date       :</label>
                                    <div class="input__field"><input type="text" required class=" color-black fullWidth dateTime_start_date icocal" name="start_date" id="start_date" placeholder="Start Date" /></div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="usergroup">End Date     :</label>
                                    <div class="input__field"><input type="text" required class=" color-black fullWidth dateTime_end_date icocal" name="end_date" id="end_date" placeholder="End Date"/></div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                    <label for="usergroup">Status       :</label>
                                    <select id="status" required name="status" data-size="5" class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left margin-top-5 padding-bottom-3 no-padding-top">
                                        <option value=""></option>
                                        <option value="1">Active</option>
                                        <option value="0">In Active</option>
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn_ btn__holo font-weight-600 box-shadow margin-left-15 margin-top-30  margin-bottom-10">Submit</button>


                        </form>
                    </div>

                </div>

            </div>
        </div>


     <div id="addpromotion" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Promotion</h4>
                </div>
                <div class="modal-body">
                    <form  id="promotionform" name="promotionform">
                        <div class="row clearfix">
                            <div class="selct-picker-plain col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-10">
                                <label for="user_group_id">User Group   :</label>
                                <select id="user_group_id" multiple name="user_group_id[]" data-size="8" class="selectpicker ignore"
                                        data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                    <option value=""></option>
                                    <?php foreach ($user_groups as $group){ ?>
                                    <option value="<?php echo $group->id;?>" ><?php echo $group->user_group;?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-10">
                                <label for="deliverytype">Delivery Type :</label>
                                <select id="deliverytype"  required name="delivery_type" data-size="8" class="selectpicker ignore"
                                        data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                    <option value=""></option>
                                    <option value="delivery">Delivery</option>
                                    <!--<option value="carryout">Carryout</option>-->

                                </select>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                <label for="amount_or_percent">Type :</label>
                                <select id="amount_or_percent" required name="amount_or_percent" data-size="8" class="selectpicker ignore"
                                        data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                    <option value=""></option>
                                    <option value="0" >Flat</option>
                                    <option value="1" >Percentage</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                <label for="condition">Condition    :</label>
                                <select id="condition" name="condition" required data-size="8" class="selectpicker ignore"
                                        data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                    <option value=""></option>
                                    <option value="=" >Equals to</option>
                                    <option value=">" >Greater than</option>
                                    <option value="<" >Less than</option>
                                    <option value="<=" >Less than equals to</option>
                                    <option value=">=" >Greater than equals to</option>
                                </select>

                               {{-- <label for="usergroup">Usage per Coupon :</label>
                                <input type="text" class="form-control" id="usergroup" placeholder="Usage per Coupon" name="uses_per_coupon">--}}
                            </div>
                        </div>
                        {{--<div class="row clearfix">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                 <label for="usergroup">Usage per Coupon    :</label>
                                 <div class="input__field"><input type="text" class="form-control" id="usergroup" placeholder="Usage per Coupon" name="uses_per_coupon"></div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">Usage per Customer   :</label>
                                <div class="input__field"><input type="text" class="form-control" id="usergroup" placeholder="Usage per Customer" name="uses_per_customer"></div>
                            </div>
                        </div>--}}
                        <div class="row clearfix">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="promotion_name">Promotion Name      :</label>
                                <div class="input__field"><input type="text" class="" required id="promotion_name" placeholder="Promotion Name" name="promotion_name"></div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="condition_amount">Minimum Amount    :</label>
                                <div class="input__field"><input type="text" class=" color-black fullWidth" required name="condition_amount" id="condition_amount"  placeholder="Minimum Amount"/></div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="discount">Discount  :</label>
                                <div class="input__field"><input type="text" class="color-black fullWidth" required name="discount" id="discount" placeholder="Discount" /></div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">Start Date       :</label>
                                <div class="input__field"><input type="text" class=" color-black fullWidth dateTime_start_date icocal"  required name="start_date" id="start_date" placeholder="Start Date" /></div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                <label for="usergroup">End Date     :</label>
                                <div class="input__field"><input type="text" class=" color-black fullWidth dateTime_end_date icocal" required name="end_date" id="end_date" placeholder="End Date"/></div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                <label for="usergroup">Status       :</label>
                                <select id="status" name="status" required data-size="5" class="selectpicker ignore"
                                        data-style="no-background-with-buttonline no-padding-left margin-top-5 padding-bottom-3 no-padding-top">
                                    <option value=""></option>
                                    <option value="1">Active</option>
                                    <option value="0">In Active</option>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn_ btn__holo font-weight-600 box-shadow margin-left-15 margin-top-30 margin-bottom-10">Submit</button>


                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="editpromotion" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Promotion</h4>
                    </div>
                    <div class="modal-body">
                        <form  id="editpromotionform" name="editpromotionform">
                            <input type="hidden" name="promotion_id" id="promotion_id">

                            <div class="row clearfix">
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-10">
                                    <label for="user_group_id">User Group   :</label>
                                    <select id="user_group_id" multiple name="user_group_id[]" data-size="8" class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                        <option value=""></option>
                                        <?php foreach ($user_groups as $group){ ?>
                                        <option value="<?php echo $group->id;?>" ><?php echo $group->user_group;?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-10">
                                    <label for="deliverytype">Delivery Type :</label>
                                    <select id="delivery_type"  required name="delivery_type" data-size="8" class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                        <option value=""></option>
                                        <option value="delivery">Delivery</option>
                                        <!--<option value="carryout">Carryout</option>-->

                                    </select>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                    <label for="amount_or_percent">Type :</label>
                                    <select id="amount_or_percent" required name="amount_or_percent" data-size="8" class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                        <option value=""></option>
                                        <option value="0" >Flat</option>
                                        <option value="1" >Percentage</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                    <label for="condition">Condition    :</label>
                                    <select id="condition" name="condition" required data-size="8" class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left padding-bottom-3 margin-top-5 no-padding-top">
                                        <option value=""></option>
                                        <option value="=" >Equals to</option>
                                        <option value=">" >Greater than</option>
                                        <option value="<" >Less than</option>
                                        <option value="<=" >Less than equals to</option>
                                        <option value=">=" >Greater than equals to</option>
                                    </select>
                                    {{-- <label for="usergroup">Usage per Coupon    :</label>
                                     <input type="text" class="form-control" id="usergroup" placeholder="Usage per Coupon" name="uses_per_coupon">--}}
                                </div>
                            </div>

                            {{-- <div class="row clearfix">
                                 <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                      <label for="usergroup">Usage per Coupon   :</label>
                                      <div class="input__field"><input type="text" class="form-control" id="usergroup" placeholder="Usage per Coupon" name="uses_per_coupon"></div>
                                 </div>
                                 <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                     <label for="usergroup">Usage per Customer  :</label>
                                     <div class="input__field"><input type="text" class="form-control" id="usergroup" placeholder="Usage per Customer" name="uses_per_customer"></div>
                                 </div>

                             </div>--}}

                            <div class="row clearfix">
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="promotion_name">Promotion Name      :</label>
                                    <div class="input__field"><input type="text" class="" required id="promotion_name" placeholder="Promotion Name" name="promotion_name"></div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="condition_amount">Minimum Amount    :</label>
                                    <div class="input__field"><input type="text" class="color-black fullWidth" required name="condition_amount" id="condition_amount"  placeholder="Minimum Amount"/></div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="discount">Discount  :</label>
                                    <div class="input__field"><input type="text" class="color-black fullWidth" required name="discount" id="discount" placeholder="Discount" /></div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="usergroup">Start Date       :</label>
                                    <div class="input__field"><input type="text" class=" color-black fullWidth dateTime_start_date icocal"  required name="start_date" id="start_date" placeholder="Start Date" /></div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
                                    <label for="usergroup">End Date     :</label>
                                    <div class="input__field"><input type="text" class="color-black fullWidth dateTime_end_date icocal" required name="end_date" id="end_date" placeholder="End Date"/></div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 selct-picker-plain margin-top-15">
                                    <label for="usergroup">Status       :</label>
                                    <select id="status" name="status" required data-size="5" class="selectpicker ignore"
                                            data-style="no-background-with-buttonline no-padding-left margin-top-5 padding-bottom-3 no-padding-top">
                                        <option value=""></option>
                                        <option value="1">Active</option>
                                        <option value="0">In Active</option>
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn_ btn__holo font-weight-600 box-shadow margin-left-15 margin-top-30 margin-bottom-10">Submit</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

        <script>

        // $(document).ready(function () {     
        //     $('.editcoupon').click(function (e) {
                
        //         $('#editcouponcode').modal('show');
        //         var data=$(this).attr('data-source');
        //         data=JSON.parse(data);
        //         console.log(data);
        //         $.each(data.user_group_id.split(","), function(i,e){
        //             $("#editcouponform #user_group_id option[value='" + e + "']").prop("selected", true);
        //         });

        //         $("#editcouponform #discount_type option[value='" + data.discount_type  + "']").prop("selected", true);
        //         $("#editcouponform #coupon_code").val(data.coupon_code);
        //         $("#editcouponform #uses_per_customer").val(data.uses_per_customer);
        //         $("#editcouponform #uses_per_coupon").val(data.uses_per_coupon);
        //         $("#editcouponform #minimum_amount").val(data.minimum_amount);
        //         $("#editcouponform #status option[value='" + data.status  + "']").prop("selected", true);
        //         $("#editcouponform #discount").val(data.discount);
        //         $("#editcouponform #start_date").val(data.start_date);
        //         $("#editcouponform #end_date").val(data.end_date);
        //         $("#editcouponform #coupon_id").val(data.id);
        //         $('.selectpicker').selectpicker('refresh');
        //     });
            
        // });


            $('#editcouponform').submit(function (e) {
                // code
                e.preventDefault();
                var formData = $(this).serialize();
                var coupon_id=$("#editcouponform #coupon_id").val();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/updateCoupon"; ?>/'+coupon_id,
                    data: formData,
                    success: function (data) {
                        //alert('Promotion added');
                        // window.location.reload();

                        alertbox('Success',data.message,function(modal){
                            $('#loader').addClass('hidden');
                            setTimeout(function() {
                                modal.modal('hide');
                                window.location.reload();
                            }, 3000);
                        });
                    },
                    error:function () {
                        // alert('Something went wrong!');
                        alertbox('Error',data.message,function(modal){
                            $('#loader').addClass('hidden');
                            setTimeout(function() {
                                modal.modal('hide');
                                window.location.reload();
                            }, 3000);
                        });
                    }
                });
            });

            $('.editpromo').click(function (e) {
                $('#editpromotion').modal('show');
                var data=$(this).attr('data-source');
                data=JSON.parse(data);
                //console.log(data);

                $.each(data.user_group_id.split(","), function(i,e){
                    $("#editpromotionform #user_group_id option[value='" + e + "']").prop("selected", true);
                });
                $("#editpromotionform #delivery_type option[value='" + data.delivery_type + "']").prop("selected", true);
                $("#editpromotionform #amount_or_percent option[value='" + data.amount_or_percent  + "']").prop("selected", true);
                $("#editpromotionform #condition option[value='" + data.condition  + "']").prop("selected", true);
                $("#editpromotionform #status option[value='" + data.status  + "']").prop("selected", true);
                $("#editpromotionform #promotion_name").val(data.name);
                $("#editpromotionform #condition_amount").val(data.condition_amount);
                $("#editpromotionform #discount").val(data.discount);
                $("#editpromotionform #start_date").val(data.start_date);
                $("#editpromotionform #end_date").val(data.end_date);
                $("#editpromotionform #promotion_id").val(data.id);
                $('.selectpicker').selectpicker('refresh');
            });



             $('#editpromotionform').submit(function (e) {

                // code
                e.preventDefault();
                var formData = $(this).serialize();
                var promo_id=$("#editpromotionform #promotion_id").val();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/updatePromotion"; ?>/'+promo_id,
                    data: formData,
                    success: function (data) {
                        //alert('Promotion added');
                        // window.location.reload();

                        alertbox('Success',data.message,function(modal){
                            $('#loader').addClass('hidden');
                            setTimeout(function() {
                                modal.modal('hide');
                                window.location.reload();
                            }, 3000);
                        });
                    },
                    error:function () {
                        // alert('Something went wrong!');
                        alertbox('Error',data.message,function(modal){
                            $('#loader').addClass('hidden');
                            setTimeout(function() {
                                modal.modal('hide');
                                window.location.reload();
                            }, 3000);
                        });
                    }
                });

            });
            //$(document).on('submit','.promotionform',function(e){
             $('#promotionform').submit(function (e) {

                // code
                e.preventDefault();
                var formData = $(this).serialize();

                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/addPromotion"; ?>',
                    data: formData,
                    success: function (data) {
                        //alert('Promotion added');
                       // window.location.reload();

                        alertbox('Success',data.message,function(modal){
                            $('#loader').addClass('hidden');
                            setTimeout(function() {
                                modal.modal('hide');
                                window.location.reload();
                            }, 2000);
                        });
                    },
                    error:function () {
                       // alert('Something went wrong!');
                        alertbox('Error',data.message,function(modal){
                            $('#loader').addClass('hidden');
                            setTimeout(function() {
                                modal.modal('hide');
                                window.location.reload();
                            }, 2000);
                        });
                    }
                });

        });

             $('#couponform').submit(function (e) {

          // code
          e.preventDefault();
          var formData = $(this).serialize();

          $.ajax({
              type: 'POST',
              url: '<?php echo URL::to('/') . "/addCoupon"; ?>',
              data: formData,
              success: function (data) {
                  //alert('Promotion added');
                  // window.location.reload();

                  alertbox('Success',data.message,function(modal){
                      $('#loader').addClass('hidden');
                      setTimeout(function() {
                          modal.modal('hide');
                          window.location.reload();
                      }, 2000);
                  });
              },
              error:function () {
                  // alert('Something went wrong!');
                  alertbox('Error',data.message,function(modal){
                      $('#loader').addClass('hidden');
                      setTimeout(function() {
                          modal.modal('hide');
                          window.location.reload();
                      }, 2000);
                  });
              }
          });

      });
      
            function editCoupons(id){
                $('#editcouponcode').modal('show');
                var data=$('.coupons_'+id).attr('data-source');
                data=JSON.parse(data);
                console.log(data);
                $.each(data.user_group_id.split(","), function(i,e){
                    $("#editcouponform #user_group_id option[value='" + e + "']").prop("selected", true);
                });
                $("#editcouponform #discount_type option[value='" + data.discount_type  + "']").prop("selected", true);
                $("#editcouponform #coupon_code").val(data.coupon_code);
                $("#editcouponform #uses_per_customer").val(data.uses_per_customer);
                $("#editcouponform #uses_per_coupon").val(data.uses_per_coupon);
                $("#editcouponform #minimum_amount").val(data.minimum_amount);
                $("#editcouponform #status option[value='" + data.status  + "']").prop("selected", true);
                $("#editcouponform #discount").val(data.discount);
                $("#editcouponform #start_date").val(data.start_date);
                $("#editcouponform #end_date").val(data.end_date);
                $("#editcouponform #coupon_id").val(data.id);
                $('.selectpicker').selectpicker('refresh');
            }


              function deleteCoupon(id) {

                  ConfirmBox('Confirm Delete','Do you really want to delete this coupon?',function(conf_status,curmodal){
                      curmodal.modal('hide');
                      if(conf_status){
                          $.ajax({
                              type: 'DELETE',
                              url: '/delete/coupon/' + id,
                              dataType: 'json',
                              success: function (data) {
                                  alertbox('Success',data.message, function(modal){
                                      setTimeout(function(){
                                          window.location.reload();
                                      },1000)
                                  });
                              },
                              error: function (data, textStatus, errorThrown) {
                                  var err='';
                                  $.each(data.responseJSON.errors, function(key, value){
                                      err+='<p>'+value+'</p>';
                                  });
                                  alertbox('Error',err,function(modal){
                                  });
                              },
                              complete: function(data) {

                              }
                          });
                      }
                  });

              }

              function deletePromotion(id) {

                  ConfirmBox('Confirm Delete','Do you really want to delete this Promotion?',function(conf_status,curmodal){
                      curmodal.modal('hide');
                      if(conf_status){

                          $.ajax({
                              type: 'DELETE',
                              url: '/delete/promotion/' + id,
                              dataType: 'json',
                              success: function (data) {
                                  alertbox('Success',data.message, function(modal){
                                      setTimeout(function(){
                                          window.location.reload();
                                      },1000)
                                  });
                              },
                              error: function (data, textStatus, errorThrown) {
                                  var err='';
                                  $.each(data.responseJSON.errors, function(key, value){
                                      err+='<p>'+value+'</p>';
                                  });
                                  alertbox('Error',err,function(modal){
                                  });
                              },
                              complete: function(data) {

                              }
                          });

                      }
                  });

              }
    $(document).ready( function () {
     
            var table  = $('#table_id').DataTable({
                responsive: true,
                dom:'<"top"fiB><"bottom"trlp><"clear">',
                buttons: [
                    // {extend: 'excelHtml5'}, 
                    // {extend: 'pdfHtml5'},   
                    // {               
                    //     extend: 'print',
                    //     exportOptions: {
                    //         columns: ':visible'
                    //     }
                    // },
                    // 'colvis'
                    {
                        extend: 'colvis',
                        text: '<i class="fa fa-eye"></i>',
                        titleAttr: 'Column Visibility'
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'Excel'
                    }, 
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF'
                    },
                    {               
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        titleAttr: 'Print',
                        exportOptions: {
                            columns: ':visible',
                        },
                    }
                ],
                // columnDefs: [ {
                //     targets: -1,
                //     visible: false
                // }],
                language: {
                    search: "",
                    searchPlaceholder: "Search"
                }              
            });
            var table1  =  $('#table_id1').DataTable({
                responsive: true,
                dom:'<"top"fiB><"bottom"trlp><"clear">',
                buttons: [
                    // {extend: 'excelHtml5'}, 
                    // {extend: 'pdfHtml5'},   
                    // {               
                    //     extend: 'print',
                    //     exportOptions: {
                    //         columns: ':visible'
                    //     }
                    // },
                    // 'colvis'
                    {
                        extend: 'colvis',
                        text: '<i class="fa fa-eye"></i>',
                        titleAttr: 'Column Visibility'
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'Excel'
                    }, 
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF'
                    },
                    {               
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        titleAttr: 'Print',
                        exportOptions: {
                            columns: ':visible',
                        },
                    }
                ],
                columnDefs: [ {
                    targets: -1,
                    visible: false
                }],
                language: {
                    search: "",
                    searchPlaceholder: "Search"
                }
            });

                //$('#table_id1').show();
                //$('#table_id1').removeClass('hidden');
    
     

        /*$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
                var min = $('#startdate').datepicker("getDate");
                //$.datepicker.formatDate('dd-mm-yy', min); 
                var max = $('#enddate').datepicker("getDate");
                var startDate1 = new Date(data[1]);
                if (min == null && max == null) { return true; }
                if (min == null && startDate1 <= max) { return true;}
                if(max == null && startDate1 >= min) {return true;}
                if (startDate <= max && startDate1 >= min) { return true; }
                return false;
        });
        //$( "#datepicker" ).datepicker( "option", "dateFormat", "dd/mm/yyyy" );
        //$("#startdate").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true ,dateFormat: 'dd/mm/yy'});
        $("#startdate").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            numberOfMonths: 1,
            buttonImage: 'contact/calendar/calendar.gif',
            buttonImageOnly: true,
            onSelect: function(selectedDate) {
                 table.draw();
             }
          });
        $("#enddate").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            numberOfMonths: 1,
            buttonImage: 'contact/calendar/calendar.gif',
            buttonImageOnly: true,
            onSelect: function(selectedDate) {
                 table.draw();
             }
          });*/
        //$("#enddate").datepicker({  onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true ,dateFormat: 'dd/mm/yy'});
        //$('#startdate, #enddate').change(function () {  table.draw({ responsive: true }); }); 
    });
    </script>
 
@endsection

<!-- Modal -->
