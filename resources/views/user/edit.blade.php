@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('User - Edit') }}</h1>

    </div>

  </div>
  <div class="card form__container">

    @if(session()->has('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}
    </div>
    @endif
    <div class="card-body">
      <div class="form__user__edit">

        {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'files' => true)) }}
        @csrf
        <div class="form-group row form_field__container">
          <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
          <div class="col-md-4">
            <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" >
              <option value="">Select Restaurant</option>
              @foreach($restaurantData as $rest)
              <option value="{{ $rest->id }}" {{ (old('restaurant_id')==$rest->id || (count(old())==0 && $user->restaurant_id==$rest->id)) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
              @endforeach
            </select>
            @if ($errors->has('restaurant_id'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('restaurant_id') }}</strong>
            </span>
            @endif
          </div>
        </div>

        <div class="form-group row form_field__container">
          <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
          <div class="col-md-4">
            <div class="input__field">
              <input id="fname" type="text" class=" {{ $errors->has('fname') ? ' is-invalid' : '' }}"
              name="fname" value="{{ count(old())==0 ? $user->fname : old('fname') }}" required autocomplete="off">
            </div>
            @if ($errors->has('fname'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('fname') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group row form_field__container">
          <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>
          <div class="col-md-4">
            <div class="input__field">
              <input id="lname" type="text" class=" {{ $errors->has('lname') ? ' is-invalid' : '' }}"
              name="lname" value="{{ count(old())==0 ? $user->lname : old('lname') }}" required autocomplete="off">
            </div>
            @if ($errors->has('lname'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('lname') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group row form_field__container">
          <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
          <div class="col-md-4">
            <div class="input__field">
              <input id="email" type="text" class=" {{ $errors->has('email') ? ' is-invalid' : '' }}"
              name="email" value="{{ count(old())==0 ? $user->email : old('email') }}" required autocomplete="off">
            </div>
            @if ($errors->has('email'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group row form_field__container">
          <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
          <div class="col-md-4">
            <div class="input__field">
              <input id="password" type="text" class=" {{ $errors->has('password') ? ' is-invalid' : '' }}"
              name="password" value="{{ old('password') }}" required autocomplete="off">
            </div>
            @if ($errors->has('password'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group row form_field__container">
          <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
          <div class="col-md-4" style="padding-top: 10px;">
            <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1' || (count(old())==0 && $user->status=='1')) ? 'checked' :'' }}> Active
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0' || (count(old())==0 && $user->status=='0')) ? 'checked' :'' }}> Inactive
            @if ($errors->has('status'))
            <span class="invalid-feedback" style="display:block;">
              <strong>{{ $errors->first('status') }}</strong>
            </span>
            @endif
          </div>
        </div>
        {{--<div class="form-group row source-image-div image-caption-div">
          <label for="display_image" class="col-md-4 col-form-label text-md-right">{{ __('Display Image') }}</label>
          <div class="col-md-4">

            <input id="display_image" type="file" class=" {{ $errors->has('display_image') ? ' is-invalid' : '' }}"
            value="{{ old('display_image') }}" name="display_image">
            @if ($errors->has('display_image'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('display_image') }}</strong>
            </span>
            @endif
          </div>
          @if(isset($user->display_pic_url) && $user->display_pic_url!='')
          <div class="col-md-4">
            @php
            $displayPic = strpos($user->display_pic_url, 'http')!==false ? $user->display_pic_url : url('/').'/..'.$user->display_pic_url;
            @endphp
            <a data-fancybox="gallery" href="{{url('/').'/..'.$user->display_pic_url}}"><img src="{{ $displayPic }}" style="width:100px;"></a>&emsp;
            <input type="checkbox" name="display_image_delete" value="1" style="margin-left:20px;">Delete
          </div>
          @endif
        </div>--}}

        <br /><br />
        <div class="row text-center mb-0">
          <div class="text-center col-md-12">
            <button type="submit" class="btn btn__primary">{{ __('Save User') }}</button>
          </div>
        </div>
      </form>

    </div>

  </div>
</div>

</div>
@endsection
