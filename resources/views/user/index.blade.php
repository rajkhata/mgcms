@extends('layouts.app')

@section('content')
<div class="main__container container__custom">

  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Users <span class="user__currentPage">({{ $users->currentPage() .'/' . $users->lastPage() }})</span>&emsp;</h1>

    </div>

    <span><a href="{{ URL::to('users/create') }}" class="btn btn__primary">Add</a></span>
    <!--<span style="float: right;">({{ $users->count() .'/' . $users->total() }})</span>-->
  </div>
  <div class="addition__functionality">
    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">
      <div class="row form-group col-md-3">
        <select name="restaurant_id" id="restaurant_id" class="form-control select__restraunt" >
          <option value="">Select Restaurant</option>
          {{--@foreach($restaurantData as $rest)
            <option value="{{ $rest->id }}" {{ (isset($searchArr['restaurant_id']) && $searchArr['restaurant_id']==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
            @endforeach--}}
          </select>
        </div>
        <div class="row col-md-6 pr0 form_field__container">

          <div style="display: flex;">
            <div class="input__field" >
              <input id="name" class="search__input" type="text" name="name" value="{{isset($searchArr['name']) ? $searchArr['name'] : ''}}">
              <label for="name">Name</label>
            </div>
            <button style="margin-left: 10px" class="btn btn__primary btn__search" type="submit" >Search</button>
          </div>

        </div>
      </div>

    </div>
    {!! Form::close() !!}
    <div class="rest__user-wrapper">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif

      <div class="guestbook__container box-shadow">
        <div class="row guestbook__table-header hidden-xs hidden-sm hidden-tablet">
          <div class="col-md-1" style="font-weight: bold;">Name</div>
          <div class="col-md-2" style="font-weight: bold;">Email</div>
          <div class="col-md-2" style="font-weight: bold;">Restaurant</div>
          <div class="col-md-2" style="font-weight: bold;">Mobile</div>
          <div class="col-md-2" style="font-weight: bold;">Phone</div>
          <div class="col-md-1" style="font-weight: bold;">Status</div>
          <div class="col-md-2" style="font-weight: bold;">Action</div>
        </div>


        @if(isset($users) && count($users)>0)
        <div class="guestbook__cutomer-details-wrapper">
          @foreach($users as $usr)
          <div class="row guestbook__customer-details-content desk__res hidden-xs hidden-sm hidden-tablet">
            <div class="col-md-1" >{{ $usr->fname .' '. $usr->lname }}</div>
            <div class="col-md-2" >{{ $usr->email }}</div>
            <div class="col-md-2" >{{ $usr->Restaurant->restaurant_name }}</div>
            <div class="col-md-2" >{{ $usr->mobile }}</div>
            <div class="col-md-2" >{{ $usr->phone }}</div>
            <div class="col-md-1" >{{ ($usr->status==1)?'Active':($usr->status==0?'Inactive':'') }}</div>
            <div class="row col-md-2 rest__cta-container">
              <div class="col-md-6">
                <a href="{{ URL::to('users/' . $usr->id . '/edit') }}" class="btn btn__primary edit"><span class="fa fa-pencil"></span>&nbsp;Edit</a>
              </div>
              <div class="col-md-6">
                {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $usr->id]]) !!}
                @csrf
                <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</button>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
          <div class="guestbook__customer-details-content hidden-lg hidden-md table-view show-tablet">
            <div class="col-sm-7 col-md-7 no-padding-left">
              <div class="row customer__name">{{ $usr->fname .' '. $usr->lname }}</div>
              <div class="row customer__email"><i class="fa fa-envelope-o" aria-hidden="true"></i>  <span class="customer__email-txt">{{ $usr->email }}</span></div>

              <div class="row"><i class="fa fa-cutlery" aria-hidden="true"></i> </i> {{ $usr->Restaurant->restaurant_name }} </div>
              <div class="row"><i class="fa fa-phone" aria-hidden="true"></i> {{ $usr->mobile }}</div>
              <div class="row"><i class="fa fa-phone" aria-hidden="true"></i> {{ $usr->phone }}</div>
              <div class="row"><i class="fa fa-users" aria-hidden="true"></i><strong>{{ ($usr->status==1)?'Active':($usr->status==0?'Inactive':'') }}</strong></div>
            </div>
            <div class="col-sm-5 col-md-5 text-right no-padding-right">

              <div class="row">
                <div class="guest_btn mob__guest__btn">
                  <div class="col-md-6 mob__btn hidden-md">
                    {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $usr->id]]) !!}
                    @csrf
                    <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</button>
                    {!! Form::close() !!}
                  </div>
                  <div class="col-md-6 mob__btn">
                    <a href="{{ URL::to('users/' . $usr->id . '/edit') }}" class="btn btn__primary edit"><span class="fa fa-pencil"></span>&nbsp;Edit</a>

                  </div>
                  <div class="col-md-6 mob__btn hidden-sm">
                    {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $usr->id]]) !!}
                    @csrf
                    <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</button>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach

        </div>
        @else
        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
          No Record Found
        </div>
        @endif
      </div>

      @if(isset($users) && count($users)>0)
      <div class="pagination__wrapper" style="margin: 0px auto;">
        {{ $users->appends($_GET)->links()}}
      </div>
      @endif
    </div>
  </div>



</div>
@endsection
