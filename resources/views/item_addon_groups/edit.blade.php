@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Item Addon Group - Edit') }}</h1>

    </div>

  </div>
  <div>

    <div class="card form__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit"><?php // {{ route('modifier_categories.add') }}?>
          <form method="POST" action="/item_addon_groups/update/{{ $itemAddonGroup->id }}" enctype="multipart/form-data">
            <input id="category_name" type="hidden" value="{{ $itemAddonGroup->id }}" />
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="form-group row form_field__container">
              <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
              <div class="col-md-6">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" >
                  <option value="">Select restaurant</option>
                  @foreach($restaurantData as $rest)
                  <option value="{{ $rest->id }}" {{ count(old())==0 ? ($itemAddonGroup->restaurant_id == $rest->id ? 'selected' : '') : ( (old('restaurant_id')==$rest->id) ? 'selected' :'' ) }}>{{ $rest->restaurant_name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="language_id" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
              <div class="col-md-4">
                <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                  <option value="">Select Language</option>
                  @foreach($languageData as $lang)
                  <option value="{{ $lang->id }}" {{ count(old()) ? ((old('language_id')==$lang->id) ? 'selected' :'') : ($itemAddonGroup->language_id == $lang->id ? 'selected' :'') }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                  @endforeach
                </select>
                @if ($errors->has('language_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('language_id') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="group_name" class="col-md-4 col-form-label text-md-right">{{ __('Item Addon Group Name *') }}</label>

              <div class="col-md-6">
                <div class="input__field">
                  <input id="group_name" type="text" class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
                         name="group_name" value="{{ count(old()) ? old('group_name') : $itemAddonGroup->group_name }}" required>
                </div>
                @if ($errors->has('group_name'))
                  <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('group_name') }}</strong>
                </span>
                @endif
              </div>
            </div>
            
            <div class="form-group row form_field__container">
              <label for="prompt" class="col-md-4 col-form-label text-md-right">{{ __('prompt') }}</label>

              <div class="col-md-6">
                <div class="input__field">
                  <input id="prompt" type="text" class="{{ $errors->has('prompt') ? ' is-invalid' : '' }}"
                         name="prompt" value="{{ count(old()) ? old('prompt') : $itemAddonGroup->prompt }}" required>
                </div>
                @if ($errors->has('prompt'))
                  <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('prompt') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="is_required" class="col-md-4 col-form-label text-md-right">{{ __('Is Required *') }}</label>
              <div class="col-md-6" style="margin:auto 0px">
                  <input type="radio"  name="is_required" required="" value="1" {{ count(old()) ? ( (old('is_required')=='1') ? 'checked' :'' ) : ($itemAddonGroup->is_required == '1' ? 'checked' :'') }}> Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio"  name="is_required" required="" value="0" {{ count(old()) ? ( (old('is_required')=='0') ? 'checked' :'' ) : ($itemAddonGroup->is_required == '0' ? 'checked' :'') }}> No
                @if ($errors->has('is_required'))
                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('is_required') }}</strong>
                </span>
                @endif
              </div>
            </div>
            
            <div class="form-group row form_field__container">
              <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('quantity') }}</label>

              <div class="col-md-6">
                <div class="input__field">
                  <input id="quantity" type="text" class="{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
                         name="quantity" value="{{ count(old()) ? old('quantity') : $itemAddonGroup->quantity }}" required>
                </div>
                @if ($errors->has('quantity'))
                  <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('quantity') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="quantity_type" class="col-md-4 col-form-label text-md-right">{{ __('Quantity Type *') }}</label>
              <div class="col-md-6" style="margin:auto 0px">
                  <input type="radio"  name="quantity_type" required="" value="Value" {{ count(old()) ? ( (old('quantity_type')=='Value') ? 'checked' :'' ) : ($itemAddonGroup->quantity_type == 'Value' ? 'checked' :'') }}> Value
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio"  name="quantity_type" required="" value="Range" {{ count(old()) ? ( (old('quantity_type')=='Range') ? 'checked' :'' ) : ($itemAddonGroup->quantity_type == 'Range' ? 'checked' :'') }}> Range
                @if ($errors->has('quantity_type'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('quantity_type') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class=" row  mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">
                  {{ __('Update') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection