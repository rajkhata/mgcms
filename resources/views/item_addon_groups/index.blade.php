@extends('layouts.app')

@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto manage__order-atto">
            <div class="reservation__title">
                <h1>Item Addon Groups<span
                            style="float: right;margin:auto 5px;">({{ $itemAddonGroups->currentPage() .'/' . $itemAddonGroups->lastPage() }}
                        )</span></h1>
            </div>

            <div class="restName__add"><a href="{{ URL::to('item_addon_groups/create') }}" class="btn btn__primary">Add</a>
            </div>
            {!! Form::open(['method'=>'get']) !!}
            <div class="search__manage-order">
                <div class="form_field__container">

                    <div class="row form-group col-md-5">
                        <select name="restaurant_id" id="restaurant_id" class="form-control">
                            <option value="">Select Restaurant</option>
                            @foreach($restaurantData as $rest)
                                <option value="{{ $rest->id }}" {{ (isset($inputData['restaurant_id']) && $inputData['restaurant_id']==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input__field">
                        <input id="group_name" type="text" class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
                               name="group_name"
                               value="{{ isset($inputData['group_name']) ? $inputData['group_name'] : NULL }}">
                        <label for="group_name">Addon Group Name</label>
                    </div>

                    <div class="row form-group col-md-5">
                        <button class="btn btn__primary" style="margin-left: 5px;" type="submit">Search</button>
                    </div>

                </div>

            </div>
            {!! Form::close() !!}
        </div>

        <div>

            <div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif

                <div class="guestbook__container box-shadow">
                    <div style="min-width: unset !important;"
                         class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
                        <div class="row col-md-12 ">
                            <div class="col-md-3" style="font-weight: bold;">Item Addon Group Name</div>
                            <div class="col-md-2" style="font-weight: bold;">Restaurant</div>
                            <div class="col-md-2" style="font-weight: bold;">Language</div>
                            <div class="col-md-2" style="font-weight: bold;">Required?</div>
                            <div class="col-md-2" style="font-weight: bold;">Action</div>
                        </div>
                    </div>
                </div>
                @if(isset($itemAddonGroups) && count($itemAddonGroups)>0)
                    @foreach($itemAddonGroups as $itemAddonGroup)
                        <div class="row guestbook__customer-details-content order__row">
                            <div class="row col-md-12 order__row    " style="min-width:275px;">
                                <div class="col-md-2" style="margin: auto 0px;">{{ $itemAddonGroup->group_name }}</div>
                                <div class="col-md-2" style="margin: auto 0px;">{{ $itemAddonGroup->restaurant->restaurant_name }}</div>
                                <div class="col-md-2" style="margin: auto 0px;">{{ $itemAddonGroup->language->language_name }}</div>
                                <div class="col-md-2" style="margin: auto 0px;">{{ isset($itemAddonGroup->is_required) && $itemAddonGroup->is_required=="1" ? "Yes" : "No" }}</div>

                                <div class="row col-md-2" style="margin: auto 0px;">
                                    <div class="col-md-12">
                                        <a style="width:100%;margin:10px 0;"
                                           href="{{ URL::to('item_addon_groups/' . $itemAddonGroup->id . '/edit') }}"
                                           class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
                                    </div>
                                    <div class="col-md-12">
                                        {!! Form::open(['method' => 'DELETE', URL::to('item_addon_groups/destroy'), $itemAddonGroup->id]) !!}
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $itemAddonGroup->id }}" />
                                        <button style="width:100%;margin:10px 0;" class="btn btn__cancel"
                                                type="submit">
                                            <span class="glyphicon glyphicon-trash"></span> Delete
                                        </button>
                                        {!! Form::close() !!}
                                    </div>

                                    @php
                                    $user = Auth::user();
                                    @endphp

                                </div>
                            </div>
                        </div>
                            @endforeach
                            @else
                                <div class="row"$itemAddonGroups
                                     style="padding:10px;text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                                    No Record Found
                                </div>
                            @endif
                        @if(isset($itemAddonGroups) && count($itemAddonGroups)>0)
                            <div style="margin: 0px auto;">
                                {{ $itemAddonGroups->appends($_GET)->links()}}
                            </div>
                        @endif
            </div>

        </div>
    </div>
@endsection
