@php
    /**
    * @see \App\Http\Controllers\OnboardingController::create
    */
@endphp

@extends('layouts.app')
@section('content')
    <link href="{{asset('css/multiple-emails.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('js/multiple-emails.js')}}" type="text/javascript" charset="utf-8"></script>
    <div class="main__container notificationcontainer margin-lr-auto">

        <div class="container margin-lr-auto templates-page">
            <div class="col-xs-12">
                <h1>Choose a Template to Start With</h1>
            </div>
            <div class="fullWidth flex-box flex-direction-row flex-wrap">
                <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
                    <div class="fullWidth box-shadow">
                        <img src="{{ asset('images/templates/template-1.jpg') }}">
                    </div>
                    <div class="fullWidth padding-top-bottom-15">
                        <h2 class="font-size-14 font-weight-800">Template Name</h2>
                        <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu. Nunc accumsan eros sem. Ut massa mauris.</p>
                        <p class="text-center margin-top-15">
                            <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                            <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
                    <div class="fullWidth box-shadow">
                        <img src="{{ asset('images/templates/template-2.jpg') }}">
                    </div>
                    <div class="fullWidth padding-top-bottom-15">
                        <h2 class="font-size-14 font-weight-800">Template Name</h2>
                        <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Asan eros sem. Ut massa mauris.</p>
                        <p class="text-center margin-top-15">
                            <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                            <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
                    <div class="fullWidth box-shadow">
                        <img src="{{ asset('images/templates/template-3.jpg') }}">
                    </div>
                    <div class="fullWidth padding-top-bottom-15">
                        <h2 class="font-size-14 font-weight-800">Template Name</h2>
                        <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu.</p>
                        <p class="text-center margin-top-15">
                            <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                            <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
                    <div class="fullWidth box-shadow">
                        <img src="{{ asset('images/templates/template-4.jpg') }}">
                    </div>
                    <div class="fullWidth padding-top-bottom-15">
                        <h2 class="font-size-14 font-weight-800">Template Name</h2>
                        <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu. Nunc accumsan eros sem. Ut massa mauris.</p>
                        <p class="text-center margin-top-15">
                            <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                            <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
                    <div class="fullWidth box-shadow">
                        <img src="{{ asset('images/templates/template-5.jpg') }}">
                    </div>
                    <div class="fullWidth padding-top-bottom-15">
                        <h2 class="font-size-14 font-weight-800">Template Name</h2>
                        <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu. Nunc accumsan eros sem. Ut massa mauris.</p>
                        <p class="text-center margin-top-15">
                            <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                            <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
                    <div class="fullWidth box-shadow">
                        <img src="{{ asset('images/templates/template-6.jpg') }}">
                    </div>
                    <div class="fullWidth padding-top-bottom-15">
                        <h2 class="font-size-14 font-weight-800">Template Name</h2>
                        <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu. Nunc accumsan eros sem. Ut massa mauris.</p>
                        <p class="text-center margin-top-15">
                            <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                            <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <script type="text/javascript">

        $("body").addClass("login-page");

        $(".select-theme").click(function(){
            ConfirmBox('Please Confirm','If you would like to use the selected template/layout for your website?',function(resp,curmodal){
                if(resp){
                    curmodal.modal('hide');
                }
            });
        });

    </script>
@endsection
