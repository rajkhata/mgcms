@extends('layouts.appWithoutSidebar')
@section('content')
    <link href="{{asset('css/multiple-emails.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('js/multiple-emails.js')}}" type="text/javascript" charset="utf-8"></script>

    <div class="main__container notificationcontainer margin-lr-auto templates-page">
        <form method="POST" action="{{ route('setrestauranttheme') }}" enctype="multipart/form-data"> @csrf
        <div class="row">
            <input type="hidden" id="id" name="id" value="0">
            <div class="fullWidth">
                <h1>Choose a Template to Start With</h1>
            </div>
            <div class="fullWidth">

                    <div class="negative-margin-lr flex-box flex-direction-row flex-wrap">
                    @foreach($templateDetails as $temp)
                    <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
                        <div class="fullWidth box-shadow">
                            <img src="{{ asset($temp->theme_image) }}">
                        </div>
                        <div class="fullWidth padding-top-bottom-15">
                            <h2 class="font-size-14 font-weight-800">{{$temp->name}}</h2>
                            <p class="font-weight-600">{{$temp->theme}}</p>
                            <p class="text-center margin-top-15">
                                <button onclick="return setTheme({{$temp->id}})" class="btn btn__primary width-120 select-theme">Select</button>
                                <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                            </p>
                        </div>
                    </div>
                    @endforeach
                    </div>
            </div>
        </div>
        </form>

    </div>
    <script type="text/javascript">

        $("body").addClass("login-page");

        $(".select-theme123").click(function(){
            ConfirmBox('Please Confirm','If you would like to use the selected template/layout for your website?',function(resp,curmodal){
                console.log(resp);
                if(resp){
                    curmodal.modal('hide');
                     return true;


                }else return false;
            });
        });
        function setTheme(tid) {
            $("#id").val(tid);
            ConfirmBox('Please Confirm','If you would like to use the selected template/layout for your website?',function(resp,curmodal){
                console.log(resp);
                if(resp==true){
                    curmodal.modal('hide');
                    return true;


                }else return false;
            });

        }
    </script>
@endsection

