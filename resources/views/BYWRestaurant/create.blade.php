@extends('layouts.appWithoutSidebar&MunchadoHeader')
@section('content')

    <div class="main__container margin-lr-auto new-design-labels setup-restaurant">

        <div class="row">

            <div class="alert alert-success" style="display:none"></div>
            <div class="alert alert-danger" style="display:none"></div>

            <form method="POST" action="{{ route('registardomain') }}" enctype="multipart/form-data" id="restaurantform" autocomplete="off">
            @csrf

            <!--Header-->
                <div class="reservation__atto flex-direction-row">
                    <div class="reservation__title margin-top-5">
                        <h1>Restaurant On Boarding Form</h1>
                    </div>
                </div>

                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                    <div class="col-xs-12 text-center text-uppercase font-weight-700">Restaurant Information</div>
                    <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-bottom-10">
                        <div class="col-xs-12 col-sm-5 margin-top-20">
                            <div class="input__field">
                                <input id="deal_id" autocomplete="off" type="text" value="{{ old('deal_id') }}" name="deal_id" required>
                                <span for="deal_id">Deal Id</span>
                                @if ($errors->has('deal_id'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('deal_id') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                            <div class="input__field">
                                <input id="restaurant_name" autocomplete="off" type="text" value="{{ old('restaurant_name') }}" name="restaurant_name" required>
                                <span for="restaurant_name">Restaurant Name</span>
                                @if ($errors->has('restaurant_name'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('restaurant_name') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-5 margin-top-30">
                            <div class="selct-picker-plain position-relative">
                                <select id="cusine" name="cusine" data-size="8" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                    <option value="">&nbsp;</option>
                                    @foreach($Cusines as $lang)
                                        <option value="{{ $lang->name }}" {{ (old('language_id')==$lang->name) ? 'selected' :'' }}>
                                            {{ isset($lang->name)?ucfirst($lang->name):'' }}
                                        </option>
                                    @endforeach
                                </select>
                                <span>Type of Restaurant (Cuisine)</span>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('id') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                            <div class="selct-picker-plain position-relative">
                                <select name="type" id="type" class="selectpicker ignore" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                    <option value="">&nbsp;</option>
                                    <option value="SHORT CUT ($199/mo)">SHORT CUT ($199/mo)</option>
                                    <option value="LEAN CUT ($399/mo)">LEAN CUT ($399/mo)</option>
                                    <option value="FIRST CUT ($599/mo)">FIRST CUT ($599/mo)</option>
                                    <option value="STANDARD CUT ($999/mo)">STANDARD CUT ($999/mo)</option>
                                    <option value="SELECT CUT ($1,299/mo)">SELECT CUT ($1,299/mo)</option>
                                    <option value="CHOICE CUT ($2,500/mo)">CHOICE CUT ($2,500/mo)</option>
                                </select>
                                <span>Package Type</span>
                                @if ($errors->has('type'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('type') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-5 margin-top-30">
                            <div class="input__field">
                                <input id="email" autocomplete="off" type="text" class="cms_user_id {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" name="email" required>
                                <span for="email">Email/Admin User Id</span>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                            <div class="input__field">
                                <input id="phoneno" autocomplete="off" type="text" class="phoneno {{ $errors->has('phoneno') ? ' is-invalid' : '' }}" value="{{ old('phoneno') }}" name="phoneno" required>
                                <span for="phoneno">Phone No</span>
                                @if ($errors->has('phoneno'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('phoneno') }}</strong></span>
                                @endif
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-5 margin-top-30">
                            <div class="input__field">
                                <input id="feature" autocomplete="off" name="feature" type="text" class="{{ $errors->has('feature') ? ' is-invalid' : '' }}" value="{{ old('feature') }}" required>
                                <span for="feature">Restaurant Feature</span>
                                <small class="font-weight-700"><i>Ex : Pizza</i></small>
                                @if ($errors->has('feature'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('feature') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                            <div class="input__field">
                                <input id="tagline" autocomplete="off" name="tagline" type="text" class="{{ $errors->has('tagtine') ? ' is-invalid' : '' }}" value="{{ old('tagline') }}" required>
                                <span for="tagline">Tagline</span>
                                @if ($errors->has('tagline'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('tagline') }}</strong></span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                    <div class="col-xs-12 text-center text-uppercase font-weight-700">Allowed Services</div>
                    <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-20 allowedServices">
                        <div class="col-xs-4 no-padding-left">
                            <label class="custom_checkbox relative font-weight-700 label-color">
                                <input type="checkbox" id="default_yes" class="hide delivery__radio" name="allowedServices[]" value="Delivery">&nbsp;&nbsp; Delivery
                                <span class="control_indicator"></span>
                            </label>
                        </div>

                        <div class="col-xs-4">
                            <label class="custom_checkbox relative font-weight-700 label-color">
                                <input type="checkbox" id="default_no" class="hide delivery__radio" name="allowedServices[]" value="Takeout">&nbsp;&nbsp;&nbsp; Takeout
                                <span class="control_indicator"></span>
                            </label>
                        </div>
                        @if ($errors->has('is_allowed_services'))
                            <span class="invalid-feedback"
                                  style="display: block;"><strong>{{ $errors->first('is_allowed_services') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                    <div class="col-xs-12 text-center text-uppercase font-weight-700">Account Manager Information</div>
                    <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-10 padding-bottom-10">

                        <div class="col-xs-12 col-sm-5 margin-top-30">
                            <div class="input__field">
                                <input id="amname" autocomplete="off" type="text" class="{{ $errors->has('amname') ? ' is-invalid' : '' }}" value="{{ old('amname') }}" name="amname" required>
                                <span for="amname">Acount Manager Name</span>
                                @if ($errors->has('amname'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('amname') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                            <div class="input__field">
                                <input id="amemail" autocomplete="off" type="text" class="{{ $errors->has('amemail') ? ' is-invalid' : '' }}" value="{{ old('amemail') }}" name="amemail" required>
                                <span for="amemail">Acount Manager Email</span>
                                @if ($errors->has('amemail'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('amemail') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                    <div class="col-xs-12 text-center text-uppercase font-weight-700">Contact Address</div>
                    <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-10">

                        <div id="address">

                            <div class="col-xs-12 col-sm-5 margin-top-10">
                                {{--<div class="font-size-13 label-color">Title</div>--}}
                                <div class="selct-picker-plain position-relative">
                                    <select name="title" id="title" class="selectpicker {{ $errors->has('type') ? ' is-invalid' : '' }}" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                        <option value="0">&nbsp;</option>
                                        <option value="Owner">Owner</option>
                                        <option value="Manager">Manager</option>
                                    </select>
                                    <span>Title</span>
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="contact_person" id="contact_person" />
                                    <span for="contact_person">Contact Person</span>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-xs-12 col-sm-5 margin-top-10">
                                <div class="input__field">
                                    <div id="locationField" class="fullWidth findArea input__field">
                                        <input id="autocomplete" autocomplete="off" placeholder="" name="address" class="{{ $errors->has('phoneno') ? ' is-invalid' : '' }}" onFocus="geolocate(0)" type="text"/>
                                        <span for="autocomplete">Address 1</span>
                                        <input type="hidden" name="latlong" id="latlong"/>
                                    </div>
                                    @if ($errors->has('contact_address'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('contact_address') }}</strong></span>
                                    @endif
                                </div>
                                <div class="input__field hide">
                                    <input class="field" autocomplete="off" name="street_number" id="street_number" disabled="true"/>
                                    <span for="street_number">Street address</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="route" id="route" disabled="true"/>
                                    <span for="route">Address 2</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="locality" id="locality" disabled="true"/>
                                    <span for="locality">City</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="administrative_area_level_1" id="administrative_area_level_1" disabled="true"/>
                                    <span for="administrative_area_level_1">State</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="postal_code" id="postal_code" disabled="true"/>
                                    <span for="postal_code">Zip code</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="country" id="country" disabled="true"/>
                                    <span for="country">Country</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                    <div class="col-xs-12 text-center text-uppercase font-weight-700">Pickup Address</div>
                    <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-10">

                        <div id="address">

                            <div class="col-xs-12 col-sm-5 margin-top-10">
                                {{--<div class="font-size-13 label-color">Title</div>--}}
                                <div class="selct-picker-plain position-relative">
                                    <select name="title1" id="title1" class="selectpicker {{ $errors->has('type') ? ' is-invalid' : '' }}" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                        <option value="0">&nbsp;</option>
                                        <option value="Owner">Owner</option>
                                        <option value="Manager">Manager</option>
                                    </select>
                                    <span>Title</span>
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="pickup_contact_person1" id="pickup_contact_person1" />
                                    <span for="pickup_contact_person1">Contact Person</span>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-xs-12 col-sm-5 margin-top-10">

                                <div class="input__field">
                                    <div id="locationField" class="fullWidth findArea input__field">
                                        <input id="autocomplete1" name="address1" autocomplete="off" placeholder=""   onFocus="geolocate(1)" type="text"/>
                                        <span>Address 1</span>
                                    </div>
                                    @if ($errors->has('pickup_address'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('pickup_address') }}</strong></span>
                                    @endif
                                </div>

                                <div class="input__field hide">
                                    <input class="field" autocomplete="off" name="street_number1" id="street_number1" disabled="true"/>
                                    <span for="street_number1">Street address</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="route1" id="route1" disabled="true"/>
                                    <span for="route1">Address 2</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="locality1" id="locality1" disabled="true"/>
                                    <span for="locality1">City</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" id="administrative_area_level_11"  name="administrative_area_level_11"  disabled="true"/>
                                    <span for="route1">State</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="postal_code1" id="postal_code1" disabled="true"/>
                                    <span for="postal_code1">Zip code</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                                <div class="input__field">
                                    <input class="field" autocomplete="off" name="country1" id="country1" disabled="true"/>
                                    <span for="country1">Country</span>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

                <div class="row text-center margin-top-30">
                    <button id="reg-new-restaurant-btn" type="button" class="btn_ btn__primary box-shadow">Register New Restaurant</button>
                </div>
            </form>


            <div id="thank-you" class="hidden">
                <div class="reservation__atto flex-direction-row hide">
                    <div class="reservation__title margin-top-5">
                        <h1>Thank You</h1>
                    </div>
                </div>
                <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30 text-center">
                    <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-10 font-weight-700">
                        <h1 class="margin-bottom-15">Thank You for connecting with Munch Ado. Sit back & relax. We are setting up your profile.</h1>
                        <p class="margin-top-20 margin-bottom-20"><i class="fa fa-check" aria-hidden="true"></i></p>
                        <!--p>Kindly check your inbox.</p-->
                    </div>
                </div>

                <style>
                    #thank-you h1 {
                        font-size: 40px;
                    }
                    #thank-you .fa-check {
                        width: 100px;
                        height: 100px;
                        line-height: 100px;
                        border:2px solid #ff8700;
                        -webkit-border-radius: 50%;
                        -moz-border-radius: 50%;
                        border-radius: 50%;
                        font-size: 60px;
                        color: #ff8700;
                    }
                </style>

            </div>
        </div>
    </div>



    {{--<div class="overlayContainer" id="overlayContainer">

        <img id="loader" src="/images/restaurant-loader1.gif"/>

    </div>--}}


    <div id="loader" class="hidden">
        <span style="background-image: url({{asset('images/Rolling.gif')}})"></span>
        <img src="{{asset('images/Rolling.gif')}}" class="hidden">

    </div>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC55ZKtM00wLqY0sG1_YGo52LhfZMg3u6w&libraries=places&callback=initAutocomplete"
            async></script>
    <script>
        // This sample uses the Autocomplete widget to help the user select a
        // place, then it retrieves the address components associated with that
        // place, and then it populates the form fields with those details.
        // This sample requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script
        // src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        var placeSearch, autocomplete, autocomplete1;

        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search predictions to
            // geographical location types.
            autocomplete = new google.maps.places.Autocomplete(
                document.getElementById('autocomplete'), {types: ['geocode']});
            autocomplete1 = new google.maps.places.Autocomplete(
                document.getElementById('autocomplete1'), {types: ['geocode']});
            // Avoid paying for data that you don't need by restricting the set of
            // place fields that are returned to just the address components.
            autocomplete.setFields(['address_component']);
            autocomplete1.setFields(['address_component']);
            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
            autocomplete1.addListener('place_changed', fillInAddress1);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                    $('#'+addressType).blur()
                    //console.log(val)
                }
            }
            codeAddress();
        }

        function fillInAddress1() {
            // Get the place details from the autocomplete object.
            var place = autocomplete1.getPlace();

            for (var component in componentForm) {
                document.getElementById(component + '1').value = '';
                document.getElementById(component + '1').disabled = false;
            }

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            //console.dir(place);
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + '1').value = val;
                    $('#'+addressType+'1').blur()
                    //console.log(val)

                }
            }
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate(id) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var circle = new google.maps.Circle(
                        {center: geolocation, radius: position.coords.accuracy});
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }


        function codeAddress() {
            var geocoder = new google.maps.Geocoder;
            var address = document.getElementById('autocomplete').value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == 'OK') {
                    var results = results.map(function (item) {
                        item.geometry.locationString = item.geometry.location.toString();
                        document.getElementById('latlong').value = item.geometry.locationString;
                        //console.log(item.geometry.locationString);
                        //console.log(item.geometry.locationString.split("("))
                        return item;

                    });
                }
            });
        }

        window.localStorage.removeItem("prevSelectOpt");

        $(".selectpicker").on("shown.bs.select", function (e) {
            window.localStorage.setItem('prevSelectOpt', $(this).val());
        });

        $(".selectpicker").change(function(){
            if($(this).val() != ""){
                $(this).parents(".bootstrap-select").next().addClass("hasVal")
            }
        });

        $(".selectpicker").on("hidden.bs.select", function () {
            if(window.localStorage.getItem('prevSelectOpt') == $(this).val()) {
                $(this).selectpicker('val','0')
                //$(this).parents("").find(".filter-option").text("")
                $(this).parents(".bootstrap-select").next().removeClass("hasVal")
            }
        });


        function setupRestaurant() {
            $('#loader').removeClass('hidden');
            $('.alert-danger').hide();
            var data1 = $('form').serialize();
            //console.dir(data1);

            $.ajax({
                accepts: {
                    text: "application/json"
                },
                url: '/createdomain',
                type: 'POST',
                data: data1,
                success: function (data) {
                    var obj = JSON.parse(data);
                    $('#loader').addClass('hidden');

                    if (obj.error == 0) {
                        $('#restaurantform')[0].reset();
                        $('.alert-success').html(obj.msg);
                        $('.alert-success').show();
                        $('.alert-danger').hide();

                        $("#restaurantform").addClass("hidden");
                        $("#thank-you").removeClass("hidden");
                        $("html, body").animate({scrollTop : 0},700);

                    } else if (obj.error == 2) {
                        $('.alert-danger').html(obj.msg);
                        $('.alert-danger').show();
                        $('.alert-success').hide();
                        $("html, body").animate({scrollTop : 0},700);
                    } else if (obj.error == 1) {
                        $('.alert-danger').html(obj.msg);
                        $('.alert-danger').show();
                        $('.alert-success').hide();
                        $("html, body").animate({scrollTop : 0},700);
                    } else if (obj.error == 3) {
                        $('.alert-danger').html(obj.msg);
                        $('.alert-danger').show();
                        $('.alert-success').hide();
                        $("html, body").animate({scrollTop : 0},700);
                    } else if (obj.error == 4) {
                        $('.alert-danger').html(obj.msg);
                        $('.alert-danger').show();
                        $('.alert-success').hide();
                        $("html, body").animate({scrollTop : 0},700);
                    }
                }
            });
        }


        $( "#restaurantform" ).validate({
            ignore: [],
            rules: {
                'deal_id': {
                    required: true,
                },
                'restaurant_name': {
                    required: true,
                },
                'email': {
                    required: true,
                    email: true
                },
                'cusine': {
                    required: true
                },
                'type': {
                    required: true
                },
                'phoneno': {
                    required: true
                },
                'amname': {
                    required: true
                },
                'amemail': {
                    required: true,
                    email: true
                },
                'contact_person': {
                    required: true
                },
                'address': {
                    required: true
                },
                'pickup_contact_person1': {
                    required: true
                },
                'address1': {
                    required: true
                },
                'allowedServices[]': {
                    required: function (){
                        return ($("input.delivery__radio:checked").length)
                    }
                }
            },
            messages:{
                deal_id:"Please the deal Id.",
                restaurant_name:"Please enter the restaurant name.",
                cusine:"Please enter the cusine.",
                type:"Please enter the type.",
                email:"Please enter the valid email/admin user Id.",
                phoneno: "Please enter the phone number.",
                amname: "Please enter the account manager name.",
                amemail: "Please enter the account manager email.",
                contact_person: "Please enter the contact person name.",
                address: "Please enter the contact address.",
                pickup_contact_person1: "Please enter the contact person name.",
                address1: "Please enter the pickup address.",
                'allowedServices[]': "Please select atleast one service."
            },
            errorPlacement: function(error, element){
                if ( element.is(".delivery__radio") )
                {
                    error.appendTo( element.parents('.allowedServices') );
                }
                else
                { // This is the default behavior
                    error.insertAfter( element );
                }
            }
        });

        function scrollToErrorEle(){
            $('html, body').animate({
                scrollTop: $('.error:visible:first').offset().top - 15
            }, 700);
            return false
        }

        $("#reg-new-restaurant-btn").click(function () {

            var $form = $('#restaurantform');



            if(!$form.valid()){
                return scrollToErrorEle();
            }
            setupRestaurant();
        });

    </script>

    <style>
        .overlayContainer {
            height: 100%;
            width: 100%;
            justify-content: center;
            align-items: center;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 99;
            background: black;
            opacity: 0.6;
            display: none;
            color: white;
            flex-direction: column;
        }

        .overlayContainer img {
            max-width: 250px;
        }

        #myProgress {
            align-items: center;
            width: 30%;
            background-color: #ddd;
        }

        #myBar {
            width: 1%;
            height: 30px;
            background-color: #4CAF50;
        }



    </style>

    </div>
    </div>
@endsection
