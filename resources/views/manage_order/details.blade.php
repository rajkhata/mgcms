@extends('layouts.app')
@section('content')
    <div class="main__container container__custom">
        <style>
            /*.a_nd_order > .row:first-child, .section_popup:after, .popup_orderdetail, .panel_list_order{display:none!important;height:0px}*/
            /*.a_nd .u_contentsection{padding-top:0;}*/
            table {
                border-collapse: initial;
            }

            .printcontainer {
                display: none !important;
            }
        </style>

        <style media="print">
            @page {
                size: auto;
                margin: 0;
            }

            .a_nd_order > .row:first-child, .section_popup:after, .popup_orderdetail, .panel_list_order {
                display: none !important;
                height: 0px
            }

            .a_nd .u_contentsection {
                padding-top: 0;
            }

            table {
                border-collapse: initial;
            }
        </style>
        <div>
            @php
                if(Auth::user()) {
                    $curSymbol = Auth::user()->restaurant->currency_symbol;
                } else {
                    $curSymbol = config('constants.currency');
                }
            @endphp

            @if(isset($orderData))

                <div class="row hidden-print order__wrapper">
                    <div class="reservation__atto">
                        <div class="reservation__title">
                            <h1>Order Details</h1>
                        </div>
                        <div>
                            @if($action=="food_item")
                                <a href="{{ URL::to('archive_order') }}" class="btn btn__primary"> Archive Orders</a>
                                <a href="{{ URL::to('user_order') }}" class="btn btn__primary" id="ipad-orders" class="">Active
                                    Orders</a>
                            @endif
                            @if($action=="product")
                                <a href="{{ URL::to('product_archive_orders') }}" class="btn btn__primary"> Merchandise Archive Orders</a>
                                <a href="{{ URL::to('product_orders') }}" class="btn btn__primary" id="ipad-orders" class="">Merchandise Active
                                    Orders</a>
                            @endif
                        </div>

                    </div>
                    <!--<div class="text-left col-md-4 col-xs-6 "> <b style=" line-height: 2.1; font-size: 17px;">Order Details</b> </div>-->

                </div>


                <div class="order-detail-page popup_orderdetail">
                    <!--    <a href="javascript:void(0);" class="a_nd_close orderDetailsPopupClose">+</a>-->
                    <div class="t-scroll1 res-bott-space  footeractive">
                        <div class="row dis-flex res-dis-block od__container">
                            <div class="col-md-6 col-xs-12 dis-flex-1 od__details">

                                <div class="txt_order_des order_del_border custom-width  ">
                                    <p class="text-center o-head"><b>Order Details </b></p>
                                <!--<div class="row">

                <span class=" col-md-5 col-xs-5 ">Order ID </span>
                <div class="detail__ans col-md-7 col-xs-7 _pl0"><b style="margin-right:15px;">:</b>{{ $orderData->id }}</div>

              </div>-->
                                    <div style="margin: 0 0 10px" class="row">
                                        <span class=" col-md-5 col-xs-5 ">Receipt No. </span>
                                        <div class="detail__ans col-md-7 col-xs-7 _pl0"
                                             style="text-transform: capitalize;"><b
                                                    style="margin-right:15px;">:</b><span>{{ $orderData->payment_receipt }}</span>
                                        </div>
                                    </div>
                                    <div style="margin: 0 0 10px" class="row">
                                        <span class=" col-md-5 col-xs-5 ">Type </span>
                                        <div class="detail__ans col-md-7 col-xs-7 _pl0 text-capitalize"><b
                                                    style="margin-right:15px;">:</b><span>{{ $orderData->order_type }} </span>
                                        </div>
                                    </div>

                                    <div style="margin: 0 0 10px" class="row">
                                        <span class=" col-md-5 col-xs-5 ">Time of Order </span>
                                        <div class="detail__ans col-md-7 col-xs-7 _pl0"><b
                                                    style="margin-right:15px;">:</b><span><?php echo $orderDate;?> </span>
                                        </div>
                                    </div>
                                    @if($action=="food_item")
                                        <div style="margin: 0 0 10px" class="row">
                                            <span class=" col-md-5 col-xs-5 ">Date of {{ $orderData->order_type }} </span>
                                            <div class="detail__ans col-md-7 col-xs-7 _pl0 tym_of_deli"><b
                                                        style="margin-right:15px;">:</b><span><?php echo (new DateTime($orderData->delivery_date))->format('l d M, Y');?></span>
                                            </div>
                                        </div>

                                        <div style="margin: 0 0 10px" class="row">
                                            <span class=" col-md-5 col-xs-5 ">Time of {{ $orderData->order_type }} </span>
                                            <div class="detail__ans col-md-7 col-xs-7 _pl0 tym_of_deli"><b
                                                        style="margin-right:15px;">:</b><span><?php echo (!empty($orderData->delivery_time) && $orderData->delivery_time!='null' && $orderData->delivery_time!=null)?(new DateTime($orderData->delivery_time))->format('h:i A'):'';?> </span>
                                            </div>
                                        </div>
                                    @endif
                                    <p class="row">
                                        <span class="col-md-5 col-xs-5 text-left">Past Activities  </span>
                                        <span class="detail__ans col-md-7 col-xs-7 _pl0 text-capitalize"><b
                                                    style="margin-right:15px;">:</b>

                                            <span class="custom__od">
                                                <span>{{ ($userOrders) }} Orders,</span><br/>
                                                <span>{{ count($userReservation) }} Reservations</span>
                                            </span>
                                             </span>

                                    </p>

                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 _pr0 dis-flex-1 od__details">
                                <div class=" full_height  custom-width">

                                    <div class="txt_profile order_del_border black-color">
                                        <p class="text-center o-head"><b>
                                                <?php if ($orderData->order_type == 'delivery') {
                                                    echo "Delivery Details";
                                                } else {
                                                    echo "Carryout Details";
                                                }
                                                ?>
                                            </b></p>

                                        <p class="row">
                                            <span class="col-md-5 col-xs-5 text-left">Name</span>
                                            <span class="detail__ans col-md-7 col-xs-7 _pl0 text-capitalize"><b
                                                        style="margin-right:15px;">:</b><span>{{ $orderData->fname }} {{ $orderData->lname }}</span></span>
                                        </p>
                                        <p class="row">
                                            <span class="col-md-5 col-xs-5 text-left">Email </span>
                                            <span class="detail__ans col-md-7 col-xs-7 _pl0"><b
                                                        style="margin-right:15px;">:</b><span><a
                                                            href="mailto:{{ $orderData->email }}"
                                                            target="_top">{{ $orderData->email }}</a></span></span>
                                        </p>
                                        <p class="row">
                                            <span class="col-md-5 col-xs-5 text-left">Telephone </span>
                                            <span class="detail__ans col-md-7 col-xs-7 _pl0"><b
                                                        style="margin-right:15px;">:</b><span>{{ $orderData->phone }} </span></span>
                                        </p>

                                        <p class="row">
                                            @if($orderData->order_type =='delivery')
                                                <span class="col-md-5 col-xs-5 text-left">Delivery Address </span>
                                            @else
                                                <span class="col-md-5 col-xs-5 text-left">Carryout Address </span>
                                            @endif
                                            <span class="detail__ans col-md-7 col-xs-7 _pl0"><b style="margin-right:15px;">:</b>

                                              <span>  <?php

                                                  $address = $orderData->address;
                                                  if ($orderData->address2) {
                                                      $address = $address . ' ' . $orderData->address2;
                                                  }
                                                  $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                                                  echo $address;
                                                  ?>
                                                  </span>
                    </span>
                                        </p>

                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row order__item-container" style="margin-top: 20px;">
                            <div class="col-xs-1 col-md-1 hide-sm"></div>
                            <div class="col-md-10  col-sm-12  _pl0">

                                <div class="text-center o-head"><b>ORDER SUMMARY</b></div>
                                <div class="order__item-wrapper">

                                    @if(isset($orderDetailData) && count($orderDetailData)>0)
                                        @foreach($orderDetailData as $oData)
                                            <div class="food__row">
                                                <div class=" row food_order_item ">
                                                    <div class="col-md-6 col-xs-6">{{ $oData->item }} </div>
                                                    <i class="col-md-3 col-xs-3 text-center">{{ $oData->quantity }}</i>
                                                    <p class="col-md-3 col-xs-3 text-right mb0">{{  $curSymbol.$oData->total_item_amt }}</p>
                                                </div>
                                                @if(isset($oData->addOnsDetail) && count($oData->addOnsDetail)>0)
                                                    @if($oData->is_byp==0)
                                                        @foreach($oData->addOnsDetail as $oAData)
                                                            <div class="row item_addons">
                                                                <div class="col-md-6 col-xs-6 add__onItem">
                                                                    + {{ @$oAData['addons_name'] }} </div>
                                                                <i class="col-md-3 col-xs-3 text-center"></i>
                                                                <p class="col-md-3 col-xs-3 text-right mb0"></p>
                                                            </div>
                                                        @endforeach
                                                    @endif

                                                    @if($oData->is_byp==1)
                                                        @foreach($oData->addOnsDetail as $key=>$dataval)
                                                            <div class="row item_addons">
                                                                <div class="col-md-6 col-xs-6 add__onItem">
                                                                    + {{ $dataval }} </div>
                                                                <i class="col-md-3 col-xs-3 text-center"></i>
                                                                <p class="col-md-3 col-xs-3 text-right mb0"></p>
                                                            </div>
                                                        @endforeach
                                                    @endif

                                                @endif
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                                @php
                                    if(Auth::user()) {
                                        $curSymbol = Auth::user()->restaurant->currency_symbol;
                                    } else {
                                        $curSymbol = config('constants.currency');
                                    }
                                @endphp
                                <div class="clearfix"></div>
                                <div class="panel_order_subtotal bor-top">
                                    <div class="w_310 ch-m-bott pull-md-right pull-xs-right op-color">
                                        <div class="row ">
                                            <p class="col-md-6 col-xs-6 text-right">&nbsp;</p>
                                            <p style="padding-right:70px " class="col-md-3 col-xs-3 text-right">Subtotal<b>:</b>
                                            </p>
                                            <p class="col-md-3 col-xs-3 text-truncate text-right">
                                                {{ $curSymbol.$orderData->order_amount }}</p>
                                        </div>
                                        @if($orderData->order_type =='delivery')
                                            <div class="row ">
                                                <p class="col-md-6 col-xs-6 text-right">&nbsp;</p>
                                                <p style="padding-right:70px " class="col-md-3 col-xs-3 text-right">
                                                    Delivery Charge<b>:</b></p>
                                                <p class="col-md-3 col-xs-3 text-truncate text-right">
                                                    {{ $curSymbol.$orderData->delivery_charge }}</p>
                                            </div>
                                        @endif
                                        <div class="row ">
                                            <p class="col-md-6 col-xs-6 text-right">&nbsp;</p>
                                            <p style="padding-right:70px " class="col-md-3 col-xs-3 text-right">
                                                Tax<b>:</b></p>
                                            <p class="col-md-3 col-xs-3 text-truncate text-right">
                                                {{ $curSymbol.$orderData->tax }}</p>
                                        </div>
                                        @if($orderData->tip_amount!=0.00)
                                            <div class="row ">
                                                <p class="col-md-6 col-xs-6 text-right">&nbsp;</p>
                                                <p style="padding-right:70px " class="col-md-3 col-xs-3 text-right">
                                                    Tip<b>:</b></p>
                                                <p class="col-md-3 col-xs-3 text-truncate text-right">
                                                    {{ $curSymbol.$orderData->tip_amount }}</p>
                                            </div>
                                        @endif
                                        <div class="row">
                                            <p class="col-md-6 col-xs-6 text-right">&nbsp;</p>
                                            <p style="padding-right:70px "
                                               class="col-md-3 col-xs-3 text-right text-bold">TOTAL<b>:</b></p>
                                            <p class="col-md-3 col-xs-3 text-truncate text-right text-bold">
                                                {{ $curSymbol.$orderData->total_amount }}</p>
                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>

                                {{--<div class="col-md-12 txt_instructions font_an_medium _fs14"><b>+</b> Please do not pack any napkins or plasticware<br> <b>+</b> cssc</div>--}}
                                @if($orderData->user_comments!="")
                                    <div class="special__otherInformation">
                                        <div class="title_order_des cust"><span>Special instructions:-</span></div>
                                        <div>
                                            <span><?php echo nl2br($orderData->user_comments); ?></span>
                                        </div>
                                    </div>
                                @endif

                                <div class="clearfix"></div>
                                @if($orderData->restaurants_comments!="")
                                    <div class="cancel-preview" style="display: block;">
                                        <p>Cancellation Reason:</p>
                                        <div><?php echo nl2br($orderData->restaurants_comments); ?></div>
                                    </div>
                                @endif

                                <div class="clearfix"></div>
                                <div id="errormess" style="display:none;">
                                </div>

                            </div>
                            <div class="col-xs-1 col-md-1 hide-sm"></div>
                        </div>

                    </div>

                    <div class="footer_orderpopup reservation_det">

                        {{--<div class="run-behind">Order is Running Behind The Time</div>--}}

                        <div class="archive-msg"></div>
                        <div class="row hidden-print">
                            <div class="change-time-popup text-center" id="timeslot" style="display:none;margin: 20px 0;border: 1px solid #ebebeb;
    padding: 50px 0;">
                                <!--   <a href="javascript:void(0);" class="a_nd_close discard-btn">+</a>-->
                                <p style="font-size: 16px;font-weight: 700;">
                                    Change {{ ucfirst($orderData->order_type) }} Time </p>
                                <p class="curr_date" curr_date="2018-06-22 07:13:27" style="display: none;"></p>
                                <div style="margin: 40px 0;">
                                    <input type="hidden" id="slot_order_id" value="{{ $orderData->id }}">
                                    <b btn-id="14876" icon-type="minus_btn" class="icon-change-tym"></b>


                                    <div class="date-time-stamp"><strong>Slot Date:</strong>
                                        <select name="delivery_date" id="delivery_date">
                                            @foreach($result as $rest=>$dat)
                                                <option value="{{ $rest }}" {{($orderData->delivery_date==$rest) ? 'selected' :'' }}>{{ $dat }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="date-time-stamp"><strong>Slot Time:</strong>

                                        <select name="delivery_time" id="delivery_time">
                                            @foreach($timeSlotData as $key=>$value)
                                                <option value="{{ $key }}" {{($orderData->delivery_time==$key) ? 'selected' :'' }}>{{ $value }}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                </div>

                                <a class="btn btn__primary" href="javascript:changeTimeSlot()" id="mange_btn-14876"
                                   class="ubtn   update_time_btn">Update Time</a>

                            </div>
                            <!-- <div id="r_msgContainer_popup" class="col-md-6 col-xs-6 line-height-35"><span id="r_msg_pop14876" class="padding_orderlate _fs14"></span><span id="r_hour_pop14876" class="r_hour_pop"></span><span class="r_seconds_pop" id="r_seconds_pop14876">:</span><span id="r_minutes_pop14876"></span></div> -->
                            <div style="margin-top: 30px" class="col-md-12 col-xs-12 text-center button__container">

                                <div class="cancel-preview <?php echo ($orderData->status =='cancelled')?'':'hide' ;?>" style="display: none;">
                                    <p>Cancellation Reason</p>
                                    <div id="reason"></div>
                                </div>
                                @if(($orderData->status =='placed') || ($orderData->status =='ready') || ($orderData->status =='confirmed'))
                                    @if(($deliveryTimestamp<=$currentTimestamp) && ($action=="food_item"))
                                        <button type="button" class="btn  btn__primary action_btn" data-toggle="modal" onclick="myFunction()">Update Time Slot</button>
                                    @endif
                                    <button type="button" class="btn  btn__reject action_btn" data-toggle="modal" data-target="#rejectOrderModal">Reject Order</button>
                                    <button type="button" class="btn  btn__cancel action_btn" data-toggle="modal" data-target="#cancelOrderModal">Cancel Order</button>

                                @endif
                                <button type="button" class="btn  btn__print" onclick="javascript:printData()">Print</button>

                                @if(($action=="product") || ($deliveryTimestamp>=$currentTimestamp))

                                    @if(($status !="cancelled") || ($status !="rejected"))

                                        @if($status =="confirmed")
                                            <button type="button" class="btn  btn__primary   action_btn" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Confirm' }}</button>
                                            <!--@if($orderData->manual_update==1 && $orderData->order_type=="delivery")
                                                <button type="button" class="btn  btn__primary   action_btn" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Manual Confirm' }}</button>
                                            @else
                                                <button type="button" class="btn  btn__primary   action_btn" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Confirm' }}</button>
                                            @endif -->
                                            <?php
                                            //$title = "Confirm";
                                            //if($orderData->manual_update==1 && $orderData->order_type=="delivery")
                                            //$title = "Manual Confirm"; ?>
                                        @endif
                                        @if($status =="sent")
                                            <button type="button" class="btn  btn__primary   action_btn" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Sent' }}</button>
                                        @endif

                                        @if($status =="ready")
                                            <button type="button" class="btn  btn__primary   action_btn" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Ready' }}</button>
                                        @endif
                                        @if(($status == "archived") && ($orderData->order_type=="carryout"))
                                            <button type="button" class="btn  btn__primary   action_btn" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">Picked Up</button>

                                        @endif

                                        @if(($status == "archiv") && ($orderData->order_type=="carryout"))
                                        <!-- <button type="button" class="btn  btn__print" onclick="javascript:printData()">Print</button>-->
                                        @endif

                                        @if(($status == "archived") && ($orderData->order_type=="delivery"))
                                        <!--<button type="button" class="btn  btn__print" onclick="javascript:printData()">Print</button>-->
                                        @endif
                                    @endif
                                @endif
                                {{--<a href="javascript:void(0)" class="ubtn change-time bg-warning change_tym" id="popUp-order-14876">Change Time</a>--}}
                                {{--<a href="javascript:void(0)" class="ubtn print-btn  " onclick="window.print()">Print</a>--}}
                                {{--<span id="foo-btn-id-14876"><a href="javascript:void(0)" class="ubtn r_order_confirm_to conf-btn " data-ordertype="Takeout" id="popUp-order-14876" style="display: none;">Confirm</a></span></div>--}}


                            </div>

                        </div>


                        <div class="modal modal-popup fade" id="cancelOrderModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="exampleModalLabel">Enter Your Comments</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="reject_order_list">
                                            <input type="hidden" id="reject_order_id" value="{{ $orderData->id }}">
                                            <input type="hidden" id="cancel_status" value="cancelled">
                                            <div class="custom__message-textContainer no-margin">
                                                <textarea placeholder="Reason for Cancellation" maxlength="300" rows="5" id="cancel_order_reason"></textarea>
                                            </div>
                                            <b class="error-canc hide text-hide">Please enter Reason to Cancel Order.</b>
                                        </div>
                                        {{--<div class=" text-center">
                                            <!-- <button type="button" class="btn btn-default  " data-dismiss="modal">Go Back</button> -->
                                            <button type="button" class="btn  btn__cancel"
                                                    onclick="javascript:cancelOrder();">Cancel Order
                                            </button>
                                        </div>--}}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn" onclick="javascript:cancelOrder();">Cancel Order</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- reject model -->
                        <div class="modal modal-popup fade" id="rejectOrderModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="exampleModalLabel">Enter Your Comments</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="reject_order_list">
                                            <input type="hidden" id="reject_order_id" value="{{ $orderData->id }}">
                                            <input type="hidden" id="reject_status" value="rejected">
                                            <div class="custom__message-textContainer no-margin">
                                                <textarea placeholder="Reason for Rejection" maxlength="300" rows="5" id="reject_order_reason"></textarea>
                                            </div>
                                            <b class="error-canc hide text-hide">Please enter Reason to Reject Order.</b>
                                        </div>
                                        {{--<div class=" text-center">
                                            <!-- <button type="button" class="btn btn-default  " data-dismiss="modal">Go Back</button> -->
                                            <button type="button" class="btn  btn__reject"
                                                    onclick="javascript:rejectOrder();">Reject Order
                                            </button>
                                        </div>--}}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn" onclick="javascript:rejectOrder();">Reject Order</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- end -->


                        <div id="r_msgContainer_popup"></div>
                    </div>
                </div>

                <!-- Print order details data -->

                <div style="display:none;">
                    <table class="receipt" style="width:100%;font-family:arial;margin:0 auto;font-size:13px;"
                           cellpadding="0" cellspacing="0" id="printTable">
                        <tr>
                            <td valign="top">
                                <table width="100%">
                                    <tr>
                                        <td align="center" style="border-bottom:1px solid #000;padding:10px 0;">
                                            <p style="margin:10px 0;font-size:14px;">{{$restaurantDetails['restaurant_name']}}
                                                Restaurant</p>
                                            <p style="margin: 0;font-size:14px;">{{$restaurantDetails['address']}} {{ $restaurantDetails['street'] }}
                                                , {{$restaurantDetails['zipcode']}}</p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding:5px 0 3px;text-align:left;">
                                            <p style="font-size:14px;margin:5px 0;">Receipt No.
                                                : {{$orderData->payment_receipt}}</p>
                                            <p style="font-size:14px;margin:5px 0;">Order Id: {{$orderData->id}}</p>
                                            <p style="font-size:14px;margin:5px 0;">Order Type: {{ ucfirst($orderData->order_type) }}</p>
                                            <table width="100%">
                                                <tr>
                                                    <td align="left">
                                                        <p style="font-size:14px;margin:5px 0;">Date
                                                            : <?php echo (new DateTime($orderData->delivery_date))->format('l d M, Y');?></p>
                                                    </td>
                                                    <td align="right">
                                                        <p style="font-size:14px;margin:5px 0;">
                                                            Time: <?php echo (!empty($orderData->delivery_time) && $orderData->delivery_time!='null' && $orderData->delivery_time!=null)?(new DateTime($orderData->delivery_time))->format('h:i A'):'';?></p>


                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px 0;">
                                            <table width="100%" cellspacing="2" cellpadding="0">

                                                <tr>
                                                    <td colspan="3">
                                                        <table class="print_items" width="100%" cellspacing="0"
                                                               cellpadding="0">
                                                            @if(isset($orderDetailData) && count($orderDetailData)>0)
                                                                @foreach($orderDetailData as $oData)

                                                                    <tr>
                                                                        <td>
                                                                            <p style="font-size:14px;margin:5px 0;">{{ $oData->item }}</p>
                                                                        </td>
                                                                        <td align="center" valign="top" width="50">
                                                                            <p style="font-size:14px;margin:5px 0;">{{ $oData->quantity }}</p>
                                                                        </td>
                                                                        <td align="right" valign="top" width="70">
                                                                            <p style="font-size:14px;margin:5px 0;">{{ $curSymbol.$oData->total_item_amt }}</p>
                                                                        </td>
                                                                    </tr>

                                                                    @if(isset($oData->addOnsDetail) && count($oData->addOnsDetail)>0)

                                                                        @if($oData->is_byp==0)
                                                                            @foreach($oData->addOnsDetail as $oAData)

                                                                                <tr style="font-size:11px;line-height:18px;">
                                                                                    <td style="padding-left:10px">
                                                                                        <p style="font-size:11px;margin:-5px 0 0;">
                                                                                            + {{ @$oAData['addons_name'] }}</p>
                                                                                    </td>
                                                                                    <td align="center" valign="top"
                                                                                        width="50">
                                                                                        <!--<p style="font-size:11px;margin:-5px 0 0;"></p>-->
                                                                                    </td>
                                                                                    <td align="right" valign="top"
                                                                                        width="70">
                                                                                        <!--    <p style="font-size:11px;margin:-5px 0 0;"></p>-->
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @endif
                                                                        @if($oData->is_byp==1)
                                                                            @foreach($oData->addOnsDetail as $key=>$dataval)

                                                                                <tr style="font-size:11px;line-height:18px;">
                                                                                    <td style="padding-left:10px">
                                                                                        <p style="font-size:11px;margin:-5px 0 0;">
                                                                                            + {{ $dataval }}</p>
                                                                                    </td>
                                                                                    <td align="center" valign="top"
                                                                                        width="50">
                                                                                        <!--<p style="font-size:11px;margin:-5px 0 0;"></p>-->
                                                                                    </td>
                                                                                    <td align="right" valign="top"
                                                                                        width="70">
                                                                                        <!--    <p style="font-size:11px;margin:-5px 0 0;"></p>-->
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @endif

                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                                <table width="100%">

                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td style="font-size: 14px" align="left" width="90%">Subtotal:</td>
                                                    <td style="font-size: 14px"
                                                        align="right">{{ $curSymbol.$orderData->order_amount}}</td>
                                                </tr>

                                                <!--<tr>
                                                  <td style="font-size: 14px" align="left">Deal Discount:</td>
                                                  <td style="font-size: 14px" align="right">$0.00</td>
                                                </tr>
                                                <tr>
                                                  <td style="font-size: 14px" align="left">Promocode Discount:</td>
                                                  <td style="font-size: 14px" align="right">$0.00</td>
                                                </tr>-->
                                                @if(($orderData->order_type=="delivery") && ($orderData->delivery_charge!=0.00))
                                                    <tr>
                                                        <td style="font-size: 14px" align="left">Delivery Charge:</td>
                                                        <td style="font-size: 14px"
                                                            align="right">{{ $curSymbol.$orderData->delivery_charge}}</td>
                                                    </tr>
                                                @endif

                                                <tr>
                                                    <td style="font-size: 14px" align="left">Tax:</td>
                                                    <td style="font-size: 14px"
                                                        align="right">{{ $curSymbol.$orderData->tax}}</td>
                                                </tr>

                                                @if($orderData->tip_amount!=0.00)
                                                    <tr>
                                                        <td style="font-size: 14px" align="left">Tip:</td>
                                                        <td style="font-size: 14px"
                                                            align="right">{{ $curSymbol.$orderData->tip_amount}}</td>
                                                    </tr>
                                                @endif
                                                <tr>
                                                    <td height="5">&nbsp;</td>

                                                </tr>

                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%"
                                                               style="border-top:1px solid #000;border-bottom:1px solid #000;padding: 10px 0;">
                                                            <tr>
                                                                <td style="font-size: 14px" align="left">Total:</td>
                                                                <td style="font-size: 14px"
                                                                    align="right">{{ $curSymbol.$orderData->total_amount}}</td>

                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 15px">
                                <p style="font-size:14px;margin:5px 0;text-align: center">See You soon!!</p>
                            </td>

                        </tr>
                    </table>

                </div>
                <!-- End Print data -->
            @else
                <div class="row"
                     style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                    No Record Found
                </div>
            @endif

        </div>

    </div>
    <script type="text/javascript">
        $('.navbar-laravel').addClass('hidden-print');
        $('.sidebar').addClass('hidden-print');

        function cancelOrder() {

            var order_id = $("#reject_order_id").val();
            var reason = $("#cancel_order_reason").val();
            var status = $("#cancel_status").val();

            if (order_id) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('#loader').removeClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: '/morders/cancel',
                    data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&reason=' + reason + '&status=' + status,
                    success: function () {
                        $('.action_btn').remove();
                        $('.cancel-preview').removeClass('hide');
                        $('#reason').html(reason);

                        $('#cancelOrderModal').modal('toggle');
                        $('#loader').addClass('hidden');
                        setTimeout(function () {// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 500);

                    }
                });
            }
        }

        function rejectOrder() {

            var order_id = $("#reject_order_id").val();

            var re_reason = $("#reject_order_reason").val();
            var re_status = $("#reject_status").val();


            if (order_id) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('#loader').removeClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: '/morders/cancel',
                    data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&reason=' + re_reason + '&status=' + re_status,
                    success: function () {
                        $('.action_btn').remove();
                        $('.cancel-preview').removeClass('hide');
                        $('#reason').html(reason);
                        $('#rejectOrderModal').modal('toggle');
                        $('#loader').addClass('hidden');
                        setTimeout(function () {// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 500);

                    }
                });
            }
        }

        function doAction(order_id, action) {

            if (order_id && action) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#loader').removeClass('hidden');
                $(".btn__print").hide();
                $.ajax({
                    type: 'PUT',
                    url: '/morders/' + order_id + '/' + action,
                    data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&action=' + action,
                    success: function (data) {
                        $('.action_btn').remove();
                        $('#loader').addClass('hidden');
                        if(data["message"] != undefined){

                            //var div = document.getElementById('errormess');
                            //div.innerHTML = '<span style="color:red;">'+data["message"]+'</span>';
                            //$('#errormess').show();

                            alertbox('Error', data["message"], function (modal) {
                            });
                            setTimeout(function () {
                                location.reload();
                            }, 3000);
                        }
                        else{
                            setTimeout(function () {
                                location.reload();
                            }, 100);
                        }

                    }
                });
            }
        }

        function printData() {

            var divToPrint = document.getElementById("printTable");
            var WindowObject = window.open("", "_blank",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.write(divToPrint.outerHTML);
            //    mainwindow.focus();
            WindowObject.print();
            window.opener.document.focus();

            WindowObject.close();



        }

        function myFunction() {
            var x = document.getElementById("timeslot");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function changeTimeSlot() {

            var order_id = $("#slot_order_id").val();
            var sdate = $("#delivery_date").val();
            var stime = $("#delivery_time").val();

            if (order_id) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('#loader').removeClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: '/order/updatetimeslot',
                    data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&sdate=' + sdate + '&stime=' + stime,
                    success: function () {

                        $('#loader').addClass('hidden');
                        setTimeout(function () {// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 500);

                    }
                });
            }
        }

    </script>

    <!--<script type="text/javascript">

    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#delivery_date').on('change', function () {

            var ajaxurl = '/get_states';

            $('#state_id').find('option').not(':first').remove();

            $.ajax({
                type: 'POST',
                url: '<?php //echo URL::to('/') . "/get_states"; ?>',
                data: '_token = <?php //echo csrf_token() ?>&country_id=' + this.value,
                success: function (data) {
                    if ($.type(data.dataObj) !== 'undefined') {
                        $.each(data.dataObj, function (i, item) {
                            $('#state_id').append('<option value="' + item.id + '">' + item.state + '</option>');
                        });
                    }
                }
            });
        });

    });
</script>-->

@endsection
