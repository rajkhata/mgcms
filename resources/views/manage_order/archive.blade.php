@extends('layouts.app')

@section('content')
  @php
    if(Auth::user()) {
        $curSymbol = Auth::user()->restaurant->currency_symbol;
    } else {
        $curSymbol = config('constants.currency');
    }
  @endphp

<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1><span class="active__order">  {{$orderData->total()}} </span> Archive Order</h1>
    </div>
    {{--<span style="float: right;margin:0px 5px;">({{ $orderData->currentPage() .'/' . $orderData->lastPage() }})</span>&emsp;--}}
    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('manage_orders') }}" class="btn btn__primary">Active Order</a></span>
    <!--<span style="float: right;">({{ $orderData->count() .'/' . $orderData->total() }})</span>-->
  </div>

  <div>

    <div class="active__order__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif
      {!! Form::open(['method'=>'get']) !!}

      {!! Form::close() !!}
      <div>

        @if(isset($orderData) && count($orderData)>0)
        @foreach($orderData as $oData)
        <div class="row guestbook__customer-details-content">

          <div class="row col-md-10 " onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details') }}'">
            <div class="col-md-1 type_of_order" style="margin: auto 0px;">{{ $oData->order_type }} <span class="orderStatus"> {{ $oData->status }}</span></div>
            <div class="col-md-2 quantity_of_order" style="margin: auto 0px;">1 item</div>
            <div class="col-md-1" style="margin: auto 0px;">{{ $curSymbol.$oData->total_amount }}</div>
            <div class="col-md-3" style="margin: auto 0px;"> <?php echo (new DateTime($oData->created_at))->format('M d, Y') ;?> <br> <strong> Order In : <?php echo (new DateTime($oData->created_at))->format('H:i A') ;?> </strong></div>
            <div class="col-md-3" style="margin: auto 0px;"> <?php echo (new DateTime($oData->delivery_date))->format('M d, Y') ;?> <br> <strong> Order Out :{{$oData->delivery_time}} </strong></div>
          </div>
          <div class="row col-md-1" style="margin: auto 0px;">
            {{--<div class="col-md-6">--}}
              {{--<a href="{{ URL::to('user_order/' . $oData->id . '/edit') }}" class="btn  btn__primary">Edit</a>--}}
            {{--</div>--}}
          </div>


        </div>
        @endforeach
        @else
        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
          No Record Found
        </div>
        @endif
      </div>

      @if(isset($orderData) && count($orderData)>0)
      <div style="margin: 0px auto;">
        {{ $orderData->appends($_GET)->links()}}
      </div>
      @endif
    </div>

  </div>
</div>
@endsection
