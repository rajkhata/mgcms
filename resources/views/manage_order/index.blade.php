<?php
  use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
  @php
    if(Auth::user()) {
        $curSymbol = Auth::user()->restaurant->currency_symbol;
    } else {
        $curSymbol = config('constants.currency');
    }
  @endphp
<div class="main__container container__custom">
  <div class="reservation__atto manage__order-atto">
    <div class="reservation__title">
      <h1><span class="active__order">{{$orderData->total()}}</span> Active Orders</h1>
    </div>
    <div class="search__manage-order">
      {!! Form::open(['method'=>'get']) !!}
      <div class="form_field__container">

        <div class="input__field">

          <select name="restaurant_id" id="restaurant_id"
                  class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}">
            <option value="">Select Restaurant</option>
            @foreach($groupRestData as $rest)
              <optgroup label="{{ $rest['restaurant_name'] }}">
                @foreach($rest['branches'] as $branch)
                  <option value="{{ $branch['id'] }}" {{ (isset($searchArr['restaurant_id']) && $searchArr['restaurant_id']==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                @endforeach
              </optgroup>
            @endforeach
          </select>

        </div>

        <div class="input__field">
          <input type="text" name="key_search" id="key_search">
          <label for="searchKey">Search</label>
        </div>

        <div>
          <select name="order_status" id="order_status">
            @foreach($orderStatus as $key=>$value)
              <option value="{{$key}}" <?php if($statusKey==$key) echo "selected";?>>{{ $value }}</option>
            @endforeach
          </select>
        </div>

        <div class="input__field">
          <input type="text" name="from_date" id="from_date">
          <label for="searchKey">Start Date</label>
        </div>

        <div class="input__field">
          <input type="text" name="to_date" id="to_date">
          <label for="searchKey">End Date</label>
        </div>

        <div>
          <button class="btn btn__primary" style="float: right;margin:0px 5px;" type="submit">Search</button>
        </div>

      </div>
      {!! Form::close() !!}
    </div>
  </div>
  <div>

    <div class="active__order__container">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif
      {!! Form::open(['method'=>'get']) !!}

      {!! Form::close() !!}
      <div>

        <div class="guestbook__container box-shadow">
          <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
            <div class="row col-md-10">
              <div class="col-md-2">ID</div>
              <div class="col-md-1">Type</div>
              <div class="col-md-2 ">User Name</div>
              <div class="col-md-2">Restaurant</div>
              <div class="col-md-1">Amount</div>
              <div class="col-md-2">Order In</div>
              <div class="col-md-2">Order Out</div>
              <div class="col-md-1">Status</div>

            </div>

            <div class="row col-md-1">
              <div class="col-md-1">Options</div>
            </div>

          </div>

          @if(isset($orderData) && count($orderData)>0)
          @foreach($orderData as $oData)
          <div class="manage__container">

            <div class="row guestbook__customer-details-content" id="restaurant-order-{{ $oData->id }}" >
              <div class="row col-md-10 manage__row" style="margin: auto 0px;">
                <div class="col-md-2 id" style="margin: auto 0px;" onclick="window.location='{{ URL::to('manage_orders/' . $oData->id . '/details') }}'"><a href="#"><strong>{{ $oData->id .'/'. $oData->payment_receipt .'/'.$oData->stripe_charge_id }}</strong></a></div>
                <div class="col-md-1 type_of_order" style="margin: auto 0px;">{{ ucfirst($oData->order_type) }}</div>
                <div class="col-md-2 " style="margin: auto 0px;"><strong>{{ ucfirst($oData->fname .' '.$oData->lname) }}</strong></div>
                <div class="col-md-2" style="margin: auto 0px;"><strong>{{ ucfirst($oData->restaurant_name) }}</strong></div>
                <div class="col-md-1" style="margin: auto 0px;"><strong>{{ $curSymbol.$oData->total_amount }}</strong></div>

                <div class="col-md-2" style="margin: auto 0px;">
                    <?php $timestamp2 = '';
                    $currentDate = date('Y-m-d H:i:s');
                    $currentTimestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData->restaurant_id, 'datetime' => $currentDate)));
                    $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData->restaurant_id, 'datetime' => $oData->created_at)));
                    echo date('l d M', $timestamp2);?> <br> <strong>Order In
                    : <?php echo date('h:i A', $timestamp2);?></strong>
                </div>
                <div class="col-md-2" style="margin: auto 0px;">
                    <?php $timestamp = '';
                    $timestamp = strtotime($oData->delivery_date . ' ' . $oData->delivery_time . ':00');
                    echo date('l d M', $timestamp);?><br> <strong> Order Out
                    :{{ date('h:i A', $timestamp) }}</strong>

                </div>

                <div class="col-md-1" style="margin: auto 0px;">{{ ucfirst($oData->status) }}</div>

               </div>

              <div class="row col-md-1 manageOrder__ctaBox" style="margin: auto 0px;">

                <!--<div class="cc__details">
                  <a href="#" class="btn btn__primary">CC Details</a>
                </div>-->

                <div class="cc__details">
                  <a href="#" class="btn btn__primary">Log</a>
                </div>

                <!--<div class="delete__manage-delete">
                  <a href="#" class="btn btn__cancel">Delete</a>
                </div>-->

              </div>

            </div>

          </div>
          @endforeach
          @else
          <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
            No Record Found
          </div>
          @endif
        </div>

        @if(isset($orderData) && count($orderData)>0)
        <div style="margin: 0px auto;text-align:center;">
          {{ $orderData->appends($_GET)->links()}}
        </div>
        @endif
      </div>

    </div>
  </div>

  <script type="text/javascript">

      $("#from_date").datepicker({
          showAnim: "slideDown",
          dateFormat: 'dd-mm-yy',
          minDate: -3
      });

      $("#to_date").datepicker({
          showAnim: "slideDown",
          dateFormat: 'dd-mm-yy',
          minDate: 0
      });

  function doAction(order_id,action,orderType) {

    if (order_id && action) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        type: 'PUT',
        url: '/morders/'+order_id+'/'+action,
        data: '_token = <?php echo csrf_token() ?>&oid='+order_id+'&action='+action,
        success: function (data) {
          var innercontent = '';
          var innermsg = '';

          if(action === 'confirmed'){
            innercontent = '<button type="button" class="btn btn__primary  " onclick="javascript:doAction('+order_id+' ,\'delivered\',\''+orderType+'\');"> Ready </button>';
            innermsg = 'Order Confirmed';
            if(orderType === 'Delivery'){
              innercontent = '<button type="button" class="btn btn__primary  " onclick="javascript:doAction(\'+order_id+\' ,\'delivered\',\''+orderType+'\');"> Sent </button>';
            }
          }
          if(action === 'ready'){
            innercontent = '<button type="button" class="btn btn__primary  "   onclick="javascript:doAction('+order_id+' ,\'delivered\',\''+orderType+'\');"> Pick Up </button>';
            innermsg = '';
          }
          if(action === 'delivered'){
            innercontent = '';
            innermsg = '';
            $('#restaurant-order-'+order_id).remove();
          }

          if(innercontent !== ''){
            $('#actionBtn_'+order_id).html(innercontent);
            //$('#actionDsc_'+order_id).html(innermsg);
          }

        }
      });
    }
  }

</script>
@endsection
