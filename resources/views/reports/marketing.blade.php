<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

    <div class="main__container reports-location-page fortitle">
        <div class="row">
            <div class="reservation__atto fullWidth">
                <div class="reservation__title">
                    <h2>Marketing Campaigns: Overview</h2>
                </div>
                <div id="sortHomeDashboard" class="selct-picker-plain margin-left-30 width-120" data-data="{!! $getDashboardDaySort !!}">
                <?php
                    $sort_by = 30;
                    if(Session::has('sort_by')){
                        $sort_by = Session::get('sort_by');
                    }
                    if($dealId =='100000001'){ ?>
                
                    <select onchange="myFunction();" class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/marketing">
                       <option value="30" {{ (isset($sort_by) && $sort_by == 30) ? 'selected' :'' }}>Last 30 Days</option>
                    </select>
               
                <?php }else{ ?>
                
            <select onchange="myFunction();" class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/marketing">
            <option value="2000" {{ (isset($sort_by) && $sort_by == 2000) ? 'selected' :'' }}>Overall</option>
            <option value="180" {{ (isset($sort_by) && $sort_by == 180) ? 'selected' :'' }}>Last 6 Months</option>
            <option value="90" {{ (isset($sort_by) && $sort_by == 90) ? 'selected' :'' }}>Last 3 Months</option>
            <option value="30" {{ (isset($sort_by) && $sort_by == 30) ? 'selected' :'' }}>Last 30 Days</option>
            </select>
                
            <?php } ?>
            </div>
            </div>
            <?php
            $spenConversion = 0;
            if(isset(json_decode($marketingOverviewResArray, true)[0]['spend'])){
                $spend = json_decode($marketingOverviewResArray, true)[0]['spend']; 
                $spenConversion = $currencyRate*$spend;
            }
            ?>
            <div class="fullWidth analysis-area flex-box overflow-visible tablet-flex-wrap-ac flex-align-item-center flex-justify-center account-overview">

                <div class="col-xs-3 no-padding-left margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Cost of running ads in Facebook, Instagram and Google Adwords</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-spend margin-bottom-15 text-color-grey"></div>

                        <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?= CommonFunctions::number_format_short(round($spenConversion),0);?></div>

                        <div class="analysis-label margin-bottom-15">Total Spend</div>
                        {!! $htmlSpendGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total number of times ads was seen</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-view text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($marketingOverviewResArray, true)[0]['views']) ? CommonFunctions::number_format_short(round(json_decode($marketingOverviewResArray, true)[0]['views']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Total Impressions</div>
                        {!! $htmlViewsGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total Number of clicks on ads in a campaign</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-clicks text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($marketingOverviewResArray, true)[0]['clicks']) ? CommonFunctions::number_format_short(round(json_decode($marketingOverviewResArray, true)[0]['clicks']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">CPC</div>
                        {!! $htmlClicksGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total number of orders generated through phone calls from PPC. (condition * phone calls which lasted for more than a minute)</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-Direct-Conversions text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($phoneCallOverviewResArray, true)[0]['@Phone_Call_Conversions']) ? number_format(round(json_decode($phoneCallOverviewResArray, true)[0]['@Phone_Call_Conversions']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Direct Conversions</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

                <div class="col-xs-3 no-padding-right margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">E-commerce transactions (orders/reservations) made on  website</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-Indirect-Conversions text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($marketingOverviewResArray, true)[0]['transactions']) ? CommonFunctions::number_format_short(round(json_decode($marketingOverviewResArray, true)[0]['transactions']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Indirect Conversions</div>
                        {!! $htmlConversionsGrowthRate !!}
                    </div>
                </div>

            </div>
        </div>



        <div class="row">
            <div class="col-xs-12 col-md-6 no-padding-left margin-top-40 tablet-no-padding show-tablet-100">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total number of impressions & conversions generated for selected period</div>
                            </div>
                            <!--tooltip -->
                            Impressions and Conversions Analysis
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="impConversionsAnalysis" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 no-padding-right margin-top-40 tablet-no-padding show-tablet-100">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">

                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Distribution of media spend for selected period</div>
                            </div>
                            <!--tooltip -->
                            Spend Analysis
                        </h2>
 
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="spendsAnalysis" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="fullWidth margin-top-40 tablet-no-padding most_loyal_customer">
                <div class="reservation__atto fullWidth ">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Detailed report of campaign performance</div>
                            </div>
                            <!--tooltip -->
                            Campaign Report
                        </h2>
                    </div>

                    {{--<a href="#" class="font-weight-700 font-size-16">More</a>--}}
                </div>

                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-15">

                    <div class="row above-mobile">
                        <div class="col-xs-12 padding-top-bottom-15 content-head margin-bottom-10">
                            <!-- <div class="col-xs-4 col-md-5"> -->
                            <div class='row'>
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">&nbsp;</div>
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">Campaign</div>
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">Channel</div>
                            <!-- </div> -->

                            <!-- <div class="col-xs-2 no-padding-right"> -->
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">Spend</div>
                            <!-- </div> -->

                            <!-- <div class="col-xs-6 col-md-5 no-padding text-center"> -->
                                <!-- <div class="col-xs-2">Spend</div> -->
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">Impressions</div>
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">Clicks</div>
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">CTR</div>
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">CPC</div>
                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">DirectConversion</div>
                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">InDirectConversion</div>
                            <!-- </div> -->
                            </div>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row content-row1">
                                    <div class="col-xs-12 no-padding-left tablet-no-padding font-weight">Awareness</div>
                                </div>
                                <div class="content-row1">
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">&nbsp;</span>&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign1</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">&nbsp;</span>&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign2</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign3</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row content-row1">
                                    <div class=" col-xs-12 no-padding-left tablet-no-padding font-weight">Conversion</div>
                                </div>
                                <div class="content-row1">
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">&nbsp;</span>&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign1</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">&nbsp;</span>&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign2</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign3</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row content-row1">
                                    <div class="col-xs-12 no-padding-left tablet-no-padding font-weight">Retention</div>
                                </div>
                                <div class="content-row1">
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">&nbsp;</span>&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign1</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">&nbsp;</span>&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign2</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign3</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row content-row1">
                                    <div class="col-xs-12 no-padding-left tablet-no-padding font-weight">Evangelical</div>
                                </div>
                                <div class="content-row1">
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">&nbsp;</span>&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign1</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">&nbsp;</span>&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign2</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12">&nbsp;</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Campaign</span>Campaign3</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Channel</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Spend</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Impressions</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">Clicks</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CTR</span>--</div>
                                        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-12"><span class="mobile-tiles">CPC</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">DirectConversion</span>--</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12"><span class="mobile-tiles">InDirectConversion</span>--</div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    <!-- @if(isset($campaignReportRes) && count($campaignReportRes)>0)
                        @foreach($campaignReportRes as $item)
                            <div class="col-xs-12 content-row1">
                                <div class="col-xs-4 col-md-5">
                                    <div class="col-xs-6 show-tablet-100 no-padding-left tablet-no-padding">{{ $item-> Campaing }}</div>
                                    <div class="col-xs-6 show-tablet-100 no-padding-left">{{ $item-> channel }}</div>
                                </div>

                                <div class="col-xs-2">{{ date('d-m-Y', strtotime($item->start_date)) }}</div>

                                <div class="col-xs-6 col-md-5 no-padding text-center">
                                    <div class="col-xs-6">{{ $item-> impressions }}</div>
                                    <div class="col-xs-6">{{ $item-> clicks }}</div>
                                    {{--<div class="col-xs-3">-</div>
                                    <div class="col-xs-3">32</div>--}}
                                </div>
                                {{--<div class="col-xs-2 text-right"><a href="#">View</a></div>--}}
                            </div>
                        @endforeach
                    @else
                        <div class="col-xs-12 text-center padding-top-bottom-20">
                            No Record Found
                        </div>
                    @endif -->


                    {{--<div class="col-xs-12 content-row1">
                        <div class="col-xs-3 col-md-4">
                            <div class="col-xs-6 show-tablet-100 no-padding-left tablet-no-padding">Display-Conversion</div>
                            <div class="col-xs-6 show-tablet-100 no-padding-left">Adwords</div>
                        </div>

                        <div class="col-xs-2">10/4/2018</div>

                        <div class="col-xs-5 col-md-4 no-padding text-center">
                            <div class="col-xs-3">738</div>
                            <div class="col-xs-3">64</div>
                            <div class="col-xs-3">-</div>
                            <div class="col-xs-3">32</div>
                        </div>
                        <div class="col-xs-2 text-right"><a href="#">View</a></div>
                    </div>--}}



                </div>
            </div>
        </div>


        <div class="row negative-margin-lr">
            <div class="col-xs-12 col-md-6 margin-top-40 show-tablet-100">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Distribution of new customers versus returning attributed to marketing</div>
                            </div>
                            <!--tooltip -->
                            New Visitors VS Re-Marketing
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="newVisitorsVSReMarketing" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 margin-top-40 show-tablet-100">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
 
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Distribution of messaging type of marketing efforts</div>
                            </div>
                            <!--tooltip -->
                            Branding VS Conversions VS Re-Marketing
                        </h2>
 
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="brandingVSConversions" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>

            <div class="col-xs-12 col-md-4 margin-top-40 show-tablet-100 hide">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>Desktop VS Devices <span class="text-color-grey font-size-14">(Impressions)</span></h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="desktopVSMobile" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>




    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{asset('js/chartjs-datalabels.js')}}"></script>
    <script src="{{asset('js/reports.js')}}"></script>

    <script>
        var currencySymbol = "{!! $currencySymbol !!}"
    </script>

    <script>

        var newVisitorsVSReMarketing = document.getElementById("newVisitorsVSReMarketing");
        newVisitorsVSReMarketing.height = 800;

        var newVisitorsVSReMarketingData = <?= $newVisitorsVSRemarkingResArray ?>

        var itemArray = [];
        var countArray = [];

        $.each(newVisitorsVSReMarketingData, function (key, value) {
            itemArray.push(value.VisitorType);
            countArray.push(value.ct_count);
        });

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        var bar_ctx = newVisitorsVSReMarketing.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstanceNewVisitorsVSReMarketing = new Chart(newVisitorsVSReMarketing, {
            type: 'pie',
            data: {
                labels: itemArray,
                datasets: [{
                    backgroundColor: [gradient, "#CECECE"],
                    hoverBackgroundColor: [gradient, "#CECECE"],
                    data: countArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                legend: {
                    //onClick: (e) => e.stopPropagation(),
                    position: "top",
                    labels: legendLabels
                },
                 tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4
                },
                layout: {
                    padding: {
                        top: 20,
                        bottom: 20
                    }
                }
            }
        });

        noDataOnChart(chartInstanceNewVisitorsVSReMarketing, newVisitorsVSReMarketing, countArray, '')

    </script>

    <script>

        var brandingVSConversions = document.getElementById("brandingVSConversions");
        brandingVSConversions.height = 800;

        var brandingVSConversionsData = <?= $brandingVSConversionResArray ?>;

        var itemArray = [];
        var countArray = [];

        $.each(brandingVSConversionsData, function (key, value) {
            itemArray.push(value.CampaignType);
            countArray.push(value.ct_CampaingType);
        });

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        var bar_ctx = brandingVSConversions.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstanceBrandingVSConversions = new Chart(brandingVSConversions, {
            type: 'pie',
            data: {
                labels: itemArray,
                datasets: [{
                    backgroundColor: [ gradient, "#CECECE", "#E4BCA4", "#D1B1E0", "#D8E2AC", "#DEA3A0", "#ACDADD"],
                    hoverBackgroundColor: [ gradient, "#CECECE", "#E4BCA4", "#D1B1E0", "#D8E2AC", "#DEA3A0", "#ACDADD"],
                    data: countArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        display: 'auto',
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        }
                    }
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: "top",
                    labels: legendLabels
                },
                 tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4
                },
                layout: {
                    padding: {
                        top: 20,
                        bottom: 20
                    }
                }
            }
        });

        noDataOnChart(chartInstanceBrandingVSConversions, brandingVSConversions, countArray, '')

    </script>

    <script>

        var desktopVSMobile = document.getElementById("desktopVSMobile");
        desktopVSMobile.height = 800;

        var desktopVSMobileData = <?= $desktopVSDeviceResArray ?>

        var itemArray = [];
        var countArray = [];

        $.each(desktopVSMobileData, function (key, value) {
            itemArray.push(value.DeviceType);
            countArray.push(value.Impressions);
        });

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        var bar_ctx = desktopVSMobile.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstanceDesktopVSMobile = new Chart(desktopVSMobile, {
            type: 'pie',
            data: {
                labels: itemArray,
                datasets: [{
                    backgroundColor: [ gradient, "#CECECE", "#E4BCA4", "#D1B1E0", "#D8E2AC", "#DEA3A0", "#ACDADD"],
                    hoverBackgroundColor: [ gradient, "#CECECE", "#E4BCA4", "#D1B1E0", "#D8E2AC", "#DEA3A0", "#ACDADD"],
                    data: countArray
                }]
            },
            options: {
                responsive: true,
                plugins: {
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        display: 'auto',
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        }
                    }
                },
                legend: {
                    //onClick: (e) => e.stopPropagation(),
                    position: "top",
                    labels: legendLabels,
                    formatter: function(value, context){

                    }
                },
                layout: {
                    padding: {
                        top: 20,
                        bottom: 20
                    }
                }
            }
        });

        noDataOnChart(chartInstanceDesktopVSMobile, desktopVSMobile, countArray, '')

    </script>


    <script>

        var impConversionsAnalysis = document.getElementById("impConversionsAnalysis");
        impConversionsAnalysis.height = 800;


        var impConversionsAnalysisData = <?= $impConversionsAnalysisResArray ?>

        var dayArray = [];
        var conversionsArray = [];
        var impressionsArray = [];

        $.each(impConversionsAnalysisData, function (key, value) {
            dayArray.push(value.day);
            conversionsArray.push(value.Conversions);
            impressionsArray.push(value.Impressions);
        });

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 20;
        };

        var chartInstanceImpConversionsAnalysis = new Chart(impConversionsAnalysis, {
            type: 'line',
            data: {
                labels: dayArray,
                datasets: [{
                    label: "Impressions",
                    //fillColor : gradient,
                    backgroundColor: "#3e7685",
                    pointBackgroundColor: '#3e7685',
                    borderWidth: 3,
                    pointRadius: 3,
                    borderColor: '#3e7685',
                    pointHighlightStroke: "#ff6c23",
                    data: impressionsArray,
                    fill: false,
                    yAxisID: 'y-axis-1'
                },{
                    label: "Conversions",
                    backgroundColor: "#DE5356",
                    pointBackgroundColor: '#DE5356',
                    borderWidth: 3,
                    pointRadius: 3,
                    borderColor: '#DE5356',
                    pointHighlightStroke: "#ff6c23",
                    data: conversionsArray,
                    fill: false,
                    yAxisID: 'y-axis-2'
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontColor: fontColor
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        id: 'y-axis-1',
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            fontColor: fontColor,
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                            // Include a dollar sign in the ticks
                            /*callback: function(value, index, values) {
                                return currencySymbol + value;
                            }*/
                        }
                    },{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        display: true,
                        id: 'y-axis-2',
                        position: "right",
                        ticks: {
                            beginAtZero: true,
                            fontColor: fontColor
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: true,
                    labels: legendLabels
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                }
            }
        });

        noDataOnChart(chartInstanceImpConversionsAnalysis, impConversionsAnalysis, impressionsArray, conversionsArray)

    </script>

    <script>

        var spendsAnalysis = document.getElementById("spendsAnalysis");
        spendsAnalysis.height = 800;

        var getDashboardDaySort = <?= $getDashboardDaySort?>;
        var spendsAnalysisData = <?= $spendAvgSpendResArray ?>;
        var conversionRate = <?= $currencyRate ?>;        
        var dayArray = [];
        var spendArray = [];
        var avgSpendArray = [];

        $.each(spendsAnalysisData, function (key, value) {
            
            dayArray.push(value.day);
 
            spenValue = (conversionRate*value.Spend).toFixed(2);
            spendArray.push(spenValue);
 
            avgSpendArray.push(value.AvgSpend);
        });

        var chartInstanceSpendsAnalysis = new Chart(spendsAnalysis, {
            type: 'line',
            data: {
                labels: dayArray,
                datasets: [{
                    //fillColor : gradient,
                    backgroundColor: "transparent",
                    pointBackgroundColor: '#3e7685',
                    borderWidth: 3,
                    pointRadius: 3,
                    borderColor: '#3e7685',
                    pointHighlightStroke: "#ff6c23",
                    data: spendArray,
                    datalabels: {
                        display: false,
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        /*display: function(context) {
                            console.log(context);
                            return context.dataset.data[context.dataIndex] > 15;
                        },*/
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return currencySymbol + Math.round(value);
                        },
                        clamp: true
                    }
                },{
                    //fillColor : gradient,
                    backgroundColor: "transparent",
                    pointBackgroundColor: '#ff6c23',
                    borderWidth: 1,
                    borderColor: '#ff6c23',
                    pointHighlightStroke: "#ff6c23",
                    data: avgSpendArray,
                    pointStyle: 'rectRot',
                    pointRadius: 0,
                    datalabels: {
                        display: true,
                        align: 'left',
                        anchor: 'start',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            if(context.dataIndex == (avgSpendArray.length-1)){
 
                                return 'Budget: \n       '+ currencySymbol + Math.round(value);
 
                            } else {
                                return "";
                            }
                        }
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontColor: fontColor
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontColor: fontColor
                            // Include a dollar sign in the ticks
                            /*callback: function(value, index, values) {
                                return currencySymbol + value;
                            }*/
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartPrimaryColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        beforeFooter: function(tooltipItem, data) {
                            return "Spend : " + currencySymbol + tooltipItem[0].yLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return "Monthly Budget : " + currencySymbol + tooltipItem[1].yLabel;
                        }
                    }
                }
            }
        });

        noDataOnChart(chartInstanceSpendsAnalysis, spendsAnalysis, spendArray, avgSpendArray)

    </script>





@endsection
