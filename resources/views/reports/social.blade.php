<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

    <div class="main__container reports-location-page fortitle">
        <div class="row">
            <div class="reservation__atto fullWidth">
                <div class="reservation__title">
                    <h2>Social Media: Overview</h2>
                </div>
                <div id="sortHomeDashboard" class="selct-picker-plain margin-left-30 width-120" data-data="{!! $getDashboardDaySort !!}">
                <?php if($dealId =='100000001'){ ?>
                
                    <select class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/social">
                       <option value="30">Last 30 Days</option>
                    </select>
                
                <?php }else{ ?>
                 
                     <select class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/social">
                         <option value="2000" selected>Overall</option>
                         <option value="180">Last 6 Months</option>
                         <option value="90">Last 3 Months</option>
                         <option value="30">Last 30 Days</option>
                     </select>
                
                 <?php } ?>
                 </div>
            </div>
            <?php
            $spenConversion = 0;
            if(isset(json_decode($socialMediaOverviewResArray, true)[0])){
                $spend = json_decode($socialMediaOverviewResArray, true)[0]['spend']; 
                $spenConversion = $currencyRate*$spend;
            }
            ?>
            <div class="fullWidth analysis-area flex-box overflow-visible tablet-flex-wrap-ac flex-align-item-center flex-justify-center account-overview">

                <div class="col-xs-3 no-padding-left margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Customers reached by Facebook campaigns</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-total-reach margin-bottom-15 text-color-grey"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($socialMediaOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($socialMediaOverviewResArray, true)[0]['reach']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Total Reach</div>
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Number of times social ads were seen</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-view text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($socialMediaOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($socialMediaOverviewResArray, true)[0]['impressions']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Total Impressions</div>
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total amount spent on running ads in social media ( Facebook, Instagram)</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-spend text-color-grey margin-bottom-15"></div>
 
                        <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?= number_format(round($spenConversion),0);?></div>
 
                        <div class="analysis-label margin-bottom-15">Total Spend</div>
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total number of times users clicked ads in your social marketing</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-clicks text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($socialMediaOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($socialMediaOverviewResArray, true)[0]['clicks']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Clicks</div>
                    </div>
                </div>

                <div class="col-xs-3 no-padding-right margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total Facebook page likes via marketing efforts</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-likes text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($socialMediaOverviewResArray, true)[0]) ? number_format(round(json_decode($socialMediaOverviewResArray, true)[0]['like']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Likes</div>
                    </div>
                </div>

            </div>
        </div>



        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Facebook Page likes over the selected period via marketing efforts</div>
                            </div>
                            <!--tooltip -->
                            Facebook Like Trends
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="facebookLikeTrends" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row hidden">
            <div class="col-xs-12 col-md-6 no-padding-left margin-top-40 tablet-no-padding">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>Post Density</h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="postDensity" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 no-padding-right margin-top-40 tablet-no-padding">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>Post Distribution</h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="postDistribution" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row facebook-stories negative-margin-lr hidden">

            <div class="col-xs-12 margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>Sample Post</h2>
                    </div>
                </div>
            </div>

            <div class="col-xs-6 col-sm-4">
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-15 fb-card">
                    <div class="fullWidth flex-box flex-direction-row margin-bottom-10">
                        <div>
                            <img src="{{asset('images/fb-logo.jpg')}}">
                        </div>
                        <div class="padding-left-right-10">For all of us here at #WentworthSeafood, there... <a href="#">more</a></div>
                    </div>
                    <div class="fullWidth">
                        <img src="{{asset('images/fb-card-img.jpg')}}" class="fullWidth" />
                    </div>
                    <div class="fullWidth margin-top-10">
                        4 People like this
                    </div>
                </div>
            </div>

            <div class="col-xs-6 col-sm-4">
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-15 fb-card">
                    <div class="fullWidth flex-box flex-direction-row margin-bottom-10">
                        <div>
                            <img src="{{asset('images/fb-logo.jpg')}}">
                        </div>
                        <div class="padding-left-right-10">For all of us here at #WentworthSeafood, there... <a href="#">more</a></div>
                    </div>
                    <div class="fullWidth">
                        <img src="{{asset('images/fb-card-img.jpg')}}" class="fullWidth" />
                    </div>
                    <div class="fullWidth margin-top-10">
                        4 People like this
                    </div>
                </div>
            </div>

            <div class="col-xs-6 col-sm-4">
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-15 fb-card">
                    <div class="fullWidth flex-box flex-direction-row margin-bottom-10">
                        <div>
                            <img src="{{asset('images/fb-logo.jpg')}}">
                        </div>
                        <div class="padding-left-right-10">For all of us here at #WentworthSeafood, there... <a href="#">more</a></div>
                    </div>
                    <div class="fullWidth">
                        <img src="{{asset('images/fb-card-img.jpg')}}" class="fullWidth" />
                    </div>
                    <div class="fullWidth margin-top-10">
                        4 People like this
                    </div>
                </div>
            </div>

            <div class="col-xs-6 col-sm-4">
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-15 fb-card">
                    <div class="fullWidth flex-box flex-direction-row margin-bottom-10">
                        <div>
                            <img src="{{asset('images/fb-logo.jpg')}}">
                        </div>
                        <div class="padding-left-right-10">For all of us here at #WentworthSeafood, there... <a href="#">more</a></div>
                    </div>
                    <div class="fullWidth">
                        <img src="{{asset('images/fb-card-img.jpg')}}" class="fullWidth" />
                    </div>
                    <div class="fullWidth margin-top-10">
                        4 People like this
                    </div>
                </div>
            </div>

            <div class="col-xs-6 col-sm-4">
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-15 fb-card">
                    <div class="fullWidth flex-box flex-direction-row margin-bottom-10">
                        <div>
                            <img src="{{asset('images/fb-logo.jpg')}}">
                        </div>
                        <div class="padding-left-right-10">For all of us here at #WentworthSeafood, there... <a href="#">more</a></div>
                    </div>
                    <div class="fullWidth">
                        <img src="{{asset('images/fb-card-img.jpg')}}" class="fullWidth" />
                    </div>
                    <div class="fullWidth margin-top-10">
                        4 People like this
                    </div>
                </div>
            </div>

            <div class="col-xs-6 col-sm-4">
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-15 fb-card">
                    <div class="fullWidth flex-box flex-direction-row margin-bottom-10">
                        <div>
                            <img src="{{asset('images/fb-logo.jpg')}}">
                        </div>
                        <div class="padding-left-right-10">For all of us here at #WentworthSeafood, there... <a href="#">more</a></div>
                    </div>
                    <div class="fullWidth">
                        <img src="{{asset('images/fb-card-img.jpg')}}" class="fullWidth" />
                    </div>
                    <div class="fullWidth margin-top-10">
                        4 People like this
                    </div>
                </div>
            </div>

        </div>





    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{asset('js/chartjs-datalabels.js')}}"></script>
    <script src="{{asset('js/reports.js')}}"></script>

    <script>

        var postDensity = document.getElementById("postDensity");
        postDensity.height = 700;

        var postDensityData = [
            {day:"Mon",value1:"8",value2:"6"},
            {day:"Tue",value1:"6",value2:"6"},
            {day:"Wed",value1:"4",value2:"6"},
            {day:"Thu",value1:"8.5",value2:"6"},
            {day:"Fri",value1:"7.5",value2:"6"},
            {day:"Sat",value1:"6",value2:"6"},
            {day:"Sun",value1:"8.5",value2:"6"}
        ]

        var dayArray = [];
        var value1Array = [];
        var value2Array = [];

        $.each(postDensityData, function (key, value) {
            dayArray.push(value.day);
            value1Array.push(value.value1);
            value2Array.push(value.value2);
        });

        var bar_ctx = postDensity.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstance = new Chart(postDensity, {
            type: 'bar',
            data: {
                labels: dayArray,
                datasets: [{
                    backgroundColor: gradient,
                    pointBackgroundColor: '#B89DCA',
                    pointHighlightStroke: "#ff6c23",
                    data: value1Array
                },{
                    type: 'line',
                    pointRadius: 0,
                    backgroundColor: "#B89DCA",
                    pointBackgroundColor: '#B89DCA',
                    pointHighlightStroke: "#ff6c23",
                    fill: false,
                    data: value2Array
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.7
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding:{
                        top: 25
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                }
            }
        });

    </script>

    <script>

        var postDistribution = document.getElementById("postDistribution");
        postDistribution.height = 700;

        var postDistributionData = [
            {day:"Jan",value1:"2",value2:"6"},
            {day:"Feb",value1:"3",value2:"6"},
            {day:"Mar",value1:"4",value2:"6"},
            {day:"Apr",value1:"4.5",value2:"6"},
            {day:"May",value1:"3.5",value2:"6"},
            {day:"Jun",value1:"2",value2:"6"},
            {day:"Jul",value1:"3.5",value2:"6"},
            {day:"Aug",value1:"1",value2:"6"},
            {day:"Sep",value1:"2",value2:"6"},
            {day:"Oct",value1:"3",value2:"6"},
            {day:"Nov",value1:"2",value2:"6"},
            {day:"Dec",value1:"1",value2:"6"}
        ]

        var dayArray = [];
        var value1Array = [];
        var value2Array = [];

        $.each(postDistributionData, function (key, value) {
            dayArray.push(value.day);
            value1Array.push(value.value1);
            value2Array.push(value.value2);
        });

        var bar_ctx = postDistribution.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstance = new Chart(postDistribution, {
            type: 'bar',
            data: {
                labels: dayArray,
                datasets: [{
                    backgroundColor: "#CECECE",
                    hoverBackgroundColor: "#CECECE",
                    pointBackgroundColor: '#CECECE',
                    pointHighlightStroke: "#ff6c23",
                    data: value1Array
                },{
                    type: 'bar',
                    backgroundColor: gradient,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#1D85FE',
                    pointHighlightStroke: "#ff6c23",
                    data: value2Array
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.7,
                        stacked: true,
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        stacked: true
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding:{
                        top: 25
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                }
            }
        });

    </script>

    <script>

        var facebookLikeTrends = document.getElementById("facebookLikeTrends");
        facebookLikeTrends.height = 600;

        var facebookLikeTrendsData = <?= $likeTrendsOverviewResArray ?>

        var dayArray = [];
        var value1Array = [];

        $.each(facebookLikeTrendsData, function (key, value) {
            dayArray.push(value.day);
            value1Array.push(value.page_like);
        });

        var chartInstanceFacebookLikeTrends = new Chart(facebookLikeTrends, {
            type: 'line',
            data: {
                labels: dayArray,
                datasets: [{
                    //fillColor : gradient,
                    label: "Likes",
                    backgroundColor: "#3A72BF",
                    pointBackgroundColor: '#3A72BF',
                    borderWidth: 3,
                    pointRadius: 5,
                    borderColor: '#3A72BF',
                    pointHighlightStroke: "#ff6c23",
                    data: value1Array,
                    fill: false,
                    datalabels: {
                        //display: false,
                        align: 'start',
                        anchor: 'start',
                        color: fontColor,
                        /*display: function(context) {
                            console.log(context);
                            return context.dataset.data[context.dataIndex] > 15;
                        },*/
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            //return '$' + Math.round(value);
                        }
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor
                            // Include a dollar sign in the ticks
                            /*callback: function(value, index, values) {
                                return '$' + value;
                            }*/
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: true,
                    labels: legendLabels
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                }
            }
        });

        noDataOnChart(chartInstanceFacebookLikeTrends, facebookLikeTrends, value1Array, '')

    </script>


@endsection
