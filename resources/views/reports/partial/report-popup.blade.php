<!-- Modal -->
<div class="modal fade modal-popup" id="viewReports" tabindex="-1" role="dialog" aria-labelledby="viewReportsModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Turnover Time Alert</h4>
            </div>
            <div class="modal-body text-center">
                <div id="reportPopupViewer" class="row"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>

    function openPDFviewer(id){
        var pdfURL = $(id).attr('data-url');
        var pdfTitle = $(id).attr('data-title');
        var pdfViewer = "<iframe id='pdfVieweriFrame' src='"+ pdfURL +"' border='0' class='fullWidth' ></iframe>";

        $("#viewReports #reportPopupViewer").empty().append(pdfViewer);
        $("#viewReports #myModalLabel").empty().append(pdfTitle);
        $("#viewReports").modal('show');
    }

</script>