<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

    <div class="main__container reports-location-page fortitle">
        <div class="row">
            <div class="reservation__atto fullWidth">
                <div class="reservation__title">
                    <h2>Site Visitors: Overview</h2>
                </div>
                <div id="sortHomeDashboard" class="selct-picker-plain margin-left-30 width-120" data-data="{!! $getDashboardDaySort !!}">
                <?php if($dealId =='100000001'){ ?>
                
                    <select class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/visitors">
                       <option value="30">Last 30 Days</option>
                    </select>
                
                <?php }else{ ?>
                
                    <select class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/visitors">
                        <option value="2000" selected>Overall</option>
                        <option value="180">Last 6 Months</option>
                        <option value="90">Last 3 Months</option>
                        <option value="30">Last 30 Days</option>
                    </select>
                
                 <?php } ?>
                </div>
            </div>


            <div class="fullWidth analysis-area flex-box overflow-visible tablet-flex-wrap-ac flex-align-item-center flex-justify-center account-overview">

                <div class="col-xs-3 no-padding-left margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total Users who visit your website</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-website-visitor margin-bottom-15 text-color-grey"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($siteVisitorsOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($siteVisitorsOverviewResArray, true)[0]['Visitors']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Website Visitors</div>
                        {!! $htmlVisitorsGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Users who had not previously visited your website</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-visitors text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($newVisitorsOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($newVisitorsOverviewResArray, true)[0]['new_visitors']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">New Visitors</div>
                        {!! $htmlNewVisitorsGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Users who had previously visited your website</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-returning-visitor text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($returnVisitorsOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($returnVisitorsOverviewResArray, true)[0]['new_visitors']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Returning Visitors</div>
                        {!! $htmlReturnVisitorsGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Average amount of time all users spend on the website</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-current-reservation text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($avgTimeSpentOverviewResArray, true)[0]) ? json_decode($avgTimeSpentOverviewResArray, true)[0]['avgtimespent'] : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Average Time Spent</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

                <div class="col-xs-3 no-padding-right margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">No of sessions per user</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-user1 text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($userPerSessionOverviewResArray, true)[0]) ? number_format(round(json_decode($userPerSessionOverviewResArray, true)[0]['userpersession']),0) : 0 ?></div>
 
                        <div class="analysis-label margin-bottom-15">Sessions Per User</div>
 
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Distribution of new users versus returning users</div>
                            </div>
                            <!--tooltip -->
                            Site Visitors <span class="ddValue"></span>
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="siteVisitors" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">A representation of customers actions on the website</div>
                            </div>
                            <!--tooltip -->
                            Customer Journey Funnel
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="customerJourneyFunnel" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row negative-margin-lr thisisnewcss">
            <div class="col-xs-12 col-md-6 margin-top-40 show-tablet-100">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Distribution of traffic based on user type-new users versus returning users</div>
                            </div>
                            <!--tooltip -->
                            Traffic By Type
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="trafficType" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 margin-top-40 show-tablet-100">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Distribution of traffic coming from different devices (mobile, desktop,tab)</div>
                            </div>
                            <!--tooltip -->
                            Traffic by Device
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="trafficDevice" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 margin-top-40 show-tablet-100 mlr25">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Distributions of traffic coming from different channels(Organic, Direct, Social, Adwords…)</div>
                            </div>
                            <!--tooltip -->
                            Traffic by Channel
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="trafficChannel" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{asset('js/chartjs-datalabels.js')}}"></script>
    <script src="{{asset('js/reports.js')}}"></script>

    <script>

        var trafficChannel = document.getElementById("trafficChannel");
        trafficChannel.height = 800;

        var trafficChannelData = <?= $trafficChannelPieResArray ?>;

        var labelArray = [];
        var valueArray = [];

        $.each(trafficChannelData, function (key, value) {
            labelArray.push(ucfirst(value.channel));
            valueArray.push(value.num_visitors);
        });

        var bar_ctx = trafficChannel.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        var chartInstanceTrafficChannel = new Chart(trafficChannel, {
            type: 'pie',
            data: {
                labels: labelArray,
                datasets: [{
                    backgroundColor: [ gradient, "#E4BCA4", "#D1B1E0", "#CECECE", "#D8E2AC", "#DEA3A0", "#ACDADD"],
                    hoverBackgroundColor: [ gradient, "#E4BCA4", "#D1B1E0", "#CECECE", "#D8E2AC", "#DEA3A0", "#ACDADD"],
                    data: valueArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
                        display:'auto',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        }
                    }
                }]
            },
            options: {
                responsive: true,
                legend: {
                    //onClick: (e) => e.stopPropagation(),
                    position: "top",
                    labels: legendLabels
                },
                 tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4
                },
                layout: {
                    padding: {
                        top: 0,
                        bottom: 20,
                        right:0
                    }
                }
            }
        });

        noDataOnChart(chartInstanceTrafficChannel, trafficChannel, valueArray, '')

    </script>

    <script>

        var trafficType = document.getElementById("trafficType");
        trafficType.height = 800;

        var trafficTypeData = <?= $trafficTypePieResArray ?>;

        var itemArray = [];
        var countArray = [];

        $.each(trafficTypeData, function (key, value) {
            itemArray.push(ucfirst(value.userType));
            countArray.push(value.num_visitors);
        });

        var bar_ctx = trafficType.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        var chartInstanceTrafficType = new Chart(trafficType, {
            type: 'pie',
            data: {
                labels: itemArray,
                datasets: [{
                    backgroundColor: [gradient, "#CECECE"],
                    hoverBackgroundColor: [gradient ,"#CECECE"],
                    data: countArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
 
                        display: 'auto',
 
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                responsive: true,
                legend: {
                    //onClick: (e) => e.stopPropagation(),
                    position: "top",
                    labels: legendLabels
                },
                 tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4
                },
                layout: {
                    padding: {
                        top: 0,
                        bottom: 20,
                        left:0,
                        right:0
                    }
                }
            }
        });

        noDataOnChart(chartInstanceTrafficType, trafficType, countArray, '')

    </script>

    <script>

        var trafficDevice = document.getElementById("trafficDevice");
        trafficDevice.height = 800;

        var trafficDeviceData = <?= $trafficDevicePieResArray ?>;

        var itemArray = [];
        var countArray = [];

        $.each(trafficDeviceData, function (key, value) {
            itemArray.push(ucfirst(value.deviceCategory));
            countArray.push(value.num_visitors);
        });

        var bar_ctx = trafficDevice.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        var chartInstanceTrafficDevice = new Chart(trafficDevice, {
            type: 'pie',
            data: {
                labels: itemArray,
                datasets: [{
                    backgroundColor: [gradient, "#CECECE", "#D1B1E0", "#CECECE"],
                    hoverBackgroundColor: [gradient ,"#CECECE", "#D1B1E0", "#CECECE"],
                    data: countArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
                        display: 'auto',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                responsive: true,
                legend: {
                    //onClick: (e) => e.stopPropagation(),
                    position: "top",
                    labels: legendLabels
                },
                 tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4
                },
                layout: {
                    padding: {
                        top: 0,
                        bottom: 20,
                        left:0,
                        right:0
                    }
                }
            }
        });

        noDataOnChart(chartInstanceTrafficDevice, trafficDevice, countArray, '')

    </script>

    <script>

        var siteVisitors = document.getElementById("siteVisitors");
        siteVisitors.height = 600;
       
        var siteNewVisitorsData = <?= $newVisitorChartResArray ?>;
        var siteOldVisitorsData = <?= $oldVisitorChartResArray ?>;

        var dayArray = [];
        var label1Array = [];
        var label2Array = [];
        
        $.each(siteNewVisitorsData, function (key, value) {          
            dayArray.push(value.day);
            label1Array.push(value.NV);            
        });
        
        $.each(siteOldVisitorsData, function (key, value) { 
            label2Array.push(siteOldVisitorsData[key].NV);
        });
           
       
        var myChartSiteVisitors = new Chart(siteVisitors, {
            type: 'line',
            data: {
                labels: dayArray,
                datasets: [{
                    label: "Returning Visitors",
                    fill: true,
                    backgroundColor: "#AAD6F8",
                    pointBackgroundColor: '#1687F9',
                    borderWidth: 3,
                    pointRadius: 5,
                    borderColor: '#1687F9',
                    pointHighlightStroke: "#ff6c23",
                    data: label2Array,
                },{
                    label: "New Visitors",
                    fill: true,
                    backgroundColor: "#f6f8f7",
                    pointBackgroundColor: '#CCCCCC',
                    borderWidth: 3,
                    pointRadius: 5,
                    borderColor: '#CCCCCC',
                    pointHighlightStroke: "#ff6c23",
                    data: label1Array

                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        stacked: true
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        stacked: true
                    }]
                },
                animation: {
                    duration: 750,
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                legend: {
                    display: true,
                    labels: legendLabels
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                }
            }
        });
               
        noDataOnChart(myChartSiteVisitors, siteVisitors, label1Array, label2Array)

    </script>

    <script>

        function getIndexForDelete(array, value) {
            var l = array.length;
            for (var k = 0; k < l; k++) {
                if (array[k].Label == value) {
                    return k;
                }
            }
            return false;
        }

        var customerJourneyFunnel = document.getElementById("customerJourneyFunnel");
        customerJourneyFunnel.height = 600;


        var finalCJFData = [];
        var customerJourneyFunnelData = <?= $customerJourneyFunnelResArray ?>

        //console.log(customerJourneyFunnelData)


        var chartLabels = ['Baseline','25% Scrolls','Add to Cart','Add to Order','Checkout','Transactions'];

        var cjdfLabel = ""
        $.each(chartLabels, function(key, value){
            $.each(customerJourneyFunnelData, function (k,v) {
                if(value == v['Label']) {
                    finalCJFData.push({
                        label: customerJourneyFunnelData[getIndexForDelete(customerJourneyFunnelData, value)]['Label'],
                        clicks_num: customerJourneyFunnelData[getIndexForDelete(customerJourneyFunnelData, value)]['clicks_num']
                    })
                    cjdfLabel = v['Label']
                }
            })
            if(value != cjdfLabel) {
                finalCJFData.push({
                    label: value,
                    clicks_num: "0"
                })
            }
        })

        //console.log(finalCJFData)

        var finalLabels = [];
        var clickSum1Array = [];
        var clickSum2Array = [];

        $.each(finalCJFData, function (key, value) {
            finalLabels.push(value.label)
            clickSum1Array.push(value.clicks_num);
            clickSum2Array.push(value.clicks_num);
        });

        var bar_ctx = customerJourneyFunnel.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        Chart.NewLegend = Chart.Legend.extend({
            afterFit: function() {
                this.height = this.height + 50;
            },
        });

        var chartInstanceCustomerJourneyFunnel = new Chart(customerJourneyFunnel, {
            type: 'bar',
            data: {
                labels: finalLabels,
                datasets: [
                    {
                    //label: 'Number of Reservations',
                    backgroundColor: gradient,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#B3D86F',
                    pointHighlightStroke: "#ff6c23",
                    data: clickSum1Array,
                    yAxisID: 'y-axis-1',
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        }
                    }

                },{
                    type:"line",
                    //label: 'Revenue',
                    fill: true,
                    backgroundColor: "#E2F6FF",
                    pointBackgroundColor: '#E2F6FF',
                    pointHighlightStroke: "#ff6c23",
                    borderColor: "transparent",
                    borderWidth: 0,
                    pointRadius: 0,
                    data: clickSum2Array,
                    yAxisID: 'y-axis-2',
                    datalabels: {
                        display: false
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            maxRotation: 0,
                            minRotation: 0,
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.6,
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        id: 'y-axis-1',
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    },{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        display: false,
                        id: 'y-axis-2',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false,
                    labels: legendLabels
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 25,
                        bottom: 0
                    }
                },
                tooltips: {
                    mode: 'nearest',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                }
            }
        });

        noDataOnChart(chartInstanceCustomerJourneyFunnel, customerJourneyFunnel, clickSum1Array, clickSum2Array)

    </script>



@endsection
