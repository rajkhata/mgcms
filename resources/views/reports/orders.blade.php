<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

    <div id="tblCustomers" class="main__container reports-location-page fortitle">
        <div class="row">
            <div class="reservation__atto fullWidth">
                <div class="reservation__title">
                    <h2>Orders: Overview <span class="ddValue"></span></h2> 
                </div>
                <div id="sortHomeDashboard" class="selct-picker-plain margin-left-30 width-120" data-data="{!! $getDashboardDaySort !!}">
                    <?php
                    $sort_by = 30;
                    if(Session::has('sort_by')){
                        $sort_by = Session::get('sort_by');
                    }
                    if($dealId =='100000001'){ ?>

                        <select onchange="myFunction();" class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/orders">
                           <option value="30" {{ (isset($sort_by) && $sort_by == 30) ? 'selected' :'' }}>Last 30 Days</option>
                        </select>

                    <?php }else{ ?>
                 
                     <select onchange="myFunction();" class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/orders">
                         <option value="2000" {{ (isset($sort_by) && $sort_by == 2000) ? 'selected' :'' }}>Overall</option>
                         <option value="180" {{ (isset($sort_by) && $sort_by == 180) ? 'selected' :'' }}>Last 6 Months</option>
                         <option value="90" {{ (isset($sort_by) && $sort_by == 90) ? 'selected' :'' }}>Last 3 Months</option>
                         <option value="30" {{ (isset($sort_by) && $sort_by == 30) ? 'selected' :'' }}>Last 30 Days</option>
                     </select>
                 
                    <?php } ?>
                </div>
            </div>


            <div class="fullWidth analysis-area flex-box overflow-visible tablet-flex-wrap-ac flex-align-item-center flex-justify-center account-overview">

                <div class="col-xs-3 no-padding-left margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total number of Delivery orders placed</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-visitors margin-bottom-15 text-color-grey"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($orderOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($orderOverviewResArray, true)[0]['orders_ct']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Total Orders</div>
                        {!! $htmlOnlineOrdersGrowthRate !!}
                    </div>
                </div>

                <!-- <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip
                        <div class="ntooltip">
                            <div class="ttip">Average amount spent each time a customer places an order on a website</div>
                        </div>
                        <!--tooltip
                        <div class="icon-order-enable text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?php //isset(json_decode($totalcoctailsOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($totalcoctailsOverviewResArray, true)[0]['cnt_qty']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Total Cocktails</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div> -->

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip-->
                        <div class="ntooltip">
                            <div class="ttip">Average number of items per online order</div>
                        </div>
                        <!--tooltip-->
                        <div class="icon-total-reach text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?= isset(json_decode($orderOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($orderOverviewResArray, true)[0]['order_avgvalue']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Avg. Price Per Order</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

                 <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total Number of Cocktails </div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-website-visitor text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($totalcoctailsorderOverviewResArray, true)[0]) ? number_format(round(json_decode($totalcoctailsorderOverviewResArray, true)[0]['cnt_quantity']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Total Cocktails</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

                <div class="col-xs-3 no-padding-right margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total revenue generated by online Delivery orders</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-revenue text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?= isset(json_decode($orderRevenueResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($orderRevenueResArray, true)[0]['orderrevenue']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Total Orders Revenue</div>
                        {!! $htmlRevenueGrowthRate !!}
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total number of orders and revenue </div>
                            </div>
                            <!--tooltip -->
                            Orders With Revenue <!--<span class="ddValue"></span>-->    
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="performanceAllLocationsMonthly" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total number of Unsuccessful Orders </div>
                            </div>
                            <!--tooltip -->
                            Unsuccessful Orders <!--<span class="ddValue"></span>-->    
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="cancelledOrderOverview" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total Spirit </div>
                            </div>
                            <!--tooltip -->
                            Total Spirit
                        </h2>
                    </div>
                </div>
                
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="totalspirint" class="fullWidth" width="1100" height="600"></canvas>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-xs-12 col-md-6 no-padding-left margin-top-40 tablet-no-padding">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip 
                            <div class="ntooltip h2">
                                <div class="ttip">Distribution of Delivery orders versus Takeout orders</div>
                            </div>
                            <!--tooltip 
                            Delivery VS Takeout
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="deliveryVsTakeout" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 no-padding-right margin-top-40 tablet-no-padding">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip 
                            <div class="ntooltip h2">
                                <div class="ttip">Distribution of new users versus returning</div>
                            </div>
                            <!--tooltip
                            New VS Returning Visitors
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="newVsReturningVisitors" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Most ordered Cocktails</div>
                            </div>
                            <!--tooltip -->
                            Most Popular Cocktails
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="mostPopularItems" class="fullWidth" width="1100" height="600"></canvas>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total Orders By Age</div>
                            </div>
                            <!--tooltip -->
                            Total Orders By Age
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="orderbyage" class="fullWidth" width="1100" height="600"></canvas>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Cocktails sold by age</div>
                            </div>
                            <!--tooltip -->
                            Cocktails sold by age
                        </h2>
                    </div>
                </div>

                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="agecocktails" class="fullWidth" width="1100" height="600"></canvas>
                </div>
            </div>
        </div>

{{--<a href="javascript:void(0)" id="btnExport">download</a>--}}

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{asset('js/chartjs-datalabels.js')}}"></script>
    <script src="{{asset('js/reports.js')}}"></script>


    {{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
        $("body").on("click", "#btnExport", function () {
            html2canvas($('#tblCustomers')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Table.pdf");
                }
            });
        });
    </script>--}}

    <script>
        var currencySymbol = "{!! $currencySymbol !!}"
    </script>

    <script>

        var deliveryVsTakeout = document.getElementById("deliveryVsTakeout");
        deliveryVsTakeout.height = 700;

        var deliveryVsTakeoutData = <?= $totalageorderOverviewResArray ?>

        var orderTypeArray = [];
        var orderCountArray = [];

        $.each(deliveryVsTakeoutData, function (key, value) {
            orderTypeArray.push(value.order_type);
            orderCountArray.push(value.ord_count);
        })

        var bar_ctx = deliveryVsTakeout.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        var chartInstanceDeliveryVsTakeout = new Chart(deliveryVsTakeout, {
            type: 'pie',
            data: {
                labels: orderTypeArray,
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: [gradient, "#CECECE"],
                    hoverBackgroundColor: [gradient, "#CECECE"],
                    data: orderCountArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                legend: {
                    position: "top",
                    labels: legendLabels
                },
                tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4
                },
                layout: {
                    padding: {
                        top: 0,
                        bottom: 20
                    }
                }
            }
        });

        noDataOnChart(chartInstanceDeliveryVsTakeout, deliveryVsTakeout, orderCountArray, '')

    </script>

    <script>

        var newVsReturningVisitors = document.getElementById("newVsReturningVisitors");
        newVsReturningVisitors.height = 700;

        var newVsReturningVisitorsData = <?= $newVSReturnPieResArray ?>

        var itemArray = [];
        var countArray = [];

        $.each(newVsReturningVisitorsData, function (key, value) {
            itemArray.push(value.User_Type);
            countArray.push(value.ct_usertype);
        });

        var bar_ctx = newVsReturningVisitors.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        var chartInstanceNewVsReturningVisitors = new Chart(newVsReturningVisitors, {
            type: 'pie',
            data: {
                labels: itemArray,
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: [gradient, "#CECECE"],
                    hoverBackgroundColor: [gradient, "#CECECE"],
                    data: countArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                legend: {
                    position: "top",
                    labels: legendLabels
                },
                 tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4
                },
                layout: {
                    padding: {
                        top: 0,
                        bottom: 20
                    }
                }
            }
        });

        noDataOnChart(chartInstanceNewVsReturningVisitors, newVsReturningVisitors, countArray, '')

    </script>

    <script>
        var performanceAllLocationsMonthly = document.getElementById("performanceAllLocationsMonthly");
        performanceAllLocationsMonthly.height = 600;

        var performanceAllLocationsMonthlyData = <?= $totalorderOverviewResArray ?>

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 60;
        };

        var dayArray = [];
        var label1Array = [];
        var label2Array = [];

        $.each(performanceAllLocationsMonthlyData, function (key, value) {
            dayArray.push(value.restaurant_name);
            label1Array.push(value.order_count);            
            label2Array.push(value.order_revenue);
        });

        var bar_ctx = performanceAllLocationsMonthly.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 600, 0);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstancePerformanceAllLocationsMonthly = new Chart(performanceAllLocationsMonthly, {
            type: 'bar',
            data: {
                labels: dayArray,
                datasets: [{
                    label:"Number of Orders",
                    backgroundColor: "#D5DCE2",
                    pointBackgroundColor: '#D5DCE2',
                    pointHighlightStroke: "#ff6c23",
                    data: label1Array,
                    yAxisID: 'y-axis-1',
                },{
                    label:"Revenue",
                    backgroundColor: gradient,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#B3D86F',
                    pointHighlightStroke: "#ff6c23",
                    data: label2Array,
                    yAxisID: 'y-axis-2',
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            var labelHTML = "";
                            if(value != "") {
                                labelHTML = currencySymbol + value
                            }
                            return labelHTML;
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        display: "auto",
                        rotation: -90,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                               /*var labelHTML = "%"
                            return labelHTML;*/
                        },
                        clamp: true
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.6
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        id: 'y-axis-1',
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                            // Include a dollar sign in the ticks
                            /*callback: function(value, index, values) {
                                return currencySymbol + value;
                            }*/
                        }
                    },{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        display: false,
                        id: 'y-axis-2',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return currencySymbol + value;
                            }
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 20,
                        bottom: 0
                    }
                },
                legend: {
                    display: true,
                    labels: legendLabels
                },

                tooltips: {
                    mode: 'index',                               
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartPrimaryColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        beforeFooter: function(tooltipItem, data) {
                            return "Number of Orders : " + tooltipItem[0].yLabel;
                        },
                        footer: function(tooltipItem, data) {
                            //return currencySymbol+tooltipItem[0].yLabel;
                            return "Revenue : " + currencySymbol+tooltipItem[1].yLabel;
                        }
                    }
                }
            }
        });

        noDataOnChart(chartInstancePerformanceAllLocationsMonthly, performanceAllLocationsMonthly, label1Array, label2Array)

    </script>
    <script>
        var cancelledOrderOverview = document.getElementById("cancelledOrderOverview");
        cancelledOrderOverview.height = 600;

        var cancelledOrderOverviewData = <?= $cancelledOrderOverviewResArray ?>

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 60;
        };

        var dayArray = [];
        var label1Array = [];
        var label2Array = [];

        $.each(cancelledOrderOverviewData, function (key, value) {
            dayArray.push(value.restaurant_name);
            label1Array.push(value.refunded_ord);            
            label2Array.push(value.cancelled_ord);
        });

        var bar_ctx = cancelledOrderOverview.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstanceCancelledOrderOverview = new Chart(cancelledOrderOverview, {
            type: 'bar',
            data: {
                labels: dayArray,
                datasets: [{
                    label:"Refund Orders",
                    backgroundColor: "#D5DCE2",
                    pointBackgroundColor: '#D5DCE2',
                    pointHighlightStroke: "#ff6c23",
                    data: label1Array,
                    yAxisID: 'y-axis-1',
                },{
                    label:"Cancel Orders",
                    backgroundColor: gradient,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#B3D86F',
                    pointHighlightStroke: "#ff6c23",
                    data: label2Array,
                    yAxisID: 'y-axis-2',
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            var labelHTML = "";
                            if(value != "") {
                                labelHTML = value
                            }
                            return labelHTML;
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        display: "auto",
                        rotation: -90,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                               /*var labelHTML = "%"
                            return labelHTML;*/
                        },
                        clamp: true
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.6
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        id: 'y-axis-1',
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                            // Include a dollar sign in the ticks
                            /*callback: function(value, index, values) {
                                return currencySymbol + value;
                            }*/
                        }
                    },{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        display: false,
                        id: 'y-axis-2',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return  value;
                            }
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 20,
                        bottom: 0
                    }
                },
                legend: {
                    display: true,
                    labels: legendLabels
                },

                tooltips: {
                    mode: 'index',                               
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartPrimaryColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        beforeFooter: function(tooltipItem, data) {
                            return "Refund Orders : " + tooltipItem[0].yLabel;
                        },
                        footer: function(tooltipItem, data) {
                            //return currencySymbol+tooltipItem[0].yLabel;
                            return "Cancel Orders : " + tooltipItem[1].yLabel;
                        }
                    }
                }
            }
        });

        noDataOnChart(chartInstanceCancelledOrderOverview, cancelledOrderOverview, label1Array, label2Array)

    </script>
    <script>

        var mostPopularItems = document.getElementById("mostPopularItems");
        mostPopularItems.height = 600;

        var mostPopularItemsData = <?=json_encode($totalcoctailsOverviewResArray)?>;

        var itemArray = [];
        var countArray = [];

        $.each(JSON.parse(mostPopularItemsData), function (key, value) {
            itemArray.push(value.menu_name);
            countArray.push(value.cnt_qty);
        });

        var bar_ctx = mostPopularItems.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);

        gradient.addColorStop(0, '#FF0000'); //#21BBFE
        gradient.addColorStop(1, '#1D85FE');

        var chartInstanceMostPopularItems = new Chart(mostPopularItems, {
            type: 'bar',
            data: {
                labels: itemArray,
                datasets: [{
                    label: 'Popular Cocktails',
                    backgroundColor: gradient,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#b3d76e',
                    pointHighlightStroke: "#ff6c23",
                    data: countArray
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.5
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "Most Popular Cocktails";
                        },
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return tooltipItem[0].yLabel;
                        }
                    }
                }
            }
        });
        noDataOnChart(chartInstanceMostPopularItems, mostPopularItems, countArray, '')
    </script>

    <script>
        var orderbyage = document.getElementById("orderbyage");
        orderbyage.height = 600;
        var orderByAgeData = <?=json_encode($totalageorderOverviewResArray)?>;
        var itemArray = [];
        var countArray = [];

        $.each(JSON.parse(orderByAgeData), function (key, value) {
            itemArray.push(value.count_age);
            countArray.push(value.ord_count);
        });

        var bar_ctx = orderbyage.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstanceOrderByAge = new Chart(orderbyage, {
            type: 'bar',
            data: {
                labels: itemArray,
                datasets: [{
                    label: 'Total Orders By Age',
                    backgroundColor: gradient,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#b3d76e',
                    pointHighlightStroke: "#ff6c23",
                    data: countArray
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.5
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "Total Orders By Age";
                        },
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return tooltipItem[0].yLabel;
                        }
                    }
                }
            }
        });
        noDataOnChart(chartInstanceOrderByAge, orderbyage, countArray, '')
    </script>

    <script>
        var agecocktails = document.getElementById("agecocktails");
        agecocktails.height = 600;
        var ageCocktailsData = <?=json_encode($totalagecocktailsOverviewResArray)?>;
        console.log(ageCocktailsData);
        var itemArray = [];
        var countArray = [];
        var colorArray = [];

        $.each(JSON.parse(ageCocktailsData), function (key, value) {
            itemArray.push(value.menu_name);
            countArray.push(value.count_age);
            colorArray.push(value.color_code);
        });
        var bar_ctx = agecocktails.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#FF9E9D');
        gradient.addColorStop(1, '#1D85FE');
      
        var chartInstanceageCocktails = new Chart(agecocktails, {
            type: 'bar',
            data: {
                 labels: itemArray,
                 datasets: [{
                    label: 'Cocktails sold by Age',
                    backgroundColor: colorArray,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#b3d76e',
                    pointHighlightStroke: "#ff6c23",
                    data: countArray
                },]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "Cocktails Sold By Age";
                        },
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return tooltipItem[0].yLabel;
                        }
                    }
                }
            }
        });
        noDataOnChart(chartInstanceageCocktails, agecocktails, countArray, colorArray)
    </script>

<script>
        var totalspirint = document.getElementById("totalspirint");
        totalspirint.height = 600;
        var totalSpirintData = <?=json_encode($totalspirintOverviewResArray)?>;
        var itemArray = [];
        var countArray = [];

        $.each(JSON.parse(totalSpirintData), function (key, value) {
            itemArray.push(value.modifier);
            countArray.push(value.spirint_cnt);
        });

        var bar_ctx = totalspirint.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#FF9E9D');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstancetotalSpirint = new Chart(totalspirint, {
            type: 'bar',
            data: {
                labels: itemArray,
                datasets: [{
                    label: 'Total Spirint',
                    backgroundColor: gradient,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#b3d76e',
                    pointHighlightStroke: "#ff6c23",
                    data: countArray
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.5
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "Total Spirits";
                        },
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return tooltipItem[0].yLabel;
                        }
                    }
                }
            }
        });
        noDataOnChart(chartInstanceageCocktails, agecocktails, countArray, '')
    </script>   
@endsection