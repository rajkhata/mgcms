@extends('layouts.app')

@section('content')
    <div class="main__container fortitle">

        <div class="row">
            <div id="reportsSection" class="col-sm-12 col-md-10 col-lg-8 margin-top-10 no-padding">

                <div class="col-sm-6 no-padding-left">
                    <div class="row">
                        <h4>{{$data['month']}} {{$data['year']}} Report</h4>
                        <p class="font-weight-700">Click the below "Download Report" button to download the latest report.</p>
                    </div>
                    <div class="row">
                        <a href="{{url('report/download').'?year='.$data['year'].'&deal_id='.$data['deal_id'].'&restaurant_name='.$data['restaurant_name'].'&month='.$data['month']}}" class="btn_ btn__primary" target="_blank">Download Report</a>
                        {{--<a href="javascript:openPDFviewer('#latestReport')" id="latestReport" data-title="{{$data['month']}} {{$data['year']}} Report" data-url="{{url('report/download').'?year='.$data['year'].'&deal_id='.$data['deal_id'].'&restaurant_name='.$data['restaurant_name'].'&month='.$data['month']}}" class="btn_ btn__primary">Download Report</a>--}}
                    </div>
                </div>

                <div class="col-sm-6 no-padding-right">
                    <div class="row">

                        <h4>Archive</h4>
                        <p class="font-weight-700">Select the Month & Year for which you would like to download the report.</p>

                        <div class="row">
                            <div class="selct-picker-plain col-sm-5 no-padding-left">
                                <select id="sel_month" data-style="no-background-with-buttonline no-padding-left" class="selectpicker reservation-book">
                                    <option value=''>Month</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07" >July</option>
                                
                                </select>
                            </div>

                            <div class="selct-picker-plain col-sm-5">
                                <select   id="sel_year" data-style="no-background-with-buttonline no-padding-left" class="selectpicker reservation-book yearchoose">
                                    <option value=''>Year</option>   

                                     <?php 

                                        while($data['report_from_year']<=$data['year']){
                                            echo ' <option  value="'.$data['report_from_year'].'">'.$data['report_from_year'].'</option>';
                                            $data['report_from_year']++;

                                        }
                                     ?>

                                   
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="row margin-top-30">
                        <a href="javascrit:void(0)" class="btn_ btn__holo hide downloadmonthwise download_report" target="_blank">Download Report</a>
                        {{--<a href="javascript:openPDFviewer('#archiveReport')" data-title="July 2018 Report" id="archiveReport" class="btn_ btn__primary hide downloadmonthwise download_report">Download Report</a>--}}
                            @if ($errors->any())

                                <div id="reportError" class="alert alert-danger">
                                  
                                        @foreach ($errors->all() as $error)
                                            <p>                    
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
                                      {{ $error }}
                                            </p>
                                        @endforeach
                                    
                                </div>
                                 
                            @endif
                    </div>
                </div>

            </div>
        </div>

    </div>
<script type="text/javascript">

        var months =[
        {
        month: "01",
        monthText: "January" 
        }, {
        month: "02",
        monthText: "February" 
        }, {
        month: "03",
        monthText: "March" 
        }, {
        month: "04",
        monthText: "April" 
        }, {
        month: "05",
        monthText: "May" 
        }, {
        month: "06",
        monthText: "June" 
        }, {
        month: "07",
        monthText: "July" 
        }, {
        month: "08",
        monthText: "August" 
        }, {
        month: "09",
        monthText: "September" 
        }, {
        month: "10",
        monthText: "October" 
        }, {
        month: "11",
        monthText: "November" 
        }, {
        month: "12",
        monthText: "December" 
        }
        ];

        function getMonthName(month){
           
           var str='';

            $.each(months, function( index, value ) {
              if(month==value.month){
             str= value.monthText.toLowerCase();                
              }
                  
            });

            return str;

        }
    
    function getMonthdropDown(uptomonth,month){
      

        var str='<option value="">Month</option>';
        $.each(months, function( index, value ) {
          console.log( value.month ==uptomonth);
          var selectcheck='';

          if(month==value.month){
            selectcheck='selected=selected';
          }

          str+=' <option value="'+value.month+'"    '+selectcheck+'>'+value.monthText+'</option>';
                if(value.month ==uptomonth) {
                    $('#sel_month').html(str);
                   $('#sel_month').selectpicker('refresh')

                return false; 
                }
              
        });

       $('#sel_month').html(str);
        $('#sel_month').selectpicker('refresh')

        
    }
    $('#sel_year').change(function(){
        var curyear="{{$data['year']}}";
        var curmonth="{{$data['month_n']}}";
        var month= $('#sel_month').val();

        var selectyear=$(this).val();
       
        if(curyear!=selectyear){

         getMonthdropDown('12',month);
        }else{
            var upto='0'+((curmonth-1).toString());

        getMonthdropDown(upto,month);
        }

        $('#reportError.alert').remove()


    });


    function getReportLink(){

        var year=$('#sel_year').val();
        var month=$('#sel_month').val();
        if(year!='' && month!=''){
        
            var url="{{url('report/download')}}";
            var deal_id="{{$data['deal_id']}}";
            var deal_id_separator="{{$data['deal_id_separator']}}";
            var restaurant_name="{{$data['restaurant_name']}}";
            var link=url+'?year='+year+'&deal_id='+deal_id+'&month='+getMonthName(month)+'&restaurant_name='+restaurant_name;

            $('.downloadmonthwise').attr('href',link);
            $('.downloadmonthwise').removeClass('hide');
        }else{

             $('.downloadmonthwise').addClass('hide');
        }

        
    }


     $('#sel_year,#sel_month').change(function(){
           getReportLink();

    });



</script>


    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/reports.css')}}" />
    {{--@include("reports.partial.report-popup")--}}
@endsection