@extends('layouts.app')

@section('content')
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

    <div class="main__container reports-location-page fortitle">
        <div class="row">
            <div class="reservation__atto fullWidth">
                <div class="reservation__title">
                    <h2>Reservation: Overview</h2>
                </div>

                <div id="sortHomeDashboard" class="selct-picker-plain margin-left-30 width-120" data-data="{!! $getDashboardDaySort !!}">
                    <select class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/reservations">
                        <option value="2000" selected>Overall</option>
                        <option value="180">Last 6 Months</option>
                        <option value="90">Last 3 Months</option>
                        <option value="30">Last 30 Days</option>
                    </select>
                </div>
            </div>


            <div class="fullWidth analysis-area flex-box overflow-visible tablet-flex-wrap-ac flex-align-item-center flex-justify-center account-overview">

                <div class="col-xs-3 no-padding-left margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <div class="icon-visitors margin-bottom-15 text-color-grey"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($reservationsOverviewResArray, true)[0]) ? number_format(round(json_decode($reservationsOverviewResArray, true)[0]['orders_ct']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Online Reservations</div>
                        {!! $htmlOnlineReservationsGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <div class="icon-order-enable text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?= isset(json_decode($reservationsOverviewResArray, true)[0]) ? number_format(round(json_decode($reservationsOverviewResArray, true)[0]['order_avgvalue']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Avg. Reservation Value</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <div class="icon-catering text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($avgCoverPerReservationOverviewResArray, true)[0]) ? number_format(round(json_decode($avgCoverPerReservationOverviewResArray, true)[0]['avg_cover']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Avg. Cover Per Reservation</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <div class="icon-contact-us text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($phoneCallReservationOverviewResArray, true)[0]) ? number_format(round(json_decode($phoneCallReservationOverviewResArray, true)[0]['@Reservation_PhoneCall']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Phone Reservations</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

                <div class="col-xs-3 no-padding-right margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <div class="icon-revenue text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?= isset(json_decode($reservationsOverviewResArray, true)[0]) ? number_format(round(json_decode($reservationsOverviewResArray, true)[0]['order_revenue']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Online Reservations Revenue</div>
                        {!! $htmlRevenueGrowthRate !!}
                    </div>
                </div>

            </div>
        </div>



        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>Reservation <span class="ddValue"></span></h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="reservationMonthly" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6 no-padding-left margin-top-40 tablet-no-padding">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>New VS Returning Customers</h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="newVSReturningCustomers" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 no-padding-right margin-top-40 tablet-no-padding">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>Status of Reservations</h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="statusReservations" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="fullWidth margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>Popular Time Slots</h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30">
                    <canvas id="timeSlots" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>




    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{asset('js/chartjs-datalabels.js')}}"></script>
    <script src="{{asset('js/reports.js')}}"></script>

    <script>
        var currencySymbol = "{!! $currencySymbol !!}"
    </script>

    <script>

        var newVSReturningCustomers = document.getElementById("newVSReturningCustomers");
        newVSReturningCustomers.height = 700;

        var newVSReturningCustomersData = <?= $newVSReturningResvResArray ?>

        var itemArray = [];
        var countArray = [];

        $.each(newVSReturningCustomersData, function (key, value) {
            itemArray.push(value.User_Type);
            countArray.push(value.ct_usertype);
        });

        var bar_ctx = newVSReturningCustomers.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstanceNewVSReturningCustomers = new Chart(newVSReturningCustomers, {
            type: 'pie',
            data: {
                labels: itemArray,
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: [gradient, "#CECECE"],
                    hoverBackgroundColor: [gradient, "#CECECE"],
                    data: countArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                legend: {
                    //onClick: (e) => e.stopPropagation(),
                    position: "left",
                    labels: legendLabels
                },
                layout: {
                    padding: {
                        top: 20,
                        bottom: 20
                    }
                }
            }
        });

        noDataOnChart(chartInstanceNewVSReturningCustomers, newVSReturningCustomers, countArray, '')

    </script>

    <script>

        var statusReservations = document.getElementById("statusReservations");
        statusReservations.height = 700;

        var statusReservationsData = <?= $reservationStatusResArray ?>;

        var itemArray = [];
        var countArray = [];

        $.each(statusReservationsData, function (key, value) {
            itemArray.push(ucfirst(value.status));
            countArray.push(value.ct_status);
        });

        var bar_ctx = statusReservations.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        var chartInstanceStatusReservations = new Chart(statusReservations, {
            type: 'pie',
            data: {
                labels: itemArray,
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: [ gradient, "#E4BCA4", "#D1B1E0", "#CECECE", "#D8E2AC", "#DEA3A0", "#ACDADD"],
                    hoverBackgroundColor: [ gradient, "#E4BCA4", "#D1B1E0", "#CECECE", "#D8E2AC", "#DEA3A0", "#ACDADD"],
                    data: countArray,
                    datalabels:{
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        display: 'auto',
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        }
                    }
                }]
            },
            options: {
                responsive: true,
                legend: {
                    //onClick: (e) => e.stopPropagation(),
                    position: "left",
                    labels: legendLabels,
                    formatter: function(value, context){

                    }
                },
                layout: {
                    padding: {
                        top: 20,
                        bottom: 20
                    }
                }
            }
        });

        noDataOnChart(chartInstanceStatusReservations, statusReservations, countArray, '')

    </script>

    <script>

        var timeSlots = document.getElementById("timeSlots");
        timeSlots.height = 600;

        var timeSlotsData = <?= $reservationTimeSlotResArray ?>

        var growthArray = [];
        var timeArray = [];

        $.each(timeSlotsData, function (key, value) {
            growthArray.push(value.ct_date);
            timeArray.push(value.time_slot);
        });

        var chartInstanceTimeSlots = new Chart(timeSlots, {
            type: 'line',
            data: {
                labels: timeArray,
                datasets: [{
                    //fillColor : gradient,
                    backgroundColor: "transparent",
                    pointBackgroundColor: '#3e7685',
                    borderWidth: 3,
                    pointRadius: 5,
                    borderColor: '#3e7685',
                    pointHighlightStroke: "#ff6c23",
                    data: growthArray,
                    datalabels: {
                        align: 'end',
                        anchor: 'end'
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                layout: {
                    padding: {
                        left: 15,
                        right: 15,
                        top: 25,
                        bottom: 0
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                //return value+"%"
                                return (value / this.max * 100).toFixed(0) + '%'; // convert it to percentage
                            }
                        }
                    }]
                },
                plugins: {
                    datalabels: {
                        display: true
                    }
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return currencySymbol +tooltipItems.yLabel;
                        }
                    }
                }}
        });

        noDataOnChart(chartInstanceTimeSlots, timeSlots, growthArray, '')

    </script>

    <script>

        var reservationMonthly = document.getElementById("reservationMonthly");
        reservationMonthly.height = 600;

        var reservationMonthlyData = <?= $reservationVsRevenueResArray ?>

        var dayArray = [];
        var label1Array = [];
        var label2Array = [];

        $.each(reservationMonthlyData, function (key, value) {
            dayArray.push(value.day);
            label1Array.push(value.orders_ct);
            label2Array.push(value.order_revenue);
        });

        var bar_ctx = reservationMonthly.getContext('2d');
        var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
        gradient.addColorStop(0, '#21BBFE');
        gradient.addColorStop(1, '#1D85FE');

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 60;
        };

        var chartInstanceReservationMonthly = new Chart(reservationMonthly, {
            type: 'bar',
            data: {
                labels: dayArray,
                datasets: [{
                    label: 'Number of Reservations',
                    backgroundColor: "#D5DCE2",
                    pointBackgroundColor: '#D5DCE2',
                    pointHighlightStroke: "#ff6c23",
                    data: label1Array,
                    yAxisID: 'y-axis-1',
                },{
                    label: 'Revenue',
                    backgroundColor: gradient,
                    hoverBackgroundColor: gradient,
                    pointBackgroundColor: '#B3D86F',
                    pointHighlightStroke: "#ff6c23",
                    data: label2Array,
                    yAxisID: 'y-axis-2',
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            var labelHTML = "";
                            if(value != "") {
                                labelHTML = currencySymbol + value
                            }
                            return labelHTML;
                        },
                        clamp: true
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        display: "auto",
                        rotation: -90,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            /*var labelHTML = "%"
                         return labelHTML;*/
                        },
                        clamp: true
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.6
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        id: 'y-axis-1',
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                            // Include a dollar sign in the ticks
                            /*callback: function(value, index, values) {
                                return currencySymbol + value;
                            }*/
                        }
                    },{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        display: false,
                        id: 'y-axis-2',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return currencySymbol + value;
                            }
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: true,
                    labels: legendLabels
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 25,
                        bottom: 0
                    }
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        beforeFooter: function(tooltipItem, data) {
                            return "Number of Reservations : " + tooltipItem[0].yLabel;
                        },
                        footer: function(tooltipItem, data) {
                            //return currencySymbol+tooltipItem[0].yLabel;
                            return "Revenue : " + currencySymbol+tooltipItem[1].yLabel;
                        }
                    }
                }
            }
        });

        noDataOnChart(chartInstanceReservationMonthly, reservationMonthly, label1Array, label2Array)

    </script>



@endsection