<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

    <div class="main__container reports-location-page fortitle">
        <div class="row">
            <div class="reservation__atto fullWidth">
                <div class="reservation__title">
                    <h2>Reports</h2>
                </div>
                
                <div id="sortHomeDashboard" class="selct-picker-plain margin-left-30 width-120" data-data="{!! $getDashboardDaySort !!}">
                    <?php
                    $sort_by = 30;
                    if(Session::has('sort_by')){
                        $sort_by = Session::get('sort_by');
                    }
                    if($dealId =='100000001'){ ?>

                        <select onchange="myFunction();" class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/locations">
                           <option value="30" {{ (isset($sort_by) && $sort_by == 30) ? 'selected' :'' }}>Last 30 Days</option>
                        </select>

                    <?php }else{ ?>
                    
                        <select onchange="myFunction();" class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="report/locations">
                            <option value="2000" {{ (isset($sort_by) && $sort_by == 2000) ? 'selected' :'' }}>Overall</option>
                            <option value="180" {{ (isset($sort_by) && $sort_by == 180) ? 'selected' :'' }}>Last 6 Months</option>
                            <option value="90" {{ (isset($sort_by) && $sort_by == 90) ? 'selected' :'' }}>Last 3 Months</option>
                            <option value="30" {{ (isset($sort_by) && $sort_by == 30) ? 'selected' :'' }}>Last 30 Days</option>
                        </select>
                    
                    <?php } ?>
                </div>
            </div>


            <div class="fullWidth analysis-area flex-box overflow-visible tablet-flex-wrap-ac flex-align-item-center flex-justify-center account-overview">

                <div class="col-xs-3 no-padding-left margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Cost of running ads in Facebook, Instagram and Google Adwords for all locations</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-spend margin-bottom-15 text-color-grey"></div>
                        <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?= isset(json_decode($locationsOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($locationsOverviewResArray, true)[0]['spend']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Spend</div>
                        {!! $htmlSpendGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">The total impressions for all the locations</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-view text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($locationsOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($locationsOverviewResArray, true)[0]['views']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Impressions</div>
                        {!! $htmlViewsGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total Number of clicks on ads in a campaign</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-clicks text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($locationsOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($locationsOverviewResArray, true)[0]['clicks']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Clicks</div>
                        {!! $htmlClicksGrowthRate !!}
                    </div>
                </div>

                <div class="col-xs-3 margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">Total number of orders generated through phone calls from PPC. (condition * phone calls which lasted for more than a minute)</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-contact-us text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($phoneCallOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($phoneCallOverviewResArray, true)[0]['phoneCall']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Phone Calls</div>
                        <div class="text-color-green font-weight-600">&nbsp;</div>
                    </div>
                </div>

                <div class="col-xs-3 no-padding-right margin-top-20">
                    <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                        <!--tooltip -->
                        <div class="ntooltip">
                            <div class="ttip">E-commerce transactions (orders/reservations) made on  website</div>
                        </div>
                        <!--tooltip -->
                        <div class="icon-conversion text-color-grey margin-bottom-15"></div>
                        <div class="analysis-amt font-weight-800"><?= isset(json_decode($locationsOverviewResArray, true)[0]) ? CommonFunctions::number_format_short(round(json_decode($locationsOverviewResArray, true)[0]['transactions']),0) : 0 ?></div>
                        <div class="analysis-label margin-bottom-15">Conversions</div>
                        {!! $htmlConversionsGrowthRate !!}
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total revenue generated by all the locations</div>
                            </div>
                            <!--tooltip -->
                            Performance of All Locations - All Time
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="performanceAllLocationsTime" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total revenue generated by all the locations over the last 30 days</div>
                            </div>
                            <!--tooltip -->
                            Performance of All Locations - Last 30 Days
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="performanceAllLocationsMonthly" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 no-padding margin-top-40">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total revenue generated by all locations plotted against monthly subscription</div>
                            </div>
                            <!--tooltip -->
                            Revenue Trends
                        </h2>
                    </div>

                    {{--<a href="#" class="font-weight-700 font-size-16">More</a>--}}
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="revenueTrends" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 no-padding-right margin-top-40 tablet-no-padding">
                <div class="reservation__atto fullWidth">
                    <div class="reservation__title">
                        <h2>
                            <!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Monthly comparative analysis of impressions and traffic for all locations</div>
                            </div>
                            <!--tooltip -->
                            Impressions Vs Traffic
                        </h2>
                    </div>

                    {{--<a href="#" class="font-weight-700 font-size-16">More</a>--}}
                </div>
                <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                    <canvas id="visitorsTrends" class="fullWidth" width="1500" height="400"></canvas>
                </div>
            </div>
        </div>



    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{asset('js/chartjs-datalabels.js')}}"></script>
    <script src="{{asset('js/reports.js')}}"></script>

    <script>
        var currencySymbol = "{!! $currencySymbol !!}"
    </script>

    <script>

        var visitorsTrends = document.getElementById("visitorsTrends");
        visitorsTrends.height = 600;

        var visitorsTrendsData = <?= $visitorTrendsResArray ?>

        var dayArray = [];
        var impressionArray = [];
        var trafficArray = [];

        $.each(visitorsTrendsData, function (key, value) {
            dayArray.push(value.day);
            impressionArray.push(value.impression);
            trafficArray.push(value.traffic);
        });

        var chartInstanceVisitorsTrends = new Chart(visitorsTrends, {
            type: 'line',
            data: {
                labels: dayArray,
                datasets: [{
                    label: "Impressions",
                    //fillColor : gradient,
                    backgroundColor: "#3e7685",
                    pointBackgroundColor: '#3e7685',
                    borderWidth: 3,
                    pointRadius: 5,
                    borderColor: '#3e7685',
                    pointHighlightStroke: "#ff6c23",
                    data: impressionArray,
                    fill: false,
                    yAxisID: 'y-axis-1'
                },{
                    label: "Traffic",
                    backgroundColor: "#DE5356",
                    pointBackgroundColor: '#DE5356',
                    borderWidth: 3,
                    pointRadius: 5,
                    borderColor: '#DE5356',
                    pointHighlightStroke: "#ff6c23",
                    data: trafficArray,
                    fill: false,
                    yAxisID: 'y-axis-2'
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        id: 'y-axis-1',
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: "#3e7685",
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                            // Include a dollar sign in the ticks
                            /*callback: function(value, index, values) {
                                return currencySymbol + value;
                            }*/
                        }
                    },{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        display: true,
                        id: 'y-axis-2',
                        position: "right",
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: "#DE5356"
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: true,
                    labels: legendLabels
                },
                tooltips: {
                     mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4
                }
            }
        });

        noDataOnChart(chartInstanceVisitorsTrends, visitorsTrends, impressionArray, trafficArray)

    </script>

    <script>

        var revenueTrends = document.getElementById("revenueTrends");
        revenueTrends.height = 600;

        var revenueTrendsData = <?= $revenueTrendsResArray ?>;
        var revenueTrendsPackageData = <?= $revenueTrendsPackageResArray ?>;

        var finalArrayData = []

        $.each(revenueTrendsData, function (key, value) {
            finalArrayData.push({
                day: value.day,
                revenue: value.revenue,
                package: revenueTrendsPackageData[0].package.toString()
            })
        })

        var dayArray = [];
        var revenueArray = [];
        var packageArray = [];


        $.each(finalArrayData, function (key, value) {
            dayArray.push(value.day);
            revenueArray.push(value.revenue);
            packageArray.push(value.package);
        });

        var chartInstanceRevenueTrends = new Chart(revenueTrends, {
            type: 'line',
            data: {
                labels: dayArray,
                datasets: [{
                    //fillColor : gradient,
                    backgroundColor: "transparent",
                    pointBackgroundColor: '#3e7685',
                    borderWidth: 3,
                    pointRadius: 5,
                    borderColor: '#3e7685',
                    pointHighlightStroke: "#ff6c23",
                    data: revenueArray,
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        //display: 'auto',
                        /*display: function(context) {
                            console.log(context);
                            return context.dataset.data[context.dataIndex] > 15;
                        },*/
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return currencySymbol + Math.round(value);
                        },
                        clamp: true
                    }
                },{
                    //fillColor : gradient,
                    backgroundColor: "transparent",
                    pointBackgroundColor: '#ff6c23',
                    borderWidth: 3,
                    borderColor: '#ff6c23',
                    pointHighlightStroke: "#ff6c23",
                    data: packageArray,
                    pointStyle: 'rectRot',
                    pointRadius: 0,
                    datalabels: {
                        align: 'start',
                        anchor: 'start',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            if(context.dataIndex == (packageArray.length-2)){
                                return 'Monthly Cost:\n      '+ currencySymbol + Math.round(value)+ "\n";
                            } else {
                                return "";
                            }
                        }
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                layout:{
                    padding: {
                        top: 20
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return currencySymbol + value;
                            }
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: 'chartPrimaryColor',
                    footerFontSize: 15,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        beforeFooter: function(tooltipItem, data) {
                            return "Revenue : " + currencySymbol+tooltipItem[0].yLabel;
                        },
                        footer: function(tooltipItem, data) {
                            //return currencySymbol+tooltipItem[0].yLabel;
                            return "Monthly Cost : " + currencySymbol+tooltipItem[1].yLabel;
                        }
                    }
                },
            }
        });

        noDataOnChart(chartInstanceRevenueTrends, revenueTrends, revenueArray, packageArray)

    </script>

    <script>

        var performanceAllLocationsTime = document.getElementById("performanceAllLocationsTime");
        performanceAllLocationsTime.height = 600;

        var performanceAllLocationsTimeData = <?= $performanceAllLocationResArray ?>

        var itemArray = [];
        var countArray = [];

        $.each(performanceAllLocationsTimeData, function (key, value) {
            itemArray.push(value.account)
            countArray.push(value.revenue)
        })

        var chartInstanceAllTime = new Chart(performanceAllLocationsTime, {
            type: 'bar',
            data: {
                labels: itemArray,
                datasets: [{
                    backgroundColor: "#B89DCA",
                    pointBackgroundColor: '#B89DCA',
                    pointHighlightStroke: "#ff6c23",
                    data: countArray
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return currencySymbol + Math.round(value);
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.5
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return currencySymbol + value;
                            }
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding:{
                        top: 25
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                     mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartPrimaryColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        footer: function(tooltipItem, data) {
                            return currencySymbol+tooltipItem[0].yLabel;
                        }
                    }
                },
            }
        });

        noDataOnChart(chartInstanceAllTime, performanceAllLocationsTime, countArray, '')

    </script>

    <script>

        var performanceAllLocationsMonthly = document.getElementById("performanceAllLocationsMonthly");
        performanceAllLocationsMonthly.height = 600;

        var performanceAllLocationsMonthlyData = <?= $performanceAllLocationLast30DaysResArray ?>

        var itemArray = [];
        var countArray = [];
        var growthArray = [];

        $.each(performanceAllLocationsMonthlyData, function (key, value) {
            itemArray.push(value.account);
            countArray.push(value.revenue);
            growthArray.push(value.growth);
        });

        //console.log(countArray)
        //console.log(countArray.allValuesSame())

        var chartInstanceMonthly = new Chart(performanceAllLocationsMonthly, {
            type: 'bar',
            data: {
                labels: itemArray,
                datasets: [{
                    backgroundColor: "#B3D86F",
                    pointBackgroundColor: '#B3D86F',
                    pointHighlightStroke: "#ff6c23",
                    data: countArray
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false,
                        align: 'end',
                        anchor: 'end',
                        color: function(value, context){
                            /*var labelHTMLColor = "black"
                            if (growthArray[value.dataIndex].indexOf('-') == -1) {
                                labelHTMLColor = "green"
                            } else {
                                labelHTMLColor = "red"
                            }

                            return labelHTMLColor;*/
                        },
                        font: {
                            family: 'FontAwesome',
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            /*var labelHTML = ""

                            if (growthArray[context.dataIndex].indexOf('-') == -1) {
                                labelHTML = "\uf062 +"+growthArray[context.dataIndex] + "%"
                            } else {
                                labelHTML = "\uf063 "+ growthArray[context.dataIndex] + "%"
                            }

                            return labelHTML;*/
                        },
                        clamp: true
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        },
                        barPercentage: 0.5
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return currencySymbol + value;
                            }
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                     mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartPrimaryColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        footer: function(tooltipItem, data) {
                            return currencySymbol+tooltipItem[0].yLabel;
                        }
                    }
                    
                },
            }
        });

        noDataOnChart(chartInstanceMonthly, performanceAllLocationsMonthly, countArray, '')



    </script>

@endsection