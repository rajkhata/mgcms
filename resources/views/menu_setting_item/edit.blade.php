@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Menu Settings Item - Edit') }}</h1>

    </div>

  </div>

  <div>

    <div class="card form__container">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit">
          {{ Form::model($menuSettingItem, array('route' => array('menu_setting_item.update', $menuSettingItem->id), 'method' => 'PUT')) }}
          @csrf
          <div class="form-group row form_field__container">
            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
            <div class="col-md-4">
              <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>
                <option value="">Select Restaurant</option>
                @foreach($groupRestData as $rest)
                <optgroup label="{{ $rest['restaurant_name'] }}">
                  @foreach($rest['branches'] as $branch)
                  <option value="{{ $branch['id'] }}" {{ ((old('restaurant_id')==$branch['id']) || (count(old())==0 && isset($menuSettingItem->restaurant_id) && $menuSettingItem->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                  @endforeach
                </optgroup>
                @endforeach
              </select>
              @if ($errors->has('restaurant_id'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('restaurant_id') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Default Language') }}</label>
            <div class="col-md-4">
              <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                <option value="">Select Language</option>
                @foreach($languageData as $lang)
                  <option value="{{ $lang->id }}" {{ ($menuSettingItem->language_id==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                @endforeach
              </select>
              @if ($errors->has('language_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('language_id') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>
            <div class="col-md-4">
              <select name="menu_category_id" id="menu_category_id" class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}" >
                <option value="">Select Category</option>
                @php
                if(session()->has('newCategoryData')){
                  $categoryData = session()->get('newCategoryData');
                }
                @endphp
                @foreach($categoryData as $cat)
                <option value="{{ $cat->id }}" {{ (old('menu_category_id')==$cat->id || (count(old())==0 && $menuSettingItem->menu_category_id==$cat->id)) ? 'selected' :'' }}>{{ $cat->name }}</option>
                @endforeach
              </select>
              @if ($errors->has('menu_category_id'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('menu_category_id') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="menu_setting_category_id" class="col-md-4 col-form-label text-md-right">{{ __('Menu Setting Category') }}</label>
            <div class="col-md-4">
              <select name="menu_setting_category_id" id="menu_setting_category_id" class="form-control {{ $errors->has('menu_setting_category_id') ? ' is-invalid' : '' }}">
                <option value="">Select Menu Setting Category</option>
                @php
                if(session()->has('newMenuSettingCategoryData')){
                  $menuSettingCategoryData = session()->get('newMenuSettingCategoryData');
                }
                @endphp
                @foreach($menuSettingCategoryData as $cat)
                <option value="{{ $cat->id }}" {{ (old('menu_setting_category_id')==$cat->id || (count(old())==0 && isset($menuSettingItem->menu_setting_category_id) && $menuSettingItem->menu_setting_category_id==$cat->id)) ? 'selected' :'' }}>{{ $cat->name }}</option>
                @endforeach
              </select>
              @if ($errors->has('menu_setting_category_id'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('menu_setting_category_id') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
            <div class="col-md-4">
              <div class="input__field">
                <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                name="name" value="{{old('name') ? old('name') : (count(old())==0 && isset($menuSettingItem->name)?$menuSettingItem->name:NULL) }}" required>
              </div>
              @if ($errors->has('name'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
            <div class="col-md-4">
              <div class="input__field">
                <input id="price" type="text" class="{{ $errors->has('price') ? ' is-invalid' : '' }}"
                name="price" value="{{old('price') ? old('price') : (count(old())==0 && isset($menuSettingItem->price)?$menuSettingItem->price:NULL) }}" required>
              </div>
              @if ($errors->has('price'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('price') }}</strong>
              </span>
              @endif
            </div>
          </div>
	  <div class="form-group row form_field__container">
                            <label for="display_order" class="col-md-4 col-form-label text-md-right">{{ __('Display Order') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="display_order" type="text"
                                        class="{{ $errors->has('display_order') ? ' is-invalid' : '' }}"
                                        name="display_order" value="{{ count(old())==0 ? $menuSettingItem->display_order : old('display_order') }}" >
                                </div>
                                @if ($errors->has('display_order'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('display_order') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> 
          <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Sides') }}</label>
            <div class="col-md-8" style="padding-top: 10px;">
              <input type="radio" id="side_flag1" name="side_flag" value="1" {{ (old('side_flag')=='1' || (count(old())==0 && isset($menuSettingItem->side_flag) && $menuSettingItem->side_flag=='1')) ? 'checked' :'' }}> Multiple Sides  &emsp;
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="side_flag0" name="side_flag" value="0" {{ (old('side_flag')=='0' || (count(old())==0 && isset($menuSettingItem->side_flag) && $menuSettingItem->side_flag=='0')) ? 'checked' :'' }}> Single Side
              @if ($errors->has('side_flag'))
              <span class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('side_flag') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="quantity_flag" class="col-md-4 col-form-label text-md-right">{{ __('Quantity') }}</label>
            <div class="col-md-8" style="padding-top: 10px;">
              <input type="radio" id="quantity_flag1" name="quantity_flag" value="1" {{ (old('quantity_flag')=='1' || (count(old())==0 && isset($menuSettingItem->quantity_flag) && $menuSettingItem->quantity_flag=='1')) ? 'checked' :'' }}> Multiple Quantity &emsp;
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="quantity_flag0" name="quantity_flag" value="0" {{ (old('quantity_flag')=='0' || (count(old())==0 && isset($menuSettingItem->quantity_flag) && $menuSettingItem->quantity_flag=='0')) ? 'checked' :'' }}> Single Quantity
              @if ($errors->has('quantity_flag'))
              <span class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('quantity_flag') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
            <div class="col-md-8" style="padding-top: 10px;">
              <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1' || (count(old())==0 && isset($menuSettingItem->status) && $menuSettingItem->status=='1')) ? 'checked' :'' }}> Active
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0' || (count(old())==0 && isset($menuSettingItem->status) && $menuSettingItem->status=='0')) ? 'checked' :''  }}> Inactive
              @if ($errors->has('status'))
              <span class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <br /><br />
          <div class="row mb-0">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn__primary">{{ __('Edit Item') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
</div>

<script type="text/javascript">
$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $("#restaurant_id").change(function() {
    var rest_id = '';
    rest_id = $('#restaurant_id  option:selected').val();
    $('#menu_category_id').find('option').not(':first').remove();
    $('#menu_setting_category_id').find('option').not(':first').remove();
    if(rest_id!='') {
      $.ajax({
        type: 'POST',
        url: '<?php echo URL::to('/')."/get_category"; ?>',
        data: '_token = <?php echo csrf_token() ?>&rid='+rest_id,
        success: function (data) {
          if($.type(data.dataObj)!=='undefined')
          {
            if($.type(data.dataObj.cat)!=='undefined')
            {
              $.each(data.dataObj.cat, function(i, item) {
                $('#menu_category_id').append( '<option value="'+item.id+'">'+ item.name +'</option>' );
              });
            }
          }
        }
      });
    }
  });
  $("#menu_category_id").change(function() {
    var cat_id = '';
    cat_id = $('#menu_category_id option:selected').val();
    $('#menu_setting_category_id').find('option').not(':first').remove();
    if(cat_id!='') {
      $.ajax({
        type: 'POST',
        url: '<?php echo URL::to('/')."/get_menu_setting_category"; ?>',
        data: '_token = <?php echo csrf_token() ?>&cid='+cat_id,
        success: function (data) {
          if($.type(data.dataObj)!=='undefined')
          {
            if($.type(data.dataObj.cat)!=='undefined')
            {
              $.each(data.dataObj.cat, function(i, item) {
                $('#menu_setting_category_id').append( '<option value="'+item.id+'">'+ item.name +'</option>' );
              });
            }
          }
        }
      });
    }
  });
});
</script>

@endsection
