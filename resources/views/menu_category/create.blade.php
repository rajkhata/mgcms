@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Category - Add') }}</h1>

    </div>

  </div>

  <div>

    <div class="card form__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit">
          <form method="POST" action="/menu_category" enctype="multipart/form-data">
            @csrf

            {{--<div class="form-group row form_field__container">
              <label for="parent_restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant *') }}</label>
              <div class="col-md-4">
                <select name="parent_restaurant_id" id="parent_restaurant_id" class="form-control{{ $errors->has('parent_restaurant_id') ? ' is-invalid' : '' }}" required>
                  <option value="">Select Restaurant</option>
                  @foreach($restaurantData as $rest)
                  <option value="{{ $rest->id }}" {{ (old('parent_restaurant_id')==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('parent_restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('parent_restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>--}}
            <div class="form-group row form_field__container">
                  <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
                  <div class="col-md-4">
                      <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                          <option value="">Select Language</option>
                          @foreach($languageData as $lang)
                              <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                          @endforeach
                      </select>
                      @if ($errors->has('language_id'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('language_id') }}</strong>
                </span>
                      @endif
                  </div>
              </div>
            
            <div class="form-group row form_field__container">
              <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant Branch *') }}</label>
              <div class="col-md-4">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>
                  <option value="">Select Restaurant</option>
                  @foreach($groupRestData as $rest)
                  <optgroup label="{{ $rest['restaurant_name'] }}">
                    @foreach($rest['branches'] as $branch)
                    <option value="{{ $branch['id'] }}" {{ (old('restaurant_id') == $branch['id']) ? 'selected' : (isset($selected_rest_id) && $selected_rest_id == $branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                    @endforeach
                  </optgroup>
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>

              <div class="form-group row form_field__container">
                                <label for="product_type"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Product Type') }}</label>
                                <div class="col-md-4">
                                    <select required name="product_type" id="product_type"
                                            class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }}">
                                        <option value="">Select Product Type</option>
                                       
                                            @foreach($product_types as $key=>$val)
                                                <option value="{{ $key }}" {{ (old('product_type')==$key) ? 'selected' :'' }}>{{ $val }}</option>
                                            @endforeach
                                    
                                    </select>
                                    @if ($errors->has('product_type'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name *') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                  name="name" value="{{ old('name') }}" required>
                </div>
                @if ($errors->has('name'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Slug *') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="slug" type="text" class="{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                  name="slug" value="{{ old('slug') }}" required>
                </div>
                @if ($errors->has('slug'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('slug') }}</strong>
                </span>
                @endif
              </div>
            </div>

              <div class="form-group row form_field__container">
                  <label for="pos_id" class="col-md-4 col-form-label text-md-right">{{ __('POS Id (Category)') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}"
                                 name="pos_id" value="{{ old('pos_id') }}" >
                      </div>
                      @if ($errors->has('pos_id'))
                          <span class="invalid-feedback"><strong>{{ $errors->first('pos_id') }}</strong></span>
                      @endif
                  </div>
              </div>

           <div class="form-group row form_field__container">
                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                <div class="col-md-4">
                    <div class="input__field">
 

                       <textarea maxlength="500" rows="4" name="description" id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} editor">
{{ old('description') }}</textarea>
 
                    </div>
                    @if ($errors->has('description'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Sub Description') }}</label>
                <div class="col-md-4">
                    <div class="input__field">

 

                       <textarea maxlength="500" rows="4" name="sub_description" id="sub_description" class="form-control{{ $errors->has('sub_description') ? ' is-invalid' : '' }} editor"
>{{ old('sub_description') }}</textarea>
 
                    </div>
                    @if ($errors->has('sub_description'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('sub_description') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
		<span class="hidden">
            <div class="form-group row form_field__container">
              <label for="image_class" class="col-md-4 col-form-label text-md-right">{{ __('Image Class') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="image_class" type="text" class="{{ $errors->has('image_class') ? ' is-invalid' : '' }}"
                  name="image_class" value="{{ old('image_class') }}" >
                </div>
                @if ($errors->has('image_class'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_class') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container source-image-div image-caption-div">
              <label for="image_svg" class="col-md-4 col-form-label text-md-right">{{ __('Image SVG') }}</label>
              <div class="col-md-4">
                <input id="image_svg" type="file" class="form-control{{ $errors->has('image_svg') ? ' is-invalid' : '' }}"
                value="{{ old('image_svg.0') }}" name="image_svg">
                @if ($errors->has('image_svg'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_svg') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container source-image-div image-caption-div">
              <label for="image_png" class="col-md-4 col-form-label text-md-right">{{ __('Image PNG (Not Selected)') }}</label>
              <div class="col-md-4">
                <input id="image_png" type="file" class="form-control{{ $errors->has('image_png') ? ' is-invalid' : '' }}"
                value="{{ old('image_png') }}" name="image_png">
                @if ($errors->has('image_png'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_png') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container source-image-div image-caption-div">
              <label for="image_png_selected" class="col-md-4 col-form-label text-md-right">{{ __('Image PNG (Selected)') }}</label>
              <div class="col-md-4">
                <input id="image_png_selected" type="file" class="form-control{{ $errors->has('image_png_selected') ? ' is-invalid' : '' }}"
                value="{{ old('image_png_selected') }}" name="image_png_selected">
                @if ($errors->has('image_png_selected'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_png_selected') }}</strong>
                </span>
                @endif
              </div>
            </div>
              <!-- add category attachment -->

              <div class="form-group row form_field__container source-image-div image-caption-div">
                  <label for="cat_attachment" class="col-md-4 col-form-label text-md-right">{{ __('Category Attachment') }}</label>
                  <div class="col-md-4">
                      <input id="cat_attachment" type="file" class="form-control{{ $errors->has('cat_attachment') ? ' is-invalid' : '' }}"
                             value="{{ old('cat_attachment') }}" name="cat_attachment">
                      @if ($errors->has('cat_attachment'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('cat_attachment') }}</strong>
                </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is Popular</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                          <input id="is_popular" type="checkbox" class="" name="is_popular">
                      </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is Favourite</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                      <input id="is_favourite" type="checkbox" class="" name="is_favourite">
                  </div>
              </div>
		</span>
            <!--end here -->

            <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Delivery / Carryout') }}</label>
            <div class="col-md-4" style="padding-top: 10px;">
              <input type="checkbox" id="status1" name="is_delivery"  {{ ((isset($menuCategory->is_delivery) && $menuCategory->is_delivery=='1') || old('is_delivery')=='1') ? 'checked' :'' }}> Delivery
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" id="status0" name="is_carryout"  {{ ((isset($menuCategory->is_carryout) && $menuCategory->is_carryout=='1') || old('is_carryout')=='1') ? 'checked' : '' }}> Carryout
            </div>
          </div>
            
            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-4" style="padding-top: 10px;">
                <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1') ? 'checked' :'' }}> Active
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }}> Inactive
                @if ($errors->has('status'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>
            </div>

             <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Priority') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="priority" type="text" class="{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                  name="priority" value="{{ old('priority') }}" required>
                </div>
                @if ($errors->has('priority'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('priority') }}</strong>
                </span>
                @endif
              </div>
            </div>

              <div class="form-group row form_field__container">
                  <label for="promotional_banner_url" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Url') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="promotional_banner_url" type="text" class="{{ $errors->has('promotional_banner_url') ? ' is-invalid' : '' }}"
                                 name="promotional_banner_url" value="{{ old('promotional_banner_url') }}" >
                      </div>
                      @if ($errors->has('promotional_banner_url'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_url') }}</strong>
                </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="promotional_banner_image" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Image') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="promotional_banner_image" type="file" class="form-control{{ $errors->has('promotional_banner_image') ? ' is-invalid' : '' }}"
                                 value="{{ old('promotional_banner_image') }}" name="promotional_banner_image">
                      </div>
                      @if ($errors->has('promotional_banner_image'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_image') }}</strong>
                </span>
                      @endif
                  </div>
              </div>
              <div class="form-group row form_field__container">
                  <label for="promotional_banner_image_mobile" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Image Mobile') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="promotional_banner_image_mobile" type="file" class="form-control{{ $errors->has('promotional_banner_image_mobile') ? ' is-invalid' : '' }}"
                                 value="{{ old('promotional_banner_image_mobile') }}" name="promotional_banner_image_mobile">
                      </div>
                      @if ($errors->has('promotional_banner_image_mobile'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_image_mobile') }}</strong>
                </span>
                      @endif
                  </div>
              </div>
            <br /><br />
            <div class="row mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">

/*$(document).ready(function() {
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$('#parent_restaurant_id').on('change', function() {
console.log(this.value);
var ajaxurl = '/get_restaurant_locations';
$('#restaurant_id').find('option').not(':first').remove();
$.ajax({
type: 'POST',
url: '<?php echo URL::to('/')."/get_restaurant_locations"; ?>',
data: '_token = <?php echo csrf_token() ?>&rest_id='+this.value,
success: function (data) {
if($.type(data.dataObj)!=='undefined')
{
if($.type(data.dataObj)!=='undefined')
{
$.each(data.dataObj, function(i, item) {
$('#restaurant_id').append('<option value="'+item.id+'">'+ item.restaurant_name +'</option>');
});
}
}
}
});
});
});*/
</script>
<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
       tinymce.init({
           selector: 'textarea.editor',
            menubar: false,
            height: 320,
            theme: 'modern',
            plugins: 'print fullpage importcss  searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help  charmap emoticons',
            image_advtab: true,
	    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment', 	
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });

    });
</script>
@endsection
