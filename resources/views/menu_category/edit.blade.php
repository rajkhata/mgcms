@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Category - Edit') }}</h1>

    </div>

  </div>

  <div>

    <div class="card form__container1">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit width100"> 
	<!--{{ Form::model($menuCategory, array('route' => array('menu_category.update', $menuCategory->id), 'method' => 'PUT', 'files' => true)) }}-->
	<form method="POST" action="/menu_category/{{$menuCategory->id}}" enctype="multipart/form-data">
         <input name="_method" type="hidden" value="PUT">  

          @csrf
          
           <div class="form-group row form_field__container">
                  <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
                  <div class="col-md-4">
                      <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                          <option value="">Select Language</option>
                          @foreach($languageData as $lang)
                              <option value="{{ $lang->id }}" {{ ($languageId==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                          @endforeach
                      </select>
                      @if ($errors->has('language_id'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('language_id') }}</strong>
                </span>
                      @endif
                  </div>
              </div>
          
          <div class="form-group row form_field__container">
            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant Branch *') }}</label>
            <div class="col-md-4">
              <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>                
                @foreach($groupRestData as $rest)
                <optgroup label="{{ $rest['restaurant_name'] }}">
                  @foreach($rest['branches'] as $branch)
                  <option value="{{ $branch['id'] }}" {{ ((old('restaurant_id')==$branch['id']) || (isset($menuCategory->restaurant_id) && $menuCategory->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                  @endforeach
                </optgroup>
                @endforeach
              </select>
              @if ($errors->has('restaurant_id'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('restaurant_id') }}</strong>
              </span>
              @endif
            </div>
          </div>

             <div class="form-group row form_field__container">
                            <label for="product_type"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Product Type') }}</label>
                            <div class="col-md-4">
                                <select name="product_type" id="product_type"
                                        class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }}">
                                    <option value="">Select Product Type</option>
                                    @foreach($product_types as $key=>$val)
                                        <option value="{{ $key }}" {{ (old('product_type')==$key || (count(old())==0 && $menuCategory->product_type==$key)) ? 'selected' :'' }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('product_type'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

          <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name *') }}</label>
            <div class="col-md-4">
              <div class="input__field">
                <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                name="name" value="{{ isset($menuCategory->name)?$menuCategory->name:old('name') }}" required>
              </div>
              @if ($errors->has('name'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Slug *') }}</label>
            <div class="col-md-4">
              <div class="input__field">
                <input id="slug" type="text" class="{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                name="slug" value="{{ isset($menuCategory->slug)?$menuCategory->slug:old('slug') }}" required>
              </div>
              @if ($errors->has('slug'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('slug') }}</strong>
              </span>
              @endif
            </div>
          </div>

            <div class="form-group row form_field__container">
                <label for="pos_id" class="col-md-4 col-form-label text-md-right">{{ __('POS Id (Category)') }}</label>
                <div class="col-md-4">
                    <div class="input__field">
                        <input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}"
                               name="pos_id" value="{{ isset($menuCategory->pos_id) ? $menuCategory->pos_id : old('pos_id') }}" >
                    </div>
                    @if ($errors->has('pos_id'))
                        <span class="invalid-feedback"><strong>{{ $errors->first('pos_id') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
            <div class="col-md-8">
              <div class="input__field">
 

                <textarea maxlength="700" rows="10" name="description" id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} editor"> {{ isset($menuCategory->description)?$menuCategory->description:old('description') }}</textarea>
 

 

              </div>
              @if ($errors->has('description'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
              </span>
              @endif
            </div>
          </div>

           <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Sub Description') }}</label>
            <div class="col-md-8">
              <div class="input__field">
 

                <textarea maxlength="500" rows="4" name="sub_description" id="sub_description" class="form-control{{ $errors->has('sub_description') ? ' is-invalid' : '' }} editor"> {{ isset($menuCategory->sub_description)?$menuCategory->sub_description:old('sub_description') }}</textarea>
 
              </div>
              @if ($errors->has('sub_description'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('sub_description') }}</strong>
              </span>
              @endif
            </div>
          </div>
	 <span class="hidden">
          <div class="form-group row form_field__container">
            <label for="image_class" class="col-md-4 col-form-label text-md-right">{{ __('Image Class') }}</label>
            <div class="col-md-4">
              <div class="input__field">
                <input id="image_class" type="text" class="{{ $errors->has('image_class') ? ' is-invalid' : '' }}"
                name="image_class" value="{{ isset($menuCategory->image_class)?$menuCategory->image_class:old('image_class') }}" >
              </div>
              @if ($errors->has('image_class'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('image_class') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container source-image-div image-caption-div">
            <label for="image_svg" class="col-md-4 col-form-label text-md-right">{{ __('Image SVG') }}</label>
            <div class="col-md-4">
              <input id="image_svg" type="file" class="form-control{{ $errors->has('image_svg') ? ' is-invalid' : '' }}"
              value="{{ old('image_svg') }}" name="image_svg">
              @if ($errors->has('image_svg'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('image_svg') }}</strong>
              </span>
              @endif
            </div>
            @if(isset($menuCategory->thumb_image_med_svg) && $menuCategory->thumb_image_med_svg!='')
            <div class="col-md-4">
              <a data-fancybox="gallery" href="{{url('/').'/..'.$menuCategory->image_svg}}"><img src="{{ url('/').'/..'.$menuCategory->thumb_image_med_svg }}" style="width:120px;"></a>&emsp;
              <input type="checkbox" name="image_svg_delete" value="1" style="margin-left:20px;">Delete
            </div>
            @endif
          </div>

          <div class="form-group row form_field__container source-image-div image-caption-div">
            <label for="image_png" class="col-md-4 col-form-label text-md-right">{{ __('Image PNG') }}</label>
            <div class="col-md-4">
              <input id="image_png" type="file" class="form-control{{ $errors->has('image_png') ? ' is-invalid' : '' }}"
              value="{{ old('image_png') }}" name="image_png">
              @if ($errors->has('image_png'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('image_png') }}</strong>
              </span>
              @endif
            </div>
            @if(isset($menuCategory->thumb_image_med_png) && $menuCategory->thumb_image_med_png!='')
            <div class="col-md-4">
              <a data-fancybox="gallery" href="{{url('/').'/..'.$menuCategory->image_png}}"><img src="{{ url('/').'/..'.$menuCategory->thumb_image_med_png }}" style="width:120px;"></a>&emsp;
              <input type="checkbox" name="image_png_delete" value="1" style="margin-left:20px;">Delete
            </div>
            @endif
          </div>

          <div class="form-group row form_field__container source-image-div image-caption-div">
            <label for="image_png_selected" class="col-md-4 col-form-label text-md-right">{{ __('Image PNG (Selected)') }}</label>
            <div class="col-md-4">
              <input id="image_png_selected" type="file" class="form-control{{ $errors->has('image_png_selected') ? ' is-invalid' : '' }}"
              value="{{ old('image_png_selected') }}" name="image_png_selected">
              @if ($errors->has('image_png_selected'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('image_png_selected') }}</strong>
              </span>
              @endif
            </div>
            @if(isset($menuCategory->thumb_image_med_png_selected) && $menuCategory->thumb_image_med_png_selected!='')
            <div class="col-md-4">
              <a data-fancybox="gallery" href="{{url('/').'/..'.$menuCategory->image_png_selected}}"><img src="{{ url('/').'/..'.$menuCategory->thumb_image_med_png_selected }}" style="width:120px;"></a>&emsp;
              <input type="checkbox" name="image_png_selected_delete" value="1" style="margin-left:20px;">Delete
            </div>
            @endif
          </div>

          <!-- Edit category attachment -->
          <div class="form-group row form_field__container source-image-div image-caption-div">
            <label for="cat_attachment" class="col-md-4 col-form-label text-md-right">{{ __('Category Attachment') }}</label>
            <div class="col-md-4">
              <input id="image_png_selected" type="file" class="form-control{{ $errors->has('cat_attachment') ? ' is-invalid' : '' }}"
                     value="{{ old('cat_attachment') }}" name="cat_attachment">
              @if ($errors->has('cat_attachment'))
                <span class="invalid-feedback">
                <strong>{{ $errors->first('cat_attachment') }}</strong>
              </span>
              @endif
            </div>
            @if(isset($menuCategory->cat_attachment) && $menuCategory->cat_attachment!='')
              <div class="col-md-4">
                <a data-fancybox="gallery" href="{{url('/').$menuCategory->cat_attachment}}">
                <?php
                $extName = substr($menuCategory->cat_attachment, -3);
                ?>
                  @if($extName=="pdf")
                    <img src="{{ url('/images/pdf.png') }}" style="width:30px;">
                    @else
                  <img src="{{ url('/').$menuCategory->cat_attachment }}" style="width:75px;">
                  @endif
                </a>&emsp;
                <input type="checkbox" name="cat_attachment_deleted" value="1" style="margin-left:20px;">Delete
              </div>
            @endif
          </div>
          <div class="form-group row form_field__container">
            <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is Popular</label>
            <div class="col-md-4" style="padding-top: 10px;">
              <input id="is_popular" type="checkbox" class="" name="is_popular" {{ ((isset($menuCategory->is_popular) && $menuCategory->is_popular=='1') || old('is_popular')=='1') ? 'checked' :'' }}>
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is Favourite</label>
            <div class="col-md-4" style="padding-top: 10px;">
              <input id="is_favourite" type="checkbox" class="" name="is_favourite" {{ ((isset($menuCategory->is_favourite) && $menuCategory->is_favourite=='1') || old('is_favourite')=='1') ? 'checked' :'' }}>
            </div>
          </div></span>
          <!-- end here -->
          <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Delivery / Carryout') }}</label>
            <div class="col-md-4" style="padding-top: 10px;">
              <input type="checkbox" id="status1" name="is_delivery"  {{ ((isset($menuCategory->is_delivery) && $menuCategory->is_delivery=='1') || old('is_delivery')=='1') ? 'checked' :'' }}> Delivery
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" id="status0" name="is_carryout"  {{ ((isset($menuCategory->is_carryout) && $menuCategory->is_carryout=='1') || old('is_carryout')=='1') ? 'checked' : '' }}> Carryout
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
            <div class="col-md-4" style="padding-top: 10px;">
              <input type="radio" id="status1" name="status" value="1" {{ ((isset($menuCategory->status) && $menuCategory->status=='1') || old('status')=='1') ? 'checked' :'' }}> Active
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="status0" name="status" value="0" {{ ((isset($menuCategory->status) && $menuCategory->status=='0') || old('status')=='0') ? 'checked' :'' }}> Inactive
              @if ($errors->has('status'))
              <span class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>
          </div>
            

            <div class="form-group row form_field__container">
                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Sort Order') }}</label>
                <div class="col-md-4">
                  <div class="input__field">
                    <input id="priority" type="text" class="{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                    name="priority" value="{{ isset($menuCategory->priority)?$menuCategory->priority:old('priority') }}" required>
                  </div>
                  @if ($errors->has('priority'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('priority') }}</strong>
                  </span>
                  @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="promotional_banner_url" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Url') }}</label>
                <div class="col-md-4">
                    <div class="input__field">
                        <input id="promotional_banner_url" type="text" class="{{ $errors->has('promotional_banner_url') ? ' is-invalid' : '' }}"
                               name="promotional_banner_url" value="{{ isset($menuCategory->promotional_banner_url)?$menuCategory->promotional_banner_url:old('promotional_banner_url') }}" >
                    </div>
                    @if ($errors->has('promotional_banner_url'))
                        <span class="invalid-feedback">
                    <strong>{{ $errors->first('promotional_banner_url') }}</strong>
                  </span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container source-image-div image-caption-div">
                <label for="promotional_banner_image" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Image') }}</label>
                <div class="col-md-4">
                    <input id="promotional_banner_image" type="file" class="form-control{{ $errors->has('promotional_banner_image') ? ' is-invalid' : '' }}"
                           value="{{ old('promotional_banner_image') }}" name="promotional_banner_image">
                    @if ($errors->has('promotional_banner_image'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('promotional_banner_image') }}</strong>
              </span>
                    @endif
                </div>
                @if(isset($menuCategory->promotional_banner_image) && $menuCategory->promotional_banner_image!='')
                    <div class="col-md-4">
                        <a data-fancybox="gallery" href="{{url('/').$menuCategory->promotional_banner_image}}">
                            <?php
                            $extName = substr($menuCategory->promotional_banner_image, -3);
                            ?>
                            @if($extName=="pdf")
                                <img src="{{ url('/images/pdf.png') }}" style="width:30px;">
                            @else
                                <img src="{{ url('/').$menuCategory->promotional_banner_image }}" style="width:75px;">
                            @endif
                        </a>&emsp;
                        <input type="checkbox" name="promotional_banner_image_deleted" value="1" style="margin-left:20px;">Delete
                    </div>
                @endif
            </div>
            <div class="form-group row form_field__container source-image-div image-caption-div">
                <label for="promotional_banner_image_mobile" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Image Mobile') }}</label>
                <div class="col-md-4">
                    <input id="promotional_banner_image_mobile" type="file" class="form-control{{ $errors->has('promotional_banner_image_mobile') ? ' is-invalid' : '' }}"
                           value="{{ old('promotional_banner_image_mobile') }}" name="promotional_banner_image_mobile">
                    @if ($errors->has('promotional_banner_image_mobile'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('promotional_banner_image_mobile') }}</strong>
              </span>
                    @endif
                </div>
                @if(isset($menuCategory->promotional_banner_image_mobile) && $menuCategory->promotional_banner_image_mobile!='')
                    <div class="col-md-4">
                        <a data-fancybox="gallery" href="{{url('/').$menuCategory->promotional_banner_image_mobile}}">
                            <?php
                            $extName = substr($menuCategory->promotional_banner_image_mobile, -3);
                            ?>
                            @if($extName=="pdf")
                                <img src="{{ url('/images/pdf.png') }}" style="width:30px;">
                            @else
                                <img src="{{ url('/').$menuCategory->promotional_banner_image_mobile }}" style="width:75px;">
                            @endif
                        </a>&emsp;
                        <input type="checkbox" name="promotional_banner_image_mobile_deleted" value="1" style="margin-left:20px;">Delete
                    </div>
                @endif
            </div>

            <div class="row margin-top-20 white-box box-shadow padding-top-20 padding-bottom-20 ">
                <div class="col-xs-12 padding-left-right-30">
                    <h4 class="font-weight-700 col-xs-12">
                      <i class="fa fa-angle-right margin-right-10 font-weight-700" aria-hidden="true"></i> 
                      Banners
                      <a href="#" class="position_plus padding-left-right-30" style="float:right"><i class="fa fa-plus"></i></a>
                    </h4>
                </div>

                <div class="col-xs-12 padding-left-right-30 margin-bottom-10 margin-top-10">
                    <fieldset>
                        <div id="position_container">
                            <?php

                            if(isset($menuCategory->promo_banners) && !empty($menuCategory->promo_banners)){

                            $promo_banners = json_decode($menuCategory->promo_banners, 1);
                            // print_r($promo_banners);
                            if(is_array($promo_banners) && count($promo_banners)){
                            foreach ($promo_banners as $key=>$pos){
                            ?>


                            <div class="form-group row form_field__container position_container content__row">


                                <div class="col-md-11 content__imageRow">
                                    <div class="form-group row">
                                        <input type="hidden" class="input__field" name="ID[{{$key}}]"
                                               value="<?php echo @$pos['Id'];?>">
                                        <div class="col-md-4">
                                            <div class="">
                                                <select id="user_group_id" name="UserGroup[{{$key}}]"
                                                        class="form-control">
                                                    <option value="">--Select User Group--</option>
                                                    <?php
                                                    $group_options='';
                                                    foreach ($user_groups as $group){
                                                    $group_options .= '<option value="' . $group->id . '" >' . $group->user_group . ' </option>';

                                                    ?>
                                                    <option value="<?php echo $group->id;?>" <?php if (@$pos[
                                                        'UserGroup']==$group->id){echo
                                                    'selected';}?>><?php echo $group->user_group;?> </option>
                                                    <?php } ?>


                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="">
                                                <input type="text" class="form-control" name="AltText[{{$key}}]"
                                                       value="<?php echo $pos['AltText'];?>" placeholder="Alt Text">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="">
                                                <select class="form-control" name="Position[{{$key}}]">
                                                    <option value="Header" <?php if (isset($pos['Position']) && $pos['Position'] == 'Header') {
                                                        echo 'selected';
                                                    }?>>Header
                                                    </option>
                                                    <option value="Footer" <?php if (isset($pos['Position']) && $pos['Position'] == 'Footer') {
                                                        echo 'selected';
                                                    }?>>Footer
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-4 typeparent">
                                        <div style="padding-top:20px;">&nbsp;</div>
                                            <div class="align-centerflex">
                                                <select class="form-control fieldtype_banner" name="Type[{{$key}}]">
                                                    <option value="Text" <?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                        echo 'selected';
                                                    }?>>Text
                                                    </option>
                                                    <option value="Image" <?php if (isset($pos['Type']) && $pos['Type'] == 'Image') {
                                                        echo 'selected';
                                                    }?>>Image
                                                    </option>
                                                    <option value="Video" <?php if (isset($pos['Type']) && $pos['Type'] == 'Video') {
                                                        echo 'selected';
                                                    }?>>Video
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="align-centerflex">
                                                Web <input type="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                    echo 'text';
                                                } else {
                                                    echo 'file';
                                                }?>" class="form-control" name="Value[{{$key}}]"
                                                           value="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                               echo $pos['Value'];
                                                           }?>" placeholder="Web">
                                                @if(isset($pos['Type']) && $pos['Type']=='Image' && isset($pos['Value'])  && $pos['Value']!='')
                                                    <div class="aligncenter-order1">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value']}}"><span
                                                                    class="glyphicon glyphicon-picture"></span></a>&emsp;
                                                    </div>
                                                @endif
                                                @if(isset($pos['Type']) && $pos['Type']=='Video' && isset($pos['Value'])  && $pos['Value']!='')
                                                    <div class="">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value']}}"></a>&emsp;
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="align-centerflex">
                                                Mobile <input type="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                    echo 'text';
                                                } else {
                                                    echo 'file';
                                                }?>" class="form-control" name="Value_Mobile[{{$key}}]"
                                                              value="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                                  echo $pos['Value_Mobile'];
                                                              }?>" placeholder="Value_Mobile">
                                                @if(isset($pos['Type']) && $pos['Type']=='Image' && isset($pos['Value_Mobile']) && $pos['Value_Mobile']!='')
                                                    <div class="aligncenter-order1">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value_Mobile']}}"><span
                                                                    class="glyphicon glyphicon-picture"></span></a>&emsp;
                                                    </div>
                                                @endif
                                                @if((isset($pos['Type']) && $pos['Type']=='Video') && isset($pos['Value_Mobile']) && $pos['Value_Mobile']!='')
                                                    <div class="">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value_Mobile']}}"></a>&emsp;
                                                    </div>
                                                @endif
                                            </div>
                                        <!--div class="">
                                                <!--select class="form-control" name="Display[{{$key}}]">
                                                    <option value="Web" <?php if (isset($pos['Display']) && $pos['Display'] == 'Web') {
                                            echo 'selected';
                                        }?>>Web
                                                    </option>
                                                    <option value="Mobile" <?php if (isset($pos['Display']) && $pos['Display'] == 'Mobile') {
                                            echo 'selected';
                                        }?>>Mobile
                                                    </option>
                                                    <option value="App" <?php if (isset($pos['Display']) && $pos['Display'] == 'App') {
                                            echo 'selected';
                                        }?>>App
                                                    </option>
                                                </select>
                                            </div-->
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <div class="">
                                                <input type="text" class="form-control" name="Link[{{$key}}]"
                                                       value="<?php echo $pos['Link'];?>" placeholder="Link">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="">
                                                <input type="number" class="form-control" name="SortOrder[{{$key}}]"
                                                       value="<?php echo $pos['SortOrder'];?>" placeholder="Sort Order">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="">
                                                <select class="form-control" name="Status[{{$key}}]">
                                                    <option value="1" <?php if (isset($pos['Status']) && $pos['Status'] == '1') {
                                                        echo 'selected';
                                                    }?>>Active
                                                    </option>
                                                    <option value="0" <?php if (isset($pos['Status']) && $pos['Status'] == '0') {
                                                        echo 'selected';
                                                    }?>>In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1">&nbsp;&nbsp;
                                    <a href="#" class="position_minus">
                                        Delete
                                    </a>
                                </div>

                            </div>


                            <?php
                            }}
                            }
                            ?>

                        </div>
                    </fieldset>
                </div>
            </div>


            <br /><br />
          <div class=" row  mb-0">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
</div>
<?php
$group_options='';
foreach ($user_groups as $group){
    $group_options .= '<option value="' . $group->id . '" >' . $group->user_group . ' </option>';
}

?>
<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', '.position_plus', function (e) {
            var lenofbox = $('.position_container').length;
            e.preventDefault();
            var groups = '<?php echo $group_options;?>';

            // $("#position_container").append('<div class="form-group row form_field__container position_container"> <div class="col-md-2"> <div class=""><input type="text" class="input__field" name="position_type['+lenofbox+']" placeholder="Type"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position['+lenofbox+']" placeholder="position"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position_price['+lenofbox+']" placeholder="price"></div> </div><div class="col-md-2"> <div class=""><input type="checkbox" class="" name="is_selected_position['+lenofbox+']" value="1">Default Position</div> </div> <div class="col-md-2"> <div class=""><input type="file" class="" name="position_image['+lenofbox+']"></div> </div> <div class="col-md-2"> &nbsp;&nbsp; <a href="#" class="position_minus"><i class="fa fa-minus"></i></a> </div> </div>');
            $("#position_container").append('<div class="form-group row form_field__container position_container content__row"><div class="col-md-11 content__imageRow"><div class="form-group row"><div class="col-md-4"><input type="hidden" class="input__field" name="ID[' + lenofbox + ']" value=""> <div class=""><select class="form-control" name="UserGroup[' + lenofbox + ']"> <option value="">--Select User Group--</option> ' + groups + '</select></div> </div><div class="col-md-4"> <div class=""><input type="text" class="form-control" name="AltText[' + lenofbox + ']" placeholder="Alt Text"> </div> </div><div class="col-md-4"> <div class=""><select class="form-control" name="Position[' + lenofbox + ']"> <option value="Header">Header</option> <option value="Footer">Footer</option> </select></div> </div></div><div class="form-group row"><div class="col-md-4 typeparent"> <div style="padding-top:20px;"><select class="form-control fieldtype_banner" name="Type[' + lenofbox + ']"> <option value="Text">Text</option> <option value="Image">Image</option> <option value="Video">Video</option> </select></div> </div><div class="col-md-4"> <div class="">Web<input type="text" class="form-control" name="Value[' + lenofbox + ']" placeholder="Value"></div> </div><div class="col-md-4"> <div class="">Mobile<input type="text" class="form-control" name="Value_Mobile[' + lenofbox + ']" placeholder="Value"></div> </div></div><div class="form-group row"><div class="col-md-4"> <div class=""><input type="text" class="form-control" name="Link[' + lenofbox + ']" placeholder="Link"></div> </div><div class="col-md-4"> <div class=""><input type="number" class="form-control" name="SortOrder[' + lenofbox + ']" placeholder="Sort Order"></div> </div><div class="col-md-4"> <div class=""><select class="form-control" name="Status[' + lenofbox + ']"> <option value="1">Active</option> <option value="0">In Active</option> </select></div> </div></div></div><div class="col-md-1"> &nbsp;&nbsp; <a href="#" class="position_minus">Delete</a></div></div>');

        });

        $(document).on('click', '.position_minus', function (e) {

            e.preventDefault();
            $(this).parents('.position_container').empty().remove();
        });


        $(document).on('change', '.fieldtype_banner', function (e) {
            e.preventDefault();
            var type = $(this).val();
            if (type == 'Text') {
                var nxt =  $(this).parents('.typeparent').next();
                $(this).parents('.typeparent').next().find('input').attr('type', 'text');
                $(nxt).next().find('input').attr('type', 'text');
            } else {
                var nxt =  $(this).parents('.typeparent').next();
                $(this).parents('.typeparent').next().find('input').attr('type', 'file');
                $(nxt).next().find('input').attr('type', 'file');
            }

        });


    });
</script>

<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    
      
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {

	 tinymce.init({
           selector: 'textarea.editor',
            menubar: false,
            height: 320,
            theme: 'modern',
            plugins: 'print  fullpage importcss  searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap emoticons',
            image_advtab: true,
	    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen   | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment', 	
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });

    });
    
    
</script>
@endsection
