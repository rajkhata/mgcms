@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Categories<span style="margin:0px 5px;">({{ $menuCategories->currentPage() .'/' . $menuCategories->lastPage() }})</span>&emsp;</h1>
    </div>
    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('menu_category/create') }}" class="btn btn__primary">Add</a></span>
  </div>

  <div class="addition__functionality">

    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">
      <div class="row form-group col-md-3">
        <select name="restaurant_id" id="restaurant_id" class="form-control" >
          <option value="">Select Restaurant</option>
          @foreach($restaurantData as $rest)
          <option value="{{ $rest->id }}" {{ (isset($restaurant_id) && $restaurant_id==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
          @endforeach
        </select>
      </div>
      <div class="row form-group col-md-5">
        <button class="btn btn__primary" style="margin-left: 5px;" type="submit" >Search</button>
      </div>
    </div>
    {!! Form::close() !!}

  </div>

  <div>

    <div class="active__order__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif



      <div class="guestbook__container box-shadow">
        <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
          <div class="row col-md-9">
            <div class="col-md-3" style="font-weight: bold;">Name</div>
            <div class="col-md-3" style="font-weight: bold;">Restaurant</div>
            <div class="col-md-3" style="font-weight: bold;">Status</div>
            <div class="col-md-3" style="font-weight: bold;">Type</div>
          </div>

          <div class="row col-md-3">
            <div class="col-md-12" style="font-weight: bold;">Action</div>
          </div>


        </div>
        @if(isset($menuCategories) && count($menuCategories)>0)
        @foreach($menuCategories as $menuCategory)
        <div class="guestbook__customer-details-content row rest__row">
          <div class="row col-md-9 order__row" style="padding-left:15px !important; padding-right: 15px !important;"><!--  -->
            <div class="col-md-3" style="margin: auto 0px;">{{ isset($menuCategory->name) ? $menuCategory->name : '' }}</div>
            <div class="col-md-3" style="margin: auto 0px;">{{ isset($menuCategory->Restaurant->restaurant_name) ? $menuCategory->Restaurant->restaurant_name : '' }}</div>
            <div class="col-md-3" style="margin: auto 0px;">{{ ($menuCategory->status==1)?'Active':($menuCategory->status==0?'Inactive':'') }}</div>

             <div class="col-md-3" style="margin: auto 0px;">{{ isset($product_types[$menuCategory->product_type]) ? $product_types[$menuCategory->product_type] : '' }}</div>
          </div>

          <div class="row col-md-3">
            <div class="col-md-6">
              <a style="width:100%;margin:10px 0;" href="{{ URL::to('menu_category/' . $menuCategory->id . '/edit') }}" class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
            </div>
            <div class="col-md-6">
              {!! Form::open(['method' => 'DELETE', 'route' => ['menu_category.destroy', $menuCategory->id]]) !!}
              @csrf
              <button style="width:100%;margin:10px 0;" class="btn btn__cancel" type="submit"><span class="glyphicon glyphicon-trash"></span> Delete</button>
              {!! Form::close() !!}
            </div>

          </div>
        </div>
        @endforeach
        @else
        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
          No Record Found
        </div>
        @endif
      </div>
      @if(isset($menuCategories) && count($menuCategories)>0)
      <div style="margin: 0px auto;text-align: center;">
        {{ $menuCategories->appends($_GET)->links()}}
      </div>
      @endif
    </div>

  </div>
</div>
@endsection

