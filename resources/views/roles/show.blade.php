@extends('layouts.app')


@section('content')
    <div class="main__container container__custom clearfix">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1 class="module__title">Show Role</h1>
            </div>
            
        </div>

        <div class="guestbook__container fullWidth box-shadow margin-top-0 margin-bottom-20" style="padding:25px!important;">
            <div class="_block">
                <div class="form-group">
                    <strong>Name:</strong>
                    {{ $role->name }}
                </div>
            </div>

            <div class="_block">
                <div class="form-group">
                    <strong>Permissions:</strong>
                    @if(!empty($rolePermissions))
                        @foreach($rolePermissions as $v)
                            <label class="label label-success">{{ $v->name }},</label>
                        @endforeach
                    @endif
                </div>
            </div>

            <div>
                <a class="btn btn__primary" href="{{ route('roles.index') }}"> Back</a>
            </div>
        </div>
        
    </div>
@endsection