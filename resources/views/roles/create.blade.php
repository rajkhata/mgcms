@extends('layouts.app')


@section('content')
    <div class="main__container container__custom clearfix">
        <div class="inner__container">
            <div class="reservation__atto">
                <div class="reservation__title">
                    <h1 class="module__title">Create New Role</h1>
                </div>
            </div>


            @if (count($errors) > 0)
                <div class="alert alert-danger margin-top-20">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul style="list-style:none;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
            <div class="guestbook__container fullWidth box-shadow margin-top-0 margin-bottom-20" style="padding:10px!important;">
                <div class="_block" style="margin-bottom: 20px;">
                    <div class="form-group form-inline">
                        <!-- <div class="pull-left">
                            <label>Name:</label>
                        </div> -->
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 padding-top-10 inputClass">
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 padding-top-10 text-align-right">
                            <a class="btn btn__primary" href="{{ route('roles.index') }}"> Back</a>
                        </div>
                    </div>
                </div>

                <div class="_block">
                    <div id="pannelBox" class="panel panel-default">
                        <div class="panel-heading permission__title"><strong>Permissions</strong></div>
                        <div class="panel-body">
                            <div class="option__wrapper">
                              <label class="custom_checkbox relative padding-left-25 margin-left-5 font-weight-600">{{ Form::checkbox('globalCheckbox', 'all', '', array('class' => 'hide globalCheckbox')) }}
                              All
                              <span class="control_indicator"></span>
                              </label>
                              {!! Form::close() !!}
                            </div>
                            <?php
                            $roleGroup = 'N/A';
                            ?>

                            @foreach($permission as $value)
                                @if ($value->name == 'role-list' || $value->name == 'role-create' || $value->name == 'role-edit' || $value->name == 'role-delete')
                                    @continue
                                @endif
                                @if($value->group_name != '' && $value->group_name != $roleGroup)
                                    @php
                                        $roleGroup = $value->group_name;
                                    @endphp
                                    <div class="options__title"><h5>{{ucfirst($roleGroup)}}</h5></div>
                                @endif

                                <div class="option__wrapper">
                                    <label class="custom_checkbox relative padding-left-25 margin-left-5 font-weight-600">
                                        {{ Form::checkbox('permission[]', $value->id, false, array('class' => 'hide permission')) }}
                                        {{ ucfirst($value->name) }}
                                        <span class="control_indicator"></span>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="_block text-center padding-bottom-10">
                    <button type="submit" class="btn btn__primary">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

<script>
$(document).ready(function() {
       
     if($(".permission").length == $(".permission:checked").length) {
            $(".globalCheckbox").prop("checked", true);
        }else {
            $(".globalCheckbox").prop("checked", false);            
        }  
       
    $('.globalCheckbox').click(function(){
        if($(this).prop("checked")) {
            $(".permission").prop("checked", true);
        } else {
            $(".permission").prop("checked", false);
        }                
    });


    $('.permission').click(function(){
        if($(".permission").length == $(".permission:checked").length) {
            $(".globalCheckbox").prop("checked", true);
        }else {
            $(".globalCheckbox").prop("checked", false);            
        }
    });
})
</script>
@endsection