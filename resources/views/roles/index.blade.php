@extends('layouts.app')

@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1 class="module__title">Roles Management</h1>
            </div>
            <div>
                @can('role-create')
                    <!-- <a class="btn btn__primary" href="{{ route('roles.create') }}"> Create New Role</a> -->
                @endcan
            </div>
        </div>
        
        <div class="active__order__container" style="padding:10px;">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            <table id="myTable2" class="responsive ui cell-border hover" style="margin-bottom:10px;" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th class="action__columns">Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($roles as $key => $role)
                
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ ucwords($role->name) }}</td>
                        <td>
                            <!-- <a class="btn btn__primary" href="javascript:void(0)" data-toggle="modal" data-target="#showroles">
                                View
                            </a> -->
                            <a class="btn btn__primary margin-5" href="{{ route('roles.show',$role->id) }}" >
                                View
                            </a>

                            @can('role-edit')
                                <a class="btn btn__primary margin-5" href="{{ route('roles.edit',$role->id) }}">
                                    Edit
                                </a>
                            @endcan

                            @can('role-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn__primary margin-5']) !!}
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>            
            </table>
            {!! $roles->render() !!}
        </div>
    </div>

    <!-- show popup -->
    <!-- <div id="showroles" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Show Role</h4>
                </div>

                <div class="modal-body clearfix">

                    <div class="_block">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {{ $role->name }}
                        </div>
                    </div>

                    <div class="_block">
                        <div class="form-group">
                            <strong>Permissions:</strong>
                            @if(!empty($rolePermissions))
                                @foreach($rolePermissions as $v)
                                    <label class="label label-success">{{ $v->name }},</label>
                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> -->
    <!-- show popup -->

<script>
    $(document).ready(function() {
    $('#myTable2').DataTable( { 
        dom:'<"top"fiB><"bottom"trlp><"clear">',
        "order": [[1, "asc" ]],
        buttons: [{
            text: 'Create New Role',
            action: function () {location.href = "roles/create";}
        }],
//        buttons: [
//            {extend: 'excelHtml5'}, 
//            {extend: 'pdfHtml5'},   
//            {               
//                extend: 'print',
//                exportOptions: {
//                    columns: ':visible'
//                }
//            },
//            'colvis'
//        ],
//        columnDefs: [ {
//            targets: -1,
//            visible: true
//        } ]
        language: {
            search: "",
            searchPlaceholder: "Search"
        },
        responsive: true
    } );
    $('#myTable').show();
    $('#myTable').removeClass('hidden');
} );
</script>
@endsection