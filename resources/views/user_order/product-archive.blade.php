<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <div class="main__container container__custom merchandise-order-page">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <div class="reservation__atto flex-align-item-start">
            <div class="reservation__title margin-top-5">
                <h1>{{$productArchiveOrderData->total()}} Archive<br class="hidden-md hidden-lg"/> Merchandise Orders</h1>
            </div>
            <div class="search__manage-order col-xs-11 col-sm-9 col-md-8 no-padding">
                <div class="pull-right hidden-lg hidden-md show-tablet margin-bottom-10">
                  <a href="{{ URL::to('product_orders') }}" class="btn btn__holo font-weight-600">Active Merchandise Orders</a>
                  <a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10" rel = "product-archive" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a>
              </div>

            {!! Form::open(['method'=>'get']) !!}
                <div class="fullWidth form_field__container flex-justify-content-end">

                    <div class="guestbook__sec2 col-xs-6 col-sm-6 col-md-4 no-padding">
                        <div class="fullWidth search-icon no-margin-right">
                        <i class="fa fa-search"></i>
                        <input type="text" name="key_search" autocomplete="off" placeholder="Name, Phone no. or Receipt no." id="key_search" value="{{ $_GET['key_search']??'' }}">
                    </div>
                </div>

                <div class="selct-picker-plain margin-left-10 filter-by-status">

<!--                    <label for="order_status" class="no-margin">Filter By Status</label>-->
                    <select class="selectpicker" data-style="no-background-with-buttonline margin-top-5 no-padding-left  no-padding-top" data-width="150" name="order_status" id="order_status">
                        @foreach($orderStatus as $key=>$value)
                            <option value="{{$key}}" <?php if($statusKey==$key) echo "selected";?>>{{ $value }}</option>
                        @endforeach
                    </select>

                </div>

                <div class="margin-left-10 no-margin-right">
                    <button class="btn btn__primary font-weight-600" type="submit">Search</button>
                </div>

                <div class="hidden-xs hidden-sm hidden-tablet margin-left-10"><a href="{{ URL::to('product_orders') }}" class="btn btn__holo font-weight-600">Active Merchandise Orders</a></div>
                <div class="hidden-xs hidden-sm hidden-tablet"><a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10" rel = "product-archive" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a></div>

            </div>
            {!! Form::close() !!}
        </div>
         
        </div>

        <div class="guestbook__container fullWidth box-shadow">

            <div class="guestbook__table-header fullWidth">
                <div class="col-xs-5 col-sm-5 col-lg-6 no-padding-left">
                    <div class="col-xs-6 col-sm-6 col-md-3 show-tablet-50 type_of_order no-padding-left">
                        Receipt No.
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 show-tablet-50 no-padding-left">Name</div>
                    <div class="col-xs-1 col-sm-1 col-md-3 hidden-sm hidden-tablet no-padding-left">Amount</div>
                    <div class="col-xs-2 col-sm-2 col-md-3 hidden-sm hidden-tablet no-padding">Phone</div>

                </div>
                <div class="col-xs-2 col-sm-2 col-lg-2 no-padding-left">
                    Products
                </div>
                <div class="col-xs-1 col-sm-1 no-padding-left">
                    Status
                </div>
                <div class="col-xs-2 col-sm-2 col-lg-2 padding-left-25 no-padding-right">
                    Order In
                </div>
                <div class="col-xs-2 col-sm-2 col-lg-1"></div>
            </div>


                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'get']) !!}

                {!! Form::close() !!}

                    @if(isset($productArchiveOrderData) && count($productArchiveOrderData)>0)
                        @foreach($productArchiveOrderData as $oData)
                            <div class="guestbook__cutomer-details-wrapper">
                            <div class="row guestbook__customer-details-content fullWidth">

                                <div class="col-xs-5 col-sm-5 col-lg-6 no-padding-left font-weight-700">
                                    <div class="col-xs-6 col-sm-6 col-md-3 show-tablet-50 type_of_order no-padding-left" role="button" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">
                                        {{ucfirst($oData->payment_receipt)}}
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-3 show-tablet-50 text-capitalize text-ellipsis no-padding-left">                                       
                                        @if($oData->user_id)
                                            <a href="/guestbook/detail/{{$oData->user_id}}" target="_blank" title="{{ $oData->fname.' '. $oData->lname }}"><b>{{ $oData->fname.' '. $oData->lname }}</b></a>
                                        @else
                                            {{ $oData->fname.' '. $oData->lname }}
                                        @endif

                                    </div>
                                    @php
                                        if(Auth::user()) {
                                            $curSymbol = Auth::user()->restaurant->currency_symbol;
                                        } else {
                                            $curSymbol = config('constants.currency');
                                        }
                                    @endphp
                                    <div role="button" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'" class="col-xs-1 col-sm-1 col-md-3 show-tablet-50 no-padding-left">{{ $curSymbol.$oData->total_amount }}</div>
                                    <div role="button" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'" class="col-xs-2 col-sm-2 col-md-3 show-tablet-50 no-padding"><i class="fa fa-phone hidden show-tablet" aria-hidden="true"></i> {{ $oData->phone }}</div>

                                </div>

                                <div role="button" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'" class="col-xs-2 col-sm-2 col-lg-2 no-padding-left font-weight-700" role="button" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">
                                    <div class="text-ellipsis">{{$oData->item}}</div>
                                    <div class="text-color-grey">
                                        <?php
                                        if($oData->num_item > 1) {
                                            echo "+".($oData->num_item - 1).' items';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div role="button" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'" class="col-xs-1 col-sm-1 no-padding-left font-weight-700 text-capitalize">
                                    {{ $oData->status }}
                                </div>
                                <div role="button" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'" class="col-xs-2 col-sm-2 col-lg-2 padding-left-25 no-padding-right">
                                    <strong><?php $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData->restaurant_id, 'datetime' => $oData->created_at)));
                                        echo date('D, M d', $timestamp2);?></strong><br><strong class="text-color-grey font-size-13"><?php echo date('h:i A', $timestamp2);?></strong>
                                </div>

                                    <div class="col-xs-2 col-sm-2 col-lg-1 guest_btn no-padding-right">
                                        <button class="btn btn__primary font-weight-600" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">View</button>
                                    </div>
                            </div>
                            </div>
                        @endforeach

                    @else
                        <div class="text-center col-xs-12 col-sm-12 margin-top-bottom-40" >
                            <strong>No Record Found</strong>
                        </div>
                    @endif




        </div>

        @if(isset($productArchiveOrderData) && count($productArchiveOrderData)>0)
            <div class="text-center">
                {{ $productArchiveOrderData->appends($_GET)->links()}}
            </div>
        @endif

    </div>
@endsection
