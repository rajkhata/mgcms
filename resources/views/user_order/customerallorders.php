<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
<div class="main__container container__custom order-page" >
    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <div class="reservation__atto flex-align-item-baseline margin-bottom-10 tablet-flex-align-item-start flex-column-tablet">
        <div class="reservation__title margin-top-5">
           <h1 class="pull-left">Orders</h1>      
        </div>
        <div class="search__manage-order col-sm-9 col-md-9 no-padding-right  no-padding-left-tablet">
            {{--<div class="pull-right hidden-lg hidden-md show-tablet margin-bottom-10">
                <a href="{{ URL::to('user_order') }}" class="btn btn__holo font-weight-600">Active Orders</a>
                <a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10 export_button" rel = "food_item-archive" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i>Export</a>
            </div>--}}

            {!! Form::open(['method'=>'get']) !!}
            <div class="fullWidth form_field__container flex-justify-content-end flex-tablet-between"> 

                {{--<div class="hidden-xs hidden-sm hidden-tablet margin-left-10"><a href="{{ URL::to('user_order') }}" class="btn btn__holo font-weight-600">Active Orders</a></div>
                <div class="hidden-xs hidden-sm hidden-tablet">
                    <!-- <a href="javascript:void(0)" rel = "food_item-archive" class="btn btn__holo font-weight-600 export_order" ><i class="fa icon-export" aria-hidden="true"></i> Export</a> -->

                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 export_button" rel = "food_item-archive" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a>

                </div>--}}

            </div>
            {!! Form::close() !!}
        </div>

    </div>

    @if(isset($orderData) && count($orderData)>0)
    <div class="guestbook__container fullWidth box-shadow margin-top-10" style="padding:10px!important;">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @elseif (session()->has('err_msg'))
        <div class="alert alert-danger">
            {{ session()->get('err_msg') }}
        </div>
        @endif
           <table class="tabwid" style="border:0px solid #000000;" height="50px">
            <tbody>
                <tr>
                    <td class="tabwid1">&nbsp;&nbsp;&nbsp;</td>
                    <td class="tabwid2"><strong>Filter By Order Date</strong></td>
                    <td class="tabwid3"><input name="min" id="min" type="text" style="border:1px solid #000000; padding:3px 5px; margin-left: 5px" placeholder="Start Date"></td>
                    <td class="tabwid4"><input name="max" id="max" type="text" style="border:1px solid #000000; padding:3px 5px; margin-left: 5px" placeholder="End Date"></td>
                </tr>                
            </tbody>
            </table>
        
        <table class="tabwid" style="border:0px solid #000000;" height="50px">
            <tbody>
                <tr>
                    <td class="tabwid1">&nbsp;&nbsp;&nbsp;</td>
                    <td class="tabwid2"><strong>Filter By Delivery Date</strong></td>
                    <td class="tabwid3"><input name="min" id="dmin" type="text" style="border:1px solid #000000; padding:3px 5px; margin-left: 5px" placeholder="Start Date"></td>
                    <td class="tabwid4"><input name="max" id="dmax" type="text" style="border:1px solid #000000; padding:3px 5px; margin-left: 5px" placeholder="End Date"></td>
                </tr>
            </tbody>
        </table>
        
        <table id="myTable" class="display responsive ui celled cell-border hover display compact" style="padding:5px; border:1px solid #000000;" width="100%">

            <thead>
                <tr id="firstTR">
                    <th>&nbsp;</th>
                    <th>Orders (ID No.)</th>
                    <th>Order Date</th>
                    <th>Delivery/Takeout Time</th>
                    <th>Total Amount</th>
                    <th>Contact</th>
                    <th class = "filterhead">Status</th>
                    <th>Action</th>
                </tr>
            </thead>



            <tbody>
                @foreach($orderData as $key => $oData)                   
                
                <?php
                if ($oData['order_type'] == 'delivery') {
                    $del_address = $oData['address'] . ' ' . $oData['address2'];
                    $cityDbzip =  $oData['city'] . ' ' . $oData['state'] . ' ' . $oData['zipcode'];
                    if ($oData['address_label']) {
                        $del_address = $oData['address_label'] . ' ' . $del_address;
                    }
                    $icon_class = 'icon-delivery';
                    $icon_title = 'Delivery';
                } else {
                    $del_address = $oData['restaurant_name'] . ' ' . $oData['raddress'];
                    $cityDbzip =  $oData['rstreet'].' '.$oData['rzipcode'];
                    $icon_class = 'icon-takeout';
                    $icon_title = 'Takeout';
                }

                ?>
                @php
                if(Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
                } else {
                $curSymbol = config('constants.currency');
                }
                @endphp

                <tr>
                    <?php 
                        $orderDate = date("d/m/y H:i", strtotime($oData['created_at']));
                        $deliveryDate = date("d/m/y H:i", strtotime($oData['delivery_datetime']));
                    ?>
                    
                     <td>
                        <?php if($oData['order_type']=="delivery"){?>
                         <img title="Delivery" alt="Delivery" src="{{asset('images/icon-delivery.png')}}" alert="{{}}">
                        <?php } else {?>
                         <img title="Takeout" alt="Takeout" src="{{asset('images/icon-takeout.png')}}" alert="{{}}">
                        <?php } ?>
                     </td>
                     <td  onclick="window.location = '{{ URL::to('user_order/' . $oData['id'] . '/mngdetails/'.$oData['product_type']) }}'">
                       
                        <div class="text-color-grey font-size-13 font-weight-600">
                            ({{$oData['payment_receipt']}})
                        </div>
                        
                    </td>
                    <td>
                        {{ $orderDate }}
                    </td>
                    
                    <td>
                        {{ $deliveryDate}}
                    </td>
                    
                    <td>
                      {{ $curSymbol.$oData['total_amount'] }}
                    </td>
                    
                    <td>
                        {{ $oData['fname'].' '. $oData['lname'] }}<br/>
                        <a href="tel:+{{$oData['phone'] }}">{{ $oData['phone'] }}</a><br/>                        
                         <?php echo $del_address; ?><br>
                         <?php echo $cityDbzip;?>
                         
                     </td>
                     
                    <td>
                        {{ ucfirst($oData['status']) }} 
                    </td>
                    <td>
                    <button  onclick="window.location = '{{ URL::to('user_order/' . $oData['id'] . '/mngdetails/'.$oData['product_type']) }}'" class="btn_ btn__primary font-weight-600 box-shadow">View</button>
                    </td>
                </tr>

                @endforeach
                 
            <tbody>
        </table>
    </div>
    @else
    <div class="fullWidth enabled-services flex-box flex-direction-column flex-justify-center">
        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center flex-box flex-direction-column flex-justify-center flex-align-item-center">
            <div class="fullWidth padding-top-bottom-20">
                <div class="icon-order-enable text-color-grey"></div>
                <div class="col-xs-12 padding-top-bottom-20 margin-top-15 font-weight-600">
                    No Record Found
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<script>
//    $(document).ready(function () {  
//    var table = $('#myTable').DataTable( {
//        lengthChange: true,
//        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
//    } );
//    table.buttons().container()
//        .insertBefore( '#firstTR' );
// } );



    $(document).ready(function() {
    $('#myTable').DataTable( {
       initComplete: function () {
            this.api().columns([6]).every( function () {
                var column = this;
                
                var select = $('<select><option value="All">Status</option><option value="All">All</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } 
       
        );
        }
    } );
} );

 $(document).ready(function(){
        $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
            var startDate = new Date(data[1]);
            if (min == null && max == null) { return true; }
            if (min == null && startDate <= max) { return true;}
            if(max == null && startDate >= min) {return true;}
            if (startDate <= max && startDate >= min) { return true; }
            return false;
        }
        );
            $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            var table = $('#myTable').DataTable();

            // Event listener to the two range filtering inputs to redraw on input
            $('#min, #max').change(function () {
                table.draw();
            });
            
            //delivery Date filter
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var dmin = $('#dmin').datepicker("getDate");
                    var dmax = $('#dmax').datepicker("getDate");
                    var dstartDate = new Date(data[2]);
                    if (dmin == null && dmax == null) { return true; }
                    if (dmin == null && dstartDate <= dmax) { return true;}
                    if(dmax == null && dstartDate >= dmin) {return true;}
                    if (dstartDate <= dmax && dstartDate >= dmin) { return true; }
                    return false;
                }
            );
            $("#dmin").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            $("#dmax").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            var table = $('#myTable').DataTable();

            // Event listener to the two range filtering inputs to redraw on input
            $('#dmin, #dmax').change(function () {
                table.draw();
            });
        });
        

</script>
                @endsection
