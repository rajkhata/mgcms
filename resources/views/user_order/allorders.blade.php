<?php
use App\Helpers\CommonFunctions;
?>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>


@extends('layouts.app')

@section('content')


<div class="main__container container__custom order-page clearfix" >
    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <div class="reservation__atto flex-align-item-baseline margin-bottom-10 tablet-flex-align-item-start flex-column-tablet">
        <div class="reservation__title margin-top-5">
           <h1 class="pull-left">
               @if(isset($action) && $action=="usrord")
                Customer Orders : {{$custormename}}
               @else
                Orders
               @endif
           </h1>
        </div>

    </div>

    @if(isset($orderData) && count($orderData)>0)
    <div class="guestbook__container fullWidth box-shadow margin-top-0 relative" style="padding:10px!important;">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @elseif (session()->has('err_msg'))
        <div class="alert alert-danger">
            {{ session()->get('err_msg') }}
        </div>
        @endif
        <table class="tabwid hide" style="border:0px solid #000000;">
            <tbody>
                <tr class="frstline">
                    <!-- <td class="tabwid1">&nbsp;&nbsp;&nbsp;</td> -->
                    <td class="tabwid2"><strong>Filter By:</strong></td>
                    <td class="tabwid3" style="width:110px;"><div id="odate" class="radioDate active"><span></span> Order Date</div></td>
                    <td class="tabwid3" style="width:130px;"><div id="ddate" class="radioDate"><span></span> Fulfillment Date</div></td>
                </tr>
                <tr class="scndline">
                    <td colspan="3" class="tabwid3" style="width:100%;">
                        <div class="odateIN" style="">
                            <div class="rdf"><input name="min" id="min" type="text" class="radioDateField" placeholder="Start Date" readonly><span title="Clear" class="clearCTA" id="clear1">X</span></div>
                            <div class="rdf"><input name="max" id="max" type="text" class="radioDateField" placeholder="End Date" readonly><span title="Clear" class="clearCTA" id="clear2">X</span></div>
                        </div>
                        <div class="ddateIN" style="display:none">
                            <div class="rdf"><input name="min" id="dmin" type="text" class="radioDateField" placeholder="Start Date" readonly><span title="Clear" class="clearCTA" id="clear3">X</span></div>
                            <div class="rdf"><input name="max" id="dmax" type="text" class="radioDateField" placeholder="End Date" readonly><span title="Clear" class="clearCTA" id="clear4">X</span></div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

         

            <table id="myTable" class="responsive ui cell-border hover" style="margin-bottom:10px;" width="100%">
                <!-- display celled -->
                <thead>
                <tr id="firstTR">
                    <th>Type</th>
                    <th>Orders ID</th>
                    <th>Order Date</th>
                    <th>Fulfillment Date</th><!-- Delivery/Takeout Time -->
                    <th>Voucher Code</th>
                    <!-- <th>Sub Total</th>
                    <th>Voucher Discount</th>
                    <th>Flat Discount</th>
                    <th>Tip</th> -->
                    <th>Amount</th><!-- Total Amount -->
                    <th>Contact</th>
                    <th>Zipcode</th><!-- Postcode/ -->
                    <th>Status</th>
		    <th>Restaurant</th>
		    <th>Order Routed To</th>	
                    <th>Action</th>
                </tr>
                </thead>

            </table>
    </div>
    @else
    <div class="fullWidth enabled-services flex-box flex-direction-column flex-justify-center">
        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center flex-box flex-direction-column flex-justify-center flex-align-item-center">
            <div class="fullWidth padding-top-bottom-20">
                <div class="icon-order-enable text-color-grey"></div>
                <div class="col-xs-12 padding-top-bottom-20 margin-top-15 font-weight-600">
                    No Record Found
                </div>
            </div>
        </div>
    </div>
    @endif

    </div>
    <div class="clearfix"></div>
    <div id="datepicker-div" style="display:none"></div>

 
<script>
    var url='';
</script>
<?php if(isset($action) && $action=='food_item'){ ?>
<script>
     url=  "/mng_order/1/food_item";
</script>

<?php } else { ?>
  <script>

  url= "/mng_order/<?php echo isset($id)?$id:1; ?>/usrord";
    </script>

<?php } ?>
<script>

var searchval1="";
var paging_start='';
var paging_end='';
var dataTableglobal;
function min_date(type,startdate="", endate="",searchval='') {
    if(searchval=='') {
        searchval = searchval1;
    }
    else
    {
        searchval1=searchval;
    }

    var dataTable = $('#myTable').DataTable({
        
        "bDestroy":true,
        'stateSave':true,
        "processing": true,
        "serverSide": true,
        "aaSorting": [ [2,"desc" ]],
        "ajax": {
            url: url, // json datasource
            data: {val: 'getEMP', startdate: startdate, endate:endate,type:type,searchval:searchval}, // Set the POST variable  array and adds action: getEMP
            type: 'post',  // method  , by default get
	    
        },
         dom:'<"top"fiB><"bottom"trlp><"clear">',
        // pagingType: "full_numbers",
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-eye"></i>',
                titleAttr: 'Column Visibility'
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                download: 'open',
                exportOptions: { columns: ':visible' }
            }, 
            {
                extend: 'pdfHtml5',
                text: '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                exportOptions: { columns: ':visible' }
            },
            {               
                extend: 'print',
                text: '<i class="fa fa-print"></i>',
                titleAttr: 'Print',
                exportOptions: {
                    columns: ':visible',
                },
            }
        ],
        columnDefs: [
            { targets:[-1,-6], visible: false },
            { targets:[0,1,2,3,5,6,7,8], className: "desktop" },
            { targets:[0,1,2,3,8], className: "tablet" },
            { targets:[0,1,8], className: "mobile" }
        ],
        language: {
            search: "",
            searchPlaceholder: "Name, Order id, Phone number"
        },

        responsive: true,
        error: function () {

        },
        dom:'<"top"fiB><"bottom"trlp><"clear">',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-eye"></i>',
                titleAttr: 'Column Visibility'
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                exportOptions: { columns: ':visible' }
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                exportOptions: { columns: ':visible' }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print"></i>',
                titleAttr: 'Print',
                exportOptions: {
                    columns: ':visible',
                },
            }
        ],
        columnDefs: [
            { targets:[-1,-6], visible: false },
            { targets:[0,1,2,3,5,6,7,8], className: "desktop" },
            { targets:[0,1,2,3,8], className: "tablet" },
            { targets:[0,1,8], className: "mobile" }
        ],
        language: {
            search: "",
            searchPlaceholder: "Name, Order id, Phone number"
        },
        responsive: true,
        pageLength: 25

    });

    
    $( "#myTable_filter" ).find( ".input-sm" ).addClass('searchdatatable');


    $(".searchdatatable").val(searchval1);
}


    $(document).ready(function() {
        
        min_date('no');

      $("#odate").click(function(){
            $(".odateIN").css({"display": "block"});
            $(".ddateIN").css({"display": "none"});
            $(this).addClass('active');
            $("#ddate").removeClass('active');
            var start1 = document.getElementById('min').value;
            var end1 = document.getElementById('max').value;
            if(start1!="")
            {
                document.getElementById('min').value="";
              $("#clear1").hide();
            }
          if(end1!="")
          {
              document.getElementById('max').value="";
              $("#clear2").hide();
          }
          searchval1 = $(".searchdatatable").val();
//
         // $('#myTable').DataTable().destroy();
          min_date('no');



        });

        $("#ddate").click(function(){
            $(".ddateIN").css({"display": "block"});
            $(".odateIN").css({"display": "none"});
            $(this).addClass('active');
            $("#odate").removeClass('active');

            var start1 = document.getElementById('dmin').value;
            var end1 = document.getElementById('dmax').value;
            if(start1!="")
            {
                document.getElementById('dmin').value="";
                $("#clear3").hide();
            }
            if(end1!="")
            {
                document.getElementById('dmax').value="";
                $("#clear4").hide();
            }
            searchval1 = $(".searchdatatable").val();

          //  $('#myTable').DataTable().destroy();
            min_date('no');
        });
        setTimeout(function() {
            $(".tabwid").css({"display": "block"});
        }, 300);

        $("#min, #max, #dmin, #dmax").click(function(e) {
            $("#datepicker-div").show();
        });

        $("#datepicker-div").click(function(e) {
            $("#ui-datepicker-div").hide();
            $("#datepicker-div").hide();
        });
        $(".slideLeft").click(function () {
            $("#orderDetailBox").css({"right": "0", "visibility": "visible"});
            $("#overlayOD").show();
        });
        $(".slideClose").click(function () {
            $("#orderDetailBox").css({"right": "-100%", "visibility": "hidden"});
            $("#overlayOD").hide();
        });


    });


$(document).ready(function() {


    $("#min").datepicker({
        onSelect: function () {
            searchval1 = $(".searchdatatable").val();

            $('#clear1').show();
            /**************************function calling********************************************/
            var startdate = document.getElementById('min').value;
            var enddate = document.getElementById('max').value;
            min_date('orderdate', startdate, enddate);

            /*************************************************************************************/
        }, changeMonth: true, changeYear: true,
    });
    $("#max").datepicker({
        onSelect: function () {
            searchval1 = $(".searchdatatable").val();

            $('#clear2').show();
            /**************************function calling********************************************/
            var startdate = document.getElementById('min').value;
            var enddate = document.getElementById('max').value;

            min_date('orderdate', startdate, enddate);
            /*************************************************************************************/
        }, changeMonth: true, changeYear: true
    });

    // Event listener to the two range filtering inputs to redraw on input
    $('#min, #max').change(function () {
        searchval1 = $(".searchdatatable").val();

        var startdate = document.getElementById('min').value;
        var enddate = document.getElementById('max').value;
        min_date('orderdate', startdate, enddate);
    });

    $("#dmin").datepicker({
        onSelect: function () {
            searchval1 = $(".searchdatatable").val();

            $('#clear3').show();
            var dmin = document.getElementById('dmin').value;
            var dmax = document.getElementById('dmax').value;
            min_date('Fulfillment', dmin, dmax);


        }, changeMonth: true, changeYear: true
    });
    $("#dmax").datepicker({
        onSelect: function () {
            searchval1 = $(".searchdatatable").val();

            $('#clear4').show();
            var dmin = document.getElementById('dmin').value;
            var dmax = document.getElementById('dmax').value;
            min_date('Fulfillment', dmin, dmax);
        }, changeMonth: true, changeYear: true
    });

    // Event listener to the two range filtering inputs to redraw on input
    $('#dmin, #dmax').change(function () {
        searchval1 = $(".searchdatatable").val();

        var dmin = document.getElementById('dmin').value;
        var dmax = document.getElementById('dmax').value;

        min_date('Fulfillment', dmin, dmax);
    });
    var clearBtn = document.getElementById('clear1');
    clearBtn.onclick = function() {
        searchval1 = $(".searchdatatable").val();

        $(this).hide();
        document.getElementById('min').value="";
        var startdate = document.getElementById('min').value;
        var enddate = document.getElementById('max').value;

        min_date('orderdate', startdate, enddate);
    }
    var clearBtn2 = document.getElementById('clear2');
    clearBtn2.onclick = function() {
        searchval1 = $(".searchdatatable").val();

        $(this).hide();
        document.getElementById('max').value="";
        var startdate = document.getElementById('min').value;
        var enddate = document.getElementById('max').value;
       // $('#myTable').DataTable().destroy();
        min_date('orderdate', startdate, enddate);
    }
    var clearBtn3 = document.getElementById('clear3');
    clearBtn3.onclick = function() {
        searchval1 = $(".searchdatatable").val();

        $(this).hide();
        document.getElementById('dmin').value ="";
        var dmin = document.getElementById('dmin').value;
        var dmax = document.getElementById('dmax').value;
      //  $('#myTable').DataTable().destroy();
        min_date('Fulfillment', dmin, dmax);
    }
    var clearBtn4 = document.getElementById('clear4');
    clearBtn4.onclick = function() {
        searchval1 = $(".searchdatatable").val();

        $(this).hide();
        document.getElementById('dmax').value ="";
        var dmin = document.getElementById('dmin').value;
        var dmax = document.getElementById('dmax').value;

        min_date('Fulfillment', dmin, dmax);
    }


});
function inputsearch()
{
    var startdate = document.getElementById('min').value;
    var enddate = document.getElementById('max').value;
    var dmin = document.getElementById('dmin').value;
    var dmax = document.getElementById('dmax').value;

    if(startdate!=''||enddate!='')
    {

        min_date('orderdate', startdate, enddate);
    }
    if(dmin!=''||dmax!='')
    {

        min_date('Fulfillment', dmin, dmax);
    }
    if(startdate==''&&enddate==''&&dmin==''&&dmax=='')
    {

        min_date('no');
    }
}
var ajax_call = function() {

    searchval1 = $(".searchdatatable").val();
    inputsearch();
//console.log('search...');
};

var interval =  1000 * 60 * 1; // where X is your every X minutes
setInterval(ajax_call, interval);

</script>
@endsection
