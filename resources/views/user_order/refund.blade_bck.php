@extends('layouts.app')
@section('content')
    @php
        if(Auth::user()) {
            $curSymbol = Auth::user()->restaurant->currency_symbol;
        } else {
            $curSymbol = config('constants.currency');
        }
    @endphp
    <div class="main__container margin-lr-auto container__custom">
        <div>       
            @if(isset($orderData))

                <div class="row hidden-print order__wrapper">
                <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
                    <div class="reservation__atto hide">
                        <div class="reservation__title">
                            <h1>Order Details</h1>
                        </div>
                    </div>
                </div>


                <div class="order-detail-page popup_orderdetail row">
                    <!--    <a href="javascript:void(0);" class="a_nd_close orderDetailsPopupClose">+</a>-->
                    <div class="t-scroll1 res-bott-space col-md-10 footeractive col-md-offset-1">
                        <div class="row margin-bottom-30">

                            <div class="col-sm-1 text-left no-padding-left no-padding-right padding-top-bottom-10">
                                <a href="/user_order/{{$orderData->id}}/details/{{$action}}" class="font-weight-600"><i class="fas fa-arrow-left margin-right-10" aria-hidden="true"></i>Back</a>
                            </div>
                           
                        </div>
                      
                        <form action="post" id = "refund_submit">                      

                        <div class="row order__item-container margin-top-20 order-summary-section border-box">                           
                            <div class="col-lg-12 col-lg-offset-0 col-sm-12 od__details box-shadow">                               
                                <div class="order-title o-head text-center"><b>REFUND ORDER</b></div>                             
                                <div class=" bor-top">
                                    <div class="w_310 ch-m-bott pull-md-right pull-xs-right op-color">
                                        <div class="row food_order_item margin-bottom-10">
                                          
                                            <div style="padding-right:70px " class="col-md-6 col-xs-6 ">Subtotal
                                            </div>
                                            <div class="col-md-6 col-xs-6 text-truncate text-right">
                                                {{ $curSymbol.$orderData->order_amount }}</div>
                                        </div>
                                        @if($orderData->order_type =='delivery' && $action != 'gift_card')
                                            <div class="row food_order_item margin-bottom-10">
                                                {{--<p class="col-md-6 col-xs-6 text-right">&nbsp;</p>--}}
                                                <div style="padding-right:70px " class="col-md-6 col-xs-6 ">
                                                    Delivery Charge</div>
                                                <div class="col-md-6 col-xs-6 text-truncate text-right">
                                                    {{ $curSymbol.number_format($orderData->delivery_charge,2) }}</div>
                                            </div>
                                        @endif
                                        <div class="row food_order_item margin-bottom-10">
                                            {{--<p class="col-md-6 col-xs-6 text-right">&nbsp;</p>--}}
                                            <div style="padding-right:70px " class="col-md-6 col-xs-6 ">
                                                Tax</div>
                                            <div class="col-md-6 col-xs-6 text-truncate text-right">
                                                {{ $curSymbol.number_format($orderData->tax,2) }}</div>
                                        </div>
                                        @if($orderData->tip_amount!=0.00)
                                            <div class="row food_order_item margin-bottom-10">
                                                {{--<p class="col-md-6 col-xs-6 text-right">&nbsp;</p>--}}
                                                <p style="padding-right:70px " class="col-md-6 col-xs-6 ">
                                                    Tip</p>
                                                <p class="col-md-6 col-xs-6 text-truncate text-right">
                                                    {{ $curSymbol.number_format($orderData->tip_amount,2) }}</p>
                                            </div>
                                        @endif
                                        @if($refunded_amount!=0.00)
                                            <div class="row food_order_item margin-bottom-10">
                                                <p style="padding-right:70px " class="col-md-6 col-xs-6 ">
                                                    Refunded Amount</p>
                                                <p class="col-md-6 col-xs-6 text-truncate text-right">
                                                    {{ $curSymbol.number_format($refunded_amount,2) }}</p>
                                            </div>
                                        @endif
                                        <div class="row food_order_item margin-bottom-10">
                                            {{--<p class="col-md-6 col-xs-6 text-right">&nbsp;</p>--}}
                                            <div style="padding-right:70px "
                                               class="col-md-6 col-xs-6 font-weight-800">Total</div>
                                            <div class="col-md-6 col-xs-6 text-truncate text-right font-weight-800">
                                                {{ $curSymbol.number_format($orderData->total_amount,2) }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="">
                                                <div class="col-lg-12 text-center ">
                                                  <p class="margin-bottom-20"> <label class="padd-left-18 font-weight-800" for="">Are you sure, you want to refund the entire amount to the customer?</label></p>
                                                </div>
                                                <div class="col-lg-6 margin-bottom-20">  
                                                    <div class="tbl-radio-btn">
                                                        <input type = "radio" name = "refund_mode" id = "sizeSmall" value = "fully" rel = "fully" class="refund_status"  checked="checked" >
                                                        <label class="text-color-grey" for = "sizeSmall">Fully Refund</label>
                                                   </div>
                                                </div>
                                               <div class="col-lg-6 margin-bottom-20">
                                                    <div class="tbl-radio-btn">
                                                        <input type = "radio" name = "refund_mode" id = "sizeSmall1" value = "partially" rel = "partially" class="refund_status">
                                                        <label class="text-color-grey" for = "sizeSmall1">Partial Refund</label>
                                                    </div>
                                               </div>
                                                <input type="hidden" class="" name="refund_type" value="F">
                                                <input type="hidden" class="" name="oid" value="{{$orderData->id}}">
                                               <div class="input__field">
                                                    <input type="text" class="partial_amt" name="refund_value" placeholder="Partial Amount" style="display: none;">
                                                </div>
                                            </div>                                           
                                        </div>                                       

                                        <div class="input__field">
                                            <input id="mobile" type="text" placeholder="Host Name" name="host_name" value="{{ $orderData->restaurant_name }}">
                                        </div>

                                        <select name="reason" id="reason_id" class="form-control">
                                            <option value="">Select Reason for Refund</option>  
                                            <option value="Reason 1">Reason 1</option>  
                                            <option value="Reason 2">Reason 2</option>  
                                            <option value="Reason 3">Reason 3</option>  
                                            <option value="other">Other</option>
                                        </select>

                                        <div class="custom__message-textContainer no-margin">
                                            <textarea placeholder="Reason for Refund" maxlength="300" rows="5" id="other_reason" name="reason_other" style="display: none;"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group row mb-0">                                    
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn__primary">
                                                {{ __('Refund') }}
                                        </button>
                                    </div>
                                </div>
                                <div id="errormess" style="display:none;">
                                </div>                                
                            </div> 
                        </div>
                        {{ Form::close() }}
                    </div>
                    </div>
                </div>
            @else
                <div class="row"
                     style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                    No Record Found
                </div>
            @endif

        </div>

    </div>


    <script type="text/javascript">
       // setTimeout(function(){ // PE-2087
           // location.reload(); // then reload the page.(3)
       // }, 80000);
        $(function() {
            //Show hide text box

            $(document.body).on('change','#reason_id',function() {
                var current_select = $(this).val();
                if(current_select == 'other') {
                    $('#other_reason').show();
                }else {
                    $('#other_reason').hide();
                }
            });

            //show Partial and fully            

            $(document.body).on('change','.refund_status',function() {
                var refund_mode = $(this).attr('rel');
                if(refund_mode == 'fully') {
                    $('.partial_amt').val('').hide();
                }else {
                    $('.partial_amt').val('').show();
                }
            });


            // Refund Form submit

            $("#refund_submit").submit(function(e){
                e.preventDefault();
                var inputData = $(this).serialize();
                $("#loader").removeClass('hidden');
                alert("sud1");
                $.ajax({
                    url: '/release_order_refund',
                    type: 'post',
                    data: inputData,
                    success: function (data) {
                        $("#loader").addClass('hidden');
                        alertbox('Success',data.success, function(modal){                           
                        });
                        window.location.href = '/user_order/'+data.order_id+'/details/'+data.product_type;
                    },
                    error: function (data, textStatus, errorThrown) {
                        $("#loader").addClass('hidden');
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                        });
                        alertbox('Error',err,function(modal){
                            setTimeout(function() {                       
                            }, 2000);
                        });
                    }
                });
            });
        });

    </script>

@endsection