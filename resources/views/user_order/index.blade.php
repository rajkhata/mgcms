<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            //setInterval("location.reload(true)", 120000 );
        });
    </script>

    <div class="main__container container__custom order-page">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <div class="reservation__atto flex-align-item-baseline margin-bottom-10 tablet-flex-align-item-start flex-column-tablet">
            <div class="reservation__title margin-top-5 ">
                <?php
                $search_padding = '';
                if(!app('request')->has('key_search')) { ?>
                <h1> @if($statusKey=="") Active
                    Orders @else {{ ucfirst($statusKey) }}Orders @endif</h1>
                <?php } else {
                $search_padding = 'padding-top-35';
                ?>
                <h1><span class="textColor__primary">{{$orderData->total()}}</span> Result with
                    "{{ app('request')->input('key_search') }}" <a href="/user_order"
                                                                   class="btn btn__holo margin-left-10">X</a></h1>
                <?php }?>
            </div>
            <div class="search__manage-order col-sm-7 col-md-7 no-padding-right no-padding-left-tablet">

                {{--<div class="fullWidth text-right hidden-lg hidden-md show-tablet margin-bottom-10">
                    <a href="{{ URL::to('archive_order') }}" class="btn btn__holo font-weight-600">Archive</a>
                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10 export_button" rel = "food_item-active" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a>
                </div>--}}

                {!! Form::open(['method'=>'get']) !!}
                <div class="fullWidth form_field__container flex-tablet-between">

                    <div class="guestbook__sec2 no-padding">
                        <div class="fullWidth search-icon no-margin-right">
                            <i class="fa fa-search"></i>
                            <input type="text" name="key_search" autocomplete="off"
                                   placeholder="Name, Phone no. or Receipt no." id="key_search"
                                   value="{{ $_GET['key_search']??'' }}">
                            <button class="btn btn__primary font-weight-600" type="submit">Search</button>
                        </div>
                    </div>

                    {{--<div class="input__field">
                        <input type="text" name="key_search" id="key_search" style="padding:5% 0;" value="{{ $_GET['key_search']??'' }}">
                        <label for="searchKey">Search</label>
                    </div>--}}

                    <div class="flex-box flex-direction-row margin-top-5 overflow-visible">
                    <!--  <div class="selct-picker-plain margin-left-20 filter-by-status">
                        <select class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" data-width="150" name="order_status" id="order_status">
                            @foreach($orderStatus as $key=>$value)
                        <option value="{{$key}}" <?php if ($statusKey == $key) echo "selected";?>>{{ $value }}</option>
                            @endforeach
                            </select>
                        </div> -->

                        <div class="selct-picker-plain margin-left-20 filter-by-status">
                            <select class="selectpicker ordersortorder"
                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top"
                                    data-width="150" name="ordersortorder" rel="user_order">
                                @foreach($search_type as $key=>$value)
                                    <option value="{{$key}}" <?php if ($default_sort_order == $key) echo "selected";?>>{{ $value }}</option>
                                @endforeach

                            <!-- Iy will be visible only incase of admin or super admin-->
                                @if(Auth::user()->role=="admin")
                                    @foreach($groupRestData as $rest)
                                        <optgroup label="{{ $rest['restaurant_name'] }}">
                                            @foreach($rest['branches'] as $branch)
                                                <option value="{{ $branch['id'] }}" <?php if ($default_sort_order == $branch['id']) echo "selected";?> >{{ $branch['restaurant_name'] }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>


                    {{--<div class="hidden-xs hidden-sm hidden-tablet margin-left-10"><a href="{{ URL::to('archive_order') }}" class="btn btn__holo font-weight-600">Archive</a></div>
                    <div class="hidden-xs hidden-sm hidden-tablet">
                        <!-- <a href="javascript:void(0)" class="btn btn__holo font-weight-600 export_order" rel = "food_item-active"><i class="fa icon-export" aria-hidden="true"></i> Export</a> -->

                        <a href="javascript:void(0)" class="btn btn__holo font-weight-600 export_button" rel = "food_item-active" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a>

                        <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#pdf_pop">Exportss</button> -->

                    </div>--}}


                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <div class="row services-alert hide">
            <div class="fullWidth flex-box flex-direction-row flex-align-item-center flex-justify-space-between padding-top-bottom-5 padding-left-right-30">
                <div class="pull-left font-weight-600 font-size-16 flex-box flex-direction-row flex-align-item-center">
                    <i class="fa fa-exclamation-circle margin-right-5" aria-hidden="true"></i>
                    <span>Incoming Orders Paused Since Feb 20, 2019</span>
                </div>

                <a href="/configure/manage_services"
                   class="btn btn__primary btn-resume-service dashboard position-relative">
                    Resume Services
                </a>
            </div>
        </div>

        @if(isset($orderData) && count($orderData)>0)

            <div class="guestbook__container fullWidth box-shadow order-listing margin-top-10">

                <div class="guestbook__table-header fullWidth no-padding-left no-padding-right">
                    <div class="col-xs-12 col-md-8 no-padding-left">
                        <div class="col-xs-2 no-padding">&nbsp;</div>
                        <div class="col-xs-3">Orders (ID No.)</div>
                        <div class="col-xs-4 no-padding-left">Contact</div>
                        <div class="col-xs-3 no-padding">Location</div>
                    </div>
                </div>


                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'get']) !!}

                {!! Form::close() !!}

                @foreach($orderData as $oData)
                    <div class="guestbook__cutomer-details-wrapper no-padding overflow-visible">
                        <div role="button"
                             onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'"
                             class="row manage__container position-relative guestbook__customer-details-content text-color-black fullWidth {{--padding-top-35--}} <?php echo $search_padding;?> {{--new-order-row--}} <?php echo ($oData->status == 'placed') ? 'new-order-row' : '';?>"
                             id="restaurant-order-{{ $oData->id }}">

                            <?php
                            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $oData->restaurant_id));

                            if (is_object($currentDateTimeObj)) {
                                $restcurrentDateTime['current_datetime'] = $currentDateTimeObj->format('Y-m-d H:i:s');
                            } else {
                                $restcurrentDateTime['current_datetime'] = date('Y-m-d H:i:s');
                            }

                            $order_is_before_time = strtotime($restcurrentDateTime['current_datetime']) <= strtotime($oData->delivery_date . ' ' . $oData->delivery_time . ':00') ? 1 : 0;
                            $status_active_class = '';
                            if(app('request')->has('key_search')) {
                            if (in_array($oData->status, array("archived", "cancelled", "arrived", "sent", "rejected", "refunded"))) {
                                $statuskey = 'Archived';
                            } elseif (strtotime($restcurrentDateTime['current_datetime']) < strtotime($oData->order_delivery_fdatetime)) {
                                $statuskey = 'Scheduled';
                            } else {
                                $statuskey = 'Active';
                                $status_active_class = 'active';
                            }
                            ?>
                            <div class="order-label font-weight-700 padding-left-right-10 {{--active--}} {{$status_active_class}}">{{$statuskey}}</div>
                            <?php } ?>

                            {{--$oData--}}
                            <div class="col-xs-12 col-md-8 no-padding-left tablet-margin-bottom-15">
                                <div class="col-xs-2 text-center">
                                    <!-- New Order -->
                                    <?php
                                    $dteStart = new DateTime($oData->delivery_date . ' ' . $oData->delivery_time . ':00');
                                    $dteEnd = new DateTime($restcurrentDateTime['current_datetime']);
                                    if($oData->status == 'placed') { ?>
                                    <div class="new-order-icon fullWidth text-center">
                                        <i class="icon-new-order" aria-hidden="true"></i>
                                        <span>NEW</span>
                                    </div>

                                    <!-- END New Order-->
                                    <?php } elseif($order_is_before_time) {
                                    $dteDiff = $dteStart->diff($dteEnd);
                                    list($day, $hours, $minute) = explode('-', $dteDiff->format("%a-%H-%I"));
                                    // Added default key and value
                                    $cloak_key = 'Min';
                                    $cloak_val = 0;
                                    if ($day != 0) {
                                        $cloak_key = $day == 1 ? 'Day' : 'Days';
                                        $cloak_val = $day;
                                    } elseif ($hours != 0) {
                                        $cloak_key = $hours == 1 ? 'Hour' : 'Hours';
                                        $cloak_val = $hours;
                                    } elseif ($minute != 0) {
                                        $cloak_key = $minute == 1 ? 'Min' : 'Mins';
                                        $cloak_val = $minute;
                                    }
                                    ?>
                                <!-- Reverse cloak-->
                                    <div class="order-timer text-center font-weight-600">
                                        <span class="font-size-18 font-weight-800 fullWidth margin-top-7">{{(int)$cloak_val}}</span>
                                        <span class="font-size-12 fullWidth">{{$cloak_key}}</span>
                                    </div>

                                    <?php } else {
                                    // forward cloak
                                    $dteDiff = $dteEnd->diff($dteStart);
                                    list($day, $hours, $minute) = explode('-', $dteDiff->format("%a-%H-%I"));
                                    // Added default key and value
                                    $cloak_key = 'Min';
                                    $cloak_val = 0;
                                    if ($day != 0) {
                                        $cloak_key = $day == 1 ? 'Day' : 'Days';
                                        $cloak_val = $day;
                                    } elseif ($hours != 0) {
                                        $cloak_key = $hours == 1 ? 'Hour' : 'Hours';
                                        $cloak_val = $hours;
                                    } elseif ($minute != 0) {
                                        $cloak_key = $minute == 1 ? 'Min' : 'Mins';
                                        $cloak_val = $minute;
                                    }
                                    ?>
                                <!-- Late Order Order-->
                                    <div class="order-timer red-timer text-center font-weight-600">
                                        <span class="font-size-18 font-weight-800 fullWidth margin-top-7">{{(int)$cloak_val}}</span>
                                        <span class="font-size-12 fullWidth">{{$cloak_key}}</span>
                                    </div>
                                    <?php } ?>


                                </div>
                                @php
                                    if(Auth::user()) {
                                        $curSymbol = Auth::user()->restaurant->currency_symbol;
                                    } else {
                                        $curSymbol = config('constants.currency');
                                    }
                                @endphp
                                <div class="col-xs-3 type_of_order ">
                                    <strong class="font-weight-700">{{ $oData->order_type}}</strong> <br>
                                    <div class="text-color-grey font-size-13 font-weight-600">
                                        ({{$oData->payment_receipt}})
                                    </div>
                                    <span class="text-color-grey font-size-13 font-weight-600">Total :</span> <strong
                                            class="font-weight-700">{{ $curSymbol.$oData->total_amount }}</strong>

                                </div>
                                <div class="col-xs-4 no-padding-left">
                                    <span class="font-weight-700">{{ $oData->fname.' '. $oData->lname }}</span> <br/>

                                    <div class="flex-box flex-direction-row flex-align-item-center">
                                        <i class="fa fa-phone margin-right-5" aria-hidden="true"></i> <span
                                                class="font-weight-600">{{ $oData->phone }}</span> <br/>
                                    </div>
                                    <div class="flex-box flex-direction-row address-row">

                                        @if ($oData->order_type == 'delivery')
                                            <i class="fa flex-align-item-center icon-home margin-right-5"></i>
                                        @endif
                                        <span class="font-weight-600">


                                        <?php
						$del_address = '';
                                            //if ($oData->order_type == 'delivery') {
                                                $del_address = $oData->address . ' ' . $oData->address2 . ' ' . $oData->city . ' ' . $oData->state . ' ' . $oData->zipcode;
                                                if ($oData->address_label) {
                                                    $del_address = $oData->address_label . ' ' . $del_address;
                                                }
                                            //} else {
                                                
                                           // }
                                            echo $del_address;
                                            ?>

                                    </span>
                                    </div>
                                </div>
                                <div class="col-xs-3 text-nowrap text-ellipsis quantity_of_order no-padding-left">
                                    <div class="font-weight-700 text-ellipsis">{{$oData->restaurant_name}}</div>
                                </div>

                            </div>
                            <div class="col-xs-12 col-md-4 no-padding {{$oData->order_type}}-order-status">
                                {{-- ucfirst($oData->status) --}}

                                <?php if($oData->order_type == 'delivery') {
                                $first_main_div = '';
                                $first_child_div = 'icon-emptyCircle';
                                if ($oData->status == 'confirmed') {
                                    $first_main_div = 'active';
                                    $first_child_div = 'icon-confirmed';
                                }
                                ?>
                                <ul class="flex-box flex-direction-row flex-justify-space-between deliveryRowStatus">
                                    <li class="flex-box flex-direction-column text-center <?php echo $first_main_div;?>">
                                        <span class="<?php echo $first_child_div;?>"></span>
                                        <span class="text-nowrap">Confirmed</span>
                                    </li>
                                    <li class="flex-box flex-direction-column text-center">
                                        <span class="icon-emptyCircle"></span>
                                        <span class="text-nowrap">Sent</span>
                                    </li>

                                </ul>
                                <?php } else {
                                $first_main_div = $second_main_div = '';
                                $first_child_div = $second_child_div = 'icon-emptyCircle';

                                if ($oData->status == 'confirmed') {
                                    $first_main_div = 'active';
                                    $first_child_div = 'icon-tick-mark';
                                }
                                if ($oData->status == 'ready') {
                                    $first_main_div = $second_main_div = 'active';
                                    $first_child_div = $second_child_div = 'icon-tick-mark';

                                }

                                ?>
                                <ul class="flex-box flex-direction-row flex-justify-space-between">
                                    <li class="flex-box flex-direction-column text-center <?php echo $first_main_div;?>">
                                        <span class="<?php echo $first_child_div;?>"></span>
                                        <span class="text-nowrap">Confirmed</span>
                                    </li>
                                    <li class="flex-box flex-direction-column text-center <?php echo $second_main_div;?>">
                                        <span class="<?php echo $second_child_div;?>"></span>
                                        <span class="text-nowrap">Ready for Pick Up</span>
                                    </li>
                                    <li class="flex-box flex-direction-column text-center">
                                        <span class="icon-emptyCircle"></span>
                                        <span class="text-nowrap">Picked Up</span>
                                    </li>
                                </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="fullWidth enabled-services flex-box flex-direction-column flex-justify-center">
                <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center flex-box flex-direction-column flex-justify-center flex-align-item-center">
                    <div class="fullWidth padding-top-bottom-20">
                        <div class="icon-order-enable text-color-grey"></div>
                        <div class="col-xs-12 padding-top-bottom-20 margin-top-15 font-weight-600">
                            No Record Found
                        </div>
                    </div>
                </div>
            </div>
        @endif


        @if(isset($orderData) && count($orderData)>0)
            <div class="text-center">
                {{ $orderData->appends($_GET)->links()}}
            </div>
        @endif

        <div class="hide col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center disabled-services flex-box flex-direction-column flex-justify-center flex-align-item-center">
            <div class="fullWidth padding-top-bottom-20">
                <div class="icon-order-disable text-color-grey"></div>
                <div class="col-xs-12 padding-top-bottom-20 margin-top-15 font-weight-600">
                    Online Ordering/Reservation service are currently <br class="hidden-xs"/>disabled for this location.
                    Tap the button to switch them on.
                </div>
                <div class="col-xs-12 margin-top-20">
                    <a href="#" class="btn_ btn__primary">Enable Services</a>
                </div>
            </div>
        </div>


    </div>

    <script type="text/javascript">

        function doAction(order_id, action, orderType) {
            if (order_id && action) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('#loader').removeClass('hidden');
                $.ajax({
                    type: 'PUT',
                    url: '/order/' + order_id + '/' + action,
                    data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&action=' + action,
                    success: function (data) {
                        var innercontent = '';
                        var innermsg = '';
                        //location.reload();

                        if (data["message"] != undefined) {
                            alertbox('Error', data["message"], function (modal) {
                                modal.on("hidden.bs.modal", function (e) {
                                    setTimeout(function () {
                                        location.reload();
                                    }, 100);

                                });
                            });
                            // setTimeout(function () {
                            //     location.reload();
                            // }, 3000);
                        }
                        if (action === 'confirmed') {

                            if (orderType === 'delivery') {
                                innercontent = '<button type="button" class="btn btn__primary btn-block" onclick="javascript:doAction(' + order_id + ' ,\'sent\',\'' + orderType + '\');"> Sent </button>';
                                innermsg = 'Order Confirmed';
                            } else {
                                innercontent = '<button type="button" class="btn btn__primary btn-block" onclick="javascript:doAction(' + order_id + ' ,\'ready\',\'' + orderType + '\');"> Ready </button>';
                                innermsg = 'Order Confirmed';
                            }
                        }
                        if (action === 'ready') {

                            innercontent = '<button type="button" class="btn btn__primary btn-block" onclick="javascript:doAction(' + order_id + ' ,\'archived\',\'' + orderType + '\');"> Picked Up </button>';

                            innermsg = '';
                        }
                        if (action === 'sent') {
                            innercontent = '';
                            innermsg = '';
                            $('#restaurant-order-' + order_id).remove();
                        }
                        if (innercontent !== '') {
                            $('#actionBtn_' + order_id).html(innercontent);
                            //$('#actionDsc_'+order_id).html(innermsg);
                        } else {
                            setTimeout(function () {// wait for 5 secs(2)
                                location.reload(); // then reload the page.(3)
                            }, 100);
                        }
                        $('#loader').addClass('hidden');

                        // setTimeout(function(){// wait for 5 secs(2)
                        //     location.reload(); // then reload the page.(3)
                        // }, 100);
                    }
                });
            }
        }

        setTimeout(function () { // PE-2087
            location.reload(); // then reload the page.(3)
        }, 600000);   // As discussed with Prakash, NKD quick updates

    </script>
@endsection
