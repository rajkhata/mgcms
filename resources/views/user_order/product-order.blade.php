<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            setInterval("location.reload(true)", 60000);   // As discussed with Prakash, NKD quick updates
        });
    </script>
 
    <div class="main__container container__custom merchandise-order-page">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <div class="reservation__atto flex-align-item-start">
            <div class="reservation__title margin-top-5">
                <h1>{{$productOrderData->total()}} Active<br class="hidden-md hidden-lg"/> Merchandise Orders</h1>
            </div>

            <div class="search__manage-order col-xs-11 col-sm-9 col-md-8 no-padding">
                <div class="pull-right hidden-lg hidden-md show-tablet margin-bottom-10">
                    <a href="{{ URL::to('product_archive_orders') }}" class="btn btn__holo font-weight-600">Archive Merchandise Orders</a>
                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10 export_button" rel = "product-active" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a>
                </div>

            {!! Form::open(['method'=>'get']) !!}
                <div class="fullWidth form_field__container flex-justify-content-end">

                    <div class="guestbook__sec2 col-xs-6 col-sm-6 col-md-4 no-padding">
                        <div class="fullWidth search-icon no-margin-right">
                        <i class="fa fa-search"></i>
                        <input type="text" name="key_search" autocomplete="off" placeholder="Name, Phone no. or Receipt no." id="key_search" value="{{ $_GET['key_search']??'' }}">
                    </div>
                </div>

                <div class="selct-picker-plain margin-left-10 filter-by-status">

<!--                    <label for="order_status" class="no-margin">Filter By Status</label>-->
                    <select class="selectpicker" data-style="no-background-with-buttonline margin-top-5 no-padding-left  no-padding-top" data-width="150" name="order_status" id="order_status">
                        @foreach($orderStatus as $key=>$value)
                            <option value="{{$key}}" <?php if($statusKey==$key) echo "selected";?>>{{ $value }}</option>
                        @endforeach
                    </select>

                </div>

                <!--<div class="input__field">
                    <input type="text" name="from_date" id="from_date">
                    <label for="searchKey">Start Date</label>
                </div>

                <div class="input__field">
                    <input type="text" name="to_date" id="to_date">
                    <label for="searchKey">End Date</label>
                </div>-->

                <div class="margin-left-10 no-margin-right">
                    <button class="btn btn__primary font-weight-600" type="submit">Search</button>
                </div>

                <div class="hidden-xs hidden-sm hidden-tablet margin-left-10"><a href="{{ URL::to('product_archive_orders') }}" class="btn btn__holo font-weight-600">Archive Merchandise Orders</a></div>
                <div class="hidden-xs hidden-sm hidden-tablet"> <a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10 export_button" rel = "product-active" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a></div>

            </div>
            {!! Form::close() !!}
        </div>
            
      
        </div>
        <div class="guestbook__container fullWidth box-shadow">

            <div class="guestbook__table-header fullWidth">
                <div class="col-xs-5 col-sm-5 col-lg-7 no-padding-left">
                    <div class="col-xs-6 col-sm-6 col-md-3 show-tablet-50 type_of_order no-padding-left">
                        Receipt No.
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 show-tablet-50 no-padding-left">Name</div>
                    <div class="col-xs-1 col-sm-1 col-md-3 hidden-sm hidden-tablet no-padding-left">Amount</div>
                    <div class="col-xs-1 col-sm-2 col-md-3 hidden-sm hidden-tablet no-padding">Phone</div>
                </div>
                <div class="col-xs-3 col-sm-3 col-lg-2 no-padding">
                    Products
                </div>
                <div class="col-xs-3 col-sm-2 col-lg-2 no-padding-right">
                    Order In
                </div>
                <div class="col-xs-2 col-sm-2 col-lg-1"></div>
            </div>



                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'get']) !!}

                {!! Form::close() !!}
                    @if(isset($productOrderData) && count($productOrderData)>0)
                        @foreach($productOrderData as $oData)

                        <div class="guestbook__cutomer-details-wrapper">
                            <div class="row guestbook__customer-details-content fullWidth text-color-black font-weight-700" id="restaurant-order-{{ $oData->id }}">

                                <div class="col-xs-5 col-sm-5 col-lg-7 no-padding-left">
                                    <div role="button" class="col-xs-6 col-sm-6 col-md-3 show-tablet-50 type_of_order no-padding-left font-weight-700" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">
                                        {{ucfirst($oData->payment_receipt)}}
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-3 show-tablet-50 text-capitalize text-ellipsis no-padding-left">
                                        @if($oData->user_id)
                                            @php if(isset($oData->user_fname)) { $name = $oData->user_fname. ' ' . $oData->user_lname; $phone = $oData->user_phone ?? $oData->user_mobile; } else { $name = $oData->fname.' '.$oData->lname; $phone = $oData->phone; }  @endphp
                                            <a href="/guestbook/detail/{{$oData->user_id}}" target="_blank" title="{{ $name }}"><b>{{ $name }}</b></a>
                                        @else
                                            @php $phone = $oData->phone; @endphp
                                            {{ $oData->fname.' '. $oData->lname }}
                                        @endif
                                        <!-- <br><small>{{substr($oData->address, 0, 35)}}..</small> -->
                                    </div>
                                    @php
                                        if(Auth::user()) {
                                            $curSymbol = Auth::user()->restaurant->currency_symbol;
                                        } else {
                                            $curSymbol = config('constants.currency');
                                        }
                                    @endphp
                                    <div role="button" class="col-xs-1 col-sm-1 col-md-3 show-tablet-50 no-padding-left" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">{{ $curSymbol.$oData->total_amount }}</div>
                                    <div role="button" class="col-xs-2 col-sm-2 col-md-3 show-tablet-50 no-padding" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'"><i class="fa fa-phone hidden show-tablet" aria-hidden="true"></i> {{ $phone }}</div>
                                </div>

                                <div role="button" class="col-xs-3 col-sm-3 col-lg-2 no-padding font-weight-700" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">
                                    <div class="text-ellipsis">{{$oData->item}}</div>
                                    <div class="text-color-grey">
                                        <?php
                                        if($oData->num_item > 1) {
                                            echo "+".($oData->num_item - 1).' items';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div role="button" class="col-xs-2 col-sm-2 col-lg-2 no-padding-right" onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">
                                    <strong><?php $timestamp2 = '';
                                        $currentDate = date('Y-m-d H:i:s');
                                        $currentTimestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData->restaurant_id, 'datetime' => $currentDate)));
                                        $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData->restaurant_id, 'datetime' => $oData->created_at)));
                                        echo date('D, M d', $timestamp2);?></strong> <br>
                                    <div class="text-color-grey font-size-13"> <?php echo date('h:i A', $timestamp2);?></div>
                                </div>


                                    <div class="col-xs-2 col-sm-2 col-lg-1 no-padding-right" >
                                  

                                  @if(($oData->order_type =='delivery'))
                                      @if(($oData->status =='placed') || ($oData->status =='ordered'))
                                          <div class="col-xs-12 col-sm-12 guest_btn no-padding" id="actionBtn_<?php echo $oData->id ?>">
                                              <?php
                                              $title = 'Confirm';
                                              if($oData->manual_update==1 && $oData->order_type=="delivery")
                                              //$title = "Manual Confirm";?>
                                              <button type="button" class="btn btn__primary status-confirm font-weight-600"
                                                      onclick="javascript:doAction(<?php echo $oData->id ?> ,'confirmed','<?php echo $oData->order_type ?>');">
                                                  {{$title}}
                                              </button>
                                          </div>
                                          <p class="italic" id="actionDsc_<?php echo $oData->id ?>"></p>
                                      @elseif($oData->status =='confirmed')
                                          <div class="col-xs-12 col-sm-12 guest_btn no-padding" id="actionBtn_<?php echo $oData->id ?>">
                                              <button type="button" class="btn btn__primary status-sent font-weight-600"
                                                      onclick="javascript:doAction(<?php echo $oData->id ?> ,'sent','<?php echo $oData->order_type ?>');">
                                                  Sent
                                              </button>
                                          </div>

                                      @elseif($oData->status =='rejected')
                                          <div class="col-xs-6 col-sm-6">
                                              <p class="italic">We have removed this order and sent a notification to the customer</p>
                                          </div>
                                      @else
                                          <div class="col-xs-6 col-sm-6">
                                              <p class="italic">{{$oData->status}}</p>
                                          </div>
                                      @endif

                                  @else
                                      @if(($oData->status =='placed') || ($oData->status =='ordered'))
                                          <div class="col-sm-12 guest_btn no-padding" id="actionBtn_<?php echo $oData->id ?>">
                                              <button type="button" class="btn btn__primary status-confirm font-weight-600"
                                                      onclick="javascript:doAction(<?php echo $oData->id ?> ,'confirmed','<?php echo $oData->order_type ?>');">
                                                  Confirm
                                              </button>
                                          </div>
                                          <p class="italic" id="actionDsc_<?php echo $oData->id ?>"></p>
                                      @elseif($oData->status =='confirmed')
                                          <div class="col-sm-12 no-padding" id="actionBtn_<?php echo $oData->id ?>">
                                              <button type="button" class="btn btn__primary status-ready font-weight-600"
                                                      onclick="javascript:doAction(<?php echo $oData->id ?> ,'ready','<?php echo $oData->order_type ?>');">
                                                  Ready
                                              </button>
                                          </div>

                                      @elseif($oData->status =='ready')
                                          <div class="col-sm-12 guest_btn no-padding" id="actionBtn_<?php echo $oData->id ?>">
                                              <button type="button" class="btn btn__primary status-picked-up font-weight-600"
                                                      onclick="javascript:doAction(<?php echo $oData->id ?> ,'archived','<?php echo $oData->order_type ?>');">
                                                  Picked Up
                                              </button>
                                          </div>

                                      @elseif($oData->status =='rejected')
                                          <div class="col-sm-6">
                                              <p class="italic">We have removed this order and sent a notification
                                                  to the customer</p>
                                          </div>
                                      @else
                                          <div class="col-sm-6">
                                              <p class="italic">{{$oData->status}}</p>
                                          </div>
                                      @endif

                                  @endif
                              

                              </div>

                                {{--<div class="col-md-1 type_of_order" >{{ $oData->status }}</div>--}}
                            </div>
                        </div>
                        @endforeach

                    @else
                        <div class="text-center col-sm-12 margin-top-bottom-40" >
                            <strong>No Record Found</strong>
                        </div>
                    @endif
        </div>
        @if(isset($productOrderData) && count($productOrderData)>0)
            <div class="text-center">
                {{ $productOrderData->appends($_GET)->links()}}
            </div>
        @endif
    </div>

    <script type="text/javascript">


        function doAction(order_id, action, orderType) {

            if (order_id && action) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('#loader').removeClass('hidden');
                $.ajax({
                    type: 'PUT',
                    url: '/order/' + order_id + '/' + action,
                    data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&action=' + action,
                    success: function (data) {
                        var innercontent = '';
                        var innermsg = '';
                        //location.reload();

                        if(data["message"] != undefined){
                            alertbox('Error', data["message"], function (modal) {
                            });
                            setTimeout(function () {
                                location.reload();
                            }, 3000);
                        }
                        if (action === 'confirmed') {

                            if (orderType === 'delivery') {
                                innercontent = '<button type="button" class="btn btn__primary btn-block" onclick="javascript:doAction(\'+order_id+\' ,\'sent\',\'' + orderType + '\');"> Sent </button>';
                                innermsg = 'Order Confirmed';
                            } else {
                                innercontent = '<button type="button" class="btn btn__primary btn-block" onclick="javascript:doAction(' + order_id + ' ,\'ready\',\'' + orderType + '\');"> Ready </button>';
                                innermsg = 'Order Confirmed';
                            }
                        }
                        if (action === 'ready') {

                            innercontent = '<button type="button" class="btn btn__primary btn-block" onclick="javascript:doAction(' + order_id + ' ,\'archived\',\'' + orderType + '\');"> Picked Up </button>';

                            innercontent = '<button type="button" class="btn btn__primary btn-block" > Picked Up</button>';
                            innermsg = '';
                        }
                        if (action === 'sent') {
                            innercontent = '';
                            innermsg = '';
                            $('#restaurant-order-' + order_id).remove();
                        }

                        if (innercontent !== '') {
                            $('#actionBtn_' + order_id).html(innercontent);
                            //$('#actionDsc_'+order_id).html(innermsg);
                        }
                        $('#loader').addClass('hidden');

                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 100);
                    }
                });
            }
        }
     setTimeout(function(){ // PE-2087
            location.reload(); // then reload the page.(3)
     }, 60000);   // As discussed with Prakash, NKD quick updates
    </script>
@endsection
