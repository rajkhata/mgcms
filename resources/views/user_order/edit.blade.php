@extends('layouts.app')
@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">

            <div class="card">
                <div class="card-header">{{ __('Localization - Edit') }}</div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    {{ Form::model($localize, array('route' => array('localization.update', $localize->id), 'method' => 'PUT', 'files' => true)) }}
                        @csrf
                        <div class="form-group row">
                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                            <div class="col-md-4">
                                <select name="lang_id" id="lang_id" class="form-control" >
                                    <option value="">Select Restaurant</option>
                                    @foreach($languages as $lang)
                                        <option value="{{ $lang->id }}" {{ (old('lang_id')==$lang->id || (count(old())==0 && $localize->lang_id==$lang->id)) ? 'selected' :'' }}>{{ $lang->language_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('Localize Key') }}</label>
                            <div class="col-md-4">
                                <input id="key" type="text" class="form-control{{ $errors->has('key') ? ' is-invalid' : '' }}"
                                       name="key" value="{{ count(old())==0 ? $localize->key : old('key') }}" required autocomplete="off">
                                @if ($errors->has('fname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('key') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Localize Name') }}</label>
                            <div class="col-md-4">
                                <input id="value" type="text" class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}"
                                       name="value" value="{{ count(old())==0 ? $localize->value : old('value') }}" required autocomplete="off">
                                @if ($errors->has('value'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Device') }}</label>
                            <div class="col-md-4">
                                <select name="platform" id="platform" class="form-control{{ $errors->has('platform') ? ' is-invalid' : '' }}" >
                                    <option value="all" {{($localize->platform =='all') ? 'selected' :''}}>All</option>
                                    <option value="web" {{($localize->platform =='web') ? 'selected' :''}}>Web</option>
                                    <option value="ios" {{($localize->platform =='ios') ? 'selected' :''}}>App(ios)</option>
                                    <option value="android" {{($localize->platform =='android') ? 'selected' :''}}>App(android)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1' || (count(old())==0 && $localize->status=='1')) ? 'checked' :'' }}> Active
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0' || (count(old())==0 && $localize->status=='0')) ? 'checked' :'' }}> Inactive
                                @if ($errors->has('status'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <br /><br />
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
