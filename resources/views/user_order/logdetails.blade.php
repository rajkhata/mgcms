<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')

    <div class="main__container container__custom">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <div class="reservation__atto logs-listing-page">
            <div class="reservation__title">
                {{--<h1><span class="active__order"></span> Log Details</h1>--}}
                <div class="padding-bottom-10">
                    <a href="javascript:goBack()" class="font-weight-600"><i class="fa fa-arrow-left margin-right-10" aria-hidden="true"></i>Back</a>
                </div>
            </div>

            <div class="col-xs-7 col-sm-11 flex-box flex-direction-row flex-justify-content-end tablet-flex-wrap no-padding-right overflow-visible">

                <?php if($action == 'food_item' ){ ?>

                    <!--div class="margin-top-5"><a href="#" class="btn btn__primary box-shadow">Confirm Order</a></div>
                    <div class="margin-left-10 margin-top-5"><a href="#" class="btn btn__white box-shadow">Update Time Slot</a></div>
                    <div class="margin-left-10 margin-top-5"><a href="#" class="btn btn__white box-shadow">Refund Order</a></div>
                    <div class="margin-left-10 margin-top-5"><a href="#" class="btn btn__white__cancelled box-shadow">Reject/Cancel Order</a></div>
                    <div class="margin-left-10 margin-top-5"><a href="#" class="btn btn__holo box-shadow">Logs</a></div-->
                    <div class="margin-left-10 margin-top-5"><a href="#" class="btn btn__holo box-shadow"><i class="fa fa-print" aria-hidden="true"></i>Print</a></div>


                    {{--<div class="margin-left-right-5 margin-top-5"><a href="{{ URL::to('user_order') }}" class="btn btn__holo font-weight-600">Active Orders</a></div>
                    <div class="margin-left-5 margin-top-5"><a href="{{ URL::to('archive_order') }}" class="btn btn__holo font-weight-600">Archive Orders</a></div>--}}

                <?php } elseif($action == 'gift_card') {?>
                    <div class="margin-left-right-5 margin-top-5"><a href="{{ URL::to('order-gift') }}" class="btn btn__holo font-weight-600">Active Gift Card Orders</a></div>
                    <div class="margin-left-5 margin-top-5"><a href="{{ URL::to('order-gift-archive') }}" class="btn btn__holo font-weight-600">Archive Gift Card Orders</a></div>
                <?php } else { ?>
                    <div class="margin-left-right-5 margin-top-5"><a href="{{ URL::to('product_orders') }}" class="btn btn__holo font-weight-600">Active Merchandise Orders</a></div>
                    <div class="margin-left-5 margin-top-5"><a href="{{ URL::to('product_archive_orders') }}" class="btn btn__holo font-weight-600">Archive Merchandise Orders</a></div>
                <?php } ?>
            </div>

        </div>



                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'get']) !!}

                {!! Form::close() !!}
                <div>

                    <div class="guestbook__container fullWidth box-shadow">
                        @if($action != 'gift_card')
                            <div class="guestbook__table-header fullWidth">
                                <div class="col-xs-2 col-sm-2 no-padding-left">User Name</div>
                                <div class="col-xs-2 col-sm-2">Action</div>
                                <!--<div class="col-sm-2">Action Type</div>-->
                                <div class="col-xs-3 col-sm-2">Time Log</div>
                                <div class="col-xs-5 col-xs-6 no-padding">Comments</div>
                            </div>
                            @if(isset($orderLogData) && count($orderLogData)>0)
                                @foreach($orderLogData as $oData)
                                    <div class="guestbook__cutomer-details-wrapper">
                                        <div class="row guestbook__customer-details-content fullWidth text-color-black font-weight-700" id="restaurant-order-{{ $oData->id }}" >
                                            <div class="col-xs-2 col-sm-2 id no-padding-left">{{ $oData->restaurant->restaurant_name }}</div>
                                            <div class="col-xs-2 col-sm-2 text-left type_of_order">{{ ucfirst($oData->module_name) }}</div>
                                            <!--<div class="col-sm-2">{{ ucfirst($oData->action_type) }}</div>-->
                                            <div class="col-xs-3 col-sm-2">
                                                <strong><?php $timestamp2 = '';
                                                $currentDate = date('Y-m-d H:i:s');
                                                $currentTimestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData->restaurant_id, 'datetime' => $currentDate)));
                                                $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData->restaurant_id, 'datetime' => $oData->created_at)));
                                                echo date('D, M d', $timestamp2);?></strong><br> <div class="text-color-grey"><?php echo date('h:i A', $timestamp2);?></div>
                                            </div>
                                            <div class="col-xs-5 col-xs-6 no-padding">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="text-center col-xs-12 col-sm-12 margin-top-bottom-40">
                                    <strong>No Record Found</strong>
                                </div>
                            @endif
                        @else
                            <div class="guestbook__table-header fullWidth">
                                <div class="col-xs-2 col-sm-2 no-padding-left">Gift Card No.</div>
                                <div class="col-xs-2 col-sm-2 no-padding-left">User Name</div>
                                <div class="col-xs-2 col-sm-2">Module Name</div>
                                <!--<div class="col-sm-2">Action Type</div>-->
                                <div class="col-xs-2 col-sm-2">Created Date</div>
                                <div class="col-xs-4 col-sm-4 no-padding">
                                    <div class="col-xs-6 col-sm-6">Status (From)</div>
                                    <div class="col-xs-6 col-sm-6">Status (To)</div>
                                </div>
                            </div>

                            <div>

                            @if(isset($orderLogData) && count($orderLogData)>0)
                                @foreach($orderLogData as $oData)
                                <div class="guestbook__cutomer-details-wrapper">
                                    <div class="row guestbook__customer-details-content fullWidth text-color-black font-weight-700" id="restaurant-order-0" >
                                        <div class="col-xs-2 col-sm-2 id no-padding-left">{{ $oData['gift_unique_code'] }}</div>
                                        <div class="col-xs-2 col-sm-2 no-padding-left">{{ $oData['name'] }}</div>
                                        <div class="col-xs-2 col-sm-2 text-left type_of_order">Gift Order</div>
                                        <!--<div class="col-sm-2">Update</div>-->
                                        <div class="col-xs-2 col-sm-2">
                                            @php
                                                    $timestamp2 = '';
                                                    $currentDate = date('Y-m-d H:i:s');
                                                    $currentTimestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData['restaurant_id'], 'datetime' => $currentDate)));
                                                    $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $oData['restaurant_id'], 'datetime' => $oData['updated_at'])));
                                                    //$createdAt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $oData['updated_at']); @endphp
                                            <strong>{{ date('D, M d', $timestamp2) }}</strong><br>
                                            <div class="text-color-grey">{{ date('h:i A', $timestamp2) }}</div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 no-padding">
                                            <div class="col-xs-6 col-sm-6">Placed</div>
                                            <div class="col-xs-6 col-sm-6">{{ $oData['status'] }}</div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        @endif
                            </div>
                        </div>

                    @if(isset($orderLogData) && count($orderLogData)>0 && $action != 'gift_card')
                        <div class="text-center">
                            {{ $orderLogData->appends($_GET)->links()}}
                        </div>
                    @endif
                </div>


        <script type="text/javascript">

            function goBack() {
                window.history.go(-1)
            }

            $("#from_date").datepicker({
                showAnim: "slideDown",
                dateFormat: 'dd-mm-yy',
                minDate: -3
            });

            $("#to_date").datepicker({
                showAnim: "slideDown",
                dateFormat: 'dd-mm-yy',
                minDate: 0
            });



        </script>
@endsection
