<div class="detailPopup">
    @include('includes.refund')

    <div class="">
        <style>/*.a_nd_order > .row:first-child, .section_popup:after, .popup_orderdetail, .panel_list_order{display:none!important;height:0px}*/ /*.a_nd .u_contentsection{padding-top:0;}*/ table{border-collapse: initial;}.printcontainer{display: none !important;}</style> <style media="print"> @page{size: auto; margin: 0;}.a_nd_order > .row:first-child, .section_popup:after, .popup_orderdetail, .panel_list_order{display: none !important; height: 0px}.a_nd .u_contentsection{padding-top: 0;}table{border-collapse: initial;}</style>


        <div>
            @php

                if(Auth::user()) {
                    $curSymbol = Auth::user()->restaurant->currency_symbol;
                } else {
                    $curSymbol = config('constants.currency');
                }
            @endphp
            @if(isset($orderData))



                <?php $show_archive_popup = false; ?>
                    <!--    <a href="javascript:void(0);" class="a_nd_close orderDetailsPopupClose">+</a>-->


                    <!-- new page -->
                        <div id="orderDetailBox"  >
                            <div class="closeDiv">
                                <a href="javascript:;" onclick="javascript:printData()" printstatus="{{$orderData->is_order_printed == 1 ? 'Re-Print' : 'Print'}}">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </a>
                                <a href="javascript:;" class="slideClose">
                                    <i class="fa fa-close" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div id="orderDetail" class="row margin-top-10 margin-bottom-20">

                                <div class="clearfix"></div>
                            @if($currentTimestamp > $deliveryTimestamp && $show_log_btn == false && $orderData->product_type == 'food_item') <!-- // PE-3265 -->
                                <?php
                                if(($orderData->status =='placed') || ($orderData->status =='ready') || ($orderData->status =='confirmed')){
                                    $show_archive_popup = true;
                                } ?>
                                <!-- <div class="col-xs-12 order-cancel-message padding-top-bottom-5 text-center">Order is Running
                                    Behind The Time
                                </div> -->
                                @endif
                                <div class="row clearfix">
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 nopad">
                                        <section class="delBlockqoute">
                                            <div class="delBlockqoute-top">
                                                <a href="javascript:;" class="showmob slideClose closeCta">
                                                    <i class="fa fa-close" aria-hidden="true"></i> Close
                                                </a>
                                                <!-- Delivery -->
                                                @if( $orderData->product_type!='product')
                                                    {{ $orderData->order_type=='delivery'?'Delivery':'Takeout' }}
                                                @endif
                                                <h4 class="{{$orderData->card_type!="Cash"?'Prepaid':'COD'}}">{{$orderData->card_type!="Cash"?'Card':'Cash'}}</h4>

                                                <p><strong>Order ID:</strong> #{{ $orderData->payment_receipt }}</p>
                                            </div>
                                            <!-- <div class="delBlockqoute-bot">
                                                Ready By:
                                                <?php echo (new DateTime($orderData->delivery_date))->format('l d M');?>, <?php echo (!empty($orderData->delivery_time) && $orderData->delivery_time!='null' && $orderData->delivery_time!=null)?(new DateTime($orderData->delivery_time))->format('h:i A'):'';?>
                                            </div> -->
                                        </section>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                        <section class="delBlockqoute-right">
                                            <!-- <p>Order ID: #{{ $orderData->payment_receipt }}</p> -->
                                            <p><strong>Received:</strong> <?php echo $orderDate;?></p>
                                            <p><strong>Ready By:</strong> <?php echo (new DateTime($orderData->delivery_date))->format('l d M');?>, <?php echo (!empty($orderData->delivery_time) && $orderData->delivery_time!='null' && $orderData->delivery_time!=null)?(new DateTime($orderData->delivery_time))->format('h:i A'):'';?></p>
                                        </section>
                                    </div>
                                </div>
                                <!-- row -->

                                <div class="row clearfix firstrow mt10">
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 norel">
                                        <section class="addressBlockqoute">
                                            <div><strong>{{$orderData->fname. ' '.$orderData->lname}}</strong></div>
                                            <div><strong style="color:#9a9a9a">{{!empty($orderData->city)?$orderData->city.',':''}}</strong></div>

                                            <div>
                                                @if($orderData->order_type =='delivery')

                                                <?php
                                                $address = $orderData->address;
                                                if($orderData->address_label) {
                                                $address = $orderData->address_label.' '.$address;
                                                }
                                                if ($orderData->address2) {
                                                $address = $address . ', ' . $orderData->address2;
                                                }
                                                $address =$address ;
                                                        $address .= !empty($orderData->city)?'<br>'.$orderData->city . ', ' :'';
                                                        $address .= !empty($orderData->state)?$orderData->state .  ', ' :'';
                                                        $address .= $orderData->zipcode ;
                                                echo $address;
                                                ?>
                                                @else
                                                    <b>{{$restaurantDetails['restaurant_name']}}, </b><br>
                                                    {{!empty($restaurantDetails['address'])?$restaurantDetails['address']:''}}&nbsp;
                                                    {{!empty($restaurantDetails['street'])?$restaurantDetails['street']:''}}&nbsp;

                                                    {{!empty($restaurantDetails['zipcode'])?$restaurantDetails['zipcode']:''}}&nbsp;


                                                @endif

                                            </div>

                                        </section>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 norel">
                                        <section class="addressBlockqoute tar">
                                            <div class="ordernum"><strong><a href="/mng_order/{{$orderData->user_id}}/usrord" target="_blank">{{ ($userOrders) }} Order(s)</a></strong></div>
                                            <div>
                                                <i class="fa fa-phone margin-right-5" aria-hidden="true"></i> {{$orderData->phone??''}}
                                            </div>
                                            <div>
                                                <a href="mailto:{{$orderData->email??''}}" target="_top">
                                                    <i class="fa fa-envelope margin-right-5" aria-hidden="true"></i>
                                                    {{$orderData->email??''}}
                                                </a>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <!-- row -->



                                @if($orderData->user_comments!="")

                                    <div class="row clearfix firstrow formob">
                                        <div class="col-xs-12 order-instruc-message">
                                            <strong>Order Instruction:</strong> <?php echo nl2br($orderData->user_comments); ?>
                                        </div>
                                    </div>

                                @endif
                                <!-- row -->

                                <div class="row clearfix tablerow">
                                    <div class="row clearfix throw">
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-2">Qty</div>
                                        <div class="col-sm-8 col-md-8 col-lg-8 col-xs-7">Description</div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-3 tar">Price</div>
                                    </div><!-- th -->

                                    <div class="tbodyCol">
                                        @if(isset($orderDetailData) && count($orderDetailData)>0)
                                            @foreach($orderDetailData as $oData)
                                                <?php $is_label_present = array();?>
                                                <div class="row clearfix tdrow">
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-2 pl5">
                                                        <span class="qty">{{ $oData->quantity }}</span>
                                                    </div>
                                                    <div class="col-sm-8 col-md-8 col-lg-8 col-xs-7">
                                                        <div class="itemname">{{ $oData->item }}</div>
                                                        {{--
                                                                                                        <div class="addonname"><strong>Size:</strong> Signature Tomato</div>
                                                        --}}
                                                        <?php $is_label_present = array();?>
                                                        <div class="food__row formob">

                                                            @if( $orderData->product_type=='product')
                                                                @if(!is_null( $oData->item_other_info))
                                                                    @php
                                                                        $item_other_info=json_decode($oData->item_other_info);
                                                                    @endphp

                                                                    @if(isset($item_other_info->gift_wrapping_fees) && $item_other_info->gift_wrapping_fees>0)


                                                                        <div class="row item_addons">
                                                                            <div class="col-md-4 col-xs-4 add__onItem padding-left-25">
                                                                                Gift Wrapping
                                                                            </div>
                                                                            <div class="col-md-4 col-xs-4 text-center">{{$oData->quantity}}</div>
                                                                            {{--<div class="col-md-2 col-xs-2 text-right mb0">{{ $curSymbol .' '.number_format($item_other_info->gift_wrapping_fees,2)}}</div>
                                                                            <div class="col-md-2 col-xs-2 text-right">{{ $curSymbol .' ' . number_format($oData->quantity * $item_other_info->gift_wrapping_fees, 2)}}</div>--}}
                                                                            <div class="col-md-4 col-xs-2 text-center">&nbsp;
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                    @if(isset($item_other_info->gift_wrapping_message) && !empty($item_other_info->gift_wrapping_message) && $item_other_info->gift_wrapping_message!='null')
                                                                        <div class="row item_addons padding-left-25">
                                                                            <div class="col-md-4 col-xs-4 add__onItem no-padding-left">
                                                                                <i>{{nl2br($item_other_info->gift_wrapping_message)}}</i>
                                                                            </div>
                                                                            <div class="col-md-4 col-xs-2 text-center">&nbsp;
                                                                            </div>
                                                                            {{--<div class="col-md-2 col-xs-2 text-center">&nbsp;</div>
                                                                            <div class="col-md-2 col-xs-2 text-center">&nbsp;</div>--}}
                                                                            <div class="col-md-4 col-xs-2 text-right mb0">
                                                                                &nbsp;
                                                                            </div>
                                                                        </div>

                                                                    @endif
                                                                @endif

                                                            @else

                                                                @if(isset($oData->addon_modifier) && count($oData->addon_modifier)>0)
                                                                    @php
                                                                        $addon_modifier=$oData->addon_modifier;
                                                                    @endphp

                                                                    @if($oData->is_byp==0 && strtolower($oData->item_size) != 'null' && $oData->item_size != "NA" && !empty($oData->item_size) && !is_null($oData->item_size))
                                                                        <div class="addonname"><strong>Size:</strong> {{$oData->item_size}}</div>

                                                                    @endif


                                                                    @if(isset($addon_modifier['modifier_data']) && count($addon_modifier['modifier_data']))

                                                                        @php
                                                                            $modifier_data_records=$addon_modifier['modifier_data'];
                                                                            $addonDataArrary = array();
                                                                            $i = 0;
                                                                        @endphp
                                                                    <!--                                                 <div class="food_order_item padding-left-25"> Modifiers :-</div>
 -->
                                                                        @foreach($modifier_data_records as $modifier_data)

                                                                            @php
                                                                                $groupname='';
                                                                                $price=0;
                                                                                $quantity=1;
                                                                                  if(isset($modifier_data['quantity']) && !empty($modifier_data['quantity'])){
                                                                                                 $quantity=$modifier_data['quantity'];
                                                                                   }
                                                                                    if(isset($modifier_data['price']) && !empty($modifier_data['price'])){
                                                                                                 $price=$modifier_data['price'];
                                                                                   }
                                                                            @endphp
                                                                            @if(isset($modifier_data['group_prompt']) && !empty($modifier_data['group_prompt']))
                                                                                @php
                                                                                    $groupname = $modifier_data['group_prompt'];
                                                                                    $addonDataArrary[$groupname][$i]['name'] = $modifier_data['modifier_name'];
                                                                                    $addonDataArrary[$groupname][$i]['quantity'] = $quantity;
                                                                                    $addonDataArrary[$groupname][$i]['price'] = $price;
                                                                                @endphp
                                                                            @endif

                                                                            {{--<div class="row item_addons">
                                                                                <div class="col-md-4 col-xs-4 add__onItem padding-left-25">
                                                                                    {{ @$modifier_data['modifier_name'] }} <b>{{ @$groupname}}</b> </div>
                                                                                <div class="col-md-2 col-xs-2 text-center">{{@$quantity}}</div>
                                                                                <div class="col-md-2 col-xs-2 text-right mb0">
                                                                                    {{$curSymbol. @number_format($price,2) }}
                                                                                </div>
                                                                                <div class="col-md-2 col-xs-2 text-right">
                                                                                    {{$curSymbol. number_format($price * $quantity,2) }}
                                                                                </div>
                                                                                <div class="col-md-2 col-xs-2 text-right"></div>
                                                                            </div>--}}
                                                                            @php
                                                                                $i++;
                                                                            @endphp
                                                                        @endforeach
                                                                        <?php
                                                                        //echo '<pre>';print_r($addonDataArrary);
                                                                        foreach($addonDataArrary as $key => $value){
                                                                            $abc = '';
                                                                            $pArr = array();
                                                                            $abc = '<strong>'.$key.': </strong>';
                                                                            foreach($value as $innerArr){
                                                                                if($innerArr['quantity'] > 1){
                                                                                    $pArr[] = $innerArr['name'].'X'.$innerArr['quantity'];
                                                                                }else{
                                                                                    $pArr[] = $innerArr['name'];
                                                                                }
                                                                            }
                                                                            echo '<div class="addonname">'.$abc.implode(', ', $pArr).'</div>';
                                                                        }
                                                                        ?>
                                                                    @elseif(isset($addon_modifier['addons_data']) && count($addon_modifier['addons_data']))
                                                                        @php
                                                                            $addons_data_records=$addon_modifier['addons_data'];
                                                                            $addonDataArrary = array();
                                                                            $i = 0;
                                                                        @endphp
                                                                    <!--   <div class="food_order_item padding-left-25"> Addons :-
                                                    </div>  -->
                                                                        @foreach($addons_data_records as $addons_data)

                                                                            @php
                                                                                $groupname='';
                                                                            @endphp
                                                                            @if(isset($addons_data['addongroup']) && !empty($addons_data['addongroup']))
                                                                                @php
                                                                                    $groupname = $addons_data['addongroup']['prompt'];
                                                                                    $addonDataArrary[$groupname][$i]['name'] = $addons_data['option_name'];
                                                                                    $addonDataArrary[$groupname][$i]['quantity'] = $addons_data['quantity'];
                                                                                    $addonDataArrary[$groupname][$i]['price'] = $addons_data['price'];
                                                                                @endphp
                                                                            @endif

                                                                            {{--<div class="row item_addons">
                                                                                <div class="col-md-4 col-xs-4 add__onItem padding-left-25">
                                                                                    {{ @$addons_data['option_name'] }} <b>{{ @$groupname}}</b> </div>
                                                                                <div class="col-md-2 col-xs-2 text-center">{{@$addons_data['quantity']}}</div>
                                                                                <div class="col-md-2 col-xs-2 text-right mb0">
                                                                                    {{$curSymbol. @number_format($addons_data['price'],2) }}
                                                                                </div>
                                                                                <div class="col-md-2 col-xs-2 text-right">
                                                                                    {{$curSymbol. number_format($addons_data['price'] * $addons_data['quantity'],2) }}
                                                                                </div>
                                                                                <div class="col-md-2 col-xs-2 text-right"></div>
                                                                            </div>--}}
                                                                            @php
                                                                                $i++;
                                                                            @endphp
                                                                        @endforeach
                                                                        <?php
                                                                        //echo '<pre>';print_r($addonDataArrary);
                                                                        foreach($addonDataArrary as $key => $value){
                                                                            $abc = '';
                                                                            $pArr = array();
                                                                            $abc = '<b>'.$key.': </b>';
                                                                            foreach($value as $innerArr){
                                                                                if($innerArr['quantity'] > 1){
                                                                                    $pArr[] = $innerArr['name'].'X'.$innerArr['quantity'];
                                                                                }else{
                                                                                    $pArr[] = $innerArr['name'];
                                                                                }
                                                                            }
                                                                            echo '<div class="addonname">'.$abc.implode(', ', $pArr).'</div>';
                                                                        }
                                                                        ?>

                                                                    @endif

                                                                @endif


                                                                @if(isset($oData->addOnsDetail) && count($oData->addOnsDetail)>0)
                                                                    @if($oData->is_byp==0)

                                                                        @foreach($oData->addOnsDetail as $oAData)
                                                                            <?php
                                                                            if(isset($oAData['addons_label']) && !in_array($oAData['addons_label'], $is_label_present)){
                                                                            $is_label_present[] = $oAData['addons_label'];
                                                                            ?>
                                                                            <div class="col-sm-12">
                                                                                <div class="margin-top-20 margin-bottom-10 no-padding-left no-padding-right">
                                                                                    <div class="color-black">
                                                                                        <span><?php echo $oAData['addons_label']; ?></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php } ?>
                                                                            <div class="row item_addons">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-md-4 col-xs-4 add__onItem padding-left-25">
                                                                                        {{ @$oAData['addons_name'] }}</div>
                                                                                    {{--<div class="col-md-2 col-xs-2 text-center">{{@$oData->quantity}}</div>--}}
                                                                                    {{--<div class="col-md-2 col-xs-2 text-right mb0">
                                                                                    {{$curSymbol. @number_format($oAData['price'],2) }}
                                                                                    </div>
                                                                                    <div class="col-md-2 col-xs-2 text-right">
                                                                                            {{$curSymbol. number_format($oAData['price'] * $oData->quantity,2) }}
                                                                                    </div>--}}
                                                                                    <div class="col-md-2 col-xs-2 text-right"></div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    @endif

                                                                    @if($oData->is_byp==1)

                                                                        @foreach($oData->addOnsDetail as $key=>$dataval)
                                                                            <div class="row item_addons">
                                                                                <div class="">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="text-capitalize">
                                                                                            <b>{{ str_replace('-', ' ', str_replace('_', ' ', $key)) }}</b>: <?php echo  $dataval; ?>
                                                                                        </div>
                                                                                        <div> </div>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-xs-2 text-center"></div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    @endif

                                                                @endif
                                                            @endif
                                                            @if( $orderData->product_type=='food_item' && !empty($oData->special_instruction))

                                                                <div class=" row food_order_item ">
                                                                    <div class="col-md-4 col-xs-12">Special Instructions

                                                                    </div>

                                                                </div>
                                                                <div class=" row item_addons ">
                                                                    <div class="col-md-4 col-xs-12 add__onItem">
                                                                        {{$oData->special_instruction}}
                                                                    </div>
                                                                </div>

                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-3 tar">{{ $curSymbol.number_format($oData->total_item_amt,2) }}</div>
                                                </div>



                                            @endforeach
                                        @endif

                                    </div>
                                    <!-- tbody -->

                                {{--   <div class="row clearfix scndrow discntRow">
                                       <div class="col-sm-8 col-md-8 col-lg-8 col-xs-8">
                                           <i class="fa fa-cut" aria-hidden="true"></i>
                                           Discount (freepizza)
                                       </div>
                                       <div class="col-sm-4 col-md-4 col-lg-4 col-xs-4 tar">
                                           £10.00
                                       </div>
                                   </div>--}}
                                <!-- coupon row -->

                                    <div class="row clearfix subtotalRow">
                                        <div class="row clearfix">
                                            <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">Subtotal</div>
                                            <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">{{ $curSymbol.$orderData->order_amount }}</div>
                                        </div>

                                        @if($orderData->flat_discount)
                                            <div class="row clearfix">
                                                <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">Discount</div>
                                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">-{{ $curSymbol.number_format($orderData->flat_discount,2) }}</div>
                                            </div>
                                        @endif
                                        @if($orderData->promocode_discount)
                                            <div class="row clearfix">
                                                <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">Coupon Discount {{isset($orderData->promocode)?"[ ".$orderData->promocode." ]":''}}</div>
                                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">-{{ $curSymbol.number_format($orderData->promocode_discount,2) }}</div>
                                            </div>

                                        @endif

                                        @if($orderData->additional_charge)
                                            <div class="row clearfix">
                                                <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">{{$orderData->additional_charge_name}}</div>
                                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">{{ $curSymbol.number_format($orderData->additional_charge,2) }}</div>
                                            </div>
                                        @endif
                                        @if($orderData->order_type =='delivery' && $action != 'gift_card')
                                            <div class="row clearfix">
                                                <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">Delivery Fee</div>
                                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">{{ $curSymbol.number_format($orderData->delivery_charge,2) }}</div>
                                            </div>

                                        @endif

                                        <div class="row clearfix">
                                            <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">Tax</div>
                                            <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">{{ $curSymbol.number_format($orderData->tax,2) }}</div>
                                        </div>

                                        @if($orderData->service_tax)
                                            <div class="row clearfix">
                                                <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">Service Tax</div>
                                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">{{ $curSymbol.number_format($orderData->service_tax,2) }}</div>
                                            </div>

                                        @endif
                                        @if($orderData->tip_amount!=0.00)
                                            <div class="row clearfix">
                                                <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">Tip</div>
                                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">{{ $curSymbol.number_format($orderData->tip_amount,2) }}</div>
                                            </div>

                                        @endif
                                        @if($total_refunded_amount > 0 )
                                            <div class="row clearfix">
                                                <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">Refund</div>
                                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">{{ $curSymbol.number_format($total_refunded_amount,2) }}</div>
                                            </div>


                                        @endif




                                    </div>
                                    <!-- sub total row -->

                                    <div class="row clearfix totalRow">
                                        <div class="col-sm-10 col-md-10 col-lg-10 col-xs-8">
                                            TOTAL
                                        </div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-4 bold">
                                            {{ $curSymbol.number_format($orderData->total_amount,2) }}
                                        </div>
                                    </div>
                                    <!-- TOTAL row -->
                                </div>
                                <!-- row -->
                                <div id="hidemob" class="row margin-bottom-20">
                                    <div class="change-time-popup text-center col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1"
                                         style="display: none;" id="timeslot">
                                        <!--   <a href="javascript:void(0);" class="a_nd_close discard-btn">+</a>-->
                                        {{--<p style="font-size: 16px;font-weight: 700;">
                                            Change {{ ucfirst($orderData->order_type) }} Time </p>--}}
                                        <p class="curr_date" curr_date="2018-06-22 07:13:27" style="display: none;"></p>
                                        <div style="margin: 10px 0 28px;">
                                            <input type="hidden" id="slot_order_id" value="{{ $orderData->id }}">
                                            <b btn-id="14876" icon-type="minus_btn" class="icon-change-tym"></b>


                                            {{--<div class="row">
                                                <div class="col-xs-12 flex-box flex-direction-row flex-align-item-center flex-justify-center overflow-visible margin-top-30">
                                                    <div class="font-weight-700">When will the order be ready?</div>
                                                    <div class="selct-picker-plain padding-left-10">
                                                        <select class="selectpicker" data-size="5" data-style="no-background-with-buttonline padding-left-5 font-weight-600" name="update_days" id="update_days">
                                                            @for ($i = 0; $i <= 30; $i++)
                                                                <option value="{{ $i }}">{{ $i }} days</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="selct-picker-plain padding-left-10">
                                                        <select class="selectpicker" data-size="5" data-style="no-background-with-buttonline padding-left-5 font-weight-600" name="update_hour" id="update_hour">
                                                            @for ($i = 0; $i <= 24; $i++)
                                                                <option value="{{ $i }}">{{ $i }} hrs</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="selct-picker-plain padding-left-10">
                                                        <select class="selectpicker" data-size="5" data-style="no-background-with-buttonline padding-left-5 font-weight-600" name="update_min" id="update_min">
                                                            @for ($i = 0; $i <= 59; $i++)
                                                                <option value="{{ $i }}">{{ $i }} mins</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>--}}

                                            <div class="row b_date_time_party margin-top-20">

                                                <div class="col-sm-6 padding-right-25">
                                                    <div class="row input__fullWidth">
                                                        <label for="" class="text-left datepicker-label-width">Date</label>
                                                        <div class="datepicker-calendar-width">
                                                            <input class="input__date start__date bore-none text-right padding-right-15"
                                                                   type="text" id="updateOrders_date" name="delivery_date"
                                                                   readonly="readonly" placeholder="" required=""
                                                                   onfocus="this.blur()">
                                                            <div class="fa icon-calendar" aria-hidden="true"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Waiting Time-->
                                                <div class="col-sm-6 padding-left-25">
                                                    <div class=" row input__fullWidth">
                                                        <label class="text-left datepicker-label-width">
                                                            Time
                                                        </label>
                                                        <div class="pull-right text-right datepicker-calendar-width">
                                                            <input type="text"
                                                                   class="edit__field text-right padding-right-30 timepicker reservation-book text-uppercase no-padding-top"
                                                                   value="" name="updateOrders_time" placeholder="HH/MM"
                                                                   id="updateOrders_time" onkeypress="return false"/>
                                                            <i class="fa fa-angle-down" aria-hidden="true" style="top:6px"></i>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                        <div class="margin-bottom-30 margin-top-10 fullWidth">
                                            <a class="btn btn__primary min-width-180" href="javascript:changeTimeSlot()"
                                               id="mange_btn-14876">Update</a>
                                            <a class="btn btn__holo min-width-180 margin-left-10" href="javascript:void(0)"
                                               onclick="myFunction()">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix ctaRow">
                                    <div class="">


                                        <div class="row margin-20-desktop tablet-flex">


                                            <div class="col-sm-12 print__button text-right no-padding">
                                               {{-- @if($currentTimestamp > $deliveryTimestamp && $show_log_btn == false && $orderData->product_type == 'food_item' && ( ($orderData->status =='placed') || ($orderData->status =='ready') || ($orderData->status =='confirmed') ))
                                                    <button type="button"
                                                            class="btn  btn__primary  action_btn min-width-180 min-sm-width-150 font-weight-600  margin-left-10 ordrview-popup">{{ 'View' }}</button>
                                                    <!-- Caption Needs to be changed RG-->
                                                @endif--}}
                                                {{--<button type="button" data-toggle="modal" class="btn btn_green">Confirm</button>--}}
                                                @if(($action=="product") || ($deliveryTimestamp>=$currentTimestamp))

                                                    @if(($status !="cancelled") || ($status !="rejected"))

                                                        @if($status =="confirmed")
                                                            @if(!$is_order_scheduled)
                                                                @can('order-confirm')
                                                                <button type="button"
                                                        class="btn confirm-tablet  btn_green  action_btn min-width-180 min-sm-width-150 font-weight-600  margin-left-10 status-confirm"
                                                        onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Confirm Order' }}</button>
                                                                @endcan
                                                            @endif

                                                            <!-- @if($orderData->manual_update==1 && $orderData->order_type=="delivery")
                                                                <button type="button" class="btn  btn__primary action_btn min-width-180 min-sm-width-150 font-weight-600 margin-left-10 status-confirm" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Manual Confirm' }}</button>
                                            @else
                                                                <button type="button" class="btn  btn__primary  action_btn min-width-180 min-sm-width-150 font-weight-600  margin-left-10 status-confirm" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Confirm Order' }}</button>
                                            @endif-->
                                                                <?php
                                                                //$title = "Confirm";
                                                                //if($orderData->manual_update==1 && $orderData->order_type=="delivery")
                                                                //$title = "Manual Confirm"; ?>
                                                            @endif
                                                                @can('order-sent')
                                                            @if($status =="sent" && $is_order_scheduled == 0)
                                                                <button type="button" class="btn  btn__primary   action_btn min-width-180 min-sm-width-150 font-weight-600  margin-left-10 status-sent" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Sent' }}</button>
                                                            @endif
                                                                @endcan
                                                                @can('order-ready')
                                                            @if($status =="ready" && $is_order_scheduled == 0)
                                                                <button type="button" class="btn  btn__primary   action_btn min-width-180 min-sm-width-150 font-weight-600 margin-left-10 status-ready" onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">{{ 'Ready' }}</button>
                                                            @endif
                                                                @endcan
                                                                @can('order-pickedup')
                                                            @if(($status == "archived") && ($orderData->order_type=="carryout") && $is_order_scheduled == 0)
                                                                <button type="button" class="btn  btn__primary action_btn min-width-180 min-sm-width-150 font-weight-600 margin-left-10 status-picked-up " onclick="javascript:doAction(<?php echo $orderData->id ?> , '<?php echo $status;?>');">Picked Up</button>

                                                            @endif
                                                                @endcan

                                                            @if(($status == "archiv") && ($orderData->order_type=="carryout"))
                                                                <!-- <button type="button" class="btn  btn__print" onclick="javascript:printData()">Print</button>-->
                                                            @endif

                                                            @if(($status == "archived") && ($orderData->order_type=="delivery"))
                                                                <!--<button type="button" class="btn  btn__print" onclick="javascript:printData()">Print</button>-->
                                                                @endif
                                                            @endif
                                                        @endif

                                                        @can('refund')
                                                            @if($orderData->status !='refunded' && $orderData->total_amount > 0)
                                                            <button type="button" data-toggle="modal" data-target="#refundOrder"
                                                                    class="btn  box-shadow action_btn min-width-180 min-sm-width-150 font-weight-600 margin-left-10 btn_pink">
                                                                Refund Order
                                                            </button>
                                                        @endif
                                                        @endcan

                                                        @if(($orderData->status =='placed') || ($orderData->status =='ready') || ($orderData->status =='confirmed'))

                                                            @if($action=="food_item")
                                                                @can('order-update-time')
                                                                                                      <button type="button"
                                                    class="btn  btn_grey box-shadow action_btn min-width-180 min-sm-width-150 font-weight-600 margin-left-10 openupdatetime"
                                                    data-toggle="modal" onclick="myFunction()">Update Time Slot
                                            </button>
                                                                    @endcan
                                                            @endif
                                                                @can('order-reject')
                                                            <button type="button"
                                                class="btn  btn_red min-width-180 min-sm-width-150 font-weight-600 margin-left-10  box-shadow"
                                                data-toggle="modal" data-target="#rejectOrderModal">Cancel/Reject Order
                                        </button>
                                                                @endcan
                                                            <!-- <button type="button" class="btn  btn__cancel action_btn min-width-180 min-sm-width-150 font-weight-600 margin-left-10" data-toggle="modal" data-target="#cancelOrderModal">Cancel Order</button>-->

                                                        @endif

                                                        {{-- @if($show_log_btn == true )
                                                             <button type="button"
                                                                     class="btn btn__primary action_btn font-weight-600 margin-left-10"
                                                                     onclick="window.location='{{ URL::to('user_order/' . $orderData->id .'/'. $action.'/logs') }}'">
                                                                 Log
                                                             </button>
                                                         @endif--}}



                                            </div>
                                        </div>

                                        {{--
                                        <button type="button" data-toggle="modal" class="btn btn_red">Cancel/Reject</button>
--}}
                                    </div>


                                </div>
                                <!-- cta row -->




                                @if($refund_reason)


                                    <div class="row clearfix firstrow cmntrow">
                                        <div class="col-xs-12 tar">
                                            <strong>Cancelation/Refund Comments:</strong>
                                           <div id="reason">
                                               <?php
                                               if($orderData->restaurants_comments){
                                               }
                                               ?>
                                            <?php foreach($refund_reason as $refund){?>
                                            <div class="reasonRow" >
                                                <p
                                                        class="col-md-10 col-xs-10 ">{{$refund['reason']}}</p>
                                                <p class="col-md-2 col-xs-2 text-truncate text-right">
                                                    {{ $curSymbol.number_format($refund['amount'],2) }}</p>
                                            </div>
                                            <?php } ?>
                                            </div>
                                            {{--
                                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                            --}}
                                        </div>
                                    </div>
                            @endif

                            <!-- cta row -->

                            </div>
                        </div>
                        <!-- new page -->

<!--
                    <div class="row order__item-container margin-top-20 order-summary-section border-box fullWidth" style="float: left;">
                        {{--<div class="col-xs-1 col-md-1 hide-sm"></div>--}}
                        <div class="col-lg-12 col-lg-offset-0 col-sm-12 od__details box-shadow">
                            @php
                                $testOrderClass = '';
                                $testOrderAria = false;
                                $testOrderVal = 0;
                                if($orderData->test_order) {
                                    $testOrderClass = 'active';
                                    $testOrderAria = true;
                                    $testOrderVal = 1;
                                }
                            @endphp
                            <div class="row">
                                <div class="col-sm-8 label__switchHead" style="padding-top: 8px;"><strong>Test Order</strong></div>
                                <div class="col-sm-4 label__switchToggle">
                                    <div class="waitList__otp no-padding" style="float: right;">
                                        <div class="toggle__container">
                                            <button type="button"
                                                    class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom {{ $testOrderClass }}"
                                                    id="test_order_btn" data-toggle="button" aria-pressed="{{ $testOrderAria }}"
                                                    autocomplete="off">
                                                <div class="handle"></div>
                                            </button>
                                            <input type="hidden" name="test_order" id="test_order" class="label_field"
                                                   value="{{ $testOrderVal }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->

                </div>

                <div class="footer_orderpopup reservation_det">

                    {{--<div class="run-behind">Order is Running Behind The Time</div>--}}

                    <div class="archive-msg"></div>



                    <div class="modal modal-popup fade" id="cancelOrderModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" data-backdrop="static">
                        <div class="modal-dialog vertical-center" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel">Enter Your Comments</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="reject_order_list">
                                        <input type="hidden" id="cancel_order_id" value="{{ $orderData->id }}">
                                        <input type="hidden" id="cancel_status" value="cancelled">
                                        <div class="custom__message-textContainer no-margin">
                                            <textarea placeholder="Reason for Cancellation" maxlength="300" rows="5"
                                                      id="cancel_order_reason"></textarea>
                                        </div>
                                        <b class="error-canc hide color-red">Please enter Reason to Cancel Order.</b>
                                    </div>
                                    {{--<div class=" text-center">
                                        <!-- <button type="button" class="btn btn-default  " data-dismiss="modal">Go Back</button> -->
                                        <button type="button" class="btn  btn__cancel"
                                                onclick="javascript:cancelOrder();">Cancel Order
                                        </button>
                                    </div>--}}
                                </div>
                                <div class="row margin-bottom-30 text-center">
                                    <button type="button" class="btn btn__cancel font-weight-600"
                                            onclick="javascript:cancelOrder();">Cancel Order
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>


                    <!-- reject model -->
                    <div class="modal modal-popup fade " id="rejectOrderModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" data-backdrop="static">
                        <div class="modal-dialog vertical-center" role="document">

                            <div class="modal-content white-smoke">
                                <div class="modal-header white-smoke">
                                    <h4 class="modal-title" id="exampleModalLabel">Cancel / Reject Order</h4>
                                    <a class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>

                                </div>
                                <div class="modal-body">

                                    <div class="row">

                                        <div class="">
                                            <div class="col-lg-12 text-center ">
                                                <p class="margin-bottom-20"><label class="padd-left-18 font-weight-800"
                                                                                   for="">Please Select one of the
                                                        following</label></p></div>
                                            <div class="col-lg-6 margin-bottom-20">
                                                <div class="tbl-radio-btn">
                                                    <input type="radio" name="fontSizeControl" id="sizeSmall"
                                                           value="small" rel="cancelled" class="reject_status">
                                                    <label class="text-color-grey" for="sizeSmall">Customer called to
                                                        cancel the order</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 margin-bottom-20">
                                                <div class="tbl-radio-btn">
                                                    <input type="radio" name="fontSizeControl" id="sizeSmall1"
                                                           value="small" / checked="checked" rel = "rejected"
                                                    class="reject_status">
                                                    <label class="text-color-grey" for="sizeSmall1">Restaurant can not
                                                        fulfill this order</label>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="reject_order_list">
                                        <input type="hidden" id="reject_order_id" value="{{ $orderData->id }}">


                                        <div class="custom__message-textContainer no-margin col-lg-12 margin-bottom-20">
                                                <textarea class="border-radius-none"
                                                          placeholder="Enter reason for Cancellation / Rejection"
                                                          maxlength="300" rows="5"

                                                          id="reject_order_reason"></textarea>
                                        </div>
                                        <b class="error-canc hide color-red">Enter reason for Cancellation /
                                            Rejection</b>
                                    </div>
                                    {{--<div class="text-center">
                                        <!-- <button type="button" class="btn btn-default  " data-dismiss="modal">Go Back</button> -->
                                        <button type="button" class="btn  btn__reject"
                                                onclick="javascript:rejectOrder();">Reject Order
                                        </button>
                                    </div>--}}
                                </div>

                                <div class="row margin-bottom-30 text-center">

                                    <button type="button" class="btn btn__reject font-weight-600 margin-bottom-30"
                                            onclick="javascript:rejectOrder();">Confirm
                                    </button>

                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end -->


                    <!-- Request Timer Model -->
                    <div class="modal modal-popup fade" id="requestTimer" tabindex="-1" role="dialog"
                         aria-labelledby="requestTimerLabel" data-backdrop="static">
                        <div class="modal-dialog vertical-center" role="document">

                            <div class="modal-content white-smoke padding-bottom-20">
                                <div class="modal-header white-smoke">
                                    <a class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
                                </div>
                                <div class="modal-body margin-bottom-30">
                                    <div class="row">
                                        <div class="col-xs-12 margin-bottom-30">
                                            <div class="col-xs-12 text-center font-weight-700 font-size-16 margin-bottom-20">
                                                Requested time for delivery/takout has elapsed for this order.<br/>
                                                Please tap on one of the buttons below to proceed:
                                            </div>
                                            <div class="fullWidth flex-box overflow-visible">
                                                <div class="col-xs-6 flex-box flex-direction-column flex-justify-space-between">
                                                    <div>
                                                        <p class="text-color-grey padding-bottom-20">This order has been
                                                            completed and the customer no longer needs to be
                                                            notified.</p>
                                                    </div>
                                                    <a href="#"
                                                       class="btn_ btn__primary text-center updatetimeslotpopuparchive">Archive
                                                        Order</a>
                                                </div>

                                                <div class="col-xs-6 flex-box flex-direction-column flex-justify-space-between">
                                                    <div>
                                                        <p class="text-color-grey padding-bottom-20">This order is still
                                                            being processed and the customer needs to be notified about
                                                            the delay.</p>
                                                    </div>
                                                    <a href="#"
                                                       class="btn_ btn__primary text-center updatetimeslotpopup">Update
                                                        Time</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end -->


                    <div id="r_msgContainer_popup"></div>
                </div>
        </div>

        <!-- Print order details data -->

        <div style="display:none;">
            <table class="receipt" style="width:100%;font-family:arial;margin:0 auto;font-size:13px;"
                   cellpadding="0" cellspacing="0" id="printTable">
                <tr>
                    <td valign="top">
                        <table width="100%">
                            <tr>
                                <td align="center" style="border-bottom:1px solid #000;padding:10px 0;">
                                    <p style="margin:10px 0;font-size:14px;">{{$restaurantDetails['restaurant_name']}}
                                        Restaurant</p>
                                    <p style="margin: 0;font-size:14px;">{{$restaurantDetails['address']}} {{ $restaurantDetails['street'] }}
                                        , {{$restaurantDetails['zipcode']}}</p>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:5px 0 3px;text-align:left;">
                                    <p style="font-size:14px;margin:5px 0;">Receipt No.
                                        : {{$orderData->payment_receipt}}</p>
                                <!--<p style="font-size:14px;margin:5px 0;">Order Id: {{$orderData->id}}</p>-->
                                    @if( $orderData->product_type!='product')

                                        <p style="font-size:14px;margin:5px 0;">Order
                                            Type:  {{ $orderData->order_type=='delivery'?'Delivery':'Takeout' }}</p>
                                        <p style="font-size:14px;margin:5px 0;">Payment
                                            Type: {{$orderData->card_type!="Cash"?'Card':'Cash'}}</p>
                                    @endif
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <p style="font-size:14px;margin:5px 0;">Date
                                                    : <?php echo (new DateTime($orderData->delivery_date))->format('l d M, Y');?></p>
                                            </td>
                                            <td align="right">
                                                @if( $orderData->product_type!='product')
                                                    <p style="font-size:14px;margin:5px 0;">
                                                        Time: <?php echo (!empty($orderData->delivery_time) && $orderData->delivery_time!='null' && $orderData->delivery_time!=null)?(new DateTime($orderData->delivery_time))->format('h:i A'):'';?></p>

                                                @endif
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td style="border-top:1px solid #000;border-bottom:1px solid #000;padding:5px 0;">
                                    <table width="100%" cellspacing="2" cellpadding="0">

                                        <tr>
                                            <td colspan="3">
                                                <table class="print_items" width="100%" cellspacing="0"
                                                       cellpadding="0">
                                                    @if(isset($orderDetailData) && count($orderDetailData)>0)
                                                        @foreach($orderDetailData as $oData)
                                                            <?php $is_label_present = array();?>
                                                            <tr>
                                                                <td>
                                                                    <p style="font-size:14px;margin:5px 0;">{{ $oData->item }}</p>

                                                                </td>
                                                                <td align="center" valign="top" width="50">
                                                                    <p style="font-size:14px;margin:5px 0;">{{ $oData->quantity }}</p>
                                                                </td>
                                                                <td align="right" valign="top" width="70">
                                                                    <p style="font-size:14px;margin:5px 0;">{{ $curSymbol.number_format($oData->quantity*$oData->unit_price ,2) }}</p>

                                                                </td>
                                                            </tr>
                                                            @if( $orderData->product_type=='product')
                                                                @if(!is_null( $oData->item_other_info))
                                                                    @php
                                                                        $item_other_info=json_decode($oData->item_other_info);
                                                                    @endphp
                                                                    @if(isset($item_other_info->gift_wrapping_fees) && ($item_other_info->is_gift_wrapping==1) && $item_other_info->gift_wrapping_fees>0)


                                                                        <tr style="font-size:11px;line-height:18px;">
                                                                            <td style="padding-left:10px">
                                                                                <p style="font-size:11px;margin:-5px 0 0;">
                                                                                    Gift Wrapping</p>
                                                                            </td>
                                                                            <td align="center" valign="top"
                                                                                width="50">

                                                                            </td>
                                                                            <td align="right" valign="top"
                                                                                width="70">{{ $curSymbol.$oData->quantity * $item_other_info->gift_wrapping_fees}}
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                    @if(isset($item_other_info->gift_wrapping_message) && !empty($item_other_info->gift_wrapping_message) && $item_other_info->gift_wrapping_message!='null')

                                                                        <tr style="font-size:11px;line-height:18px;">
                                                                            <td style="padding-left:10px">
                                                                                <p style="font-size:11px;margin:-5px 0 0;">
                                                                                    Message
                                                                                    :{{$item_other_info->gift_wrapping_message}}</p>
                                                                            </td>
                                                                            <td align="center" valign="top"
                                                                                width="50">

                                                                            </td>
                                                                            <td align="right" valign="top"
                                                                                width="70">

                                                                            </td>
                                                                        </tr>


                                                                    @endif
                                                                @endif

                                                            @else


                                                                @if(isset($oData->addon_modifier) && count($oData->addon_modifier)>0)
                                                                    @php
                                                                        $addon_modifier=$oData->addon_modifier;
                                                                    @endphp
                                                                    @if($oData->is_byp==0 && strtolower($oData->item_size) != 'null' && $oData->item_size != "NA" && !empty($oData->item_size) && !is_null($oData->item_size))
                                                                        <tr style="font-size:11px;line-height:18px;">
                                                                            <td style="padding-left:10px"><b>Size: </b>{{ $oData->item_size }}</td>
                                                                        </tr>
                                                                    @endif
                                                                    @if(isset($addon_modifier['modifier_data']) && count($addon_modifier['modifier_data']))

                                                                        @php
                                                                            $modifier_data_records=$addon_modifier['modifier_data'];
                                                                            $addonDataArrary = array();
                                                                            $i = 0;
                                                                        @endphp
                                                                        {{--<div class="food_order_item padding-left-25"> Modifiers :-</div>--}}

                                                                        @foreach($modifier_data_records as $modifier_data)

                                                                            @php
                                                                                $groupname='';
                                                                                $price=0;
                                                                                $quantity=1;
                                                                                  if(isset($modifier_data['quantity']) && !empty($modifier_data['quantity'])){
                                                                                                 $quantity=$modifier_data['quantity'];
                                                                                   }
                                                                                    if(isset($modifier_data['price']) && !empty($modifier_data['price'])){
                                                                                                 $price=$modifier_data['price'];
                                                                                   }
                                                                            @endphp
                                                                            @if(isset($modifier_data['group_prompt']) && !empty($modifier_data['group_prompt']))
                                                                                @php
                                                                                    $groupname = $modifier_data['group_prompt'];
                                                                                    $addonDataArrary[$groupname][$i]['name'] = $modifier_data['modifier_name'];
                                                                                    $addonDataArrary[$groupname][$i]['quantity'] = $quantity;
                                                                                    $addonDataArrary[$groupname][$i]['price'] = $price;
                                                                                @endphp
                                                                            @endif


                                                                            {{--<tr style="font-size:11px;line-height:18px;">
                                                                                <td style="padding-left:10px" >
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        + {{ @$modifier_data['modifier_name'] }} <b>{{ @$groupname}}</b>                                                                        <span style="padding-left: 600px">
                                                                                        <span>
                                                                                    </p>
                                                                                </td>
                                                                                <td align="center" valign="top"
                                                                                >
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        {{$quantity}}
                                                                                    </p>
                                                                                </td>
                                                                                <td align="right" valign="top"
                                                                                >
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        {{$quantity}} *{{ $curSymbol. number_format($price * $quantity,2) }}
                                                                                    </p>
                                                                                </td>
                                                                            </tr>--}}
                                                                            @php
                                                                                $i++;
                                                                            @endphp
                                                                        @endforeach
                                                                        <?php
                                                                        //echo '<pre>';print_r($addonDataArrary);
                                                                        foreach($addonDataArrary as $key => $value){
                                                                            $abc = '';
                                                                            $pArr = array();
                                                                            $abc = '<b>'.$key.': </b>';
                                                                            foreach($value as $innerArr){
                                                                                if($innerArr['quantity'] > 1){
                                                                                    $pArr[] = $innerArr['name'].' X'.$innerArr['quantity'];
                                                                                }else{
                                                                                    $pArr[] = $innerArr['name'];
                                                                                }
                                                                            }
                                                                            echo '<tr style="font-size:11px;line-height:18px;">
                                                                                        <td style="padding-left:10px" >'.$abc.implode(', ', $pArr).'</td>
                                                                                    </tr>';
                                                                        }
                                                                        ?>
                                                                    @elseif(isset($addon_modifier['addons_data']) && count($addon_modifier['addons_data']))
                                                                        @php
                                                                            $addons_data_records=$addon_modifier['addons_data'];
                                                                            $addonDataArrary = array();
                                                                            $i = 0;
                                                                        @endphp

                                                                        @foreach($addons_data_records as $addons_data)

                                                                            @php
                                                                                $groupname='';
                                                                            @endphp
                                                                            @if(isset($addons_data['addongroup']) && !empty($addons_data['addongroup']))
                                                                                @php
                                                                                    $groupname = $addons_data['addongroup']['prompt'];
                                                                                    $addonDataArrary[$groupname][$i]['name'] = $addons_data['option_name'];
                                                                                    $addonDataArrary[$groupname][$i]['quantity'] = $addons_data['quantity'];
                                                                                    $addonDataArrary[$groupname][$i]['price'] = $addons_data['price'];
                                                                                @endphp
                                                                            @endif

                                                                            {{--<tr style="font-size:11px;line-height:18px;">
                                                                                <td style="padding-left:10px" >
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        +  {{ @$addons_data['option_name'] }} <b>{{ @$groupname}} </b>                                                                        <span style="padding-left: 600px">
                    <!--
                                                                                {{$addons_data['price']}}*   {{$addons_data['quantity']}} =
                                                                                  {{$curSymbol. number_format($addons_data['price'] * $addons_data['quantity'],2) }}
                                                                              -->
                        <span>
                                                                                    </p>
                                                                                </td>
                                                                                <td align="center" valign="top"
                                                                                >
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        {{$addons_data['quantity']}}
                                                                                    </p>
                                                                                </td>
                                                                                <td align="right" valign="top"
                                                                                >
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        {{$addons_data['quantity']}} *    {{$curSymbol. number_format($addons_data['price'] * $addons_data['quantity'],2) }}
                                                                                    </p>
                                                                                </td>
                                                                            </tr>--}}
                                                                            @php
                                                                                $i++;
                                                                            @endphp
                                                                        @endforeach
                                                                        <?php
                                                                        //echo '<pre>';print_r($addonDataArrary);
                                                                        foreach($addonDataArrary as $key => $value){
                                                                            $abc = '';
                                                                            $pArr = array();
                                                                            $abc = '<b>'.$key.': </b>';
                                                                            foreach($value as $innerArr){
                                                                                if($innerArr['quantity'] > 1){
                                                                                    $pArr[] = $innerArr['name'].' X'.$innerArr['quantity'];
                                                                                }else{
                                                                                    $pArr[] = $innerArr['name'];
                                                                                }
                                                                            }
                                                                            echo '<tr style="font-size:11px;line-height:18px;">
                                                                                        <td style="padding-left:10px" >'.$abc.implode(', ', $pArr).'</td>
                                                                                    </tr>';
                                                                        }
                                                                        ?>
                                                                    @endif

                                                                @endif
                                                                @if(isset($oData->addOnsDetail) && count($oData->addOnsDetail)>0)

                                                                    @if($oData->is_byp==0)
                                                                        @foreach($oData->addOnsDetail as $oAData)
                                                                            <?php
                                                                            if(isset($oAData['addons_label']) && !in_array($oAData['addons_label'], $is_label_present)){
                                                                            $is_label_present[] = $oAData['addons_label'];
                                                                            ?>
                                                                            <tr style="font-size:11px;line-height:18px;">
                                                                                <td style="padding-left:10px"
                                                                                    colspan="3">
                                                                                    <strong style="font-size:12px;margin:-5px 0 0;">
                                                                                        {{ @$oAData['addons_label'] }}</strong>
                                                                                </td>
                                                                            </tr>
                                                                            <?php } ?>
                                                                            <tr style="font-size:11px;line-height:18px;">
                                                                                <td style="padding-left:10px">
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        + {{ @$oAData['addons_name'] }}</p>
                                                                                </td>
                                                                                <td align="center" valign="top"
                                                                                    width="50">
                                                                                    {{$oData->quantity}}
                                                                                </td>
                                                                                <td align="right" valign="top"
                                                                                    width="70">
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        {{ $curSymbol. number_format(@$oAData['price'] * $oData->quantity,2) }}
                                                                                    </p>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                    @if($oData->is_byp==1)
                                                                        @foreach($oData->addOnsDetail as $key=>$dataval)

                                                                            <tr style="font-size:11px;line-height:18px;">
                                                                                <td style="padding-left:10px">
                                                                                    <b>{{ str_replace('-', ' ', str_replace('_', ' ', $key)) }}</b>
                                                                                    <p style="font-size:11px;margin:-5px 0 0;">
                                                                                        <?php echo  $dataval; ?> </p>
                                                                                </td>
                                                                                <td align="center" valign="top"
                                                                                    width="50">
                                                                                    <!--<p style="font-size:11px;margin:-5px 0 0;"></p>-->
                                                                                </td>
                                                                                <td align="right" valign="top"
                                                                                    width="70">
                                                                                    <!--    <p style="font-size:11px;margin:-5px 0 0;"></p>-->
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif

                                                                @endif
                                                            @endif

                                                            @if( $orderData->product_type=='food_item')


                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                        <table width="100%">

                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td style="font-size: 14px" align="left" width="90%">Subtotal:</td>
                                            <td style="font-size: 14px"
                                                align="right">{{ $curSymbol.number_format($orderData->order_amount,2)}}</td>
                                        </tr>

                                        <!--<tr>
                                          <td style="font-size: 14px" align="left">Deal Discount:</td>
                                          <td style="font-size: 14px" align="right">$0.00</td>
                                        </tr>
                                        <tr>
                                          <td style="font-size: 14px" align="left">Promocode Discount:</td>
                                          <td style="font-size: 14px" align="right">$0.00</td>
                                        </tr>-->
                                        @if($orderData->flat_discount)
                                            <tr>
                                                <td style="font-size: 14px" align="left">Discount:</td>
                                                <td style="font-size: 14px"
                                                    align="right">
                                                    -{{ $curSymbol.number_format($orderData->flat_discount,2)}}</td>
                                            </tr>
                                        @endif

                                        @if($orderData->promocode_discount)
                                            <tr>
                                                <td style="font-size: 14px" align="left">Coupon Discount:</td>
                                                <td style="font-size: 14px"
                                                    align="right">
                                                    -{{ $curSymbol.number_format($orderData->promocode_discount,2)}}</td>
                                            </tr>
                                        @endif
                                        @if($orderData->additional_charge)
                                            <tr>
                                                <td style="font-size: 14px"
                                                    align="left">{{$orderData->additional_charge_name}}:
                                                </td>
                                                <td style="font-size: 14px"
                                                    align="right">{{ $curSymbol.number_format($orderData->additional_charge,2)}}</td>
                                            </tr>
                                        @endif


                                        @if(($orderData->order_type=="delivery") && ($orderData->delivery_charge!=0.00))
                                            <tr>
                                                <td style="font-size: 14px" align="left">Delivery Charge:</td>
                                                <td style="font-size: 14px"
                                                    align="right">{{ $curSymbol.number_format($orderData->delivery_charge,2)}}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td style="font-size: 14px" align="left">Tax:</td>
                                            <td style="font-size: 14px"
                                                align="right">{{ $curSymbol.$orderData->tax}}</td>
                                        </tr>
                                        @if($orderData->service_tax)
                                            <tr>
                                                <td style="font-size: 14px" align="left">Service Tax:</td>
                                                <td style="font-size: 14px"
                                                    align="right">{{ $curSymbol.number_format($orderData->service_tax,2)}}</td>
                                            </tr>
                                        @endif

                                        @if($orderData->tip_amount!=0.00)
                                            <tr>
                                                <td style="font-size: 14px" align="left">Tip:</td>
                                                <td style="font-size: 14px"
                                                    align="right">{{ $curSymbol.number_format($orderData->tip_amount,2)}}</td>
                                            </tr>
                                        @endif


                                        @if($total_refunded_amount > 0 )
                                            <tr>
                                                <td style="font-size: 14px" align="left">Refunded Amount:</td>
                                                <td style="font-size: 14px"
                                                    align="right"> {{ $curSymbol.number_format($total_refunded_amount,2) }}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td height="5">&nbsp;</td>

                                        </tr>

                                        <tr>
                                            <td colspan="2">
                                                <table width="100%"
                                                       style="border-top:1px solid #000;border-bottom:1px solid #000;padding: 10px 0;">
                                                    <tr>
                                                        <td style="font-size: 14px" align="left">Total:</td>
                                                        <td style="font-size: 14px"
                                                            align="right">{{ $curSymbol.number_format($orderData->total_amount,2)}}</td>

                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px">
                        <p style="font-size:14px;margin:5px 0;text-align: center">See You soon!!</p>
                    </td>

                </tr>
            </table>

        </div>
        <!-- End Print data -->
        @else
            <div class="row"
                 style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                No Record Found
            </div>
        @endif

    </div>

</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.timepicker.css')}}"/>

<script type="text/javascript">
    $('.navbar-laravel').addClass('hidden-print');
    $('.sidebar').addClass('hidden-print');
    function cancelOrder() {
        var order_id = $("#cancel_order_id").val();
        var reason = $("#cancel_order_reason").val();
        var status = $("#cancel_status").val();
        if (order_id && reason != '') {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#loader').removeClass('hidden');
            $.ajax({
                type: 'POST',
                url: '/order/cancel',
                data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&reason=' + reason + '&status=' + status,
                success: function (){
                    $('.action_btn').remove();
                    $('.cancel-preview').removeClass('hide');
                   // $('#reason').html(reason);
                    $('#cancelOrderModal').modal('toggle');
                    $('#loader').addClass('hidden');
                    // setTimeout(function () {// wait for 5 secs(2)
                    //     location.reload(); // then reload the page.(3)
                    // }, 100);
                }
            });
        }
        else {
            $('.error-canc').removeClass('hide');
        }
    }
    function rejectOrder() {
        var order_id = $("#reject_order_id").val();
        var re_reason = $("#reject_order_reason").val();
        var re_status = $(".reject_status:checked").attr('rel');
        if (order_id && re_reason != '') {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#loader').removeClass('hidden');
            $.ajax({
                type: 'POST',
                url: '/order/cancel',
                data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&reason=' + re_reason + '&status=' + re_status,
                success: function () {
                    $('.action_btn').remove();
                    $('.cancel-preview').removeClass('hide');
                   // $('#reason').html(reason);
                    $('#rejectOrderModal').modal('toggle');
                    $('#loader').addClass('hidden');
                    setTimeout(function () {// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 100);
                }
            });
        }
        else {
            $('.error-canc').removeClass('hide');
        }
    }
    function doAction(order_id, action) {
        if (order_id && action) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#loader').removeClass('hidden');
            $(".btn__print").hide();
            $.ajax({
                type: 'PUT',
                url: '/order/' + order_id + '/' + action,
                data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&action=' + action,
                success: function (data) {
                    $('.action_btn').remove();
                    $('#loader').addClass('hidden');
                    if (data["message"] != undefined) {
                        //var div = document.getElementById('errormess');
                        //div.innerHTML = '<span style="color:red;">'+data["message"]+'</span>';
                        //$('#errormess').show();
                        alertbox('Error', data["message"], function (modal) {
                            modal.on("hidden.bs.modal", function (e) {
                                setTimeout(function () {
                                    location.reload();
                                }, 100);
                            });
                        });
                    }
                    else {
                        setTimeout(function () {
                            location.reload();
                        }, 100);
                    }
                }
            });
        }
    }
    function printData() {
        var order_id = $("#slot_order_id").val();
        if (order_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#loader').removeClass('hidden');
            $.ajax({
                type: 'POST',
                url: '/order/updateprint',
                data: '_token = <?php echo csrf_token() ?>&oid=' + order_id,
                success: function () {
                    $('#loader').addClass('hidden');
                }
            });
        }
        var divToPrint = document.getElementById("printTable");
        var winPrint = window.open('', '_blank');
        winPrint.document.write(divToPrint.outerHTML);
        winPrint.document.close();
        winPrint.focus();
        winPrint.print();
        winPrint.close();
        //location.reload();
    }
    function myFunction() {
        var x = document.getElementById("timeslot");
        if (x.style.display === "none") {
            x.style.display = "block";
            // $("html, body").animate({
            //     scrollTop: $("#timeslot").offset().top
            // }, 500);
        } else {
            x.style.display = "none";
        }
    }
    function changeTimeSlot() {
        var order_id = $("#slot_order_id").val();
        var sdate = $("#updateOrders_date").val();
        var stime = $("#updateOrders_time").val();
        if (stime !== undefined && stime != '') {
            if (order_id) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('#loader').removeClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: '/order/updatetimeslot',
                    data: '_token = <?php echo csrf_token() ?>&oid=' + order_id + '&sdate=' + sdate + '&stime=' + stime,
                    success: function () {
                        $('#loader').addClass('hidden');
                        setTimeout(function () {// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 100);
                    }
                });
            }
        } else {
            alertbox('Error', 'Please choose the time.', function (modal) {
                setTimeout(function () {
                    // $('#add-tabletype-modal').modal('hide');
                    // location.reload();
                }, 2000);
            });
        }
    }
    function goBack() {
        window.history.go(-1)
    }
    var pausecontent = []
    <?php foreach($result as $rest=>$dat){ ?>
    pausecontent.push('<?php echo date('d-m-Y', strtotime($dat))?>')
    //console.log(moments('<?php echo $dat; ?>').format('h'));
        <?php } ?>
    var enabledDays = ['18-10-2018', '10-12-2018'];
    function enableAll(date) {
        var sdate = $.datepicker.formatDate('dd-mm-yy', date);
        if ($.inArray(sdate, pausecontent) != -1) {
            return [true]; //
        }
        return [false];
    }
    $("#updateOrders_date").datepicker({
        showAnim: "slideDown",
        dateFormat: 'DD, M dd, yy',
        minDate: 0,
        beforeShowDay: enableAll,
        onSelect: function (e) {
            /* $(this).valid();*/
            $(this).trigger('change');
            $(this).addClass('addDate');
            //$('#updateOrders_time').timepicker('maxTime', '11:00');
            var selected_date = $(this).val();
            if (selected_date == '<?php echo $currentDate;?>') {
                var disabledTime = [['0am', '<?php echo $currentTime;?>']];
                $('#updateOrders_time').timepicker('option', 'disableTimeRanges', disabledTime);
            } else {
                var disabledTime = [['0am', '0am']];
                $('#updateOrders_time').timepicker('option', 'disableTimeRanges', disabledTime);
            }
        }
    }).attr('readonly', 'readonly').datepicker('setDate', 'today');
    $('#updateOrders_time').timepicker({
        interval: 15,
        step: '15',
        timeFormat: "h:i A",
        closeOnWindowScroll: true,
        'minTime': '0:00',
        'maxTime': '23:30',
        'disableTimeRanges': [
            ['0am', '<?php echo $currentTime;?>']
        ],
    }).on('click', function (e) {
        $(".ui-timepicker-wrapper").mCustomScrollbar({
            theme: "dark"
        });
    });
    //.timepicker('setTime', '<?php echo $currentTime;?>')
    $('#rejectOrderModal').on('hidden.bs.modal', function () {
        $('#rejectOrderModal #reject_order_reason').val('');
    });
    $('#cancelOrderModal').on('hidden.bs.modal', function () {
        $('#cancelOrderModal #cancel_order_reason').val('');
    });
    setTimeout(function () { // PE-2087
        //location.reload(); // then reload the page.(3)
    }, 60000);   // As discussed with Prakash, NKD quick updates
    //show popup when Order is running behind the time @RG 27-03-2019
    $(document).ready(function () {
        var show_popup = '<?php echo $show_archive_popup;?>';
        $('#test_order_btn').on('click', function () {
            console.log('test order clicked');
            var test_order = $('#test_order').val();
            var order_id = $("#slot_order_id").val();
            $.ajax({
                type: 'POST',
                url: '/testorder',
                data: {test_order: test_order, order_id: order_id},
                success: function () {
                    $('#loader').addClass('hidden');
                },
                error: function (data, textStatus, errorThrown) {
                    var err = '';
                    $.each(data.responseJSON.errors, function (key, value) {
                        err += '<p>' + value + '</p>';
                    });
                    alertbox('Error', err, function (modal) {
                    });
                }
            });
        });
        $(document).on('click', '.updatetimeslotpopup', function () {
            $("#requestTimer").modal("hide");
            $('.openupdatetime').trigger('click');
        });
        $(document).on('click', '.ordrview-popup', function () {
            $("#requestTimer").modal("show");
        });
        $(document).on('click', '.updatetimeslotpopuparchive', function () {
            var order_id = $("#slot_order_id").val();
            if (order_id) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('#loader').removeClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: '/order/move_to_archive',
                    data: '_token = <?php echo csrf_token() ?>&oid=' + order_id,
                    success: function () {
                        $('#loader').addClass('hidden');
                        setTimeout(function () {// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 100);
                    }
                });
            }
        });
        $(document).ready(function(){
            var boxWidth = $("#orderDetailBox").width();
            $(".slideLeft").click(function(){
                $("#orderDetailBox").animate({
                    width: 0
                });
            });
            $(".slideClose").click(function(){
                $("#orderDetailBox").animate({
                    width: boxWidth
                });
            });
        });
    });
</script>





</div>