<?php
use App\Helpers\CommonFunctions;
?>
@extends('layouts.app')

@section('content')
    <div class="main__container container__custom order-page">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <div class="reservation__atto flex-align-item-baseline margin-bottom-10 tablet-flex-align-item-start flex-column-tablet">
            <div class="reservation__title margin-top-5">
                <?php
                $search_padding = '';
                if(!app('request')->has('key_search')) { ?>
                <h1 class="pull-left"><span
                            class="textColor__primary hide">{{$orderData->total()}}</span> @if($statusKey=="")Archived
                    Orders @else {{ ucfirst($statusKey) }}Orders @endif </h1>
                <?php } else {
                $search_padding = 'padding-top-35';
                ?>
                <h1><span class="textColor__primary">{{$orderData->total()}}</span> Result with
                    "{{ app('request')->input('key_search') }}" <a href="/archive_order"
                                                                   class="btn btn__holo margin-left-10">X</a></h1>
                <?php }?>
            </div>
            <div class="search__manage-order col-sm-9 col-md-9 no-padding-right  no-padding-left-tablet">
                {{--<div class="pull-right hidden-lg hidden-md show-tablet margin-bottom-10">
                    <a href="{{ URL::to('user_order') }}" class="btn btn__holo font-weight-600">Active Orders</a>
                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 margin-left-10 export_button" rel = "food_item-archive" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a>
                </div>--}}

                {!! Form::open(['method'=>'get']) !!}
                <div class="fullWidth form_field__container flex-justify-content-end flex-tablet-between">

                    <div class="guestbook__sec2 no-padding">
                        <div class="fullWidth search-icon no-margin-right">
                            <i class="fa fa-search"></i>
                            <input type="text" name="key_search" autocomplete="off"
                                   placeholder="Name, Phone no. or Receipt no." id="key_search"
                                   value="{{ $_GET['key_search']??'' }}">
                            <button class="btn btn__primary font-weight-600" type="submit">Search</button>
                        </div>
                    </div>

                    {{--<div class="input__field">
                        <input type="text" name="key_search" id="key_search" style="padding:5% 0;" value="{{ $_GET['key_search']??'' }}">
                        <label for="searchKey">Search</label>
                    </div>--}}
                    <div class="flex-box flex-direction-row margin-top-5 overflow-visible">
                    <!-- <div class="selct-picker-plain margin-left-10 filter-by-status">
                        <select class="selectpicker" data-style="no-background-with-buttonline margin-top-5 no-padding-left no-padding-top" data-width="150" name="order_status" id="order_status">
                            @foreach($orderStatus as $key=>$value)
                        <option value="{{$key}}" <?php if ($statusKey == $key) echo "selected";?>>{{ $value }}</option>
                            @endforeach
                            </select>

                        </div> -->
                        <div class="selct-picker-plain margin-left-20 filter-by-status">
                            <select class="selectpicker ordersortorder"
                                    data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top"
                                    data-width="150" name="ordersortorder" rel="archive_order">
                                @foreach($search_type as $key=>$value)
                                    <option value="{{$key}}" <?php if ($default_sort_order == $key) echo "selected";?>>{{ $value }}</option>
                                @endforeach

                            <!-- Iy will be visible only incase of admin or super admin-->
                                @if(Auth::user()->role=="admin")
                                    @foreach($groupRestData as $rest)
                                        <optgroup label="{{ $rest['restaurant_name'] }}">
                                            @foreach($rest['branches'] as $branch)
                                                <option value="{{ $branch['id'] }}" <?php if ($default_sort_order == $branch['id']) echo "selected";?> >{{ $branch['restaurant_name'] }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>

                    {{--<div class="hidden-xs hidden-sm hidden-tablet margin-left-10"><a href="{{ URL::to('user_order') }}" class="btn btn__holo font-weight-600">Active Orders</a></div>
                    <div class="hidden-xs hidden-sm hidden-tablet">
                        <!-- <a href="javascript:void(0)" rel = "food_item-archive" class="btn btn__holo font-weight-600 export_order" ><i class="fa icon-export" aria-hidden="true"></i> Export</a> -->

                        <a href="javascript:void(0)" class="btn btn__holo font-weight-600 export_button" rel = "food_item-archive" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export</a>

                    </div>--}}

                </div>
                {!! Form::close() !!}
            </div>

        </div>

        @if(isset($orderData) && count($orderData)>0)

            <div class="guestbook__container fullWidth box-shadow margin-top-10">

                <div class="guestbook__table-header fullWidth no-padding-left no-padding-right">
                    <div class="col-xs-12 col-md-8 no-padding-left">
                        <div class="col-xs-2 no-padding">&nbsp;</div>
                        <div class="col-xs-3">Orders (ID No.)</div>
                        <div class="col-xs-4 no-padding-left">Contact</div>
                        <div class="col-xs-3 no-padding">Location</div>
                    </div>
                </div>


                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'get']) !!}

                {!! Form::close() !!}

                @foreach($orderData as $oData)
                    <div class="guestbook__cutomer-details-wrapper no-padding overflow-visible">

                        <?php
                        $icon_color = '';
                        $order_row_color = '';
                        $icon_title = '';

                        if ($oData->order_type == 'delivery') {
                            $del_address = $oData->address . ' ' . $oData->address2 . ' ' . $oData->city . ' ' . $oData->state . ' ' . $oData->zipcode;
                            if ($oData->address_label) {
                                $del_address = $oData->address_label . ' ' . $del_address;
                            }
                            $icon_class = 'icon-delivery';
                            $icon_title = 'Delivery';
                        } else {
                            $del_address = $oData->restaurant_name . ' ' . $oData->raddress . ' ' . $oData->rstreet . ' ' . $oData->rzipcode;
                            $icon_class = 'icon-takeout';
                            $icon_title = 'Takeout';
                        }

                        if ($oData->status == 'cancelled' || $oData->status == 'rejected') {
                            $icon_class = 'icon-not_confirmed';
                            $icon_color = 'color-red';
                            $order_row_color = 'rejected-order-row';
                            $icon_title = 'Not Confirmed';
                        }

                        if ($oData->status == 'refunded') {
                            $icon_class = 'icon-refund';
                            $icon_color = 'color-red';
                            $order_row_color = 'refunded-order-row';
                            $icon_title = 'Refunded';
                        }
                        ?>


                        <div role="button"
                             onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'"
                             class="row manage__container position-relative guestbook__customer-details-content text-color-black fullWidth <?php echo $order_row_color;?> {{--padding-top-35--}} <?php echo $search_padding;?> {{--rejected-order-row--}}"
                             id="restaurant-order-{{ $oData->id }}">
                            <?php
                            $status_active_class = '';
                            if(app('request')->has('key_search')) {
                            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $oData->restaurant_id));

                            if (is_object($currentDateTimeObj)) {
                                $restcurrentDateTime['current_datetime'] = $currentDateTimeObj->format('Y-m-d H:i:s');
                            } else {
                                $restcurrentDateTime['current_datetime'] = date('Y-m-d H:i:s');
                            }

                            if (in_array($oData->status, array("archived", "cancelled", "arrived", "sent", "rejected", "refunded"))) {
                                $statuskey = 'Archived';
                            } elseif (strtotime($restcurrentDateTime['current_datetime']) < strtotime($oData->order_delivery_fdatetime)) {
                                $statuskey = 'Scheduled';
                            } else {
                                $statuskey = 'Archived';
                              //  $status_active_class = 'active';
                            }
                            ?>
                            <div class="order-label font-weight-700 padding-left-right-10 {{--active--}} {{$status_active_class}}">{{$statuskey}}</div>
                            <?php } ?>
                            <div role="button" class="col-xs-12 col-md-8 no-padding-left tablet-margin-bottom-15 ">
                                <div class="col-xs-2 text-center">

                                    <div class="icon-status fullWidth text-center margin-top-5 text-color-grey <?php echo $icon_color;?> font-weight-700">
                                        <i class="<?php echo $icon_class;?>" title="{{ $icon_title }}"
                                           aria-hidden="true"></i>
                                    </div>

                                    {{--<div class="order-timer text-center font-weight-600">
                                        <span class="font-size-18 font-weight-800 fullWidth margin-top-7">4</span>
                                        <span class="font-size-12 fullWidth">mins</span>
                                    </div>--}}

                                </div>
                                @php
                                    if(Auth::user()) {
                                        $curSymbol = Auth::user()->restaurant->currency_symbol;
                                    } else {
                                        $curSymbol = config('constants.currency');
                                    }
                                @endphp
                                <div class="col-xs-3 type_of_order "
                                     onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">
                                    <strong class="font-weight-700">{{ $oData->order_type}}</strong> <br>
                                    <div class="text-color-grey font-size-13 font-weight-600">
                                        ({{$oData->payment_receipt}})
                                    </div>
                                    <span class="text-color-grey font-size-13 font-weight-600">Total :</span> <strong
                                            class="font-weight-700">{{ $curSymbol.$oData->total_amount }}</strong>

                                </div>
                                <div class="col-xs-4 no-padding-left"
                                     onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">
                                    <span class="font-weight-700 text-capitalize">{{ $oData->fname.' '. $oData->lname }}</span>
                                    <br/>
                                    <div class="flex-box flex-direction-row flex-align-item-center">
                                        <i class="fa fa-phone margin-right-5" aria-hidden="true"></i> <span
                                                class="font-weight-600">{{ $oData->phone }}</span> <br/>
                                    </div>
                                    <div class="flex-box flex-direction-row  align-items-center">
                                        <i class="fa flex-align-item-center icon-home margin-right-5"></i>
                                        <span
                                                class="font-weight-600 text-ellipsis">
                                            <?php

                                            echo $del_address;
                                            ?>

                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-3 text-nowrap text-ellipsis quantity_of_order no-padding-left">
                                    <div class="font-weight-700 text-ellipsis">
                                        {{$oData->restaurant_name}}
                                    </div>
                                </div>

                            </div>
                            <div role="button"
                                 class="col-xs-12 col-md-4 {{ $oData->order_type}}-order-status font-weight-600"
                                 onclick="window.location='{{ URL::to('user_order/' . $oData->id . '/details/'.$oData->product_type) }}'">

                                <?php
                                if($oData->status == 'rejected'|| $oData->status == 'cancelled' || $oData->status == 'refunded' || $oData->status == 'archived') {
                                ?>
                                <ul class="flex-box flex-direction-row flex-justify-center">

                                    <li class="flex-box flex-direction-column text-center active">
                                        <span class="icon-tick-mark"></span>
                                        <span class="text-nowrap">{{ ucfirst($oData->status) }}</span>
                                    </li>
                                </ul>
                                <?php
                                } else if($oData->order_type == 'delivery') {
                                $first_main_div = $second_main_div = '';
                                $first_child_div = $second_child_div = 'icon-emptyCircle';
                                if ($oData->status == 'sent') {
                                    $first_main_div = $second_main_div = 'active';
                                    $first_child_div = $second_child_div = 'icon-tick-mark';
                                }

                                ?>
                                <ul class="flex-box flex-direction-row flex-justify-space-between">
                                    <li class="flex-box flex-direction-column text-center <?php echo $first_main_div;?>">
                                        <span class="<?php echo $first_child_div;?>"></span>
                                        <span class="text-nowrap">Confirmed</span>
                                    </li>
                                    <li class="flex-box flex-direction-column text-center <?php echo $second_main_div;?>">
                                        <span class="<?php echo $second_child_div;?>"></span>
                                        <span class="text-nowrap">Sent</span>
                                    </li>

                                </ul>
                                <?php } else {
                                    $first_main_div = $second_main_div=$third_main_div = '';
                                    $first_child_div = $second_child_div=$third_child_div = 'icon-emptyCircle';

                                    if ($oData->status == 'archived' || $oData->status == 'arrived' || $oData->status == 'confirmed') {
                                        $first_main_div = 'active';
                                        $first_child_div = 'icon-tick-mark';
                                    }
                                    if ($oData->status == 'archived' || $oData->status == 'arrived' || $oData->status == 'ready') {
                                        $first_main_div = $second_main_div = 'active';
                                        $first_child_div = $second_child_div = 'icon-tick-mark';
                                    }

                                    if ($oData->status == 'archived') {
                                        $first_main_div = $second_main_div = $third_main_div= 'active';
                                        $first_child_div = $second_child_div = $third_child_div = 'icon-tick-mark';
                                    }
                                    ?>
                                <ul class="flex-box flex-direction-row flex-justify-space-between">
                                    <li class="flex-box flex-direction-column text-center <?php echo $first_main_div;?>">
                                        <span class="<?php echo $first_child_div;?>"></span>
                                        <span class="text-nowrap">Confirmed</span>
                                    </li>
                                    <li class="flex-box flex-direction-column text-center <?php echo $second_main_div;?>">
                                        <span class="<?php echo $second_child_div;?>"></span>
                                        <span class="text-nowrap">Ready for Pick Up</span>
                                    </li>
                                    <li class="flex-box flex-direction-column text-center <?php echo $third_main_div;?>">
                                        <span class="<?php echo $third_child_div;?>"></span>
                                        <span class="text-nowrap">Picked Up</span>
                                    </li>
                                </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="fullWidth enabled-services flex-box flex-direction-column flex-justify-center">
                <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center flex-box flex-direction-column flex-justify-center flex-align-item-center">
                    <div class="fullWidth padding-top-bottom-20">
                        <div class="icon-order-enable text-color-grey"></div>
                        <div class="col-xs-12 padding-top-bottom-20 margin-top-15 font-weight-600">
                            No Record Found
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(isset($orderData) && count($orderData)>0)
            <div class="text-center">
                {{ $orderData->appends($_GET)->links()}}
            </div>
        @endif

    </div>
@endsection
