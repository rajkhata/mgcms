@extends('layouts.app')
@section('content')
<link  href="{{asset('js/cropperjs/cropper.css')}}" rel="stylesheet">
<script src="{{asset('js/cropperjs/cropper.js')}}"></script>
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Category - Edit') }}</h1>
    </div>
  </div>

  <div>
    <div class="card form__container1 margin-top-0" id="categoryAdd">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit width100">
          {{ Form::model($menuCategory, array('route' => array('menu_category.update', $menuCategory->id), 'method' => 'PUT',  'enctype' => 'multipart/form-data')) }}
          @csrf

          <div class="form-group row">
            

            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
              <label for="restaurant_id" class="col-form-label text-md-right">{{ __('Restaurant Branch *') }}</label>
              <div class="selct-picker-plain position-relative">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }} selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>      
                  @foreach($groupRestData as $rest)
                  <optgroup label="{{ $rest['restaurant_name'] }}">
                    @foreach($rest['branches'] as $branch)
                    <option value="{{ $branch['id'] }}" {{ ((old('restaurant_id')==$branch['id']) || (isset($menuCategory->restaurant_id) && $menuCategory->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                    @endforeach
                  </optgroup>
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12  margin-top-10">
              <label for="parent_restaurant_id" class="col-form-label text-md-right">{{ __('Parent Category *') }}</label>
              <div class="selct-picker-plain position-relative">
                <select name="parent" id="parent" class="form-control{{ $errors->has('parent_restaurant_id') ? ' is-invalid' : '' }} selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>
                  <option value="0"> Parent Category</option>
                  @foreach($menuCategories as $rest)
		                <option value="{{ $rest->id }}" {{ ($menuCategory->parent==$rest->id) ? 'selected' :'' }}>{{ $rest->name }}</option>
		                @endforeach
                </select>
                @if ($errors->has('parent_restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('parent_restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
              <label for="product_type" class="col-form-label text-md-right">{{ __('Category Type') }}</label>
              <div class="selct-picker-plain position-relative">
                  <select name="product_type" id="product_type" class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }} selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                      <option value="">Select Category Type</option>
                      @foreach($product_types as $key=>$val)
                          <option value="{{ $key }}" {{ (old('product_type')==$key || (count(old())==0 && $menuCategory->product_type==$key)) ? 'selected' :'' }}>{{ $val }}</option>
                      @endforeach
                  </select>
                  @if ($errors->has('product_type'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('product_type') }}</strong>
                      </span>
                  @endif
              </div>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-10">
              <label for="priority" class="col-form-label text-md-right">{{ __('Name *') }}</label>
              <div class="input__field">
                <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                name="name" value="{{ isset($menuCategory->name)?$menuCategory->name:old('name') }}" required>
              </div>
              @if ($errors->has('name'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>

            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
              <label for="priority" class="col-form-label text-md-right">{{ __('URL Key *') }} {{{$source_url}}}</label>
              <div class="input__field">
                <input id="slug" type="text" class="{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                name="slug" value="{{ isset($menuCategory->slug)?$menuCategory->slug:old('slug') }}" required>
              </div>
              @if ($errors->has('slug'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('slug') }}</strong>
              </span>
              @endif
            </div>

            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-20">
              <label for="pos_id" class="col-form-label text-md-right">{{ __('POS Id (Category)') }}</label>
              <div class="input__field">
                  <input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}" name="pos_id" value="{{ isset($menuCategory->pos_id) ? $menuCategory->pos_id : old('pos_id') }}" >
              </div>
              @if ($errors->has('pos_id'))
                  <span class="invalid-feedback"><strong>{{ $errors->first('pos_id') }}</strong></span>
              @endif
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
              <label for="priority" class="col-form-label text-md-right">{{ __('Description') }}</label>
              <div>
                <textarea maxlength="700" rows="10" name="description" id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} editor"> {{ isset($menuCategory->description)?$menuCategory->description:old('description') }}</textarea>
              </div>
              @if ($errors->has('description'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
              </span>
              @endif
            </div>

            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
              <label for="priority" class="col-form-label text-md-right">{{ __('Sub Description') }}</label>
              <div class="">
                <textarea maxlength="500" rows="4" name="sub_description" id="sub_description" class="form-control{{ $errors->has('sub_description') ? ' is-invalid' : '' }} editor"> {{ isset($menuCategory->sub_description)?$menuCategory->sub_description:old('sub_description') }}</textarea>
              </div>
              @if ($errors->has('sub_description'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('sub_description') }}</strong>
              </span>
              @endif
            </div>
          </div>


 <!-- work is pending -->
          <div class="form-group row">
            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
              <label for="image_class" class="col-form-label text-md-right">{{ __('Category Icon Class') }}</label>
              <!-- <div class="selct-picker-plain position-relative">
		            <select name="image_class" id="image_class" class="form-control selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" multiple>
								  <option value="Class -A" selected>Class -A</option>
								  <option value="">Class -B</option>
							  </select>	
              </div> -->
              <div class="input__field">
                  <input id="image_class" type="text" class="" name="pos_id" value="" readonly>
                </div>
                @if ($errors->has('image_class'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_class') }}</strong>
                </span>
                @endif
            </div>

            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
            </div>
          </div>

          <hr>

          

          <div class="form-group row source-image-div image-caption-div checkrow">
          <div class="clearfix" style="padding:0 15px 10px"><label for="image_png_selected" class=" col-form-label text-md-right">{{ __('Category Banner') }}</label></div>
            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
              <input id="image_png_selected" type="file" class="form-control{{ $errors->has('image_png_selected') ? ' is-invalid' : '' }}"
              value="{{ old('image_png_selected') }}" name="image_png_selected" accept="image/jpeg, image/jpg" onchange="showImage(this)" >
              @if ($errors->has('image_png_selected'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('image_png_selected') }}</strong>
              </span>
              @endif
            </div>
            @if(isset($menuCategory->thumb_image_med_png_selected) && $menuCategory->thumb_image_med_png_selected!='')
            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
              <a data-fancybox="gallery" href="{{url('/').'/..'.$menuCategory->image_png_selected}}"><img src="{{ url('/').'/..'.$menuCategory->thumb_image_med_png_selected }}" style="width:120px;"></a>&emsp;
              <div class="option__wrapper">
                    <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="delete3" type="checkbox" name="image_png_selected_delete" value="1" style="margin-left:20px;"> Delete <span class="control_indicator"></span>
                    </label>
                  </div>
              <!-- <input type="checkbox" name="image_png_selected_delete" value="1" style="margin-left:20px;">Delete -->
            </div>
            @endif
          </div>

          <hr>

          <!-- Edit category attachment -->
          <div class="form-group row source-image-div image-caption-div checkrow">
          <div class="clearfix" style="padding:0 15px 10px"><label for="cat_attachment" class="col-form-label text-md-right">{{ __('Category Attachment') }}</label></div>
            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
              <input id="image_png_selected" type="file" class="form-control{{ $errors->has('cat_attachment') ? ' is-invalid' : '' }}"
                     value="{{ old('cat_attachment') }}" name="cat_attachment" accept="application/pdf">
              @if ($errors->has('cat_attachment'))
                <span class="invalid-feedback">
                <strong>{{ $errors->first('cat_attachment') }}</strong>
              </span>
              @endif
            </div>
            @if(isset($menuCategory->cat_attachment) && $menuCategory->cat_attachment!='')
              <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                <a data-fancybox="gallery" href="{{url('/').$menuCategory->cat_attachment}}">
                <?php
                $extName = substr($menuCategory->cat_attachment, -3);
                ?>
                  @if($extName=="pdf")
                    <img src="{{ url('/images/pdf.png') }}" style="width:30px;">
                    @else
                  <img src="{{ url('/').$menuCategory->cat_attachment }}" style="width:75px;">
                  @endif
                </a>&emsp;
                <div class="option__wrapper">
                    <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="delete4" type="checkbox" name="cat_attachment_deleted" value="1" style="margin-left:20px;"> Delete <span class="control_indicator"></span>
                    </label>
                  </div>
                <!-- <input type="checkbox" name="cat_attachment_deleted" value="1" style="margin-left:20px;">Delete -->
              </div>
            @endif
          </div>

          <hr>

          <div id="cateAdd" class="form-group row">
            <div class="col-sm-8 col-md-7 col-lg-7 col-xs-12 margin-top-20 ">
              <div class="clearfix"><label for="is_popular" class="col-form-label text-md-right">Settings</label></div>
              <div class="option__wrapper hide">
                  <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="is_popular" type="checkbox" name="is_popular" {{ ((isset($menuCategory->is_popular) && $menuCategory->is_popular=='1') || old('is_popular')=='1') ? 'checked' :'' }}> Is Popular <span class="control_indicator"></span>
                  </label>
              </div>
              <div class="option__wrapper hide">
                  <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="is_favourite" type="checkbox" name="is_favourite" {{ ((isset($menuCategory->is_favourite) && $menuCategory->is_favourite=='1') || old('is_favourite')=='1') ? 'checked' :'' }}> Is Favourite <span class="control_indicator"></span>
                  </label>
              </div>
              <div class="option__wrapper ">
                  <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" type="checkbox" id="status1" name="is_delivery"  {{ ((isset($menuCategory->is_delivery) && $menuCategory->is_delivery=='1') || old('is_delivery')=='1') ? 'checked' :'' }}> Delivery <span class="control_indicator"></span>
                  </label>
              </div>
              <div class="option__wrapper">
                  <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" type="checkbox" id="status0" name="is_carryout"  {{ ((isset($menuCategory->is_carryout) && $menuCategory->is_carryout=='1') || old('is_carryout')=='1') ? 'checked' : '' }}>  Carryout <span class="control_indicator"></span>
                  </label>
              </div>
            </div>

            <div class="col-sm-4 col-md-5 col-lg-5 col-xs-12 pull-right margin-top-20">
              <div class="clearfix"><label for="priority" class="col-form-label text-md-right">{{ __('Status') }}</label></div>
              <div class="pull-left tbl-radio-btn">
                <input type="radio" id="status12" name="status" value="1" {{ ((isset($menuCategory->status) && $menuCategory->status=='1') || old('status')=='1') ? 'checked' :'' }}>
                <label for="status12">Active</label>
              </div>
              <div class="pull-left tbl-radio-btn margin-left-30">
                <input type="radio" id="status02" name="status" value="0" {{ ((isset($menuCategory->status) && $menuCategory->status=='0') || old('status')=='0') ? 'checked' :'' }}>
                <label for="status02">Inactive</label>
              </div>
              @if ($errors->has('status'))
              <span class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>
          </div><!-- changes radio ID for -->

 

          
          <div class="form-group row">
            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
              <label for="priority" class="col-form-label text-md-right">{{ __('Sort Order') }}</label>
              <div class="input__field">
                <input id="priority" type="text" class="{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                name="priority" value="{{ isset($menuCategory->priority)?$menuCategory->priority:old('priority') }}" required>
              </div>
              @if ($errors->has('priority'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('priority') }}</strong>
              </span>
              @endif
            </div>

            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-20 hide">
              <label for="promotional_banner_url" class="col-form-label text-md-right">{{ __('Promotional Banner Url') }} </label>
              <div class="input__field">
                <input id="promotional_banner_url" type="text" class="{{ $errors->has('promotional_banner_url') ? ' is-invalid' : '' }}"
                            name="promotional_banner_url" value="{{ isset($menuCategory->promotional_banner_url)?$menuCategory->promotional_banner_url:old('promotional_banner_url') }}" >
              </div>
              @if ($errors->has('promotional_banner_url'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('promotional_banner_url') }}</strong>
              </span>
              @endif
            </div>
          </div>

           
          <div class=" row  mb-0">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
</div>
<?php
$group_options='';
foreach ($user_groups as $group){
    $group_options .= '<option value="' . $group->id . '" >' . $group->user_group . ' </option>';
}

?>


<!-- modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="selectImage">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Select Image</h4>
      </div>
      <div class="modal-body">
        <button class="ico ico1">Twitter</button>
        <button class="ico ico2">Facebook</button>
        <button class="ico ico3">Stumble Upon</button>
        <button class="ico ico4">Vimeo</button>
        <button class="ico ico5">Dribble</button>
        <button class="ico ico6">Twitter Blue</button>
        <button class="ico ico7">Facebook Blue</button>
        <button class="ico ico8">Stumble Upon Blue</button>
        <button class="ico ico9">Vimeo Blue</button>
        <button class="ico ico10">Dribble Blue</button>
        <button class="ico ico11">Twitter Sky Blue</button>
        <button class="ico ico12">Facebook Sky Blue</button>
        <button class="ico ico13">Stumble Upon Sky Blue</button>
        <button class="ico ico14">Vimeo Sky Blue</button>
        <button class="ico ico45">Dribble Sky Blue</button>
        <button class="ico ico16">Twitter Red</button>
        <button class="ico ico17">Facebook Red</button>
        <button class="ico ico18">Stumble Upon Red</button>
        <button class="ico ico19">Vimeo Red</button>
        <button class="ico ico20">Dribble Red</button>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- modal -->

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', '.position_plus', function (e) {
            var lenofbox = $('.position_container').length;
            e.preventDefault();
            var groups = '<?php echo $group_options;?>';

            // $("#position_container").append('<div class="form-group row form_field__container position_container"> <div class="col-md-2"> <div class=""><input type="text" class="input__field" name="position_type['+lenofbox+']" placeholder="Type"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position['+lenofbox+']" placeholder="position"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position_price['+lenofbox+']" placeholder="price"></div> </div><div class="col-md-2"> <div class=""><input type="checkbox" class="" name="is_selected_position['+lenofbox+']" value="1">Default Position</div> </div> <div class="col-md-2"> <div class=""><input type="file" class="" name="position_image['+lenofbox+']"></div> </div> <div class="col-md-2"> &nbsp;&nbsp; <a href="#" class="position_minus"><i class="fa fa-minus"></i></a> </div> </div>');
            $("#position_container").append('<div class="form-group row position_container content__row"><div class="col-sm-10 col-md-10 col-lg-11 col-xs-12 content__imageRow no-mob-pad7"><div class="form-group row no-bot"><div class="col-md-4 col-xs-12 margin-top-15"><input type="hidden" class="input__field" name="ID[' + lenofbox + ']" value=""> <div class=""><select class="form-control" name="UserGroup[' + lenofbox + ']"> <option value="">--Select User Group--</option> ' + groups + '</select></div></div><div class="col-md-4 col-xs-12 margin-top-15"> <div class=""><input type="text" class="form-control" name="AltText[' + lenofbox + ']" placeholder="Alt Text"> </div></div><div class="col-md-4 col-xs-12 margin-top-15"> <div class=""><select class="form-control" name="Position[' + lenofbox + ']"> <option value="Header">Header</option> <option value="Footer">Footer</option> </select></div></div></div><div class="form-group row no-bot"><div class="col-md-4 typeparent col-xs-12 margin-top-15"> <div style="padding-top:20px;"><select class="form-control fieldtype_banner" name="Type[' + lenofbox + ']"> <option value="Text">Text</option> <option value="Image">Image</option> <option value="Video">Video</option> </select></div></div><div class="col-md-4 col-xs-12 margin-top-15"> <div><strong>Web</strong></div><div class="align-centerflex"> <input type="<?php if (isset($pos['Type']) && $pos['Type']=='Text'){echo 'text';}else{echo 'file';}?>" class="form-control" name="Value[{{$key}}]" value="<?php if (isset($pos['Type']) && $pos['Type']=='Text'){echo $pos['Value'];}?>" placeholder="Web"> @if(isset($pos['Type']) && $pos['Type']=='Image' && isset($pos['Value']) && $pos['Value']!='') <div class="aligncenter-order1"> <a data-fancybox="gallery" href="{{url('/').'/'.$pos['Value']}}"><span class="glyphicon glyphicon-picture"></span></a>&emsp; </div>@endif @if(isset($pos['Type']) && $pos['Type']=='Video' && isset($pos['Value']) && $pos['Value']!='') <div class=""> <a data-fancybox="gallery" href="{{url('/').'/'.$pos['Value']}}"></a>&emsp; </div>@endif </div></div><div class="col-md-4 col-xs-12 margin-top-15"> <div><strong>Mobile</strong></div><div class="align-centerflex"> <input type="<?php if (isset($pos['Type']) && $pos['Type']=='Text'){echo 'text';}else{echo 'file';}?>" class="form-control" name="Value_Mobile[{{$key}}]" value="<?php if (isset($pos['Type']) && $pos['Type']=='Text'){echo $pos['Value_Mobile'];}?>" placeholder="Value_Mobile"> @if(isset($pos['Type']) && $pos['Type']=='Image' && isset($pos['Value_Mobile']) && $pos['Value_Mobile']!='') <div class="aligncenter-order1"> <a data-fancybox="gallery" href="{{url('/').'/'.$pos['Value_Mobile']}}"><span class="glyphicon glyphicon-picture"></span></a>&emsp; </div>@endif @if((isset($pos['Type']) && $pos['Type']=='Video') && isset($pos['Value_Mobile']) && $pos['Value_Mobile']!='') <div class=""> <a data-fancybox="gallery" href="{{url('/').'/'.$pos['Value_Mobile']}}"></a>&emsp; </div>@endif </div></div></div><div class="form-group row no-bot"><div class="col-md-4 col-xs-12 margin-top-15"> <div class=""><input type="text" class="form-control" name="Link[' + lenofbox + ']" placeholder="Link"></div></div><div class="col-md-4 col-xs-12 margin-top-15"> <div class=""><input type="number" class="form-control" name="SortOrder[' + lenofbox + ']" placeholder="Sort Order"></div></div><div class="col-md-4 col-xs-12 margin-top-15"> <div class=""><select class="form-control" name="Status[' + lenofbox + ']"> <option value="1">Active</option> <option value="0">In Active</option> </select></div></div></div></div><div class="col-sm-2 col-md-2 col-lg-1 col-xs-12 pad-top-15"> <a href="#" class="btn btn__primary position_minus">Delete</a></div></div>');

        });

        $(document).on('click', '.position_minus', function (e) {

            e.preventDefault();
            $(this).parents('.position_container').empty().remove();
        });


        $(document).on('change', '.fieldtype_banner', function (e) {
            e.preventDefault();
            var type = $(this).val();
            if (type == 'Text') {
                var nxt =  $(this).parents('.typeparent').next();
                $(this).parents('.typeparent').next().find('input').attr('type', 'text');
                $(nxt).next().find('input').attr('type', 'text');
            } else {
                var nxt =  $(this).parents('.typeparent').next();
                $(this).parents('.typeparent').next().find('input').attr('type', 'file');
                $(nxt).next().find('input').attr('type', 'file');
            }

        });

     
    });
     $('#name').keyup(function(){
	    var name = $('#name').val();
	    var slugname = name;
	     
	     var slugname = slugname.replace(/"/g, " Inch");
	     var slugname = slugname.toLowerCase();
	     var slugname = slugname.replace(/ /g, "_");
	     
	   $('#slug').val(slugname);    
	    
	});
	 	
</script>

<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    
      
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {

	 tinymce.init({
           selector: 'textarea.editor',
            menubar: false,
            height: 320,
            theme: 'modern',
            plugins: 'print  fullpage importcss  searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap emoticons',
            image_advtab: true,
	    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen   | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment', 	
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });

    });
    
    
</script>
<script type="text/javascript">
$( document ).ready(function() {
  $('#image_class').on('click', function(){
    $('.modal').modal('show');
  });
  $(".modal-body button").click(function() {
    var text = $( this ).text();
    $( "#image_class" ).val(text);
    $('.modal').modal('hide');
  });
});
</script>
@endsection
