@extends('layouts.app')
@section('content')
<link  href="{{asset('js/cropperjs/cropper.css')}}" rel="stylesheet">
<script src="{{asset('js/cropperjs/cropper.js')}}"></script>
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Category - Add') }}</h1>
    </div>
  </div>

  <div>

    <div class="card form__container1 margin-top-0" id="categoryAdd">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
       @if(session()->has('error'))
      <div class="alert alert-success">
        {{ session()->get('error') }}
      </div>
      @endif	
      <div class="card-body">
        <div class="form__user__edit width100">
         <form method="POST" action="{{ route('menu_category.store') }}" enctype="multipart/form-data" class="form">
            @csrf

            {{--<div class="form-group row form_field__container">
              <label for="parent_restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant *') }}</label>
              <div class="col-md-4">
                <select name="parent_restaurant_id" id="parent_restaurant_id" class="form-control{{ $errors->has('parent_restaurant_id') ? ' is-invalid' : '' }}" required>
                  <option value="">Select Restaurant</option>
                  @foreach($restaurantData as $rest)
		  @if($restaurant_id ==  $rest->id)
                  <option value="{{ $rest->id }}" {{ (old('parent_restaurant_id')==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
		  @endif
                  @endforeach
                </select>
                @if ($errors->has('parent_restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('parent_restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>--}}


            <div class="form-group row">
              {{--<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
                <label for="restaurant_id" class="col-form-label text-md-right">{{ __('Language') }}</label>
                  <div class="selct-picker-plain position-relative">
                      <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }} selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" >
                          <option value="">Select Language</option>
                          @foreach($languageData as $lang)
                              <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                          @endforeach
                      </select>
                      @if ($errors->has('language_id'))
                          <span class="invalid-feedback">
                             <strong>{{ $errors->first('language_id') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>--}}

              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
                 <label for="restaurant_id" class="col-form-label text-md-right">{{ __('Restaurant Branch *') }}</label>
                <div class="selct-picker-plain position-relative">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }} selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                  <option value="">Select Restaurant</option>
                  @foreach($groupRestData as $branch)
                    
                    		<option value="{{ $branch['id'] }}"  {{ (old('restaurant_id')== $branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
		                 
                     
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
                </div>
              </div>
            </div>
            

            <div class="form-group row">
            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
            <label for="parent_restaurant_id" class="col-form-label text-md-right">{{ __('Parent Category *') }}</label>
              <div class="selct-picker-plain position-relative">
                <select name="parent" id="parent" class="form-control{{ $errors->has('parent_restaurant_id') ? ' is-invalid' : '' }} selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>
                  <option value="0"> Parent Category</option>
                  @foreach($menuCategories as $rest)
		  <option value="{{ $rest->id }}" {{ (old('parent_restaurant_id')==$rest->id) ? 'selected' :'' }}>{{ $rest->name }}</option>
		  @endforeach
                </select>
                @if ($errors->has('parent_restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('parent_restaurant_id') }}</strong>
                </span>
                @endif
              </div>
              </div>

              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
                 <label for="product_type" class="col-form-label text-md-right">{{ __('Category Type') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select required name="product_type" id="product_type" class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }} selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Category Type</option>
                                       
                                            @foreach($product_types as $key=>$val)
                                                <option value="{{ $key }}" {{ (old('product_type')==$key) ? 'selected' :'' }}>{{ $val }}</option>
                                            @endforeach
                                    
                                    </select>
                                    @if ($errors->has('product_type'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
                                    @endif
                </div>
              </div>
            </div>


	          <div class="form-group row">
               <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-10">
               <label for="priority" class="col-form-label text-md-right">{{ __('Name *') }}</label>
                <div class="input__field">
                  <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                  name="name" value="{{ old('name') }}" required>
                </div>
                @if ($errors->has('name'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
               </div>

               <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
               <label for="priority" class="col-form-label text-md-right">{{ __('URL Key *') }} {{{$source_url}}}</label>
                <div class="input__field">
                  <input id="slug" type="text" class="{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                  name="slug" value="{{ old('slug') }}" required>
                </div>
                @if ($errors->has('slug'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('slug') }}</strong>
                </span>
                @endif
              </div>

              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-20">
                 <label for="pos_id" class="col-form-label text-md-right">{{ __('POS Id (Category)') }}</label>
                      <div class="input__field">
                          <input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}"
                                 name="pos_id" value="{{ old('pos_id') }}" >
                      </div>
                      @if ($errors->has('pos_id'))
                          <span class="invalid-feedback"><strong>{{ $errors->first('pos_id') }}</strong></span>
                      @endif
              </div>
            </div>


            <div class="form-group row">
              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
                <label for="priority" class="col-form-label text-md-right">{{ __('Description') }}</label>
                <div class="">
                    <div class="input__field">
                       <textarea maxlength="700" rows="10" name="description" id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} editor">{{ old('description') }}</textarea>
                    </div>
                    @if ($errors->has('description'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
              </div>

              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
                <label for="priority" class="col-form-label text-md-right">{{ __('Sub Description') }}</label>
                <div class="">
                    <div class="input__field">
                       <textarea maxlength="500" rows="4" name="sub_description" id="sub_description" class="form-control{{ $errors->has('sub_description') ? ' is-invalid' : '' }} editor">{{ old('sub_description') }}</textarea>
                    </div>
                    @if ($errors->has('sub_description'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('sub_description') }}</strong>
                    </span>
                    @endif
                </div>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
                <label for="image_class" class="col-form-label text-md-right">{{ __('Category Icon Class') }}</label>
                <!-- <div class="selct-picker-plain position-relative">
                  <select name="image_class" id="image_class" class="form-control selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
								    <option value="Class-A" selected>Class-A</option>
								    <option value="Class-B">Class-B</option>
							    </select>
                </div> -->
                <div class="input__field">
                  <input id="image_class" type="text" class="" name="pos_id" value="" readonly>
                </div>
                @if ($errors->has('image_class'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_class') }}</strong>
                </span>
                @endif
              </div>

              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
              </div>
            </div>


    		<hr>

            <!--div class="form-group hide row source-image-div image-caption-div">
            <div class="clearfix" style="padding:0 15px 10px"><label for="image_svg" class="col-form-label text-md-right">{{ __('Image SVG') }}</label></div>
              <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                <input id="image_svg" type="file" class="form-control{{ $errors->has('image_svg') ? ' is-invalid' : '' }}"
                value="{{ old('image_svg.0') }}" name="image_svg">
                @if ($errors->has('image_svg'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_svg') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row source-image-div image-caption-div hide">
              <div class="clearfix" style="padding:0 15px 10px"><label for="image_png" class="col-form-label text-md-right">{{ __('Category Banner (Not Selected)') }}</label></div>
              <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                <input id="image_png" type="file" class="form-control{{ $errors->has('image_png') ? ' is-invalid' : '' }}"
                value="{{ old('image_png') }}" name="image_png">
                @if ($errors->has('image_png'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_png') }}</strong>
                </span>
                @endif
              </div>
            </div-->

            

            <div class="form-group row source-image-div image-caption-div">
            <div class="clearfix" style="padding:0 15px 10px"><label for="image_png_selected" class="col-form-label text-md-right">{{ __('Category Banner') }}</label></div>
              <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                <input id="image_png_selected" type="file" class="form-control{{ $errors->has('image_png_selected') ? ' is-invalid' : '' }}"
                value="{{ old('image_png_selected') }}" name="image_png_selected" accept="image/jpeg, image/jpg"  >
		 
                @if ($errors->has('image_png_selected'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_png_selected') }}</strong>
                </span>
                @endif
              </div>
            </div>
              <!-- add category attachment -->

              <hr>

              <div class="form-group row source-image-div image-caption-div">
              <div class="clearfix" style="padding:0 15px 10px"><label for="cat_attachment" class=" col-form-label text-md-right">{{ __('Category Attachment') }}</label></div>
                  <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                      <input id="cat_attachment" type="file" class="form-control{{ $errors->has('cat_attachment') ? ' is-invalid' : '' }}"
                             value="{{ old('cat_attachment') }}" name="cat_attachment" accept="application/pdf">
                      @if ($errors->has('cat_attachment'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('cat_attachment') }}</strong>
                </span>
                      @endif
                  </div>
              </div>

              <hr>

              <div id="cateAdd" class="form-group row ">
                <div class="col-sm-8 col-md-7 col-lg-7 col-xs-12 margin-top-20">
                  <div class="clearfix"><label for="is_popular" class="col-form-label text-md-right">Settings</label></div>

                  <div class="option__wrapper hide">
                    <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="is_popular" type="checkbox" name="is_popular"> Is Popular <span class="control_indicator"></span>
                    </label>
                  </div>

                  <div class="option__wrapper hide">
                    <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="is_favourite" type="checkbox" name="is_favourite"> Is Favourite <span class="control_indicator"></span>
                    </label>
                  </div>

                  <div class="option__wrapper ">
                    <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="status1" type="checkbox" name="is_delivery"  {{ ((isset($menuCategory->is_delivery) && $menuCategory->is_delivery=='1') || old('is_delivery')=='1') ? 'checked' :'' }}> Delivery <span class="control_indicator"></span>
                    </label>
                  </div>

                  <div class="option__wrapper">
                    <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="status0" type="checkbox" name="is_carryout"  {{ ((isset($menuCategory->is_carryout) && $menuCategory->is_carryout=='1') || old('is_carryout')=='1') ? 'checked' : '' }}> Carryout <span class="control_indicator"></span>
                    </label>
                  </div>
                </div>
                <div class="col-sm-4 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                  <div class="clearfix"><label for="priority" class="col-form-label text-md-right">{{ __('Status') }}</label></div>
                  <div class="pull-left tbl-radio-btn">
                  <input type="radio" id="status12" name="status" value="1" {{ (old('status')=='1') ? 'checked' :'' }} checked>
                    <label for="status12">Active</label>
                  </div>
                  <div class="pull-left tbl-radio-btn margin-left-30">
                  <input type="radio" id="status02" name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }} >
                    <label for="status02">Inactive</label>
                  </div>
                  @if ($errors->has('status'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                  @endif
                </div>
              </div>

            <!--end here -->

             <div class="form-group row ">
              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
              <label for="priority" class="col-form-label text-md-right">{{ __('Sort Order') }}</label>
                <div class="input__field">
                  <input id="priority" type="text" class="{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                  name="priority" value="{{ old('priority') }}" required>
                </div>
                @if ($errors->has('priority'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('priority') }}</strong>
                </span>
                @endif
              </div>

              <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-20 hide">
                 <label for="promotional_banner_url" class="col-form-label text-md-right">{{ __('Promotional Banner Url') }}</label>
                      <div class="input__field">
                          <input id="promotional_banner_url" type="text" class="{{ $errors->has('promotional_banner_url') ? ' is-invalid' : '' }}"
                                 name="promotional_banner_url" value="{{ old('promotional_banner_url') }}" >
                      </div>
                      @if ($errors->has('promotional_banner_url'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_url') }}</strong>
                </span>
                      @endif
               </div>
            </div>

            
            <div class="form-group row margin-top-20 hide">
                <div class="clearfix" style="padding:0 15px 10px"><label for="promotional_banner_image" class="col-form-label text-md-right">{{ __('Promotional Banner Image') }}</label></div>
                  <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                          <input id="promotional_banner_image" type="file" class="form-control{{ $errors->has('promotional_banner_image') ? ' is-invalid' : '' }}" value="{{ old('promotional_banner_image') }}" name="promotional_banner_image">
                      @if ($errors->has('promotional_banner_image'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_image') }}</strong>
                </span>
                      @endif
                  </div>
              </div>
               
              <div class="form-group row hide">
              <div class="clearfix" style="padding:0 15px 10px "><label for="promotional_banner_image_mobile" class="col-form-label text-md-right">{{ __('Promotional Banner Image Mobile') }}</label></div>
                  <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                          <input id="promotional_banner_image_mobile" type="file" class="form-control{{ $errors->has('promotional_banner_image_mobile') ? ' is-invalid' : '' }}"
                                 value="{{ old('promotional_banner_image_mobile') }}" name="promotional_banner_image_mobile">
                      @if ($errors->has('promotional_banner_image_mobile'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_image_mobile') }}</strong>
                </span>
                      @endif
                  </div>
              </div>

            <br />
	    <textarea name="blobimage" id="blobimage" style="display:none"></textarea>	
            <div class="row mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="selectImage">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Select Image</h4>
      </div>
      <div class="modal-body">
        <button class="ico ico1">Twitter</button>
        <button class="ico ico2">Facebook</button>
        <button class="ico ico3">Stumble Upon</button>
        <button class="ico ico4">Vimeo</button>
        <button class="ico ico5">Dribble</button>
        <button class="ico ico6">Twitter Blue</button>
        <button class="ico ico7">Facebook Blue</button>
        <button class="ico ico8">Stumble Upon Blue</button>
        <button class="ico ico9">Vimeo Blue</button>
        <button class="ico ico10">Dribble Blue</button>
        <button class="ico ico11">Twitter Sky Blue</button>
        <button class="ico ico12">Facebook Sky Blue</button>
        <button class="ico ico13">Stumble Upon Sky Blue</button>
        <button class="ico ico14">Vimeo Sky Blue</button>
        <button class="ico ico45">Dribble Sky Blue</button>
        <button class="ico ico16">Twitter Red</button>
        <button class="ico ico17">Facebook Red</button>
        <button class="ico ico18">Stumble Upon Red</button>
        <button class="ico ico19">Vimeo Red</button>
        <button class="ico ico20">Dribble Red</button>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- modal -->

<script type="text/javascript">
  $('#name').keyup(function(){
	    var name = $('#name').val();
	    var slugname = name;
	     
	     var slugname = slugname.replace(/"/g, " Inch");
	     var slugname = slugname.toLowerCase();
	     var slugname = slugname.replace(/ /g, "_");
	     
	   $('#slug').val(slugname);    
	    
	});
/*var croppers = [];
var is_image = false;
function showImage(img){
    document.getElementById('img1').src = window.URL.createObjectURL(img.files[0]);
     
      var image = document.getElementById('img1');
        
      var minAspectRatio = 1.0;
      var maxAspectRatio = 1.0;
      for (i = 0; i < 1; i++) {
       
        croppers.push(new Cropper(image, {
        ready: function () { is_image = true;
          var cropper = this.cropper;
          var containerData = cropper.getContainerData();
          var cropBoxData = cropper.getCropBoxData();
          var aspectRatio = cropBoxData.width / cropBoxData.height;
          var newCropBoxWidth;

          if (aspectRatio < minAspectRatio || aspectRatio > maxAspectRatio) {
            newCropBoxWidth = cropBoxData.height * ((minAspectRatio + maxAspectRatio) / 2);

            cropper.setCropBoxData({
              left: (containerData.width - newCropBoxWidth) / 2,
              width: newCropBoxWidth
            });
          }
        },
        cropmove: function () {
          var cropper = this.cropper;
          var cropBoxData = cropper.getCropBoxData();
          var aspectRatio = cropBoxData.width / cropBoxData.height;

          if (aspectRatio < minAspectRatio) {
            cropper.setCropBoxData({
              width: cropBoxData.height * minAspectRatio
            });
          } else if (aspectRatio > maxAspectRatio) {
            cropper.setCropBoxData({
              width: cropBoxData.height * maxAspectRatio
            });
          }
        }
      });
      } 
     $(".img1").show();
}
function uploadData(){
      $("#image_png_selected").val('');	
      const formData = new FormData($('.form')[0]);
      //var image = document.getElementById('img1');
       
	 
	if(is_image==true){
	croppers[0].getCroppedCanvas().toBlob((blob) => {
	    formData.append('croppedImage'   , blob , 'cat.jpg'  );
	    formData.append('image_png_selected'   , '' , 'none.jpg'  );
 	} , 'image/jpg' );
        } 	
     //} 
	$.ajax('/v2/menu_category/createCat', {
		    method: "POST",
		    data: formData,
		    processData: false,
		    contentType: false,
		    success() {
		      console.log('Upload success');
		    },
		    error() {
		      console.log('Upload error');
		    },
	});
     return false;  
} */	 
</script>
<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
       tinymce.init({
           selector: 'textarea.editor',
            menubar: false,
            height: 320,
            theme: 'modern',
            plugins: 'print fullpage importcss  searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help  charmap emoticons',
            image_advtab: true,
	    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment', 	
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });
    });
</script>
<script type="text/javascript">
$( document ).ready(function() {
  $('#image_class').on('click', function(){
    $('.modal').modal('show');
  });
  $(".modal-body button").click(function() {
    var text = $( this ).text();
    $( "#image_class" ).val(text);
    $('.modal').modal('hide');
  });
});
</script>
@endsection
