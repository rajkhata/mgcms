@extends('layouts.app')

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel='stylesheet'/>
@section('content')

<div class="main__container container__custom">
	<div class="reservation__atto">
		<div class="reservation__title">
			<h1>Categories<span style="margin:0px 5px;">({{ $menuCategories->currentPage() .'/' . $menuCategories->lastPage() }})</span>&emsp;</h1>
		</div>
	</div>
	<div class="addition__functionality">
		<div class="row functionality__wrapper margin-top-0">
			<div class="row form-group col-md-3 pull-right">
			    <div id="sortcol" class="selct-picker-plain position-relative">
					<select name="menu_sort_by" id="menu_sort_by" class="form-control selectpicker" 
					 data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" style="background:transparent!important">
						<option value="0" {{!isset($sortAlgo[0])?'Selected':''}}>Sort By</option>
						<option value="1" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==1)?'Selected':''}}>Move Inactive to bottom</option>
						<option value="2" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==2)?'Selected':''}}>Newest products first</option>
						<option value="3" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==3)?'Selected':''}}>Bestseller products first</option>
						<option value="4" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==4)?'Selected':''}}>Biggest Saving products first</option>
						<option value="5" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==5)?'Selected':''}}>Most Viewed products first</option>
						<option value="6" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==6)?'Selected':''}}>Name: Ascending</option>
						<option value="7" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==7)?'Selected':''}}>Name: Descending</option>
						<option value="8" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==8)?'Selected':''}}>Price: Ascending</option>
						<option value="9" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==9)?'Selected':''}}>Price: Descending</option>
						<option value="10" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==10)?'Selected':''}}>Sort Order: Ascending</option>
						<option value="11" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==11)?'Selected':''}}>Sort Order: Descending</option>
					    </select>
			    </div>
     			</div>
			<div class="row form-group col-md-1 pull-right">
				<button class="btn btn__primary" style="margin-left: 5px;" type="submit" onclick="shortMenu()">Sort</button>
			</div>
		</div>
	</div>
	<div>
		<div class="active__order__container">@if(session()->has('message'))
			<div class="alert alert-success">{{ session()->get('message') }}</div>@elseif (session()->has('err_msg'))
			<div class="alert alert-danger">{{ session()->get('err_msg') }}</div>@endif
			<div class="guestbook__container box-shadow margin-top-0">
			   <table id="table_id1" class="first responsive ui cell-border hover" style="width:100%; margin-bottom:10px;"></table> 
			</div>
        </div>
    </div>
</div>
@endsection
 
 
<script type="text/javascript">
  $(document).ready( function () {
    var columns = [
                {
                    title: '',
                    target: 0,
                    className: 'treegrid-control',
                    data: function (item) {
                        if (item.children) {
                            return '<span>+</span>';
                        }
                        return '';
                    }
                },
                {
                    title: 'Name',
                    target: 1,
                    data: function (item) {
                        return item.name;
                    }
                },
                {
                    title: 'Type',
                    target: 2,
                    data: function (item) {
                        return item.product_type;
                    }
                },
		{
                    title: 'No Of Item Sold',
                    target: 3,
                    data: function (item) {
                        return item.noofitem_sold;
                    }
                },
                {
                    title: 'Delivery',
                    target: 4,
                    data: function (item) {
                        //return item.is_delivery;
			//return (item.is_delivery==1)?'Yes':'No';
			var htm = '';
			htm +='<div class="waitList__otp no-padding" style="float: left; margin-right: 20px;">';
		        htm +='<div class="toggle__container" style="margin-left: 0;">';
		        if(item.is_delivery==1){
		          var ariaPressed = true;
		          var activeClass = 'active';
		        } else {
		          var ariaPressed = false;
		          var activeClass = '';
		        }
		        htm +='<button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom deliveryid'+item.id+' menu_cat_active '+activeClass+'"';
		        htm +=' title="Open" data-toggle="button" data-cat="'+item.id+'" onclick="changeDelivery('+item.id+')"  aria-pressed=""'+ariaPressed+'" autocomplete="off">';
		        htm +='<div class="handle"></div> </button> </div> </div>';
			return htm;
                    }
                },
		 				 
		{
                    title: 'Carryout',
                    target: 5,
                    data: function (item) {
                        //return item.is_carryout;
			//return (item.is_carryout==1)?'Yes':'No';
			var htm = '';
			htm +='<div class="waitList__otp no-padding" style="float: left; margin-right: 20px;">';
		        htm +='<div class="toggle__container" style="margin-left: 0;">';
		        if(item.is_carryout==1){
		          var ariaPressed = true;
		          var activeClass = 'active';
		        } else {
		          var ariaPressed = false;
		          var activeClass = '';
		        }
		        htm +='<button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom takeoutid'+item.id+' menu_cat_active '+activeClass+'"';
		        htm +=' title="Open" data-toggle="button" data-cat="'+item.id+'" onclick="changeCarryout('+item.id+')"  aria-pressed=""'+ariaPressed+'" autocomplete="off">';
		        htm +='<div class="handle"></div> </button> </div> </div>';
			return htm;
                    }
                }, 			  
                 
                {
                    title: 'Status',
                    target: 6,
                    data: function (item) {
                        //return (item.status==1)?'Active':'In-Active';
			var htm = '';
			htm +='<div class="waitList__otp no-padding" style="float: left; margin-right: 20px;">';
		        htm +='<div class="toggle__container" style="margin-left: 0;">';
		        if(item.status==1){
		          var ariaPressed = true;
		          var activeClass = 'active';
		        } else {
		          var ariaPressed = false;
		          var activeClass = '';
		        }
		        htm +='<button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom statusid'+item.id+' menu_cat_active '+activeClass+'"';
		        htm +=' title="Open" data-toggle="button" data-cat="'+item.id+'" onclick="changeStatus('+item.id+')"  aria-pressed=""'+ariaPressed+'" autocomplete="off">';
		        htm +='<div class="handle"></div> </button> </div> </div>';
			return htm;
                    }
                },
                {
                    title: 'Action',
                    target: 7,
                    data: function (item) {
			 var htm = '';
			     htm +='<a title="View" style="width:100%;margin:10px 0;" href="/v2/menu/item/'+item.id+'/0/0/list"><span class="icon-view"></span></a>&nbsp;&nbsp;';
			     htm +='<a title="View" style="width:100%;margin:10px 0;" href="/v2/menu_category/'+item.id+'/edit"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;&nbsp;';
			     htm +='';
 			    //htm +='<button type="button" class="btn btn-info open-child-modal"  value="'+item.id+'" onclick="createAttributeValue('+item.id+')">Add Child </button>';		
			    //htm += '<button type="button" class="btn btn-info open-modal" value="'+item.id+'">Edit </button>';
		             //htm += '<button type="button" class="btn btn-danger delete-link" value="'+item.id+'">Delete </button>';
				
                        return htm;
                    }
                }
 ];
 


        $('#table_id1').DataTable({
     	//'select': { 'style': 'multi', 'selector': 'td:not(:first-child)' },
	    'columns': columns,
	    'ajax': '/v2/menu_category/0/{{$restaurant_id}}/list',
	    
	    responsive: true,
	    dom:'<"top"fiB><"bottom"trlp><"clear">',	
	    buttons: [
          {
            text: 'Add Category',
            action: function ( e, dt, node, config ) {window.location="{{ URL::to('/v2/menu_category/create') }}";}
          }
        //   {
        //     extend: 'colvis',
        //     text: '<i class="fa fa-eye"></i>',
        //     titleAttr: 'Column Visibility'
        //   },
        //   {
        //     extend: 'excelHtml5',
        //     text: '<i class="fa fa-file-excel-o"></i>',
        //     titleAttr: 'Excel'
        //   }, 
        //   {
        //     extend: 'pdfHtml5',
        //     text: '<i class="fa fa-file-pdf-o"></i>',
        //     titleAttr: 'PDF'
        //   },
        //   {               
        //     extend: 'print',
        //     text: '<i class="fa fa-print"></i>',
        //     titleAttr: 'Print',
        //     exportOptions: {
        //     columns: ':visible',
        //   },
        // }
      ],
      columnDefs: [ {
                    targets: -1,
                    visible: true
      }],
      language: {
                    search: "",
                    searchPlaceholder: "Search"
      },
      treeGrid: {
		    'left': 10,
 
		    'expandIcon': '<span>+<\/span>',
		    'collapseIcon': '<span>-<\/span>'
	 },
	//responsive: true,
	//dom:'<"top"fiB><"bottom"trlp><"clear">',	
	//  buttons: [
	// 	  {
	// 	    text: 'Add Category',
	// 	    action: function ( e, dt, node, config ) {window.location="{{ URL::to('/v2/menu_category/create') }}";}
	// 	  },
	// 	  {extend: 'excelHtml5'}, 
	// 	  {extend: 'pdfHtml5'},   
	// 	  {               
	// 	                extend: 'print',
	// 	                exportOptions: {
	// 	                    columns: ':visible'
	// 	                }
	// 	  },
	// 	  'colvis'
	// ],
	columnDefs: [ {
		            targets: -1,
		            visible: true
	}],
	language: {
		            search: "",
		            searchPlaceholder: "Search"
	}
       
    });
    $('h4').on('click', function () {
	 var h4 = $(this);
	 if (h4.hasClass('show')) {
		 h4.removeClass('show').addClass('showed').html('-hide code');
		 h4.next().removeClass('hide');
	 } else {
		 h4.removeClass('showed').addClass('show').html('+show code');
		 h4.next().addClass('hide');
	  }
    }); 
    	 
});
function changeStatus(cat_id) {   
            //var cat_id = $(this).data('cat'); 
            var active_flag = 1;
            if($('.statusid'+cat_id).hasClass('active')) {
                active_flag = 0;
            }
            $.ajax({
                url: '/v2/menu_category/toggleactive',
                type: 'POST',
                data: {
                    'cat_id': cat_id,
                    'active_flag': active_flag
                },
                success: function (data) {
                    alertbox('Updated',data.message,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            panel.remove();
                        }, 2000);
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                        }, 2000);
                    });
                }
            });
}
function changeDelivery(cat_id) {   
            //var cat_id = $(this).data('cat'); 
            var active_flag = 1;
            if($('.deliveryid'+cat_id).hasClass('active')) {
                active_flag = 0;
            }
            $.ajax({
                url: '/v2/menu_category/toggledelivery',
                type: 'POST',
                data: {
                    'cat_id': cat_id,
                    'active_flag': active_flag
                },
                success: function (data) {
                    alertbox('Updated',data.message,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            panel.remove();
                        }, 2000);
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                        }, 2000);
                    });
                }
            });
}
function changeCarryout(cat_id) {   
            //var cat_id = $(this).data('cat'); 
            var active_flag = 1;
            if($('.takeoutid'+cat_id).hasClass('active')) {
                active_flag = 0;
            }
            $.ajax({
                url: '/v2/menu_category/togglecarryout',
                type: 'POST',
                data: {
                    'cat_id': cat_id,
                    'active_flag': active_flag
                },
                success: function (data) {
                    alertbox('Updated',data.message,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            panel.remove();
                        }, 2000);
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                        }, 2000);
                    });
                }
            });
}
function shortMenu(){
	var type_id = $('#menu_sort_by').val();
	//if(type_id>0)
	window.location = '/v2/menu/item/0/{{$restaurant_id}}/'+type_id+'/list'; 
}
</script>
