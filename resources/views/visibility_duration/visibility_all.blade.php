@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<div class="main__container container__custom_working">
    <div class="text-right"><a href="{{url('configure/visibility-duration/create')}}"> Add</a></div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Resturant</th>
            <th scope="col">Name</th>
            {{--<th scope="col">Day</th>
            <th scope="col">Day Off</th>--}}
            <th scope="col">Slot Details</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @if(count($visibilityData) > 0)
            @foreach($visibilityData as $data)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{ $data->restaurant_name }}</td>
            <td>{{ $data->slot_name }}</td>
            {{--<td>{{ $data->calendar_day }}</td>
            <td>{{ $data->is_dayoff }}</td>--}}
            <td><a href="javascript:void()" onclick="fetchSlotDetail('{{base64_encode($data->slot_name)}}')">view</a></td>
            <td><a href="{{ url('configure/visibility-duration/'.$data->id.'/edit') }}"><i class="glyphicon glyphicon-edit"></i></a></td>
        </tr>
            @endforeach
         @else
        <tr>No record found.</tr>
        @endif
        </tbody>
    </table>

    {{----modal--}}
    <div class="modal fade modal-popup" id="show-slot-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                {{--<div class="modal-header">
                    <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Turnover Time Alert</h4>
                </div>--}}
                <div class="modal-body text-center" id="slotData">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        function fetchSlotDetail(slot_name){
            $.ajax({
                type: "POST",
                url: "visibility-duration/fetch/slot/detail",
                data: {slot_name : slot_name},
                cache: false,
                success: function(data){
                    $('#show-slot-detail').modal('show');
                    $("#slotData").html(data);
                }
            });
        }
    </script>
@endsection