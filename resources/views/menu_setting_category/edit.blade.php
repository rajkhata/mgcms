@extends('layouts.app')
@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>{{ __('Menu Setting Category - Edit') }}</h1>

            </div>

        </div>

        <div>

            <div class="card form__container">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="form__user__edit">
                        {{ Form::model($menuSettingCategory, array('route' => array('menu_setting_category.update', $menuSettingCategory->id), 'method' => 'PUT')) }}
                        @csrf
                        <div class="form-group row form_field__container">
                            <label for="restaurant_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                            <div class="col-md-4">
                                <select name="restaurant_id" id="restaurant_id"
                                        class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                                        required>
                                    <option value="">Select Restaurant</option>
                                    @foreach($groupRestData as $rest)
                                        <optgroup label="{{ $rest['restaurant_name'] }}">
                                            @foreach($rest['branches'] as $branch)
                                                <option value="{{ $branch['id'] }}" {{ ((old('restaurant_id')==$branch['id']) || (count(old())==0 && isset($menuSettingCategory->restaurant_id) && $menuSettingCategory->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                                @if ($errors->has('restaurant_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Default Language') }}</label>
                            <div class="col-md-4">
                                <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                                    <option value="">Select Language</option>
                                    @foreach($languageData as $lang)
                                        <option value="{{ $lang->id }}" {{ ($menuSettingCategory->language_id==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('language_id'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('language_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="restaurant_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>
                            <div class="col-md-4">
                                <select name="menu_category_id" id="menu_category_id"
                                        class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}">
                                    <option value="">Select Category</option>
                                    @php
                                        if(session()->has('newCategoryData')){
                                            $categoryData = session()->get('newCategoryData');
                                        }
                                    @endphp
                                    @foreach($categoryData as $cat)
                                        <option value="{{ $cat->id }}" {{ (old('menu_category_id')==$cat->id || (count(old())==0 && $menuSettingCategory->menu_category_id==$cat->id)) ? 'selected' :'' }}>{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('menu_category_id'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('menu_category_id') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
			<div class="form-group row form_field__container">
                            <label for="display_order" class="col-md-4 col-form-label text-md-right">{{ __('Display Order') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="display_order" type="text"
                                        class="{{ $errors->has('display_order') ? ' is-invalid' : '' }}"
                                        name="display_order" value="{{ count(old())==0 ? $menuSettingCategory->display_order : old('display_order') }}" >
                                </div>
                                @if ($errors->has('display_order'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('display_order') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>  
                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-4">
                                <input id="name" type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name"
                                       value="{{ isset($menuSettingCategory->name)?$menuSettingCategory->name:old('name') }}"
                                       required>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                          <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Maximum Selection') }}</label>
                            <div class="col-md-4">
                                <input id="maximum_selection" type="number"
                                       class="form-control{{ $errors->has('maximum_selection') ? ' is-invalid' : '' }}"
                                       name="maximum_selection"
                                       value="{{ isset($menuSettingCategory->maximum_selection)?$menuSettingCategory->maximum_selection:old('maximum_selection') }}"
                                       >
                                @if ($errors->has('maximum_selection'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('maximum_selection') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Minimum Selection') }}</label>
                            <div class="col-md-4">
                                <input id="minimum_selection" type="number"
                                       class="form-control{{ $errors->has('minimum_selection') ? ' is-invalid' : '' }}"
                                       name="minimum_selection"
                                       value="{{ isset($menuSettingCategory->minimum_selection)?$menuSettingCategory->minimum_selection:old('minimum_selection') }}"
                                       >
                                @if ($errors->has('minimum_selection'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('minimum_selection') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Equal Selection') }}</label>
                            <div class="col-md-4">
                                <input id="equal_selection" type="number"
                                       class="form-control{{ $errors->has('equal_selection') ? ' is-invalid' : '' }}"
                                       name="equal_selection"
                                       value="{{ isset($menuSettingCategory->equal_selection)?$menuSettingCategory->equal_selection:old('equal_selection') }}"
                                       >
                                @if ($errors->has('equal_selection'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('equal_selection') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="is_multi_select"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Selection Type') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="is_multi_select1" name="is_multi_select"
                                       value="1" {{ ((isset($menuSettingCategory->is_multi_select) && $menuSettingCategory->is_multi_select=='1' && count(old())==0) || old('is_multi_select')=='1') ? 'checked' :'' }}>
                                Multi Select
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="is_multi_select0" name="is_multi_select"
                                       value="0" {{ ((isset($menuSettingCategory->is_multi_select) && $menuSettingCategory->is_multi_select=='0' && count(old())==0) || old('is_multi_select')=='0') ? 'checked' :'' }}>
                                Single Select
                                @if ($errors->has('is_multi_select'))
                                    <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('is_multi_select') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="is_mandatory"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Mandatory') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="is_mandatory1" name="is_mandatory"
                                       value="1" {{ ((isset($menuSettingCategory->is_mandatory) && $menuSettingCategory->is_mandatory=='1' && count(old())==0) || old('is_mandatory')=='1') ? 'checked' :'' }}>
                                Yes
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="is_mandatory0" name="is_mandatory"
                                       value="0" {{ ((isset($menuSettingCategory->is_mandatory) && $menuSettingCategory->is_mandatory=='0' && count(old())==0) || old('is_mandatory')=='0') ? 'checked' :'' }}>
                                No
                                @if ($errors->has('is_mandatory'))
                                    <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('is_mandatory') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="status1" name="status"
                                       value="1" {{ ((isset($menuSettingCategory->status) && $menuSettingCategory->status=='1' && count(old())==0) || old('status')=='1') ? 'checked' :'' }}>
                                Active
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="status0" name="status"
                                       value="0" {{ ((isset($menuSettingCategory->status) && $menuSettingCategory->status=='0' && count(old())==0) || old('status')=='0') ? 'checked' :'' }}>
                                Inactive
                                @if ($errors->has('status'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <br/><br/>
                        <div class=" row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $('option:selected').val();
                $('#menu_category_id').find('option').not(':first').remove();
                if (rest_id != '') {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_category"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
                        success: function (data) {
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj.cat) !== 'undefined') {
                                    $.each(data.dataObj.cat, function (i, item) {
                                        $('#menu_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    });
                                }
                            }
                        }
                    });
                }
            });
        });
    </script>

@endsection
