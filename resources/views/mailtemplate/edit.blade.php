@extends('layouts.app')
@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">

            <div class="card">
                <div class="card-header">{{ __('User - Edit') }}</div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'files' => true)) }}
                        @csrf
                        <div class="form-group row">
                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                            <div class="col-md-4">
                                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" >
                                    <option value="">Select Restaurant</option>
                                    @foreach($restaurantData as $rest)
                                        <option value="{{ $rest->id }}" {{ (old('restaurant_id')==$rest->id || (count(old())==0 && $user->restaurant_id==$rest->id)) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('restaurant_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
                            <div class="col-md-4">
                                <input id="fname" type="text" class="form-control{{ $errors->has('fname') ? ' is-invalid' : '' }}"
                                       name="fname" value="{{ count(old())==0 ? $user->fname : old('fname') }}" required autocomplete="off">
                                @if ($errors->has('fname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>
                            <div class="col-md-4">
                                <input id="lname" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}"
                                       name="lname" value="{{ count(old())==0 ? $user->lname : old('lname') }}" required autocomplete="off">
                                @if ($errors->has('lname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
                            <div class="col-md-4">
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email" value="{{ count(old())==0 ? $user->email : old('email') }}" required autocomplete="off">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-md-4">
                                <input id="password" type="text" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" value="{{ old('password') }}" required autocomplete="off">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1' || (count(old())==0 && $user->status=='1')) ? 'checked' :'' }}> Active
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0' || (count(old())==0 && $user->status=='0')) ? 'checked' :'' }}> Inactive
                                @if ($errors->has('status'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="form-group row source-image-div image-caption-div">
                            <label for="display_image" class="col-md-4 col-form-label text-md-right">{{ __('Display Image') }}</label>
                            <div class="col-md-4">
                                <input id="display_image" type="file" class="form-control{{ $errors->has('display_image') ? ' is-invalid' : '' }}"
                                       value="{{ old('display_image') }}" name="display_image">
                                @if ($errors->has('display_image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('display_image') }}</strong>
                                    </span>
                                @endif
                            </div>
                            @if(isset($user->display_pic_url) && $user->display_pic_url!='')
                                <div class="col-md-4">
                                    @php
                                        $displayPic = strpos($user->display_pic_url, 'http')!==false ? $user->display_pic_url : url('/').'/..'.$user->display_pic_url;
                                    @endphp
                                    <a data-fancybox="gallery" href="{{url('/').'/..'.$user->display_pic_url}}"><img src="{{ $displayPic }}" style="width:100px;"></a>&emsp;
                                    <input type="checkbox" name="display_image_delete" value="1" style="margin-left:20px;">Delete
                                </div>
                            @endif
                        </div>--}}

                        <br /><br />
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Save User') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
