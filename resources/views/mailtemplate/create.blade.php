@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Create Mail Template') }}</h1>

    </div>

  </div>
  <div>

    <div class="card ">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif


      <div class="card-body">
        <div class="form__user__edit1">
<form name="loaddefaultform" method="get">

            <div class="form-group row form_field__container">
              <label for="template_id" class="col-md-4 col-form-label text-md-right">{{ __('Load Default Template') }}</label>
              <div class="col-md-6">

                <select class="form-control" name="default_template">
                  <option value="">--Select--</option>
                  <?php
                  if(count($default_templates)){
                    foreach ($default_templates as $key=>$item) {
                  ?>
                  <optgroup label="<?php echo $key;?>">
                    <?php
                    if(count($item)){
                      foreach ($item as $val) {
                        ?>
                        <option value="<?php echo $val['template_name'];?>"    <?php if(isset($_REQUEST['default_template']) && $_REQUEST['default_template']==$val['template_name']){echo  'selected';} ?>><?php echo $val['name'];?></option>
                      <?php
                      }
                    }
                    ?>
                  </optgroup>
                  <?php
                    }
                   }
                   ?>

                </select>

              </div>
              <div> <button type="submit" class="btn btn__primary">
                  {{ __('Load') }}
                </button></div>
            </div>
</form>
          <form method="POST" action="{{ route('mailtemplate.create') }}" enctype="multipart/form-data">
            <input id="sms_id" type="hidden" name="template_id" value="{{ isset($mailtemplate->id)?$mailtemplate->id:'' }}">
            @csrf
            <div class="form-group row form_field__container">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Template Name') }}</label>
              <div class="col-md-6">
                <input id="name"  type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                       name="name" value="{{ isset($mailtemplate->name)?$mailtemplate->name:'' }}" required>

                @if ($errors->has('name'))
                  <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <?php
            //print_r($default_template);
            ?>
            <div class="form-group row form_field__container">
              <label for="subject" class="col-md-4 col-form-label text-md-right">{{ __('Subject') }}</label>
              <div class="col-md-6">
                <?php
                      $subject='';
                if(isset($default_template->subject) && !empty($default_template->subject)){
                   $subject=$default_template->subject;
                }else{
                  $subject=$mailtemplate->subject;
                }
                ?>
                <input id="subject"  type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}"
                       name="subject" value="{{ $subject }}" required>



                @if ($errors->has('subject'))
                  <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('subject') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
{{--
              <label for="template_id" class="col-md-4 col-form-label text-md-right">{{ __('Template Name') }}</label>
--}}
              <div class="col-md-6">
                <input id="template_name"  type="hidden" class="form-control{{ $errors->has('template_name') ? ' is-invalid' : '' }}"
                name="template_name" value="{{ isset($mailtemplate->template_name)?$mailtemplate->template_name:'' }}" required>

                @if ($errors->has('template_name'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('template_name') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="content" class="col-md-2 col-form-label text-md-right">{{ __('Template') }}</label>
              <?php
              $content='';
              if(isset($default_template->content) && !empty($default_template->content)){
                $content=$default_template->content;
              }else{
                $content=$mailtemplate->content;
              }
              ?>
              <div class="col-md-8">
                <textarea name="content" id="content" class="form-control{{ $errors->has('content') ? 'is-invalid':''}}" required>{{ $content }}</textarea>
                <!--                                    <input id="content" type="text" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}"
                name="content" value="{{ old('content') }}" required>-->
                @if ($errors->has('content'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('content') }}</strong>
                </span>
                @endif
              </div>

            </div>

            <div class="form-group row form_field__container">
              <label for="content" class="col-md-2 col-form-label text-md-right">{{ __('Template Style') }}</label>

              <div class="col-md-8">
                <textarea name="template_style" id="content" class="form-control{{ $errors->has('template_style') ? 'is-invalid':''}}" required>{{ isset($mailtemplate->template_style)?$mailtemplate->template_style:'' }}</textarea>

                @if ($errors->has('template_style'))
                  <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('template_style') }}</strong>
                </span>
                @endif
              </div>

            </div>

            <div class="form-group row form_field__container">
              <label for="credit card" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-6" style="margin:auto 0px">
                <input type="radio"  name="status" value="1" {{ (old('status')=='1') ? 'checked' :'' }}> Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio"  name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }}> No

                @if ($errors->has('status'))
                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>

            </div>
            <div class="row mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">
                  {{ __('Add') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>

  </div>
</div>
@endsection

<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
tinymce.init({
  selector: '#content',
  height: 320,
  theme: 'modern',
  plugins: 'code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern ',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,


});
</script>
