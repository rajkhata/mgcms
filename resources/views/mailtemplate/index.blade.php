@extends('layouts.app')

@section('content')

<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Mail Templates<span style="margin:0px 5px;">({{ $template->currentPage() .'/' . $template->lastPage() }})</span>&emsp;</h1>
    </div>
    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('mailtemplate/create') }}" class="btn btn__primary">Add</a></span>
  </div>
  <div class="addition__functionality">
    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">

      <div class="row form-group col-md-2">
        <div class="input__field">
          <input id="tName" type="text" name="name" value="{{isset($searchArr['template_name']) ? $searchArr['template_name'] : ''}}">
          <label for="tName">Template Name</label>
        </div>
      </div>
      <div class="row form-group col-md-3">
        <button class="btn btn__primary" style="margin-left: 5px;" type="submit" >Search</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>



  <div>

    <div class="active__order__container">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif

      <div class="guestbook__container box-shadow">
        <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
          <div class="row col-md-10">
            <div class="col-md-5" style="font-weight: bold;">Template No.</div>
            <div class="col-md-5" style="font-weight: bold;">Template Name</div>
            <div class="col-md-2" style="font-weight: bold;">Status</div>
          </div>

          <div class="row col-md-2">
            <div class="col-md-12" style="font-weight: bold;">Action</div>
          </div>


        </div>
        @if(isset($template) && count($template)>0)

        @foreach($template as $keys => $value)
        <div class="guestbook__customer-details-content rest__row">
          <div class="row col-md-10">
            <div class="col-md-5" style="margin: auto 0px;">{{ $value->id }}</div>
            <div class="col-md-5" style="margin: auto 0px;">{{ $value->template_name }}</div>
            <div class="col-md-2" style="margin: auto 0px;">{{ ($value->status==1)?'Active':($value->status==0?'Inactive':'') }}</div>
          </div>
          <div class="row col-md-2" style="margin: auto 0px;">
            <div class="col-md-12">
              <a style="margin:10px 0; width:100%;" href="{{ URL::to('mailtemplate/' . $value->id . '/update') }}" class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
            </div>

            <div class="col-md-12">
              {!! Form::open(['method' => 'DELETE', 'route' => ['mailtemplate.destroy', $value->id]]) !!}
              @csrf
              <button style="margin:10px 0; width:100%;" class="btn btn__cancel" type="submit"><span class="glyphicon glyphicon-trash"></span> Delete</button>
              {!! Form::close() !!}
            </div>

          </div>
        </div>

        @endforeach
        @else
        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
          No Record Found
        </div>
        @endif
      </div>

      @if(isset($template) && count($template)>0)
      <div class="text-center" style="margin: 0px auto;">
        {{ $template->appends($_GET)->links()}}
      </div>
      @endif
    </div>

  </div>
</div>
@endsection
