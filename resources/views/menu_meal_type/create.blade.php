@extends('layouts.app')
@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>{{ __('Meal Type - Create') }}</h1>

            </div>

        </div>

        <div>

            <div class="card form__container">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="form__user__edit">
                        <form method="POST" action="{{ route('menu_meal_type.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row form_field__container">
                                <label for="restaurant_id"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                                <div class="col-md-4">
                                    <select name="restaurant_id" id="restaurant_id"
                                            class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="">Select Restaurant</option>
                                        @foreach($restaurantData as $rest)
                                            <optgroup label="{{ $rest['restaurant_name'] }}">
                                                @foreach($rest['branches'] as $branch)
                                                    <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                    {{--<select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" >
                                        <option value="">Select Restaurant</option>
                                        @foreach($restaurantData as $rest)
                                            <option value="{{ $rest->id }}" {{ (old('restaurant_id')==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                                        @endforeach
                                    </select>--}}
                                    @if ($errors->has('restaurant_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        <div class="form-group row form_field__container">
                        <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Default Language') }}</label>
                        <div class="col-md-4">
                        <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                            <option value="">Select Language</option>
                            @foreach($languageData as $lang)
                                <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('language_id'))
                            <span class="invalid-feedback">
                        <strong>{{ $errors->first('language_id') }}</strong>
                        </span>
                        @endif
                        </div>
                        </div>

                            <div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="name" type="text"
                                               class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               name="name" value="{{ old('name') }}" required>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row form_field__container">
                                <label for="from_time"
                                       class="col-md-4 col-form-label text-md-right">{{ __('From Time') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="from_time" type="text" name="from_time"
                                               class="timepicker {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               value="{{old('from_time')}}"/>
                                    </div>
                                    @if ($errors->has('from_time'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('from_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row form_field__container">
                                <label for="from_time"
                                       class="col-md-4 col-form-label text-md-right">{{ __('To Time') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="to_time" type="text" name="to_time"
                                               class="timepicker {{ $errors->has('to_time') ? ' is-invalid' : '' }}"
                                               value="{{old('to_time')}}"/>
                                    </div>
                                    @if ($errors->has('to_time'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('to_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                                <div class="col-md-4" style="padding-top: 10px;">
                                    <input type="radio" id="status1" name="status"
                                           value="1" {{ (old('status')=='1') ? 'checked' :'' }}> Active
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="status0" name="status"
                                           value="0" {{ (old('status')=='0') ? 'checked' :'' }}> Inactive
                                    @if ($errors->has('status'))
                                        <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <br/><br/>
                            <div class=" mb-0 ">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            //var options = { now: "12:35", //hh:mm 24 hour format only, defaults to current time twentyFour: false, //Display 24 hour format, defaults to false upArrow: 'wickedpicker__controls__control-up', //The up arrow class selector to use, for custom CSS downArrow: 'wickedpicker__controls__control-down', //The down arrow class selector to use, for custom CSS close: 'wickedpicker__close', //The close class selector to use, for custom CSS hoverState: 'hover-state', //The hover state class to use, for custom CSS title: 'Timepicker', //The Wickedpicker's title, showSeconds: false, //Whether or not to show seconds, secondsInterval: 1, //Change interval for seconds, defaults to 1  , minutesInterval: 1, //Change interval for minutes, defaults to 1 beforeShow: null, //A function to be called before the Wickedpicker is shown show: null, //A function to be called when the Wickedpicker is shown clearable: false, //Make the picker's input clearable (has clickable "x")  };
            // var options1 = { {{old('from_time') ? ('now:"'.old('from_time').'",') : ''}}twentyFour: true  };
            // var options2 = { {{old('to_time') ? ('now:"'.old('to_time').'",') : ''}}twentyFour: true  };
            var fTime = '{{old('from_time') ? old('from_time') : '00:00'}}';
            var tTime = '{{old('to_time') ? old('to_time') : '00:00'}}';
            var options1 = {now: fTime, twentyFour: true, showSeconds: true};
            var options2 = {now: tTime, twentyFour: true, showSeconds: true};
            $('#to_time').wickedpicker(options2);
            $('#from_time').wickedpicker(options1);
        });
        var check_restaurant = '<?php echo $selected_rest_id;?>';
        if (check_restaurant != 0) {
            // trigger restautaurnat checks
            $("#restaurant_id").val(check_restaurant);
            //$("#restaurant_id").attr('change');
        }
    </script>

@endsection
