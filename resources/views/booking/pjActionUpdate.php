<?php
if (isset($tpl['status']))
{
	$status = __('status', true);
	switch ($tpl['status'])
	{
		case 2:
			pjUtil::printNotice(NULL, $status[2]);
			break;
	}
} else {
	if (isset($_GET['err']))
	{
		$titles = __('error_titles', true);
		$bodies = __('error_bodies', true);
		pjUtil::printNotice(@$titles[$_GET['err']], @$bodies[$_GET['err']]);
	}
	$week_start = isset($tpl['option_arr']['o_week_start']) && in_array((int) $tpl['option_arr']['o_week_start'], range(0,6)) ? (int) $tpl['option_arr']['o_week_start'] : 0;
	$jqDateFormat = pjUtil::jqDateFormat($tpl['option_arr']['o_date_format']);
	
	list($date_from, $time_from) = explode(" ", $tpl['arr']['dt']);
	list($hour_from, $minute_from,) = explode(":", $time_from);
	
	list($date_to, $time_to) = explode(" ", $tpl['arr']['dt_to']);
	list($hour_to, $minute_to,) = explode(":", $time_to);
	
	$ampm_arr = array('am' => 'AM', 'pm' => 'PM');
	$hour_arr = array('12', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11');
	$time_ampm = 0;
	$ampm_from = 'am';
	$ampm_to = 'am';
	if(strpos($tpl['option_arr']['o_time_format'], 'a') > -1)
	{
		$time_ampm = 1;
		$_ts = strtotime($tpl['arr']['dt']);
		$hour_from = date('h', $_ts);
		$minute_from = date('i', $_ts);
		$ampm_from = date('a', $_ts);
		$_ts = strtotime($tpl['arr']['dt_to']);
		$hour_to = date('h', $_ts);
		$minute_to = date('i', $_ts);
		$ampm_to = date('a', $_ts);
	}
	if(strpos($tpl['option_arr']['o_time_format'], 'A') > -1)
	{
		$time_ampm = 2;
		$_ts = strtotime($tpl['arr']['dt']);
		$hour_from = date('h', $_ts);
		$minute_from = date('i', $_ts);
		$ampm_from = date('A', $_ts);
		$_ts = strtotime($tpl['arr']['dt_to']);
		$hour_to = date('h', $_ts);
		$minute_to = date('i', $_ts);
		$ampm_to = date('A', $_ts);
	}
	?>
	<div class="ui-tabs ui-widget ui-widget-content ui-corner-all b10">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?controller=pjAdminBookings&amp;action=pjActionSchedule"><?php __('menuSchedule'); ?></a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?controller=pjAdminBookings&amp;action=pjActionIndex"><?php __('menuBookingList'); ?></a></li>
		</ul>
	</div>
	<?php
	pjUtil::printNotice(__('infoUpdateBookingTitle', true, false), __('infoUpdateBookingDesc', true, false)); 
	?>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>?controller=pjAdminBookings&amp;action=pjActionUpdate&amp;id=<?php echo $tpl['arr']['id']; ?>" method="post" class="form pj-form" id="frmUpdateBooking">
		<input type="hidden" name="booking_update" value="1" />
		<input type="hidden" id="id" name="id" value="<?php echo $tpl['arr']['id']; ?>" />
		<input type="hidden" name="tab_id" value="<?php echo isset($_GET['tab_id']) && !empty($_GET['tab_id']) ? $_GET['tab_id'] : 'tabs-1'; ?>" />
		
		<div id="tabs">
			<ul>
				<li><a href="#tabs-1"><?php __('lblBookingDetails');?></a></li>
				<li><a href="#tabs-2"><?php __('lblClientDetails');?></a></li>
			</ul>
		
		
			<div id="tabs-1">
				<div class="float_right">
					<p>
						<label class="title-block"><?php __('lblMadeOn'); ?></label>
						<label class="title-block"><?php echo pjUtil::formatDate(date('Y-m-d', strtotime($tpl['arr']['created'])), 'Y-m-d', $tpl['option_arr']['o_date_format']) . ', ' . pjUtil::formatTime(date('H:i:s', strtotime($tpl['arr']['created'])), 'H:i:s', $tpl['option_arr']['o_time_format']); ?></label>
					</p>
					<p>
						<label class="title-block"><?php __('lblIpAddress'); ?></label>
						<label class="title-block"><?php echo $tpl['arr']['ip']; ?></label>
					</p>
					<p>
						<label class="title-block"><a href="#" class="pjRbSendConfirm" data-id="<?php echo $tpl['arr']['id']?>"><?php __('lblBookingRemind'); ?></a></label>
						<label class="title-block"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?controller=pjAdminBookings&amp;action=pjActionPrinter&amp;id=<?php echo $tpl['arr']['id']; ?>" target="_blank"><?php __('lblBookingPrint'); ?></a></label>
						<label class="title-block"><a href="#" class="pjRbSendCancel" data-id="<?php echo $tpl['arr']['id']?>"><?php __('lblSendCancellationEmail'); ?></a></label>
					</p>
				</div>
				<p>
					<label class="title"><?php __('lblDateTimeFrom'); ?></label>
					<span class="pj-form-field-custom pj-form-field-custom-after float_left r5">
						<input type="text" name="date" id="date" class="pj-form-field pointer w80 datepick" readonly="readonly" rel="<?php echo $week_start; ?>" rev="<?php echo $jqDateFormat; ?>" value="<?php echo pjUtil::formatDate($date_from, 'Y-m-d', $tpl['option_arr']['o_date_format']); ?>" />
						<span class="pj-form-field-after"><abbr class="pj-form-field-icon-date"></abbr></span>
					</span>
					<span class="inline_block">
					<?php
					if($time_ampm == 0)
					{
						echo pjTime::factory()
							->attr('name', 'hour')
							->attr('id', 'hour')
							->attr('class', 'pj-form-field pj-form-field-select pj-booking-time')
							->prop('selected', $hour_from)
							->hour();
						echo pjTime::factory()
							->attr('name', 'minute')
							->attr('id', 'minute')
							->attr('class', 'pj-form-field pj-form-field-select pj-booking-time')
							->prop('step', 5)
							->prop('selected', $minute_from)
							->minute();
					}else{
						?>
						<select id="hour" name="hour" class="pj-form-field pj-form-field-select pj-booking-time">
							<?php
							foreach($hour_arr as $v)
							{
								?><option value="<?php echo $v;?>"<?php echo $hour_from == $v ? ' selected="selected"' : null;?>><?php echo $v;?></option><?php
							} 
							?>
						</select>
						<select id="minute" name="minute" class="pj-form-field pj-form-field-select pj-booking-time">
							<?php
							for($i = 0; $i < 60; $i = $i + 5)
							{
								?><option value="<?php echo str_pad($i, 2, '0', STR_PAD_LEFT); ;?>"<?php echo $minute_from == $i ? ' selected="selected"' : null;?>><?php echo str_pad($i, 2, '0', STR_PAD_LEFT); ;?></option><?php
							} 
							?>
						</select>
						<select id="ampm" name="ampm" class="pj-form-field pj-form-field-select pj-booking-time">
							<?php
							foreach($ampm_arr as $k => $v)
							{
								?><option value="<?php echo $time_ampm == 1 ? $k : $v;?>"<?php echo strtolower($ampm_from) == $k ? ' selected="selected"' : null;?>><?php echo $time_ampm == 1 ? $k : $v;?></option><?php
							} 
							?>
						</select>
						<?php
					}
					?>
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblDateTimeTo'); ?></label>
					<span class="inline_block">
						<span class="pj-form-field-custom pj-form-field-custom-after float_left r5">
							<input type="text" name="date_to" id="date_to" class="pj-form-field pointer w80 datepick" readonly="readonly" rel="<?php echo $week_start; ?>" rev="<?php echo $jqDateFormat; ?>"  value="<?php echo pjUtil::formatDate($date_to, 'Y-m-d', $tpl['option_arr']['o_date_format']); ?>" />
							<span class="pj-form-field-after"><abbr class="pj-form-field-icon-date"></abbr></span>
						</span>
						<span class="inline_block">
						<?php
						if($time_ampm == 0)
						{
							echo pjTime::factory()
								->attr('name', 'hour_to')
								->attr('id', 'hour_to')
								->attr('class', 'pj-form-field pj-form-field-select')
								->prop('selected', $hour_to)
								->hour();
							
							echo pjTime::factory()
								->attr('name', 'minute_to')
								->attr('id', 'minute_to')
								->attr('class', 'pj-form-field pj-form-field-select')
								->prop('selected', $minute_to)
								->prop('step', 5)
								->minute();
						}else{
							?>
							<select id="hour_to" name="hour_to" class="pj-form-field pj-form-field-select pj-booking-time">
								<?php
								foreach($hour_arr as $v)
								{
									?><option value="<?php echo $v;?>"<?php echo $hour_to == $v ? ' selected="selected"' : null;?>><?php echo $v;?></option><?php
								} 
								?>
							</select>
							<select id="minute_to" name="minute_to" class="pj-form-field pj-form-field-select pj-booking-time">
								<?php
								for($i = 0; $i < 60; $i = $i + 5)
								{
									?><option value="<?php echo str_pad($i, 2, '0', STR_PAD_LEFT); ;?>"<?php echo $minute_to == $i ? ' selected="selected"' : null;?>><?php echo str_pad($i, 2, '0', STR_PAD_LEFT); ;?></option><?php
								} 
								?>
							</select>
							<select id="ampm_to" name="ampm_to" class="pj-form-field pj-form-field-select pj-booking-time">
								<?php
								foreach($ampm_arr as $k => $v)
								{
									?><option value="<?php echo $time_ampm == 1 ? $k : $v;?>"<?php echo strtolower($ampm_to) == $k ? ' selected="selected"' : null;?>><?php echo $time_ampm == 1 ? $k : $v;?></option><?php
								} 
								?>
							</select>
							<?php
						}
						?>
						</span>
						<input type="hidden" id="validate_datetime" name="validate_datetime" value="<?php echo mt_rand(1, 9999); ?>"/>
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblUniqueID');?></label>
					<span class="inline-block">
						<input type="text" id="uuid" name="uuid" value="<?php echo pjSanitize::clean($tpl['arr']['uuid']); ?>" class="pj-form-field w136"/>
					</span>
				</p>
				<div class="p">
					<label class="title"><?php __('lblStatus'); ?></label>
					<span class="inline-block">
						<select name="status" id="status" class="pj-form-field w150 required">
							<option value="">-- <?php __('lblChoose'); ?>--</option>
							<?php
							foreach (__('booking_statuses', true, false) as $k => $v)
							{
								?><option value="<?php echo $k; ?>"<?php echo $k == $tpl['arr']['status'] ? ' selected="selected"' : NULL; ?>><?php echo $v; ?></option><?php
							}
							?>
						</select>
					</span>
				</div>
				<p>
					<label class="title"><?php __('lblDepositFee'); ?></label>
					<span class="pj-form-field-custom pj-form-field-custom-before">
						<span class="pj-form-field-before"><abbr class="pj-form-field-icon-text"><?php echo pjUtil::formatCurrencySign(NULL, $tpl['option_arr']['o_currency'], ""); ?></abbr></span>
						<input type="text" id="total" name="total" value="<?php echo pjSanitize::clean($tpl['arr']['total']); ?>" class="pj-form-field number required w108"/>
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblVoucherCode'); ?></label>
					<span class="inline-block">
						<input type="text" name="code" id="code" value="<?php echo pjSanitize::clean($tpl['arr']['code']); ?>" class="pj-form-field float_left r10 w136"/>
						<label id="discount_format" style="display:<?php echo $tpl['discount'] != '' ? 'block' : 'none';?>;"><?php echo $tpl['discount'] != '' ? $tpl['discount'] : null;?></label>
					</span>
				</p>
				
				<div class="p">
					<label class="title"><?php __('lblDepositPaid'); ?></label>
					<div id="is_paid">
					<?php
					foreach (__('booking_is_paids', true, false) as $k => $v)
					{
						?><input type="radio" id="is_paid_<?php echo $k; ?>" name="is_paid" value="<?php echo $k; ?>"<?php echo $k == 'F' ? ' checked="checked"' : NULL; ?><?php echo $tpl['arr']['is_paid'] == $k ? ' checked="checked"' : NULL; ?> /><label for="is_paid_<?php echo $k; ?>"><?php echo $v; ?></label><?php
					}
					?>
					</div>
				</div>
				<p>
					<label class="title"><?php __('lblPaymentMethod');?></label>
					<span class="inline-block">
						<select name="payment_method" id="payment_method" class="pj-form-field w150">
							<option value="">-- <?php __('lblChoose'); ?>--</option>
							<?php
							foreach (__('payment_methods', true, false) as $k => $v)
							{
								?><option value="<?php echo $k; ?>"<?php echo $k == $tpl['arr']['payment_method'] ? ' selected="selected"' : NULL; ?>><?php echo $v; ?></option><?php
							}
							?>
						</select>
					</span>
				</p>
				<p class="boxCC" style="display: <?php echo $tpl['arr']['payment_method'] != 'creditcard' ? 'none' : 'block'; ?>">
					<label class="title"><?php __('lblCCType'); ?></label>
					<span class="inline-block">
						<select name="cc_type" class="pj-form-field w150">
							<option value="">---</option>
							<?php
							foreach (__('cc_types', true, false) as $k => $v)
							{
								if (isset($tpl['arr']['cc_type']) && $tpl['arr']['cc_type'] == $k)
								{
									?><option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option><?php
								} else {
									?><option value="<?php echo $k; ?>"><?php echo $v; ?></option><?php
								}
							}
							?>
						</select>
					</span>
				</p>
				<p class="boxCC" style="display: <?php echo $tpl['arr']['payment_method'] != 'creditcard' ? 'none' : 'block'; ?>">
					<label class="title"><?php __('lblCCNum'); ?></label>
					<span class="inline-block">
						<input type="text" name="cc_num" id="cc_num" value="<?php echo pjSanitize::clean($tpl['arr']['cc_num']); ?>" class="pj-form-field w136"/>
					</span>
				</p>
				<p class="boxCC" style="display: <?php echo $tpl['arr']['payment_method'] != 'creditcard' ? 'none' : 'block'; ?>">
					<label class="title"><?php __('lblCCExp'); ?></label>
					<span class="inline-block">
						<select name="cc_exp_month" class="pj-form-field">
							<option value="">---</option>
							<?php
							list($year, $month) = explode("-", $tpl['arr']['cc_exp']);
							$month_arr = __('months', true, false);
							ksort($month_arr);
							foreach ($month_arr as $key => $val)
							{
								?><option value="<?php echo $key;?>"<?php echo (int) $month == $key ? ' selected="selected"' : NULL; ?>><?php echo $val;?></option><?php
							}
							?>
						</select>
						<select name="cc_exp_year" class="pj-form-field">
							<option value="">---</option>
							<?php
							$y = (int) date('Y');
							for ($i = $y; $i <= $y + 10; $i++)
							{
								?><option value="<?php echo $i; ?>"<?php echo $year == $i ? ' selected="selected"' : NULL; ?>><?php echo $i; ?></option><?php
							}
							?>
						</select>
					</span>
				</p>
				<p class="boxCC" style="display: <?php echo $tpl['arr']['payment_method'] != 'creditcard' ? 'none' : 'block'; ?>">
					<label class="title"><?php __('lblCCCode'); ?></label>
					<span class="inline-block">
						<input type="text" name="cc_code" id="cc_code" value="<?php echo pjSanitize::clean($tpl['arr']['cc_code']); ?>" class="pj-form-field w100" />
					</span>
				</p>
				
				<p>
					<label class="title"><?php __('lblPeople'); ?></label>
					<span class="inline-block">
						<input type="text" name="people" id="people" class="pj-form-field field-int w80" value="<?php echo pjSanitize::clean($tpl['arr']['people']); ?>" />
					</span>
				</p>
				<div class="p">
					<label class="title"><?php __('lblTable'); ?></label>
					<table id="tblBookingTables" class="pj-table" cellpadding="0" cellspacing="0" style="width: auto;">
						<thead>
							<tr>
								<th><?php __('lblTableName', false, true); ?></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if (count($tpl['bt_arr']) > 0)
						{
							foreach ($tpl['bt_arr'] as $table_id)
							{
								?>
								<tr class="pj-table-row-odd">
									<td>
										<select name="table_id[]" class="rbTable select w150">
											<option value="">---</option>
											<?php
											foreach ($tpl['table_arr'] as $table)
											{
												?>
												<option value="<?php echo $table['id']; ?>"<?php echo $table_id == $table['id'] ? ' selected="selected"' : NULL; ?>>
													<?php echo stripslashes($table['name']); ?>, <?php echo $table['seats'] . ' ' . ($table['seats'] > 1 ? __('lblPeople', true, false) : __('lblPerson', true, false)); ?>
												</option>
												<?php
											}
											?>
										</select>
									</td>
									<td><a href="<?php echo $_SERVER['PHP_SELF']; ?>" class="btnRemoveTable"></a></td>
								</tr>
								<?php
							}
						} 
						?>
						</tbody>
					</table>
				</div>
				<p>
					<label class="title">&nbsp;</label>
					<span class="inline_block">
						<a href="<?php echo $_SERVER['PHP_SELF']; ?>" class="btnAddTable"><?php __('lblAddTable');?></a>
						<input type="hidden" id="validate_table" name="validate_table" class="required" value="<?php echo count($tpl['bt_arr']) > 0 ? mt_rand(1, 9999) : NULL; ?>"/>
					</span>
				</p>
				<p>
					<label class="title">&nbsp;</label>
					<input type="submit" value="<?php __('btnSave', false, true); ?>" class="pj-button" />
					<input type="button" value="<?php __('btnCancel'); ?>" class="pj-button" onclick="window.location.href='<?php echo PJ_INSTALL_URL; ?>index.php?controller=pjAdminBookings&action=pjActionIndex';" />
				</p>
			</div>
			
			<div id="tabs-2">
				<p>
					<label class="title"><?php __('lblBookingTitle'); ?></label>
					<span class="inline-block">
						<select name="c_title" id="c_title" class="pj-form-field w150<?php echo $tpl['option_arr']['o_bf_include_title'] == 3 ? ' required' : NULL; ?>">
							<option value="">-- <?php __('lblChoose'); ?>--</option>
							<?php
							$title_arr = pjUtil::getTitles();
							$name_titles = __('name_titles', true, false);
							foreach ($title_arr as $v)
							{
								?><option value="<?php echo $v; ?>"<?php echo $tpl['arr']['c_title'] == $v ? ' selected="selected"' : NULL; ?>><?php echo $name_titles[$v]; ?></option><?php
							}
							?>
						</select>
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingFname'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_fname" id="c_fname" class="pj-form-field w250<?php echo $tpl['option_arr']['o_bf_include_fname'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_fname'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingLname'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_lname" id="c_lname" class="pj-form-field w250<?php echo $tpl['option_arr']['o_bf_include_lname'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_lname'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingPhone'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_phone" id="c_phone" class="pj-form-field w250<?php echo $tpl['option_arr']['o_bf_include_phone'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_phone'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingEmail'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_email" id="c_email" class="pj-form-field w250<?php echo $tpl['option_arr']['o_bf_include_email'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_email'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingNotes'); ?></label>
					<span class="inline-block">
						<textarea name="c_notes" id="c_notes" class="pj-form-field w500 h120<?php echo $tpl['option_arr']['o_bf_include_notes'] == 3 ? ' required' : NULL; ?>"><?php echo stripslashes($tpl['arr']['c_notes']); ?></textarea>
					</span>
				</p>
				
				<p>
					<label class="title"><?php __('lblBookingCompany'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_company" id="c_company" class="pj-form-field w300<?php echo $tpl['option_arr']['o_bf_include_company'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_company'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingAddress'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_address" id="c_address" class="pj-form-field w300<?php echo $tpl['option_arr']['o_bf_include_address'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_address'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingCity'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_city" id="c_city" class="pj-form-field w300<?php echo $tpl['option_arr']['o_bf_include_city'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_city'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingState'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_state" id="c_state" class="pj-form-field w300<?php echo $tpl['option_arr']['o_bf_include_state'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_state'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingZip'); ?></label>
					<span class="inline-block">
						<input type="text" name="c_zip" id="c_zip" class="pj-form-field w300<?php echo $tpl['option_arr']['o_bf_include_zip'] == 3 ? ' required' : NULL; ?>" value="<?php echo htmlspecialchars(stripslashes($tpl['arr']['c_zip'])); ?>" />
					</span>
				</p>
				<p>
					<label class="title"><?php __('lblBookingCountry'); ?></label>
					<span class="inline-block">
						<select name="c_country" id="c_country" class="pj-form-field w300<?php echo $tpl['option_arr']['o_bf_include_country'] == 3 ? ' required' : NULL; ?>">
							<option value="">-- <?php __('lblChoose'); ?>--</option>
							<?php
							foreach ($tpl['country_arr'] as $v)
							{
								?><option value="<?php echo $v['id']; ?>"<?php echo $tpl['arr']['c_country'] == $v['id'] ? ' selected="selected"' : NULL; ?>><?php echo stripslashes($v['country_title']); ?></option><?php
							}
							?>
						</select>
					</span>
				</p>
				<p>
					<label class="title">&nbsp;</label>
					<input type="submit" value="<?php __('btnSave', false, true); ?>" class="pj-button" />
					<input type="button" value="<?php __('btnCancel'); ?>" class="pj-button" onclick="window.location.href='<?php echo PJ_INSTALL_URL; ?>index.php?controller=pjAdminBookings&action=pjActionIndex';" />
				</p>
			</div>
		</div>
	</form>
	<div style="display: none" title="<?php __('lblBookingAvailability'); ?>" id="dialogAvailability"></div>
	
	<div id="dialogConfirmation" title="<?php __('booking_confirmation_title'); ?>" style="display: none"></div>
	<div id="dialogCancellation" title="<?php __('booking_cancellation_title'); ?>" style="display: none"></div>
	
	<script type="text/javascript">
	var myLabel = myLabel || {};
	myLabel.uuid_used = "<?php __('uuid_used', false, true); ?>";
	myLabel.validate_datetime = "<?php __('lblValidateDateTime', false, true); ?>";
	myLabel.validate_table = "<?php __('lblValidateTable', false, true); ?>";
	var disabledDates = [];
	var disabledWeekDays = [];
	var enabledDates = [];
	<?php
	foreach($tpl['date_arr'] as $k => $v)
	{
		if($v['is_dayoff'] == 'T')
		{
			?>disabledDates.push("<?php echo date('m-j-Y', strtotime($v['date']));?>");<?php
		}else{
			?>enabledDates.push("<?php echo date('m-j-Y', strtotime($v['date']));?>");<?php
		}
	}
	$week_arr = array('sunday'=>0,'monday'=>1,'tuesday'=>2,'wednesday'=>3,'thursday'=>4,'friday'=>5,'saturday'=>6);
	foreach($tpl['week_dayoff_arr'] as $k => $v)
	{
		?>disabledWeekDays.push(<?php echo $week_arr[$k];?>);<?php
	}  
	?>
	</script>	
	<?php
	if (isset($_GET['tab_id']) && !empty($_GET['tab_id']))
	{		
		$tab_id = $_GET['tab_id'];
		$tab_id = $tab_id < 0 ? 0 : $tab_id;
		?>
		<script type="text/javascript">
		(function ($) {
			$(function () {
				$("#tabs").tabs("option", "selected", <?php echo $tab_id; ?>);
			});
		})(jQuery);
		</script>
		<?php
	}
}
?>