<tr class="pj-table-row-odd">
	<td>
		<select name="table_id[]" class="rbTable select w150">
			<option value="">---</option>
			<?php
			foreach ($tpl['table_arr'] as $table)
			{
				?>
					<option value="<?php echo $table['id']; ?>">
						<?php echo stripslashes($table['name']); ?>, <?php echo $table['seats'] . ' ' . ($table['seats'] > 1 ? __('lblPeople', true, false) : __('lblPerson', true, false)); ?>
					</option>
				<?php
			}
			?>
		</select>
	</td>
	<td><a href="<?php echo $_SERVER['PHP_SELF']; ?>" class="btnRemoveTable"></a></td>
</tr>