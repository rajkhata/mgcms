@extends('layouts.app')
@section('content')
<div class="b10">
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" class="float_left pj-form r10">
		<input type="hidden" name="controller" value="pjAdminBookings" />
		<input type="hidden" name="action" value="pjActionCreate" />
		<input type="submit" class="pj-button" value="+ Add reservation" />
	</form>
	<form action="" method="get" class="float_left pj-form frm-filter">
		<input type="text" name="q" class="pj-form-field pj-form-field-search w150" placeholder="Search"/>
		<button type="button" class="pj-button pj-button-detailed"><span class="pj-button-detailed-arrow"></span></button>
	</form>
	<div class="float_right t5">
		<a href="#" class="pj-button btn-all">All</a>
		<a href="#" class="pj-button btn-filter btn-status btn-resv-filter" data-column="status" data-value="confirmed">Confirmed</a>
		<a href="#" class="pj-button btn-filter btn-status btn-resv-filter" data-column="status" data-value="pending">Pending</a>
		<a href="#" class="pj-button btn-filter btn-status btn-resv-filter" data-column="status" data-value="cancelled">Cancelled</a>
		<a href="#" class="pj-button btn-filter btn-status btn-resv-filter" data-column="status" data-value="enquiry">Enquiry</a>
	</div>
	<br class="clear_both" />
</div>
@endsection
