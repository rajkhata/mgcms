@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
            <div class="reservation__atto">
                <div class="reservation__title col-md-8 no-col-padding">
                    <h1>Service Disabled Report
                        
                    </h1>
                </div>
            </div>
            <div>
                <div class="active__order__container">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @elseif (session()->has('err_msg'))
                        <div class="alert alert-danger">
                            {{ session()->get('err_msg') }}
                        </div>
                    @endif

                    <div class="guestbook__container box-shadow no-margin-top">
                         
      <table id="table_id" class="responsive ui cell-border hover " style="width:100%; margin-bottom:10px;">
          <thead>
        <tr>
            <th>Bar Name</th>
            <th>Penalty Charge</th>
            <th>Disabled Date</th>
        </tr>
          </thead>
         <tbody>
        @if(isset($servicedisabledrecord) && count($servicedisabledrecord)>0)
                @foreach($servicedisabledrecord as $keys => $value)
        <tr>
              <td>{{ $value->restaurant_name }}</td>
              <td> £ {{ $value->amount }}</td>
              <td>{{ $value->created_at }}</td>
        </tr>
                @endforeach
                        @else
                            <tr>
                                 <td colspan = "3" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03); ">
                                No Record Found
                            </td></tr>
                        @endif
                    </tbody>
            </table>
                </div>
            </div>
    </div>
<script>  
$(document).ready( function () {
       $('#table_id').DataTable(  {
      responsive: true,
            dom:'<"top"fiB><"bottom"trlp><"clear">',
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye"></i>',
                    titleAttr: 'Column Visibility'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                }, 
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                },
                {               
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    exportOptions: {
                        columns: ':visible',
                    },
                }
            ],
            columnDefs: [ {
                targets: -1,
                visible: false
            }],
            language: {
                search: "",
                searchPlaceholder: "Search"
            },
            responsive: true
    });
        //$('#table_id').show();
        //$('#table_id').removeClass('hidden');
  } );
    </script>
 
@endsection
