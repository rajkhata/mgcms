@extends('layouts.app')

@section('content')
<div class="content-container loginpage">
<div class="container">
    
        <div class="col-md-12  col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">Reset Password</div>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card-body">

                    <form method="POST" id="resetPassword" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row margin-bottom-5">
                            <label for="email" class="col-xs-12 col-form-label text-md-right text-right margin-top-5">{{ __('E-Mail Address') }}</label>

                            <div class="col-xs-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" />
                            </div>

                        </div>
                        <div class="col-xs-12"></div>
                            <div class="col-xs-12">
                                @if ($errors->has('email'))
                                        <span class="invalid-feedback color-red font-weight-500">
                                          {{ $errors->first('email') }}
                                        </span>
                                 @endif
                            </div>
                        <div class="form-group row mb-0">
                            <!-- <div class="col-xs-4">&nbsp;</div> -->
                            <div class="col-xs-12 send-link">
                                <button type="submit" class="btn btn__primary">Send Password Reset Link</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    
</div>
</div>

<script type="text/javascript">
    $("#resetPassword").validate({
        rules: {
            'email': {
                required: true,
                email: true
            }
        },
        messages:{
            email:"Please enter the valid email address."
        }
    });
</script>

@endsection
