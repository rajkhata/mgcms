@extends('layouts.app')

@section('content')
<div class="content-container loginpage">
<div class="container">

    
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">Login</div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </div>
                @endif


                <div class="card-body">
                    <form method="POST" id="loginForm" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-xs-12 col-form-label text-md-right text-right margin-top-5">{{ __('E-Mail Address') }}</label>

                            <div class="col-xs-12">
                                <input id="identity" type="text" class="form-control{{ $errors->has('identity') ? ' is-invalid' : '' }}" name="identity" value="{{ old('identity') }}" required autofocus>

                                @if ($errors->has('identity'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('identity') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-xs-12 col-form-label text-md-right text-right margin-top-5">{{ __('Password') }}</label>

                            <div class="col-xs-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div>

                        <div class="form-group row">
                            <!-- <div class="col-xs-4">&nbsp;</div> -->
                            <div class="col-xs-12">
                                <label class="custom_checkbox">
                                    <input class="hide" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    <span class="control_indicator"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <!-- <div class="col-xs-4">&nbsp;</div> -->
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn__primary">Login</button>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <!-- <div class="col-xs-4">&nbsp;</div> -->
                            <div class="col-xs-12 text-center">
                                <a class="font-weight-600" href="{{ route('password.request') }}">Forgot Your Password?</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

</div>

</div>


<div class="container margin-lr-auto templates-page hide">
    <div class="col-xs-12">
        <h1>Choose a Template to Start With</h1>
    </div>
    <div class="fullWidth flex-box flex-direction-row flex-wrap">
        <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
            <div class="fullWidth box-shadow">
                <img src="{{ asset('images/templates/template-1.jpg') }}">
            </div>
            <div class="fullWidth padding-top-bottom-15">
                <h2 class="font-size-14 font-weight-800">Template Name</h2>
                <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu. Nunc accumsan eros sem. Ut massa mauris.</p>
                <p class="text-center margin-top-15">
                    <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                    <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
            <div class="fullWidth box-shadow">
                <img src="{{ asset('images/templates/template-2.jpg') }}">
            </div>
            <div class="fullWidth padding-top-bottom-15">
                <h2 class="font-size-14 font-weight-800">Template Name</h2>
                <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Asan eros sem. Ut massa mauris.</p>
                <p class="text-center margin-top-15">
                    <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                    <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
            <div class="fullWidth box-shadow">
                <img src="{{ asset('images/templates/template-3.jpg') }}">
            </div>
            <div class="fullWidth padding-top-bottom-15">
                <h2 class="font-size-14 font-weight-800">Template Name</h2>
                <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu.</p>
                <p class="text-center margin-top-15">
                    <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                    <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
            <div class="fullWidth box-shadow">
                <img src="{{ asset('images/templates/template-4.jpg') }}">
            </div>
            <div class="fullWidth padding-top-bottom-15">
                <h2 class="font-size-14 font-weight-800">Template Name</h2>
                <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu. Nunc accumsan eros sem. Ut massa mauris.</p>
                <p class="text-center margin-top-15">
                    <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                    <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
            <div class="fullWidth box-shadow">
                <img src="{{ asset('images/templates/template-5.jpg') }}">
            </div>
            <div class="fullWidth padding-top-bottom-15">
                <h2 class="font-size-14 font-weight-800">Template Name</h2>
                <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu. Nunc accumsan eros sem. Ut massa mauris.</p>
                <p class="text-center margin-top-15">
                    <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                    <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30 template">
            <div class="fullWidth box-shadow">
                <img src="{{ asset('images/templates/template-6.jpg') }}">
            </div>
            <div class="fullWidth padding-top-bottom-15">
                <h2 class="font-size-14 font-weight-800">Template Name</h2>
                <p class="font-weight-600">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac nulla ligula. Fusce et hendrerit lacus, et interdum arcu. Nunc accumsan eros sem. Ut massa mauris.</p>
                <p class="text-center margin-top-15">
                    <a href="javascript:void(0)" class="btn btn__primary width-120 select-theme">Select</a>
                    <a href="#" target="_blank" class="btn btn__holo width-120 margin-left-10 font-weight-600">Preview</a>
                </p>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript">

        $("body").addClass("login-page");

        $(".select-theme").click(function(){
            ConfirmBox('Please Confirm','If you would like to use the selected template/layout for your website?',function(resp,curmodal){
                if(resp){
                    curmodal.modal('hide');
                }
            });
        });

        $("#loginForm").validate({
            rules: {
                'identity': {
                    required: true,
                    email: true
                },
                'password': {
                    required: true,
                }
            },
            messages:{
                identity:"Please enter the valid email address.",
                password:"Please enter the password.",
            }
        });
    </script>

@endsection
