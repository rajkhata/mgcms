@extends('layouts.app')

@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto manage__order-atto">
            <div class="reservation__title">
                <h1>Restaurants<span
                            style="float: right;margin:auto 5px;">({{ $restaurant->currentPage() .'/' . $restaurant->lastPage() }}
                        )</span></h1>
            </div>

            <div class="restName__add"><a href="{{ URL::to('restaurant/create') }}" class="btn btn__primary">Add</a>
            </div>
            {!! Form::open(['method'=>'get']) !!}
            <div class="search__manage-order">
                <div class="form_field__container">

                    <div class="row form-group col-md-5">
                        <select name="restaurant_id" id="restaurant_id" class="form-control">
                            <option value="">Select Restaurant</option>
                            @foreach($restaurantData as $rest)
                                <option value="{{ $rest->id }}" {{ (isset($inputData['restaurant_id']) && $inputData['restaurant_id']==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input__field">
                        <input id="rest_code" type="text" class="{{ $errors->has('rest_code') ? ' is-invalid' : '' }}"
                               name="rest_code"
                               value="{{ isset($inputData['rest_code']) ? $inputData['rest_code'] : NULL }}">
                        <label for="rest_code">Code</label>
                    </div>

                    <div class="row form-group col-md-5">
                        <button class="btn btn__primary" style="margin-left: 5px;" type="submit">Search</button>
                    </div>

                </div>

            </div>
            {!! Form::close() !!}
        </div>

        <div>

            <div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif

            <!-- <div class="row" style="margin-top:15px;justify-content: center;">

      <div class="row col-md-12">
      <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>
      <div class="col-md-2">

      @if ($errors->has('restaurant_name'))
                <span class="invalid-feedback" style="display: block;">
                <strong>{{ $errors->first('restaurant_name') }}</strong>
    </span>
    @endif
                    </div>
                    <label for="name" class="col-md-1 col-form-label text-md-right">{{ __('Code') }}</label>
  <div class="col-md-2">
  <input id="rest_code" type="text" class="form-control{{ $errors->has('rest_code') ? ' is-invalid' : '' }}"
  name="rest_code" value="{{ isset($inputData['rest_code']) ? $inputData['rest_code'] : NULL }}">
  @if ($errors->has('rest_code'))
                <span class="invalid-feedback" style="display: block;">
                <strong>{{ $errors->first('rest_code') }}</strong>
</span>
@endif
                    </div>
                    <div class="row form-group col-md-4">
                    <button class="btn btn-info" style="margin-left: 5px;" type="submit" >Search</button>
                    </div>
                    </div>

                    </div> -->

                <div class="guestbook__container box-shadow">
                    <div style="min-width: unset !important;"
                         class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
                        <div class="row col-md-12 ">
                            <div class="col-md-2" style="font-weight: bold;">Code</div>
                            <div class="col-md-2" style="font-weight: bold;">Name</div>
                            <div class="col-md-2" style="font-weight: bold;">Street</div>
                            <div class="col-md-2" style="font-weight: bold;">Zipcode</div>
                            <div class="col-md-2" style="font-weight: bold;">Status</div>
                            <div class="col-md-2" style="font-weight: bold;">Action</div>

                        </div>
                    </div>
                </div>
                @if(isset($restaurant) && count($restaurant)>0)
                    @foreach($restaurant as $rest)
                        <div class="row guestbook__customer-details-content order__row">
                            <div class="row col-md-12 order__row    " style="min-width:275px;">
                                @php if(!$rest->parent_restaurant_id) { $branchType = '(Head Office)'; } else { $branchType = '(Branch)'; } @endphp
                                <div class="col-md-2" style="margin: auto 0px;">{{ $rest->rest_code }}</div>
                                <div class="col-md-2" style="margin: auto 0px;">{{ $rest->restaurant_name .' ' . $branchType }}</div>
                                <div class="col-md-2" style="margin: auto 0px;">{{ $rest->street }}</div>
                                <div class="col-md-2" style="margin: auto 0px;">{{ $rest->zipcode }}</div>
                                <div class="col-md-2"
                                     style="margin: auto 0px;">{{ isset($rest->status) && $rest->status=="1" ? "Active" : "Inactive" }}</div>


                                <div class="row col-md-2" style="margin: auto 0px;">
                                    <div class="col-md-12">
                                        <a style="width:100%;margin:10px 0;"
                                           href="{{ URL::to('restaurant/' . $rest->id . '/edit') }}"
                                           class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
                                    </div>
                                    {{--<div class="col-md-12">
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['restaurant.destroy', $rest->id]]) !!}
                                        @csrf
                                        <button style="width:100%;margin:10px 0;" class="btn btn__cancel"
                                                type="submit">
                                            <span class="glyphicon glyphicon-trash"></span> Delete
                                        </button>
                                        {!! Form::close() !!}
                                    </div>--}}

                                    @php
                                    $user = Auth::user();
                                    @endphp
                                    {{--@if ($user->role=="admin" && $rest->parent_restaurant_id!=0)
                                     <div class="col-md-12">
                                       <a style="width:100%;margin:10px 0;"
                                           href="{{ URL::to('restaurant/' . $rest->id . '/import') }}"
                                           class="btn btn-primary"><span class="fa fa-upload"></span> Import</a>
                                    </div>

                                    @endif--}}

                                </div>
                            </div>
                        </div>
                            @endforeach
                            @else
                                <div class="row"
                                     style="padding:10px;text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                                    No Record Found
                                </div>
                            @endif
                        @if(isset($restaurant) && count($restaurant)>0)
                            <div style="margin: 0px auto;">
                                {{ $restaurant->appends($_GET)->links()}}
                            </div>
                        @endif
            </div>

        </div>
    </div>
@endsection
