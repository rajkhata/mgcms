@extends('layouts.app')

@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>{{ __('Create Restaurant') }}</h1>
            </div>
        </div>

        <div>

            <div class="card form__container">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="form__user__edit">
                        <form method="POST" action="{{ route('restaurant.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="">
                                <ul class="nav nav-tabs">
                                    <li class="{{!isset($section_tab)?'active':''}}"><a data-toggle="tab" href="#general">General</a></li>
                                    <li><a data-toggle="tab" href="#delivery">Delivery</a></li>
                                    <li><a data-toggle="tab" href="#takeout">Takeout</a></li>
                                    <li><a data-toggle="tab" href="#reservation">Reservation</a></li>
                                    <li><a data-toggle="tab" href="#social">Social</a></li>
                                    <li><a data-toggle="tab" href="#config">Config</a></li>
                                    <li><a data-toggle="tab" href="#media">Media</a></li>
                                    <li><a data-toggle="tab" href="#emails">Emails</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">

                                <div id="general" class="tab-pane {{!isset($section_tab)?'fade in active':''}}">
                                    <br>
                                    <div class="form-group row form_field__container">
                                        <label for="restaurant_id"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Parent Restaurant') }}</label>
                                        <div class="col-md-6">
                                            <select name="parent_restaurant_id" id="parent_restaurant_id" class="form-control{{ $errors->has('parent_restaurant_id') ? ' is-invalid' : '' }}">
                                                <option value="0">Select Parent restaurant/Leave it to make it Parent
                                                </option>
                                                @foreach($restaurant as $rest)
                                                    <option value="{{ $rest->id }}" {{(old('parent_restaurant_id')==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('parent_restaurant_id'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('parent_restaurant_id') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="is_default_outlet" class="col-md-4 col-form-label text-md-right">{{ __('Is Default Outlet') }}</label>
                                        <div class="col-md-6">
                                            <input type="radio" id="default_yes" class="delivery__radio" name="is_default_outlet" value="1" {{ (old('is_default_outlet')=='1') ? 'checked' :'' }} > Yes
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="default_no" class="delivery__radio" name="is_default_outlet" value="0" {{ (old('is_default_outlet')=='0') ? 'checked' :'' }} > No
                                            @if ($errors->has('is_default_outlet'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('is_default_outlet') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="restaurant_id"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Default Language') }}</label>
                                        <div class="col-md-4">
                                            <select name="language_id" id="language_id"
                                                    class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}">
                                                <option value="">Select Language</option>
                                                @foreach($languageData as $lang)
                                                    <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('language_id'))
                                                <span class="invalid-feedback"><strong>{{ $errors->first('language_id') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="supported_languages" class="col-md-4 col-form-label text-md-right">{{ __('Supported Languages') }}</label>
                                        <div class="col-md-4">
                                            <select name="supported_languages[]" id="supported_languages" multiple=""
                                                    class="form-control{{ $errors->has('supported_languages') ? ' is-invalid' : '' }}">
                                                <?php
                                                foreach($languageData as $lang) {
                                                    echo '<option value="'.$lang->id.'">'.ucfirst($lang->language_name) .'</option>';
                                                }
                                                ?>
                                            </select>
                                            @if ($errors->has('supported_languages'))
                                                <span class="invalid-feedback"><strong>{{ $errors->first('supported_languages') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name *') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="restaurant_name" type="text"
                                                       class="{{ $errors->has('restaurant_name') ? ' is-invalid' : '' }}"
                                                       name="restaurant_name" value="{{ old('restaurant_name') }}"
                                                       required autofocus>
                                            </div>
                                            @if ($errors->has('restaurant_name'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('restaurant_name') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                      <label for="slug" class="col-md-4 col-form-label text-md-right">{{ __('Slug *') }}</label>
                                      <div class="col-md-6">
                                        <div class="input__field">
                                          <input id="slug" type="text" class="{{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" value="{{ old('slug') }}" required>
                                        </div>
                                        @if ($errors->has('slug'))
                                          <span class="invalid-feedback" style="display: block;" ><strong>{{ $errors->first('slug') }}</strong></span>
                                        @endif
                                      </div>
                                    </div>


                                      <div class="form-group row form_field__container">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email *') }}</label>
                                        <div class="col-md-6">
                                          <div class="input__field">
                                            <input id="email" type="text" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                          </div>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" style="display: block;" ><strong>{{ $errors->first('email') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    {{--<div class="form-group row form_field__container">
                                        <label for="seats" class="col-md-4 col-form-label text-md-right">{{ __('Seats') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="total_seats" type="text"
                                                       class="{{ $errors->has('total_seats') ? ' is-invalid' : '' }}"
                                                       name="total_seats"
                                                       value="{{ old('total_seats') }}">
                                            </div>
                                            @if ($errors->has('total_seats'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('total_seats') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>--}}

                                    <div class="form-group row form_field__container">
                                        <label for="seats" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="title" type="text" class="{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title"
                                                       value="{{ old('title') }}">
                                            </div>
                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('title') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                                        <div class="col-md-6">
                                            <div class="custom__message-textContainer">
                                                <textarea maxlength="5000" rows="4" name="description" id="description" class="{{ $errors->has('description') ? ' is-invalid' : '' }}">{{ old('description') }}</textarea>
                                            </div>
                                            @if ($errors->has('description'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('description') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="header_script" class="col-md-4 col-form-label text-md-right">{{ __('Header Script') }}</label>
                                        <div class="col-md-6">
                                            <div class="custom__message-textContainer">
                                                <textarea maxlength="5000" rows="4" name="header_script" id="header_script" class="{{ $errors->has('header_script') ? ' is-invalid' : '' }}">{{ old('header_script') }}</textarea>
                                            </div>
                                            @if ($errors->has('header_script'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('header_script') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="footer_script" class="col-md-4 col-form-label text-md-right">{{ __('Footer Script') }}</label>
                                        <div class="col-md-6">
                                            <div class="custom__message-textContainer">
                                                <textarea maxlength="5000" rows="4" name="footer_script" id="footer_script" class="{{ $errors->has('footer_script') ? ' is-invalid' : '' }}">{{ old('footer_script') }}</textarea>
                                            </div>
                                            @if ($errors->has('footer_script'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('footer_script') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <fieldset>
                                        <legend>Meta Tags</legend>
                                        <div id="keyword_container">
                                            <div class="form-group row form_field__container">
                                                <input type="text" class="input__field" name="meta_tags_type[]" value="" placeholder="type">
                                                <div class="col-md-6">
                                                    <div class=""><input type="text" class="" name="meta_tags_label[]" value="" placeholder="label"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class=""><input type="text" class="" name="meta_tags_content[]" value="" placeholder="content"></div>
                                                </div>
                                                <a href="javsacript:void(0);" class="meta_tags_plus"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;
                                                <a href="javsacript:void(0);" class="meta_tags_minus"><i class="fa fa-minus"></i></a>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="form-group row form_field__container">
                                        <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile *') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="mobile" type="text" class="{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required autofocus>
                                            </div>
                                            @if ($errors->has('mobile'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('mobile') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="phone" type="text" class="{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}">
                                            </div>
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('phone') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="fax" class="col-md-4 col-form-label text-md-right">{{ __('Fax') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="fax" name="fax" value="{{ old('fax') }}"/>
                                            </div>
                                            @if ($errors->has('fax'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('fax') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="lat" class="col-md-4 col-form-label text-md-right">{{ __('Latitude *') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="lat" type="text" class="{{ $errors->has('lat') ? ' is-invalid' : '' }}" name="lat" value="{{ old('lat') }}">
                                            </div>
                                            @if ($errors->has('lat'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('lat') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="lng" class="col-md-4 col-form-label text-md-right">{{ __('Longitude *') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="lng" type="text" class="{{ $errors->has('lng') ? ' is-invalid' : '' }}" name="lng" value="{{ old('lng') }}">
                                            </div>
                                            @if ($errors->has('lng'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('lng') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="longitude" class="col-md-4 col-form-label text-md-right">&nbsp;</label>
                                        <div class="col-md-6">
                                            <a href="javascript:void(0);" id="geo_lat_lng_address">Click here</a> to verify latitude longitude
                                        </div>
                                    </div>
                                    <fieldset>
                                        <legend>Pickup Address</legend>
                                        <div class="form-group row form_field__container">
                                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address *') }}</label>
                                            <div class="col-md-6">
                                                <div class="input__field">
                                                    <input id="address" type="text" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required>
                                                </div>
                                                @if ($errors->has('address'))
                                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('address') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="street" class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>
                                            <div class="col-md-6">
                                                <div class="input__field">
                                                    <input id="street" type="text" class="{{ $errors->has('street') ? ' is-invalid' : '' }}" name="street" value="{{ old('street') }}">
                                                </div>
                                                @if ($errors->has('street'))
                                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('street') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="zipcode" class="col-md-4 col-form-label text-md-right">{{ __('Zipcode') }}</label>
                                            <div class="col-md-6">
                                                <div class="input__field">
                                                    <input id="zipcode" type="text" class="{{ $errors->has('zipcode') ? ' is-invalid' : '' }}" name="zipcode" value="{{old('zipcode') }}">
                                                </div>
                                                @if ($errors->has('zipcode'))
                                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('zipcode') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>
                                            <div class="col-md-4">
                                                <select name="country_id" id="country_id" class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}">
                                                    <option value="">Select Country</option>
                                                    @foreach($countryObj as $data)
                                                        <option value="{{ $data->id }}" {{ (old('id')==$data->id) ? 'selected' :'' }}>{{ isset($data->country_name)?ucfirst($data->country_name):'' }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('country_id'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('country_id') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>
                                            <div class="col-md-4">
                                                <select name="state_id" id="state_id" class="form-control{{ $errors->has('state_id') ? ' is-invalid' : '' }}">
                                                    <option value="">Select State</option>
                                                </select>
                                                @if ($errors->has('state_id'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('state_id') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
                                            <div class="col-md-4">
                                                <select name="city_id" id="city_id" class="form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}">
                                                    <option value="">Select City</option>
                                                </select>
                                                @if ($errors->has('city_id'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('city_id') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <legend>Contact Address</legend>
                                        <div class="form-group row form_field__container">
                                            <label for="contact_address" class="col-md-4 col-form-label text-md-right">{{ __('Address *') }}</label>
                                            <div class="col-md-6">
                                                <div class="input__field">
                                                    <input id="contact_address" type="text" class="{{ $errors->has('contact_address') ? ' is-invalid' : '' }}" name="contact_address" value="{{ old('contact_address')  }}" >
                                                </div>
                                                @if ($errors->has('contact_address'))
                                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('contact_address') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row form_field__container">
                                            <label for="contact_street" class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>
                                            <div class="col-md-6">
                                                <div class="input__field">
                                                    <input id="contact_street" type="text" class="{{ $errors->has('contact_street') ? ' is-invalid' : '' }}" name="contact_street" value="{{ old('contact_street') }}">
                                                </div>
                                                @if ($errors->has('contact_street'))
                                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('contact_street') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="contact_zipcode" class="col-md-4 col-form-label text-md-right">{{ __('Zipcode') }}</label>
                                            <div class="col-md-6">
                                                <div class="input__field">
                                                    <input id="contact_zipcode" type="text" class="{{ $errors->has('contact_zipcode') ? ' is-invalid' : '' }}" name="contact_zipcode" value="{{old('contact_zipcode') }}">
                                                </div>
                                                @if ($errors->has('contact_zipcode'))
                                                    <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('contact_zipcode') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="contact_country_id" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>
                                            <div class="col-md-4">
                                                <select name="contact_country_id" id="contact_country_id" class="form-control{{ $errors->has('contact_country_id') ? ' is-invalid' : '' }}">
                                                    <option value="">Select Country</option>
                                                    @foreach($countryObj as $data)
                                                        <option value="{{ $data->id }}" {{ (old('contact_country_id')==$data->id) ? 'selected' :'' }}>{{ isset($data->country_name)?ucfirst($data->country_name):'' }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('contact_country_id'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('contact_country_id') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="contact_state_id" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>
                                            <div class="col-md-4">
                                                <select name="contact_state_id" id="contact_state_id" class="form-control{{ $errors->has('contact_state_id') ? ' is-invalid' : '' }}">
                                                    <option value="">Select State</option>
                                                </select>
                                                @if ($errors->has('contact_state_id'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('contact_state_id') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container">
                                            <label for="contact_city_id" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
                                            <div class="col-md-4">
                                                <select name="contact_city_id" id="contact_city_id" class="form-control{{ $errors->has('contact_city_id') ? ' is-invalid' : '' }}">
                                                    <option value="">Select City</option>
                                                </select>
                                                @if ($errors->has('contact_city_id'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('contact_city_id') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="form-group row form_field__container">
                                        <label for="borough" class="col-md-4 col-form-label text-md-right">{{ __('Borough') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="borough" name="borough" value="{{ old('borough') }}"/>
                                            </div>
                                            @if ($errors->has('borough'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('borough') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="landmark" class="col-md-4 col-form-label text-md-right">{{ __('Landmark') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="landmark" name="landmark" value="{{ old('landmark') }}"/>
                                            </div>
                                            @if ($errors->has('landmark'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('landmark') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div id="delivery" class="tab-pane {{(isset($section_tab) && $section_tab=='delivery')?'active in':''}}">
                                    <br>

                                    <div class="form-group row form_field__container">
                                        <label for="is_delivery_geo" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Coverage') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <input type="radio" id="is_geo" class="delivery__radio" name="is_delivery_geo" value="1" {{ (old('is_delivery_geo')=='1') ? 'checked' :'' }} > GeoJSON
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="is_geo1" class="delivery__radio" name="is_delivery_geo" value="0" {{ (old('is_delivery_geo')=='0') ? 'checked' :'' }} > Zipcodes
                                            @if ($errors->has('is_delivery_geo'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('is_delivery_geo') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <?php if(old('is_delivery_geo')=='1'){
                                        $disp = "block";
                                        $disp2 = "none"; }
                                    else{
                                        $disp = "none";
                                        $disp2 = "block"; }?>

                                    <div class="form-group row form_field__container" id="delivery_geo" style="display:{{$disp}};">
                                        <label for="delivery_geo" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Geo') }}</label>
                                        <div class="col-md-5">
                                            <div class="custom__message-textContainer" style="margin-top:0;">

                                    <textarea rows="4" name="delivery_geo" id="delivery_geo"
                                              class="{{ $errors->has('delivery_geo') ? ' is-invalid' : '' }}">{{ old('delivery_geo') }}</textarea>
                                            </div>
                                            @if ($errors->has('delivery_geo'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery_geo') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container" id="delivery_zipcode" style="display:{{$disp}}; ">
                                        <label for="delivery_zipcode" class="col-md-4 col-form-label text-md-right" >{{ __('Delivery Zipcode') }}</label>
                                        <div class="col-md-5">
                                            <div class="custom__message-textContainer" style="margin-top: 0;">
                                                <textarea rows="4" name="delivery_zipcode" id="delivery_zipcode"
                                                          class="{{ $errors->has('delivery_zipcode') ? ' is-invalid' : '' }}">{{ old('delivery_zipcode') }}</textarea>
                                            </div>
                                            @if ($errors->has('delivery_zipcode'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery_zipcode') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="delivery" class="col-md-4 col-form-label text-md-right">{{ __('Delivery') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <input type="radio" name="delivery" value="1" {{ (old('delivery')=='1') ? 'checked' :'' }}> Yes
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="delivery" value="0" {{ (old('delivery')=='0') ? 'checked' :'' }}> No
                                            @if ($errors->has('delivery'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="delivery_from"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Delivery Interval') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="delivery_interval" type="text"
                                                       class="{{ $errors->has('delivery_interval') ? ' is-invalid' : '' }}"
                                                       name="delivery_interval"
                                                       value="{{ old('delivery_interval') }}" autofocus>
                                            </div>
                                            @if ($errors->has('delivery_interval'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery_interval') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="delivery_order_gap" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Order Gap') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="delivery_order_gap" type="text"
                                                       class="{{ $errors->has('delivery_order_gap') ? ' is-invalid' : '' }}"
                                                       name="delivery_order_gap"
                                                       value="{{ old('delivery_order_gap') }}" autofocus>
                                            </div>
                                            @if ($errors->has('delivery_order_gap'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery_order_gap') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group row form_field__container">
                                        <label for="delivery_from" class="col-md-4 col-form-label text-md-right">{{ __('Minimum Preparation Time/ASAP Gap') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="delivery_asap_gap" type="text"
                                                       class="{{ $errors->has('delivery_asap_gap') ? ' is-invalid' : '' }}"
                                                       name="delivery_asap_gap"
                                                       value="{{ old('delivery_asap_gap') }}" autofocus>
                                            </div>
                                            @if ($errors->has('delivery_asap_gap'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery_asap_gap') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--<div class="form-group row form_field__container">
                                        <label for="delivery_from"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Delivery email From') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="delivery_from" type="text"
                                                       class="{{ $errors->has('delivery_from') ? ' is-invalid' : '' }}"
                                                       name="delivery_from"
                                                       value="{{ old('delivery_from') }}" autofocus>
                                            </div>
                                            @if ($errors->has('delivery_from'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery_from') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="delivery_to"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Delivery email To') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="delivery_to" type="text"
                                                       class="{{ $errors->has('delivery_to') ? ' is-invalid' : '' }}"
                                                       name="delivery_to"
                                                       value="{{ old('delivery_to') }}">
                                            </div>
                                            @if ($errors->has('delivery_to'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery_to') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>--}}
                                    <div class="form-group row form_field__container">
                                        <label for="delivery_desc" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Description') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="delivery_desc" name="delivery_desc" value="{{ old('delivery_desc') }}"/>
                                            </div>
                                            @if ($errors->has('delivery_desc'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('delivery_desc') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="tip" class="col-md-4 col-form-label text-md-right">{{ __('Tip % (comma separated)') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="tip" type="text" class="{{ $errors->has('tip') ? ' is-invalid' : '' }}" name="tip" value="{{ old('tip') }}" autofocus>
                                            </div>
                                            @if ($errors->has('tip'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('tip') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="free_delivery" class="col-md-4 col-form-label text-md-right">{{ __('Free Delivery (on sub total above this value)') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="free_delivery" value="{{ old('free_delivery') }}"  name="free_delivery" value=""/>
                                            </div>
                                            @if ($errors->has('free_delivery'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('free_delivery') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Service Provider') }}</label>
                                        <div class="col-md-4">
                                            <select name="delivery_provider_id" id="delivery_provider_id" class="form-control{{ $errors->has('delivery_provider_id') ? ' is-invalid' : '' }}" >
                                                <option value="0">Service Provider</option>
                                                @foreach($deliveryService as $dataval)
                                                    <option value="{{ $dataval->id }}" {{ ( old('delivery_provider_id')==$dataval->id) ? 'selected' :'' }}>{{ isset($dataval->short_name)?ucfirst($dataval->short_name):'' }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('delivery_provider_id'))
                                                <span class="invalid-feedback"><strong>{{ $errors->first('delivery_provider_id') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="delivery_charge" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Charge (Flat)') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="delivery_charge" name="delivery_charge" value="{{ old('delivery_charge') }}" placeholder="%">
                                            </div>
                                            @if ($errors->has('delivery_charge'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('delivery_charge') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="minimum_delivery" class="col-md-4 col-form-label text-md-right">{{ __('Minimum Delivery') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="minimum_delivery" name="minimum_delivery" value="{{ old('minimum_delivery') }}" placeholder="0.0">
                                            </div>
                                            @if ($errors->has('minimum_delivery'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('minimum_delivery') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="delivery_service_charge" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Charge (%)') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="delivery_service_charge" name="delivery_service_charge" value="{{ old('delivery_service_charge') }}" placeholder="%">
                                            </div>
                                            @if ($errors->has('delivery_service_charge'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('delivery_service_charge') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="additional_charge   " class="col-md-4 col-form-label text-md-right">{{ __('Additional Charge') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <div class="input__field">
                                                <input type="text" id="additional_charge" name="additional_charge" value="{{ old('additional_charge') }}" placeholder="%">
                                            </div>
                                            @if ($errors->has('additional_charge'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('additional_charge') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

    </div>

      <div id="takeout" class="tab-pane {{(isset($section_tab) && $section_tab=='carryout')?'active in':''}}">
          <br>
          <div class="form-group row form_field__container">
              <label for="takeout" class="col-md-4 col-form-label text-md-right">{{ __('Takeout') }}</label>
              <div class="col-md-6" style="margin:auto 0px">
                  <input type="radio"  name="takeout" value="1" {{ (old('takeout')=='1') ? 'checked' :'' }}> Yes
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio"  name="takeout" value="0" {{ (old('takeout')=='0') ? 'checked' :'' }}> No
                  @if ($errors->has('takeout'))
                      <span class="invalid-feedback" style="display: block;" ><strong>{{ $errors->first('takeout') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="carryout_interval"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Takeout Interval') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="carryout_interval" type="text"
                                                       class="{{ $errors->has('carryout_interval') ? ' is-invalid' : '' }}"
                                                       name="carryout_interval"
                                                       value="{{ old('carryout_interval') }}" autofocus>
                                            </div>
                                            @if ($errors->has('carryout_interval'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('carryout_interval') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="carryout_order_gap"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Takeout Order Gap') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="carryout_order_gap" type="text"
                                                       class="{{ $errors->has('carryout_order_gap') ? ' is-invalid' : '' }}"
                                                       name="carryout_order_gap"
                                                       value="{{ old('carryout_order_gap') }}" autofocus>
                                            </div>
                                            @if ($errors->has('carryout_order_gap'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('carryout_order_gap') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="carryout_asap_gap"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Minimum Preparation Gap/ASAP Gap') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="carryout_asap_gap" type="text"
                                                       class="{{ $errors->has('carryout_asap_gap') ? ' is-invalid' : '' }}"
                                                       name="carryout_asap_gap"
                                                       value="{{ old('carryout_asap_gap') }}" autofocus>
                                            </div>
                                            @if ($errors->has('carryout_asap_gap'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('carryout_asap_gap') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>

                                    {{--<div class="form-group row form_field__container">
                                        <label for="dinning"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Dinning') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <input type="radio" name="dinning"
                                                   value="1" {{ (old('dinning')=='1') ? 'checked' :'' }}> Yes
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="dinning"
                                                   value="0" {{ (old('dinning')=='0') ? 'checked' :'' }}> No
                                            @if ($errors->has('dinning'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('dinning') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="carryout_from"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Carryout email From') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="carryout_from" type="text"
                                                       class="{{ $errors->has('carryout_from') ? ' is-invalid' : '' }}"
                                                       name="carryout_from"
                                                       value="{{ old('carryout_from') }}" autofocus>
                                            </div>
                                            @if ($errors->has('carryout_from'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('carryout_from') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">

                                        <label for="carryout_to"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Carryout email To') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="carryout_to" type="text"
                                                       class="{{ $errors->has('carryout_to') ? ' is-invalid' : '' }}"
                                                       name="carryout_to"
                                                       value="{{ old('carryout_to') }}">
                                            </div>
                                            @if ($errors->has('carryout_to'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('carryout_to') }}</strong>
                </span>
                  @endif
              </div>

          </div>--}}

      </div>

      <div id="reservation" class="tab-pane {{(isset($section_tab) && $section_tab=='reservation')?'active in':''}}">
                                    <br>

                                    <div class="form-group row form_field__container">
                                        <label for="reservation" class="col-md-4 col-form-label text-md-right">{{ __('Reservation') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <input type="radio"  name="reservation" value="1" {{ (old('reservation')=='1') ? 'checked' :'' }}> Yes
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio"  name="reservation" value="0" {{ (old('reservation')=='0') ? 'checked' :'' }}> No
                                            @if ($errors->has('reservation'))
                                                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('reservation') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--<div class="form-group row form_field__container">
                                        <label for="reservation_fee"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Reservation fee') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="reservation_fee" type="text"
                                                       class="{{ $errors->has('reservation_fee') ? ' is-invalid' : '' }}"
                                                       name="reservation_fee"
                                                       value="{{ old('reservation_fee') }}" autofocus>
                                            </div>
                                            @if ($errors->has('reservation_fee'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('reservation_fee') }}</strong>
                </span>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="reservation_w_payment"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Reservation w/ payment') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <input type="radio" id="reservation_w_payment1" name="reservation_w_payment"
                                                   value="1" {{ (old('reservation_w_payment')=='1') ? 'checked' :'' }}>
                                            Active
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="reservation_w_payment0" name="reservation_w_payment"
                                                   value="0" {{ (old('reservation_w_payment')=='0') ? 'checked' :'' }}>
                                            Inactive
                                            @if ($errors->has('reservation_w_payment'))
                                                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('reservation_w_payment') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group row form_field__container">
                                        <label for="reservation_wo_payment"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Reservation w/o payment') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <input type="radio" id="reservation_wo_payment1"
                                                   name="reservation_wo_payment"
                                                   value="1" {{ (old('reservation_wo_payment')=='1') ? 'checked' :'' }}>
                                            Active
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="reservation_wo_payment0"
                                                   name="reservation_wo_payment"
                                                   value="0" {{ (old('reservation_wo_payment')=='0') ? 'checked' :'' }}>
                                            Inactive
                                            @if ($errors->has('reservation_wo_payment'))
                                                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('reservation_wo_payment') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">

                                        <label for="reservation_w_dinein"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Reservation w/ dine-in') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <input type="radio" id="reservation_w_dinein1" name="reservation_w_dinein"
                                                   value="1" {{ (old('reservation_w_dinein')=='1') ? 'checked' :'' }}>
                                            Active
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="reservation_w_dinein0" name="reservation_w_dinein"
                                                   value="0" {{ (old('reservation_w_dinein')=='0') ? 'checked' :'' }}>
                                            Inactive
                                            @if ($errors->has('reservation_w_dinein'))
                                                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('reservation_w_dinein') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="closed" class="col-md-4 col-form-label text-md-right">{{ __('Closed') }}</label>
                                        <div class="col-md-6" style="margin:auto 0px">
                                            <input type="radio" id="closed1" name="closed"
                                                   value="1" {{ (old('closed')=='1') ? 'checked' :'' }}>
                                            Active
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="closed0" name="closed"
                                                   value="0" {{ (old('closed')=='0' ) ? 'checked' :'' }}>
                                            Inactive
                                            @if ($errors->has('closed'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('closed') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>--}}

                                    {{--<div class="form-group row form_field__container">
                                        <label for="reservation_from"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Reservation email From') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="reservation_from" type="text"
                                                       class="{{ $errors->has('reservation_from') ? ' is-invalid' : '' }}"
                                                       name="reservation_from"
                                                       value="{{ old('reservation_from') }}" autofocus>
                                            </div>
                                            @if ($errors->has('reservation_from'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('reservation_from') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="reservation_to"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Reservation email To') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="reservation_to" type="text"
                                                       class="{{ $errors->has('reservation_to') ? ' is-invalid' : '' }}"
                                                       name="reservation_to"
                                                       value="{{ old('reservation_to') }}">
                                            </div>
                                            @if ($errors->has('reservation_to'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('reservation_to') }}</strong>
                </span>
                  @endif
              </div>

          </div>--}}

                                </div>






      <div id="social" class="tab-pane {{(isset($section_tab) && $section_tab=='social')?'active in':''}}">
          <br>
          <div class="form-group row form_field__container">
              <label for="facebook url"
                     class="col-md-4 col-form-label text-md-right">{{ __('Facebook Url') }}</label>

              <div class="col-md-6">
                  <div class="input__field">
                      <input id="facebook_url" type="text"
                             class="{{ $errors->has('facebook_url') ? ' is-invalid' : '' }}" name="facebook_url"
                             value="{{ old('facebook_url') }}" autofocus>
                  </div>
                  @if ($errors->has('facebook_url'))
                      <span class="invalid-feedback" style="display: block;">

                  <strong>{{ $errors->first('facebook_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="twitter url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Twitter Url') }}</label>

                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="twitter_url" type="text"
                                                       class="{{ $errors->has('twitter_url') ? ' is-invalid' : '' }}"
                                                       name="twitter_url"
                                                       value="{{ old('twitter_url') }}">
                                            </div>
                                            @if ($errors->has('twitter_url'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('twitter_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="gmail url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Gmail Url') }}</label>

                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="gmail_url" type="text"
                                                       class="{{ $errors->has('gmail_url') ? ' is-invalid' : '' }}"
                                                       name="gmail_url"
                                                       value="{{ old('gmail_url') }}">
                                            </div>
                                            @if ($errors->has('gmail_url'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('gmail_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="pinterest url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Pinterest Url') }}</label>

                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="pinterest_url" type="text"
                                                       class="{{ $errors->has('pinterest_url') ? ' is-invalid' : '' }}"
                                                       name="pinterest_url"
                                                       value="{{ old('pinterest_url') }}">
                                            </div>
                                            @if ($errors->has('pinterest_url'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('pinterest_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="instagram_url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Instagram Url') }}</label>

                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="instagram_url" type="text"
                                                       class="{{ $errors->has('instagram_url') ? ' is-invalid' : '' }}"
                                                       name="instagram_url"
                                                       value="{{ old('instagram_url') }}">
                                            </div>
                                            @if ($errors->has('instagram_url'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('instagram_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="yelp url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Yelp Url') }}</label>

                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="yelp_url" type="text"
                                                       class="{{ $errors->has('yelp_url') ? ' is-invalid' : '' }}"
                                                       name="yelp_url"
                                                       value="{{ old('yelp_url') }}">
                                            </div>
                                            @if ($errors->has('yelp_url'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('yelp_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="trip advisor url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Trip Advisor Url') }}</label>

                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="tripadvisor_url" type="text"
                                                       class="{{ $errors->has('tripadvisor_url') ? ' is-invalid' : '' }}"
                                                       name="tripadvisor_url"
                                                       value="{{ old('tripadvisor_url') }}">
                                            </div>
                                            @if ($errors->has('tripadvisor_url'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('tripadvisor_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="Four square url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Four Square Url') }}</label>

                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="foursquare_url" type="text"
                                                       class="{{ $errors->has('foursquare_url') ? ' is-invalid' : '' }}"
                                                       name="foursquare_url"
                                                       value="{{ old('foursquare_url') }}">
                                            </div>
                                            @if ($errors->has('foursquare_url'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('foursquare_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="twitch url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Twitch Url') }}</label>

                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="twitch_url" type="text"
                                                       class="{{ $errors->has('twitch_url') ? ' is-invalid' : '' }}"
                                                       name="twitch_url"
                                                       value="{{ old('twitch_url') }}">
                                            </div>
                                            @if ($errors->has('twitch_url'))
                                                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('twitch_url') }}</strong>
                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="youtube url"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Youtube Url') }}</label>
                                        <div class="col-md-6">
                                            <div class="input__field">
                                                <input id="youtube_url" type="text"
                                                       class="{{ $errors->has('youtube_url') ? ' is-invalid' : '' }}"
                                                       name="youtube_url"
                                                       value="{{ old('youtube_url') }}">
                                            </div>
                                            @if ($errors->has('youtube_url'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('youtube_url') }}</strong></span>
                                              @endif
                                        </div>
                                    </div>

          <fieldset>
              <legend>API Keys</legend>

              <div class="form-group row form_field__container">
                  <label for="google_verification_code" class="col-md-4 col-form-label text-md-right">{{ __('Google Verification Code') }}</label>
                  <div class="col-md-6">
                      <div class="input__field">
                          <textarea maxlength="5000" rows="4" name="google_verification_code" id="google_verification_code" class="{{ $errors->has('google_verification_code') ? ' is-invalid' : '' }}">{{ old('google_verification_code') }}</textarea>
                      </div>
                      @if ($errors->has('google_verification_code'))
                          <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('google_verification_code') }}</strong></span>
                      @endif
                  </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="gtm_code" class="col-md-4 col-form-label text-md-right">{{ __('GTM Code') }}</label>
                  <div class="col-md-6">
                      <div class="custom__message-textContainer">
                          <input id="gtm_code" type="text"
                                 class="{{ $errors->has('gtm_code') ? ' is-invalid' : '' }}"
                                 name="gtm_code"
                                 value="{{  old('gtm_code') }}">
                      </div>
                      @if ($errors->has('gtm_code'))
                          <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('gtm_code') }}</strong></span>
                      @endif
                  </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="google_map_id" class="col-md-4 col-form-label text-md-right">{{ __('Google Map Id') }}</label>
                  <div class="col-md-6">
                      <div class="input__field">
                          <textarea maxlength="5000" rows="4" name="google_map_id" id="google_map_id" class="{{ $errors->has('google_map_id') ? ' is-invalid' : '' }}">{{ old('google_map_id') }}</textarea>
                      </div>
                      @if ($errors->has('google_map_id'))
                          <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('google_map_id') }}</strong></span>
                      @endif
                  </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="google_client_id" class="col-md-4 col-form-label text-md-right">{{ __('Google Client Id') }}</label>
                  <div class="col-md-6">
                      <div class="input__field">
                          <textarea maxlength="5000" rows="4" name="google_client_id" id="google_client_id" class="{{ $errors->has('google_client_id') ? ' is-invalid' : '' }}">{{ old('google_client_id') }}</textarea>
                      </div>
                      @if ($errors->has('google_client_id'))
                          <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('google_client_id') }}</strong></span>
                      @endif
                  </div>
              </div>
              <div class="form-group row form_field__container">
                  <label for="facebook_client_id" class="col-md-4 col-form-label text-md-right">{{ __('Facebook Client Id') }}</label>
                  <div class="col-md-6">
                      <div class="input__field">
                          <textarea maxlength="5000" rows="4" name="facebook_client_id" id="facebook_client_id" class="{{ $errors->has('facebook_client_id') ? ' is-invalid' : '' }}">{{ old('facebook_client_id') }}</textarea>
                      </div>
                      @if ($errors->has('facebook_client_id'))
                          <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('facebook_client_id') }}</strong></span>
                      @endif
                  </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="netcore_api_key" class="col-md-4 col-form-label text-md-right">{{ __('Netcore API Key') }}</label>
                  <div class="col-md-6">
                      <div class="input__field">
                          <textarea maxlength="5000" rows="4" name="netcore_api_key" id="netcore_api_key" class="{{ $errors->has('netcore_api_key') ? ' is-invalid' : '' }}">{{ old('netcore_api_key') }}</textarea>
                      </div>
                      @if ($errors->has('netcore_api_key'))
                          <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('netcore_api_key') }}</strong></span>
                      @endif
                  </div>
              </div>
          </fieldset>

      </div>

        <div id="config" class="tab-pane {{(isset($section_tab) && $section_tab=='config')?'active in':''}}">
            <br>

            <div class="form-group row form_field__container">
                <label for="city_tax" class="col-md-4 col-form-label text-md-right">When will the order be ready?</label>
                <div class="col-md-8 no-padding">
                    <div class="selct-picker-plain col-xs-4 no-padding-left padding-right-5">
                        <select class="selectpicker" data-size="5" data-style="no-background-with-buttonline padding-left-5 font-weight-600" name="update_days" id="update_days">
                            @for ($i = 0; $i <= 30; $i++)
                                <option value="{{ $i }}">{{ $i }} days</option>
                            @endfor
                        </select>
                    </div>

                    <div class="selct-picker-plain col-xs-4 no-padding-left padding-right-5">
                        <select class="selectpicker" data-size="5" data-style="no-background-with-buttonline padding-left-5 font-weight-600" name="update_hour" id="update_hour">
                            @for ($i = 0; $i <= 24; $i++)
                                <option value="{{ $i }}">{{ $i }} hrs</option>
                            @endfor
                        </select>
                    </div>

                    <div class="selct-picker-plain col-xs-4 no-padding-left padding-right-5">
                        <select class="selectpicker" data-size="5" data-style="no-background-with-buttonline padding-left-5 font-weight-600" name="update_min" id="update_min">
                            @for ($i = 0; $i <= 59; $i++)
                                <option value="{{ $i }}">{{ $i }} mins</option>
                            @endfor
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="city_tax"
                       class="col-md-4 col-form-label text-md-right">{{ __('Allow City Tax *') }}</label>
                <div class="col-md-6" style="margin:auto 0px">
                    <input type="radio" id="city_tax" name="city_tax"
                           value="1" {{ (old('city_tax')=='1') ? 'checked' :'' }}>
                    Yes
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" id="status0" name="city_tax"
                           value="0" {{ (old('city_tax')=='0') ? 'checked' :'' }}>
                    No
                    @if ($errors->has('city_tax'))
                        <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('city_tax') }}</strong>
                                    </span>
                    @endif

                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="tax" class="col-md-4 col-form-label text-md-right">{{ __('Tax') }}</label>
                <div class="col-md-6" style="margin:auto 0px">
                    <div class="input__field">
                        <input type="text" id="tax" name="tax" value="{{ old('tax') }}" placeholder="%"/>
                    </div>
                    @if ($errors->has('tax'))
                        <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('tax') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="service_tax" class="col-md-4 col-form-label text-md-right">{{ __('Service Tax') }}</label>
                <div class="col-md-6" style="margin:auto 0px">
                    <div class="input__field">
                        <input type="text" id="service_tax" name="service_tax" value="{{ old('service_tax') }}" placeholder="%" />
                    </div>
                    @if ($errors->has('service_tax'))
                        <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('service_tax') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="status"
                       class="col-md-4 col-form-label text-md-right">{{ __('Status *') }}</label>
                <div class="col-md-6" style="margin:auto 0px">
                    <input type="radio" id="status1" name="status"
                           value="1" {{ (old('status')=='1') ? 'checked' :'' }}>
                    Active
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" id="status0" name="status"
                           value="0" {{ (old('status')=='0') ? 'checked' :'' }}>
                    Inactive
                    @if ($errors->has('status'))
                        <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                    @endif

                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="allowed_zip"
                       class="col-md-4 col-form-label text-md-right">{{ __('Allowed Zip') }}</label>
                <div class="col-md-6" style="margin:auto 0px">
                    <div class="input__field">
                        <input type="text" id="allowed_zip" name="allowed_zip" value="{{ old('allowed_zip') }}"/>
                    </div>
                    @if ($errors->has('allowed_zip'))
                        <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('allowed_zip') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="source_url" class="col-md-4 col-form-label text-md-right">{{ __('Website URL') }}</label>
                <div class="col-md-6" style="margin:auto 0px">
                    <div class="input__field">
                        <input type="text" id="source_url" name="source_url" value="{{ old('source_url') }}" name="delivery_geo">
                    </div>
                    @if ($errors->has('source_url'))
                        <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('source_url') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="support_from" class="col-md-4 col-form-label text-md-right">{{ __('Support email From') }}</label>
                <div class="col-md-6">
                    <div class="input__field">
                        <input id="support_from" type="text"
                               class="{{ $errors->has('support_from') ? ' is-invalid' : '' }}"
                               name="support_from"
                               value="{{ old('support_from') }}">
                    </div>
                    @if ($errors->has('support_from'))
                        <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('support_from') }}</strong></span>
                    @endif
                </div>
            </div>


                                </div>

                                <div id="media" class="tab-pane {{(isset($section_tab) && $section_tab=='carryout')?'active in':''}}">
                                    <br />
                                    <div class="form-group row form_field__container">
                                        <label for="restaurant_image_name" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant Image') }}</label>
                                        <div class="col-md-4" style="margin:auto 0px">
                                            <input id="restaurant_image_name" type="file"
                                                   class="form-control{{$errors->has('restaurant_image_name') ? ' is-invalid' : '' }}"
                                                   value="{{ old('restaurant_image_name') }}" name="restaurant_image_name">
                                            @if ($errors->has('restaurant_image_name'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('restaurant_image_name') }}</strong></span>
                                            @endif
                                        </div>

                                        <input type="hidden" name="image_hidden" value="1" >
                                        {{--<div class="col-md-5 ">
                                            <a data-fancybox="gallery" href="{{url('/').'/..'.@$restaurantData->restaurant_image_name}}">
                                                <img class="img-responsive img-thumbnail"
                                                     src="{{ url('/').'/..'.@$restaurantData->restaurant_image_name }}" width="45%"></a>&emsp;
                                        </div>--}}
                                        <!--<div class="col-md-1">
                                            <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >

                                        </div>-->
                                    </div>

                                    <div class="form-group row form_field__container">
                                        <label for="restaurant_video_name" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant Video') }}</label>
                                        <div class="col-md-4" style="margin:auto 0px">
                                            <input id="restaurant_video_name" type="file"
                                                   class="form-control{{ $errors->has('restaurant_video_name') ? ' is-invalid' : '' }}"
                                                   value="{{ old('restaurant_video_name') }}" name="restaurant_video_name">
                                            @if ($errors->has('restaurant_video_name'))
                                                <span class="invalid-feedback" style="display: block;"><strong>{{ $errors->first('restaurant_video_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row form_field__container">
                                        <label for="restaurant_logo_name" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant Logo') }}</label>
                                        <div class="col-md-4" style="margin:auto 0px">
                                            <input id="restaurant_logo_name" type="file"
                                                   class="form-control{{ $errors->has('restaurant_logo_name') ? ' is-invalid' : '' }}"
                                                   value="{{ old('restaurant_logo_name') }}" name="restaurant_logo_name">
                                            @if ($errors->has('restaurant_logo_name'))
                                                <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('restaurant_logo_name') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div id="emails" class="tab-pane">
                                    <br />
                                    <div class="form-group row form_field__container">
                                        <input type="text" class="input__field" name="email_type[]" value="" placeholder="Email Type" autocomplete="off">
                                        <div class="col-md-3">
                                            <div class=""><input type="text" class="" name="from_name[]" value="" placeholder="From Name" autocomplete="off"></div>
                                            <div class=""><input type="text" class="" name="to_name[]" value="" placeholder="To Name" autocomplete="off"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class=""><input type="text" class="" name="from_email[]" value="" placeholder="From Email" autocomplete="off"></div>
                                            <div class=""><input type="text" class="" name="to_email[]" value="" placeholder="To Email" autocomplete="off"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="javsacript:void(0);" class="email_field_plus"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;
                                            <a href="javsacript:void(0);" class="email_field_minus"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class=" row  mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn__primary">
                                        {{ __('Add') }}
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#country_id').on('change', function () {

                var ajaxurl = '/get_states';

                $('#state_id').find('option').not(':first').remove();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/get_states"; ?>',
                    data: '_token = <?php echo csrf_token() ?>&country_id=' + this.value,
                    success: function (data) {
                        if ($.type(data.dataObj) !== 'undefined') {
                            $.each(data.dataObj, function (i, item) {
                                $('#state_id').append('<option value="' + item.id + '">' + item.state + '</option>');
                            });
                        }
                    }
                });
            });

            $('#state_id').on('change', function () {
                var ajaxurl = '/get_cities_by_state';
                $('#city_id').find('option').not(':first').remove();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/get_cities_by_state"; ?>',
                    data: '_token = <?php echo csrf_token() ?>&state_id=' + this.value,
                    success: function (data) {
                        if ($.type(data.dataObj) !== 'undefined') {
                            $.each(data.dataObj, function (i, item) {
                                $('#city_id').append('<option value="' + item.id + '">' + item.city_name + '</option>');
                            });
                        }
                    }
                });
            });

            $('#contact_country_id').on('change', function () {

                var ajaxurl = '/get_states';

                $('#contact_state_id').find('option').not(':first').remove();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/get_states"; ?>',
                    data: '_token = <?php echo csrf_token() ?>&country_id=' + this.value,
                    success: function (data) {
                        if ($.type(data.dataObj) !== 'undefined') {
                            $.each(data.dataObj, function (i, item) {
                                $('#contact_state_id').append('<option value="' + item.id + '">' + item.state + '</option>');
                            });
                        }
                    }
                });
            });

            $('#contact_state_id').on('change', function () {
                var ajaxurl = '/get_cities_by_state';
                $('#contact_city_id').find('option').not(':first').remove();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo URL::to('/') . "/get_cities_by_state"; ?>',
                    data: '_token = <?php echo csrf_token() ?>&state_id=' + this.value,
                    success: function (data) {
                        if ($.type(data.dataObj) !== 'undefined') {
                            $.each(data.dataObj, function (i, item) {
                                $('#contact_city_id').append('<option value="' + item.id + '">' + item.city_name + '</option>');
                            });
                        }
                    }
                });
            });

        });
    </script>

    <script type="text/javascript">

        $(document).ready(function () {

            if($('#reservation_type').val() == 'third_party') {
                $('#third_party_url_group').show();
            } else {
                $('#third_party_url_group').hide();
            }

            $('.delivery__radio').on('click', function () {

                if (this.value == 1) {
                    $("#delivery_geo").show();
                    $("#delivery_zipcode").hide();
                }

                if (this.value == 0) {
                    $("#delivery_zipcode").show();
                    $("#delivery_geo").hide();
                }

            });

            if($('#is_geo:checked').val()) {
                // delivery geo enabled
                $('#delivery_zipcode').hide();
                $('#delivery_geo').show();
            } else {
                // delivery allowed zipcodes
                $('#delivery_geo').hide();
                $('#delivery_zipcode').show();
            }

            $('#geo_lat_lng_address').on('click', function () {
                console.log('geo_lat_lng_address clicked');
                var lat = $('#lat').val();
                var lng = $('#lng').val();
                window.open(
                    'https://maps.google.com/?q='+lat+','+lng,
                    '_blank' // <- This is what makes it open in a new window.
                );
            });

            $('#keyword_container').on('click', 'a.meta_tags_plus', function() {
                var html = '<div class="form-group row form_field__container">\n' +
                    '<input type="text" class="input__field" name="meta_tags_type[]" value="" placeholder="type">' +
                    '<div class="col-md-6">' +
                    '<div class=""><input type="text" class="" name="meta_tags_label[]" value="" placeholder="label"></div>' +
                    '</div>' +
                    '<div class="col-md-6">' +
                    '<div class=""><input type="text" class="" name="meta_tags_content[]" value="" placeholder="content"></div>' +
                    '</div>' +
                    '<a href="javsacript:void(0);" class="meta_tags_plus"><i class="fa fa-plus"></i></a>' +
                    '&nbsp;&nbsp;'+
                    '<a href="javsacript:void(0);" class="meta_tags_minus"><i class="fa fa-minus"></i></a>'+
                    '</div>';
                $('#keyword_container').append(html);
            });

            $('#keyword_container').on('click', 'a.meta_tags_minus', function() {
                $(this).parents('.form_field__container').remove();
            });

            $('#emails').on('click', 'a.email_field_plus', function() {
                var html =
                    '<div class="form-group row form_field__container">'
                    +'<input type="text" class="input__field" name="email_type[]" value="" placeholder="Email Type" autocomplete="off">'
                    +'<div class="col-md-3">'
                    +'<div class=""><input type="text" class="" name="from_name[]" value="" placeholder="From Name" autocomplete="off"></div>'
                    +'<div class=""><input type="text" class="" name="to_name[]" value="" placeholder="To Name" autocomplete="off"></div>'
                    +'</div>'
                    +'<div class="col-md-3">'
                    +'<div class=""><input type="text" class="" name="from_email[]" value="" placeholder="From Email" autocomplete="off"></div>'
                    +'<div class=""><input type="text" class="" name="to_email[]" value="" placeholder="To Email" autocomplete="off"></div>'
                    +'</div>'
                    +'<div class="col-md-3">'
                    +'<a href="javsacript:void(0);" class="email_field_plus"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;'
                    +'<a href="javsacript:void(0);" class="email_field_minus"><i class="fa fa-minus"></i></a>'
                    +'</div>'
                    +'</div>';
                $('#emails').append(html);
            });
            $('#emails').on('click', 'a.email_field_minus', function() {
                $(this).parents('.form_field__container').remove();
            });

            $('#reservation_type').on('change', function(){
                if($(this).val() == 'third_party') {
                    $('#third_party_url_group').show();
                } else {
                    $('#third_party_url_group').hide();
                }
            });
        });

    </script>


@endsection