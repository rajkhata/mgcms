@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
            <div class="reservation__atto">
                <div class="reservation__title col-md-8 no-col-padding">
                    <h1>Miles Report
                        
                    </h1>
                </div>
            </div>
            <div>
                <div class="active__order__container">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @elseif (session()->has('err_msg'))
                        <div class="alert alert-danger">
                            {{ session()->get('err_msg') }}
                        </div>
                    @endif

                    <div class="guestbook__container box-shadow no-margin-top">
                         
      <table id="table_id" class="responsive ui cell-border hover " style="width:100%; margin-bottom:10px;">
          <thead>
        <tr>
            <th>Restaurant</th>
            <th>Order Type</th>
            <th>Order No</th>
            <th>Order Amount</th>
           <th>Customer Phone</th>
           <th>Customer Email</th>
        </tr>
          </thead>
         <tbody>
        @if(isset($milesrecord) && count($milesrecord)>0)
                        @foreach($milesrecord as $keys => $value)
        <tr>
              <td>{{ $value->restaurant_name }}</td>
              <td>{{ $value->order_type }}</td>
              <td><a href="/mng_order/{{ $value->ma_order_id }}/food_item">{{ $value->id }}</a></td>
              <td> £ {{ $value->total_amount }}</td>
              <td>{{ $value->phone }}</td>
              <td>{{ $value->email }}</td>
        </tr>
                @endforeach
                        @else
                            <tr>
                                 <td colspan = "6" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03); ">
                                No Record Found
                            </td></tr>
                        @endif
        </tbody>
       </table>
                </div>
            </div>
    </div>
<script>  
$(document).ready( function () {
       $('#table_id').DataTable(  {
      responsive: true,
            dom:'<"top"fiB><"bottom"trlp><"clear">',
            buttons: [
//                {
//                    text: 'Add',
//                    action: function () {location.href = "item/create";}
//                },
                // {extend: 'excelHtml5'}, 
                // {extend: 'pdfHtml5'},   
                // {               
                //     extend: 'print',
                //     exportOptions: {
                //         columns: ':visible'
                //     }
                // },
                // 'colvis'
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye"></i>',
                    titleAttr: 'Column Visibility'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                }, 
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                },
                {               
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    exportOptions: {
                        columns: ':visible',
                    },
                }
            ],
            columnDefs: [ {
                targets: -1,
                visible: false
            }],
            language: {
                search: "",
                searchPlaceholder: "Search"
            },
            responsive: true
    });
        //$('#table_id').show();
        //$('#table_id').removeClass('hidden');
  } );
    </script>
 
@endsection