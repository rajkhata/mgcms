<?php
  use App\Helpers\CommonFunctions;
  use App\Models\Restaurant;
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="Add to Home">
  <meta name="format-detection" content="telephone=no">

  <!--- No Cache --->
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='-1'>
  <meta http-equiv='pragma' content='no-cache'>
  <!--- /No Cache --->

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'MunchAdo CMS') }}</title>

  <!--[if IE]><link rel="shortcut icon" href="{{asset('images/favicon.ico')}}"><![endif]-->
  <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
  <link rel="shortcut icon" href="{{asset('images/icon.png')}}" type="image/png">
  <link rel="apple-touch-icon-precomposed" href="{{asset('images/icon.png')}}" >
  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/reservation/responsive.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />

    <link rel="stylesheet" href="{{URL::asset('css/addtohomescreen.css')}}">
    <link rel="stylesheet" href="{{URL::asset('icomoon.css')}}">
    <!--<link rel="stylesheet" href="//s3.amazonaws.com/icomoon.io/134436/TableReservationSystem/style.css?1jdlol">-->
    <link rel="stylesheet" href="{{URL::asset('css/reservation.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/jquery.mCustomScrollbar.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/wickedpicker.css')}}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-select.min.css')}}">
    {{--Semantic--}}
    <link rel="stylesheet" href="{{URL::asset('css/semantic.min.css')}}">
    {{--Sass Compiled css--}}
    <link rel="stylesheet" href="{{URL::asset('dist/css/style.min.css')}}">

    <!-- Scripts -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>-->

  <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <!-- Timepicker js -->
  <script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.js')}}"></script>
  <script src="{{ asset('js/swiper.min.js') }}" defer></script>
  <script src="{{ asset('js/common.js') }}" defer></script>
  <script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}" defer></script>
  <script src="{{ asset('js/wickedpicker.js') }}" defer></script>
  <script src="{{ asset('js/addtohomescreen.js') }}"></script>
  <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
  <script src="{{ asset('js/svg-inject.js') }}"></script>
  <script>
  addToHomescreen();
</script>
<script language="javascript" type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Latest compiled and minified JavaScript -->

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script language="javascript" type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>

</head>
<body class="no-padding withoutSidebar">
  {{--<div id="app" class="active-sidebar">--}}
  <div id="app" class="active-sidebar">


{{--<div class="nav__spacer"></div>--}}
<div class="flex-box">
    <div class="content-container">
    <nav class="navbar-laravel box-shadow">
      <div>
      <!-- <a class="navbar-brand" href="{{ url('/') }}">
        {{ config('app.name', 'MunchAdo CMS') }}
              </a> -->
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button> -->

        <div class="navbar-head padding-left-right-20 padding-top-bottom-15">

          <div class="navbar-content">


            <!-- Authentication Links -->
            @guest
              <div class="navContent-right">
                <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
              </div>
            @else
              <div class="navContent-left">
                {{--<li class="hide">
                  <a href="/"><img src="{{asset('images/dish-icon.png')}}" /></a>
                </li>--}}
                <div class="nav-item dropdown">
                  <a href="/" id="navbarDropdown" class="nav-link dropdown-toggle user__name"   aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }}
                  <!-- <span class="caret"></span> -->
                  </a><span class="dashboard hide">Dashboard</span>
                </div>

                <div class="selct-picker-plain margin-left-30 width-120">
                 {{-- <select class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                      <option value="0" selected>Super Admin</option>
                      <option value="1">Admin</option>
                  </select>--}}
                </div>

              </div>
            <!-- <div class="navContent-left">

            <li class="nav-item dropdown">
            <span class="dashboard">
                <?php
                  if((new \App\Helpers\CommonFunctions)->dashboard_pause_service()) {
                  echo (new \App\Helpers\CommonFunctions)->dashboard_pause_service(); ?>
                    <a href ="/configure/manage_services" class="dashboard">Resume Service</a>

<?php } ?>
                    </span>
                    </li>
                </div> -->
        <?php        
              //$restInfo= CommonFunctions::dashboardRestInfo();
              //$current_version=$announcement['current_version'];
                  $user = Auth::user();

                  $restarant_detail = Restaurant::find($user->restaurant_id);
                  $prestid= ($restarant_detail->parent_restaurant_id>0)?$restarant_detail->parent_restaurant_id:$user->restaurant_id;
                  $counts =Restaurant::select('id')
                      ->where('parent_restaurant_id', $prestid)->where('status', 1)
                      ->get()->count();
                  //print_r($counts);die;
                  //print_r($restarant_detail->plan_type);die;
                  ?>
                  <div class="navContent-right">

                      <div class="font-weight-700 margin-right-10 hidden-tablet">{{$counts}} Location(s)</div>
                      <span class="font-weight-700 hidden-tablet" {{ ($restarant_detail->plan_type!='' )?'':'style=display:none;'}}>|</span>
                      <div class="font-weight-700 margin-left-10 margin-right-10 hidden-tablet"  {{ ($restarant_detail->plan_type!='' )?'':'style=display:none;'}}>{{ $restarant_detail->plan_type}}</div>
                      <span class="font-weight-700 hidden-tablet">|</span>
                      <div class="font-weight-700 margin-left-10 margin-right-20 hidden-tablet">Start Date: <?php echo date("d M, Y",strtotime($restarant_detail->created_at));?> </div>
                <!--  <li class="nav-item dropdown"><a class="navbarDropdown notifications" href="#">Notifications <span class="notification__count">10</span></a></li>
                 <li class="nav-item dropdown"><a class="navbarDropdown" href="#">Help & Support</a></li> -->
                <div class="nav-item dropdown">
                  <a class="navbarDropdown font-weight-700" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                    {{ __('Sign Out') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </div>
            @endguest
          </div>
        </div>
      </div>



        <?php if((new \App\Helpers\CommonFunctions)->dashboard_pause_service()) { ?>
      <div class="service-alert">
          <?php echo (new \App\Helpers\CommonFunctions)->dashboard_pause_service(); ?>

        <a href ="/online-ordering-setup" class="btn btn__primary btn-resume-service dashboard">Resume Services</a>
      </div>
        <?php } ?>


    </nav>


    @yield('content')
  </div>



</div>

</div>
       <div class="tablet-hidden-text hidden">
                <div class="disable-text-part">
                    Please view the Dashboard on your Tablet in Landscape view only.
                </div>

       </div>
          <div class="mobile-hidden-text hidden">
            <div class="disable-text-part">
              You cannot view Dashboard in this mode. Please contact Munch Ado for assistance.

             </div>
       </div>
  <script src="{{ asset('js/all.js') }}?v=<?php echo date('YmdHis'); ?>"></script>
  <div id="loader" class="hidden">
    <span style="background-image: url({{asset('images/Rolling.gif')}})"></span>
    <img src="{{asset('images/Rolling.gif')}}" class="hidden">

  </div>
  <!-- Route changes PE-3070 RG -->
  @if(ends_with(Route::currentRouteAction(), 'UserOrderController@index') || ends_with(Route::currentRouteAction(), 'UserOrderController@archiveOrder') || ends_with(Route::currentRouteAction(), 'UserOrderController@productOrders') || ends_with(Route::currentRouteAction(), 'UserOrderController@productArchiveOrder') || ends_with(Route::currentRouteAction(), 'HomeController@index'))
    @include('includes.orderexport')
    @include('reservation::reservation.export')
  @endif


  @if(ends_with(Route::currentRouteAction(), 'UserOrderController@orderDetail') || ends_with(Route::currentRouteAction(), 'UserOrderController@getGiftCardDetail') || ends_with(Route::currentRouteAction(), 'UserOrderController@getGiftCardArchiveDetail') )
    @include('includes.refund')
  @endif

  <?php
  //use App\Helpers\CommonFunctions;

  $announcement= CommonFunctions::dashboardAnnouncement();
  $current_version=$announcement['current_version'];

  if(!isset($_COOKIE['dashboard_version']) || (isset($_COOKIE['dashboard_version']) && $current_version>$_COOKIE['dashboard_version'])){

  ?>
  <script>
document.querySelector("input").addEventListener("keydown",function(e){
    var charCode = e.charCode || e.keyCode || e.which;
    if (charCode == 27){

        return false;
    }
});
    function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

  </script>

<div class="modal fade " id="IsdashboardAnnouncementSet" tabindex="-1" role="dialog" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog"> <!-- Modal content-->
        <div class="modal-content white-smoke">
            <div class="modal-header"><h4 class="modal-title text-center">Announcement</h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-lg-offset-0 col-sm-12">
                        <div class="margin-top-30">
                            <div class="w_310 ch-m-bott pull-md-right pull-xs-right op-color">
                                <div class="row margin-top-10">
                                    <div class="">
                                        <div class="col-lg-12 text-center margin-bottom-30"> <?php echo isset($announcement) ? $announcement['message'] : ''; ?> </div> <?php if(isset($announcement['features']) and count($announcement['features'])){ ?>
                                        <div class="col-xs-12 margin-top-10 margin-bottom-30">
                                            <div class="col-xs-12 text-center" style="margin-bottom: 24px;">New in This
                                                Update:<br>
                                            </div> <?php foreach($announcement['features'] as $key=>$feature){ ?>
                                            <div class="col-xs-12 text-left">
                                                <div class="tbl-radio-btn"><label
                                                            for="refundAmtSizeSmall"><?php echo $key;?>
                                                        : </label><?php echo $feature;?>  </div>
                                            </div> <?php } ?> </div> <?php } ?>
                                        <div class="col-lg-12 text-left margin-bottom-30"> <?php echo isset($announcement) ? $announcement['message_footer'] : ''; ?> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center margin-top-30 margin-bottom-30">
                                <button type="button" class="btn btn__primary min-width-180 font-weight-600"
                                        data-dismiss="modal"
                                        onclick="setCookie('dashboard_version', <?php echo $current_version; ?>, 365);">
                                    Ok
                                </button>
                            </div>
                        </div>
                        <div id="errormess" style="display:none;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#IsdashboardAnnouncementSet').modal('show', {backdrop: 'static', keyboard: false});
    // $('.modal-backdrop').css('z-index',0);
  </script>
  <style>
    .pane-center{
      z-index:inherit !important;
    }
  </style>

  <?php
  }
  ?>

</body>
</html>
