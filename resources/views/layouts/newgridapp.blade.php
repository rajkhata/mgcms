<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Bravvura | {{ $title ?? config('app.name', 'Laravel') }}</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link href="{{ asset('css/appbyw.css') }}" rel="stylesheet">
     
 <script>
		$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
</script>

</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-dark bg-info navbar-laravel">
        <div class="container">
            <a class="navbar-brand text-white">
                {{ config('app.name123', 'Bravvura') }}
            </a>
            <!--button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button-->

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                         
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        
                    </li>
                    <li class="nav-item">
                         
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<main class="py-0 ">
    <div class="jumbotron">
        <div class="container">
             @yield('content')
        </div>
    </div>
    

</main>

<script src="{{ asset('js/app.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js"></script>
<script src="{{ asset('vendor/grid-view/js/amigrid.js') }}"></script>
@stack('scripts')
</body>
</html> 
