<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Add to Home">
    <meta name="format-detection" content="telephone=no">

    <!--- No Cache --->
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='-1'>
    <meta http-equiv='pragma' content='no-cache'>
    <!--- /No Cache --->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MunchAdo CMS') }}</title>

    <!--[if IE]>
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}"><![endif]-->
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('images/icon.png')}}" type="image/png">
    <link rel="apple-touch-icon-precomposed" href="{{asset('images/icon.png')}}">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/reservation/responsive.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css"/>

    <link rel="stylesheet" href="{{URL::asset('css/addtohomescreen.css')}}">
<link rel="stylesheet" href="{{URL::asset('icomoon.css')}}">
<!--<link rel="stylesheet" href="//s3.amazonaws.com/icomoon.io/134436/TableReservationSystem/style.css?1jdlol">-->
    <link rel="stylesheet" href="{{URL::asset('css/reservation.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/jquery.mCustomScrollbar.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/wickedpicker.css')}}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-select.min.css')}}">
    {{--Semantic--}}
    <link rel="stylesheet" href="{{URL::asset('css/semantic.min.css')}}">
    {{--Sass Compiled css--}}
    <link rel="stylesheet" href="{{URL::asset('dist/css/style.min.css')}}">

    <!-- Scripts -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>-->

    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <!-- Timepicker js -->
    <script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.js')}}"></script>
    <script src="{{ asset('js/swiper.min.js') }}" defer></script>
    <script src="{{ asset('js/common.js') }}" defer></script>
    <script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}" defer></script>
    <script src="{{ asset('js/wickedpicker.js') }}" defer></script>
    <script src="{{ asset('js/addtohomescreen.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/svg-inject.js') }}"></script>
    <script>
        addToHomescreen();
    </script>
    <script language="javascript" type="text/javascript"
            src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Latest compiled and minified JavaScript -->

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript"
            charset="utf-8"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script language="javascript" type="text/javascript"
            src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>

</head>
<body class="no-padding withoutSidebar">
{{--<div id="app" class="active-sidebar">--}}
<div id="app" class="active-sidebar">


    {{--<div class="nav__spacer"></div>--}}
    <div class="flex-box">
        <div class="content-container">

            <nav class="navbar-laravel box-shadow" style="background-color: #3D3A39">
                <div>

                    <div class="navbar-head padding-left-right-20 padding-top-bottom-5 text-center">

                        <img src="{{ asset("/images/munchado-logo.png") }}">

                    </div>
                </div>


            </nav>


            @yield('content')
        </div>


    </div>

</div>
<div class="tablet-hidden-text hidden">
    <div class="disable-text-part">
        Please view the Dashboard on your Tablet in Landscape view only.
    </div>

</div>
<div class="mobile-hidden-text hidden">
    <div class="disable-text-part">
        You cannot view Dashboard in this mode. Please contact Munch Ado for assistance.

    </div>
</div>
<script src="{{ asset('js/all.js') }}?v=<?php echo date('YmdHis'); ?>"></script>
<div id="loader" class="hidden">
    <span style="background-image: url({{asset('images/Rolling.gif')}})"></span>
    <img src="{{asset('images/Rolling.gif')}}" class="hidden">

</div>
<!-- Route changes PE-3070 RG -->
@if(ends_with(Route::currentRouteAction(), 'UserOrderController@index') || ends_with(Route::currentRouteAction(), 'UserOrderController@archiveOrder') || ends_with(Route::currentRouteAction(), 'UserOrderController@productOrders') || ends_with(Route::currentRouteAction(), 'UserOrderController@productArchiveOrder') || ends_with(Route::currentRouteAction(), 'HomeController@index'))
    @include('includes.orderexport')
    @include('reservation::reservation.export')
@endif


@if(ends_with(Route::currentRouteAction(), 'UserOrderController@orderDetail') || ends_with(Route::currentRouteAction(), 'UserOrderController@getGiftCardDetail') || ends_with(Route::currentRouteAction(), 'UserOrderController@getGiftCardArchiveDetail') )
    @include('includes.refund')
@endif

<?php
use App\Helpers\CommonFunctions;

$announcement = CommonFunctions::dashboardAnnouncement();
$current_version = $announcement['current_version'];

if(!isset($_COOKIE['dashboard_version']) || (isset($_COOKIE['dashboard_version']) && $current_version > $_COOKIE['dashboard_version'])){

?>
<script>
    document.querySelector("input").addEventListener("keydown", function (e) {
        var charCode = e.charCode || e.keyCode || e.which;
        if (charCode == 27) {

            return false;
        }
    });
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

</script>

<div class="modal fade " id="IsdashboardAnnouncementSet" tabindex="-1" role="dialog" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog"> <!-- Modal content-->
        <div class="modal-content white-smoke">
            <div class="modal-header"><h4 class="modal-title text-center">Announcement</h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-lg-offset-0 col-sm-12">
                        <div class="margin-top-30">
                            <div class="w_310 ch-m-bott pull-md-right pull-xs-right op-color">
                                <div class="row margin-top-10">
                                    <div class="">
                                        <div class="col-lg-12 text-center margin-bottom-30"> <?php echo isset($announcement) ? $announcement['message'] : ''; ?> </div> <?php if(isset($announcement['features']) and count($announcement['features'])){ ?>
                                        <div class="col-xs-12 margin-top-10 margin-bottom-30">
                                            <div class="col-xs-12 text-center" style="margin-bottom: 24px;"><b>New in
                                                    This
                                                    Update:</b><br>
                                            </div> <?php foreach($announcement['features'] as $key=>$feature){ ?>
                                            <div class="col-xs-12 text-left">
                                                <div class="tbl-radio-btn"><label
                                                            for="refundAmtSizeSmall"><?php echo $key;?>
                                                        : </label><?php echo $feature;?>  </div>
                                            </div> <?php } ?> </div> <?php } ?>
                                        <div style="padding: 0 30px;"
                                             class="col-lg-12 text-left margin-bottom-30"> <?php echo isset($announcement) ? $announcement['message_footer'] : ''; ?> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center margin-top-30 margin-bottom-30">
                                <button type="button" class="btn btn__primary min-width-180 font-weight-600"
                                        data-dismiss="modal"
                                        onclick="setCookie('dashboard_version', <?php echo $current_version; ?>, 365);">
                                    Ok
                                </button>
                            </div>
                        </div>
                        <div id="errormess" style="display:none;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#IsdashboardAnnouncementSet').modal('show', {backdrop: 'static', keyboard: false});
    // $('.modal-backdrop').css('z-index',0);
</script>
<style>
    .pane-center {
        z-index: inherit !important;
    }
</style>

<?php
}
?>

</body>
</html>
