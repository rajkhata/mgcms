@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Add Sms Content') }}</h1>

    </div>

  </div>
  <div>

    <div class="card form__container">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif


      <div class="card-body">
        <div class="form__user__edit">
          <form method="POST" action="{{ route('sms.create') }}" enctype="multipart/form-data">
            @csrf
            <input id="sms_id" type="hidden" name="sms_id" value="{{ isset($smsDetails->id)?$smsDetails->id:'' }}">
            <div class="form-group row form_field__container">
              <label for="sms_id" class="col-md-4 col-form-label text-md-right">{{ __('Sms Name') }}</label>
              <div class="col-md-6">
                <div class="input__field">
                  <input id="sms_name" type="text" class="{{ $errors->has('sms_name') ? ' is-invalid' : '' }}"
                  name="sms_name" value="{{ isset($smsDetails->sms_name)?$smsDetails->sms_name:'' }}" required>
                </div>
                @if ($errors->has('sms_name'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('sms_name') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="content" class="col-md-4 col-form-label text-md-right">{{ __('Sms Content') }}</label>

              <div class="col-md-6">
                <div class="custom__message-textContainer">
                  <textarea rows="5"  name="content" id="content" class="{{ $errors->has('content') ? 'is-invalid':''}}" required>{{ isset($smsDetails->content)?$smsDetails->content:'' }}</textarea>
                </div>


                @if ($errors->has('content'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('content') }}</strong>
                </span>
                @endif
              </div>

            </div>



            <div class="form-group row form_field__container">
              <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-6" style="margin:auto 0px">
                <input type="radio"  name="status" value="1" {{ (isset($smsDetails->status) && $smsDetails->status==1) ? 'checked' :'' }}> Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio"  name="status" value="0" {{ (isset($smsDetails->status) && $smsDetails->status==0) ? 'checked' :'' }}> No

                @if ($errors->has('status'))
                <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>

            </div>
            <div class="row mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">
                  {{ __('Add') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>

  </div>
</div>
@endsection
