@extends('layouts.app')


@section('content')
<div class="main__container container__custom clearfix">
    <div class="reservation__atto">

        Activity Logs
    </div>

    <div class="guestbook__container fullWidth box-shadow margin-top-20 margin-bottom-20" style="padding:10px!important;">


        <div class="_block">
            <div class="form-group">                    
                @if($cmsuser)
                @foreach($cmsuser as $key => $v)
                <strong>{{ucfirst($key)}}:</strong> <label class="label label-success">{{ $v }},</label>
                @endforeach
                @else
                    Empty Logs
                @endif
            </div>
        </div>
    </div>


    @if($action)
    @foreach($action as $key => $v)
    @foreach($v as $k =>$val)
    <div class="guestbook__container fullWidth box-shadow margin-top-0 margin-bottom-20" style="padding:10px!important;">
        <div class="_block">
            <div class="form-group">   
                <div class="_block">
                    <label style="margin-left:10px;">Dated : </label>
                    <div class="label label-success" >        
                        {{$k}} 
                    </div>
                    <label style="margin-left:10px;">Action : </label>
                    <div class="label label-success" style="margin-left:10px;">        
                        {{$val['action']}}
                    </div>
                </div>

                <div class="_block">
                    <div class="form-group">  
                        <label style="margin-left:10px;">Activity :  {{$val['action_name']}}</label>
                        <label style="margin-left:10px;">Request Type :  {{$val['method']}}</label>
                        <label style="margin-left:10px;">Request From :  {{$val['ip_address']}}</label>
                        <label style="margin-left:10px;">Brouser Type :  {{$val['user_agent']['user-agent']}}</label>
                        <label style="margin-left:10px;">Requested Host :  {{$val['user_agent']['host']}}</label>

                    </div>
                </div>

            </div>
        </div>
    </div>
    @endforeach
    @endforeach
    @endif




    @endsection