@extends('layouts.app')

@section('content')

<div class="main__container container__custom">

  @if(session()->has('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}
    </div>
  @elseif (session()->has('err_msg'))
    <div class="alert alert-danger">
      {{ session()->get('err_msg') }}
    </div>
  @endif

  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Payment Gateway List</h1>
    </div>
    <div class="guestbook__sec2">
      <div class="margin-left-10">
        <a href="{{ URL::to('paymentgateway/create') }}" class="btn btn__primary width-90">Add</a>
      </div>
    </div>
  </div>

    <div class="booking">

      <div class="archive_reservation">

        <div class="booking_container tab-content clearfix box-shadow margin-top-20">
          <div class="booking_content tab-pane active no-margin">
            <div class="row row__reservation-head row__reservation-archive header-color font-weight-700">
              <div class="col-xs-3 no-padding-left">Id.</div>
              <div class="col-xs-3">Name</div>
              <div class="col-xs-3">Status</div>
            </div>
            <div>
              @if(isset($paymentgateway) && count($paymentgateway)>0)

                @foreach($paymentgateway as $keys => $value)
                  <div class="row  row__customer row__new-customer row__customer-archive no-padding">
                    <div class="row__reservation">
                      <div class="col-xs-3 no-padding-left font-weight-700">{{ $value->id }}</div>
                      <div class="col-xs-3 font-weight-700">{{ $value->name }}</div>
                      <div class="col-xs-3 font-weight-700">{{ ($value->status==1)?'Active':($value->status==0?'Inactive':'') }}</div>
                      <div class="col-xs-3 text-right no-padding-right">
                        <div class="pull-right margin-left-15">
                          {!! Form::open(['method' => 'DELETE', 'route' => ['paymentgateway.destroy', $value->id]]) !!}
                          @csrf
                          <button class="btn btn__holo width-90 font-weight-600" type="submit">Delete</button>
                          {!! Form::close() !!}
                        </div>
                        <div class="pull-right">
                          <a href="{{ URL::to('paymentgateway/' . $value->id . '/edit') }}" class="btn btn__primary width-90">Edit</a>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              @else
                <div class="text-center col-xs-12 col-sm-12 margin-top-bottom-40">
                  <strong>No Record Found</strong>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection
