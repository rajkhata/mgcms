@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Add Payment Gateway') }}</h1>

    </div>

  </div>
  <div>

    <div class="card form__container margin-top-20">
      @if(session()->has('message'))
      <div class="alert alert-success margin-bottom-30">
        {{ session()->get('message') }}
      </div>
      @endif


      <div class="card-body">
        <div class="form__user__edit">
          <form method="POST" action="{{ route('paymentgateway.store') }}" enctype="multipart/form-data">

            @csrf
            <div class="form-group fullWidth">
              <label for="template_id" class="col-md-4 col-form-label text-md-right">{{ __(' Name') }}</label>
              <div class="col-md-6 selct-picker-plain">
                <select name="name" required id="name" class="selectpicker {{ $errors->has('name') ? ' is-invalid' : '' }}" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                  <option value="0">Select</option>
                  <option value="Stripe">Stripe</option>
		  <option value="Paybycash">Pay By Cash</option>
		  <option value="Nopayment">No Payment</option>	
		   
                  <option value="Ingenico">Ingenico</option>
                  <option value="Braintress">Braintress</option>
                  <option value="Authorize">Authorize.Net</option>
		  <option value="Bridgepay">Bridgepay</option>
		
                </select>

                @if ($errors->has('name'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group fullWidth">
              <label for="credit card" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-6 no-padding">

                <div class="col-xs-4 tbl-radio-btn width-60">
                  <input type="radio" id="statusYes" name="status" value="1" {{ (old('status')=='1') ? 'checked' :'' }} checked>
                  <label for="statusYes">Yes</label>
                </div>

                <div class="col-xs-4 tbl-radio-btn width-60">
                  <input type="radio" id="statusNo" name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }}>
                  <label for="statusNo">No</label>
                </div>

                @if ($errors->has('status'))
                  <span class="invalid-feedback" style="display: block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>

            </div>
            <div class="form-group fullWidth no-margin-bottom">
              <fieldset class="fullWidth">
                <legend>Config</legend>
                <div id="keyword_container" class="col-xs-12 no-padding">
                  <div class="form-group">
                    <div class="col-xs-5 input__field"><input type="text" class="font-size-14" name="configk[]" value="" placeholder="Key" required></div>
                    <div class="col-xs-5 input__field"><input type="text" class="font-size-14" name="configv[]" value="" placeholder="value" required></div>
                    <div class="col-xs-2"><a href="#" class="meta_tags_plus"><i class="fa fa-plus"></i></a></div>
                  </div>
                </div>
              </fieldset>
            </div>

            <div class="form-group fullWidth">
              <div class="fullWidth">
                <label for="api_documentation" class="col-xs-12 col-form-label text-md-right">Api Documentation</label>

                <div class="col-xs-12 textarea__input">
                  <textarea name="api_documentation" id="api_documentation" class="form-control{{ $errors->has('api_documentation') ? 'is-invalid':''}}" required>{{ isset($mailtemplate->api_documentation)?$mailtemplate->api_documentation:'' }}</textarea>

                  @if ($errors->has('api_documentation'))
                    <span class="invalid-feedback" style="display: block;" >
                    <strong>{{ $errors->first('api_documentation') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

            </div>


            <div class="row mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">Add</button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>

  </div>
</div>
<script>
  var count = "1"
  $('#keyword_container').on('click', 'a.meta_tags_plus', function() {
    var html = '<div id="meta'+ count +'" class="form-group">' +
            '<div class="col-xs-5 input__field"><input type="text" class="font-size-14" name="configk[]" value="" placeholder="key"></div>' +
            '<div class="col-xs-5 input__field"><input type="text" class="font-size-14" name="configv[]" value="" placeholder="value"></div>' +
            '<div class="col-xs-2"><a href="#" class="meta_tags_plus"><i class="fa fa-plus"></i></a>' +
            '&nbsp;&nbsp;'+
            '<a href="#" onclick=removeRow("meta'+ count +'") class="meta_tags_minus meta'+ count +'"><i class="fa fa-minus"></i></a>'+
            '</div>';
    $('#keyword_container').append(html);
  });

  function removeRow(id){
    $("#"+id).remove();
  }
</script>

@endsection
