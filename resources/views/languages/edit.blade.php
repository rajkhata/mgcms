@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Language - Edit') }}</h1>

    </div>

  </div>
  <div>

    <div class="card form__container">
      <div class="card-header"></div>
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit">
          {{ Form::model($languages, array('route' => array('languages.update', $languages->id), 'method' => 'PUT', 'files' => true)) }}
          @csrf
          <div class="form-group row form_field__container">
            <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('Language Name') }}</label>
            <div class="col-md-4">
              <div class="input__field">
                <input id="language_name" type="text" class="{{ $errors->has('language_name') ? ' is-invalid' : '' }}"
                name="language_name" value="{{ count(old())==0 ? $languages->language_name : old('language_name')}}" required autocomplete="off">
              </div>
              @if ($errors->has('language_name'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('language_name') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group row form_field__container">
            <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Language Code') }}</label>
            <div class="col-md-4">
              <div class="input__field">
                <input id="code" type="text" class="{{ $errors->has('code') ? ' is-invalid' : '' }}"
                name="code" value="{{ count(old())==0 ? $languages->code : old('code') }}" required autocomplete="off">
              </div>
              @if ($errors->has('code'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('code') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group row form_field__container">
            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
            <div class="col-md-4" style="padding-top: 10px;">
              <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1' || (count(old())==0 && $languages->status=='1')) ? 'checked' :'' }}> Active
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0' || (count(old())==0 && $languages->status=='0')) ? 'checked' :'' }}> Inactive
              @if ($errors->has('status'))
              <span class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>
          </div>


          <br /><br />
          <div class="row mb-0">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn__primary">{{ __('Save Language') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
</div>
@endsection
