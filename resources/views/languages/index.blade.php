@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Users<span style="margin:0px 5px;">({{ $languages->currentPage() .'/' . $languages->lastPage() }})</span>&emsp;</h1>
    </div>
    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('languages/create') }}" class="btn btn__primary">Add</a></span>
  </div>
  <div class="addition__functionality">
    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">
      <div class="row form-group col-md-2">
        <div class="input__field">
          <input id="sName" class=""  type="text" name="name" value="{{isset($searchArr['name']) ? $searchArr['name'] : ''}}">
          <label for="sName">Name</label>
        </div>
      </div>
      <div class="row form-group col-md-3">
        <button class="btn btn__primary" style="margin-left: 5px;" type="submit" >Search</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>




  <div>

    <div class="active__order__container">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif

      <div class="guestbook__container box-shadow">
        <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
          <div class="col-md-10">
            <div class="col-md-2" style="font-weight: bold;">Language</div>
            <div class="col-md-1" style="font-weight: bold;">Code</div>
            <div class="col-md-3" style="font-weight: bold;">Created On</div>
            <div class="col-md-3" style="font-weight: bold;">Updated On</div>
            <div class="col-md-1" style="font-weight: bold;">Status</div>
          </div>
          <div class="col-md-2">
            <div class="col-md-12" style="font-weight: bold;">Action</div>
          </div>
        </div>
        @if(isset($languages) && count($languages)>0)
        @foreach($languages as $lang)
        <div class="guestbook__customer-details-content rest__row">
          <div class="col-md-10">

            <div class="col-md-2 text-capitalize" style="margin: auto 0px;">{{ $lang->language_name }}</div>
            <div class="col-md-1 text-capitalize" style="margin: auto 0px;">{{ $lang->code }}</div>
            <div class="col-md-3 text-capitalize" style="margin: auto 0px;">{{ $lang->created_at }}</div>
            <div class="col-md-3 text-capitalize" style="margin: auto 0px;">{{ $lang->updated_at }}</div>
            <div class="col-md-1 text-capitalize" style="margin: auto 0px;">{{ ($lang->status==1)?'Active':($lang->status==0?'Inactive':'') }}</div>

          </div>
          <div class="row col-md-2" style="margin: auto 0px;">
            <div class="col-md-12">
              <a style="width:100%;margin:10px 0;" href="{{ URL::to('languages/' . $lang->id . '/edit') }}" class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
            </div>
            {{--<div class="col-md-7">--}}
              {{--{!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $lang->id]]) !!}--}}
              {{--@csrf--}}
              {{--<button class="btn btn-default" type="submit">Delete</button>--}}
              {{--{!! Form::close() !!}--}}
              {{--</div>--}}
            </div>
          </div>
          @endforeach
          @else
          <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
            No Record Found
          </div>
          @endif
        </div>

        @if(isset($languages) && count($languages)>0)
        <div style="margin: 0px auto;">
          {{ $languages->appends($_GET)->links()}}
        </div>
        @endif
      </div>

    </div>
  </div>
  @endsection
