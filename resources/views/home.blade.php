@php
use App\Helpers\CommonFunctions;
@endphp

@extends('layouts.app')

@section('content')

     <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />

  @php  
   $user = Auth::user();
    @endphp
  <div class="main__container dashboard-page container__custom fortitle">
    <div class="side-bar-open hidden margin-top-minus-5"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <div class="reservation__atto hide">
      <div class="reservation__title">
        <h1>Dashboard</h1>
      </div>
      <div class="reservation__title">
      
         <a href="#" onclick="event.preventDefault()"  class="btn btn__holo font-weight-600 margin-left-10 export_button" rel = "food_item-active" data-toggle="modal" data-target="#pdf_pop"><i class="fa icon-export" aria-hidden="true"></i> Export Orders</a>
      </div>
    </div>

    <!--DashBoard Wrapper-->

    <div class="dashBoard__wrapper">

    </div>

      {{--<div class="hide">--}}

  <div class="row">
      <div class="reservation__atto fullWidth">
          <div class="reservation__title">
            <h2>Overview</h2>
          </div>
          <div id="sortHomeDashboard" class="selct-picker-plain margin-left-30 width-120" data-data="{!! $getDashboardDaySort !!}">
          <?php
              $sort_by = 30;
              if(Session::has('sort_by')){
                  $sort_by = Session::get('sort_by');
              }
              if($dealId =='100000001'){ ?>
          
              <select onchange="myFunction();" class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="dashboard">
                 <option value="30" {{ (isset($sort_by) && $sort_by == 30) ? 'selected' :'' }}>Last 30 Days</option>
              </select>
          
         <?php }else{ ?>
          
              <select onchange="myFunction();" class="selectpicker dashboardDaySort" data-style="no-background-with-buttonline sortDropDownRight no-padding-left margin-top-5 no-padding-top font-weight-700" rel="dashboard">
                  <option value="2000" {{ (isset($sort_by) && $sort_by == 2000) ? 'selected' :'' }}>Overall</option>
                  <option value="180" {{ (isset($sort_by) && $sort_by == 180) ? 'selected' :'' }}>Last 6 Months</option>
                  <option value="90" {{ (isset($sort_by) && $sort_by == 90) ? 'selected' :'' }}>Last 3 Months</option>
                  <option value="30" {{ (isset($sort_by) && $sort_by == 30) ? 'selected' :'' }}>Last 30 Days</option>
              </select>
          
          <?php } ?>
         </div>
      </div>
    <?php
    //print_r($accountOverviewResArray);

    //echo json_decode($accountOverviewResArray, true)[0]['reach']; die;
    $spenConversion = 0;
    if(isset(json_decode($accountOverviewResArray, true)[0]['spend'])){
        $spend = json_decode($accountOverviewResArray, true)[0]['spend']; 
        $spenConversion = $currencyRate*$spend;
    }
    ?>
      <div class="fullWidth analysis-area flex-box overflow-visible tablet-flex-wrap-ac flex-align-item-center flex-justify-center account-overview">

          <div class="col-xs-3 no-padding-right no-padding-left margin-top-20">
              <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                  <!--tooltip -->
                  <div class="ntooltip">
                    <div class="ttip">Total number of Takeout and delivery orders placed through  website</div>
                  </div>
                  <!--tooltip -->
                  <div class="icon-order-enable margin-bottom-15 text-color-grey"></div>
 
                  <div class="analysis-amt font-weight-800"><?= isset(json_decode($accountOverviewResArray, true)[0]['Conversions']) ? CommonFunctions::number_format_short(round(json_decode($accountOverviewResArray, true)[0]['Conversions']),0) : 0 ?></div>
 
                  <div class="analysis-label margin-bottom-15">Total Order</div>
                  {!! $htmlConversionsGrowthRate !!}
              </div>
          </div>

          <div class="col-xs-3 no-padding-right margin-top-20">
              <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                  <!--tooltip -->
                  <div class="ntooltip">
                    <div class="ttip">Revenue generated from online orders and reservations</div>
                  </div>
                  <!--tooltip -->
                  <div class="icon-revenue text-color-grey margin-bottom-15"></div>
                  <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?= isset(json_decode($accountOverviewResArray, true)[0]['revenue']) ? CommonFunctions::number_format_short(round(json_decode($accountOverviewResArray, true)[0]['revenue']),0) : 0 ?></div>
                  <div class="analysis-label margin-bottom-15">Total Revenue</div>
                  {!! $htmlRevenueGrowthRate !!}
              </div>
          </div>

          <div class="col-xs-3 no-padding-right margin-top-20">
              <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                  <!--tooltip -->
                  <div class="ntooltip">
                    <div class="ttip">People who visit the website</div>
                  </div>
                  <!--tooltip -->
                  <div class="icon-visitors text-color-grey margin-bottom-15"></div>
                  <div class="analysis-amt font-weight-800"><?= isset(json_decode($accountOverviewVisitorsResArray, true)[0]['Visitors']) ? CommonFunctions::number_format_short(round(json_decode($accountOverviewVisitorsResArray, true)[0]['Visitors']),0) : 0 ?></div>
                  <div class="analysis-label margin-bottom-15">Total Visitors</div>
                  {!! $htmlVisitorsGrowthRate !!}
              </div>
          </div>

          <div class="col-xs-3 no-padding-right margin-top-20">
              <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                  <!--tooltip -->
                  <div class="ntooltip">
                    <div class="ttip">Total number of times ads was seen</div>
                  </div>
                  <!--tooltip -->
                  <div class="icon-spend text-color-grey margin-bottom-15"></div>
                  <div class="analysis-amt font-weight-800">{!! $currencySymbol !!}<?=CommonFunctions::number_format_short(round($spenConversion),0);?></div>
                  <div class="analysis-label margin-bottom-15">Total Media Spend</div>
                  {!! $htmlSpendGrowthRate !!}
              </div>
          </div>

          <div class="col-xs-3 margin-top-20 no-padding-right">
              <div class="fullWidth text-center box-shadow white-box padding-top-bottom-30">
                  <!--tooltip -->
                  <div class="ntooltip">
                    <div class="ttip">Total impressions over the month/days</div>
                  </div>
                  <!--tooltip -->
                  <div class="icon-total-reach text-color-grey margin-bottom-15"></div>
                  <div class="analysis-amt font-weight-800"><?= isset(json_decode($accountOverviewResArray, true)[0]['reach']) ? CommonFunctions::number_format_short(round(json_decode($accountOverviewResArray, true)[0]['reach']),0) : 0 ?></div>
                  <div class="analysis-label margin-bottom-15">Total Impressions</div>
                  {!! $htmlReachGrowthRate !!}
              </div>
          </div>
      </div>
  </div>

   <div class="row">
       <div class="col-xs-12 col-md-6 no-padding-left margin-top-40 tablet-no-padding">
       <div class="reservation__atto fullWidth">
                <div class="reservation__title">
                    <h2>
                        <!--tooltip -->
                        <div class="ntooltip h2">
                            <div class="ttip">Revenue generated from online orders and reservations</div>
                        </div>
                        <!--tooltip -->
                        Revenue Trends
                    </h2>
               </div>
               <a href="/report/orders" class="font-weight-700 font-size-18">More</a>
               <!-- <a href="/report/locations" class="font-weight-700 font-size-13">More</a> -->
           </div>
           <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
               <canvas id="revenuetrends" class="fullWidth" width="1500" height="400"></canvas>
           </div>
       </div>
       <div class="col-xs-12 col-md-6 no-padding-right margin-top-40 tablet-no-padding">
       <div class="reservation__atto fullWidth">
                <div class="reservation__title">
                    <h2>
                        <!--tooltip -->
                        <div class="ntooltip h2">
                            <div class="ttip">Revenue generated from online orders and reservations</div>
                        </div>
                        <!--tooltip -->
                        Online Revenue
                    </h2>
               </div>
               <a href="/report/orders" class="font-weight-700 font-size-18">More</a>
               <!-- <a href="/report/locations" class="font-weight-700 font-size-13">More</a> -->
           </div>
           <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
               <canvas id="revenue" class="fullWidth" width="1500" height="400"></canvas>
           </div>
       </div>
   </div>

      <div class="row">
          <div class="col-xs-12 col-md-6 no-padding-left margin-top-40 tablet-no-padding">
              <div class="reservation__atto fullWidth">
                  <div class="reservation__title">
                      <h2>
                        <!--tooltip -->
                        <div class="ntooltip h2">
                            <div class="ttip">Total social reach over the selected time period</div>
                        </div>
                        <!--tooltip -->
                        Social Engagement
                    </h2>
                  </div>

                  <a href="/report/social" class="font-weight-700 font-size-18">More</a>
              </div>
              <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                  <canvas id="socialReach" class="fullWidth" width="1500" height="400"></canvas>
              </div>
          </div>
          <div class="col-xs-12 col-md-6 no-padding-right margin-top-40 tablet-no-padding">
              <div class="reservation__atto fullWidth">
                  <div class="reservation__title">
 
                    <h2>
                        <!--tooltip -->
                        <div class="ntooltip h2">
                            <div class="ttip">Total social reach over the month/days</div>
                        </div>
                        <!--tooltip -->
                        Awareness
                    </h2>
 
                  </div>

                  <a href="/report/marketing" class="font-weight-700 font-size-18">More</a>
              </div>
              <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                  <canvas id="reach" class="fullWidth" width="1500" height="400"></canvas>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-12 col-md-6 no-padding-left margin-top-40 tablet-no-padding">
              <div class="reservation__atto fullWidth">
                  <div class="reservation__title">
                      <h2>
                        <div class="ntooltip h2">
                            <div class="ttip">Most ordered menu items</div>
                        </div>
                        Most Popular Items
                    </h2>
                  </div>

                  <a href="/report/orders" class="font-weight-700 font-size-18">More</a>
              </div>
              <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
                  <canvas id="mostPopularItems" class="fullWidth" width="1500" height="400"></canvas>
              </div>
          </div>
          <div class="col-xs-12 col-md-6 no-padding-right margin-top-40 tablet-no-padding">
           <div class="reservation__atto fullWidth">
               <div class="reservation__title">
                   <h2>
                       <!--tooltip -->
                       <div class="ntooltip h2">
                            <div class="ttip">Total Users(New + returning) over the month/days</div>
                        </div>
                        <!--tooltip -->
                       Website Visitors
                    </h2>
               </div>

               <a href="/report/visitors" class="font-weight-700 font-size-18">More</a>
           </div>
           <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-30 padding-left-right-30">
               <canvas id="visitor" class="fullWidth" width="1500" height="400"></canvas>
           </div>
       </div>
      </div>

      <div class="row">
          <div class="fullWidth margin-top-40 tablet-no-padding most_loyal_customer">
              <div class="reservation__atto fullWidth ">
                  <div class="reservation__title">
                    <h2>
                        <div class="ntooltip h2">
                            <div class="ttip">Details of top 5 customers by orders/reservations made online</div>
                        </div>
                        Most Loyal Customer
                    </h2>
                  </div>

                  <a href="/guestbook" class="font-weight-700 font-size-18">More</a>
              </div>

              <div class="col-xs-12 white-box box-shadow margin-top-20 padding-top-bottom-15">
                  <div class="col-xs-12 padding-top-bottom-15 content-head no-padding margin-bottom-10">
                      <div class="col-xs-4 col-sm-4 col-md-5">
                        <div class="col-xs-5 show-tablet-100 no-padding-left tablet-no-padding">FULL NAME</div>
                        <div class="col-xs-7 hidden-tablet no-padding-left">EMAIL/PHONE</div>
                      </div>
                      <div class="col-xs-3">AMOUNT</div>
                      <div class="col-xs-3 col-sm-3 col-md-2 no-padding">FIRST ACTIVITY</div>
                      <div class="col-xs-2 no-padding">LAST ACTIVITY</div>
                  </div>



                  @if(isset($mostLoyalCustomerQueryRes) && count($mostLoyalCustomerQueryRes)>0)
                      @foreach($mostLoyalCustomerQueryRes as $item)

                          <div class="col-xs-12 content-row">
                              <div class="col-xs-4 col-sm-4 col-md-5">
                                  <div class="col-xs-5 show-tablet-100 no-padding-left tablet-no-padding text-capitalize">{{ $item-> name }}</div>
                                  <div class="col-xs-7 show-tablet-100 no-padding-left tablet-no-padding">
                                      <div class="flex-box flex-direction-row flex-align-item-center"><span class="fa fa-envelope margin-right-5"></span> <span class="text-ellipsis">{{ $item-> email }}</span></div>
                                      <div class="flex-box flex-direction-row flex-align-item-center data-icons home-contact"><span class="icon-contact-us margin-right-5"></span> {{ $item-> phone }}</div>
                                  </div>
                              </div>
                              <div class="col-xs-3">
 
                                  {{ $item-> ct_orders }} Orders
                                  <br/><span class="text-color-blue">${{ $item-> total_amount }}</span>
 
                              </div>
                              <div class="col-xs-3 col-sm-3 col-md-2 no-padding">{{ date('d-m-Y', strtotime($item->first_activity)) }}</div>
                              <div class="col-xs-2 no-padding">{{ date('d-m-Y', strtotime($item->last_activity)) }}</div>
                          </div>

                      @endforeach
                  @else
                      <div class="col-xs-12 text-center padding-top-bottom-20">
                          No Record Found
                      </div>
                  @endif

              </div>
          </div>
      </div>
      {{--</div>--}}


  <!-- <div class="justify-content-center" style="width:70%;float:left;margin-left:12px;">
  <div class="card">
  <div class="card-header">Dashboard</div>

  <div class="card-body">
  @if (session('status'))
    <div class="alert alert-success">
{{ session('status') }}
            </div>
@endif

          You are logged in!
          </div>
          </div>
          </div> -->
  </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{asset('js/chartjs-datalabels.js')}}"></script>
    {{--<script src="{{asset('js/chartjs-plugin-annotation.js')}}"></script>--}}
    <script src="{{asset('js/reports.js')}}"></script>


    

    <script>
        var currencySymbol = '{!! $currencySymbol !!}';
    </script>

<script>
        var revenuetrends = document.getElementById("revenuetrends");
        revenuetrends.height = 800;
        var revenuetrendsData = <?=json_encode($revenuetrends);?>;
        var dayArray = [];
        var revenuetrendsArray = [];
        $.each(revenuetrendsData, function (key, value) {
            dayArray.push(value.day);
            revenuetrendsArray.push(value.revenuetrend);
        });
        var chartInstanceRevenue = new Chart(revenuetrends, {
            type: 'line',
            data: {
                labels: dayArray,
                datasets: [{
                    //fillColor : gradient,
                    backgroundColor: "transparent",
                    pointBackgroundColor: chartPrimaryColor,
                    borderWidth: 3,
                    borderColor: chartPrimaryColor,
                    pointHighlightStroke: chartSecondaryColor,
                    data: revenuetrendsArray,
                    datalabels: {
                        align: 'end',
                        anchor: 'end'
                    }
                }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 25,
                        bottom: 0
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: fontSize,
                            fontColor: fontColor,
                            padding: 10,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return currencySymbol + value;
                            }
                        }
                    }]
                },
                plugins: {
                    datalabels: {
                        color: fontColor,
                        display: 'auto',
                        /*display: function(context) {
                            console.log(context);
                            return context.dataset.data[context.dataIndex] > 15;
                        },*/
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return currencySymbol + Math.round(value);
                        },
                        clamp: true
                    }
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "revenuetrends";
                        },
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return currencySymbol+tooltipItem[0].yLabel;
                        }
                    }
                }}
        });
        noDataOnChart(chartInstanceRevenue, revenuetrends, revenuetrendsArray, '')
    </script>

    <script>

        var revenue = document.getElementById("revenue");
        revenue.height = 800;

        var revenueData = <?=json_encode($revenueRes);?>;

        var dayArray = ["New Customers", "Returning Customers"];
        // var revenueArray = [];
        var revenueArray = [];

        $.each(revenueData, function (key, value) {
            // dayArray.push(value.day);
             revenueArray.push(value.revenue);
        });
        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        // var bar_ctx = revenue.getContext('2d');
        // var gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);

        var chartInstanceRevenue = new Chart(revenue, {
            type: 'doughnut',
            data: {
                labels: dayArray,
                datasets: [{
                backgroundColor: [
                    "#1689fb",
                    "#ed78fd"
                ],
                    data: revenueArray,
                }]
            },
            options: {
                responsive: true,
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        align: 'end',
                        anchor: 'end',
                        color: fontColor,
                        display: 'auto',
                        font: {
                            weight: fontWeight,
                            size: fontSize
                        },
                        formatter: function(value, context) {
                            return percentage(value, context);
                        }
                    }
                },
                legend: {
                    display: true,
                    labels: {
                         boxWidth: 15,

                    }
                },
                layout: {
                    padding: {
                        top: 0,
                        bottom: 20,
                        left:0,
                        right:0
                    }
                },
                tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        title: function(tooltipItem, data) {
                          return data['labels'][tooltipItem[0]['index']];
                        },
                        label: function(tooltipItem, data) {
                          return currencySymbol + data['datasets'][0]['data'][tooltipItem['index']];
                        },
                      //   afterLabel: function(tooltipItem, data) {
                      //   var dataset = data['datasets'][0];
                      //   var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
                      //   return '(' + percent + '%)';
                      // }
                  }
                }
            }
        });

        // var chartInstanceRevenue = new Chart(revenue, {
        //     type: 'line',
        //     data: {
        //         labels: dayArray,
        //         datasets: [{
        //             backgroundColor: "#1da9a0",
        //             pointBackgroundColor: "#1da9a0",
        //             borderWidth: 3,
        //             label: "Retention",
        //             fill: false,
        //             borderColor: "#1da9a0",
        //             pointHighlightStroke: chartSecondaryColor,
        //             data: revenueArray,
        //             datalabels: {
        //                 display: false,
        //                 align: 'end',
        //                 anchor: 'end'
        //             }
        //         },
        //         {
        //             backgroundColor: "#1689fb",
        //             pointBackgroundColor: "#1689fb",
        //             borderWidth: 3,
        //             label: "New Acquisition",
        //             fill: false,
        //             borderColor: "#1689fb",
        //             pointHighlightStroke: chartSecondaryColor,
        //             data: revenueArray1,
        //             datalabels: {
        //                 display: false,
        //                 align: 'end',
        //                 anchor: 'end'
        //             }
        //         }]
        //     },
        //     options: {
        //         animation: {
        //             easing: 'easeInOutQuad',
        //             duration: 520
        //         },
        //         layout: {
        //             padding: {
        //                 left: 0,
        //                 right: 0,
        //                 top: 0,
        //                 bottom: 0
        //             }
        //         },
        //         scales: {
        //             xAxes: [{
        //                 gridLines: {
        //                     color: gridColor,
        //                     lineWidth: gridWidth
        //                 },
        //                 ticks: {
        //                     fontSize: fontSize,
        //                     fontColor: fontColor
        //                 }
        //             }],
        //             yAxes: [{
        //                 gridLines: {
        //                     display: false,
        //                     color: gridColor,
        //                     lineWidth: gridWidth
        //                 },
        //                 ticks: {
        //                     beginAtZero: true,
        //                     fontSize: fontSize,
        //                     fontColor: fontColor,
        //                     padding: 10,
        //                     callback: function(value, index, values) {
        //                         return currencySymbol + value;
        //                     }
        //                 }
        //             }]
        //         },
        //         plugins: {
        //             datalabels: {
        //                 color: fontColor,
        //                 display: 'auto',
        //                 font: {
        //                     weight: fontWeight,
        //                     size: fontSize
        //                 },
        //                 formatter: function(value, context) {
        //                     return currencySymbol + Math.round(value);
        //                 },
        //                 clamp: true
        //             }
        //         },
        //         elements: {
        //             line: {
        //                 tension: 0
        //             }
        //         },
        //         legend: {
        //             display: true,
        //             labels: {
        //                 boxWidth: 15,
        //     }
        //         },
        //         tooltips: {
        //             yPadding: 15,
        //             xPadding: 25,
        //             backgroundColor: "white",
        //             borderColor: chartPrimaryColor,
        //             borderWidth: 1,
        //             displayColors: false,
        //             titleFontColor: chartSecondaryColor,
        //             titleFontSize: 16,
        //             _titleAlign: 'center',
        //             titleAlign: 'center',
        //             titleMarginBottom:10,
        //             bodyFontColor: chartPrimaryColor,
        //             bodyFontSize: 14,
        //             bodyFontStyle: "bold",
        //             _bodyAlign: 'center',
        //             bodyAlign: 'center',
        //             footerFontColor: chartBlackColor,
        //             footerFontSize: 14,
        //             _footerAlign: 'center',
        //             footerAlign: 'center',
        //             cornerRadius: 4,
        //             callbacks: {
        //                 title: function(tooltipItem, data) {
        //                     return "Revenue";
        //                 },
        //                 label: function(tooltipItem, data) {
        //                     return tooltipItem.xLabel;
        //                 },
        //                 footer: function(tooltipItem, data) {
        //                     return currencySymbol+tooltipItem[0].yLabel;
        //                 }
        //             }
        //         }}
        // });

        noDataOnChart(chartInstanceRevenue, revenue, revenueArray, '')

    </script>
    <script>

      var visitor = document.getElementById("visitor");
      visitor.height = 800;

      var visitorsData = <?= $visitorChartResArray; ?>;
      var paidvisitorsData = <?= $paidVisitorChartResArray; ?>;

      var dayArray = [];
      var visitorsArray = [];
      var paidvisitorsArray = [];

      $.each(visitorsData, function (key, value) {
          dayArray.push(value.day);
          visitorsArray.push(value.Visitors);
      });

      $.each(paidvisitorsData, function (key, value) {
          paidvisitorsArray.push(paidvisitorsData[key].Visitors);
      });


      var chartInstanceVisitor = new Chart(visitor, {
          type: 'line',
          data: {
              labels: dayArray,
              datasets: [{
                    //fillColor : gradient,
                    backgroundColor: "#c3ea77",
                    pointBackgroundColor: "#7eb144",
                    borderWidth: 3,
                    label: "Organic/Direct",
                    fill: true,
                    borderColor: "#7eb144",
                    pointHighlightStroke: chartSecondaryColor,
                    data: visitorsArray
              },
              {
                  //fillColor : gradient,
                  backgroundColor: "#54c3f0",
                    pointBackgroundColor: "#1689fb",
                    borderWidth: 3,
                    label: "Paid",
                    fill: true,
                    borderColor: "#1689fb",
                    pointHighlightStroke: chartSecondaryColor,
                    data: paidvisitorsArray
              }]
          },
          options: {
              animation: {
                  easing: 'easeInOutQuad',
                  duration: 520
              },
              plugins: {
                  datalabels: {
                      display: false
                  }
              },
              scales: {
                  xAxes: [{
                      gridLines: {
                          color: gridColor,
                          lineWidth: gridWidth
                      },
                      ticks: {
                          fontSize: fontSize,
                          fontColor: fontColor
                      }
                  }],
                  yAxes: [{
                      gridLines: {
                          display: false,
                          color: gridColor,
                          lineWidth: gridWidth
                      },
                      ticks: {
                          beginAtZero: true,
                          fontSize: fontSize,
                          fontColor: fontColor
                      },
                      stacked: true
                  }]
              },
              elements: {
                  line: {
                      tension: 0
                  }
              },
              legend: {
                display: true,
                    labels: {
                        boxWidth: 15,
                }
              },
              
              tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartPrimaryColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        beforeFooter: function(tooltipItem, data) {
                            return "Organic/Direct : " + tooltipItem[0].yLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return "Paid : " + tooltipItem[1].yLabel;
                        }
                    }
                }
            }
      });

      noDataOnChart(chartInstanceVisitor, visitor, visitorsArray, paidvisitorsArray, '')

    </script>

    <script>

        var reach = document.getElementById("reach");
        reach.height = 800;

        var reachData = <?= json_encode($reachRes)?>;
        var conversionRate = <?= $currencyRate ?>;
        var dayArray = [];
        var reachArray = [];
        var reachArray1 = [];

        $.each(reachData, function (key, value) {
            dayArray.push(value.day);
            reachArray.push(value.impression);
            //reachArray1.push(value.spend);
            var spenValue = (conversionRate*value.spend).toFixed(2);
            reachArray1.push(spenValue);
        });

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 20;
        };

        var chartInstanceReach = new Chart(reach, {
            type: 'line',
            data: {
                labels: dayArray,
                datasets: [{
                    //fillColor : gradient,
                    backgroundColor: "#1da9a0",
                    pointBackgroundColor: "#1da9a0",
                    borderWidth: 3,
                    label: "Impression",
                    fill: false,
                    borderColor: "#1da9a0",
                    pointHighlightStroke: chartSecondaryColor,
                    data: reachArray,
                    yAxisID: 'y-axis-1'
                },
                    {
                        //fillColor : gradient,
                        backgroundColor: "#ff9700",
                        pointBackgroundColor: "#ff9700",
                        borderWidth: 3,
                        label: "Spend",
                        fill: false,
                        borderColor: "#ff9700",
                        pointHighlightStroke: chartSecondaryColor,
                        data: reachArray1,
                        yAxisID: 'y-axis-2'
                    }]
            },
            options: {
                animation: {
                    easing: 'easeInOutQuad',
                    duration: 520
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        ticks:{
                            fontSize: fontSize,
                            fontColor: fontColor
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        id: 'y-axis-1',
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            fontColor: fontColor,
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                            // Include a dollar sign in the ticks
                            /*callback: function(value, index, values) {
                                return currencySymbol + value;
                            }*/
                        }
                    },{
                        gridLines: {
                            display: false,
                            color: gridColor,
                            lineWidth: gridWidth
                        },
                        display: true,
                        id: 'y-axis-2',
                        position: "right",
                        ticks: {
                            beginAtZero: true,
                            fontColor: fontColor
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: true,
                    labels: {
                        boxWidth: 15,
                    }
                },
                tooltips: {
                    mode: 'index',
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartPrimaryColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return false;
                        },
                        beforeFooter: function(tooltipItem, data) {
                            return "Impression : " + tooltipItem[0].yLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return "Spend : " + currencySymbol + tooltipItem[1].yLabel;
                        }
                    }
                }

            }
        });

        noDataOnChart(chartInstanceReach, reach, reachArray, reachArray1, '')

    </script>

    <script>

      var socialReach = document.getElementById("socialReach");
      socialReach.height = 800;

      var socialReachData = <?= $socialReachResArray ?>;

        var dayArray = [];
        var fbReachArray = [];

        $.each(socialReachData, function (key, value) {
        dayArray.push(value.day);
        fbReachArray.push(value.fb_reach);
        });

      var chartInstanceSocialReach = new Chart(socialReach, {
          type: 'line',
          data: {
              labels: dayArray,
            //   datasets: [{
            //       //fillColor : gradient,
            //       backgroundColor: "transparent",
            //       pointBackgroundColor: chartPrimaryColor,
            //       borderWidth: 3,
            //       borderColor: chartPrimaryColor,
            //       pointHighlightStroke: chartSecondaryColor,
            //       data: fbReachArray
            //   }]

            datasets: [{
                  //fillColor : gradient,
                    backgroundColor: "#c8814e",
                    pointBackgroundColor: "#c8814e",
                    borderWidth: 3,
                    label: "Engagement",
                    fill: false,
                    borderColor: "#c8814e",
                    pointHighlightStroke: chartSecondaryColor,
                    data: fbReachArray
              }]
          },
          options: {
              animation: {
                  easing: 'easeInOutQuad',
                  duration: 520
              },
              plugins: {
                  datalabels: {
                      display: false
                  }
              },
              scales: {
                  xAxes: [{
                      gridLines: {
                          color: gridColor,
                          lineWidth: gridWidth
                      },
                      ticks:{
                          fontSize: fontSize,
                          fontColor: fontColor
                      }
                  }],
                  yAxes: [{
                      gridLines: {
                          display: false,
                          color: gridColor,
                          lineWidth: gridWidth
                      },
                      ticks: {
                          beginAtZero: true,
                          fontSize: fontSize,
                          fontColor: fontColor
                      }
                  }]
              },
              elements: {
                  line: {
                      tension: 0
                  }
              },
              legend: {
                display: true,
                    labels: {
                        boxWidth: 15,
                }
              },
              tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "Social Engagement";
                        },
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return tooltipItem[0].yLabel;
                        }
                    }
                }

          }
      });

      noDataOnChart(chartInstanceSocialReach, socialReach, fbReachArray, '')

    </script>
    <script>

      var mostPopularItems = document.getElementById("mostPopularItems");
      mostPopularItems.height = 800;

      var mostPopularItemsData = <?=json_encode($menuItemsRes)?>;
      
      var itemArray = [];
      var countArray = [];

      $.each(mostPopularItemsData, function (key, value) {
          itemArray.push(value.item);
          countArray.push(value.ct_item);
      });

      var chartInstanceMostPopularItems = new Chart(mostPopularItems, {
          type: 'bar',
          data: {
              labels: itemArray,
              datasets: [{
                  //fillColor : gradient,
                  label: 'Popupar Item No',
                  backgroundColor: "#b3d76e",
                  pointBackgroundColor: '#b3d76e',
                  pointHighlightStroke: chartSecondaryColor,
                  data: countArray
              }]
          },
          options: {
              animation: {
                  easing: 'easeInOutQuad',
                  duration: 520
              },
              plugins: {
                  datalabels: {
                      display: false
                  }
              },
              scales: {
                  xAxes: [{
                      gridLines: {
                          color: gridColor,
                          lineWidth: gridWidth
                      },
                      ticks: {
                          fontSize: fontSize,
                          fontColor: fontColor
                      },
                      barPercentage: 0.6
                  }],
                  yAxes: [{
                      gridLines: {
                          display: false,
                          color: gridColor,
                          lineWidth: gridWidth
                      },
                      ticks: {
                          beginAtZero: true,
                          steps: 10,
                          stepValue: 5,
                          fontSize: fontSize,
                          fontColor: fontColor
                      }
                  }]
              },
              elements: {
                  line: {
                      tension: 0
                  }
              },
              legend: {
                  display: false
              },
              tooltips: {
                    yPadding: 15,
                    xPadding: 25,
                    backgroundColor: "white",
                    borderColor: chartPrimaryColor,
                    borderWidth: 1,
                    displayColors: false,
                    titleFontColor: chartSecondaryColor,
                    titleFontSize: 16,
                    _titleAlign: 'center',
                    titleAlign: 'center',
                    titleMarginBottom:10,
                    bodyFontColor: chartPrimaryColor,
                    bodyFontSize: 14,
                    bodyFontStyle: "bold",
                    _bodyAlign: 'center',
                    bodyAlign: 'center',
                    footerFontColor: chartBlackColor,
                    footerFontSize: 14,
                    _footerAlign: 'center',
                    footerAlign: 'center',
                    cornerRadius: 4,
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return "Most Popular Item";
                        },
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel;
                        },
                        footer: function(tooltipItem, data) {
                            return tooltipItem[0].yLabel;
                        }
                    }
                }
//              tooltips: {
//                  mode: 'index',
//                  titleFontFamily: 'Arial',
//                  backgroundColor: 'orange',
//                  titleFontColor: 'white',
//                  tooltipFontStyle: fontWeight,
//                  caretSize: 10,
//                  cornerRadius: 4
//              }
          }
      });

      noDataOnChart(chartInstanceMostPopularItems, mostPopularItems, countArray, '')

    </script>


  <script type="text/javascript">
    
    //setTimeout(function(){ // PE-2087
       // location.reload(); // then reload the page.(3)
   // }, 60000);

  </script>
@endsection
