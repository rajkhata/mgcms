@extends('layouts.app')

@section('content')

    <div class="main__container container__custom">
            <div class="reservation__atto">
                    <div class="reservation__title">
                        <h1>Menu Items<span	style="margin:0px 5px;">({{ $menuItems->currentPage() .'/' . $menuItems->lastPage() }})</span>&emsp;</h1>
                    </div>
                    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('menu_item/create') }}" class="btn btn__primary">Add</a></span>
            </div>
            <div class="addition__functionality">
                {!! Form::open(['method'=>'get']) !!}
                <div class="row functionality__wrapper">
                    <div class="row form-group col-md-3">
                        <select name="restaurant_id" id="restaurant_id"
                                class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}">
                            <option value="">Select Restaurant</option>
                            @foreach($groupRestData as $rest)
                                <optgroup label="{{ $rest['restaurant_name'] }}">
                                    @foreach($rest['branches'] as $branch)
                                        <option value="{{ $branch['id'] }}" {{ (isset($searchArr['restaurant_id']) && $searchArr['restaurant_id']==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="row form-group col-md-1">
                        &nbsp;
                    </div>
                    <div class="row form-group col-md-3">
                        <input class="form-control" placeholder="Name" type="text" name="name"
                               value="{{isset($searchArr['name']) ? $searchArr['name'] : ''}}">
                    </div>
                    <div class="row form-group col-md-3">
                        <button class="btn btn__primary" style="margin-left: 5px;" type="submit">Search</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            <div>
                <div class="active__order__container">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @elseif (session()->has('err_msg'))
                        <div class="alert alert-danger">
                            {{ session()->get('err_msg') }}
                        </div>
                    @endif

                    <div class="guestbook__container box-shadow">
                        <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
                            <div class="col-md-10 row">
                                <div class="col-md-4" style="font-weight: bold;">Name</div>
                                <div class="col-md-4" style="font-weight: bold;">Restaurant</div>
                                <div class="col-md-2" style="font-weight: bold;">Category</div>
                                <div class="col-md-2" style="font-weight: bold;">Status 34</div>
                            </div>
                            <div class="col-md-2 row">
                                <div class="col-md-12" style="font-weight: bold;">Action</div>
                            </div>
                        </div>
                        @if(isset($menuItems) && count($menuItems)>0)
                            @foreach($menuItems as $menuItem)
                                <div class="guestbook__customer-details-content rest__row">
                                    <div class="col-md-10 order__row">
                                        <div class="col-md-4" style="margin: auto 0px;">{{ isset($menuItem->name) ? $menuItem->name : ''}}</div>
                                        <div class="col-md-4"
                                             style="margin: auto 0px;">{{ isset($menuItem->Restaurant->restaurant_name) ? $menuItem->Restaurant->restaurant_name : '' }}</div>
                                        <div class="col-md-2"
                                             style="margin: auto 0px;">{{ isset($menuItem->MenuCategory->name) ? $menuItem->MenuCategory->name : '' }}</div>
                                        <div class="col-md-2"
                                             style="margin: auto 0px;">{{ ($menuItem->status==1)?'Active':($menuItem->status==0?'Inactive':'') }}</div>
                                    </div>
                                    <div class="row col-md-2" style="margin: auto 0px;">
                                        <div class="col-md-12">
                                            <a style="width:100%;margin:10px 0;" href="{{ URL::to('menu_item/' . $menuItem->id . '/edit') }}"
                                               class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
                                        </div>
                                        <div class="col-md-12">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['menu_item.destroy', $menuItem->id]]) !!}
                                            @csrf
                                            <button style="width:100% ;margin:10px 0;" class="btn btn__cancel" type="submit"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row"
                                 style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                                No Record Found
                            </div>
                        @endif
                    </div>

                    @if(isset($menuItems) && count($menuItems)>0)
                        <div style="margin: 0px auto;text-align:center;">
                            {{ $menuItems->appends($_GET)->links()}}
                        </div>
                    @endif
                </div>

            </div>
    </div>

@endsection