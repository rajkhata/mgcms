@extends('layouts.app')
@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>{{ __('Menu Item - Edit') }}</h1>
 <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
            </div>

        </div>

        <div>

            <div style="max-width: 750px !important;" class="card form__container">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="">
                        {{ Form::model($menuItem, array('route' => array('menu_item.update', $menuItem->id), 'method' => 'PUT', 'files' => true)) }}
                        @csrf
                        <div class="form-group row form_field__container">
                            <label for="restaurant_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                            <div class="col-md-4">
                                <select name="restaurant_id" id="restaurant_id"
                                        class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                                        required>
                                    @foreach($groupRestData as $rest)
                                        <optgroup label="{{ $rest['restaurant_name'] }}">
                                            @foreach($rest['branches'] as $branch)
                                                <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id'] || (count(old())==0 && $menuItem->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                                @if ($errors->has('restaurant_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Default Language') }}</label>
                            <div class="col-md-4">
                                <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                                    <option value="">Select Language</option>
                                    @foreach($languageData as $lang)
                                        <option value="{{ $lang->id }}" {{ ($menuItem->language_id==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('language_id'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('language_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="menu_category_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>
                            <div class="col-md-4">
                                <select name="menu_category_id" id="menu_category_id"
                                        class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}">
                                    <option value="">Select Category</option>
                                    @foreach($categoryData as $cat)
                                        <option value="{{ $cat->id }}" {{ (old('menu_category_id')==$cat->id || (count(old())==0 && $menuItem->menu_category_id==$cat->id)) ? 'selected' :'' }}>{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('menu_category_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('menu_category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="menu_sub_category_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Sub Category') }}</label>
                            <div class="col-md-4">
                                <select name="menu_sub_category_id" id="menu_sub_category_id"
                                        class="form-control {{ $errors->has('menu_sub_category_id') ? ' is-invalid' : '' }}">
                                    <option value="">Select Sub-Category</option>
                                  
                                    {!! $subcat_options !!}
                      
                                </select>
                                @if ($errors->has('menu_sub_category_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('menu_sub_category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row form_field__container">
                            <label for="product_type"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Product Type') }}</label>
                            <div class="col-md-4">
                                <select name="product_type" id="product_type"
                                        class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }}">
                                    <option value="">Select Product Type</option>
                                    @foreach($product_types as $key=>$val)
                                        <option value="{{ $key }}" {{ (old('product_type')==$key || (count(old())==0 && $menuItem->product_type==$key)) ? 'selected' :'' }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('product_type'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Edit availibility code -->

                        <div class="form-group row form_field__container">
                            <label for="slot_code"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Availability') }}</label>
                            <div class="col-md-4">
                                <select name="slot_code" id="slot_code"
                                        class="form-control {{ $errors->has('slot_code') ? ' is-invalid' : '' }}">
                                    <option value="">Select Availability</option>

                                        @foreach($visibilityData as $data)
                                            <option value="{{ $data->slot_code }}" {{ (old('slot_code')==$data->slot_code) ? 'selected' :'' }}>{{ $data->slot_name }}</option>
                                        @endforeach

                                </select>
                                @if ($errors->has('slot_code'))
                                    <span class="invalid-feedback">
                                <strong>{{ $errors->first('slot_code') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!-- end code -->

                          <div class="product_type_product @if($menuItem->product_type=='product') @else hide  @endif">
                                <div class="form-group row form_field__container">
                                    <label for="gift_wrapping_fees" class="col-md-4 col-form-label text-md-right">{{ __('Gift Wrapping Fees') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <input id="gift_wrapping_fees" type="text"
                                                class="{{ $errors->has('gift_wrapping_fees') ? ' is-invalid' : '' }}"
                                                name="gift_wrapping_fees" value="{{ count(old())==0 ? $menuItem->gift_wrapping_fees : old('gift_wrapping_fees') }}" >
                                        </div>
                                        
                                    </div>
                                </div>

                               <!--  <div class="form-group row form_field__container">
                                    <label for="gift_message" class="col-md-4 col-form-label text-md-right">{{ __('Gift Message') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                        <textarea maxlength="500" rows="4" name="gift_message" id="gift_message"
                                              class="form-control{{ $errors->has('gift_message') ? ' is-invalid' : '' }}"
                                              >{{ count(old())==0 ? $menuItem->gift_message : old('gift_message') }}</textarea>
                                        </div>
                                    
                                    </div>
                                </div> -->
                            </div>   
                       

                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="name" type="text"
                                           class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ count(old())==0 ? $menuItem->name : old('name') }}"
                                           required>
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="description"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                            <div class="col-md-4">
                                <div class="custom__message-textContainer">
 
 
                                <textarea  rows="4" name="description" id="description"
                                          class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} editor"         >{{ count(old())==0 ? $menuItem->description : old('description')}}</textarea>
 
                                </div>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row form_field__container">
                            <label for="sub_description"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Sub Description') }}</label>
                            <div class="col-md-4">
                                <div class="custom__message-textContainer">
 
 
                                <textarea  rows="4" name="sub_description" id="sub_description"
                                          class="form-control{{ $errors->has('sub_description') ? ' is-invalid' : '' }} editor"
  >{{ count(old())==0 ? $menuItem->sub_description : old('sub_description')}}</textarea>
 
                                </div>
                                @if ($errors->has('sub_description'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('sub_description') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        {{--<div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Size') }}</label>
                            <div class="col-md-4">
                                <input id="size" type="text" class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}"
                                       name="size" value="{{ $menuItem->size }}" required>
                                @if ($errors->has('size'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('size') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>--}}
                    <!-- Is Tax applicable Yes/ No etc Default Yes RG-->
                        <div class="form-group row form_field__container">
                            <label for="is_tax_applicable"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Taxable') }}</label>
                            <div class="col-md-4">
                                <select name="is_tax_applicable" id="is_tax_applicable"
                                        class="form-control {{ $errors->has('is_tax_applicable') ? ' is-invalid' : '' }}">
                                    <option value="1" Selected>Yes</option>
                                    <option value="0">No</option>


                                </select>
                                @if ($errors->has('is_tax_applicable'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('is_tax_applicable') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                            <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is Popular</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input id="is_popular" type="checkbox" class="" name="is_popular" {{ ((isset($menuItem->is_popular) && $menuItem->is_popular=='1') || old('is_popular')=='1') ? 'checked' :'' }}>
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is Favourite</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input id="is_favourite" type="checkbox" class="" name="is_favourite" {{ ((isset($menuItem->is_favourite) && $menuItem->is_favourite=='1') || old('is_favourite')=='1') ? 'checked' :'' }}>
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="status1" name="status"
                                       value="1" {{ (old('status')=='1' || (count(old())==0 && $menuItem->status=='1')) ? 'checked' :'' }}>
                                Active
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="status0" name="status"
                                       value="0" {{ (old('status')=='0' || (count(old())==0 && $menuItem->status=='0')) ? 'checked' :'' }}>
                                Inactive
                                @if ($errors->has('status'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row form_field__container">
                            <label for="display_order" class="col-md-4 col-form-label text-md-right">{{ __('Display Order') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="display_order" type="text"
                                        class="{{ $errors->has('display_order') ? ' is-invalid' : '' }}"
                                        name="display_order" value="{{ count(old())==0 ? $menuItem->display_order : old('display_order') }}" >
                                </div>
                                @if ($errors->has('display_order'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('display_order') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>                      
                        <?php 
                        $allimages=json_decode($menuItem->images,true);
                        /*echo '<pre>';
                        print_r( $allimages);
                        echo '</pre>';*/
                        ?>
                        <!-- Mobile app Image-->
                        <div class="form-group row form_field__container source-image-div image-caption-div">
                            <label for="image"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Mobile App Image') }}</label>
                            <div class="col-md-8">
       
                                                                
                            <div class="image_cont_main row">
                                 @if(empty($allimages['mobile_app_images']))
 
                                <div class="col-md-9">
                                      <div class="col-md-10">
                                        <input id="image" type="file"
                                               class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                                               value="{{ old('image.0') }}" name="image[]">
                                        @if ($errors->has('image'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        <div class="input__field">
                                            <input id="image_caption" type="text"
                                                   class="{{ $errors->has('image_caption') ? ' is-invalid' : '' }}"
                                                   name="image_caption[]"
                                                   value="{{old('image_caption.0') }}"
                                                   placeholder="Image Caption">
                                        </div>
                                        @if ($errors->has('image_caption.0'))
                                            <span class="invalid-feedback" style="display: block;">
                                                <strong>{{ $errors->first('image_caption.0') }}</strong>
                                            </span>
                                        @endif
                                </div> 
                                 <div class="col-md-2">

                                <button type="button" imagetype="image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                </div>                                      
                            </div>       
                                 @else
                                 <div class="col-md-6">

                                <button type="button" imagetype="image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                </div>    
                               @endif  
                            </div>

                            @if(isset($allimages['mobile_app_images']) && count($allimages['mobile_app_images'])>0)
                            <?php 
                            $i=0;
                             ?>                             
                             @foreach($allimages['mobile_app_images'] as $key=>$image)
                                <div class="image_cont_main row">
                                    
                                    <input type="hidden" name="image_hidden[]" value="{{$key}}" >
                                    <div class="col-md-3 ">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['image']}}">
                                            <img class="img-responsive img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['thumb_image_sml'] }}"></a>&emsp;
                                      </div>
                                    <div class="col-md-9">
                                         <div class="col-md-10">
                                            <input id="image" type="file"
                                                   class="form-control"
                                                   value="" name="image[{{$key}}]">
                                            
                                            <div class="input__field">
                                                <input id="image_caption" type="text"
                                                       class=""
                                                       name="image_caption[{{$key}}]"
                                                       value="{{$image['image_caption']}}"
                                                       placeholder="Image Caption">
                                            </div>
                                      </div> 
                                      <div class="col-md-2">
                                           <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >

                                      </div>       

                                </div>
                                    
                                </div>

                              <?php 
                            $i++;
                             ?>
                            @endforeach

                            @endif
                            </div>
                           
                        </div>
 <hr>
                        <!-- Desktop Web Image-->
                        <div class="form-group row form_field__container source-image-div image-caption-div">
                            <label for="web_image"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Desktop Web Image') }}</label>
                            <div class="col-md-8">
                                <div class="image_cont_main row">
                                     @if(empty($allimages['desktop_web_images']))
                                        <div class="col-md-9">
                                            <div class="col-md-10">
                                                <input id="web_image" type="file"
                                                       class="form-control{{ $errors->has('web_image') ? ' is-invalid' : '' }}"
                                                       value="{{ old('web_image.0') }}" name="web_image[]">
                                                @if ($errors->has('web_image'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('web_image') }}</strong>
                                                    </span>
                                                @endif
                                                <div class="input__field">
                                                    <input id="web_image_caption" type="text"
                                                           class="{{ $errors->has('web_image_caption') ? ' is-invalid' : '' }}"
                                                           name="web_image_caption[]"
                                                           value="{{ count(old())==0 ?@$allimages['desktop_web_images'][0]['web_image_caption']  : old('web_image_caption.0') }}"
                                                           placeholder="Image Caption">
                                                </div>
                                                @if ($errors->has('web_image_caption.0'))
                                                    <span class="invalid-feedback" style="display: block;">
                                                        <strong>{{ $errors->first('web_image_caption.0') }}</strong>
                                                    </span>
                                                @endif
                                           </div>
                                            <div class="col-md-2">
                                              <button type="button" imagetype="web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                            </div>
                                    </div>
                                    @else 
                                    <div class="col-md-6">

                                   <button type="button" imagetype="web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                   </div>      
                                    @endif;
                            </div>
                               @if(isset($allimages['desktop_web_images']) && count($allimages['desktop_web_images'])>0)
                            <?php 
                            $i=0;
                             ?>
                             @foreach($allimages['desktop_web_images'] as $key=>$image)
                                <div class="image_cont_main row">
                                    <input type="hidden" name="web_image_hidden[]" value="{{$key}}" >
                                     <div class="col-md-3 ">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['web_image']}}">
                                            <img class="img-responsive img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['web_thumb_sml'] }}"></a>&emsp;
                                      </div>
                                    <div class="col-md-9">

                                       <div class="col-md-10">
                                            <input id="web_image" type="file"
                                                   class="form-control"
                                                   value="" name="web_image[{{$key}}]">
                                            
                                            <div class="input__field">
                                                <input id="web_image_caption" type="text"
                                                       class=""
                                                       name="web_image_caption[{{$key}}]"
                                                       value="{{$image['web_image_caption']}}"
                                                       placeholder="Image Caption">
                                            </div>
                                        </div>    
                                        <div class="col-md-2">
                                           <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >  
                                        </div>
                                        

                                </div>
                                   
                                </div>

                            @endforeach

                            @endif
                            </div>
                            
                        </div>
 <hr>

                        <!-- Mobile web Image -->
                        <div class="form-group row form_field__container source-image-div image-caption-div">
                            <label for="mobile_web_image"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Mobile Web Image') }}</label>
                            <div class="col-md-8">
                                <div class="image_cont_main row">
                                    @if(empty($allimages['mobile_web_images'])))
                            
                                <div class="col-md-9">
                                     <div class="col-md-10">
                                    <input id="mobile_web_image" type="file"
                                           class="form-control{{ $errors->has('mobile_web_image') ? ' is-invalid' : '' }}"
                                           value="{{ old('mobile_web_image.0') }}" name="mobile_web_image[]">
                                    @if ($errors->has('mobile_web_image'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('mobile_web_image') }}</strong>
                                        </span>
                                    @endif
                                    <div class="input__field">
                                        <input id="mobile_web_image_caption" type="text"
                                               class="{{ $errors->has('mobile_web_image_caption') ? ' is-invalid' : '' }}"
                                               name="mobile_web_image_caption[]"
                                               value="{{old('mobile_web_image_caption.0') }}"
                                               placeholder="Image Caption">
                                    </div>
                                    @if ($errors->has('mobile_web_image_caption.0'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('mobile_web_image_caption.0') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                      <button type="button" imagetype="mobile_web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>

                                </div>
                            </div>
                            @else
                                <div class="col-md-2">
                                      <button type="button" imagetype="mobile_web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>

                                </div>                            
                            @endif
                            </div>

   @if(isset($allimages['mobile_web_images']) && count($allimages['mobile_web_images'])>0)
                             @foreach($allimages['mobile_web_images'] as $key=>$image)

                                <div class="image_cont_main row">
                                    <input type="hidden" name="mobile_web_image_hidden[]" value="{{$key}}" >
                                     <div class="col-md-3">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['mobile_web_image']}}">
                                            <img class="img-responsive  img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['mobile_web_thumb_sml'] }}"></a>&emsp;
                                      </div>
                                    <div class="col-md-9">
                                         <div class="col-md-10">
                                        <input id="mobile_web_image" type="file"
                                               class="form-control"
                                               value="" name="mobile_web_image[{{$key}}]">
                                        
                                        <div class="input__field">
                                            <input id="mobile_web_image_caption" type="text"
                                                   class=""
                                                   name="mobile_web_image_caption[{{$key}}]"
                                                   value="{{$image['mobile_web_image_caption']}}"
                                                   placeholder="Image Caption">
                                        </div>
                                         </div>
                                         <div class="col-md-2">
                                               <input type="button" value="Delete" class="btn  btn-sm btn__cancel deleteImage" >
                                         </div>

                                  </div>
                                   
                                </div>

                            @endforeach

                            @endif

                            </div>
                            
                        </div>


                        <div id="size_div" style="background-color:   ">
                            @php
                                $sizeCount = 0;
                                if(session()->has('sizeCount'))
                                {
                                    $sizeCount = session()->get('sizeCount');
                                }
                                else if(isset($mealPriceData))
                                {
                                    $sizeCount = count($mealPriceData);
                                }
                            //print_r( $mealPriceData[0]->price[0]);
                            //exit;
                            @endphp
                            @if(isset($sizeCount) && $sizeCount>0)
                                @for($i=0;$i<$sizeCount;$i++)
                                    <div class="size_div" id="size_div_{{$i}}"
                                         style="padding:20px 0px;border: 1px solid #ccc;margin: 10px 0;">
                                        <div class="form-group row form_field__container">
                                            <label for="size"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Size') }}</label>
                                            <div class="col-md-4">
                                                <div class="input__field">
                                                    <input type="text"
                                                           class="size {{ $errors->has('size.'.$i) ? ' is-invalid' : '' }}"
                                                           name="size[]"
                                                           value="{{ (count(old())==0 && isset($mealPriceData[$i]['size'])) ? $mealPriceData[$i]['size'] : old('size.'.$i) }}">
                                                </div>
                                                @if ($errors->has('size.'.$i))
                                                    <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('size.'.$i) }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                            <a class="btn btn__cancel" href="javascript:void(0);" id="remove_div_{{$i}}"
                                               style="{{($i==0)?'display: none':''}}"
                                               onclick="remove_div_by_id({{$i}})">Delete</a>
                                        </div>
                                        <div class="form-group row form_field__container">
                                            <label for="price"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
                                            <div class="row col-md-8">

                                                <div class="row">
                                                    <div class="col-md-12" style="margin:auto;">
                                                        <input type='radio' name='type[{{$i}}]' class='meal_type'
                                                               data-val='{{$i}}'
                                                               value="1" {{old("type.".$i)=="1" || (count(old())==0 && isset($mealPriceData[$i]['price'][0])) ? 'checked="checked"' : ''}} >
                                                        All
                                                    </div>
                                                </div>

                                                <div style="margin-top: 15px" class="row">
                                                    <div class="col-md-12">
                                                        <div class="input__field">
                                                            <input class=" price_all" placeholder='Price'
                                                                   type='text' size='4' id='price_all'
                                                                   name='price_all[{{$i}}]'
                                                                   value="{{(count(old())==0 && isset($mealPriceData[$i]['price'][0])) ? $mealPriceData[$i]['price'][0] : old("price_all.".$i)}}"
                                                            >
                                                        </div>
                                                        @if ($errors->has('price_all.'.$i))
                                                            <span class="invalid-feedback" style="display: block;">
                                                        <strong>{{ $errors->first('price_all.'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div style="margin-top: 15px" class="row">
                                                    <!-- @16-07-2018 By RG-->
                                                    <div class="col-md-6" style="margin:auto;padding-top:auto;">
                                                        <label class="checkbox-inline"><input type="checkbox"
                                                                                              class="delivery_all"
                                                                                              name='delivery_all[{{$i}}]' <?php echo count(old()) == 0 && isset($mealPriceData[$i]['available']['is_delivery']) && $mealPriceData[$i]['available']['is_delivery'] == 1 ? 'checked' : '' ?> >Is
                                                            Delivery</label>
                                                    </div>

                                                    <div class="col-md-6" style="margin:auto;padding-top:auto;">
                                                        <label class="checkbox-inline"><input type="checkbox"
                                                                                              class="carryout_all"
                                                                                              name='carryout_all[{{$i}}]'

                                                            <?php echo count(old()) == 0 && isset($mealPriceData[$i]['available']['is_carryout']) && $mealPriceData[$i]['available']['is_carryout'] == 1 ? 'checked' : '' ?>
                                                            >Is Carryout</label>
                                                    </div>
                                                </div>

                                                <!-- Delivery / carryout Enable or not-->
                                                <div class="row">
                                                    <div class="col-md-8" style="padding:auto;margin-top:10px;">
                                                        <input type='radio' name='type[{{$i}}]' class='meal_type'
                                                               data-val='{{$i}}' value='0'
                                                                {{(old("type.".$i)=="0" || (count(old())==0 && isset($mealPriceData) && !isset($mealPriceData[$i]['price'][0]))) ? 'checked="checked"' : ''}} >&nbsp;By
                                                        Meal Type
                                                    </div>
                                                </div>

                                                @if ($errors->has('type.'.$i))
                                                    <div class="col-md-8">
                                                            <span class="invalid-feedback" style="display: block;">
                                                                <strong>{{ $errors->first('type.'.$i) }}</strong>
                                                            </span>
                                                    </div>
                                                @endif
                                                <div class="row col-md-12"
                                                     style="padding-top: 10px; {{(old("type.".$i)=='0' || (count(old())==0 && isset($mealPriceData) && !isset($mealPriceData[$i]['price'][0]))) ? '' : 'display:none;'}}"
                                                     id="meal_type_{{$i}}">
                                                    {{--@if(session()->has('menuTypeData'))
                                                        @php
                                                            $menuTypeData = session()->get('menuTypeData');
                                                        @endphp--}}
                                                    @foreach($menuTypeData as $mtype)
                                                        <div class="row">
                                                            <div class="row">
                                                                <div class="col-md-12"
                                                                     style="margin:auto;padding-top:auto;">
                                                                    <input type='checkbox'
                                                                           name='meal_type[{{$i}}][{{$mtype->id}}]'
                                                                           value="{{$mtype->id}}" {{(old("meal_type.".$i.'.'.$mtype->id) || ( count(old())==0 && array_key_exists($mtype->id,$mealPriceData[$i]['price'])))? 'checked=checked' : ''}} > {{$mtype->name}}
                                                                    &nbsp;
                                                                </div>
                                                            </div>
                                                            <div style="margin-top: 5px;" class="row">

                                                                <div class="col-md-12">
                                                                    <div class="input__field">
                                                                    <input class=""
                                                                                              placeholder='Price'
                                                                                              type='text'
                                                                                              size='4'
                                                                                              name='price[{{$i}}][{{$mtype->id}}]'
                                                                                              value="{{(count(old())==0 && array_key_exists($mtype->id,$mealPriceData[$i]['price'])) ? $mealPriceData[$i]['price'][$mtype->id] : old("price.".$i.'.'.$mtype->id)}}"
                                                                    >
                                                                    </div>
                                                                    @if ($errors->has('price.'.$i.'.'.$mtype->id))
                                                                        <span class="invalid-feedback"
                                                                              style="display: block;">
                                                            <strong>{{ $errors->first('price.'.$i.'.'.$mtype->id) }}</strong>
                                                            </span>
                                                                    @endif
                                                                </div>
                                                            </div>


                                                        </div>

                                                        <!-- @16-07-2018 By RG-->
                                                        <div style="margin-top: 15px" class="row">
                                                            <div class="col-md-6" style="margin:auto;padding-top:auto;">
                                                                <label class="checkbox-inline"><input type="checkbox"
                                                                                                      name="delivery[{{$i}}][{{$mtype->id}}]"
                                                                    <?php echo count(old()) == 0 && isset($mealPriceData[$i]['available'][$mtype->id]) && $mealPriceData[$i]['available'][$mtype->id]['is_delivery'] == 1 ? 'checked' : '' ?>
                                                                    >Is Delivery</label>
                                                            </div>
                                                            <div class="col-md-6" style="margin:auto;padding-top:auto;">
                                                                <label class="checkbox-inline"><input type="checkbox"
                                                                                                      name="carryout[{{$i}}][{{$mtype->id}}]" <?php echo count(old()) == 0 && isset($mealPriceData[$i]['available'][$mtype->id]) && $mealPriceData[$i]['available'][$mtype->id]['is_carryout'] == 1 ? 'checked' : '' ?> >Is
                                                                    Carryout</label>
                                                            </div>
                                                            <br/>
                                                            <!-- Delivery / carryout Enable or not-->
                                                        </div>
                                                    @endforeach
                                                    {{--@endif--}}
                                                    @if ($errors->has('meal_type.'.$i))
                                                        <span class="invalid-feedback" style="display: block;">
                                                            <strong>{{ $errors->first('meal_type.'.$i) }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                            @endif
                        </div>
                        {{--<div class="form-group row form_field__container">
                            <label id="meal_type_lab" for="Meal Type" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
                            <div class="row col-md-8">
                                <div class="col-md-2" style="margin:auto;">
                                    <input type='radio' name='type' value="1" {{$menuItem->price_flag=="1" ? 'checked="checked"' : ''}} >&nbsp;All
                                </div>
                                <div class="col-md-10">
                                    <input class="form-control" placeholder='Price' type='text' size='4' id='price_all' name='price_all' value="{{isset($mealPriceData[0])?$mealPriceData[0]:''}}" style="width: 25%">
                                    @if ($errors->has('price_all'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('price_all') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-8" style="padding:auto;margin-top:10px;">
                                    <input type='radio' name='type' value="0" {{$menuItem->price_flag=="0" ? 'checked="checked"' : ''}} >&nbsp;By Meal Type
                                </div>
                                @if ($errors->has('type'))
                                    <div class="col-md-8">
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('type') }}</strong>
                                        </span>
                                    </div>
                                @endif
                                <div class="col-md-8 row" style="padding-top: 10px; {{$menuItem->price_flag=="1" || old("type")=="1"?'display:none;':''}}" id="meal_type">
                                    @foreach($menuTypeData as $mtype)

                                        <div class="col-md-4" style="margin:auto;"> <input type='checkbox'  name='meal_type[{{$mtype->id}}]' value="{{$mtype->id}}"  {{array_key_exists($mtype->id,$mealPriceData)? 'checked="checked"' : ''}} >&nbsp&nbsp;{{$mtype->name}}&nbsp;</div>
                                        <div class="col-md-8"><input placeholder='Price' class="form-control" type='text' size='4' name='price[{{$mtype->id}}]' value="{{array_key_exists($mtype->id,$mealPriceData)? $mealPriceData[$mtype->id] : ''}}" style="width: 50%">
                                            @if ($errors->has('price.'.$mtype->id))
                                                <span class="invalid-feedback" style="display: block;">
                                                    <strong>{{ $errors->first('price.'.$mtype->id) }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    @endforeach
                                    @if ($errors->has('meal_type'))
                                        <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('meal_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>--}}

                        <div style="text-align: right;margin-right: 10%;"><input type="button" value="Add"
                                                                                 class="btn btn__primary"
                                                                                 onclick="add_size_div()">&ensp;<input
                                    type="button" value="Delete" class="btn btn__cancel" onclick="remove_size_div()">
                        </div>
                        <br/><br/>
                        @foreach($menuSettingData as $menuSet)
                            @php
                                $menuCustData = $menuItem->customizable_data ?? '';
                                $menuCustIndex = false;
                                if($menuCustData)
                                    $menuCustIndex = array_search($menuSet['id'], array_column($menuCustData, 'id'));
                            @endphp
                            <div class="form-group row form_field__container" style="padding: 20px;">
                                <label for="image"
                                       class="col-md-4 col-form-label text-md-right">{{ __(ucwords($menuSet['name'])) }}</label>
                                <div class="col-md-6">
                                    <table class="table-bordered">
                                        <tr>
                                            <td><input type="checkbox" name="{{ $menuSet['id'].'_' }}is_mandatory"
                                                       value="1" @if($menuCustIndex!==false && $menuCustData[$menuCustIndex]['is_mandatory']){{ 'checked="checked"' }} @endif>{{ __('Is mandatory') }}
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @foreach($menuSet['items'] as $item)
                                            @php
                                                $menuCustItemData = '';
                                                $menuCustItemIndex = false;
                                                if($menuCustIndex !== false) {
                                                    $menuCustItemIndex = array_search($item['id'], array_column($menuCustData[$menuCustIndex]['items'], 'id'));
                                                    if($menuCustItemIndex !== false) {
                                                        $menuCustItemData = $menuCustData[$menuCustIndex]['items'][$menuCustItemIndex];
                                                    }
                                                }
                                            @endphp
                                            <tr>
                                                <td><input type="checkbox" name="{{ $menuSet['name'] }}[]"
                                                           value="{{ $item['id'] }}" @if($menuCustItemIndex!==false && $menuCustItemData['id'] == $item['id']){{ 'checked="checked"' }} @endif>{{ __(ucwords($item['name'])) }}
                                                </td>
                                                <td>
                                                    @foreach($hriSides as $side)
                                                        @php
                                                            $sideIndex = false;
                                                            if($menuCustItemData && isset($menuCustItemData['sides'])) {
                                                                $sideIndex = array_search($side, array_column($menuCustItemData['sides'], 'label'));
                                                            }
                                                        @endphp
                                                        <input type="checkbox" name="{{ $item['id'].'_'.$side }}"
                                                               value="{{ $side }}"
                                                               ch="{{ $sideIndex }}" @if($sideIndex!==false && isset($menuCustItemData['sides']) && $menuCustItemData['sides'][$sideIndex]['is_selected']==1){{ 'checked="checked"' }} @endif>{{ __(ucwords($side)) }}
                                                        <br>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach($hriQuant as $quant)
                                                        @php
                                                            $quantIndex = false;
                                                            if($menuCustItemData && isset($menuCustItemData['quantity'])) {
                                                                $quantIndex = array_search($quant, array_column($menuCustItemData['quantity'], 'label'));
                                                            }
                                                        @endphp
                                                        <input type="checkbox" name="{{ $item['id'].'_'.$quant }}"
                                                               value="{{ $quant }}"
                                                               ch="{{ $quantIndex }}" @if($quantIndex!==false && isset($menuCustItemData['quantity']) && $menuCustItemData['quantity'][$quantIndex]['is_selected']==1){{ 'checked="checked"' }} @endif>{{ __(ucwords($quant)) }}
                                                        <br>
                                                    @endforeach
                                                </td>
                                                <td><input type="checkbox" name="{{ $item['id'].'_' }}default_selected"
                                                           value="1" @if($menuCustItemIndex!==false && $menuCustItemData['default_selected']){{ 'checked="checked"' }} @endif>{{ __('Default Selected') }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        @php
                                            $menuCustIndex = false;
                                        @endphp
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    @if ($errors->has('image'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endforeach

                        <div class="row  mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script type="text/javascript">
      $("#product_type").change(function () {
           var type = $(this).val();
           if(type=='product'){
            $('.product_type_product').removeClass('hide');

           }else{
             $('.product_type_product').addClass('hide');

           }
    });  
        var size_div_cnt = 0;
        size_div_cnt = '<?php echo (isset($sizeCount) && $sizeCount > 0) ? ($sizeCount - 1) : 0 ?>';
        size_div_cnt = parseInt(size_div_cnt);

        function remove_div_by_id(id) {
            var div_cnt = $('.size_div').length;
            if (div_cnt > 1) {
                $('#size_div_' + id).remove();
            }
        }

        function add_size_div() {
            var restId = $('#restaurant_id :selected').val();
            if (restId == '') {
                alert('Please select Restaurant');
                return;
            }
            //var div_html = $('#size_div_0').html();

            var div_id_str = $('.size_div:first').attr('id');
            var div_id = div_id_str.slice(div_id_str.lastIndexOf("_") + 1);
            var div_html = $('#' + div_id_str).html();

            size_div_cnt = size_div_cnt + 1;
            $('#size_div').append("<div id='size_div_" + size_div_cnt + "' class='size_div' style='border: 1px solid #ccc;padding: 10px 0;margin: 10px 0;'></div>");
            var newIndex = '[' + size_div_cnt + ']';
            var newDataVal = "data-val='" + size_div_cnt + "'";
            div_html = div_html.replace(/\[0\]/g, newIndex);
            div_html = div_html.replace(/data-val=\"0\"/g, newDataVal);

            div_html = div_html.replace('meal_type_' + div_id, 'meal_type_' + size_div_cnt);
            div_html = div_html.replace('remove_div_' + div_id, 'remove_div_' + size_div_cnt);
            div_html = div_html.replace('remove_div_by_id(' + div_id + ')', 'remove_div_by_id(' + size_div_cnt + ')');

            //div_html = div_html.replace('meal_type_0', 'meal_type_'+size_div_cnt);
            $('#size_div_' + size_div_cnt).html(div_html);
            $('#size_div_' + size_div_cnt).find('.size').val('');
            $('#size_div_' + size_div_cnt).find('.price_all').val('');
            $('#size_div_' + size_div_cnt).find('.meal_type').prop('checked', false);
            $('#meal_type_' + size_div_cnt).hide();
            $('#remove_div_' + size_div_cnt).show();
        }

        function remove_size_div() {
            /*if (size_div_cnt > 0) {
                $('#size_div_' + size_div_cnt).remove();
                size_div_cnt = size_div_cnt - 1;
            }*/
            var div_cnt = $('.size_div').length;
            if (div_cnt > 1) {
                $('.size_div:last').remove();
            }
        }

        function remove_all_size_div() {
            for (i = 1; i <= size_div_cnt; i++) {
                $('#size_div_' + i).remove();
            }
            $('#meal_type_0').html('');
            $('#size_div_0').find('.price_all').val('');
            $('#size_div_0').find('.meal_type').prop('checked', false);
            $('#meal_type_0').hide();
        }

        $(function () {
            $('body').on('click', '.meal_type', function () {
                var mealTypeVal = $(this).val();
                var idx = $(this).attr('data-val');
                if (mealTypeVal == '1') {
                    $('#meal_type_' + idx).find('input[type=checkbox]:checked').removeAttr('checked');
                    $('#meal_type_' + idx).find('input[type=text]').val('');
                    $('#meal_type_' + idx).hide();
                }
                else {
                    $('#size_div_' + idx).find('.price_all').val('');
                    $('#size_div_' + idx).find('.carryout_all:checked').removeAttr('checked');
                    $('#size_div_' + idx).find('.delivery_all:checked').removeAttr('checked');
                    $('#meal_type_' + idx).show();
                }
            });

            /*$('input[name=type]').click(function(){
                var typeVal = $('input[name=type]:checked').val();
                if(typeVal=='1') {
                    $('#meal_type').find('input[type=checkbox]:checked').removeAttr('checked');
                    $('#meal_type').find('input[type=text]').val('');
                    $('#meal_type').hide();
                }
                else {
                    $('#price_all').val('');
                    $('#meal_type').show();
                }
            });*/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $('option:selected').val();
                if (rest_id != '') {
                    $('#menu_category_id').find('option').not(':first').remove();
                    remove_all_size_div();
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_category_and_type"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj.cat) !== 'undefined') {
                                    $.each(data.dataObj.cat, function (i, item) {
                                        $('#menu_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    });
                                }
                                if ($.type(data.dataObj.type) !== 'undefined') {
                                    var fielsStr = '';
                                    $.each(data.dataObj.type, function (i, item) {
                                        fielsStr = fielsStr + "<div class=\"col-md-4\" style=\"margin:auto;padding-top:auto;\"><input type='checkbox' style=\"margin:auto;\" name='meal_type[0][" + item.id + "]' value='" + item.id + "'>&nbsp;" + item.name + "</div><div class=\"col-md-6\"><div class=\"input__field\"><input placeholder='Price' type='text' size='4' class=\"form-control\" name='price[0][" + item.id + "]' style=\"\"></div></div>";
                                    });
                                    $("#meal_type_0").html(fielsStr);
                                }
                            }
                        }
                    });
                }
            });

            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $('option:selected').val();
                $('#slot_code').find('option').not(':first').remove();
                if (rest_id != '') {
                    remove_all_size_div();
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_availibility"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj.cat) !== 'undefined') {
                                    $.each(data.dataObj.cat, function (i, item) {
                                        $('#slot_code').append('<option value="' + item.slot_code + '">' + item.slot_name + '</option>');
                                    });
                                }

                            }
                        }
                    });
                }
            });

            // change sub-category @21-09-2018 by RG


             $("#menu_category_id").change(function () {
                var rest_id = $('#restaurant_id').val();
                var cat_id = $(this).val();
                console.log(rest_id + ' - ' + cat_id);
                $('#menu_sub_category_id').find('option').not(':first').remove();
                if (rest_id != '' && cat_id != '') {
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_customization_options"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id + '&cid=' + cat_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                               
                                // set sub categories
                                $.each(data.dataObj.sub_categories, function (i, item) {
                                  //  $('#menu_sub_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    $('#menu_sub_category_id').html(data.dataObj.subcat_options);
                                });
                            
                            }
                        }
                    });
                }
            });

            // check uncheck the checkbox
            $("input[type=checkbox]").on("click", function () {
                if ($(this).prop('checked') == true) {
                    $(this).attr('checked', 'checked');
                } else {
                    $(this).removeAttr('checked');
                }
            });

        });



$('.add_new_image').click(function(){
   
    var type=$.trim($(this).attr('imagetype'));
    var filename=type+'[]';
    var lastsib= $(this).parents('.image-caption-div').find(".image_cont_main:last");
    var str='<div class="image_cont_main row"> <div class="col-md-3"></div> <div class="col-md-9"> <div class="col-md-10"> <input id="'+type+'" type="file" class="form-control" name="'+filename+'"> <div class="input__field"> <input id="image_caption" type="text" name="'+type+'_caption[]" placeholder="Image Caption"> </div> </div> <div class="col-md-2"> <input type="button" value="Delete" class="btn btn__cancel deleteImage" > </div> </div> </div>';
     lastsib.after(str);
})
$(document).on('click','.deleteImage',function(){


    $(this).parents('.image_cont_main').remove();
  
})
    </script>
    <script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        //code added by jawed to facilitate FCK editor
        $( document ).ready(function() {
            tinymce.init({
                selector: 'textarea.editor',
                menubar:false,
                height: 320,
                theme: 'modern',
                plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                image_advtab: true,
                templates: [
                    { title: 'Test template 1', content: 'Test 1' },
                    { title: 'Test template 2', content: 'Test 2' }
                ],
            });
        });
    </script>
@endsection

