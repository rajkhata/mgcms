@extends('layouts.app')
@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">
            <div class="card">
                <div class="card-header">{{ __('Restaurant Translation - Edit') }}</div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif
                <div class="card-body">

                    {{ Form::model($restTran, array('route' => array('restaurant_translation.update', $restTran->id), 'method' => 'PUT')) }}

                    @csrf

                    <div class="form-group row">
                        <label for="restaurants_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                        <div class="col-md-4">
                            <select name="restaurants_id" id="restaurants_id" class="form-control{{ $errors->has('restaurants_id') ? ' is-invalid' : '' }}" >
                                <option value="">Select Restaurant</option>
                                @foreach($restaurantData as $rest)
                                    <option value="{{ $rest->id }}" {{ ((old('restaurants_id')==$rest->id) || (count(old())==0 && isset($restTran->restaurants_id) && $restTran->restaurants_id==$rest->id)) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('restaurants_id'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurants_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
                        <div class="col-md-4">
                            <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                                <option value="">Select Language</option>
                                @foreach($languageData as $lang)
                                    <option value="{{ $lang->id }}" {{ ((old('language_id')==$lang->id) || (count(old())==0 && isset($restTran->language_id) && $restTran->language_id==$lang->id)) ? 'selected' :'' }} >{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('language_id'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('language_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name *') }}</label>

                        <div class="col-md-3">
                            <input id="restaurant_name" type="text" class="form-control{{ $errors->has('restaurant_name') ? ' is-invalid' : '' }}"
                                   name="restaurant_name" value="{{ isset($restTran->restaurant_name)? $restTran->restaurant_name : old('restaurant_name')}}" required>

                            @if ($errors->has('restaurant_name'))
                                <span class="invalid-feedback" style="display: block;" >
                                        <strong>{{ $errors->first('restaurant_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description *') }}</label>
                        <div class="col-md-6">
                            <textarea name="description" id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" required>{{ isset($restTran->description)? $restTran->description : old('description')}}</textarea>
                            @if ($errors->has('description'))
                                <span class="invalid-feedback" style="display: block;" >
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address *') }}</label>

                        <div class="col-md-6">
                            <input id="address" type="text"
                                   class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address"
                                   value="{{ isset($restTran->address)? $restTran->address : old('address')  }}" required>

                            @if ($errors->has('address'))
                                <span class="invalid-feedback" style="display: block;" >
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="street" class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>
                        <div class="col-md-2">
                            <input id="street" type="text"
                                   class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" name="street"
                                   value="{{ isset($restTran->street)? $restTran->street : old('street')  }}" required >

                            @if ($errors->has('street'))
                                <span class="invalid-feedback" style="display: block;" >
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-1">&nbsp;</div>
                        <label for="borough" class="col-md-1 col-form-label text-md-right">{{ __('Borough') }}</label>

                        <div class="col-md-2">
                            <input id="borough" type="text"
                                   class="form-control{{ $errors->has('borough') ? ' is-invalid' : '' }}" name="borough"
                                   value="{{ isset($restTran->borough)? $restTran->borough : old('borough')  }}" >

                            @if ($errors->has('borough'))
                                <span class="invalid-feedback" style="display: block;" >
                                        <strong>{{ $errors->first('borough') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="Source url"
                               class="col-md-4 col-form-label text-md-right">{{ __('Source Url') }}</label>

                        <div class="col-md-6">
                            <input id="source_url" type="text"
                                   class="form-control{{ $errors->has('source_url') ? ' is-invalid' : '' }}" name="source_url"
                                   value="{{ isset($restTran->source_url)? $restTran->source_url : old('source_url') }}" >

                            @if ($errors->has('source_url'))
                                <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('source_url') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="delivery_desc" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Description *') }}</label>
                        <div class="col-md-6">
                            <textarea name="delivery_desc" id="delivery_desc" class="form-control{{ $errors->has('delivery_desc') ? ' is-invalid' : '' }}" >{{ isset($restTran->delivery_desc)? $restTran->delivery_desc : old('delivery_desc') }}</textarea>
                            @if ($errors->has('delivery_desc'))
                                <span class="invalid-feedback" style="display: block;" >
                                        <strong>{{ $errors->first('delivery_desc') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="notable_chef_desc" class="col-md-4 col-form-label text-md-right">{{ __('Notable Chef Description *') }}</label>
                        <div class="col-md-6">
                            <textarea name="notable_chef_desc" id="notable_chef_desc" class="form-control{{ $errors->has('notable_chef_desc') ? ' is-invalid' : '' }}" >{{ isset($restTran->notable_chef_desc)? $restTran->notable_chef_desc : old('notable_chef_desc')}}</textarea>
                            @if ($errors->has('notable_chef_desc'))
                                <span class="invalid-feedback" style="display: block;" >
                                        <strong>{{ $errors->first('notable_chef_desc') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="notable_chef_desc" class="col-md-4 col-form-label text-md-right">{{ __('Parking Description *') }}</label>
                        <div class="col-md-6">
                            <textarea name="parking_desc" id="parking_desc" class="form-control{{ $errors->has('parking_desc') ? ' is-invalid' : '' }}" >{{ isset($restTran->parking_desc)? $restTran->parking_desc : old('parking_desc') }}</textarea>
                            @if ($errors->has('parking_desc'))
                                <span class="invalid-feedback" style="display: block;" >
                                        <strong>{{ $errors->first('parking_desc') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <br /><br />
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">{{ __('Edit') }}</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var restPageData = '';
        function getRestaurantMenus(sel) {
            var id = 'restaurant/'+sel.value;
            var ajaxurl = id;
            $("#page_heading").val('');
            $("#page_sub_heading").val('');
            $("#page_content").val('');
            $("#priority").val('');
            $("#image").val('');
            $.ajax({
                url: ajaxurl,
                type: "GET",
                success: function(data){
                    console.log(data);
                    restPageData = data;
                    var optionHtml = '<option value="">Select page type</option>';
                    data.forEach(function(dt) {
                        optionHtml += '<option value="' + dt.page_type + '">' + dt.page_type + '</option>';
                    });
                    console.log(optionHtml);
                    $("#page_type").empty().append(optionHtml);
                }
            });
        }
        function getPageData(sel) {
            console.log(restPageData);
            restPageData.forEach(function(dt) {
                breakOut = false;
                if(dt.page_type == sel.value) {
                    console.log('ma' + dt.page_type + ' - ' + sel.value);
                    $("#page_heading").val(dt.page_heading);
                    $("#page_sub_heading").val(dt.page_sub_heading);
                    $("#page_content").val(dt.page_content);
                    $("#priority").val(dt.priority);
                    breakOut = true;
                } else {
                    console.log('nm' + dt.page_type + ' - ' + sel.value);
                }
                if(breakOut) return false;
            });
        }
        function addFileDiv(sel) {
            var type = $(sel).hasClass('image-caption-button') ? 'image' : 'video';

            var divHtml = $('.source-'+type+'-div').html(); // $('.image-caption-div')[0].outerHTML;
            $('.'+type+'-caption-div').last().after('<div class="form-group row '+type+'-caption-div">'+divHtml+'</div>');
        }
        function removeFileDiv(sel) {
            var type = $(sel).hasClass('image-caption-button') ? 'image' : 'video';

            $('.'+type+'-caption-div').last().remove();
        }
    </script>

@endsection
