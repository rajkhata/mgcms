@extends('layouts.app')

@section('content')
    <div class="container" style="max-width:100%;">
        <div class="justify-content-center" style="width:80%;float:left;margin-left:10px;">

            <div class="card">
                <div class="card-header" style="padding: 8px;">
                    Restaurant Translation
                    <span style="float: right;margin:0px 5px;">({{ $restTrans->currentPage() .'/' . $restTrans->lastPage() }})</span>&emsp;
                    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('restaurant_translation/create') }}" class="btn btn-primary">Add</a></span>
                    <!--<span style="float: right;">({{ $restTrans->count() .'/' . $restTrans->total() }})</span>-->
                </div>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @elseif (session()->has('err_msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('err_msg') }}
                    </div>
                @endif

                {!! Form::open(['method'=>'get']) !!}
                <div class="row" style="margin:10px;justify-content: center;">
                    <div class="row form-group col-md-4">&nbsp;</div>
                    <div class="row form-group col-md-3">
                        <select name="restaurants_id" id="restaurants_id" class="form-control" >
                            <option value="">Select Restaurant</option>
                            @foreach($restaurantData as $rest)
                                <option value="{{ $rest->id }}" {{ (isset($restaurant_id) && $restaurant_id==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row form-group col-md-5">
                        <button class="btn btn-info" style="margin-left: 5px;" type="submit" >Search</button>
                    </div>
                </div>
                {!! Form::close() !!}

                <div class="card-body" style="padding-top:5px;">
                    <div class="row panel-heading" style="background-color: rgba(0,0,0,.03);padding:8px 0px;margin-bottom: 5px;">
                        <div class="col-md-3" style="font-weight: bold;">Restaurant</div>
                        <div class="col-md-3" style="font-weight: bold;">Language</div>
                        <div class="col-md-4" style="font-weight: bold;">Name</div>
                        <div class="col-md-2" style="font-weight: bold;">Action</div>
                    </div>
                    @if(isset($restTrans) && count($restTrans)>0)
                        @foreach($restTrans as $restTran)
                            <div class="row" style="padding-bottom: 6px;">
                                <div class="col-md-3" style="margin: auto 0px;">{{ $restTran->Restaurant->restaurant_name}}</div>
                                <div class="col-md-3" style="margin: auto 0px;">{{ $restTran->Languages->language_name }}</div>
                                <div class="col-md-4" style="margin: auto 0px;">{{ $restTran->restaurant_name }}</div>

                                <div class="row col-md-2">
                                    <div class="col-md-5">
                                        <a href="{{ URL::to('restaurant_translation/' . $restTran->id . '/edit') }}" class="btn btn-info">Edit</a>
                                    </div>
                                    <div class="col-md-7">
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['restaurant_translation.destroy', $restTran->id]]) !!}
                                        @csrf
                                        <button class="btn btn-default" type="submit" onclick="return confirm('Do you really want to Delete ?')">Delete</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                            No Record Found
                        </div>
                    @endif
                </div>
                @if(isset($restTrans) && count($restTrans)>0)
                    <div style="margin: 0px auto;">
                        {{ $restTrans->appends($_GET)->links()}}
                    </div>
                @endif
            </div>

        </div>
    </div>
@endsection