@extends('layouts.app')
@section('content')
    <style>
	#result {
        border: 1px solid #888;
        background: #f7f7f7;
        padding: 1em;
        margin-bottom: 1em;
    }
    </style>  
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
    <link href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css" rel='stylesheet' />
    <script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
	<script src="https://cdn.datatables.net/rowgroup/1.1.1/js/dataTables.rowGroup.min.js"></script>
    <div class="main__container deal-discounts-page container__custom_working new-design-labels prl20">
	 
	 
	<div id="cms-accordion" class="row">
		<div class="reservation__atto">
	    	<div class="reservation__title">
	      		<h1 style="uppercase">Attribute Set</h1>
	    	</div>
	  	</div>
	  
		<div id="cms-accordion-msg">
		@if(session()->has('message'))
			<div class="alert alert-success margin-top-20">
				{{ session()->get('message') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endif
		</div>
		<div class="guestbook__container fullWidth box-shadow margin-top-0 relative">
			<table id="table_id1" class="responsive ui cell-border hover" style="width:100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th>Action</th>	
					</tr>
				</thead>
				<tbody id="links-list" name="links-list">
				@if(count($menuAttributeSet))
				@foreach($menuAttributeSet as $menu)
					<tr id="link{{$menu->id}}">
						<td>{{ isset($menu->id)?($menu->id):'' }}</td>
						<td>{{ isset($menu->name)?$menu->name:'' }}</td>
						<td>{{ ($menu->status==0) ? 'InActive' :'Active' }}</td>
						<td>
							<a class="btn btn__primary open-modal" href="/v2/systemattributeset/edit/{{$menu->id}}">Edit </a>
							<button class="hide btn btn-danger delete-link" value="{{$menu->id}}">Delete </button>
						</td>
					</tr>
				@endforeach
				@else
					<tr id="link0">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				@endif
				</tbody>
			</table>
    	</div>    
	</div>

<script type="text/javascript">
function validateAttributeForm(formData){
	//console.log(formData);
	var error = [0, "", ""];
	if(formData.attribute_item_name==""){
	   error = [1, "#attribute_item_name_error", "Name required"];
	}
	return error; 
}
function validateImageWeb(fileName){
	var error = [0, "", ""];
	if(fileName){
		var imgname  =  $('input[type=attribute_item_image]').val();
		var size  =  $('#attribute_item_image')[0].files[0].size;
		var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
		if(ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='PNG' || ext=='JPG' || ext=='JPEG') {
		    if(size<=1000000) { 
			return error; 
		    }else{
			 error = [2, "#attribute_item_image_error", 'Sorry File size exceeding from 1 Mb']; 
		    }
		}else error = [2, "#attribute_item_image_error", 'File must be jpg,png']; 
	}
	return error; 
}
 
 
function sortRow(arr){
	var formData = { data: arr  }; 
	$.post( '/v2/systemattribute/item/sort/1', formData );
	 
	      
}
$(document).ready( function () {
	
    		var table1  =  $('#table_id1').DataTable({
		        @if(count($menuAttributeSet))
				"order": [[ 1, "asc" ]],
			@endif
                 
			rowReorder: true,
			responsive: true,
			dom:'<"top"fiB><"bottom"trlp><"clear">',
			buttons: [
			{
				text: 'Create Set',
				action: function ( e, dt, node, config ) {
				window.location ='/v2/systemattributeset/create';
				}
			},
			{
				extend: 'colvis',
				text: '<i class="fa fa-eye"></i>',
				titleAttr: 'Column Visibility'
			},
			{
				extend: 'excelHtml5',
				text: '<i class="fa fa-file-excel-o"></i>',
				titleAttr: 'Excel',
				exportOptions: { columns: ':visible' }
			},
			{
				extend: 'pdfHtml5',
				text: '<i class="fa fa-file-pdf-o"></i>',
				titleAttr: 'PDF',
				exportOptions: { columns: ':visible' }
			},
			{
				extend: 'print',
				text: '<i class="fa fa-print"></i>',
				titleAttr: 'Print',
				exportOptions: {
					columns: ':visible',
				},
			}
			],
			columnDefs: [
				{ targets:[0,1,2,3], className: "desktop" },
				{ targets:[0,1,2,3], className: "tablet" },
				{ targets:[1,2,3], className: "mobile" }
			],
			language: {
				search: "",
				searchPlaceholder: "Search"
			}
	    });
	
	    table1.on( 'row-reorder', function ( e, diff, edit ) {
		var result = 'Reorder started on row: '+edit.triggerRow.data()[1]+'<br>';
	 	
		var newData = [];
		for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
		   newData.push([diff[i].oldData,diff[i].newData]);	
		   /* var rowData = table1.row( diff[i].node ).data();
	 	   	
		    result += rowData[1]+' updated to be in position '+
		        diff[i].newData+' (was '+diff[i].oldData+')<br>';*/
		}
		//console.log(newData);
		sortRow(newData);
	 	//console.log(result);
	       // $('#result').html( 'Event result:<br>'+result );
	    } );
   
    $("#btn-save").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
	$('#cms-accordion-msg-popup').html('');
	$('.imgclss').html('');
	$('.imgclss').addClass('hide');		
        var formData = {
            attribute_item_name: $('#attribute_item_name').val(),
            attribute_item_parent_id: $('#attribute_item_parent_id').val(),
            status: $("input[name='status']:checked").val(),
	    attribute_item_description: $('#attribute_item_description').val(),
 	    attribute_item_image_class: $('#attribute_item_image_class').val(),
	    dependent_attribute_code: $('#dependent_attribute_code').val(),
	    dependent_attribute_item_id: $('#dependent_attribute_item_id').val(),
	    sort_order: $('#sort_order').val(),
	    attribute_id:$('#attribute_id').val(), 	
        };
	var fileName = $("#attribute_item_image").val();
	var formerror = validateAttributeForm(formData);
 
	//var formerror1 = validateImageWeb(fileName);
	if(formerror[0]==1){ 
		console.log(formerror);
		$('.error').addClass('hide');
		$(formerror[1]).removeClass('hide');
		$(formerror[1]+'1').text(formerror[2]);
		
		
	}else{
		
		$('.error').addClass('hide');
		//return false;
		var state = $('#btn-save').val();
		var type = "POST";
		var id = $('#id').val();
		var ajaxurl = '/v2/systemattribute/item/add';
		if (state == "update") {
		    //type = "PUT";
		    ajaxurl = '/v2/systemattribute/item/update/' + id;
		}
		data = new FormData();
		
	    	if(fileName)data.append('attribute_item_image', $('#attribute_item_image')[0].files[0]);
		data.append('attribute_item_parent_id', $('#attribute_item_parent_id').val());
		data.append('attribute_item_name', $('#attribute_item_name').val());
		data.append('attribute_item_description', $('#attribute_item_description').val());
		data.append('attribute_item_image_class', $('#attribute_item_image_class').val());
		data.append('attribute_item_image', $('#attribute_item_image').val());
		//data.append('dependent_attribute_code', $('#dependent_attribute_code').val());
		//data.append('dependent_attribute_item_id', $('#dependent_attribute_item_id').val());
		data.append('sort_order', $('#sort_order').val());
		data.append('status',  $("input[name='status']:checked").val());
		 
		data.append('is_default',  $("input[name='is_default']:checked").val());
	    	data.append('attribute_id', $('#attribute_id').val()); 
		 data.append('attribute_item_code', $('#attribute_item_code').val()); 
			
			$.ajax({
			    type: type,
			    url: ajaxurl,
			    data: data,
			    dataType: 'json',
			    enctype: 'multipart/form-data',
      			    processData: false,  // tell jQuery not to process the data
      			    contentType: false,   // tell jQuery not to set contentType
			    success: function (data1) {  
				if(data1.error==0){
					//table1.ajax.reload();
					var data = data1.data;
					var status='InActive';
					if(data.status==1)
					  status='Active';
					var parent = '';
					  if(data.parent_attribute_value!="")parent = data.parent_attribute_value + " --> ";
					var rrow = '<tr id="link' + data.id + '">'+ '<td>' + data.id + '</td><td>' + data.attribute_item_parent_id + '</td><td>' + data.attribute_label+ '</td> <td>' +parent + data.attribute_item_name + '</td><td>' + data.is_default + '</td><td>' +  data.sort_order + '</td><td>' + status + '</td> ';
					rrow += '<td><button class="btn btn-info open-child-modal" value="' + data.id + '" onclick="createAttributeValue(' + data.id + ')">Add Child </button>&nbsp;';
					rrow += '<button class="btn btn-info open-modal" value="' + data.id + '">Edit</button>&nbsp;';
					rrow += '<button class="btn btn-danger delete-link" value="' + data.id + '">Delete</button></td>';
					rrow += '</tr>';
					if (state == "add") {
					    $('#links-list').append(rrow);
					    showMessageOnPopUp('Modifier Item added successfully!' , 1)	
					} else {
					    $("#link" + data.id).replaceWith(rrow);
					    showMessageOnPopUp('Modifier Item updated successfully!' , 1)	
					}
					setTimeout(function(){ 
						$('#modalFormData').trigger("reset");
						$('#linkEditorModal').modal('hide');
						$('#cms-accordion-msg').html('');	
						$('#cms-accordion-msg-popup').html('');	
						@if(count($menuAttributeSet)==0)
							location.reload();
						@endif 
					}, 2000);
					
				}
			    },
			    error: function (data) {
				//console.log('Error:', data);
				showMessageOnPopUp('Please try again!' , 0);
				//$('#linkEditorModal').modal('hide');	
			    }
			});
		
	}
    });
     $('body').on('click', '.open-modal', function () {
        var link_id = $(this).val();
        $.get('/v2/systemattribute/item/info/' + link_id, function (data1) {
	   if(data1.error==0){
		    var data = data1.data;
		    $('#id').val(data.id);
		    //$('#file').val(data.attribute_value_image);
		    $('#attribute_id').val(data.attribute_id);
		    $('#sort_order').val(data.sort_order);	
		    $('#attribute_item_name').val(data.attribute_item_name);
		    $('#attribute_item_description').val(data.attribute_item_description); 
		    $('#attribute_item_image_class').val(data.attribute_item_image_class); 
		   // $('#dependent_attribute_code').val(data.dependent_attribute_code);	
		    //$('#dependent_attribute_item_id').val(data.dependent_attribute_item_id);
		    $('#attribute_item_code').val(data.attribute_item_code);
		    if(data.attribute_item_image!=''){
				var imgUrl = '<a href="'+data.attribute_item_image+'" target="_blank">Click here!</a> ';		
		    		$('.imgclss').html(imgUrl);	
				$('.imgclss').removeClass('hide');	
		    }	 	
		    if(data.status==0)$('#instatus').attr('checked', true);
		    else $('#status').attr('checked', true);
		     if(data.use_in_promo=='No')$('#is_default').attr('checked', true);
		    else $('#is_default').attr('checked', true);	

		    if(data.use_in_promo=='No')$('#is_required').attr('checked', true);
		    else $('#is_required').attr('checked', true);	

		    $('select[name^="attribute_item_parent_id"] option:selected').attr("selected",null);
		    $('select[name^="attribute_item_parent_id"] option[value="'+data.attribute_item_parent_id+'"]').attr("selected","selected");	
		    $('#btn-save').val("update");//img
		    $('#linkEditorModal').modal('show');
	  }
        })
    });	
    ////----- DELETE a link and remove from the page -----////
    $('.delete-link').click(function () {
        var link_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "DELETE",
            url: '/v2/systemattribute/item/delete/' + link_id,
            success: function (data) {
                console.log(data);
                $("#link" + link_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
     $('#attribute_item_name').keyup(function(){
	    var name = $('#attribute_item_name').val();
	    var skuName = name;
	     
	     var skuName = skuName.replace(/"/g, " Inch");
	     var skuName = skuName.toLowerCase();
	     var skuName = skuName.replace(/ /g, "_");
	     
	    	$('#attribute_item_code').val(skuName);    
	    
	});
	$('#attribute_item_name').focusout(function(){
	    var name = $('#attribute_item_name').val();
	    var skuName = name;
	     
	     var skuName = skuName.replace(/"/g, " Inch");
	     var skuName = skuName.toLowerCase();
	     var skuName = skuName.replace(/ /g, "_");
	     
	    	$('#attribute_item_code').val(skuName);    
	    
	});  	
});
function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	$('#cms-accordion-msg').html(msg);	
         
}
function showMessageOnPopUp(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	$('#cms-accordion-msg-popup').html(msg);	
         
}
</script>

<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
        tinymce.init({
            selector: 'textarea.editor',
            menubar: "tools",
            height: 320,
            theme: 'modern',
            plugins: 'code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar: 'code | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });
    });
</script> 
@endsection
