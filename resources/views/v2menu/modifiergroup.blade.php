@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
   <link href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css" rel='stylesheet' />
<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>	
    <div class="main__container deal-discounts-page container__custom_working new-design-labels prl20">
	<div id="cms-accordion" class="row margin-top-30">
	<div id="cms-accordion-msg">
	@if(session()->has('message'))
                <div class="alert alert-success margin-top-20">
                    {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
	
        @endif
	</div>	
        <table id="table_id1" class="ui celled cell-border hover display compact nowrap responsive" style="width:100%">
        <thead>
          <tr>
           <th>Id</th>
	   <th>Type</th>
	   <th>Field Type</th>
           <th>Product Type</th>
	   <th>Attribute Prompt</th>	
           <th>Sort Order</th>
	   
	   <th>Status</th>		
	   <th>List or Edit</th>
         </tr>
        </thead>
        <tbody id="links-list" name="links-list">
        @if(count($menuAttributes))
        @foreach($menuAttributes as $menu)
        <tr id="link{{$menu->id}}">
		<td>{{ isset($menu->id)?$menu->id:'' }}</td>
		<td>{{ ($menu->type==0) ? 'System' :'Product' }}</td>
		<td>{{ isset($menu->system_type_field) ?$menu->system_type_field:'--' }}</td>  	
		<td>{{ isset($menu->name)?$menu->name:'--' }}</td>	
		<td>{{ isset($menu->attribute_prompt)?$menu->attribute_prompt:'' }}</td>
		<td>{{ isset($menu->sort_order)?$menu->sort_order:'' }}</td>
		<td>{{ ($menu->status==0) ? 'InActive' :'Active' }}</td>
		<td>  <a href="{{ URL::to('/v2/modifiergroup/item/' . $menu->attribute_code) }}" >
			<button class="btn btn-info  " value="{{$menu->id}}">List </button></a>
		        <button class="btn btn-info open-modal" value="{{$menu->id}}">Edit </button>
                        <button class="btn btn-danger delete-link hide" value="{{$menu->id}}">Delete </button>
                    
                </td>
        </tr>
        @endforeach
        @else
          <tr><td colspan="11">No Group found</td></tr>
        @endif
        </tbody>
        </table>
    </div>    
</div>
<div class="modal fade" id="linkEditorModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="linkEditorModalLabel">Modifier Group</h4>
                        </div>
                        <div class="modal-body">
			    <div id="cms-accordion-msg-popup"></div>
                            <form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate="">
 				<div class="form-group" >
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Language</label>
                                    <div class="col-sm-6">
                                         <select name="language_id" id="language_id" >
					  <option value="">Select</option>
					  @foreach($language as $l)	
					  <option value="{{$l->id}}">{{$l->language_name}}</option>
					  @endforeach 
					</select> 
                                    </div>
				    <div class="col-sm-3 invalid-feedback hide error" id="language_id_error">
                                         <b id="language_id_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group" >
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Type Of Attribute</label>
                                    <div class="col-sm-6">
                                         <select name="type" id="type" >
					  <option value="">Select</option>
					   <option value="0">System</option>
					  <option value="1" selected>Product</option>
					</select> 
                                    </div>
				    <div class="col-sm-3 invalid-feedback hide error" id="type_error">
                                         <b id="type_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group" >
                                    <label class="col-sm-3 control-label" style="text-align:left!important">System Type Field</label>
                                    <div class="col-sm-6">
                                         <select name="system_type_field" id="system_type_field" >
					  <option value="">Select</option>
					    <option value="text">Text</option>
					    <option value="textarea">Text Area</option>
					    <option value="checkbox">Checkbox</option>
					    <option value="radio">Radio</option>	
					    <option value="date">Date</option>
					    <option value="email">Email</option>
					    <option value="file">File</option>
					    <option value="hidden">hidden</option>
					    <option value="image">Image</option>
					    <option value="url">Url</option>
					</select> 
                                    </div>
				    <div class="col-sm-3 invalid-feedback hide error" id="system_type_field_error">
                                         <b id="system_type_field_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Attribute Filed Options</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="system_type_field_option" name="system_type_field_option"
                                               placeholder="Enter Options" value="">
                                    </div>
				    <div class="col-sm-3 invalid-feedback hide error" id="system_type_field_option_error">
                                         <b id="system_type_field_option_error1" class="error1"></b>
                                    </div>	
                                </div>
				<div class="form-group" >
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Product Type</label>
                                    <div class="col-sm-6">
                                         <select name="product_type" id="product_type" >
					  <option value="">Select</option>
					  @foreach($product_type as $l)	
					  <option value="{{$l->id}}">{{$l->name}}</option>
					  @endforeach 
					</select> 
                                    </div>
				    <div class="col-sm-3 invalid-feedback hide error" id="product_type_error">
                                         <b id="product_type_error1" class="error1"></b>
                                    </div>
                                </div>
				
                                <div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Label</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="attribute_label" name="attribute_label"
                                               placeholder="Enter Label" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_label_error">
                                         <b id="attribute_label_error1" class="error1"></b>
                                    </div>
                                </div>
 
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Prompt</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="attribute_prompt" name="attribute_prompt"
                                               placeholder="Enter Prompt" value="">
                                    </div>
				    <div class="col-sm-3 invalid-feedback hide error" id="attribute_prompt_error">
                                         <b id="attribute_prompt_error1" class="error1"></b>
                                    </div>	
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Sort Order</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="sort_order" name="sort_order"
                                               placeholder="Enter Sort Order" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Is Configurable</label>
                                    <div class="col-sm-9">
                                        <input type="radio" name="is_configuration" id="is_configuration" value="1" checked> Yes &nbsp; &nbsp;<input type="radio" id="is_configuration_no" name="is_configuration" value="0"> No
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Status</label>
                                    <div class="col-sm-9">
                                        <input type="radio" name="status" id="status" value="1" checked> Active &nbsp; &nbsp; <input type="radio" id="status_no" name="status" value="0"> InActive
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Is Searchable</label>
                                    <div class="col-sm-9">
                                        <input type="radio" name="use_in_search" id="use_in_search" value="1" checked> Yes &nbsp; &nbsp; <input type="radio" name="use_in_search" id="use_in_search_no"  value="0"> No
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Is Filterable</label>
                                    <div class="col-sm-9">
                                        <input type="radio" name="use_as_filter" id="use_as_filter" value="1" checked> Yes &nbsp; &nbsp;<input type="radio" id="use_as_filter_no" name="use_as_filter" value="0"> No
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Is Promo</label>
                                    <div class="col-sm-9">
                                        <input type="radio" name="use_in_promo" id="use_in_promo" value="1" checked> Yes &nbsp; &nbsp; <input type="radio" id="use_in_promo_no" name="use_in_promo" value="0"> No
                                    </div>
                                </div>
				
				
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                            <input type="hidden" id="id" name="id" value="0">
                        </div>
                    </div>
                </div>
            </div>
<script type="text/javascript">
function validateAttributeForm(formData){
	//console.log(formData);
	var error = [0, "", ""];
	if(formData.language_id==""){
	   error = [1, "#language_id_error", "Language required"];
	}else if(formData.product_type=="" && formData.type==1){
	   error = [1, "#product_type_error", "Product Type required"];
	}else if(formData.attribute_label==""){
	   error = [1, "#attribute_label_error", "Label required"];
	}
	else if(formData.attribute_prompt==""){
	   error = [1, "#attribute_prompt_error", "Prompt required"];
	}
	
	
 	return error; 
}
function sortRow(arr){
	var formData = { data: arr  }; 
	$.post( '/v2/modifiergroup/sort/1', formData );
	 
	      
}
$(document).ready( function () {
    var table1  =  $('#table_id1').DataTable({
		"order": [[ 3, "asc" ]],
		rowReorder: true,
                responsive: true,
                dom:'lBfrtip',
                buttons: [
		    {
                text: 'Add Group',
                action: function ( e, dt, node, config ) {
		    jQuery('#btn-save').val("add");
		    jQuery('#btn-save').text("Add");		
                    jQuery('#linkEditorModal').modal('show');
		    jQuery('#cms-accordion-msg-popup').html('');		
                }
            },
                    {extend: 'excelHtml5'}, 
                    {extend: 'pdfHtml5'},   
                    {               
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ],
                columnDefs: [ {
                    targets: -1,
                    visible: true
                }],
                language: {
                    search: "",
                    searchPlaceholder: "Search"
                }
	   });
    table1.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[1]+'<br>';
 	
        var newData = [];
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
	   newData.push([diff[i].oldData,diff[i].newData]);	
           /* var rowData = table1.row( diff[i].node ).data();
 	   	
            result += rowData[1]+' updated to be in position '+
                diff[i].newData+' (was '+diff[i].oldData+')<br>';*/
        }
	//console.log(newData);
	sortRow(newData);
 	//console.log(result);
       // $('#result').html( 'Event result:<br>'+result );
    } );
    $("#btn-save").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
        var formData = {
            attribute_label: jQuery('#attribute_label').val(),
            attribute_prompt: jQuery('#attribute_prompt').val(),
	    is_configuration: $("input[name='is_configuration']:checked").val(),
            status: $("input[name='status']:checked").val(),
 	    use_as_filter:$("input[name='use_as_filter']:checked").val(),
            use_in_search: $("input[name='use_in_search']:checked").val(),
            use_in_promo: $("input[name='use_in_promo']:checked").val(),
	    product_type: jQuery('#product_type').val(),
            language_id: jQuery('#language_id').val(),
	    type: jQuery('#type').val(),
	    system_type_field:	jQuery('#system_type_field').val(),		
        };
	var formerror = validateAttributeForm(formData);
	if(formerror[0]==1){ console.log(formerror);
		jQuery('.error').addClass('hide');
		jQuery(formerror[1]).removeClass('hide');
		jQuery(formerror[1]+'1').text(formerror[2]);
		
	}else{
		jQuery('.error').addClass('hide');
		//return false;
		var state = jQuery('#btn-save').val();
		var type = "POST";
		var id = jQuery('#id').val();
		var ajaxurl = '/v2/modifiergroup/add';
		if (state == "update") {
		    type = "POST";
		    ajaxurl = '/v2/modifiergroup/edit/' + id;
		}
		$.ajax({
		    type: type,
		    url: ajaxurl,
		    data: formData,
		    dataType: 'json',
		    success: function (data1) { console.log(data1);
			if(data1.error==0){
				var data = data1.data;
				var status='InActive';
				if(data.status==1)
				  status='Active';
				var type='Product';
				var system_type_field = '--';
				var productType = '--';
				if(data.type==0){
				  type='System';
				  system_type_field = data.system_type_field;
				}else{
					productType =data.name;
				}
				var rrow = '<tr id="link' + data.id + '">'+ '<td>' + data.id + '</td><td>' + type + '</td><td>' + system_type_field + '</td><td>' + productType+ '</td><td>' + data.attribute_prompt + '</td><td>'+ data.sort_order + '</td><td>' + status + '</td>';
				rrow += '<td><a href="{{ URL::to('v2/attribute/value/' . $menu->attribute_code) }}" ><button class="btn btn-info  " value="' + data.id + '">List</button></a>';
				rrow += ' <button class="btn btn-info open-modal" value="' + data.id + '">Edit</button>&nbsp;';
				//rrow += '<button class="btn btn-danger delete-link" value="' + data.id + '">Delete</button></td>';
				rrow += '</tr>';
				if (state == "add") {
				    jQuery('#links-list').append(rrow);
				    showMessageOnPopUp('Modifier Group added successfully!' , 1)	
				} else {
				    $("#link" + id).replaceWith(rrow);
				    showMessageOnPopUp('Modifier Group updated successfully!' , 1)	
				}
				setTimeout(function(){ jQuery('#modalFormData').trigger("reset");
					jQuery('#linkEditorModal').modal('hide'); }, 2000);
			}
		    },
		    error: function (data) {
		       // console.log('Error:', data);
			showMessageOnPopUp('Please try again!' , 0);
			//jQuery('#linkEditorModal').modal('hide');
		    }
		});
	}
    });
    jQuery('body').on('click', '.open-modal', function () {
        var id = $(this).val();
        $.get('/v2/modifiergroup/list/' + id, function (data1) {
	   if(data1.error==0){
		    var data = data1.data;
		    jQuery('#id').val(data.id);
		    $('select[name^="language_id"] option:selected').attr("selected",null);
		    $('select[name^="language_id"] option[value="'+data.language_id+'"]').attr("selected","selected");
		    $('select[name^="product_type"] option:selected').attr("selected",null);
		    $('select[name^="product_type"] option[value="'+data.product_type+'"]').attr("selected","selected");
		    //jQuery('#file').val(data.attribute_value_image);
		    $('select[name^="type"] option:selected').attr("selected",null);
		    $('select[name^="type"] option[value="'+data.type+'"]').attr("selected","selected");
		    $('select[name^="system_type_field"] option:selected').attr("selected",null);
		    $('select[name^="system_type_field"] option[value="'+data.system_type_field+'"]').attr("selected","selected");
		    jQuery('#attribute_label').val(data.attribute_label);
		    jQuery('#sort_order').val(data.sort_order);
		    jQuery('#attribute_prompt').val(data.attribute_prompt);	
		    if(data.is_configuration==0)jQuery('#is_configuration_no').attr('checked', true);
		    else jQuery('#is_configuration').attr('checked', true);
		    if(data.status==0)jQuery('#status_no').attr('checked', true);
		    else jQuery('#status').attr('checked', true);
		    if(data.use_in_search==0)jQuery('#use_in_search_no').attr('checked', true);
		    else jQuery('#use_in_search').attr('checked', true);
		    if(data.use_as_filter==0)jQuery('#use_as_filter_no').attr('checked', true);
		    else jQuery('#use_as_filter').attr('checked', true);
		    if(data.use_in_promo==0)jQuery('#use_in_promo_no').attr('checked', true);
		    else jQuery('#use_in_promo').attr('checked', true);			
		    jQuery('#btn-save').val("update");//img
		    jQuery('#linkEditorModal').modal('show');
	  }
        })
    });	 	
});
function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg').html(msg);	
         
}
function showMessageOnPopUp(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg-popup').html(msg);	
         
}
</script>
@endsection
