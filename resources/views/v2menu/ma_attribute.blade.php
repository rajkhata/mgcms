@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
    <link href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css" rel='stylesheet' />
	<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>	
	
    <div class="main__container deal-discounts-page container__custom_working new-design-labels prl20">
		<div class="reservation__atto">
			<div class="reservation__title">
				<h1 style="uppercase">Attribute</h1>
			</div>
		</div>
		<div id="cms-accordion" class="guestbook__container fullWidth box-shadow margin-top-0 relative">
			
			<div id="cms-accordion-msg">
			@if(session()->has('message'))
				<div class="alert alert-success margin-top-20">
					{{ session()->get('message') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
			</div>

        	<table id="table_id1" class="responsive ui cell-border hover" style="width:100%">
        		<thead>
          			<tr>
						<th>Id</th>
						<th>Attribute Code</th>
						<th>Attribute Type</th>
						<th>Attribute Label</th>
						<th>Attribute Prompt</th>
						<th>Is Customizable</th>
						<th>Sort Order</th>	
						<th>Status</th>		
						<th>Actions</th>
					</tr>
        		</thead>
				<tbody id="links-list" name="links-list">
				@if(count($menuAttributes))
				@foreach($menuAttributes as $menu)
					<tr id="link{{$menu->id}}">
						<td>{{ isset($menu->id)?$menu->id:'' }}</td>
						<td>{{ isset($menu->attribute_code)?$menu->attribute_code:'' }}</td>
						<td>{{ isset($menu->attribute_type) ?$menu->attribute_type:'--' }}</td>  	
						<td>{{ isset($menu->attribute_label)?$menu->attribute_label:'' }}</td>
						<td>{{ isset($menu->attribute_prompt)?$menu->attribute_prompt:'' }}</td>
						<td>{{ isset($menu->is_customizable)?$menu->is_customizable:'' }}</td>	
						<td>{{ isset($menu->sort_order)?$menu->sort_order:'' }}</td>	
						<td>{{ ($menu->status==0) ? 'InActive' :'Active' }}</td>
						<td>  
							<?php if($menu->backend_field_type=='Dropdown' || $menu->backend_field_type=='Multiple Select' || $menu->backend_field_type=='Checkbox' || $menu->backend_field_type=='Radio'){ ?>
								<a title="List" href="{{ URL::to('/v2/systemattribute/item/' . $menu->id) }}" class="glyphicon glyphicon-list margin-right-10"></a>
							<?php }if($menu->attribute_type!='System'){}?>
								<button class="open-modal bgnone margin-right-10" value="{{$menu->id}}"><a title="Edit" href="javascript:void(0)" value="{{$menu->id}}" class="glyphicon glyphicon-edit"></a></button>

								<a href="javascript:void(0)" title="Delete" onclick="deleteAttribute('{{$menu->id}}')" value="{{$menu->id}}" class="glyphicon glyphicon-trash delete-link"></a>
								<!-- <button class="btn btn-danger delete-link" value="{{$menu->id}}" onclick="deleteAttribute('{{$menu->id}}')">Delete</button> -->
							<?php ?>      
						</td>
					</tr>
				@endforeach
				@else
          			<tr>
						<td id="link0"></td>
						<td id="link0"></td>
						<td id="link0"></td>
						<td id="link0"></td>
						<td id="link0"></td>
						<td id="link0"></td>
						<td id="link0"></td>
						<td id="link0"></td>
						<td id="link0"></td>
					</tr>
        		@endif
        		</tbody>
        	</table>
    	</div>    
	</div>

	<div class="modal fade" id="linkEditorModal" aria-hidden="true">
		<form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate=""  enctype="multipart/form-data">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="linkEditorModalLabel">Attribute</h4>
					</div>
					<div class="modal-body" style="padding:0!important">
						<div id="cms-accordion-msg-popup"></div>
						
						<div class="form-group row"><input type="hidden" name="language_id"  id="language_id"  value=1 >
							 

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<label class="control-label col-form-label" style="text-align:left!important">Attribute Type <sup><b>*</b></sup></label>
								<div class="selct-picker-plain position-relative">
									<select name="attribute_type" id="attribute_type" onchange="showAndHideField(this.value)" class="form-control"  data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
										<option value="">Select</option>
										<option value="General" selected>General</option>
										<option value="System" >System</option>
										<option value="Modifier" >Modifier</option>
										<option value="SystemModifier" >System Modifier</option>
										<option value="SystemModifierItem" >System Modifier Item</option>
										<option value="SystemMeal" >System Meal</option>
										<option value="SystemMealItem" >System Meal Item</option>
										<option value="SystemAddon" >System Addon</option>
										<option value="SystemAddonItem" >System Addon Item</option>
									</select> 
								</div>
								<div class="invalid-feedback hide error" id="type_error">
									<b id="type_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15 hide">
								<label class="control-label col-form-label" style="text-align:left!important; display:none;">Frontend Type Field</label>
								<div class="selct-picker-plain position-relative">
									<select name="frontend_field_type" id="frontend_field_type" class="form-control"  data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" >
										<option value="">Select</option>
										<option value="Text" selected>Text</option>
										<option value="Dropdown">Drop Down</option>	
										<option value="Multiple Select">Multiple Select</option>
										<option value="Checkbox">Checkbox</option>
										<option value="Radio">Radio</option>
										<option value="TextArea">Text Area</option>
										<option value="Editor">Editor</option>	
										<option value="DateTime">DateTime</option>
										<option value="File">File</option>
										<option value="Float">Float [0.00]</option>
										<option value="Number">Number [0.00]</option>	
										<option value="Image">Image</option>
									</select> 
								</div>
								<div class="invalid-feedback hide error" id="frontend_field_type_error">
									<b id="frontend_field_type_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<label class="control-label col-form-label" style="text-align:left!important">Backend Type Field<sup><b>*</b></sup></label>
								<div class="selct-picker-plain position-relative">
									<select name="backend_field_type" id="backend_field_type" class="form-control"  data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" onchange="showHideIsCustomizable()">
										<option value="">Select</option>
										<option value="Text">Text</option>
										<option value="Dropdown">Drop Down</option>	
										<option value="Multiple Select">Multiple Select</option>
										<option value="Checkbox">Checkbox</option>
										<option value="Radio">Radio</option>
										<option value="TextArea">Text Area</option>
										<option value="Editor">Editor</option>	
										<option value="DateTime">DateTime</option>
										<option value="File">File</option>
										<option value="Float">Float [0.00]</option>
										<option value="Number">Number [0.00]</option>
										<option value="Image">Image</option>
									</select> 
								</div>
								<div class="invalid-feedback hide error" id="backend_field_type_error">
									<b id="backend_field_type_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<label for="inputLink" class="control-label col-form-label" style="text-align:left!important">Label<sup><b>*</b></sup></label>
								<div class="input__field">
									<input type="text" id="attribute_label" name="attribute_label" placeholder="Enter Label" value="">
								</div>
								<div class="invalid-feedback hide error" id="attribute_label_error">
									<b id="attribute_label_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<label for="attribute_code" class="control-label col-form-label" style="text-align:left!important">Attribute Code<sup><b>*</b></sup></label>
								<div class="input__field">
									<input type="text" id="attribute_code" name="attribute_code" placeholder="Enter Code" value="">
								</div>
								<div class="invalid-feedback hide error" id="attribute_label_error">
									<b id="attribute_code_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-15">
								<label for="inputLink" class="control-label col-form-label padding-bottom-10" style="text-align:left!important">Description</label>
								<div class="input__field">
									<textarea cols='25' rows='5' id="attribute_description" name="attribute_description" class=""> </textarea>
								</div>
								<div class="invalid-feedback hide error" id="attribute_description_error">
									<b id="attribute_description_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<label class="control-label col-form-label" style="text-align:left!important">Prompt<sup><b>*</b></sup></label>
								<div class="input__field">
									<input type="text" id="attribute_prompt" name="attribute_prompt" placeholder="Enter Prompt" value="">
								</div>
								<div class="invalid-feedback hide error" id="attribute_prompt_error">
									<b id="attribute_prompt_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<label for="inputLink" class="control-label col-form-label" style="text-align:left!important">Attribute Image Class</label>
								<div class="input__field">
									<input type="text" id="attribute_image_class" name="attribute_image_class" placeholder="Enter Image Class" value="">
								</div>
								<div class="invalid-feedback hide error" id="attribute_image_class_class_error">
									<b id="attribute_image_class_class_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<label for="inputLink" class="control-label col-form-label" style="text-align:left!important">Image</label>
								<div class="source-image-div image-caption-div">
									<input id="attribute_image" type="file" name="attribute_image" class="form-control" /> 
									<img src="" width="50" height="50" class="hide" id="img"> 
								</div>
								<div class="col-xs-12 imgclss hide"> </div>
								<input type="hidden" value="0" name="deletemyimage" id="deletemyimage"/>
								<div class="invalid-feedback hide error" id="attribute_image_error">
									<b id="attribute_image_error1" class="error1"></b>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<label for="inputLink" class="control-label col-form-label" style="text-align:left!important">Sort Order<sup><b>*</b></sup></label>
								<div class="input__field">
									<input type="text" id="sort_order" name="sort_order" placeholder="Enter Sort Order" value="99999">
								</div>
								<div class="invalid-feedback hide error" id="attribute_value_error">
									<b id="attribute_value_error1" class="error1"></b>
								</div>
							</div>
						</div> 
							
							
						<div class="form-group row ">
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15 hide">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Show Prompt in Frontend</label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="show_prompt_in_frontend" id="show_prompt_in_frontend" value="Yes" checked>
									<label for="show_prompt_in_frontend">Yes</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input  type="radio" id="show_prompt_in_frontend_no" name="show_prompt_in_frontend" value="No">
									<label for="show_prompt_in_frontend_no">No</label>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15 hide">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Show in Frontend</label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="show_in_frontend" id="show_in_frontend" value="Yes" checked>
									<label for="show_in_frontend">Yes</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input type="radio" id="show_in_frontend_no" name="show_in_frontend" value="No">
									<label for="show_in_frontend_no">No</label>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15 hide">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Show in Backend</label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="show_in_backend" id="show_in_backend" value="Yes" checked>
									<label for="show_in_backend">Yes</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input type="radio" id="show_in_backend_no" name="show_in_backend" value="No">
									<label for="show_in_backend_no">No</label>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15 is_customizable hide">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Is Customizable</label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="is_customizable" id="is_customizable" value="Yes">
									<label for="is_customizable">Yes</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input type="radio" checked id="is_customizable_no" name="is_customizable" value="No">
									<label for="is_customizable_no">No</label>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15 use_as_filter hide">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Is Filterable</label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="use_as_filter" id="use_as_filter" value="Yes">
									<label for="use_as_filter">Yes</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input type="radio" id="use_as_filter_no" name="use_as_filter" value="No" checked>
									<label for="use_as_filter_no">No</label>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15 use_in_search hide">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Is Searchable</label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="use_in_search" id="use_in_search" value="Yes">
									<label for="use_in_search">Yes</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input type="radio" name="use_in_search" id="use_in_search_no"  value="No" checked>
									<label for="use_in_search_no">No</label>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15 use_in_promo hide">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Is Promo</label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="use_in_promo" id="use_in_promo" value="Yes" >
									<label for="use_in_promo">Yes</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input type="radio" id="use_in_promo_no" name="use_in_promo" value="No" checked>
									<label for="use_in_promo_no">No</label>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Is Required<sup><b>*</b></sup></label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="is_required" id="is_required" value="1" checked>
									<label for="is_required">Active</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input type="radio" id="is_required_no" name="is_required" value="0">
									<label for="is_required_no">Inactive</label>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
								<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Status<sup><b>*</b></sup></label></div>
								<div class="pull-left tbl-radio-btn">
									<input type="radio" name="status" id="status" value="1" checked>
									<label for="status">Active</label>
								</div>
								<div class="pull-left tbl-radio-btn margin-left-30">
									<input type="radio" id="status_no" name="status" value="0">
									<label for="status_no">Inactive</label>
								</div>
							</div>
							
						</div>
			
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn__primary" id="btn-save" value="add">Save changes</button>
						<input type="hidden" id="id" name="id" value="0">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
					
				</div>
			</div>
		</form>
	</div>
		
<script type="text/javascript">
function showHideIsCustomizable(){
	var tp = $('#backend_field_type').val(); 
	if(tp == "Dropdown"){
		$('.is_customizable').removeClass('hide');
	}else{
		$('.is_customizable').addClass('hide');
	}	
}

function validateImageWeb(fileName){
	var error = [0, "", ""];
	if(fileName){
		//var imgname  =  $('input[type=attribute_image]').val();
		
		var size  =  $('#attribute_image')[0].files[0].size; 
		var ext =  fileName.substr( (fileName.lastIndexOf('.') +1) );
		if(ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='PNG' || ext=='JPG' || ext=='JPEG') {
		    if(size<=1000000) { 
			return error; 
		    }else{
			 error = [2, "#attribute_image_error", 'Sorry File size exceeding from 1 Mb']; 
		    }
		}else error = [2, "#attribute_image_error", 'File must be jpg,png']; 
	}
	return error; 
}
function sortRow(arr){
	var formData = { data: arr  }; 
	$.post( '/v2/systemattribute/sort/1', formData );
	 
	      
}
function validateFormData(data){
	//console.log(data);return false;
	var attribute_type = $('#attribute_type').val();
	var backend_field_type = $('#backend_field_type').val();
	var attribute_label = $('#attribute_label').val();
	var attribute_code = $('#attribute_code').val();
	var attribute_prompt = $('#attribute_prompt').val();
	var sort_order = $('#sort_order').val();
	var is_pass = true;
	var msg = '';
	if(attribute_type==""){
		msg = "Please Select Attribute Type.";
		is_pass =false;
	}else if(backend_field_type==""){
		msg = "Please Select Backend Field Type.";
		is_pass =false;
	}else if(attribute_label==""){
		msg = "Attribute Label cannot be blank.";
		is_pass =false;
	}else if(attribute_code==""){
		msg = "Attribute Code cannot be blank.";
		is_pass =false;
	}else if(attribute_prompt==""){
		msg = "Attribute Prompt cannot be blank.";
		is_pass =false;
	}else if(sort_order==""){
		msg = "Sort Order cannot be blank.";
		is_pass =false;
	}
	if(is_pass!=true){
		showMessageOnPopUp(msg , 0);
		return false;
	}else{
		return true;	
	}
}
function validateAttributeForm(formData){
	//console.log(formData);
	var error = [0, "", ""];
	if(formData.language_id==""){
	   error = [1, "#language_id_error", "Language required"];
	}else if(formData.frontend_field_type=="" || formData.backend_field_type==""){
	   error = [1, "#frontend_field_type_error", "Frontend/Backend field required"];
	}else if(formData.attribute_label==""){
	   error = [1, "#attribute_label_error", "Label required"];
	}
	else if(formData.attribute_prompt==""){
	   error = [1, "#attribute_prompt_error", "Prompt required"];
	}
	
	
 	return error; 
}
$(document).ready( function () {
    var table1  =  $('#table_id1').DataTable({
		"order": [[ 6, "asc" ]],
		rowReorder: true,
                responsive: true,
                dom:'<"top"fiB><"bottom"trlp><"clear">',
                buttons: [
		    		{
                		text: 'Add Group',
                		action: function ( e, dt, node, config ) {
		    				$('#modalFormData').trigger("reset");
		    				$('#btn-save').val("add");
		    				$('#btn-save').text("Add");		
                    		$('#linkEditorModal').modal('show');
		    				$('#cms-accordion-msg-popup').html('');		
                		}
            		},
                    {
						extend: 'colvis',
						text: '<i class="fa fa-eye"></i>',
						titleAttr: 'Column Visibility'
					},
					{
						extend: 'excelHtml5',
						text: '<i class="fa fa-file-excel-o"></i>',
						titleAttr: 'Excel',
						exportOptions: { columns: ':visible' }
					},
					{
						extend: 'pdfHtml5',
						text: '<i class="fa fa-file-pdf-o"></i>',
						titleAttr: 'PDF',
						exportOptions: { columns: ':visible' }
					},
					{
						extend: 'print',
						text: '<i class="fa fa-print"></i>',
						titleAttr: 'Print',
						exportOptions: {
							columns: ':visible',
						},
					}
                ],
                columnDefs: [
					//{ targets:[-1,-6], visible: false },
					{ targets:[0,1,2,3,4,5,6,7,8], className: "desktop" },
					{ targets:[0,1,2,3,8], className: "tablet" },
					{ targets:[0,1,8], className: "mobile" }
				],
                language: {
                    search: "",
                    searchPlaceholder: "Search"
                }
    });
    table1.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[1]+'<br>';
 	
        var newData = [];
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
	   newData.push([diff[i].oldData,diff[i].newData]);	
           /* var rowData = table1.row( diff[i].node ).data();
 	   	
            result += rowData[1]+' updated to be in position '+
                diff[i].newData+' (was '+diff[i].oldData+')<br>';*/
        }
	//console.log(newData);
	sortRow(newData);
 	 
    } );
    $("#btn-save").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
		e.preventDefault();
		$('#cms-accordion-msg-popup').html('');
		$('.imgclss').html('');
		$('.imgclss').addClass('hide');	
         
		var form = $("#modalFormData")[0];
		var formdata = new FormData(form);
		if(validateFormData(formdata)){ 
			var state = $('#btn-save').val();
			var type = "POST";
			var id = $('#id').val();
			var ajaxurl = '/v2/systemattribute/add';
			if (state == "update") {
			    type = "POST";
			    ajaxurl = '/v2/systemattribute/edit/' + id;
			}
			 
			
			$.ajax({
			    type: type,
			    url: ajaxurl,
			    data: formdata,
			    enctype: 'multipart/form-data',
	      		    processData: false,  // tell jQuery not to process the data
	      		    contentType: false,   // tell jQuery not to set contentType	
			    success: function (data1) {  
				if(data1.error==0){
					var data = data1.data;
					var status='InActive';
					if(data.status==1)
					  status='Active';
					var url = "{{ URL::to('v2/systemattribute/item/') }}";		
					var rrow = '<tr id="link' + data.id + '">'+ '<td>' + data.id + '</td><td>' + data.attribute_code + '</td><td>' + data.attribute_type + '</td><td>' + data.attribute_label + '</td><td>' + data.attribute_prompt + '</td><td>' + data.is_customizable + '</td><<td>'+ data.sort_order + '</td><td>' + status + '</td>';
					rrow += '<td><a href="'+url +'/' + data.id + '"  class="btn btn-info  " value="' + data.id + '"  class="glyphicon glyphicon-list margin-right-10"></a>';
					rrow += ' <button class="btn btn-info open-modal" value="' + data.id + '"> <a title="Edit" href="javascript:void(0)" value="' + data.id + '" class="glyphicon glyphicon-edit"></a></button>&nbsp;';
					rrow += '<a href="javascript:void(0)" title="Delete" onclick="deleteAttribute(' + data.id + ')" value="{{$menu->id}}" class="glyphicon glyphicon-trash delete-link"></a></td>';
					rrow += '</tr>';
					if (state == "add") {
					    $('#links-list').append(rrow);
					    showMessageOnPopUp('Attribute added successfully!' , 1)	
					} else {
					    $("#link" + id).replaceWith(rrow);
					    showMessageOnPopUp('Attribute updated successfully!' , 1)	
					}
					setTimeout(function(){ 
						//$('#modalFormData').trigger("reset");
						$('#linkEditorModal').modal('hide');
						$('#accordion-msg-popup').html('');
						$('#accordion-msg').html('');	 
					}, 2000);
					$('.error').addClass('hide');
					$('#cms-accordion-msg').html('');
						
					$('#modalFormData').trigger("reset");
				}else{
					showMessageOnPopUp(data1.message , 0);
				}
				
			    },
			    error: function (data) {
			       // console.log('Error:', data);
				showMessageOnPopUp('Please try again!' , 0);
				//$('#linkEditorModal').modal('hide');
			    }
			});
	  }
    });
    $('body').on('click', '.open-modal', function () {
        var id = $(this).val();
        $.get('/v2/systemattribute/list/' + id, function (data1) {
	   if(data1.error==0){
			$('#linkEditorModal').modal('show');	
		    var data = data1.data;
		    $('#id').val(data.id);
		    //$("input:radio").attr("checked", false);	
		    //$('select[name^="language_id"] option:selected').attr("selected",null);
		   // $('select[name^="language_id"] option[value="'+data.language_id+'"]').attr("selected","selected");
		     
		   
		    //$('#file').val(data.attribute_value_image);
		    $('select[name^="attribute_type"] option:selected').attr("selected",null);
		    $('select[name^="attribute_type"] option[value="'+data.attribute_type+'"]').attr("selected","selected");
		    $('select[name^="frontend_field_type"] option:selected').attr("selected",null);
		    $('select[name^="frontend_field_type"] option[value="'+data.frontend_field_type+'"]').attr("selected","selected");
		    $('select[name^="backend_field_type"] option:selected').attr("selected",null);
		    $('select[name^="backend_field_type"] option[value="'+data.backend_field_type+'"]').attr("selected","selected");
		     $('select[name^="show_in_backend"] option:selected').attr("selected",null);
		    $('select[name^="show_in_backend"] option[value="'+data.show_in_backend+'"]').attr("selected","selected");
		    $('#attribute_label').val(data.attribute_label);
		    $('#sort_order').val(data.sort_order);
		    $('#attribute_prompt').val(data.attribute_prompt);
		    $('#attribute_code').val(data.attribute_code);
		    if(data.show_prompt_in_frontend=='No')$('#show_prompt_in_frontend_no').attr('checked', true);
		    else $('#show_prompt_in_frontend').attr('checked', true);
		    if(data.show_in_frontend=='No')$('#show_in_frontend_no').attr('checked', true);
		    else $('#show_in_frontend').attr('checked', true);	 	
		    if(data.is_customizable=='No')$('#is_customizable_no').attr('checked', true);
		    else $('#is_customizable').attr('checked', true);
		    if(data.status==0)$('#status_no').attr('checked', true);
		    else $('#status').attr('checked', true);
		    if(data.use_in_search=='No')$('#use_in_search_no').attr('checked', true);
		    else $('#use_in_search').attr('checked', true);
		    if(data.use_as_filter=='No')$('#use_as_filter_no').attr('checked', true);
		    else $('#use_as_filter').attr('checked', true);
		    if(data.use_in_promo=='No')$('#use_in_promo_no').attr('checked', true);
		    else $('#use_in_promo').attr('checked', true);
		    if(data.is_required==0)$('#is_required_no').attr('checked', true);
		    else $('#is_required').attr('checked', true);
			
		    $('#attribute_description').val(data.attribute_description);
	        $('#attribute_image_class').val(data.attribute_image_class);
		     
		    
		    
		    if(data.attribute_image!=''){
				var imgUrl = '<br><div class="img" id="showimage"><a href="'+data.attribute_image+'" target="_blank"><img src="'+data.attribute_image+'" width="50" height="50"></a><br/><input type="checkbox"  id="deleteimage"  name="deleteimage" >Delete Image</div>';		
		    		$('.imgclss').html(imgUrl);	
				$('.imgclss').removeClass('hide');	
		    }	
		    $('#btn-save').val("update");//img
	  }
        })
    });
	$('#deleteimage').click(function () {
		var is_checked = $("input[name='deleteimage']:checked").val();	
		alert(is_checked)
		$('#deletemyimage').val(is_checked);	
	});	
     
    $('#attribute_label').keyup(function(){
	    var name = $('#attribute_label').val();
	    var skuName = name;
	     
	     var skuName = skuName.replace(/"/g, " Inch");
	     var skuName = skuName.toLowerCase();
	     var skuName = skuName.replace(/ /g, "_");
	     
	    	$('#attribute_code').val(skuName);    
	    
	});
	$('#attribute_label').focusout(function(){
	    var name = $('#attribute_label').val();
	    var skuName = name;
	     
	     var skuName = skuName.replace(/"/g, " Inch");
	     var skuName = skuName.toLowerCase();
	     var skuName = skuName.replace(/ /g, "_");
	     
	    	$('#attribute_code').val(skuName);    
	    
	});	 	
});
function deleteAttribute(id){
	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "DELETE",
            url: '/v2/systemattribute/delete/' + id,
            success: function (data) {
                console.log(data);
                $("#link" + id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
}
function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	$('#cms-accordion-msg').html(msg);	
         
}
function showMessageOnPopUp(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	$('#cms-accordion-msg-popup').html(msg);	
         
}
function showAndHideField(type){
	if(type=='General'){
	 	$('.is_customizable').removeClass('hide');
		$('.use_as_filter').removeClass('hide');
		$('.use_in_search').removeClass('hide');
		$('.use_in_promo').removeClass('hide');
	}else{
		$('.is_customizable').addClass('hide');
		$('.use_as_filter').addClass('hide');
		$('.use_in_search').addClass('hide');
		$('.use_in_promo').addClass('hide');
	}	
		     
}
</script>
<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
        tinymce.init({
            selector: 'textarea.editor',
            menubar: "tools",
            height: 320,
            theme: 'modern',
            plugins: 'code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar: 'code | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });
	});
</script> 
@endsection
