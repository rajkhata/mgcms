@extends('layouts.app')
@section('content')
    <style>
	#result {
        border: 1px solid #888;
        background: #f7f7f7;
        padding: 1em;
        margin-bottom: 1em;
    }
    </style>  
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
<link href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css" rel='stylesheet' />
<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
    <div class="main__container deal-discounts-page container__custom_working new-design-labels prl20">
	 
	 
	<div id="cms-accordion" class="row margin-top-30">
	<div class="reservation__atto">
	    <div class="reservation__title">
	      <h1 style="uppercase"><a href="/v2/modifiergroup">Modifier Group</a> >> {{$attribute_label}}</h1>
	    </div>
	    
	  </div><!--span style="float: right;margin:0px 5px;"><a href="javascript:void();" onclick="createAttributeValue();" class="btn btn__primary">Add</a></span-->
	<div id="cms-accordion-msg">
	@if(session()->has('message'))
                <div class="alert alert-success margin-top-20">
                    {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        @endif
	</div>
        <table id="table_id1" class="ui celled cell-border hover display compact nowrap responsive" style="width:100%">
        <thead>
          <tr>
           <th>Id</th>
           <th>Attribute </th>
	   <th>Parent </th>	
	   <th>Value </th>
	   <th>Price</th>
	   <th>Special Price</th>		
	   <th>Calories</th>	
           <th>Status</th>
	   <th>Sort Order</th>
	   <th>Edit</th>	
         </tr>
        </thead>
        <tbody id="links-list" name="links-list">
        @if(count($menuAttributes))
        @foreach($menuAttributes as $menu)
        <tr id="link{{$menu->id}}">
		<td>{{ isset($menu->id)?($menu->id):'' }}</td>
		<td>{{ isset($menu->attribute_label)?$menu->attribute_label:'' }}</td>
		<td>{{ isset($menu->parent_attribute_value)?$menu->parent_attribute_value:'' }}</td>
		<td>{{ isset($menu->attribute_value)?$menu->attribute_value:'' }}</td>
		<td>{{ isset($menu->attribute_price)?$menu->attribute_price:'' }}</td>
		<td>{{ isset($menu->attribute_special_price)?$menu->attribute_special_price:'' }}</td>
		<td>{{ isset($menu->calories)?$menu->calories:'' }}</td>
		<td>{{ ($menu->status==0) ? 'InActive' :'Active' }}</td>
		<td>{{ isset($menu->sort_order)?$menu->sort_order:'' }}</td>
		<td> <button class="btn btn-info open-modal" value="{{$menu->id}}">Edit </button>
                      <button class="btn btn-danger delete-link hidden" value="{{$menu->id}}">Delete </button>
                </td>
        </tr>
        @endforeach
        @else
          <tr><td colspan="11">No Attribute Value found</td></tr>
        @endif
        </tbody>
        </table>
    </div>    
</div>
<div class="modal fade" id="linkEditorModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="linkEditorModalLabel">Modifier Item</h4>
                        </div>
                        <div class="modal-body">
			<div id="cms-accordion-msg-popup"></div>
                            <form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate="">
 				<div class="form-group" >
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Product Type</label>
                                    <div class="col-sm-6">
                                         <select name="parent_id" id="parent_id" >
					  <option value="0">Parent</option>
					  @foreach($menuAttributes as $l)	
					  <option value="{{$l->id}}">{{$l->attribute_value}}</option>
					  @endforeach 
					</select> 
                                    </div>
				    <div class="col-sm-3 invalid-feedback hide error" id="parent_id_error">
                                         <b id="parent_id_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Price</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="attribute_price" name="attribute_price"
                                               placeholder="Enter Price" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_price_error">
                                         <b id="attribute_price_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Special Price</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="attribute_special_price" name="attribute_special_price"
                                               placeholder="Enter Special Price" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_special_price_error">
                                         <b id="attribute_special_price_error1" class="error1"></b>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Attribute Value</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="attribute_value" name="attribute_value"
                                               placeholder="Enter Value" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Sort Order</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="sort_order" name="sort_order"
                                               placeholder="Enter Sort Order" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Calories</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="calories" name="calories"
                                               placeholder="Enter Calories" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="calories_error">
                                         <b id="calories_error1" class="error1"></b>
                                    </div>
                                </div>
 				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">POS ID</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="pos_id" name="pos_id"
                                               placeholder="Enter POS ID" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Group Dependency</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="group_dependency" name="group_dependency"
                                               placeholder="Enter Dependency" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Item Dependency</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="item_dependency" name="item_dependency"
                                               placeholder="Enter Dependency" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Web Image </label>
                                    <div class="col-sm-3">
                                        <input type="file" id="file" name='file'> 
                                    </div><div class="col-sm-3 img hide"> </div>
					<div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div> 
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Mobile Image </label>
                                    <div class="col-sm-3">
                                        <input type="file" id="filemobile" name='filemobile'> 
                                    </div><div class="col-sm-3 img hide"> </div>
					<div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">App Image </label>
                                    <div class="col-sm-3">
                                        <input type="file" id="fileapp" name='fileapp'> 
                                    </div><div class="col-sm-3 img hide"> </div>
					<div class="col-sm-3 invalid-feedback hide error" id="attribute_value_error">
                                         <b id="attribute_value_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Status</label>
                                    <div class="col-sm-9">
                                        <input type="radio" class="status" name="status" id="status" value="1" checked> Active &nbsp; &nbsp; <input type="radio"  class="status" id="instatus" name="status" value="0"> InActive
                                    </div>
                                </div>
				 
				
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes
                            </button>
                            <input type="hidden" id="id" name="id" value="0">
			    <input type="hidden" id="attribute_code" name="attribute_code" value="{{$attribute_code}}">
                        </div>
                    </div>
                </div>
            </div>

<script type="text/javascript">
function validateAttributeForm(formData){
	//console.log(formData);
	var error = [0, "", ""];
	if(formData.attribute_label==""){
	   error = [1, "#attribute_value_error", "Attribute Value required"];
	}
	return error; 
}
function validateImageWeb(fileName){
	var error = [0, "", ""];
	if(fileName){
		var imgname  =  $('input[type=file]').val();
		var size  =  $('#file')[0].files[0].size;
		var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
		if(ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='PNG' || ext=='JPG' || ext=='JPEG') {
		    if(size<=1000000) { 
			return error; 
		    }else{
			 error = [2, "#attribute_value_error", 'Sorry File size exceeding from 1 Mb']; 
		    }
		}else error = [2, "#attribute_value_error", 'File must be jpg,png']; 
	}
	return error; 
}
function validateImageMobile(fileName){
	var error = [0, "", ""];
	if(fileName){
		var imgname  =  $('input[type=filemobile]').val();
		var size  =  $('#file')[0].files[0].size;
		var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
		if(ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='PNG' || ext=='JPG' || ext=='JPEG') {
		    if(size<=1000000) { 
			return error; 
		    }else{
			 error = [2, "#attribute_value_error", 'Sorry File size exceeding from 1 Mb']; 
		    }
		}else error = [2, "#attribute_value_error", 'File must be jpg,png']; 
	}
	return error; 
}
function validateImageApp(fileName){
	var error = [0, "", ""];
	if(fileName){
		var imgname  =  $('input[type=fileapp]').val();
		var size  =  $('#file')[0].files[0].size;
		var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
		if(ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='PNG' || ext=='JPG' || ext=='JPEG') {
		    if(size<=1000000) { 
			return error; 
		    }else{
			 error = [2, "#attribute_value_error", 'Sorry File size exceeding from 1 Mb']; 
		    }
		}else error = [2, "#attribute_value_error", 'File must be jpg,png']; 
	}
	return error; 
}
function createAttributeValue(){
  jQuery('#btn-save').val("add");
  jQuery('#btn-save').text("Add");
  jQuery('#id').val(0);			
  jQuery('#linkEditorModal').modal('show');
}
function sortRow(arr){
	var formData = { data: arr  }; 
	$.post( '/v2/modifiergroup/item/sort/1', formData );
	 
	      
}
$(document).ready( function () {
    var table1  =  $('#table_id1').DataTable({
		"order": [[ 8, "asc" ]],
		rowReorder: true,
                responsive: true,
                dom:'lBfrtip',
                buttons: [
		    {
                text: 'Add Items',
                action: function ( e, dt, node, config ) {
		   createAttributeValue();
                }
            },
                    {extend: 'excelHtml5'}, 
                    {extend: 'pdfHtml5'},   
                    {               
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ],
                columnDefs: [ {
                    targets: -1,
                    visible: true
                }],
                language: {
                    search: "",
                    searchPlaceholder: "Search"
                }
	   });
    table1.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[1]+'<br>';
 	
        var newData = [];
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
	   newData.push([diff[i].oldData,diff[i].newData]);	
           /* var rowData = table1.row( diff[i].node ).data();
 	   	
            result += rowData[1]+' updated to be in position '+
                diff[i].newData+' (was '+diff[i].oldData+')<br>';*/
        }
	//console.log(newData);
	sortRow(newData);
 	//console.log(result);
       // $('#result').html( 'Event result:<br>'+result );
    } );
    $("#btn-save").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
        var formData = {
            attribute_value: jQuery('#attribute_value').val(),
            attribute_code: jQuery('#attribute_code').val(),
            status: $("input[name='status']:checked").val(),
 	     
        };
	var fileName = $("#file").val();
	var fileNameMobile = $("#filemobile").val();
	var fileNameApp = $("#fileapp").val();	
	var formerror = validateAttributeForm(formData);
	var formerror1 = validateImageWeb(fileName);
	var formerror2 = validateImageMobile(fileName);
	var formerror3 = validateImageApp(fileName);
	if(formerror[0]==1){ 
		console.log(formerror);
		jQuery('.error').addClass('hide');
		jQuery(formerror[1]).removeClass('hide');
		jQuery(formerror[1]+'1').text(formerror[2]);
		
		
	}else if(formerror1[0]==2){ 
		 alert(formerror1[2]);
		
	}else if(formerror2[0]==2){ 
		 alert(formerror2[2]);
		
	}else if(formerror3[0]==2){ 
		 alert(formerror3[2]);
		
	}else{
		
		jQuery('.error').addClass('hide');
		//return false;
		var state = jQuery('#btn-save').val();
		var type = "POST";
		var id = jQuery('#id').val();
		var ajaxurl = '/v2/modifiergroup/item/add';
		if (state == "update") {
		    //type = "PUT";
		    ajaxurl = '/v2/modifiergroup/item/update/' + id;
		}
		data = new FormData();
		
	    	if(fileName)data.append('file', $('#file')[0].files[0]);
		if(fileNameMobile)data.append('filemobile', $('#filemobile')[0].files[0]);
		if(fileNameApp)data.append('fileapp', $('#fileapp')[0].files[0]);
		data.append('attribute_value', jQuery('#attribute_value').val());
		data.append('attribute_code', jQuery('#attribute_code').val());
		data.append('parent_id', jQuery('#parent_id').val());
		data.append('attribute_price', jQuery('#attribute_price').val());
		data.append('attribute_special_price', jQuery('#attribute_special_price').val());
		data.append('calories', jQuery('#calories').val());
		data.append('pos_id', jQuery('#pos_id').val());
		data.append('sort_order', jQuery('#sort_order').val());
		data.append('item_dependency', jQuery('#item_dependency').val());
		data.append('group_dependency', jQuery('#group_dependency').val());
		data.append('status',  $("input[name='status']:checked").val());
	    	 
			
			$.ajax({
			    type: type,
			    url: ajaxurl,
			    data: data,
			    dataType: 'json',
			    enctype: 'multipart/form-data',
      			    processData: false,  // tell jQuery not to process the data
      			    contentType: false,   // tell jQuery not to set contentType
			    success: function (data1) {  
				if(data1.error==0){
					//table1.ajax.reload();
					var data = data1.data;
					var status='InActive';
					if(data.status==1)
					  status='Active';
					 
					var rrow = '<tr id="link' + data.id + '">'+ '<td>' + data.id + '</td><td>' + data.attribute_label+ '</td><td>&nbsp;'+ data.parent_attribute_value+ '</td><td>' + data.attribute_value + '</td><td>' + data.attribute_price + '</td><td>' + data.attribute_special_price  + '</td><td>' + data.calories + '</td><td>' +  status + '</td><td>' + data.sort_order + '</td> ';
					rrow += '<td><button class="btn btn-info open-modal" value="' + data.id + '">Edit</button>&nbsp;';
					//rrow += '<button class="btn btn-danger delete-link" value="' + data.id + '">Delete</button></td>';
					rrow += '</tr>';
					if (state == "add") {
					    jQuery('#links-list').append(rrow);
					    showMessageOnPopUp('Modifier Item added successfully!' , 1)	
					} else {
					    $("#link" + data.id).replaceWith(rrow);
					    showMessageOnPopUp('Modifier Item updated successfully!' , 1)	
					}
					setTimeout(function(){ jQuery('#modalFormData').trigger("reset");
					jQuery('#linkEditorModal').modal('hide'); }, 15000);
					
				}
			    },
			    error: function (data) {
				//console.log('Error:', data);
				showMessageOnPopUp('Please try again!' , 0);
				//jQuery('#linkEditorModal').modal('hide');	
			    }
			});
		
	}
    });
     jQuery('body').on('click', '.open-modal', function () {
        var link_id = $(this).val();
        $.get('/v2/modifiergroup/item/list/' + link_id, function (data1) {
	   if(data1.error==0){
		    var data = data1.data;
		    jQuery('#id').val(data.id);
		    //jQuery('#file').val(data.attribute_value_image);
		    jQuery('#attribute_value').val(data.attribute_value);
		    jQuery('#sort_order').val(data.sort_order);	
		    jQuery('#attribute_special_price').val(data.attribute_special_price);
		    jQuery('#attribute_price').val(data.attribute_price); 
		    jQuery('#pos_id').val(data.pos_id); 
		    jQuery('#calories').val(data.calories); 	
		    jQuery('#item_dependency').val(data.item_dependency);	
		    jQuery('#group_dependency').val(data.group_dependency);	
		    if(data.status==0)jQuery('#instatus').attr('checked', true);
		    else jQuery('#status').attr('checked', true);
		    $('select[name^="parent_id"] option:selected').attr("selected",null);
		    $('select[name^="parent_id"] option[value="'+data.parent_id+'"]').attr("selected","selected");	
		    jQuery('#btn-save').val("update");//img
		    jQuery('#linkEditorModal').modal('show');
	  }
        })
    });	
    ////----- DELETE a link and remove from the page -----////
    jQuery('.delete-link').click(function () {
        var link_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "DELETE",
            url: '/v2/modifiergroup/item/delete/' + link_id,
            success: function (data) {
                console.log(data);
                $("#link" + link_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
     	
});
function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg').html(msg);	
         
}
function showMessageOnPopUp(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg-popup').html(msg);	
         
}
</script>
@endsection
