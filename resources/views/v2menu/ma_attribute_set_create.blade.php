@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1><a href="/v2/systemattributeset">Attribute Set</a> - Create</h1>
    </div>
  </div>

  <div>
    <div class="card form__container" id="maAttribute">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div>
          <form method="POST" action="/v2/systemattributeset/add" enctype="multipart/form-data">
            @csrf
 	           
	    <div class="form-group row form_field__container">
                 <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant Branch *') }}</label>
                <div class="col-md-8">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }} " 
			required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                  <option value="0">Select Restaurant</option>
                  @foreach($groupRestData as $branch)
                     <option value="{{ $branch['id'] }}"  {{ ($restaurant_id== $branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
		  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
                
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
              <div class="col-md-8">
                <div class="input__field">
                  <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                  name="name" value="{{ old('name') }}" required>
                </div>
                @if ($errors->has('name'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="is_popular" class="col-md-4 col-form-label text-md-right">Select General Attribute</label>
                <div class="col-md-8" style="padding-top: 10px;">
                <div class="clearfix ptb5">
                <div class="option__wrapper pr15">
                @if($menuAttributeGeneral)
                @foreach($menuAttributeGeneral as $item)
                  <!-- &nbsp;&nbsp;<input id="{{$item->id}}" type="checkbox" class="" name="general_attribute_ids[{{$item->id}}]" checked>&nbsp;{{$item->attribute_label}} -->
                    
                  <label class="custom_checkbox relative font-weight-600 ci0 cid{{$item->restaurant_id}}"><input class="hide permission" type="checkbox" id="{{$item->id}}" name="general_attribute_ids[{{$item->id}}]"> {{$item->attribute_label}} <span class="control_indicator" ></span>
                  </label>
                @endforeach
                @endif
                </div>
								</div>
                </div>
            </div>
 
             
 		
            <div class="form-group row form_field__container">
              <label for="is_popular" class="col-md-4 col-form-label text-md-right">Select Modifier Attribute</label>
              <div class="col-md-8" style="padding-top: 10px;">
                <div class="clearfix ptb5">
                <div class="option__wrapper pr15">
			          @if($menuAttributeModifier)
			          @foreach($menuAttributeModifier as $item)
                  <label class="custom_checkbox relative font-weight-600 ci0 cid{{$item->restaurant_id}}"><input class="hide permission" type="checkbox" id="{{$item->id}}" name="system_modifier[{{$item->id}}]"> {{$item->attribute_label}} <span class="control_indicator" ></span>
                  </label>
			          @endforeach
                @endif
                </div>
								</div>
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="is_popular" class="col-md-4 col-form-label text-md-right">Select Modifier Item Attribute</label>
              <div class="col-md-8" style="padding-top: 10px;">
                <div class="clearfix ptb5">
                <div class="option__wrapper pr15">
			          @if($menuAttributeModifierItem)
			          @foreach($menuAttributeModifierItem as $item)
                  <label class="custom_checkbox relative font-weight-600 ci0 cid{{$item->restaurant_id}}"><input class="hide permission" type="checkbox" id="{{$item->id}}" name="system_modifier_item[{{$item->id}}]"> {{$item->attribute_label}} <span class="control_indicator" ></span>
                  </label>
			          @endforeach
                @endif
                </div>
								</div>
              </div>
            </div>
              
             <div class="form-group row form_field__container">
              <label for="is_popular" class="col-md-4 col-form-label text-md-right">Select Addon</label>
              <div class="col-md-8" style="padding-top: 10px;">
                <div class="clearfix ptb5">
                <div class="option__wrapper pr15">
			          @if($menuAttributeAddon)
			          @foreach($menuAttributeAddon as $item)
                  <label class="custom_checkbox relative font-weight-600 ci0 cid{{$item->restaurant_id}}">
			<input class="hide permission" type="checkbox" id="{{$item->id}}" name="system_addon[{{$item->id}}]"> {{$item->attribute_label}} <span class="control_indicator" ></span>
                  </label>
			          @endforeach
                @endif
                </div>
								</div>
              </div>
            </div>
	    <div class="form-group row form_field__container">
              <label for="is_popular" class="col-md-4 col-form-label text-md-right">Select Addon Item</label>
              <div class="col-md-8" style="padding-top: 10px;">
                <div class="clearfix ptb5">
                <div class="option__wrapper pr15">
			          @if($menuAttributeModifierItem)
			          @foreach($menuAttributeModifierItem as $item)
                  <label class="custom_checkbox relative font-weight-600 ci0"><input class="hide permission" type="checkbox" id="{{$item->id}}" name="system_addon_item[{{$item->id}}]"> {{$item->attribute_label}} <span class="control_indicator" ></span>
                  </label>
			          @endforeach
                @endif
                </div>
								</div>
              </div>
            </div>	
              
            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-8" style="padding-top: 10px;">
                <div class="pull-left tbl-radio-btn">
                  <input type="radio" id="status1" name="status" value="1" checked>
                  <label for="status1">Active</label>
                </div>
                <div class="pull-left tbl-radio-btn margin-left-30">
                  <input type="radio" id="status0" name="status" value="0"  > 
                  <label for="status0">Inactive</label>
                </div>
                @if ($errors->has('status'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <br /><br />
            <div class="row mb-0">
              <div class="col-md-10 text-center">
                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
