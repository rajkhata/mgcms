<script> 
var columns = [
                {
                    title: '',
                    target: 0,
                    className: 'treegrid-control',
                    data: function (item) {
                        if (item.children) {
                            return '<span>+<\/span>';
                        }
                        return '';
                    }
                },
                {
                    title: 'Attribute',
                    target: 1,
                    data: function (item) {
                        return item.attribute_label;
                    }
                },
                {
                    title: 'Parent Name',
                    target: 2,
                    data: function (item) {
                        return item.parent_attribute_item_name;
                    }
                },
                {
                    title: 'Item Name',
                    target: 3,
                    data: function (item) {
                        return item.attribute_item_name;
                    }
                },
		 				 
		 			  
                 
                {
                    title: 'Status',
                    target: 4,
                    data: function (item) {
                        return (item.status==1)?'Yes':'No';
                    }
                },
                {
                    title: 'Action',
                    target: 5,
                    data: function (item) {
			 var htm = '';
			    htm +='<button type="button" class="open-child-modal bgnone margin-right-10"  value="'+item.id+'" onclick="createAttributeValue('+item.id+')"><a title="Add Child" href="javascript:void(0)" class="glyphicon glyphicon-plus"></a></button>';		
			    htm += '<button type="button" class="open-modal bgnone margin-right-10" value="'+item.id+'"><a title="Edit" href="javascript:void(0)" class="glyphicon glyphicon-edit"></a></button>';
		             htm += '<button type="button" class="delete-link bgnone margin-right-10" value="'+item.id+'" onclick="deleteItem('+item.id+')"><a title="Delete" href="javascript:void(0)" class="glyphicon glyphicon-trash"></a></button>';
				
                        return htm;
                    }
                }
 ];
             
        </script>
@extends('layouts.app')
@section('content')
    <style>
	#result {
        border: 1px solid #888;
        background: #f7f7f7;
        padding: 1em;
        margin-bottom: 1em;
    }
    </style>  
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/dashboard.css')}}" rel='stylesheet' />
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
    <link href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css" rel='stylesheet' />
    <script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
    <div class="main__container deal-discounts-page container__custom_working new-design-labels prl20">
	 
	 
	<div id="cms-accordion">
	<div class="reservation__atto">
	    <div class="reservation__title">
	    <h1 style="uppercase"><a href="/v2/systemattribute">Attribute</a> >> {{$attribute_label}}</h1>
	    </div>
	</div>
	@if(count($menuAttributes)==0)<!--span style="float: right;margin:0px 5px;"><a href="javascript:void();" onclick="createAttributeValue();" class="btn btn__primary">Add</a></span-->@endif
	<div id="cms-accordion-msg">
		@if(session()->has('message'))
			<div class="alert alert-success margin-top-20">
				{{ session()->get('message') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
        @endif
	</div>
	<div class="guestbook__container fullWidth box-shadow margin-top-0 relative">
		<table id="table_id1" class="responsive ui cell-border hover" width="100%"></table> 
</div>
    </div>    
</div>
 
<div class="modal fade" id="linkEditorModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="linkEditorModalLabel">Attribute Item</h4>
                        </div>
                        <div class="modal-body" style="padding:0!important">
							<div id="cms-accordion-msg-popup"></div>
							<form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate="" enctype="multipart/form-data">
							
 								<div class="form-group row">
								 	<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
										<label class="control-label col-form-label" style="text-align:left!important">Item Type</label>
										<div class="selct-picker-plain position-relative">
											<select name="attribute_item_parent_id" id="attribute_item_parent_id" class="form-control selectpicker"  data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
												<option value="0">Parent</option>
												@foreach($menuAttributesList as $l)	
												<option value="{{$l->id}}">{{$l->attribute_item_name}}</option>
												@endforeach 
											</select> 
										</div>
										<div class="invalid-feedback hide error" id="attribute_item_parent_id_error">
											<b id="attribute_item_parent_id_error1" class="error1"></b>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
										<label for="inputLink" class="control-label col-form-label" style="text-align:left!important">Item Name</label>
										<div class="input__field">
											<input type="text" id="attribute_item_name" name="attribute_item_name" placeholder="Enter Name" value="">
										</div>
										<div class="invalid-feedback hide error" id="attribute_item_name_error">
											<b id="attribute_item_name_error1" class="error1"></b>
										</div>
									</div>

									<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
										<label for="inputLink" class="control-label col-form-label" style="text-align:left!important">Item Code</label>
										<div class="input__field">
											<input type="text" id="attribute_item_code" name="attribute_item_code" placeholder="Enter Code" value="">
										</div>
										<div class="invalid-feedback hide error" id="attribute_item_name_error">
											<b id="attribute_item_code_error1" class="error1"></b>
										</div>
									</div>

									<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-15">
										<label for="inputLink" class="control-label col-form-label padding-bottom-10" style="text-align:left!important"> Description</label>
										<div class="input__field">
											<textarea cols='25' rows='5' id="attribute_item_description" name="attribute_item_description" > </textarea>
										</div>
										<div class="invalid-feedback hide error" id="attribute_item_description_error">
											<b id="attribute_item_description_error1" class="error1"></b>
										</div>
									</div>

									<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
										<label for="inputLink" class="control-label col-form-label" style="text-align:left!important">Sort Order</label>
										<div class="input__field">
											<input type="text" id="sort_order" name="sort_order" placeholder="Enter Sort Order" value="99999">
										</div>
										<div class="invalid-feedback hide error" id="attribute_value_error">
											<b id="attribute_value_error1" class="error1"></b>
										</div>
									</div>

									<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
										<label for="inputLink" class="control-label col-form-label" style="text-align:left!important">Attribute Image Class</label>
										<div class="input__field">
											<input type="text" id="attribute_item_image_class" name="attribute_item_image_class" placeholder="Enter Image Class" value="">
										</div>
										<div class="invalid-feedback hide error" id="attribute_item_image_class_error">
											<b id="attribute_item_image_class_error1" class="error1"></b>
										</div>
									</div>

									<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-15">
										<label for="inputLink" class="control-label col-form-label padding-bottom-10" style="text-align:left!important">Item Image </label>
										<div class="source-image-div image-caption-div">
											<input type="file" id="attribute_item_image" name='attribute_item_image' class="form-control"> 
										</div> 
										<div class="col-xs-12 imgclss hide"> </div>
										<div class="invalid-feedback hide error" id="attribute_item_image_error">
											<b id="attribute_item_image_error1" class="error1"></b>
										</div>
									</div>

									<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
										<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Is Default</label></div>
										<div class="pull-left tbl-radio-btn">
											<input type="radio" name="is_default" id="is_default" value="Yes">
											<label for="is_default">Yes</label>
										</div>
										<div class="pull-left tbl-radio-btn margin-left-30">
											<input type="radio" id="is_default_no" name="is_default" value="No" checked>
											<label for="is_default_no">No</label>
										</div>
									</div>

									<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-15">
										<div class="clearfix padding-bottom-10"><label class="control-label col-form-label" style="text-align:left!important">Status</label></div>
										<div class="pull-left tbl-radio-btn">
											<input type="radio" class="status" name="status" id="status" value="1" checked>
											<label for="status">Active</label>
										</div>
										<div class="pull-left tbl-radio-btn margin-left-30">
											<input type="radio" class="status" id="instatus" name="status" value="0">
											<label for="instatus">Inactive</label>
										</div>
									</div>

								</div>
				 
								<input type="hidden" id="id" name="id" value="0">
			    				<input type="hidden" id="attribute_id" name="attribute_id" value="{{$attribute_id}}">
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn__primary" id="btn-save" value="add">Save changes </button>
                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
			</div>
			
<script type="text/javascript" src="{{asset('js/dataTables.treeGrid.js')}}"></script>
<script type="text/javascript">
function validateAttributeForm(formData){
	//console.log(formData);
	var error = [0, "", ""];
	if(formData.attribute_item_name==""){
	   error = [1, "#attribute_item_name_error", "Name required"];
	}
	return error; 
}
function validateImageWeb(fileName){
	var error = [0, "", ""];
	if(fileName){
		var imgname  =  $('input[type=attribute_item_image]').val();
		var size  =  $('#attribute_item_image')[0].files[0].size;
		var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
		if(ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='PNG' || ext=='JPG' || ext=='JPEG') {
		    if(size<=1000000) { 
			return error; 
		    }else{
			 error = [2, "#attribute_item_image_error", 'Sorry File size exceeding from 1 Mb']; 
		    }
		}else error = [2, "#attribute_item_image_error", 'File must be jpg,png']; 
	}
	return error; 
}
 
function createAttributeValue(id){
  jQuery('#btn-save').val("add");
  jQuery('#btn-save').text("Add");
  jQuery('#id').val(0);			
  jQuery('#linkEditorModal').modal('show');
  jQuery('#cms-accordion-msg-popup').html('');
  if(id>0){
	$.get('/v2/systemattribute/item/info/' + id, function (data1) {
	   if(data1.error==0){
		     var data = data1.data;
		     var pdata =  data1.pdata;  
		     var selected = 'selected';	
		     var html = '<select name="attribute_item_parent_id" id="attribute_item_parent_id" >';
		     html +='<option value="'+data.id+'" '+selected+'>' + data.attribute_item_name + '</option>';	
		     /*if(data.attribute_item_parent_id == 0){  
		     		html +='<option value="0" '+selected+'>' + data.attribute_item_name + '</option>';	
		     }else {
			     $.each( pdata, function( key, dat ) {
				 if(dat.id == data.attribute_item_parent_id) selected = 'selected';
				 else selected = ''; 
				  
				 html +='<option value="' + dat.id + '" '+selected+'>' + dat.attribute_item_name + '</option>';
			     });
		     	
		     }*/
		     html += '</select>';
		     $('.selectparent').html(html);	
		     
	  }
        })	
   }else{
	var html = '<select name="attribute_item_parent_id" id="attribute_item_parent_id" ><option value="0" selected>Parent</option>';
	/*@foreach($menuAttributesList as $l)	
		html +='<option value="{{$l->id}}">{{$l->attribute_item_name}}</option>';
	@endforeach*/
	html += '</select>';
	$('.selectparent').html(html);
   } 	 		
}
function validateFormData(data){
	//console.log(data);return false;
	var attribute_item_name = $('#attribute_item_name').val();
	var attribute_item_code = $('#attribute_item_code').val();
	 
	var sort_order = $('#sort_order').val();
	var is_pass = true;
	var msg = '';
	if(attribute_item_name==""){
		msg = "Attribute Name cannot be blank.";
		is_pass =false;
	}else if(attribute_item_code==""){
		msg = "Attribute Code cannot be blank.";
		is_pass =false;
	}else if(sort_order==""){
		msg = "Sort Order cannot be blank.";
		is_pass =false;
	}
	if(is_pass!=true){
		showMessageOnPopUp(msg , 0);
		return false;
	}else{
		return true;	
	}
}
function sortRow(arr){
	var formData = { data: arr  }; 
	$.post( '/v2/systemattribute/item/sort/1', formData );
	 
	      
}
$(document).ready( function () {
	
    		var table1  =  $('#table_id1').DataTable({
		        @if(count($menuAttributes))
			"order": [[ 0, "asc" ]],
			 @endif
			'select': { 'style': 'multi', 'selector': 'td:not(:first-child)' },
				'columns': columns,
				 'ajax': '/v2/systemattribute/item/' + {{$attribute_id}} + '/list' ,
						    'treeGrid': {
							'left': 10,
							'expandIcon': '<span>+<\/span>',
							'collapseIcon': '<span>-<\/span>'
						    },
						    responsive: true,
						    dom:'<"top"fiB><"bottom"trlp><"clear">',	
		        buttons: [
			    {
				text: 'Add Items',
				action: function ( e, dt, node, config ) {
				   createAttributeValue(0);
				}
				},
				{
					extend: 'colvis',
					text: '<i class="fa fa-eye"></i>',
					titleAttr: 'Column Visibility'
				},
					{
					extend: 'excelHtml5',
					text: '<i class="fa fa-file-excel-o"></i>',
					titleAttr: 'Excel',
					exportOptions: { columns: ':visible' }
				},
				{
					extend: 'pdfHtml5',
					text: '<i class="fa fa-file-pdf-o"></i>',
					titleAttr: 'PDF',
					exportOptions: { columns: ':visible' }
				},
				{
					extend: 'print',
					text: '<i class="fa fa-print"></i>',
					titleAttr: 'Print',
					exportOptions: {
						columns: ':visible',
					},
				}
		        ],
				columnDefs: [
					{ targets:[0,1,2,3,4,5], className: "desktop" },
					{ targets:[0,1,2,3,4,5], className: "tablet" },
					{ targets:[1,5], className: "mobile" }
				],
		        language: {
		            search: "",
		            searchPlaceholder: "Search"
		        }
	    });
	    $('h4').on('click', function () {
		 var h4 = $(this);
		 if (h4.hasClass('show')) {
			 h4.removeClass('show').addClass('showed').html('-hide code');
			 h4.next().removeClass('hide');
		 }  else {
			 h4.removeClass('showed').addClass('show').html('+show code');
			 h4.next().addClass('hide');
		 }
	   });
	     
   
    $("#btn-save").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
	$('#cms-accordion-msg-popup').html('');
	$('.imgclss').html('');
	$('.imgclss').addClass('hide');		
        /*var formData = {
            attribute_item_name: $('#attribute_item_name').val(),
            attribute_item_parent_id: $('#attribute_item_parent_id').val(),
            status: $("input[name='status']:checked").val(),
	    attribute_item_description: $('#attribute_item_description').val(),
 	    attribute_item_image_class: $('#attribute_item_image_class').val(),
	    dependent_attribute_code: $('#dependent_attribute_code').val(),
	    dependent_attribute_item_id: $('#dependent_attribute_item_id').val(),
	    sort_order: $('#sort_order').val(),
	    attribute_id:$('#attribute_id').val(), 	
        };
	var fileName = $("#attribute_item_image").val();
	//var formerror = validateAttributeForm(formData);
 
	//var formerror1 = validateImageWeb(fileName);
	/*if(formerror[0]==1){ 
		console.log(formerror);
		$('.error').addClass('hide');
		$(formerror[1]).removeClass('hide');
		$(formerror[1]+'1').text(formerror[2]);
		
		
	}else{*/
		
		$('.error').addClass('hide');
		//return false;
		var state = $('#btn-save').val();
		var type = "POST";
		var id = $('#id').val();
		var ajaxurl = '/v2/systemattribute/item/add';
		if (state == "update") {
		    //type = "PUT";
		    ajaxurl = '/v2/systemattribute/item/update/' + id;
		}
		data = new FormData();
		var fileName = $("#attribute_item_image").val();
	    	if(fileName)data.append('attribute_item_image', $('#attribute_item_image')[0].files[0]);
		data.append('attribute_item_parent_id', $('#attribute_item_parent_id').val());
		data.append('attribute_item_name', $('#attribute_item_name').val());
		data.append('attribute_item_description', $('#attribute_item_description').val());
		data.append('attribute_item_image_class', $('#attribute_item_image_class').val());
		data.append('attribute_item_image', $('#attribute_item_image').val());
		 
		data.append('sort_order', $('#sort_order').val());
		data.append('status',  $("input[name='status']:checked").val());
		 
		data.append('is_default',  $("input[name='is_default']:checked").val());
	    	data.append('attribute_id', $('#attribute_id').val()); 
		data.append('attribute_item_code', $('#attribute_item_code').val()); 
		if(validateFormData(data)){	
			$.ajax({
			    type: type,
			    url: ajaxurl,
			    data: data,
			    dataType: 'json',
			    enctype: 'multipart/form-data',
      			    processData: false,  // tell jQuery not to process the data
      			    contentType: false,   // tell jQuery not to set contentType
			    success: function (data1) {  
				if(data1.error==0){
					//table1.ajax.reload();
					var data = data1.data;
					var status='InActive';
					if(data.status==1)
					  status='Active';
					var parent = '';
					  if(data.parent_attribute_value!="")parent = data.parent_attribute_value + " --> ";
					var rrow = '<tr id="link' + data.id + '">'+ '<td>' + data.id + '</td><td>' + data.attribute_item_parent_id + '</td><td>' + data.attribute_label+ '</td> <td>' +parent + data.attribute_item_name + '</td><td>' + data.is_default + '</td><td>' +  data.sort_order + '</td><td>' + status + '</td> ';
					rrow += '<td><button class="btn btn-info open-child-modal" value="' + data.id + '" onclick="createAttributeValue(' + data.id + ')">Add Child </button>&nbsp;';
					rrow += '<button class="btn btn-info open-modal" value="' + data.id + '">Edit</button>&nbsp;';
					rrow += '<button class="btn btn-danger delete-link" value="' + data.id + '" onclick="deleteItem(' + data.id + ')">Delete</button></td>';
					rrow += '</tr>';
					if (state == "add") {
					    $('#links-list').append(rrow);
					    showMessageOnPopUp('Modifier Item added successfully!' , 1)	
					} else {
					    $("#link" + data.id).replaceWith(rrow);
					    showMessageOnPopUp('Modifier Item updated successfully!' , 1)	
					}
					setTimeout(function(){ 
						$('#modalFormData').trigger("reset");
						$('#linkEditorModal').modal('hide');
						$('#cms-accordion-msg').html('');	
						$('#cms-accordion-msg-popup').html('');	
						@if(count($menuAttributes)==0)
							location.reload();
						@endif 
					}, 2000);
					
				}else{
				  showMessageOnPopUp(data1.message , 0);		
				}
			    },
			    error: function (data) {
				//console.log('Error:', data);
				showMessageOnPopUp('Please try again!' , 0);
				//jQuery('#linkEditorModal').modal('hide');	
			    }
			});
		} 
		
	//}
    });
     jQuery('body').on('click', '.open-modal', function () {
        var link_id = $(this).val();
        $.get('/v2/systemattribute/item/info/' + link_id, function (data1) {
	   if(data1.error==0){
		    var data = data1.data;
		    var pdata = data1.pdata;	
		    $('#id').val(data.id);
		    //jQuery('#file').val(data.attribute_value_image);
		    $('#attribute_id').val(data.attribute_id);
		    $('#sort_order').val(data.sort_order);	
		    $('#attribute_item_name').val(data.attribute_item_name);
		    $('#attribute_item_description').val(data.attribute_item_description); 
		    $('#attribute_item_image_class').val(data.attribute_item_image_class); 
		   // jQuery('#dependent_attribute_code').val(data.dependent_attribute_code);	
		    //jQuery('#dependent_attribute_item_id').val(data.dependent_attribute_item_id);
		    $('#attribute_item_code').val(data.attribute_item_code);
		    if(data.attribute_item_image!=''){
				var imgUrl = '<a href="'+data.attribute_item_image+'" target="_blank">Click here!</a> ';		
		    		$('.imgclss').html(imgUrl);	
				$('.imgclss').removeClass('hide');	
		    }	 	
		    if(data.status==0)jQuery('#instatus').attr('checked', true);
		    else jQuery('#status').attr('checked', true);
		     if(data.use_in_promo=='No')$('#is_default').attr('checked', true);
		    else $('#is_default').attr('checked', true);	

		    if(data.use_in_promo=='No')$('#is_required').attr('checked', true);
		    else $('#is_required').attr('checked', true);	

		    //$('select[name^="attribute_item_parent_id"] option:selected').attr("selected",null);
		    //$('select[name^="attribute_item_parent_id"] option[value="'+data.attribute_item_parent_id+'"]').attr("selected","selected");
		     var selected ='';	
		     var html = '<select name="attribute_item_parent_id" id="attribute_item_parent_id" >';
		     if(data.attribute_item_parent_id == 0){ console.log('parent')
		     		html +='<option value="0" '+selected+'>' + data.attribute_item_name + '</option>';	
		     }else {
			     $.each( pdata, function( key, dat ) {
				 if(dat.id == data.attribute_item_parent_id) selected = 'selected';
				 else selected = ''; 
				  //console.log(dat.id);
				 console.log('child')
				 html +='<option value="' + dat.id + '" '+selected+'>' + dat.attribute_item_name + '</option>';
			     });
		     	
		     }
		     html += '</select>';
		     $('.selectparent').html(html);		
		    $('#btn-save').val("update");//img
		    $('#linkEditorModal').modal('show');
	  }
        })
    });	
    ////----- DELETE a link and remove from the page -----////
    
     $('#attribute_item_name').keyup(function(){
	    var name = $('#attribute_item_name').val();
	    var skuName = name;
	     
	     var skuName = skuName.replace(/"/g, " Inch");
	     var skuName = skuName.toLowerCase();
	     var skuName = skuName.replace(/ /g, "_");
	     
	    	$('#attribute_item_code').val(skuName);    
	    
	});
	$('#attribute_item_name').focusout(function(){
	    var name = $('#attribute_item_name').val();
	    var skuName = name;
	     
	     var skuName = skuName.replace(/"/g, " Inch");
	     var skuName = skuName.toLowerCase();
	     var skuName = skuName.replace(/ /g, "_");
	     
	    	$('#attribute_item_code').val(skuName);    
	    
	});  	
});
function deleteItem(link_id) {
        //var link_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "DELETE",
            url: '/v2/systemattribute/item/delete/' + link_id,
            success: function (data) {
               location.reload();
                $("#link" + link_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    };
function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg').html(msg);	
         
}
function showMessageOnPopUp(msg , type){  
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg-popup').html(msg);	
         
}
</script>
<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
        tinymce.init({
            selector: 'textarea.editor',
            menubar: "tools",
            height: 320,
            theme: 'modern',
            plugins: 'code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar: 'code | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });
    });
</script> 
@endsection

