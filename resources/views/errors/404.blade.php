@extends('layouts.app')
@section('content')

<div class="container page-404 flex-verti-center">

    <div class="col-xs-12 text-center content-404">
        <span class="icon-frown"></span>
        <h2>404 - Oops, we couldn't<br/>find that page!</h2>
    </div>

</div>
@endsection