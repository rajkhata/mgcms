<!-- sidebar nav -->
{{--<div class="justify-content-left" style="width:17%; float:left;">
      <div class="card" style="margin-left:12px;margin-top:24px;">
            <div class="card-header">Menu</div>
            <div class="card-body">
      <ul>
        <li><a href="/">Dashboard</a></li>
        <li><a href="/users">Rest. Users</a></li>
        <li><a href="/restaurant">Restaurant:</a>
          <ul>
            <li><a href="/restaurant">Rest. list</a></li>
            <li>Rest. create</li>--}}{{--<a href="/restaurant/create"></a>--}}{{--
            </ul>
          </li>
          <li><a href="/pizzaexp">Pizza Exp:</a>
            <ul>
              <li><a href="/pizzaexp">PE list</a></li>
              <li><a href="/pizzaexp/create">PE Create</a></li>
            </ul>
          </li>
          <li><a href="/static">Static Pages:</a>
            <ul>
              <li><a href="/static">SP list</a></li>
              <li><a href="/static/create">SP create</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
</div>--}}
<div class="sidebar">

  <!--Sidebar Item-->


   {{-- <div class="side-item {{ (Request::is('floor') ? 'selected' : '') }}">
        <!-- <a href="/floor"> -->
      <div>
        <div class="title">
          <p>
            <span class="icon-floor"></span>
            Floor
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
          </p>
        </div>
      </div>
        <div class="sub_item {{Request::is('floor') ? 'selected' : ''}}">
            <a href="/floor">Listing</a>
        </div>
        <div class="sub_item {{ (Request::is('floor/configure') ? 'selected' : '') }}">
          <a href="/floor/welcome">Welcome</a>
        </div>
        <div class="sub_item {{ (Request::is('floor/configure') ? 'selected' : '') }}">
          <a href="/floor">Floor Setting</a>
        </div>
        <div class="sub_item {{Request::is('floor/create') ? 'selected' : ''}}">
          <a href="/floor/create">Create</a>
        </div>
        <div class="sub_item {{ (Request::is('floor/configure') ? 'selected' : '') }}">
          <a href="/table-management/floor-view/1">Floor2</a>
        </div>
        <div class="sub_item {{ (Request::is('floor/configure') ? 'selected' : '') }}">
          <a href="/floor/configure">Configure</a>
        </div>

      </div>--}}
    <!--Sidebar Item-->

    <div class="side-item {{ (Request::is('/')  ? 'selected' : '') }}">
      <a href="/">
        <div class="title">
          <p>
            <span class="icon-guest-book"></span>
            Dashboard
          </p>
        </div>
      </a>
    </div> <div class="side-item {{ (Request::is('users')  ? 'selected' : '') }}">
      <a href="/users">
        <div class="title">
          <p>
            <span class="icon-guest-book"></span>
            {{\Lang::get('sidebar.users')}}
          </p>
        </div>
      </a>
    </div><div class="side-item {{ (Request::is('restaurant')  ? 'selected' : '') }}">
      <a href="/restaurant">
        <div class="title">
          <p>
            <span class="icon-guest-book"></span>
             {{\Lang::get('sidebar.restaurant')}}
          </p>
        </div>
      </a>
    </div>
     <div class="side-item {{ (Request::is('pizzaexp') ? 'selected' : '') }}">
      <!-- <a href="/configure/working-hours"> -->
      <div>
        <div class="title">
          <p>
            <span class="icon-configure"></span>
            {{\Lang::get('sidebar.pizzaexp')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
          </p>
        </div>
      </div>
      <div class="sub_menu {{ (Request::is('pizzaexp') ? 'selected' : '')}}">
        <div class="sub_item {{Request::is('/pizzaexp/create') ? 'selected' : ''}}">
          <a href="/pizzaexp/create">{{\Lang::get('sidebar.pizzaexplist')}}</a>
        </div>
        <div class="sub_item {{ (Request::is('/pizzaexpitem/create') ? 'selected' : '') }}">
          <a href="/pizzaexpitem/create">{{\Lang::get('sidebar.pizzaexpsetting')}}</a>
        </div>
      </div>
    </div><div class="side-item {{ (Request::is('menu_category') || Request::is('menu_subcategory')  || Request::is('menu_meal_type')  || Request::is('menu_item') ? 'selected' : '') }}">
      <!-- <a href="/configure/working-hours"> -->
      <div>
        <div class="title">
          <p>
            <span class="icon-configure"></span>
            {{\Lang::get('sidebar.normalmenu')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
          </p>
        </div>
      </div>
      <div class="sub_menu {{ (Request::is('menu_category') ? 'selected' : '')}}">
        <div class="sub_item {{Request::is('menu_category') ? 'selected' : ''}}">
          <a href="/menu_category">{{\Lang::get('sidebar.categories')}}</a>
        </div>
        <div class="sub_item {{ (Request::is('menu_subcategory') ? 'selected' : '') }}">
          <a href="/menu_subcategory">{{\Lang::get('sidebar.subcategories')}}</a>
        </div>
      </div> <div class="sub_menu {{ (Request::is('menu_meal_type') ? 'selected' : '')}}">
        <div class="sub_item {{Request::is('menu_meal_type') ? 'selected' : ''}}">
          <a href="/menu_meal_type">{{\Lang::get('sidebar.meal_type')}}</a>
        </div>
        <div class="sub_item {{ (Request::is('menu_item') ? 'selected' : '') }}">
          <a href="/menu_subcategory">{{\Lang::get('sidebar.menu_item')}}</a>
        </div>
      </div>
    </div>
    <!--Sidebar Item-->
    <div class="side-item  {{ (Request::is('reservation') || Request::is('reservation/archive') ? 'selected' : '') }}">
      <a href="/reservation">
        <div class="title">
          <p>
            <span class="icon-reservation"></span>
            Reservations
          </p>
        </div>
      </a>
    </div>

    <!--Sidebar Item-->

    <div class="side-item {{ (Request::is('configure/working-hours')  || Request::is('configure/turnovertime') ? 'selected' : '') }}">
      <!-- <a href="/configure/working-hours"> -->
      <div>
        <div class="title">
          <p>
            <span class="icon-configure"></span>
            {{\Lang::get('sidebar.configure')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
          </p>
        </div>
      </div>
      <div class="sub_menu {{ (Request::is('configure/turnovertime') ? 'selected' : '')}}">
        <div class="sub_item {{Request::is('configure/turnovertime') ? 'selected' : ''}}">
          <a href="/configure/turnovertime">{{\Lang::get('sidebar.turnover_time')}}</a>
        </div>
        <div class="sub_item {{ (Request::is('configure/working-hours') ? 'selected' : '') }}">
          <a href="/configure/working-hours">{{\Lang::get('sidebar.hours')}}</a>
        </div>
      </div>
    </div>

  </div>
