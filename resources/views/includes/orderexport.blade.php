
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

   
<div class="modal fade" id="pdf_pop" role="dialog">
    <div class="modal-dialog vertical-center">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">


                <h5 class="modal-title text-left">Export Orders</h5>
            </div>
            <div class="modal-body">
                @if(Route::getCurrentRoute()->getName() !='home')
                <div class="row margin-bottom-30">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 text-left">
                    <input type="radio" name="1" class="export_type" value="all">
                    <label>Full Report</label>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 text-right">
                    <input type="radio" name="1" class="export_type" value="filtered" checked="checked">
                    <label>Filtered Report</label>
                    </div>
                </div>
                @endif
                <div class="row text-left edit-res-pop">


                    <div class="col-lg-6 col-xs-6 edit-label">Select From Date :</div>
                    <div class="col-lg-6 col-xs-6 text-right">
                        <div class="form-group1 pull-right">
                            <div class='input-group date'>
                                <input type="text" class="full-width down-btn datepickerF text-right datepicker_style btn btn__holo" id="datepickerF" readonly='true' value="">
<i class="fa fa-calendar position" aria-hidden="true"></i>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-xs-6 edit-label text-left">Select To Date :</div>
                    <div class="col-lg-6 col-xs-6 text-right">
                        <div class="form-group1 pull-right">
                            <div class='input-group date'>
                                <input type="text" class="full-width down-btn datepickerT text-right datepicker_style btn btn__holo" id="datepickerT" readonly='true' value="">
<i class="fa fa-calendar position" aria-hidden="true"></i>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer text-center">
                <div class="col-lg-6 col-xs-6 text-left padd-zero">
                    <a href="javascript:void(0)" data-dismiss="modal" class="btn btn__primary font-weight-600 down-btn " >Cancel</a>
                </div>
                <div class="text-right padd-zero">
                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 submit_download export_order" rel = "food_item-active" id="cmd"><i class="fa icon-export" aria-hidden="true"></i>Export</a>
                </div>
            </div>


        </div>

    </div>
</div>

<script>
	$('#pdf_pop').on('shown.bs.modal', function (e) {
        $("#datepickerF").datepicker({
            maxDate: 0,
            dateFormat: 'yy-mm-dd',
            onClose: function( selectedDate ) {
                $( "#datepickerT" ).datepicker( "option", "minDate", selectedDate );
           }
        }).datepicker('setDate', '-1m');


        $("#datepickerT").datepicker({
            maxDate: 0,
            dateFormat: 'yy-mm-dd',
            onClose: function( selectedDate ) {
              $("#datepickerF" ).datepicker( "option", "maxDate", selectedDate );
           }
        }).datepicker('setDate', 'today');
});
         $("body").on("click", ".modal-backdrop", function(e) {
                if ($(e.target).hasClass('modal-backdrop')) {
                    $('#pdf_pop').modal('hide');
                }
    });

</script>




