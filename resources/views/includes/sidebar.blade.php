<div class="sidebar box-shadow">
    <div class="menu-button">
        <a href="javascript:closeSibebarMenu(0)" class="menu-close"><i class="icon-arrow"></i></a>
        <a href="javascript:openSibebarMenu(0)" class="menu-open hide"><i class="icon-hamburger"></i></a>
    </div>
@php
    use App\Helpers\CommonFunctions;
    use Illuminate\Support\Facades\DB;
    use App\Models\Restaurant;
    use App\Models\UserOrder;

    $user = Auth::user();

    #Setting for Sidebar RG 24-20-2018
    if(!empty($user->restaurant_id)){
       $restarant_detail = Restaurant::find($user->restaurant_id);
    $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
    $reservation_locations = Restaurant::whereIn('id', $restArray)->select(DB::raw('sum(reservation) as reservation_count'), DB::raw('sum(is_merchandise_allowed) as merchandise_count'),DB::raw('sum(is_gift_card_allowed) as gift_card_count'),
                             DB::raw('sum(is_enquiry_reservation_allowed) as enquiry_resv_count'),DB::raw('sum(is_catering_allowed) as catering_count'), DB::raw('sum(is_contact_allowed) as contact_count'),DB::raw('sum(is_event_allowed) as event_count'),DB::raw('sum(is_career_allowed) as career_count'))->get()->toArray();

     #sidebar Count The Records
    /*$sidebarordercounts =UserOrder::select('user_orders.id')
        ->whereIn('user_orders.restaurant_id', $restArray)
        ->whereIn('user_orders.status', array("placed", "confirmed", "ready"))
        ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
        ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
        ->groupBy('user_orders.id')
        ->where('user_orders.product_type','=',"food_item")
        ->where('user_orders.is_order_viewed','=',"0")
        ->where(DB::raw('CONVERT_TZ(user_orders.created_at,"+00:00", cities.timezone_value)') ,'>=', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )
        ->get()->count();*/
  $notificationordercounts =UserOrder::select('user_orders.id')
        ->whereIn('user_orders.restaurant_id', $restArray)
        ->whereIn('user_orders.status', array("placed"))
        ->get()->count();

   $notificationorderId =UserOrder::select(['id','product_type','payment_receipt','order_type'])->whereIn('user_orders.restaurant_id', $restArray)
        ->whereIn('user_orders.status', array("placed"))->orderBy('id', 'DESC')->limit(1)
        ->get()->toArray();

        $sidebarordercounts='';
        $statusArray = array("placed");
$ScheduledorderCount =$ActiveorderCount=0;
     $ScheduledorderCount =UserOrder::select('user_orders.id')
        ->whereIn('user_orders.restaurant_id', $restArray)
        ->where('user_orders.is_order_viewed', 0)
        //->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
       // ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
        ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
        ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
       // ->whereRaw('DATE_ADD(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL 12 HOUR)>NOW()')
        ->groupBy('user_orders.id','user_orders.user_id','user_orders.order_type','user_orders.restaurant_id','user_orders.fname','user_orders.lname','user_orders.payment_receipt','user_orders.phone','user_orders.total_amount','user_orders.created_at',
            'user_orders.delivery_time','user_orders.delivery_date','user_orders.manual_update','user_orders.status','user_orders.product_type')->where('user_orders.product_type','=',"food_item")->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'<', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )->whereIn('user_orders.status', $statusArray)->get()->count();


 $ActiveorderCount =UserOrder::select('user_orders.id')
        ->whereIn('user_orders.restaurant_id', $restArray)
        ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
        ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
        ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
      ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
    //  ->whereRaw('DATE_ADD(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL 12 HOUR)>NOW()')
        ->groupBy('user_orders.id','user_orders.user_id','user_orders.order_type','user_orders.restaurant_id','user_orders.fname','user_orders.lname','user_orders.payment_receipt','user_orders.phone','user_orders.total_amount','user_orders.created_at',

            'user_orders.delivery_time','user_orders.delivery_date','user_orders.manual_update','user_orders.status','user_orders.product_type')->where('user_orders.product_type','=',"food_item")->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'>=', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )->whereIn('user_orders.status', $statusArray)->get()->count();


    }


    //echo $user->restaurant_id; die;
@endphp
@if ($user->role=="admin" || $user->role=="")

    {{--@if(count($floormenu))

    <!--Sidebar Item-->
        <div class="side-item {{ ((Request::is('floor/*') && !Request::is('floor/configure/*')) ? 'selected' : '') }}">
            <div>
                <div class="title">
                    <p>
                        <span class="icon-floor"></span>
                        Floor
                        <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                        <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
                    </p>
                </div>
            </div>
            <div class="sub_menu">
                @foreach ($floormenu as $floor)
                    <div class="sub_item {{Request::is('floor/'.$floor->id) ? 'selected' : ''}}">
                        <a href="/floor/{{$floor->id}}">{{$floor->name}}</a>
                    </div>
                @endforeach
            </div>
        </div>


    @else
        <div class="side-item {{ (Request::is('floor')  ? 'selected' : '') }}">
            <a href="/floor/welcome">
                <div class="title">
                    <p>
                        <span class="icon-floor"></span>
                        Floor
                    </p>
                </div>
            </a>
        </div>
    @endif--}}

    <!--Sidebar Item-->
        <div class="side-item {{ (Request::is('/')  ? 'selected' : '') }}">
            <a href="/">
            <span class="title">
                <span class="flex-box flex-direction-row text-nowrap">
                    <span class="icon-dashboard1"></span>
                    <span class="menu-text">Dashboard</span>
                </span>
            </span>
            </a>
        </div>
        <div class="side-item {{Request::is('deal-setup') ||   Request::is('media-setup') || Request::is('online-ordering-setup') || Request::is('contact') || Request::is('general-info-setup') || Request::is('content/pages') || Request::is('menu/all_items')  || Request::is('social-account-setup') || Request::is('enquiry/franchise') ? 'selected' : '' }}">
            <div>
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-online"></span>
                        <span class="menu-text">Website
                            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                               aria-hidden="true"></i>
                        </span>
                    </span>
                </span>
            </div>
            <div class="sub_menu {{ Request::is('general-info-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('general-info-setup') ? 'selected' : '' }}">
                    <a href="/general-info-setup">General info</a>
                </div>
            </div>
            {{-- <div class="sub_menu {{ Request::is('sitebuilder/design') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('sitebuilder/design') ? 'selected' : '' }}">
                    <a href="#">Design</a>
                </div>
            </div>--}}
            <div class="sub_menu {{ Request::is('content/pages') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('content/pages') ? 'selected' : '' }}">
                    <a href="/content/pages">Content</a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('menu/item') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('menu/item') ? 'selected' : '' }}">
                    <a href="/menu/item">Menu</a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('online-ordering-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ (Request::is('online-ordering-setup/') || Request::is('online-ordering-setup')) ? 'selected' : ''}}">
                    <a href="/online-ordering-setup">Ordering</a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('social-account-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('social-account-setup') ? 'selected' : '' }}">
                    <a href="/social-account-setup">Social Media</a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('media-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('media-setup') ? 'selected' : '' }}">
                    <a href="/media-setup">Design</a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('deal-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('deal-setup') ? 'selected' : '' }}">
                    <a href="/deal-setup">Promotions</a>
                </div>
            </div>
            {{--  <div class="sub_menu {{ Request::is('menu/all_items') ? 'selected' : ''}}">
                 <div class="sub_item {{ Request::is('menu/all_items') ? 'selected' : '' }}">
                     <a href="/menu/all_items">Menu</a>
                 </div>
             </div>--}}
            {{--<div class="sub_menu {{ Request::is('user_order') ? 'selected' : ''}}">
               <div class="sub_item {{ Request::is('user_order') ? 'selected' : '' }}">
                   <a href="/user_order">Ordering</a> <i class="fa fa-angle-right" aria-hidden="true"></i>
               </div>
           </div>--}}
            {{--<div class="sub_menu {{ Request::is('social-account-setup') ? 'selected' : ''}}">
               <div class="sub_item {{ Request::is('social-account-setup') ? 'selected' : '' }}">
                   <a href="/social-account-setup">Social Media</a>
               </div>
           </div>--}}
            <!-- <div class="sub_menu {{ Request::is('contact') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('contact') ? 'selected' : '' }}">
                    <a href="/contact">Domain</a>
                </div>
            </div> -->

        </div>

    <!--
    <div class="side-item {{ (Request::is('offers')  ? 'selected' : '') }}">
        <a href="/offers">
            <div class="title">
                <p>
                    <span class="icon-guest-book"></span>
                    Offers
                </p>
            </div>
        </a>
    </div>
    <div class="side-item {{ (Request::is('reviews-feedback')  ? 'selected' : '') }}">
        <a href="/reviews-feedback">
            <div class="title">
                <p>
                    <span class="icon-guest-book"></span>
                    Reviews & Feedback
                </p>
            </div>
        </a>
    </div>
-->
    {{--<div class="side-item {{ (Request::is('user_order') || Request::is('user_order/*/details/food_item') || Request::is('user_order/*/logs') || Request::is('archive_order')  ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p>
                    <span class="icon-floor"></span>
                    Order
                    <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                    <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
                </p>
            </div>
        </div>
        <div class="sub_menu">
            <div class="sub_item {{ (Request::is('user_order') || Request::is('user_order/*/details/food_item') || Request::is('user_order/*/logs') || Request::is('archive_order')  ? 'selected' : '') }}">
                <a href="/user_order">Orders</a>
            </div>
            <div class="sub_item {{ (Request::is('order-gift') ? 'selected' : '') }}">
                <a href="/order-gift">Gift Card Orders</a>
            </div>
        </div>
    </div>--}}

    <!--  <div class="side-item {{ (Request::is('user_order') || Request::is('user_order/*/details/food_item') || Request::is('user_order/*/food_item/logs') || Request::is('archive_order')  ? 'selected' : '') }}">
        <a href="/user_order">
            <div class="title">
                <p>
                    <span class="icon-floor"></span>
                    Orders
                </p>
            </div>
        </a>
    </div> -->


        <!-- <div class="side-item {{ (Request::is('user_order')  || Request::is('user_order/*/details/food_item') || Request::is('user_order/*/food_item/logs') || Request::is('archive_order') || Request::is('scheduled_order') ) ? 'selected' : '' }}">

            <div>
                <div class="title">
                    <p class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-floor"></span>
                        <span class="menu-text">Orders
                <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i></span>
                    </p>
                </div>
            </div>
            <div class="sub_menu {{ (Request::is('user_order') ? 'selected' : '')}}">

                <div class="sub_item {{ (Request::is('user_order') ? 'selected' : '') }}">
                    <a href="/user_order"
                       class="position-relative">Active <?php if (isset($ActiveorderCount) && $ActiveorderCount) {
                            echo ' <span class="notification__count">' . $ActiveorderCount . '</span>';
                        } ?></a>{{--<span class="notification__count">{{ $sidebarordercounts ? $sidebarordercounts : '' }}</span></a>--}}
                </div>
                <div class="sub_item {{ (Request::is('scheduled_order') ? 'selected' : '') }}">
                    <a href="/scheduled_order">Scheduled <?php if (isset($ScheduledorderCount) && $ScheduledorderCount) {
                            echo ' <span class="notification__count">' . $ScheduledorderCount . '</span>';
                        } ?></a>
                </div>
                <div class="sub_item {{ (Request::is('archive_order') ? 'selected' : '') }}">
                    <a href="/archive_order">Archived</a>
                </div>

            </div>

        </div> -->

        @if($reservation_locations['0']['gift_card_count'])
            <div class="side-item {{ (Request::is('order-gift')  || Request::is('order-gift/*') || Request::is('order-gift-archive') || Request::is('order-gift-archive/*') || Request::is('user_order/*/gift_card/logs') ? 'selected' : '') }}">

                <a href="/order-gift">
            <span class="title">
                <span class="flex-box flex-direction-row text-nowrap">
                    <span class="icon-gift"></span>
                    <span class="menu-text">Gift Card Orders</span>
                </span>
            </span>
                </a>
            </div>
        @endif
        @if($reservation_locations['0']['merchandise_count'] && $user->role=="admin")
            <div class="side-item {{ (Request::is('product_orders') || Request::is('user_order/*/details/product') || Request::is('product_archive_orders')   ? 'selected' : '') }}">
                <a href="/product_orders">
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-merchandise"></span>
                        <span class="menu-text">Merchandise</span>
                    </span>
                </span>
                </a>
            </div>
        @endif
    <!--<div class="side-item {{ (Request::is('menu_category') || Request::is('menu_subcategory')  || Request::is('menu_meal_type')  || Request::is('menu_item') || Request::is('menu_products') || Request::is('menu_giftcards') ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p class="padding-right-15">
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.normalmenu')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('menu_category') ? 'selected' : '')}}">
            <div class="sub_item {{Request::is('menu_category') ? 'selected' : ''}}">
                <a href="/menu_category">{{\Lang::get('sidebar.categories')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('menu_subcategory') ? 'selected' : '') }}">
                <a href="/menu_subcategory">{{\Lang::get('sidebar.subcategories')}}</a>
            </div>
        </div> <div class="sub_menu {{ (Request::is('menu_meal_type') ? 'selected' : '')}}">
            <div class="sub_item {{Request::is('menu_meal_type') ? 'selected' : ''}}">
                <a href="/menu_meal_type">{{\Lang::get('sidebar.meal_type')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('menu_item') ? 'selected' : '') }}">
                <a href="/menu_item">{{\Lang::get('sidebar.menu_item')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('menu_products') ? 'selected' : '') }}">
                <a href="/menu_products">{{\Lang::get('sidebar.menu_products')}}</a>
            </div>
            div class="sub_item {{ (Request::is('menu_giftcards') ? 'selected' : '') }}">
                <a href="/menu_giftcards">{{\Lang::get('sidebar.menu_giftcards')}}</a>
        </div>
    </div>
    <div class="side-item {{ (Request::is('manage-produts')  ? 'selected' : '') }}">
        <a href="/manage-produts">
            <div class="title">
                <p>
                    <span class="icon-guest-book"></span>
                    {{\Lang::get('sidebar.manage_produts')}}
            </p>
        </div>
    </a>
</div>
<div class="side-item {{ (Request::is('cms_user')  ? 'selected' : '') }}">
        <a href="/cms_user">
            <div class="title">
                <p>
                    <span class="icon-guest-book"></span>
                    CMS USERS
                </p>
            </div>
        </a>
    </div>
    <div class="side-item {{ (Request::is('users')  ? 'selected' : '') }}">
        <a href="/users">
            <div class="title">
                <p>
                    <span class="icon-guest-book"></span>
                    {{\Lang::get('sidebar.users')}}
            </p>
        </div>
    </a>
</div>

<div class="side-item {{ (Request::is('restaurant')  ? 'selected' : '') }}">
        <a href="/restaurant">
            <div class="title">
                <p>
                    <span class="icon-guest-book"></span>
                    {{\Lang::get('sidebar.restaurant')}}
            </p>
        </div>
    </a>
</div>

<div class="side-item {{ (Request::is('pizzaexp')  || Request::is('pizzaexpitem')  ? 'selected' : '')}}">
        <div>
            <div class="title">
                <p>
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.pizzaexp')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('pizzaexp') ? 'selected' : '')}}">
            <div class="sub_item {{Request::is('/pizzaexp') ? 'selected' : ''}}">
                <a href="/pizzaexp">{{\Lang::get('sidebar.pizzaexplist')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('/pizzaexpitem') ? 'selected' : '') }}">
                <a href="/pizzaexpitem">{{\Lang::get('sidebar.pizzaexpsetting')}}</a>
            </div>
        </div>
    </div>

    <div class="side-item {{ (Request::is('menu_setting') || Request::is('menu_setting_category')  || Request::is('menu_setting_item') ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p>
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.menu_setting')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('menu_setting_category') ? 'selected' : '')}}">
            <div class="sub_item {{Request::is('menu_setting_category') ? 'selected' : ''}}">
                <a href="/menu_setting_category">{{\Lang::get('sidebar.menu_setting_category')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('menu_setting_item') ? 'selected' : '') }}">
                <a href="/menu_setting_item">{{\Lang::get('sidebar.menu_setting_item')}}</a>
            </div>
        </div>
    </div>


    <div class="side-item {{ (Request::is('languages') || Request::is('localization')  ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p>
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.language_setting')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('languages') ? 'selected' : '')}}">
            <div class="sub_item {{Request::is('languages') ? 'selected' : ''}}">
                <a href="/languages">{{\Lang::get('sidebar.languages')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('localization') ? 'selected' : '') }}">
                <a href="/localization">{{\Lang::get('sidebar.localization')}}</a>
            </div>
        </div>
    </div>


    <div class="side-item {{ (Request::is('static')  ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p class="padding-right-20">
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.static_pages')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('static') ? 'selected' : '')}}">
            <div class="sub_item {{Request::is('static') ? 'selected' : ''}}">
                <a href="/static">{{\Lang::get('sidebar.static_pages_list')}}</a>
            </div>
            <div class="sub_item {{Request::is('static') ? 'selected' : ''}}">
                <a href="/block">{{\Lang::get('sidebar.static_block_list')}}</a>
            </div>
            <div class="sub_item {{Request::is('static') ? 'selected' : ''}}">
                <a href="/request_list">{{\Lang::get('sidebar.static_request_list')}}</a>
            </div>
        </div>
    </div>



    <div class="side-item {{ (Request::is('mailtemplate')  ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p>
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.templates')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('mailtemplate') ? 'selected' : '')}}">
            <div class="sub_item {{Request::is('mailtemplate') ? 'selected' : ''}}">
                <a href="/mailtemplate">{{\Lang::get('sidebar.mailtemplate')}}</a>
            </div>

        </div>
    </div>


    <div class="side-item {{ (Request::is('sms')  ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p>
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.manage_sms')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('sms') ? 'selected' : '')}}">
            <div class="sub_item {{Request::is('sms') ? 'selected' : ''}}">
                <a href="/sms">{{\Lang::get('sidebar.sms')}}</a>
            </div>

        </div>
    </div>
    <!-- Manage Country, State, City Rahul Gupta 10-07-2018 -->
    <!--<div class="side-item {{ (Request::is('countries/*') || Request::is('states/*')  || Request::is('cities/*')  ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p>
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.manage_countries')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('countries/*') ? 'selected' : '')}}">
            <div class="sub_item {{ (Request::is('countries/*') ? 'selected' : '') }}">
                <a href="/countries">{{\Lang::get('sidebar.country')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('menu_subcategory') ? 'selected' : '') }}">
                <a href="/menu_subcategory">{{\Lang::get('sidebar.states')}}</a>
            </div>
            <div class="sub_item {{Request::is('menu_meal_type') ? 'selected' : ''}}">
                <a href="/menu_meal_type">{{\Lang::get('sidebar.cities')}}</a>
            </div>
        </div>
    </div>-->
        <!-- Manage Country, State, City Rahul Gupta END -->

        {{--<div class="side-item {{ (Request::is('guestbook') || Request::is('guestbook/detail/*') ? 'selected' : '') }}">
            <a href="/guestbook">
                <div class="title">
                    <p>
                        <span class="icon-guest-book"></span>
                        Guest Book
                    </p>
                </div>
            </a>
        </div>--}}

    <!--Sidebar Item-->
        {{--<div class="side-item  {{ (Request::is('reservation') || Request::is('reservation/archive') ? 'selected' : '') }}">
            <a href="/reservation">
                <div class="title">
                    <p>
                        <span class="icon-reservations"></span>
                        Reservations
                    </p>
                </div>
            </a>
        </div>--}}

    <!--Sidebar Item-->
    <!--
        <div class="side-item  {{ (Request::is('server/listing') ? 'selected' : '') }}">
            <a href="/server/listing">
                <div class="title">
                    <p>
                        <span class="icon-Configure"></span>
                        {{\Lang::get('sidebar.server_list')}}
            </p>
        </div>
    </a>
</div><-->
        <!--Sidebar Item-->
    <!--<div class="side-item  {{ (Request::is('reports') ? 'selected' : '') }}">
            <a href="/reports">
                <div class="title">
                    <p>
                        <span class="icon-report"></span>
                        Reports
                    </p>
                </div>
            </a>
        </div>-->

        <!--Sidebar Item-->


    <!--<div class="side-item {{ ( Request::is('configure/carryout-hours') || Request::is('configure/delivery-hours') || Request::is('configure/operation-hours') ? 'selected' : '') }}">
        <div>
            <div class="title">
                <p>
                    <span class="icon-Configure"></span>
                    {{\Lang::get('sidebar.configure')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('floor') || Request::is('configure/carryout-hours') || Request::is('configure/delivery-hours') || Request::is('configure/operation-hours') ? 'selected' : '') }}">
            {{--<div class="sub_item {{ (Request::is('floor') ? 'selected' : '') }}">
                <a href="/floor">Floor</a>
            </div>
            <div class="sub_item {{Request::is('configure/tags') ? 'selected' : ''}}">
                <a href="/configure/tags">Tags</a>
            </div>
            <div class="sub_item {{Request::is('configure/turnovertime') ? 'selected' : ''}}">
                <a href="/configure/turnovertime">{{\Lang::get('sidebar.turnover_time')}}</a>
            </div>
            <div class="hidden-tablet sub_item {{ (Request::is('configure/working-hours') ? 'selected' : '') }}">
                <a href="/configure/working-hours">{{\Lang::get('sidebar.hours')}}</a>
            </div>--}}
            <div class="sub_item {{ (Request::is('configure/delivery-hours') ? 'selected' : '') }}">
                <a href="/configure/delivery-hours">{{\Lang::get('sidebar.delivery-hours')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('configure/carryout-hours') ? 'selected' : '') }}">
                <a href="/configure/carryout-hours">{{\Lang::get('sidebar.carryout-hours')}}</a>
            </div>
            <div class="sub_item {{ (Request::is('configure/operation-hours') ? 'selected' : '') }}">
                <a href="/configure/operation-hours">{{\Lang::get('sidebar.operational-hours')}}</a>
            </div>
            {{--<div class="sub_item {{ (Request::is('configure/global-settings') ? 'selected' : '') }}">
                <a href="/configure/global-settings">{{\Lang::get('sidebar.global_settings')}}</a>
            </div>--}}

            </div>
        </div>-->
        @if($reservation_locations['0']['enquiry_resv_count'] || $reservation_locations['0']['catering_count'] || $reservation_locations['0']['contact_count'] || $reservation_locations['0']['event_count'] || $reservation_locations['0']['career_count'])
            <div class="side-item {{ Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') || Request::is('enquiry/catering') || Request::is('enquiry/contact')  || Request::is('enquiry/events') || Request::is('enquiry/reservations') || Request::is('enquiry/career') || Request::is('enquiry/franchise') ? 'selected' : '' }}">
                <div>
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-enquiry"></span>
                        <span class="menu-text">Enquiry
                        <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                        <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                           aria-hidden="true"></i>
                        </span>
                    </span>
                </span>
                </div>
                @if($reservation_locations['0']['enquiry_resv_count'])
                    <div class="sub_menu {{ (Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') ? 'selected' : '')}}">
                        <div class="sub_item {{ (Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') ? 'selected' : '') }}">
                            <a href="<?php echo $reservation_locations['0']['reservation_count'] ? '/enquiry/reservations' : 'javascript:void(0)'?>">Reservations</a>
                        </div>
                    </div>
                @endif
                @if($reservation_locations['0']['catering_count'])
                    <div class="sub_menu {{ Request::is('enquiry/catering') ? 'selected' : ''}}">
                        <div class="sub_item {{ Request::is('enquiry/catering') ? 'selected' : '' }}">
                            <a href="/enquiry/catering">Catering</a>
                        </div>
                    </div>
                @endif
                @if($reservation_locations['0']['contact_count'])
                    <div class="sub_menu {{ Request::is('enquiry/contact') ? 'selected' : ''}}">
                        <div class="sub_item {{ Request::is('enquiry/contact') ? 'selected' : '' }}">
                            <a href="/enquiry/contact">Contact</a>
                        </div>
                    </div>
                @endif
                @if($reservation_locations['0']['event_count'])
                    <div class="sub_menu {{ Request::is('enquiry/events') ? 'selected' : ''}}">
                        <div class="sub_item {{ Request::is('enquiry/events') ? 'selected' : '' }}">
                            <a href="/enquiry/events">Events</a>
                        </div>
                    </div>
                @endif
                @if($reservation_locations['0']['career_count'])
                    <div class="sub_menu {{ Request::is('enquiry/career') ? 'selected' : ''}}">
                        <div class="sub_item {{ Request::is('enquiry/career') ? 'selected' : '' }}">
                            <a href="/enquiry/career">Careers</a>
                        </div>
                    </div>
                @endif

            </div>
        @endif
    @endif
    @if($user->role=="manager")
        <div class="side-item {{ (Request::is('/')  ? 'selected' : '') }}">
            <a href="/">
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-dashboard1"></span>
                        <span class="menu-text">Dashboard</span>
                    </span>
                </span>
            </a>
        </div>


        <div class="side-item {{Request::is('deal-setup') || Request::is('media-setup') || Request::is('online-ordering-setup')  || Request::is('online-ordering-setup/') || Request::is('contact') || Request::is('general-info-setup') || Request::is('content/pages') || Request::is('menu/all_items')  || Request::is('social-account-setup') || Request::is('enquiry/franchise') ? 'selected' : '' }}">
            <div>
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-online"></span>
                        <span class="menu-text">Website
                            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                               aria-hidden="true"></i>
                        </span>
                    </span>
                </span>
            </div>
            <div class="sub_menu {{ Request::is('general-info-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('general-info-setup') ? 'selected' : '' }}">
                    <a href="/general-info-setup">General info</a>
                </div>
            </div>
            {{-- <div class="sub_menu {{ Request::is('sitebuilder/design') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('sitebuilder/design') ? 'selected' : '' }}">
                    <a href="#">Design</a>
                </div>
            </div>--}}
            <div class="sub_menu {{ Request::is('content/pages') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('content/pages') ? 'selected' : '' }}">
                    <a href="/content/pages">Content</a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('menu/item') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('menu/item') ? 'selected' : '' }}">
                    <a href="/menu/item">Menu</a>
                </div>
            </div>

            <div class="sub_menu {{ (Request::is('online-ordering-setup/') || Request::is('online-ordering-setup')) ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('online-ordering-setup') ? 'selected' : '' }}">
                    <a href="/online-ordering-setup">Ordering</a>
                </div>
            </div>
            <!-- <div class="sub_menu {{ Request::is('media-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('media-setup') ? 'selected' : '' }}">
                    <a href="/media-setup">Media</a>
                </div>
            </div> -->
            <div class="sub_menu {{ Request::is('deal-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('deal-setup') ? 'selected' : '' }}">
                    <a href="/deal-setup">Promotions</a>
                </div>
            </div>
            {{--  <div class="sub_menu {{ Request::is('menu/all_items') ? 'selected' : ''}}">
                 <div class="sub_item {{ Request::is('menu/all_items') ? 'selected' : '' }}">
                     <a href="/menu/all_items">Menu</a>
                 </div>
             </div>--}}
            {{--<div class="sub_menu {{ Request::is('user_order') ? 'selected' : ''}}">
               <div class="sub_item {{ Request::is('user_order') ? 'selected' : '' }}">
                   <a href="/user_order">Ordering</a> <i class="fa fa-angle-right" aria-hidden="true"></i>
               </div>
           </div>--}}
            <div class="sub_menu {{ Request::is('social-account-setup') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('social-account-setup') ? 'selected' : '' }}">
                    <a href="/social-account-setup">Social Media</a>
                </div>
            </div>
            <!-- <div class="sub_menu {{ Request::is('contact') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('contact') ? 'selected' : '' }}">
                    <a href="/contact">Domain</a>
                </div>
            </div> -->

        </div>

        <!-- <div class="side-item {{ Request::is('user_order') || Request::is('scheduled_order') || Request::is('archive_order') ? 'selected' : '' }}">
            <div>
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-order"></span>
                        <span class="menu-text">Orders
                            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                               aria-hidden="true"></i>
                        </span>
                    </span>
                </span>
            </div>
            <div class="sub_menu {{ Request::is('user_order') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('user_order') ? 'selected' : '' }}">
                    <a href="/user_order"
                       class="position-relative">Active <?php if (isset($ActiveorderCount) && $ActiveorderCount) {
                            echo ' <span class="notification__count">' . $ActiveorderCount . '</span>';
                        } ?></a>{{--<span class="notification__count">{{ $sidebarordercounts ? $sidebarordercounts : '' }}</span></a>--}}
                </div>
            </div>
            <div class="sub_menu {{ Request::is('scheduled_order') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('scheduled_order') ? 'selected' : '' }}">
                    <a href="/scheduled_order">Scheduled<?php if (isset($ScheduledorderCount) && $ScheduledorderCount) {
                            echo ' <span class="notification__count">' . $ScheduledorderCount . '</span>';
                        } ?></a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('archive_order') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('archive_order') ? 'selected' : '' }}">
                    <a href="/archive_order">Archived</a>
                </div>
            </div>
        </div> -->
    
        <div class="side-item {{ Request::is('mng_order') ? 'selected' : '' }}">
            <div><a href="/mng_order/1/food_item">
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-order"></span>
                        <span class="menu-text">Orders</span>
                    </span>
                </span>
            </a></div>
        </div>
        <!-- <div class="side-item {{ (Request::is('guestbook') || Request::is('guestbook/detail/*') ? 'selected' : '') }}">
            <a href="/guestbook">
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-guest-book"></span>
                        <span class="menu-text">Guestbook</span>
                    </span>
                </span>
            </a>
        </div> -->
        @if($reservation_locations['0']['enquiry_resv_count'] || $reservation_locations['0']['catering_count'] || $reservation_locations['0']['contact_count'] || $reservation_locations['0']['event_count'] || $reservation_locations['0']['career_count'])
            <div class="side-item {{ Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') || Request::is('enquiry/catering') || Request::is('enquiry/contact')  || Request::is('enquiry/events') || Request::is('enquiry/reservations') || Request::is('enquiry/career') || Request::is('enquiry/franchise') ? 'selected' : '' }}">
                <div>
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-enquiry"></span>
                        <span class="menu-text">Enquiries
                        <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                        <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                           aria-hidden="true"></i>
                        </span>
                    </span>
                </span>
                </div>
                <?php /* ?>
                @if($reservation_locations['0']['enquiry_resv_count'])
                    <div class="sub_menu {{ (Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') ? 'selected' : '')}}">
                        <div class="sub_item {{ (Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') ? 'selected' : '') }}">
                            <a href= "<?php echo $reservation_locations['0']['reservation_count'] ? '/enquiry/reservations':'javascript:void(0)'?>">Reservations</a>
                        </div>
                    </div>
                @endif
                @if($reservation_locations['0']['catering_count'])
                    <div class="sub_menu {{ Request::is('enquiry/catering') ? 'selected' : ''}}">
                        <div class="sub_item {{ Request::is('enquiry/catering') ? 'selected' : '' }}">
                            <a href="/enquiry/catering">Catering</a>
                        </div>
                    </div>
                @endif

  <?php */ ?>
                @if($reservation_locations['0']['contact_count'])
                    <div class="sub_menu {{ Request::is('enquiry/contact') ? 'selected' : ''}}">
                        <div class="sub_item {{ Request::is('enquiry/contact') ? 'selected' : '' }}">
                            <a href="/enquiry/contact">Contact</a>
                        </div>
                    </div>
                @endif
                <?php /* ?>
                @if($reservation_locations['0']['event_count'])
                    <div class="sub_menu {{ Request::is('enquiry/events') ? 'selected' : ''}}">
                        <div class="sub_item {{ Request::is('enquiry/events') ? 'selected' : '' }}">
                            <a href="/enquiry/events">Events</a>
                        </div>
                    </div>
                @endif
                @if($reservation_locations['0']['career_count'])
                    <div class="sub_menu {{ Request::is('enquiry/career') ? 'selected' : ''}}">
                        <div class="sub_item {{ Request::is('enquiry/career') ? 'selected' : '' }}">
                            <a href="/enquiry/career">Careers</a>
                        </div>
                    </div>
                @endif
 <?php */ ?>

            </div>
        @endif

        <div class="side-item {{ Request::is('dashboard') || Request::is('report/social') || Request::is('report/marketing') || Request::is('report/locations') || Request::is('report/orders') || Request::is('report/reservations') || Request::is('report/visitors') ? 'selected' : '' }}">
            <div>
                <span class="title">
                    <span class="flex-box flex-direction-row text-nowrap">
                        <span class="icon-report"></span>
                        <span class="menu-text">Reports
                            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                               aria-hidden="true"></i>
                        </span>
                    </span>
                </span>
            </div>
            <div class="sub_menu {{ Request::is('dashboard') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('dashboard') ? 'selected' : '' }}">
                    <a href="/dashboard">Overview</a>
                </div>
            </div>
          {{--  <div class="sub_menu {{ Request::is('report/locations') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('report/locations') ? 'selected' : '' }}">
                    <a href="/report/locations">Locations</a>
                </div>
            </div>--}}
            <div class="sub_menu {{ Request::is('report/orders') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('report/orders') ? 'selected' : '' }}">
                    <a href="/report/orders">Orders</a>
                </div>
            </div>
            @if($restarant_detail->is_reservation_allowed)
            <div class="sub_menu {{ Request::is('report/reservations') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('report/reservations') ? 'selected' : '' }}">
                    <a href="/report/reservations">Reservations</a>
                </div>
            </div>
            @endif
            <div class="sub_menu {{ Request::is('report/visitors') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('report/visitors') ? 'selected' : '' }}">
                    <a href="/report/visitors">Site Visitors</a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('report/marketing') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('report/marketing') ? 'selected' : '' }}">
                    <a href="/report/marketing">Marketing Campaign</a>
                </div>
            </div>
            <div class="sub_menu {{ Request::is('report/social') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('report/social') ? 'selected' : '' }}">
                    <a href="/report/social">Social Media</a>
                </div>
            </div>

        </div>


    <!-- for demo of reservation, showing reservation menu for qc1@bravvura.in -->
        {{--@if($user->email=="qc1@bravvura.in")--}}
        @if($restarant_detail->reservation_type == config('reservation.reservation_type.full'))

            <div class="side-item  {{ (Request::is('server/listing') ? 'selected' : '') }}">
                <a href="/server/listing">
                        <span class="title">
                            <span class="flex-box flex-direction-row text-nowrap">
                                <span class="fa fa-users"></span>
                                {{\Lang::get('sidebar.server_list')}}
                            </span>
                        </span>
                </a>
            </div>

            @if(count($floormenu))

            <!--Sidebar Item-->
                <div class="side-item {{ ((Request::is('floor/*') && !Request::is('floor/configure/*')) ? 'selected' : '') }}">
                    <!-- <a href="/floor"> -->
                    <div>
                        <div class="title">
                            <p class="flex-box flex-direction-row text-nowrap">
                                <span class="icon-floor"></span>
                                Floor
                                <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                                <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                                   aria-hidden="true"></i>
                            </p>
                        </div>
                    </div>
                    <div class="sub_menu">
                        @foreach ($floormenu as $floor)
                            <div class="sub_item {{Request::is('floor/'.$floor->id) ? 'selected' : ''}}">
                                <a href="/floor/{{$floor->id}}">{{$floor->name}}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <div class="side-item {{ (Request::is('floor')  ? 'selected' : '') }}">
                    <a href="/floor/welcome">
                        <span class="title">
                            <span class="flex-box flex-direction-row text-nowrap">
                                <span class="icon-floor"></span>
                                <span class="menu-text">Floor</span>
                            </span>
                        </span>
                    </a>
                </div>
            @endif

            <div class="side-item  {{ (Request::is('reservation') || Request::is('reservation/archive') ? 'selected' : '') }}">
                <a href="/reservation">
                    <span class="title">
                        <span class="flex-box flex-direction-row text-nowrap">
                            <span class="icon-reservations"></span>
                            <span class="menu-text">Reservations</span>
                        </span>
                    </span>
                </a>
            </div>
            <div class="side-item {{ (Request::is('guestbook') || Request::is('guestbook/detail/*') ? 'selected' : '') }}">
                <a href="/guestbook">
                        <span class="title">
                            <span class="flex-box flex-direction-row text-nowrap">
                                <span class="icon-guest-book"></span>
                                <span class="menu-text">Guest Book</span>
                            </span>
                        </span>
                </a>
            </div>

            <div class="side-item {{ (Request::is('configure/working-hours')  || Request::is('configure/tags') || Request::is('configure/turnovertime')   || Request::is('floor') || Request::is('configure/carryout-hours')  || Request::is('configure/global-settings') || Request::is('configure/delivery-hours') || Request::is('configure/operation-hours') ? 'selected' : '') }}">

                <div>
                    <div class="title">
                        <p class="flex-box flex-direction-row text-nowrap">
                            <span class="icon-Configure"></span>
                            <span class="menu-text">{{\Lang::get('sidebar.configure')}}
                                <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                               aria-hidden="true"></i></span>
                        </p>
                    </div>
                </div>
                <div class="sub_menu {{ (Request::is('configure/turnovertime') ? 'selected' : '')}}">
                    <div class="sub_item {{ (Request::is('floor') ? 'selected' : '') }}">
                        <a href="/floor">Floor</a>
                    </div>
                    <div class="sub_item {{Request::is('configure/tags') ? 'selected' : ''}}">
                        <a href="/configure/tags">Tags</a>
                    </div>
                    <div class="sub_item {{Request::is('configure/turnovertime') ? 'selected' : ''}}">
                        <a href="/configure/turnovertime">{{\Lang::get('sidebar.turnover_time')}}</a>
                    </div>
                    <div class="hidden-only-tablet sub_item {{ (Request::is('configure/working-hours') ? 'selected' : '') }}">
                        <a href="/configure/working-hours">{{\Lang::get('sidebar.hours')}}</a>
                    </div>
                    <div class="sub_item {{ (Request::is('configure/global-settings') ? 'selected' : '') }}">
                        <a href="/configure/global-settings">{{\Lang::get('sidebar.global_settings')}}</a>
                    </div>

                </div>

            </div>

        @else


            {{--<div class="side-item {{ (Request::is('user_order') || Request::is('user_order/*/details/food_item') || Request::is('user_order/*/logs') || Request::is('archive_order')  ? 'selected' : '') }}">
                <div>
                    <div class="title">
                        <p>
                            <span class="icon-floor"></span>
                            Order
                            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
                        </p>
                    </div>
                </div>
                <div class="sub_menu">
                    <div class="sub_item {{ (Request::is('user_order') || Request::is('user_order/*/details/food_item') || Request::is('user_order/*/logs') || Request::is('archive_order')  ? 'selected' : '') }}">
                        <a href="/user_order">Orders</a>
                    </div>
                    <div class="sub_item {{ (Request::is('order-gift') ? 'selected' : '') }}">
                        <a href="/order-gift">Gift Card Orders</a>
                    </div>
                </div>
            </div>--}}
            {{--
             <div class="side-item {{ (Request::is('user_order')  || Request::is('user_order/*/details/food_item') || Request::is('user_order/*/logs') || Request::is('archive_order') || Request::is('scheduled_order') ) ? 'selected' : '' }}">

                 <div>
                     <div class="title">
                         <p class="flex-box flex-direction-row text-nowrap">
                         <span class="icon-floor position-relative">
                             <span class="notification__count">{{$sidebarordercounts}}</span>
                         </span>
                             <span class="menu-text">Orders
                         <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                         <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i></span>
                         </p>
                     </div>
                 </div>
                 <div class="sub_menu {{ (Request::is('user_order') ? 'selected' : '')}}">

                     <div class="sub_item {{ (Request::is('user_order') ? 'selected' : '') }}">
                         <a href="/user_order" class="position-relative">Active <span class="notification__count">{{$sidebarordercounts}}</span></a>
                     </div>
                     <div class="sub_item {{ (Request::is('scheduled_order') ? 'selected' : '') }}">
                         <a href="/scheduled_order">Scheduled</a>
                     </div>
                     <div class="sub_item {{ (Request::is('archive_order') ? 'selected' : '') }}">
                         <a href="/archive_order">Archived</a>
                     </div>

                 </div>

             </div>
             <div class="side-item {{ (Request::is('order-gift') ? 'selected' : '') }}">
                 <a href="/order-gift">
                     <div class="title">
                         <p>
                             <span class="icon-floor"></span>
                             Gift Card Orders
                         </p>
                     </div>
                 </a>
             </div>--}}
            @if($reservation_locations['0']['merchandise_count'] && $user->role=="admin")
                <div class="side-item {{ (Request::is('product_orders') || Request::is('user_order/*/details/product') || Request::is('user_order/*/product/logs')  || Request::is('product_archive_orders')   ? 'selected' : '') }}">
                    <a href="<?php echo $reservation_locations['0']['merchandise_count'] ? '/product_orders':'javascript:void(0)'?>">
                    <span class="title">
                        <span class="flex-box flex-direction-row text-nowrap">
                            <span class="icon-merchandise"></span>
                            <span class="menu-text">Merchandise</span>
                        </span>
                    </span>
                    </a>
                </div>
            @endif
            @if(in_array(Auth::user()->restaurant_id, array(30,33,34,35,36)))
                <div class="side-item {{ (Request::is('configure/working-hours')  || Request::is('configure/tags') || Request::is('configure/turnovertime')   || Request::is('floor') || Request::is('configure/carryout-hours')  || Request::is('configure/global-settings') || Request::is('configure/delivery-hours') || Request::is('configure/operation-hours') || (Request::is('configure/manage_services'))? 'selected' : '') }}">

                    <div>
                        <div class="title">
                            <p class="flex-box flex-direction-row text-nowrap">
                                <span class="icon-Configure"></span>
                                <span class="menu-text">{{\Lang::get('sidebar.configure')}}
                                    <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                               aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="sub_menu {{ (Request::is('configure/turnovertime') ? 'selected' : '')}}">
                    <!--  <div class="sub_item {{ (Request::is('floor') ? 'selected' : '') }}">
                        <a href="/floor">Floor</a>
                    </div>
                    <div class="sub_item {{Request::is('configure/tags') ? 'selected' : ''}}">
                        <a href="/configure/tags">Tags</a>
                    </div>
                    <div class="sub_item {{Request::is('configure/turnovertime') ? 'selected' : ''}}">
                        <a href="/configure/turnovertime">{{\Lang::get('sidebar.turnover_time')}}</a>
                    </div>
                    <div class="hidden-only-tablet sub_item {{ (Request::is('configure/working-hours') ? 'selected' : '') }}">
                        <a href="/configure/working-hours">{{\Lang::get('sidebar.hours')}}</a>
                    </div> -->
                    <!--   <div class="sub_item {{ (Request::is('configure/global-settings') ? 'selected' : '') }}">
                        <a href="/configure/global-settings">{{\Lang::get('sidebar.global_settings')}}</a>
                    </div> -->
                        <div class="sub_item {{ (Request::is('configure/manage_services') ? 'selected' : '') }}">
                            <a href="/configure/manage_services">{{\Lang::get('sidebar.manage_services')}}</a>
                        </div>

                    </div>

                </div>
            @endif
        <!-- RG 08-03-2019 New Menu Changes -->
            {{--
                        <div class="side-item {{ (Request::is('configure/working-hours')  || Request::is('configure/tags') || Request::is('configure/turnovertime')   || Request::is('floor') || Request::is('configure/carryout-hours')  || Request::is('configure/global-settings') || Request::is('configure/delivery-hours') || Request::is('configure/operation-hours') || (Request::is('configure/manage_services'))? 'selected' : '') }}">
                            <div>
                                <div class="title">
                                    <p class="flex-box flex-direction-row text-nowrap">
                                        <span class="icon-Configure"></span>
                                        <span class="menu-text">
                                            {{\Lang::get('sidebar.menu')}}
                                            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                                            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                            <div class="sub_menu {{ (Request::is('menu/all_items') ? 'selected' : '')}}">
                                <div class="sub_item {{ (Request::is('menu/all_items') ? 'selected' : '') }}">
                                    <a href="/menu/all_items">All Items</a>
                                </div>
                                 <div class="sub_item {{ (Request::is('menu/all_items') ? 'selected' : '') }}">
                                    <a href="/menu/schedules">Schedules</a>
                                </div>
                                 <div class="sub_item {{ (Request::is('menu/all_items') ? 'selected' : '') }}">
                                    <a href="/menu/modifiers">Modifiers</a>
                                </div>
                                 <div class="sub_item {{ (Request::is('menu/all_items') ? 'selected' : '') }}">
                                    <a href="/menu/labels">Labels</a>
                                </div>
                            </div>

                        </div>

                    --}}
        <!-- END -->
        <!--<div class="side-item {{ (Request::is('guestbook') || Request::is('guestbook/detail/*') ? 'selected' : '') }}">
            <a href="/guestbook">
                <div class="title">
                    <p>
                        <span class="icon-guest-book"></span>
                        Guest Book
                    </p>
                </div>
                </a>
        </div>
        <div class="side-item  {{ (Request::is('reports') ? 'selected' : '') }}">
            <a href="/reports">
                <div class="title">
                    <p>
                        <span class="icon-report"></span>
                        Reports
                    </p>
                </div>
            </a>
        </div>
        <div class="side-item {{ (Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') || Request::is('enquiry/catering') || Request::is('enquiry/contact')  || Request::is('enquiry/events') || Request::is('enquiry/reservations') || Request::is('enquiry/career')) ? 'selected' : '' }}">
            <div>
                <div class="title">
                    <p>
                        <span class="icon-enquiry"></span>
                            Enquiry
                        <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
                        <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close"
                           aria-hidden="true"></i>
                    </p>
                </div>
            </div>

            <div class="sub_menu {{ (Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') ? 'selected' : '')}}">
                <div class="sub_item {{ (Request::is('enquiry/reservations/*') || Request::is('enquiry/reservations') ? 'selected' : '') }}">
                    <a href= "<?php echo $reservation_locations['0']['reservation_count'] ? '/enquiry/reservations':'javascript:void(0)'?>" >Reservations</a>
                </div>
            </div>

            <div class="sub_menu {{ Request::is('enquiry/catering') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('enquiry/catering') ? 'selected' : '' }}">
                    <a href="/enquiry/catering">Catering</a>
                </div>
            </div>

            <div class="sub_menu {{ Request::is('enquiry/contact') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('enquiry/contact') ? 'selected' : '' }}">
                    <a href="/enquiry/contact">Contact</a>
                </div>
            </div>

            <div class="sub_menu {{ Request::is('enquiry/events') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('enquiry/events') ? 'selected' : '' }}">
                    <a href="/enquiry/events">Events</a>
                </div>
            </div>

             <div class="sub_menu {{ Request::is('enquiry/career') ? 'selected' : ''}}">
                <div class="sub_item {{ Request::is('enquiry/career') ? 'selected' : '' }}">
                    <a href="/enquiry/career">Career</a>
                </div>
            </div>
        </div>-->
        @endif
    @endif

    @if($user->role=="finance" || $user->role=="")
        @if($user->role=="finance")
            <div class="side-item {{ (Request::is('/')  ? 'selected' : '') }}">
                <a href="/">
                    <span class="title">
                        <span class="flex-box flex-direction-row text-nowrap">
                            <span class="icon-dashboard1"></span>
                            <span class="menu-text">Dashboard</span>
                        </span>
                    </span>
                </a>
            </div>
        @endif
    <!--
            <div class="side-item {{ (Request::is('manage_orders')  ? 'selected' : '') }}">
                <a href="/manage_orders">
                    <div class="title">
                        <p>
                            <span class="fas fa-utensils"></span>
                            Manage Orders
                        </p>
                    </div>
                </a>
            </div>
            <-->
    <!--<div class="side-item {{ (Request::is('configure/working-hours')  || Request::is('tags') || Request::is('configure/turnovertime')   || Request::is('floor') || Request::is('configure/carryout-hours') || Request::is('configure/delivery-hours') ? 'selected' : '') }}">

                <div>
                    <div class="title">
                        <p>
                            <span class="icon-Configure"></span>
                            {{\Lang::get('sidebar.configure')}}
            <i class="fa fa-angle-down acc-icon acc-icon-open" aria-hidden="true"></i>
            <i style="display:none;" class="fa fa-angle-up acc-icon acc-icon-close" aria-hidden="true"></i>
        </p>
    </div>
</div>
<div class="sub_menu {{ (Request::is('configure/turnovertime') ? 'selected' : '')}}">

                    <div class="sub_item {{ (Request::is('configure/delivery-hours') ? 'selected' : '') }}">
                        <a href="/configure/delivery-hours">{{\Lang::get('sidebar.delivery-hours')}}</a>
                    </div>
                    <div class="sub_item {{ (Request::is('configure/carryout-hours') ? 'selected' : '') }}">
                        <a href="/configure/carryout-hours">{{\Lang::get('sidebar.carryout-hours')}}</a>
                    </div>
                    <div class="sub_item {{ (Request::is('configure/operation-hours') ? 'selected' : '') }}">
                        <a href="/configure/operation-hours">{{\Lang::get('sidebar.operational-hours')}}</a>
                    </div>

                </div>

                <div class="sub_menu {{ (Request::is('static') ? 'selected' : '')}}">

                <!--  <div class="sub_item {{Request::is('static') ? 'selected' : ''}}">
                    <a href="/request_list">{{\Lang::get('sidebar.static_request_list')}}</a>
                </div>
                </div>

            </div>-->


    @endif

</div>
<div id="sidebar__overlay" class="overlay"></div>
<div id="sidebar__overlay1" class="overlay notification-msg" style="display:none;"><br><br>
    <div>
        <a href="/user_order" class="position-relative">Active Orders</a>
    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <span onclick="return closeNotification()" class="position-relative close icon-close gray"
          style="font-size:30px;font-weight:bold;"></span>
</div>
<div id="sidebar__overlay1 " class="overlay order_notification popupNotification overlay_popNoti" style="display:none;">
    <br><br>
    <div id="centercls" class="centercls">
        <?php if(isset($notificationorderId[0])) {?>
        <a href="/user_order/{{$notificationorderId[0]['id']}}/details/{{$notificationorderId[0]['product_type']}}"

           class="position-relative">You have a new {{$notificationorderId[0]['order_type']}} order. Receipt
            number: {{$notificationorderId[0]['payment_receipt']}}. Way to go!</a>

        <?php  }else {?>
        No New Order...
        <?php }  ?>
        <span onclick="return closeNotification()" class="close icon-close gray"
              style="font-size: 15px;font-weight:bold;position: absolute;top: 5px;right: 5px;"></span></div>

</div>

<script src='https://cdn.rawgit.com/admsev/jquery-play-sound/master/jquery.playSound.js'></script>
<script src='https://cdn.pubnub.com/sdk/javascript/pubnub.4.24.3.min.js'></script>
<script>

    //var count = {{$notificationordercounts}};
        <?php if(isset($notificationorderId[0])){ ?>
    var count = {{$notificationorderId[0]['id']}};
    var obj = <?php echo json_encode($notificationorderId[0]);?>
        //console.log(obj);
        callNotification(count, obj);
    <?php }  ?>
setAjaxCall();
    function closeNotification() {
        $('.order_notification').hide();
        $.stopSound();
    }
    function setAjaxCall() {

        setTimeout(function () { // PE-2087
            callNotificationAjax();
            setAjaxCall();

        }, 120000);   // As discussed with Prakash, NKD quick updates

    }
    function callNotificationAjax() {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
        $.ajax({
            type: 'GET',
            url: '/ordernotification',
            success: function (data) {
                //console.log(data);
                //var obj1 = JSON.parse(data);
                console.log(data);
                callNotification(data.id, data)
            }
        });
    }
    function callNotification(count, data) {

        var cookieValue = getCookie("notificationordercounts");
        setCookie("notificationordercounts", count, 365);
        //$('.order_notification').show();
        if (cookieValue == count) {
        } else {


            $('.centercls').html('<a href="/user_order/' + data.id + '/details/' + data.product_type + '" class="position-relative">You have a new ' + data.order_type + ' order. Receipt number: ' + data.payment_receipt + ' . Way to go!</a> <span onclick="return closeNotification()" class="close icon-close gray" style="font-size: 20px;font-weight:bold;position: absolute;top: 5px;right: 5px;"></span>');

            //$.playSound("https://dev-nkdcms.munchadoshowcase.biz/images/open-your-eyes-and-see.mp3");

            $('.order_notification').show();
            NotifyMe(data);
        }
    }
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function NotifyMe(data) {
//var notification = new Notification("I am Desktop Notification");
        if (!("Notification" in window)) {
            //alert("This browser does not support desktop notification");
        }
        else if (Notification.permission === "granted") {
            var options = {
                body: " You have a new " + data.order_type + "  order. Receipt number: " + data.payment_receipt + " . Way to go!",
                icon: "icon.jpg",
                dir: "ltr"
            };
            var notification = new Notification("Hi {{$user->name}}", options);
        }
        else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                if (!('permission' in Notification)) {
                    Notification.permission = permission;
                }

                if (permission === "granted") {
                    var options = {
                        body: " You have a new " + data.order_type + "  order. Receipt number: " + data.payment_receipt + " . Way to go!",
                        icon: "icon.jpg",
                        dir: "ltr"
                    };
                    var notification = new Notification("Hi {{$user->name}}", options);
                }
            });
        }
    }
    /*function render(){
     var that = this;
     this.model = new App.models.Notification();
     this.model.fetch({
     success: function(model, response, xhr){
     $(that.el).html(that.tmplNotiifcations(model.toJSON()));
     //$('#notification-icon').off('click');
     //$('#notification-icon').on('click',$.proxy(that.renderHistory,that));
     //$('#notification-time').html(response.datediff);
     that.renderNotifcations();
     that.getNotify();
     return that ;
     }
     });
     }
     function getNotify(){
     var that = this;
     PUBNUB.subscribe({
     channel  : "dashboard_" + {{$user->restaurant_id}},
     callback : function(pubnubMessage) {
     if(pubnubMessage.msg){
     $('.notification-msg').html(pubnubMessage.msg);
     that.notify(pubnubMessage.msg) ;
     that.notificatioTimer(1);
     that.notificationClass(pubnubMessage.type);
     that.renderNotifcations();
     if(pubnubMessage.type=='order'){
     that.orderReadCounter();
     //that.printOrderSummry(pubnubMessage.order_id);
     }
     }
     },
     error: function(e) {
     console.log(e);
     }
     });
     }
     render();
     getNotify();
     var pubnub = new PubNub({
     subscribeKey: "sub-c-88a8f7a2-a5f9-11e8-87b0-ca0e85b4e44e",
     publishKey: "pub-c-6bb88f76-4a5c-4e08-801a-f99c1bd5de3b",
     ssl: true
     });
     pubnub.subscribe({
     channels: ["dashboard_" + {{$user->restaurant_id}}],
     });*/


    $(document).on('click', function (event) {
        if (!$(event.target).closest('#centercls').length) {
            $('.order_notification').hide();
            $.stopSound();
        }
    });

</script>
