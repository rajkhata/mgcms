@php
    if(Auth::user()) {
        $curSymbol = Auth::user()->restaurant->currency_symbol;
    } else {
        $curSymbol = config('constants.currency');
    }
@endphp


<div class="modal fade" id="refundOrder" role="dialog" data-backdrop="static">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content white-smoke">
            <div class="modal-body">
                <form class="validation-msg" id="fund_submit" action="">

                    <div class="row">
                        <div class="col-lg-12 col-lg-offset-0 col-sm-12 col-xs-12 zero2">
                            <div class="order-title o-head text-center text-uppercase margin-bottom-30 font-weight-700">
                                Refund
                                @if($orderData->card_type=="Cash")
                                &nbsp;Cash
                                @else
                                &nbsp;Card
                                @endif
                                &nbsp;Payment
                            </div>
                            <div class="margin-top-30">
                                <div class="w_310 ch-m-bott pull-md-right pull-xs-right op-color">
                                    <div class="top_section padding-bottom-10 margin-top-10 margin-bottom-30">
                                        <div class="row margin-bottom-10">
                                            <div class="col-xs-5 text-right font-weight-700">Subtotal</div>
                                            <div class="col-xs-2 text-center">:</div>
                                            <div class="col-xs-5 text-left font-weight-700">{{ $curSymbol.$orderData->order_amount }}</div>
                                        </div>
                                        {{--@if($orderData->order_type =='delivery' && $action != 'gift_card')
                                            <div class="row margin-bottom-10">
                                                <div class="col-xs-5 text-right">Delivery Charge</div>
                                                <div class="col-xs-2 text-center">:</div>
                                                <div class="col-xs-5 text-left">{{ $curSymbol.number_format($orderData->delivery_charge,2) }}</div>
                                            </div>
                                        @endif
                                        <div class="row margin-bottom-10">
                                            <div class="col-xs-5 text-right">Tax</div>
                                            <div class="col-xs-2 text-center">:</div>
                                            <div class="col-xs-5 text-left">{{ $curSymbol.number_format($orderData->tax,2) }}</div>
                                        </div>
                                        @if($orderData->tip_amount!=0.00)
                                            <div class="row margin-bottom-10">
                                                <div class="col-xs-5 text-right">Tip</div>
                                                <div class="col-xs-2 text-center">:</div>
                                                <div class="col-xs-5 text-left">{{ $curSymbol.number_format($orderData->tip_amount,2) }}</div>
                                            </div>
                                        @endif--}}

                                        <?php
                                        if(count($order_other_details)){
                                            foreach ($order_other_details as $val){
                                        ?>

                                        <div class="row margin-bottom-10">
                                            <div class="col-xs-5 text-right">{{$val['name']}}</div>
                                            <div class="col-xs-2 text-center">:</div>
                                            <div class="col-xs-5 text-left"><?php echo $val['value']; ?></div>
                                        </div>
                                        <?php
                                            }
                                        }

                                        ?>




                                        @if($total_refunded_amount!=0.00)
                                            <div class="row margin-bottom-10">
                                                <div class="col-xs-5 text-right">Refunded Amount</div>
                                                <div class="col-xs-2 text-center">:</div>
                                                <div class="col-xs-5 text-left">-{{ $curSymbol.number_format($total_refunded_amount,2) }}</div>
                                            </div>
                                        @endif
                                        <div class="row food_order_item margin-bottom-10">
                                            <div class="col-xs-5 text-right font-weight-700">Grant Total</div>
                                            <div class="col-xs-2 text-center">:</div>
                                            <div class="col-xs-5 text-left font-weight-700">{{ $curSymbol.number_format($orderData->total_amount,2) }}</div>
                                        </div>
                                    </div>

                                    <div class="row margin-top-10">
                                        <div class="">
                                            <div class="col-lg-12 text-center margin-bottom-30">
                                                Are you sure, you want to refund the entire amount to the customer?
                                            </div>

                                            <div class="col-xs-12 margin-top-10 margin-bottom-30">
                                                <div class="col-xs-6 text-center">
                                                    <div class="tbl-radio-btn">
                                                        <input type="radio" name="refund_mode" id="refundAmtSizeSmall" value="fully" rel="fully" class="refund_status" checked="checked" >
                                                        <label for="refundAmtSizeSmall">Full Refund</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 text-center">
                                                    <div class="tbl-radio-btn">
                                                        <input type="radio" name="refund_mode" id="refundSizeSmall" value="partially" rel="partially" class="refund_status">
                                                        <label for="refundSizeSmall">Partial Refund</label>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <!-- Flat and Percentage Start-->
                                                <div class="row fpdiv hidden" style="margin-top: 16px;">
                                                    <div class="col-xs-offset-2 col-xs-8 no-padding">
                                                    <div class="col-xs-6 text-center no-padding">
                                                        <div class="tbl-radio-btn">
                                                            <input type="radio" name="refund_flat_or_percentage" id="refund_flat_or_percentage" value="flat" rel="flat" class="refund_fp" checked="checked" >
                                                            <label for="refund_flat_or_percentage">Amount</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 text-center no-padding">
                                                        <div class="row percentage-box">
                                                            <div class="col-xs-8 text-center no-padding">
                                                                <div class="tbl-radio-btn" style="margin-left: 10px;">
                                                                    <input type="radio" name="refund_flat_or_percentage" id="refundSizefpSmall" value="percentage" rel="percentage" class="refund_fp">
                                                                    <label for="refundSizefpSmall">Percentage</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-offset-1 col-xs-6 col-sm-2 no-padding text-center refund_fp_input hidden">
                                                                <input type="text" class="percentage_value number" maxlength="3" max="100" autocomplete="off" name="refund_fp_value" style="width: 100%; max-width:50px; border:1px solid #000;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    
                                                </div>
                                                <!-- Flat and Percentage Refund END-->
                                                <input type="hidden" class="" name="refund_type" value="F">
                                                
                                                <input type="hidden" class="" name="oid" value="{{$orderData->id}}">


                                                <div class="col-xs-12 text-center margin-top-20 margin-bottom-20 partial-amt" style="display: none;">
                                                    <div class="padding-left-right-15 padding-top-bottom-5">
                                                        <strong>{{ $curSymbol }}</strong> <input type="text" class="partial_amt" autocomplete="off" name="refund_value">
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-xs-offset-1 col-xs-10 margin-top-10">

                                        <div class="input__field col-xs-12 no-padding margin-bottom-20">
                                            <span>Host Name</span>
                                            <input id="hostName" class="required" type="text" name="host_name" value="{{$orderData->restaurant_name}}" style="margin-top:20px;">
                             
                                        </div>

                                        <div class="col-xs-12 no-padding">
                                            <span class="hasVal text-color-grey reason-refund-heading">Reason For Refund</span>

                                            <div class="selct-picker-plain">
                                                <select data-style="no-background-with-buttonline no-padding-left no-padding-top" name="reason" id="reason_id" class="selectpicker">

                                                    <option value="">Select Reason for Refund</option>
                                                    <option value="Customer called to cancel the order">Customer called to cancel the order</option>
                                                    <option value="Customer was not satisfied with the order">Customer was not satisfied with the order</option>
                                                    <option value="Some item(s) in the order were not available">Some item(s) in the order were not available</option>
                                                    <option value="The order cannot be fulfilled for the time requested">The order cannot be fulfilled for the time requested</option>
                                                    <option value="other">Other</option>
                                                </select>
                                            </div>

                                            <div class="custom__message-textContainer margin-top-10 no-margin-bottom" style="display: none;">
                                                <textarea placeholder="Add reason for Refund here" class="no-padding" maxlength="100" rows="2" id="other_reason" name="reason_other"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div id="error_refund1" class="col-md-12 col-xs-12 error_refund" style="font-size: 12px;
    color: #f00;
    text-align: center; padding-top: 10px;"></div>
                                <div class="col-md-12 col-xs-12 text-center margin-top-30 margin-bottom-30 zero2">
                                    <button type="submit" class="btn btn__primary min-width-180 font-weight-600 disable">Yes, Submit Refund</button>
                                    <button onClick="myclosepopup()" id="refundNoCancel" type="button" data-dismiss="modal" aria-label="Close" class="btn btn__holo min-width-180 margin-left-10 font-weight-600">No, Cancel</button>
                                </div>

                            </div>

                            <div id="errormess" style="display:none;">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/refund.js') }}"></script>

    <script type="text/javascript">
        // setTimeout(function(){ // PE-2087
        // location.reload(); // then reload the page.(3)
        // }, 80000);
          function myclosepopup()
          {
              var $radios = $('input:radio[name=refund_mode]');
             $radios.filter('[value=fully]').prop('checked', true);
              $('.fpdiv').addClass('hidden');
              $("#error_refund1").html("");

          }

        $(function() {


           /** var $radios = $('input:radio[name=refund_mode]');
            if($radios.is(':checked') === false) {
                $radios.filter('[value=fully]').prop('checked', true);
            }**/
            $(document.body).on('change','#reason_id',function() {
                var current_select = $(this).val();
                if(current_select == 'other') {
                    $('#other_reason').parent().show();
                }else {
                    $('#other_reason').parent().hide();
                }
            });

            //show Partial and fully            

            $(document.body).on('change','.refund_status',function() {

                var refund_mode = $(this).attr('rel');
                if(refund_mode == 'fully') {
                    $('.partial_amt').val('').parents('.partial-amt').hide();
                    $(".partial_amt + label.error").remove();
                    $('.fpdiv').addClass('hidden');
                }else {
                    $('.partial_amt').val('').parents('.partial-amt').show();
                    $('.fpdiv').removeClass('hidden');
                    $('.percentage_value').val('');
                    var selected_option = $('.refund_fp:checked').attr('rel');
                    if(selected_option == 'percentage') {
                        $('.partial_amt').val('').attr('readonly','readonly');
                    }else {
                        $('.partial_amt').val('').attr('readonly',false);
                    }
                }
            });


            // Refund Form submit

            $("#fund_submit").submit(function(e){

                var $form = $(this);
                if(!$form.valid()){
                    return false;
                }
                $("#error_refund1").html("");
                e.preventDefault();

                var inputData = $(this).serialize();
                $("#loader").removeClass('hidden');
              //  alert("hhh");
                 //return false;
               var reason = $("#reason_id").val();

                $.ajax({
                    url: '/release_order_refund',
                    type: 'post',
                    data: inputData,
                    success: function (data) {

                        $("#loader").addClass('hidden');
                        $(".disable").hide();
                        alertbox('Success',data.success, function(modal){

                            $(".disable").hide();
                             modal.on("hidden.bs.modal", function(e) {
                                 setTimeout(function () {
                                   $("#refundorder_success").hide();
                               /***********************refund data***************************************/
                               var url=  '<?php echo URL::to('user_order/' .$orderData->id. '/mngdetails/'.strtolower($orderData->product_type)); ?>';
                                     getOrderDetail(url);
                                /**********************************************/
                                }, 100);
                            
                                });
                        });
                        /**************send request page load****************************/


                        //var u ='<?php echo URL::to('user_order/' .$orderData->id. '/mngdetails/'.$orderData->product_type); ?>';
                      //  alert("v---------"+u);
                        //getOrderDetail('<?php echo URL::to('user_order/' .$orderData->id. '/mngdetails/'.$orderData->product_type); ?>');

                        /*********************************************************************/
                        // if(data.product_type == 'gift_card') {
                        //     window.location.href = '/order-gift/'+data.order_id;
                        // }else {
                        //     window.location.href = '/user_order/'+data.order_id+'/details/'+data.product_type;
                        // }
                       
                    },
                    error: function (data, textStatus, errorThrown) {

                    //    $("#loader").addClass('hidden');
                        var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';

                        });


                        $("#error_refund1").html(err);
                        // alertbox('Error',err,function(modal){
                        //     setTimeout(function() {
                        //     }, 2000);
                        // });


                    }

                });
                return false;
            });

            //show Partial and fully

            $(document.body).on('change','.refund_fp',function() {

                var refund_mode = $(this).attr('rel');
                if(refund_mode == 'percentage') {
                    $('.refund_fp_input').removeClass('hidden');
                    $('.partial_amt').val('').attr('readonly','readonly');
                }else {
                    $('.percentage_value').val('');
                    $('.partial_amt').val('').attr('readonly',false);
                    $('.refund_fp_input').addClass('hidden');
                }
            });

            // calculate the amount
            $(document.body).on('keyup','.percentage_value',function(){

                var percentage_val = $(this).val();
                var total_amount = '<?php echo $orderData->total_amount;?>';
                if(percentage_val <=100 && percentage_val > 0) {
                    var amt = ((percentage_val * total_amount) / 100).toFixed(2);
                    $('.partial_amt').val(amt);
                }else {
                    $('.partial_amt').val('');
                }
            });
        });

    </script>
