@extends('layouts.app')

@section('content')
<div class="main__container container__custom">

  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Countries <span class="user__currentPage">({{ $countries->currentPage() .'/' . $countries->lastPage() }})</span>&emsp;</h1>

    </div>

    <span><a href="{{ URL::to('countries/create') }}" class="btn btn__primary">Add</a></span>
    <!--<span style="float: right;">({{ $countries->count() .'/' . $countries->total() }})</span>-->
  </div>
  <div class="addition__functionality">
    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">

        <div class="row pr0 form_field__container">

          <div class="input__field">
            <div>
              <input id="name" class="search__input" type="text" autocomplete="off" name="country_name" value="{{isset($searchArr['name']) ? $searchArr['name'] : ''}}">
              <label for="name">Name</label>
            </div>
            <button class="btn btn__primary btn__search" type="submit" ><i class="fa fa-search"></i></button>
          </div>

        </div>
      </div>

    </div>
    {!! Form::close() !!}
    <div class="rest__user-wrapper">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif

      <div class="guestbook__container box-shadow">
        <div class="row guestbook__table-header hidden-xs hidden-sm hidden-tablet">
          <div class="col-md-1" style="font-weight: bold;">S.No</div>
          <div class="col-md-1" style="font-weight: bold;">Name</div>
          <div class="col-md-2" style="font-weight: bold;">Short Name</div>
          <div class="col-md-1" style="font-weight: bold;">Status</div>
          <div class="col-md-2" style="font-weight: bold;">Action</div>
        </div>


        @if(isset($countries) && count($countries)>0)
        <div class="guestbook__cutomer-details-wrapper">
          @foreach($countries as $key => $usr)
          <div class="row guestbook__customer-details-content desk__res hidden-xs hidden-sm hidden-tablet">
            <div class="col-md-1" >{{ $key+1 }}</div>
            <div class="col-md-1" >{{ $usr->country_name .' '. $usr->lname }}</div>
            <div class="col-md-2" >{{ $usr->country_short_name }}</div>
            <div class="col-md-1" >{{ ($usr->status==1)?'Active':($usr->status==0?'Inactive':'') }}</div>
            <div class="row col-md-2 rest__cta-container">
              <div class="col-md-6">
                <a href="{{ URL::to('countries/' . $usr->id . '/edit') }}" class="btn btn__primary edit"><span class="fa fa-pencil"></span>&nbsp;Edit</a>
              </div>
              <div class="col-md-6">
                {!! Form::open(['method' => 'DELETE', 'route' => ['countries.destroy', $usr->id]]) !!}
                @csrf
                <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</button>
                {!! Form::close() !!}
              </div>
            </div>
          </div>

          @endforeach

        </div>
        @else
        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
          No Record Found
        </div>
        @endif
      </div>

      @if(isset($countries) && count($countries)>0)
      <div class="pagination__wrapper" style="margin: 0px auto;">
        {{ $countries->appends($_GET)->links()}}
      </div>
      @endif
    </div>
  </div>



</div>
@endsection
