@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Country - Edit') }}</h1>

    </div>

  </div>
  <div class="card form__container">

    @if(session()->has('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}
    </div>
    @endif
    <div class="card-body">
      <div class="form__user__edit">

        {{ Form::model($countries, array('route' => array('countries.update', $countries->id))) }}
        @csrf


        <div class="form-group row form_field__container">
          <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('Country Name') }}</label>
          <div class="col-md-4">
            <div class="input__field">
              <input id="country_name" type="text" class=" {{ $errors->has('country_name') ? ' is-invalid' : '' }}"
              name="country_name" value="{{ count(old())==0 ? $countries->country_name : old('country_name') }}" required autocomplete="off">
            </div>
            @if ($errors->has('country_name'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('country_name') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group row form_field__container">
          <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Country Short Name') }}</label>
          <div class="col-md-4">
            <div class="input__field">
              <input id="country_short_name" type="text" class=" {{ $errors->has('country_short_name') ? ' is-invalid' : '' }}"
              name="country_short_name" value="{{ count(old())==0 ? $countries->country_short_name : old('country_short_name') }}" required autocomplete="off">
            </div>
            @if ($errors->has('country_short_name'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('country_short_name') }}</strong>
            </span>
            @endif
          </div>
        </div>

        <div class="form-group row form_field__container">
          <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
          <div class="col-md-4" style="padding-top: 10px;">
            <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1' || (count(old())==0 && $countries->status=='1')) ? 'checked' :'' }}> Active
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0' || (count(old())==0 && $countries->status=='0')) ? 'checked' :'' }}> Inactive
            @if ($errors->has('status'))
            <span class="invalid-feedback" style="display:block;">
              <strong>{{ $errors->first('status') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <br /><br />
        <div class="row text-center mb-0">
          <div class="text-center col-md-12">
            <button type="submit" class="btn btn__primary">{{ __('Save Country') }}</button>
          </div>
        </div>
      </form>

    </div>

  </div>
</div>

</div>
@endsection
