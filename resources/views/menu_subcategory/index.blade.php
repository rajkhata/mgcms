@extends('layouts.app')

@section('content')

<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Menu Subcategories<span style="margin:0px 5px;">({{ $menuSubcategories->currentPage() .'/' . $menuSubcategories->lastPage() }})</span>&emsp;</h1>
    </div>
    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('menu_subcategory/create') }}" class="btn btn__primary">Add</a></span>
  </div>
  <div class="addition__functionality">
    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">
      <div class="row form-group col-md-3">
        <select name="restaurant_id" id="restaurant_id" class="form-control" >
          <option value="">Select Restaurant</option>
          @foreach($restaurantData as $rest)
          <option value="{{ $rest->id }}" {{ (isset($searchArr['restaurant_id']) && $searchArr['restaurant_id']==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
          @endforeach
        </select>
      </div>
      <div class="row form-group col-md-3">
        <select name="menu_category_id" id="menu_category_id" class="form-control" >
          <option value="">Select Menu Category</option>
          @foreach($categoryData as $cat)
          <option value="{{ $cat->id }}" {{ (isset($searchArr['menu_category_id']) && $searchArr['menu_category_id']==$cat->id) ? 'selected' :'' }}>{{ $cat->name }}</option>
          @endforeach
        </select>
      </div>
      <div class="row form-group col-md-3">
        <div class="input__field">
          <input id="mName" type="text" name="name" value="{{isset($searchArr['name']) ? $searchArr['name'] : ''}}">
          <label for="mName">Name</label>
        </div>
      </div>
      <div class="row form-group col-md-1">
        <button class="btn btn__primary" style="margin-left: 5px;" type="submit" >Search</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>

  <div>

    <div class="active__order__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif

      <div class="guestbook__container box-shadow">
        <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
          <div class="row col-md-10">
            <div class="col-md-3" style="font-weight: bold;">Name</div>
            <div class="col-md-3" style="font-weight: bold;">Restaurant</div>
            <div class="col-md-3" style="font-weight: bold;">Menu Category</div>
            <div class="col-md-1" style="font-weight: bold;">Priority</div>
            <div class="col-md-2" style="font-weight: bold;">Status</div>
          </div>

          <div class="col-md-2">
            <div class="col-md-12" style="font-weight: bold;">Action</div>
          </div>

        </div>
        @if(isset($menuSubcategories) && count($menuSubcategories)>0)
        @foreach($menuSubcategories as $menuSubcategory)
        <div class="guestbook__customer-details-content rest__row">
          <div class="row col-md-10 order__row">
            <div class="col-md-3" style="margin: auto 0px;">{{ isset($menuSubcategory->name) ? $menuSubcategory->name : ''}}</div>
            <div class="col-md-3" style="margin: auto 0px;">{{ isset($menuSubcategory->Restaurant->restaurant_name) ? $menuSubcategory->Restaurant->restaurant_name : '' }}</div>
            <div class="col-md-3" style="margin: auto 0px;">{{ isset($menuSubcategory->MenuCategory->name) ? $menuSubcategory->MenuCategory->name : ''}}</div>
            <div class="col-md-1" style="margin: auto 0px;">{{ isset($menuSubcategory->priority) ? $menuSubcategory->priority : ''}}</div>
            <div class="col-md-2" style="margin: auto 0px;">{{ ($menuSubcategory->status==1)?'Active':($menuSubcategory->status==0?'Inactive':'') }}</div>
          </div>
          <div class="row col-md-2">
            <div class="col-md-12">
              <a style="width:100%;margin:10px 0;" href="{{ URL::to('menu_subcategory/' . $menuSubcategory->id . '/edit') }}" class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
            </div>
            <div class="col-md-12">
              {!! Form::open(['method' => 'DELETE', 'route' => ['menu_subcategory.destroy', $menuSubcategory->id]]) !!}
              @csrf
              <button style="width:100%;margin:10px 0;" class="btn btn__cancel" type="submit" onclick="return confirm('Do you really want to Delete ?')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
        @endforeach
        @else
        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
          No Record Found
        </div>
        @endif
      </div>
      @if(isset($menuSubcategories) && count($menuSubcategories)>0)
      <div style="margin: 0px auto;text-align: center;">
        {{ $menuSubcategories->appends($_GET)->links()}}
      </div>
      @endif
    </div>

  </div>
</div>
<script type="text/javascript">
$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $("#restaurant_id").change(function() {
    var rest_id = '';
    rest_id = $('#restaurant_id  option:selected').val();
    $('#menu_category_id').find('option').not(':first').remove();
    if(rest_id!='') {
      $('#loader').removeClass('hidden');
      $.ajax({
        type: 'POST',
        url: '<?php echo URL::to('/')."/get_category"; ?>',
        data: '_token = <?php echo csrf_token() ?>&rid='+rest_id,
        success: function (data) {
          $('#loader').addClass('hidden');
          if($.type(data.dataObj)!=='undefined')
          {
            if($.type(data.dataObj.cat)!=='undefined')
            {
              $.each(data.dataObj.cat, function(i, item) {
                $('#menu_category_id').append( '<option value="'+item.id+'">'+ item.name +'</option>' );
              });
            }
          }
        }
      });
    }
  });
    var currentURL=window.location.href.split('?')[1];
    var check_restaurant = '<?php echo $selected_rest_id;?>';
    if(check_restaurant != 0 && currentURL === undefined) { 
        // trigger restautaurnat checks
        $("#restaurant_id").val(check_restaurant).trigger('change'); 
        //$("#restaurant_id").attr('change');
    }

});
</script>
@endsection
