@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Menu Subcategory - Create') }}</h1>

    </div>
   

  </div>
  <div>

    <div class="card form__container">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit">
          <form method="POST" action="{{ route('menu_subcategory.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row form_field__container">
              <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
              <div class="col-md-4">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>
                  <option value="">Select Restaurant</option>
                  @foreach($groupRestData as $rest)
                  <optgroup label="{{ $rest['restaurant_name'] }}">
                    @foreach($rest['branches'] as $branch)
                    <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' : '' }}>{{ $branch['restaurant_name'] }}</option>
                    @endforeach
                  </optgroup>
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>

              <div class="form-group row form_field__container">
                  <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Default Language') }}</label>
                  <div class="col-md-4">
                      <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                          <option value="">Select Language</option>
                          @foreach($languageData as $lang)
                              <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                          @endforeach
                      </select>
                      @if ($errors->has('language_id'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('language_id') }}</strong>
                </span>
                      @endif
                  </div>
              </div>

            <div class="form-group row form_field__container">
              <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>
              <div class="col-md-4">
                <select name="menu_category_id" id="menu_category_id" class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}" >
                  <option value="">Select Category</option>
                  @if(session()->has('categoryData'))
                  @php
                  $categoryData = session()->get('categoryData');
                  @endphp
                  @foreach($categoryData as $cat)
                  <option value="{{ $cat->id }}" {{ (old('menu_category_id')==$cat->id) ? 'selected' :'' }}>{{ $cat->name }}</option>
                  @endforeach
                  @endif
                </select>
                @if ($errors->has('menu_category_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('menu_category_id') }}</strong>
                </span>
                @endif
              </div>
            </div>

              <div class="form-group row form_field__container">
              <label for="parent_sub_cat" class="col-md-4 col-form-label text-md-right">{{ __('Parent Sub Category') }}</label>
              <div class="col-md-4">
                <select name="parent" id="parent_sub_cat" class="form-control {{ $errors->has('parent') ? ' is-invalid' : '' }}" >
                  <option value="">Select Parent Sub Category</option>
                 
                </select>
                @if ($errors->has('parent'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('parent') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                  name="name" value="{{ old('name') }}" required>
                </div>
                @if ($errors->has('name'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
            </div>

              <div class="form-group row form_field__container">
                  <label for="pos_id" class="col-md-4 col-form-label text-md-right">{{ __('POS Id') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}"
                                 name="pos_id" value="{{ old('pos_id') }}" >
                      </div>
                      @if ($errors->has('pos_id'))
                          <span class="invalid-feedback"><strong>{{ $errors->first('pos_id') }}</strong></span>
                      @endif
                  </div>
              </div>

            <div class="form-group row form_field__container">
                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                <div class="col-md-4">
                    <div class="input__field">

                       <textarea maxlength="500" rows="4" name="description" id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                        >{{ old('description') }}</textarea>
                    </div>
                    @if ($errors->has('description'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Sub Description') }}</label>
                <div class="col-md-4">
                    <div class="input__field">

                       <textarea maxlength="500" rows="4" name="sub_description" id="sub_description" class="form-control{{ $errors->has('sub_description') ? ' is-invalid' : '' }}"
                                        >{{ old('sub_description') }}</textarea>
                    </div>
                    @if ($errors->has('sub_description'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('sub_description') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="image_class" class="col-md-4 col-form-label text-md-right">{{ __('Image Class') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="image_class" type="text" class="{{ $errors->has('image_class') ? ' is-invalid' : '' }}"
                  name="image_class" value="{{ old('image_class') }}" >
                </div>
                @if ($errors->has('image_class'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_class') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container source-image-div image-caption-div">
              <label for="image_svg" class="col-md-4 col-form-label text-md-right">{{ __('Image SVG') }}</label>
              <div class="col-md-4">
                <input id="image_svg" type="file" class="form-control{{ $errors->has('image_svg') ? ' is-invalid' : '' }}"
                value="{{ old('image_svg.0') }}" name="image_svg">
                @if ($errors->has('image_svg'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_svg') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container source-image-div image-caption-div">
              <label for="image_png" class="col-md-4 col-form-label text-md-right">{{ __('Image PNG (Not Selected)') }}</label>
              <div class="col-md-4">
                <input id="image_png" type="file" class="form-control{{ $errors->has('image_png') ? ' is-invalid' : '' }}"
                value="{{ old('image_png') }}" name="image_png">
                @if ($errors->has('image_png'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_png') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container source-image-div image-caption-div">
              <label for="image_png_selected" class="col-md-4 col-form-label text-md-right">{{ __('Image PNG (Selected)') }}</label>
              <div class="col-md-4">
                <input id="image_png_selected" type="file" class="form-control{{ $errors->has('image_png_selected') ? ' is-invalid' : '' }}"
                value="{{ old('image_png_selected') }}" name="image_png_selected">
                @if ($errors->has('image_png_selected'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('image_png_selected') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Priority') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="priority" type="text" class="{{ $errors->has('priority') ? ' is-invalid' : '' }}"
                  name="priority" value="{{ old('priority') }}" required>
                </div>
                @if ($errors->has('priority'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('priority') }}</strong>
                </span>
                @endif
              </div>
            </div>
            
              <div class="form-group row form_field__container">
                  <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is Popular</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                      <input id="is_popular" type="checkbox" class="" name="is_popular">
                  </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="is_popular" class="col-md-4 col-form-label text-md-right">Is Favourite</label>
                  <div class="col-md-4" style="padding-top: 10px;">
                      <input id="is_favourite" type="checkbox" class="" name="is_favourite">
                  </div>
              </div>

            <div class="form-group row form_field__container">
              <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-4" style="padding-top: 10px;">
                <input type="radio" id="status1" name="status" checked value="1" {{ (old('status')=='1') ? 'checked' :'' }}> Active
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }}> Inactive
                @if ($errors->has('status'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>
            </div>
              <div class="form-group row form_field__container">
                  <label for="promotional_banner_url" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Url') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="promotional_banner_url" type="text" class="{{ $errors->has('promotional_banner_url') ? ' is-invalid' : '' }}"
                                 name="promotional_banner_url" value="{{ old('promotional_banner_url') }}" >
                      </div>
                      @if ($errors->has('promotional_banner_url'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_url') }}</strong>
                </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row form_field__container">
                  <label for="promotional_banner_image" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Image') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="promotional_banner_image" type="file" class="form-control{{ $errors->has('promotional_banner_image') ? ' is-invalid' : '' }}"
                                 value="{{ old('promotional_banner_image') }}" name="promotional_banner_image">
                      </div>
                      @if ($errors->has('promotional_banner_image'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_image') }}</strong>
                </span>
                      @endif
                  </div>
              </div>
              <div class="form-group row form_field__container">
                  <label for="promotional_banner_image_mobile" class="col-md-4 col-form-label text-md-right">{{ __('Promotional Banner Image Mobile') }}</label>
                  <div class="col-md-4">
                      <div class="input__field">
                          <input id="promotional_banner_image_mobile" type="file" class="form-control{{ $errors->has('promotional_banner_image_mobile') ? ' is-invalid' : '' }}"
                                 value="{{ old('promotional_banner_image_mobile') }}" name="promotional_banner_image_mobile">
                      </div>
                      @if ($errors->has('promotional_banner_image_mobile'))
                          <span class="invalid-feedback">
                  <strong>{{ $errors->first('promotional_banner_image_mobile') }}</strong>
                </span>
                      @endif
                  </div>
              </div>

            <br /><br />
            <div class=" row  mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
$(function() {
    
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $("#restaurant_id").change(function() {
    var rest_id = '';
    rest_id = $(this).find('option:selected').val();
    console.log(rest_id);
    $('#menu_category_id').find('option').not(':first').remove();
    if(rest_id!='') {
        $('#loader').removeClass('hidden');
        $.ajax({
            type: 'POST',
            url: '<?php echo URL::to('/')."/get_category"; ?>',
            data: '_token = <?php echo csrf_token() ?>&rid='+rest_id,
            success: function (data) {
                $('#loader').addClass('hidden');
              if($.type(data.dataObj)!=='undefined')
              {
                if($.type(data.dataObj.cat)!=='undefined')
                {
                  $.each(data.dataObj.cat, function(i, item) {
                    $('#menu_category_id').append( '<option value="'+item.id+'">'+ item.name +'</option>' );
                  });
                }
              }
            }
        });
    }
  });
  $("#menu_category_id").change(function() {
    var rest_id = $("#restaurant_id").val();
    var menu_category_id = $(this).val();
   
    if(rest_id!='' && menu_category_id!='') {
        $('#loader').removeClass('hidden');
        $.ajax({
            type: 'GET',
            url: '<?php echo URL::to('/')."/get_sub_cats_group"; ?>',
            data: 'rid='+rest_id+'&menu_category_id='+menu_category_id,
            success: function (data) {
                $('#loader').addClass('hidden');
                
                if(data.data.length)
                {
                  var str='';
                   str+=data.data;
                   $('#parent_sub_cat').html(str);
                }else{
                   $('#parent_sub_cat').html('<option value="">Select Parent Sub Category</option>');

                }
               
            }
        });
    }
  });

    var check_restaurant = '<?php echo $selected_rest_id;?>';
    if(check_restaurant != 0) { 
        // trigger restautaurnat checks
        $("#restaurant_id").val(check_restaurant).trigger('change'); 
        //$("#restaurant_id").attr('change');
    }

});
</script>
@endsection
