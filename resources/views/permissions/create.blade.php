@extends('layouts.app')


@section('content')
    <div class="main__container container__custom clearfix">
        <div class="inner__container">
            <div class="reservation__atto">
                <div class="reservation__title">
                    <h1 class="module__title">Create New Permission</h1>
                </div>
                <!-- <div>
                    <a class="btn btn__primary" href="{{ route('permissions.index') }}"> Back</a>
                </div> -->
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger margin-top-20">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul style="list-style:none;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::open(array('route' => 'permissions.store','method'=>'POST')) !!}
            <div class="guestbook__container fullWidth box-shadow margin-top-0 margin-bottom-20" style="padding:10px!important;">
                <div class="_block permissionTopForm" style="margin-bottom: 20px;">
                    <div class="form-group form-inline">
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12" style="margin-top: 8px;">
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                       
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12" style="margin-top: 8px;">
                            {{Form::select('guard_name', $onlyGuards, '', array('placeholder'=>'Guard Name', 'class' => 'form-control')) }}
                        </div>
                       
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12" style="margin-top: 8px;">
                            {{Form::select('group_name', $onlyGroups, '', array('placeholder'=>'Group Name','class' => 'form-control')) }}
                        </div>

                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12" style="margin-top: 8px;">
                            <button type="submit" class="btn btn__primary">Submit</button>
                            <a class="btn btn__primary" href="{{ route('permissions.index') }}"> Back</a>
                        </div>
                    </div>
                </div>

                <div class="_block">
                    <div id="pannelBox" class="panel panel-default">
                        <div class="panel-heading permission__title"><strong>Permissions</strong></div>
                        <div class="panel-body">
                            <?php
                            $permissionGroup = 'N/A';
                            ?>

                            @foreach($permission as $key => $value)
                               <div class="options__title"><h5>{{ucfirst($key)}}</h5></div>
                                @foreach($value as $pkey =>$val)
                                   
                                <div class="option__wrapper">                                   
                                    <label>{{ ucwords($val['name']) }}</label>
                                </div>
                                    
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection