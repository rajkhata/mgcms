@extends('layouts.app')


@section('content')
    <div class="main__container container__custom clearfix">
        <div class="inner__container">
            <div class="reservation__atto">
                <div class="reservation__title">
                    <h1 class="module__title">Show Permission</h1>
                </div>
                <!-- <div>
                    <a class="btn btn__primary" href="{{ route('permissions.index') }}"> Back</a>
                </div> -->
            </div>


            <!-- new -->
            <div class="card form__container no-margin-top">
                <div style="text-align:left">
                    <a class="btn" href="{{ route('permissions.index') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </div>

                <div class="row newtype">
                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <strong>Name</strong>
                        <div class="newrel">{{ $permissions['name'] }}</div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <strong>Guard Name</strong>
                        <div class="newrel">{{ $permissions['guard_name'] }}</div>
                    </div>
                </div>
                <div class="row newtype">
                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <strong>Group Name</strong>
                        <div class="newrel">{{ $permissions['group_name'] }}</div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <strong>Created On</strong>
                        <div class="newrel">{{ $permissions['created_at'] }}</div>
                    </div>
                </div>
                <div class="row newtype">
                    <div class="col-xs-12 col-sm-5 margin-top-30 margin-bottom-10">
                        <strong>Updated On</strong>
                        <div class="newrel">{{ $permissions['updated_at'] }}</div>
                    </div>
                </div>
            </div>
            <!-- new -->

            <!-- <div class="text-align-right">
                <a class="btn btn__primary" href="{{ route('permissions.index') }}"> Back</a>
            </div>
            <div class="_block" style="margin-top:20px;border:1px solid #999999; width:100%;background:#ffffff">
                
                <div class="form-group" style="border-bottom: 1px solid #999999;height:40px;text-align:center;padding-top:10px; background-color:#cccccc">   
                    <strong>Name:</strong>
                    {{ $permissions['name'] }}
                </div>
                
                <div class="form-group" style="border-bottom: 1px solid #999999;height:40px;text-align:center;padding-top:10px;">
                    <strong>Guard Name:</strong>
                    {{ $permissions['guard_name'] }}
                </div>
                
                <div class="form-group" style="border-bottom: 1px solid #999999;height:40px;text-align:center;padding-top:10px;">
                    <strong>Group Name:</strong>
                    {{ $permissions['group_name'] }}
                </div>
                
                <div class="form-group" style="border-bottom: 1px solid #999999;height:40px;text-align:center;padding-top:10px;">
                    <strong>Created On:</strong>
                    {{ $permissions['created_at'] }}
                </div>
                
                <div class="form-group" style="height:30px;text-align:center;padding-top:10px;">
                    <strong>Updated On:</strong>
                    {{  $permissions['updated_at']  }}
                </div>
            </div> -->

            
        </div>
    </div>
@endsection