@extends('layouts.app')
@section('content')
    <div class="main__container container__custom clearfix">
        <div class="inner__container">
            <div class="reservation__atto">
                <div class="reservation__title">
                    <h1 class="module__title">Edit Permission</h1>
                </div>
                <div>
                    <a class="btn btn-primary" href="{{ route('permissions.index') }}"> Back</a>
                </div>
            </div>


            @if (count($errors) > 0)
                <div class="alert alert-danger margin-top-20">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul style="list-style:none;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            {!! Form::model($permissions, ['method' => 'PATCH','route' => ['permissions.update', $permissions['id']]]) !!}
            <div class="guestbook__container fullWidth box-shadow margin-top-0 margin-bottom-20" style="padding:10px!important;">
                <div class="_block permissionTopForm" style="margin-bottom: 20px;">
                    <div class="form-group form-inline">
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12" style="margin-top: 8px;">
                            {!! Form::text('name', $permissions['name'], array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                       
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12" style="margin-top: 8px;">
                            {{Form::select('guard_name', $onlyGuards, in_array($permissions['guard_name'], $onlyGuards) ? $permissions['guard_name']: false, array('placeholder'=>'Guard Name', 'class' => 'form-control')) }}
                        </div>
                       
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12" style="margin-top: 8px;">
                            {{Form::select('group_name', $onlyGroups, in_array($permissions['group_name'], $onlyGroups) ? $permissions['group_name'] : false, array('placeholder'=>'Group Name', 'class' => 'form-control')) }}
                        </div>

                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12" style="margin-top: 8px;">
                            <button type="submit" class="btn btn-success btn-block btn-justify" style='width:200px;'>Submit</button>
                            <a class="btn btn__primary" href="{{ route('permissions.index') }}"> Back</a>
                        </div>
                    </div>
            
                    <!-- <div class="_block text-center" style="margin-top: 8px;">
                        <button type="submit" class="btn btn-success btn-block btn-justify" style='width:200px;'>Submit</button>
                    </div> -->
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection