@extends('layouts.app')

@section('content')
    <div class="main__container container__custom">
        <div class="inner__container">
            <div class="reservation__atto">
                <div class="reservation__title">
                    <h1 class="module__title">Permissions Management</h1>
                </div>
                <div>
                    @can('permission-create')
                        <!-- <a class="btn btn__primary" href="{{ route('permissions.create') }}"> Create New Permission</a> -->
                    @endcan
                </div>
            </div>

            <div class="active__order__container" style="padding:10px;">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table id="myTable2" class="responsive ui cell-border hover " style="margin-bottom:10px;" width="100%">
                    <thead>
                    <tr id="firstTR">
                        <th>Name</th>
                        <th>Guard</th>
                        <th>Group</th>
                        <th class="action__columns">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($permission as $key => $prms)
                    
                        <tr>
                            <td>{{ ucwords($prms->name) }}</td>
                            <td>{{ ucwords($prms->guard_name) }}</td>
                            <td>{{ ucwords($prms->group_name) }}</td>
                            
                            <td>
                                <a class="btn btn__primary" href="{{ route('permissions.show',$prms->id) }}">
                                    View
                                </a>

                                <!--@can('permission-edit')
                                    <a class="btn btn__primary" href="{{ route('permissions.edit',$prms->id) }}">
                                        Edit
                                    </a>
                                @endcan

                                @can('permission-delete')
                                    {!! Form::open(['method' => 'DELETE','route' => ['permissions.destroy', $prms->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                @endcan-->
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

<script>
    $(document).ready(function() {
    $('#myTable2').DataTable( { 
        dom:'<"top"fiB><"bottom"trlp><"clear">',
        "order": [[0, "asc" ]],
        buttons: [{
            text: 'Create New Permission',
            action: function () {location.href = "permissions/create";}
        }],
        // buttons: [
        //     {extend: 'excelHtml5'}, 
        //     {extend: 'pdfHtml5'},   
        //     {               
        //         extend: 'print',
        //         exportOptions: {
        //             columns: ':visible'
        //         }
        //     },
        //     'colvis'
        // ],
        // columnDefs: [ {
        //     targets: -1,
        //     visible: true
        // }],
        language: {
            search: "",
            searchPlaceholder: "Search"
        },
        responsive: true
       
    } );
    $('#myTable').show();
    $('#myTable').removeClass('hidden');
} );
</script>
@endsection