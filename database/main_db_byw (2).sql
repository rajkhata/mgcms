-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 10, 2019 at 11:58 AM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.2.17-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `main_db_byw`
--

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(3, 'App\\Models\\CmsUser', 5),
(2, 'App\\Models\\CmsUser', 8);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `group_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', 'roles', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(2, 'role-create', 'web', 'roles', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(3, 'role-edit', 'web', 'roles', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(4, 'role-delete', 'web', 'roles', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(5, 'order-list', 'web', 'orders', '2019-09-30 13:00:00', '2019-10-07 13:00:00'),
(7, 'order-view', 'web', 'orders', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(8, 'refund', 'web', 'orders', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(9, 'General info', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(10, 'Content', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(11, 'Ordering', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(12, 'Media', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(13, 'Promotions', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(14, 'Social Media', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(15, 'Domain', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(19, 'Guestbook', 'web', 'guestbook', '2019-10-07 13:00:00', '2019-10-09 08:19:33'),
(20, 'Contact', 'web', 'enquiries', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(21, 'Overview', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(22, 'Locations', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(23, 'Reservations', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(24, 'Site Visitors', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(25, 'Marketing Campaign', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(26, 'Overview Report', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(27, 'Locations Report', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(28, 'Orders Report', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(29, 'Reservations Report', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(30, 'Site Visitors Report', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(31, 'Marketing Campaign Report', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(32, 'Social Media Report', 'web', 'reports', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(33, 'Guestbook-view', 'web', 'guestbook', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(34, 'Guestbook-edit', 'web', 'guestbook', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(35, 'Change-password', 'web', 'reset cms password', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(36, 'Update General Info', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(37, 'Menu List', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(38, 'Menu Edit', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(39, 'Menu Status', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(40, 'Menu Delete', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(41, 'Menu Add', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(42, 'Ordering Edit', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(43, 'Add Promotions', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(44, 'Add Coupon', 'web', 'website', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(45, 'permission-list', 'web', 'roles', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(46, 'permission-create', 'web', 'roles', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(47, 'permission-edit', 'web', 'roles', '2019-10-07 13:00:00', '2019-10-07 13:00:00'),
(48, 'permission-delete', 'web', 'roles', '2019-10-07 13:00:00', '2019-10-07 13:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2019-06-30 13:00:00', '2019-06-30 13:00:00'),
(2, 'Branch', 'web', '2019-01-21 01:54:50', '2019-01-21 01:54:50'),
(3, 'Brand', 'web', '2019-01-21 01:54:50', '2019-01-21 01:54:50'),
(4, 'Content Team', 'web', '2019-01-21 01:54:50', '2019-01-21 01:54:50'),
(5, 'Store Manager', 'web', '2019-01-21 01:54:50', '2019-01-21 01:54:50'),
(6, 'CRM', 'web', '2019-01-21 01:54:50', '2019-01-21 01:54:50');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(13, 3),
(14, 3),
(19, 3),
(20, 3),
(35, 3),
(37, 3),
(45, 3),
(46, 3),
(47, 3),
(48, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
