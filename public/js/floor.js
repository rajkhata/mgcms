/***** Floor View *****/

var blockSnapSize = 20,
    tableAvailableColor = "#ccc",
    stageWidth = 5 * (blockSnapSize * 6), //Default Floor Width 600,
    stageHeight = 5 * (blockSnapSize * 6), //Default Floor Height 600,
    centerTableXaxis = Math.round(blockSnapSize * 3), //60
    centerTableYaxis = Math.round(blockSnapSize * 3), //60
    tableColor = "#ccc",
    tableTextSize = Math.round(blockSnapSize/1.2), //26,
    chairOffset = Math.round(blockSnapSize * 2), //40,,
    tableIcon = "",
    tableIconColor = "white",
    sidebarStatusColor = "",
    chairSize = "",
    chairWidth = "26",
    chairColor = "#aaa",
    circleTableSize = "",
    movingTableWidth = "",
    movingTableHeight = "",
    tableTextWidth = "",
    tableTextXaxis = "",
    tableTextYaxis = "",
    tableLinkYaxis= "",
    tableSelectedColor = "#ff8700",
    enableDoubleClick = false,
    tableStroke = "white",
    serverFLName = "",
    serverColor = "#55B2DE",
    serverOffsetX = "",
    serverOffsetY = "",
    blockTableColor = "#ff0000",
    fontFamilyType = "Calibri,Arial"


var selectTablesIdForServers = [];
var finalSelectedTablesIdForServers = [];
var selectedTablesId = [];
var tableManagement = [];
var curArray = []
var finalArray = []

//window.localStorage.removeItem("reservationTimer");

function getServerAssignedTable() {

    finalSelectedTablesIdForServers = []

    var server_id = $('#server_id').val();
    var floor_id = $('#floor_name').val();
    var slot_id = $('.assign #slot_name').val();
    var slot_name = $('.assign #slot_name option:selected').html();
    if(slot_name!=undefined || slot_name!=null){
        slot_name = slot_name.trim()
    }
    var assign_date = $('#serverdatepicker_assign').val();
    var inputData = 'server_id=' + server_id + '&floor_id=' + floor_id + '&slot_id=' + slot_id + '&slot_name=' + slot_name + '&assign_date=' + assign_date;
    $.ajax(
        {
            url: '/server/assign/' + server_id,
            type: 'get',
            data: inputData,
            dataType: 'json',
            success: function (data) {
                floor_and_tables = data.data.floor_data;
                currentServerName = data.data.server_short_name;

                floorView(JSON.parse(floor_and_tables),"#floor-tables-view");
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });
                alertbox('Error', err, function (modal) {
                });
            },
            complete: function (data) {

            }
        });
}

//Stage
var stage = new Konva.Stage({
    container: "floor-tables-view",
    width: stageWidth,
    height: stageHeight
})

//Layer 1
var layer = new Konva.Layer();

function getStatus(obj, val) {
  var resp=false;
    $.each( obj, function( key, value ) {
        if (value === val) {
            resp= val;
        }
    });
    return resp;
}

function getIndexForDelete(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k].tableID == value) {
            array.splice(k, 1);
            return k;
        }
    }
    return false;
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function editCreateTable(array,status){

    stage.destroyChildren();

    var table;

    //console.log(array)

    $("#statusStyle").empty();

    $.each(array, function(key, value){

        //array[key]['reservations']['upcoming_reservation'] = "11:00 AM"

        var keyGroup = new Konva.Group({
            x: value.tableXAxis - Math.round(value.tableWidth / 2),
            y: value.tableYAxis - Math.round(value.tableHeight / 2),
            id: value.id,
            opacity: 1
        });

        var bottomGroup = new Konva.Group({
            x: value.tableBottomXAxis,
            y: value.tableBottomYAxis,
            width: value.tableWidth,
            height: value.tableHeight,
            offset: {
                x: Math.round(value.tableWidth / 2),
                y: Math.round(value.tableHeight / 2)
            },
            rotation: value.tableRotation
        });


        var bottomTableGroup = new Konva.Group();
        var bottomChairGroup = new Konva.Group();

        if(getStatus(status,value.tableAvailability) == value.tableAvailability){
            tableColor = JSON.parse(value.reservations[0]['status']['color']).table
            tableIcon = JSON.parse(value.reservations[0]['status']['icon']).table
            sidebarStatusColor = JSON.parse(value.reservations[0]['status']['color']).sidebar
        } else {
            tableColor = tableAvailableColor
        }

        var statusBtnStyle = ".bootstrap-select .btn.status-"+ value.tableAvailability +"{";
        statusBtnStyle += "background-color: "+ sidebarStatusColor +";";
        statusBtnStyle += "border-color: "+ sidebarStatusColor +";";
        statusBtnStyle += "outline: none !important";
        statusBtnStyle += "}";

        $("#statusStyle").append(statusBtnStyle)

        if(value.tableShape == "Round") {
            if (value.maxGuests <= 4) {
                circleTableSize = Math.round(blockSnapSize * 2) - Math.round(blockSnapSize / 2);
                chairSize = Math.round(blockSnapSize / 1.5)
                chairOffset = Math.round(blockSnapSize * 1.3)
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
                tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/2)
            } else if ((value.maxGuests > 4) && (value.maxGuests <= 6)) {
                circleTableSize = Math.round(blockSnapSize * 1.8)
                chairOffset = Math.round(blockSnapSize * 1.6)
                chairSize = Math.round(blockSnapSize / 1.5)
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
                tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/1.5)
            } else if ((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                circleTableSize = Math.round(blockSnapSize * 2.2)
                chairOffset = Math.round(blockSnapSize * 2)
                chairSize = Math.round(blockSnapSize / 1.5)
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
                tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/1.2)
            } else if ((value.maxGuests > 8) && (value.maxGuests <= 10)) {
                circleTableSize = Math.round(blockSnapSize * 2.8)
                chairOffset = Math.round(blockSnapSize * 2.6)
                chairSize = Math.round(blockSnapSize / 1.5)
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
                tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/0.9)
            }

            tableTextXaxis = centerTableXaxis - Math.round(blockSnapSize/2)

        }

        if(value.tableShape == "Square"){
            if((value.maxGuests <= 4) && (value.maxGuests < 5)){
                circleTableSize = Math.round(blockSnapSize * 2) - Math.round(blockSnapSize / 2);
                chairSize = Math.round(blockSnapSize/1.5)
                chairOffset = Math.round(blockSnapSize * 1.3)
                tableTextWidth = Math.round((blockSnapSize*2) + (blockSnapSize/2))
            }

            tableTextXaxis = centerTableXaxis - Math.round(blockSnapSize/2)
            tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/2)
        }

        if(value.tableShape == "Rectangle"){

            if(value.maxGuests <= 4){
                recTableWidth = Math.round(blockSnapSize*4)
                tableTextWidth = Math.round(blockSnapSize*3.5)

                tableTextXaxis = Math.round(centerTableXaxis/2) + Math.round(tableTextSize*1.8)

            } else if((value.maxGuests > 4) && (value.maxGuests <= 6)) {
                recTableWidth = Math.round(blockSnapSize*6)
                tableTextWidth = Math.round(blockSnapSize*5.3)
                recTableChairGapping = Math.round(blockSnapSize/2)

                tableTextXaxis = centerTableXaxis + Math.round(tableTextSize*1.05)

            } else if((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                recTableWidth = Math.round(blockSnapSize*8)
                tableTextWidth = Math.round(blockSnapSize*7.5)
                recTableChairGapping = Math.round(blockSnapSize/2)

                tableTextXaxis = centerTableXaxis + Math.round(tableTextSize*2.35)

            } else if((value.maxGuests > 8) && (value.maxGuests <= 10)) {
                recTableWidth = Math.round(blockSnapSize*10)
                tableTextWidth = Math.round(blockSnapSize*9.5)
                recTableChairGapping = Math.round(blockSnapSize/2)

                tableTextXaxis = centerTableXaxis + Math.round(tableTextSize*3.55)

            }

            chairOffset = Math.round(blockSnapSize * 1.35)
            chairSize = Math.round(blockSnapSize/1.5)
            tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/2)

        }

        tableTextYaxis = centerTableYaxis - Math.round(tableTextSize - 18)

        if(value.tableShape == "Round"){

            table = new Konva.Circle({
                id: "tablesId",
                x: centerTableXaxis,
                y: centerTableYaxis,
                radius: circleTableSize,
                fill: tableColor,
                stroke: 'white',
                strokeWidth: 2,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        } else if(value.tableShape == "Square"){

            table = new Konva.Rect({
                id: "tablesId",
                x: centerTableXaxis - Math.round(blockSnapSize * 1.5),
                y: centerTableYaxis - Math.round(blockSnapSize * 1.5),
                width: Math.round(blockSnapSize * 3),
                height: Math.round(blockSnapSize * 3),
                fill: tableColor,
                stroke: 'white',
                strokeWidth: 1,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        } else if(value.tableShape == "Rectangle"){

            table = new Konva.Rect({
                id: "tablesId",
                x: centerTableXaxis - Math.round(blockSnapSize * 1.5),
                y: centerTableYaxis - Math.round(blockSnapSize * 1.5),
                width: recTableWidth,
                height: Math.round(blockSnapSize * 3),
                fill: tableColor,
                stroke: "white",
                strokeWidth: 1,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        }

        bottomTableGroup.add(table)
        bottomGroup.add(bottomTableGroup)

        var topGroup = new Konva.Group();
        topGroup.setListening(false);

        var topShowTimerGroup = new Konva.Group();
        topShowTimerGroup.setListening(false);


        if(!$(".main__container").hasClass("server-tables-assignment")) {

            /*** Start Show Hide Timer ***/

            if (value.tableShape == "Round") {

                var showHideTableText = new Konva.Text({
                    x: centerTableXaxis - Math.round(tableTextSize * 1.45),
                    y: centerTableYaxis - Math.round(tableTextSize + 1),
                    text: value.tableText,
                    width: tableTextWidth,
                    align: 'center',
                    fontSize: Math.round(tableTextSize / 1.4),
                    fontFamily: fontFamilyType,
                    fill: tableColor
                });

                var showHideTableTime = new Konva.Text({
                    x: centerTableXaxis - Math.round(tableTextSize * 1.45),
                    y: centerTableYaxis - Math.round(tableTextSize - 22),
                    text: getShowHideTimer(value.timer[0]),
                    width: tableTextWidth,
                    align: 'center',
                    fontSize: Math.round(tableTextSize / 1.7),
                    fontFamily: fontFamilyType,
                    fill: 'white'
                });

                var topSemiPart = new Konva.Wedge({
                    x: centerTableXaxis,
                    y: centerTableYaxis - 0.5,
                    radius: 26,
                    angle: 180,
                    fill: 'black',
                    rotation: -180,
                });

                var bottomSemiPart = new Konva.Wedge({
                    x: centerTableXaxis,
                    y: centerTableYaxis + 0.5,
                    radius: 26,
                    angle: 180,
                    fill: 'black',
                    rotation: -0,
                });
            }

            if ((value.tableShape == "Square") || (value.tableShape == "Rectangle")) {

                var timerXaxis = "0";
                var timerYaxis = centerTableYaxis - Math.round(tableTextSize - 21);

                if (value.tableShape == "Rectangle") {
                    if ((value.maxGuests <= 4) || ((value.maxGuests > 6) && (value.maxGuests <= 8)) || ((value.maxGuests > 8) && (value.maxGuests <= 10))) {
                        timerXaxis = centerTableXaxis - Math.round(tableTextSize * 1.5)
                    } else {
                        timerXaxis = centerTableXaxis - Math.round(tableTextSize * 1.4)
                    }
                } else {
                    timerXaxis = centerTableXaxis - Math.round(tableTextSize * 1.45)
                }

                var showHideTableText = new Konva.Text({
                    x: centerTableXaxis - Math.round(tableTextSize * 1.45),
                    y: centerTableYaxis - Math.round(tableTextSize / 1.1),
                    text: value.tableText,
                    width: tableTextWidth,
                    align: 'center',
                    fontSize: Math.round(tableTextSize / 1.4),
                    fontFamily: fontFamilyType,
                    fill: tableColor
                });

                var showHideTableTime = new Konva.Text({
                    x: timerXaxis,
                    y: timerYaxis,
                    text: getShowHideTimer(value.timer[0]),
                    width: tableTextWidth,
                    align: 'center',
                    fontSize: Math.round(tableTextSize / 1.7),
                    fontFamily: fontFamilyType,
                    fill: 'white'
                });

                var topSemiPart = new Konva.Rect({
                    x: Math.round((table.attrs.width + 14) / 2), //centerTableXaxis - Math.round(blockSnapSize + 3),
                    y: centerTableYaxis - Math.round(blockSnapSize - 1),
                    width: 45,
                    height: 18,
                    fill: 'black'
                });

                var bottomSemiPart = new Konva.Rect({
                    x: Math.round((table.attrs.width + 14) / 2),
                    y: centerTableYaxis,
                    width: 45,
                    height: 18,
                    fill: 'black'
                });

            }

            /*** End Show Hide Timer ***/

        }

        if(getStatus(status,value.tableAvailability) == value.tableAvailability){

            var tableText = new Konva.Text({
                x: centerTableXaxis - Math.round(tableTextSize*1.45), //14,
                y: centerTableYaxis - Math.round(tableTextSize - 20), //14,
                text: value.tableText,
                width: tableTextWidth,
                align: 'center',
                fontSize: tableTextSize,
                fontFamily: fontFamilyType,
                fill: 'white'
            });

            var tableIcon = new Konva.Path({
                x: tableTextXaxis,
                y: tableTextYaxis,
                data: tableIcon,
                fill: tableIconColor,
                scale: {
                    x: 0.02,
                    y: -0.02
                },
                rotation:0
            });

            $.each(value.reservations, function(key,value){
                if(value.table_id.indexOf(',') > -1){

                    var tableLink = new Konva.Path({
                        x: tableTextXaxis,
                        y: tableLinkYaxis,
                        data: "M384 368s30.944 56.432 96-32c0 0 92.4-27.456 144 0s256 232.848 304 304c16.297 24.845 25.994 55.29 25.994 88s-9.697 63.155-26.374 88.617l0.379-0.617c-18.88 24.688-84.4 83.488-128 80-56.689-1.894-107.393-26.042-143.939-63.937l-0.061-0.063c-48.224-49.936-176-160-176-160s-54.704-25.6-48 32 211.504 220.592 240 240 159.152 18.768 192 0 107.2-14.896 160-160c0 0 24.512-148-112-272s-202.96-238.016-272-256-257.36 2.912-256 112zM640 523.088s-30.864-56.16-95.776 31.84c0 0-92.8 32.4-144.224 5.072s-254.912-236.8-302.72-307.568c-16.259-24.711-25.935-55.009-25.935-87.568s9.676-62.857 26.31-88.176l-0.375 0.608c18.832-24.576 84.208-83.088 127.696-79.6 56.529 1.872 107.106 25.897 143.609 63.623l0.055 0.057c48.096 49.696 175.584 159.216 175.584 159.216s54.576 25.488 47.888-31.84-211.008-219.504-239.44-238.816-158.784-18.672-191.552 0-106.912 14.864-159.616 159.2c0 0-24.448 147.2 111.744 270.656s202.496 236.8 271.36 254.736 256.752-2.912 255.392-111.44zM320 928h16c17.673 0 32-14.327 32-32v-128c0-17.673-14.327-32-32-32h-16c-17.673 0-32 14.327-32 32v128c0 17.673 14.327 32 32 32zM140.8 828.112l10.992 11.632c5.846 6.176 14.102 10.019 23.256 10.019 8.517 0 16.257-3.327 21.991-8.753l-0.015 0.014 93.040-87.904c6.176-5.846 10.019-14.102 10.019-23.256 0-8.517-3.327-16.257-8.753-21.991l0.014 0.015-10.992-11.632c-5.846-6.176-14.102-10.019-23.256-10.019-8.517 0-16.257 3.327-21.991 8.753l0.015-0.014-93.104 87.904c-6.156 5.844-9.986 14.087-9.986 23.224 0 8.533 3.34 16.285 8.783 22.022l-0.013-0.014zM58.528 639.536l-0.448 16c-0.008 0.271-0.013 0.589-0.013 0.909 0 17.351 13.81 31.477 31.038 31.986l0.047 0.001 128 3.632c0.271 0.008 0.589 0.013 0.909 0.013 17.351 0 31.477-13.81 31.986-31.038l0.001-0.047 0.448-16c0.008-0.271 0.013-0.589 0.013-0.909 0-17.351-13.81-31.477-31.038-31.986l-0.047-0.001-128-3.632c-0.271-0.008-0.589-0.013-0.909-0.013-17.351 0-31.477 13.81-31.986 31.038l-0.001 0.047zM656 144.32h14.4c17.673 0 32-14.327 32-32v0-128.32c0-17.673-14.327-32-32-32v0h-14.4c-17.673 0-32 14.327-32 32v0 128.32c0 17.673 14.327 32 32 32v0zM862.64 36.032l-10.768-11.648c-5.663-6.179-13.771-10.039-22.78-10.039-8.383 0-15.985 3.342-21.547 8.766l0.006-0.006-91.2 88c-6.064 5.919-9.825 14.174-9.825 23.308 0 8.495 3.253 16.23 8.582 22.028l-0.021-0.023 10.768 11.648c5.663 6.179 13.771 10.039 22.78 10.039 8.383 0 15.985-3.342 21.547-8.766l-0.006 0.006 91.2-88c6.064-5.919 9.825-14.174 9.825-23.308 0-8.495-3.253-16.23-8.582-22.028l0.021 0.023zM927.472 240.896l0.448-16c0.014-0.347 0.021-0.754 0.021-1.162 0-17.087-13.493-31.022-30.405-31.732l-0.064-0.002-125.344-3.632c-0.168-0.003-0.366-0.005-0.564-0.005-17.304 0-31.365 13.88-31.659 31.113v0.028l-0.448 16c-0.015 0.362-0.023 0.786-0.023 1.212 0 17.070 13.467 30.995 30.357 31.73l0.066 0.002 125.392 3.552c0.168 0.003 0.366 0.005 0.565 0.005 17.292 0 31.347-13.862 31.659-31.080v-0.029z",
                        fill: value.tables_link_color, //"#da98ea",
                        scale: {
                            x: 0.016,
                            y: -0.016
                        },
                        rotation:0
                    });

                    topGroup.add(tableText,tableIcon,tableLink)

                } else {
                    topGroup.add(tableText,tableIcon)
                }
            })


            if((localStorage.getItem("reservationTimer") === "currently_seated_time") ||
                (localStorage.getItem("reservationTimer") === "remaining_time") ||
                ((localStorage.getItem("reservationTimer") === "next_resv_time") && (value.timer[0].next_resv_time != 0))
            )
            {
                //topGroup.hide()
                topShowTimerGroup.add(topSemiPart,bottomSemiPart,showHideTableText,showHideTableTime)
                topShowTimerGroup.show()
            } else {
                //topGroup.show()
                topShowTimerGroup.hide()
            }

        } else {

            var tableText = new Konva.Text({
                x: centerTableXaxis - Math.round(tableTextSize*1.45), //14,
                y: centerTableYaxis - Math.round(tableTextSize/2), //14,
                text: value.tableText,
                width: tableTextWidth,
                align: 'center',
                fontSize: tableTextSize,
                fontFamily: fontFamilyType,
                fill: 'white'
            });

            topGroup.add(tableText)

            if((localStorage.getItem("reservationTimer") === "next_resv_time") && (value.timer[0].next_resv_time != 0)){
                topGroup.hide()
                topShowTimerGroup.add(topSemiPart,bottomSemiPart,showHideTableText,showHideTableTime)
                topShowTimerGroup.show()
            } else {
                topGroup.show()
                topShowTimerGroup.hide()
            }

        }

        if((value.server[0].serverTableId != "") && (!$(".main__container").hasClass("server-tables-assignment"))){

            if(value.tableShape == "Round"){
                if (value.maxGuests <= 4) {
                    serverOffsetX = Math.round(centerTableXaxis/2.6)
                    serverOffsetY = Math.round(centerTableYaxis/2.6)
                } else if ((value.maxGuests > 4) && (value.maxGuests <= 6)) {
                    serverOffsetX = Math.round(centerTableXaxis/4)
                    serverOffsetY = Math.round(centerTableYaxis/4)
                } else if ((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                    serverOffsetX = Math.round(centerTableXaxis/2.5) - Math.round(centerTableXaxis/2.4)
                    serverOffsetY = Math.round(centerTableYaxis/2) - Math.round(centerTableYaxis/2.4)
                } else if ((value.maxGuests > 8) && (value.maxGuests <= 10)) {
                    serverOffsetX = Math.round(centerTableXaxis/2.5) - Math.round(centerTableXaxis/1.7)
                    serverOffsetY = Math.round(centerTableYaxis/2.5) - Math.round(centerTableYaxis/1.7)
                }
            }

            if(value.tableShape == "Square"){
                if (value.maxGuests <= 4) {
                    serverOffsetX = Math.round(centerTableXaxis/2.6)
                    serverOffsetY = Math.round(centerTableYaxis/2.6)
                }
            }

            if(value.tableShape == "Rectangle"){

                if (value.maxGuests <= 4) {

                    if(value.tableRotation == "45" || value.tableRotation == "225"){
                        serverOffsetX = Math.round(centerTableXaxis/1.6)
                        serverOffsetY = Math.round(centerTableYaxis/2) - Math.round(centerTableXaxis/2.7)
                    } else if (value.tableRotation == "90" || value.tableRotation == "270"){
                        serverOffsetX = Math.round(centerTableXaxis/3)
                        serverOffsetY = Math.round(centerTableYaxis/1.7)
                    } else if (value.tableRotation == "135" || value.tableRotation == "315"){
                        serverOffsetX = Math.round(centerTableXaxis/1.7)
                        serverOffsetY = Math.round(centerTableYaxis/2) - Math.round(centerTableXaxis/1.7)
                    } else if (value.tableRotation == undefined || value.tableRotation == "0" || value.tableRotation == "180" || value.tableRotation == "360"){
                        serverOffsetX = Math.round(centerTableXaxis/2.3)
                        serverOffsetY = Math.round(centerTableYaxis/2.3)
                    }

                } else if ((value.maxGuests > 4) && (value.maxGuests <= 6)) {

                    //alert(value.tableRotation)

                    if(value.tableRotation == "45" || value.tableRotation == "225"){
                        serverOffsetX = Math.round(centerTableXaxis/1.8)
                        serverOffsetY = Math.round(centerTableYaxis/2.4)
                    } else if (value.tableRotation == "90" || value.tableRotation == "270"){
                        serverOffsetX = Math.round(centerTableXaxis/2) - Math.round(centerTableXaxis/2)
                        serverOffsetY = Math.round(centerTableYaxis/2) + Math.round(centerTableYaxis/2)
                    } else if (value.tableRotation == "135" || value.tableRotation == "315"){
                        serverOffsetX = Math.round(centerTableXaxis/1.7)
                        serverOffsetY = Math.round(centerTableYaxis/2) - Math.round(centerTableXaxis/1.2)
                    } else if (value.tableRotation == undefined || value.tableRotation == "0" || value.tableRotation == "180" || value.tableRotation == "360"){
                        serverOffsetX = Math.round(centerTableXaxis/2.3)
                        serverOffsetY = Math.round(centerTableYaxis/2.3)
                    }

                } else if ((value.maxGuests > 6) && (value.maxGuests <= 8)) {

                    if(value.tableRotation == "45" || value.tableRotation == "225"){
                        serverOffsetX = Math.round(centerTableXaxis/2.3)
                        serverOffsetY = Math.round(centerTableYaxis/1.7)
                    } else if (value.tableRotation == "90" || value.tableRotation == "270"){
                        serverOffsetX = Math.round(centerTableXaxis/2) - Math.round(centerTableXaxis/1.3)
                        serverOffsetY = Math.round(centerTableYaxis*1.3)
                    } else if (value.tableRotation == "135" || value.tableRotation == "315"){
                        serverOffsetX = Math.round(centerTableXaxis/2.3)
                        serverOffsetY = Math.round(centerTableYaxis/2) - Math.round(centerTableXaxis)
                    } else if (value.tableRotation == undefined || value.tableRotation == "0" || value.tableRotation == "180" || value.tableRotation == "360"){
                        serverOffsetX = Math.round(centerTableXaxis/2.3)
                        serverOffsetY = Math.round(centerTableYaxis/2.3)
                    }

                } else if ((value.maxGuests > 8) && (value.maxGuests <= 10)) {

                    if(value.tableRotation == "45" || value.tableRotation == "225"){
                        serverOffsetX = Math.round(centerTableXaxis/3)
                        serverOffsetY = Math.round(centerTableYaxis/1.2)
                    } else if (value.tableRotation == "90" || value.tableRotation == "270"){
                        serverOffsetX = Math.round(centerTableXaxis/2) - Math.round(centerTableXaxis/0.9)
                        serverOffsetY = Math.round(centerTableYaxis/2) + Math.round(centerTableYaxis/0.9)
                    } else if (value.tableRotation == "135" || value.tableRotation == "315"){
                        serverOffsetX = Math.round(centerTableXaxis/2.3)
                        serverOffsetY = Math.round(centerTableYaxis/2) - Math.round(centerTableXaxis/0.8)
                    } else if (value.tableRotation == undefined || value.tableRotation == "0" || value.tableRotation == "180" || value.tableRotation == "360"){
                        serverOffsetX = Math.round(centerTableXaxis/2.3)
                        serverOffsetY = Math.round(centerTableYaxis/2.3)
                    }

                }
            }

            var serverGroup = new Konva.Group({
                x: table.getWidth() + serverOffsetX,
                y: table.getHeight() + serverOffsetY
            });

            var server = new Konva.Circle({
                x: 0,
                y: 0,
                radius: Math.round(blockSnapSize/1.5),
                fill: value.server[0].serverTableColor,
                stroke: 'white',
                strokeWidth: 1,
            })

            var serverName = new Konva.Text({
                x: -10,
                y: -5,
                text: value.server[0].serverName.toUpperCase(),
                width: "20",
                align: 'center',
                fontSize: 11,
                fontFamily: fontFamilyType,
                fill: '#fff'
            });

            serverGroup.add(server,serverName)
            topGroup.add(serverGroup)

        }

        for (var i = 1; i <= value.maxGuests; i++) {
            if((value.maxGuests <= 4) && (value.tableShape == "Round") || (value.tableShape == "Square")) {

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset;
                    tableYAxis = centerTableYaxis;
                    chairRotation = 90;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset;
                    tableYAxis = centerTableYaxis;
                    chairRotation = 270;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                    chairRotation = 0;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                    chairRotation = 180;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 4) && (value.maxGuests <= 6) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")) {
                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 5;
                    tableYAxis = centerTableYaxis - 15;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 5;
                    tableYAxis = centerTableYaxis  - 15;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 5;
                    tableYAxis = centerTableYaxis + 15;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 5;
                    tableYAxis = centerTableYaxis + 15;
                }


                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 6) && (value.maxGuests <= 8) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")){

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 11;
                    tableYAxis = centerTableYaxis - 28;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 11;
                    tableYAxis = centerTableYaxis  - 28;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 11;
                    tableYAxis = centerTableYaxis + 28;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 11;
                    tableYAxis = centerTableYaxis + 28;
                } else if (i === 7) {
                    tableXAxis = centerTableXaxis + chairOffset;
                    tableYAxis = centerTableYaxis;
                } else if (i === 8) {
                    tableXAxis = centerTableXaxis - chairOffset;
                    tableYAxis = centerTableYaxis;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 8) && (value.maxGuests <= 10) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")){

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 22;
                    tableYAxis = centerTableYaxis - 42;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 22;
                    tableYAxis = centerTableYaxis  - 42;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 22;
                    tableYAxis = centerTableYaxis + 42;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 22;
                    tableYAxis = centerTableYaxis + 42;
                } else if (i === 7) {
                    tableXAxis = centerTableXaxis - chairOffset + 2;
                    tableYAxis = centerTableYaxis + 16;
                } else if (i === 8) {
                    tableXAxis = centerTableXaxis - chairOffset + 2;
                    tableYAxis = centerTableYaxis - 16;
                } else if (i === 9) {
                    tableXAxis = centerTableXaxis + chairOffset - 2;
                    tableYAxis = centerTableYaxis + 16;
                } else if (i === 10) {
                    tableXAxis = centerTableXaxis + chairOffset - 2;
                    tableYAxis = centerTableYaxis - 16;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);



            } else if(value.tableShape == "Rectangle"){

                if(value.maxGuests <= 2){

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairOffset - 1;
                        tableYAxis = centerTableYaxis;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis + (chairOffset*1.85);
                        tableYAxis = centerTableYaxis;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 2) && (value.maxGuests <= 4)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairOffset + 18;
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairOffset + (chairOffset*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairOffset + 18;
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairOffset + (chairOffset*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 4) && (value.maxGuests <= 6)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.6);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.2);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.6);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.2);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.7);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.4);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.7);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 7) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.4);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 8) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 8) && (value.maxGuests <= 10)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.9);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.6);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.4);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*17.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 7) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.9);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 8) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.6);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 9) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.4);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 10){
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*17.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                }
            }

            bottomChairGroup.setListening(false);
            bottomGroup.add(bottomChairGroup);
            bottomChairGroup.moveDown();
            bottomTableGroup.moveUp();

            layer.draw();

        }

        keyGroup.on('mouseenter', function () {

            if(($("#book-a-table.fade").hasClass('getAvailability')) && (value.tableAvailability != "Available")) {
                bottomGroup.setListening(false);
                layer.draw();
                stage.container().style.cursor = 'default';
            } else {
                stage.container().style.cursor = 'pointer';
            }

        });

        keyGroup.on('mouseleave', function () {
            stage.container().style.cursor = 'default';
        });


        keyGroup.on('click touchstart', function(e) {

            if (!$("#book-a-table.fade").hasClass('getAvailability') && !$(".main__container").hasClass('server-tables-assignment')) {
                clearTableSelection();
                if (isEmpty(value.reservations)) {
                    window.targetTableNameOnFloor = topGroup.children[0].partialText;
                    //window.targetTableIdFloor = e.target.parent.parent.parent.attrs.id;
                } else {
                    window.targetTableNameOnFloor = topGroup.children[1].partialText;
                    //window.targetTableIdFloor = e.target.parent.parent.parent.attrs.id;
                }
            }


            if ($(".main__container").hasClass('server-tables-assignment')) {
                selectTablesForServers(e.target.parent.parent.parent.attrs.id, e.target,value.server[0]);
            } else if (!$("#book-a-table.fade").hasClass('getAvailability')) {
                selectCreatedTable(e.target.parent.parent.parent.attrs.id, e.target);
            } else {
                selectMultipleTables(e.target.parent.parent.parent.attrs.id, e.target)
            }

        });


        if($(".main__container").hasClass("server-tables-assignment")){

            if(value.server[0].serverTableId != ""){

                if(value.server[0].serverId != currentServerId){
                    bottomGroup.setListening(false);
                } else {

                    finalSelectedTablesIdForServers.push({
                        id: value.id,
                        tableId: value.tableID,
                        tableText: value.tableText,
                        minGuests:value.minGuests,
                        maxGuests:value.maxGuests
                    });

                    selectTablesIdForServers.push(value.id);

                    //console.log(selectTablesIdForServers)

                    /*if(finalSelectedTablesIdForServers.length > 0) {
                        $(".server-tables-assignment .add_tables_btn a").removeAttr('disabled')
                    } else {
                        $(".server-tables-assignment .add_tables_btn a").attr('disabled', true)
                    }*/

                }

                table.stroke(value.server[0].serverTableColor)
                $.each(bottomGroup.children[0].children, function(key,val){
                    val.fill(value.server[0].serverTableColor)
                })

            }
        }

        if($("#tableManagement__wrapper").hasClass("tableManagement__wrapper")){
            if(value.blocked == true){
                table.fill(blockTableColor)
            }
        }

        if($("#reservation_info_popup.fade").hasClass('in') && value.tableText == targetTableNameOnFloor) {

            //alert(targetTableNameOnFloor)
            //console.log(value.tableText == targetTableNameOnFloor)

            table.stroke(tableSelectedColor);

            $.each(bottomChairGroup.children,function(k,v){
                v.fill(tableSelectedColor)
            })
        }

        keyGroup.add(bottomGroup,topGroup);

        if(localStorage.getItem("reservationTimer") != ""){
            keyGroup.add(topShowTimerGroup)
        }

        //Add Shape 1 to Layer 1
        layer.add(keyGroup);

        //Add Layer 1 to Stagehe table cannot be deleted as it has upcoming reservations. Move reservations on this table to another table and try deleting.
        stage.add(layer);

        tableText.moveToTop();
        layer.draw();
    });
}

function getShowHideTimer(arr){

    if(arr != undefined) {

        //if((arr.currently_seated_time > 0) || (arr.remaining_time > 0)) {
            var distance = "";
            var currentTime = (moment.utc().format('HH') * 60 * 60 * 1000) + (moment.utc().format('mm') * 60 * 1000)

            if (localStorage.getItem("reservationTimer") == "currently_seated_time") {
                //distance = currentTime - ((moment(arr.start_time).format('HH') * 60 * 60 * 1000) + (moment(arr.start_time).format('mm') * 60 * 1000));
                distance = arr.currently_seated_time
            } else if (localStorage.getItem("reservationTimer") === "remaining_time") {
                distance = arr.remaining_time
                //distance = ((moment(arr.end_time).format('HH') * 60 * 60 * 1000) + (moment(arr.end_time).format('mm') * 60 * 1000)) - currentTime;
            }

            // Time calculations for days, hours, minutes and seconds
            var hours = Math.floor((distance % (60 * 60 * 24)) / (60 * 60));
            var minutes = Math.floor((distance % (60 * 60)) / (60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            var timer = (hours < 9 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes;

            if((localStorage.getItem("reservationTimer") == "next_resv_time") && (arr.next_resv_time != 0)) {
                timer = moment(arr.next_resv_time).format('hh:mm A')
                //console.log(timer)
            }

    }

    return timer
}

var tableManagement = [];

function floorView(arr,id){
    var floor_data = [];
    tableManagement = [];

    floor_data[0] = JSON.parse(arr[0]);
    floor_data[2] = JSON.parse(arr[2]);
    floor_data[1] = [];

    $.each(arr[1], function (key, value) {
        floor_data[1][key] = JSON.parse(value)
    })

    tableManagement = floor_data;

    if(tableManagement[0]['floorType'] == "Round"){
        $(id).addClass("typeCircle")
    }

    $(".floor_nav .current__floor h2").text(tableManagement[0]['floorName'])

    var stageWidth = 120 * tableManagement[0]['floorTypeColumn']
    var stageHeight = 120 * tableManagement[0]['floorTypeRow']
    var marginArea = 40

    $(id).width(stageWidth).height(stageHeight);
    stage.width(stageWidth).height(stageHeight)

    editCreateTable(tableManagement[1], tableManagement[2]);
    makeServerList(tableManagement[1])
}

function deleteMultipleTable(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k] == value) {
            array.splice(k, 1);
            return k;
        }
    }
    return false;
}

function deleteFinalMultipleTable(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k]['id'] == value) {
            array.splice(k, 1);
            return k;
        }
    }
    return false;
}

function getArrayIndexOf(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k] == value) {
            return value;
        }
    }
    return false;
}

var selectedTablesId = []

function selectMultipleTables(id,targetElement){

    var selectedTable = targetElement
    var selectedChairs = selectedTable.parent.parent.children[0].children

    if($("#reservation-booking #multipleTableSelectpicker .bootstrap-select").hasClass('open')){
        if(getArrayIndexOf(selectedTablesId, id) == false){
            selectedTable.stroke(tableSelectedColor);

            $.each(selectedChairs, function (key, value) {
                value.fill(tableSelectedColor)
            })

            selectedTablesId.push(id)

            layer.draw();

        } else {
            //console.log(id)
            //console.log(selectedTablesId)

            selectedTable.stroke(tableStroke);

            $.each(selectedChairs, function (key, value) {
                value.fill(chairColor)
            })

            layer.draw();

            deleteMultipleTable(selectedTablesId, id)
        }
    } else {

        if (getArrayIndexOf(selectedTablesId, id) == id) {

            selectedTable.stroke(tableStroke);

            $.each(selectedChairs, function (key, value) {
                value.fill(chairColor)
            })

            layer.draw();

            deleteMultipleTable(selectedTablesId, id)
        } else {
            selectedTable.stroke(tableSelectedColor);

            $.each(selectedChairs, function (key, value) {
                value.fill(tableSelectedColor)
            })

            selectedTablesId.push(id)

            layer.draw();
        }

        $("#reservation-booking #tables.selectpicker").selectpicker('val', selectedTablesId)
        $("#reservation-booking #tables.selectpicker").selectpicker('refresh')
    }

}

$("#book-a-table").on("hidden.bs.modal", function () {
    //clearTableSelection();
    $(this).removeClass("getAvailability")
    selectedTablesId = []

    if(isEmpty(floor_and_tables) != true){
        floorView(floor_and_tables,"#floor-tables-view");
    }

});

$("#book_button").click(function(){

    $("#book-a-table").removeClass("getAvailability")
    selectedTablesId = []

    if(isEmpty(floor_and_tables) != true){
        floorView(floor_and_tables,"#floor-tables-view");
    }

})

$(".booking__type > div").click(function(){
   $("#reservation_info_popup").modal('hide')
});

function arr_diff (a1, a2) {

    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
}

var curTSL = "1";
var curArray = []
var finalArray = []

$('#book-a-table #tables').on('change', function(){

    var tableNameSelected = $(this).find("option:selected");
    var tableSelection = [];

    $.each(tableNameSelected, function(key,value){
        tableSelection.push({
            "id":$(this).val(),
            "name":$(this).text()
        });
    });

    var tableSelectionLength = tableSelection.length;

    setTimeout(function(){
        curTSL = tableSelection.length;
        curArray = tableSelection
    },10)

    if(tableSelectionLength > curTSL) {
        finalArray = tableSelection
    } else {
        finalArray = curArray
    }

    //console.log(tableSelectionLength)
    //console.log(tableSelection)
    //console.log(curArray)
    //console.log(finalArray)

    //getAutoTableSelectionByText(finalArray)

});

window.getAutoTableSelectionByText = function(tableSelectionArray){

    //console.log(tableSelectionArray);

    var selectedTextOnTable,
        tableID;
    var allTextOfTables = stage.find('Text');

    $.each(tableSelectionArray, function(key,value){

        selectedTextOnTable = $.grep(allTextOfTables, function(e) {
            return e.partialText === value.name;
        });

        tableID = value
    })

    $.each(selectedTextOnTable, function(key,val){

        //Selected Center Table
        $.each(val.parent.parent.children[0].children[1].children, function(k,ele){
            selectMultipleTables(tableID.id,ele)
        })
    })
}

function selectCreatedTable(id,targetElement){

    var selectedTable = targetElement
    var selectedChairs = selectedTable.parent.parent.children[0].children

    //console.log(id)

    if(($("#reservation_info_popup.fade").hasClass('in')) && ($("#reservation_info_popup .modal-header").attr('data-tableid') == id)){

        $("#reservation_info_popup").modal("hide")
        clearTableSelection();

    } else {

        selectedTable.stroke(tableSelectedColor);

        $.each(selectedChairs, function(key,value){
            value.fill(tableSelectedColor)
        })

        layer.draw();

        if(!$("#book-a-table.fade").hasClass('in')) {
            getTableId(id);


            $("body").addClass("overflow-visible").addClass("no-padding");
            $("html").removeClass("freezePage");
            $("body").removeClass("freezePage");

            $("#reservation_info_popup").on("show.bs.modal", function () {
                $("#reservation_info_popup").removeClass('selectedTable');
                $("#reservation_info_popup .modal-header").attr('data-tableid',id)
            });
        }


        clearTableSelection();
    }
}

var selectTablesIdForServers = []
var finalSelectedTablesIdForServers = []
function selectTablesForServers(id,targetElement,getCurrentServerArray){

    var selectedTable = targetElement
    var selectedChairs = selectedTable.parent.parent.children[0].children;

    var getTableID = "";
    var getTableText = "";
    var getMinGuests = "";
    var getMaxGuests = "";

    $.each(tableManagement[1], function(key,val){
        if(val.id == id){
            getTableID = val.tableID
            getTableText = val.tableText;
            getMinGuests = val.minGuests;
            getMaxGuests = val.maxGuests;
        }
    })

    if (getArrayIndexOf(selectTablesIdForServers, id) == id) {

        selectedTable.stroke(tableStroke);

        $.each(selectedChairs, function (key, value) {
            value.fill(chairColor)
        })

        layer.draw();

        deleteMultipleTable(selectTablesIdForServers, id)
        deleteFinalMultipleTable(finalSelectedTablesIdForServers, id)

    } else {
        selectedTable.stroke(getCurrentServerArray.serverTableColor);

        $.each(selectedChairs, function (key, value) {
            value.fill(getCurrentServerArray.serverTableColor)
        })

        finalSelectedTablesIdForServers.push({
            id: id,
            tableId: getTableID,
            tableText: getTableText,
            minGuests:getMinGuests,
            maxGuests:getMaxGuests
        });

        selectTablesIdForServers.push(id);

        layer.draw();
    }

    if(finalSelectedTablesIdForServers.length > 0) {
        $(".server-tables-assignment .add_tables_btn a").attr('disabled', false)
    } else {
        $(".server-tables-assignment .add_tables_btn a").attr('disabled', true)
    }

    if(($(".server-tables-assignment select#slot_name").val() == "") || ($(".server-tables-assignment select#slot_name").val() == null)){
        $(".server-tables-assignment .add_tables_btn a").attr('disabled', true)
    } else {
        $(".server-tables-assignment .add_tables_btn a").attr('disabled', false)
    }

    //console.log(selectTablesIdForServers)
    //console.log(id)
    //console.log(getArrayIndexOf(selectTablesIdForServers, id))
    //console.log(finalSelectedTablesIdForServers)

}

window.clearTableSelection = function () {
    //Find all shapes
    /*var allGroups = stage.find(node => {
        return node.getType() === 'Shape';
    });*/

    var allShape = stage.find('Shape')

    //Find all tables from shapes array
    var selectedShape = $.grep(allShape, function(e) {
        return e.attrs.id === 'tablesId';
    });

    $.each(selectedShape, function(key,value){
        value.stroke("white")
    })

    var selectedChairsGroup = $.grep(allShape, function(e) {
        if(e.attrs.id != "undefined"){
            return e.attrs.id === 'chairsid';
        }
    });

    $.each(selectedChairsGroup, function(key,value){
        value.fill(chairColor)
    })


    $("#book-a-table, #reservation_info_popup").on("hidden.bs.modal", function () {

        $.each(selectedShape, function(key,value){
            value.stroke("white")
        })

        $.each(selectedChairsGroup, function(key,value){
            value.fill(chairColor)
        })

        enableDoubleClick = false

        layer.draw();
    });

    if(enableDoubleClick == true){
        $.each(selectedShape, function(key,value){
            value.stroke("white")
        })

        $.each(selectedChairsGroup, function(key,value){
            value.fill(chairColor)
        })

        enableDoubleClick = false

        layer.draw();
    }

}

if(isEmpty(floor_and_tables) != true){
    floorView(floor_and_tables,"#floor-tables-view");
}

function currentlySeatedTime(){
    checkingNextResvTime("currently_seated_time");
    floorGridViewUpdate('currently_seated_time');
    $("#show-hide-timer #showTimer").addClass('hide')
    $("#show-hide-timer #hideTimer").removeClass('hide');
    window.localStorage.setItem('reservationTimer', 'currently_seated_time');
}

function timeRemaining(){
    checkingNextResvTime("remaining_time");
    floorGridViewUpdate('remaining_time');
    $("#show-hide-timer #showTimer").addClass('hide');
    $("#show-hide-timer #hideTimer").removeClass('hide');
    window.localStorage.setItem('reservationTimer', 'remaining_time');
}

function nextReservationTime(){
    checkingNextResvTime("next_resv_time");
    floorGridViewUpdate('next_resv_time');
    $("#show-hide-timer #showTimer").addClass('hide');
    $("#show-hide-timer #hideTimer").removeClass('hide');
    window.localStorage.setItem('reservationTimer', 'next_resv_time');
}

function checkingNextResvTime(timeType){

    floorGridViewUpdate()

    var reservationTimerArray = []
    var msg = ""

    if(timeType == "next_resv_time"){
        msg = "There is no upcoming reservation.";
        $.each(tableManagement[1],function(key,val){
            $.each(val.timer,function(k,v){
                if(v.next_resv_time != 0){
                    reservationTimerArray.push(v.next_resv_time)
                }
            })
        })
    } else if(timeType == "currently_seated_time"){
        msg = "There is no seated reservation in the current time.";
        $.each(tableManagement[1],function(key,val){
            $.each(val.timer,function(k,v){
                if(v.currently_seated_time != 0) {
                    reservationTimerArray.push(v.currently_seated_time)
                }
            })
        })
    } else if(timeType == "remaining_time"){
        msg = "There is no reservation in the current time.";
        $.each(tableManagement[1],function(key,val){
            $.each(val.timer,function(k,v){
                if(v.remaining_time != 0) {
                    reservationTimerArray.push(v.remaining_time)
                }
            })
        })
    }

    //console.log(reservationTimerArray)

    //if all values are same in reservationTimerArray array
    /*if((reservationTimerArray.every( (val, i, arr) => val === arr[0] )) && (reservationTimerArray.length > 0)){
        //if all values are same
        alertbox("Show Timer",msg,function (modal) {
            floorResetTimer();
        })
    }*/

    if(reservationTimerArray.length == 0){
        //if all values are same
        alertbox("Show Timer",msg,function (modal) {
            floorResetTimer();
        })
    }
}

function floorResetTimer(){
    //$('#show-hide-timer').dropdown('toggle');
    floorGridViewUpdate();
    setTimeout(function(){
        window.localStorage.removeItem("reservationTimer");
        showTimerReset()
    },200)
    $("#show-hide-timer span.fa-angle-down").removeClass('hide')
    $("#show-hide-timer button").addClass('padding-right-25')
}

$(".sidebar .side-item a").click(function(){
    floorResetTimer()
})

function showTimerReset() {
    if (localStorage.getItem("reservationTimer") == null) {
        $("#show-hide-timer #hideTimer").addClass('hide');
        $("#show-hide-timer #showTimer").removeClass('hide');
    } else {
        $("#show-hide-timer #showTimer").addClass('hide');
        $("#show-hide-timer #hideTimer").removeClass('hide');
    }
}

showTimerReset()

function multiDimensionalUnique(arr) {
    var uniques = [];
    var itemsFound = {};
    for(var i = 0, l = arr.length; i < l; i++) {
        var stringified = JSON.stringify(arr[i]);
        if(itemsFound[stringified]) { continue; }
        uniques.push(arr[i]);
        itemsFound[stringified] = true;
    }
    return uniques;
}

function makeServerList(arr){
    if($("#server_listing").length){
        var serverList = ""
        var serverIDList = []

        $.each(arr, function(key,value){
            serverIDList.push(value.server[0])
        })

        $.each(multiDimensionalUnique(serverIDList), function(key,value){
            if(value.serverFullName != "") {
                var html = "<div class='server__name flex-box flex-align-item-center'>";
                html += "<span class='server__color' style='background-color: " + value.serverTableColor + "'></span>";
                html += "<p>" + value.serverFullName + "</p>";
                html += "</div>";
                serverList += html
            }
        })

        $("#floor-view #server_listing").empty().append(serverList);
    }
}
