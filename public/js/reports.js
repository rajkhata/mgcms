function percentage(value, context){
    var dataArr = context.chart.data.datasets[0].data;
    var sum = 0;

    $.each(dataArr, function (key, value) {
        sum += parseInt(value);
    })

    var percentage = (value*100 / sum).toFixed(2)+"%";

    return percentage;
}

function ucfirst(str,force){
    str=force ? str.toLowerCase() : str;
    return str.replace(/(\b)([a-zA-Z])/,
        function(firstLetter){
            return   firstLetter.toUpperCase();
        });
}

var fontSize = 12;
var fontColor = "#000000";
var fontWeight = "bold";
var gridColor = "rgba(240, 240, 240, 1)";
var gridWidth = 1;
var chartPrimaryColor = "#3e7685";
var chartSecondaryColor = "#ff6c23"
var chartBlackColor = "#000000"

var legendLabels = {
    boxWidth: 13,
    fontSize: fontSize,
    fontColor: fontColor
}

function getSumOfArray(array){
    var sum = 0;
    for (var i = 0; i < array.length; i++) {
        sum += Math.round(array[i])
    }

    return sum;
}


function noDataOnChart(chartInstance, chartID, dataArray, dataArray2) {
    Chart.plugins.register({
        afterDraw: function(chart) {
            if (dataArray2 == "") {
                if (((chart.canvas.id == chartID.id) && (getSumOfArray(dataArray) == 0)) ||
                    ((chart.canvas.id == chartID.id) && (dataArray.length == 0))) {

                    // No data is present
                    var ctx = chartID.getContext('2d');
                    var ctx = chart.chart.ctx;
                    var width = chart.chart.width;
                    var height = chart.chart.height
                    chartInstance.clear();

                    ctx.save();
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    ctx.font = "24px normal 'Helvetica Nueue'";
                    ctx.fillStyle  = "black";
                    ctx.fillText('No Record Found', width / 2, height / 2);
                    ctx.restore();
                }

            } else {

                if (((chart.canvas.id == chartID.id) && (getSumOfArray(dataArray) == 0) && (getSumOfArray(dataArray2) == 0) ) ||
                    ((chart.canvas.id == chartID.id) && (dataArray.length == 0) && (dataArray2.length == 0))) {

                    // No data is present
                    var ctx = chartID.getContext('2d');
                    var ctx = chart.chart.ctx;
                    var width = chart.chart.width;
                    var height = chart.chart.height
                    chartInstance.clear();

                    ctx.save();
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    ctx.font = "24px normal 'Helvetica Nueue'";
                    ctx.fillStyle  = "black";
                    ctx.fillText('No Record Found', width / 2, height / 2);
                    ctx.restore();
                }

            }
        }
    });
}