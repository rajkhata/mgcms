

function refundOrderReset(){
    $("#refundOrder").find("#reason_id").selectpicker("val", "")
    $("#refundOrder .partial_amt").val("").parents(".partial-amt").hide();
    $("#refundOrder #other_reason").parent().hide();

    $("#refundOrder span.hasVal").each(function(key, value) {
        $(this).removeClass('hasVal')
    });

    $("#refundOrder label.error").each(function(key, value) {
        $(this).remove()
    });

    document.getElementById("refund_submit").reset();
}

$("#refundNoCancel, button[data-target='#refundOrder']").click(function(){
    refundOrderReset();
})

jQuery.validator.addMethod("notAllowDecimalVal", function(value, element, param) {
    console.log(value)
    return !(value % 1);
	//return this.optional(element) || Number(value) < Number($("select[name="+param+"]").val());
}, "Not allowed decimal vales");	

$("#refund_submit").validate({
    rules: {
        'host_name': {
            required: true,
        },
        'reason': {
            required: true,
        },
        'refund_value': {
            required: function(ele){
                return $("#refund_submit input:radio[name='refund_mode']:checked").val() == 'partially';
            },
            min: 1
        },
        'reason_other': {
            required: function(ele){
                return $("#refund_submit select#reason_id").val() == 'other';
            }
        },
        'refund_fp_value': {
            required: function(ele){
                return $("#refund_submit input:radio[name='refund_flat_or_percentage']:checked").val() == 'percentage';
            },
            notAllowDecimalVal: true
        }
        
    },
    messages:{
        host_name:"Please enter the host name.",
        reason:"Please enter the reason for refund.",
        reason_other:"Please enter the reason for refund."
    }
});