/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    console.log("loading addon group DOM");
    var addonGroupPencilToClick = null;
    $("#addongroup_form").submit(function (e) {
        e.preventDefault();
        var inputData = $(this).serialize();
        console.log(inputData);
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            dataType: 'json',
            //contentType: "application/json; charset=utf-8",
            type: 'post',
            data: inputData,
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                } else {
                    addonGroupPencilToClick = null;
                }

                alertbox(alertboxHeader, data.message, function (modal) {

                    setTimeout(function () {
                        getAddonGroups();
                        modal.modal('hide');
                    }, 2000);
                    
                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error- item addon group add: ' + err, err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
                setTimeout(function () {
                    $('#addongroup').modal('hide');
                }, 2000);
            }
        });
    });

    getAddonGroups = function () {
        var menuItemId = $('#menuItemId').val();
        var menuItemRestaurantId = $('#menuItemRestaurantId').val();
        var languageId = $('#menu_item_edit_form_language_id').val();
        var url = '/menu/item-addon-groups/list/' + menuItemRestaurantId + '/' + menuItemId + '/' + languageId;

        $.ajax({
            url: url,
            dataType: 'json',
            // contentType: "application/json; charset=utf-8",
            type: 'get',
            success: function (data) {
                $('#addons .panel-group').html(data.data);
                if (addonGroupPencilToClick !== null) {
                    $('#'+addonGroupPencilToClick).click();
                }
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error-Addon Group Listing', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                        $('#addons .panel-group').html('Error to load addon groups.');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    };

    reloadAddonItems = function (group_id) {

        var menuItemRestaurantId = $('#menuItemRestaurantId').val();
        var url = '/menu/item-addons/list/' + menuItemRestaurantId + '/' + group_id;

        $.ajax({
            url: url,
            dataType: 'json',
            //contentType: "application/json; charset=utf-8",
            type: 'get',
//        data: inputData,
            success: function (data) {
                $('#additemform').html(data.data);
                $("#searchiteminput").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#myUL li").filter(function () {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error-Addon Group Listing', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    };

    addAddonItems = function () {

//        var menuItemRestaurantId = $('#menuItemRestaurantId').val();
        var url = '/menu/item-addons/add';
        var inputData = $('#additemform').serialize();

        $.ajax({
            url: url,
            dataType: 'json',
//            contentType: "application/json; charset=utf-8",
            type: 'post',
            data: inputData,
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                }
                alertbox(alertboxHeader, data.message, function (modal) {
                    addonGroupPencilToClick = $('#additemform').find('input[name="addon_group_id"]').val();
                    getAddonGroups();
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);

                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error- item addon add', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    };

    updateAddonItemsGroups = function (udpateForm) {

        var url = udpateForm.attr('action');
        var inputData = udpateForm.serialize();

        $.ajax({
            url: url,
            dataType: 'json',
//            contentType: "application/json; charset=utf-8",
            type: 'post',
            data: inputData,
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                }
                alertbox(alertboxHeader, data.message, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });
                //alert(err);
                alertbox('Error- item addon group add: ' + err, err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    };

    deleteGroup = function (id) {
        if (!confirm('Are you sure you want to delete this item group?')) {
            return;
        }
        var url = '/menu/item-addon-groups/destroy/' + id;
//        var inputData = $('#addongroup_form').serialize();

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'delete',
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                }
                alertbox(alertboxHeader, data.message, function (modal) {
                    if (alertboxHeader === 'Success') {
                        $('#addonGroupPanel' + id).hide('slow');
                    } else {
                        addonGroupPencilToClick = id;
                        getAddonGroups();
                    }
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });
                alertbox('Error- item addon group delete ' + err, err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    };

    deleteItemAddons = function (id, row_id) {
        if (!confirm('Are you sure you want to delete this item?')) {
            return;
        }
        var url = '/menu/item-addons/destroy/' + id;
//        var inputData = $('#addongroup_form').serialize();

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'delete',
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                }
                alertbox(alertboxHeader, data.message, function (modal) {
//                    alert(data.message);
                    setTimeout(function () {
                        //location.reload();
                        modal.modal('hide');
                        $('#' + row_id).hide('slow');
                    }, 2000);

                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });
                alertbox('Error- item addon group delete ' + err, err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    };


    getAddonGroups();

    $('#saveAddonItemsToGroup').click(function () {
        addAddonItems();
    });

});