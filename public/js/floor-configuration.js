//Global Array

var tableManagement = [{}];
var floorTableManagement = [];
var tempTableManagement = [];
var alertMessage = []

var finalSelectedTablesIdForCombRules = [];
var selectedTablesId = [];
var tableStroke = "white"
var getTableDBId = "";
var getTableDBName = "";
var getTableDBMin = "";
var getTableDBMax = "";

//Global Variable
window.floorType = "";
window.floorColumn = $("#floor_property .column-controler input[type='text']");
window.floorRow = $("#floor_property .row-controler input[type='text']");
window.circleMinGuests = $("#circle .guest__inc_dec input[type='text'].minGuest");
window.circleMaxGuests = $("#circle .guest__inc_dec input[type='text'].maxGuest");
window.gettableText = $("#circleTableProperties input[name='tableId']");
window.getGroupId = "";
window.getTableType = $("#circleTableProperties select#table_type");
window.getGroupXAxis = "";
window.getGroupYAxis = "";
window.selectedTableType = "";
window.getTableRotation = "";
window.viewMode = "";
window.layoutCancel = "Added";
window.circleTableSize = "";
window.chairSize = "";
window.recTableWidth = "";
window.recTableChairGapping = "";
window.tableTextWidth = "";
window.chairWidth = "26";
window.movingTableWidth = "";
window.movingTableHeight = "";
window.movingTableMaxGuest = "";
window.movingTableType = "";
window.movingTableOffsetWidth = "";
window.movingTableOffsetHeight = "";
window.notUnselectTable = "0";
window.getFloorId = parseInt($("#floorName").attr('data-floorid'));
window.tableCombinationRules = false;
window.fontFamilyType = "Calibri,Arial";

var name = $("#floorNameForm input[type='text']").val();

if(floor_and_tables[0] == null){

    //Add Mode
    viewMode = "Add";

    $("body").addClass("floor-configuration")

    //popoverHints();

} else {

    //Edit Mode
    viewMode = "Edit";

    var floor_data=[];
    floor_data[0]=JSON.parse(floor_and_tables[0]);
    floor_data[1]=[];
    floor_data[2]=floor_and_tables[2];

    $.each(floor_and_tables[1], function(key, value){
        floor_data[1][key] = JSON.parse(value)
    });

    setTimeout(function(){
        $("#accordion > div").eq(1).find("h4").trigger('click');
    }, 10);

    //Function To Create Edit Floor
    createEditFloor(floor_data)
}

var layoutArea = $("#layout_area");
var tableXAxis,tableYAxis,chairRotation,
    stageWidth = 600,
    stageHeight = 600,
    blockSnapSize = 20,
    centerTableXaxis = Math.round(blockSnapSize * 3), //60
    centerTableYaxis = Math.round(blockSnapSize * 3), //60
    tableTextSize = Math.round(blockSnapSize), //26,
    //gridColor = "#ccc",
    chairOffset = "",
    tableColor = "#ccc",
    chairColor = "#aaa",
    tableGroupWidth = Math.round(blockSnapSize * 6),
    tableGroupHeight = Math.round(blockSnapSize * 6),
    rectableGroupWidth = "",
    rectableGroupHeight = "",
    tableSelectedColor = "#ff8700";

window.popoverHints = function(){

    setTimeout(function(){
        if(localStorage.getItem('floorLayoutPopover') == null){
            $('#floorCreateProperties').popover({
                placement : 'left',
                html : true,
                content: "<a href='#' class='close icon-close closeFloors' data-dismiss='alert'></a><div class='media no-margin'>Select a shape resembling your restaurant's floor plan and start customizing by adding different table types.</div>"
            }).addClass('floorPopover').popover('show')
        }
    },600)

    if(localStorage.getItem('floorTablesPopover') == null) {
        $('#accordion').on('shown.bs.collapse', function (e) {
            if($("#accordion #collapse2").hasClass("in") == true) {
                $('.floor-configration__title').popover({
                    placement: 'left',
                    html: true,
                    content: "<a href='#' class='close icon-close closeTables' data-dismiss='alert'></a><div class='media no-margin'>Select a shape resembling your restaurant's floor tables and customize them according to you needs.</div>"
                }).removeClass('floorPopover').addClass('tablePopover').popover('show')
            }
        })
    }
}

function floorSize(type,row,column){

    //Floor Resize
    floorColumn.val(column);
    floorRow.val(row);

    stage.width((115 * column)+2);
    stage.height((120 * row)+2);

    $("#layout_area > div:nth-child(1)").width(120 * column);
    $("#alert").width((120 * column) - 30);

    //layoutArea.find('div').eq(1).width(120 * column)

    stage.draw();

    tableManagement[0].floorName = name;
    tableManagement[0].floorType = type;
    tableManagement[0].floorTypeColumn = column;
    tableManagement[0].floorTypeRow = row;

    getTableName();
}

function resetLayout(){
    if(tableManagement[2] == undefined){
        ConfirmBox('Confirm Reset Tables', 'Do you really want to reset tables?', function (reset_status, curmodal) {
            if (reset_status) {
                //Clear Stage and Array
                resetLayoutWithoutConfirmBox();
                curmodal.modal('hide');
            }
        });
    } else {
        $.each(tableManagement[2], function (key, value) {

            console.log(value)

            if ((value.current_reservation == true) || (value.upcoming_reservation == true)) {
                alertbox("Cannot Reset Floor", "The floor cannot be reset as it has upcoming reservations on one or more tables. Move reservations on this floor to another floor and try again.", function (modal) {
                    setTimeout(function () {
                        //modal.modal('hide');
                    }, 3000);
                });
            } else {
                ConfirmBox('Confirm Reset Tables', 'All tables on this floor will be deleted. Do you really want to reset tables?', function (reset_status, curmodal) {
                    if (reset_status) {
                        resetLayoutWithoutConfirmBox();
                        curmodal.modal('hide');
                    }
                });
            }
        })
    }
}

function resetLayoutWithoutConfirmBox(){
    //Clear Stage and Array
    stage.destroyChildren();
    tableManagement[1] = [];

    createTablesRow(tableManagement[1]);
    saveLayoutBtn(tableManagement[1]);
    cancelUpdateTable();

    layoutCancel = "Clear";

    if (isEmpty(floor_and_tables[1])) {
        $('#undoLayout').addClass('hide');
    } else {
        $('#undoLayout').removeClass('hide');
    }

    $("#cancelLayout").addClass("hide");

    if($("#msgTableOutside").hasClass('hide') && $("#msgTableOverlap").hasClass('hide')){
        $("#alert").removeClass('alert-danger').addClass('hide').find('#msgTableOverlap').empty();
        $("#alert").removeClass('alert-danger').addClass('hide').find('#msgTableOutside').empty();
    }

    $("#saveLayout,#circleTableProperties .createTable, #tableListing .btn").attr('disabled',false).removeClass('disabled');
}



function cancelLayout() {
    location.href = "/floor"
}

//Stage
var stage = new Konva.Stage({
    container: "layout",
    width: stageWidth,
    height: stageHeight
});

//Layer 1
var layer = new Konva.Layer();
var gridLayer = new Konva.Layer();
//var snapBox = new Konva.Layer();

var padding = blockSnapSize;
for (var i = 0; i < stageWidth / padding; i++) {
    gridLayer.add(new Konva.Line({
        points: [Math.round(i * padding) + 0.5, 0, Math.round(i * padding) + 0.5, stageHeight],
        //stroke: gridColor,
        //strokeWidth: 1,
    }));
}

gridLayer.add(new Konva.Line({points: [0,0,10,10]}));
for (var j = 0; j < stageHeight / padding; j++) {
    gridLayer.add(new Konva.Line({
        points: [0, Math.round(j * padding), stageWidth, Math.round(j * padding)],
        //stroke: gridColor,
        //strokeWidth: 0.5,
    }));
}


function checkTableName(array, value,currentindex) {

    var duplicatecount=0;
    if (array.length > 0) {

        var l = array.length;
        for (var k = 0; k < l; k++) {

            if(viewMode == "Update"){
                if (array[k].tableText == value  && currentindex!=undefined && k!=currentindex) {
                    duplicatecount=duplicatecount+1;
                }
            }

            if(viewMode == "Add"){
                if (array[k].tableText == value && currentindex === undefined) {
                    duplicatecount = duplicatecount + 1;
                }
            }

        }
    }
    if(duplicatecount==0){
        return false;
    } else {
        return duplicatecount;
    }
}

$('#circleTableProperties').validate({
    rules: {
        tableId: {
            required: true,
            alphanumeric: true,
            maxlength: 3
        }
    }
});

function getTableName(){
    setTimeout(function () {
        selectedTableType = $("#collapse2 ul.nav-pills li.active a").attr("data-tabletype");
        var getMinGuestValue = $("#collapse2 ul.nav-pills li.active a").attr("data-minGuest");
        var getMaxGuestValue = $("#collapse2 ul.nav-pills li.active a").attr("data-maxGuest");

        $("#accordion > div:nth-child(2) .panel-title small").text("(" + selectedTableType + ")");
        $("#circleTableProperties .calc__row .guest__inc_dec input").attr('data-min',getMinGuestValue);
        $("#circleTableProperties .calc__row .guest__inc_dec input").attr('data-max',getMaxGuestValue);
        $("#circleTableProperties .calc__row .guest__inc_dec input").val(getMinGuestValue)

    }, 100)
}

$("#collapse2 ul.nav a").click(function(){
    getTableName();
});

//Check Empty Array
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

$("#circleTableProperties .createTable").click(function(){

    //Change View Mode
    viewMode = "Add";

    if(isEmpty(tableManagement[1])){
        tableManagement[1]=[];
    }

    var checkTableTextInArray = checkTableName(tableManagement[1], gettableText.val());
    var getMinGuestValue = parseInt($("#circleTableProperties .calc__row input.minGuest").val());
    var minGuestValue = parseInt($("#circleTableProperties .calc__row input.minGuest").attr('data-min'));
    var maxGuestValue = parseInt($("#circleTableProperties .calc__row input.minGuest").attr('data-max'));
    var getMaxGuestValue = parseInt($("#circleTableProperties .calc__row input.maxGuest").val());

    var outsideTablesIdArray = [];
    var outsideTablesTextArray = [];
    var checkTableTextInArray = checkTableName(tableManagement[1], gettableText.val())
    var minGuestValue = parseInt($("#circleTableProperties .calc__row input.minGuest").attr('data-min'))
    var maxGuestValue = parseInt($("#circleTableProperties .calc__row input.minGuest").attr('data-max'))
    var getMinGuestValue = parseInt($("#circleTableProperties .calc__row input.minGuest").val())
    var getMaxGuestValue = parseInt($("#circleTableProperties .calc__row input.maxGuest").val())

    //Find all shapes
    /*var allGroups = stage.find(node => {
        return node.getType() === 'Group';
    });*/

    var allGroups = stage.find('Group');

    //Find all tables from shapes array
    var selectedGroup = $.grep(allGroups, function(e) {
        return e.attrs.name === 'mainGroup';
    });


    var floorTableWidth = "";
    var floorTableHeight = Math.round(blockSnapSize*8);

    if(selectedTableType == "Rectangle") {

        if (getMaxGuestValue <= 4) {
            floorTableWidth = Math.round(blockSnapSize*10);
            floorTableHeight = Math.round(blockSnapSize*9);
        } else if ((getMaxGuestValue > 4) && (getMaxGuestValue <= 6)) {
            floorTableWidth = Math.round(blockSnapSize*11);
            floorTableHeight = Math.round(blockSnapSize*9);
        } else if ((getMaxGuestValue > 6) && (getMaxGuestValue <= 8)) {
            floorTableWidth = Math.round(blockSnapSize*14);
            floorTableHeight = Math.round(blockSnapSize*12);
        } else if ((getMaxGuestValue > 8) && (getMaxGuestValue <= 10)) {
            floorTableWidth = Math.round(blockSnapSize*16);
            floorTableHeight = Math.round(blockSnapSize*12);
        }

    } else {

        if (getMaxGuestValue <= 6) {
            floorTableWidth = Math.round(blockSnapSize*9);
        } else if ((getMaxGuestValue > 6) && (getMaxGuestValue <= 8)) {
            floorTableWidth = Math.round(blockSnapSize*10);
            floorTableHeight = Math.round(blockSnapSize*9);
        } else if ((getMaxGuestValue > 8) && (getMaxGuestValue <= 10)) {
            floorTableWidth = Math.round(blockSnapSize*12);
            floorTableHeight = Math.round(blockSnapSize*10);
        }

    }

    if((floorType == "Square") || (floorType == "Rectangle") ){

        $.each(selectedGroup, function (key, value) {

            if ((value.attrs.x < floorTableWidth) && (value.attrs.y < floorTableHeight)) {
                outsideTablesIdArray.push(value.attrs.id);
                outsideTablesTextArray.push(value.children[1].children[0].partialText)
            }

        })

    } else if (floorType == "Round") {

        $.each(selectedGroup, function (key, value) {

            if (value.attrs.y < 150) {
                outsideTablesIdArray.push(value.attrs.id);
                outsideTablesTextArray.push(value.children[1].children[0].partialText)
            }

        })

    }


    if(checkTableTextInArray !=false) {

        $("#circleTableProperties label[for='tableId']").remove();
        $( "<label for='tableId' generated='true' class='error'>Please use a unique Table ID.</label>" ).insertAfter("#tableId");

    } else {

        if($("#circleTableProperties").valid()){
            //$("#circleTableProperties").valid();

            $('#circleTableProperties .guest__done-btn').attr('disabled','disabled')

            if(getMaxGuestValue < getMinGuestValue) {

                alertbox("Alert","Max guests size should always greater than or equal to min guests size.",function(modal){
                    setTimeout(function() {
                        //modal.modal('hide');
                    }, 3000);
                    removeBtnDisabledAttr()
                });

            } else {

                if((selectedTableType == "Square") && (circleMaxGuests.val() > 5)){

                    alertbox("Alert","Square table dosen't get more then 4 guests.",function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                        }, 3000);
                        removeBtnDisabledAttr()
                    });

                } else {

                    if(outsideTablesTextArray.length > 0) {

                        alertbox('Alert','Table ('+ outsideTablesTextArray.sort().join(", ") +') is/are blocking new table creation. Please reposition to add new table.',function(modal){
                            setTimeout(function() {
                                modal.modal('hide');
                            }, 3000);
                            removeBtnDisabledAttr()
                        });

                    } else {

                        createTable();

                        $("#cancelLayout").removeClass("hide");
                        $("#undoLayout").addClass("hide");

                        gettableText.val("").nextAll().removeClass('hasVal');
                        removeBtnDisabledAttr()

                    }

                }
            }
        }
    }

    setTimeout(function () {
        $(".floor-configration__title").popover('hide');
        localStorage.setItem("floorTablesPopover", "OpenOnce")
    },100)

});

if ($('#floorNameForm').length) {

    $('#floorNameForm').validate({
        rules: {
            floorName: {
                required: true
            }
        }
    });

    $("a#saveLayout").click(function(){
        if($("#floorNameForm").valid() == true) {
            $("#floorNameForm").valid();

        }
    });

}


var id = "0";
var bottomId = "0";

var newId = "0";
var newBottomId = "0";


function createTable(){

    var table;

    var createTableManagement = [];
    var keyGroupX = "",
        keyGroupY = "",
        keyGroupId = "",
        bottomGroupId = "",
        bottomRotation = "",
        selectedTableShape = "";

    if(viewMode == "Add"){
        tempTableManagement.push({
            tableShape: selectedTableType,
            tableText: gettableText.val(),
            minGuests: circleMinGuests.val(),
            maxGuests: circleMaxGuests.val(),
            tableXAxis: "0",
            tableYAxis: "0"
        });

        if(tempTableManagement.length > 1) {
            tempTableManagement.shift();
        }

        createTableManagement = tempTableManagement;

        //Disable Table Suggestion Section
        $("#accordion > div:last-child").addClass("hide");

    } else if(viewMode == "Edit") {

        $.each(tableManagement[1], function(key, value){
            createTableManagement.push(value)
        })

        //Enable Table Suggestion Section
        $("#accordion > div:last-child").removeClass("hide");

    } else if(viewMode == "Update") {

        stage.destroyChildren();

        $.each(tableManagement[1], function(key, value){
            createTableManagement.push(value)
        })

    }

    $.each(createTableManagement, function(key, value) {

        if (createTableManagement.length == (key + 1)) {
            if (viewMode == "Edit") {
                id = value.tableID.match(/\d/g).join("");
                bottomId = value.tableBottomId.match(/\d/g).join("");
            }
            newId = id++;
            newBottomId = bottomId++;
        }

        if (viewMode == "Add") {

            if (floorType == "Round") {
                keyGroupX = Math.round(stage.getWidth() / 2)
            } else {
                keyGroupX = Math.round(tableGroupWidth / 2)
            }

            keyGroupY = Math.round(tableGroupHeight / 2);
            keyGroupId = "tableId_" + newId;
            bottomGroupId = "tablebottomId_" + newBottomId;
            selectedTableShape = selectedTableType;
            bottomRotation = 0
        } else {
            keyGroupX = value.tableXAxis;
            keyGroupY = value.tableYAxis;
            keyGroupId = value.tableID;
            bottomGroupId = value.tableBottomId,
                selectedTableShape = value.tableShape,
                bottomRotation = value.tableRotation
        }


        if (value.tableShape == "Rectangle") {

            rectableGroupHeight = Math.round(blockSnapSize*6.1);

            if (value.maxGuests <= 4) {
                rectableGroupWidth = Math.round(blockSnapSize*7);
            } else if ((value.maxGuests > 4) && (value.maxGuests <= 6)) {
                rectableGroupWidth = Math.round(blockSnapSize*9);
            } else if ((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                rectableGroupWidth = Math.round(blockSnapSize*11);
            } else if ((value.maxGuests > 8) && (value.maxGuests <= 10)) {
                rectableGroupWidth = Math.round(blockSnapSize*13);
            }

            if (viewMode == "Add") {

                keyGroupX = Math.round(rectableGroupWidth/2);
                keyGroupY = Math.round(rectableGroupHeight/2);

                if (selectedTableType == "Rectangle") {
                    if(circleMaxGuests.val() > 6 && circleMaxGuests.val() <= 8){
                        keyGroupY = Math.round(rectableGroupHeight/1.25)
                    } else if(circleMaxGuests.val() > 8 && circleMaxGuests.val() <= 10){
                        keyGroupY = Math.round(rectableGroupHeight/1.2)
                    }
                }

            } else {
                keyGroupX = value.tableXAxis;
                keyGroupY = value.tableYAxis
            }

            if ((floorType == "Round") && (viewMode == "Add")) {
                keyGroupX = Math.round(stage.getWidth()/2) + 40
            }

            var keyGroup = new Konva.Group({
                draggable: true,
                x: keyGroupX,
                y: keyGroupY,
                width: rectableGroupWidth,
                height: rectableGroupHeight,
                id: keyGroupId,
                offset: {
                    x: Math.round(rectableGroupWidth/2),
                    y: Math.round(rectableGroupHeight/2)
                },
                dragBoundFunc: theDragBoundFunc,
                name: "mainGroup"
            });

            var bottomGroup = new Konva.Group({
                id: bottomGroupId,
                x: Math.round(rectableGroupWidth/2),
                y: Math.round(rectableGroupHeight/2),
                width: rectableGroupWidth,
                height: rectableGroupHeight,
                offset: {
                    x: Math.round(rectableGroupWidth/2),
                    y: Math.round(rectableGroupHeight/2)
                },
                rotation: bottomRotation,
                name: "Rectangle"
            });

        } else {

            if(floorType == ("Round" || "Rectangle")){
                if(value.tableShape == "Round"){
                    if(viewMode == "Add"){
                        if((value.maxGuests > 8) == (value.maxGuests <= 10)){
                            keyGroupX = keyGroupX + 10;
                            keyGroupY = keyGroupY + 10
                        }
                    }
                }
            } else {
                if(value.tableShape == "Round"){
                    if(viewMode == "Add"){
                        if((value.maxGuests > 8) == (value.maxGuests <= 10)){
                            keyGroupX = keyGroupX + 20;
                            keyGroupY = keyGroupY + 20
                        }
                    }
                }
            }

            var keyGroup = new Konva.Group({
                draggable: true,
                x: keyGroupX,
                y: keyGroupY,
                width: tableGroupWidth,
                height: tableGroupHeight,
                id: keyGroupId,
                offset: {
                    x: Math.round(tableGroupWidth / 2),
                    y: Math.round(tableGroupHeight / 2)
                },
                dragBoundFunc: theDragBoundFunc,
                name: "mainGroup"
            });

            var bottomGroup = new Konva.Group({
                id: bottomGroupId,
                x: Math.round(tableGroupWidth/2),
                y: Math.round(tableGroupHeight/2),
                width: tableGroupWidth,
                height: tableGroupHeight,
                offset: {
                    x: Math.round(tableGroupWidth/2),
                    y: Math.round(tableGroupHeight/2)
                },
                rotation: bottomRotation,
                name: "Round-Square"
            });

        }

        /*var shadowRectangle = new Konva.Rect({
            x: Math.round(rectableGroupWidth/2),
            y: Math.round(rectableGroupHeight/2),
            width: rectableGroupWidth,
            height: rectableGroupHeight,
            opacity: 1,
            offset: {
                x: Math.round(rectableGroupWidth / 2),
                y: Math.round(rectableGroupHeight / 2)
            },
            stroke: '#c9c9c9',
            strokeWidth: 1,
            dash: [15, 4]
        }).hide();*/



        var bottomTableGroup = new Konva.Group();
        var bottomChairGroup = new Konva.Group();

        if(viewMode == "Add") {

            if(layoutCancel === "Clear"){
                floorTableManagement = []
            }

            layoutCancel = "Added";

            floorTableManagement.push({
                id: "0",
                tableID: keyGroup.attrs.id,
                tableWidth: keyGroup.attrs.width,
                tableHeight: keyGroup.attrs.height,
                tableXAxis: keyGroup.attrs.x,
                tableYAxis: keyGroup.attrs.y,
                tableBottomId: bottomGroup.attrs.id,
                tableBottomXAxis: bottomGroup.attrs.x,
                tableBottomYAxis: bottomGroup.attrs.y,
                tableRotation: keyGroup.attrs.rotate,
                tableShape: selectedTableType,
                //tableType: getTableType.val(),
                tableText: gettableText.val(),
                minGuests: circleMinGuests.val(),
                maxGuests: circleMaxGuests.val(),
                floorId: getFloorId
            })

        } else {
            floorTableManagement = tableManagement[1]
        }

        if(value.tableShape == "Round") {
            if (value.maxGuests <= 4) {
                circleTableSize = Math.round(blockSnapSize * 2) - Math.round(blockSnapSize / 2);
                chairSize = Math.round(blockSnapSize / 1.5);
                chairOffset = Math.round(blockSnapSize * 1.3);
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
            } else if ((value.maxGuests > 4) && (value.maxGuests <= 6)) {
                circleTableSize = Math.round(blockSnapSize * 1.8);
                chairOffset = Math.round(blockSnapSize * 1.6);
                chairSize = Math.round(blockSnapSize / 1.5);
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
            } else if ((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                circleTableSize = Math.round(blockSnapSize * 2.2);
                chairOffset = Math.round(blockSnapSize * 2);
                chairSize = Math.round(blockSnapSize / 1.5);
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
            } else if ((value.maxGuests > 8) && (value.maxGuests <= 10)) {
                circleTableSize = Math.round(blockSnapSize * 2.8);
                chairOffset = Math.round(blockSnapSize * 2.6);
                chairSize = Math.round(blockSnapSize / 1.5);
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
            }
        }

        if(value.tableShape == "Square"){
            if((value.maxGuests <= 4) && (value.maxGuests < 5)){
                circleTableSize = Math.round(blockSnapSize * 2) - Math.round(blockSnapSize / 2);
                chairSize = Math.round(blockSnapSize/1.5);
                chairOffset = Math.round(blockSnapSize * 1.3);
                tableTextWidth = Math.round((blockSnapSize*2) + (blockSnapSize/2))
            }
        }

        if(value.tableShape == "Rectangle"){
            if(value.maxGuests <= 4){
                recTableWidth = Math.round(blockSnapSize*4);
                tableTextWidth = Math.round(blockSnapSize*3.5)
            } else if((value.maxGuests > 4) && (value.maxGuests <= 6)) {
                recTableWidth = Math.round(blockSnapSize*6);
                tableTextWidth = Math.round(blockSnapSize*5.3);
                recTableChairGapping = Math.round(blockSnapSize/2)
            } else if((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                recTableWidth = Math.round(blockSnapSize*8);
                tableTextWidth = Math.round(blockSnapSize*7.5);
                recTableChairGapping = Math.round(blockSnapSize/2)
            } else if((value.maxGuests > 8) && (value.maxGuests <= 10)) {
                recTableWidth = Math.round(blockSnapSize*10);
                tableTextWidth = Math.round(blockSnapSize*9.5);
                recTableChairGapping = Math.round(blockSnapSize/2)
            }

            chairOffset = Math.round(blockSnapSize * 1.35);
            chairSize = Math.round(blockSnapSize/1.5)

        }

        if(selectedTableShape == "Round"){

            table = new Konva.Circle({
                id: "tablesId",
                x: centerTableXaxis,
                y: centerTableYaxis,
                radius: circleTableSize,
                fill: tableColor,
                stroke: "white",
                strokeWidth: 2,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        } else if(selectedTableShape == "Square"){

            table = new Konva.Rect({
                id: "tablesId",
                x: centerTableXaxis - Math.round(blockSnapSize * 1.5),
                y: centerTableYaxis - Math.round(blockSnapSize * 1.5),
                width: Math.round(blockSnapSize * 3),
                height: Math.round(blockSnapSize * 3),
                fill: tableColor,
                stroke: "white",
                strokeWidth: 1,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        } else if(selectedTableShape == "Rectangle"){

            table = new Konva.Rect({
                id: "tablesId",
                x: centerTableXaxis - Math.round(blockSnapSize * 1.5),
                y: centerTableYaxis - Math.round(blockSnapSize * 1.5),
                width: recTableWidth,
                height: Math.round(blockSnapSize * 3),
                fill: tableColor,
                stroke: "white",
                strokeWidth: 1,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        }

        bottomTableGroup.add(table);
        bottomGroup.add(bottomTableGroup);


        for (var i = 1; i <= value.maxGuests; i++) {
            if((value.maxGuests <= 4) && (value.tableShape == "Round") || (value.tableShape == "Square")) {

                //alert(value.maxGuests)

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset;
                    tableYAxis = centerTableYaxis;
                    chairRotation = 90;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset;
                    tableYAxis = centerTableYaxis;
                    chairRotation = 270;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                    chairRotation = 0;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                    chairRotation = 180;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 4) && (value.maxGuests <= 6) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")) {
                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 5;
                    tableYAxis = centerTableYaxis - 15;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 5;
                    tableYAxis = centerTableYaxis  - 15;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 5;
                    tableYAxis = centerTableYaxis + 15;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 5;
                    tableYAxis = centerTableYaxis + 15;
                }


                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 6) && (value.maxGuests <= 8) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")){

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 11;
                    tableYAxis = centerTableYaxis - 28;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 11;
                    tableYAxis = centerTableYaxis  - 28;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 11;
                    tableYAxis = centerTableYaxis + 28;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 11;
                    tableYAxis = centerTableYaxis + 28;
                } else if (i === 7) {
                    tableXAxis = centerTableXaxis + chairOffset;
                    tableYAxis = centerTableYaxis;
                } else if (i === 8) {
                    tableXAxis = centerTableXaxis - chairOffset;
                    tableYAxis = centerTableYaxis;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 8) && (value.maxGuests <= 10) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")){

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 22;
                    tableYAxis = centerTableYaxis - 42;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 22;
                    tableYAxis = centerTableYaxis  - 42;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 22;
                    tableYAxis = centerTableYaxis + 42;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 22;
                    tableYAxis = centerTableYaxis + 42;
                } else if (i === 7) {
                    tableXAxis = centerTableXaxis - chairOffset + 2;
                    tableYAxis = centerTableYaxis + 16;
                } else if (i === 8) {
                    tableXAxis = centerTableXaxis - chairOffset + 2;
                    tableYAxis = centerTableYaxis - 16;
                } else if (i === 9) {
                    tableXAxis = centerTableXaxis + chairOffset - 2;
                    tableYAxis = centerTableYaxis + 16;
                } else if (i === 10) {
                    tableXAxis = centerTableXaxis + chairOffset - 2;
                    tableYAxis = centerTableYaxis - 16;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if(value.tableShape == "Rectangle"){

                if(value.maxGuests <= 2){

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairOffset - 1;
                        tableYAxis = centerTableYaxis;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis + (chairOffset*1.85);
                        tableYAxis = centerTableYaxis;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 2) && (value.maxGuests <= 4)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairOffset + 18;
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairOffset + (chairOffset*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairOffset + 18;
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairOffset + (chairOffset*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 4) && (value.maxGuests <= 6)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.6);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.2);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.6);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.2);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.7);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.4);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.7);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 7) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.4);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 8) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 8) && (value.maxGuests <= 10)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.9);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.6);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.4);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*17.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 7) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.9);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 8) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.6);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 9) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.4);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 10){
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*17.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                }
            }


            bottomGroup.add(bottomChairGroup);
            bottomChairGroup.setListening(false);
            bottomChairGroup.moveDown();
            bottomTableGroup.moveUp();

            layer.draw();
        }

        //Main Group
        keyGroup.add(bottomGroup);

        var topGroup = new Konva.Group();

        topGroup.setListening(false);


        if(value.tableText){
            var tableText = new Konva.Text({
                id: "textId",
                x: centerTableXaxis - Math.round(tableTextSize*1.25), //14,
                y: centerTableYaxis - Math.round(tableTextSize/2), //14,
                text: value.tableText,
                width: tableTextWidth,
                align: 'center',
                fontSize: tableTextSize,
                fontFamily: fontFamilyType,
                fill: 'white'
            });

            topGroup.add(tableText);
        }

        //Main Group
        keyGroup.add(topGroup);

        /*if(viewMode == "Add"){
            tableOverlap(keyGroup,keyGroup.getClientRect());
        }*/

        keyGroup.on('dragstart', function(e) {
            //shadowRectangle.show();
            //shadowRectangle.moveToTop();
            keyGroup.moveToTop();
        });

        keyGroup.on('dragend', function(e) {
            keyGroup.position({
                x: Math.round(keyGroup.x() / blockSnapSize) * blockSnapSize,
                y: Math.round(keyGroup.y() / blockSnapSize) * blockSnapSize
            });

            stage.batchDraw();
            //shadowRectangle.hide();
            getTableAxis(floorTableManagement, e.target.attrs.id, e.target.attrs.x, e.target.attrs.y);

            //console.log(e.target)
            //console.log(e.target.getClientRect())
            tableOverlap(e.target,e.target.getClientRect());
            tableGoingOutsideFloor()
        });

        keyGroup.on('dragmove', function(e){
            /*shadowRectangle.position({
                x: Math.round(keyGroup.x() / blockSnapSize) * blockSnapSize,
                y: Math.round(keyGroup.y() / blockSnapSize) * blockSnapSize
            });*/

            movingTableWidth = e.target.attrs.width; //.parent.parent.width
            movingTableHeight = e.target.attrs.height; //.parent.parent.width

            movingTableMaxGuest = e.target.children[0].children[0].children.length;
            movingTableType = e.target.children[0].attrs.name;

            movingTableOffsetWidth = 5;
            movingTableOffsetHeight = 0

            if((floorType == "Square") || (floorType == "Rectangle")){

                if(movingTableType == "Round-Square"){

                    if((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)){
                        movingTableWidth = movingTableWidth + 40;
                        movingTableHeight = movingTableHeight + 40

                    }

                } else {

                    if((movingTableMaxGuest > 6) == (movingTableMaxGuest <= 8)){
                        movingTableHeight = movingTableHeight + 80;
                        movingTableOffsetWidth = 5;
                        movingTableOffsetHeight = 5
                    } else if((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)){
                        movingTableHeight = movingTableHeight + 80;
                        movingTableOffsetWidth = 5;
                        movingTableOffsetHeight = 5
                    }

                }

            } else {

                if(movingTableType == "Round-Square") {

                    if((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)){
                        movingTableWidth = movingTableWidth + 160;
                        movingTableHeight = movingTableHeight + 160
                    }
                }

            }

            stage.batchDraw();
        });

        keyGroup.on('mouseenter', function () {
            stage.container().style.cursor = 'pointer';
        });

        keyGroup.on('mouseleave', function () {
            stage.container().style.cursor = 'default';
        });

        keyGroup.on('click touchstart', function (e) {
            //if(!$("#accordion > div:last-child h4.panel-title").hasClass('collapsed')){
            if(tableCombinationRules == true){
                selectMultipleTables(e.target.parent.parent.parent.attrs.id, e.target)
            } else {
                selectCreatedTable(e.target.parent.parent.parent.attrs.id,e.target);
            }
        });

        //Add Shape 1 to Layer 1
        layer.add(keyGroup);
        //snapBox.add(shadowRectangle);

        //Add Layer 1 to Stage
        stage.add(gridLayer);
        //stage.add(snapBox);
        stage.add(layer);

        topGroup.moveToTop();
        layer.draw();
    });

    if(viewMode == "Add") {
        tableManagement[1] = floorTableManagement;
    }

    saveLayoutBtn(tableManagement[1]);
    createTablesRow(tableManagement[1])

}

function haveIntersection(r1, r2) {

    //console.log(r1.x)
    //console.log(r2.x)
    //console.log(Number.isInteger(r1.x) +" - "+ r1.x)
    //console.log(Number.isInteger(r2.x) +" - "+ r2.x)

    return !(
        r2.x > r1.x + r1.width ||
        r2.x + r2.width < r1.x ||
        r2.y > r1.y + r1.height ||
        r2.y + r2.height < r1.y
    );

}

function tableOverlap(target,targetRect){

    var isOverlaped=false;
    var a=[];
    var conflictObj="";
    var getTextArray = [];
    var getNameArray = [];
    var alltables = [];

    /*console.log(layer)
    console.log(target)*/

    layer.children.each(function (group) {

        //console.log(group)
        //console.log(group.getClientRect())
        //console.log(targetRect)
        //console.log(group.findOne('.fillShape'))
        //console.log(tableColor)

        group.findOne('.fillShape').fill(tableColor);
        // do not check intersection with itself
        if (group === target) {
            return true;
        }

        a.push(group.getClientRect());
        alltables.push(group);
        conflictObj = group;



        if (haveIntersection(group.getClientRect(), targetRect)) {

            group.findOne('.fillShape').fill('red');

            isOverlaped=true;
            var mainobj=[];
            mainobj.name=group.children[1].children[0].partialText;
            mainobj.groupobj=group;
            getTextArray.push(mainobj);
            getNameArray.push(mainobj.name);
        }

    });

    $.each(a, function(key, value){
        $.each(a, function(key2, value2){
            if(haveIntersection(value2, value)){
                if(value!=value2) {
                    isOverlaped = true;
                    var mainobj = [];
                    mainobj.name = alltables[key2].children[1].children[0].partialText;
                    mainobj.groupobj = alltables[key2];

                    if (getNameArray.indexOf(mainobj.name) == -1) {
                        getTextArray.push(mainobj);
                        getNameArray.push(mainobj.name);
                        mainobj.groupobj.findOne('.fillShape').fill('red')
                    }
                }
            }
        });
    });

    //console.log(getNameArray);

    if(isOverlaped){
        $("#saveLayout,#circleTableProperties .createTable, #tableListing .btn,#resetLayout,.updateTable").attr('disabled',true).addClass('disabled');

        var tableOverlapMsg;

        if(getNameArray.sort().length < 2){
            tableOverlapMsg = 'Table <span id="overlapedTable">'+ getNameArray.sort().join(", ") +'</span> is overlapping with another table.'
        } else {
            tableOverlapMsg = 'Tables <span id="overlapedTable">'+ getNameArray.sort().join(", ") +'</span> are overlapping with another table.'
        }

        $("#alert").addClass('alert-danger').removeClass('hide').find('#msgTableOverlap').removeClass('hide').html(tableOverlapMsg)

    } else {

        if(!$("#msgTableOverlap").hasClass('hide')){
            $("#alert").removeClass('alert-danger').addClass('hide').find('#msgTableOverlap').empty();
        }

        $("#saveLayout,#circleTableProperties .createTable, #tableListing .btn,#resetLayout,.updateTable").attr('disabled',false).removeClass('disabled');
    }

    layer.draw();

}

function getArrayMultipleTableIndexOf(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k] == value) {
            return value;
        }
    }
    return false;
}

function deleteMultipleTable(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k] == value) {
            array.splice(k, 1);
            return k;
        }
    }
    return false;
}

function deleteFinalMultipleTable(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k]['tableId'] == value) {
            array.splice(k, 1);
            return k;
        }
    }
    return false;
}

function selectCreatedTable(id,targetElement){

    var selectedTable = targetElement;
    var selectedChairs = selectedTable.parent.parent.children[0].children;

    $.each(tableManagement[1], function(key,val){
        if(val.tableID == id){
            getTableDBId = val.id
            getTableDBName = val.tableText
            getTableDBMin = val.minGuests
            getTableDBMax = val.maxGuests
        }
    })

    if(($("#tableListing ul li").hasClass('active')) && ($("#tableListing ul li.active").attr('data-tableid') == id)){

        if(notUnselectTable == "0"){
            $("#tableListing li").removeClass('active');
            clearTableSelection()
        }

    } else {

        clearTableSelection();
        $("#tableListing li").removeClass('active');
        $("#tableListing li[data-tableid="+id+"]").addClass('active');

        selectedTable.stroke(tableSelectedColor);
        $.each(selectedChairs, function(key,value){
            value.fill(tableSelectedColor)
        });

        finalSelectedTablesIdForCombRules.push({
            id: getTableDBId,
            tableId: id,
            tableName: getTableDBName,
            tableMinGuest: getTableDBMin,
            tableMaxGuest: getTableDBMax
        })

        selectedTablesId.push(id)

        if(finalSelectedTablesIdForCombRules.length > 1){
            finalSelectedTablesIdForCombRules.shift();
            selectedTablesId.shift();
        }

        layer.draw();
    }

    //console.log(selectedTablesId)
    //console.log(finalSelectedTablesIdForCombRules)

}


function selectMultipleTables(id,targetElement){

    var selectedTable = targetElement
    var selectedChairs = selectedTable.parent.parent.children[0].children

    $.each(tableManagement[1], function(key,val){
        if(val.tableID == id){
            getTableDBId = val.id
            getTableDBName = val.tableText
            getTableDBMin = val.minGuests
            getTableDBMax = val.maxGuests
        }
    })

    if (getArrayMultipleTableIndexOf(selectedTablesId, id) == id) {

        selectedTable.stroke(tableStroke);

        $.each(selectedChairs, function (key, value) {
            value.fill(chairColor)
        })

        layer.draw();

        deleteMultipleTable(selectedTablesId, id)
        deleteFinalMultipleTable(finalSelectedTablesIdForCombRules, id)
    } else {

        selectedTable.stroke(tableSelectedColor);

        $.each(selectedChairs, function (key, value) {
            value.fill(tableSelectedColor)
        })

        finalSelectedTablesIdForCombRules.push({
            id: getTableDBId,
            tableId: id,
            tableName: getTableDBName,
            tableMinGuest: getTableDBMin,
            tableMaxGuest: getTableDBMax
        });

        selectedTablesId.push(id)

        //console.log(id)

        layer.draw();
    }

    //console.log(selectedTablesId)
    makeCombinationRules()

}

function sumAndMin(arr,type){

    var value = 0;

    if(type == "sum"){
        $.each(arr,function(k,v){
            value += +v;
        });
    } else if(type == "min"){
        value = Math.min.apply(Math,arr)
    }

    return value
}

function makeCombinationRules(){
    var crTableName = []
    var crTableMinGuest = []
    var crTableMaxGuest = []
    var finalTableCR = []

    $.each(finalSelectedTablesIdForCombRules, function(k,v){
        crTableName.push(v.tableName)
        crTableMinGuest.push(v.tableMinGuest)
        crTableMaxGuest.push(v.tableMaxGuest)
    })

    if(finalSelectedTablesIdForCombRules.length > 0) {
        var html = "<li class='width-120'>" + crTableName.join(" + ") + "</li>";
        html += "<li><input type='text' name='tableMinGuest' maxlength='2' max='" + sumAndMin(crTableMaxGuest, "sum") + "' value='" + sumAndMin(crTableMinGuest, "min") + "'></li>";
        html += "<li>" + sumAndMin(crTableMaxGuest, "sum") + "</li>";
        html += "<li></li>";

        //console.log(html)

        $("#table-combination-add-container ul.add-CR-list").empty().append(html)
    }

}

function addTableCombinationRules(){
    finalSelectedTablesIdForCombRules = []
    clearTableSelection();

    $("#table-combination-add,#noCRsetValidation").addClass('hide')
    $("#table-combination-save,#table-combination-add-container .add-CR-list").removeClass('hide')

    tableCombinationRules = true
}

function cancelTableCombinationRules(){
    finalSelectedTablesIdForCombRules = []
    clearTableSelection();

    $("#table-combination-add").removeClass('hide')
    $("#table-combination-save, #table-combination-add-container .add-CR-list").addClass('hide')

    tableCombinationRules = false

    var html = "<li class='width-120'>&nbsp;</li>";
    html += "<li><input type='text' name='tableMinGuest' maxlength='2' max='2' value='0'></li>";
    html += "<li>0</li>";
    html += "<li></li>";

    $("#table-combination-add-container ul.add-CR-list").empty().append(html)

    if(($("#table-combination-add-container input[name='tableMinGuest']").val() == 0) && ($("#table-combination-add-container ul").eq().prevObject.length <= 1)){
        $("#noCRsetValidation").removeClass('hide')
    }
}

function saveTableCombinationRules(){
    $(this).attr('disabled', true);
    $('#loader').removeClass('hidden');
    var floor_id = parseInt(floor_url.split("/floor/configure/")[1]);
    var tables='';
    if(finalSelectedTablesIdForCombRules!==null && finalSelectedTablesIdForCombRules!==undefined && finalSelectedTablesIdForCombRules!=''){
        tables = JSON.stringify(finalSelectedTablesIdForCombRules);
    }
    var inputData = 'tables='+tables+'&min='+$("input[name=tableMinGuest]").val();
    $.ajax({
        type: 'POST',
        url: '/floor/'+floor_id+'/combine-table',
        data: inputData,
        dataType: 'json', //mispelled
        success: function (resp) {
            //Clear Selected Table After Getting Availability
            enableDoubleClick = true;
            clearTableSelection();
            $('#loader').addClass('hidden');
            alertbox('Success', resp.message, function (modal) {
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            });
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            alertbox('Error',err,function(modal){
                setTimeout(function() {
                }, 2000);
            });
        }
    });
    //console.log(finalSelectedTablesIdForCombRules)
}

$("#tableCombinationRules").validate({
    rules: {
        tableMinGuest: {
            required: true,
        }
    }
});

function deleteTableCombination(combination_id){
    $(this).attr('disabled', true);
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'delete',
        url: '/floor/combine-table/'+combination_id,
        dataType: 'json', //mispelled
        success: function (resp) {
            enableDoubleClick = true;
            clearTableSelection();
            $('#loader').addClass('hidden');
            alertbox('Success', resp.message, function (modal) {
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            });
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            alertbox('Error',err,function(modal){
                setTimeout(function() {
                }, 2000);
            });
        }
    });
    //console.log(finalSelectedTablesIdForCombRules)
}

/*function notUnselectTable(id,targetElement){

    var selectedTable = targetElement
    var selectedChairs = selectedTable.parent.parent.children[0].children

    console.log(selectedTable)

    if(($("#tableListing ul li").hasClass('active')) && ($("#tableListing ul li.active").attr('data-tableid') == id)) {
        selectedTable.stroke(tableSelectedColor);
        $.each(selectedChairs, function (key, value) {
            value.fill(tableSelectedColor)
        })

        layer.draw();
        clearTableSelection()
    }

}*/

function clearTableSelection(){

    //alert("clearTableSelection")

    //Find all shapes
    /*var allGroups = stage.find(node => {
        return node.getType() === 'Shape';
    });*/

    var allGroups = stage.find('Shape');

    //Find all tables from shapes array
    var selectedGroup = $.grep(allGroups, function(e) {
        return e.attrs.id === 'tablesId';
    });

    $.each(selectedGroup, function(key,value){
        value.stroke("white")
    });

    var selectedChairsGroup = $.grep(allGroups, function(e) {
        if(e.attrs.id != "undefined"){
            return e.attrs.id === 'chairsid';
        }
    });

    $.each(selectedChairsGroup, function(key,value){
        value.fill(chairColor)
    });

    layer.draw();
}

function saveLayoutBtn(arr){
    if(arr.length > 0){
        $("#saveLayout, #resetLayout").removeClass("hide");
    } else {
        $("#saveLayout, #resetLayout, #cancelLayout").addClass("hide");
    }
}

function theDragBoundFunc(pos){

    //console.log(movingTableMaxGuest)
    //console.log(movingTableType)

    if(floorType == "Round"){

        if(movingTableType == "Rectangle"){

            if(movingTableMaxGuest <= 4){
                var radiusOffset = 60
            } else if((movingTableMaxGuest > 4) == (movingTableMaxGuest <= 6)){
                var radiusOffset = 80
            } else if((movingTableMaxGuest > 6) == (movingTableMaxGuest <= 8)){
                var radiusOffset = 100
            } else if((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)){
                var radiusOffset = 120
            }

        } else if(movingTableType == "Round-Square") {

            if ((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)) {
                var radiusOffset = 75
            } else {
                var radiusOffset = 61
            }

        }

        var x = stage.getWidth() / 2;
        var y = stage.getHeight() / 2;

        var radius = (((stage.getWidth()/2) + (stage.getHeight()/2))/2) -  radiusOffset;
        var scale = radius / Math.sqrt(Math.pow(pos.x - x, 2) + Math.pow(pos.y - y, 2));
        if(scale < 1)
            return {
                y: Math.round((pos.y - y) * scale + y),
                x: Math.round((pos.x - x) * scale + x)
            };
        else
            return pos;
    } else {

        // set ub the boundary rect - used in the rectfunc later
        var boundary = {
            x: Math.round(movingTableWidth/2) - movingTableOffsetWidth,
            y: Math.round(movingTableHeight/2) - movingTableOffsetHeight,
            width: (stage.getWidth() - movingTableWidth) + movingTableOffsetWidth,
            height: (stage.getHeight() - movingTableHeight) + movingTableOffsetHeight
        };

        //console.log(boundary.width)
        //console.log(boundary.height)

        var thisRect = {x: this.x(), y: this.y(), width: this.width(), height: this.height()};

        var testRect={
            left: boundary.x,
            top: boundary.y,
            right: boundary.x + boundary.width,
            bottom: boundary.y + boundary.height
        };

        // left edge check
        var newX = (pos.x < testRect.left ? testRect.left : pos.x);

        // right edge check
        newX = (newX > testRect.right ? testRect.right : newX);

        // top edge check
        var newY = (pos.y < testRect.top ? testRect.top : pos.y);

        // bottom edge check
        newY = (newY > testRect.bottom ? testRect.bottom : newY);

        // return the point we calculated
        return {
            x: newX,
            y: newY
        }
    }



}

//Get Floor Type
$("#floor-configration__layout a").click(function() {

    var minColumnSize = $(this).attr('data-minColumn');
    var minRowSize = $(this).attr('data-minRow');
    var currentFloorType = $(this).attr('data-floorLayout');

    //Layout Area
    layoutArea.removeClass("hide");

    //Set Min Column
    floorColumn.attr("data-min",minColumnSize);
    floorRow.attr("data-min",minRowSize);

    var outsideTablesIdArray = [];
    var outsideTablesTextArray = [];

    var getFloorDefaultWidth = ($("#floor-configration__layout li a[data-floorlayout='"+ currentFloorType +"']").attr("data-mincolumn") * tableGroupWidth) + 2;
    var getFloorDefaultHeight = ($("#floor-configration__layout li a[data-floorlayout='"+ currentFloorType +"']").attr("data-minrow") * tableGroupHeight) + 2;

    //Find all shapes
    /*var allGroups = stage.find(node => {
        return node.getType() === 'Group';
    });*/

    var allGroups = stage.find('Group');

    //Find all tables from shapes array
    var selectedGroup = $.grep(allGroups, function(e) {
        return e.attrs.name === 'mainGroup';
    });

    //Find all tables from shapes array
    /*var getRectangleTables = $.grep(allGroups, function(e) {
        return e.attrs.name === 'Rectangle';
    });

    var getRoundSquareTables = $.grep(allGroups, function(e) {
        return e.attrs.name === 'Round-Square';
    });*/

    if (currentFloorType == "Round") {
        var x = getFloorDefaultWidth/2;
        var y = getFloorDefaultHeight/2;
        var radiusOffset = 0;

        //var getTableName = stage.find()

        if(movingTableType == "Rectangle"){

            if(movingTableMaxGuest <= 4){
                radiusOffset = 60
            } else if((movingTableMaxGuest > 4) == (movingTableMaxGuest <= 6)){
                radiusOffset = 80
            } else if((movingTableMaxGuest > 6) == (movingTableMaxGuest <= 8)){
                radiusOffset = 100
            } else if((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)){
                radiusOffset = 120
            }

        } else if(movingTableType == "Round-Square") {

            if ((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)) {
                radiusOffset = 75
            } else {
                radiusOffset = 61
            }

        } else {
            radiusOffset = 61
        }

        $.each(selectedGroup, function (key, value) {

            var cRadius = (((getFloorDefaultWidth / 2) + (getFloorDefaultHeight / 2)) / 2) - radiusOffset;
            var circle = cRadius / Math.sqrt(Math.pow(value.attrs.x - x, 2) + Math.pow(value.attrs.y - y, 2));

            if (circle < 1) {
                if (
                    ((value.attrs.x <= Math.round((value.attrs.x - x) * circle + x)) || (value.attrs.x >= Math.round((value.attrs.x - x) * circle + x)))
                /*&&
                ((value.attrs.y <= Math.round((value.attrs.y - y) * circle + y)) || (value.attrs.y >= Math.round((value.attrs.y - y) * circle + y)))*/
                ) {
                    outsideTablesIdArray.push(value.attrs.id);
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }
            }

        })

    }



    if ((currentFloorType == "Round") || (currentFloorType == "Square")) {

        //if ((stage.getWidth() < getFloorDefaultWidth) && (stage.getHeight() <= getFloorDefaultHeight)) {
        if (

            ((stage.getWidth() > getFloorDefaultWidth) && (stage.getHeight() > getFloorDefaultHeight)) ||
            ((stage.getWidth() > getFloorDefaultWidth) || (stage.getHeight() > getFloorDefaultHeight))

        ) {



            $.each(selectedGroup, function (key, value) {

                if (value.attrs.x > getFloorDefaultWidth) {
                    outsideTablesIdArray.push(value.attrs.id);
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }

                if (value.attrs.y > getFloorDefaultHeight) {
                    outsideTablesIdArray.push(value.attrs.id);
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }

            })
        }

    } else if (currentFloorType == "Rectangle") {

        if ((stage.getWidth() > getFloorDefaultWidth) || (stage.getHeight() > getFloorDefaultHeight)) {

            $.each(selectedGroup, function (key, value) {

                if (value.attrs.x > getFloorDefaultWidth) {
                    outsideTablesIdArray.push(value.attrs.id);
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }

                if (value.attrs.y > getFloorDefaultHeight) {
                    outsideTablesIdArray.push(value.attrs.id);
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }

            })
        }

    }

    currentFloorType = "";

    $(this).addClass('in');
    outsideTablesIdArray = outsideTablesIdArray.filter(function(itm, i, a) { return i == a.indexOf(itm) });
    outsideTablesTextArray = outsideTablesTextArray.filter(function(itm, i, a) { return i == a.indexOf(itm) });

    if(outsideTablesTextArray.length > 0) {

        ConfirmBox('Alert', 'Table ('+ outsideTablesTextArray.sort().join(", ") +') are going outsite the floor if you want those table the re-arrage it otherwise click yes to remove them.', function (reset_status, curmodal) {
            if (reset_status) {

                floorType =  $("#floor-configration__layout a.in").attr('data-floorLayout'); //$(this).attr('data-floorLayout');
                floorColumn.val(minColumnSize);
                floorRow.val(minRowSize);

                $.each(outsideTablesIdArray, function(key, value){
                    resetTableLayout(value)
                });

                if (floorType == "Round") {
                    $("#layout > div").addClass("typeCircle");
                } else {
                    $("#layout > div").removeClass("typeCircle");
                }

                $("#accordion > div").eq(1).removeClass("hide");

                floorSize(floorType, minRowSize, minColumnSize);

                //Add Active Class on <li>
                $("#floor-configration__layout li").removeClass('active');
                $("#floor-configration__layout li a[data-floorlayout='"+ floorType +"']").parent().addClass('active');

                //Layout Property
                $("#floor_property").removeClass("hide");
                $("#accordion > div:nth-child(1) .panel-title small").text("(" + floorType + ")");

                $("#floor-configration__layout a").removeClass('in');
                curmodal.modal('hide');
                return true
            }

        });

    } else {

        $("#floor-configration__layout a").removeClass('in');

        floorType = $(this).attr('data-floorLayout');
        floorColumn.val(minColumnSize);
        floorRow.val(minRowSize);

        if (floorType == "Round") {
            $("#layout > div").addClass("typeCircle");
        } else {
            $("#layout > div").removeClass("typeCircle");
        }

        $("#accordion > div").eq(1).removeClass("hide");

        floorSize(floorType, minRowSize, minColumnSize);

        //Add Active Class on <li>
        $("#floor-configration__layout li").removeClass('active');
        $(this).parent().addClass('active');

        //Layout Property
        $("#floor_property").removeClass("hide");
        $("#accordion > div:nth-child(1) .panel-title small").text("(" + floorType + ")")
    }

    //$('#undoLayout').removeClass('hide');


    $("#floorCreateProperties").popover('hide').popover('disable')

});

$("#floorCreateProperties").on("hidden.bs.popover", function(e) {
    $(this).popover('disable');
    localStorage.setItem("floorLayoutPopover", "OpenOnce")
});

$("#collapse2 ul.nav li a").click(function(){
    $(".floor-configration__title").popover('hide').popover('disable');
    localStorage.setItem("floorTablesPopover", "OpenOnce")
})

$(document).on("click", ".popover .close" , function(e){
    $(this).parents(".popover").popover('hide');

    if(e.target.className.split(" ").indexOf("closeTables") == 2){
        localStorage.setItem("floorTablesPopover", "OpenOnce")
    } else {
        localStorage.setItem("floorLayoutPopover", "OpenOnce")
    }
});

function getListOutsideTable(sizeType,value){

    var outsideTablesIdArray = [];
    var outsideTablesTextArray = [];

    var getFloorDefaultWidth = ($("#floor-configration__layout li.active a").attr("data-mincolumn") * tableGroupWidth) + 2;
    var getFloorDefaultHeight = ($("#floor-configration__layout li.active a").attr("data-minrow") * tableGroupHeight) + 2;

    //Find all shapes
    /*var allGroups = stage.find(node => {
        return node.getType() === 'Group';
    });*/

    var allGroups = stage.find('Group');

    //Find all tables from shapes array
    var selectedGroup = $.grep(allGroups, function(e) {
        return e.attrs.name === 'mainGroup';
    });

    //Find all tables from shapes array
    var getRectangleTables = $.grep(allGroups, function(e) {
        return e.attrs.name === 'Rectangle';
    });


    if (floorType == "Round") {
        var x = stage.getWidth()/2; //getFloorDefaultWidth/2
        var y = stage.getHeight()/2; //getFloorDefaultHeight/2
        var radiusOffset = 0;

        //var getTableName = stage.find()

        if(movingTableType == "Rectangle"){

            if(movingTableMaxGuest <= 4){
                radiusOffset = 60
            } else if((movingTableMaxGuest > 4) == (movingTableMaxGuest <= 6)){
                radiusOffset = 80
            } else if((movingTableMaxGuest > 6) == (movingTableMaxGuest <= 8)){
                radiusOffset = 100
            } else if((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)){
                radiusOffset = 120
            }

        } else if(movingTableType == "Round-Square") {

            if ((movingTableMaxGuest > 8) == (movingTableMaxGuest <= 10)) {
                radiusOffset = 75
            } else {
                radiusOffset = 61
            }

        } else {
            radiusOffset = 61
        }

        $.each(selectedGroup, function (key, value) {

            var cRadius = (((stage.getWidth() / 2) + (stage.getHeight() / 2)) / 2) - radiusOffset;
            var circle = cRadius / Math.sqrt(Math.pow(value.attrs.x - x, 2) + Math.pow(value.attrs.y - y, 2));

            if (circle < 1) {
                if (
                    ((value.attrs.x <= Math.round((value.attrs.x - x) * circle + x)) || (value.attrs.x >= Math.round((value.attrs.x - x) * circle + x)))
                /*&&
                ((value.attrs.y <= Math.round((value.attrs.y - y) * circle + y)) || (value.attrs.y >= Math.round((value.attrs.y - y) * circle + y)))*/
                ) {
                    outsideTablesIdArray.push(value.attrs.id);
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }
            }

        })

    }


    if ((floorType == "Square") || (floorType == "Round")) {

        if ((stage.getWidth() > getFloorDefaultWidth) && (stage.getHeight() > getFloorDefaultHeight)) {
            $.each(selectedGroup, function (key, value) {
                if ((Math.round(value.attrs.x+value.attrs.width/1.9) > (stage.getWidth() - 120)) || (Math.round(value.attrs.y+value.attrs.height/1.9) > (stage.getHeight() - 120))) {
                    outsideTablesIdArray.push(value.attrs.id);
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }
            })
        }

    } else if (floorType == "Rectangle") {

        if (
            ((stage.getWidth() > getFloorDefaultWidth) && (sizeType == "column")) ||
            ((stage.getHeight() > getFloorDefaultHeight) && (sizeType == "row"))
        ) {

            $.each(selectedGroup, function (key, value) {

                //console.log(value.attrs.x);

                if (
                    ((Math.round(value.attrs.x+value.attrs.width/1.9) > (stage.getWidth() - 120)) && (sizeType == "column")) ||
                    ((Math.round(value.attrs.y+value.attrs.height/1.9) > (stage.getHeight() - 120)) && (sizeType == "row"))
                ) {
                    outsideTablesIdArray.push(value.attrs.id);
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }

            })
        }

    }


    var $this = $(".size-dec-btn");

    if(outsideTablesTextArray.length > 0) {
        ConfirmBox('Alert', 'Table ('+ outsideTablesTextArray.sort().join(", ") +') are going outsite the floor if you want those table the re-arrage it otherwise click yes to remove them.', function (reset_status, curmodal) {
            if (reset_status) {

                $.each(outsideTablesIdArray, function(key, value){
                    resetTableLayout(value)
                });

                if ((floorType == "Square") || (floorType == "Round")) {

                    floorSize(floorType, value, value);

                } else if (floorType == "Rectangle") {

                    if(sizeType == "column"){
                        var rectangleRow = floorRow.val();
                        floorSize(floorType, rectangleRow, value);
                        $this.parent().parent(".column-controler").find("input").val(value)
                    } else if(sizeType == "row"){
                        var rectangleColumn = floorColumn.val();
                        floorSize(floorType, value, rectangleColumn);
                        $this.parent().parent(".row-controler").find("input").val(value)
                    }

                }
                curmodal.modal('hide');
                return true
            }
        });
        return false
    } else {

        if ((floorType == "Square") || (floorType == "Round")) {
            floorSize(floorType, value, value);
        } else if (floorType == "Rectangle") {

            if(sizeType == "column"){
                var rectangleRow = floorRow.val();
                floorSize(floorType, rectangleRow, value);
                $this.parent().parent(".column-controler").find("input").val(value)
            } else if(sizeType == "row"){
                var rectangleColumn = floorColumn.val();
                floorSize(floorType, value, rectangleColumn);
                $this.parent().parent(".row-controler").find("input").val(value)
            }

        }
    }
}

$(".size-inc-btn").click(function(e){

    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var maxValue = parseInt($this.closest("div").find("input").attr('data-max'));
    var diffValue = parseInt($this.closest("div").find("input").attr('data-diff'));
    var value = parseInt($input.val());

    value = value + diffValue;

    if (value <= maxValue) {


        if (floorType == "Round") {

            if($this.parent().parent().hasClass('column-controler')){
                var sizeType = "column"
            } else {
                var sizeType = "row"
            }

            //getListOutsideTable(sizeType,value)

            if(getListOutsideTable(sizeType,value) == true){
                $input.val(value);
                floorSize(floorType, value, value);
            }

        } else if (floorType == "Square") {

            floorSize(floorType, value, value);

        } else if (floorType == "Rectangle") {

            if ($this.parent().parent().hasClass('column-controler')) {
                var rectangleRow = floorRow.val();
                floorSize(floorType, rectangleRow, value);
            } else {
                var rectangleColumn = floorColumn.val();
                floorSize(floorType, value, rectangleColumn);
            }

        }



    }



});

$(".size-dec-btn").click(function(e){
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var minValue = $this.closest("div").find("input").attr('data-min');
    var diffValue = parseInt($this.closest("div").find("input").attr('data-diff'));
    var value = parseInt($input.val());

    value = value - diffValue;

    if (value >= minValue) {

        if($this.parent().parent().hasClass('column-controler')){
            var sizeType = "column"
        } else {
            var sizeType = "row"
        }

        getListOutsideTable(sizeType,value)

    }

});

function createTablesRow(array){

    var createdTable="";
    $.each(array, function(key, value){
        var html = '<li class="fullWidth" data-tableid="'+ value.tableID +'">';
        html += '<div class="pull-left">';
        html +=  value.tableText +" - "+ value.tableShape +" ("+ value.minGuests +"-"+ value.maxGuests +")";
        html += ' - <button class="btn rotate__table no-padding btn-link" onclick="rotateTable(\''+ value.tableBottomId +'\')" ><i class="fa fa-repeat" aria-hidden="true"></i></button>';
        html += ' - <button class="btn edit__table no-padding btn-link" onclick="editTable(\''+ value.tableID +'\')" ><i class="fa fa-pencil" aria-hidden="true"></i></button>';
        html += '</div><div class="pull-right">';
        html += '<button class="btn remove__table no-padding btn-link" onclick="removeTable('+ value.id +',\''+ value.tableID +'\')" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
        html += '</div>';
        html += '</li>';
        createdTable += html
    });

    $("#tableListing").empty().append('<ul>'+ createdTable +'</ul>');
    removeBtnDisabledAttr()
    //console.log(tableManagement[1]);
}

function getIndexForDelete(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k].tableID == value) {
            array.splice(k, 1);
            return k;
        }
    }
    return false;
}

function getIndexRotation(array, value, r) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k].tableBottomId == value) {
            tableManagement[1][k].tableRotation = r;
            return k;
        }
    }
    return false;
}

function getTableAxis(array, value, x, y) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k].tableID == value) {
            tableManagement[1][k].tableXAxis = x;
            tableManagement[1][k].tableYAxis = y;
            return k;
        }
    }
    return false;
}

function getArrayIndexOf(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k].tableID == value) {
            return k;
        }
    }
    return false;
}

function resetTableLayout(getCurrentTableId){

    var getTableChair = stage.find("#"+getCurrentTableId);
    getTableChair.remove();

    getIndexForDelete(tableManagement[1], getCurrentTableId);
    createTablesRow(tableManagement[1]);
    saveLayoutBtn(tableManagement[1]);
    cancelUpdateTable();

    /*$.each(getTableChair, function(key, value){
        tableOverlap(value,value.getClientRect())
    })*/

    layer.draw();
}

function removeTable(id,getCurrentTableId) {
    //stage.destroyChildren()
    if(tableManagement[1].length < 2){
        ConfirmBox('Confirm Delete','This floor will not appear on the floor view as there are no tables on this floor. Do you really want to delete this table?',function(resp,curmodal) {
            if (resp) {
                deleteTable(id,getCurrentTableId)
                curmodal.modal('hide');
            }
        })
    } else if(id > 0){
        ConfirmBox('Confirm Delete','On Floor View this table will not appear. Do you really want to delete this table?',function(resp,curmodal) {
            if (resp) {
                deleteTable(id,getCurrentTableId)
                curmodal.modal('hide');
            }
        })
    } else {
        ConfirmBox('Confirm Delete','Do you really want to delete this table?',function(conf_status,curmodal){
            if(conf_status){
                resetTableLayout(getCurrentTableId);
                curmodal.modal('hide');
            }
        });
    }
}

var dbTableRotation= 0;
var rotationAngle = 0;

function rotateTable(getCurrentTableId) {

    var getTableChair = stage.find("#"+getCurrentTableId);
    var oldAngle = getTableChair[0]['attrs']['rotation']
    var getCurrentTableStrokeColor = getTableChair[0].parent.attrs.id;
    var getCurrentChairColor = getTableChair[0].parent.children[0].children[0].children[0];
    var getIndex = getArrayIndexOf(tableManagement[1],getCurrentTableStrokeColor);
    var getTableRotation = tableManagement[1][getIndex]['tableRotation']

    $.each(floor_and_tables[1], function(key, value){
        if(JSON.parse(value).tableID == getCurrentTableStrokeColor){
            dbTableRotation = JSON.parse(value).tableRotation
        }
    });

    if(rotationAngle == "315" || getTableRotation == undefined) {
        rotationAngle = 0
    }

    if(getTableRotation != undefined) {
        if(rotationAngle < "315") {
            rotationAngle = getTableRotation
        } else {
            rotationAngle = 0
        }
    }

    console.log(getCurrentTableStrokeColor+" - "+rotationAngle+" - "+getTableRotation)

    if(dbTableRotation != undefined) {
        if(dbTableRotation == oldAngle){
            rotationAngle = oldAngle
        }
    }

    rotationAngle = rotationAngle + 45
    //getTableChair.rotate(45);
    getTableChair.rotation(rotationAngle);
    layer.draw();


    if(($("#tableListing ul li").hasClass('active')) && ($("#tableListing ul li.active").attr('data-tableid') == getCurrentTableStrokeColor)) {
        notUnselectTable = "1";
        selectCreatedTable(getCurrentTableStrokeColor,getCurrentChairColor)
    } else {
        $("#tableListing li").removeClass('active');
        clearTableSelection()
    }

    var selectedGroup = $.grep(getTableChair, function(v) {
        getIndexRotation(tableManagement[1],getCurrentTableId,v.attrs.rotation)
    });

    $.each(getTableChair, function(key, value){
        //tableOverlap(value,value.getClientRect())
    })

}

function getArrayIndexOf(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k].tableID == value) {
            return k;
        }
    }
    return false;
}

function editTable(getCurrentTableId){
    var getIndex = getArrayIndexOf(tableManagement[1],getCurrentTableId);

    var getTableID = tableManagement[1][getIndex]['id'];
    var getCurrentTableID = tableManagement[1][getIndex]['tableID'];
    var getCurrentTableName = tableManagement[1][getIndex]['tableText'];
    // var getCurrentTableType = tableManagement[1][getIndex]['tableType'];
    var getCurrentTableShape = tableManagement[1][getIndex]['tableShape'];
    var getCurrentMinGuests = tableManagement[1][getIndex]['minGuests'];
    var getCurrentMaxGuests = tableManagement[1][getIndex]['maxGuests'];

    $("#circleTableProperties label[for='tableId']").remove();
    $("#collapse2 ul.nav li").removeClass("active");
    $("#collapse2 ul.nav li a[data-tabletype='"+ getCurrentTableShape +"']").parent().addClass("active");

    var getCurrentMinLengthGuests = $("#collapse2 ul.nav li.active a").attr('data-minguest');
    var getCurrentMaxLengthGuests = $("#collapse2 ul.nav li.active a").attr('data-maxguest');

    $("#circleTableProperties .createTable").addClass('hide');
    $("#circleTableProperties .cancelUpdateTable, #circleTableProperties .updateTable").removeClass("hide");
    $("#collapse2").attr("data-tableid", getCurrentTableID).attr("data-ID", getTableID);

    gettableText.val(getCurrentTableName).focus();
    circleMinGuests.attr('data-min',getCurrentMinLengthGuests);
    circleMinGuests.attr('data-max',getCurrentMaxLengthGuests);
    circleMaxGuests.attr('data-min',getCurrentMinLengthGuests);
    circleMaxGuests.attr('data-max',getCurrentMaxLengthGuests);
    circleMinGuests.val(getCurrentMinGuests);
    circleMaxGuests.val(getCurrentMaxGuests);


    $("#table_type option").filter(function() {
        return this.value == getCurrentTableType;
    }).attr('selected', true);

    $('.selectpicker').selectpicker('refresh');
    $('#table_type').trigger('change')
}

window.tableModifyRefresh = function(){

    var getCurrentTableId = $("#collapse2").attr("data-tableid");
    var getIndex = getArrayIndexOf(tableManagement[1],getCurrentTableId);
    var getCurrentTableShape = $("#collapse2 ul.nav li.active a").attr("data-tabletype");

    tableManagement[1][getIndex]['tableText'] = gettableText.val();
    //tableManagement[1][getIndex]['tableType'] = getTableType.val();
    tableManagement[1][getIndex]['tableShape'] = getCurrentTableShape;
    tableManagement[1][getIndex]['minGuests'] = circleMinGuests.val();
    tableManagement[1][getIndex]['maxGuests'] = circleMaxGuests.val();

    createTablesRow(tableManagement[1]);
    createTable();
    cancelUpdateTable();

    var getTableChair = stage.find("#"+getCurrentTableId);

    $.each(getTableChair, function(key, value){
        tableOverlap(value,value.getClientRect())
    });

    tableGoingOutsideFloor();
    //gettableText();
};

function updateTable(){

    $('#circleTableProperties .guest__done-btn').attr('disabled','disabled')

    viewMode = "Update";
    var getCurrentTableId = $("#collapse2").attr("data-tableid");
    var getTableId = $("#collapse2").attr("data-id");
    var getIndex = getArrayIndexOf(tableManagement[1],getCurrentTableId);
    var getMinGuestValue = parseInt($("#circleTableProperties .calc__row input.minGuest").val());
    var getMaxGuestValue = parseInt($("#circleTableProperties .calc__row input.maxGuest").val());

    if(isEmpty(tableManagement[1]) == false){
        var checkTableTextInArray = checkTableName(tableManagement[1], gettableText.val(),getIndex)
    }

    if(checkTableTextInArray >=1) {
        $("#circleTableProperties label[for='tableId']").remove();
        $( "<label for='tableId' generated='true' class='error'>Please use another table name</label>" ).insertAfter("#tableId");
    } else {

        if(getMaxGuestValue < getMinGuestValue) {
            alertbox("Alert","Max guests size should always greater than or equal to min guests size.",function(modal){
                setTimeout(function() {
                    //modal.modal('hide');
                }, 3000);
                removeBtnDisabledAttr()
            });
        } else {

            if (getTableId > 0) {
                if ($("#circleTableProperties").valid()) {
                    $("#circleTableProperties").valid();
                    tableModifyRefresh();
                    updateTableInfo(getTableId, tableManagement[1][getIndex])
                }
            } else {
                if ($("#circleTableProperties").valid()) {
                    $("#circleTableProperties").valid();
                    tableModifyRefresh();
                }
            }
        }
    }
    /* else {

        if($("#circleTableProperties").valid()){
            //$("#circleTableProperties").valid();
            tableModifyRefresh();
            //Table Overlaped


            if(getTableId > 0){
              console.log(updateTableInfo(getTableId,tableManagement[1][getIndex]));
            }


        }
    }*/

}

window.removeBtnDisabledAttr = function (){
    $('#circleTableProperties .guest__done-btn').removeAttr('disabled')
}

function cancelUpdateTable(){

    /*$("#table_type option").filter(function() {
        return this.text == "Select";
    }).attr('selected', true);*/

    //$('.selectpicker').selectpicker('refresh')

    $("#circleTableProperties .createTable").removeClass('hide');
    $("#circleTableProperties .cancelUpdateTable, #circleTableProperties .updateTable").addClass("hide");
    $("#collapse2").attr("data-tableid","");

    //$('#circleTableProperties').rules('remove');
    formReset('#circleTableProperties')
    removeBtnDisabledAttr();

}

function formReset(id){
    $(id).find('label[class="error"]').remove();
    $(id).find('input').removeClass("error");

    var getMinGuestValue = $("#collapse2 ul.nav-pills li.active a").attr("data-minGuest");

    circleMinGuests.val(getMinGuestValue);
    circleMaxGuests.val(getMinGuestValue);
    gettableText.val("").nextAll().removeClass('hasVal');
}

$('.table_type').change(function(){
    var min=$('option:selected', this).attr('min');
    var max=$('option:selected', this).attr('max');
    var tat=$('option:selected', this).attr('tat');
    $('.minGuest').val(min);
    $('.maxGuest').val(max);

    if($(this).val()){
        $("#circleTableProperties .calc__row").removeClass("hide")
    } else {
        $("#circleTableProperties .calc__row").addClass("hide")
    }
});


/****************  Create Edit Floor *****************/

function createEditFloor(arr){

    tableManagement[0] = arr[0];
    tableManagement[1] = arr[1];
    tableManagement[2] = arr[2];

    var editFloorType = tableManagement[0].floorType;
    var editFloorRow = tableManagement[0].floorTypeRow;
    var editFloorColumn = tableManagement[0].floorTypeColumn;

    $("#accordion > div:first-child .panel-title").trigger("click");
    $("#floor-configration__layout > li").removeClass("active");
    $("#floor-configration__layout > li a[data-floorlayout="+ editFloorType+"]").parent().addClass("active");

    setTimeout(function(){
        $("#floor-configration__layout > li a[data-floorlayout="+ editFloorType+"]").trigger("click");
        floorSize(editFloorType, editFloorRow, editFloorColumn);
        //editCreateTable(tableManagement[1])
        //createTablesRow(tableManagement[1]);
        createTable();
        tableGoingOutsideFloor();
    },50)

    //console.log(tableManagement[1])

}

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

function getArrayIndexOfFloor(array, value ) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if ((array[k].name).toLowerCase() == value.toLowerCase()) {
            return true;
        }
    }
    return false;
}

//setup before functions
var typingTimer;                //timer identifier
var uniqueFloorNameInterval = 500;  //time in ms, 5 second for example
var $input = $("#floorNameForm #floorName");

//on keyup, start the countdown
$input.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(uniqueFloorName, uniqueFloorNameInterval);
});

//on keydown, clear the countdown
$input.on('keydown', function () {
    clearTimeout(typingTimer);
});

// paste
$input.bind('paste', function() {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(uniqueFloorName, uniqueFloorNameInterval);
})

//cut
$input.bind('cut', function() {
    typingTimer = setTimeout(uniqueFloorName, uniqueFloorNameInterval);
});


function uniqueFloorName(){
    var existingFloorName = $("#floorNameForm #floorName").attr('data-floorName');
    $("#floorNameForm").valid();

    if( ($("#floorNameForm").valid() && getArrayIndexOfFloor(JSON.parse(floor_list),$input.val().trim())) &&
        (existingFloorName != $input.val().trim())
    ){

        $input.parents("#floorNameForm").find('i.fa-check-circle').addClass('hide');
        $("<label for='floorName' class='error uniqueFloor'>Floor Name already exists. Please provide a unique Floor Name.</label>" ).insertAfter("#floorName");
        $('#layout_area #saveLayout').attr('disabled','disabled');

        //alert(existingFloorName)
        //alert($input.val().trim())
    } else {

        if(($input.val() == "") || (!$("#floorNameForm").valid())){
            $input.parents('#floorNameForm').find('i.fa-check-circle').addClass('hide');
            $('#layout_area #saveLayout').attr('disabled','disabled');
            $("#floorNameForm label.uniqueFloor").remove();
            $("#floorNameForm input#floorName").blur();
        } else {
            $input.parents('#floorNameForm').find('i.fa-check-circle').removeClass('hide');
            $('#layout_area #saveLayout').removeAttr('disabled');
        }

    }
}


function tableGoingOutsideFloor(){

    var outsideTablesIdArray = [];
    var outsideTablesTextArray = [];

    var currentFloorType = $("#floor-configration__layout li.active a").attr('data-floorLayout');
    var getFloorDefaultWidth = ($("#floor-configration__layout li a[data-floorlayout='"+ currentFloorType +"']").attr("data-mincolumn") * tableGroupWidth) + 2;
    var getFloorDefaultHeight = ($("#floor-configration__layout li a[data-floorlayout='"+ currentFloorType +"']").attr("data-minrow") * tableGroupHeight) + 2;

    //Find all shapes
    var allGroups = stage.find('Group');

    //Find all tables from shapes array
    var selectedGroup = $.grep(allGroups, function(e) {
        return e.attrs.name === 'mainGroup';
    });

    var getRoundSquareTables = $.grep(allGroups, function(e) {
        return e.attrs.name === 'Round-Square';
    });

    if (currentFloorType == "Round") {
        var x = getFloorDefaultWidth/2;
        var y = getFloorDefaultHeight/2;
        var radiusOffset = 0;
        var getTableMaxGuest = ""
        var getTableType = ""

        $.each(selectedGroup, function (key, value) {

            getTableMaxGuest = value.children[0].children[0].children.length
            getTableType = tableManagement[1][key]['tableShape']

            //console.log(radiusOffset+"-"+getTableType+"-"+getTableMaxGuest+"->"+tableManagement[1][key]['tableText'])

            if(getTableType == "Rectangle"){

                if(getTableMaxGuest <= 4){
                    radiusOffset = 50
                } else if((getTableMaxGuest > 4) == (getTableMaxGuest <= 6)){
                    radiusOffset = 46
                } else if((getTableMaxGuest > 6) == (getTableMaxGuest <= 8)){
                    radiusOffset = 85
                } else if((getTableMaxGuest > 8) == (getTableMaxGuest <= 10)){
                    radiusOffset = 100
                }

            } else if((getTableType == "Round") || (getTableType == "Square")) {

                if ((getTableMaxGuest > 8) == (getTableMaxGuest <= 10)) {
                    radiusOffset = 75
                } else {
                    radiusOffset = 51
                }

            } else {
                radiusOffset = 61
            }

            //console.log(radiusOffset)

            var cRadius = (((getFloorDefaultWidth / 2) + (getFloorDefaultHeight / 2)) / 2) - radiusOffset;
            var circle = cRadius / Math.sqrt(Math.pow(value.attrs.x - x, 2) + Math.pow(value.attrs.y - y, 2));

            if (circle < 1) {
                if (
                    ((value.attrs.x <= Math.round((value.attrs.x - x) * circle + x)) || (value.attrs.x >= Math.round((value.attrs.x - x) * circle + x)))
                ) {
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }
            }

        })

    }

    if ((currentFloorType == "Square") || (currentFloorType == "Rectangle")) {
        if (
            ((stage.getWidth() >= getFloorDefaultWidth) && (stage.getHeight() >= getFloorDefaultHeight)) ||
            ((stage.getWidth() >= getFloorDefaultWidth) || (stage.getHeight() >= getFloorDefaultHeight))

        ) {

            var tableWidth = "0"
            var tableHeight = "0"
            var tableOffsetWidth = "0"
            var tableOffsetHeight = "0"
            var floorBoundary = "0"

            $.each(selectedGroup, function (key, value) {

                //console.log(value.children[key])
                //console.log(key)
                //console.log(getRoundSquareTables[key])

                if((getRoundSquareTables[key] != undefined) && (isEmpty(getRoundSquareTables) == false) && (getRoundSquareTables[key].attrs.name == "Round-Square")) {

                    if((getRoundSquareTables[key].children[0].children.length > 8) == (getRoundSquareTables[key].children[0].children.length <= 10)){
                        tableWidth = value.attrs.width + 40;
                        tableHeight = value.attrs.height + 40;
                    }

                    tableOffsetWidth = 5;
                    tableOffsetHeight = 0;

                    floorBoundary = {
                        x: Math.round(tableWidth/2) - tableOffsetWidth,
                        y: Math.round(tableHeight/2) - tableOffsetHeight,
                        width: (stage.getWidth() - tableWidth) + tableOffsetWidth,
                        height: (stage.getHeight() - tableHeight) + tableOffsetHeight
                    };


                    //console.log(floorBoundary.width)
                    //console.log(floorBoundary.height)

                } else {

                    floorBoundary = {
                        x: Math.round(value.attrs.width/2) - 10,
                        y: Math.round(value.attrs.height/2) - 0,
                        width: (stage.getWidth() - value.attrs.width) + 5,
                        height: (stage.getHeight() - value.attrs.height) + 0
                    };
                }


                var thisTableRect = {x: this.x(), y: this.y(), width: this.width(), height: this.height()};

                var thisTableRect={
                    left: floorBoundary.x,
                    top: floorBoundary.y,
                    right: floorBoundary.x + floorBoundary.width + 15,
                    bottom: floorBoundary.y + floorBoundary.height
                };

                // left edge check
                var newTableX = (value.attrs.x < thisTableRect.left ? thisTableRect.left : value.attrs.x);

                // right edge check
                newTableX = (newTableX > thisTableRect.right ? thisTableRect.right : newTableX);

                // top edge check
                var newTableY = (value.attrs.y < thisTableRect.top ? thisTableRect.top : value.attrs.y);

                // bottom edge check
                newTableY = (newTableY > thisTableRect.bottom ? thisTableRect.bottom : newTableY);

                //console.log(newTableX+" -- "+value.attrs.x)
                //console.log(newTableY+" -- "+value.attrs.y)

                if(newTableX > (value.attrs.x) || newTableX < value.attrs.x){
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }

                if(newTableY > (value.attrs.y+1) || newTableY < value.attrs.y){
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }
            })
        }

    } /*else if (currentFloorType == "Rectangle") {

        if ((stage.getWidth() > getFloorDefaultWidth) || (stage.getHeight() > getFloorDefaultHeight)) {

            $.each(selectedGroup, function (key, value) {

                if (value.attrs.x > getFloorDefaultWidth) {
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }

                if (value.attrs.y > getFloorDefaultHeight) {
                    outsideTablesTextArray.push(value.children[1].children[0].partialText)
                }

            })
        }

    }*/

    outsideTablesTextArray = outsideTablesTextArray.filter(function(itm, i, a) { return i == a.indexOf(itm) });

    if(outsideTablesTextArray.length > 0) {
        $("#alert").addClass('alert-danger').removeClass('hide').find('#msgTableOutside').removeClass('hide').html("Table "+outsideTablesTextArray.join(", ")+" is going outside the floor.")
        $("#saveLayout,#circleTableProperties .createTable, #tableListing .btn,#resetLayout,.updateTable").attr('disabled',true).addClass('disabled');
    } else {
        if(!$("#msgTableOutside").hasClass('hide') && $("#msgTableOverlap").hasClass('hide')){
            $("#alert").find('#msgTableOutside').addClass('hide').empty();
            $("#alert").removeClass('alert-danger').addClass('hide')
        }
    }
}
