$(document).ready(function () {
    var dateView = 'date';
    var dayView = 'day';
    var dayoffToggle = false;
    displayWeekCalendar();
    var timeInterval=0;
    //$( "#exTab1" ).tabs( { disabled: [1] } );
    //$( "#custTab" ).tabs( { disabled: [1] } );
    $('#custTab').click( function(){ return false; });

    $("#custom_calendar").fullCalendar({
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        header: false,
        defaultView: 'timelineDay',
        selectable: true,
        defaultTimedEventDuration: '00:30:00',
        minTime: "00:00:00",
        maxTime: "24:00:00",
        slotMinutes: 30,
        slotDuration: '00:30:00',
        slotLabelFormat: 'HH:mm',
        slotWidth:60,
        scrollTime: '00:00:00',
        editable: true,
        eventStartEditable: false,
        eventDurationEditable: true,
        allDaySlot: false,
        height: 'auto',
        businessHours: [{
            dow: [0, 1, 2, 3, 4, 5, 6], // Monday - Friday
            start: '00:00',
            end: '24:00',
        }],
        select: function (start, end, jsEvent, view) {

            var $form = $('#custom_hours');
            if(!$form.valid()){
                return false;
            }

            weekHoursResetForm();

            var open_time_hour = moment(start).format("HH");
            var open_time_min = moment(start).format("mm");
            var close_time_hour = moment(end).format("HH");
            var close_time_min = moment(end).format("mm");



            clearFormdata();
            saveWeekTableAvailability(dateView, start, end);
            toggleFormEditable(true);


        },
        selectAllow: function(selectInfo) {
            var duration = moment.duration(selectInfo.end.diff(selectInfo.start));
            timeInterval = round(duration.asMinutes(), 1);
        }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if(target=='#custom_operation_hours'){
            displayCustomRangeCalendar();
            $("#type").val(dateView);
        }else{
            displayWeekCalendar();
            $("#type").val(dayView);
        }
    });

    function isOverlapping(event){
        var array = calendar.fullCalendar('clientEvents');

        for(i in array){
            if(array[i].id != event.id){
                console.log(array[i].id)
                if(!(array[i].start >= event.end || array[i].end <= event.start)){
                    return true;
                }
            }
        }



        return false;
    }

    function displayWeekCalendar(){
        $("#type").val(dayView);
        $("#calendar").fullCalendar('render');
        var url = $('#url').val();
        var edit_id = $('#edit_id').val();
        var slot_name = $('#slot_name').val();
        var slot_code = $('#visibility_dur_code').val();
        $.ajax({
            type : 'POST',
            url: '/configure/visibility-duration-schedule',
            data : {url : url, edit_id : edit_id, slot_name : slot_name, slot_code : slot_code},
            dataType: 'json',
            success: function (data) {

                window.dataLogCalendar = data.date_range;

                $('#calendar').fullCalendar({
                    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                    header: false,
                    defaultView: 'timelineDay',
                    selectable: true,
                    defaultTimedEventDuration: '00:30:00',
                    minTime: "00:00:00",
                    maxTime: "24:00:00",
                    disableDragging: true,
                    slotMinutes: 30,
                    slotWidth:60,
                    slotDuration: '00:30:00',
                    slotLabelFormat: 'HH:mm',
                    scrollTime: '00:00:00',
                    //editable: true,
                    eventStartEditable: false,
                    //eventDurationEditable: true,
                    allDaySlot: false,
                    businessHours: [{
                        dow: [0, 1, 2, 3, 4, 5, 6], // Monday - Friday
                        start: '00:00',
                        end: '24:00',
                    }],
                    navLinks: true,
                    resourceAreaWidth: '170',
                    resourceColumns: [
                        {
                            labelText: '',
                            field: 'title'
                        },
                        {
                            labelText: 'Open',
                            field: 'open'
                        }
                    ],
                    resources:data.date_range,
                    resourceRender: function(resourceObj, $td) {
                        $td.eq(1).find('.fc-cell-content')
                            .html('<button type="button" class="btn btn-xs btn-secondary btn-toggle dayoff active" title="Close"  aria-pressed="true" autocomplete="off" id="dayoff_'+resourceObj.title+'"><div class="handle"></div></button>');

                        if(!resourceObj.dayoff){
                            $('#dayoff_'+resourceObj.title).addClass('active').attr('title','Open');
                            //$("tr[data-resource-id="+ resourceObj.id +"]").removeClass("dayOff");

                        } else {
                            $('#dayoff_'+resourceObj.title).removeClass('active').attr('title','Close');
                            //$("tr[data-resource-id="+ resourceObj.id +"]").addClass("dayOff");
                        }
                    },
                    events: data.events,
                    selectOverlap: function(event) {
                        $("#slot-overlap-alert").modal('show');
                    },
                    eventRender: function(event,element) {
                        element.find('.fc-title').html(event.title);
                        
                        if(event.title){

                            $("tr[data-resource-id="+ event.resourceId +"]").find('.dayoff').addClass("has_events")   
                        }

                        var getDayOffBtnId = $("tr[data-resource-id="+ event.resourceId +"]").find('.dayoff').attr('id');

                        $.each(data.date_range, function(key,value){
                            if($('#dayoff_'+value.title).hasClass("active")){
                                $('#dayoff_'+value.title).attr('title','Open');
                                $("tr[data-resource-id="+ value.id +"]").removeClass("dayOff");
                            } else {
                                $('#dayoff_'+value.title).attr('title','Close');
                                $("tr[data-resource-id="+ value.id +"]").addClass("dayOff");
                            }
                        })

                    },
                    height: 'auto',
                    selectConstraint: "businessHours",
                    slotLabelInterval: 30,
                    firstDay: 1,
                    fixedWeekCount: true,
                    aspectRatio: 0.1,
                    select: function (start, end, jsEvent, view, resource) {

                        weekHoursResetForm();

                        $('#week_hours .day_repeat_div').show();
                        $('#week_hours .check__box__input').prop('checked', false);

                        var open_time_hour = moment(start).format("HH");
                        var open_time_min = moment(start).format("mm");
                        var close_time_hour = moment(end).format("HH");
                        var close_time_min = moment(end).format("mm");


                       
                            clearFormdata();
                            saveWeekTableAvailability(dayView, start, end, resource);
                            toggleFormEditable(true);
                     

                        $.each(dataLogCalendar, function(key, value){
                            var dayName = value.title.toLowerCase();
                            if(value.dayoff) {
                                $("#week_hours #repeat_"+dayName).attr("disabled",true);
                            } else {
                                $("#week_hours #repeat_"+dayName).removeAttr("disabled");
                            }
                        });

                        var selectedDay = resource.title.toLowerCase();

                        $("#week_hours .check__box__input").removeAttr("checked").removeAttr("readonly");
                        if($("#week_hours #repeat_"+selectedDay)){
                            $("#week_hours #repeat_"+selectedDay).attr("checked","checked").attr("readonly","readonly");
                        }

                    },
                    eventClick: function (event, jsEvent, view) {

                        if(!$("tr[data-resource-id="+ event.resourceId +"]").hasClass("dayOff")){
                            weekHoursResetForm();

                            updateWeekTableAvailability(event, dayView);
                            toggleFormEditable(false);
                            $('#week_hours .check__box__input').prop('checked', false);
                            $('#week_hours .day_repeat_div').hide();


                            var open_time_hour = moment(event.start._i).format("HH");
                            var open_time_min = moment(event.start._i).format("mm");
                            var close_time_hour = moment(event.end._i).format("HH");
                            var close_time_min = moment(event.end._i).format("mm");


                            $.each(dataLogCalendar, function(key, value){
                                var dayName = value.title.toLowerCase();
                                if(value.dayoff) {
                                    $("#week_hours #repeat_"+dayName).attr("disabled",true);
                                } else {
                                    $("#week_hours #repeat_"+dayName).removeAttr("disabled");
                                }
                            });

                            var filteredDay = dataLogCalendar.filter(function( idx ) {
                                return idx.id == event.resourceId;
                            });

                            $.each(filteredDay, function(key, value){

                                var selectedDay = value.title.toLowerCase();

                                $("#week_hours .check__box__input").removeAttr("checked").removeAttr("readonly");
                                if($("#week_hours #repeat_"+selectedDay)){
                                    $("#week_hours #repeat_"+selectedDay).attr("checked","checked").attr("readonly","readonly");
                                }
                            })

                        }
                    },
                    eventResize: function (event, delta, revertFunc) {
                        updateWeekTableAvailability(event, dayView);
                        toggleFormEditable(true);
                    },
                    selectAllow: function(selectInfo) {
                        var duration = moment.duration(selectInfo.end.diff(selectInfo.start));
                        timeInterval = round(duration.asMinutes(), 1);
                    }
                });
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('addEventSource', data.events);
                $('#calendar').fullCalendar('rerenderEvents' );

                $("#availabilityModal,#confirm-turnover-alert").on("hidden.bs.modal", function () {
                    // put your default event here
                    //location.reload(true);
                });
            }
        });
    }

    function isOverlapping(event) {
        var arrCalEvents = $("#calendar").fullCalendar('clientEvents');
        for (i in arrCalEvents) {
            console.log(event);
            if (arrCalEvents[i].id != event.id) {
                if ((event.end >= arrCalEvents[i].start && event.start <= arrCalEvents[i].end) || (event.end == null && (event.start >= arrCalEvents[i].start && event.start <= arrCalEvents[i].end))) {//!(Date(arrCalEvents[i].start) >= Date(event.end) || Date(arrCalEvents[i].end) <= Date(event.start))
                    return true;
                }
            }
        }
        return false;
    }

    function displayCustomRangeCalendar() {
        $("#custom_calendar_view").fullCalendar('destroy');

        $.ajax({
            url: 'custom-visibility-duration-schedule',
            dataType: 'json',
            success: function (data) {
                if(data.date_range == ""){
                    $('#custom_calendar_view').addClass("hide");
                } else {
                    $('#custom_calendar_view').removeClass("hide").fullCalendar({
                        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                        header: false,
                        defaultView: 'timelineDay',
                        selectable: true,
                        defaultTimedEventDuration: '00:30:00',
                        minTime: "00:00:00",
                        maxTime: "24:00:00",
                        disableDragging: true,
                        slotMinutes: 30,
                        slotWidth:60,
                        slotDuration: '00:30:00',
                        slotLabelFormat: 'HH:mm',
                        scrollTime: '00:00:00',
                        //editable: true,
                        eventStartEditable: false,
                        //eventDurationEditable: true,
                        allDaySlot: false,
                        businessHours: [{
                            dow: [0, 1, 2, 3, 4, 5, 6], // Monday - Friday
                            start: '00:00',
                            end: '24:00',
                        }],
                        navLinks: true,
                        resourceAreaWidth: '300',
                        resourceColumns: [
                            {
                                labelText: '',
                                field: 'title'
                            },
                            {
                                labelText: 'Open',
                                field: 'open'
                            },
                            {
                                labelText: 'Delete',
                                field: 'delete'
                            }
                        ],
                        resources:data.date_range,
                        resourceRender: function(resourceObj, $td) {

                            $td.eq(1).find('.fc-cell-content')
                                .html('<button type="button" class="btn btn-xs btn-secondary btn-toggle dayoff"  aria-pressed="" autocomplete="off" id="dayoff_'+resourceObj.id+'"><div class="handle"></div></button>');

                            if(!resourceObj.dayoff){
                                $('#dayoff_'+resourceObj.id).addClass('active').attr('title','Open');
                                $("tr[data-resource-id="+ resourceObj.id +"]").removeClass("dayOff");

                            } else {
                                $('#dayoff_'+resourceObj.id).removeClass('active').attr('title','Close');
                                $("tr[data-resource-id="+ resourceObj.id +"]").addClass("dayOff");
                            }

                            $td.eq(2).find('.fc-cell-content')
                                .html("<div class='custom_delete deleteworkingslot'  id='deleteCustomSlot_"+resourceObj.id+"'><img src='/images/delete.png' alt='delete'/></div>")
                        },
                        events: data.events,
                        selectOverlap: function(event) {
                            $("#slot-overlap-alert").modal('show');
                        },
                        eventRender: function(event, element) {

                            element.find('.fc-title').html(event.title);
                            if(event.title){
                                $("tr[data-resource-id="+ event.resourceId +"]").find('.dayoff').addClass("has_events")
                            }
                            $.each(data.date_range, function(key,value){
                                if($('#dayoff_'+value.id).hasClass("active")){
                                    $('#dayoff_'+value.id).attr('title','Open');
                                    $("tr[data-resource-id="+ value.id +"]").removeClass("dayOff");
                                } else {
                                    $('#dayoff_'+value.id).attr('title','Close');
                                    $("tr[data-resource-id="+ value.id +"]").addClass("dayOff");
                                }
                            })

                        },
                        height: 'auto',
                        selectConstraint: "businessHours",
                        select: function (start, end, jsEvent, view, resource) {
                            weekHoursResetForm();

                            var open_time_hour = moment(start).format("HH");
                            var open_time_min = moment(start).format("mm");
                            var close_time_hour = moment(end).format("HH");
                            var close_time_min = moment(end).format("mm");



                            clearFormdata();
                            saveWeekTableAvailability(dateView, start, end, resource);
                            toggleFormEditable(true);


                        },
                        eventClick: function (event, jsEvent, view) {
                            if(!$("tr[data-resource-id="+ event.resourceId +"]").hasClass("dayOff")){
                                weekHoursResetForm();

                                updateWeekTableAvailability(event, dateView);
                                toggleFormEditable(false);

                                $('#week_hours .check__box__input').prop('checked', false);

                                $.each(view.options.resources, function(key, value){
                                    var dayName = value.title.toLowerCase();

                                    if(value.dayoff) {
                                        $("#week_hours #repeat_"+dayName).attr("disabled",true);
                                    } else {
                                        $("#week_hours #repeat_"+dayName).removeAttr("disabled");
                                    }
                                });
                            }
                        },
                        eventResize: function (event, delta, revertFunc) {
                            updateWeekTableAvailability(event, dateView);
                            toggleFormEditable(true)
                        },
                        selectAllow: function(selectInfo) {
                            var duration = moment.duration(selectInfo.end.diff(selectInfo.start));
                            timeInterval = round(duration.asMinutes(), 1);
                        }
                    });

                }


                $('#custom_calendar_view').fullCalendar('removeEvents');
                $('#custom_calendar_view').fullCalendar('addEventSource', data.events);
                $('#custom_calendar_view').fullCalendar('rerenderEvents' );
            }
        });
    }


    function saveWeekTableAvailability(type, start, end, resource) {
        if (resource === undefined) {
            resource = "";
        }


        var open_time_hour = moment(start).format("HH");
        var open_time_min = moment(start).format("mm");
        var close_time_hour = moment(end).format("HH");
        var close_time_mins = moment(end).format("mm");

        close_time_hour=((close_time_hour=="00" && open_time_hour!="00") || (close_time_hour=="00" &&  open_time_hour=="00"  &&  close_time_mins=="00" &&  open_time_min=="00" ))?24:close_time_hour;


        $("#slot_time").val(open_time_hour+":"+open_time_min +" - "+ close_time_hour+":"+close_time_mins);

        //console.log(close_time - open_time)

        if(type == dayView){
            $('.day_repeat_div').show();
        }else{
            $('#customSettingModal').modal('hide');
            $('.day_repeat_div').hide();
        }

        $('#availabilityModal').modal('show');

        $(".done").unbind('click');

        $(".done").click(function () {
            var $form = $('#week_hours');
            if(!$form.valid() || $('#visibility_dur_name').val() == ''){
                //return false;

                $('#availabilityModal').modal('hide');
                $('#visibility_dur_name_err').addClass('text-danger').html('Please enter name.');
                $('#visibility_dur_name').focus();
                //return false;
            }

            if(!$form.valid() || $('#visibility_dur_code').val() == ''){
                $('#availabilityModal').modal('hide');
                $('#visibility_dur_code_err').addClass('text-danger').html('Please enter code.');
                if($('#visibility_dur_name').val() != ''){
                    $('#visibility_dur_code').focus();
                }

                //return false;
            }
            if($('#visibility_dur_name').val() != ''){
                $('#visibility_dur_name_err').removeClass('text-danger').html('');
            }
            if($('#visibility_dur_code').val() != ''){
                $('#visibility_dur_code_err').removeClass('text-danger').html('');
            }



            var inputData = $form.serialize();
            var url = $form.attr('action');
            if(resource){
                var resourceId = resource.id;
            }

            if(type == dateView){
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                inputData+= "&start_date="+start_date+"&end_date="+end_date;
                if(resourceId){
                    inputData+= "&schd_id="+resourceId;
                }
            }else{
                inputData+= "&calendar_day=" + resourceId;

            }
            var editData;
            if($('#edit_id').val() !=''){
                editData = '&edit_id='+$('#edit_id').val();
            }else{
                editData= '&edit_id=-1';
            }
            inputData+= "&open_time=" + open_time_hour+":"+open_time_min + "&close_time=" + close_time_hour+":"+close_time_mins + "&type="+type+"&slot_name="+$('#visibility_dur_name').val()+"&slot_code="+$('#visibility_dur_code').val()+editData;
            $.ajax({
                type: 'POST',
                url: url,
                data: inputData,
                success: function (data) {
                    if (data.success == false){
                        $('#availabilityModal').modal('hide');
                        $('#visibility_dur_code').val('');
                        $('#visibility_dur_code_err').addClass('text-danger').html(data.message);
                    }else {
                        $('#edit_id').val(data.insertedId);
                        $('#slot_name').val(data.slot_name);
                        $('#availabilityModal').modal('hide');
                        $('#visibility_dur_code_err').removeClass('text-danger').html('');
                        $('#custTab').unbind('click');
                        if (type == dayView) {
                            displayWeekCalendar();
                            $("tr[data-resource-id=" + resourceId + "]").removeClass("dayOff");
                        } else {
                            $('#customSettingModal').modal('hide');
                            $('#custom_hours').find('input:text').val('');
                            $(".modal-backdrop").remove();
                            displayCustomRangeCalendar();
                        }
                    }
                },
                error: function (data, textStatus, errorThrown) {
                    $.each(data.responseJSON.errors, function(key, value){
                        $('.alert-danger').empty().html('<p>'+value+'</p>');
                        $('.alert-danger').show();
                    });
                }
            });
        });
    }

    




    function updateWeekTableAvailability(event, type) {
        if(type == dateView){
            $('.day_repeat_div').hide();
        }else{
            $('.day_repeat_div').show();
        }
        //event.resourceId is the day of the weeek, 0 for sun, 1 for monday and so on
        $.ajax({
            url: '/configure/visibility-slot-detail/'+event.resourceId,
            data: 'type='+type+'&slot_id='+event.slot_id+'&slot_name='+$('#slot_name').val(),
            dataType: 'json',
            success: function (data) {
                var totalTable = 0;
                var totalCover = 0;

                $('#slot_name').val(data.slot_detail.slot_name);
                $('#slot_id').val(event.slot_id);
                //$('#slot_id').val(data.slot_detail.pk_id);
                //$('#slot_id').val(data.slot_detail.slot_id);
                $('#schd_id').val(event.resourceId);


                var open_time_hour = moment(event.start).format("HH");
                var open_time_min = moment(event.start).format("mm");
                var close_time_hour = moment(event.end).format("HH");
                var close_time_mins = moment(event.end).format("mm");
                close_time_hour=((close_time_hour=="00" && open_time_hour!="00") || (close_time_hour=="00" &&  open_time_hour=="00"  &&  close_time_min=="00" &&  open_time_min=="00" ))?24:close_time_hour;

                $("#slot_time").val(open_time_hour+":"+open_time_min +" - "+ close_time_hour+":"+close_time_mins);

                $('#availabilityModal').modal('show');
                $(".done").unbind('click');
                $(".done").click(function () {alert('done clicked');
                    var $form = $('#week_hours');
                    if(!$form.valid()){
                        return false;
                    }
                    var selectedDay = event.resourceId;
                    var data = $("#week_hours").serialize();
                    var url = $('#week_hours').attr('action');
                    $.ajax({
                        type: 'PUT',
                        url: url + '/' + event.resourceId,
                        data: data + "&open_time=" + open_time_hour+":"+open_time_min + "&close_time=" + close_time_hour+":"+close_time_mins + "&calendar_day=" + selectedDay + "&type="+type+"&_token=" + $('#token').val() + "&schd_id=" + event.resourceId+'&slot_id='+data.slot_detail.pk_id,
                        success: function (data) {
                            $('#availabilityModal').modal('toggle');
                            if(type == dateView){
                                displayCustomRangeCalendar();
                            }else{
                                displayWeekCalendar();
                            }
                        },
                        error: function (data, textStatus, errorThrown) {
                            $.each(data.responseJSON.errors, function(key, value){
                                $('.alert-danger').show();
                                $('.alert-danger').empty().html('<p>'+value+'</p>');
                            });
                        }
                    });
                });
            }
        });
    }
    function updateReservation(resourceId,type,status){
        $.ajax({
            type: 'POST',
            url: '/configure/visibility-hours-dayoff',
            data: "resourceId=" + resourceId + "&status=" + status+"&type="+type+"&_token=" + $('#token').val() + '&slot_name=' + $('#visibility_dur_name').val(),
            success: function (data) {
                if(type == dateView){
                    displayCustomRangeCalendar();
                }else{
                    displayWeekCalendar();

                }
            },
            error: function (data, textStatus, errorThrown) {

            }
        });
    }
    $(document).on('click','.dayoff',function(event){

        var currel=$(this);
        var resourceId = this.id;
        var type = $("#type").val();

        //var status = this.getAttribute("aria-pressed");
        var status =$(this).hasClass('active')?false:true;
        console.log(status);

        if($(this).hasClass('active') && $(this).hasClass('has_events')){

            ConfirmBox('Confirm','All reservation made on this day will be cancelled. Do you wish to proceed?',function(resp,curmodal){

                if(resp){
                    updateReservation(resourceId,type,status);
                    curmodal.modal('hide');
                    currel.removeClass('active');
                    currel.removeClass('has_events');
                    currel.attr("aria-pressed",false);
                }
            });

        }else{
            if(status){
                currel.addClass('active');
            }else{
                currel.removeClass('active');
            }

            currel.attr("aria-pressed",true);
            updateReservation(resourceId,type,status);
        }


    });

    /*
        $(document).on('click','.dayoff',function(){

            var resourceId = this.id;
            var type = $("#type").val();
            var status = this.getAttribute("aria-pressed");
            console.log(type);
            $.ajax({
                type: 'POST',
                url: 'delivery-hours-dayoff',
                data: "resourceId=" + resourceId + "&status=" + status+"&type="+type+"&_token=" + $('#token').val(),
                success: function (data) {
                    if(type == dateView){
                        displayCustomRangeCalendar();
                    }else{
                        displayWeekCalendar();

                    }
                },
                error: function (data, textStatus, errorThrown) {

                }
            });
        });
        */

    $('#customSettingModal').on('shown.bs.modal', function () {
        $('#custom_hours').find('input:text, checkbox').val('');
        $("#custom_calendar").fullCalendar('render');
    });

    $(function(){
        $('#start_date').datepicker({
            showAnim: "slideDown",
            dayNamesMin: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
            minDate:0,
            beforeShow : function(){
                $( this ).datepicker('option', {
                    maxDate: ($('#end_date').val())?$('#end_date').val():"+12m",
                    minDate: 0,
                    dateFormat: 'dd-mm-yy'
                });
            },
            onSelect: function(dateText, inst) {
                $(this).focusout();
            }
        });
        $('#end_date').datepicker({
            showAnim: "slideDown",
            dayNamesMin: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
            minDate: 0,
            beforeShow : function(){
                $( this ).datepicker('option', {
                    minDate: ($('#start_date').val())?$('#start_date').val():0,
                    dateFormat: 'dd-mm-yy',
                    onSelect: function(dateText, inst){
                        $(this).focusout();
                        //$("#start_date").datepicker('option', 'minDate', dateText);
                    }
                });
            },
            onSelect: function(dateText, inst) {
                $(this).focusout();
            }
        });

        jQuery.validator.addMethod("min_custom", function(value, element, param) {
            return this.optional(element) || value >= param;
        }, jQuery.format("Please enter atleast 1 table."))


        /* jQuery.validator.addMethod("require_from_group_custom", function(value, element, options) {
             // Nathan's code without any changes
             var validator = this;
             var selector = options[1];
             var validOrNot = $(selector, element.form).filter(function() {
                     return validator.elementValue(this);
                 }).length >= options[0];

             if(!$(element).data('being_validated')) {
                 var fields = $(selector, element.form);
                 fields.data('being_validated', true);
                 fields.valid();
                 fields.data('being_validated', false);
             }
             return validOrNot;

             //return this.optional(element) || 0 >= param
         }, jQuery.format("Please enter a value greater than or equal to 1 in Table Type."));

         // "filone" is the class we will use for the input elements at this example
         jQuery.validator.addClassRules("required-table-value-greater-then-0", {
             require_from_group_custom: [1,".required-table-value-greater-then-0"]
         });*/


    });



    function clearFormdata() {
        //$('#week_hours').find('input:text').val('');

        $('.alert-danger').hide();
        $('.alert-danger').empty();

        //Form Reset
        $('#week_hours')[0].reset();
    }

    function weekHoursResetForm(){
        var weekHoursValidator = $("#week_hours").validate();
        weekHoursValidator.resetForm();
    }

    $('.edit').on('click', function () {
        toggleFormEditable(true);

        if(!$("#custom_operation_hours").hasClass("active")){
            $('#week_hours .day_repeat_div').show();
        }

    });

    $('.dayoff_custom').on('click', function () {
        dayoffToggle = this.getAttribute("aria-pressed");
        if($(this).attr('title') == "Open"){
            $(this).attr('title','Close');
            $("#customSettingModal .custom_modalHrs").addClass("hide");
            $("#customSettingModal .custom_done").removeClass("hide");
        } else {
            $(this).attr('title','Open');
            $("#customSettingModal .custom_modalHrs").removeClass("hide");
            $("#customSettingModal .custom_done").addClass("hide");
        }
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(".btn-ok").unbind('click');
        $(this).find('.btn-ok').on('click', function () {
            var schd_id = $('#schd_id').val();
            var slot_id = $('#slot_id').val();
            var slot_name = $('#slot_name').val();
            var type = $('#type').val();
            var inputData = 'type='+type+'&_token=' + $('#token').val()+'&slot_id='+slot_id+'&slot_name='+slot_name;
            $.ajax({
                type: 'DELETE',
                url: '/configure/visibility-duration/'+schd_id,
                data: inputData,
                success: function (data) {
                    $('#confirm-delete').modal('toggle');
                    $('#availabilityModal').modal('hide');
                    if(type == dateView){
                        displayCustomRangeCalendar();
                    }else{
                        displayWeekCalendar();
                    }
                },
                error: function (data, textStatus, errorThrown) {
                    $.each(data.responseJSON.errors, function(key, value){
                        $('.alert-danger').empty().html('<p>'+value+'</p>');
                        $('.alert-danger').show();
                    });
                }
            });
        });
    });

    function toggleFormEditable(switcher) {
        if(switcher){
            $(".editable_footer").show();
            $(".non_editable_footer").hide();
            $("#week_hours input[type='text']").attr("disabled", false);

        }else{
            $(".editable_footer").hide();
            $(".non_editable_footer").show();
            $("#week_hours input[type='text']").attr("disabled", true);
            $('input:checkbox').removeAttr('checked');
            $("#week_hours .modal-footer :input").attr("disabled", false);
        }
    }

    $(".custom_done").on('click', function () {
        var $form = $("#custom_hours");
        if(!$form.valid()){
            return false;
        }
        var inputData = $("#custom_hours").serialize();
        var url = $('#custom_hours').attr('action');
        inputData+="&status="+dayoffToggle+"&type="+$("#type").val()+"&_token=" + $('#token').val();
        $.ajax({
            type: 'POST',
            url: url,
            data: inputData,
            success: function (data) {
                $('#customSettingModal').modal('hide');
                $('#custom_hours').find('input:text').val('');
                $(".modal-backdrop").remove();
                displayCustomRangeCalendar();
            },
            error: function (data, textStatus, errorThrown) {
                $.each(data.responseJSON.errors, function(key, value){
                    $('.alert-danger').show();
                    $('.alert-danger').empty().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+value);
                });
            }
        });
    });

    $(document).on('click', '.custom_delete', function () {
        var schdId = this.id;
        schdId = schdId.split('_');
        $('#schd_id').val(schdId[1]);
        $('#slot_id').val(0);
    });

    function round(value, decimals) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    }


    $('#custom_hours').validate({
        errorPlacement: $.datepicker.errorPlacement,
        rules: {
            slot_time: {
                required: true
            }
        }
    });

    $( "#week_hours" ).validate({
        rules: {
            'slot_time': {
                required: true,
                maxlength: 20
            },

        }
    });


    $('#equal-pacing').click(function(){
        if($(this).prop("checked") == true){
            $(this).parents('td').nextAll('td').find('div').removeClass('hide');
            $(this).parents('tr').nextAll('tr').addClass('hide');
        }
        else if($(this).prop("checked") == false){
            $(this).parents('td').nextAll('td').find('div').addClass('hide');
            $(this).parents('tr').nextAll('tr').removeClass('hide');
        }
    });

});

function confirmDeleteWorkingHour(deltype){
    //var type=$('#type').val();
    var desc="";
    if(deltype=='slot'){
        desc="You are about to delete one slot. Do you want to proceed?";
    }else{
        desc="You are about to delete a custom setting. Do you want to proceed?";
    }
    $('#confirm-delete .desc p,#confirm-delete:hidden .desc p').text(desc);

    $('#confirm-delete').modal('toggle');
}
$('body').on('click', '.deleteworkingslot', function() {
    var deltype='setting';
    if($(this).hasClass('deleteslot')){
        deltype='slot';
    }
    confirmDeleteWorkingHour(deltype);
});
/*
 $('.timepicker').timepicker({
            interval: 30,
            minTime: '00:30',
            maxTime: '23:30'
        }).on('click', function (e) {
            $(".ui-timepicker-wrapper").mCustomScrollbar({
                theme: "dark"
            });
        });*/
