function update(jscolor) {
    document.getElementById('rect').style.backgroundColor = '#' + jscolor
}

/*

var blockSnapSize = 20,
    tableAvailableColor = "#ccc",
    stageWidth = 5 * (blockSnapSize * 6), //Default Floor Width 600,
    stageHeight = 5 * (blockSnapSize * 6), //Default Floor Height 600,
    centerTableXaxis = Math.round(blockSnapSize * 3), //60
    centerTableYaxis = Math.round(blockSnapSize * 3), //60
    tableColor = "#ccc",
    tableTextSize = Math.round(blockSnapSize/1.2), //26,
    chairOffset = Math.round(blockSnapSize * 2), //40,,
    tableIcon = "",
    tableIconColor = "white",
    sidebarStatusColor = "",
    chairSize = "",
    chairWidth = "26",
    chairColor = "#aaa",
    circleTableSize = "",
    movingTableWidth = "",
    movingTableHeight = "",
    tableTextWidth = "",
    tableTextXaxis = "",
    tableTextYaxis = "",
    tableLinkYaxis= "",
    tableSelectedColor = "#ff8700",
    enableDoubleClick = false,
    tableStroke = "white"

//Stage
var stage = new Konva.Stage({
    container: "floor-tables-view",
    width: stageWidth,
    height: stageHeight
})

//Layer 1
var layer = new Konva.Layer();

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function deleteMultipleTable(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k] == value) {
            array.splice(k, 1);
            return k;
        }
    }
    return false;
}

function getArrayIndexOf(array, value) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if (array[k] == value) {
            return value;
        }
    }
    return false;
}


var selectedTablesId = []

function selectMultipleTables(id,targetElement){

    var selectedTable = targetElement
    var selectedChairs = selectedTable.parent.parent.children[0].children

    if (getArrayIndexOf(selectedTablesId, id) == id) {

        selectedTable.stroke(tableStroke);

        $.each(selectedChairs, function (key, value) {
            value.fill(chairColor)
        })

        layer.draw();

        deleteMultipleTable(selectedTablesId, id)
    } else {
        selectedTable.stroke(tableSelectedColor);

        $.each(selectedChairs, function (key, value) {
            value.fill(tableSelectedColor)
        })

        selectedTablesId.push(id)

        layer.draw();
    }

    console.log(selectedTablesId)

}

function floorView(arr){

    var floor_data=[];
    var tableManagement=[];

    floor_data[0]=JSON.parse(arr[0]);
    floor_data[2]=JSON.parse(arr[2]);
    floor_data[1]=[];

    $.each(arr[1], function(key, value){
        floor_data[1][key] = JSON.parse(value)
    })

    tableManagement = floor_data;

    if(tableManagement[0]['floorType'] == "Round"){
        $("#floor-tables-view").addClass("typeCircle")
    }

    $(".floor_nav .current__floor h2").text(tableManagement[0]['floorName'])

    var stageWidth = 120 * tableManagement[0]['floorTypeColumn']
    var stageHeight = 120 * tableManagement[0]['floorTypeRow']
    var marginArea = 40

    $("#floor-tables-view").width(stageWidth).height(stageHeight);
    stage.width(stageWidth).height(stageHeight)

    editCreateTable(tableManagement[1],tableManagement[2])
}

function getStatus(obj, val) {
    var resp=false;
    $.each( obj, function( key, value ) {
        if (value === val) {
            resp= val;
        }
    });
    return resp;
}

function editCreateTable(array,status){

    stage.destroyChildren();

    var table;

    $("#statusStyle").empty();

    $.each(array, function(key, value){

        tableSelectedColor = value.serverColor

        $(".table_assign_name i, .table_assign_name span").css('color',tableSelectedColor)

        var keyGroup = new Konva.Group({
            x: value.tableXAxis - Math.round(value.tableWidth / 2),
            y: value.tableYAxis - Math.round(value.tableHeight / 2),
            id: value.id,
            opacity: 1
        });

        var bottomGroup = new Konva.Group({
            x: value.tableBottomXAxis,
            y: value.tableBottomYAxis,
            width: value.tableWidth,
            height: value.tableHeight,
            offset: {
                x: Math.round(value.tableWidth / 2),
                y: Math.round(value.tableHeight / 2)
            },
            rotation: value.tableRotation
        });


        var bottomTableGroup = new Konva.Group();
        var bottomChairGroup = new Konva.Group();

        if(getStatus(status,value.tableAvailability) == value.tableAvailability){
            tableColor = JSON.parse(value.reservations[0]['status']['color']).table
            tableIcon = JSON.parse(value.reservations[0]['status']['icon']).table
            sidebarStatusColor = JSON.parse(value.reservations[0]['status']['color']).sidebar
        } else {
            tableColor = tableAvailableColor
        }

        var statusBtnStyle = ".bootstrap-select .btn.status-"+ value.tableAvailability +"{";
        statusBtnStyle += "background-color: "+ sidebarStatusColor +";";
        statusBtnStyle += "border-color: "+ sidebarStatusColor +";";
        statusBtnStyle += "outline: none !important";
        statusBtnStyle += "}";

        $("#statusStyle").append(statusBtnStyle)

        if(value.tableShape == "Round") {
            if (value.maxGuests <= 4) {
                circleTableSize = Math.round(blockSnapSize * 2) - Math.round(blockSnapSize / 2);
                chairSize = Math.round(blockSnapSize / 1.5)
                chairOffset = Math.round(blockSnapSize * 1.3)
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
                tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/2)
            } else if ((value.maxGuests > 4) && (value.maxGuests <= 6)) {
                circleTableSize = Math.round(blockSnapSize * 1.8)
                chairOffset = Math.round(blockSnapSize * 1.6)
                chairSize = Math.round(blockSnapSize / 1.5)
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
                tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/1.5)
            } else if ((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                circleTableSize = Math.round(blockSnapSize * 2.2)
                chairOffset = Math.round(blockSnapSize * 2)
                chairSize = Math.round(blockSnapSize / 1.5)
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
                tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/1.2)
            } else if ((value.maxGuests > 8) && (value.maxGuests <= 10)) {
                circleTableSize = Math.round(blockSnapSize * 2.8)
                chairOffset = Math.round(blockSnapSize * 2.6)
                chairSize = Math.round(blockSnapSize / 1.5)
                tableTextWidth = Math.round((blockSnapSize * 2) + (blockSnapSize / 2))
                tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/0.9)
            }

            tableTextXaxis = centerTableXaxis - Math.round(blockSnapSize/2)

        }

        if(value.tableShape == "Square"){
            if((value.maxGuests <= 4) && (value.maxGuests < 5)){
                circleTableSize = Math.round(blockSnapSize * 2) - Math.round(blockSnapSize / 2);
                chairSize = Math.round(blockSnapSize/1.5)
                chairOffset = Math.round(blockSnapSize * 1.3)
                tableTextWidth = Math.round((blockSnapSize*2) + (blockSnapSize/2))
            }

            tableTextXaxis = centerTableXaxis - Math.round(blockSnapSize/2)
            tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/2)
        }

        if(value.tableShape == "Rectangle"){

            if(value.maxGuests <= 4){
                recTableWidth = Math.round(blockSnapSize*4)
                tableTextWidth = Math.round(blockSnapSize*3.5)

                tableTextXaxis = Math.round(centerTableXaxis/2) + Math.round(tableTextSize*1.8)

            } else if((value.maxGuests > 4) && (value.maxGuests <= 6)) {
                recTableWidth = Math.round(blockSnapSize*6)
                tableTextWidth = Math.round(blockSnapSize*5.3)
                recTableChairGapping = Math.round(blockSnapSize/2)

                tableTextXaxis = centerTableXaxis + Math.round(tableTextSize*1.05)

            } else if((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                recTableWidth = Math.round(blockSnapSize*8)
                tableTextWidth = Math.round(blockSnapSize*7.5)
                recTableChairGapping = Math.round(blockSnapSize/2)

                tableTextXaxis = centerTableXaxis + Math.round(tableTextSize*2.35)

            } else if((value.maxGuests > 8) && (value.maxGuests <= 10)) {
                recTableWidth = Math.round(blockSnapSize*10)
                tableTextWidth = Math.round(blockSnapSize*9.5)
                recTableChairGapping = Math.round(blockSnapSize/2)

                tableTextXaxis = centerTableXaxis + Math.round(tableTextSize*3.55)

            }

            chairOffset = Math.round(blockSnapSize * 1.35)
            chairSize = Math.round(blockSnapSize/1.5)
            tableLinkYaxis = centerTableYaxis - Math.round(circleTableSize/2) - Math.round(blockSnapSize/2)

        }

        tableTextYaxis = centerTableYaxis - Math.round(tableTextSize - 18)


        if(value.tableShape == "Round"){

            table = new Konva.Circle({
                id: "tablesId",
                x: centerTableXaxis,
                y: centerTableYaxis,
                radius: circleTableSize,
                fill: tableColor,
                stroke: 'white',
                strokeWidth: 1,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        } else if(value.tableShape == "Square"){

            table = new Konva.Rect({
                id: "tablesId",
                x: centerTableXaxis - Math.round(blockSnapSize * 1.5),
                y: centerTableYaxis - Math.round(blockSnapSize * 1.5),
                width: Math.round(blockSnapSize * 3),
                height: Math.round(blockSnapSize * 3),
                fill: tableColor,
                stroke: 'white',
                strokeWidth: 1,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        } else if(value.tableShape == "Rectangle"){

            table = new Konva.Rect({
                id: "tablesId",
                x: centerTableXaxis - Math.round(blockSnapSize * 1.5),
                y: centerTableYaxis - Math.round(blockSnapSize * 1.5),
                width: recTableWidth,
                height: Math.round(blockSnapSize * 3),
                fill: tableColor,
                stroke: "white",
                strokeWidth: 1,
                name: "fillShape",
                shadowColor: 'black',
                shadowBlur: 3,
                shadowOpacity: 0.3
            });

        }

        bottomTableGroup.add(table)
        bottomGroup.add(bottomTableGroup)

        var topGroup = new Konva.Group();
        topGroup.setListening(false);

        if(getStatus(status,value.tableAvailability) == value.tableAvailability){

            var tableText = new Konva.Text({
                x: centerTableXaxis - Math.round(tableTextSize*1.45), //14,
                y: centerTableYaxis - Math.round(tableTextSize - 20), //14,
                text: value.tableText,
                width: tableTextWidth,
                align: 'center',
                fontSize: tableTextSize,
                fontFamily: 'Calibri',
                fill: 'white'
            });
*/
            /*var tableIcon = new Konva.Text({
                x: centerTableXaxis - Math.round(tableTextSize/2), //14,
                y: centerTableYaxis - Math.round(tableTextSize), //14,
                text: "\uf21c",
                fontSize: tableTextSize,
                fontFamily: 'FontAwesome',
                fill: 'green'
            });*/
/*
            var tableIcon = new Konva.Path({
                x: tableTextXaxis,
                y: tableTextYaxis,
                data: tableIcon,
                fill: tableIconColor,
                scale: {
                    x: 0.02,
                    y: -0.02
                },
                rotation:0
            });

            $.each(value.reservations, function(key,value){
                if(value.table_id.indexOf(',') > -1){

                    var tableLink = new Konva.Path({
                        x: tableTextXaxis,
                        y: tableLinkYaxis,
                        data: "M384 368s30.944 56.432 96-32c0 0 92.4-27.456 144 0s256 232.848 304 304c16.297 24.845 25.994 55.29 25.994 88s-9.697 63.155-26.374 88.617l0.379-0.617c-18.88 24.688-84.4 83.488-128 80-56.689-1.894-107.393-26.042-143.939-63.937l-0.061-0.063c-48.224-49.936-176-160-176-160s-54.704-25.6-48 32 211.504 220.592 240 240 159.152 18.768 192 0 107.2-14.896 160-160c0 0 24.512-148-112-272s-202.96-238.016-272-256-257.36 2.912-256 112zM640 523.088s-30.864-56.16-95.776 31.84c0 0-92.8 32.4-144.224 5.072s-254.912-236.8-302.72-307.568c-16.259-24.711-25.935-55.009-25.935-87.568s9.676-62.857 26.31-88.176l-0.375 0.608c18.832-24.576 84.208-83.088 127.696-79.6 56.529 1.872 107.106 25.897 143.609 63.623l0.055 0.057c48.096 49.696 175.584 159.216 175.584 159.216s54.576 25.488 47.888-31.84-211.008-219.504-239.44-238.816-158.784-18.672-191.552 0-106.912 14.864-159.616 159.2c0 0-24.448 147.2 111.744 270.656s202.496 236.8 271.36 254.736 256.752-2.912 255.392-111.44zM320 928h16c17.673 0 32-14.327 32-32v-128c0-17.673-14.327-32-32-32h-16c-17.673 0-32 14.327-32 32v128c0 17.673 14.327 32 32 32zM140.8 828.112l10.992 11.632c5.846 6.176 14.102 10.019 23.256 10.019 8.517 0 16.257-3.327 21.991-8.753l-0.015 0.014 93.040-87.904c6.176-5.846 10.019-14.102 10.019-23.256 0-8.517-3.327-16.257-8.753-21.991l0.014 0.015-10.992-11.632c-5.846-6.176-14.102-10.019-23.256-10.019-8.517 0-16.257 3.327-21.991 8.753l0.015-0.014-93.104 87.904c-6.156 5.844-9.986 14.087-9.986 23.224 0 8.533 3.34 16.285 8.783 22.022l-0.013-0.014zM58.528 639.536l-0.448 16c-0.008 0.271-0.013 0.589-0.013 0.909 0 17.351 13.81 31.477 31.038 31.986l0.047 0.001 128 3.632c0.271 0.008 0.589 0.013 0.909 0.013 17.351 0 31.477-13.81 31.986-31.038l0.001-0.047 0.448-16c0.008-0.271 0.013-0.589 0.013-0.909 0-17.351-13.81-31.477-31.038-31.986l-0.047-0.001-128-3.632c-0.271-0.008-0.589-0.013-0.909-0.013-17.351 0-31.477 13.81-31.986 31.038l-0.001 0.047zM656 144.32h14.4c17.673 0 32-14.327 32-32v0-128.32c0-17.673-14.327-32-32-32v0h-14.4c-17.673 0-32 14.327-32 32v0 128.32c0 17.673 14.327 32 32 32v0zM862.64 36.032l-10.768-11.648c-5.663-6.179-13.771-10.039-22.78-10.039-8.383 0-15.985 3.342-21.547 8.766l0.006-0.006-91.2 88c-6.064 5.919-9.825 14.174-9.825 23.308 0 8.495 3.253 16.23 8.582 22.028l-0.021-0.023 10.768 11.648c5.663 6.179 13.771 10.039 22.78 10.039 8.383 0 15.985-3.342 21.547-8.766l-0.006 0.006 91.2-88c6.064-5.919 9.825-14.174 9.825-23.308 0-8.495-3.253-16.23-8.582-22.028l0.021 0.023zM927.472 240.896l0.448-16c0.014-0.347 0.021-0.754 0.021-1.162 0-17.087-13.493-31.022-30.405-31.732l-0.064-0.002-125.344-3.632c-0.168-0.003-0.366-0.005-0.564-0.005-17.304 0-31.365 13.88-31.659 31.113v0.028l-0.448 16c-0.015 0.362-0.023 0.786-0.023 1.212 0 17.070 13.467 30.995 30.357 31.73l0.066 0.002 125.392 3.552c0.168 0.003 0.366 0.005 0.565 0.005 17.292 0 31.347-13.862 31.659-31.080v-0.029z",
                        fill: value.tables_link_color, //"#da98ea",
                        scale: {
                            x: 0.02,
                            y: -0.02
                        },
                        rotation:0
                    });

                    topGroup.add(tableText,tableIcon,tableLink)

                } else {
                    topGroup.add(tableText,tableIcon)
                }
            })

        } else {

            var tableText = new Konva.Text({
                x: centerTableXaxis - Math.round(tableTextSize*1.45), //14,
                y: centerTableYaxis - Math.round(tableTextSize/2), //14,
                text: value.tableText,
                width: tableTextWidth,
                align: 'center',
                fontSize: tableTextSize,
                fontFamily: 'Calibri',
                fill: 'white'
            });

            topGroup.add(tableText)
        }

        var server = new Konva.Circle({
            x: centerTableXaxis + blockSnapSize, //Math.round(blockSnapSize * 7) + Math.round(blockSnapSize/5),
            y: centerTableXaxis + blockSnapSize, //Math.round(blockSnapSize * 7) + Math.round(blockSnapSize/5),
            radius: Math.round(blockSnapSize/1.5),
            fill: "#55B2DE",
            stroke: 'white',
            strokeWidth: 2,
        })

        var serverName = new Konva.Text({
            x: centerTableXaxis + Math.round(blockSnapSize/1.3),
            y: centerTableXaxis + Math.round(blockSnapSize/1.3),
            text: "JN",
            fontSize: 11,
            fontFamily: 'Calibri',
            fill: 'white'
        });

        //topGroup.add(server,serverName)



        for (var i = 1; i <= value.maxGuests; i++) {
            if((value.maxGuests <= 4) && (value.tableShape == "Round") || (value.tableShape == "Square")) {

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset;
                    tableYAxis = centerTableYaxis;
                    chairRotation = 90;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset;
                    tableYAxis = centerTableYaxis;
                    chairRotation = 270;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                    chairRotation = 0;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                    chairRotation = 180;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 4) && (value.maxGuests <= 6) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")) {
                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 5;
                    tableYAxis = centerTableYaxis - 15;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 5;
                    tableYAxis = centerTableYaxis  - 15;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 5;
                    tableYAxis = centerTableYaxis + 15;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 5;
                    tableYAxis = centerTableYaxis + 15;
                }


                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 6) && (value.maxGuests <= 8) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")){

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 11;
                    tableYAxis = centerTableYaxis - 28;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 11;
                    tableYAxis = centerTableYaxis  - 28;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 11;
                    tableYAxis = centerTableYaxis + 28;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 11;
                    tableYAxis = centerTableYaxis + 28;
                } else if (i === 7) {
                    tableXAxis = centerTableXaxis + chairOffset;
                    tableYAxis = centerTableYaxis;
                } else if (i === 8) {
                    tableXAxis = centerTableXaxis - chairOffset;
                    tableYAxis = centerTableYaxis;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);

            } else if((value.maxGuests > 8) && (value.maxGuests <= 10) && (value.tableShape != "Square") && (value.tableShape != "Rectangle")){

                if (i === 1) {
                    tableXAxis = centerTableXaxis - chairOffset + 22;
                    tableYAxis = centerTableYaxis - 42;
                } else if (i === 2) {
                    tableXAxis = centerTableXaxis + chairOffset - 22;
                    tableYAxis = centerTableYaxis  - 42;
                } else if (i === 3) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis + chairOffset;
                } else if (i === 4) {
                    tableXAxis = centerTableXaxis;
                    tableYAxis = centerTableYaxis - chairOffset;
                } else if (i === 5) {
                    tableXAxis = centerTableXaxis + chairOffset - 22;
                    tableYAxis = centerTableYaxis + 42;
                } else if (i === 6) {
                    tableXAxis = centerTableXaxis - chairOffset + 22;
                    tableYAxis = centerTableYaxis + 42;
                } else if (i === 7) {
                    tableXAxis = centerTableXaxis - chairOffset + 2;
                    tableYAxis = centerTableYaxis + 16;
                } else if (i === 8) {
                    tableXAxis = centerTableXaxis - chairOffset + 2;
                    tableYAxis = centerTableYaxis - 16;
                } else if (i === 9) {
                    tableXAxis = centerTableXaxis + chairOffset - 2;
                    tableYAxis = centerTableYaxis + 16;
                } else if (i === 10) {
                    tableXAxis = centerTableXaxis + chairOffset - 2;
                    tableYAxis = centerTableYaxis - 16;
                }

                var chair = new Konva.Circle({
                    id: "chairsid",
                    x: tableXAxis,
                    y: tableYAxis,
                    radius: chairSize,
                    fill: chairColor
                });

                bottomChairGroup.add(chair);



            } else if(value.tableShape == "Rectangle"){

                if(value.maxGuests <= 2){

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairOffset - 1;
                        tableYAxis = centerTableYaxis;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis + (chairOffset*1.85);
                        tableYAxis = centerTableYaxis;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 2) && (value.maxGuests <= 4)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairOffset + 18;
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairOffset + (chairOffset*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairOffset + 18;
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairOffset + (chairOffset*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 4) && (value.maxGuests <= 6)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.6);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.2);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.6);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.2);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 6) && (value.maxGuests <= 8)) {
                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.7);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.4);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.7);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 7) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.4);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 8) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                } else if((value.maxGuests > 8) && (value.maxGuests <= 10)) {

                    if (i === 1) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 2) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.9);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 3) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.6);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 4) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.4);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 5) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*17.1);
                        tableYAxis = centerTableYaxis - chairOffset;
                    } else if (i === 6) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*2.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 7) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*5.9);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 8) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*9.6);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 9) {
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*13.4);
                        tableYAxis = centerTableYaxis + chairOffset;
                    } else if (i === 10){
                        tableXAxis = centerTableXaxis - chairWidth + (recTableChairGapping*17.1);
                        tableYAxis = centerTableYaxis + chairOffset;
                    }

                    var chair = new Konva.Circle({
                        id: "chairsid",
                        x: tableXAxis,
                        y: tableYAxis,
                        radius: chairSize,
                        fill: chairColor
                    });

                    bottomChairGroup.add(chair);

                }
            }

            bottomChairGroup.setListening(false);
            bottomGroup.add(bottomChairGroup);
            bottomChairGroup.moveDown();
            bottomTableGroup.moveUp();

            layer.draw();

        }

        keyGroup.on('mouseenter', function () {

            if(($("#book-a-table.fade").hasClass('getAvailability')) && (value.tableAvailability != "Available")) {
                bottomGroup.setListening(false);
                layer.draw();
                stage.container().style.cursor = 'default';
            } else {
                stage.container().style.cursor = 'pointer';
            }

        });

        keyGroup.on('mouseleave', function () {
            stage.container().style.cursor = 'default';
        });

        keyGroup.on('click touchstart', function(e){
             selectMultipleTables(e.target.parent.parent.parent.attrs.id, e.target)
        })

        keyGroup.add(bottomGroup,topGroup);

        //Add Shape 1 to Layer 1
        layer.add(keyGroup);

        //Add Layer 1 to Stage
        stage.add(layer);

        tableText.moveToTop();
        layer.draw();
    });

}

window.clearTableSelection = function () {
    //Find all shapes
    var allShape = stage.find('Shape')

    //Find all tables from shapes array
    var selectedShape = $.grep(allShape, function(e) {
        return e.attrs.id === 'tablesId';
    });

    $.each(selectedShape, function(key,value){
        value.stroke("white")
    })

    var selectedChairsGroup = $.grep(allShape, function(e) {
        if(e.attrs.id != "undefined"){
            return e.attrs.id === 'chairsid';
        }
    });

    $.each(selectedChairsGroup, function(key,value){
        value.fill(chairColor)
    })


    $("#book-a-table, #reservation_info_popup").on("hidden.bs.modal", function () {

        $.each(selectedShape, function(key,value){
            value.stroke("white")
        })

        $.each(selectedChairsGroup, function(key,value){
            value.fill(chairColor)
        })

        enableDoubleClick = false

        layer.draw();
    });

    if(enableDoubleClick == true){
        $.each(selectedShape, function(key,value){
            value.stroke("white")
        })

        $.each(selectedChairsGroup, function(key,value){
            value.fill(chairColor)
        })

        enableDoubleClick = false

        layer.draw();
    }
}

if(isEmpty(floor_and_tables) != true){
    floorView(floor_and_tables);
}

*/