$(document).ready(function() {
  $(".btn-number").on("click", function(e) {
    e.preventDefault();
    var fieldName = $(this).attr("data-field");
    var type = $(this).attr("data-type");
    var input = $("input[name='" + fieldName + "']");
    var inc_val = parseInt(input.attr('data-inc-val'));
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
      if (type == "minus") {
        if (currentVal > input.attr("min")) {
          if (
            fieldName == "party_size" ||
            fieldName == "min" ||
            fieldName == "max"
          ) {
            input.val(currentVal - 1).change();
          } else {
            input.val(currentVal - inc_val).change();
          }
        }
        if (parseInt(input.val()) == input.attr("min")) {
          // $(this).attr("disabled", true);
        }
      } else if (type == "plus") {
        if (currentVal < input.attr("max")) {
          if (
            fieldName == "party_size" ||
            fieldName == "min" ||
            fieldName === "max"
          ) {
            input.val(currentVal + 1).change();
          } else {
            input.val(currentVal + inc_val).change();
          }
        }
        if (parseInt(input.val()) == input.attr("max")) {
          // $(this).attr("disabled", true);
        }
      }
    } else {
      input.val(0);
    }
  });
  $(".turnovertime_input").focusin(function() {
    $(this).data("oldValue", $(this).val());
  });
  $(".turnovertime_input").change(function() {
    var minValue = parseInt($(this).attr("min"));
    var maxValue = parseInt($(this).attr("max"));
    var valueCurrent = parseInt($(this).val());
    var name = $(this).attr("name");
    console.log(valueCurrent);
    console.log(minValue);

    if (valueCurrent >= minValue) {
      $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr(
        "disabled"
      );
    } else {
      //alert("Sorry, the minimum value was reached");
      $(this).val($(this).data("oldValue"));
    }
    if (valueCurrent <= maxValue) {
      $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr(
        "disabled"
      );
    } else {
      // alert("Sorry, the maximum value was reached");
      $(this).val($(this).data("oldValue"));
    }
  });
  $(".input-number").keydown(function(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if (
      $.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  });
if($(window).width() > 1024){
  table_type_width();
}
});


function table_type_width() {
  var table_width = $("#tat_form").find(".turnOver__row");
  var table_submit = $("#tat_form").find(".turnOver__submitBtn");
  var tot_width = table_width.width();
  table_submit.css("width", tot_width);
}
