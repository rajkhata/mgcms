$(function() {

    // commenting below line to fix bug - NTRS-1022
   //getGuestList(1);
   //  $("#addtag").addAttr('disabled');
    setTimeout(function () {
        $("#addtag").attr('disabled', 'disabled');
    }, 500)

    $("#tag_form, #item_form").submit(function(e){
        e.preventDefault();
    });
    $('.guestbook__details-edit .edit_guest_details').on('click', function () {
        $("#user_guestname").hide();
        if(!$(".guestbook__user-info").hasClass("edit__mode")){
            $(".guestbook__user-info").addClass("edit__mode");
            $(".guestName input").show();
            $(".guestName span").hide();
        }

        $('.guestbook__details-edit .update_guest_details-first').removeClass('edit-input');
        $('.guestbook__details-edit .cancel_edit_guest_details').removeClass('edit-input');
        $('.guestbook__details-edit .edit_guest_details').addClass('edit-input');
        $(".guestbook__user-info input, .guestbook__user-info select").removeAttr('disabled');
        $(".guestbook__user-info .selct-picker-plain").removeClass('pointer-none');
        $("#fl_name, #category", "#addtag").removeAttr('disabled');//
        $(".guestbook_userDetails-edit .edit_guest_details").trigger('click');
    });

   $('.cancel_edit_guest_details-first').on('click', function () {

        $("label.error").hide();
        $(".error").removeClass("error");
        if($(".guestbook__user-info").hasClass("edit__mode")){
            $(".guestbook__user-info").removeClass("edit__mode");
            $(".guestName input").hide();
            $(".guestName span").show();
        }

        $('.guestbook__details-edit .update_guest_details-first').addClass('edit-input');
        $('.guestbook__details-edit .cancel_edit_guest_details-first').addClass('edit-input');
        $('.guestbook__details-edit .edit_guest_details').removeClass('edit-input');
        $(".guestbook__user-info input, .guestbook__user-info select").attr('disabled', 'disabled');
        $(".guestbook__user-info .selct-picker-plain").addClass('pointer-none');
        $(".guestbook_userDetails-edit .cancel_edit_guest_details-second").trigger('click');
        $.ajax({
            type: 'GET',
            url: '/guestbook/guestdetail/'+$("#cust_id").val(),
            success: function (data) {
             var   userval=    $("#userval").val();
                $("#user_guestname").show();
     

                $('#fl_name').val(data.fname+' '+data.lname);
                $("#category").val(data.category).change();
                $("#addtag").val(data.category).change();
                $('#birthday').val(data.dob);
                $('#anniversary').val(data.doa);
            },
            error: function (data, textStatus, errorThrown) {

            }
        });
    });

    $('.guestbook_userDetails-edit .edit_guest_details').on('click', function () {
        if(!$(".guestbook_userDetails").hasClass("edit__mode")){
            $(".guestbook_userDetails").addClass("edit__mode");
        }
        $('.guestbook_userDetails-edit .update_guest_details-second').removeClass('edit-input');
        $('.guestbook_userDetails-edit .cancel_edit_guest_details-second').removeClass('edit-input');
        $('.guestbook_userDetails-edit .edit_guest_details').addClass('edit-input');
        $(".guestbook_userDetails input, .guestbook_userDetails textarea").removeAttr('disabled');
        $(".guestbook_userDetails .user__info").tooltip('disable');
        //$(".guestbook_userDetails #email").removeClass("text-ellipsis");
    });

    $('.guestbook_userDetails-edit .cancel_edit_guest_details-second').on('click', function () {
        $("label.error").hide();
        $(".error").removeClass("error");
        //$(".guestbook_userDetails #email").addClass("text-ellipsis");
        $(".guestbook_userDetails .user__info").tooltip('enable');
        if($(".guestbook_userDetails").hasClass("edit__mode")){
            $(".guestbook_userDetails").removeClass("edit__mode");
        }
        
        $('.guestbook_userDetails-edit .update_guest_details-second').addClass('edit-input');
        $('.guestbook_userDetails-edit .cancel_edit_guest_details-second').addClass('edit-input');
        $('.guestbook_userDetails-edit .edit_guest_details').removeClass('edit-input');
        $(".guestbook_userDetails input, .guestbook_userDetails textarea").attr('disabled', 'disabled');
        $.ajax({
            type: 'GET',
            url: '/guestbook/guestdetail/'+$("#cust_id").val(),
            success: function (data) {

                $('#email').val(data.email);
                $('#mobile').val(data.mobile);
                $('#address').val(data.shipping_address);

            },
            error: function (data, textStatus, errorThrown) {

            }
        });
    });

    $('.detail_body-edit .edit_guest_details').on('click', function () {
        if(!$(".guest-note").hasClass("edit__mode")){
            $(".guest-note").addClass("edit__mode");
        }

        $('.detail_body-edit .update_guest_details-third').removeClass('edit-input');
        $('.detail_body-edit .cancel_edit_guest_details-third').removeClass('edit-input');
        $('.detail_body-edit .edit_guest_details').addClass('edit-input');
        $("#description").removeAttr('disabled') //.focus();

        var searchInput = $('#description');

        // Multiply by 2 to ensure the cursor always ends up at the end;
        // Opera sometimes sees a carriage return as 2 characters.
        var strLength = searchInput.val().length * 2;

        searchInput.focus();
        searchInput[0].setSelectionRange(strLength, strLength);

    });
    $('.detail_body-edit .cancel_edit_guest_details-third').on('click', function () {

        if($(".guest-note").hasClass("edit__mode")){
            $(".guest-note").removeClass("edit__mode");
        }

        $('.detail_body-edit .update_guest_details-third').addClass('edit-input');
        $('.detail_body-edit .cancel_edit_guest_details-third').addClass('edit-input');
        $('.detail_body-edit .edit_guest_details').removeClass('edit-input');
        $("#description").attr('disabled', 'disabled');
        $.ajax({
            type: 'GET',
            url: '/guestbook/guestdetail/'+$("#cust_id").val(),
            success: function (data) {
                $('#description').val(data.description);
            },
            error: function (data, textStatus, errorThrown) {

            }
        });
    });

    $('.add_tag').on('click', function () {
        $("#guest_tags_div").removeClass('edit-input');
        $(".tagit-choice-editable").remove();
        $("#guest_tags input").focus()
    });
    
    $('.close_tag_button').on('click', function () {
        $('#guest_tags_div').addClass('edit-input');
    });
    $('.add_tag_button').on('click', function () {
        var data = $("#tag_form").serialize();
        var tags = $("#guest_tags").tagit("assignedTags");      
       if(tags.length){

        var url = $("#tag_form").attr('action');
        var tagHtml='';
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                $('#guest_tags_div').addClass('edit-input');
                $.each(data.data, function (key, value) {
                    tagHtml+='<div class="detail__tag"><span class="tag_name">'+value+'</span><span class="text-icon"> ×</span></div>';
                });
                if($('#guest_detail_tag .tags_div').length) {
                    $('.tags_div').empty().html(tagHtml);
                } else {
                    $('#guest_detail_tag').empty().html('<div class="tags_div">'+tagHtml+'</div>');
                }
            },
            error: function (data, textStatus, errorThrown) {
                $.each(data.responseJSON.errors, function(key, value){
                    $('.alert-danger').show();
                    $('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        });

       }
       
    });

    $('.update_guest_details-first').on('click', function () {

        if ( $("#basicInfoForm_first").valid() ) {
            if($(".guestbook__user-info").hasClass("edit__mode")){
                $(".guestbook__user-info").removeClass("edit__mode");
                $(".guestName input").hide();
                $(".guestName span").show();
            }


            var data = 'fl_name='+$('#fl_name').val()+'&mobile='+$('#mobile').val()+'&tag='+tags_val+'&birthday='+$('#birthday').val()+'&anniversary='+$("#anniversary").val()+'&id='+$('#cust_id').val()+'&_token='+$('#_token').val();
            var url = $(".guest_book_info_basic").attr('action');

            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                success: function (data) {
                 //   alert(data);
                    $('.activeUser').html($('#fl_name').val());
                    $('.guestName span').text($('#fl_name').val());
                    $("#user_guestname").show();


                    // $('.activeUser').html($('.guestName span').val());
                    $('.guestbook__details-edit .update_guest_details-first').addClass('edit-input');
                    $('.guestbook__details-edit .cancel_edit_guest_details').addClass('edit-input');
                    $('.guestbook__details-edit .edit_guest_details').removeClass('edit-input');
                    $(".guestbook__user-info input").attr('disabled', 'disabled');
                    $(".guestbook__user-info select").attr('disabled', 'disabled');
                },
                error: function (data, textStatus, errorThrown) {
                    $.each(data.responseJSON.errors, function(key, value){
                        $('.alert-danger').show();
                        $('.alert-danger').append('<p>'+value+'</p>');
                    });
                }
            });
        }

        $(".update_guest_details-second").trigger("click")

    });

    $('.update_guest_details-second').on('click', function () {
        if ( $("#basicInfoForm").valid() ) {

            if ($(".guestbook_userDetails").hasClass("edit__mode")) {
                $(".guestbook_userDetails").removeClass("edit__mode");
            }
            var data = $(".guest_book_info_basic").serialize();
            var url = $(".guest_book_info_basic").attr('action');
            var cust_id = $('#cust_id').val();
            var token = $('#_token').val();
            $.ajax({
                type: 'POST',
                url: url,
                data: data + '&id=' + cust_id + '&_token=' + token,
                success: function (data) {
                    $('.guestbook_userDetails-edit .update_guest_details-second').addClass('edit-input');
                    $('.guestbook_userDetails-edit .cancel_edit_guest_details').addClass('edit-input');
                    $('.guestbook_userDetails-edit .edit_guest_details').removeClass('edit-input');
                    $(".guestbook_userDetails input, .guestbook_userDetails textarea").attr('disabled', 'disabled');
                    $(".guestbook_userDetails #email").addClass("text-ellipsis");
                    $(".guestbook_userDetails .user__info").tooltip('enable');
                },
                error: function (data, textStatus, errorThrown) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.alert-danger').show();
                        $('.alert-danger').append('<p>' + value + '</p>');
                    });
                }
            });
        }
    });


    $('div.guestbookDetails__body .update_guest_details-third').on('click', function () {
        if($(".guest-note").hasClass("edit__mode")){
            $(".guest-note").removeClass("edit__mode");
        }
        var data = 'description=' + $('#description').val() + '&id=' + $('#cust_id').val() + '&_token=' + $('#_token').val();
        var url = $(".guest_book_info_basic").attr('action');
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                $('.detail_body-edit .update_guest_details-third').addClass('edit-input');
                $('.detail_body-edit .cancel_edit_guest_details').addClass('edit-input');
                $('.detail_body-edit .edit_guest_details').removeClass('edit-input');
                $("#description").attr('disabled', 'disabled');
            },
            error: function (data, textStatus, errorThrown) {
                $.each(data.responseJSON.errors, function (key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<p>' + value + '</p>');
                });
            }
        });
    });


    $('.btn__add').on('click', function () {
        $("#guest_items_div").removeClass('edit-input');
        $('.tagit-choice-editable').remove();
        $("#guest_items input").focus()
    });

    $('.close_item_button').on('click', function () {
         $('#guest_items_div').addClass('edit-input');
    });   
    
    $('.add_item_button').on('click', function () {

        //var tagval=$( "#item_form input:text" ).val();
        var tagval=$('#guest_items').tagit("assignedTags").length;
        //console.log(tagval);
        if(tagval==0){
            $( "#item_form input:text" ).attr('required',true);
           // $("#item_form").valid();
            return false;
        }
        var data = $("#item_form").serialize();
        var url = $("#item_form").attr('action');
        var itemHtml='';
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                $('#guest_items_div').addClass('edit-input');
                $.each(data.data, function (key, value) {
                    itemHtml+='<li><span class="item_name">'+value+'</span><span class="text-icon-item"> ×</span></li>';
                });
                $('.item_ul').empty().html(itemHtml);
            },
            error: function (data, textStatus, errorThrown) {
                $.each(data.responseJSON.errors, function(key, value){
                    $('.alert-danger').show();
                    $('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        });
    });
    $( "#birthday").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false,
        dateFormat: 'dd-mm-yy',
        yearRange: '-80y:c+nn',
        maxDate: '-1d',
        onSelect: function() {
             $(this).valid();
            $(this).change();
        }
    });

    $('#anniversary').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false,
        dateFormat: 'dd-mm-yy',
        yearRange: '-80y:c+nn',
        maxDate: '-1d',
        onSelect: function() {
             $(this).valid();
            $(this).change();
        }
    });

    $(document).on('click', '.text-icon', function () {
        var cust_id = $('#cust_id').val();
        var tag = $(this).prev('.tag_name').html().trim();
        var token = $('#_token').val();
        var inputData = 'tag='+encodeURIComponent(tag)+'&_token=' + token;
        var tagHtml='';
        $.ajax({
            type: 'DELETE',
            url: '/guestbook/delete-tag/'+cust_id,
            data: inputData,
            success: function (data) {
                $.each(data.data, function (key, value) {
                    tagHtml+='<div class="detail__tag"><span class="tag_name">'+value+'</span><span class="text-icon"> ×</span></div>';
                });
                $('.tags_div').empty().html(tagHtml);
            },
            error: function (data, textStatus, errorThrown) {
                $.each(data.responseJSON.errors, function(key, value){
                    $('.alert-danger').show();
                    $('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        });
    });

    $(document).on('click', '.text-icon-item', function () {
        
        var cust_id = $('#cust_id').val();
        var token = $('#_token').val();
        var item = $(this).prev('.item_name').html().trim();
        var inputData = 'item='+encodeURIComponent(item)+'&_token=' + token;
        var itemHtml='';
        $.ajax({
            type: 'DELETE',
            url: '/guestbook/delete-item/'+cust_id,
            data: inputData,
            success: function (data) {
                $.each(data.data, function (key, value) {
                    itemHtml+='<li><span class="item_name">'+value+'</span><span class="text-icon-item"> ×</span></li>';
                });
                $('.item_ul').empty().html(itemHtml);
            },
            error: function (data, textStatus, errorThrown) {
                $.each(data.responseJSON.errors, function(key, value){
                    $('.alert-danger').show();
                    $('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        });
    });

    $('#guestbook_activity_year').on('change', function () {
        var year = $('#guestbook_activity_year').find(":selected").text();
        var start_date = '01-01-'+year.trim();
        var end_date= '31-12-'+year.trim();
        var cust_id = $('#cust_id').val();
        $.ajax({
            //url: '/reservation/all?page=1' + '&start_date=' + start_date + '&end_date=' + end_date+'&user_id='+cust_id,
            url: '/guestbook/user/'+ cust_id +'/listing/'+year,
            type: "get",
            datatype: "html",
            // beforeSend: function()
            // {
            //     you can show your loader
            // }
        }).done(function(data)
        {
            $(".guestbook__activities-activity").empty().html(data);
        })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                alert('No response from server');
            });
    });
    $( "#basicInfoForm" ).validate({
        rules: {
            /*mobile: {
                required: true,
                phoneUS: true //or look at the additional-methods.js to see available phone validations
            },*/
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            /*mobile: {
                required: "Please enter the customer's phone number.",
                phoneUS:"Please enter a valid 10-digit phone number."
            },*/
            email: {
                required: "Please enter the customer's email address.",
                email: "Please enter a valid email address (e.g. yourname@example.com)."
            },
        }
    });
    $( "#basicInfoForm_first" ).validate({
        rules: {
            fl_name: {
                required: true,
            },
            category: {
                required: true,
            }
            /*,
            birthday: {
                required: true,
            }
            ,
            anniversary: {
                required: false,
            }*/
        },
        messages:{
            fl_name:"Please enter the customer's name."
        }
    });
    $('#search #keyword').on('change keyup paste',function(){
        //if($(this).val().trim()){
            getGuestList(1);
        //}
    });

    /*$('#search').on('click',function(){
        alert($('#keyword').val().trim());
        alert('hello');
        getGuestList(1);
    });*/

    $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                getGuestList(page);
            }
        }
    });

    $(document).ready(function()
    {
        $(document).on('click', '.pagination a',function(event)
        {
            $('li').removeClass('active');
            $(this).parent('li').addClass('active');
            event.preventDefault();
            var myurl = $(this).attr('href');
            var page=$(this).attr('href').split('page=')[1];
            getGuestList(page);
        });
    });

    function getGuestList(page){
        var keyword = $('#keyword').val() ? $('#keyword').val().trim() : '';
        keyword = keyword.toLowerCase();

        var inputData = $('#guestfilters').serialize();

        $.ajax(
            {
                 url: '?page=' + page + '&keyword=' + keyword,
                 type: 'get',
                 data: inputData,
                 datatype: "html",
                // beforeSend: function()
                // {
                //     you can show your loader
                // }
            })
            .done(function(data)
            {
                $(".guest_listing").empty().html(data);
                location.hash = page;

                setTimeout(function(){
                    $('body,html').animate({
                        scrollTop: 0
                    }, 600);
                },1000)

            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                //alert('No Guest found');
            });
    }


    $("#guest_book_info_basic").on('change keyup keydown paste cut', 'textarea#address', function () {
        if ($(this).val().length < 35) {
            $(this).attr("rows", "1")
        } else if ($(this).val().length > 35 && $(this).val().length < 110) {
            $(this).attr("rows", "2")
        }
    }).find('textarea').change();
});
