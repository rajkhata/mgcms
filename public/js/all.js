$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#guest_remove_all_filters').on('click', function() {
    console.log('guest remove all filters');
    $('#filterModal .guest_filter_checkbox').each(function() {
        $(this).prop('checked', false);
    });
    $('#filterModal #Week').prop('checked', true);
    $('#filterModal #Month').prop('checked', false);
    $('#guest_tags_filter').tagit('removeAll');
    $("#filterModal #guest_filter_sort").val('').selectpicker('refresh');
    reset_sliders();
    //$('#guestfilters').trigger('submit');
    //$('#search input#keyword').val('');
});
$("#guestfilters").submit(function(e){
     e.preventDefault();
     var inputData = $(this).serialize();
    $.ajax({
        type: 'post',
        data: inputData,
        success: function (data) {
         $(".guest_listing").empty().html(data);
         
            
        },
        error: function (data, textStatus, errorThrown) {

            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){

                setTimeout(function() {
                   // $('#add-tabletype-modal').modal('hide');
                   // location.reload();
                }, 2000);

            });
        },
        complete: function(data) {
            $('#filterModal').modal('hide');
        }
    });
    });
 $("#reservation_settings .cuttoff_type").change(function(e){
   var type=$(this).val();

   if(type=='Hour'){
     $('.cut_off_time_hour').removeClass('hide');
     $('.cut_off_time_day').addClass('hide');
   }else{

    $('.cut_off_time_hour').addClass('hide');
     $('.cut_off_time_day').removeClass('hide');
   }
  
 }).is(function(){
     var type=$(this).val();
     if(type=='Hour'){
         $('.cut_off_time_hour').removeClass('hide');
         $('.cut_off_time_day').addClass('hide');
     }else{
         $('.cut_off_time_hour').addClass('hide');
         $('.cut_off_time_day').removeClass('hide');
     }
 });

$("#reservation_settings .allow_guest_advance_online_reservation_type").change(function(e){
   var type=$(this).val();
   
   if(type=='Days'){
     $('.allow_guest_advance_online_reservation_months').addClass('hide');
     $('.allow_guest_advance_online_reservation_days').removeClass('hide');
   }else{
     $('.allow_guest_advance_online_reservation_months').removeClass('hide');
     $('.allow_guest_advance_online_reservation_days').addClass('hide');
   }
  
 });
 $(".size-inc-btn-cmn").click(function(e){
        e.preventDefault();
        var $this = $(this);
        var $input = $this.prev("input");
        var maxValue = $this.prev("input").attr('data-max');
        var value = parseInt($input.val());

        if (value < maxValue) {
            value = value + 1;
        } else {
            value;
        }


        $input.val(value);
    });

    $(".size-dec-btn-cmn").click(function(e){
        e.preventDefault();
        var $this = $(this);
        var $input = $this.next("input");
        var minValue = $this.next("input").attr('data-min');
        var value = parseInt($input.val());

        if (value > minValue) {
            value = value - 1;
        } else {
            value;
        }


        $input.val(value);
});
/*
Reservation add/edit/view js code start
*/

function openCancellationPopUp(reservation_id, reservation_status_new, target) {
    var inputData = 'reservation_status_new='+reservation_status_new+'&target='+target;
    $.ajax({
        type: 'POST',
        url: '/reservation/cancellation/charge/'+reservation_id,
        data: inputData,
        success: function (data) {
            if(data.data!=="" && parseInt(data.data.cancellation_charge)>0){
                var warning_html;
                warning_html = 'Guest will be charged a cancellation fee of $'+data.data.cancellation_charge+' ';
                if(data.data.cancellation_charge_type=='per_cover'){
                    warning_html+=data.data.cancellation_charge_type.replace('_', ' ');
                }
                warning_html+='. Are you sure you want to cancel this reservation?';
                $('.warning_text').html(warning_html);
            }
        },
        error: function (data, textStatus, errorThrown) {

            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){
            });
        },
        complete: function(data) {
            $('#cancel-reservation').modal('show');
            $('#manageWaitlistModal').modal('hide');
            $("input[name=reservation_status_new]").val(reservation_status_new);
            $("input[name=reservation_id]").val(reservation_id);
            if(target!=undefined){
                $("input[name=target]").val(target);
            }
            $("#cancellation_reason_other").val('');
        }
    });
}

$('.cancel_reservation').on('click', function () {
    var cancellation_reason = $("input[name=cancellation_reason]:checked").val();
    if(cancellation_reason=='other'){
        cancellation_reason = $("#cancellation_reason_other").val();
    }
    changeReservationStatus($("input[name=reservation_id]").val(), $("input[name=reservation_status_new]").val(), $("input[name=target]").val(), null, cancellation_reason);
});

$('.edit_reservation').on('click', function () {
    makeBookingInfoEditable();
    $('.edit_reservation').hide();
    $('.update_reservation').show();
    $('.cancel_edit_reservation').show();
});

function openReservationDetailPopUp(resv_id) {
    $("#reservation-edit input").prop("readonly", true).prop("disabled", true); 
    $('#reservation-details').modal('show');
    openSibebarController(true)
    clearNewBookSidebar();
    getAndSetReservationDetail(resv_id);
    makeBookingInfoUnEditable();
}


$(function () {
    $(".archive__page_nav #start_date").datepicker({
        showAnim: "slideDown",
        dateFormat: 'mm-dd-yy',
        maxDate: 0,
        onSelect: function () {
            $(this).trigger('change');
            $(this).addClass('addDate');
            getReservations(1);
        }
    }).attr('readonly', 'readonly');
    $("#reservation_date").datepicker({
        showAnim: "slideDown",
        dateFormat: 'mm-dd-yy',
        minDate: 0,
        onSelect: function () {
            $(this).valid();
            $(this).trigger('change');
            $(this).addClass('addDate');
            setTimeout(function(){
                $('#ui-datepicker-div').removeClass('reservation_date');
            },400)
        },
        beforeShow: function(input, inst) {
            $('#ui-datepicker-div').addClass(this.id);
        }
            
    }).attr('readonly', 'readonly')

    $('.archive__page_nav #end_date').datepicker({
        showAnim: "slideDown",
        beforeShow: function () {
            $(this).datepicker('option', {
                minDate: $('#start_date').val(),
                maxDate: 0,
                dateFormat: 'mm-dd-yy',
                onSelect: function (dateText, inst) {
                    $(this).trigger('change');
                    $(".archive__page_nav #start_date").datepicker('option', 'maxDate', dateText);
                    getReservations(1);
                    $(this).addClass('addDate');
                }
            });
        }
    }).attr('readonly', 'readonly');

    $('input.timepicker').on('click', function () {
        var tym_pick_id = $(this).attr('id');
        $('.ui-timepicker-wrapper').addClass(tym_pick_id);
        //$(this).valid();
    });

    $("#edit_start_date").datepicker({
        showAnim: "slideDown",
        minDate: 0,
        dateFormat: 'mm-dd-yy'
    }).attr('readonly', 'readonly')

    if($('#book-a-table').length){
        $("#book-a-table").on("show.bs.modal", function () {
            /*console.log($("#reservation_date").datepicker('getDate'))
           var newYork= moment.utc().format('HH:mm');
           var timeRangesDisable = [['00:00', newYork]]*/
            openSibebarController(true)
            $("#blockTables,#newWalkinModal,#newWaitlistModal,#reservation_info_popup").modal('hide');
            $("#bookATable").attr("data-popup",true)
            $("#block_a_table,#new_walkin_button").removeAttr("data-popup")

            $('#book-a-table .timepicker').timepicker({
                interval: 15,
                step: '15',
                minTime: '00:00',
                maxTime: '23:30',
                timeFormat: 'g:i A',
                closeOnWindowScroll: true,
                //disableTimeRanges: timeRangesDisable
            }).on('click', function (e) {
                $(".ui-timepicker-wrapper").mCustomScrollbar({
                    theme: "dark"
                });
            });
        })
    }

    if($('#reservationdeatails-timepicker').length) {
        $('#reservationdeatails-timepicker').timepicker({
            interval: 15,
            step: '15',
            minTime: '00:00',
            maxTime: '23:30',
            timeFormat: "g:i A",
            closeOnWindowScroll: true
        }).on('click', function (e) {
            $(".ui-timepicker-wrapper").mCustomScrollbar({
                theme: "dark"
            });
        });
    }
});


function getAndSetReservationDetail(resv_id) {
    $('.cancellation_msg p:first').html('');
    $('.cancellation_msg p:last').html('');
    $('.icon_div').hide();
    $('.message_div').empty().hide();

    removeValidation("#reservation-edit")

    var tagsDiv = '';
    $.ajax({
        url: '/reservation/detail/'+resv_id,
        dataType: 'json',
        cache: false,
        success: function (data) {
            //console.log(data.reservation_detail.user);
            if(data.reservation_detail.user === null) {
                $('#reservation-details #register_guest').prop('checked', false);
                $('#reservation-details .add_view1').hide();
                $('#reservation-details #user_id').val('');
            }else{
                fetchUserDataAndActivityInReservationDetail(data.reservation_detail.user.id, 'detail');
            }
            $('#reservation-details #guest_tags_booking').tagit({
                itemName: 'item',
                fieldName: 'tags[]',
                //autocomplete: {delay: 0, minLength: 2},
                allowDuplicates:false,
                availableTags :data.tags,
                placeholderText : "Please select the existing tags",
                beforeTagAdded: function(event, ui) {
                    $("#reservation-details .tagit-new #guest_tags_booking").find('.ui-autocomplete-input').val('');
                    if($.inArray(ui.tagLabel, data.tags)==-1) return false;
                }
            });

            //Reset Tags
            $('#reservation-details #guest_tags_booking').tagit("removeAll");
            $('#reservation-details #resv_id_title').html(' '+data.reservation_detail.receipt_no);

            $("#reservation-edit").removeClass('walkin');

            if(data.reservation_detail.fname===null && data.reservation_detail.lname===null){
                $('#reservation-details #fname').val('');
                $('#reservation-details #lname').val('');
                $("#reservation-edit").addClass('walkin');
            }else{
                $('#reservation-details #fname').val(data.reservation_detail.fname).next('span').addClass("hasVal");
                $('#reservation-details #lname').val(data.reservation_detail.lname).next('span').addClass("hasVal");
            }

            if(data.reservation_detail.email){
                $('#reservation-details #email').val(data.reservation_detail.email).next('span').addClass("hasVal");
            }

            if(data.reservation_detail.mobile){
                $('#reservation-details #mobile').val(data.reservation_detail.mobile).next('span').addClass("hasVal");
            }

            if($("#reservation-edit").hasClass('walkin')){
                $("#reservation-details span.hasVal").each(function(key, value) {
                    $(this).removeClass('hasVal');
                });
            }

            $('#reservation-details #reservationdeatails-datepicker').val(data.reservation_detail.reservation_start_date);
            $('#reservation-details #reservationdeatails-timepicker').val(data.reservation_detail.reservation_start_time);
            $('#reservation-details #party_size').val(data.reservation_detail.reserved_seat);

            if(typeof data.reservation_detail.table.floor !== 'undefined') {
                floor_name = data.reservation_detail.table.floor.name;
                floor_id = data.reservation_detail.table.floor.id;
            } else {
                floor_name = data.reservation_detail.floor_name;
                floor_id = data.reservation_detail.floor_id;
            }
            if(typeof data.reservation_detail.table.name !== 'undefined') {
                table_name = data.reservation_detail.table.name;
                table_id = data.reservation_detail.table.id;
            } else {
                // multiple ids
                table_name = data.reservation_detail.table_name;
                table_id = data.reservation_detail.table_id;
            }
            var status_dropdown = '<select id="status_current" name="status" class="sidbar_status dropdown-menu dropdown__text_ul selectpicker show-tick" data-style="status-'+data.reservation_detail.status.slug+' dropdown-position-rel">';
            status_dropdown += '<option value="" selected="selected" style="background-color:'+data.reservation_detail.status_color.sidebar+'" data-icon="'+data.reservation_detail.status_icon.sidebar+'">'+data.reservation_detail.status_name+'</option>';
            $.each(data.reservation_detail.next_status, function (status_key, status_val) {
                status_dropdown+='<option value="'+status_val.id+'" style="background-color: '+status_val.color+'" data-icon="'+status_val.icon+'" >'+status_val.name+'</option>';
            });
            status_dropdown +='</select>';
            $('.status-dropdown').html(status_dropdown);
            $(".selectpicker").selectpicker('refresh');
            $('#reservation-details #reservation_tags_div').html(tagsDiv);
            var special_occasion = data.reservation_detail.special_occasion;
            if(special_occasion !== null) {
                $("#reservation-details #special_occasion option").each(function(){
                    if ($(this).text() == special_occasion)
                        $(this).attr("selected","selected");
                });
                $(".selectpicker").selectpicker('refresh');
            }
            $('#reservation-details #special_instruction').val(data.reservation_detail.user_instruction);
            $('#reservation-details #host').val(data.reservation_detail.host);
            $('#reservation-details #reservation_id').val(data.reservation_detail.id);
            if(data.reservation_detail.description) {
                $('#reservation-details #guest_note').val(data.reservation_detail.description).next('span').addClass("hasVal");
            }
            if(data.reservation_detail.can_change_status){
                $('#reservation-details #status_current').attr('disabled', false);
            }else{
                $('#reservation-details #status_current').attr('disabled', true);
            }
            showHideChangeDateInDetail('show');

            $('#reservation-details #host option:nth-child(2)').attr('selected','selected');

            var floorHtml = '<option value="">Select Floor</option>';
            var tableHtml = '<option value="">Select Tables</option>';
            $('#reservation-details #floors').html(floorHtml+'<option value="'+floor_id+'">'+floor_name+'</option>').selectpicker('val', floor_id);
            if(table_id.toString().indexOf(',')>-1) {
                table_id = table_id.split(',');
                table_name = table_name.split('+');
                var i = 0;
                $.each(table_id, function(table_key, table_value_id) {
                    tableHtml += '<option value="'+table_value_id+'">'+table_name[i]+'</option>';
                    i++;
                });
            } else {
                tableHtml += '<option value="'+table_id+'">'+table_name+'</option>';
            }
            $('#reservation-details #tables').empty().append(tableHtml);
            $('#reservation-details #tables').selectpicker('val', table_id)
            $('#reservation-details .payment_status').html(data.reservation_detail.payment_status);
            $(".selectpicker").selectpicker('refresh');
            $('#reservation-details #can_modify_reservation').val(data.reservation_detail.can_modify_reservation);
            if(data.reservation_detail.is_cancellable)
            {
                $('.rem-btn-border').show();
            }else{
                $('.rem-btn-border').hide();
                $('#reservation-details button.modify-reservation_btn').addClass('hide');
            }
            if(data.reservation_detail.status.slug=='cancelled'){
                $('.cancellation_msg p:first').html("Cancelled at "+data.reservation_detail.cancelled_at);
                if(parseInt(data.reservation_detail.total_charged_amount)>0 && $.trim(data.reservation_detail.payment_status)=='charged'){
                    $('.cancellation_msg p:last').html("<b class='color-red'>Cancellation charge of $"+data.reservation_detail.total_charged_amount+" was charged for this reservation</b>");
                }
            }

            //alert($('#reservation-details #tables').val());

        },
        error: function() {
            alertbox('Failure', 'An error has occurred.', function(modal){

            });
        }
    });
}

function makeBookingInfoEditable() {
    $('#reservation-details #detail_dob').attr('disabled', false);
    $('#reservation-details #detail_doa').attr('disabled', false);
    $("#reservation-details #guest_note").prop('disabled', false);
    $('#reservation-details #reservationdeatails-datepicker').attr('disabled', false);
    $('#reservation-details #reservationdeatails-timepicker').attr('disabled', false);
    $('#reservation-details .party_size').attr('disabled', false);
    $('#reservation-details #floors').attr('disabled', true);
    $('#reservation-details #tables').attr('disabled', true);
    $('#reservation-details #floors, #tables').selectpicker('refresh');
    $('#reservation-details #guest_tags_booking').attr('disabled', false);
    $('#reservation-details .b_change_date').show();
    $('#reservation-details #special_occasion').prop('disabled', false);
    $('#reservation-details #special_instruction').prop('disabled', false);
    $('#reservation-details #host').prop('disabled', false);
    setTimeout(function () {
        $('#reservation-details .tagit-new .ui-autocomplete-input').prop('disabled', false);
    },1000);
}

function makeBookingInfoUnEditable() {
    $('#reservation-details #fname, #reservation-details #lname, #reservation-details #mobile, #reservation-details #email').prop('disabled', true);
    $('#reservation-details #detail_dob').prop('disabled', true);
    $('#reservation-details #detail_doa').prop('disabled', true);
    $("#reservation-details #guest_note").prop('disabled', true);
    $('#reservation-details #reservationdeatails-datepicker').prop('disabled', true);
    $('#reservation-details #reservationdeatails-timepicker').prop('disabled', true);
    $('#reservation-details .party_size').prop('disabled', true);
    $('#reservation-details #start_time').prop('readonly', 'readonly');
    $('#reservation-details #floors').prop('disabled', true);
    $('#reservation-details #tables').prop('disabled', true);
    $('#reservation-details #special_occasion').prop('disabled', true);
    $('#reservation-details #special_instruction').prop('disabled', true);
    $('#reservation-details #host').prop('disabled', true);
    $('#reservation-details #floors, #tables').selectpicker('refresh');
    $('#reservation-details #guest_tags_booking').prop('disabled', true);
    $('#reservation-details #status_current').prop('disabled', true);
    //$("#reservation-details #register_guest").unbind("click");
    $('#reservation-details #register_guest').prop('disabled', true);
    $('#reservation-details #register_guest').prop('checked', false);
    setTimeout(function () {
        $('#reservation-details .tag-plus .ui-autocomplete-input').prop('disabled', true).val('');
    },1000);
}
/*/    ------add/view guest details slide up and down-------Amit*/
$('.add-view-guest-list').slideUp();
$('#add-view-guest').on('click', function(){
    if($('.add-view-guest-list').is(':visible')) {
        $('#add-view-guest').text('Add / View Guest Details');
        $('.add-view-guest-list').slideUp(500);
    }else{
        $('#add-view-guest').text('Hide Guest Details');
        $('.add-view-guest-list').slideDown(500);
    }
});

$('.tbl-radio-btn input[type="radio"]').on('click', function() {
    if ($('#other[type="radio"]').is(':checked')===true) {
        $('.input__field.boxx').removeClass('hide');
    } else{
        $('.input__field.boxx').addClass('hide');
    }
});

$('#reservation-edit .check-availability__btn.save').on('click', function () {
    if(!$("#reservation-edit").valid()){
        return false;
    }
    var formdata = $('#reservation-edit').serialize();
    var tags = $('#reservation-details #guest_tags_booking').tagit("assignedTags");

    console.log(tags)

    var url = $('#reservation-edit').attr('action');
    var method = $('#reservation-edit').attr('method');
    window.localStorage.setItem('reservation_going_on', 'yes');
    $('#loader').removeClass('hidden');
    $.ajax({
        type: method,
        url: url,
        data: formdata,
        success: function (data) {
            $('#loader').addClass('hidden');
            alertbox('Success',data.message, function(modal){
                if (window.localStorage.getItem("gridActiveTab") === null) {
                        floorGridViewUpdate();
                }else{
                    if ($('#grid-view').length === 1) {
                        getUpdateGridView();
                        floorGridViewUpdate();
                    }
                }
            });
            window.localStorage.removeItem("reservation_going_on");
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            $('.message_div').empty().html(err);
            $('.message_div').show();
        }
    });
});

$( ".fname_booking_detail" ).autocomplete({
    source: "/guestbook/search",
    minLength: 2,
    open: function() {
        $('.ui-autocomplete').width($(this).width());
        $('.ui-autocomplete > li')
            .wrapAll($('<div class="scroller_auto max-height"></div>')
                .width($(this).width())
                .height(_jSheight));
        $(".scroller_auto").mCustomScrollbar(options);

    },

    select: function( event, ui ) {
        $("#reservation-details").find(':input').not('.reservation-book').val('');
        fetchUserDataAndActivityInReservationDetail(ui.item.id, 'search');
    },   //HERE - make sure to add the comma after your select
    response: function(event, ui) {
        if (!ui.content.length) {
            $("#reservation-edit").valid();
            $('#reservation-details #clock_detail .oldreservations_bookinform').html('No record exist.');
            $('#reservation-details .register_customer').show();
            $('#reservation-details .add_view1').hide();
            $('#reservation-details .add-view-guest-list1').hide();
            $("#reservation-edit").find(':input').not('.reservation-book').val('');
            $('#reservation-details #user_id').val('');
            $('#reservation-details #register_guest').prop('checked', false);
            $('#reservation-details #register_guest').removeAttr('disabled');
        }
    }
});

function fetchUserDataAndActivityInReservationDetail(user_id, source)
{
    if(user_id){
        $.ajax({
            type: 'GET',
            url: '/guestbook/detail/'+user_id ,
            dataType: 'json',
            success: function (data) {
                var reservations = '';
                var userdata = data.data;
                if (userdata.guest) {
                    //console.log(userdata.guest);
                    var availableTags = userdata.tags;
                    $('#reservation-details .add_view1').show();
                    $('#reservation-details #user_id').val(userdata.guest.id);
                    if(source!='detail'){
                        $("#reservation-details #fname").val(userdata.guest.fname);
                        $("#reservation-details #lname").val(userdata.guest.lname);
                        $("#reservation-details #mobile").val(userdata.guest.mobile);
                        $("#reservation-details #email").val(userdata.guest.email);
                    }
                    $("#reservation-details #guest_note").val(userdata.guest.description);
                    $("#reservation-details #detail_dob").val(userdata.guest.dob);
                    $("#reservation-details #detail_doa").val(userdata.guest.doa);
                    $("#reservation-details #total_orders").val(userdata.total_orders);
                    $("#reservation-details #total_resv_count").val(userdata.total_resv_count);
                    $("#reservation-details #total_walk_in_count").val(userdata.total_walk_in_count);
                    $("#reservation-details #total_cancelled_count").val(userdata.total_cancelled_count);
                    if (userdata.guest.reservations) {
                        $.each(userdata.guest.reservations, function (key, value) {
                            var icon = JSON.parse(value.status.icon);
                            var color = JSON.parse(value.status.color);
                            reservations += '<li><div class="clock--tab-flex"> <div class=""> <div><span><i style="color:'+color.sidebar+'" class="' + icon.sidebar + '" ></i></span></div> <span><b>' + value.reservation_day + ', ' + value.reservation_date + ' </b> <span class="lyt-color">' + value.reservation_time + ', party of ' + value.reserved_seat + '</span></span> </div> <div class="">' + value.status.name + '</div> </div>';
                            reservations += '</li>';
                        });
                        if(reservations){
                            $('#reservation-details #clock_detail .oldreservations_bookinform').html(reservations);
                        }
                    }
                    $("#reservation-details #guest_tags_booking").tagit("removeAll");
                    if (userdata.guest.tags) {
                        var taglist = '';
                        $.each(userdata.guest.tags, function (key, value) {
                            $("#reservation-details #guest_tags_booking").tagit("createTag", value);  //Using option 3
                        });
                    }
                    $('#reservation-details #register_guest').prop('checked', true);
                    $('#reservation-details #register_guest').attr('disabled', 'disabled');
                } else {
                    $("#reservation-edit").find(':input').not('.reservation-book').val('');
                    $('#reservation-details #clock .oldreservations_bookinform').html('No record exist.');
                    $('#reservation-details #user_id').val('');
                    $('#reservation-details #register_guest').prop('checked', false);
                    $('#reservation-details #register_guest').removeAttr('disabled');
                }

                $('#reservation-edit input').nextAll().addClass('hasVal');

                if($('#reservation-edit input#special_instruction').val() == "" || $('#reservation-edit input#special_instruction').val() == null){
                    $('#reservation-edit input#special_instruction').next().removeClass('hasVal');
                }

                $("#reservation-edit").valid();
            },
            error: function (data, textStatus, errorThrown) {
                $("#reservation-edit").valid();
                $('#reservation-details #clock .oldreservations_bookinform').html('No record exist.');
                $('#reservation-details .add_view1').hide();
                $('#reservation-details .add-view-guest-list1').hide();
                $("#reservation-edit").find(':input').not('.reservation-book').val('');
                $('#reservation-details #user_id').val('');
                $('#reservation-details .register_customer').show();
                $('#reservation-details #register_guest').removeAttr('disabled');
                $('#reservation-details #register_guest').prop('checked', false);
            }
        });
    }
}

$('.alert').on('click','.close',function(){
    $(this).closest('.alert').slideUp();
});

$('#floor_id').on('change', function() {
    var floor_id = $(this).val();
    $.each(walk_in_floor_data, function(key, floor){
        if(floor_id == floor.id) {
            if(floor.tables) {
                var tableHtml = '<option value="">Select Table</option>';
                $.each(floor.tables, function(table_key, table_value) {
                    tableHtml += '<option value="'+table_value.id+'">'+table_value.name+'</option>';
                });
                $('#table_id').html(tableHtml);
                $(".selectpicker").selectpicker('refresh');
            }
        }
    });
});

window.currentTime= "";
function getCurrentClock() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    m = checkTime(m);
    currentTime = h + ":" + m + ":" +'00' ;
    var t = setTimeout(getCurrentClock, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function gridView(arr){
    var floor_data=[];
    floor_data[0]=JSON.parse(arr[0]);
    floor_data[1]=[];
    floor_data[2]=[];
    floor_data[3]=[];
    var inc=1;
    $.each(arr[1], function(key, value){
        var table=JSON.parse(value);
        var gridtables={};
        gridtables['id']=table.id;
        gridtables['building']='Tables';
        gridtables['title']=table.tableText+'('+table.maxGuests+")";
        floor_data[1][key] =gridtables;
        if(table.reservations && table.reservations.length){
            $.each(table.reservations, function(reskey, resvalue){
                var gridrsr={};
                gridrsr['id']=inc;
                gridrsr['resourceId']=resvalue.resourceId;
                gridrsr['start']=resvalue.start;
                gridrsr['end']=resvalue.end;
                gridrsr['title']=resvalue.title;
                gridrsr['resv_id']=resvalue.resv_id;
                if(gridrsr!=null){
                    floor_data[2][inc] =gridrsr;
                }
                /*setTimeout(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                },100);*/
                inc++;
            });
        }
    });
    floor_data[2].shift();
    floor_data[3] = arr[3];
    grid_view_func(floor_data);
}

// DatePicker Init

var newDate = new Date();
var currentYear = newDate.getFullYear();
var currentMonth =
    newDate.getMonth() + 1 < 10
        ? "0" + (newDate.getMonth() + 1)
        : newDate.getMonth() + 1;

var todayDate =
    newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate();
var yesterdayDate =
    newDate.getDate() - 1 < 10
        ? "0" + (newDate.getDate() - 1)
        : newDate.getDate() - 1;
var tommDate =
    newDate.getDate() + 1 < 10
        ? "0" + (newDate.getDate() + 1)
        : newDate.getDate() + 1;

var rnDate = currentMonth + "-" + todayDate + "-" + currentYear;
var ysDate = currentMonth + "-" + yesterdayDate + "-" + currentYear;
var tmDate = currentMonth + "-" + tommDate + "-" + currentYear;

window.pluginDate = "";

window.dateFormat = function(){
    pluginDate = $(".table_datePicker").val();

    if (pluginDate == rnDate) {
        $(".table_datePicker").val("Today");
    } else if (pluginDate == ysDate) {
        $(".table_datePicker").val("Yesterday");
    } else if (pluginDate == tmDate) {
        //alert(tmDate)
        //$(".table_datePicker").val("Tomorrow");
    }

    if ($(".table_datePicker").val() === "Today") {
        //$(".table_datePicker + i + i.greenColor").removeClass("hide");
        $(".table_datePicker").nextAll().removeClass("hide");
        $("#show-hide-timer").removeClass("hide")
    } else {
        //$(".table_datePicker + i + i.greenColor").addClass("hide");
        $(".table_datePicker").nextAll().addClass("hide");
        $("#show-hide-timer").addClass("hide")
    }
}

$( ".tableManagement__wrapper #datepicker").datepicker({
    minDate: 0,
    dateFormat:'mm-dd-yy'
}).datepicker('setDate', new Date());

$(".tableManagement__wrapper #datepicker").on('change', function() {
    dateFormat();
    floorGridViewUpdate();
    getUpdateGridView();
    $("#reservation_info_popup,#book-a-table").modal('hide');
});

dateFormat();

function getReservationTableInfo(table_id){
    if(table_id){
        $.ajax({
            type: 'get',
            url: '/floor/reservation/'+table_id,
            success: function (data) {
                $('#reservation_info_popup .modal-content').html(data);
                $(".selectpicker").selectpicker('refresh');

                if(data){

                    if(!$("#reservation_info_popup.fade").hasClass("in")) {
                        $('#reservation_info_popup').modal('show');
                    }

                    openSibebarController(true)
                    //$('#tableManagement__wrapper').addClass('openSibebar');
                    $('#reservation_info_popup').on('hidden.bs.modal', function () {
                        //openSibebarController(false)
                        clearInterval(reservation_info_popup)
                        $(".bookingnew").attr('table-id',0)
                    })
                    difftime();
                }

                $("#reservation_info_popup .current_status").change(function(){

                    var reservation_id=$(this).attr('reservation_id');
                    var current_status=$(this).attr('current_status');
                    var new_status=$(this).val();
                    var type='current';
                    if($(".status_tab.active").hasClass('current')){
                        type='current';
                    }else if($(".status_tab.active").hasClass('upcoming')){
                        type='upcoming';

                    }else if($(".status_tab.active").hasClass('waitlist')){
                        type='waitlist';
                    }
                    var floor_id = floor_url.split("/floor/")[1];
                    if($('#Current #current_status option:selected').text().trim() == 'Cancelled'){
                        openCancellationPopUp(reservation_id, this.value, type);
                        return false;
                    }
                    changeReservationStatus(reservation_id, new_status, type, null, null, floor_id, table_id);

                    getReservationTableInfo(table_id);
                    //$(".floor_fetch #datepicker").trigger('change');

                })

            },
            error: function (data, textStatus, errorThrown) {
                $('#reservation_info_popup').modal('hide');
                if(data.responseJSON.errors){
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            // location.reload();
                        }, 2000);

                    });

                }

            }
        });
    }

}

function getTableId(id){
    getReservationTableInfo(id);
   /* window.setTableInterval = setInterval(function(){
        getReservationTableInfo(id);
        console.log(1)
    },5000)*/
}

/*$("#reservation_info_popup").on("hidden.bs.modal", function () {
    clearInterval(setTableInterval)
})*/


function extendTillNextSlot(cname, cvalue, curr) {
    console.log(cname);
    console.log(cvalue);
    console.log(curr);
    $.ajax({
        type: 'POST',
        url: '/reservation/extend/'+cvalue,
        //data: cvalue,
        success: function (data) {
            curr.parents('.reservationStatusPanel__user').removeClass('reserved__timer__late');
            $('.' + cname).hide();

            alertbox('Success',data.message,function(modal){
                $('#loader').addClass('hidden');
                setTimeout(function() {
                    modal.modal('hide');
                }, 2000);
            });
        },
        error: function (data, textStatus, errorThrown) {
            if(tab_target!=undefined) {
                floorGridViewUpdate();
            }
            $('#loader').addClass('hidden');
            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){
            });
        }
    });
    return true;
    /*console.log($(this).text());
    var min         = 5;
    var d           = new Date();
    d.setTime(d.getTime() + (min * 60 * 1000));
    var expires     = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";*/

}


function changeReservationStatus(reservation_id, new_status, tab_target, no_show, cancellation_reason, floor_id, table_id) {
    var inputData = 'new_status='+new_status;
    if(floor_id!=undefined){
        inputData+='&floor_id='+floor_id;
    }
    if(table_id!=undefined){
        if(table_id instanceof Array) {
            inputData+= '&table_id='+table_id.join(",");
        } else {
            inputData+= '&table_id='+table_id;
        }
    }
    if(no_show!=undefined){
        inputData+='&no_show='+no_show;
    }else{
        inputData+='&type='+tab_target;
    }
    if(cancellation_reason!=undefined){
        inputData+='&cancellation_reason='+cancellation_reason;
    }
    window.localStorage.setItem('reservation_going_on', 'yes');
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'POST',
        url: '/reservation/change-status/'+reservation_id,
        data: inputData,
        success: function (data) {
            alertbox('Success',data.message,function(modal){
                $('#loader').addClass('hidden');
                if(window.location.href.indexOf('reservation')>=0){
                    window.location.reload(true);
                }else{
                    if(tab_target!=undefined || tab_target!=null || tab_target!='') {
                        displayRightSideReservation(tab_target);
                    }
                    if($("#grid-view-btn").hasClass('active')){
                        getUpdateGridView();
                    }
                }
            });
            openSibebarController(false)
            window.localStorage.removeItem('reservation_going_on');
        },
        error: function (data, textStatus, errorThrown) {
            var cur_url = window.location.href.split('/').pop();
            if(tab_target!=undefined && window.location.href.indexOf('reservation')<0) {
                floorGridViewUpdate();
            }
            $('#loader').addClass('hidden');
            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){
            });
            openSibebarController(false)
        }
    });

    if(window.localStorage.getItem("gridActiveTab") === null) {
        floorGridViewUpdate();
    }
    if(window.localStorage.getItem("gridActiveTab") !== null) {
        if($('#grid-view').length === 1) {
            getUpdateGridView();
        }
    }

}

$(document).ready(function() {
  
            
    $("#reservation_settings").validate({
        rules: {
            accept_online_reservation: {
                required: true,
            },
            min_for_online_reservation: {
                required: true,
            },
            max_for_online_reservation: {
                required: true,
            },
            allow_guest_advance_reservation_time: {
                required: true,
            },
            cuttoff_time: {
                required: true,
            },
            cuttoff_type: {
                required: true,
            },
            require_hostname_accepting: {
                required: true,
            },
            enable_online_waitlist: {
                required: true,
            }
        }
    });

   $("#notification_settings").validate({
        rules: {
            guest_sms_email_on_reservation: {
                required: true,
            },
            guest_reservation_reminder_via_sms: {
                required: true,
            },
            restaurant_receive_notification_emails: {
                required: true,
            },
            multiple_emails_input :{
                email: true
            }
        }
    });



    $("#third_step_walkin").validate({
        rules: {
            firstname: {
                required: true,
            },
            phoneNo: {
                required: true,
               // phoneUS: true
            },
            email: {
                required: true,
                email: true
            }
        },
           messages:{
            firstname :"Please enter the customer's first name.",
            email: {
                required: "Please enter the customer's email address.",
                email: "Please enter a valid email address (e.g. yourname@example.com)."
            },
            phoneNo:{
               //required:"Please enter the customer's phone number.",
               required:"Please enter a valid 10-digit US phone number.",
                //phoneUS:"Please enter a valid 10-digit US phone number."
             }
        }
    });
    $('#waitlist_no_tables_form').validate({
        rules: {
            firstName: {
                required: true,
            },
            phoneNo: {
                required: true,
                //phoneUS: true
            },
            email: {
                required: true,
                email: true
            }
        },
         messages:{
            firstName :"Please enter the customer's first name.",
            email:{
                required: "Please enter the customer's email address.",
                email: "Please enter a valid email address (e.g. yourname@example.com)."
            },
            phoneNo:{
              // required:"Please enter the customer's phone number.",
               required:"Please enter a valid 10-digit US phone number",
               // phoneUS:"Please enter a valid 10-digit US phone number."
             }
        }
    });

    $('#waitlist_popup').validate({
        rules: {
            firstName: {
                required: true,
            },
            phoneNo: {
                required: true,
                //phoneUS: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages:{
            firstName :"Please enter the customer's first name.",
            email:{
                required: "Please enter the customer's email address.",
                email: "Please enter a valid email address (e.g. yourname@example.com)."
            },
            phoneNo:{
               // required:"Please enter the customer's phone number.",
                required:"Please enter a valid 10-digit US phone number.",
                //phoneUS:"Please enter a valid 10-digit US phone number."
            }
        }
    });

    $("#walkin_floor_table_form").validate({
        rules: {
            walkin_floor: {
                required: true,
            },
            walkin_table: {
                required: true,
            },
        },
        messages: {
            walkin_floor: {
                required: "Floor should be selected",
            },
            walkin_table: {
                required: "Table should be selected",
            },
        }
    });


    $( "#add-tabletype" ).validate({
        rules: {
            name: {
                required: true,
            },
            min: {
                required: true,
            },
            max: {
                required: true,
            },
            tat: {
                required: true,
            },
        },
        messages: {
            name: {
                required: "Table type name should not be empty",
            },
            min: {
                required: "Table type minimum party size should not be empty",
            },
            max: {
                required: "Table type maximum party size should not be empty",
            },
            tat: {
                required: "Table type turn around time should not be empty",
            }
        }
    });
    $("#update-tabletype" ).validate({
        rules: {
            name: {
                required: true,
            },
            min: {
                required: true,
            },
            max: {
                required: true,
            },
            tat: {
                required: true,
            },
        },
        messages: {
            name: {
                required: "Table type name should not be empty",
            },
            min: {
                required: "Table type minimum party size should not be empty",
            },
            max: {
                required: "Table type maximum party size should not be empty",
            },
            tat: {
                required: "Table type turn around time should not be empty",
            }
        }
    });


    $.validator.addMethod("noSpecialChars", function(value, element) {
        return this.optional(element) || /^[a-z0-9\ ]+$/i.test(value);
    }, "Please use only alphanumeric characters.");

    $( "#add-floor" ).validate({
        rules: {
            name: {
                required: true,
                maxlength:50

            }
        },
        messages: {
            name: {
                required: "Please enter the "+$( "#add-floor #name" ).attr('floortype')+" name.",
                maxlength:"Maximum character limit is 50"
            }
        }
    });

    $( ".delete_floor" ).click(function(){

        var floor_id=$(this).attr('floor_id');

        ConfirmBox('Confirm Floor Delete','Do you really want to delete this floor? All layout and table configurations will be deleted.',function(resp,curmodal){

            if(resp){
                console.log('deleting '+floor_id);

                $.ajax({
                    type: 'delete',
                    url: '/floor/delete/'+floor_id,
                    // data: formdata,
                    success: function (data) {
                        alertbox('Deleted',data.message,function(modal){

                            setTimeout(function() {
                                modal.modal('hide');
                                location.reload();
                            }, 2000);

                        });
                    },
                    error: function (data, textStatus, errorThrown) {
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        alertbox('Error',err,function(modal){
                            setTimeout(function() {
                                modal.modal('hide');
                                // location.reload();
                            }, 2000);

                        });
                    }
                });

                curmodal.modal('hide');
            }
        });

    });

    window.deleteTable = function (table_id,getCurrentTableId){

        //ConfirmBox('Cannot Delete Table','The table cannot be deleted as it has upcoming reservations. Move reservations on this table to another table and try deleting.',function(resp,curmodal){

           // if(resp){
                console.log('deleting '+table_id);

                $.ajax({
                    type: 'delete',
                    url: '/floor/table/delete/'+table_id,
                    // data: formdata,
                    success: function (data) {
                        //ConfirmBox('Confirm Delete','On Floor View this table will not appear. Do you really want to delete this table?',function(resp,curmodal){
                            //if(resp){
                               alertbox('Deleted',data.message,function(modal){
                                    resetTableLayout(getCurrentTableId);
                                    setTimeout(function() {
                                       // modal.modal('hide');
                                        //location.reload();
                                    }, 2000);
                                });
                            //curmodal.modal('hide');
                            //}
                        //});
                    },
                    error: function (data, textStatus, errorThrown) {
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        alertbox('Cannot Delete Table','The table cannot be deleted as it has upcoming or ongoing reservations. Move reservation on this table to another table and try deleting.',function(modal){
                            setTimeout(function() {
                               // modal.modal('hide');
                                //location.reload();
                            }, 2000);
                        });
                    }
                });

               // curmodal.modal('hide');
           // }
        //});

    }



    window.updateTableInfo=function(table_id,tabledata){

        var getReturn;

        $.ajax({
            type: 'post',
            contentType: 'application/json',
            url: '/floor/table/update/'+table_id,
            data: JSON.stringify(tabledata),
            success: function (data) {

                alertbox('Success',data.message,function(modal){
                    setTimeout(function() {
                        modal.modal('hide');
                        //window.location.href='/floor';
                        //location.reload();
                    }, 1000);

                    //tableModifyRefresh();

                    getReturn = data.success;

                });

            },
            error: function (data, textStatus, errorThrown) {

                var err='';
                $.each(data.responseJSON.errors, function(key, value){
                    err+='<p>'+value+'</p>';
                });
                alertbox('Error',err,function(modal){

                    setTimeout(function() {
                        modal.modal('hide');
                    }, 2000);

                    removeBtnDisabledAttr();

                    getReturn = data.success

                });

            }
        });


        return getReturn;

    }
    function uniqueFloorName(fname,cnt){

        console.log('counting ');
        if (cnt === undefined) {
            cnt=0;
        }
        var oldname=fname.split('copy')[0];
        flrname=oldname+'copy'+cnt;
        if(floor_names.indexOf(flrname)!=-1){
            console.log('duplicate found');
            uniqueFloorName(fname,cnt+1);
        }

        return flrname;
    }

    $( ".update_floor" ).click(function(){

        var floor_id=$(this).attr('floor_id');
        var cur_floor=$(this).parents('.floor__element').find('.floor__name').text();

        ConfirmBox('Duplicate Floor',' A duplicate floor will be created with the same layout and table configuration. Do you really want to proceed?',function(resp,curmodal){
            var floorname=uniqueFloorName(cur_floor);

            if(resp && floorname){
                console.log('duplicating '+floor_id);

                $.ajax({
                    type: 'get',
                    url: '/floor/duplicate/'+floor_id,
                    data: {'floorname':floorname},
                    success: function (data) {

                        alertbox(data.title,data.message,function(modal){

                            setTimeout(function() {
                                modal.modal('hide');
                                // location.reload();
                                window.location.href='/floor/configure/'+data.floor_id;
                                //window.location.href='/floor';

                            }, 2000);

                        });
                    },
                    error: function (data, textStatus, errorThrown) {
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        alertbox('Error',err,function(modal){
                            setTimeout(function() {
                                modal.modal('hide');
                                location.reload();
                            }, 2000);

                        });
                    }
                });

                curmodal.modal('hide');
            }
        });

    });

    $( ".delete_table_type" ).click(function(){

        var table_type_id=$(this).attr('table_type_id');

        ConfirmBox('Confirm Delete','All data related to this  will be deleted. Do you wish to proceed?',function(resp,curmodal){

            if(resp){
                console.log('deleting '+table_type_id);

                $.ajax({
                    type: 'delete',
                    url: '/turnovertime/'+table_type_id,
                    // data: formdata,
                    success: function (data) {
                        alertbox('Deleted',data.message,function(modal){

                            setTimeout(function() {
                                modal.modal('hide');
                                location.reload();
                            }, 2000);

                        });
                    },
                    error: function (data, textStatus, errorThrown) {
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        alertbox('Error',err,function(modal){
                            setTimeout(function() {
                                modal.modal('hide');
                                location.reload();
                            }, 2000);

                        });
                    }
                });

                curmodal.modal('hide');
            }
        });

    });
    $("#book-a-table .dob").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false,
        dateFormat: 'mm-dd-yy',
        yearRange: '-80y:c+nn',
        maxDate: '-1d',
        onSelect: function() {
            $(this).change();
        }
    });

    $('#book-a-table .doa').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false,
        dateFormat: 'mm-dd-yy',
        yearRange: '-80y:c+nn',
        maxDate: '-1d',
        onSelect: function() {
            $(this).change();
        }
    });

    $("#reservation-details #detail_dob, #reservation-details #detail_doa").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false,
        dateFormat: 'mm-dd-yy',
        yearRange: '-80y:c+nn',
        maxDate: '-1d',
        onSelect: function() {
            $(this).change();
        }
    });
    

});


function saveTabeltype(){

    var $form = $('#add-tabletype');
    if(!$form.valid()){
        return false;
    }
    var inputData = $form.serialize();
    console.log(inputData);

    $.ajax({
        type: 'post',
        url: '/turnovertime/add',
        data: inputData,
        success: function (data) {

            alertbox('Success',data.message,function(modal){
                setTimeout(function() {
                    $('#add-tabletype-modal').modal('hide');
                    location.reload();
                }, 1000);

            });
        },
        error: function (data, textStatus, errorThrown) {

            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){

                setTimeout(function() {
                    $('#add-tabletype-modal').modal('hide');
                    location.reload();
                }, 2000);

            });
        }
    });

}



function openEditTableTypeform(table_type_id,table_type_json){

    $("#update-tabletype-modal").modal('show');
    $('#update-tabletype .name').val(table_type_json.name);
    $('#update-tabletype .max').val(table_type_json.max_seat);
    $('#update-tabletype .min').val(table_type_json.min_seat);
    $('#update-tabletype .tat').val(table_type_json.tat);


    $('#update-tabletype .table_type_id').val(table_type_id);

    $('#update-tabletype-modal .updatebtn').click(function(){

        var table_type_id= $('#update-tabletype .table_type_id').val();
        var $form = $('#update-tabletype');
        if(!$form.valid()){
            return false;
        }
        var inputData = $form.serialize();
        console.log(inputData);

        $.ajax({
            type: 'post',
            url: '/turnovertime/update/'+table_type_id,
            data: inputData,
            success: function (data) {

                alertbox('Success',data.message,function(modal){
                    setTimeout(function() {
                        $('#update-tabletype-modal').modal('hide');
                        location.reload();
                    }, 1000);

                });
            },
            error: function (data, textStatus, errorThrown) {

                var err='';
                $.each(data.responseJSON.errors, function(key, value){
                    err+='<p>'+value+'</p>';
                });
                alertbox('Error',err,function(modal){

                    setTimeout(function() {
                        $('#update-tabletype-modal').modal('hide');
                        location.reload();
                    }, 2000);

                });
            }
        });

    });

}


function saveLayout(){
    if($("#floorNameForm").valid() == true && tableManagement[1] ){
        tableManagement[0].floorName=$('#floorName').val();
        $.ajax({
            type: 'post',
            contentType: 'application/json',
            cache: false,
            data: JSON.stringify(tableManagement),
            success: function (data) {

                if(viewMode == "Add"){
                    alertbox("Floor Created", "A new floor plan has been created.", function (modal) {
                        setTimeout(function () {
                            window.location.href='/floor';
                            //location.reload();
                        }, 1500);

                    });
                } else {
                    alertbox(data.title, data.message, function (modal) {
                        setTimeout(function () {
                            window.location.href='/floor';
                            //location.reload();
                        }, 1500);

                    });
                }

            },
            error: function (data, textStatus, errorThrown) {

                var err='';
                $.each(data.responseJSON.errors, function(key, value){
                    err+='<p>'+value+'</p>';
                });
                alertbox('Error',err,function(modal){

                    setTimeout(function() {
                        // window.location.href='/floor';
                    }, 2000);

                });

            }
        });
    }

}



(function () {

    $('.savetags').click(function (e) {
       e.preventDefault();
      
            $.ajax({
                type: 'post',
                data: $('#alltags').serialize() ,
                success: function (data) {

                    alertbox('Success',data.message,function(modal){
                        setTimeout(function() {
                            location.reload();
                        }, 1000);

                    });

                },
                error: function (data, textStatus, errorThrown) {

                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){

                        setTimeout(function() {
                            //location.reload();
                        }, 2000);

                    });

                }
            });

    });

    $(document).on("changed.bs.select", "select",
        function(e, clickedIndex, newValue, oldValue) {
        
        if(this.value != "") {
                $(this).removeClass('error')
                $(this).next().css("display","none")
            }
    })
    
    
    $(".reservationStatusPanel").on("changed.bs.select", "select",
        function(e, clickedIndex, newValue, oldValue) {
            //console.log($(this).hasClass('table_type_configure'));
            //console.log(this.value, newValue, oldValue);
            if(this.value != "") {
                $(this).removeClass('error')
                $(this).next().css("display","none")
            }

            if($(this).hasClass('sidbar_status')){

                var reservation_id=$(this).parents('.reservationStatusPanel__user').attr('reservation-id');
                var type='current';
                if($(".status_tab.active").hasClass('upcoming')){
                    type='upcoming';
                }if($(".status_tab.active").hasClass('current')){
                    type='current';
                }if($(".status_tab.active").hasClass('waitlist')){
                    type='waitlist';
                }
                if($(this).find(":selected").text().trim() == 'Cancelled'){
                    openCancellationPopUp(reservation_id, this.value, type);
                    return false;
                }
                var floor_id = floor_url.split("/floor/")[1];
                var table_id = null;
                if($(".status_tab.active").hasClass('waitlist')) {
                    table_id = $('#manageWaitlistModal #table_plan_walkin').val();
                }
                console.log(floor_id, table_id);
                changeReservationStatus(reservation_id, this.value, type, null, null, floor_id, table_id);
            }

        });
    $("#reservation-details .status-dropdown").on("changed.bs.select", "select",
    //$("#reservation-details").on("change", ".status-dropdown",
        function(e, clickedIndex, newValue, oldValue) {

            //console.log($(this).hasClass('table_type_configure'));
            //console.log(this.value, newValue, oldValue);
            if(this.value != "") {
                $(this).removeClass('error');
                $(this).next().css("display","none")
            }
            var reservation_id=$('#reservation-details #reservation_id').val();
            if($('#status_current option:selected').text().trim() == 'Cancelled'){
                openCancellationPopUp(reservation_id, this.value);
                return false;
            }

            var floor_id = $("#reservation-details #floors").val();
            var table_id = $("#reservation-details #tables").val();
            console.log(reservation_id, this.value, floor_id, table_id);

            changeReservationStatus(reservation_id, this.value, tab_target, null, null, floor_id, table_id);
    });
}(jQuery));

   $('.update_custom_tag_container .edit_custom_tag_delete').click(function (e) {
          var tag_id=$(this).attr('tag_id');

        ConfirmBox('Confirm ','Do you really want to delete this tag? ',function(resp,curmodal){

            if(resp){

                $.ajax({
                type: 'delete',
                url: 'custom-tag/delete/'+tag_id,
                success: function (data) {

                    alertbox('Success',data.message,function(modal){
                        setTimeout(function() {
                         location.reload();
                        }, 1000);

                    });

                },
                error: function (data, textStatus, errorThrown) {
                      $('.new_custom_tag').val('');
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){

                        setTimeout(function() {
                            //location.reload();
                        }, 2000);

                    });

                }
            });

                curmodal.modal('hide');
            }
        });
          
   });
  $('.update_custom_tag_container .update_custom_tag').click(function (e) {

        e.preventDefault();
       var $form = $(this).parents('form');
        if(!$form.valid()){
            return false;
        }
        var inputelement=$(this).parents('.update_custom_tag_container').find('.custom_tag');
         var data={};
            data.tag_id=inputelement.attr('tag_id');
            data.tag_name=inputelement.val();
            console.log(data);
         $.ajax({
                type: 'post',
                url: '/configure/custom-tag/update',
                data:data,
                success: function (data) {

                    alertbox('Success',data.message,function(modal){
                        setTimeout(function() {
                         location.reload();
                        }, 1000);

                    });

                },
                error: function (data, textStatus, errorThrown) {
                      $('.new_custom_tag').val('');
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){

                        setTimeout(function() {
                            //location.reload();
                        }, 2000);

                    });

                }
            });

});

function tagscheck(){
    var visibletags=$('.tag_listing_box:visible').length;
    if(visibletags){
        $(".savetags").show();
    }else{
        $(".savetags").hide(); 
    }
}
  $('.tags_search_filter #tag_keyword').on('input',function (e) {
    $('.update_custom_tag_form').show();
    var keyword=$(this).val();
    if(keyword!='' && keyword.length>2){
        keyword=keyword.toLowerCase();

         var type=  $('.tags_tab_head .active').attr('type');    
              
              $('.tag_listing_box').each(function(){

                var curr_val=$(this).attr('val').toLowerCase();   
                //console.log(curr_val.indexOf(keyword));
                if(curr_val.indexOf(keyword) != -1){
                    $(this).parent().show();
                }else{                     
                    $(this).parent().hide();

                }
              })

        $(".add-customtag").hide();
    } else {
        $(".add-customtag").show();
    }

    tagscheck();

});


$('#addcustom_form button.close_tags').click(function (e) {
    $('#addcustom').removeClass('in').attr("aria-expanded",false);
});
  $('#addcustom_form .add_tags').click(function (e) {
        
       e.preventDefault();
       var $form = $('#addcustom_form');
        if(!$form.valid()){
            return false;
        }

        var custom_tag=$('#addcustom_form .new_custom_tag').val();
        if(!custom_tag.trim()) {
            return false;
        }
     
            $.ajax({
                type: 'get',
                url: '/configure/custom-tag/'+custom_tag,

                success: function (data) {

                    alertbox('Success',data.message,function(modal){
                        setTimeout(function() {
                         location.reload();
                        }, 1000);

                    });

                },
                error: function (data, textStatus, errorThrown) {
                      $('.new_custom_tag').val('');
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){

                        setTimeout(function() {
                            //location.reload();
                        }, 2000);

                    });

                }
            });

    });

    $('.working__tab.customtag a').on('click', function(e) {
        window.localStorage.setItem('gridActiveTab', $(e.target).attr('href'));
        tabReset();
        console.log(gridActiveTab);
       
    });
    var gridActiveTab = window.localStorage.getItem('gridActiveTab');
    if (gridActiveTab) {
        $('.working__tab.customtag a[href="' + gridActiveTab + '"]').tab('show');
      
    }


$('a[href="#predefined_tags"]').click(function (e) {
    window.localStorage.removeItem("gridActiveTab");
    tabReset();
});

function tabReset(){
    $('.tags_search_filter #tag_keyword').val('');
   // $('.tag_listing_box,.add-customtag').removeAttr('style');
    $('.tags_search_filter #tag_keyword').trigger("input");
    tagscheck();
    $('.tag_listing_box,.add-customtag,.savetags').removeAttr('style');
}

$('#tagmoveright').click(function (e) {
    var selectedOpts = $('.alltags option:selected').not( "[disabled]" );
    if (selectedOpts.length == 0) {
        alertbox('',"Nothing to move.",function(modal){
        });
        e.preventDefault();
    }

    $('.restaurant_tags').append($(selectedOpts).clone());
    $(selectedOpts).attr('selected', 'selected' );
    $(selectedOpts).attr('disabled', true );
    //var form = $('#restaurant_tags');
    // form.removeData('validator');
    // form.removeData('unobtrusiveValidation');
    // $.validator.unobtrusive.parse(document);

    e.preventDefault();
});

function ajaxPullReservations(date,status,timer_type){
    if(date){
        var dataobj={};
        dataobj.date=date;
        (status === undefined)?'':dataobj.type=status;
        (timer_type === undefined) ? '':dataobj.timer_type=timer_type;
        $.ajax({
            type: 'post',
            contentType: 'application/json',

            data: JSON.stringify(dataobj),
            success: function (data) {
                //console.log(data.data);
                //floor_and_tables=data.data;

                    var date=$('.floor_fetch #datepicker').val();
                    var selectedDate = $('.floor_fetch #datepicker').datepicker('getDate');
                    var today = new Date();
                    today.setHours(0);
                    today.setMinutes(0);
                    today.setSeconds(0);
                    //console.log($(".status_tab.active"));
                    //console.log($(".status_tab.active").hasClass('upcoming'));

                    if ($("#floor-view-btn").hasClass('active') && (date=='Today' || Date.parse(today) == Date.parse(date))) {

                          floorView(JSON.parse(data.data))

                    }
                    /*if($("#grid-view-btn").hasClass('active')){

                        gridView(JSON.parse(data.data),'update');
                    }*/
                    


                
                //console.log(JSON.parse(data.data));
                //console.log(floor_and_tables);
                //console.log('data captured');


            },
            error: function (data, textStatus, errorThrown) {



            }
        });
    }

}



window.floorGridViewUpdate = function(timer_type){
    var date=$('.floor_fetch #datepicker').val();

    var selectedDate = $('.floor_fetch #datepicker').datepicker('getDate');
    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    if($('.sidebar_right__content .right-searchbar #keyword').length){
       var keyword = $('.sidebar_right__content .right-searchbar #keyword').val().trim();
    }
    if (date=='Today' || Date.parse(today) == Date.parse(date)) {
        $(".status_tab").show();
        var floor_id = window.location.pathname.split('/')[2];
        if($(".status_tab.active").hasClass('current')){
            //displayCurrentReservation(floor_id, 'current', date, keyword);
            //var date=$('.floor_fetch #datepicker').val();
            //  ajaxPullReservations(date,'current');
            displayRightSideReservation('current', timer_type);

            $(".status_tab").removeClass('active');
            $(".current").addClass('active');
            $('.nav-tabs a[href="#current"]').tab('show');
            // call getTableId() if right side bar is open
        }else if($(".status_tab.active").hasClass('upcoming')){
            //displayUpcomingReservation(floor_id, 'upcoming', date, keyword);
            //var date=$('.floor_fetch #datepicker').val();
            // ajaxPullReservations(date,'upcoming');
            displayRightSideReservation('upcoming', timer_type)
            $(".status_tab").removeClass('active');
            $(".upcoming").addClass('active');
            $('.nav-tabs a[href="#upcoming"]').tab('show');
        }else{
            // displayWaitlistReservation(floor_id, 'waitlist', date, keyword);
            //var date=$('.floor_fetch #datepicker').val();
            //ajaxPullReservations(date,'waitlist');
            displayRightSideReservation('waitlist', timer_type);

            $(".status_tab").removeClass('active');
            $(".waitlist").addClass('active');
            $('.nav-tabs a[href="#waitlist"]').tab('show');

        }
        //auto refresh table pop up
        if($('.bookingnew').length > 0){
            getTableId($('.bookingnew').attr('table-id'));
        }
    }else{

        console.log('Tab hide show');

        $(".status_tab").removeClass('active');
        //$(".status_tab").not('.upcoming').hide();
        $(".upcoming").show().addClass('active');
        $('.nav-tabs a[href="#upcoming"]').tab('show');
        //displayUpcomingReservation(floor_id, 'upcoming', date, keyword);
        var date=$('.floor_fetch #datepicker').val(); //UNCOMMENTED ACCORDING TO NEW CHANGES
        //ajaxPullReservations(date,'upcoming'); //UNCOMMENTED ACCORDING TO NEW CHANGES
        displayRightSideReservation('upcoming');
    }
    if($('.sidebar_right__content .right-searchbar #keyword').length){
        $('.sidebar_right__content .right-searchbar #keyword').val(keyword);
    }
    //getUpdateGridView();
}

$(".floor_fetch_table_info").change(function(){
    //console.log('sdfsd');

    var date=$(this).val();
    if(date){
        var dataobj={};
        dataobj.date=date;
        $.ajax({
            type: 'post',
            contentType: 'application/json',

            data: JSON.stringify(dataobj),
            success: function (data) {

                console.log(data);
            },
            error: function (data, textStatus, errorThrown) {



            }
        });
    }


});


/*****************************Reservation****************************/
var tab_target = 'current';
$('#reservations a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    tab_target = $(e.target).attr("href").replace('#', '');
    displayRightSideReservation(tab_target);
    $(".selectpicker").selectpicker('refresh');
    $('.side_bar_right #keyword').val('')
});

function displayRightSideReservation(tab_target, timer_type) {
    var date = $('#datepicker').val();

    var keyword='';
    if($('#keyword').length){
        keyword = $('#keyword').val().trim();
    }
    var floor_id = window.location.pathname.split('/')[2];
    if(tab_target=='current'){
        displayCurrentReservation(floor_id, tab_target, date, keyword);
        var date=$('.floor_fetch #datepicker').val();
        ajaxPullReservations(date,'current', timer_type);
    }else if(tab_target=='upcoming'){
        displayUpcomingReservation(floor_id, tab_target, date, keyword);
        var date=$('.floor_fetch #datepicker').val();
        //ajaxPullReservations(date,'upcoming');

        
        if($("#floor-view-btn").hasClass('active')){
            ajaxPullReservations(date,'current');
        }
        /*if($("#grid-view-btn").hasClass('active')){
            ajaxPullReservations(date,'upcoming');
        }*/
    }else{
        displayWaitlistReservation(floor_id, tab_target, date, keyword);
        var date=$('.floor_fetch #datepicker').val();
        // ajaxPullReservations(date,'waitlist');
         if($("#floor-view-btn").hasClass('active')){
            ajaxPullReservations(date,'current', timer_type);
        }
        /*if($("#grid-view-btn").hasClass('active')){
            ajaxPullReservations(date,'waitlist');
        }*/


    }
}

function displayCurrentReservation(floor_id, tab_target, date, keyword) {
    var current_reservation_html='<div class="text-center fullWidth">No Record Found</div>';
    var total_cover=0;
    $.ajax({
        url: '/floor/'+floor_id+'/reservations',
        data: 'type='+tab_target+'&date='+date+'&keyword='+keyword,
        dataType: 'json',
        success: function (data) {

            if(data.length!==undefined && data.length>0){
                current_reservation_html='';
            }
            $.each(data, function (key, value) {
                total_cover=parseInt(total_cover)+parseInt(value.reserved_seat);
                var timeclass='';
                if((value.is_no_show && value.wait_for==0) || value.is_about_to_finish) {
                    timeclass='reserved__timer__late';
                }
                current_reservation_html += '<div class="reservationStatusPanel__user seated__user '+timeclass+'" reservation-id="'+value.id+'" reservation-status-slug="'+value.status_slug+'">';
                current_reservation_html += '<div class="reservationStatusPanel__user--details">';
                current_reservation_html += '<div class="reservationStatusPanel-user__name" data-toggle="modal" data-target="#tableManagement__popup">';
                current_reservation_html += '<div><span class="'+value.status_icon.sidebar+'" aria-hidden="true" style="color: '+value.status_color.sidebar+'"></span>';
                current_reservation_html += '<h3 onclick="openReservationDetailPopUp('+value.id+')">'+value.name+'</h3></div>';
                current_reservation_html += '<div class="special--occasion">';
                if(value.special_occasion.icon){
                    current_reservation_html += '<span class="'+value.special_occasion.icon+'" title="'+value.special_occasion.name+'" aria-hidden="true"></span>';
                }
                if(value.user_type.icon){
                    current_reservation_html += '<span class="'+value.user_type.icon+'" title="'+value.user_type.name+'" aria-hidden="true"></span>';
                }
                current_reservation_html+='</div>';
                current_reservation_html += '</div><div class="reservationStatusPanel-user__size margin-left-40"><div class="user__time">';
                current_reservation_html += '<span>'+value.start_time+'</span></div><span class="dash">-</span>';
                current_reservation_html += '<div class="numberOfPeople"><span>'+value.reserved_seat+'</span> People</div><span class="dash">-</span>';
                current_reservation_html += '<div class="tableId text-ellipsis">'+value.table_name+'</div></div>';
                current_reservation_html += '<select id="status_current" name="status" class="sidbar_status dropdown-menu dropdown__text_ul selectpicker show-tick" required="" data-style="margin-left-40 status-'+value.status_slug+'">';
                current_reservation_html += '<option value="" selected="selected" style="background-color:'+value.status_color.sidebar+'" data-icon="'+value.status_icon.sidebar+'">'+value.status_name+'</option>';
                $.each(value.next_status, function (status_key, status_val) {
                    current_reservation_html+='<option value="'+status_val.id+'" style="background-color: '+status_val.color+'" data-icon="'+status_val.icon+'" >'+status_val.name+'</option>';
                });
                current_reservation_html +='</select>';
                if(value.is_no_show && value.wait_for==0){
                    var waitid='wait_'+value.id;
                    current_reservation_html +='<div class="reserved__timer wait_'+value.id+'" ><div class="reserved__timer_header">The Guest is late by: <span class="timer_minute">'+value.late_minutes+' min</span></div><div class="timer_cta"><a href="javascript:void(0)" class="timer_noShow" onclick=markAsNoShow('+value.id+',\"'+value.status_slug+'\",$(this))><span class=" noShow__icon" aria-hidden="true"><img src="/images/cross_icon.png"></span><span class="cta__content">Mark as<br />No Show</span></a><a href="javascript:void(0)" class="timer_wait" onclick="wait('+'\''+waitid+'\''+','+value.id+',$(this))"><span class="wait__icon" aria-hidden="true"><img src="/images/clock_icon.png"></span><span class="cta__content">Wait another<br />5 min</span></a></div></div>';
                } else if(value.is_about_to_finish) {
                    var waitid='wait_'+value.id;
                    var resv_message = 'Reservation is about to finish';
                    if(value.time_left < 0) {
                        value.time_left = -value.time_left;
                        resv_message = 'Reservation end time exceeded by'
                    }
                    current_reservation_html +='<div class="show-timer-container reserved__timer wait_'+value.id+'" ><div class="reserved__timer_header">'+resv_message+': <span class="timer_minute">'+value.time_left+' min</span></div><div class="timer_cta"><a href="javascript:void(0)" class="timer_noShow" onclick="changeReservationStatus('+value.id+','+value.finish_status+',\'current\',null,null,'+floor_id+','+value.table_id+')"><span class="noShow__icon icon-cancelled" aria-hidden="true"></span><span class="cta__content">Mark as<br />Finished</span></a><a href="javascript:void(0)" class="timer_wait" onclick="extendTillNextSlot('+'\''+waitid+'\''+','+value.id+',$(this))"><span class="wait__icon icon-anti_clockwisetimer" aria-hidden="true"></span><span class="cta__content">Extend till<br />Next slot</span></a></div></div>';
                }
                current_reservation_html +='</div></div>';
            });

            $('.total_reservation').html(data.length+' Reservations');
            $('.total_cover').html(total_cover+' Covers');
            $('.side-section .total_cover_current').html(total_cover);
            $('.current_div').empty().html("<div class='scroller'>"+current_reservation_html+"</div>");
            $(".selectpicker").selectpicker('refresh');
            $(".scroller").mCustomScrollbar(options);
        },
        error: function (data, textStatus, errorThrown) {
            $('.current_div').empty().html("<div class='scroller text-center'>"+current_reservation_html+"</div>");
        }
    });
}

function displayUpcomingReservation(floor_id, tab_target, date, keyword) {
    var upcoming_reservation_html='<div class="text-center fullWidth">No Record Found</div>';
    var total_cover=0;
    $.ajax({
        url: '/floor/'+floor_id+'/reservations',
        data: 'type='+tab_target+'&date='+date+'&keyword='+keyword,
        dataType: 'json',
        success: function (data) {
            if(data.length!==undefined && data.length>0){
                upcoming_reservation_html='';
            }
            $.each(data, function (key, value) { 
                total_cover=parseInt(total_cover)+parseInt(value.reserved_seat);
                upcoming_reservation_html+='<div class="reservationStatusPanel__user reserved__user" reservation-id="'+value.id+'"><div class="reservationStatusPanel__user--details"><div class="reservationStatusPanel-user__name" data-toggle="modal" data-target="#tableManagement__popup"><div><span class="'+value.status_icon.sidebar+'" aria-hidden="true" style="color: '+value.status_color.sidebar+'"></span><h3 onclick="openReservationDetailPopUp('+value.id+')">'+value.name+'</h3></div><div class="special--occasion">';
                if(value.special_occasion.icon){
                    upcoming_reservation_html += '<span class="'+value.special_occasion.icon+'" title="'+value.special_occasion.name+'" aria-hidden="true"></span>';
                }
                if(value.user_type.icon){
                    upcoming_reservation_html += '<span class="'+value.user_type.icon+'" title="'+value.user_type.name+'" aria-hidden="true"></span>';
                }
                upcoming_reservation_html+='</div></div><div class="reservationStatusPanel-user__size margin-left-40"><div class="user__time"><span>'+value.start_time+'</span></div><span class="dash">-</span><div class="numberOfPeople"><span>'+value.reserved_seat+'</span> People</div><span class="dash">-</span><div class="tableId text-ellipsis">'+value.table_name+'</div></div>';
                upcoming_reservation_html += '<select id="status_current" name="status" class="sidbar_status dropdown-menu dropdown__text_ul selectpicker" required="" data-style="margin-left-40 status-'+value.status_slug+'">';

                upcoming_reservation_html += '<option value="" selected="selected" style="background-color:'+value.status_color.sidebar+'" data-icon="'+value.status_icon.sidebar+'">'+value.status_name+'</option>';
                $.each(value.next_status, function (status_key, status_val) {
                    upcoming_reservation_html+='<option value="'+status_val.id+'" style="background-color: '+status_val.color+'" data-icon="'+status_val.icon+'" >'+status_val.name+'</option>';
                });
                upcoming_reservation_html +='</select></div></div>';
            });
            $('.total_reservation').html(data.length+' Reservations');
            $('.total_cover').html(total_cover+' Covers');
            $('.side-section .total_cover_current').html(total_cover);
            $('.upcoming_div').empty().html("<div class='scroller'>"+upcoming_reservation_html+"</div>");
            //setTimeout(function(){
                $(".selectpicker").selectpicker('refresh');
                $(".scroller").mCustomScrollbar(options);
            //},10);
        },
        error: function (data, textStatus, errorThrown) {
            $('.upcoming_div').empty().html("<div class='scroller'>"+upcoming_reservation_html+"</div>");
        }
    });
}

function displayWaitlistReservation(floor_id, tab_target, date, keyword) {
    var waiting_reservation_html='<div class="text-center fullWidth">No Record Found</div>';
    var total_cover = 0;
    $.ajax({
        url: '/floor/' + floor_id + '/reservations',
        data: 'type='+tab_target+'&date='+date+'&keyword='+keyword,
        dataType: 'json',
        success: function (data) {
            if(data.length!==undefined && data.length>0){
                waiting_reservation_html='';
            }
            $.each(data, function (key, value) {
                total_cover = parseInt(total_cover) + parseInt(value.reserved_seat);
                //console.log(value.name + ' - ' + value.source);
                if(value.name.trim() == '' && value.source == 'Walk In') {
                    value.name = 'Walk in';
                }
                waiting_reservation_html += '<div class="waitlist_div"><div class="reservationStatusPanel__user waiting__user" reservation-id="'+value.id+'"><div class="reservationStatusPanel__user--details"><div class="reservationStatusPanel-user__name" data-toggle="modal" data-target="#tableManagement__popup"><div><span class="'+value.status_icon.sidebar+'" aria-hidden="true" style="color: '+value.status_color.sidebar+'"></span><h3 class="waitlist_details">' + value.name + '</h3></div><div class="special--occasion">';
                if(value.special_occasion.icon){
                    waiting_reservation_html += '<span class="'+value.special_occasion.icon+'" title="'+value.special_occasion.name+'" aria-hidden="true"></span>';
                }
                if(value.user_type.icon){
                    waiting_reservation_html += '<span class="'+value.user_type.icon+'" title="'+value.user_type.name+'" aria-hidden="true"></span>';
                }
                //value.waiting_time
                waiting_reservation_html+='</div></div><div class="reservationStatusPanel-user__size margin-left-40"><div class="user__time"><span></span></div><div class="numberOfPeople"><span>' + value.reserved_seat + '</span> People</div></div>';
                waiting_reservation_html += '<div class="margin-left-40"><select id="status_current" name="status" class="sidbar_status dropdown-menu dropdown__text_ul selectpicker" required="" data-width="170" data-style="status-'+value.status_slug+'">';

                waiting_reservation_html += '<option value="" selected="selected" style="background-color:'+value.status_color.sidebar+'" data-icon="'+value.status_icon.sidebar+'">'+value.status_name+'</option>';
                $.each(value.next_status, function (status_key, status_val) {
                    waiting_reservation_html+='<option value="'+status_val.id+'" style="background-color: '+status_val.color+'" data-icon="'+status_val.icon+'" >'+status_val.name+'</option>';
                });
                waiting_reservation_html += '</select></div></div></div><div class="reservationStatusPanel__user--mode"><div class="reservation__type online">' + value.source + '</div></div></div>';
            });
            $('.total_reservation').html(data.length + ' Reservations');
            $('.total_cover').html(total_cover + ' Covers');

            $('#waitlist').empty().html("<div class='scroller'>"+waiting_reservation_html+"</div>");

            //setTimeout(function () {
                $(".selectpicker").selectpicker('refresh');
                $(".scroller").mCustomScrollbar(options);
            //}, 10);
        },
        error: function (data, textStatus, errorThrown) {
            $('#waitlist').empty().html("<div class='scroller'>"+waiting_reservation_html+"</div>");
        }
    });

}

$('.right-searchbar #keyword').on('change keyup paste',function(){
    displayRightSideReservation(tab_target);
});

/********************** NEW WAITLIST - START ************************/
// NEW WALK IN BUTTON CLICK
$('body').on('click', '#new_walkin_button', function (){
    openSibebarController(true)
    $("#blockTables,#book-a-table,#reservation_info_popup").modal('hide');
    $("#block_a_table,#bookATable").removeAttr("data-popup")

    $('#newWalkinModal #table_id').val(null);
    $('#newWalkinModal #user_id').val(null);
    $('#newWalkinModal #floor_id').val(null);
    $('#newWalkinModal #party_size').val(2);
    $('#newWalkinModal .message_div').empty().addClass('hide');
    $(".seat_btn , .second__step-walkin").hide();
    $(".done_btn , .third__step-walkin").hide();
    $(".check__availability, .first__step-walkin").show();
    $('.third__step-walkin').find('input').val('');
    $('#third_step_walkin #register_customer').prop('checked', false);
    $('#third_step_walkin input').nextAll().removeClass('hasVal');
});
$('body').on('click', '.new_walkin_button', function (){

    if($(this).attr('table-id') && $(this).attr('floor-id')){

        $('#newWalkinModal #table_id').val($(this).attr('table-id'));
        $('#newWalkinModal #floor_id').val($(this).attr('floor-id'));
    }
    $('#newWalkinModal #party_size').val(2);
    $(".seat_btn , .second__step-walkin").hide();
    $(".done_btn , .third__step-walkin").hide();
    $(".check__availability, .first__step-walkin").show();
    $('.third__step-walkin').find('input').val('');
});

var walk_in_floor_data;
var floor_id;
var table_id;
var floorFound = false;
var yield_floor_id = '';
var yield_table_id = '';


function grid_view_func(data) {

    $("#grid-calendar").fullCalendar('destroy');

    var scrollTime = moment.tz(timeZoneType).format('HH');
    scrollTime = (scrollTime-1)+":30"

    //console.log(data[2])

    $('#grid-calendar').fullCalendar({
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        header: false,
        defaultView: 'timelineDay',
        resourceGroupField: 'building',
        selectable: false,
        defaultTimedEventDuration: '00:15:00',
        minTime: "00:00:00",
        maxTime: "24:00:00",
        now: nowTimeShow,
        nowIndicator: true,
        //timezone: timeZoneType,
        disableDragging: true,
        slotMinutes: 15,
        slotWidth:100,
        slotDuration: '00:15:00',
        slotLabelFormat: ['HH:mm'],
        scrollTime: scrollTime,
        resourceAreaWidth: '80',
        //editable: true,
        //selectable: true,
        /*select: function (start, end, jsEvent, view, resource) {
            $("#book-a-table").modal('show');
            $("#book-a-table").find("#reservation_date").selectpicker('val', floorName)
        },*/
        eventStartEditable: false,
        //height: 'parent',
        contentHeight: 'auto',
        //eventDurationEditable: true,
        allDaySlot: false,
        resourceColumns: [
            {
                labelText: 'Time   Covers',
                field: 'title'
            }

        ],
        //resources: data[1],
        resources: data[1],
        //events: dd, //data[2],
        events: data[2],
        eventRender: function(event,element) {
            element.find('.fc-title').html(event.title);

            if((element[0].firstChild.firstChild.firstChild.className == "grid-view__slotAvailable odd past_time") && (moment(event.start._i).format("HH:mm") != "23:45")){
                element.addClass('past_time')
                element.attr('readonly',true)
            }

            if((element[0].firstChild.firstChild.firstChild.className == "grid-view__slotAvailable even past_time") && (moment(event.start._i).format("HH:mm") != "23:45")){
                element.addClass('past_time')
                element.attr('readonly',true)
            }

        },
        eventClick: function (event, jsEvent, view) {
            console.log(event);

            $("#book-a-table #reservation-booking label.error.message_div").hide()

            if(event.resv_id){
                openReservationDetailPopUp(event.resv_id);
            }else{
                var dateTypeVar = $('.tableManagement__wrapper #datepicker').datepicker('getDate');
                var date = $.datepicker.formatDate('mm-dd-yy', dateTypeVar);
                floor_id = parseInt(floor_url.split("/floor/")[1]);
                table_id = event.resourceId;
                $("#book-a-table").modal('show');
                $("#loader").removeClass('hidden')

                setTimeout(function(){
                    $("#loader").addClass('hidden')
                    $("#book-a-table #reservation_date").val(date);
                    $("#book-a-table #book-a-table-tp").val(moment(event.start).format('hh:mm A'));
                },300)

            }
        }
    });

    $('#grid-calendar').fullCalendar('addResource',data[1]);
    $('#grid-calendar').fullCalendar( 'refetchResources');
    var dateTypeVar = $('.tableManagement__wrapper #datepicker').datepicker('getDate');
    var gridDate =  $.datepicker.formatDate('yy-mm-dd', dateTypeVar);
    $('#grid-calendar').fullCalendar( 'gotoDate', gridDate );

    var th
    for(var i = 0; i < data[3].length; i++){
        th += "<th>"+data[3][i]+"</th>"
    }
    $("#grid-view .fc-head .fc-time-area.fc-widget-header table tbody").append("<tr class='reservationCountCol'>"+ th +"</tr>");
}

// WALK IN - CHECK AVAILABILITY
$('.availability').on('click', function () {
    $('.message_div').empty();
    $('.register_customer').show();
    $('#register_customer').prop('checked', false);
    $('.alert-danger, .alert-success').hide();
    var party_size = $('#party_size').val();
    $('#loader').removeClass('hidden');
    $('#newWalkinModal #table_plan_walkin').children('option:not(:first)').remove();
    $(".selectpicker").selectpicker('refresh');
    //var reservation_date = moment().format('mm-dd-yy');
    //var reservation_time = moment().format('H:m');
    var slotHtml = '';
    var request_data= {
            party_size: party_size,
            walk_in: 1,
            // reservation_date: reservation_date,
            // reservation_time: reservation_time,
        };

        var table_id= $('#newWalkinModal #table_id').val();
        if(table_id!=null){
         request_data.table_id=table_id;
        }
        var floor_id=$('#newWalkinModal #floor_id').val();
        if(floor_id!=null){
         request_data.floor_id=floor_id;
        }
    $('#newWalkinModal #b_warning_message').html('');
    $.ajax({
        type: 'POST',
        url: '/reservation/check-availability',
        data:request_data,
        dataType: 'json',
        success: function (data) {
            //console.log(data);

            if(typeof data.data !== 'undefined') {
                if(typeof data.data.floor_table_info !== 'undefined') {
                    walk_in_floor_data = data.data.floor_table_info;
                    var floorHtml = '<option value="">Select Floor</option>';
                    var current_floor = parseInt(floor_url.split("/floor/")[1]);
                    var temp_floor = current_floor;
                    var i = 0;
                    $.each(walk_in_floor_data, function (key, floor) {
                        floorHtml += '<option value="'+floor.id+'">'+floor.name+'</option>';
                        $.each(floor.tables, function(table_key, table_value) {
                            if(party_size == table_value.max && i==0) {
                                temp_floor = floor.id;
                                i++;
                            } else if(parseInt(floor.id) == current_floor && party_size == table_value.max) {
                                floorFound = true;
                            }
                        });

                    });
                    $('#floor_plan_walkin').html(floorHtml);
                    if(floorFound){
                        $("#floor_plan_walkin").val(floor_url.split("/floor/")[1]);
                    } else {
                        $("#floor_plan_walkin").val(temp_floor);
                    }
                    $("#floor_plan_walkin").trigger('change');
                    $(".selectpicker").selectpicker('refresh');
                    $(".first__step-walkin , .check__availability").hide();
                    $(".seat_btn , .second__step-walkin").show();
                    if(data.message!==null && data.message!==undefined){
                        $('.message_div').empty().html(data.message).show();
                    }
                }
            } else {
                $('.alert-success').show();
                $('.alert-success').html(data.errors);
            }
            /*$('.check-avilable').html(slotHtml).removeClass('hide');
            $('.reserve-table__btn.check').hide();
            $('.reserve-table__container').show();*/
        },
        complete: function() {
            $('#loader').addClass('hidden');
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
            var err = '';
            var err_flag = false;
            if(typeof data !== 'undefined') {
                if($.isArray(data.errors)) {
                    console.log('array');
                    $.each(data.responseJSON.errors, function (key, value) {
                        err += '<p>' + value + '</p>';
                        if(value == 'Restaurant is not operational currently. This could be because the timeslot was not defined.') {
                            err_flag = true;
                        }
                    });
                } else {
                    err = '<p>' + data.responseJSON.errors + '</p>';
                    if(data.responseJSON.errors == 'Turnover time of selected party size is greater than the remaining shift time.') {
                        err_flag = true;
                    }
                }

                if(err_flag) {
                    $('#newWalkinModal .message_div').empty().removeClass('hide').html(err).show();
                } else {
                    floorFound = false;
                    $('#newWalkinModal').modal('hide');
                    $('#newWaitlistModal form')[0].reset();
                    $('#newWaitlistModal .party_size').val(party_size);
                    $('#newWaitlistModal #fname').next('label.error').remove();
                    $('#newWaitlistModal #email').next('label.error').remove();
                    $('#newWaitlistModal #phone').next('label.error').remove();
                    $('#newWaitlistModal').modal('show');
                    openSibebarController(true)
                    $('#waitlist_no_tables_form input').next().removeClass('hasVal');
                }
            }
        }
    });
});

// NEW WALKIN - POPULATE TABLE ON FLOOR SELECTION
$('#manageWaitlistModal,#newWalkinModal').on('change', '#floor_plan_walkin', function() {
    var nearest_form = $(this).closest('form').attr('id');
    var floor_id = $(this).val();
    var selectedTable;
    var party_size = $('#newWalkinModal #party_size').val();
    var tableHtml = '<option value="">Select Table</option>';
    if(floor_id === '') {
        if(nearest_form == 'waitlist_popup') {
            $('#waitlist_popup #table_plan_walkin').html(tableHtml);
        } else if(nearest_form == 'walkin_floor_table_form') {
            $('#walkin_floor_table_form #table_plan_walkin').html(tableHtml);
        }
        $(".selectpicker").selectpicker('refresh');
    } else {
        $.each(walk_in_floor_data, function(key, floor){
            if(floor_id == floor.id) {
                if(floor.tables) {

                    var i=0;
                    $('#newWalkinModal #floor_plan_walkin').next('label.error').remove();
                    $('#newWalkinModal #table_plan_walkin').next('label.error').remove();
                    $.each(floor.tables, function(table_key, table_value) {
                        if(floorFound && table_value.max == party_size){
                            selectedTable = table_value.id;
                        } else if(table_value.max == party_size) {
                            selectedTable = table_value.id;
                        }
                        tableHtml += '<option value="'+table_value.id+'">'+table_value.name+'</option>';
                        i++;
                    });
                    if(nearest_form == 'waitlist_popup') {
                        $('#waitlist_popup #table_plan_walkin').html(tableHtml);
                    } else if(nearest_form == 'walkin_floor_table_form') {
                        $('#walkin_floor_table_form #table_plan_walkin').html(tableHtml);
                        if(!isNaN(selectedTable)){
                            console.log(selectedTable);
                            $('#walkin_floor_table_form #table_plan_walkin').val(selectedTable);
                        }
                    }
                    $(".selectpicker").selectpicker('refresh');
                }
            }
        });
    }
});

$('#manageWaitlistModal,#newWalkinModal').on('change', '#table_plan_walkin', function () {
    var selected_tables = $(this).val();
    if ($('#manageWaitlistModal').is(':visible')) {
        var selector_modal  = 'manageWaitlistModal';
    } else {
        var selector_modal  = 'newWalkinModal';
    }
    if ($('#manageWaitlistModal').is(':visible')) {
        var selector_modal  = 'manageWaitlistModal';
        var party_size      = $('#'+selector_modal+' #waitlist_party_size').val();
    } else {
        var selector_modal  = 'newWalkinModal';
        var party_size      = $('#'+selector_modal+' #party_size').val();
    }
    var selected_floor  = $('#'+selector_modal+' #floor_plan_walkin').val();
    selected_floor      = parseInt(selected_floor);
    party_size          = parseInt(party_size);
    //console.log(walk_in_floor_data); console.log(selected_floor); console.log(party_size); console.log(selected_tables);
    var min_seat = max_seat = 0;
    if(!isEmpty(walk_in_floor_data)) {
        //console.log(walk_in_floor_data);
        var floor_ids   = array_column(walk_in_floor_data, 'id');
        var floor_index = $.inArray(selected_floor, floor_ids);
        //console.log(floor_ids); console.log(floor_index);
        if(floor_index > -1) {
            var floor_details = walk_in_floor_data.slice(floor_index, floor_index+1);
            var table_details = floor_details[0].tables;
            var table_ids     = array_column(table_details, 'id');
            //console.log(table_ids);
            $.each(selected_tables, function(key,tab) {
                var table_index = $.inArray(parseInt(tab), table_ids);
                if(table_index > -1) {
                    var temp_table = table_details.slice(table_index, table_index+1);
                    min_seat += temp_table[0].min;
                    max_seat += temp_table[0].max;
                }
            });
        }
    }
    if(party_size > max_seat) {
        $('#'+selector_modal+' #b_warning_message').html('Table capacity is lesser than the party size.');
    } else if(party_size < min_seat) {
        $('#'+selector_modal+' #b_warning_message').html('Table capacity is greater than the party size.');
    }
    console.log(min_seat + ' - ' + max_seat + ' - ' + party_size);
});

// JAVASCRIPT array_column
function array_column(arr, col) {
    return arr.map(function(value, index) {
        return value[col];
    });
}

// NEW WALKIN - SMS BUTTON TOGGLE
$('#sms_notification_button').on('click',function(){
    if($(this).attr('aria-pressed') == 'true') {
        $('#sms_notification').val(0);
    } else{
        $('#sms_notification').val(1);
    }
});

$('.notificationcontainer .toggle__container button').on('click',function(){

    var val=$(this).next('input').val();
     val==0? $(this).next('input').val(1):  $(this).next('input').val(0);
});
// NEW WAITLIST - NO TABLES AVAILABLE
$('#waitlist_no_tables_form .waitlist_no_tables').on('click', function() {
    var is_checked = $('#newWaitlistModal  #register_customer').is(':checked');
    var form = $('#waitlist_no_tables_form');
    if(!form.valid()){
        console.log('form not valid');
        return false;
    } else {
        console.log('form valid');
        // clear validation msgs if any
        $('#newWaitlistModal #fname').next('label.error').remove();
        $('#newWaitlistModal #email').next('label.error').remove();
        $('#newWaitlistModal #phone').next('label.error').remove();
    }
    var formdata = $('#waitlist_no_tables_form').serialize();
    //console.log(formdata);
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'POST',
        url: '/reservation/wait-list-add', // new ajax call just to save
        data: formdata,
        dataType: 'json',
        success: function (data) {
            if(data.data==1) {
                openSibebarController(false)
                $('#newWaitlistModal').modal('hide');
                $('#reservations a[href="#waitlist"]').trigger('click');
                alertbox('Success',data.message, function(modal){
                    setTimeout(function() {
                        modal.modal('hide');
                    }, 2000);
                });
            } else {
                $('.alert-success').show();
                $('.alert-success').html(data.errors);
            }
        },
        complete: function() {
            $('#loader').addClass('hidden');
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
            $('#newWalkinModal').modal('hide');
            $('#newWaitlistModal').modal('show');
            openSibebarController(true)
            $('#waitlist_no_tables_form input').next().removeClass('hasVal');
        }
    });
});

$('#newWalkinModal .done_btn').on('click', function () {
    var is_checked = $('#newWalkinModal  #register_customer').is(':checked');
    //console.log(is_checked);

    var form = $('#third_step_walkin');
    if(is_checked || !form.valid()){
        return false; 
    }

    removeValidation("#third_step_walkin")

    // clear validation msgs if any
    //$('#newWalkinModal #fname').next('label.error').remove();
    //$('#newWalkinModal #email').next('label.error').remove();
    //$('#newWalkinModal #phone').next('label.error').remove();
    $(".done_btn , .third__step-walkin").hide();
    $(".seat_btn , .second__step-walkin").show();

});

$('#newWalkinModal .seat_btn').on('click', function() {
    if(!$("#walkin_floor_table_form").valid()){
        return false;
    }
    //console.log('seat btn');
    var first_name  = $('#third_step_walkin #fname').val();
    var last_name   = $('#third_step_walkin #lname').val();
    var number      = $('#third_step_walkin #phone').val();
    var email       = $('#third_step_walkin #email').val();
    var user_id     = $('#third_step_walkin #userid').val();
    var floor_id    = $('#floor_plan_walkin').val();
    var table_id    = $('#table_plan_walkin').val();
    var party_size  = $('#party_size').val();
    if($('#third_step_walkin #register_customer').is(":checked")) {
        var register_customer = 1;
    } else {
        var register_customer = 0;
    }
    //console.log(first_name + last_name + number + email);
    //console.log(floor_id + table_id + party_size);
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'POST',
        url: '/reservation/book-table',
        data: {
            first_name          : first_name,
            last_name           : last_name,
            number              : number,
            email               : email,
            userid              : user_id,
            floor_id            : floor_id,
            table_id            : table_id,
            party_size          : party_size,
            register_customer   : register_customer,
            walk_in             : 1
        },
        dataType: 'json',
        success: function (data) {
            if(typeof data.data !== 'undefined') {
                $('#newWalkinModal').modal('hide');
                openSibebarController(false)
                $('#reservations a[href="#current"]').trigger('click');
                if (window.localStorage.getItem("gridActiveTab") === null) {
                    floorGridViewUpdate();
                }else{
                    if ($('#grid-view').length === 1) {
                        getUpdateGridView();
                    }
                }
                window.localStorage.removeItem("reservation_going_on");
                alertbox('Success',data.message, function(modal){
                    setTimeout(function() {
                        modal.modal('hide');
                    }, 2000);
                });
            } else {
                $('.alert-success').show();
                $('.alert-success').html(data.errors);
            }

        },
        complete: function() {
            $('#loader').addClass('hidden');
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
        }
    });
});

$('#waitlist').on('click', 'h3.waitlist_details', function() {
    var resr_id = $(this).parents('.waiting__user').attr('reservation-id');
    openWaitlistPopUp(resr_id);
});

$(document).on('click', '.resv_detail_waitlist', function () {
    var resv_id = this.id.split('_');
    $('#manageWaitlistModal').show();
    openWaitlistPopUp(resv_id[1]);
});

function openWaitlistPopUp(resr_id)
{
    removeValidation("#waitlist_popup");

    $('.message_div').empty();
    $('#b_warning_message').empty();
    $('#manageWaitlistModal').modal('show');
    $('#manageWaitlistModal #fname').attr('disabled', 'disabled');
    $('#manageWaitlistModal #lname').attr('disabled', 'disabled');
    $('#manageWaitlistModal #email').attr('disabled', 'disabled');
    $('#manageWaitlistModal #phone').attr('disabled', 'disabled');
    $('#manageWaitlistModal #instruction').attr('disabled', 'disabled');
    $('#manageWaitlistModal #floor_plan_walkin').next('label.error').remove();
    $('#manageWaitlistModal #table_plan_walkin').next('label.error').remove();
    $('#manageWaitlistModal #waitlist_party_size').prop('disabled',true);
    $('#manageWaitlistModal #waitlist_wait_time').prop('disabled',true);
    $("#manageWaitlistModal .input-group-btn").addClass('hide');
    $('#manageWaitlistModal .update_guest_details-third').addClass('edit-input');
    $('#manageWaitlistModal .cancel_edit_guest_details-third').addClass('edit-input');
    $('#manageWaitlistModal .edit_guest_details').removeClass('edit-input');
    $('#manageWaitlistModal .register_customer').show();
    // fetch waiting list details
    //console.log('waiting list details');
    //$('#manageWaitlistModal #table_plan_walkin').prop("selectedIndex", 0);
    $(".selectpicker").selectpicker('refresh');
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'GET',
        url: '/reservation/detail/'+resr_id+'?walk_in=1',
        dataType: 'json',
        success: function (data) {
            //console.log(data);
            if(typeof data.reservation_detail !== undefined) {
                $('#manageWaitlistModal #reservation_id').val(data.reservation_detail.id);
                $('#manageWaitlistModal #userid').val(data.reservation_detail.user_id);
                if(data.reservation_detail.user_id!==null){
                    $('#manageWaitlistModal .register_customer').hide();
                }

                $('#manageWaitlistModal #fname').val(data.reservation_detail.fname).attr('disabled','disabled').next('span').addClass("hasVal");
                $('#manageWaitlistModal #lname').val(data.reservation_detail.lname).attr('disabled','disabled').next('span').addClass("hasVal");
                $('#manageWaitlistModal #email').val(data.reservation_detail.email).attr('disabled','disabled').next('span').addClass("hasVal");
                $('#manageWaitlistModal #phone').val(data.reservation_detail.mobile).attr('disabled','disabled').next('span').addClass("hasVal");
                $('#manageWaitlistModal #floor').val(data.reservation_detail.floor_id);
                $('#manageWaitlistModal #table').val(data.reservation_detail.table_id);
                $('#manageWaitlistModal #waitlist_party_size').val(data.reservation_detail.reserved_seat).attr('disabled','disabled');
                $('#manageWaitlistModal #waitlist_wait_time').val(data.reservation_detail.wait_time).attr('disabled','disabled');
                if(data.reservation_detail.notify_me == 1) {
                    $('#manageWaitlistModal #sms_notification_button').attr("aria-pressed", true).addClass('active');
                    $('#manageWaitlistModal #sms_notification').val(1)
                } else {
                    $('#manageWaitlistModal #sms_notification_button').attr("aria-pressed", false).removeClass('active');
                    $('#manageWaitlistModal #sms_notification').val(0)
                }
                $('#manageWaitlistModal #instruction').val(data.reservation_detail.user_instruction).attr('disabled','disabled');

                if($("#manageWaitlistModal #fname, #manageWaitlistModal #lname, #manageWaitlistModal #email, #manageWaitlistModal #phone").val() != ""){
                    $("#manageWaitlistModal input + span, #manageWaitlistModal input + label.error + span").addClass('hasVal')
                }

                // '<select id="status_current" name="status" class="sidbar_status dropdown-menu dropdown__text_ul selectpicker" required="" data-style="status-'+value.status_slug+'">';
                var waiting_reservation_html = '<option value="" selected="selected" style="background-color:'+data.reservation_detail.status_color.sidebar+'" data-icon="'+data.reservation_detail.status_icon.sidebar+'">'+data.reservation_detail.status_name+'</option>';
                $.each(data.reservation_detail.next_status, function (status_key, status_val) {
                    waiting_reservation_html+='<option value="'+status_val.id+'" style="background-color: '+status_val.color+'" data-icon="'+status_val.icon+'" >'+status_val.name+'</option>';
                });
                $('#manageWaitlistModal #waitlist_status').html(waiting_reservation_html);
                $(".selectpicker").selectpicker('refresh');

                $('#loader').removeClass('hidden');
                // check availability
                $.ajax({
                    type: 'POST',
                    url: '/reservation/check-availability',
                    data: {
                        party_size: data.reservation_detail.reserved_seat,
                        walk_in: 1,
                        // reservation_date: reservation_date,
                        // reserva  tion_time: reservation_time,
                    },
                    dataType: 'json',
                    success: function (data) {
                        //console.log(data);
                        if(typeof data.data !== 'undefined') {
                            if(typeof data.data.floor_table_info !== 'undefined') {
                                walk_in_floor_data = data.data.floor_table_info;
                                var floorHtml = '<option value="">Select Floor</option>';
                                $.each(walk_in_floor_data, function (key, floor) {
                                    floorHtml += '<option value="'+floor.id+'">'+floor.name+'</option>';
                                });
                                $('#manageWaitlistModal #floor_plan_walkin').html(floorHtml);
                                $(".selectpicker").selectpicker('refresh');
                                if(data.message!==null && data.message!==undefined){
                                    $('.message_div').empty().html(data.message).show();
                                }
                            }
                        } else {
                            $('.alert-success').show();
                            $('.alert-success').html(data.errors);
                        }
                        $('#loader').addClass('hidden');
                    },
                    complete: function() {
                        $('#loader').addClass('hidden');
                    },
                    error: function (data, textStatus, errorThrown) {
                        $('#loader').addClass('hidden');
                    }
                });
            }
        },
        complete: function() {
            $('#loader').addClass('hidden');
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
        }
    });
}

// user concent - register user
$('#register_customer_button').on('click',function(){
    if($(this).attr('aria-pressed') == 'true') {
        $('#register_customer').val(0);
    } else{
        $('#register_customer').val(1);
    }
});
$('#manageWaitlistModal #waitlist_status').on('change', function() {
    var sel_status = $(this).val();
    // remove validation error messages
    $('#manageWaitlistModal #floor_plan_walkin').next('label.error').remove();
    $('#manageWaitlistModal #table_plan_walkin').next('label.error').remove();
    // get floor and table ids
    var floor_id = $('#manageWaitlistModal #floor_plan_walkin').val();
    var table_id = $('#manageWaitlistModal #table_plan_walkin').val();

    var reservation_id = $('#manageWaitlistModal #reservation_id').val();
    var status='current';
    if($(".status_tab.active").hasClass('upcoming')){
        status='upcoming';
    }if($(".status_tab.active").hasClass('current')){
        status='current';
    }if($(".status_tab.active").hasClass('waitlist')){
        status='waitlist';
    }
    var oldValue = '';
    if(this.value) {
        if($('#manageWaitlistModal #waitlist_status option:selected').text().trim() == 'Cancelled'){
            openCancellationPopUp(reservation_id, this.value, status);
            return false;
        }else if(this.value == 7) {
            if(floor_id && table_id) {
                changeReservationStatus(reservation_id, this.value, status, null, null, floor_id, table_id);
            } else {
                var floor_error_html = '<label for="floor_plan_walkin" generated="true" class="error">Floor should be selected</label>';
                var table_error_html = '<label for="table_plan_walkin" generated="true" class="error">Table should be selected</label>';
                $('#manageWaitlistModal #floor_plan_walkin').parents('.btn-group').append(floor_error_html);
                $('#manageWaitlistModal #table_plan_walkin').parents('.btn-group').append(table_error_html);
                $('#manageWaitlistModal #waitlist_status').prop("selectedIndex", 0).selectpicker('refresh');
            }
        }
    }
    // change button status
    // app mail notify.php
});

$('#waitlist_popup .cancel_edit_guest_details-third').on('click', function() {
    //console.log('cancel done');
    var resr_id = $('#manageWaitlistModal #reservation_id').val();
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'GET',
        url: '/reservation/detail/'+resr_id+'?walk_in=1',
        dataType: 'json',
        success: function (data) {
            //console.log(data);
            if(typeof data.reservation_detail !== undefined) {
                $('#manageWaitlistModal #reservation_id').val(data.reservation_detail.id);
                $('#manageWaitlistModal #user_id').val(data.reservation_detail.user_id);
                $('#manageWaitlistModal #fname').val(data.reservation_detail.fname).attr('disabled','disabled').next('span').addClass("hasVal");
                $('#manageWaitlistModal #lname').val(data.reservation_detail.lname).attr('disabled','disabled').next('span').addClass("hasVal");
                $('#manageWaitlistModal #email').val(data.reservation_detail.email).attr('disabled','disabled').next('span').addClass("hasVal");
                $('#manageWaitlistModal #phone').val(data.reservation_detail.mobile).attr('disabled','disabled').next('span').addClass("hasVal");
                $('#manageWaitlistModal #floor').val(data.reservation_detail.floor_id);
                $('#manageWaitlistModal #table').val(data.reservation_detail.table_id);
                $('#manageWaitlistModal #waitlist_party_size').val(data.reservation_detail.reserved_seat).attr('disabled','disabled');
                $('#manageWaitlistModal #waitlist_wait_time').val(data.reservation_detail.wait_time).attr('disabled','disabled');
                if(data.reservation_detail.notify_me == 1) {
                    $('#manageWaitlistModal #sms_notification_button').attr("aria-pressed", true).addClass('active');
                    $('#manageWaitlistModal #sms_notification').val(1)
                } else {
                    $('#manageWaitlistModal #sms_notification_button').attr("aria-pressed", false).removeClass('active');
                    $('#manageWaitlistModal #sms_notification').val(0)
                }
                $('#manageWaitlistModal #instruction').val(data.reservation_detail.user_instruction).attr('disabled','disabled');
                var waiting_reservation_html = '<option value="" selected="selected" style="background-color:'+data.reservation_detail.status_color.sidebar+'" data-icon="'+data.reservation_detail.status_icon.sidebar+'">'+data.reservation_detail.status_name+'</option>';
                $.each(data.reservation_detail.next_status, function (status_key, status_val) {
                    waiting_reservation_html+='<option value="'+status_val.id+'" style="background-color: '+status_val.color+'" data-icon="'+status_val.icon+'" >'+status_val.name+'</option>';
                });
                $('#manageWaitlistModal #waitlist_status').html(waiting_reservation_html);
                $(".selectpicker").selectpicker('refresh');
            }
        },
        complete: function() {
            $('#loader').addClass('hidden');
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
        }
    });
});

$('#waitlist_popup .update_guest_details-third').on('click', function() {
    if(!$("#waitlist_popup").valid()){
        return false;
    }
    var formData = $('#waitlist_popup').serialize();
    var reservation_id = $('#reservation_id').val();
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'PUT',
        url: '/reservation/wait-list-update/'+reservation_id,
        data: formData,
        dataType: 'json',
        success: function (data) {
            //console.log(data);
            if(typeof data.data !== 'undefined' && data.data == 1) {
                $('#manageWaitlistModal .update_guest_details-third').addClass('edit-input');
                $('#manageWaitlistModal .cancel_edit_guest_details-third').addClass('edit-input');
                $('#manageWaitlistModal .edit_guest_details').removeClass('edit-input');
                $('#manageWaitlistModal').modal('hide');
                $('#reservations a[href="#waitlist"]').trigger('click');
            } else {
                $('.alert-success').show();
                $('.alert-success').html(data.errors);
            }
        },
        complete: function() {
            $('#loader').addClass('hidden');
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
        }
    });
});

$('#customMessageModal #custom_sms_send, #manageWaitlistModal #template_sms_send').on('click', function() {
    var reservation_id = $('#reservation_id').val();
    if($(this).attr('id') == 'template_sms_send') {
        var custom_message = '';
    } else {
        var custom_message = $('#customMessageModal #custom_sms').val();
    }
    $('#loader').removeClass('hidden');
    $.ajax({
        type: 'POST',
        url: '/reservation/wait-list-sms',
        data: {
            custom_message: custom_message,
            reservation_id: reservation_id
        },
        dataType: 'json',
        success: function (data) {
            //console.log(data);
            if(typeof data.data !== 'undefined' && data.data == 1) {
                $('#customMessageModal').modal('hide');
                $('#manageWaitlistModal').modal('hide');
                $('#reservations a[href="#waitlist"]').trigger('click');
            } else {
                $('.alert-success').show();
                $('.alert-success').html(data.errors);
            }
            $('#loader').addClass('hidden');
        },
        complete: function() {
            $('#loader').addClass('hidden');
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
        }
    });

});


// ajax to save - DONE
// ajax to cancel - revert -
// ajax to send sms -

$('#manageWaitlistModal .edit_guest_details').on('click', function(){
    $('#manageWaitlistModal #fname').removeAttr('disabled');
    $('#manageWaitlistModal #lname').removeAttr('disabled');
    $('#manageWaitlistModal #email').removeAttr('disabled');
    $('#manageWaitlistModal #phone').removeAttr('disabled');
    $('#manageWaitlistModal #register_customer').removeAttr('disabled');
    $('#manageWaitlistModal #instruction').removeAttr('disabled');
    $('#manageWaitlistModal #waitlist_party_size').removeAttr('disabled');
    $('#manageWaitlistModal #waitlist_wait_time').removeAttr('disabled');
});

$('#manageWaitlistModal .cancel_edit_guest_details').on('click', function(){
    $('#manageWaitlistModal #fname').attr('disabled', 'disabled');
    $('#manageWaitlistModal #lname').attr('disabled', 'disabled');
    $('#manageWaitlistModal #email').attr('disabled', 'disabled');
    $('#manageWaitlistModal #phone').attr('disabled', 'disabled');
    $('#manageWaitlistModal #register_customer').attr('disabled', 'disabled');
    $('#manageWaitlistModal #instruction').attr('disabled', 'disabled');
    $('#manageWaitlistModal #waitlist_party_size').attr('disabled','disabled');
    $('#manageWaitlistModal #waitlist_wait_time').attr('disabled','disabled');
});

$('.waitlist-edit-note .edit_guest_details').on('click', function () {
    $('.waitlist-edit-note .update_guest_details-third').removeClass('edit-input');
    $('.waitlist-edit-note .cancel_edit_guest_details-third').removeClass('edit-input');
    $('.waitlist-edit-note .edit_guest_details').addClass('edit-input');
    $("#instruction").removeAttr('disabled');
});
$('.waitlist-edit-note .cancel_edit_guest_details-third').on('click', function () {
    $('.waitlist-edit-note .update_guest_details-third').addClass('edit-input');
    $('.waitlist-edit-note .cancel_edit_guest_details-third').addClass('edit-input');
    $('.waitlist-edit-note .edit_guest_details').removeClass('edit-input');
    $("#instruction").attr('disabled', 'disabled');
});
$('.waitlist-edit .edit_guest_details').on('click', function () {
    $('.waitlist-edit .update_guest_details-third').removeClass('edit-input');
    $('.waitlist-edit .cancel_edit_guest_details-third').removeClass('edit-input');
    $('.waitlist-edit .edit_guest_details').addClass('edit-input');
    $(".input-group-btn").removeClass('hide');
});
$('.waitlist-edit .cancel_edit_guest_details-third').on('click', function () {
    $('.waitlist-edit .update_guest_details-third').addClass('edit-input');
    $('.waitlist-edit .cancel_edit_guest_details-third').addClass('edit-input');
    $('.waitlist-edit .edit_guest_details').removeClass('edit-input');
    $(".input-group-btn").addClass('hide');
});


/********************** NEW WAITLIST - ENDS ************************/
/*********************************************************/


/*********************** New Booking - Start***********************/

$(document).ready(function () {

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });

    $('#walkin_floor_table_form').validate({
        rules: {
            walkin_floor: {
                required: true,
            },
            walkin_table: {
                required: true,
            }
        }
    });

    $('#addcustom_form').validate({
        rules: {
            custom_tag: {
                required: true,
            }
        }
    });

    $('.update_custom_tag_form').validate({
        rules: {
            new_custom_tag: {
                required: true,
            }
        }
    });

    $( "#reservation-booking" ).validate({
        rules: {
            party_size: {
                required: true,
            },
            reservation_date: {
                required: true,
            },
            reservation_time: {
                required: true,
            },
            floor_id: {
                required: true,
            },
            "table_id[]": {
                required: true,
            }
        },
         messages:{
            reservation_date :" Please choose a date when the reservation needs to be made for the customer.",
            reservation_time:"Please choose a valid timeslot for reservation."
        }
    });

    $( "#reservation-details #reservation-edit" ).validate({
        rules: {
            fname: {
                required: function(ele){
                    return $("#reservation-details #reservation-edit input[name='register_guest']:checked").val() == "1"
                }
            },
            phone: {
                required: function(ele){
                    return $("#reservation-details #reservation-edit input[name='register_guest']:checked").val() == "1"
                },
                // phoneUS: true, //or look at the additional-methods.js to see available phone validations
                number: true
            },
            email: {
                required: function(ele){
                    return $("#reservation-details #reservation-edit input[name='register_guest']:checked").val() == "1"
                },
                email: true
            },
            party_size: {
                required: true,
            },
            reservation_date: {
                required: true,
            },
            reservation_time: {
                required: true,
            },
            floor_id: {
                required: true,
            }
        },
        messages:{
            fname: {
                required:"Please enter the customer's first name.",
            },
            phone:{
                // required:"Please enter the customer's phone number.",
                required:"Please enter a valid 10-digit US phone number",
               // phoneUS:"Please enter a valid 10-digit US phone number"
            },
            email: {
                required: "Please enter the customer's email address.",
                email: "Please enter a valid email address (e.g. yourname@example.com)."
            },
            reservation_date :" Please choose a date when the reservation needs to be made for the customer.",
            reservation_time:"Please choose a valid timeslot for reservation."
        }
        
    });

    $( "#book-a-table #customer_detail" ).validate({
        rules: {
            fname: {
                required: true,
            },
            phone: {
                required: true,
                // phoneUS: function(ele){
                //     return phoneNoCount(ele) 
                // }, //or look at the additional-methods.js to see available phone validations
                // number: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages:{
            fname: {
                required:"Please enter the customer's first name.",
            },
            phone:{
                required:"Please enter a valid 10-digit US phone number",
                // phoneUS:"Please enter a valid 10-digit US phone number"
            },
            email: {
                required: "Please enter the customer's email address.",
                email: "Please enter a valid email address (e.g. yourname@example.com)."
            }
        }
        
    });



    getCurrentClock();
    //getTableCombinationSuggestions();

});

/***************** Phone Number Validation**********************/
// function phoneNoCount(ele){

//     console.log(ele.value)
//     if($("#book-a-table #customer_detail input[name='phone']").val().length == "10"){
        
//         removeValidation("#book-a-table #customer_detail");
//         return true
//     } else {
//         return false
//     }
// }
/*****************************************************************/

$("#book-a-table .register_customer").on('click', '#register_guest', function () {
    if($('#book-a-table #register_guest').prop('checked')) {
    // if($(this).attr('aria-pressed') == 'false') {
        $('#book-a-table .add_view').show();
        $('#book-a-table #add-view-guest').text('Add / View Guest Details');
    } else {
        $('#book-a-table .add_view').hide();
        $('#book-a-table .add-view-guest-list').hide();
    }
    if(parseInt($('#book-a-table #total_orders').val())==0 && parseInt($('#book-a-table #total_resv_count').val())==0 && parseInt($('#book-a-table #total_walk_in_count').val())==0 && parseInt($('#book-a-table #total_cancelled_count').val())==0){
        $('#book-a-table #clock .oldreservations_bookinform').html('No record exist.');
    }
});


$('.booking__btn').on('click', function (e) {
    clearAddBookingPopUp();
    floor_id = null;
    table_id = null;
});

$('#book-a-table').on('hidden.bs.modal', function () {
   clearAddBookingPopUp();
});



$('#reservation_info_popup').on('click', '.bookingnew', function (e) {
    clearAddBookingPopUp();
    floor_id = $(this).attr('floor-id');
    table_id = $(this).attr('table-id');
});

$('#reservation-details #reservationdeatails-datepicker, #reservationdeatails-timepicker, #party_size, #floors, #tables').on('change', function() {
    // floors & tables
    console.log($(this).attr('id'));
    if($(this).attr('id') == 'floors' || $(this).attr('id') == 'tables') {
        $('#reservation-details #yield_cancelled').val(2);
    } else {
        // show only check availability, hide modify, and save
        $('#reservation-details button.modify-reservation_btn').addClass('hide');
        $('#reservation-details button.save').addClass('hide');
        $('#reservation-details button.check').removeClass('hide');
    }
});

function clearNewBookSidebar() {
    $('#reservation-details #resv_id_title').html('');
    $('#reservation-details #fname').val('');
    $('#reservation-details #lname').val('');
    $('#reservation-details #email').val('');
    $('#reservation-details #mobile').val('');
    $('#reservation-details #total_orders').val(0);
    $('#reservation-details #total_resv_count').val(0);
    $('#reservation-details #total_walk_in_count').val(0);
    $('#reservation-details #total_cancelled_count').val(0);
    $('#reservation-details #reservationdeatails-datepicker').val(0);
    $('#reservation-details #reservationdeatails-timepicker').val(0);
    $('#reservation-details #party_size').val(0);
    $('#reservation-details #special_instruction').val('');
    $('#reservation-details #detail_dob').val('');
    $('#reservation-details #detail_doa').val('');
    $('#reservation-details #reservation_id').val(0);
    $('#reservation-details #yield_cancelled').val(0);
    $('#reservation-details #tat').val(0);
    $('#reservation-details #slot_range').val(0);
    $('#reservation-details #register_guest').prop('checked', false);
    $('#reservation-details #floors, #tables').children('option:not(:first)').remove(); //#special_occasion, #host
    $(".selectpicker").selectpicker('refresh');
    if($('#reservation-details #add-view-guest1').text() == 'Hide Guest Details') {
        $('#add-view-guest1').text('Add / View Guest Details');
        $('.add-view-guest-list1').slideUp(500);
    }
    $('#reservation-details #clock_detail .oldreservations_bookinform').html('No record exist.');
    $('#reservation-details button.modify-reservation_btn').removeClass('hide');
    $('#reservation-details button.save').addClass('hide');
    $('#reservation-details button.check').addClass('hide');
    $('#reservation-details button.cancle').addClass('hide');
    setTimeout(function () {
        $('#reservation-details .tag-plus .ui-autocomplete-input').prop('disabled', true).val('');
    },1000);
}

$('#reservation-details button.cancle').on('click', function() {
    $('#reservation-details').modal('hide');
});

/*
    reservation detail pop up code start
*/
$(document).on('click', '.resv_detail', function () {
    $('.input-group-btn').hide();
    var resv_id = this.id.split('_');
    $('#reservation-details').show();
    makeBookingInfoUnEditable();
    clearNewBookSidebar();
    getAndSetReservationDetail(resv_id[1]);
});


$('.reservation__edit .cancel_edit_name-third').on('click', function () {
    $('.reservation__edit .update_guest_details-third').addClass('hide');
    $('.reservation__edit .cancel_edit_name-third').addClass('hide');
    $('.reservation__edit .edit_name').removeClass('hide');
});

$("#reservation-details .register_customer").on('click', '#register_guest', function () {
    if($(this).prop('checked')) {
    // if($(this).attr('aria-pressed') == 'false') {
        $('#reservation-details .add_view1').show();
        $('#reservation-details #add-view-guest1').text('Add / View Guest Details');
    } else {
        $('#reservation-details .add_view1').hide();
        $('#reservation-details .add-view-guest-list1').hide();
    }
    if(parseInt($('#reservation-details #total_orders').val())==0 && parseInt($('#reservation-details #total_resv_count').val())==0 && parseInt($('#reservation-details #total_walk_in_count').val())==0 && parseInt($('#reservation-details #total_cancelled_count').val())==0){
        $('#reservation-details #clock_detail .oldreservations_bookinform').html('No record exist.');
    }
});

$('#reservation-details .add-view-guest-list1').slideUp();
$('#reservation-details #add-view-guest1').on('click', function () {
    if ($('#reservation-details .add-view-guest-list1').is(':visible')) {
        $('#reservation-details #add-view-guest1').text('Add / View Guest Details');
        $('#reservation-details .add-view-guest-list1').slideUp(500);
    } else {
        $('#reservation-details #add-view-guest1').text('Hide Guest Details');
        $('#reservation-details .add-view-guest-list1').slideDown(500);
    }
});
/*
$('.reservation__edit .cancel_edit_name-third').on('click', function () {
    $('.reservation__edit .update_guest_details-third').addClass('hide');
    $('.reservation__edit .cancel_edit_name-third').addClass('hide');
    $('.reservation__edit .edit_name').removeClass('hide');
});
*/
$('.modify-reservation_btn').on('click', function () {
    makeBookingInfoEditable();
    $("#reservation-edit input").prop("readonly", false);
    $("#reservation-edit input#party_size").prop("readonly", true);
    $("#reservation-edit input").prop("disabled", false);
    if($('#reservation-details #user_id').val()===null || $('#reservation-details #user_id').val()===undefined || $('#reservation-details #user_id').val()==''){
        $("#reservation-details #register_guest").prop("disabled", false);
    }else{
        $("#reservation-details #register_guest").attr("disabled", true);
    }
    $('#reservation-details #party_size').prop('disabled', false);
    $('#reservation-details #resv_detail_check_availability').addClass('hide');
    $('#reservation-details .cancle').removeClass('hide');
    $('#reservation-details .save').removeClass('hide');
    $('#reservation-details .modify-reservation_btn').addClass('hide');
    $('#manageWaitlistModal #waitlist_party_size').prop('disabled', false);
    $("#reservation-details #floors").prop('readonly', false).prop('disabled', false);
    $("#reservation-details #tables").prop('readonly', false).prop('disabled', false);
    $("#reservation-details .selectpicker").selectpicker('refresh');
    $("#reservation-details .selct-picker-plain,#reservation-details .statusbar,#reservation-details .reserve").removeClass('pointer-none');
    $('#reservation-details .tag-plus.icon').removeClass('pointer-none');

    // check availability for display multiple table when click on modify button

    var reservation_time = $('#reservation-details #reservationdeatails-timepicker').val();
    var reservation_date = $("#reservation-details #reservationdeatails-datepicker").val();
    var party_size = $('#reservation-details #party_size').val();
    var formdata = "reservation_date="+reservation_date+"&reservation_time="+reservation_time+"&party_size="+party_size+"&reservation_id="+$('#reservation-details #reservation_id').val();
    var floorHtml = '<option value="">Select Floor</option>';
    var tableHtml = '<option value="">Select Tables</option>';
    $.ajax({
        type: 'POST',
        url: '/reservation/check-availability',
        data: formdata,
        dataType: 'json',
        success: function (resp) {
            walk_in_floor_data = resp.data.floor_table_info;
            $.each(resp.data.floor_table_info, function (key, floor) {
                floorHtml += '<option value="'+floor.id+'">'+floor.name+'</option>';
                if(floor.tables && floor.id==floor_id) {
                    $.each(floor.tables, function(table_key, table_value) {
                        tableHtml += '<option value="'+table_value.id+'">'+table_value.name+'</option>';
                    });
                }
            });
            $('#reservation-details #floors').empty().html(floorHtml);
            $('#reservation-details #tables').empty().html(tableHtml);
            $("#reservation-details #floors.selectpicker").selectpicker('val', floor_id);
            if(table_id.toString().indexOf(',')>-1) {
                $("#reservation-details #tables.selectpicker").selectpicker('val', table_id.toString().split(','));
            } else {
                $("#reservation-details #tables.selectpicker").selectpicker('val', table_id);
            }
            if(typeof resp.data.tat !== 'undefined' && $('#reservation-details #tat').length) {
                $('#reservation-details #tat').val(resp.data.tat);
            }
            if(typeof resp.data.slot_range !== 'undefined' && $('#reservation-details #slot_range').length) {
                $('#reservation-details #slot_range').val(resp.data.slot_range);
            }
            $(".selectpicker").selectpicker('refresh');
        },
        error: function (data, textStatus, errorThrown) {
            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            //$('.message_div').empty().html(err).show();
            //$('.alert-danger').show();
        }
    });
    var can_modify_reservation = $('#reservation-details #can_modify_reservation').val();
    if(can_modify_reservation.trim()=='true'){
        $('#reservation-details .input-group-btn').removeClass('hide').show();
    }else{
        $('#reservation-details .input-group-btn').addClass('hide');
    }
});

$('#reservation-details .check-availability__btn.fullWidth.cancle').on('click', function () {
    $('#reservation-details .input-group-btn').addClass('hide');
    $("#reservation-details .selct-picker-plain,#reservation-details .statusbar,#reservation-details .reserve,#reservation-details .tag-plus.icon").addClass('pointer-none');
});

$('#reservation-details  #resv_detail_check_availability').on('click', function () {
    var formdata = $('#reservation-details form').serialize();
    $('#reservation-details .message_div').empty();
    $.ajax({
        type: 'POST',
        url: '/reservation/check-availability',
        data: formdata,
        dataType: 'json', //mispelled
        success: function (resp) {
            //Clear Selected Table After Getting Availability
            enableDoubleClick = true;
            //clearTableSelection();

            $('#reservation-details button.check').addClass('hide');
            $('#reservation-details button.save').removeClass('hide');
            $('#loader').addClass('hidden');
            if (resp.data.available_nearby_slots) {
                /*$.each(resp.data.available_nearby_slots, function (key, slot) {
                    slotHtml += '<li><input type="radio" id="toggle-off' + key + '" class="hidden slot_time reservation-book" name="slot_time" value="'+slot+'"><label for="toggle-off' + key + '">' + slot + '</label></li>';
                });
                $('.near_by_slots .time--buttons').empty().html(slotHtml);
                $('.near_by_slots').show();*/
            } else {
                walk_in_floor_data = resp.data.floor_table_info;
                var floorHtml = '<option value="">Select Floor</option>';
                $.each(walk_in_floor_data, function (key, floor) {
                    floorHtml += '<option value="'+floor.id+'">'+floor.name+'</option>';
                });
                $('#reservation-details #floors').html(floorHtml);

                var yield_found_flag = 0;
                if(typeof resp.data.yield_floor_table != "undefined") {
                    console.log(resp.data.yield_floor_table.yield);
                    $.each(resp.data.yield_floor_table.yield, function(key, value){
                        if(yield_found_flag == 0) {
                            yield_floor_id = value.floor_id;
                            yield_table_id = value.table_id;
                            yield_found_flag = 1;
                        }
                    });
                    console.log(yield_floor_id + ' - ' + yield_table_id);
                    $("#reservation-details #floors").val(yield_floor_id);
                    $("#reservation-details #floors").trigger('change');


                }
                if(typeof resp.data.tat !== 'undefined') {
                    $('#reservation-details #tat').val(resp.data.tat);
                }
                if(typeof resp.data.slot_range !== 'undefined') {
                    $('#reservation-details #slot_range').val(resp.data.slot_range);
                }
                console.log('ca - hide show');
                $('#reservation-details button.modify-reservation_btn').addClass('hide');
                $('#reservation-details button.check').addClass('hide');
                $('#reservation-details button.save').removeClass('hide');
                $('#reservation-details button.cancle').removeClass('hide');

                /*$("#reservation-details #reservationdeatails-datepicker").prop('readonly', true);
                $("#reservation-details #reservationdeatails-timepicker").prop('readonly', true);
                $('#reservation-details #party_size').prop('readonly', true);
                $("#reservation-details #floors").prop('readonly', true);
                $("#reservation-details #tables").prop('readonly', true);*/
                $("#reservation-details .selct-picker-plain,#reservation-details .statusbar,#reservation-details .reserve").removeClass('pointer-none');

                setTimeout(function () {
                    $(".selectpicker").selectpicker('refresh');
                }, 10);
            }
            if(resp.message!==null && resp.message!==undefined){
                $('#reservation-details .message_div').empty().html(resp.message).show();
            }
        },
        error: function (data, textStatus, errorThrown) {
            floorFound = false;
            $('#loader').addClass('hidden');
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            $('#reservation-details .message_div').empty().html(err).show();
        }
    });
});

function clearAddBookingPopUp()
{
    $('#book-a-table .add_view').hide();
    $('#book-a-table .add-view-guest-list').hide();
    $('#book-a-table #reservation-booking')[0].reset();
    $('#book-a-table #customer_detail')[0].reset();
    $('#book-a-table #guest_tags_booking').html('');
    $('#book-a-table .floor_table').hide();
    $('#book-a-table .newWaitlist__btn-container').show();
    $("#book-a-table #check_avail_button").show();
    $('#book-a-table #book_button').hide();
    $('#book-a-table .message_div').empty().hide();
    removeValidation("#reservation-booking");
    removeValidation("#customer_detail");
    $("#book-a-table #guest_tags_booking").tagit("removeAll");

    $("#book-a-table .register_customer").show();
    $('#book-a-table .near_by_slots .time--buttons').html('');
    $('#book-a-table .near_by_slots').hide();
    $('#book-a-table #reservation_date').attr('disabled', false);
    $('#book-a-table #book-a-table-tp').attr('disabled', false);
    $('#book-a-table #clock .oldreservations_bookinform').html('No record exist.');
    $('#book-a-table #register_guest').prop('checked', false);
    $("#book-a-table span.hasVal").each(function(key, value) {  
        $(this).removeClass('hasVal')
    });

    if($("#book-a-table #add-view-guest").text() == "Hide Guest Details"){
        $('#book-a-table #add-view-guest').text('Add / View Guest Details');
        $('.add-view-guest-list1').slideUp(500);
    }
}

$('#check_avail_button').on('click', function () {
    checkFloorAndTableAvailability();
});

$('.near_by_slots').on("change", '.slot_time', function () {
    checkFloorAndTableAvailability();
});

window.removeValidation = function (id){
    $(id).find('input.error').removeClass('error');
    $(id).find('label.error').hide();
}

function checkFloorAndTableAvailability()
{
    $('.message_div').empty();
    if($( "#reservation-booking" ).valid()) {
        $(this).attr('disabled',true);
        $('#loader').removeClass('hidden');
        var formdata = $('#reservation-booking').serialize();
        if(floor_id!=null){
            formdata+='&floor_id='+floor_id;
        }
        if(table_id!=null){
            formdata+='&table_id='+table_id;
        }
        var slotHtml = '';

        $("#book-a-table").removeClass('getAvailability')

        $.ajax({
            type: 'POST',
            url: '/reservation/check-availability',
            data: formdata,
            dataType: 'json', //mispelled
            success: function (resp) {
                //Clear Selected Table After Getting Availability
                enableDoubleClick = true;
                clearTableSelection();
                showHideChangeDate('show');
                $('#loader').addClass('hidden');
                if (resp.data.available_nearby_slots) {
                    $.each(resp.data.available_nearby_slots, function (key, slot) {
                        slotHtml += '<li><input type="radio" id="toggle-off' + key + '" class="hidden slot_time reservation-book" name="slot_time" value="'+slot+'"><label for="toggle-off' + key + '">' + slot + '</label></li>';
                    });
                    $('#book-a-table .near_by_slots .time--buttons').empty().html(slotHtml);
                    $('#book-a-table .near_by_slots').show();
                } else {
                    walk_in_floor_data = resp.data.floor_table_info;
                    console.log(walk_in_floor_data);
                    var floorHtml = '<option value="">Select Floor</option>';
                    var current_floor = parseInt(floor_url.split("/floor/")[1]);
                    $.each(walk_in_floor_data, function (key, floor) {
                        floorHtml += '<option value="'+floor.id+'">'+floor.name+'</option>';
                        if(parseInt(floor.id) == current_floor && !floorFound && table_id!=null){
                            $.each(floor.tables, function(table_key, table_value) {
                                if(table_value.id == table_id){
                                    floorFound = true;
                                }
                            });
                        }
                    });
                    $('#floors').html(floorHtml);
                    var yield_found_flag = 0;
                    if(floorFound) {
                    $("#floors").val(floor_url.split("/floor/")[1]);
                    $("#floors").trigger('change');
                    }else if(typeof resp.data.yield_floor_table != "undefined") {
                        console.log(resp.data.yield_floor_table.yield);
                        $.each(resp.data.yield_floor_table.yield, function(key, value){
                            if(yield_found_flag == 0) {
                                yield_floor_id = value.floor_id;
                                yield_table_id = value.table_id;
                            }
                            if(current_floor == value.floor_id) {
                                yield_found_flag = 1;
                            }
                        });
                        console.log(yield_floor_id + ' - ' + yield_table_id);
                        $("#floors").val(yield_floor_id);
                        $("#floors").trigger('change');
                    }
                    //$(".selectpicker").selectpicker('refresh');
                    $('.floor_table').show();

                    setTimeout(function () {
                        $(".selectpicker").selectpicker('refresh');
                    }, 10);

                    $("#check_avail_button").hide();
                    $("#book_button").show();
                    $('.host_name').show();
                }
                if(resp.message!==null && resp.message!==undefined){
                    $('.message_div').empty().html(resp.message).show();
                }
                $("#book-a-table").addClass('getAvailability');

                //$('#reservation_date').attr('disabled', true);
                //$('#book-a-table-tp').attr('disabled', true);
                // $('.input-group-btn').hide();
            },
            error: function (data, textStatus, errorThrown) {
                floorFound = false;
                $('#loader').addClass('hidden');
                $('#check_avail_button').attr('disabled',false);
                $('#check_avail_button').html($('#check_avail_button').attr('button-name'));
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });
                $('.message_div').empty().html(err).show();
            }
        });
    }
}

$('#book-a-table #floors').on('change', function() {
    console.log(yield_table_id);
    var floor_id = $(this).val();
    if(floor_id=='' || floor_id===null || floor_id==undefined){
        $('#book-a-table #tables').html('<option value="">Select Table</option>');
        $(".selectpicker").selectpicker('refresh');
    }else{
        var selectedTable,selectedTableText;
        var party_size = $('input[name=party_size]').val();
        $.each(walk_in_floor_data, function(key, floor){
            if(floor_id == floor.id) {
                if(floor.tables) {
                    var tableHtml = '<option value="">Select Table</option>';
                    $.each(floor.tables, function(table_key, table_value) {
                        //if(floorFound && table_value.max == party_size){
                        if(floorFound && table_value.id == table_id){
                            selectedTable = table_value.id;
                            selectedTableText = table_value.name;
                        } else if(table_value.max == party_size) {
                            selectedTable = table_value.id;
                            selectedTableText = table_value.name;
                        }
                        tableHtml += '<option value="'+table_value.id+'">'+table_value.name+'</option>';
                    });
                    if(floorFound || selectedTable) {
                        var table_to_select = selectedTable;
                    }else if(yield_table_id != '') {
                        var table_to_select = yield_table_id;
                        //getAutoTableSelectionByText([yield_table_id]);
                    } /*else if(floorFound || selectedTable) {
                        var table_to_select = selectedTable;
                        /*getAutoTableSelectionByText(
                            [{
                                "id":selectedTable,
                                "name":selectedTableText
                            }]
                        )*/
                    //}
                    $('#book-a-table #tables').html(tableHtml);
                    if(!isNaN(table_to_select)){
                        $('#book-a-table #tables').val(table_to_select);
                    }

                    $(".selectpicker").selectpicker('refresh');
                } 
            }
        });
    }
});
$('#reservation-details #floors').on('change', function() {
    console.log(yield_table_id);
    var floor_id = $(this).val();
    if(floor_id=='' || floor_id===null || floor_id==undefined){
        $('#reservation-details #tables').html('<option value="">Select Table</option>');
        $(".selectpicker").selectpicker('refresh');
    }else{
        var selectedTable,selectedTableText;
        var party_size = $('input[name=party_size]').val();
        $.each(walk_in_floor_data, function(key, floor){
            if(floor_id == floor.id) {
                if(floor.tables) {
                    var tableHtml = '<option value="">Select Table</option>';
                    $.each(floor.tables, function(table_key, table_value) {
                        if(floorFound && table_value.max == party_size){
                            selectedTable = table_value.id;
                            selectedTableText = table_value.name;
                        } else if(table_value.max == party_size) {
                            selectedTable = table_value.id;
                            selectedTableText = table_value.name;
                        }
                        tableHtml += '<option value="'+table_value.id+'">'+table_value.name+'</option>';
                    });
                    if(yield_table_id != '') {
                        var table_to_select = yield_table_id;
                        //getAutoTableSelectionByText([yield_table_id]);
                    } else if(floorFound || selectedTable) {
                        var table_to_select = selectedTable;
                        /*getAutoTableSelectionByText(
                            [{
                                "id":selectedTable,
                                "name":selectedTableText
                            }]
                        )*/
                    }
                    $('#reservation-details #tables').html(tableHtml);
                    if(!isNaN(table_to_select)) {
                        $('#reservation-details #tables').val(table_to_select);
                    }
                    $(".selectpicker").selectpicker('refresh');
                } 
            }
        });
    }
});

$('#book-a-table .selectpicker').on('change', function(){
    console.log(yield_floor_id + ' - ' + yield_table_id);
    if($('#book-a-table #yield_cancelled').val() == '') {
        $('#book-a-table #yield_cancelled').val(0);
    }
    var yield_changed = parseInt($('#book-a-table #yield_cancelled').val()) + 1
    $('#book-a-table #yield_cancelled').val(yield_changed);

});

$('#book_button').on('click', function () {
    $('#reservation_date').attr('disabled', false);
    $('#book-a-table-tp').attr('disabled', false);
    if ( $("#reservation-booking").valid() &&  $("#customer_detail").valid()) {
        $(this).attr('disabled',true);
        //$(this).html('<i class="fa fa-spinner fa-spin"></i>'+$(this).text());
        window.localStorage.setItem('reservation_going_on', 'yes');
        $('#loader').removeClass('hidden');
        $('.alert-danger, .alert-success').empty();
        var formdata = $('#reservation-booking, #customer_detail').serialize();
        $.ajax({
            type: 'POST',
            url: '/reservation/book-table',
            data: formdata+"&source=offline",
            dataType: 'json', //mispelled
            success: function (data) {
                $('#book-a-table').modal('hide');
                $('#book_button').attr('disabled',false);
                alertbox('Success',data.message, function(modal){
                    $('#loader').addClass('hidden');
                });

                openSibebarController(false);

                if (window.localStorage.getItem("gridActiveTab") === null) {
                        floorGridViewUpdate();
                    }else{
                        if ($('#grid-view').length === 1) {
                        getUpdateGridView();
                        floorGridViewUpdate();
                        }
                }
                window.localStorage.removeItem("reservation_going_on");

            },
            error: function (data, textStatus, errorThrown) {
                $('#loader').addClass('hidden');
                $('#book_button').attr('disabled',false);
                var err='';
                $.each(data.responseJSON.errors, function(key, value){
                    err+='<p>'+value+'</p>';
                });
                $('.message_div').empty().html(err);
                $('.message_div').show();
            }
        });
    }
});


/*$('#grid-view-btn').on('click', function(){
    $('.view').removeClass('active');
    $(this).addClass('active');
    $('#grid-view').removeClass('hide');
    $('#floor-view').addClass('hide');

    floorGridViewUpdate();
});*/

$("#grid-view-btn").on('click', function () {

    $('.view').removeClass('active');
    $(this).addClass('active');
    $('#grid-view').removeClass('hide');
    $('#floor-view,#show-hide-timer').addClass('hide');

    getUpdateGridView();

});

function getUpdateGridView(){
    var dataobj={};
    var dateTypeVar = $('.tableManagement__wrapper #datepicker').datepicker('getDate');
    var date = $.datepicker.formatDate('mm-dd-yy', dateTypeVar);

    //var date=$('.floor_fetch #datepicker').val();


    dataobj.date=date;
    dataobj.type='grid';
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify(dataobj),
        success: function (data) {
            //console.log(JSON.parse(data.data));
            gridView(JSON.parse(data.data),'update');
            $('#loader').addClass('hidden')
        },
        error: function (data, textStatus, errorThrown) {
        }
    });
    //auto refresh table pop up
    if($('.bookingnew').length > 0){
        getTableId($('.bookingnew').attr('table-id'));
    }
    //update right sidebar
    var tab_target = 'current';
    tab_target = $('#reservations a.status_tab.active').attr("href").replace('#', '');
    displayRightSideReservation(tab_target);
    $(".selectpicker").selectpicker('refresh');
    $('.side_bar_right #keyword').val('')
}

$('#floor-view-btn').on('click', function(){
    $('.view').removeClass('active');
    $(this).addClass('active');
    $('#floor-view').removeClass('hide');

    if($("#datepicker").val() == "Today"){
        $('#show-hide-timer').removeClass('hide');
    }

    $('#grid-view').addClass('hide');
    floorGridViewUpdate();
});



var settings = {
    showArrows: true
  };

var _jScrollPane, _jScrollPaneAPI,
    _jSheight;

$( ".fname_booking" ).autocomplete({
    source: "/guestbook/search",
    minLength: 2,
    open: function() {
      $('.ui-autocomplete').width($(this).width());
      $('.ui-autocomplete > li')
        .wrapAll($('<div class="scroller_auto max-height"></div>')
          .width($(this).width())
          .height(_jSheight));
        $(".scroller_auto").mCustomScrollbar(options);
    },
    select: function( event, ui ) {
        //console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );
        $("#book-a-table").find(':input').not('.reservation-book').val('');
        if(ui.item.id){
            $.ajax({
                type: 'GET',
                url: '/guestbook/detail/'+ui.item.id ,
                dataType: 'json',
                success: function (data) {
                    var reservations = '';
                    var userdata = data.data;
                    if (userdata.guest) {
                        var availableTags = userdata.tags;
                        $('#book-a-table .register_customer').hide();
                        $('#book-a-table .add_view').show();
                        $('#book-a-table #user_id').val(userdata.guest.id);
                        $("#book-a-table #fname").val(userdata.guest.fname);
                        $("#book-a-table #lname").val(userdata.guest.lname);
                        $("#book-a-table #phone").val(userdata.guest.mobile);
                        $("#book-a-table #email").val(userdata.guest.email);
                        $("#book-a-table #description").val(userdata.guest.description);
                        $("#book-a-table #dob").val(userdata.guest.dob);
                        $("#book-a-table #doa").val(userdata.guest.doa);
                        $('#book-a-table #guest_note').val(userdata.guest.description);
                        $("#book-a-table #total_orders").val(userdata.total_orders);
                        $("#book-a-table #total_resv_count").val(userdata.total_resv_count);
                        $("#book-a-table #total_walk_in_count").val(userdata.total_walk_in_count);
                        $("#book-a-table #total_cancelled_count").val(userdata.total_cancelled_count);

                        // waitlist - no tables form
                        if($('#waitlist_no_tables_form').length) {
                            $("#waitlist_no_tables_form #fname").val(userdata.guest.fname).nextAll('span').addClass("hasVal");
                            $("#waitlist_no_tables_form #lname").val(userdata.guest.lname).nextAll('span').addClass("hasVal");
                            $("#waitlist_no_tables_form #phone").val(userdata.guest.mobile).nextAll('span').addClass("hasVal");
                            $("#waitlist_no_tables_form #email").val(userdata.guest.email).nextAll('span').addClass("hasVal");
                            $("#waitlist_no_tables_form #userid").val(userdata.guest.id);
                            $('#waitlist_no_tables_form #register_customer').prop('checked', false);
                            $("#waitlist_no_tables_form .register_customer").hide();
                            $( "#waitlist_no_tables_form" ).valid();
                        }
                        // waitlist - edit
                        if($('#manageWaitlistModal #waitlist_popup').is(':visible')) {
                            $("#waitlist_popup #fname").val(userdata.guest.fname).nextAll('span').addClass("hasVal");
                            $("#waitlist_popup #lname").val(userdata.guest.lname).nextAll('span').addClass("hasVal");
                            $("#waitlist_popup #phone").val(userdata.guest.mobile).nextAll('span').addClass("hasVal");
                            $("#waitlist_popup #email").val(userdata.guest.email).nextAll('span').addClass("hasVal");
                            $("#waitlist_popup #userid").val(userdata.guest.id);
                            $('#waitlist_popup #register_customer').prop('checked', false);
                            $("#waitlist_popup .register_customer").hide();
                            $( "#waitlist_popup" ).valid();
                        }

                        // waitlist - book seat
                        if($('#third_step_walkin').length) {
                            $("#third_step_walkin #fname").val(userdata.guest.fname).nextAll('span').addClass("hasVal");
                            $("#third_step_walkin #lname").val(userdata.guest.lname).nextAll('span').addClass("hasVal");
                            $("#third_step_walkin #phone").val(userdata.guest.mobile).nextAll('span').addClass("hasVal");
                            $("#third_step_walkin #email").val(userdata.guest.email).nextAll('span').addClass("hasVal");
                            $("#third_step_walkin #userid").val(userdata.guest.id);
                            $('#third_step_walkin #register_customer').prop('checked', false);
                            $("#third_step_walkin .register_customer").hide();
                            $( "#third_step_walkin" ).valid();
                        }

                        if (userdata.guest.reservations) {
                            $.each(userdata.guest.reservations, function (key, value) {
                                var icon = JSON.parse(value.status.icon);
                                var color = JSON.parse(value.status.color);
                                reservations += '<li><div class="clock--tab-flex"> <div class=""> <div><span><i style="color:'+color.sidebar+'" class="' + icon.sidebar + '" ></i></span></div> <span><b>' + value.reservation_day + ', ' + value.reservation_date + ' </b> <span class="lyt-color">' + value.reservation_time + ', party of ' + value.reserved_seat + '</span></span> </div> <div class="">' + value.status.name + '</div> </div>';
                                reservations += '</li>';
                            });
                            if(reservations){
                                $("#book-a-table #reservation_history").attr('href', '#clock');
                                $('#book-a-table #clock .oldreservations_bookinform').html(reservations);
                            }
                        }
                        $("#guest_tags_booking").tagit("removeAll");
                        if (userdata.guest.tags) {
                            var taglist = '';
                            $.each(userdata.guest.tags, function (key, value) {
                                $("#book-a-table #guest_tags_booking").tagit("createTag", value);  //Using option 3
                            });
                        }
                        $('#book-a-table #register_guest').prop('checked', true);
                        $('#book-a-table #register_guest').prop('disabled', 'disabled');
                    } else {
                        $('#book-a-table #user_id').val('');
                        $("#book-a-table").find(':input').not('.reservation-book').val('');
                        $('#book-a-table #clock .oldreservations_bookinform').html('No record exist.');
                        $(".register_customer").show();
                        $('#book-a-table #register_guest').prop('checked', false);
                        $('#book-a-table #register_guest').removeAttr('disabled');
                    }
                    $('#book-a-table #customer_detail .input__field input').blur();
                    $( "#book-a-table #customer_detail" ).valid();

                    $(".fname_booking").blur();
                },
                error: function (data, textStatus, errorThrown) {
                    $('#book-a-table #clock .oldreservations_bookinform').html('No record exist.');
                    $("#book-a-table #customer_detail" ).valid();
                    $('#book-a-table .register_customer').show();
                    $('#book-a-table .add_view').hide();
                    $('#book-a-table .add-view-guest-list').hide();
                    $("#book-a-table").find(':input').not('.reservation-book').val('');
                    $('#book-a-table #user_id').val('');
                    $('#book-a-table #register_guest').removeAttr('disabled');
                    $('#book-a-table #register_guest').prop('checked', false);
                    if($('#waitlist_no_tables_form').length) {
                        $("#waitlist_no_tables_form #lname").val('');
                        $("#waitlist_no_tables_form #phone").val('');
                        $("#waitlist_no_tables_form #email").val('');
                        $("#waitlist_no_tables_form #userid").val('');
                        $('#waitlist_no_tables_form #register_customer').removeAttr('disabled');
                        $('#waitlist_no_tables_form #register_customer').prop('checked', false);
                        $("#waitlist_no_tables_form .register_customer").show();
                    }
                    // waitlist - edit
                    if($('#manageWaitlistModal #waitlist_popup').is(':visible')) {
                        $("#waitlist_popup #lname").val('');
                        $("#waitlist_popup #phone").val('');
                        $("#waitlist_popup #email").val('');
                        $("#waitlist_popup #userid").val('');
                        $('#waitlist_popup #register_customer').removeAttr('disabled');
                        $('#waitlist_popup #register_customer').prop('checked', false);
                        $("#waitlist_popup .register_customer").show();
                    }

                    // waitlist - book seat
                    if($('#third_step_walkin').length) {
                        $("#third_step_walkin #lname").val('');
                        $("#third_step_walkin #phone").val('');
                        $("#third_step_walkin #email").val('');
                        $("#third_step_walkin #userid").val('');
                        $('#third_step_walkin #register_customer').removeAttr('disabled');
                        $('#third_step_walkin #register_customer').prop('checked', false);
                        $("#third_step_walkin .register_customer").show();
                    }
                }
            });
        }
    },   //HERE - make sure to add the comma after your select
    response: function(event, ui) {
        console.log($('#book-a-table #register_guest').prop('checked'));
        if (!ui.content.length) {
            $('#book-a-table #clock .oldreservations_bookinform').html('No record exist.');
            //$("#book-a-table #customer_detail" ).valid();
            $('#book-a-table .register_customer').show();
            $('#book-a-table .add_view').hide();
            $('#book-a-table .add-view-guest-list').hide();
            $("#book-a-table").find(':input').not('.reservation-book').val('');
            $('#book-a-table #user_id').val('');
            $('#book-a-table #register_guest').removeAttr('disabled');
            $('#book-a-table #register_guest').prop('checked', false);

            if($('#waitlist_no_tables_form').length) {
                $("#waitlist_no_tables_form #lname").val('');
                $("#waitlist_no_tables_form #phone").val('');
                $("#waitlist_no_tables_form #email").val('');
                $("#waitlist_no_tables_form #userid").val('');
                $('#waitlist_no_tables_form #register_customer').removeAttr('disabled');
                $('#waitlist_no_tables_form #register_customer').prop('checked', false);
                $("#waitlist_no_tables_form .register_customer").show();
            }
            // waitlist - edit
            if($('#manageWaitlistModal #waitlist_popup').is(':visible')) {
                $("#waitlist_popup #lname").val('');
                $("#waitlist_popup #phone").val('');
                $("#waitlist_popup #email").val('');
                $("#waitlist_popup #userid").val('');
                $('#waitlist_popup #register_customer').removeAttr('disabled');
                $('#waitlist_popup #register_customer').prop('checked', false);
                $("#waitlist_popup .register_customer").show();
            }

            // waitlist - book seat
            if($('#third_step_walkin').length) {
                $("#third_step_walkin #lname").val('');
                $("#third_step_walkin #phone").val('');
                $("#third_step_walkin #email").val('');
                $("#third_step_walkin #userid").val('');
                $('#third_step_walkin #register_customer').removeAttr('disabled');
                $('#third_step_walkin #register_customer').prop('checked', false);
                $("#third_step_walkin .register_customer").show();
            }
        }
    }
});


//No show/wait for 5 minute
function markAsNoShow(reservation_id, reservation_status_slug,curr)
{
    curr.parents('.reservationStatusPanel__user').removeClass('reserved__timer__late');
    $(this).parents('.reservationStatusPanel__user').removeClass('reserved__timer__late');
    var type='current';
    changeReservationStatus(reservation_id, 'no_show', type, 1);
}

//function for get available floors and tables
/*
function getAvailableFloorsAndTables(reservation_date, reservation_time, party_size, reservation_id) {
    var formdata = "reservation_date="+reservation_date+"&reservation_time="+reservation_time+"&party_size="+party_size+"&reservation_id="+reservation_id;
    $.ajax({
        type: 'POST',
        url: '/reservation/check-availability',
        data: formdata,
        dataType: 'json',
        success: function (resp) {
            walk_in_floor_data = resp.data.floor_table_info;
        },
        error: function (data, textStatus, errorThrown) {
            walk_in_floor_data=null;
            return data.responseJSON.errors;
        }
    });
}*/

$('#setfloor').change(function(){
    var url=floor_url.split("/floor/")[0];
    if($(this).val()!=''){ window.location.href=url+$(this).val();}
});

$("#newWalkinModal").on("show.bs.modal", function () {
    $("#new_walkin_button").attr("data-popup",true)
    $('#book-a-table,#blockTables').modal('hide');
    removeValidation("#third_step_walkin");
});

$('#book-a-table').on('shown.bs.modal', function () {
    clearAddBookingPopUp();
    showHideChangeDate('hide');
});

function showHideChangeDate(action) {
    if(action=='show'){
        $('#book-a-table .b_check_availability_change').removeClass('hide');
        $('#book-a-table .b_date_time_party').hide();
        var reservation_date = $('#book-a-table #reservation_date').val();
        var reservation_time = $('#book-a-table #book-a-table-tp').val();
        var reservation_party = $('#book-a-table .party_size').val();
        $('#book-a-table .b_reservation_text').empty().html('Reservation for '+reservation_party+' on '+reservation_date+' <br/> at '+reservation_time+'.');
    }else{
        $('#book-a-table .near_by_slots .time--buttons').empty();
        $('#book-a-table .near_by_slots').hide();
        $('#check_avail_button').show();
        $('#book_button').hide();
        $('#book-a-table .b_check_availability_change').addClass('hide');
        $('#book-a-table .floor_table').hide();
        $('#reservation-booking #floors').children('option:not(:first)').remove();
        $('#reservation-booking #tables').children('option:not(:first)').remove();
        $('#book-a-table .b_date_time_party').show();
        $('#book-a-table .b_reservation_text').text('');
        $('#book-a-table .message_div').empty().hide();
    }
}

function showHideChangeDateInDetail(action) {
    $("#reservation-details .b_change_date").hide();
    if(action=='show'){
        $('#reservation-details .b_check_availability_change, #reservation-details .b_check_availability_change + div.border-sepration').removeClass('hide');
        $('#reservation-details .b_reservation_data').hide();
        var reservation_date = $('#reservation-details #reservationdeatails-datepicker').val();
        var reservation_time = $('#reservation-details #reservationdeatails-timepicker').val();
        var reservation_party = $('#reservation-details .party_size').val();
        $('#reservation-details .b_reservation_text').empty().html('Reservation for '+reservation_party+' on '+reservation_date+' <br/> at '+reservation_time+'.');
    }else{
        $('#reservation-details .b_check_availability_change, #reservation-details .b_check_availability_change + div.border-sepration').addClass('hide');
        $('#reservation-details .b_reservation_data').show();
        $('#reservation-details #floors').children('option:not(:first)').remove();
        $('#reservation-details #tables').children('option:not(:first)').remove();
        $(".selectpicker").selectpicker('refresh');
        $('#reservation-details .b_reservation_text').text('');
        $('#reservation-details #resv_detail_check_availability').removeClass('hide');
        $('#reservation-details .cancle').removeClass('hide');
        $('#reservation-details .save').addClass('hide');
        $('#reservation-details .modify-reservation_btn').addClass('hide');
    }
    $('#reservation-details #reservationdeatails-datepicker').attr('readonly','readonly');
}

$('#book-a-table .b_change_date').on('click', function () {
    showHideChangeDate('hide');
});

$('#reservation-details .b_change_date').on('click', function () {
    showHideChangeDateInDetail('hide');
});

/*$(document).ready(function(){
    if($('html').hasClass("win")){
        $("#instruction").keypress(function (e) {
           if (e.keyCode === 32 || e.keyCode === 13){
                return false;
            }
        });
    }
});*/


    $('#grid-view-btn').on('click', function(e) {
        $('#loader').removeClass('hidden')
        window.localStorage.setItem('gridActiveTab', '#grid-view');
    });
    var gridActiveTab = window.localStorage.getItem('gridActiveTab');
    if (gridActiveTab && $('#grid-view').length===1) {
        $('#grid-view').removeClass('hide');
        $('#floor-view,#show-hide-timer').addClass('hide');
        $('.view').removeClass('active');
        $('#grid-view-btn').addClass('active');
        getUpdateGridView();

    }
    $('#floor-view-btn , .sidebar a').click(function (e) {
        window.localStorage.removeItem("gridActiveTab");
    });

$('#customSettingModal').on('hidden.bs.modal', function () {
   removeValidation("#custom_hours");
});

/*
    reservation detail pop up code start
*/
$(document).on('click', '.resv_detail_enquiry', function () {
    $('#extra_field').empty()
    var resv_id = this.id.split('_');
    $('#reservation-enquiry-details').show();
    $.ajax({
        url: '/reservation/detail/' + resv_id[1],
        dataType: 'json',
        success: function (data) {
            var extra_field='';
            var reservation_date_time = data.reservation_detail.start_time.split(' ');
            $('#reservation-enquiry-details #enquiry_resv_id_title').html(' '+data.reservation_detail.id);
            $('#reservation-enquiry-details #enquiry_fname').val(data.reservation_detail.fname).next('span').addClass("hasVal");
            $('#reservation-enquiry-details #enquiry_lname').val(data.reservation_detail.lname).next('span').addClass("hasVal");
            $('#reservation-enquiry-details #enquiry_email').val(data.reservation_detail.email).next('span').addClass("hasVal");
            $('#reservation-enquiry-details #enquiry_mobile').val(data.reservation_detail.mobile).next('span').addClass("hasVal");
            $('#reservation-enquiry-details #enquiry_reservation_date').val(reservation_date_time[0]);
            $('#reservation-enquiry-details #enquiry_reservation_time').val(reservation_date_time[1]);
            $('#reservation-enquiry-details #enquiry_party_size').val(data.reservation_detail.reserved_seat);
            $.each(data.reservation_detail.extra_fields, function (key, value) {
                if(key!=undefined || key!==null){
                    key = key.replace(/\_/g, ' ');
                    extra_field+='<div class="row input__field fullWidth"><div class="input">'+value+'</div><span for="text" class="hasVal text-capitalize">'+key+'</span></div>';
                    //extra_field+='<div class="row input__fullWidth pointer-none uppercase"><span class="input__field fullWidth"><input type="text" value="'+value+'"><span for="text" class="hasVal">'+key+'</span></span></div>';
                }
            });
            $('#extra_field').html(extra_field);
        }
    });
});


$(document).on('click','.enquiry_fname',function(){
    var user_id = $("#user_id").val();
    
    if(user_id){
        var url = "/guestbook/detail/"+user_id;    
        getGuestbookDetail(url);    
    }
    return false;
});


$(document).on('click', '.catering_detail_enquiry', function () {
    $('#extra_field').empty();
    $("#enquiry_error").fadeOut();
    $('#replyrecord').fadeIn();
    var resv_id = this.id.split('_');   
    
    
    $('#reservation-enquiry-details').show();
    $.ajax({
        url: '/enquirydetail/' + resv_id[1],        
        dataType: 'json',
        success: function (data) {

            var userName = '';
            var extra_field='';
            $('#recevedOn').html(data.reservation_detail.created_date);
            $('.enquiry_resv_id_title').val(data.reservation_detail.id);

            userName = data.reservation_detail.first_name + " " + data.reservation_detail.last_name;
            
            
            
            $('.enquiry_email').html(data.reservation_detail.email).next('span').addClass("hasVal");
            var phone = "<a href='tel:"+ data.reservation_detail.phone +"' target='_top'><i class='fa fa-phone margin-right-5' aria-hidden='true'></i><span>" + data.reservation_detail.phone + "</span></a>";
            $('.enquiry_mobile').html(phone).next('span').addClass("hasVal");
                  
            
             //reply Data
            var replyResponse = "";
            
            if(data.replylog.response=="success"){
                
                $('#loginusername').val(data.loginuser.name);
                $('#loginuseremail').val(data.loginuser.email);
                $('#loginuser').html(data.loginuser.name+"&nbsp; &lt"+data.loginuser.email+"&gt");
                $('#user_id').val(data.user_id);
                
                if(data.user_id){
                    userName = '<strong><a class="enquiry_fname" url="'+location.hostname+'" style="cursor:pointer">'+userName+'</a></strong>';
                    $('.enquiry_fname').html(userName).next('span').addClass("hasVal");
                }else{
                    $('.enquiry_fname').html(userName).next('span').addClass("hasVal");
                }
                
                
                $.each(data.replylog.records, function (key, record) {
                    replyResponse+='<div class="cateringReplySection">';
                    $.each(record, function (key, value) {
                
                    replyResponse+='<div class="cateringReplyContent"><span>'+key+': </span><span>'+value+'</span></div>';
                                       
                });  
                replyResponse+='</div>'
            });            
           
            }else{
                replyResponse = '<div style="color:green">No anybody reply.</div>'
            }
            
            $('#replyrecord').html(replyResponse);
            
           
            $.each(data.reservation_detail.extra_fields, function (key, value) {                
                if(key!=undefined || key!==null){
                    if(key == 'enquiry_type'){
                        var toptext = value+" Enquiry";
                        $('.delBlockqoute-top span').html(toptext);
                        $('#enquirytype').val(value);
                    }
                    if(value){
                        key = key.replace(/\_/g, ' ');
                        extra_field+='<div class="row input__field"><div class="input" id="'+key+'">'+value+'</div><span for="text" class="hasVal text-capitalize">'+key+'</span></div>';
                    }
                }
            });
            
            $('#extra_field').html(extra_field);
        }
    });

    $('.emailToggleBox').slideUp();

    $('#reply, #emailActionReply').off('click');
    $('#reply, #emailActionReply').on('click', function(){
        $("#messageAll").show();
        $('#userdata').hide();
        $('#from').hide();
        $('#replyrecord').hide();
        var enquiryEmail = $('.enquiry_email').eq(0).text();

        if($('.emailToggleBox').slideUp()) {
            $('.emailToggleBox').slideDown();
            $('#email_message').attr('readonly', true);
            $('#email_message').val(enquiryEmail);
            $('#repFor').val('Reply');
            $('.enquiry_email').text();
            $('#messageAll').val('');
            $('#userdataEdit').text('');

        }
    });
    $('#forward').off('click');
    $('#forward').on('click', function(){
        $("#messageAll").hide();
        $('#userdata').show();
        $('#from').show();
        $('#replyrecord').hide();

        /*setTimeout( function() {
            var container = $('.modal-dialog'),
                scrollTo = $(".emailToggleBox", $('.cateringformdetails'));
            container.stop().animate({
                scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()+500
            });
           // $(".emailToggleBox").animate({ scrollTop: "0" });
        }, 1000);*/

       // $('.emailToggleBox').animate({scrollTop : 0},700);
        var FromEmail = $('.enquiry_email').eq(0).text();

        if($('.emailToggleBox').slideUp()){

            $('.emailToggleBox').slideDown();
            $('#email_message').attr('readonly', false);
            $('#email_message').val('');
            $('#repFor').val('Forward');

            var massage = '';
            massage += '<br />---------- Forwarded message ---------\n' +
                '<br /> From: ' + FromEmail +
                '<br /> Date: '+ $('#recevedOn').text() + '<br />';

            massage += ' <br />' + $('#userContectDetail').html() + ' <div class="clearfix"></div><br /><br />' + $('#extra_field').html();
            $('#userdataForward').html(massage);


        }
    });
    $('#btncancel').on('click', function() {
        $('.emailToggleBox').slideUp()
        $('#email_message').attr('readonly', false);
        $('#email_message').val('');
        $('#userdataForward').html('');
        $('#userdataEdit').text('');
        $('#enquiry_error').fadeOut();
        $('#replyrecord').show();
    })
});


function basename(path) {
   return path.split('/').reverse()[0];
}


$(document).on('click','#btnreply',function(){

    var replytype = $("#repFor").val();

    if(replytype == "Reply"){
        var message = $("#messageAll").val();
    }else{
        var message = $("#userdataEdit").html();
    }

    var email = $("#email_message").val();    
    var titleid = $("#enquiry_resv_id_title").val();

    var enquirytype = $("#enquirytype").val();
    var userName = $(".enquiry_fname").text();
 
    if(message.length < 3 || email.length < 10 || userName.length < 1){
        $("#enquiry_error").html("<span style='color:red'>Please check your message length greater than 3 or First name and Email should be correct.</span>").fadeIn();
        setTimeout(function() {
            $("#enquiry_error").fadeOut().empty();
        },5000);
        return false;
    }
    
    
     $.ajax({
        method:'post',
        url: '/enquiryreplymail',
        data:{'email':email,'id':titleid,'message':message,'type':enquirytype,'user_name':userName,'replytype':replytype},
        dataType: 'json',
        success: function (data) { 
          
           if(data.response=="success"){
                var replyResponse = "";
                replyResponse+='<div class="cateringReplySection" style=" border:1px solid green;">';
                    $.each(data, function (key, value) {   
                        if(value!="success")
                        replyResponse+='<div class="cateringReplyContent"><span>'+key+': </span><span>'+value+'</span></div>';
                    });     
                replyResponse+='</div>';
                $("#enquiry_error").html("<span style='color:green'>Massage has been sent.</span>").fadeIn();
                setTimeout(function() {
                    $("#enquiry_error").fadeOut().empty();
                },2000);
                
            }else{
              $("#enquiry_error").html("<span style='color:red'>Failed to reply.</span>").fadeIn();
               setTimeout(function() {
                   $("#enquiry_error").fadeOut().empty();
               },2000);
            }

            $('#successsend').prepend(replyResponse);

            setTimeout(function() {
                $('.cateringReplySection').fadeOut();
            },2000);
            $("#messageAll").val(null);
            $("#userdataEdit").text(null);
            $('.emailToggleBox').slideUp();
        }
    });
    return false;
  
});


$("#newWalkinModal").on("show.bs.modal", function () {
       $('body').addClass('overflow--visible');
       $('html,body').addClass('overflow--visible');
   });   
$("#newWalkinModal").on("hidden.bs.modal", function () {
       $('body').removeClass('overflow--visible');
       $('html,body').removeClass('overflow--visible');
   });

$("#newWaitlistModal").on("show.bs.modal", function () {
       $('body').css('overflow','visible');
       $('html,body').css('overflow','visible');
    console.log($('html,body').css('overflow','visible'));
   });   
$("#newWaitlistModal").on("hidden.bs.modal", function () {
       $('body').removeAttr("style");
       $('html,body').removeAttr("style");
   });


function openSibebarController(switcher){

    //alert(switcher)
    if (window.innerWidth >= 1280) {
        if (switcher) {
            //alert("AddClass "+switcher)side-section
            $("#tableManagement__wrapper").addClass("openSibebar");
        } else {
            //alert("RemoveClass "+switcher)
            if (!$(".side_bar_right").hasClass("active")) {
                $("#tableManagement__wrapper").removeClass("openSibebar");
                $("[data-popup=true]").removeAttr("data-popup")
            }

        }
    }
}

$(document).on('click', '.event_detail_enquiry', function () {
    $('#extra_field').empty();
    $("#enquiry_error").fadeOut();
    var resv_id = this.id.split('_');
    actionType =  $(this).attr("databtname");
    $('#reservation-enquiry-details').show();
    $.ajax({
        url: '/eventdetail/' + resv_id[1]+'?replytype='+actionType, 
        dataType: 'json',
        success: function (data) {
            var extra_field='';     
            $('#recevedOn').html(data.reservation_detail.created_date);
            $('.enquiry_resv_id_title').html(data.reservation_detail.id);
            $('.enquiry_fname').val(data.reservation_detail.first_name).next('span').addClass("hasVal");
            $('.enquiry_lname').val(data.reservation_detail.last_name).next('span').addClass("hasVal");
            $('.enquiry_email').val(data.reservation_detail.email).next('span').addClass("hasVal");
            $('.enquiry_mobile').val(data.reservation_detail.phone).next('span').addClass("hasVal");
            $('.event_date').val(data.reservation_detail.reservation_date);
            $('.event_time').val(data.reservation_detail.reservation_time);
            $('#btnreply').val(actionType);            
            
            if(actionType == "Reply"){
                $('#email_message').val(data.reservation_detail.email);               
            }else{
                $('#email_message').val("");
            }
            if(data.reservation_detail.reservation_time==''){
               $('.eventtimeouter').hide();
            }
            $('.event_party_size').val(data.reservation_detail.reserved_seat);
            
            
            //reply Data
            var replyResponse = "";
            
            if(data.replylog.response=="success"){
                
                $.each(data.replylog.records, function (key, record) {
                    replyResponse+='<div style="margin:10px;border-bottom:1px solid #000000">';
                    $.each(record, function (key, value) {
                
                    replyResponse+='<div style="height:21px;"><span style="float: left;color: #000000;font-size: 12px;font-weight: bold;">'+key+': </span><span style="float:left;margin-left:10px;color:#999999;font-size: 9px;">'+value+'</span></div>';
                                       
                });  
                replyResponse+='</div>'
            });            
           
            }else{
                replyResponse = '<div style="color:green">No body reply.</div>'
            }
            
            $('#replyrecord').html(replyResponse);
            
            
            //console.log(data.reservation_detail.extra_fields);
            $.each(data.reservation_detail.extra_fields, function (key, value) {
                console.log(key);
                if(key!=undefined || key!==null){
                    key = key.replace(/\_/g, ' ');
                    extra_field+='<div class="row input__field fullWidth"><div class="input" style=" white-space: pre-line">'+value+'</div><span for="text" class="hasVal text-capitalize">'+key+'</span></div>';
                   
                }
            });
            $('.extra_field').html(extra_field);
        }
    });
});
function getGuestListExport(keyword){
        var inputData = $('#guestfilters').serialize();
 window.location.href='?export=1&keyword=' + keyword+'&'+inputData;
 /*
        $.ajax(
            {
                 url: '?page=' + page + '&export=1&keyword=' + keyword,
                 type: 'get',
                 data: inputData,
                 datatype: "html",
               
            })
            .done(function(data)
            {


            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                //alert('No Guest found');
            });
            */
    }

$(".auth-ok").click(function () {
$("#authentication-form").valid();
});
$( "#authentication-form" ).validate({
rules: {
        'auth_name': {
            required: true,
        },
        'auth_password': {
            required: true,
        }
    },
        messages:{
            auth_name:"Please enter the user name.",
            auth_password:"Please enter the password."
       }
});

/* 10-10-2018 By RG  */
$(document).on('click','.export_guest', function() {
    var keyword =  $('#search input#keyword').val();
   getGuestListExport(keyword);
});

/*
 * server JS start
 */
/*var floor_and_tables;
var currentServerName;*/

function resetAddServerForm(){
    removeValidation("#addNewServer");

    if($('#addNewServer #fname, #addNewServer #lname, #addNewServer #email, #addNewServer #phone').val() == ""){
        $('#addNewServer input + span, #addNewServer input + label.error + span').removeClass('hasVal');
    }
}

$("#addNewServer button.saveCloseBtn").click(function () {
    $("#addNewServer").valid();
});

$("#addserver").on("show.bs.modal", function () {
    resetAddServerForm();
});

$("#addserver").on("hide.bs.modal", function () {
    $("#addserver #addNewServer").removeClass("addingServer");
});

$('#add_server_btn').on('click', function(){

    //console.log(server_list)

    $("#addNewServer input[name='color']").each(function(){
        var getColor = $(this).val().toLowerCase()
        $.each(server_list,function(key,value){
            if(getColor == value.color.toLowerCase()){
                $("#addNewServer input[value="+ value.color +"] + label").addClass("pointer-none")
            }
        })
    })

    $("#addserver #addNewServer").addClass("addingServer");
    document.getElementById('add-color-btn').jscolor.fromString('ffffff');
    $("#addserver #addNewServer").trigger("reset");
});

$.validator.addMethod("uniquePhoneNo", function(value, element) {
    var getPhoneNo = $("#addNewServer #mobile").val()
    return getUniqueMobNo(server_list,getPhoneNo)
});

function getUniqueMobNo(array,num) {
    var l = array.length;
    for (var k = 0; k < l; k++) {
        if($("#addNewServer").hasClass("addingServer")) {
            if (array[k].mobile == num) {
                return false;
            }
        } else {
            var getEditServerId = $("#addNewServer").attr("data-server-id")

            if((getEditServerId != array[k].id) && (array[k].mobile == num)){
                return false;
            }
        }
    }
    return true;
}

function addCustomColorChecked(){
    $('#customServerColor').attr('checked','checked');
    $("#addNewServer").valid();
}

$( "#addNewServer" ).validate({
    rules: {
        'fname': {
            required: true,
        },
        'lname': {
            required: true,
        },
        'mobile': {
            required: true,
            maxlength: 10,
            uniquePhoneNo:true
        },
        'color': {
            required: true
        }
    },
    messages:{
        fname:"Please enter the server's first name.",
        lname:"Please enter the server's last name.",
        mobile:{
            required: "Please enter a valid 10 digit mobile number.",
            uniquePhoneNo: "Please enter a unique mobile number."
        },
        color:"Please choose the server's color."
    },
    errorPlacement: function(error, element){
        if ( element.is(":radio") )
        {
            error.appendTo( element.parents('.pick-color') );
        }
        else
        { // This is the default behavior
            error.insertAfter( element );
        }
    }
});

$('#addserver #addNewServer .saveCloseBtn').on('click', function (e) {
    var $form = $('#addNewServer');
    if(!$form.valid()){
        return false;
    }
    var inputData = $form.serialize();
    var server_id = $("#server_id").val();
    var method_type = 'post';
    var method_url = '/server/add';
    if(server_id>0){
        method_type = 'put';
        method_url = '/server/update/'+server_id;
    }
    $.ajax({
        type: method_type,
        url: method_url,
        data: inputData,
        success: function (data) {
            alertbox('Success',data.message, function(modal){
                setTimeout(function(){
                    window.location.reload();
                },1000)
            });
        },
        error: function (data, textStatus, errorThrown) {

            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){
            });
        },
        complete: function(data) {
            $('#addserver').modal('hide');
        }
    });
});

function editServer(server_id) {
    $.ajax({
        url: '/server/' + server_id,
        dataType: 'json',
        success: function (data) {
            $("#addNewServer").attr("data-server-id",server_id)
            $('#addserver #fname').val(data.data.fname);
            $('#addserver #lname').val(data.data.lname);
            $('#addserver #mobile').val(data.data.mobile);
            $('#addserver #email').val(data.data.email);
            $('#addserver #server_id').val(data.data.id);
            var is_custom_color = false;
            $('input[name^="color"]').each(function(){
                if($.trim(data.data.color)==$.trim($(this).attr('value'))){
                    is_custom_color = true;
                    $("input[name=color][value='" + data.data.color + "']").prop('checked', 'checked');
                }
            });
            if(is_custom_color===false){
                $('#customServerColor').val(data.data.color).prop('checked', 'checked');
                document.getElementById('add-color-btn').jscolor.fromString(data.data.color);
            } else {
                document.getElementById('add-color-btn').jscolor.fromString('ffffff');
            }
            if($('#addNewServer #fname, #addNewServer #lname, #addNewServer #email, #addNewServer #phone').val() != "") {
                $('#addNewServer input + span, #addNewServer input + label.error + span').addClass('hasVal');
            }
            $("#addserver #addNewServer").removeClass("addingServer");
        },
        error: function (data, textStatus, errorThrown) {
            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){
            });
        },
        complete: function(data) {
            $('#addserver').modal('show');
        }
    });
}

function deleteServer(server_id) {
    $.ajax({
        type: 'DELETE',
        url: '/server/delete/' + server_id,
        dataType: 'json',
        success: function (data) {
            alertbox('Success',data.message, function(modal){
                setTimeout(function(){
                    window.location.reload();
                },1000)
            });
        },
        error: function (data, textStatus, errorThrown) {
            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){
            });
        },
        complete: function(data) {

        }
    });
}

window.getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

$("#serverdatepicker").datepicker({
    dateFormat: 'mm-dd-yy',
    minDate: 0,
    onSelect: function(selected,evnt) {
        getSlotNames(selected);
        getServerList();
    }
}).datepicker("setDate", "0");

var getAssignDateURL = getUrlParameter('assign_date')
var getAssignSlotURL = getUrlParameter('slot_id')

$("#serverdatepicker_assign").datepicker({
    dateFormat: 'mm-dd-yy',
    minDate: 0,
    onSelect: function(selected,evnt) {
        getSlotNames(selected);
        getServerAssignedTable();
    }
}).datepicker("setDate", getAssignDateURL)



function getSlotNames(assign_date) {
    $('#slot_name').html('').selectpicker('refresh');
    $("#loader").removeClass('hidden')
    $.ajax({
        type: 'get',
        url: '/server/slot-name-by-date/' + assign_date,
        dataType: 'json',
        async: false,
        success: function (data) {

            if(data.length > 0) {

                //if((getAssignDateURL == $("#serverdatepicker_assign").val()) == false){
                    $(".server-tables-assignment .add_tables_btn a").removeClass('pointer-none');
                //}

                $(".server_list a.assign_table").removeAttr('disabled').removeClass('pointer-none');
                $(".server-calendar .food-type .list, .server-calendar .food-type .assign").removeClass('hide')
                $(".server-calendar .food-type #slotValidation").addClass('hide')
            } else {
                $(".server_list a.assign_table, .server-tables-assignment .add_tables_btn a").attr('disabled', 'disabled').addClass('pointer-none');
                $(".server-calendar .food-type .list, .server-calendar .food-type .assign").addClass('hide')
                $(".server-calendar .food-type #slotValidation").removeClass('hide')
            }

            var optionHtml = '';
            var slotValArray = []
            $.each(data, function (key, value) {
                optionHtml+='<option value="'+value.slot_id+'">'+value.slot_name+'</option>';
                slotValArray.push(value.slot_id)
            });

            $('#slot_name').html(optionHtml).selectpicker('refresh');

            if(getAssignSlotURL != "") {
                if((getAssignDateURL == $("#serverdatepicker_assign").val()) == true){
                    if(slotValArray.indexOf(getAssignSlotURL) != "-1"){
                        $('#slot_name').selectpicker('val', slotValArray[0]);
                    } else {
                        $('#slot_name').selectpicker('val', getAssignSlotURL);
                    }
                } else {
                    $('#slot_name').selectpicker('val', slotValArray[0]);
                }
            }

            $("#loader").addClass('hidden')
        },
        error: function (data, textStatus, errorThrown) {
            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            alertbox('Error',err,function(modal){
            });
            $("#loader").addClass('hidden')
        },
        complete: function(data) {
            $("#loader").addClass('hidden')
        }
    });
}

getSlotNameValidation();
function getSlotNameValidation(){
    if($("#serverdatepicker, #serverdatepicker_assign").length){
        var getDate = $("#serverdatepicker, #serverdatepicker_assign").val()
        getSlotNames(getDate)
    }
}

$('#assign_server').on('click', function (e) {
    e.preventDefault();

    if($(this).attr('disabled') == "disabled"){
        return false;
    }

    var server_id = $('#server_id').val();
    var floor_id = $('#floor_name').val();
    var slot_id = $('.assign #slot_name').val();
    var slot_name = $('.assign #slot_name option:selected').html();
    var assign_date = $('#serverdatepicker_assign').val();
    var tables='';
    if(finalSelectedTablesIdForServers!==null && finalSelectedTablesIdForServers!==undefined && finalSelectedTablesIdForServers!=''){
        tables = JSON.stringify(finalSelectedTablesIdForServers)
    }
    var inputData = 'tables='+tables+'&server_id='+server_id+'&floor_id='+floor_id+'&slot_id='+slot_id+'&slot_name='+slot_name.trim()+'&assign_date='+assign_date;
    $.ajax({
        type: 'POST',
        url: '/server/table-assign',
        data: inputData,
        success: function (data) {
            alertbox('Success',data.message, function(modal){
            setTimeout(function(){
                window.location.href = "/server/listing";
            },1000)
            });
        },
        error: function (data, textStatus, errorThrown) {
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            alertbox('Error', err, function (modal) {
            });
        },
        complete: function (data) {
            //$('#addserver').modal('hide');
        }
    });
});

$(document).on('click', 'a.assign_table', function(e){
    e.preventDefault();
    $("#serverdatepicker_assign").val($("#serverdatepicker").val());
    var oldUrl = $(this).attr("href"); // Get current url
    var newUrl = oldUrl+'&slot_id='+ $('.list #slot_name').val();
    window.location.href = newUrl;
});


$('.assign #slot_name, #floor_name').on('change', function(e){
    e.preventDefault();
    getServerAssignedTable();
});

$('.list #slot_name').on('change', function(e){
    e.preventDefault();
    getServerList();
});

function getServerList(){
    var slot_id = $('.list #slot_name option:selected').val();
    var slot_name = $('.list #slot_name option:selected').html();
    if(slot_name!=undefined || slot_name!=null){
        slot_name = slot_name.trim()
    }
    var assign_date = $('#serverdatepicker').val();
    var inputData = 'slot_id='+slot_id+'&slot_name='+slot_name+'&assign_date='+assign_date;
    $.ajax(
        {
            url: '/server/listing',
            type: 'get',
            data: inputData,
            datatype: "html",
            // beforeSend: function()
            // {
            //     you can show your loader
            // }
        })
        .done(function(data)
        {
            $(".server_list").empty().html(data);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
            //alert('No Guest found');
        });
}

$('.giftcard-listing-page #gift_export').on('click', function() {
    //var type = $(this).attr('rel').split('-');
    //var keyword =  $('#key_search').val();
    //var order_status = $('.selectpicker').val();
    //var order_type = type['0'];
    //var status =  type['1'];
    var order_type = $('.giftcard-listing-page #order_type').val();
    var key_search = $('.giftcard-listing-page #key_search').val();
    var gift_type = $(this).attr('rel');

    window.open(
        '/order-gift-export/?key_search='+key_search+'&order_type='+order_type+'&gift_type='+gift_type,
        '_blank' // <- This is what makes it open in a new window.
    );
    //window.location.href = '/order-gift-export/?key_search='+key_search+'&order_type='+order_type+'&gift_type='+gift_type;
});

$('#giftcard-container .send-email-gift-coupon').on('click', function() {
    $('#loader').removeClass('hidden');
    console.log('send email');
    var order_id = $(this).data('order-id');
    var order_detail_id = $(this).data('order-detail-id');
    var action = 'confirmed';

    var send_all = $(this).attr('send_all');
    var button_container = $(this).parents('.gift-action-button');
    var id = $(this).attr('id');
    $.ajax({
        type: 'PUT',
        url: '/order/' + order_id + '/' + action,
        data: 'oid=' + order_id + '&action=' + action+ '&send_all=' + send_all+ '&id=' + id + '&order_detail_id='+order_detail_id+'&gift_order=1',
        success: function (data) {
            //$('.action_btn').remove();
            $('#loader').addClass('hidden');
            console.log(data);
            if(data.data == 1) {
                if(send_all == 0) {
                    // update button div
                    var button_html = '<a href="javascript:void(0)" data-order-id="" data-order-detail-id="" send_all="0" disabled="disabled" class="btn btn__primary fullWidth font-weight-700 " id="">Email eGift Card</a>';
                    button_container.html(button_html);
                    var all_gift_done_flag = true;
                    $('#giftcard-container .gift-action-button').each(function() {
                        if(!$(this).find('a').is('[disabled=disabled]')) {
                            console.log('not disabled');
                            all_gift_done_flag = false;
                        }
                    });
                    if(all_gift_done_flag) {
                        $('#giftcard-container .send_all_btn').removeClass('send-email-gift-coupon').attr('disabled', true).prop("onclick", null).off("click");
                    }
                    alertbox('Success',data.message, function(modal){
                    });
                } else {
                    // reload page
                    alertbox('Success',data.message, function(modal){
                        setTimeout(function(){
                            window.location.reload();
                        },2000)
                    });
                }

            }
        },
        error: function (data, textStatus, errorThrown) {
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            alertbox('Error', err, function (modal) {
            });
        }
    });
});


function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

$(function() {
    //Plug-in function for the bootstrap version of the multiple email
    if($(".invite_emails").length){
        $('.invite_emails').multiple_emails({position: "top"});
    }
});
    
$("#invite_send").click(function () {
    $("#invite_form").valid();
});


$( "#invite_form" ).validate({
rules: {
        'invite_emails': {
            required: true,
        },
        'personalized_msg': {
            required: true,
        },
        multiple_emails_input :{
                email: true
            }
    },
        messages:{
            invite_emails:"Please enter a valid email address.",
            personalized_msg:"Please enter the Message."
       }
});

/**
 * Suggestions for various table combinations
 */
/*function getTableCombinationSuggestions() {
    if (typeof tableManagement[1] !== 'undefined') {
        //console.log(tableManagement[1]);
        var combined_table_ids  = '';
        var combined_tables     = '';
        var combined_min        = 0;
        var combined_max        = 0;
        $.each(tableManagement[1], function (key, value) {
            //console.log(value);
            combined_tables     += value.tableText + ' + ';
            combined_table_ids  += value.id + ',';
            combined_min        += parseInt(value.minGuests);
            combined_max        += parseInt(value.maxGuests);
        });
        if (combined_tables.length) {
            combined_table_ids  = combined_table_ids.substr(0, combined_table_ids.length - 1);
            combined_tables     = combined_tables.substr(0, combined_tables.length - 1);
            console.log(combined_tables + ' - ' + combined_min + ' - ' + combined_max);

            var suggestion_html = '<ul class="combination-list"><li id="'+combined_table_ids+'">' + combined_tables + '</li><li>' + combined_min + '</li><li>' + combined_max +
                '</li><li><button type="button" class="btn btn-sm btn-secondary suggestion-add-btn btn-toggle" title="Open" data-toggle="button" aria-pressed="false" autocomplete="off">' +
                '<div class="handle"></div></button></li>';
            $('#floorCreateProperties #table-combination-list').html(suggestion_html);
        }
    } else {
        console.log("tables don't exists");
    }
}*/

$('#floorCreateProperties').on('click', '#table-combination-list .suggestion-add-btn', function() {
    console.log('clicked');
    if(!$(this).hasClass('active')) {
        var add_html        = $(this).parents('ul.combination-list')[0];
        var parsed_table_ids= $(add_html).find('li:eq(0)').attr('id');
        var parsed_tables   = $(add_html).find('li:eq(0)').html();
        var parsed_min      = $(add_html).find('li:eq(1)').html();
        var parsed_max      = $(add_html).find('li:eq(2)').html();
        console.log(parsed_tables + ' - ' + parsed_min + ' - ' + parsed_max);

        var add_html = '<ul class="combination-list suggestion"><li id="'+parsed_table_ids+'">' + parsed_tables + '</li><li>' + parsed_min + '</li><li>' + parsed_max +
            '</li><li><li><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></li>';

        $(this).parents('ul.combination-list')[0].remove();
        $('#floorCreateProperties #table-combination-add-container').append(add_html);
    }
});

$('#floorCreateProperties').on('click', '#table-combination-add-container .fa-times', function() {
    console.log('x clicked');
    var remove_html = $(this).parents('ul.combination-list')[0];
    if($(remove_html).hasClass('suggestion')) {
        console.log($(remove_html).hasClass('suggestion'));
        // move from added list to suggestions
        var parsed_table_ids    = $(remove_html).find('li:eq(0)').attr('id');
        var parsed_tables       = $(remove_html).find('li:eq(0)').html();
        var parsed_min          = $(remove_html).find('li:eq(1)').html();
        var parsed_max          = $(remove_html).find('li:eq(2)').html();

        var suggestion_html = '<ul class="combination-list"><li id="' +parsed_table_ids+ '">' + parsed_tables + '</li><li>' + parsed_min + '</li><li>' + parsed_max +
            '</li><li><button type="button" class="btn btn-sm btn-secondary suggestion-add-btn btn-toggle" title="Open" data-toggle="button" aria-pressed="false" autocomplete="off">' +
            '<div class="handle"></div></button></li>';

        $(this).parents('ul.combination-list')[0].remove();
        $('#floorCreateProperties #table-combination-list').append(suggestion_html);
    } else {
        // not a suggestion, from DB, so don't do anything
    }
});

/*********************Block table JS Start ***********************/
/*$('#block_floors').on('change', function() {
    var floor_id = $(this).val();
    $.ajax({
        type: 'GET',
        url:  floor_id + '/tables',
        success: function (data) {
            var tableHtml = '<option value="">Select Table</option>';
            $.each(data.data, function (key, table) {
                tableHtml += '<option value="' + table.id + '">' + table.name + '</option>';
            });
            $('#block_tables').html(tableHtml);
            $(".selectpicker").selectpicker('refresh');
        },
        error: function (data, textStatus, errorThrown) {
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            alertbox('Error', err, function (modal) {
            });
        }
    });
});*/
$('#block_floor').on('change', function() {
    var floor_id = $(this).val();
    var selectedTable,selectedTableText;

    if($(this).val() == ""){
        $("#block_tables").selectpicker('deselectAll');
    }

    $.each(walk_in_floor_data, function(key, floor){
        if(floor_id == floor.id) {
            if(floor.tables) {
                var tableHtml = '<option value="">Select Table</option>';
                $.each(floor.tables, function(table_key, table_value) {
                    if(floorFound && table_value.max == party_size){
                        selectedTable = table_value.id;
                        selectedTableText = table_value.name;
                    } else if(table_value.max == party_size) {
                        selectedTable = table_value.id;
                        selectedTableText = table_value.name;
                    }
                    tableHtml += '<option value="'+table_value.id+'">'+table_value.name+'</option>';
                });
                if(floorFound || selectedTable) {
                    var table_to_select = selectedTable;
                }
                $('#block_tables').html(tableHtml);
                if(!isNaN(table_to_select)){
                    $('#block_tables').val(table_to_select);
                }
                $(".selectpicker").selectpicker('refresh');
            }
        }
    });
});

$('#check_block_avail_button').on('click', function () {

    var $form = $('#table-block');
    if(!$form.valid()){
        return false;
    }

    $('.message_div').empty();
    $(this).attr('disabled', true);
    $('#loader').removeClass('hidden');
    //if($('#block_resume_time').valid()){
        var block_date=$('.floor_fetch #datepicker').val();
        var formdata = $('#table-block').serialize();
        $.ajax({
            type: 'POST',
            url: '/block-table/check-availability',
            data: formdata+'&block_date='+block_date,
            dataType: 'json', //mispelled
            success: function (resp) {
                //Clear Selected Table After Getting Availability
                enableDoubleClick = true;
                clearTableSelection();
                $('#loader').addClass('hidden');
                $('.floor-table-block').show();
                $("#check_block_avail_button").hide();
                walk_in_floor_data = resp.data;
                var floorHtml = '<option value="">Select Floor</option>';
                var current_floor = parseInt(floor_url.split("/floor/")[1]);
                $.each(walk_in_floor_data, function (key, floor) {
                    floorHtml += '<option value="' + floor.id + '">' + floor.name + '</option>';
                    if (parseInt(floor.id) == current_floor && !floorFound) {
                        floorFound = true;
                    }
                });
                $('#block_floor').html(floorHtml);
                $('#block_tables').html('<option value="">Select Table</option>');
                $('#block_floor').trigger('change');
                setTimeout(function () {
                    $(".selectpicker").selectpicker('refresh');
                }, 10);
            },
            error: function (data, textStatus, errorThrown) {
                $('#loader').addClass('hidden');
                $('.floor-table-block').hide();
                var err = '';
                floorFound = false;
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });
                $('.message_div').empty().html(err).show();
            }
        });
    //}
});


// add table combination list
/*$('#floorCreateProperties').on('click', '#table-combination-add', function() {
    console.log('add clicked');
    var restaurant_id   = 1;
    var floor_id        = 190;
    var data_arr        = [];

    $('#table-combination-add ul.combination-list').each(function() {
        var table_ids = $(this).find('li:eq(0)').attr('id');

        if(table_ids.length) {
            var table_min = $(this).find('li:eq(1)').html();
            var table_max = $(this).find('li:eq(2)').html();
            if(table_min.length) { table_min = parseInt(table_min); }
            if(table_max.length) { table_max = parseInt(table_max); }
            data_arr.push({
                restaurant_id   : restaurant_id,
                floor_id        : floor_id,
                table_ids       : table_ids,
                min             : table_min,
                max             : table_max
            });
        }
    });
    console.log(data_arr);
});*/
function resumeBooking(table_id, block_date) {
    $.ajax({
        type: 'POST',
        url: '/block-table/resume-booking',
        data: 'table_id=' + table_id + '&blocked_date=' + block_date,
        dataType: 'json', //mispelled
        success: function (resp) {
            alertbox('Success', resp.message, function (modal) {
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            });
        },
        error: function (data, textStatus, errorThrown) {
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            alertbox('Error', err, function (modal) {

            });
        }
    });
}

$( "#table-block" ).validate({
    rules: {
        block_start_time: {
            required: true
        },
        block_resume_time: {
            required: true
        },
        'block_floor': {
            required: true
        },
        'block_tables[]': {
            required: true
        }
    },
    messages:{
        block_start_time :"Please choose table block start time.",
        block_resume_time:"Please choose a table block resume time."
    },
    errorPlacement: function(error, element){
        if ( element.is("#blockTables-resume-timepicker") )
        {
            error.appendTo( element.parents('.resume-booking-table') );
        }
        else
        { // This is the default behavior
            error.insertAfter( element );
        }
    }
});

$('.confirm-block').on('click', function(){
    if(!$("#table-block").valid()){
        return false
    }
    $('.message_div').empty();
    $(this).attr('disabled', true);

    $('#loader').removeClass('hidden');
    var block_date = $('.floor_fetch #datepicker').val();

    var formdata = $('#table-block').serialize();
    $.ajax({
        type: 'POST',
        url: '/block-table/save',
        data: formdata + '&block_date=' + block_date,
        dataType: 'json', //mispelled
        success: function (resp) {
            //Clear Selected Table After Getting Availability
            enableDoubleClick = true;
            clearTableSelection();
            openSibebarController(false)
            $('#loader').addClass('hidden');
            $('.floor-table-block').show();
            $("#check_block_avail_button").hide();
            alertbox('Success', resp.message, function (modal) {
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            });
        },
        error: function (data, textStatus, errorThrown) {
            $('#loader').addClass('hidden');
            $('.floor-table-block').hide();
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            $('.message_div').empty().html(err).show();
        }
    });
});

/****************Block table js end *******************/

$("#cancel-reservation").on("hidden.bs.modal", function () {
    if($("#tableManagement__wrapper").length){
        floorGridViewUpdate()
    }
});

$(window).on( "orientationchange", function( event ) {
    openSibebarController(false)
    $('#reservationdeatails-datepicker,#reservation_date,#dob,#doa,#datepicker,#datepickerStart,#datepickerEnd').datepicker('hide');
    if($('div[data-toggle="popover"]').length) {
        $('div[data-toggle="popover"]').popover('disable');
    }
});
function preview(img, selection) {
    var scaleX = 100 / (selection.width || 1);
    var scaleY = 100 / (selection.height || 1);

    $('body #imagecroperbox .preview_final > img').css({
        width: Math.round(scaleX * 400) + 'px',
        height: Math.round(scaleY * 300) + 'px',
        marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
        marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
    });

}
$(".imagecroper").change(function(){

   // alert($(this).attr('name'));
    console.log(this);
    var p = $("#previewimage");
    if(!$("#imagecroperbox").length){
    $('body').append('<div id="imagecroperbox" class="modal fade" role="dialog"> <div class="modal-dialog modal-md"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header">Image Upload <button type="button" class="close" data-dismiss="modal">&times;</button> </div> <div class="modal-body"><div class="row padding-top-20 padding-bottom-20"> <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 firstImgArea"> <img id="previewimage" style="display:none;" src="" alt="Image"> </div></div></div> <div class="modal-footer">        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button> </div> </div> </div> </div>');
    }else{
        $('body').append('<div id="imagecroperbox" class="modal fade" role="dialog"> <div class="modal-dialog modal-md"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header">Image Upload <button type="button" class="close" data-dismiss="modal">&times;</button> </div> <div class="modal-body"><div class="row padding-top-20 padding-bottom-20"> <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 firstImgArea"> <img id="previewimage" style="display:none;" src="" alt="Image"> </div></div></div> <div class="modal-footer">        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button> </div> </div> </div> </div>');

        // $('body').append('<div id="imagecroperbox" class="modal fade" role="dialog"> <div class="modal-dialog modal-lg"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header">Image Upload <button type="button" class="close" data-dismiss="modal">&times;</button> </div> <div class="modal-body"><div class="row padding-top-20 padding-bottom-20"> <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12 firstImgArea"> <img id="previewimage" style="display:none;" src="" alt="Image"> </div> <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 secondImgArea preview_final" > <img src="" alt="Image"> </div> </div></div> <div class="modal-footer">        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button> </div> </div> </div> </div>');


    }
    $('#imagecroperbox').modal('show');


    var imageReader = new FileReader();
    imageReader.readAsDataURL(this.files[0]);

    imageReader.onload = function (oFREvent) {
        $("body #previewimage").attr('src', oFREvent.target.result).fadeIn();
       // $("body .preview_final img").attr('src', oFREvent.target.result).fadeIn();
    };

    var axis=$(".imagecroper").attr('name')+String('_axis');
    var nextelement=$(".imagecroper").next();
    $('#previewimage').imgAreaSelect({
       // maxWidth: 250,
       // maxHeight: 250,
       // minHeight: 250, //Edit here
        //minWidth: 250 ,  //Edit here
        onSelectEnd: function (img, selection) {

            nextelement.val(String(selection.x1)+','+String(selection.y1)+','+String(selection.width)+','+String(selection.height));

            /*  $('input[name="x1"]').val(selection.x1);
              $('input[name="y1"]').val(selection.y1);
              $('input[name="w"]').val(selection.width);
              $('input[name="h"]').val(selection.height);*/
        },onSelectChange: preview
    });

    $('body #imagecroperbox').on('hide.bs.modal', function () {
        $('#previewimage').imgAreaSelect( {remove: true} );
    });
});
