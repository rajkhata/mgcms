$(document).on('click', '.modal-backdrop', function () {
    hideBackDrop()
});

$(document).keyup(function (e) {
    /*if (e.keyCode == 27) {
        hideBackDrop()
        $('#alertbox,#confirmboxcommon, #refundOrder').modal('hide');
    }*/
});



window.checkHasVal = function(ele){
    $(ele).each(function (key, value) {
        console.log($(this).val())

        if ($(this).val()) {
            $(this).next().addClass('hasVal')
        } else {
            $(this).next().removeClass('hasVal')
        }
    });
}

function hideBackDrop() {
    if (!($('#IsdashboardAnnouncementSet').length)) {

        $(".modal-backdrop").removeClass('in');
        setTimeout(function () {
            $(".modal-backdrop").remove();
        }, 400)

    }
}

$(document).ready(function () {

    if ($("#customtagtab").length && ($("#customtagtab + .tab-content > #predefined_tags").hasClass('active'))) {
        $("#customtagtab + .tab-content > #custom_tags").removeClass('active')
    }

    window.options = {
        theme: "dark",
        axis: "y",
        scrollInertia: 0,
        callbacks: {
            onScroll: function () {
                $('#person #guest_tags_booking + ul.tagit input[type=\'text\']').prop('disabled', false)
                $('#person1 #guest_tags_booking + ul.tagit input[type=\'text\']').prop('disabled', false)
            },
            onScrollStart: function () {

                if ($('#reservationdeatails-datepicker').length) {
                    $('#reservationdeatails-datepicker').datepicker('hide').blur()
                    $('#reservationdeatails-timepicker').timepicker('hide')
                }

                if ($('#book-a-table.fade').hasClass('in')) {
                    $('#reservation_date,#dob,#doa').datepicker('hide')
                }

                $(".fname_booking").autocomplete("close");
                $('#person #guest_tags_booking + ul.tagit input[type=\'text\']').prop('disabled', true)
                $('#person1 #guest_tags_booking + ul.tagit input[type=\'text\']').prop('disabled', true)

                if ($('#reservation-details.fade').hasClass('in')) {
                    $('#detail_dob,#detail_doa,#reservationdeatails-datepicker').datepicker('hide').blur()
                    $('#reservationdeatails-datepicker').datepicker('hide')
                }
            }
        },
        advanced: {
            autoScrollOnFocus: false
        },
        onTotalScrollOffset: 100,
        alwaysTriggerOffsets: false
    };

    if ($('.input__field input').val()) {
        $('.input__field input').next().addClass("hasVal");
    }

    var options2 = {
        theme: "dark",
        axis: "x"
    };

    /*$("#custom-tags-scroller").mCustomScrollbar(options)

window.openSidebar = function(){
    alert("openSidebar")
}

window.closeSidebar = function(){
    alert("closeSidebar")
}

$("#custom-tags-scroller").mCustomScrollbar("disable");

  // Header Spacer
  //var header_height = $(".navbar-laravel").outerHeight();
  //$(".nav__spacer").css("height", header_height);

                $("#custom-tags-scroller").mCustomScrollbar("update")

            }

        }
    })*/

    function hexToRgb(hex, alpha) {
        hex = hex.replace('#', '');
        var r = parseInt(hex.length == 3 ? hex.slice(0, 1).repeat(2) : hex.slice(0, 2), 16);
        var g = parseInt(hex.length == 3 ? hex.slice(1, 2).repeat(2) : hex.slice(2, 4), 16);
        var b = parseInt(hex.length == 3 ? hex.slice(2, 3).repeat(2) : hex.slice(4, 6), 16);
        if (alpha) {
            return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
        } else {
            return 'rgb(' + r + ', ' + g + ', ' + b + ')';
        }
    }

    $("#custom-tags-scroller").mCustomScrollbar("disable");
    $(".subRowContainer").mCustomScrollbar({
        theme: "dark",
        axis: "y",
    });

    // Header Spacer
    //var header_height = $(".navbar-laravel").outerHeight();
    //$(".nav__spacer").css("height", header_height);

    // custom scroll bar
    $(".activities__container").mCustomScrollbar(options);
    //$(".reservationStatusPanel").mCustomScrollbar(options);
    $(".scroller_tags__otherInformation").mCustomScrollbar(options);
    $(".scroller").mCustomScrollbar(options);
    //  $("#Upcoming").mCustomScrollbar(options);
    $("#newBooking__popup .tags__otherInformation").mCustomScrollbar(options);

    // $(".ui-timepicker-wrapper").on("click", function() {
    //   $(".ui-timepicker-wrapper").mCustomScrollbar(options);
    // });

    // $(".content-container").mCustomScrollbar(options2);

    // custom select
    //$(".select2-selection__rendered.category").select2();
    //$(".select2-selection__rendered.year").select2();

    // On nav bar Open
    $(".open_rightBar").on("click", function () {

        if ($("#tableManagement__wrapper").hasClass("floor-tables-management")) {
            popoverHints();
        }

        $(".open_rightBar").animate(
            {
                right: "0px"
            },
            100,
            function () {
                if (window.innerWidth <= 1280) {
                    $(".side_bar_right").css("position", "fixed");
                    $(".open_rightBar").show();
                    //$(".side_bar_right").show();
                    $(".side_bar_right").addClass("active");
                } else {
                    $(".side_bar_right").css("position", "fixed");
                    //$(".side_bar_right").show();

                    if (!$("#tableManagement__wrapper").hasClass("openSibebar")) {
                        openSibebarController(true)
                        $(".side_bar_right").addClass("active");
                    }
                }
                positionHandler();
            }
        );
    });

    // on nav bar close
    $(".close__sidebar").on("click", function () {
        //  $(".side-section").on("click", function() {
        if (window.innerWidth < 1280) {
            /*setTimeout(function() {
                        $(".open_rightBar").show();
                      }, 300);*/
            $(".side_bar_right").removeClass("active");
        } else {
            //$(".side_bar_right").hide();

            if ($("#tableManagement__wrapper").hasClass("openSibebar")) {
                openSibebarController(false)
                $(".side_bar_right").removeClass("active");
            }
            setTimeout(function () {
                $(".open_rightBar").animate(
                    {
                        right: "0px"
                    },
                    100
                );
            }, 200);
        }

        if ($("#tableManagement__wrapper").hasClass("openSibebar")) {
            openSibebarController(false)
            $(".side_bar_right").removeClass("active");
        }
    });

    //alert($("html").attr('class'))

    // Checking for active class on navbar

    $(".side-item.selected").find(".sub_menu").addClass("clicked");
    $(".side-item.selected").find(".acc-icon-open").css("display", "none");
    $(".side-item.selected").find(".acc-icon-close").css("display", "inline-block");

    // function calls
    lastUser();
    addDownArrow();
    positionHandler();
    setTimeout(function () {
        ifHeightChange();
    }, 20000);

    $(".details__customer .cancel_edit_guest_details").on("click", function () {
        if (!$(".edit-field.autoExpand").val().length == 0) {
            $(this)
                .parents(".guest-note")
                .addClass("have_value");
        } else {
            $(this)
                .parents(".guest-note")
                .removeClass("have_value");
        }
        console.log($(".edit-field.autoExpand").val().length);
    });

//  checkForInit();

    $("select#table_type").change(function () {
        var selectedval = $("select#table_type option:selected").val();
        console.log('hii' + selectedval);
        if (selectedval === "") {
        } else {
            $('label[for="table_type"]').remove();
        }

    });

});


// on window resize
$(window).resize(function () {
    positionHandler();
    $(".activities__container").mCustomScrollbar("update");
    $(".reservationStatusPanel").mCustomScrollbar("update");
});

function lastUser() {
    var reservationContainer = $(".reservationStatusPanel");
    $.each(reservationContainer, function (index, value) {
        var dropDownArray = $(value).find(".dropdown__text_ul");

        // console.log(dropDownArray);

        $.each(dropDownArray, function (ind, elem) {
            // console.log(ind);
            if (dropDownArray.length - 1 == ind && dropDownArray.length - 1 != 0) {
                $(elem).addClass("top-postion");
            }
        });
    });
}

//right side bar position handler

function positionHandler() {
    if ($(".side_bar_right").hasClass("active")) {
        if (window.innerWidth <= 1280) {
            openSibebarController(false)
        } else {
            openSibebarController(true)
        }
    }
}

$(window).resize(positionHandler());

// Adding down arrow

function addDownArrow() {
    var sideItems = $(".side-item");

    $.each(sideItems, function (index, val) {
        if (
            $(this)
                .children()
                .hasClass("sub_menu")
        ) {
            $(this).addClass("accordion");
        }
    });
}

function currentTime() {
    // getting current date
    var date = new Date();
    // getting hrs in required format
    var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 12 ? "PM" : "AM";
    var minutes =
        date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();

    hours = hours < 10 ? "0" + hours : hours;
    // inserting data into html
    $(".current__time-hour").html(hours);
    $(".current__time-minute").html(minutes);
    $(".pm-am").html(am_pm);
}

$(document).on("hidden.bs.modal", ".modal", function () {
    $(".modal:visible").length && $(document.body).addClass("modal-open");
});

// active class switcher for sideBar
$(".status_tab").on("click", function () {
    $(".status_tab").removeClass("active");
    $(this).addClass("active");
});

// cancel reservation popup ui
$(".reservation__popup .btn__cancel").on("click", function () {
    $(
        ".scroller_tags__otherInformation , .special__otherInformation ,.reservation__popup .btn__primary, .optional__otf"
    ).hide();
    $(".ot-inf, .reason__otherInformation").show();
    $(".btn__cancel").addClass("cancel__mode");
});

$(".ot-inf").on("click", function () {
    $(
        ".scroller_tags__otherInformation  , .special__otherInformation ,.reservation__popup .btn__primary , .optional__otf"
    ).show();
    $(".ot-inf, .reason__otherInformation").hide();
    $(".btn__cancel").removeClass("cancel__mode");
});

$(".radio__cancel").on("change", function () {
    $(".reason").removeClass("reason__selected");

    $(this)
        .parent()
        .addClass("reason__selected");
    console.log($(this).attr("id"));
    if ($(this).attr("id") == "other_cancel") {
        $(".text__areaReason").show();
    } else {
        $(".text__areaReason").hide();
    }
});

// Height finder
var lastHeight = $(".guestbook__detail").height() + 2;

function ifHeightChange() {
    var $element = $(".guestbook__detail");
    if ($element.height() != lastHeight) {
        heightFinder();
    }
}

function heightFinder() {
    var currentH = $(".guestbook__detail").height();
    var actualH = currentH - 180 + "px";
    $(".activities__container").css("max-height", actualH);
    $(".activities__container").mCustomScrollbar("update");
}

// SideBar
//For Later

$(".side-item .title").on("click", function () {
    var currentSub = $(this)
        .parent()
        .parent()
        .find(".sub_menu");
    if (!currentSub.hasClass("clicked")) {
        $(".sub_menu").removeClass("clicked");
        $(".acc-icon-close").css("display", "none");
        $(".acc-icon-open").css("display", "inline-block");
        currentSub.addClass("clicked");
        $(this)
            .find(".acc-icon-open")
            .css("display", "none");
        $(this)
            .find(".acc-icon-close")
            .css("display", "inline-block");
    } else {
        currentSub.removeClass("clicked");
        $(this)
            .find(".acc-icon-open")
            .css("display", "inline-block");
        $(this)
            .find(".acc-icon-close")
            .css("display", "none");
    }


    if($(this).parents('.accordion').find('.sub_menu').hasClass('clicked') && !$("#app").hasClass('active-sidebar')){
        openSibebarMenu();
    }
});

// guestbook tab
/*=================
    $(".tab-content")
    .slice(1)
    .hide();
    $(".tab-menu li")
    .eq(0)
    .addClass("active");
    $(".tab-menu li a").click(function(e) {
      e.preventDefault();
      var content = $(this).attr("href");
      $(this)
      .parent()
      .addClass("active");
      $(this)
      .parent()
      .siblings()
      .removeClass("active");
      $(content).show();
      $(content)
      .siblings(".tab-content")
      .hide();
    });
    ==============*/

/* guest value decrement */
$(".minus-btn").on("click", function (e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var minInput = parseInt($this.closest("div").find("input").attr('min'));
    var maxinput = parseInt($this.closest("div").find("input").attr('max'));
    var diffInput = parseInt($this.closest("div").find("input").attr('diff'));
    var value = parseInt($input.val());
    value = value - diffInput;

    if (value >= minInput) {
        $input.val(value);
    }
    var newWalkinModa__pop = $(this).parents('#newWalkinModal').hasClass('in');
    if (value < maxinput && newWalkinModa__pop == true) {
        $('.max--cover').addClass('hide').css({'z-index': '-1', 'bottom': '-13px'});
    }
});

/* guest value increment */
$(".plus-btn").on("click", function (e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var maxinput = parseInt($this.closest("div").find("input").attr('max'));
    var diffInput = parseInt($this.closest("div").find("input").attr('diff'));
    var value = parseInt($input.val());
    value = value + diffInput

    if (value <= maxinput) {
        $input.val(value);
    }
    var newWalkinModa__pop = $(this).parents('#newWalkinModal').hasClass('in');
    console.log(newWalkinModa__pop);
    if (value == maxinput && newWalkinModa__pop == true) {
        $('.max--cover').removeClass('hide').css({'z-index': '9', 'bottom': '13px'});
    }
});
$(".icon-close").on("click", function () {
    $('.max--cover').addClass('hide');
});

/*==============*/

//Check Availability Walkin

/*$(".check__avail_btn").on("click", function () {
        $(".first__step-walkin , .check__availability").hide();
        $(".seat_btn , .second__step-walkin").show();
    });*/

// Add Customer Details
$(".add_customer_details__walkin").on("click", function () {
    $(".done_btn , .third__step-walkin").show();
    $(".seat_btn , .second__step-walkin").hide();
});

//Input Label Animations //
$(document).on("keypress keyup blur", ".input__field input,#add-a-note textarea,.api-key textarea,.textarea__input textarea" , function(event) {
    if ($(this).attr('id') == 'phone') {

        // console.error($(this).attr('id'))

        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    }
    //console.log("val "+$(this).val());
    if ($(this).val()) {
        $(this)
            .nextAll()
            .addClass("hasVal");
    } else {
        $(this)
            .nextAll()
            .removeClass("hasVal");
        //  console.log('2 '+$(this).val());
    }

    if ($("#customer_detail")
            .find("#phone")
            .length
        &&
        $("#customer_detail")
            .find("#phone")
            .val().length
    ) {
        $(this)
            .nextAll("span")
            .addClass("hasVal");
    }
});

function ConfirmBox(title, message, confirmresp) {
    var boxstr =
        '<div class="modal fade modal-popup" id="confirmboxcommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static"> <div class="modal-dialog  modal-sm"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title" id="myModalLabel">' +
        title +
        '</h4> </div> <div class="modal-body text-center"> <p>' +
        message +
        '</p> </div> <div class="modal-footer"> <a class="btn btn__primary btn-ok yes" id="yesconfirm">Yes</a> <button id="noconfirm" type="button" class="btn btn__holo" data-dismiss="modal">No</button> </div> </div> </div> </div>';
    var resp = false;

    if ($("#confirmboxcommon").length) {
        $("#confirmboxcommon").remove();
    }
    $("body").append(boxstr);
    var modalcur = $("#confirmboxcommon");
    modalcur.modal("show").one("click", "#yesconfirm", function (e) {
        resp = true;
        confirmresp(resp, modalcur);
    });

    modalcur.modal("show").one("click", "#noconfirm", function (e) {
        resp = false;
        confirmresp(resp, modalcur);
    });
}

function alertbox(title, message, confirmresp) {
    $(".modal").modal("hide");
    var boxstr =
        '<div class="modal fade modal-popup" id="alertbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static"> <div class="modal-dialog  modal-sm"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title" id="myModalLabel">' +
        title +
        '</h4> </div> <div class="modal-body text-center"> <p>' +
        message +
        '</p> </div> <div class="modal-footer"></div> </div> </div> </div>';
    var resp = false;

    if ($("#alertbox").length) {
        $("#alertbox").remove();
    }

    $("body").append(boxstr);
    var modalcur = $("#alertbox");
    modalcur.modal("show");
    confirmresp(modalcur);
}

///Input Label Animations End //
///Input Label Animations End //
///modal layout fixed//
$(".modal").on("shown.bs.modal", function (e) {
    $("html").addClass("freezePage");
    $("body").addClass("freezePage");
});
$(".modal").on("hidden.bs.modal", function (e) {
    $("html").removeClass("freezePage");
    $("body").removeClass("freezePage");
});


$("div[data-target='#book-a-table']").click(function () {
    $("body").addClass("overflow-visible").addClass("no-padding");
    $("html").removeClass("freezePage");
    $("body").removeClass("freezePage");
})

///current table popup not scrolling issue fixed amit//
$("#reservation_info_popup,#book-a-table").on("shown.bs.modal", function (e) {
    $("body").addClass("overflow-visible");
    $("html").removeClass("freezePage");
    $("body").removeClass("freezePage");
});
$("#reservation_info_popup,#book-a-table").on("hidden.bs.modal", function (e) {
    $("body").removeClass("overflow-visible");
});


$("#availabilityModal,#reservationDetailModal,#customSettingModal").on(
    "hidden.bs.modal",
    function () {
        $(".alert").hide();
    }
);

///modal layout fixed end//
var slider = document.getElementById("myRange");
var output = document.getElementById("value");
if (slider) {
    output.innerHTML = slider.value;

    slider.oninput = function () {
        output.innerHTML = this.value;
    };
}

$("#reservation_info_popup").on("show.bs.modal", function () {
    window.reservation_info_popup = setInterval(function () {
        var reserv_info_head_hyt = $("#reservation_info_popup").find(".modal-header").height();
        $("#reservation_info_popup").find(".modal-body").css("height", "calc(100% - " + Math.round(reserv_info_head_hyt) + "px)");
        $("#reservation_info_popup .scroller").mCustomScrollbar(options);
    }, 100);
});


$('[data-toggle="tooltip"]').tooltip({
    position: {my: "left bottom-40%", at: "left center"}
});

$(document).ajaxComplete(function (event, request, settings) {
    $('[data-toggle="tooltip"]').not('[data-original-title]').tooltip({
        position: {my: "left bottom-40%", at: "left center"}
    });
});

function secondsToHms(d) {
    d = Number(d);

    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2);

    // return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}

window.difftime = function () {
    if ($("#reservation_info_popup.fade").hasClass('in') && ($('#Current .time_sec2').attr('out_time'))) {

        window.xtimer = setInterval(function () {
            var now = new Date().getTime();
            var diffTime = "";
            var distance = "";
            var currentTime = (moment.tz(timeZoneType).format('HH') * 60 * 60 * 1000) + (moment.tz(timeZoneType).format('mm') * 60 * 1000);

            $('#Current .time_sec2').each(function (k, v) {
                $worked = (moment($(this).attr('out_time')).format('HH') * 60 * 60 * 1000) + (moment($(this).attr('out_time')).format('mm') * 60 * 1000);

                // Find the distance between now an the count down date
                distance = $worked - currentTime;

                if (distance < 0) {
                    diffTime = currentTime - $worked
                } else {
                    diffTime = $worked - currentTime
                }

                // Time calculations for days, hours, minutes and seconds
                var hours = Math.floor((diffTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((diffTime % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((diffTime % (1000 * 60)) / 1000);
                var timer = (hours < 9 ? "0" : "") + hours + ":" + (minutes < 9 ? "0" : "") + minutes;

                $(this).empty().text(timer)


                // If the count down is over, write some text
                if (distance < 0) {
                    $(this).addClass('extra-time');
                    $(this).parents(".time__section").find('.time_sec1').html("Exceed the time limit HH:MM").addClass('extra-time');
                }
            })
        }, 999);

    }

}

$(document).on("hidden.bs.modal", "#reservation_info_popup.modal", function () {
    if ($("#reservation_info_popup.fade").hasClass('in') && ($('#Current .time_sec2').attr('out_time'))) {
        clearInterval(xtimer);
    }
});

/*========================================*/

// Guest Book Filter Silder
function sliderRange(id, sliderMinID, sliderMaxID, minValueInput, maxValueInput, minValue, maxValue, startValue, endValue, currency, slider_avg) {
    $(id).slider({
        range: true,
        values: [startValue, endValue],
        min: minValue,
        max: maxValue,
        create: function () {
            var currency_symbol = '';
            if (currency == 1) {
                currency_symbol = '$';
            }

            var minValString = $(this).slider("values", 0).toString();
            var maxValString = $(this).slider("values", 1).toString();

            $(sliderMinID).text(currency_symbol + minValString);
            $(sliderMaxID).text(currency_symbol + maxValString);

            $(minValueInput).val(minValString);
            $(maxValueInput).val(maxValString);

        },
        slide: function (event, ui) {
            if (ui.values[1] - ui.values[0] < slider_avg) {
                return false;
            } else {
                $("#slidestart").text(ui.values[0]);
                $("#slideend").text(ui.values[1]);
            }
        },
        change: function (event, ui) {
            if ((ui.values[0]) >= ui.values[1]) {
                return false;
            }
            var currency_symbol = '';
            if (currency == 1) {
                currency_symbol = '$';
            }
            var minValString = ui.values[0].toString();
            var maxValString = ui.values[1].toString();

            $(sliderMinID).text(currency_symbol + minValString);
            $(sliderMaxID).text(currency_symbol + maxValString);

            $(minValueInput).val(minValString);
            $(maxValueInput).val(maxValString);

        }
    });
}

/*sliderRange("#slider-range-min-reservation","#min-reservation","#max-reservation","#min-reservation-input","#max-reservation-input",0,50,0,50);
sliderRange("#slider-range-min-order","#min-order","#max-order","#min-order-input","#max-order-input",0,50,0,50);
sliderRange("#slider-range-min-spend","#min-spend","#max-spend","#min-spend-input","#max-spend-input",0,120,0,120)*/

/*============================================================*/

// Applied globally on all textareas with the "autoExpand" class
$(document)
    .one('focus.autoExpand', 'textarea.autoExpand', function () {
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input.autoExpand', 'textarea.autoExpand', function () {
        var minRows = this.getAttribute('data-min-rows') | 0, rows;
        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 16);
        this.rows = minRows + rows;
    });


/*======================================================================*/
// Add Custom Tag

$('.custom__tags-edit .edit_custom_tag').on('click', function () {
    $(this).addClass('hide');
    $(this).parent().find('.custom-tag-third').removeClass('hide');
    $(this).parent().find('.cancel_edit_custom_tag-third').removeClass('hide');
    $(this).parent().find('.delete-custom-tag').addClass('hide');
    $(this).parent().find('.add-custom-tag').removeClass('hide');
    $(this).parents('.guest--tag--bar').find('.custom__tag-name').addClass('hide');
    $(this).parents('.guest--tag--bar').find('input').removeClass('hide');
    $(this).parents('.guest--tag--bar').find('input').css("color", "#ff9300");
});

$('.custom__tags-edit .cancel_edit_custom_tag-third').on('click', function () {
    $(this).addClass('hide');
    $(this).parent().find('.custom-tag-third').addClass('hide');
    $(this).parent().find('.cancel_edit_custom_tag-third').addClass('hide');
    $(this).parent().find('.delete-custom-tag').removeClass('hide');
    $(this).parent().find('.add-custom-tag').removeClass('hide');
    $(this).parent().find('.edit_custom_tag').removeClass('hide');
    $(this).parents('.guest--tag--bar').find('.custom__tag-name').removeClass('hide');
    $(this).parents('.guest--tag--bar').find('input').addClass('hide');
});

$('.custom__tags-edit .update_custom_tag').on('click', function () {
    $(this).addClass('hide');
    $(this).parent().find('.custom-tag-third').addClass('hide');
    $(this).parent().find('.cancel_edit_custom_tag-third').addClass('hide');
    $(this).parent().find('.edit_custom_tag').removeClass('hide');
    $(this).parent().find('.delete-custom-tag').removeClass('hide');
    $(this).parent().find('.add-custom-tag').addClass('hide');
    $(this).parents('.guest--tag--bar').find('.custom__tag-name').removeClass('hide');
    $(this).parents('.guest--tag--bar').find('input').addClass('hide');
});


$('.custom_toogle_checkbox').on('click', function () {

    var val = $(this).parents('.toggle__container').find('input').val();

    val = (val == 0 ? 1 : 0);
    $(this).parents('.toggle__container').find('input').val(val)
});
/*======================================================================*/
/*$('.reservation__edit .edit_name').on('click', function () {
  $('.reservation__edit .update_guest_details-third').removeClass('hide');
  $('.reservation__edit .cancel_edit_name-third').removeClass('hide');
  $('.reservation__edit .edit_name').addClass('hide');
  $(".modify__reservation input").removeAttr("readonly");
});*/


$("#reservationdeatails-datepicker").datepicker({
    dateFormat: 'mm-dd-yy',
    minDate: 0

});

/*==================================================================*/

// =====================  server edit ============= //
// $('.edit-server .edit-server-details').on('click', function () {
//   $(this).addClass('hide');
//   $(this).parent().find('.edit-server-details-cross,.edit-server-details-check').removeClass('hide');
// });

// $('.edit-server .edit-server-details-cross').on('click', function () {
//   $(this).addClass('hide');
//   $(this).parent().find('.edit-server-details-check').addClass('hide');
//   $(this).parent().find('.edit-server-details').removeClass('hide');
// });

// $('.edit-server .edit-server-details-check').on('click', function () {
//   $(this).addClass('hide');
//   $(this).parent().find('.edit-server-details-cross').addClass('hide');
//   $(this).parent().find('.edit-server-details').removeClass('hide');
// });

/*=================================================================*/


//** Block A Table **//
if ($('#blockTables').length) {

    function timeDifference(x, y) {
        x = convertTimeFormat(x);
        y = convertTimeFormat(y);
        start = x.split(":");
        resume = y.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var resumeDate = new Date(0, 0, 0, resume[0], resume[1], 0);
        var diff = resumeDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        diff -= minutes * (1000 * 60);
        var seconds = Math.floor(diff / 1000);

        if (hours < 0)
            hours = hours + 24;

        return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes

    }

    function twoDigitNumbers(n) {
        return n > 9 ? "" + n : "0" + n;
    }

    function disableTimeRanges() {
        window.getCurrentSlotRange = $("#current_time_slot_range").val();
        var getCurrentTimeHH = moment.tz(timeZoneType).format('HH')
        var getCurrentTimeMM = moment.tz(timeZoneType).format('mm')

        if ((getCurrentTimeMM % 5) == 0) {
            var getCurrentTime = getCurrentTimeHH + ":" + twoDigitNumbers(parseInt(parseInt(getCurrentTimeMM) + 1))
        } else {
            var getCurrentTime = getCurrentTimeHH + ":" + getCurrentTimeMM
        }

        if (getCurrentSlotRange != "") {
            getCurrentSlotRange = getCurrentSlotRange.split("-")[1].split(":")[0] + ":" + twoDigitNumbers(parseInt(parseInt(getCurrentSlotRange.split("-")[1].split(":")[1]) + 1));
            var timeSlot = [["00:00", getCurrentTime], [getCurrentSlotRange, "24:01"]];
            $(".ui-timepicker-disabled").hide()
        } else {
            var timeSlot = [["00:00", "24:01"]];
        }

        return timeSlot;
    }

    function addMinutesToTime(time, minsAdd) {
        function z(n) {
            return (n < 10 ? '0' : '') + n;
        };
        time = convertTimeFormat(time);
        var bits = time.split(':');
        var mins = bits[0] * 60 + +bits[1] + +minsAdd;
        return timeTo12HrFormat(z(mins % (24 * 60) / 60 | 0) + ':' + z(mins % 60));
    }

    $('#blockTables-resume-timepicker').timepicker({
        interval: 15,
        step: '15',
        minTime: '00:15',
        maxTime: '24:00',
        timeFormat: 'g:i A',
        closeOnWindowScroll: true,
        show2400: true
    }).on('changeTime', function () {
        var x = $('#blockTables-start-timepicker').val();
        var y = $('#blockTables-resume-timepicker').val();

        // if(x==""){
        //   $(".resumetimings p").text("Please Select Block Time").css('color', 'red');		
        //   return false;
        // }

        t = timeDifference(x, y);
        $(".time span").text(t);
        $(".resumetimings").removeClass("hide");

    }).on('click', function (e) {

        if (getCurrentSlotRange != "") {
            $(".ui-timepicker-disabled").hide()
        }
        $(".ui-timepicker-wrapper").mCustomScrollbar({
            theme: "dark"
        })
    })

    /*$('#blockTables-start-timepicker').timepicker({
        interval: 15,
        step: '15',
        minTime: '00:00',
        maxTime: '23:45',
        timeFormat: 'g:i A',
        closeOnWindowScroll: true
    }).on('click', function (e) {
      $(".ui-timepicker-wrapper").mCustomScrollbar({
          theme: "dark"
      })
    }).on('selectTime', function() {

      $('#blockTables-resume-timepicker').timepicker('remove'); 
        var x = $(this).val();
        x = convertTimeFormat(x);
        start = x.split(":");
        startInt = start[0] + start[1];
        if(startInt == 2330){
          $('#blockTables-resume-timepicker').val("24:00");
          return false;
        }

         $('#blockTables-resume-timepicker').val( addMinutesToTime($('#blockTables-start-timepicker').val(),15));
         var x=$('#blockTables-start-timepicker').val();
         var y=$('#blockTables-resume-timepicker').val();
         t = timeDifference(x,y);
         $(".time span").text(t);

         $(".resumetimings").removeClass("hide");
         $('#blockTables-resume-timepicker').timepicker({
          interval: 15,
          minTime: '00:15',
          maxTime: '24:00',
          step: '15',
          timeFormat: 'g:i A',
          closeOnWindowScroll: true,
          show2400 : true,
          disableTimeRanges :disableTimeRanges(x)
        });
      })*/


    function resetBlockTable() {
        $("#blockTables #blockTables-resume-timepicker").val("");
        $("#blockTables .resumetimings").addClass("hide");
        $("#blockTables #check_block_avail_button").show();
        $('#blockTables #block_floor,#blockTables #block_tables').val("");
        setTimeout(function () {
            $(".selectpicker").selectpicker('refresh');
        }, 10);
        $("#blockTables .floor-table-block, #table-block .message_div ").hide();
        removeValidation("#table-block");
    }

    $("#block_a_table").click(function () {
        openSibebarController(true)
        $("#book-a-table,#newWalkinModal,#newWaitlistModal,#reservation_info_popup").modal('hide');
        $("#bookATable,#new_walkin_button").removeAttr("data-popup")

        resetBlockTable()
        $('#blockTables-start-timepicker').val(moment.tz(timeZoneType).format('hh:mm A'))

        $('#blockTables-resume-timepicker').timepicker({
            interval: 15,
            minTime: '00:15',
            maxTime: '24:00',
            step: '15',
            timeFormat: 'g:i A',
            closeOnWindowScroll: true,
            show2400: true,
            forceRoundTime: true,
            disableTimeRanges: disableTimeRanges()
        });
    })
}
/*====================================================*/
$('#reservationTab .week,#reservationTab .month').on('click', function () {
    $(this).parents().find('.reservation__searchbar').addClass('hide');
    $(this).parents().find('.booking-nav').addClass('reservationlisting');
});

$('#reservationTab .today').on('click', function () {
    $(this).parents().find('.reservation__searchbar').removeClass('hide');
    $(this).parents().find('.booking-nav').removeClass('reservationlisting');
});
/*=======================================================*/
// $('#reservation_settings .selct-picker-plain').on('click', function (e) {
//     $(".dropdown-menu.inner").mCustomScrollbar({
//       theme: "dark",
//       autoHideScrollbar: true,
//     });
//  })
/*================================================*/


$('.timepicker').on('keydown', function (e) {
    var key = e.which || event.charCode;
    if (key == 8 || e.which == 32) return false;
});

$(document).on('click', '.booking__btn[data-popup=true],#reservation_info_popup a.close', function () {
    openSibebarController(false)
})

$('#book-a-table,reservation_info_popup,#blockTables').on('hidden.bs.modal', function () {
    //openSibebarController(false)
});

$("#newWalkinModal a.close, #newWaitlistModal a.close,#blockTables a.close, #book-a-table a.close,#reservation-details a.close").click(function () {
    openSibebarController(false)
})

/*** Page refresh onClick on back button ***/
window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted ||
        (typeof window.performance != "undefined" &&
            window.performance.navigation.type === 2);
    if (historyTraversal) {
        // Handle page restore.
        window.location.reload();
    }
});

/******** block a table ******************************/
$("#blockTables").on("show.bs.modal", function () {
    $('body').addClass('overflow--visible');
    $('html,body').addClass('overflow--visible');
    openSibebarController(true)
    $("#block_a_table").attr("data-popup", true)
});

$("#blockTables").on("hidden.bs.modal", function () {
    $('body').removeClass('overflow--visible');
    $('html,body').removeClass('overflow--visible');
});
/*******************************************************/

//@16-10-2018 BY RG Export Food and Merchandise Orders
$(document).on('click', '.export_order', function () {
    var page = $('.export_button').attr('rel');
    var type = page.split('-'); //$(this).attr('rel').split('-');
    var keyword = $('#key_search').val();
    var order_status = $('.selectpicker').val();
    var order_type = type['0'];
    var status = type['1'];
    var export_type = $('.export_type:checked').val();
    var from_date = $('.datepickerF').val();
    var to_date = $('.datepickerT').val();
    window.open(
        '/order/export/?key_search=' + keyword + '&order_status=' + order_status + '&order_type=' + order_type + '&status=' + status + '&from_date=' + from_date + '&to_date=' + to_date + '&export_type=' + export_type,
        '_blank' // <- This is what makes it open in a new window.
    );
    //window.location.href = '/order/export/?key_search='+keyword+'&order_status='+order_status+'&order_type='+order_type+'&status='+status+'&from_date='+from_date+'&to_date='+to_date+'&export_type='+export_type;
});

$(".side-bar-open").on("click", function () {
    $(this).parents().find('.sidebar').addClass('active');
    $(this).parents().find('#sidebar__overlay').addClass('overlay');
})
$("#sidebar__overlay").on("click", function () {
    closeSibebarMenu()
    //$(this).parents().find('#sidebar__overlay').removeClass('overlay');
    //$(this).parents().find('.sidebar').removeClass('active');
})

/************ Teatarea auto expand *************************/
if ($('.guestbook_userDetails').length) {
    var textarea = document.querySelector('textarea');

    textarea.addEventListener('keydown', autosize);

    function autosize() {
        var el = this;
        setTimeout(function () {
            el.style.cssText = 'height:auto; padding:0';
            el.style.cssText = 'height:' + el.scrollHeight + 'px';
        }, 0);
    }
}

function goBack() {
    window.history.go(-1)
}

/************** RESERVATION DETAILS POPUP ************************/
$('#reservation-details .check-availability__btn.fullWidth.save').on('click', function () {
    $("#reservation-details .tag-plus.icon").addClass('pointer-none');
});

/*
This method will convert 12 hour format to 24 hour format
input - 10:00 PM
output - 22:00
 */
function convertTimeFormat(time) {
    if (time === undefined || time === null) {
        return null;
    }
    var hrs = Number(time.match(/^(\d+)/)[1]);
    var mnts = Number(time.match(/:(\d+)/)[1]);
    var format = time.match(/\s(.*)$/)[1];
    if (format == "PM" && hrs < 12) hrs = hrs + 12;
    if (format == "AM" && hrs == 12) hrs = hrs - 12;
    var hours = hrs.toString();
    var minutes = mnts.toString();
    if (hrs < 10) hours = "0" + hours;
    if (mnts < 10) minutes = "0" + minutes;
    return hours + ":" + minutes;
}

/*
This method will convert 24 hour format to 12 hour format
input - 22:00
output - 10:00 PM
 */
function timeTo12HrFormat(time) {   // Take a time in 24 hour format and format it in 12 hour format
    var time_part_array = time.split(":");
    var ampm = 'AM';

    if (time_part_array[0] >= 12) {
        ampm = 'PM';
    }

    if (time_part_array[0] > 12) {
        time_part_array[0] = time_part_array[0] - 12;
    }

    formatted_time = time_part_array[0] + ':' + time_part_array[1] + ' ' + ampm;

    return formatted_time;
}

/**************************************************************/

/*************** vertically center  Popup  **********/

function alignModal() {
    var modalDialog = $(this).find(".modal-dialog.vertical-center");

    modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
}

$(".modal").on("shown.bs.modal", alignModal);

$(window).on("resize", function () {
    $(".modal:visible").each(alignModal);
});


/******************  book a table guest note auto expand ******************* */
$('.bookatablenote.guest_note').on('change keyup keydown paste cut', 'textarea#guest_note', function () {
    if ($(this).val().length < 45) {
        $(this).attr("rows", "1")
    } else if ($(this).val().length > 45 && $(this).val().length < 90) {
        $(this).attr("rows", "2")
    } else if ($(this).val().length > 90) {
        $(this).attr("rows", "3")
    }
}).find('textarea').change();
//*====================popover js=========================*/
if(!$(".main__container").hasClass("guestbook-details")) {
    $('[data-toggle="popover"]').popover();
}


/*****************************************************************************/

$(".side-section").click(function () {
    $(".side_bar_right").addClass("active");
    $(this).addClass('hide');
    if (window.innerWidth >= 1280) {
        $(this).parents(".tableManagement__wrapper").addClass("openSibebar");
    }
    //$(".booking__type").removeClass("margin-right-60");
});

$(".popupclose").click(function () {
    $(".side_bar_right").removeClass("active");
    $(".floor-tables-sidebar").removeClass("active");
    $(".side-section").removeClass('hide');
    $(".tableManagement__wrapper").removeClass("openSibebar");
    //$(".booking__type").addClass("margin-right-60");
});

/**************************** TAB TRIGGER ********************************/

$('[data-trigger="tab"]').click(function (e) {
    var href = $(this).attr('href');
    e.preventDefault();
    $('[data-toggle="tab"][href="' + href + '"]').trigger('click');
});

/***************************** CANCLE RESERVATION POPUP **************/

$("#cancel-reservation .icon-close").click(function () {
    $("#cancellation_reason1").attr("checked", true);
});

/*******************************************************/
$(".floor-close").click(function () {
    $(this).parents(".tableManagement__wrapper").find(".floor-tables-management").addClass("openSibebar");
});

$(".floor-open").click(function () {
    $(this).parents().find(".floor-tables-management").removeClass("openSibebar");
});

/***********************************************/
function insertNumberOnly(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

/*******************************************************/
$("#availabilityModal .icon-close").click(function () {
    $("#uniqueSlotName").addClass("hide");
});

/************************************************/

$("#datepickerShiftOverview").datepicker({
    minDate: 0,
    dateFormat: 'mm-dd-yy',
    onSelect: function () {
        dateFormat()
    }
})

function openTab(id) {
    $('a[href="' + id + '"]').click()
}

/******************** Add a Note Popup **************************/

$('#add-a-note .note-text').on('change keyup keydown paste cut', 'textarea#note_message', function () {
    if ($(this).val().length < 55) {
        $(this).attr("rows", "1")
    } else if ($(this).val().length > 55 && $(this).val().length < 110) {
        $(this).attr("rows", "2")
    } else if ($(this).val().length > 110) {
        $(this).attr("rows", "3")
    }
}).find('textarea').change();

$("#add-a-note #addANote").validate({
    rules: {
        note_title: {
            required: true
        },
        note_type: {
            required: true
        },
        note_message: {
            required: true
        }
    },
    errorPlacement: function (error, element) {
        if (element.is("select#note_type")) {
            error.appendTo(element.parents('.note-type'));
        } else { // This is the default behavior
            error.insertAfter(element);
        }
    },
    ignore: ":hidden:not(.selectpicker)"
});

$('#add-a-note .add-note-btn').on('click', function () {
    if (!$("#add-a-note #addANote").valid()) {
        return false;
    }
});

$("button[data-target=\"#add-a-note\"]").click(function () {
    resetAddANotePopup()
})

function resetAddANotePopup() {

    $("#add-a-note .hasVal").each(function (key, value) {
        $(this).removeClass('hasVal');
    })

    $("#add-a-note input,#add-a-note textarea").val("");
    $("#add-a-note #note_type").selectpicker('val', '');
    removeValidation("#addANote")
}


/******************** End Add a Note Popup **************************/



  //@19-03-2019 BY RG SOrt Order based on Delivery Date and Order Date
  $(document).on('change','.ordersortorder', function(e) {
    e.stopImmediatePropagation();
      var page = $(this).attr('rel'); 
      var sortorder = $(this).val(); 
      var keyword =  $('#key_search').val();
      var order_status = $('.selectpicker').val();
      if(keyword == '') {
        window.location.href =  '/'+page+'/?sortorder='+sortorder; 
      }else {
        window.location.href =  '/'+page+'/?key_search='+keyword+'&sortorder='+sortorder; 
      }
  });


if(getUrlParameter('dashboardDaySort') == undefined){
    window.localStorage.removeItem("dashboardDaySort")
    window.localStorage.removeItem('dashboardDayName');
}


if(getUrlParameter('dashboardDaySort') && (window.localStorage.getItem("dashboardDaySort"))) {
    var dashboardDaySort = window.localStorage.getItem("dashboardDaySort");
    var dashboardDayName = window.localStorage.getItem("dashboardDayName");

    if(dashboardDayName != "Overall") {
        $(".ddValue").text("for " + dashboardDayName);
    }

    $(".dashboardDaySort").selectpicker('val', dashboardDaySort).selectpicker('refresh')
}


$(document).on('change','.dashboardDaySort', function(e) {
    e.stopImmediatePropagation();
    var page = $(this).attr('rel');
    var dashboardDaySort = $(this).val();
    var getDropDownText =  $(this).find('option:selected').text();

    window.localStorage.setItem('dashboardDaySort', dashboardDaySort);
    window.localStorage.setItem('dashboardDayName', getDropDownText);
    window.location.href =  '/'+page+'/?dashboardDaySort='+dashboardDaySort;
});




/************ left Menu ************/
window.closeSibebarMenu = function(){
    setTimeout(function(){
        $('.accordion .sub_menu, .accordion.selected .sub_menu').removeClass('clicked');
    },200)
        $("#app").removeClass("active-sidebar");
        $(".menu-button").find(".menu-close").addClass("hide")
        $(".menu-button").find(".menu-open").removeClass("hide")
        //$(".accordion").find(".title").trigger('click')
        $('.title').find(".acc-icon-open").css("display", "inline-block");
        $('.title').find(".acc-icon-close").css("display", "none");
        $(".sidebar").css("width",70);
        
        if($(window).width() < 1024) {
            $(".sidebar").css({"left": "-174px", "visibility": "visible"});
        }

}

window.openSibebarMenu = function(){
    $("#app").addClass("active-sidebar");
    $(".menu-button").find(".menu-close").removeClass("hide");
    $(".menu-button").find(".menu-open").addClass("hide");
    $(".sidebar").css("width",174);
    
    if($(window).width() < 1024) {
        $(".sidebar").css({"left": "0", "visibility": "visible"});
    }
}


function menuCloseBelowWidth(width){
    if($(window).width() < width){
        closeSibebarMenu();
    }
}

//$(document).ready(function(){menuCloseBelowWidth(992)});
menuCloseBelowWidth(992);
$(window).resize(function(){menuCloseBelowWidth(992)});

/*$(".sidebar").swipe({
    swipeStatus:function(event, phase, direction, distance, duration, fingers)
    {
        if (phase=="move" && direction =="right") {
            alert(1)
            $("#app").addClass("active-sidebar");
            return false;
        }
        if (phase=="move" && direction =="left") {
            alert(2)
            $("#app").removeClass("active-sidebar");
            return false;
        }
    }
});*/

$(".sidebar").swipe({
    swipeLeft: function () {
        closeSibebarMenu()
    },
    swipeRight: function(){
        openSibebarMenu()
    }
}).resizable({
    maxWidth: 174,
    minWidth: 70,
    handles: 'e, w',
    resize: function( event, ui ) {
        if((ui.originalSize.width) < ui.size.width){
            openSibebarMenu()
        }

        if((ui.originalSize.width) > ui.size.width){
            closeSibebarMenu()
        }
    }
});


// Social Media API

function textAreaHeight(ele){
    $(ele).height(0).height(ele.scrollHeight);
    console.log(ele)
}


$('.textarea__input textarea').each(function(){
    autosize(this);
}).on('autosize:resized', function(){
    console.log('textarea height updated');
});


$('.social-media-apis textarea.apiCode, .textarea__input textarea').on('change keyup keydown paste blur cut', function () {
    /*if ($(this).val().length < 100) {
        $(this).attr("rows", "1")
    } else if ($(this).val().length > 100 && $(this).val().length < 200) {
        $(this).attr("rows", "2")
    } else if ($(this).val().length > 200) {
        $(this).attr("rows", "3")
    }*/

    //textAreaHeight(this)

}).find('textarea').change();

function SetCaretAtEnd(elem) {
    var elemLen = elem.value.length;
    // For IE Only
    if (document.selection) {
        // Set focus
        elem.focus();
        // Use IE Ranges
        var oSel = document.selection.createRange();
        // Reset position to 0 & then set at end
        oSel.moveStart('character', -elemLen);
        oSel.moveStart('character', elemLen);
        oSel.moveEnd('character', 0);
        oSel.select();
    }
    else if (elem.selectionStart || elem.selectionStart == '0') {
        // Firefox/Chrome
        elem.selectionStart = elemLen;
        elem.selectionEnd = elemLen;
        elem.focus();
    } // if
}

$( "#cms-accordion" ).accordion({
    header: ".accordion-header",
    event: "click hoverintent",
    collapsible: true,
    heightStyle: "content",
    active: 0,
    beforeActivate: function(event, ui) {

        // The accordion believes a panel is being opened
        if (ui.newHeader[0]) {
            var currHeader  = ui.newHeader;
            var currContent = currHeader.next('.ui-accordion-content');
            // The accordion believes a panel is being closed
        } else {
            var currHeader  = ui.oldHeader;
            var currContent = currHeader.next('.ui-accordion-content');
        }
        // Since we've changed the default behavior, this detects the actual status
        var isPanelSelected = currHeader.attr('aria-selected') == 'true';

        // Toggle the panel's header
        currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

        // Toggle the panel's icon
        currHeader.children('.fa').toggleClass('fa-plus',isPanelSelected).toggleClass('fa-minus',!isPanelSelected);

        // Toggle the panel's content
        currContent.toggleClass('accordion-content-active',!isPanelSelected)
        if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

        return false; // Cancels the default action
    }
});

$(".uploadFile").click(function () {
    $(this).parents(".uploadCSV_field").find("input[type='file']").trigger("click");
});

$(".uploadCSV_field input[type='file']").change(function (e) {
    var fileName = e.target.files[0].name

    $(this).parents(".uploadCSV_field").find(".uploadFileLabel").addClass('hasVal');
    $(this).parents(".uploadCSV_field").find(".uploadFileName").val(fileName);
    $(this).parents(".uploadCSV_field").find(".fileUpload").val(fileName);
    $(this).parents(".uploadCSV_field").find("label.error").hide();

});
