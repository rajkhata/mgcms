$(document).ready(function () {
    var today_reservation_html;
    $('.reservation_listing a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if (target == '#week_reservation') {
            $("#week_reservation_calendar").fullCalendar('destroy');
            displayWeekReservation();
        } else if (target == '#month_reservation') {
            $("#month_reservation_calendar").fullCalendar('destroy');
            displayMonthReservation();
        } else {

            displayDayReservation();
        }
    });

    function displayDayReservation(start_date, end_date) {
        var total_covers = 0;
        $('.today_reservation').html('');
        var keyword = $('#reservation_search').val().trim();

        // for url of listing, all, upcoming, archived
        var ajax_url = '';
        if(window.location.href.includes('?')) {
            ajax_url = window.location.href.split('/').pop().split('?')[0];
        } else {
            ajax_url = window.location.href.split('/').pop().split('#')[0];
        }
        var dataType = 'html';
        if(ajax_url=='reservation') {
            ajax_url = 'reservation/listing';
            dataType = 'json';
            if (start_date === undefined) {
                start_date = "";
            }
            if (end_date === undefined) {
                end_date = "";
            }
            if (!start_date || !end_date) {
                start_date = moment().format("MM-DD-YYYY");
                end_date = moment().format("MM-DD-YYYY");
            }
        } else {
            var start_date = $('.start_date').val();
            var end_date = $('.end_date').val(); 
            
        }
        console.log(ajax_url);
        $.ajax({
            url: ajax_url,
            data: 'start_date=' + start_date + '&end_date=' + end_date + '&keyword='+keyword,
            dataType: dataType,
            success: function (data) {
                console.log('success');
                today_reservation_html = '';
                var resv_class='resv_detail';
                var resv_modal='reservation-details';
                if(data.reservations && data.reservations.length<=0){
                    $('#today_reservation .no_record').show();
                }
                $.each(data.reservations, function (key, value) {
                    if(value.floor_id===null){
                        if(value.waiting_icon!==null){
                            resv_class = 'resv_detail_waitlist';
                            resv_modal = 'manageWaitlistModal';
                        }else{
                            resv_class = 'resv_detail_enquiry';
                            resv_modal = 'reservation-enquiry-details';
                        }
                    }
                    total_covers += parseInt(value.reserved_seat);
                    var category_class = (value.waiting_icon!==null)?value.waiting_icon:value.user_category_icon;
                    var waiting_row_class = (value.waiting_icon!==null)?'waitlist__customer-color':'';
                    var waiting_party_class = (value.waiting_icon!==null)?'customer_size-waitlist':'';
                    var mobile = (value.mobile!==null)?value.mobile:' ';
                    var email = (value.email!==null)?value.email:' ';
                    var guestbook_link = (value.user_id!==null) ? '/guestbook/detail/'+value.user_id:'#';
                    var reservation_time = (value.reservation_start_time!==null)?value.reservation_start_time:'';
                    today_reservation_html += '<div class="row row__reservation row__customer flex-align-item-start '+waiting_row_class+'">';
                    today_reservation_html += '<div class="col-xs-4 col-sm-4 col-lg-7 no-padding-left flex-box lex-align-item-start tablet-flex-wrap">';
                    today_reservation_html += '<div class="col-xs-6 col-sm-6 col-md-4 show-tablet-50 table-content customer__' + category_class + ' no-padding-left"><a class="text-ellipsis fullWidth" href="'+guestbook_link+'">'+ value.full_name +'</a></div>';
                    today_reservation_html += '<div class="col-xs-6 col-sm-6 col-md-5 show-tablet-50 table-content no-padding-left text-ellipsis" data-toggle="tooltip" title="'+email+'">'+email+'</div>';
                    today_reservation_html += '<div class="col-xs-6 col-sm-2 show-tablet-50 table-content no-padding"><i class="fa fa-phone font-size-13 hidden show-tablet" aria-hidden="true"></i> '+mobile+'</div>';
                    today_reservation_html += '</div>';
                    today_reservation_html += '<div class="col-xs-1 col-sm-1 table-content customer_time no-padding text-nowrap">'+ reservation_time +'</div>';
                    today_reservation_html += '<div class="col-xs-2 col-sm-2 table-content">';
                    today_reservation_html += '<div class="customer_size '+waiting_party_class+'">' + value.reserved_seat + '</div>';
                    today_reservation_html += '</div>';
                    today_reservation_html += '<div class="col-xs-1 col-sm-1 table-content icon-size no-padding-left no-padding text-center flex-verti-center" data-toggle="tooltip" title="'+value.status.name+'">';
                    today_reservation_html += '<span class="'+value.status_icon+'" style="color: '+value.status_color+';"></span>';
                    today_reservation_html += '</div>';
                    today_reservation_html += '<div class="col-xs-1 col-sm-1 table-content text-center flex-verti-center" data-toggle="tooltip" title="'+value.source+'">';
                    today_reservation_html += '<i class="'+value.source_icon+'" aria-hidden="true"></i>';
                    today_reservation_html += '</div>';
                    today_reservation_html +='<div class="col-xs-1 col-sm-1 col-lg-1 table-content no-padding-right paytment-refund">'+value.payment_status;
                    today_reservation_html +='</div>';
                    today_reservation_html += '<div class="col-xs-1 col-sm-1 col-lg-1 table-content text-right guest_btn no-padding-right">';
                    today_reservation_html += '<div class="btn btn_res btn__primary font-weight-600 '+resv_class+' today_resv" data-toggle="modal" data-target="#'+resv_modal+'" id="resv_' + value.id + '">View</div>';
                    today_reservation_html += '</div>';
                    today_reservation_html += '</div>';
                    $('#today_reservation .no_record').hide();
                });
                if(ajax_url=='reservation/listing') {
                    //console.log('clear reservation');
                    $('.booking_count').html(data.reservations.length);
                    $('.booking_covers').html(total_covers);
                    $('#today_reservation .today_reservation').empty().html(today_reservation_html);
                } else {
                    console.log('clear others'+ $('.archive_reservation').length);
                    $('.archive_reservation').empty().html(data);
                    var counter_record = parseInt($('.total_active_count').html());
                    if(counter_record > 1) {
                        var record_type = 'Reservations';   
                    }else {
                        var record_type = 'Reservation';
                    }
                    $('.total_active_count_render').html($('.total_active_count').html()+ ' Active '+record_type);
                    $('.reservation-cover-count').html('Total Covers '+$('.total_count_cover').html());
                }
            }
        });
    }

    function displayWeekReservation(start_date, end_date) {

        if (start_date === undefined) {
            start_date = "";
        }
        if (end_date === undefined) {
            end_date = "";
        }

        if (!start_date || !end_date) {
            start_date = moment().startOf('week').format("MM-DD-YYYY");
            end_date = moment().endOf('week').format("MM-DD-YYYY");
        }
        $("#week_reservation_calendar").fullCalendar('render');
        $.ajax({
            url: 'reservation/listing',
            data: 'start_date=' + start_date + '&end_date=' + end_date,
            dataType: 'json',
            success: function (data) {
                $('.booking_count').html(data.total_reservation);
                $('.booking_covers').html(data.total_cover_count);

                $('#week_reservation_calendar').fullCalendar({
                    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                    header: {
                        left: '',
                        center: 'prev,title,next',
                        right: ''
                    },
                    now: nowTimeShow,
                    firstDay: 1,
                    titleFormat: 'MMMM',
                    defaultView: 'agendaWeek',
                    slotLabelFormat: 'h:mm a',
                    defaultTimedEventDuration: '00:15:00',
                    slotMinutes: 15,
                    slotDuration: '00:15:00',
                    columnHeaderHtml: function (mom) {
                        return '<span class=weekName>' + mom.format('ddd') + '</span>' + '<span class=weekNumber>' + mom.format('D') + '</span>';
                    },
                    events: data.events,
                    eventRender: function (event, element) {
                        element.find('.fc-title').html(event.title);
                    },
                    contentHeight: 570,
                    allDaySlot: false,
                    viewRender: function (view, element) {
                        $("#week_reservation_calendar .fc-prev-button").html('<i class="fa fa-chevron-left" aria-hidden="true"></i> Previous Week');
                        $("#week_reservation_calendar .fc-next-button").html('Next Week <i class="fa fa-chevron-right" aria-hidden="true"></i>');
                    },
                });
                $('#week_reservation_calendar').fullCalendar('removeEvents');
                $('#week_reservation_calendar').fullCalendar('addEventSource', data.events);
                $('#week_reservation_calendar').fullCalendar('rerenderEvents');
            }
        });
    }

    $('body').on('click', '#week_reservation_calendar button.fc-prev-button', function () {
        var currentDate = $('#week_reservation_calendar').fullCalendar('getDate');
        var beginOfWeek = moment($('#week_reservation_calendar').fullCalendar('getView').start._i).format('MM-DD-YYYY');
        var endOfWeek = moment(currentDate.endOf('week')).format("MM-DD-YYYY");
        displayWeekReservation(beginOfWeek, endOfWeek);
    });

    $('body').on('click', '#week_reservation_calendar button.fc-next-button', function () {
        var currentDate = $('#week_reservation_calendar').fullCalendar('getDate');
        var beginOfWeek = moment($('#week_reservation_calendar').fullCalendar('getView').start._i).format('MM-DD-YYYY');
        var endOfWeek = moment(currentDate.endOf('week')).format("MM-DD-YYYY");
        displayWeekReservation(beginOfWeek, endOfWeek);
    });

    $('body').on('click', '#month_reservation_calendar button.fc-prev-button', function () {
        var currentDate = $('#month_reservation_calendar').fullCalendar('getDate');
        var beginOfMonth = moment($('#month_reservation_calendar').fullCalendar('getView').start._i).format('MM-DD-YYYY');
        var endOfMonth = moment(currentDate.endOf('month')).format("MM-DD-YYYY");
        console.log(beginOfMonth + "*****" + endOfMonth);
        displayMonthReservation(beginOfMonth, endOfMonth);
    });

    $('body').on('click', '#month_reservation_calendar button.fc-next-button', function () {
        var currentDate = $('#month_reservation_calendar').fullCalendar('getDate');
        var beginOfMonth = moment($('#month_reservation_calendar').fullCalendar('getView').start._i).format('MM-DD-YYYY');
        var endOfMonth = moment(currentDate.endOf('month')).format("MM-DD-YYYY");
        console.log(beginOfMonth + "*****" + endOfMonth);
        displayMonthReservation(beginOfMonth, endOfMonth);
    });

    function displayMonthReservation(start_date, end_date) {

        if (start_date === undefined) {
            start_date = "";
        }

        if (!start_date || !end_date) {
            start_date = moment().startOf('month').format("MM-DD-YYYY");
            end_date = moment().endOf('month').format("MM-DD-YYYY");
        }
        $("#month_reservation_calendar").fullCalendar('render');
        $.ajax({
            url: 'reservation/listing',
            data: 'start_date=' + start_date + '&end_date=' + end_date,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                $('.booking_count').html(data.total_reservation);
                $('.booking_covers').html(data.total_cover_count);
                $('#month_reservation_calendar').fullCalendar({
                    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                    header: {
                        left: '',
                        center: 'prev,title,next',
                        right: ''
                    },
                    defaultView: 'month',
                    titleFormat: 'MMMM YYYY',
                    fixedWeekCount: false,
                    events: data.events,
                    eventRender: function (event, element) {
                        element.find('.fc-title').html(event.title);
                        $("[data-date='" + event.start._i.split(" ")[0] + "']").addClass("hasEvent");
                    },
                    eventAfterRender: function(event,element,view){
                        var dateString = $('#month_reservation_calendar').fullCalendar('getDate').format('YYYY-MM-DD');
                        if(event.start._i.split(" ")[0] == dateString) {
                            element.find('.time-wrapper').addClass("todayEvent");
                        }
                    },
                    eventClick: function (event, jsEvent, view) {
                        //alert(event.id);
                    },
                    height: 'auto',
                    viewRender: function (view, element) {
                        $("#month_reservation_calendar .fc-prev-button").html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
                        $("#month_reservation_calendar .fc-next-button").html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
                    },
                });
                $('#month_reservation_calendar').fullCalendar('removeEvents');
                $('#month_reservation_calendar').fullCalendar('addEventSource', data.events);
                $('#month_reservation_calendar').fullCalendar('rerenderEvents');
            }
        });
    }


    /*$('#confirm-cancel').on('show.bs.modal', function(e) {
        $(".btn-ok").unbind('click');
        $(this).find('.btn-ok').on('click', function () {
            var resv_id = $('#resv_id').val();
            var inputData = '&_token=' + $('#token').val();
            $('#loader').removeClass('hidden');
            $.ajax({
                type: 'POST',
                url: '/reservation/cancel/'+resv_id,
                data: inputData,
                success: function (data) {
                    makeBookingInfoUnEditable();
                    $('#loader').addClass('hidden');
                    $('#confirm-cancel').modal('toggle');
                    getAndSetReservationDetail(resv_id);
                    var json_data = JSON.parse(data);
                    $('.alert-success').show();
                    $('.alert-success').empty().html('<p>'+json_data.message+'</p>');
                    setInterval(function(){ $('.alert-success').hide(); }, 1000);
                },
                error: function (data, textStatus, errorThrown) {
                    $('#loader').addClass('hidden');
                    var json_data = JSON.parse(data);
                    $('.alert-danger').show();
                    $('.alert-danger').empty().html('<p>'+json_data.message+'</p>');
                    setInterval(function(){ $('.alert-danger').hide(); }, 1000);
                }
            });
        });
    });*/

    $(function () {
        $('#reservation_search').on('change keyup paste',function(){
            var search_term = $(this).val().trim();
            //if(search_term) {
                //getReservationList(1);
                displayDayReservation();
            //}
        });

        $("#start_date").datepicker({
            showAnim: "slideDown",
            dateFormat: 'mm-dd-yy',
            maxDate: 0,
            onSelect: function () {
                $(this).trigger('change');
                $(this).addClass('addDate');
                getReservations(1);
            }
        }).attr('readonly', 'readonly');
        $("#reservation_date").datepicker({
            showAnim: "slideDown",
            dateFormat: 'mm-dd-yy',
            minDate: 0,
            onSelect: function () {
                $(this).valid();
                $(this).trigger('change');
                $(this).addClass('addDate');
            }
        }).attr('readonly', 'readonly');

        $('#end_date').datepicker({
            showAnim: "slideDown",
            beforeShow: function () {
                $(this).datepicker('option', {
                    minDate: $('#start_date').val(),
                    maxDate: 0,
                    dateFormat: 'mm-dd-yy',
                    onSelect: function (dateText, inst) {
                        $(this).trigger('change');
                        $("#start_date").datepicker('option', 'maxDate', dateText);
                        getReservations(1);
                        $(this).addClass('addDate');
                    }
                });
            }
        }).attr('readonly', 'readonly');

        $('input.timepicker').on('click', function () {
            var tym_pick_id = $(this).attr('id');
            $('.ui-timepicker-wrapper').addClass(tym_pick_id);
            //$(this).valid();
        });
        $("#start_date_upcoming").datepicker({
            showAnim: "slideDown",
            dateFormat: 'mm-dd-yy',
            onSelect: function () {
                $(this).trigger('change');
                $(this).addClass('addDate');
                getReservations(1);
            }
        }).attr('readonly', 'readonly');
        $('#end_date_upcoming').datepicker({
            showAnim: "slideDown",
            beforeShow: function () {
                $(this).datepicker('option', {
                    minDate: $('#start_date_upcoming').val() ? $('#start_date_upcoming').val() : 0,
                    //maxDate:0,
                    dateFormat: 'mm-dd-yy',
                    onSelect: function (dateText, inst) {
                        $(this).trigger('change');
                        //$("#start_date_upcoming").datepicker('option', 'maxDate', dateText);
                        getReservations(1);
                        $(this).addClass('addDate');
                    }
                });
            }
        }).attr('readonly', 'readonly');

        $("#start_date_all").datepicker({
            showAnim: "slideDown",
            dateFormat: 'mm-dd-yy',
            onSelect: function () {
                $(this).trigger('change');
                $(this).addClass('addDate');
                getReservations(1);
            }
        }).attr('readonly', 'readonly');
        $('#end_date_all').datepicker({
            showAnim: "slideDown",
            beforeShow: function () {
                $(this).datepicker('option', {
                    minDate: $('#start_date_all').val(),
                    //maxDate:0,
                    dateFormat: 'mm-dd-yy',
                    onSelect: function (dateText, inst) {
                        $(this).trigger('change');
                        //$("#start_date_upcoming").datepicker('option', 'maxDate', dateText);
                        getReservations(1);
                        $(this).addClass('addDate');
                    }
                });
            }
        }).attr('readonly', 'readonly');
        $("#start_date_archive").datepicker({
            showAnim: "slideDown",
            dateFormat: 'mm-dd-yy',
            onSelect: function () {
                $(this).trigger('change');
                $(this).addClass('addDate');
                getReservations(1);
            }
        }).attr('readonly', 'readonly');
        $('#end_date_archive').datepicker({
            showAnim: "slideDown",
            beforeShow: function () {
                $(this).datepicker('option', {
                    minDate: $('#start_date_archive').val(),
                    //maxDate:0,
                    dateFormat: 'mm-dd-yy',
                    onSelect: function (dateText, inst) {
                        $(this).trigger('change');
                        getReservations(1);
                        $(this).addClass('addDate');
                    }
                });
            }
        }).attr('readonly', 'readonly');
        $( "#edit_start_date" ).datepicker({
            showAnim: "slideDown",
            minDate: 0,
            dateFormat: 'mm-dd-yy'
        }).attr('readonly','readonly');

       /*$('#book-a-table .timepicker').timepicker({
             interval: 30,
             minTime: '00:00',
             maxTime: '23:30',
             timeFormat: "H:i",
            closeOnWindowScroll: true
        }).on('click', function (e) {
            $(".ui-timepicker-wrapper").mCustomScrollbar({
                theme: "dark"
            });
        })*/
        //$('.timepicker').timepicker('setTime', new Date());
    });

    $(window).on('hashchange', function () {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getReservations(page);
            }
        }
    });

    
    $('.resv_detail.today_resv').on('click', function(){ 

        $('#reservation-details').addClass('in');
    });
    $('.icon-close').on('click', function(){ 
       $('#reservation-details').css('display','none');
    });
    
    

    $(document).on('click', '.pagination a', function (event) {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];
        getReservations(page);
    });

    checkForInit();

    function getReservations(page) {
        var start_date = $('.start_date').val();
        var end_date = $('.end_date').val();
        //var keyword = $('#reservation_search').val().trim();
        var keyword = $('#reservation_search').val() ? $('#reservation_search').val().trim() : '';

        $.ajax(
            {
                url: '?page=' + page + '&start_date=' + start_date + '&end_date=' + end_date+ '&keyword='+keyword,
                type: "get",
                datatype: "html",
                // beforeSend: function()
                // {
                //     you can show your loader
                // }
            })
            .done(function (data) {
                $(".archive_reservation").empty().html(data);
                console.log(data);
                location.hash = page;

                setTimeout(function(){
                    $('body,html').animate({
                        scrollTop: 0
                    }, 600);
                },1000)

            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                //alert('No response from server');
            });
    }

    $('#reset_filter').on('click', function () {
        $('.start_date').val('').removeClass('addDate');
        $('.end_date').val('').removeClass('addDate');
        location.replace(location.href.split('?')[0]);
        getReservations(1);
    });


    /*$('.update_reservation').on('click', function () {
        $('.alert-success, .alert-danger').hide();
        var formdata = $('#reservation-edit').serialize();
        var url = $('#reservation-edit').attr('action');
        var method = $('#reservation-edit').attr('method');
        var resv_id = $('#resv_id').val();
        $('#loader').removeClass('hidden');
        $.ajax({
            type: method,
            url: url,
            data: formdata,
            success: function (data) {
                console.log(data);
                getAndSetReservationDetail(resv_id);
                $('.alert-success').show();
                $('.alert-success  .message_cont').empty().html('<p>'+data.message+'</p>');
                $('.edit_reservation').show();
                $('.update_reservation').hide();
                $('.cancel_edit_reservation').hide();
                $('#loader').addClass('hidden');
                makeBookingInfoUnEditable();
            },
            error: function (data, textStatus, errorThrown) {
                $('#loader').addClass('hidden');
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });
                $('.alert-danger .message_cont').empty().html(err);
                $('.alert-danger').show();
            }
        });
    });*/
    /*$('.alert').on('click','.close',function(){
        $(this).closest('.alert').slideUp();
    });*/
    /*$('.cancel_edit_reservation').on('click', function () {
        var resv_id = $('#resv_id').val();
        getAndSetReservationDetail(resv_id);
        $('.edit__field.date').attr('disabled', true);
        $('.edit__field.time').attr('disabled', true);
        $('.party_size').attr('disabled', true);
        $('.input-group-btn').hide();
        $('.edit_reservation').show();
        $('.update_reservation').hide();
        $('.cancel_edit_reservation').hide();
        makeBookingInfoUnEditable();
    });*/


    $("#reservationDetailModal").on("hidden.bs.modal", function () {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            page = 1;
        }
        getReservations(page);
    });
});

function checkForInit() {
  if ($("#grid-calendar .fc-view").children().length) {
    updateClock();
    $(".grid-clock-container").removeClass("hide");
  } else {
    setTimeout(function() {
      checkForInit();
    }, 10);
  }
}

function updateClock() {

    var today = new Date();
    var currentTime= moment.tz(timeZoneType).format('HH:mm'); //moment.utc().format('HH:mm'); //moment.tz(today, "UTC").format('HH:mm');
    var h = today.getHours();
    var m = today.getMinutes();
    m = checkTime(m);
    //document.getElementById('grid-clock').innerHTML = h + ":" + m ;
    $("#gridTimerStyle").empty().append(".fc-now-indicator.fc-now-indicator-line:before{content: '"+currentTime+"';}");
    var comp_min = h * 60;
    var tot_min = comp_min + m;
    var tot_min_per = (tot_min * 100) / 1440;
    $(".grid-clock").css("left", tot_min_per + "%");
    var t = setTimeout(updateClock, 500);
}

function checkTime(i) {
    if (i < 10) {
    i = "0" + i;
    } // add zero in front of numbers < 10
    return i;
}

/*function getReservationList(page) {
    var search_term = $('#reservation_search').val().trim();
    $.ajax(
        {
            url: '?page=' + page + '&keyword=' + search_term,
            type: 'get',
            //data: inputData,
            datatype: "html",
            // beforeSend: function()
            // {
            //     you can show your loader
            // }
        })
        .done(function (data) {
            //$(".guest_listing").empty().html(data);
            $('#today_reservation').html(data);
            console.log(data);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            //alert('No Reservations found');
        });
}*/
