
function Confirm(title, message, confirmresp){

    var boxstr =
          '<div class="modal fade modal-popup" id="confirmboxcommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> <div class="modal-dialog  modal-sm"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title" id="myModalLabel">' +
          title +
          '</h4> </div> <div class="modal-body text-center"> <p>' +
          message +
          '</p> </div> <div class="modal-footer"> <a class="btn btn__primary btn-ok yes" id="yesconfirm">Yes</a> <button id="noconfirm" type="button" class="btn" data-dismiss="modal">No</button> </div> </div> </div> </div>';

    var resp = false;

    if ($("#confirmboxcommon").length) {
      $("#confirmboxcommon").remove();
    }

    $("body").append(boxstr);
    var modalcur = $("#confirmboxcommon");
    modalcur.modal("show").one("click", "#yesconfirm", function(e) {
      resp = true;
      confirmresp(resp, modalcur);
    });

    modalcur.modal("show").one("click", "#noconfirm", function(e) {
      resp = false;
      confirmresp(resp, modalcur);
    });
}

function Alert(title, message, confirmresp) {

 // $(".modal").modal("hide");
    var boxstr =
      '<div class="modal fade modal-popup" id="alertbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> <div class="modal-dialog  modal-sm"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title" id="myModalLabel">' +
      title +
      '</h4> </div> <div class="modal-body text-center"> <p>' +
      message +
      '</p> </div> <div class="modal-footer"></div> </div> </div> </div>';

    var resp = false;

    if ($("#alertbox").length) {
      $("#alertbox").remove();
    }

    $("body").append(boxstr);
    var modalcur = $("#alertbox");
    modalcur.modal("show");
    confirmresp(modalcur);
}



$("#modifiergroup_form").submit(function(e){

      e.preventDefault();
      var languageId = $('#menu_item_edit_form_language_id').val();
      var inputData = $(this).serialize() + '&language_id=' + languageId;
      //console.log(inputData);
      var url=$(this).attr('action');

      $.ajax({
        url:url,
        dataType: 'json',
//        contentType: "application/json; charset=utf-8",
        type: 'post',
        data: inputData,
        success: function (data) {
         
           Alert('Success',data.message, function(modal){
               setTimeout(function() {
                   modal.modal('hide');
               }, 2000);
               $('#itemodifiergroup_form .text-center').remove();
               $(data.data).insertAfter($('#itemodifiergroup_form').find('input[name="_token"]'));
            });
            
        },
        error: function (data, textStatus, errorThrown) {

            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            Alert('Error',err,function(modal){

                setTimeout(function() {
                    // $('#add-tabletype-modal').modal('hide');
                    // location.reload();
                }, 2000);

            });
        },
        complete: function(data) {
            $('#modifiergroup').modal('hide');
        }
    });
    });


$(document).on('submit','#itemodifiergroup_form',function(e){

     e.preventDefault();
     var languageId = $('#menu_item_edit_form_language_id').val();
     var inputData = $(this).serialize() + '&language_id=' + languageId;
     //console.log(inputData);
     var url=$(this).attr('action');

     $.ajax({
        url:url,
        dataType: 'json',
//        contentType: "application/json; charset=utf-8",
        type: 'post',
        data: inputData,
        success: function (data) {
         
           Alert('Success',data.message, function(modal){
                setTimeout(function() {
                  modal.modal('hide');
                //$('#modifiers').html(data.data);
                   //location.reload();
                }, 2000);

            });
            
        },
        error: function (data, textStatus, errorThrown) {

            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            Alert('Error',err,function(modal){

                setTimeout(function() {
                   // $('#add-tabletype-modal').modal('hide');
                   // location.reload();
                }, 2000);

            });
        },
        complete: function(data) {
        }
    });
    });


$(document).on('change','.quantity_type',function(){

      var quantity_type=$(this).val();
      var quantityval_item=$(this).parents('.quantity_type_select').next('.quantity_type_val');
      console.log(quantityval_item.html());
      if(quantity_type!=''){
        if(quantity_type=='Value'){
           quantityval_item.find('.range_quantity').hide();
          quantityval_item.find('.value_quantity').show();
        }else{
          quantityval_item.find('.value_quantity').hide();
          quantityval_item.find('.range_quantity').show();
        }

      }else{
        $('.range_quantity,.value_quantity').hide();
      }
})


$(document).on('click','.modifier_item_temp_dlt',function(){
    //var temp_row_count=$(this).parents('.modifer_group_list').find('.modifier_item_temp_dlt').length;
    var temp_row_count=$(this).parents('.modifer_group_list').find('tr').length;

    if(temp_row_count>1){
        $(this).parents('tr').remove();
    }else{
        $(this).parents('.modifer_group_list').remove(); 
    }
});

$(".delete_modifier_group").click(function(e){

    var group_id=$(this).attr('modifiergroup');
    var panel=$(this).parents('.panel-default');
    e.preventDefault();    

    Confirm('Confirm Delete','All data related to this  will be deleted. Do you wish to proceed?',function(resp,curmodal){

        if(resp){
            console.log('deleting '+group_id);

            $.ajax({
                type: 'delete',
                url: '/menu/modifier-group/'+group_id,
                // data: formdata,
                success: function (data) {
                    Alert('Deleted',data.message,function(modal){

                        setTimeout(function() {
                            modal.modal('hide');
                            //location.reload();
                            panel.remove();
                        }, 2000);

                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    Alert('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            location.reload();
                        }, 2000);

                    });
                }
            });

            curmodal.modal('hide');
        }
    });
  
    });


$(".delete_modifier_item").click(function(e){

    var panel=$(this).parents('tr');
    var modifieritem=$(this).attr('modifieritem');  
    e.preventDefault();    

    Confirm('Confirm Delete','All data related to this  will be deleted. Do you wish to proceed?',function(resp,curmodal){

        if(resp){
            console.log('deleting '+modifieritem);

            $.ajax({
                type: 'delete',
                url: '/menu/modifieritem/'+modifieritem,
                // data: formdata,
                success: function (data) {
                    Alert('Deleted',data.message,function(modal){

                        setTimeout(function() {
                            modal.modal('hide');
                            panel.remove();
                           // location.reload();
                        }, 2000);

                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    Alert('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            location.reload();
                        }, 2000);

                    });
                }
            });

            curmodal.modal('hide');
        }
    });
  
});



$(".get_modifier_item").click(function(e){

    e.preventDefault();
    var modifier_item_id=$(this).attr('modifieritem');
    var languageId = $('#menu_item_edit_form_language_id').val();
    
    if(modifier_item_id){

        $.ajax({
            type: 'get',
//            contentType: "application/json; charset=utf-8",
            url: '/menu/modifieritem/'+modifier_item_id + '/' + languageId,
            success: function (data) {                
                if(data){
                    $('#modifier_item_option_info_popup').modal('show');
                    $('#modifier_item_option_info_popup .modal-content').html(data.data);                
                }
            },
            error: function (data, textStatus, errorThrown) {
                $('#modifier_item_option_info_popup').modal('hide');
                if(data.responseJSON.errors){
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    Alert('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            // location.reload();
                        }, 2000);

                    });
                }
            }
        });
    }

});


  $(document).on('click','#modifieroptiongroupopen',function(e){
      e.preventDefault();
      var modifier_item_id=$(this).attr('modifieritem');
      $('#modifieroptiongroup').modal('show');
      $('#modifieroptiongroup_form').find('#modifier_item_id').val(modifier_item_id);   
  });


  $("#modifieroptiongroup_form").submit(function(e){

      e.preventDefault();
      var languageId = $('#menu_item_edit_form_language_id').val();
      var inputData = $(this).serialize() + '&language_id=' + languageId;
      console.log(inputData);
      var url=$(this).attr('action');
      
      $.ajax({
         url:url,
          dataType: 'json',
//          contentType: "application/json; charset=utf-8",
          type: 'post',
          data: inputData,
          success: function (data) {
           
            // Alert('Success',data.message, function(modal){ 
                 // setTimeout(function() {
                     //modal.modal('hide');
                      if(data){
                  $("#modifieroptiongroup").modal('hide');
                          $('#modifier_item_option_info_popup').modal('show');
                          $('#modifier_item_option_info_popup .modal-content').html(data.data);
                      }

                  //}, 2000);
                 
             // });
              
          },
          error: function (data, textStatus, errorThrown) {

              var err='';
              $.each(data.responseJSON.errors, function(key, value){
                  err+='<p>'+value+'</p>';
              });
              Alert('Error',err,function(modal){

                  setTimeout(function() {
                     // $('#add-tabletype-modal').modal('hide');
                      location.reload();
                  }, 2000);

              });
          },
          complete: function(data) {
              $('#modifiergroup').modal('hide');
          }
      });
});


 $(document).on('click','.delete_modifier_option_group',function(e){

 //$(".delete_modifier_option_group").click(function(e){
    var panel=$(this).parents('.panel-default');
    var group_id=$(this).attr('modifier_item_option_group_id');
    var modifier_item=$(this).attr('modifier_item');  
    e.preventDefault();   

    Confirm('Confirm Delete','All data related to this  will be deleted. Do you wish to proceed?',function(resp,curmodal){

        if(resp){
            console.log('deleting '+group_id);

            $.ajax({
                type: 'delete',
                url: '/menu/item/modifieroptiongroup/'+group_id,
                data:{'modifier_item':modifier_item},
                success: function (data) {
                    Alert('Deleted',data.message,function(modal){
                        setTimeout(function() {
                           modal.modal('hide');
                           panel.remove();
                        }, 2000);  
                    });
             /* if(data){
              $("#modifieroptiongroup").modal('hide');
              $('#modifier_item_option_info_popup').modal('show');
              $('#modifier_item_option_info_popup .modal-content').html(data.data);
              }*/
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    Alert('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            location.reload();
                        }, 2000);

                    });
                }
            });

            curmodal.modal('hide');
        }
    });
  
    });

 $(document).on('submit','#modifiergroup_form_option',function(e){

     e.preventDefault();
     var languageId = $('#menu_item_edit_form_language_id').val();
     var inputData = $(this).serialize() + '&language_id=' + languageId;
     console.log(inputData); 
     var url=$(this).attr('action');

      $.ajax({

        url:url,
        dataType: 'json',
//        contentType: "application/json; charset=utf-8",
        type: 'post',
        data: inputData,
        success: function (data) {
         
           Alert('Success',data.message, function(modal){
             setTimeout(function() {
              $('#modifier_item_option_info_popup').modal('hide');
              modal.modal('hide');
                }, 2000);
           

            });
            
        },
        error: function (data, textStatus, errorThrown) {

            var err='';
            $.each(data.responseJSON.errors, function(key, value){
                err+='<p>'+value+'</p>';
            });
            Alert('Error',err,function(modal){

                setTimeout(function() {
                   // $('#add-tabletype-modal').modal('hide');
                   // location.reload();
                }, 2000);

            });
        },
        complete: function(data) {
        }
    });
});


 $(document).on('click','.add_modifier_option',function(){

    var modifieroptiongroup=$(this).attr('modifier_item_option_group'); 
    var modifer_option_list=$(this).parent('.row').siblings('.modifer_option_list');
    var modifier_item_count=modifer_option_list.find('tbody tr').length;
    var modifier_option_name="modifier_item_option_group["+modifieroptiongroup+"][modifier_options]["+modifier_item_count+"][modifier_option_name]";
    var modifier_option_price="modifier_item_option_group["+modifieroptiongroup+"][modifier_options]["+modifier_item_count+"][modifier_option_price]";

    if(modifer_option_list.length){

        var data='<tr> <td><div class="form-group"><input type="text" class="form-control" name="'+modifier_option_name+'" placeholder="Modifier Option Name" value=""></div> </td> <td><div class="form-group"><input type="text" class="form-control" name="'+modifier_option_price+'" placeholder="Price" value=""></div></td> <td><i class="fas fa-times delete_modifier_option"></i> <a href="#"></a></td> </tr>';
        modifer_option_list.find('tr:last').after(data);

    }else{
      // insertBefore
      var data='<table class="table table-striped modifer_option_list"> <thead> <tr><th>Modifier Option Name</th> <th>Price</th> <th>Options</th> </tr> </thead> <tbody> <tr> <td><div class="form-group"><input type="text" class="form-control" name="'+modifier_option_name+'" placeholder="Modifier Name" value=""></div> </td> <td><div class="form-group"><input type="text" class="form-control" name="'+modifier_option_price+'" placeholder="Price" value=""></div></td> <td><i class="fas fa-times delete_modifier_option"></i> <a href="#"></a></td> </tr> </tbody> </table>';
      $( this).parent('.row').prev('.form-group').after( data);

    }

})
 $(document).on('click','.delete_modifier_option',function(e){

    var panel=$(this).parents('tr');
    var modifieroptionid=$(this).attr('modifieroptionid');  
    e.preventDefault();   

    Confirm('Confirm Delete','All data related to this  will be deleted. Do you wish to proceed?',function(resp,curmodal){

        if(resp){
            console.log('deleting '+modifieroptionid);

            $.ajax({
                type: 'delete',
                url: '/menu/modifieroption/'+modifieroptionid,
                // data: formdata,
                success: function (data) {
                    Alert('Deleted',data.message,function(modal){

                        setTimeout(function() {
                            modal.modal('hide');
                            panel.remove();
                           // location.reload();
                        }, 2000);

                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    Alert('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            location.reload();
                        }, 2000);

                    });
                }
            });

            curmodal.modal('hide');
        }
    });
  
    });
//additemsvisibilityform  -- for pop up form
//addvisibilityschedule  -- for pop up trigger id
//visibility_schedule  -- for schedule tab div

    getRestaurantDurationHours = function () {
        var menuItemId = $('#menuItemId').val();
        var restaurantId = $('#menuItemRestaurantId').val();
        var url = '/menu/menu-item-visibility/list/'+restaurantId+'/' + menuItemId;

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'get',
            success: function (data) {
                $('#visibility_schedule_id').html(data.data);
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error- Menu Item Schedule Listing', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    };
    
    setDurationHours = function () {
        var menuItemId = $('#menuItemId').val();
        var visibility_schedule_id = $('#visibility_schedule_id').val();
        var url = '/menu/menu-item-visibility/add/'+visibility_schedule_id+'/' + menuItemId;

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'get',
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                }

                alertbox(alertboxHeader, data.message, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                    
                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error- Menu Item Schedule', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {$('#language')
//            $('#addongroup').modal('hide');
            }
        });
    };

    $(document).ready(function () {
        $('#language').change(function(){
            var currUrl = window.location.href;
            if(currUrl.endsWith('edit')) {
                window.location.href = currUrl + '/' + $('#language').val()
            } else {
                var currUrlWithoutId = currUrl.substr(0, currUrl.lastIndexOf("/"));
                window.location.href = currUrlWithoutId + '/' + $('#language').val()
            }
            // $('#menu_item_edit_form_language_id').val($('#language').val());
            // $('#menu_item_store_form_language_id').val($('#language').val());
        });
        
        getRestaurantDurationHours();
        $('#schedule button').click(function(){
            setDurationHours();
        });
        
        if ($('#manage_inventory').prop('checked')==true){ 
            $('.manage_inventory_hide_div').show();
        } else {
            $('.manage_inventory_hide_div').hide();
        }
        
        $('#manage_inventory').change(function(){
            if ($(this).prop('checked')==true){ 
                $('.manage_inventory_hide_div').show('slow');
            } else {
                $('.manage_inventory_hide_div').hide('slow');
            }

        });
        
    });
