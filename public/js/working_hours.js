var slot_diff=15;
$(document).ready(function () {
    var dateView = 'date';
    var dayView = 'day';
    var dayoffToggle = false;
    displayWeekCalendar();
    var timeInterval=0;
    var is_custom_popup_open=0;
    $("#custom_calendar").fullCalendar({
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        header: false,
        defaultView: 'timelineDay',
        selectable: true,
        defaultTimedEventDuration: '00:30:00',
        minTime: "00:00:00",
        maxTime: "24:00:00",
        slotMinutes: 15,
        slotDuration: '00:15:00',
        slotLabelFormat: 'HH:mm',
        slotWidth:60,
        scrollTime: '00:00:00',
        editable: true,
        eventStartEditable: false,
        eventDurationEditable: true,
        allDaySlot: false,
        height: 'auto',
        businessHours: [{
            dow: [0, 1, 2, 3, 4, 5, 6], // Monday - Friday
            start: '00:00',
            end: '24:00',
        }],
        select: function (start, end, jsEvent, view) {
            is_custom_popup_open=1;
            resetWorkingForm();
            var $form = $('#custom_hours');
            if(!$form.valid()){
                return false;
            }

            weekHoursResetForm();

            var open_time_hour = moment(start).format("HH");
            var open_time_min = moment(start).format("mm");
            var close_time_hour = moment(end).format("HH");
            var close_time_min = moment(end).format("mm");

            openTimeCloseTime(open_time_hour,open_time_min,close_time_hour,close_time_min)

            var arry = [];

            $("#adjust-turnoverTime .table_types_tat[data-turnoverTime]").each(function(key, value){
                var valueTurnoverTime = $(this).val();
                arry.push(valueTurnoverTime);
            })

            var getMinTouroverTime = JSON.stringify(Math.min.apply(null, arry))

            if(getMinTouroverTime <= finalTimeInMins) {
                clearFormdata();

                saveWeekTableAvailability(dateView, start, end);
                toggleFormEditable(true);
            } else if(getMinTouroverTime!=null) {
                $("#confirm-turnover-alert").modal('show');
                $("#confirm-turnover-alert #minTurnoverTime").text(getMinTouroverTime);
            }

        },
        selectAllow: function(selectInfo) {
            var duration = moment.duration(selectInfo.end.diff(selectInfo.start));
            timeInterval = round(duration.asMinutes(), 1);
        }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if(target=='#custom_working_hours'){
            displayCustomRangeCalendar();
            $("#type").val(dateView);
        }else{
            displayWeekCalendar();
            $("#type").val(dayView);
        }
    });

    function isOverlapping(event){
        var array = calendar.fullCalendar('clientEvents');

        for(i in array){
            if(array[i].id != event.id){
                //console.log(array[i].id)
                if(!(array[i].start >= event.end || array[i].end <= event.start)){
                    return true;
                }
            }
        }



        return false;
    }


    function displayWeekCalendar(){
        $("#type").val(dayView);
        $("#calendar").fullCalendar('render');
        $.ajax({
            url: 'working-hours-schedule',
            dataType: 'json',
            success: function (data) {

                window.dataLogCalendar = data.date_range;

                //console.log(data.events)
                //console.log(data.date_range)

                $('#calendar').fullCalendar({
                    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                    header: false,
                    defaultView: 'timelineDay',
                    selectable: true,
                    defaultTimedEventDuration: '00:30:00',
                    minTime: "00:00:00",
                    maxTime: "24:00:00",
                    disableDragging: true,
                    now: nowTimeShow,
                    slotMinutes: 15,
                    slotWidth:60,
                    slotDuration: '00:15:00',
                    slotLabelFormat: 'HH:mm',
                    scrollTime: '00:00:00',
                    //editable: true,
                    eventStartEditable: false,
                    //eventDurationEditable: true,
                    allDaySlot: false,
                    businessHours: [{
                        dow: [0, 1, 2, 3, 4, 5, 6], // Monday - Friday
                        start: '00:00',
                        end: '24:00',
                    }],
                    navLinks: true,
                    resourceAreaWidth: '170',
                    resourceColumns: [
                        {
                            labelText: '',
                            field: 'title'
                        },
                        {
                            labelText: 'Open',
                            field: 'open'
                        }
                    ],
                    resources:data.date_range,
                    resourceRender: function(resourceObj, $td) {
                        $td.eq(1).find('.fc-cell-content')
                            .html('<button type="button" class="btn btn-xs btn-secondary btn-toggle dayoff active" title="Close"  aria-pressed="true" autocomplete="off" id="dayoff_'+resourceObj.title+'"><div class="handle"></div></button>');

                        if(!resourceObj.dayoff){
                            $('#dayoff_'+resourceObj.title).addClass('active').attr('title','Open');
                            //$("tr[data-resource-id="+ resourceObj.id +"]").removeClass("dayOff");

                        } else {
                            $('#dayoff_'+resourceObj.title).removeClass('active').attr('title','Close');
                            //$("tr[data-resource-id="+ resourceObj.id +"]").addClass("dayOff");
                        }
                    },
                    events: data.events,
                    selectOverlap: function(event) {
                        $("#slot-overlap-alert").modal('show');
                    },
                    eventRender: function(event,element) {
                        element.find('.fc-title').html(event.title);

                        if(event.title){

                            $("tr[data-resource-id="+ event.resourceId +"]").find('.dayoff').addClass("has_events")
                        }

                        var getDayOffBtnId = $("tr[data-resource-id="+ event.resourceId +"]").find('.dayoff').attr('id');

                        $.each(data.date_range, function(key,value){
                            if($('#dayoff_'+value.title).hasClass("active")){
                                $('#dayoff_'+value.title).attr('title','Open');
                                $("tr[data-resource-id="+ value.id +"]").removeClass("dayOff");
                            } else {
                                $('#dayoff_'+value.title).attr('title','Close');
                                $("tr[data-resource-id="+ value.id +"]").addClass("dayOff");
                            }
                        })

                    },
                    height: 'auto',
                    selectConstraint: "businessHours",
                    slotLabelInterval: 15,
                    firstDay: 1,
                    fixedWeekCount: true,
                    aspectRatio: 0.1,
                    select: function (start, end, jsEvent, view, resource) {
                        resetWorkingForm();
                        weekHoursResetForm();

                        $("#availabilityModal .modal-content").addClass('addNewSlot').removeClass('editOldSlot');

                        $('#week_hours').attr('resource_id',resource.id);
                        $('#week_hours .day_repeat_div').show();
                        $('#week_hours .check__box__input').prop('checked', false); //weekdays checkbox reset
                        $( "#week_hours .accordion .btn__holo" ).not('.collapsed').trigger('click');

                        var open_time_hour = moment(start).format("HH");
                        var open_time_min = moment(start).format("mm");
                        var close_time_hour = moment(end).format("HH");
                        var close_time_min = moment(end).format("mm");

                        openTimeCloseTime(open_time_hour,open_time_min,close_time_hour,close_time_min)

                        var arry = [];
                        $("#adjust-turnoverTime .table_types_tat[data-turnoverTime]").each(function(key, value){
                            var valueTurnoverTime = $(this).val();
                            arry.push(valueTurnoverTime);
                        })

                        //isOverlapping(jsEvent)

                        var getMinTouroverTime = JSON.stringify(Math.min.apply(null, arry))

                        if(getMinTouroverTime <= finalTimeInMins) {
                            clearFormdata();
                            saveWeekTableAvailability(dayView, start, end, resource);
                            toggleFormEditable(true);
                        } else if(getMinTouroverTime!=null) {
                            $("#confirm-turnover-alert").modal('show');
                            $("#confirm-turnover-alert #minTurnoverTime").text(getMinTouroverTime);
                        }

                        $.each(dataLogCalendar, function(key, value){
                            var dayName = value.title.toLowerCase();
                            if(value.dayoff) {
                                $("#week_hours .repeat_"+dayName).attr("disabled",true);
                            } else {
                                $("#week_hours .repeat_"+dayName).removeAttr("disabled");
                            }
                        });

                        var selectedDay = resource.title.toLowerCase();

                        $("#week_hours .check__box__input").removeAttr("checked").removeAttr("readonly");
                        if($("#week_hours .repeat_"+selectedDay)){
                            $("#week_hours .repeat_"+selectedDay).attr("checked","checked").attr("readonly","readonly");
                        }

                    },
                    eventClick: function (event, jsEvent, view) {
                        resetWorkingForm();

                        $("#availabilityModal .modal-content").addClass('editOldSlot').removeClass('addNewSlot');


                        if(!$("tr[data-resource-id="+ event.resourceId +"]").hasClass("dayOff")){
                            weekHoursResetForm();

                            $('#week_hours').attr('resource_id',event.resourceId);
                            $('#week_hours').attr('is_editable',1);


                            updateWeekTableAvailability(event, dayView);
                            // $('#week_hours .check__box__input').prop('checked', false);
                            $('#week_hours .day_repeat_div').hide();
                            toggleFormEditable(false);

                            var open_time_hour = moment(event.start._i).format("HH");
                            var open_time_min = moment(event.start._i).format("mm");
                            var close_time_hour = moment(event.end._i).format("HH");
                            var close_time_min = moment(event.end._i).format("mm");

                            openTimeCloseTime(open_time_hour,open_time_min,close_time_hour,close_time_min)

                            $.each(dataLogCalendar, function(key, value){
                                var dayName = value.title.toLowerCase();
                                if(value.dayoff) {
                                    $("#week_hours #repeat_"+dayName).attr("disabled",true);
                                } else {
                                    $("#week_hours #repeat_"+dayName).removeAttr("disabled");
                                }
                            });

                            var filteredDay = dataLogCalendar.filter(function( idx ) {
                                return idx.id == event.resourceId;
                            });

                            $.each(filteredDay, function(key, value){

                                var selectedDay = value.title.toLowerCase();
                                $("#week_hours .check__box__input").removeAttr("checked").removeAttr("readonly");
                                if($("#week_hours #repeat_"+selectedDay)){
                                    $("#week_hours #repeat_"+selectedDay).attr("checked","checked").attr("readonly","readonly");
                                }
                            })

                        }
                    },
                    eventResize: function (event, delta, revertFunc) {
                        updateWeekTableAvailability(event, dayView);
                        toggleFormEditable(true);
                    },
                    selectAllow: function(selectInfo) {
                        var duration = moment.duration(selectInfo.end.diff(selectInfo.start));
                        timeInterval = round(duration.asMinutes(), 1);
                    }
                });
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('addEventSource', data.events);
                $('#calendar').fullCalendar('rerenderEvents' );

                $("#availabilityModal,#confirm-turnover-alert").on("hidden.bs.modal", function () {
                    // put your default event here
                    //location.reload(true);
                });
            }
        });
    }

    //setup before functions
    var typingTimer;                //timer identifier
    var uniqueSlotNameInterval = 100;  //time in ms, 5 second for example
    var $input = $("#week_hours #slot_name");

    //on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(getSlotNameUnique, uniqueSlotNameInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    // paste
    $input.bind('paste', function() {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(getSlotNameUnique, uniqueSlotNameInterval);
    })

    //cut
    $input.bind('cut', function() {
        typingTimer = setTimeout(getSlotNameUnique, uniqueSlotNameInterval);
    });

    $("#week_hours .day_repeat_div input[type=checkbox]").change(function(){
        getSlotNameUnique();
    })

    function getSlotNameUnique(){
        $("#week_hours").valid();
        var selectedDays = [];
        var listOfDays = [];
        var uniqueSlotName = false;
        var getInputData = $("#week_hours #slot_name").val().toLowerCase();
        $("#week_hours .day_repeat_div input:checked").each(function(){
            selectedDays.push($(this).attr("data-dayName"))
        })

        //console.log(dataLogCalendar)

        $.each(dataLogCalendar,function(key,value){

            if($("#week_hours .day_repeat_div").is(":visible")) {

                //For Working Hours
                $.each(selectedDays, function (ky, val) {
                    if (value.title == val) {
                        $.each(value.slot_name, function (k, v) {
                            if (getInputData == v.toLowerCase()) {
                                //console.log(value.slot_name)
                                listOfDays.push(value.title)
                            }
                        })
                    }
                })

            } else {

                //For Custom Rules
                $.each(value.slot_name, function (key, val) {
                    if (getInputData == val.toLowerCase()) {
                        //console.log(value.slot_name)
                        listOfDays.push(value.title)
                    }
                })

            }
        })

        listOfDays = listOfDays.filter(function(itm, i, a) { return i == a.indexOf(itm) });

        if(listOfDays.length == 0){
            if(getInputData.length > 0) {
                $('#week_hours #uniqueSlotName').addClass("hide")
                $('#week_hours .editable_footer .save__btn').removeAttr('disabled');
                $("#week_hours .shift-name i").removeClass("hide");
            } else {
                $('#week_hours .shift-name i, #week_hours #uniqueSlotName').addClass("hide")
            }
        } else {
            $('#week_hours #uniqueSlotName').removeClass("hide").text("This shift name is already used on "+ listOfDays.join(", ") +". Please enter a unique shift name.")
            $('#week_hours .editable_footer .save__btn').attr('disabled','disabled');
            $("#week_hours .shift-name i").addClass("hide");
        }

    }

    function isOverlapping(event) {
        var arrCalEvents = $("#calendar").fullCalendar('clientEvents');
        for (i in arrCalEvents) {
            //console.log(event);
            if (arrCalEvents[i].id != event.id) {
                if ((event.end >= arrCalEvents[i].start && event.start <= arrCalEvents[i].end) || (event.end == null && (event.start >= arrCalEvents[i].start && event.start <= arrCalEvents[i].end))) {//!(Date(arrCalEvents[i].start) >= Date(event.end) || Date(arrCalEvents[i].end) <= Date(event.start))
                    return true;
                }
            }
        }
        return false;
    }

    function displayCustomRangeCalendar() {
        // $("#custom_calendar_view").fullCalendar('render');
        $("#custom_calendar_view").fullCalendar('destroy');
        
        $.ajax({
            url: 'custom-working-hours-schedule',
            dataType: 'json',
            success: function (data) {

                window.dataLogCalendar = data.date_range;

                //console.log(data.date_range)

                if(data.date_range == ""){
                    $('#custom_calendar_view').addClass("hide");
                } else {
                    $('#custom_calendar_view').removeClass("hide").fullCalendar({
                        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                        header: false,
                        defaultView: 'timelineDay',
                        selectable: true,
                        defaultTimedEventDuration: '00:15:00',
                        minTime: "00:00:00",
                        maxTime: "24:00:00",
                        disableDragging: true,
                        slotMinutes: 15,
                        slotWidth:60,
                        slotDuration: '00:15:00',
                        slotLabelFormat: 'HH:mm',
                        scrollTime: '00:00:00',
                        //editable: true,
                        eventStartEditable: false,
                        //eventDurationEditable: true,
                        allDaySlot: false,
                        businessHours: [{
                            dow: [0, 1, 2, 3, 4, 5, 6], // Monday - Friday
                            start: '00:00',
                            end: '24:00',
                        }],
                        navLinks: true,
                        resourceAreaWidth: '300',
                        resourceColumns: [
                            {
                                labelText: '',
                                field: 'title'
                            },
                            {
                                labelText: 'Open',
                                field: 'open'
                            },
                            {
                                labelText: 'Delete',
                                field: 'delete'
                            }
                        ],
                        resources:data.date_range,
                        resourceRender: function(resourceObj, $td) {

                            $td.eq(1).find('.fc-cell-content')
                                .html('<button type="button" class="btn btn-xs btn-secondary btn-toggle dayoff"  aria-pressed="" autocomplete="off" id="dayoff_'+resourceObj.id+'"><div class="handle"></div></button>');

                            if(!resourceObj.dayoff){
                                $('#dayoff_'+resourceObj.id).addClass('active').attr('title','Open');
                                $("tr[data-resource-id="+ resourceObj.id +"]").removeClass("dayOff");

                            } else {
                                $('#dayoff_'+resourceObj.id).removeClass('active').attr('title','Close');
                                $("tr[data-resource-id="+ resourceObj.id +"]").addClass("dayOff");
                            }

                            $td.eq(2).find('.fc-cell-content')
                                .html("<div class='custom_delete deleteworkingslot'  id='deleteCustomSlot_"+resourceObj.id+"'><img src='/images/delete.png' alt='delete'/></div>")
                        },
                        events: data.events,
                        selectOverlap: function(event) {
                            $("#slot-overlap-alert").modal('show');
                        },
                        eventRender: function(event, element) {

                            element.find('.fc-title').html(event.title);
                            if(event.title){
                                $("tr[data-resource-id="+ event.resourceId +"]").find('.dayoff').addClass("has_events")
                            }
                            $.each(data.date_range, function(key,value){
                                if($('#dayoff_'+value.id).hasClass("active")){
                                    $('#dayoff_'+value.id).attr('title','Open');
                                    $("tr[data-resource-id="+ value.id +"]").removeClass("dayOff");
                                } else {
                                    $('#dayoff_'+value.id).attr('title','Close');
                                    $("tr[data-resource-id="+ value.id +"]").addClass("dayOff");
                                }
                            })

                        },
                        height: 'auto',
                        selectConstraint: "businessHours",
                        select: function (start, end, jsEvent, view, resource) {
                            is_custom_popup_open=0;

                            resetWorkingForm();
                            weekHoursResetForm();
                            $('#week_hours').attr('resource_id',resource.id);
                            var open_time_hour = moment(start).format("HH");
                            var open_time_min = moment(start).format("mm");
                            var close_time_hour = moment(end).format("HH");
                            var close_time_min = moment(end).format("mm");

                            openTimeCloseTime(open_time_hour,open_time_min,close_time_hour,close_time_min)

                            var arry = [];
                            $("#adjust-turnoverTime .table_types_tat[data-turnoverTime]").each(function(key, value){
                                var valueTurnoverTime = $(this).val();
                                arry.push(valueTurnoverTime);
                            })


                            var getMinTouroverTime = JSON.stringify(Math.min.apply(null, arry))

                            if(getMinTouroverTime <= finalTimeInMins) {
                                clearFormdata();

                                saveWeekTableAvailability(dateView, start, end, resource);
                                toggleFormEditable(true);
                            } else if(getMinTouroverTime!=null) {
                                $("#confirm-turnover-alert").modal('show');
                                $("#confirm-turnover-alert #minTurnoverTime").text(getMinTouroverTime);
                            }

                        },
                        eventClick: function (event, jsEvent, view) {
                            is_custom_popup_open=0;
                            resetWorkingForm();
                            if(!$("tr[data-resource-id="+ event.resourceId +"]").hasClass("dayOff")){
                                weekHoursResetForm();
                                $('#week_hours').attr('resource_id',event.resourceId);
                                $('#week_hours').attr('is_editable',1);

                                updateWeekTableAvailability(event, dateView);
                                toggleFormEditable(false);

                                $('#week_hours .check__box__input').prop('checked', false);

                                $.each(view.options.resources, function(key, value){
                                    var dayName = value.title.toLowerCase();

                                    if(value.dayoff) {
                                        $("#week_hours #repeat_"+dayName).attr("disabled",true);
                                    } else {
                                        $("#week_hours #repeat_"+dayName).removeAttr("disabled");
                                    }
                                });
                            }
                        },
                        eventResize: function (event, delta, revertFunc) {
                            updateWeekTableAvailability(event, dateView);
                            toggleFormEditable(true)
                        },
                        selectAllow: function(selectInfo) {
                            var duration = moment.duration(selectInfo.end.diff(selectInfo.start));
                            timeInterval = round(duration.asMinutes(), 1);
                        }
                    });

                }


                $('#custom_calendar_view').fullCalendar('removeEvents');
                $('#custom_calendar_view').fullCalendar('addEventSource', data.events);
                $('#custom_calendar_view').fullCalendar('rerenderEvents' );
            }
        });
    }
    /******************************Custom function for phase1 changes****************************************/


    function alertOverlaped(reserourceid,e_startdate,e_enddate){
        /*   console.error(e_startdate);
           console.error(e_enddate);*/
        var type=$('#type').val();
        if(type=='day'){
            var array = $('#calendar').fullCalendar('clientEvents');

        }else{
            if(is_custom_popup_open==1){
                var array = $('#custom_calendar').fullCalendar('clientEvents');

            }else{
                var array = $('#custom_calendar_view').fullCalendar('clientEvents');

            }

        }
        //console.log(array);
        var resp=false;
        var slot_id=$('#week_hours .slot_id').val();
        for(i in array){
            var start=moment(array[i].start).format("YYYY-MM-DD HH:mm:ss");
            var end=moment(array[i].end).format("YYYY-MM-DD HH:mm:ss");


            /* if((array[i].resourceId ==reserourceid)){
                alert((array[i].resourceId ==reserourceid) );
                alert(e_startdate);
                alert(start);
                  alert(e_startdate);
                alert(end);

             }*/
            //console.log(array[i].resourceId ==reserourceid);
            //console.log(array[i].resourceId );
            //console.log(reserourceid);
            if((array[i].resourceId === reserourceid) && ((e_startdate>=start && e_startdate<end) || (e_enddate>start && e_enddate<=end) || (e_startdate==start && e_enddate==end)  || (e_startdate < start && e_enddate>end) )){
                if(slot_id!=0 && slot_id!=array[i].slot_id ){
                    resp=true;
                } else if(slot_id==0){
                    resp=true;
                }
            }

        }
        return resp;

    }

    function getSelectBoxLimitedoptions(max,defaultoption){
        $str='';

        while(max>-1){
            var defaultval='';
            if(defaultoption!=undefined){
                if( max==defaultoption){
                    defaultval='selected';
                }
            }
            $str+='<option '+defaultval+' value="'+max+'">'+max+'</option>';
            max--;
        }



        return $str;
    }

    function getDateforslot(slottime){
        var currdate = new Date();
        currdate.setMinutes(Number(slottime.split(":")[1]));
        currdate.setHours(Number(slottime.split(":")[0]));
        currdate.setSeconds('00');
        return currdate = moment(currdate).format("YYYY-MM-DD HH:mm:ss");
    }

    function getSlotWithIntevals(start,end,interval,onlydiffslot){
        var newslot='';
        var slot=start;
        if(interval==undefined){
            var diff=slot_diff;
        } else {
            var diff=interval;
        }

        var newlsots=[];
        var i=0;
        if(onlydiffslot!=1){
            newlsots.push(start);
        }

        while(i<100 && slot!=end){
            var curr=slot.split(":");
            var newmin=Number(curr[1])+diff;

            if(curr[1]==60 || newmin==60){
                slot=(Number(curr[0])+1)+':00';
            } else if(newmin>60){
                slot=(Number(curr[0])+1)+':'+(newmin-60);
            } else {
                slot=(Number(curr[0]))+':'+newmin;
            }

            slot = ("0" + slot.split(":")[0]).slice(-2)+':'+slot.split(":")[1];

            newlsots.push(slot);
            i++;
        }

        if(onlydiffslot!=1){
            newlsots.pop();
        }


        return newlsots;
    }


    function getTurnaroundBasedonMax(max_count){
        //console.log('heyyy'+max_count);
        var arry = [];
        var resp='';

        $("#adjust-turnoverTime .table_types_tat[data-turnovertime]").each(function(key, elem){
            var max=Number($(this).attr('data-max'));
            var min=Number($(this).attr('data-min'));
            var curval=$(elem).val();


            if(max_count>=min &&  max_count<=max){
                //console.log('found'+max_count+''+curval);
                //console.log(min+'minmaxmatch'+max);
                resp= Number(curval);
                return false;
            }
        })
        return resp;
    }

    function tablecovers_count(max_count,tablecount){

        var start= $("#week_hours .open_time").val();
        var end= $("#week_hours .close_time").val();

        var allsolts=  getSlotWithIntevals(start,end,slot_diff,1);
        //  console.error(allsolts);

        var inter_val=(allsolts.length*slot_diff);
        //console.log(inter_val+"*********");

        var turnaroundCount=getTurnaroundBasedonMax(max_count);
        //console.log(turnaroundCount+"#########");

        /*console.log(turnaroundCount)

         $("#adjust-turnoverTime .table_types_tat").each(function(){
             var tac = $(this).attr('data-turnoverTime')

             $("#adjust-turnoverTime .table_types_tat[data-turnoverTime='"+ turnaroundCount +"']").parents('.tot_outer').hide();

             if(tac != turnaroundCount){
                 $(this).parents('.tot_outer').show();
                 //$("#adjust-turnoverTime .table_types_tat[data-turnoverTime='"+ turnaroundCount +"']").parents('.tot_outer').show();
             }

         })*/


        //$("#adjust-turnoverTime .table_types_tat").parents('.tot_outer').hide();
        //$("#adjust-turnoverTime .table_types_tat[data-turnoverTime='"+ turnaroundCount +"']").parents('.tot_outer').hide();

        //console.error(max_count+'<<<<<<'+inter_val+'====>'+turnaroundCount);
        //console.error(max_count+'===='+tablecount+'==='+parseInt(inter_val/turnaroundCount));
        //console.error(allsolts);
        //alert(turnaroundCount);
        //console.log(parseInt(inter_val/turnaroundCount)+"()()()()");
        //console.log(parseInt(inter_val)+"()()()()");
        //console.error(parseInt(turnaroundCount)+"()()()()");
        var covercount=(max_count*tablecount*(parseInt(inter_val/turnaroundCount)));
        //console.error(covercount);
        return covercount;
    }

    function tablecovers_countWithoutTurnover(max_count,tablecount){
        var start= $("#week_hours .open_time").val();
        var end= $("#week_hours .close_time").val();
        var allsolts=  getSlotWithIntevals(start,end,slot_diff,1);
        //  console.error(allsolts);
        var turnaroundCount=getTurnaroundBasedonMax(max_count);
        var covercount=(max_count*tablecount);
        //console.error(covercount);
        return covercount;
    }


    function refreshTableCoverCountTemp(){

        //alert("refreshTableCoverCountTemp")
        var tot_cover_count=0;
        var cover_count_array=[];
        var total_table_count=0;//tablecount set
        $(".tableSeaterlist .table_cover_count").each(function(key, value){
            var max_count=$(this).attr('table_max');
            var table_count=$(this).attr('table_count');

            //var cov_count=  tablecovers_countWithoutTurnover(max_count,table_count);
            var cov_count=  tablecovers_count(max_count,table_count);
             //console.error(max_count +'--'+table_count +'--'+cov_count);

            cover_count_array[key]=cov_count;
            tot_cover_count+=cov_count;
            total_table_count+=Number(table_count);//TablesCount set
        });

        //$('#week_hours').attr('totaltables',total_table_count);//TablesCount set
        //$('#week_hours').attr('totaltablescovers',tot_cover_count);//TablesCoverCount set

        $(".tableSeaterlist .t_covers").text(tot_cover_count);
        $(".tableSeaterlist .t_tables").text(total_table_count);

        $(".tableSeaterlist .table_cover_count").each(function(key, value){
            $(this).val(cover_count_array[key]);
        });

        //console.log(cover_count_array);
        var tablelen=$('#week_hours').attr('totaltables')?$('#week_hours').attr('totaltables'):1;

        //$('.equal_pacing_reservations,.pacing_reservation').attr('max',tablelen);
        //$('.equal_pacing_covers,.pacing_cover').attr('max',tot_cover_count);

        //$('.equal_pacing_reservations,.pacing_reservation').val(tablelen);
        //$('.equal_pacing_covers,.pacing_cover').val(tot_cover_count);

        var max_covers_limit_opt=getSelectBoxLimitedoptions(tot_cover_count);

        $('select.max_covers_limit_select').html(max_covers_limit_opt);

        $('.selectpicker').selectpicker('refresh')

    }

    function refreshTableCoverCount(){
        var floors = $("#week_hours #multipleFloorsSelectpicker select[name='floors[]'] option:selected").map(function(){return $(this).val();}).get();
        getTableDataForSlotByFloor(floors);
        $('.selectpicker').selectpicker('refresh');
    }

    function UpdatePacing(){

        //alert("UpdatePacing")

        var start= $("#week_hours .open_time").val();
        var end= $("#week_hours .close_time").val();
        var allsolts=  getSlotWithIntevals(start,end,slot_diff,0);
        var tablelen=$('#week_hours').attr('totaltables')?$('#week_hours').attr('totaltables'):1;
        //console.log(allsolts);
        //var pacing_count=$('.t_covers').text();
        var pacing_count=$('#week_hours').attr('totaltablescovers')?$('#week_hours').attr('totaltablescovers'):1;
        var  str='<table class="modify-tbl-avail adjust--pacing"> <thead> <tr> <th width="33.33%">Time</th> <th width="33.33%"> Reservations</th> <th width="33.33%">Covers</th> </tr> </thead> <tbody> <tr> <td> <div> <input class="styled-checkbox" id="equal-pacing" type="checkbox" value="1" name="equal_pacing" checked> <label for="equal-pacing">Equal Pacing</label> </div> </td> <td class=""> <div class="input__field fullWidth"> <input type="number" pattern="[0-9]*" class="text-center" name="equal_pacing_reservations" min=1 value="'+tablelen+'" max="'+tablelen+'"> </div> </td> <td class=""> <div class="input__field fullWidth"> <input type="number" class="text-center" name="equal_pacing_covers" min=1 value="'+pacing_count+'"  max="'+pacing_count+'"> </div> </td> </tr>';
        $.each(allsolts,function(key,val){
            str+='<tr class="hide"> <td>'+val+'</td> <td> <div class="input__field fullWidth"> <input type="number" pattern="[0-9]*" class="pacing_reservation text-center equalPacingValidation" name="pacing_reservation['+val+']" min=1 max="'+tablelen+'" value="'+tablelen+'"> </div> </td> <td> <div class="input__field fullWidth"> <input type="number" pattern="[0-9]*" class="pacing_cover text-center equalPacingValidation" name="pacing_cover['+val+']" min=1 value="'+pacing_count+'" max="'+pacing_count+'"> </div> </td></tr>';
        });
        str+='</tbody> </table>';
        $('.pacingcontainer').html("<div class='scroller'>"+str+"</div>");

        $(".scroller").mCustomScrollbar(options)

    }

    function UpdatePacingWithoutTurnoverTime(){



        var start= $("#week_hours .open_time").val();
        var end= $("#week_hours .close_time").val();
        var allsolts=  getSlotWithIntevals(start,end,slot_diff,0);
        var tablelen=$('#week_hours').attr('totaltables')?$('#week_hours').attr('totaltables'):1;

        //console.log(allsolts);
        //var pacing_count=$('.t_covers').text();


        var total_pacing_count = 0

        $(".tableSeaterlist .table_cover_count").each(function(key, value){
            var max_count=$(this).attr('table_max');
            var table_count=$(this).attr('table_count');
            var pacing_count =  tablecovers_countWithoutTurnover(max_count,table_count);
            total_pacing_count+=pacing_count
            //console.error(max_count +'--'+table_count +'--'+pacing_count);
        });

        $('#week_hours').attr('totaltablescovers',total_pacing_count);

        //console.log(total_pacing_count)

        //var pacing_count=$('#week_hours').attr('totaltablescovers')?$('#week_hours').attr('totaltablescovers'):1;
        var  str='<table class="modify-tbl-avail adjust--pacing"> <thead> <tr> <th width="33.33%">Time</th> <th width="33.33%"> Reservations</th> <th width="33.33%">Covers</th> </tr> </thead> <tbody> <tr> <td> <div> <input class="styled-checkbox" id="equal-pacing" type="checkbox" value="1" name="equal_pacing" checked> <label for="equal-pacing">Equal Pacing</label> </div> </td> <td class=""> <div class="input__field fullWidth"> <input type="number" pattern="[0-9]*" class="text-center" name="equal_pacing_reservations" min=1 value="'+tablelen+'" max="'+tablelen+'"> </div> </td> <td class=""> <div class="input__field fullWidth"> <input type="number" class="text-center" name="equal_pacing_covers" min=1 value="'+total_pacing_count+'"  max="'+total_pacing_count+'"> </div> </td> </tr>';
        $.each(allsolts,function(key,val){
            str+='<tr class="hide"> <td>'+val+'</td> <td> <div class="input__field fullWidth"> <input type="number" pattern="[0-9]*" class="pacing_reservation text-center equalPacingValidation" name="pacing_reservation['+val+']" min=1 max="'+tablelen+'" value="'+tablelen+'"> </div> </td> <td> <div class="input__field fullWidth"> <input type="number" pattern="[0-9]*" class="pacing_cover text-center equalPacingValidation" name="pacing_cover['+val+']" min=1 value="'+total_pacing_count+'" max="'+total_pacing_count+'"> </div> </td></tr>';
        });
        str+='</tbody> </table>';
        $('.pacingcontainer').html("<div class='scroller'>"+str+"</div>");

        $(".scroller").mCustomScrollbar(options)

    }


    $(document).on('click','#week_hours .plus-btn ,#week_hours .minus-btn',function(){
        var resource_id=$('#week_hours').attr('resource_id');

        var start=getDateforslot($("#week_hours .open_time").val());
        var end=getDateforslot($("#week_hours .close_time").val());
        var resp= alertOverlaped(resource_id,start,end);
        if(resp==true){
            alertbox('Slot Overlapping','This slot Overlapping with other slot for same day',function(modal){});
        } else {
            refreshTableCoverCount();
            // UpdatePacing();
        }
    })

    $(document).on('change','#week_hours select.number_of_tables',function(){
        $(this).parents('tr').find('.table_cover_count').attr('table_count',$(this).val());

        var resource_id=$('#week_hours').attr('resource_id');
        //alert(resource_id);
        var start=getDateforslot($("#week_hours .open_time").val());
        var end=getDateforslot($("#week_hours .close_time").val());
        var resp= alertOverlaped(resource_id,start,end);

        refreshTableCoverCountTemp();
        //UpdatePacing();
        //UpdatePacingWithoutTurnoverTime();
    })

    $("#week_hours .open_time , #week_hours .close_time").change(function(){
        var resource_id=$('#week_hours').attr('resource_id');
        var start=getDateforslot($("#week_hours .open_time").val());
        var end=getDateforslot($("#week_hours .close_time").val());
        var resp= alertOverlaped(resource_id,start,end);

        var startTime = $("#week_hours input[name='open_time']").val().match(/\d/g).join("")
        var endTime = $("#week_hours input[name='close_time']").val().match(/\d/g).join("")

        if(startTime > endTime){
            alertbox('Shift Time Alert','Shift start time should be before shift end time',function(modal){});
        } else if(resp==true){
            alertbox('Slot Overlapping','This slot overlapping with other slot for same day',function(modal){});
        } else {
            refreshTableCoverCount();
            //  UpdatePacing();
        }
    });


    function getDefaultTotalCoverCount(){
        var total_pacing_count = 0

        $(".tableSeaterlist .table_cover_count").each(function(key, value){
            var max_count=$(this).attr('table_max');
            var table_count=$(this).attr('table_count');
            var pacing_count =  tablecovers_countWithoutTurnover(max_count,table_count);
            total_pacing_count+=pacing_count
            //console.error(max_count +'--'+table_count +'--'+pacing_count);
        });

        $('#week_hours').attr('totaltablescovers',total_pacing_count);
    }




    function getTableDataForSlotByFloor($floors){
        $.ajax({
            type: 'POST',
            url: '/configure/slot/add',
            data: {floors:JSON.stringify($floors)},
            success: function (data) {
                var data=data.data;
                var tablecount=0;
                var tablecovers=0;

                $str = '<div id="tableSeaterlist" class="scrollerNot"><table class="modify-tbl-avail tableSeaterlist"><tr class="head"><th width="33%">Table Type</th><th width="33%">No of Table for Online Reservations</th><th width="33%">Covers</th></tr>';

                if(data.length){
                    $.each(data,function(key,val){

                        //var covercount_curr=tablecovers_countWithoutTurnover(val.tabletype,val.total);
                        var covercount_curr=tablecovers_count(val.tabletype,val.total);
                        //console.error(val.tabletype +'--'+val.total +'--'+covercount_curr);

                        tablecovers+=covercount_curr;
                        //console.log(covercount_curr+'************');
                        if(covercount_curr > 0){
                            tablecount+=val.total;
                            $str+='<tr class="body"><td>'+val.tabletype+' Seater</td><td><div class="selct-picker-plain"> <select data-size="5" class="selectpicker reservation-book number_of_tables" name="number_of_tables['+val.tabletype+']" class="number_of_tables">'+getSelectBoxLimitedoptions(val.total)+'</select></div></td><td><div class="selct-picker-plain"><input type="text" readonly  name="table_cover_count['+val.tabletype+']" class="table_cover_count" value="'+covercount_curr+'"   table_count="'+val.total+'"  table_max="'+val.tabletype+'"></div></td></tr>';
                        }
                    });

                }

                //alert("getTableDataForSlotByFloor")

                $('#week_hours').attr('totaltables',tablecount);
                $('#week_hours').attr('totaltablescovers',tablecovers);
                $('.equal_pacing_covers,.pacing_cover').attr('max',tablecovers);
                $('.equal_pacing_reservations,.pacing_reservation').attr('max',tablecount);
                $('.equal_pacing_reservations,.pacing_reservation').val(tablecount);
                $('.equal_pacing_covers,.pacing_cover').val(tablecovers);


                $str += '<tr class="foot"><th>Total</th><th class="t_tables">'+tablecount+'</th><th class="t_covers">'+tablecovers+'</th></tr></table></div><div class="max-cover"> Specify maximum cover for this shift<div class="selct-picker-plain"> <select data-size="5" class="selectpicker reservation-book max_covers_limit_select" name="max_covers_limit" id="max_covers_limit">'+getSelectBoxLimitedoptions(tablecovers)+'</select></div></div>';


                $(".table_type_inner").html($str);
                //UpdatePacing();
                UpdatePacingWithoutTurnoverTime();
                $('.selectpicker').selectpicker('refresh')
                //$('.done_slot_btn').attr('disabled',false);

                setTimeout(function(){
                    $(".scroller").mCustomScrollbar(options);
                },10)

                var minPartySize = []

                $("#adjust-turnoverTime .table_types_tat[data-turnoverTime]").each(function(key, value){

                    var minPartySizeTurnoverTime = $(this).attr('data-min')
                    var valueTurnoverTime = $(this).val()

                    minPartySize.push({
                        'data-min':minPartySizeTurnoverTime,
                        'data-turnoverTime':valueTurnoverTime
                    })

                })

                //console.log(tablecovers)

                if(tablecovers == 0){
                    $("#week_hours #availabilityModalAlert").addClass("alert-danger").show().text("Kindly select a shift time greater than or equal to "+ minPartySize[0]['data-turnoverTime'] +" minutes.")
                    $('#week_hours #tableSeaterlist tr.head').eq(0).addClass('hide')
                    $('#week_hours #tableSeaterlist tr.head').eq(1).removeClass('hide')
                    $('#week_hours .editable_footer button').attr('disabled',true);
                    $("#week_hours .accordion").addClass('disabled');
                    $("#week_hours .accordion .btn__holo").not('.collapsed').trigger('click');
                } else {
                    $("#week_hours .accordion").removeClass('disabled');
                    $("#week_hours #availabilityModalAlert").hide().removeClass('alert-danger').text('')
                    $('#week_hours #tableSeaterlist tr.head').eq(0).removeClass('hide')
                    $('#week_hours #tableSeaterlist tr.head').eq(1).addClass('hide')
                    $('#week_hours .editable_footer button').attr('disabled',false);
                }

                //$( "#week_hours .accordion .btn__holo.collapsed" ).trigger('click');
                //$( "#week_hours .accordion  h5:first-child" ).removeClass( "ui-state-disabled" );

            },
            error: function (data, textStatus, errorThrown) {

                //$( "#week_hours .accordion h5:first-child" ).addClass( "ui-state-disabled" );
                $( "#week_hours .accordion .btn__holo" ).not('.collapsed').trigger('click');
                $("#week_hours .accordion").addClass('disabled');

                $('.done_slot_btn').attr('disabled',true);
                //$( "#week_hours accordion" ).accordion( "option", "disabled", true );
                //$( "#week_hours accordion" ).accordion( "option", "disabled", true );

            }
        });
    }

    /**********************************************************************/

    function saveWeekTableAvailability(type, start, end, resource) {

        if (resource === undefined) {
            resource = "";
        }
        //console.log(resource);

        var open_time_hour = moment(start).format("HH");
        var open_time_min = moment(start).format("mm");
        var close_time_hour = moment(end).format("HH");
        var close_time_mins = moment(end).format("mm");

        close_time_hour=((close_time_hour=="00" && open_time_hour!="00") || (close_time_hour=="00" &&  open_time_hour=="00"  &&  close_time_mins=="00" &&  open_time_min=="00" ))?24:close_time_hour;

        //$("#slot_time").val(open_time_hour+":"+open_time_min +" - "+ close_time_hour+":"+close_time_mins);
        var custom_name='';
        if($('#custom_hours #custom_name').length){
            custom_name=$('#custom_hours #custom_name').val();
            $('#custom_hours #custom_name').val('');

        }
        //$("#week_hours .slot_name").val(custom_name);

        $("#week_hours .open_time").val(open_time_hour+":"+open_time_min );
        $("#week_hours .close_time").val(close_time_hour+":"+close_time_mins);
        UpdatePacing();

        ////console.log(close_time - open_time)

        if(type == dayView){
            $('.day_repeat_div').show();
        }else{
            $('#customSettingModal').modal('hide');
            $('.day_repeat_div').hide();
        }

        $('#availabilityModal').modal('show');

        $(".done").unbind('click');


        /*$(".slot_available_floors input").click(function () {
            var floors = $("input[name='floors[]']:checkbox:checked")
              .map(function(){return $(this).val();}).get();
              getTableDataForSlotByFloor(floors);
        });*/

        var getAllfloors = $("#week_hours #multipleFloorsSelectpicker select[name='floors[]'] option").map(function(){return $(this).val();}).get();
        $("#week_hours #multipleFloorsSelectpicker select").selectpicker('val',getAllfloors);
        refreshTableCoverCount();
        /* getTableDataForSlotByFloor(getAllfloors);
*/
        $("#week_hours #multipleFloorsSelectpicker select").on('change', function () {
            var floors = $("#week_hours #multipleFloorsSelectpicker select[name='floors[]'] option:selected").map(function(){return $(this).val();}).get();
            getTableDataForSlotByFloor(floors);
        });

        $(".done").click(function () {
            var $form = $('#week_hours');
            if(!$form.valid()){
                return false;
            }
            $(".done").unbind('click');
            var inputData = $form.serialize();


            var url = $form.attr('action');
            if(resource){
                var resourceId = resource.id;
            }

            if(type == dateView){
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                inputData+= "&start_date="+start_date+"&end_date="+end_date+'&custom_name='+custom_name;
                if(resourceId){
                    inputData+= "&schd_id="+resourceId;
                }
            }else{
                inputData+= "&calendar_day=" + resourceId;

            }
            inputData+="&type="+type;


            $.ajax({
                type: 'POST',
                url: url,
                data: inputData,
                success: function (data) {
                    $('#availabilityModal').modal('hide');
                    alertbox('Success',data.message, function(modal){
                        setTimeout(function() {
                            location.reload();
                            if($("#working_hours").hasClass('active')){
                                addRulesActiveTabs('setWeeklyRulesActiveTab','#working_hours');
                            } else if($("#custom_working_hours").hasClass('active')){
                                addRulesActiveTabs('setCustomRulesActiveTab','#custom_working_hours');
                            }
                        }, 1000);
                    });
                    if(type == dayView){
                        displayWeekCalendar();
                        $("tr[data-resource-id="+ resourceId +"]").removeClass("dayOff");
                    }else{
                        $('#customSettingModal').modal('hide');
                        $('#custom_hours').find('input:text').val('');
                        $(".modal-backdrop").remove();
                        displayCustomRangeCalendar();
                    }
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){

                        setTimeout(function() {
                            //location.reload();
                        }, 2000);

                    });
                }
            });
        });
    }

    /**** Last Tab Open ****/

    if($(".container__custom_working").length){
        var setWeeklyRulesActiveTab = window.localStorage.getItem('setWeeklyRulesActiveTab');
        if (setWeeklyRulesActiveTab && $(setWeeklyRulesActiveTab).length===1) {
            $('#exTab1 .nav a[href="'+ setWeeklyRulesActiveTab +'"]').tab('show')
        }

        var setCustomRulesActiveTab = window.localStorage.getItem('setCustomRulesActiveTab');
        if (setCustomRulesActiveTab && $(setCustomRulesActiveTab).length===1) {
            $('#exTab1 .nav a[href="'+ setCustomRulesActiveTab +'"]').tab('show')
        }

        $("#exTab1 .nav a[href='#custom_working_hours'], .sidebar a").click(function(){
            removeRulesActiveTabs('setWeeklyRulesActiveTab')
        })

        $("#exTab1 .nav a[href='#working_hours'], .sidebar a").click(function(){
            removeRulesActiveTabs('setCustomRulesActiveTab')
        })
    }

    function addRulesActiveTabs(key,id) {
        window.localStorage.setItem(key, id);
    }

    function removeRulesActiveTabs(key) {
        window.localStorage.removeItem(key);
    }

    /**** Last Tab Open ****/

    function openTimeCloseTime(open_time_hour,open_time_min,close_time_hour,close_time_min){

        close_time_hour=((close_time_hour=="00" && open_time_hour!="00") || (close_time_hour=="00" &&  open_time_hour=="00"  &&  close_time_min=="00" &&  open_time_min=="00" ))?24:close_time_hour;


        var finalHour = (close_time_hour - open_time_hour)*60 //convert to mins
        var finalMins = close_time_min - open_time_min;
        window.finalTimeInMins = Number(finalHour + finalMins);

        /*$("#adjust-turnoverTime .table_types_tat[data-turnoverTime]").each(function(key, value){
             var valueTurnoverTime =Number($(this).val());

             if((finalTimeInMins+1) < valueTurnoverTime) {
                 $("#adjust-turnoverTime .table_types_tat[data-turnoverTime='"+ valueTurnoverTime +"']").parents('.tot_outer').hide();
             } else {
                 $("#adjust-turnoverTime .table_types[data-turnoverTime='"+ valueTurnoverTime +"']").parents('.tot_outer').show();
             }
         })*/

    }

    function fillTurnOverTime(party_size){
        var party_size=JSON.parse(party_size);
        $("#adjust-turnoverTime .table_types_tat[data-turnovertime]").each(function(key, value){
            var tat_id=$(this).attr('tat_id');
            $(this).attr('data-turnoverTime',party_size[tat_id]);
            $(this).val(party_size[tat_id]);
        })

    }
    function FillPacing(equal_pacing,pacing){

        //console.log(pacing)

        // var allsolts=  getSlotWithIntevals(start,end,slot_diff,0);
        // var tablelen=$('#week_hours').attr('totaltables')?$('#week_hours').attr('totaltables'):1;
        ////console.log(allsolts);
        //var pacing_count=$('.t_covers').text();
        var pacing=JSON.parse(pacing);

        var tableMaxlen=$('#week_hours').attr('totaltables')?$('#week_hours').attr('totaltables'):1;
        var tablesCoversMaxlen=$('#week_hours').attr('totaltablescovers')?$('#week_hours').attr('totaltablescovers'):1;

        var  str='<table class="modify-tbl-avail adjust--pacing"> <thead> <tr> <th width="33.33%">Time</th> <th width="33.33%"> Reservations</th> <th width="33.33%">Covers</th> </tr> </thead> <tbody>';
        var hidpacing='';
        var nopacing='';
        var is_equal_pacing='';
        var tablelen=0
        var tablesCoverslen=0
        var total_pacing_count = 0

        //console.log(pacing)

        //alert(equal_pacing)

        if(equal_pacing==1){
            hidpacing='hide';
            is_equal_pacing='checked';
            tablelen=pacing[0]['reservations']
            tablesCoverslen=pacing[0]['covers']
        }else{

            tablesCoversMaxlen=$('#week_hours').attr('totaltablescovers')?$('#week_hours').attr('totaltablescovers'):1;

            nopacing='hide';
            tablelen = $('#week_hours').attr('totaltables')?$('#week_hours').attr('totaltables'):1;
            tablesCoverslen = tablesCoversMaxlen
        }

        //console.log(" Tables "+ tablelen +" == "+ tableMaxlen)
        //console.log(" Covers "+ tablesCoverslen +" == "+ tablesCoversMaxlen)

        str+='<tr> <td> <div> <input class="styled-checkbox" id="equal-pacing" type="checkbox" value="1" name="equal_pacing" '+is_equal_pacing+'> <label for="equal-pacing">Equal Pacing</label> </div> </td> <td class=""> <div class="input__field fullWidth '+nopacing+'"> <input type="number" pattern="[0-9]*" class="text-center" name="equal_pacing_reservations" min=1 value="'+tablelen/*pacing[0].reservations*/+'" max="'+tableMaxlen+'"> </div> </td> <td class=""> <div class="input__field fullWidth  '+nopacing+'"> <input type="number" pattern="[0-9]*" class="text-center" name="equal_pacing_covers" min=1 value="'+tablesCoverslen+'"  max="'+tablesCoversMaxlen+'"> </div> </td> </tr>';

        $.each(pacing,function(key,val){
            str+='<tr class="'+hidpacing+'"> <td>'+val.time+'</td> <td> <div class="input__field fullWidth"> <input type="number" pattern="[0-9]*" class="pacing_reservation text-center equalPacingValidation required" name="pacing_reservation['+val.time+']" min=1 max="'+tablelen+'" value="'+val.reservations+'"> </div> </td> <td> <div class="input__field fullWidth"> <input type="number" pattern="[0-9]*" class="pacing_cover text-center equalPacingValidation required" name="pacing_cover['+val.time+']" min=1 value="'+val.covers+'" max="'+tablesCoversMaxlen+'"> </div> </td></tr>';
        });
        str+='</tbody> </table>';


        $('#week_hours .pacingcontainer').html("<div class='scroller'>"+str+"</div>");

        $(".scroller").mCustomScrollbar(options)


    }

  function fillTableSitterInfo(data,totaltable,totalcover,max_covers_limit){

      var floors = $("#week_hours #multipleFloorsSelectpicker select[name='floors[]'] option:selected").map(function(){return $(this).val();}).get();
      var newdata;
      $.ajax({
          type: 'POST',
          url: '/configure/slot/add',
          data: {floors: JSON.stringify(floors)},
          success: function (newdata) {
              var newdata = newdata.data;
              var table_types_count = [];
              $.each(newdata,function(newkey,newval) {
                  table_types_count[newval.tabletype] = newval.total;
              });
              //console.log(table_types_count);

              //$str='<div class="scrollerNot"><table class="modify-tbl-avail tableSeaterlist"><tr><th width="33%">Table Type</th><th width="33%">No of Table for Online Reservations</th><th width="33%">Covers</th></tr>';

              $str = '<div id="tableSeaterlist" class="scrollerNot"><table class="modify-tbl-avail tableSeaterlist"><tr class="head"><th width="33%">Table Type</th><th width="33%">No of Table for Online Reservations</th><th width="33%">Covers</th></tr>';

              var finalTablecount=0;
              var finalTablecovers=0;
              if(data){
                  $.each(data,function(key,val) {
                      //if(val.table_count!=0){
                          var max_covers = table_types_count[val.table_type];
                          finalTablecovers += (max_covers*val.table_type)
                          finalTablecount += max_covers
                          //console.log(val.table_type + ' - ' + max_covers + ' - ' + val.table_count);
                          $str += '<tr class="body"><td>' + val.table_type + ' Seater</td><td><div class="selct-picker-plain"> <select data-size="5" data-dropup-auto="false" class="selectpicker reservation-book number_of_tables" name="number_of_tables[' + val.table_type + ']" class="number_of_tables">' + getSelectBoxLimitedoptions(max_covers, val.table_count) + '</select></div></td><td><div class="selct-picker-plain"><input type="text" readonly  name="table_cover_count[' + val.table_type + ']" class="table_cover_count" value="' + val.table_cover_count + '"   table_count="' + val.table_count + '"  table_max="' + val.table_type + '"></div></td></tr>';
                      //}
                  });

              }

              $('#week_hours').attr('totaltables',finalTablecount);
              $('#week_hours').attr('totaltablescovers',finalTablecovers);
              $('#week_hours .equal_pacing_covers,#week_hours .pacing_cover').attr('max',finalTablecovers);
              $('#week_hours .equal_pacing_reservations,#week_hours .pacing_reservation').attr('max',finalTablecount);

              $str+='<tr><th>Total</th><th class="t_tables">'+totaltable+'</th><th class="t_covers">'+totalcover+'</th></tr></table></div><div class="max-cover"> Specify maximum cover for this shift<div class="selct-picker-plain"> <select data-size="5" data-height="auto" data-width="auto" class="selectpicker reservation-book max_covers_limit_select" name="max_covers_limit" id="max_covers_limit">'+getSelectBoxLimitedoptions(totalcover,max_covers_limit)+'</select></div></div>';
              $(".table_type_inner").html($str);
              $('.selectpicker').selectpicker('refresh')
              $('.done_slot_btn').attr('disabled',false);
              //$( "#week_hours .accordion .btn__holo.collapsed" ).trigger('click');

          }
      });

  }



    function updateWeekTableAvailability(event, type) {

        $("#loader").removeClass('hidden')

        if(type == dateView){
            $('.day_repeat_div').hide();
        }else{
            $('.day_repeat_div').show();
        }
        $.ajax({
            url: 'slot-detail/'+event.resourceId,
            data: 'type='+type+'&slot_id='+event.slot_id,
            dataType: 'json',
            success: function (data) {
                $('#availabilityModal').modal('show');
                $("#loader").addClass('hidden')

                var totalTable = 0;
                var totalCover = 0;
                //$('.total_cover').empty();
                // $('.total_table').empty();

                //alert("updateWeekTableAvailability")

                //console.log(data.slot_detail)

                //$('#week_hours').attr('totaltables',data.slot_detail.available_table_count);
                //getDefaultTotalCoverCount();


                var floors=JSON.parse(data.slot_detail.floors);

                /*$(".slot_available_floors input").each(function () {
                  var cur_val=$(this).val();

                    if(jQuery.inArray(cur_val, floors) !== -1){
                       $(this).attr('checked',true)

                        setTimeout(function(){
                            $(".scroller").mCustomScrollbar(options);
                        },10)
                     }


                 })*/


                $("#week_hours #multipleFloorsSelectpicker select").selectpicker('val',floors);

                $("#week_hours #multipleFloorsSelectpicker select").on('change', function () {
                    var floors = $("#week_hours #multipleFloorsSelectpicker select[name='floors[]'] option:selected").map(function(){return $(this).val();}).get();
                    getTableDataForSlotByFloor(floors);
                });

                $("#week_hours #multipleFloorsSelectpicker select[name='floors[]'] option").each(function () {
                    var cur_val=$(this).val();

                    //console.log(floors)

                    if(jQuery.inArray(cur_val, floors) !== -1){

                        //console.log(floors)

                        $("#week_hours #multipleFloorsSelectpicker select").selectpicker('val',floors);
                        //$(this).attr('selected','selected')

                        setTimeout(function(){
                            $(".scroller").mCustomScrollbar(options);
                        },10)
                    }
                })

                fillTableSitterInfo(data.slot_detail.available_tables,data.slot_detail.available_table_count,data.slot_detail.available_cover_count,data.slot_detail.max_covers_limit);

                setTimeout(function () {
                    FillPacing(data.slot_detail.equal_pacing,data.slot_detail.pacing);
                },500)

                fillTurnOverTime(data.slot_detail.party_size);

                $('#slot_name').val(data.slot_detail.slot_name);
                $('#week_hours .open_time').val(data.slot_detail.open_time);
                $('#week_hours .close_time').val(data.slot_detail.close_time);

                $('#week_hours .slot_id').val(data.slot_detail.slot_id);
                $('#week_hours .schd_id').val(event.resourceId);
                $('#slot_id').val(data.slot_detail.slot_id);

                $('#schd_id').val(event.resourceId);
                $('#available_cover_count').val(data.slot_detail.available_cover_count);
                /*  $.each(data.slot_detail.available_tables, function (key, value) {
                      var table_count =  parseInt(value.table_count);
                      timeInterval = round(moment.duration(moment(event.end).diff(event.start)).asMinutes(), 1);
                      var table_cover = $("#max_seat_"+value.table_type_id).val()*value.table_count*parseInt(timeInterval/$("#turnover_time_"+value.table_type_id).val());
                      $(".table_cover_"+key).html(table_cover);
                      $("#table_type_count_"+value.table_type_id).val(table_count);
                      totalCover+=table_cover;
                      totalTable+=table_count;
                      $('.total_cover').html(totalCover);
                      $('.total_table').html(totalTable);
                      $('#total_table').val(totalTable);
                      $('#total_cover').val(totalCover);onsole.log(selectedDa
                  });*/

                /* var open_time_hour = moment(event.start).format("HH");
                 var open_time_min = moment(event.start).format("mm");
                 var close_time_hour = moment(event.end).format("HH");
                 var close_time_mins = moment(event.end).format("mm");
                 close_time_hour=((close_time_hour=="00" && open_time_hour!="00") || (close_time_hour=="00" &&  open_time_hour=="00"  &&  close_time_mins=="00" &&  open_time_min=="00" ))?24:close_time_hour;
 */
                // $("#slot_time").val(open_time_hour+":"+open_time_min +" - "+ close_time_hour+":"+close_time_mins);


                $(".done").unbind('click');
                $(".done").click(function () {
                    var $form = $('#week_hours');
                    if(!$form.valid()){
                        return false;
                    }
                    var selectedDay = event.resourceId;
                    var data = $("#week_hours").serialize();
                    var url = $('#week_hours').attr('action');

                    $('#availabilityModal').modal('toggle');

                    $.ajax({
                        type: 'PUT',
                        url: url + '/' + event.resourceId,
                        data: data +"&calendar_day=" + selectedDay + "&type="+type + "&schd_id=" + event.resourceId+'&slot_id='+event.slot_id,
                        success: function (data) {
                            alertbox('Success',data.message, function(modal){
                                location.reload();
                                if($("#working_hours").hasClass('active')){
                                    addRulesActiveTabs('setWeeklyRulesActiveTab','#working_hours');
                                } else if($("#custom_working_hours").hasClass('active')){
                                    addRulesActiveTabs('setCustomRulesActiveTab','#custom_working_hours');
                                }
                            });
                            if(type == dateView){
                                displayCustomRangeCalendar();
                            }else{
                                displayWeekCalendar();
                            }
                        },
                        error: function (data, textStatus, errorThrown) {
                            var err='';
                            $.each(data.responseJSON.errors, function(key, value){
                                err+='<p>'+value+'</p>';
                            });
                            alertbox('Error',err,function(modal){

                                setTimeout(function() {
                                    //location.reload();
                                }, 2000);

                            });
                        }
                    });
                });
            }
        });
    }
    function updateReservation(resourceId,type,status,currel){
        $.ajax({
            type: 'POST',
            url: 'working-hours-dayoff',
            data: "resourceId=" + resourceId + "&status=" + status+"&type="+type,
            success: function (data) {
                if(currel!=undefined){
                    currel.removeClass('active');
                    currel.removeClass('has_events');
                    currel.attr("aria-pressed",false);
                }

                if(type == dateView){
                    displayCustomRangeCalendar();
                }else{
                    displayWeekCalendar();

                }
            },
            error: function (data, textStatus, errorThrown) {
                var err='';
                $.each(data.responseJSON.errors, function(key, value){
                    err+='<p>'+value+'</p>';
                });
                alertbox('Error',err,function(modal){

                    setTimeout(function() {
                        //location.reload();
                    }, 2000);

                });

            }
        });
    }
    $(document).on('click','.dayoff',function(event){

        var currel=$(this);
        var resourceId = this.id;
        var type = $("#type").val();

        //var status = this.getAttribute("aria-pressed");
        var status =$(this).hasClass('active')?false:true;
        //console.log(status);

        if($(this).hasClass('active') && $(this).hasClass('has_events')){
            var inputData = 'type='+type+'&resourceId='+resourceId+'&action=dayoff';
            $.ajax({
                type: 'GET',
                url: 'working-hours-check-reservation',
                data: inputData,
                success: function (data) {
                    ConfirmBox('Confirm restaurant closing','You are about to close a day. All the shifts will be deleted. Do you want to proceed?',function(resp,curmodal){
                        if(resp){
                            updateReservation(resourceId,type,status,currel);
                            curmodal.modal('hide');

                        }
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err = '';
                    $.each(data.responseJSON.errors, function (key, value) {
                        err += '<p>' + value + '</p>';
                    });
                    alertbox('Error', err, function (modal) {

                        setTimeout(function () {
                            //location.reload();
                        }, 2000);

                    });
                }
            });
        }else{
            if(status){
                currel.addClass('active');
            }else{
                currel.removeClass('active');
            }

            currel.attr("aria-pressed",true);
            updateReservation(resourceId,type,status);
        }


    });

    /*
        $(document).on('click','.dayoff',function(){

            var resourceId = this.id;
            var type = $("#type").val();
            var status = this.getAttribute("aria-pressed");
            //console.log(type);
            $.ajax({
                type: 'POST',
                url: 'working-hours-dayoff',
                data: "resourceId=" + resourceId + "&status=" + status+"&type="+type+"&_token=" + $('#token').val(),
                success: function (data) {
                    if(type == dateView){
                        displayCustomRangeCalendar();
                    }else{
                        displayWeekCalendar();

                    }
                },
                error: function (data, textStatus, errorThrown) {

                }
            });
        });
        */

    $('#customSettingModal').on('shown.bs.modal', function () {
        $('#custom_hours').find('input:text, checkbox').val('');
        $("#custom_calendar").fullCalendar('render');
    });

    $(function(){
        $('#custom_hours #start_date').datepicker({
            showAnim: "slideDown",
            dayNamesMin: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
            minDate:0,
            beforeShow : function(){
                $( this ).datepicker('option', {
                    maxDate: ($('#end_date').val())?$('#end_date').val():"+12m",
                    minDate: 0,
                    dateFormat: 'mm-dd-yy'
                });
            },
            onSelect: function(dateText, inst) {
                $(this).focusout();
            }
        });
        $('#custom_hours #end_date').datepicker({
            showAnim: "slideDown",
            dayNamesMin: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
            minDate: 0,
            beforeShow : function(){
                $( this ).datepicker('option', {
                    minDate: ($('#start_date').val())?$('#start_date').val():0,
                    dateFormat: 'mm-dd-yy',
                    onSelect: function(dateText, inst){
                        $(this).focusout();
                        //$("#start_date").datepicker('option', 'minDate', dateText);
                    }
                });
            },
            onSelect: function(dateText, inst) {
                $(this).focusout();
            }
        });

        /*  $(".moreThen0").rules("add", {
              required:true,
              min:1
          });*/

        jQuery.validator.addMethod("min_custom", function(value, element, param) {
            return this.optional(element) || value >= param;
        }, jQuery.format("Please enter atleast 1 table."))

        /*  $(".notLessThen0").rules("add", {
              required: true,
              min_custom:1
          });*/

        /* jQuery.validator.addMethod("require_from_group_custom", function(value, element, options) {
             // Nathan's code without any changes
             var validator = this;
             var selector = options[1];
             var validOrNot = $(selector, element.form).filter(function() {
                     return validator.elementValue(this);
                 }).length >= options[0];

             if(!$(element).data('being_validated')) {
                 var fields = $(selector, element.form);
                 fields.data('being_validated', true);
                 fields.valid();
                 fields.data('being_validated', false);
             }
             return validOrNot;

             //return this.optional(element) || 0 >= param
         }, jQuery.format("Please enter a value greater than or equal to 1 in Table Type."));

         // "filone" is the class we will use for the input elements at this example
         jQuery.validator.addClassRules("required-table-value-greater-then-0", {
             require_from_group_custom: [1,".required-table-value-greater-then-0"]
         });*/


    });

    $(document).on("change", "[name='tableTypeCount[]']", function() {
        var sum = 0;
        var total_cover = 0;
        var max_seat = 0;
        var table_cover = 0;
        var tat = 0;

        /*$('.total_cover').empty();
        $('.total_table').empty();
        $("[name='tableTypeCount[]']").each(function(key, value){
            if($(this).val()){
                max_seat = $("[name='tableTypeMax["+key+"]']").val();
                tat = $("[name='turnoverTime["+key+"]']").val();
                table_cover = $(this).val()*max_seat*(parseInt(timeInterval/tat));
                $(".table_cover_"+key).html(table_cover);
                total_cover += table_cover
            }
            sum += +$(this).val();
        });
        $(".total_table").html(sum);
        $(".total_cover").html(total_cover);
        $("#total_table").val(sum);
        $("#total_cover").val(total_cover);*/


        $('.total_cover').html('');
        $('.total_table').val('');
        $("[name='tableTypeCount[]']").each(function(key, value){
            if($(this).val()){
                max_seat = $("[name='tableTypeMax["+key+"]']").val();
                tat = $("[name='turnoverTime["+key+"]']").val();
                table_cover = $(this).val()*max_seat*(parseInt(timeInterval/tat));
                $(".table_cover_"+key).html(table_cover);
                total_cover += table_cover
            } else {
                $('#week_hours div[class^="table_cover_'+ key +'"]').html('');
            }
            sum += +$(this).val();
        });



        //$(".total_table").html(sum);
        $(".total_cover").html(total_cover);
        $("#total_table").val(sum).focus();
        $("#total_cover").val(total_cover);


    });

    function clearFormdata() {

        $('#week_hours .slot_id').val(0);
        $('#week_hours .schd_id').val(0);
        //$('#week_hours').find('input:text').val('');
        $('#week_hours div[class^="table_cover_"]').html('');
        $('.total_cover').html('');
        //$('.total_table').val('');
        $('.alert-danger').hide();
        $('.alert-danger').empty();

        //Form Reset
        $('#week_hours')[0].reset();
        $('#week_hours input:checkbox').removeAttr('checked');
        //$('#week_hours input:number').val('');



        $('#slot_id').val(0);
        $('#schd_id').val(0);
        // $('#week_hours').reset();

        //$( "#week_hours .accordion .btn__holo" ).not('.collapsed').trigger('click');

    }

    function weekHoursResetForm(){
        var weekHoursValidator = $("#week_hours").validate();
        weekHoursValidator.resetForm();

    }
    function resetWorkingForm(){
        $("#week_hours").trigger("reset");
        //$("#availabilityModal .accordion").addClass('disabled');
        //$("#availabilityModal .accordion, #availabilityModal .available-floors, #availabilityModal .shift-time, #availabilityModal .shift-name").addClass('disabled');
        //$("#week_hours .slot_available_floors input.styled-checkbox").attr('checked', false)
        //$("#week_hours .accordion .card-header").not('.collapsed').trigger('click');
        $("#week_hours .editable_footer button").attr("disabled", true);
        $("#week_hours .accordion .btn__holo" ).not('.collapsed').trigger('click');
        $("#week_hours .shift-name i").addClass("hide");
    }

    $('.edit').on('click', function () {
        toggleFormEditable(true);
        if(!$("#custom_working_hours").hasClass("active")){
            $('#week_hours .day_repeat_div').show();
        }

    });

    $('.dayoff_custom').on('click', function () {
        dayoffToggle = this.getAttribute("data-status");

        if($(this).attr('title') == "Open"){
            $(this).attr('title','Close').attr('data-status','true');
            $("#customSettingModal .custom_modalHrs").addClass("hide");
            $("#customSettingModal .custom_done").removeClass("hide");
        } else {
            $(this).attr('title','Open').attr('data-status','false');
            $("#customSettingModal .custom_modalHrs").removeClass("hide");
            $("#customSettingModal .custom_done").addClass("hide");
        }
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(".btn-ok").unbind('click');
        $(this).find('.btn-ok').on('click', function () {
            var schd_id = $('#schd_id').val();
            var slot_id = $('#slot_id').val();
            var type = $('#type').val();
            var inputData = 'type='+type+'&slot_id='+slot_id;
            $.ajax({
                type: 'DELETE',
                url: 'working-hours/'+schd_id,
                data: inputData,
                success: function (data) {
                    $('#confirm-delete').modal('toggle');
                    $('#availabilityModal').modal('hide');
                    if(type == dateView){
                        displayCustomRangeCalendar();
                    }else{
                        displayWeekCalendar();
                    }
                },
                error: function (data, textStatus, errorThrown) {

                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){

                        setTimeout(function() {
                            //location.reload();
                        }, 2000);

                    });
                }
            });
        });
    });

    function toggleFormEditable(switcher) {

        var floors = $("#week_hours #multipleFloorsSelectpicker select[name='floors[]'] option:selected").map(function(){return $(this).val();}).get();

        if(switcher){

            $(".editable_footer").show();
            $(".non_editable_footer").hide();
            $("#week_hours input[type='text']").attr("disabled", false);

            //if($("#availabilityModal .modal-content").hasClass('addNewSlot')){
            $("#availabilityModal .available-floors, #availabilityModal .shift-time, #availabilityModal .shift-name").removeClass('disabled');

            if(floors.length > 0){
                $("#week_hours .accordion").removeClass('disabled');
            }
            //}

        }else{
            $("#week_hours .accordion, #availabilityModal .available-floors, #availabilityModal .shift-time, #availabilityModal .shift-name").addClass('disabled');
            $(".editable_footer").hide();
            $(".non_editable_footer").show();
            $("#week_hours input[type='text']").attr("disabled", true);
            $('input:checkbox').removeAttr('checked');
            $("#week_hours .modal-footer :input").attr("disabled", false);
        }
    }

    $(".custom_done").on('click', function () {
        var $form = $("#custom_hours");
        if(!$form.valid()){
            return false;
        }
        var inputData = $("#custom_hours").serialize();
        var url = $('#custom_hours').attr('action');
        inputData+="&status="+dayoffToggle+"&type="+$("#type").val();
        $.ajax({
            type: 'POST',
            url: url,
            data: inputData,
            success: function (data) {
                $('#customSettingModal').modal('hide');
                $('#custom_hours').find('input:text').val('');
                $(".modal-backdrop").remove();
                displayCustomRangeCalendar();
            },
            error: function (data, textStatus, errorThrown) {
                var err='';
                $.each(data.responseJSON.errors, function(key, value){
                    err+='<p>'+value+'</p>';
                });
                alertbox('Error',err,function(modal){

                    setTimeout(function() {
                        //location.reload();
                    }, 2000);

                });            }
        });
    });

    $(document).on('click', '.custom_delete', function () {
        var schdId = this.id;
        schdId = schdId.split('_');
        $('#schd_id').val(schdId[1]);
        $('#slot_id').val(0);
    });

    function round(value, decimals) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    }

    $("#available_cover_count, #table_type_count_1, #table_type_count_2, #table_type_count_3").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            return false;
        }
    });
    $('#custom_hours').validate({
        errorPlacement: $.datepicker.errorPlacement,
        rules: {
            start_date: {
                required: true
            },
            end_date: {
                required: true
            },
            available_cover_count: {
                required: true
            },
            "tableTypeCount[]":{
                required: true
            }
        },
        messages:{
            start_date:"Please choose a start date.",
            end_date:"Please choose an end date."
        }
    });

    $( "#week_hours" ).validate({
        rules: {
            'open_time': {
                required: true,
            },
            'close_time': {
                required: true,
            },
            'slot_name': {
                required: true,
                maxlength: 20
            },
            'floors[]': {
                required: true,
                minlength: 1
            }
            ,
            'number_of_tables[]': {
                required: true,
            }
            ,
            'table_cover_count[]': {
                required: true,

            },
            'max_covers_limit': {
                required: true,
            },
            'party_size[]': {
                required: true,

            },
            'equal_pacing_reservations': {
                required: "#equal-pacing:checked",
            },
            'equal_pacing_covers': {
                required: "#equal-pacing:checked",
            }/*,
            'pacing_reservation[]': {
                required: function(element) {
                    alert(1)
                    //return $("#equal-pacing:not(:checked)").length==1;
                  },
            },
            'pacing_cover[]': {
                required: function(element) {
                   // return $("#equal-pacing:not(:checked)").length==1;
                  },
            }*/


            /* 'available_cover_count': {
                 min: 0,
                 maxlength: 3
             },
             'tableTypeCount[]': {
                 min: 0,
                 maxlength: 2
             }*/
        },
        messages:{
            slot_name:"Please enter a unique shift name."
        }

    });


    $(document).on('click','#equal-pacing',function(){


        if($(this).prop("checked") == true){
            $(this).parents('td').nextAll('td').find('div').removeClass('hide');
            $(this).parents('tr').nextAll('tr').addClass('hide');
        }
        else if($(this).prop("checked") == false){
            $(this).parents('td').nextAll('td').find('div').addClass('hide');
            $(this).parents('tr').nextAll('tr').removeClass('hide');

            jQuery.validator.addMethod("equalPacingValidation", function(value, element) {
                // allow any non-whitespace characters as the host part
                var val = $( element ).val();
                return val && val.length > 0; ////console.log(2) //this.optional( element ) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@(?:\S{1,63})$/.test( value );
            }, 'This field is required.');

        }

        /*setTimeout(function(){
          $(".scroller").mCustomScrollbar(options);
        },10)*/
    });

});

function confirmDeleteWorkingHour(deltype){
    var desc="";
    var schd_id = $('#schd_id').val();
    var slot_id = $('#slot_id').val();
    var type = $('#type').val();
    var inputData = 'id='+schd_id+'&type='+type+'&slot_id='+slot_id+'&action=delete';
    $.ajax({
        type: 'GET',
        url: 'working-hours-check-reservation',
        data: inputData,
        success: function (data) {
            if(deltype=='slot'){
                desc="You are about to delete one slot. Do you want to proceed?";
            }else{
                desc="You are about to delete a custom setting. Do you want to proceed?";
            }
            $('#confirm-delete .desc p,#confirm-delete:hidden .desc p').text(desc);
            $('#confirm-delete').modal('toggle');
        },
        error: function (data, textStatus, errorThrown) {
            var err = '';
            $.each(data.responseJSON.errors, function (key, value) {
                err += '<p>' + value + '</p>';
            });
            alertbox('Error', err, function (modal) {

                setTimeout(function () {
                    //location.reload();
                }, 2000);

            });
        }
    });
}

$('body').on('click', '.deleteworkingslot', function() {

    if($(this).hasClass('custom_delete')){
        var schdId = this.id;
        schdId = schdId.split('_');

        $('#schd_id').val(schdId[1]);
        $('#slot_id').val(0);

    }
    var deltype='setting';
    if($(this).hasClass('deleteslot')){
        deltype='slot';
    }
    confirmDeleteWorkingHour(deltype);
});
$('#availabilityModal .open_time').timepicker({
    interval: 15,
    step: '15',
    minTime: '00:00',
    maxTime: '23:45',
    timeFormat: "H:i",
    closeOnWindowScroll: true
}).on('click', function (e) {
    $(".ui-timepicker-wrapper").mCustomScrollbar({
        theme: "dark"
    });
});

$('#availabilityModal .close_time').timepicker({
    interval: 15,
    step: '15',
    minTime: '00:30',
    maxTime: '24:00',
    timeFormat: "H:i",
    closeOnWindowScroll: true,
    show2400 : true
}).on('click', function (e) {
    $(".ui-timepicker-wrapper").mCustomScrollbar({
        theme: "dark"
    });
})