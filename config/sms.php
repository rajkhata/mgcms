<?php

return [

       'SMS_AUTH_URL' => env('SMS_AUTH_URL'),
       'SMS_SEND_URL' => env('SMS_SEND_URL'),
       'SMS_API_ID' => env('SMS_API_ID'),
       'SMS_USERNAME' => env('SMS_USERNAME'),
       'SMS_PASSWORD' => env('SMS_PASSWORD'),
       'SMS_FROM' => env('SMS_FROM'),
       'SMS_COUNTRY_CODE' => env('SMS_COUNTRY_CODE'),


];


