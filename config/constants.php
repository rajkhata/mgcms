<?php
    /**
     * Created by PhpStorm.
     * User: Amit Malakar
     * Date: 11/4/18
     * Time: 11:56 AM
     */

    return [
        'builder'=>[
            'upload_path'=>public_path('builder/uploads/'),
            'upload_main_path'=>'builder/uploads/'
        ],
        'dashboard_announcement'=>[
            'last_version'=>1,
            'current_version'=>3,
            'features'=>[
                'Refunds'=>'Enjoy even more control over your orders with the ability to initiate full or partial refunds right from the Believers Dashboard.',
            ],
            'message'=>'Congratulations, the Believers Dashboard has been successfully updated!',
            'message_footer'=>"We're always looking to make it easier to manage your restaurant . Feel free to make suggestions to your Relationship manager or email <a href='mailto:rm@believerssoftware.com'> rm@believerssoftware.com</a> anytime."
        ],
       'static_block_type'=>[
          'CMS'=>'CMS',
          'Gallery'=>'Gallery',
          'Event'=>'Event',
          'Team'=>'Team',
          'Form'=>'Form',
          'Press'=>'Press',
          'News'=>'News',
          'Custom'=>'Custom'
    
         ],
       'static_block_fields_type'=>[
          'text'=>'Text',
          'email'=>'Email',
          'url'=>'URL',
          'number'=>'Number',
          'quantity'=>'Quantity',
          'price'=>'Price',
          'textarea'=>'Textarea',
          'editor'=>'Editor',
          'hidden'=>'Hidden',
          'file'=>'File',
          'date'=>'Date',
          'select'=>'Select box',
          'multi-select'=>'Multi Select box',
          'radio'=>'Radio button',
          'checkbox'=>'Checkbox'
         ],
 
        'report_api_configs'=>[
            'rest_id'=>'58339',
            'deal_id'=>'286733506',
            'deal_id_separator'=>'',
            'restaurant_name'=>'Indian Project',
            'month'=>'August',
            'month_n'=>'08',
            'year'=>2018,
            'report_from_year'=>2018,
            'report_url'=>'http://52.52.251.75/monthly_reports/',
         ],
        'product_type'=>[
            'food_item'=>'Food Item',
            'product'=>'Product',
            'gift_card'=>'Gift Card'        
          ],
        'mail_image_url'=>env('APP_BASE_URL').'images',
        'image'               => [
            'path'      => [
                'static'   => public_path('images/static'),
		'static_block'   => public_path('images/static_block'),
                'user'     => public_path('images/user'),
                'pizzaexp' => public_path('images/pizzaexp'),
                'menu'     => public_path('images/menu'),
                'category' => public_path('images/category'),
		'attribute' => public_path('images/attribute'),
		'productimage' => public_path('images/productimage'),
                'category_attachment' => public_path('images/category_attachment'),
                'subcategory' => public_path('images/subcategory'),
                'temp'     => public_path('images/temp'),
                'restaurant' => public_path('images/restaurant'),
            ],
            'rel_path'  => [
                'static'   => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR,
		'static_block'   => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'static_block' . DIRECTORY_SEPARATOR,
                'user'     => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR,
                'pizzaexp' => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'pizzaexp' . DIRECTORY_SEPARATOR,
                'menu'     => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'menu' . DIRECTORY_SEPARATOR,
                'attribute' => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'attribute' . DIRECTORY_SEPARATOR,
		'productimage' => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'productimage' . DIRECTORY_SEPARATOR,
		'category' => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR,
                'category_attachment' => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'category_attachment' . DIRECTORY_SEPARATOR,
                'subcategory' => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'subcategory' . DIRECTORY_SEPARATOR,
                'temp'     => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR,
                'restaurant'     => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'restaurant' . DIRECTORY_SEPARATOR,
            ],
            'size'      => ['med' => 2, 'sml' => 3],
            'file_size' => 2048,
        ],
        'video'               => [
            'path'      => [
                'static' => public_path('videos/static'),
                'restaurant' => public_path('videos/restaurant'),
            ],
            'rel_path'  => [
                'static' => DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR,
                'restaurant'     => DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . 'restaurant' . DIRECTORY_SEPARATOR,
            ],
            'file_size' => 25600,
        ],
        'content_trim_length' => 700,
        'currency' => '$',
        'currency_code' => 'USD',
        // any change in static, requires db field enum change (re-migrate or add enum field in db)
        'static_page_type'    => ['home', 'about', 'story', 'menu', 'team', 'branch'],
        'pizza_setting'       => ['crust', 'size', 'sauce', 'cheese', 'topping_veg', 'topping_non_veg', 'quantity', 'side', 'bake_level', 'cut', 'seasoning'],
        'sides'               => ['left', 'whole', 'right'],
        'quantity'            => ['light', 'regular', 'extra'],
        'order_status' => [
            'placed'    => 'placed',
            'ordered'   => 'ordered',
            'confirmed' => 'confirmed',
            'delivered' => 'delivered',
            'arrived'   => 'arrived',
            'cancelled' => 'cancelled',
            'rejected'  => 'rejected',
            'archived'  => 'archived',
            'test'      => 'test',
            'edited'    => 'edited',
            'pending'   => 'pending',
            'sent'      => 'sent',
            'ready'     => 'ready',
        ],
        'status_code' => [
            'STATUS_SUCCESS' => 200,
            'STATUS_CREATED' => 201,
            'UNAUTHORIZED_REQUEST' => 401,
            'BAD_REQUEST' => 400,
            'INTERNAL_ERROR' => 500,
        ],
	'payment_gateway'     => [
            'stripe' => [
                #'key'    => 'pk_live_vAk3fvhMN0vWaoDz0Tp02YnL',
                #'secret' => 'sk_live_8Ui52w8VmHJSAgxCqndOR3n5',

                 #'key'    => 'pk_test_3ksHOaiyAisVKWE7xOa1Z99D',
                 #'secret' => 'sk_test_OxrK6FHy2yuax9TB60fspW3S',
                 'key'    => env('STRIPE_KEY'),
                 'secret' => env('STRIPE_SECRET'),
            ],
        ],
	'setup_restaurant'     => [
            'domain' =>  'munchadoshowcase.biz',
	    'qcdomain' => 'munchado.it', 
        ],
    ];
