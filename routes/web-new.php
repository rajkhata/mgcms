<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\CmsUser;

Auth::routes();

Route::get('users', 'UserController@index')->name('user');

Route::resource('restaurant', 'RestaurantController');

Route::resource('pizzaexp', 'PizzaExpController');

Route::resource('static', 'StaticController');

Route::get('/', 'HomeController@index')->name('home');

Route::resource('users', 'UserController');

Route::resource('restaurant', 'RestaurantController');

Route::resource('pizzaexp', 'PizzaExpController');

Route::resource('pizzaexpitem', 'PizzaExpItemController');

Route::resource('static', 'StaticController');

Route::resource('menu', 'MenuController');

Route::resource('menu_category', 'MenuCategoryController');

Route::resource('menu_subcategory', 'MenuSubcategoryController');

Route::resource('menu_meal_type', 'MenuMealTypeController');

Route::resource('menu_item', 'MenuItemController');

Route::resource('menu_setting_category', 'MenuSettingCategoryController');

Route::resource('menu_setting_item', 'MenuSettingItemController');


Route::resource('languages', 'LanguagesController');

Route::resource('localization', 'LocalizationController');


Route::post('/get_category_and_type', 'MenuItemController@getCategoryAndType');

Route::post('/get_customization_options', 'MenuItemController@getCustomizationOptions');

Route::post('/get_category', 'MenuCategoryController@getCategory');

Route::post('/get_menu_setting_category', 'MenuSettingCategoryController@getSettingCategory');

Route::post('/set_language', 'HomeController@setLanguage');

Route::resource('sms','SmsContentController'); //https://laravel.com/docs/5.6/controllers
Route::POST('sms/create','SmsContentController@addSmsContent');
Route::get('sms/{id}/update','SmsContentController@editSms');

Route::resource('mailtemplate', 'MailTemplateController');
Route::POST('mailtemplate/create', 'MailTemplateController@addTemplate');
Route::get('mailtemplate/{id}/update', 'MailTemplateController@editTemplate');


// AJAX calls
Route::post('/get_restaurant_settings', 'PizzaExpController@getRestaurantSettings');

$router->get('update/sides/quantity', function() {
    exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan cache:clear');
    $menuItemObj = \App\Models\MenuItem::orderBy('id','DESC')->get();
    $menuItems = $menuItemObj->toArray();

    foreach($menuItems as $menuItem) {
        if (!empty($menuItem['customizable_data'])) {
            $custData = json_decode($menuItem['customizable_data'], true);
            $tmpCustData = $custData;
            foreach ($custData as $k => $v) {
                if (!empty($v) && is_array($v)) {
                    foreach ($v['items'] as $k1 => $v1) {
                        if (!empty($v1) && is_array($v1) && isset($v1['sides'])) {
                            $tmpSidesArr = array();
                            foreach ($v1['sides'] as $k2 => $v2) {
                                if(!empty($v2['is_selected']))
                                {
                                    $tmpSidesArr[] = $v2;
                                }
                            }
                            if(count($tmpSidesArr)>0)
                                $custData[$k]['items'][$k1]['sides'] = $tmpSidesArr;
                            else
                                $custData[$k]['items'][$k1]['sides']=NULL;

                            $tmpQuanArr = array();
                            foreach ($v1['quantity'] as $k2 => $v2) {
                                if(!empty($v2['is_selected']))
                                {
                                    $tmpQuanArr[] = $v2;
                                }
                            }
                            if(count($tmpQuanArr)>0)
                                $custData[$k]['items'][$k1]['quantity'] = $tmpQuanArr;
                            else
                                $custData[$k]['items'][$k1]['quantity']=NULL;
                            //echo "mmmm".$custData[$k]['items'][$k1]['sides'][0]['is_selected'];
                        }
                    }
                }
            }
            \App\Models\MenuItem::where('id', $menuItem['id'])
                ->update(['customizable_data' => json_encode($custData)]);

           /* $menuItem['customizable_data'] = json_encode($custData);
            echo "<prE>";
            //print_r($tmpCustData);
            print_r(json_decode($menuItem['customizable_data']));
            exit;*/
        }
    }
    echo "Data updated";
    exit;
});

Route::get('reserve-table', '\Modules\Reservation\Http\Controllers\ReservationController@userReservation');
Route::post('check-availability', '\Modules\Reservation\Http\Controllers\ReservationController@checkAvailabilityAndGetAvailableSlots');
Route::post('table-reservation', '\Modules\Reservation\Http\Controllers\ReservationController@reserveTable');

