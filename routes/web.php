<?php
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
use App\CmsUser;
use App\Http\Middleware\CmsUserActionLogs;
Auth::routes();
Route::resource('user_order', 'UserOrderController');
$router->get('user_order/{id}/details/{action}', ['uses' => 'UserOrderController@orderDetail']);
$router->get('user_order/{id}/refund/{action}', ['uses' => 'UserOrderController@refundOrder'])->name('refund-order');
$router->post('release_order_refund', ['uses' => 'UserOrderController@releaseOrderRefund']);
$router->get('archive_order', ['uses' => 'UserOrderController@archiveOrder']);
Route::any('mng_order/{id}/{action}', ['uses' => 'UserOrderController@allOrder'])->name('order-list');

$router->post('user_order/replymail', ['uses' => 'UserOrderController@orderReplyMail']);

$router->get('user_order/{id}/mngdetails/{action}', ['uses' => 'UserOrderController@mngorderDetail'])->name('order-details');
$router->post('user_order/{id}/replymail', ['uses' => 'UserOrderController@orderReplyMail']);
$router->get('user_order/{id}/customer_order/{action}', ['uses' => 'UserOrderController@allUserOrders']);
$router->get('scheduled_order', ['uses' => 'UserOrderController@scheduled_order']);
Route::put('/order/{id}/{action}', 'UserOrderController@updateOrderStatus');
Route::post('/order/cancel', 'UserOrderController@cancelOrder');
$router->get('product_orders', ['uses' => 'UserOrderController@productOrders']);
$router->get('product_archive_orders', ['uses' => 'UserOrderController@productArchiveOrder']);
$router->get('user_order/{id}/{action}/logs', ['uses' => 'UserOrderController@orderLogs']);
$router->post('testorder', 'UserOrderController@testOrderUpdate');
//Route::put('/order/{id}/{slot_date}/{slot_time}', 'UserOrderController@updateTimeSlot');
Route::post('/order/updatetimeslot', 'UserOrderController@updateTimeSlot');
Route::post('/order/updateprint', 'UserOrderController@updateprint');
Route::post('/order/move_to_archive', 'UserOrderController@move_to_archive');
//Route::post('/order/updatetimeslot', 'UserOrderController@updateTimeSlot');
Route::get('/order/orderupdatearchive', 'UserOrderController@orderUpdateArchive');
#16-10-2018 RG Export orders
$router->get('/order/export', ['uses' => 'UserOrderController@export']);
/*
 * Gift cards routes added by RK on  27/10/2018
 */
Route::get('/order-gift', 'UserOrderController@getGiftCardListing');
Route::get('/order-gift/{id}', 'UserOrderController@getGiftCardDetail');
Route::get('/order-gift-archive', 'UserOrderController@giftCardArchiveOrder');
Route::get('/order-gift-archive/{id}', 'UserOrderController@getGiftCardArchiveDetail');
Route::get('/order-gift-export', 'UserOrderController@giftExport');
Route::resource('manage_orders', 'ManageOrderController');
$router->get('manage_orders/{id}/details', ['uses' => 'ManageOrderController@orderDetail']);
$router->get('manage_archive_order', ['uses' => 'ManageOrderController@archiveOrder']);
Route::put('/morders/{id}/{action}', 'ManageOrderController@updateOrderStatus');
Route::post('/morders/cancel', 'ManageOrderController@cancelOrder');
Route::get('/dashboard/movetoarchive', 'MoveOrderToArchiveController@index');
Route::get('/dashboard/movetoactive', 'MoveOrderToActiveController@index');
Route::resource('static', 'StaticController');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/dashboard/call', 'CallSentController@sentCall');
Route::resource('users', 'UserController');
Route::get('/restaurant/{restaurant_id}/import', 'ImportController@index');
Route::post('/restaurant/{restaurant_id}/import', 'ImportController@save');
Route::resource('restaurant-old', 'RestaurantController');
Route::resource('pizzaexp', 'PizzaExpController');
Route::resource('pizzaexpitem', 'PizzaExpItemController');
Route::get('/pizzaexpitem/{id}/delete/{item}', 'PizzaExpItemController@delete');
Route::resource('static', 'StaticController');
Route::resource('block', 'BlockController');
Route::get('enquiry/{type}/{enctype?}', ['uses' => 'BlockController@getRequestList'])->name('enquiry');
Route::get('enquirydetail/{id}', 'BlockController@requestDetails');
Route::get('contactenquirydetail/{id}', 'BlockController@contactDetails')->name('contactDetails');
Route::get('careerdetails/{id}', 'BlockController@careerDetails');
Route::get('eventdetail/{id}', 'BlockController@eventDetails');
Route::post('enquiryreplymail', 'BlockController@enquirySendMail');
$router->get('block/{id}/info', ['uses' => 'BlockController@requestDetails']);
Route::get('staticblock', ['uses' => 'StaticController@createBlock']);
Route::post('static/create', 'StaticController@saveBlock');
Route::get('block_list', ['uses' => 'StaticController@getblock']);
Route::get('static_editblock/{id}', ['uses' => 'StaticController@edit_block']);
Route::post('static_editblock', 'StaticController@saveEditBlock');
Route::get('static_deleteblock/{id}', ['uses' => 'StaticController@deleteBlock']);
Route::resource('menu', 'MenuController');
Route::resource('menu_category', 'MenuCategoryController');
Route::get('/get_sub_cats_group', 'MenuSubcategoryController@getSubcatsGroup');
Route::resource('menu_subcategory', 'MenuSubcategoryController');
Route::resource('menu_meal_type', 'MenuMealTypeController');
Route::get('menu_products', 'MenuItemController@product');
Route::get('menu_giftcards', 'MenuItemController@giftcard');
Route::get('menu_giftcards/{id}', 'MenuItemController@giftCardDetail');
Route::resource('menu_item', 'MenuItemController');
Route::resource('menu_setting_category', 'MenuSettingCategoryController');
Route::resource('menu_setting_item', 'MenuSettingItemController');
Route::resource('languages', 'LanguagesController');
Route::resource('localization', 'LocalizationController');
Route::post('/get_category_and_type', 'MenuItemController@getCategoryAndType');
Route::post('/get_availibility', 'MenuItemController@getAvailibility');
Route::post('/get_customization_options', 'MenuItemController@getCustomizationOptions');
Route::post('/get_category', 'MenuCategoryController@getCategory');
Route::post('/get_menu_setting_category', 'MenuSettingCategoryController@getSettingCategory');
Route::post('/set_language', 'HomeController@setLanguage');
Route::resource('sms', 'SmsContentController'); //https://laravel.com/docs/5.6/controllers
Route::POST('sms/create', 'SmsContentController@addSmsContent');
Route::get('sms/{id}/update', 'SmsContentController@editSms');
Route::resource('mailtemplate', 'MailTemplateController');
Route::POST('mailtemplate/create', 'MailTemplateController@addTemplate');
Route::get('mailtemplate/{id}/update', 'MailTemplateController@editTemplate');
// AJAX calls
Route::post('/get_restaurant_settings', 'PizzaExpController@getRestaurantSettings');
Route::post('/get_restaurant_locations', 'MenuCategoryController@get_restaurant_locations');
Route::post('/get_cities_by_state', 'CmsUserController@get_cities_by_state');
Route::post('/get_restaurant_by_city', 'CmsUserController@get_restaurant_by_city');
Route::post('/get_static_blocks', 'BlockController@getStaticBlocks');
//reports
Route::get('report/download', 'ReportsController@download');
Route::get('report/locations',      'ReportsController@locations');
Route::get('report/orders',         'ReportsController@orders');
Route::get('report/reservations',   'ReportsController@reservations');
Route::get('report/visitors',       'ReportsController@visitors')->name('visitorsreport');
Route::get('report/marketing',      'ReportsController@marketing')->name('marketingreport');
Route::get('report/social',         'ReportsController@social')->name('socialreport');
Route::get('reports', 'ReportsController@index')->name('reports');
Route::post('/get_states', 'RestaurantController@getStates')->name('getstate');
$router->get('update/sides/quantity', function() {
    exec('php c:\wamp\apache2\htdocs\multipage_cms\artisan cache:clear');
    $menuItemObj = \App\Models\MenuItem::orderBy('id', 'DESC')->get();
    $menuItems = $menuItemObj->toArray();
    foreach ($menuItems as $menuItem) {
        if (!empty($menuItem['customizable_data'])) {
            $custData = json_decode($menuItem['customizable_data'], true);
            $tmpCustData = $custData;
            foreach ($custData as $k => $v) {
                if (!empty($v) && is_array($v)) {
                    foreach ($v['items'] as $k1 => $v1) {
                        if (!empty($v1) && is_array($v1) && isset($v1['sides'])) {
                            $tmpSidesArr = array();
                            foreach ($v1['sides'] as $k2 => $v2) {
                                if (!empty($v2['is_selected'])) {
                                    $tmpSidesArr[] = $v2;
                                }
                            }
                            if (count($tmpSidesArr) > 0)
                                $custData[$k]['items'][$k1]['sides'] = $tmpSidesArr;
                            else
                                $custData[$k]['items'][$k1]['sides'] = NULL;
                            $tmpQuanArr = array();
                            foreach ($v1['quantity'] as $k2 => $v2) {
                                if (!empty($v2['is_selected'])) {
                                    $tmpQuanArr[] = $v2;
                                }
                            }
                            if (count($tmpQuanArr) > 0)
                                $custData[$k]['items'][$k1]['quantity'] = $tmpQuanArr;
                            else
                                $custData[$k]['items'][$k1]['quantity'] = NULL;
                            //echo "mmmm".$custData[$k]['items'][$k1]['sides'][0]['is_selected'];
                        }
                    }
                }
            }
            \App\Models\MenuItem::where('id', $menuItem['id'])
                ->update(['customizable_data' => json_encode($custData)]);
            
        }
    }
    echo "Data updated";
    exit;
});
// Route for Clickatell customer response
Route::get('online-waitlist-status', '\Modules\Reservation\Http\Controllers\ReservationController@onlineWaitlistStatus');
 
# Route By Rahul Gupta 10-07-2018
Route::resource('countries', 'CountryController');
Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'configure'], function() {
    Route::resource('carryout-hours', 'CarryoutHoursController');
    Route::get('carryout-hours-schedule', 'CarryoutHoursController@getWorkingHoursSchedule');
    Route::get('custom-carryout-hours-schedule', 'CarryoutHoursController@getCustomWorkingHoursSchedule');
    Route::post('carryout-hours-dayoff', 'CarryoutHoursController@dayOffToggle');
    Route::get('carryout-slot-detail/{id}', 'CarryoutHoursController@getSlotDetail');
    Route::resource('delivery-hours', 'DeliveryHoursController');
    Route::get('delivery-hours-schedule', 'DeliveryHoursController@getWorkingHoursSchedule');
    Route::get('custom-delivery-hours-schedule', 'DeliveryHoursController@getCustomWorkingHoursSchedule');
    Route::post('delivery-hours-dayoff', 'DeliveryHoursController@dayOffToggle');
    Route::get('delivery-slot-detail/{id}', 'DeliveryHoursController@getSlotDetail');
    Route::resource('operation-hours', 'OperationHoursController');
    Route::get('operation-hours-schedule', 'OperationHoursController@getOperationHoursSchedule');
    Route::get('custom-operation-hours-schedule', 'OperationHoursController@getCustomOperationHoursSchedule');
    Route::post('operation-hours-dayoff', 'OperationHoursController@dayOffToggle');
    Route::get('operation-slot-detail/{id}', 'OperationHoursController@getSlotDetail');
    #RG 29-01-2019 Manage Services
    Route::get('/manage_services', 'RestaurantController@manageServices');
    Route::post('/restaurantsettings', 'RestaurantController@restaurantsettings');
});
// Code changes done by Jawed for menu visibility hours.
Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'configure'], function()
{
    Route::resource('visibility-duration', 'VisibilityDurationController');
    Route::post('visibility-duration-schedule', 'VisibilityDurationController@getVisibilityHoursSchedule');
    Route::get('custom-visibility-duration-schedule', 'VisibilityDurationController@getCustomVisibilityHoursSchedule');
    Route::post('visibility-hours-dayoff', 'VisibilityDurationController@dayOffToggle');
    Route::get('visibility-slot-detail/{id}', 'VisibilityDurationController@getSlotDetail');
    //Route::get('visibility-duration/all', 'VisibilityDurationController@getVisibilityDuration');
    Route::post('visibility-duration/fetch/slot/detail', 'VisibilityDurationController@fetchSlotDetail');
});
//Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'modifier'], function() {
//
//    Route::get('modifier_categories', 'ModifierCategoriesController@index');
//
//});
Route::get('modifier_categories', 'ModifierCategoriesController@index');
Route::get('modifier_categories/create', 'ModifierCategoriesController@create');
Route::post('modifier_categories/store', 'ModifierCategoriesController@store');
Route::get('modifier_categories/{id}/edit', 'ModifierCategoriesController@edit');
Route::put('modifier_categories/update/{id}', 'ModifierCategoriesController@update');
Route::delete('modifier_categories', 'ModifierCategoriesController@destroy');
Route::get('item_addon_groups', 'ItemAddonGroupsController@index');
Route::get('item_addon_groups/create', 'ItemAddonGroupsController@create');
Route::post('item_addon_groups/store', 'ItemAddonGroupsController@store');
Route::get('item_addon_groups/{id}/edit', 'ItemAddonGroupsController@edit');
Route::put('item_addon_groups/update/{id}', 'ItemAddonGroupsController@update');
Route::delete('item_addon_groups', 'ItemAddonGroupsController@destroy');
Route::get('/setup-restaurant', 'OnboardingController@create')->name('setup-restaurant');
//Route::resource('registardomain', 'OnboardingController',['only' => ['store']]);
Route::post('/registardomain', 'OnboardingController@store')->name('registardomain');
Route::post('/createdomain', 'OnboardingController@store')->name('createdomain');
Route::get('/template-setup', 'HomeController@templatesetup')->name('template-setup');
Route::post('/template-setup', 'HomeController@templatesetup')->name('template-setup');
Route::get('/profile-setup', 'RestaurantProfileSetupController@index')->name('profile-setup');
Route::get('/general-info-setup', 'RestaurantProfileSetupController@generalinfo')->name('general-info-setup');
Route::post('/setrestauranttheme', 'HomeController@setrestauranttheme')->name('setrestauranttheme');
Route::post('/updateprofile', 'RestaurantProfileSetupController@updateprofile')->name('updateprofile');
Route::post('/updatesocialprofile', 'RestaurantProfileSetupController@updatesocialprofile')->name('updatesocialprofile');
Route::post('/updatedeal', 'RestaurantProfileSetupController@updatedeal')->name('updatedeal');
Route::post('/updatediscount', 'RestaurantProfileSetupController@updatediscount')->name('updatediscount');
Route::post('/addPromotion', 'RestaurantProfileSetupController@addPromotion')->name('addPromotion');
Route::post('/updatePromotion/{id}', 'RestaurantProfileSetupController@updatePromotion')->name('updatePromotion');
Route::post('/addCoupon', 'RestaurantProfileSetupController@addCoupon')->name('addCoupon');
Route::post('/updateCoupon/{id}', 'RestaurantProfileSetupController@updateCoupon')->name('updateCoupon');
Route::delete('delete/coupon/{id}', ['uses' => 'RestaurantProfileSetupController@deleteCoupon']);
Route::delete('delete/promotion/{id}', ['uses' => 'RestaurantProfileSetupController@deletePromotion']);
Route::get('/media-setup', 'RestaurantProfileSetupController@mediasettup')->name('media-setup');
Route::post('/updaterestaurantmedia', 'RestaurantProfileSetupController@updaterestaurantmedia')->name('updaterestaurantmedia');
Route::post('/removerestimage', 'RestaurantProfileSetupController@removerestimage')->name('removerestimage');
Route::get('/social-account-setup', 'RestaurantProfileSetupController@socialinfo')->name('social-account-setup');
Route::get('/deal-setup', 'RestaurantProfileSetupController@dealinfo')->name('deal-setup');
Route::get('/online-ordering-setup', 'RestaurantProfileSetupController@onlineorderinginfo')->name('online-ordering-setup');
Route::get('/online-ordering-location-setup/{id}', 'RestaurantProfileSetupController@onlineorderinglocationinfo')->name('online-ordering-location-setup');
Route::post('/online-ordering-restaurantsettings', 'RestaurantProfileSetupController@restaurantsettings')->name('online-ordering-restaurantsettings');
Route::post('/online-ordering-update', 'RestaurantProfileSetupController@restaurantsettingsupdate')->name('online-ordering-update');
Route::post('/import_parse', 'RestaurantProfileSetupController@parseImport')->name('import_parse');
Route::post('/import_process', 'RestaurantProfileSetupController@processImport')->name('import_process');
Route::post('/contactus', 'RestaurantProfileSetupController@contactus')->name('contactus');
Route::get('/contact', 'RestaurantProfileSetupController@contact')->name('contact');
Route::get('/download_delivery_service_rates/{id}', 'RestaurantProfileSetupController@downloadServiceRates')->name('download_delivery_service_rates');
Route::get('/ordernotification', 'UserOrderController@orderNotification')->name('ordernotification');
Route::resource('paymentgateway', 'PaymentController');
//@RG 19-02-2019 Pause / Resume History
Route::get('/services/history', 'RestaurantController@pauseResumeServiceHistory');
Route::resource('menu_attribute', 'MenuAttributeController');
$router->post('save/attribute/value/{id}', ['uses' => 'MenuAttributeController@saveAttributeValue']);
$router->get('custom-gen-passw',  function(){
    echo $rawpass=str_random(8);
    echo '<br><hr>';
    echo $hashed_random_password = Hash::make($rawpass);
});
Route::get('/change-password', 'ChangePasswordController@index')->name('change-password');
Route::post('/update-password', 'ChangePasswordController@updatePassword')->name('update-password');
Route::post('compare/category', 'CompareController@getDataV2')->name('categorycompare');
Route::get('compare/menu/{id}', 'CompareController@getMenuOfCategory')->name('menucompare');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('permissions','PermissionsController');
    Route::resource('cms_user', 'CmsUserController');
    Route::resource('user_action_log','CmsActivityLogsController');
    Route::resource('compare', 'CompareController');
});

Route::get('/finance/report', 'FinanceController@index')->name('referal-report');
Route::get('/miles/report', 'MilesController@index')->name('miles-report');
Route::get('/servicedisabled/report', 'FinanceController@servicedisabled')->name('servicedisabled-report');
Route::get('/orderrejection/report', 'FinanceController@orderrejection')->name('orderrejection-report');
