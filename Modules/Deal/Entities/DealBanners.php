<?php

namespace Modules\Deal\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealBanners extends Model
{
    use SoftDeletes;

    protected $fillable = [];
    protected $guarded = [];

}
