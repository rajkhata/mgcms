<?php

namespace Modules\Deal\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealSpends extends Model
{
    use SoftDeletes;
    protected  $table='deal_spends';
    protected $fillable = [];
    protected $guarded = [];

}
