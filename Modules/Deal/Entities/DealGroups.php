<?php

namespace Modules\Deal\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealGroups extends Model
{
    use SoftDeletes;
    protected  $table='deal_groups';
    protected $fillable = [];
    protected $guarded = [];

    public function dealItems()
    {
        return $this->hasMany('Modules\Deal\Entities\DealGroupItems','deal_group_id','id');
    }

}
