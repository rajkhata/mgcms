<?php

namespace Modules\Deal\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealAvailability extends Model
{
    use SoftDeletes;
    protected  $table='deal_availability';
    protected $fillable = [];
    protected $guarded = [];

}
