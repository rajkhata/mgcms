<?php

namespace Modules\Deal\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealGroupItems extends Model
{
    use SoftDeletes;
    protected  $table='deal_group_items';
    protected $fillable = [];
    protected $guarded = [];

}
