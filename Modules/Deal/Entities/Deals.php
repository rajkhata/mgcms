<?php

namespace Modules\Deal\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deals extends Model
{
    use SoftDeletes;

    protected $fillable = [];
    protected $guarded = [];

    public function availablity()
    {
        return $this->hasOne('Modules\Deal\Entities\DealAvailability','deal_id','id');
    }
    public function banner()
    {
        return $this->hasOne('Modules\Deal\Entities\DealBanners','deal_id','id');
    }
    public function dealFlatDiscounts()
    {
        return $this->hasOne('Modules\Deal\Entities\DealFlatDiscounts','deal_id','id');
    }
    public function dealSpecificProductAndCats()
    {
        return $this->hasOne('Modules\Deal\Entities\DealSpecificProductAndCats','deal_id','id');
    }

    public function dealSpends()
    {
        return $this->hasOne('Modules\Deal\Entities\DealSpends','deal_id','id');
    }

    public function dealGroups()
    {
        return $this->hasMany('Modules\Deal\Entities\DealGroups','deal_id','id');
    }



}
