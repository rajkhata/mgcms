<?php

namespace Modules\Deal\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealSpecificProductAndCats extends Model
{
    use SoftDeletes;
    protected  $table='deal_specific_product_and_cats';
    protected $fillable = [];
    protected $guarded = [];

}
