<?php

namespace Modules\Deal\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealFlatDiscounts extends Model
{
    use SoftDeletes;
    protected  $table='deal_flat_discounts';
    protected $fillable = [];
    protected $guarded = [];

}
