<?php

return [
    'name' => 'Deal',
    'deal_type'=>[
        1=> 'Buy Item Get Item Free',//
        2=> 'Buy 2 Item Get Item Free',//
        3=> 'Buy Item Get % Off',//
        4=> 'Buy Item Get $ Amount Off',//
        5=> 'Spend $ Amount Get % Off',//
        6=> 'Spend $ Amount Get $ Amount Off',//
        7=> 'Spend $ Amount Get Item Free',//
        8=> 'Get % Off',//
        9=> 'Get $ Amount Off',//
        10=> 'First Order With % Off',//
        11=> 'First Order with $ Amount Off',//
        12=> 'Spend $ Amount Get Free Delivery',//
        13=> 'Buy Item on Day Get % Off',//
        14=> 'Buy Item on Day Get $ Amount Off',//
        15=> 'Buy Item between Time Get % Off',//
        16=> 'Buy Item between Time Get $ Amount Off',//
        17=> 'Service Type Get % Off',  //
        18=> 'Service Type Get $ Amount Off' //

    ],
    'available_for'=>[
        'Both'=>'Both',
        'Takeout'=>'Takeout',
        'Delivery'=>'Delivery'
    ],
    'type'=>[
        1=>'Automatic (This deal will be applied automatically when the order qualifies)',
        2=>'Coupon Code',
    ],
    'combinable'=>[
        1=> 'Combinable(This deal can be used in conjuction with other deals)',
        2=> 'Non Combinable(This deal prevent any other deal to apply)',
        3=> 'Make the deal combinable with non combinable deals',
    ],
    'images_size'=>[
        'image'=>['height'=>500,'width'=>500],
        'small'=>['height'=>200,'width'=>200],
        'thumbnail'=>['height'=>50,'width'=>50]
    ],
    //folders for deal image =>image,small,thumb

];
