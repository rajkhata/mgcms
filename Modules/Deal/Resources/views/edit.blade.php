@extends('layouts.app')
@section('content')
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />

    <div class="main__container container__custom new-design-labels">
        <div class="row">
<?php 
$curr_deal_id=$data->id;
$curr_deal_type=$data->deal_type;
$curr_user_groups=explode(',',$data->user_group_id);

$deal_banner=$banner=$data->banner;
$dealFlatDiscounts=$banner=$data->dealFlatDiscounts;
$dealSpecificProductAndCats=$banner=$data->dealSpecificProductAndCats;
$dealSpends=$banner=$data->dealSpends;
$dealGroups=$banner=$data->dealGroups;

$availablity=$data->availablity;
$availablity_days=isset($availablity->days)?explode(',',$availablity->days):[];

$web_image=isset($deal_banner)?json_decode($deal_banner->web_image, true):[];
$mobile_image=isset($deal_banner)?json_decode($deal_banner->mobile_image, true):[];
$app_image=isset($deal_banner)?json_decode($deal_banner->app_image, true):[];

?>
    <style>
        .select2-hidden-accessible { width:500px!important}
        .select2-container { width:100%!important;}
        .select2-container {
            z-index: 99999;
        }
    </style>
            <div class="reservation__atto flex-direction-row">
                <div class="reservation__title margin-top-5">
                    <h1>Deal Update</h1>
                </div>
            </div>

        @include('sitebuilder::common.message')
        <!-- deal form -->
            <div>
                <div class="card margin-top-0">
                    <div class="card-body">
                        <div class="">
                            <!-- tabnav -->
                            <ul class="nav nav-tabs">
                                <li class="{{$step==1?'active':''}}"><a data-toggle="tab" href="#generalinfo">General Info</a></li>
                                <li class="{{$step==2?'active':''}}"><a data-toggle="tab" href="#dealdetails">Deal Details</a></li>
                            </ul>
                            <!-- tabnav -->
                            <!-- tabcontent -->
                            <div class="tab-content">
                                <!-- generalinfo -->
                                <div id="generalinfo" class="tab-pane fade {{$step==1?'in active':''}}">
                                    <form name="dealinfo"  method="POST" action="/deal/{{$curr_deal_id}}"  enctype='multipart/form-data'>
                                        @csrf
                                        @method('PUT')

                                        <input type="hidden" name="deal_step" value="1">

                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                <div class="pull-left font-weight-700 wid">Enabled <span class="mandat">*</span></div>
                                                <div class="pull-left tbl-radio-btn margin-left-30">
                                                    <input type="radio" id="include_in_navigation_yes" name="status" checked value="1" {{ ($data->status==1 || old('status')=='1') ? 'checked' :'' }}   >
                                                    <label for="include_in_navigation_yes">Yes</label>
                                                </div>
                                                <div class="pull-left tbl-radio-btn margin-left-30">
                                                    <input type="radio" id="include_in_navigation_no" name="status" value="0" {{ ($data->status==0 ||  old('status')=='0') ? 'checked' :'' }}>
                                                    <label for="include_in_navigation_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                <label for="" class="col-form-label text-md-right">Deal Type <span class="mandat">*</span></label>
                                                <div class="selct-picker-plain position-relative">
                                                    <select name="deal_type" id="deal_type"
                                                            class="form-control selectpicker"
                                                            required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" readonly="">
                                                        @foreach($deal_type as $key=>$value)
                                                            <option value="{{$key}}"  @if($data->deal_type==$key) selected @endif>{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                <label for="" class="col-form-label text-md-right">User Group <span class="mandat">*</span></label>
                                                <div class="selct-picker-plain position-relative">
                                                    <select multiple name="user_group_id[]" id="user_group_id" class="form-control selectpicker"
                                                            required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                        <?php foreach ($user_groups as $group){ ?>
                                                        <option value="<?php echo $group->id;?>" @if(in_array($group->id,$curr_user_groups)) selected @endif  ><?php echo $group->user_group;?> </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                <label for="deal_title" class="col-form-label text-md-right padbot">Deal Title <span class="mandat">*</span></label>
                                                <div class="input__field">
                                                    <input id="deal_title" type="text" class="" name="deal_title" value="{{$data->deal_title}}" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                <label for="pos_code" class="col-form-label text-md-right padbot">POS Code</label>
                                                <div class="input__field">
                                                    <input id="pos_code" type="text" class="" name="pos_code" value="{{$data->pos_code}}" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                <label for="pos_code" class="col-form-label text-md-right padbot">Discount Details</label>
                                                <div class="input__field">
                                                    <textarea class="textarea__input newtextarea" row="5" col="5" name="discount_details">{{$data->discount_details}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                <label for="" class="col-form-label text-md-right">Takeout/Delivery</label>
                                                <div class="selct-picker-plain position-relative">
                                                    <select name="available_for" id="available_for"
                                                            class="form-control selectpicker"
                                                            required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                        @foreach($available_for as $key=>$value)
                                                            <option value="{{$key}}" @if($key==$data->available_for) selected @endif>{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                <label for="" class="col-form-label text-md-right">Type</label>
                                                <div class="selct-picker-plain position-relative">
                                                    <select name="type" id="type"
                                                            class="form-control selectpicker"
                                                            required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                        @foreach($type as $key=>$value)
                                                            <option value="{{$key}}" @if($key==$data->type) selected @endif>{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                <label for="" class="col-form-label text-md-right">Combinable</label>
                                                <div class="selct-picker-plain position-relative">
                                                    <select name="combinable" id="combinable"
                                                            class="form-control selectpicker"
                                                            required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                        @foreach($combinable as $key=>$value)
                                                            <option value="{{$key}}" @if($key==$data->combinable) selected @endif>{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <!-- <div class="option__wrapper">
                                                    <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="pid" name="permission[]" type="checkbox" value="51"> Make this deal combinable with Not Combinable deals
                                                        <span class="control_indicator"></span>
                                                    </label>
                                                </div> -->
                                            </div>
                                        </div>

                                        <div class="row addtionalRow {{$data->type==1?'hide':' '}} coupon_code_block">
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                                                <label for="deal_title" class="col-form-label text-md-right padbot">Coupon <span class="mandat">*</span></label>
                                                <div class="input__field">
                                                    <input id="coupon" type="text" class="" name="coupon" value="{{$data->coupon}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                                                <label for="deal_title" class="col-form-label text-md-right padbot">Number of Usage Per Customer <span class="mandat">*</span></label>
                                                <div class="input__field">
                                                    <input id="usage_per_customer" type="text" class="" name="usage_per_customer" value="{{$data->usage_per_customer}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                                                <label for="deal_title" class="col-form-label text-md-right padbot">Number of Usage Per Coupon <span class="mandat">*</span></label>
                                                <div class="input__field">
                                                    <input id="usage_per_coupon" type="text" class="" name="usage_per_coupon" value="{{$data->usage_per_coupon}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                <label for="deal_title" class="col-form-label text-md-right padbot">Delivery charge</label>
                                                <div class="input__field">
                                                    <input id="delivery_charge" type="text" class="" name="delivery_charge" value="{{$data->delivery_charge}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                <label for="pos_code" class="col-form-label text-md-right padbot">Min Order Amount</label>
                                                <div class="input__field">
                                                    <input id="min_order_amount" type="text" class="" name="min_order_amount" value="{{$data->min_order_amount}}">
                                                </div>
                                            </div>
                                        </div>

                                        <!-- box -->
                                        <div class="row addtionalRow2">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-20">
                                                <h4>Avaiability</h4>
                                                <div class="row">
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                        <label for="pos_code" class="col-form-label text-md-right padbot">Start Time</label>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                        <div class="input__field"><input type="text" class=" color-black fullWidth time_start icotime" value="{{$availablity->start_time??''}}"   name="start_time" id="time_start" placeholder="Start Time" /></div>
                                                    </div>
                                                    <div class="col-sm-0 col-md-0 col-lg-2 col-xs-12 tabhide">&nbsp;</div>

                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                        <label for="pos_code" class="col-form-label text-md-right padbot">End Time</label>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                        <div class="input__field"><input type="text" class=" color-black fullWidth time_end icotime" value="{{$availablity->end_time??''}}"   name="end_time" id="time_end" placeholder="End Time" /></div>
                                                    </div>
                                                </div>
                                                <div class="row margin-top-20">
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                        <label for="pos_code" class="col-form-label text-md-right padbot">Start Date</label>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                        <div class="input__field"><input type="text" class=" color-black fullWidth date_start icocal" value="{{$availablity->start_date??''}}"   name="start_date" id="start_date" placeholder="Start Date" /></div>
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 tabhide">&nbsp;</div>

                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                        <label for="pos_code" class="col-form-label text-md-right padbot">End Date</label>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                        <div class="input__field"><input type="text" class=" color-black fullWidth date_end icocal" value="{{$availablity->end_date??''}}"   name="end_date" id="end_date" placeholder="End Date" /></div>
                                                    </div>
                                                </div>
                                                <div class="row margin-top-20">
                                                    <h4>Reccurence</h4>
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                        <label for="pos_code" class="col-form-label text-md-right padbot">Days</label>
                                                    </div>
                                                    <div class="col-sm-10 col-md-10 col-lg-10 col-xs-12 formob">
                                                        <div class="option__wrapper">
                                                            <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Sunday" {{in_array('Sunday',$availablity_days)?'checked':''}}> Sunday <span class="control_indicator"></span>
                                                            </label>
                                                        </div>
                                                        <div class="option__wrapper">
                                                            <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Monday" {{in_array('Monday',$availablity_days)?'checked':''}}> Monday <span class="control_indicator"></span>
                                                            </label>
                                                        </div>
                                                        <div class="option__wrapper">
                                                            <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Tuesday" {{in_array('Tuesday',$availablity_days)?'checked':''}}> Tuesday <span class="control_indicator"></span>
                                                            </label>
                                                        </div>
                                                        <div class="option__wrapper">
                                                            <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Wednesday" {{in_array('Wednesday',$availablity_days)?'checked':''}}> Wednesday <span class="control_indicator"></span>
                                                            </label>
                                                        </div>
                                                        <div class="option__wrapper">
                                                            <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Thursday" {{in_array('Thursday',$availablity_days)?'checked':''}}> Thursday <span class="control_indicator"></span>
                                                            </label>
                                                        </div>
                                                        <div class="option__wrapper">
                                                            <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Friday" {{in_array('Friday',$availablity_days)?'checked':''}}> Friday <span class="control_indicator"></span>
                                                            </label>
                                                        </div>
                                                        <div class="option__wrapper">
                                                            <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Saturday" {{in_array('Saturday',$availablity_days)?'checked':''}}> Saturday <span class="control_indicator"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- box -->

                                        <!-- banner -->
                                        <div class="row addtionalRow2">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-20">
                                                <h4  data-toggle="modal" data-target="#showroles" style="cursor:pointer">Banner</h4>
                                                <div class="row">
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                        <div class="selct-picker-plain position-relative">
                                                            <select name="banner_status" id="restaurant_id4"
                                                                    class="form-control selectpicker"
                                                                     data-style="no-background-with-buttonline no-padding-left no-padding-top">
                                                                <option value="">Status</option>
                                                                <option value="1" {{(isset($banner->status) && $banner->status==1)?'selected':''}}>Active</option>
                                                                <option value="0" {{(isset($banner->status) && $banner->status==0)?'selected':''}}>Inactive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 mob-mar-20">
                                                        <div class="selct-picker-plain position-relative">
                                                            <select name="banner_position" id="restaurant_id4"
                                                                    class="form-control selectpicker"
                                                                     data-style="no-background-with-buttonline no-padding-left no-padding-top">
                                                                <option value="">Position</option>
                                                                <option value="home" {{(isset($banner->position) && $banner->position=='home')?'selected':''}}>Home</option>
                                                                <option value="menu" {{(isset($banner->position) && $banner->position=='menu')?'selected':''}}>Menu</option>
                                                                <option value="cart" {{(isset($banner->position) && $banner->position=='cart')?'selected':''}}>Cart</option>
                                                                <option value="checkout" {{(isset($banner->position) && $banner->position=='checkout')?'selected':''}}>Checkout</option>
                                                                <option value="mini_cart" {{(isset($banner->position) && $banner->position=='mini_cart')?'selected':''}}> Mini Cart</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 mob-mar-20">
                                                        <div class="input__field">
                                                            <input id="pos_code" type="text" class="" name="banner_alt_text" value="{{isset($banner->alt_text)?$banner->alt_text:''}}" placeholder="Alt text">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-4 col-lg-4 col-xs-12 mob-mar-20">
                                                        <div class="input__field">
                                                            <input id="pos_code" type="text" class="" name="banner_link" value="{{isset($banner->link)?$banner->link:''}}" placeholder="Link">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-2 col-lg-2 col-xs-12 mob-mar-20">
                                                        <div class="input__field">
                                                            <input id="pos_code" type="text" class="" name="banner_sort_order" value="{{isset($banner->sort_order)?$banner->sort_order:''}}" placeholder="Sort Order">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12 col-md-6 col-lg-4 col-xs-12 margin-top-20">
                                                        <div class="image_cont_main row imgcaption">
                                                            <div class="f1">
                                                                <input id="image" type="file" class="form-control imagecroper1" value="" name="banner_web_image" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                                @if(isset($web_image['image']))
                                                                    <div class="">
                                                                        <a data-fancybox="gallery"
                                                                           href="{{url('/').'/'.$web_image['image']}}">
                                                                            <span class="glyphicon glyphicon-picture"></span>
                                                                        </a>&emsp;
                                                                    </div>
                                                                @endif
                                                                <input  type="hidden" name="banner_web_image_axis" >

                                                                <div class="input__field">
                                                                    <input id="image_caption" type="text" class="" name="banner_web_image_caption" value="{{isset($web_image['caption'])?$web_image['caption']:''}}" placeholder="Web Image Caption">

                                                                </div>
                                                            </div>
                                                            {{-- <div class="f2">
                                                                 <!-- <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage"> -->
                                                                 <a href="javascript:;" class="glyphicon glyphicon-trash deleteImage"></a>
                                                             </div>
                                                             <input type="hidden" name="image_hidden" value="">
                                                             <div class="f3">
                                                                 <a data-fancybox="gallery" href="">
                                                                 <img class="img-responsive img-thumbnail"src=""></a>&emsp;
                                                             </div> --}}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-6 col-lg-4 col-xs-12 margin-top-20">
                                                        <div class="image_cont_main row imgcaption">
                                                            <div class="f1">
                                                                <input id="image" type="file" class="form-control imagecroper1" value="" name="banner_mobile_image" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                                @if(isset($mobile_image['image']))
                                                                    <div class="">
                                                                        <a data-fancybox="gallery"
                                                                           href="{{url('/').'/'.$mobile_image['image']}}">

                                                                            <span class="glyphicon glyphicon-picture"></span>

                                                                        </a>&emsp;
                                                                    </div>
                                                                @endif
                                                                <input  type="hidden" name="banner_mobile_image_axis" >
                                                                <div class="input__field">
                                                                    <input id="image_caption" type="text" class="" name="banner_mobile_image_caption" value="{{isset($mobile_image['caption'])?$mobile_image['caption']:''}}" placeholder="Mobile Image Caption">

                                                                </div>
                                                            </div>
                                                            {{-- <div class="f2">
                                                                 <!-- <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage"> -->
                                                                 <a href="javascript:;" class="glyphicon glyphicon-trash deleteImage"></a>
                                                             </div>
                                                             <input type="hidden" name="image_hidden" value="">
                                                             <div class="f3">
                                                                 <a data-fancybox="gallery" href="">
                                                                 <img class="img-responsive img-thumbnail"src=""></a>&emsp;
                                                             </div> --}}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-6 col-lg-4 col-xs-12 margin-top-20">
                                                        <div class="image_cont_main row imgcaption">
                                                            <div class="f1">
                                                                <input id="image" type="file" class="form-control imagecroper1" value="" name="banner_app_image" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                                @if(isset($app_image['image']))
                                                                    <div class="">
                                                                        <a data-fancybox="gallery"
                                                                           href="{{url('/').'/'.$app_image['image']}}">

                                                                            <span class="glyphicon glyphicon-picture"></span>

                                                                        </a>&emsp;
                                                                    </div>
                                                                @endif
                                                                <input  type="hidden" name="banner_app_image_axis" >
                                                                <div class="input__field">
                                                                    <input id="image_caption" type="text" class="" name="banner_app_image_caption" value="{{isset($app_image['caption'])?$app_image['caption']:''}}" placeholder="App Image Caption">
                                                                </div>
                                                            </div>
                                                            {{-- <div class="f2">
                                                                 <!-- <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage"> -->
                                                                 <a href="javascript:;" class="glyphicon glyphicon-trash deleteImage"></a>
                                                             </div>
                                                             <input type="hidden" name="image_hidden" value="">
                                                             <div class="f3">
                                                                 <a data-fancybox="gallery" href="">
                                                                 <img class="img-responsive img-thumbnail"src=""></a>&emsp;
                                                             </div> --}}
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- banner -->
                                        <!-- submit CTA -->
                                        <div class="row margin-top-20">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-align-center">
                                                <button type="submit" class="btn btn__primary">Save & Continue</button>
                                            </div>
                                        </div>
                                        <!-- submit CTA -->
                                    </form>
                                </div>
                                <!-- generalinfo -->
                                <!-- dealdetails -->
                                <div id="dealdetails" class="tab-pane fade {{$step==2?'in active':''}}">


                                        @if(in_array($curr_deal_type,[8,9,10,11,13,14,15,16,17,18]))
                                              @include('deal::deal_types.type1')
                                        @elseif(in_array($curr_deal_type,[1,2]))
                                           @include('deal::deal_types.type2')
                                        @elseif(in_array($curr_deal_type,[3,4]))
                                            @include('deal::deal_types.type3')
                                        @elseif(in_array($curr_deal_type,[5,6]))
                                            @include('deal::deal_types.type4')
                                        @elseif(in_array($curr_deal_type,[7]))
                                             @include('deal::deal_types.type5')
                                        @elseif(in_array($curr_deal_type,[12]))
                                              @include('deal::deal_types.type6')
                                        @else

                                        @endif


                                </div>
                                <!-- dealdetails -->
                            </div>
                            <!-- tabcontent -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- deal form -->

        </div>
    </div>


    <script type="text/javascript" src="{{asset('cropimage/jquery.imgareaselect.min.js')}}"></script>
    <link href="{{asset('cropimage/imgareaselect-default.css')}}" rel='stylesheet' />

    <script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>


    <link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' type='text/css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $(".selectproductsku").select2({
                ajax: {
                    url: '<?php echo URL::to('/')."/sku/product"; ?>',
                    dataType: 'json',
                    type: "post",
                   // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                           // page: params.page
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: $.map(response, function (item) {
                                return {
                                    text: item.text,
                                    id: item.sku
                                }
                            })
                        };
                    },

                  //  cache: true
                },
                placeholder: 'Search SKU',
               // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
            });

        /*    $(".chosen").select2({
                //dropdownParent: "#dealdetails",
                ajax: {
                    url: '<?php echo URL::to('/')."/sku/product"; ?>',
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });*/
        });
        $(document).ready(function () {



            $('.time_start').datetimepicker({
                datepicker:false,
                format:'H:i'
            });
            $('.time_end').datetimepicker({
                datepicker:false,
                format:'H:i'
            });
            $('.date_start').datetimepicker({
                timepicker:false,
                format:'d-m-Y',
                formatDate:'Y/m/d',
                minDate:'-1970/01/02', // yesterday is minimum date
                maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });
            $('.date_end').datetimepicker({
                timepicker:false,
                format:'d-m-Y',
                formatDate:'Y/m/d',
                minDate:'-1970/01/02', // yesterday is minimum date
                maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });
        });


    </script>
    <script>


        $('body #type').change(function(){

            if($(this).val()==2){
                $('body .coupon_code_block').removeClass('hide');

            }else{
                $('body .coupon_code_block').addClass('hide');

            }
        })
    </script>
@endsection