@extends('layouts.app')
@section('content')
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />

    <div class="main__container container__custom new-design-labels">
        <div class="row">


                <div class="reservation__atto flex-direction-row">
                    <div class="reservation__title margin-top-5">
                        <h1>Deal Create</h1>
                    </div>
                    {{--<div class="guestbook__sec2">
                        <div class="margin-left-10">
                            <a href="javascript:void(0)" class="btn btn__primary font-weight-600 width-120 box-shadow">Save</a>
                            <a href="#" class="btn btn__white width-120 margin-left-10 box-shadow">Discard</a>
                        </div>
                    </div>--}}
                </div>

                @include('sitebuilder::common.message')
                <!-- deal form -->
                <div>
                    <div class="card margin-top-0">
                        <div class="card-body">
                            <div class="">
                                <!-- tabnav -->
                                <ul class="nav nav-tabs">
                                    <li class="{{!isset($section_tab)?'active':''}}"><a data-toggle="tab" href="#generalinfo">General Info</a></li>
                                    <li><a data-toggle="tab" href="#dealdetails">Deal Details</a></li>
                                </ul>
                                <!-- tabnav -->
                                <!-- tabcontent -->
                                <div class="tab-content">
                                    <!-- generalinfo -->
                                    <div id="generalinfo" class="tab-pane fade  in active">
                                        <form name="dealinfo"  method="POST" action="{{ route('deal.store') }}"  enctype='multipart/form-data'>
                                            @csrf

                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                    <div class="pull-left font-weight-700 wid">Enabled <span class="mandat">*</span></div>
                                                    <div class="pull-left tbl-radio-btn margin-left-30">
                                                        <input type="radio" id="include_in_navigation_yes" name="status" checked value="1" {{ (old('status')=='1') ? 'checked' :'' }}   >
                                                        <label for="include_in_navigation_yes">Yes</label>
                                                    </div>
                                                    <div class="pull-left tbl-radio-btn margin-left-30">
                                                        <input type="radio" id="include_in_navigation_no" name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }}>
                                                        <label for="include_in_navigation_no">No</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                    <label for="" class="col-form-label text-md-right">Deal Type <span class="mandat">*</span></label>
                                                    <div class="selct-picker-plain position-relative">
                                                        <select name="deal_type" id="deal_type"
                                                                class="form-control selectpicker"
                                                                required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                            @foreach($deal_type as $key=>$value)
                                                                     <option value="{{$key}}">{{$value}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                    <label for="" class="col-form-label text-md-right">User Group <span class="mandat">*</span></label>
                                                    <div class="selct-picker-plain position-relative">
                                                        <select multiple name="user_group_id[]" id="user_group_id" class="form-control selectpicker"
                                                                required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                            <?php foreach ($user_groups as $group){ ?>
                                                            <option value="<?php echo $group->id;?>" ><?php echo $group->user_group;?> </option>
                                                            <?php } ?>
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                    <label for="deal_title" class="col-form-label text-md-right padbot">Deal Title <span class="mandat">*</span></label>
                                                    <div class="input__field">
                                                        <input id="deal_title" type="text" class="" name="deal_title" value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                    <label for="pos_code" class="col-form-label text-md-right padbot">POS Code</label>
                                                    <div class="input__field">
                                                        <input id="pos_code" type="text" class="" name="pos_code" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                    <label for="pos_code" class="col-form-label text-md-right padbot">Discount Details</label>
                                                    <div class="input__field">
                                                        <textarea class="textarea__input newtextarea" row="5" col="5" name="discount_details"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                    <label for="" class="col-form-label text-md-right">Takeout/Delivery</label>
                                                    <div class="selct-picker-plain position-relative">
                                                        <select name="available_for" id="available_for"
                                                                class="form-control selectpicker"
                                                                required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                            @foreach($available_for as $key=>$value)
                                                                <option value="{{$key}}">{{$value}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                    <label for="" class="col-form-label text-md-right">Type</label>
                                                    <div class="selct-picker-plain position-relative">
                                                        <select name="type" id="type"
                                                                class="form-control selectpicker"
                                                                required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                            @foreach($type as $key=>$value)
                                                                <option value="{{$key}}">{{$value}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                    <label for="" class="col-form-label text-md-right">Combinable</label>
                                                    <div class="selct-picker-plain position-relative">
                                                        <select name="combinable" id="combinable"
                                                                class="form-control selectpicker"
                                                                required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                            @foreach($combinable as $key=>$value)
                                                                <option value="{{$key}}">{{$value}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <!-- <div class="option__wrapper">
                                                        <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="pid" name="permission[]" type="checkbox" value="51"> Make this deal combinable with Not Combinable deals
                                                            <span class="control_indicator"></span>
                                                        </label>
                                                    </div> -->
                                                </div>
                                            </div>

                                            <div class="row addtionalRow hide coupon_code_block">
                                                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                                                    <label for="deal_title" class="col-form-label text-md-right padbot">Coupon <span class="mandat">*</span></label>
                                                    <div class="input__field">
                                                        <input id="coupon" type="text" class="" name="coupon" value="">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                                                    <label for="deal_title" class="col-form-label text-md-right padbot">Number of Usage Per Customer <span class="mandat">*</span></label>
                                                    <div class="input__field">
                                                        <input id="usage_per_customer" type="text" class="" name="usage_per_customer" value="">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                                                    <label for="deal_title" class="col-form-label text-md-right padbot">Number of Usage Per Coupon <span class="mandat">*</span></label>
                                                    <div class="input__field">
                                                        <input id="usage_per_coupon" type="text" class="" name="usage_per_coupon" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20">
                                                    <label for="deal_title" class="col-form-label text-md-right padbot">Delivery charge</label>
                                                    <div class="input__field">
                                                        <input id="delivery_charge" type="text" class="" name="delivery_charge" value="">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                                    <label for="pos_code" class="col-form-label text-md-right padbot">Min Order Amount</label>
                                                    <div class="input__field">
                                                        <input id="min_order_amount" type="text" class="" name="min_order_amount" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- box -->
                                            <div class="row addtionalRow2">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-20">
                                                    <h4>Avaiability</h4>
                                                    <div class="row">
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">Start Time</label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                            <div class="input__field"><input type="text" class=" color-black fullWidth time_start icotime"   name="start_time" id="time_start" placeholder="Start Time" /></div>
                                                        </div>
                                                        <div class="col-sm-0 col-md-0 col-lg-2 col-xs-12 tabhide">&nbsp;</div>

                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">End Time</label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                            <div class="input__field"><input type="text" class=" color-black fullWidth time_end icotime"   name="end_time" id="time_end" placeholder="End Time" /></div>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-top-20">
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">Start Date</label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                            <div class="input__field"><input type="text" class=" color-black fullWidth date_start icocal"   name="start_date" id="start_date" placeholder="Start Date" /></div>
                                                        </div>
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 tabhide">&nbsp;</div>

                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">End Date</label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                            <div class="input__field"><input type="text" class=" color-black fullWidth date_end icocal"   name="end_date" id="start_date" placeholder="End Date" /></div>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-top-20">
                                                        <h4>Reccurence</h4>
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">Days</label>
                                                        </div>
                                                        <div class="col-sm-10 col-md-10 col-lg-10 col-xs-12 formob">
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Sunday"> Sunday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Monday"> Monday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Tuesday"> Tuesday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Wednesday"> Wednesday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Thursday"> Thursday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Friday"> Friday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Saturday"> Saturday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- box -->

                                            <!-- banner -->
                                            <div class="row addtionalRow2">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-20">
                                                    <h4  data-toggle="modal" data-target="#showroles" style="cursor:pointer">Banner</h4>
                                                    <div class="row">
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <div class="selct-picker-plain position-relative">
                                                                <select name="banner_status" id="restaurant_id4"
                                                                        class="form-control selectpicker"
                                                                         data-style="no-background-with-buttonline no-padding-left no-padding-top">
                                                                    <option value="">Status</option>
                                                                    <option value="1">Active</option>
                                                                    <option value="0">Inactive</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 mob-mar-20">
                                                            <div class="selct-picker-plain position-relative">
                                                                <select name="banner_position" id="restaurant_id4"
                                                                        class="form-control selectpicker"
                                                                         data-style="no-background-with-buttonline no-padding-left no-padding-top">
                                                                    <option value="">Position</option>
                                                                    <option value="home">Home</option>
                                                                    <option value="menu">Menu</option>
                                                                    <option value="cart">Cart</option>
                                                                    <option value="checkout">Checkout</option>
                                                                    <option value="mini_cart">Mini Cart</option>
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 mob-mar-20">
                                                            <div class="input__field">
                                                                <input id="pos_code" type="text" class="" name="banner_alt_text" placeholder="Alt text">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-4 col-lg-4 col-xs-12 mob-mar-20">
                                                            <div class="input__field">
                                                                <input id="pos_code" type="text" class="" name="banner_link" placeholder="Link">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-2 col-lg-2 col-xs-12 mob-mar-20">
                                                            <div class="input__field">
                                                                <input id="pos_code" type="text" class="" name="banner_sort_order" placeholder="Sort Order">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-6 col-lg-4 col-xs-12 margin-top-20">
                                                            <div class="image_cont_main row imgcaption">
                                                                <div class="f1">
                                                                    <input id="image" type="file" class="form-control imagecroper1" value="" name="banner_web_image" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                                    <input  type="hidden" name="banner_web_image_axis" >

                                                                    <div class="input__field">
                                                                        <input id="image_caption" type="text" class="" name="banner_web_image_caption" value="" placeholder="Web Image Caption">
                                                                    </div>
                                                                </div> 
                                                               {{-- <div class="f2">
                                                                    <!-- <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage"> -->
                                                                    <a href="javascript:;" class="glyphicon glyphicon-trash deleteImage"></a>
                                                                </div> 
                                                                <input type="hidden" name="image_hidden" value="">
                                                                <div class="f3">
                                                                    <a data-fancybox="gallery" href="">
                                                                    <img class="img-responsive img-thumbnail"src=""></a>&emsp;
                                                                </div> --}}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6 col-lg-4 col-xs-12 margin-top-20">
                                                            <div class="image_cont_main row imgcaption">
                                                                <div class="f1">
                                                                    <input id="image" type="file" class="form-control imagecroper1" value="" name="banner_mobile_image" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                                    <input  type="hidden" name="banner_mobile_image_axis" >
                                                                    <div class="input__field">
                                                                        <input id="image_caption" type="text" class="" name="banner_mobile_image_caption" value="" placeholder="Mobile Image Caption">
                                                                    </div>
                                                                </div> 
                                                               {{-- <div class="f2">
                                                                    <!-- <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage"> -->
                                                                    <a href="javascript:;" class="glyphicon glyphicon-trash deleteImage"></a>
                                                                </div> 
                                                                <input type="hidden" name="image_hidden" value="">
                                                                <div class="f3">
                                                                    <a data-fancybox="gallery" href="">
                                                                    <img class="img-responsive img-thumbnail"src=""></a>&emsp;
                                                                </div> --}}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6 col-lg-4 col-xs-12 margin-top-20">
                                                            <div class="image_cont_main row imgcaption">
                                                                <div class="f1">
                                                                    <input id="image" type="file" class="form-control imagecroper1" value="" name="banner_app_image" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                                    <input  type="hidden" name="banner_app_image_axis" >
                                                                    <div class="input__field">
                                                                        <input id="image_caption" type="text" class="" name="banner_app_image_caption" value="" placeholder="App Image Caption">
                                                                    </div>
                                                                </div> 
                                                               {{-- <div class="f2">
                                                                    <!-- <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage"> -->
                                                                    <a href="javascript:;" class="glyphicon glyphicon-trash deleteImage"></a>
                                                                </div> 
                                                                <input type="hidden" name="image_hidden" value="">
                                                                <div class="f3">
                                                                    <a data-fancybox="gallery" href="">
                                                                    <img class="img-responsive img-thumbnail"src=""></a>&emsp;
                                                                </div> --}}
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- banner -->
                                            <!-- submit CTA -->
                                            <div class="row margin-top-20">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-align-center">
                                                    <button type="submit" class="btn btn__primary">Save & Continue</button>
                                                </div>
                                            </div>
                                            <!-- submit CTA -->
                                        </form>
                                    </div>
                                    <!-- generalinfo -->
                                    <!-- dealdetails -->
                                    <!-- dealdetails -->
                                </div>
                                <!-- tabcontent -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- deal form -->

        </div>
    </div>



    <script type="text/javascript" src="{{asset('cropimage/jquery.imgareaselect.min.js')}}"></script>
    <link href="{{asset('cropimage/imgareaselect-default.css')}}" rel='stylesheet' />
    <link href="{{asset('css/reservation/chosen.css')}}" rel='stylesheet' />

    <script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/chosen.js')}}"></script>
    <script type="text/javascript">
            $(document).ready(function () {
                $('.time_start').datetimepicker({
                    datepicker:false,
                    format:'H:i'
                });
                $('.time_end').datetimepicker({
                    datepicker:false,
                    format:'H:i'
                });
                $('.date_start').datetimepicker({
                    timepicker:false,
                    format:'d-m-Y',
                    formatDate:'Y/m/d',
                    minDate:'-1970/01/02', // yesterday is minimum date
                    maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });
                $('.date_end').datetimepicker({
                    timepicker:false,
                    format:'d-m-Y',
                    formatDate:'Y/m/d',
                    minDate:'-1970/01/02', // yesterday is minimum date
                    maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });

                $(".chosen").chosen();
            });

        // setTimeout(function () {
        //     // checkHasVal("#restaurantform .input__field input, .selct-picker-plain select");
        //     $('.dateTime_start_date').datetimepicker(commonOptions);
        //     $('.dateTime_end_date').datetimepicker(commonOptions);
        //     $('.time_start').datetimepicker(commonOptions);
        //     $('.time_end').datetimepicker(commonOptions);
        // }, 400);

        // var startDate;
        // var today = new Date();
        // var date = ("0" + today.getDate()).slice(-2)+'-'+("0" + (today.getMonth()+1)).slice(-2)+'-'+today.getFullYear();
        // var time = today.getHours() + ":" + today.getMinutes()
        // var currentDateTime = date+' '+time;

        // var commonOptions = {
        //     i18n:{
        //         en: {
        //             dayOfWeekShort: [
        //                 "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"
        //             ]
        //         }
        //     },
        //     format: 'M-D-Y H:i',
        //     formatTime: 'H:i'
        // }

        // $(function(){
        //     jQuery('#date_timepicker_start').datetimepicker({
        //         onShow:function( ct ){
        //             this.setOptions({
        //                 maxDate:(jQuery('#date_timepicker_end').val()).split(" ",2)[0]?(jQuery('#date_timepicker_end').val()).split(" ",2)[0]:false
        //             })
        //         }
        //     });
        //     jQuery('#date_timepicker_end').datetimepicker({
        //         //startDate: $('#date_timepicker_end').val()?(jQuery('#date_timepicker_start').val()).split(" ",2)[0]:false,
        //         onShow:function( ct ){
        //             this.setOptions({
        //                 minDate:(jQuery('#date_timepicker_start').val()).split(" ",2)[0]?(jQuery('#date_timepicker_start').val()).split(" ",2)[0]:false
        //             })
        //         }
        //     });
        // });
    </script>
    <script>
/*

        var deal_type=2;
        if(deal_type==1){
            $('.parent_div_section').hide();
            $('.deal_div').show();

        }else if(deal_type==2){
            $('.parent_div_section').hide();
            $('.product_div').show();
        }else if(deal_type==3){

        }else if(deal_type==4){

        }else if(deal_type==5){

        }
*/

$('body #type').change(function(){

    if($(this).val()==2){
        $('body .coupon_code_block').removeClass('hide');

    }else{
        $('body .coupon_code_block').addClass('hide');

    }
})
</script>
@endsection