@extends('layouts.app')

@section('content')

    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title col-md-8 no-col-padding">
                <h1>Deals
                </h1>
            </div>

        </div>

        @can('Menu Add')
            <div class="addition__functionality">
                {!! Form::open(['method'=>'get']) !!}

                <div class="row functionality__wrapper" style="margin-top:0">

                    <div class="row form-group col-md-4 no-col-padding">
                        <span><a href="{{ URL::to('deal/create') }}" class="btn btn__primary">Add</a></span>
                    </div>

                </div>
                {!! Form::close() !!}
            </div>
        @endcan

        <div>
            <div class="active__order__container">
                @include('sitebuilder::common.message')


                <div class="guestbook__container box-shadow no-margin-top">

                    <table id="table_id" class="responsive ui cell-border hover " style="width:100%; margin-bottom:10px;">
                        <thead>
                        <tr>
                            <th>Deal Type</th>
                            <th>Deal Title</th>
                            <th>Takeout/Delivery</th>
                            <th>Status</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($data) && count($data)>0)
                            @foreach($data as $deal)
                                <tr>
                                    <td>
                                        {{@$deal_type[$deal->deal_type]}}

                                    </td>
                                    <td>{{ $deal->deal_title }}</td>
                                    <td>{{$deal->available_for}}</td>

                                    <td data-order="{{$deal->status}}">

                                            <div class="waitList__otp no-padding" style="float: left; margin-right: 20px;">

                                                <div class="toggle__container" style="margin-left: 0;">
                                                    @php
                                                        if($deal->status==1){
                                                            $ariaPressed = true;
                                                            $activeClass = 'active';
                                                        } else {
                                                            $ariaPressed = false;
                                                            $activeClass = '';
                                                        }
                                                    @endphp
                                                    <button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom menu_item_active {{ $activeClass }}"
                                                            title="Open" data-toggle="button" data-menu="{{ $deal->id }}" aria-pressed="{{ $ariaPressed }}" autocomplete="off">
                                                        <div class="handle"></div>
                                                    </button>
                                                </div>
                                            </div>

                                        @if(isset($_GET['admin']))
                                                <a style="margin-top: 10px;" href="{{ URL::to('deal/' . $deal->id . '/edit') }}" class="glyphicon glyphicon-edit" ></a>
                                                <a onclick="return confirm('Are you sure, you want to delete it?')" href="{{ URL::to('deal/' . $deal->id . '/destroy') }}" class="glyphicon glyphicon-trash"></a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                                No Record Found
                                </td></tr>
                        @endif
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
        <script>

            $(document).ready( function () {
                $('#table_id').DataTable(  {
                    responsive: true,
                    dom:'<"top"fiB><"bottom"trlp><"clear">',
                    buttons: [
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-eye"></i>',
                            titleAttr: 'Column Visibility'
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'Excel'
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF'
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            titleAttr: 'Print',
                            exportOptions: {
                                columns: ':visible',
                            },
                        }
                    ],
                    columnDefs: [ {
                        targets: -1,
                        visible: false
                    }],
                    language: {
                        search: "",
                        searchPlaceholder: "Search"
                    },
                    responsive: true
                });
                //$('#table_id').show();
                //$('#table_id').removeClass('hidden');
            } );
        </script>

@endsection
