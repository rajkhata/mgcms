<form method="POST" action="/deal/{{$curr_deal_id}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <input type="hidden" name="deal_step" value="2">

    <div class="deal_div parent_div_section">
        <div class="row margin-top-30">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                <div class="option__wrapper">
                    <input class="hide permission" id="" name="deal_group[]" type="checkbox" value="deal" checked>
                    <label class="custom_checkbox relative padding-left-25 font-weight-600">Deal
                    </label>
                </div>
            </div>

        </div>


        <div class="row fststpRow">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                    <div class="pull-left tbl-radio-btn">
{{--
                        <input type="radio" id="include_in_navigation_yes" name="include_in_navigation"  value="1" {{ (old('include_in_navigation')=='1') ? 'checked' :'' }}   >
--}}
                        <label for="include_in_navigation_yes">On deal Products</label>
                    </div>
                </div>
            </div>
            <div class="row scndstpRow">
                <div class="col-sm-3 col-md-2 col-lg-2 col-xs-12">
                    <label for="" class="col-form-label text-md-right">Group Price</label>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12 deal-form-group">
                    <div class="selct-picker-plain position-relative">
                        <select name="group_price_type" id="3" class="form-control selectpicker"
                                required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                            <option value="value" {{$data->group_price_type=='value'?'selected':''}}>Value</option>
                            <option value="percentage" {{$data->group_price_type=='percentage'?'selected':''}}>Percentage</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 col-xs-12">
                    <div class="input__field">
                        <input id="group" type="text" class="" name="group_price" value="{{$data->group_price}}" placeholder="Group Price(Value/Percentage)">
                    </div>
                </div>
            </div>
            <div class="row margin-top-20">
                <div class="col-sm-10 col-md-10 col-lg-10 col-xs-12">

                    @if($data->dealGroups && count($data->dealGroups))

                        @foreach($data->dealGroups   as $dealgroupid=> $group)

                            <div class="row addtionalRow3 deal_group group1">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                        <div class="input__field">
                                            <input id="1" type="hidden" class="" name="group[{{$dealgroupid}}][id]" value="{{$group->id}}">

                                            <input id="1" type="text" class="" required name="group[{{$dealgroupid}}][name]" value="{{$group->group_name}}" required placeholder="Group Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-top-30">
                                    <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12 deal-form-group2">
                                        <label for="" class="col-form-label text-md-right">Quantity</label>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-3 col-xs-12 deal-form-group2">
                                        <div class="selct-picker-plain position-relative">
                                            <select name="group[{{$dealgroupid}}][quantity_type]" required id="3" class="form-control selectpicker"
                                                    required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top">
                                                <option value="value" {{$group->quantity_type=='value'?'selected':''}}>Value</option>
                                                <option value="range" {{$group->quantity_type=='range'?'selected':''}}>Range</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-3 col-xs-12">
                                        <div class="input__field">
                                            <input id="group2" required type="text" class="" value="{{$group->quantity}}" name="group[{{$dealgroupid}}][quantity_value]" placeholder="Value/Range">
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-top-20">
                                    <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12">
                                        <label for="" class="col-form-label text-md-right">Is Required</label>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                        <div class="pull-left tbl-radio-btn">
                                            <input type="radio" id="include_in_navigation_yes2" name="group[{{$dealgroupid}}][is_required]"  value="1" {{$group->is_required=='1'?'checked':''}} >
                                            <label for="include_in_navigation_yes">Yes</label>
                                        </div>
                                        <div class="pull-left tbl-radio-btn margin-left-30">
                                            <input type="radio" id="include_in_navigation_yes2" name="group[{{$dealgroupid}}][is_required]"  value="0"  {{$group->is_required=='0'?'checked':''}}  >
                                            <label for="include_in_navigation_yes">No</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-top-30 text-align-center">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                        <h4>Add Deal Item</h4>
                                    </div>
                                </div>
                                <div class="row margin-top-20">
                                    <div class="col-sm-9 col-md-9 col-lg-10 col-xs-12">
                                        @if($group->dealItems && count($group->dealItems))


                                            @foreach($group->dealItems as $dealgroupitem=>$groupitem)

                                                <div class="row addtionalRow3 group_item">
                                                    <div class="row margin-top-10">
                                                        <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">


                                                            <div class="selct-picker-plain position-relative select2main">
                                                                <input  type="hidden" class="" name="group[{{$dealgroupid}}][product][{{$dealgroupitem}}][id]" value="{{$groupitem->id}}">

                                                                <select  name="group[{{$dealgroupid}}][product][{{$dealgroupitem}}][sku]"  class="selectproductsku skusearch" multiple="multiple">
                                                                  @php $skus= !empty($groupitem->sku)?explode(',',$groupitem->sku):[]; @endphp
                                                                    @foreach($skus as $sk)
                                                                        <option selected="" value="{{$sk}}">{{$sk}}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2 deal-form-group3">
                                                            <div class="selct-picker-plain position-relative">
                                                                <select required name="group[{{$dealgroupid}}][product][{{$dealgroupitem}}][price_type]" id="3" class="form-control selectpicker"
                                                                        required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top">
                                                                    <option value="">-- Price --</option>
                                                                    <option value="unchanged" {{$groupitem->price_type=='unchanged'?'selected':''}}>Unchanged</option>
                                                                    <option value="free" {{$groupitem->price_type=='free'?'selected':''}}>Free</option>
                                                                    <option value="fixed_price" {{$groupitem->price_type=='fixed_price'?'selected':''}}>Fixed Price</option>
                                                                    <option value="value_discount" {{$groupitem->price_type=='value_discount'?'selected':''}}>Value Discount</option>
                                                                    <option value="percentage_discount" {{$groupitem->price_type=='percentage_discount'?'selected':''}}>Percentage Discount</option>
                                                                    <option value="percentage_discount_inc_toppping" {{$groupitem->price_type=='percentage_discount_inc_toppping'?'selected':''}}>Percentage Discount incl. Topping</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2">
                                                            <div class="input__field">
                                                                <input id="group2" type="text" required  value="{{$groupitem->price_type_value}}" class="" name="group[{{$dealgroupid}}][product][{{$dealgroupitem}}][price_type_value]" placeholder="Value">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">
                                                            <div class="input__field">
                                                                <input id="group2" type="text" required value="{{$groupitem->free_topping_quantity}}" class="" name="group[{{$dealgroupid}}][product][{{$dealgroupitem}}][free_topping_quantity]" placeholder="Free Topping Qty">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        @else
                                            <div class="row addtionalRow3 group_item">
                                                <div class="row margin-top-10">
                                                    <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">

                                                        <div class="selct-picker-plain position-relative select2main">
                                                            <input  type="hidden" class="" name="group[{{$dealgroupid}}][product][0][id]" value="">

                                                            <select  name="group[{{$dealgroupid}}][product][0][sku]" class="selectproductsku skusearch" multiple="multiple">

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2 deal-form-group3">
                                                        <div class="selct-picker-plain position-relative">
                                                            <select required name="group[{{$dealgroupid}}][product][0][price_type]" id="3" class="form-control selectpicker"
                                                                    required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top">
                                                                <option value="">-- Price --</option>
                                                                <option value="unchanged">Unchanged</option>
                                                                <option value="free">Free</option>
                                                                <option value="fixed_price">Fixed Price</option>
                                                                <option value="value_discount">Value Discount</option>
                                                                <option value="percentage_discount">Percentage Discount</option>
                                                                <option value="percentage_discount_inc_toppping">Percentage Discount incl. Topping</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2">
                                                        <div class="input__field">
                                                            <input id="group2" type="text" required class="" name="group[{{$dealgroupid}}][product][0][price_type_value]" placeholder="Value">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">
                                                        <div class="input__field">
                                                            <input id="group2" type="text" required class="" name="group[{{$dealgroupid}}][product][0][free_topping_quantity]" placeholder="Free Topping Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12">
                                        <button type="button" class="btn btn__primary group_item_add">ADD</button>
                                    </div>
                                </div>

                            </div>

                        @endforeach
                    @else

                        <div class="row addtionalRow3 deal_group group1">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                    <div class="input__field">
                                        <input id="1" type="hidden" class="" name="group[0][id]">

                                        <input id="1" type="text" class="" required name="group[0][name]" required placeholder="Group Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top-30">
                                <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12 deal-form-group2">
                                    <label for="" class="col-form-label text-md-right">Quantity</label>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-3 col-xs-12 deal-form-group2">
                                    <div class="selct-picker-plain position-relative">
                                        <select name="group[0][quantity_type]" required id="3" class="form-control selectpicker"
                                                required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top">
                                            <option value="value">Value</option>
                                            <option value="range">Range</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-3 col-xs-12">
                                    <div class="input__field">
                                        <input id="group2" required type="text" class="" name="group[0][quantity_value]" placeholder="Value/Range">
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top-20">
                                <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12">
                                    <label for="" class="col-form-label text-md-right">Is Required</label>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                    <div class="pull-left tbl-radio-btn">
                                        <input type="radio" id="include_in_navigation_yes2" name="group[0][is_required]"  value="1" checked>
                                        <label for="include_in_navigation_yes">Yes</label>
                                    </div>
                                    <div class="pull-left tbl-radio-btn margin-left-30">
                                        <input type="radio" id="include_in_navigation_yes2" name="group[0][is_required]"  value="0"    >
                                        <label for="include_in_navigation_yes">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top-30 text-align-center">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                    <h4>Add Deal Item</h4>
                                </div>
                            </div>
                            <div class="row margin-top-20">
                                <div class="col-sm-9 col-md-9 col-lg-10 col-xs-12">

                                    <div class="row addtionalRow3 group_item">
                                        <div class="row margin-top-10">
                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">
                                                <div class="input__field">
                                                </div>
                                                <div class="selct-picker-plain position-relative select2main">
                                                    <input  type="hidden" class="" name="group[0][product][0][id]" value="">

                                                    <select  name="group[0][product][0][sku]" class="selectproductsku skusearch" multiple="multiple">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2 deal-form-group3">
                                                <div class="selct-picker-plain position-relative">
                                                    <select required name="group[0][product][0][price_type]" id="3" class="form-control selectpicker"
                                                            required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top">
                                                        <option value="">-- Price --</option>
                                                        <option value="unchanged">Unchanged</option>
                                                        <option value="free">Free</option>
                                                        <option value="fixed_price">Fixed Price</option>
                                                        <option value="value_discount">Value Discount</option>
                                                        <option value="percentage_discount">Percentage Discount</option>
                                                        <option value="percentage_discount_inc_toppping">Percentage Discount incl. Topping</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2">
                                                <div class="input__field">
                                                    <input id="group2" type="text" required class="" name="group[0][product][0][price_type_value]" placeholder="Value">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">
                                                <div class="input__field">
                                                    <input id="group2" type="text" required class="" name="group[0][product][0][free_topping_quantity]" placeholder="Free Topping Qty">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12">
                                    <button type="button" class="btn btn__primary group_item_add">ADD</button>
                                </div>
                            </div>

                        </div>

                    @endif


                </div>
                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                    <button type="button" class="btn btn__primary add_group">ADD</button>
                </div>
            </div>
        </div>
    </div>
<!-- submit CTA -->
    <div class="row margin-top-20">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-align-center">
            <button type="submit" class="btn btn__primary">Save & Continue</button>
        </div>
    </div>
    <!-- submit CTA -->

</form>


<script>
$('body .add_group').click(function () {
    var index=(($('.deal_group').length ));

    var groupdata='<div class="row addtionalRow3 group1"> <div class="row"> <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12"> <div class="input__field"> <input id="1" type="hidden" class="" name="group['+index+'][id]"> <input id="1" type="text" class="" required name="group['+index+'][name]" required placeholder="Group Name"> </div> </div> </div> <div class="row margin-top-30"> <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12 deal-form-group2"> <label for="" class="col-form-label text-md-right">Quantity</label> </div> <div class="col-sm-4 col-md-4 col-lg-3 col-xs-12 deal-form-group2"> <div class="selct-picker-plain position-relative"> <select name="group['+index+'][quantity_type]" required id="3" class="form-control selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top"> <option value="value">Value</option> <option value="range">Range</option> </select> </div> </div> <div class="col-sm-4 col-md-4 col-lg-3 col-xs-12"> <div class="input__field"> <input id="group2" required type="text" class="" name="group['+index+'][quantity_value]" placeholder="Value/Range"> </div> </div> </div> <div class="row margin-top-20"> <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12"> <label for="" class="col-form-label text-md-right">Is Required</label> </div> <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12"> <div class="pull-left tbl-radio-btn"> <input type="radio" id="include_in_navigation_yes2" name="group['+index+'][is_required]" value="1" checked> <label for="include_in_navigation_yes">Yes</label> </div> <div class="pull-left tbl-radio-btn margin-left-30"> <input type="radio" id="include_in_navigation_yes2" name="group['+index+'][is_required]" value="0" > <label for="include_in_navigation_yes">No</label> </div> </div> </div> <div class="row margin-top-30 text-align-center"> <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12"> <h4>Add Deal Item</h4> </div> </div> <div class="row margin-top-20"> <div class="col-sm-9 col-md-9 col-lg-10 col-xs-12"> <div class="row addtionalRow3 group_item"> <div class="row margin-top-10"> <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12"> <div class="selct-picker-plain position-relative select2main"> <input type="hidden" class="" name="group['+index+'][product][0][id]" value=""> <select name="group['+index+'][product][0][sku]" class="selectproductsku skusearch" multiple="multiple"> </select> </div> </div> <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2 deal-form-group3"> <div class="selct-picker-plain position-relative"> <select required name="group['+index+'][product][0][price_type]" id="3" class="form-control selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top"> <option value="">-- Price --</option> <option value="unchanged">Unchanged</option> <option value="free">Free</option> <option value="fixed_price">Fixed Price</option> <option value="value_discount">Value Discount</option> <option value="percentage_discount">Percentage Discount</option> <option value="percentage_discount_inc_toppping">Percentage Discount incl. Topping</option> </select> </div> </div> <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2"> <div class="input__field"> <input id="group2" type="text" required class="" name="group['+index+'][product][0][price_type_value]" placeholder="Value"> </div> </div> <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12"> <div class="input__field"> <input id="group2" type="text" required class="" name="group['+index+'][product][0][free_topping_quantity]" placeholder="Free Topping Qty"> </div> </div> </div> </div> </div> <div class="col-sm-3 col-md-3 col-lg-2 col-xs-12"> <button type="button" class="btn btn__primary group_item_add">ADD</button> </div> </div> </div>';
    $('.deal_group').after(groupdata);
    $('.selectpicker').selectpicker('refresh')

})

$(document).on("click", '.group_item_add', function(event) {
    var index=(($(this).parents('.deal_group').find('.group_item').length ));

    var groupindex=$(this).parents('.deal_group').index();
    groupindex=groupindex;
    console.error(groupindex);

    var  group_item='<div class="row addtionalRow3 group_item"> <div class="row margin-top-10"> <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12"> <div class="selct-picker-plain position-relative select2main"> <input type="hidden" class="" name="group['+groupindex+'][product]['+index+'][id]" value=""> <select name="group['+groupindex+'][product]['+index+'][sku]" class="selectproductsku skusearch" multiple="multiple"> </select> </div> </div> <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2 deal-form-group3"> <div class="selct-picker-plain position-relative"> <select required name="group['+groupindex+'][product]['+index+'][price_type]" id="3" class="form-control selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top"> <option value="">-- Price --</option> <option value="unchanged">Unchanged</option> <option value="free">Free</option> <option value="fixed_price">Fixed Price</option> <option value="value_discount">Value Discount</option> <option value="percentage_discount">Percentage Discount</option> <option value="percentage_discount_inc_toppping">Percentage Discount incl. Topping</option> </select> </div> </div> <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12 deal-form-group2"> <div class="input__field"> <input id="group2" type="text" required class="" name="group['+groupindex+'][product]['+index+'][price_type_value]" placeholder="Value"> </div> </div> <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12"> <div class="input__field"> <input id="group2" type="text" required class="" name="group['+groupindex+'][product]['+index+'][free_topping_quantity]" placeholder="Free Topping Qty"> </div> </div> </div> </div>';
    $('.group_item').after(group_item);
    $('.selectpicker').selectpicker('refresh')

})
</script>

