<form method="POST" action="/deal/{{$curr_deal_id}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <input type="hidden" name="deal_step" value="2">

    <div class="product_div parent_div_section">
        <div class="row margin-top-40">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 ">
                <div class="option__wrapper">
                    <input class="hide permission" id="" name="deal_group[]" type="checkbox" value="product" checked>
                    <label class="custom_checkbox relative padding-left-25 font-weight-600">Product
                    </label>
                </div>
            </div>
        </div>
        <?php
        $specific_product=$dealSpecificProductAndCats;

        $sku=[];
        $discount_percentage='';

        if($specific_product && $specific_product->type=='product'){

            $sku=explode(',',$specific_product->sku);
            $discount_percentage=$specific_product->discount_percentage;

        }
        ?>
        <div class="row fststpRow">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                    <div class="pull-left tbl-radio-btn">
                        <input type="radio" class="hidden" id="include_in_navigation_yes" name="on_specific"  value="product" checked  >
                        <label for="include_in_navigation_yes">On Specific Products</label>
                    </div>
                </div>
            </div>
            <div class="row scndstpRow">
                <div class="col-sm-3 col-md-2 col-lg-2 col-xs-12">
                    <label for="" class="col-form-label text-md-right">Contain Product SKU</label>
                </div>
                <div class="col-sm-4 col-md-5 col-lg-5 col-xs-12">
                    <div class="selct-picker-plain position-relative select2main">
                        <select  name="products[]" class="selectproductsku skusearch" multiple="multiple">
                          @foreach($sku as $sk)
                              <option selected="" value="{{$sk}}">{{$sk}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12">
                    <label for="" class="col-form-label text-md-right"><span class="light">(Comma Separated Product SKU)</span></label>
                </div>
            </div>
            <div class="row scndstpRow">
                <div class="col-sm-3 col-md-2 col-lg-2 col-xs-12">
                    <label for="" class="col-form-label text-md-right">Discount Percentage</label>
                </div>
                <div class="col-sm-4 col-md-5 col-lg-5 col-xs-12">
                    <div class="input__field">
                        <input id="group" type="text" class="" name="discount_percentage" value="{{$discount_percentage}}" placeholder="Percentage">
                    </div>
                </div>
                <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12">
                    <label for="" class="col-form-label text-md-right"><span class="light">(%)</span></label>
                </div>
            </div>
        </div>

    </div>

<!-- submit CTA -->
    <div class="row margin-top-20">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-align-center">
            <button type="submit" class="btn btn__primary">Save & Continue</button>
        </div>
    </div>
    <!-- submit CTA -->

</form>
