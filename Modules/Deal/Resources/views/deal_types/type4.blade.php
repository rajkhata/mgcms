<form method="POST" action="/deal/{{$curr_deal_id}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <input type="hidden" name="deal_step" value="2">

    <div class="spend_div parent_div_section">


        <div class="row margin-top-30 dealhide1 box">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                <div class="option__wrapper">
                    <input class="hide permission" id="" name="deal_group[]" type="checkbox" value="spend" checked>
                    <label class="custom_checkbox relative padding-left-25 font-weight-600"> Spend
                    </label>
                </div>
            </div>
        </div>

        <div class="row fststpRow plr">
            <?php
            $order_value_type='';
            $order_value='';
            $discount_type='';
            $discount_value='';

            if($dealSpends && $dealSpends->is_discount=='1'){

                $order_value_type=$dealSpends->order_value_type;
                $order_value=$dealSpends->order_value;
                $discount_type=$dealSpends->discount_type;
                $discount_value=$dealSpends->discount_value;

            }
            ?>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12 deal-form-group2">
                    <div class="selct-picker-plain position-relative">
                        <select name="order_value_type"  required id="3" class="form-control selectpicker"
                                required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top">
                            <option value="total_order_value" {{$order_value_type=='total_order_value'?'selected':''}}>Total Order value</option>
                            <option value="total_discounted_order" {{$order_value_type=='total_discounted_order'?'selected':''}}>Total Discounted order</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                    <div class="input__field">
                        <input id="group2" type="text" class="" name="order_value" value="{{$order_value}}"  required placeholder="Value">
                    </div>
                </div>
                <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12">
                    <label for="" class="col-form-label text-md-right"><span class="light">(is less than OR equal)</span></label>
                </div>
            </div>

            <div class="row margin-top-20">
                <div class="col-sm-3 col-md-2 col-lg-2 col-xs-12">
                    <div class="option__wrapper">
                        <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="spend_type[]" type="checkbox" value="discount" checked> Discount
                        </label>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 deal-form-group3 deal-form-group2">
                    <div class="selct-picker-plain position-relative">
                        <select name="discount_type" id="3" class="form-control selectpicker"
                                required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top">
                            <option value="">-- Price --</option>
                            <option value="fixed_price" {{$discount_type=='fixed_price'?'selected':''}}>Fixed Price</option>
                            <option value="percentage_discount" {{$discount_type=='percentage_discount'?'selected':''}}>Percentage Discount</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                    <div class="input__field">
                        <input id="group2" type="text" class="" name="discount_value" value="{{$discount_value}}" placeholder="Value/Percentage">
                    </div>
                </div>
            </div>



        </div>
    </div>

<!-- submit CTA -->
    <div class="row margin-top-20">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-align-center">
            <button type="submit" class="btn btn__primary">Save & Continue</button>
        </div>
    </div>
    <!-- submit CTA -->

</form>
