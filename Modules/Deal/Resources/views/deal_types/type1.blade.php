<form method="POST" action="/deal/{{$curr_deal_id}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <input type="hidden" name="deal_step" value="2">

    <div class="flat_div parent_div_section">


        <div class="row margin-top-30">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                <div class="option__wrapper">
                    <input class="hide permission" id="" name="deal_group[]" type="checkbox" value="flat_discount" checked>
                    <label class="custom_checkbox relative padding-left-25 font-weight-600">Flat Discount
                    </label>
                </div>
            </div>
        </div>

        <div class="row fststpRow">
            <div class="row">
                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                    <label for="" class="col-form-label text-md-right">Discount</label>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-3 col-xs-12 deal-form-group2">
                    <div class="selct-picker-plain position-relative">
                        <select name="discount_type" id="3" class="form-control selectpicker"
                                required data-style="no-background-with-buttonline no-padding-left margin-top-0 no-padding-top">
                            <option value="">-- Price --</option>
                            <option value="fixed" {{(isset($dealFlatDiscounts) && $dealFlatDiscounts['discount_type']=='fixed')?'selected':''}}>Fixed</option>
                            <option value="percentage" {{(isset($dealFlatDiscounts) && $dealFlatDiscounts['discount_type']=='percentage')?'selected':''}}>Percentage Discount</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-3 col-xs-12">
                    <div class="input__field">
                        <input id="group2" type="text" class="" required name="discount_value" placeholder="Vaule/Percentage" value="{{$dealFlatDiscounts['discount_value']??''}}">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row margin-top-20">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 text-align-center">
            <button type="submit" class="btn btn__primary">Save & Continue</button>
        </div>
    </div>


</form>
