<?php

namespace Modules\Deal\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Config;
use DB;

use Modules\Deal\Entities\Deals;
use Modules\Deal\Entities\DealAvailability;
use Modules\Deal\Entities\DealBanners;
use Modules\Deal\Entities\DealFlatDiscounts;
use Modules\Deal\Entities\DealSpecificProductAndCats;
use Modules\Deal\Entities\DealSpends;
use Modules\Deal\Entities\DealGroups;
use Modules\Deal\Entities\DealGroupItems;

use Carbon\Carbon;
use App\Helpers\CommonFunctions;
use Modules\MenuManagement\Entities\v2menu\MenuItem;
use Modules\MenuManagement\Entities\v2menu\MenuCategory;


class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {        $deal_type=Config::get('deal.deal_type');

        $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $restaurant_id     = \Auth::user()->restaurant_id;

        //$data = Deals::whereIn('restaurant_id',$location)->get();
        $data = Deals::where('restaurant_id',$restaurant_id)->get();


        return view('deal::index',compact('data','deal_type'));
    }


    public  function getSku($type,Request $request){

        $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $restaurant_id     = \Auth::user()->restaurant_id;


       // echo json_encode([array("id"=>1, "text"=>1),array("id"=>2, "text"=>2)]);die;
        //echo $type;

        if($type=='category'){
            $data = MenuCategory::where('restaurant_id',$restaurant_id)->where('sku', 'like', '%' . $request->get('q') . '%')->get(['sku','sku as text']);
            if($data){
                echo json_encode($data);
            }
        }else{
            //product sku here
            $data = MenuItem::where('restaurant_id',$restaurant_id)->where('sku', 'like', '%' . $request->get('q') . '%')->get(['sku','sku as text']);
            if($data){
                echo json_encode($data);
            }
        }
        echo '';
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $deal_type=Config::get('deal.deal_type');
        $available_for=Config::get('deal.available_for');
        $type=Config::get('deal.type');
        $combinable=Config::get('deal.combinable');
        $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $user_groups = DB::table('user_groups')
            ->whereIn('restaurant_id', [0,$location[0]])
            ->where(['status'=>1])
            ->get();
        return view('deal::create',compact('deal_type','available_for','type','combinable','user_groups'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */

    public function store(Request $request)
    {

       /*echo '<pre>';
        print_r($request->all());
        die;*/
        try {
            DB::beginTransaction();

            $data              = $request->all();
            $restaurant_id     = \Auth::user()->restaurant_id;

            $data_request = [
                    'restaurant_id'    => $restaurant_id,
                    'user_group_id'    => $request->has('user_group_id')?implode(',',$data['user_group_id']):'',
                    'deal_type'            => $data['deal_type'],
                    'deal_title'             => $data['deal_title'],
                    'pos_code'	=> $data['pos_code']??'',
                    'discount_details' =>$data['discount_details'],
                    'available_for' => $data['available_for'],
                    'type'     => $data['type'],
                    'combinable'      => $data['combinable'],
                    'delivery_charge'  => $data['delivery_charge']??'',
                    'min_order_amount'  => $data['min_order_amount']??'',
                ];

                if($data['type']==2){
                    if($request->filled('coupon') && $request->has('usage_per_customer') && $request->has('usage_per_coupon')){
                        $data_request['coupon']=$data['coupon'];
                        $data_request['usage_per_customer']=$data['usage_per_customer']??0;
                        $data_request['usage_per_coupon']=$data['usage_per_coupon']??0;
                    }
                }

                $dealobj=Deals::create($data_request);

                $avaiability=[];
                if($request->filled('days')){
                    $days=implode(',',$data['days']);
                    $avaiability['days']=$days;
                }
                $avaiability['start_time']=$request->filled('start_time')?$request->input('start_time'):'';
                $avaiability['end_time']=$request->filled('end_time')?$request->input('end_time'):'';
                $avaiability['start_date']= $request->input('start_date')?date('Y-m-d', strtotime($request->input('start_date'))):'';
                $avaiability['end_date']=$request->input('end_date')? date('Y-m-d', strtotime($request->input('end_date'))):'';

                if(count($avaiability)){
                    $avaiability['deal_id']=$dealobj->id;
                    DealAvailability::create($avaiability);
                }

                if($request->has('banner_status') && $request->filled('banner_position')&& $request->has('banner_sort_order')){
                    $banners=[
                        'deal_id'=>$dealobj->id,
                        'status'=>$request->input('banner_status')??0,
                        'position'=>$request->input('banner_position')??'',
                        'sort_order'=>$request->input('banner_sort_order')??'',
                        'alt_text'=>$request->input('banner_alt_text')??'',
                        'link'=>$request->input('banner_link')??'',
                    ];

                    $restaurant=CommonFunctions::getRestaurant($restaurant_id);
                    $restaurant_name=$restaurant?$restaurant->restaurant_name:'';

                    if($request->hasFile('banner_web_image')){

                        $images=CommonFunctions::imageUploader($request->input('banner_web_image_axis'),$request->file('banner_web_image'),'restaurant',$restaurant_name,'deals','web',true,false);
                        if($images){
                            $images['caption']=$request->input('banner_web_image_caption')??'';
                            $banners['web_image']=json_encode($images);
                        }
                    }

                    if($request->hasFile('banner_mobile_image')){

                        $images=CommonFunctions::imageUploader($request->input('banner_mobile_image_axis'),$request->file('banner_mobile_image'),'restaurant',$restaurant_name,'deals','mobile',true,false);
                        if($images){
                            $images['caption']=$request->input('banner_mobile_image_caption')??'';
                            $banners['mobile_image']=json_encode($images);
                        }
                    }

                    if($request->hasFile('banner_app_image')){

                        $images=CommonFunctions::imageUploader($request->input('banner_app_image_axis'),$request->file('banner_app_image'),'restaurant',$restaurant_name,'deals','app',true,false);
                        if($images){
                            $images['caption']=$request->input('banner_app_image_caption')??'';
                            $banners['app_image']=json_encode($images);
                        }
                    }
                    if($request->hasFile('banner_web_image')||$request->hasFile('banner_mobile_image')||$request->hasFile('banner_app_image')) {
                        DealBanners::create($banners);
                    }
                }

                DB::commit();
                if ($request->ajax()) {
                    return \Response::json(['success' => true, 'message' => 'Deal has been created successfully.'], 200);

                } else {
                    return redirect('deal/'.$dealobj->id.'/edit?step=2')->with('status_msg', 'Deal has been created successfully.');

                }

       } catch (\Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);

            }else{
                return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
            }
        }
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('deal::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($deal_id)
    {
        $deal_type=Config::get('deal.deal_type');
        $available_for=Config::get('deal.available_for');
        $type=Config::get('deal.type');
        $combinable=Config::get('deal.combinable');
        $location = CommonFunctions::getRestaurantDetails(array('restaurants.id', 'r2.restaurant_name'));
        $user_groups = DB::table('user_groups')
            ->whereIn('restaurant_id', [0,$location[0]])
            ->where(['status'=>1])
            ->get();
       //return view('deal::edit' ,compact('deal_id'));

        $step=$_GET['step']??1;
        $restaurant_id     = \Auth::user()->restaurant_id;


        $data = Deals::where('restaurant_id',$restaurant_id)->where('id', '=' ,$deal_id)->first();


        return view('deal::edit',compact('data','step','deal_id','deal_type','available_for','type','combinable','user_groups'));

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {

        $step=$request->input('deal_step')??1;
        if(!isset($id) || !is_numeric($id)){
            return redirect()->back()->with('err_msg','Invalid Request');
        }

        DB::beginTransaction();
        try {

            $data              = $request->all();
            $restaurant_id     = \Auth::user()->restaurant_id;
            if($request->has('deal_step') && $request->input('deal_step')==1){
                /* echo '<pre>';
                 print_r($request->all());
                 die;*/

                $data_request = [
                    'restaurant_id'    => $restaurant_id,
                    'user_group_id'    => $request->has('user_group_id')?implode(',',$data['user_group_id']):'',
                   // 'deal_type'            => $data['deal_type'],
                    'deal_title'             => $data['deal_title'],
                    'pos_code'	=> $data['pos_code']??'',
                    'discount_details' =>$data['discount_details'],
                    'available_for' => $data['available_for'],
                    'type'     => $data['type'],
                    'combinable'      => $data['combinable'],
                    'delivery_charge'  => $data['delivery_charge']??'',
                    'min_order_amount'  => $data['min_order_amount']??'',
                    'deal_group'=>isset($data['deal_group'])?implode(',',$data['deal_group']):'',
                ];

                if($data['type']==2){
                    if($request->filled('coupon') && $request->has('usage_per_customer') && $request->has('usage_per_coupon')){
                        $data_request['coupon']=$data['coupon'];
                        $data_request['usage_per_customer']=$data['usage_per_customer']??0;
                        $data_request['usage_per_coupon']=$data['usage_per_coupon']??0;
                    }
                }
                $dealobj=Deals::where('restaurant_id',$restaurant_id)->where('id', '=' ,$id)->update($data_request);

                $avaiability=[];
                if($request->filled('days')){
                    $days=implode(',',$data['days']);
                    $avaiability['days']=$days;
                }
                $avaiability['start_time']=$request->filled('start_time')?$request->input('start_time'):'';
                $avaiability['end_time']=$request->filled('end_time')?$request->input('end_time'):'';
                $avaiability['start_date']= $request->input('start_date')?date('Y-m-d', strtotime($request->input('start_date'))):'';
                $avaiability['end_date']=$request->input('end_date')? date('Y-m-d', strtotime($request->input('end_date'))):'';

                if(count($avaiability)){
                    $avaiability['deal_id']=$id;
                    DealAvailability::updateOrCreate(
                        ['deal_id' => $id],
                        $avaiability
                    );
                }


                if($dealobj && $request->has('banner_status') && $request->filled('banner_position')&& $request->has('banner_sort_order')){
                    $banners=[
                        'deal_id'=>$id,
                        'status'=>$request->input('banner_status')??0,
                        'position'=>$request->input('banner_position')??'',
                        'sort_order'=>$request->input('banner_sort_order')??'',
                        'alt_text'=>$request->input('banner_alt_text')??'',
                        'link'=>$request->input('banner_link')??'',
                    ];
                    $deal_banner= DealBanners::where('deal_id', '=' ,$id)->first();

                    $restaurant=CommonFunctions::getRestaurant($restaurant_id);
                    $restaurant_name=$restaurant?$restaurant->restaurant_name:'';
                    $web_image=$deal_banner?json_decode($deal_banner->web_image, true):[];
                    $mobile_image=$deal_banner?json_decode($deal_banner->mobile_image, true):[];
                    $app_image=$deal_banner?json_decode($deal_banner->app_image, true):[];

                    if($request->hasFile('banner_web_image')){

                        $images=CommonFunctions::imageUploader($request->input('banner_web_image_axis'),$request->file('banner_web_image'),'restaurant',$restaurant_name,'deals','web',true,false);
                        if($request->filled('banner_web_image_caption')){
                            $images['caption']=$request->input('banner_web_image_caption');
                        }else{
                            $images['caption']=$web_image?$web_image['caption']:'';
                        }
                        if($images){
                            $banners['web_image']=json_encode($images);
                        }
                    }else{
                        if($request->filled('banner_web_image_caption')){
                            $images=$web_image;
                            $images['caption']=$request->input('banner_web_image_caption');
                            $banners['web_image']=json_encode($images);

                        }
                    }

                    if($request->hasFile('banner_mobile_image')){

                        $images=CommonFunctions::imageUploader($request->input('banner_mobile_image_axis'),$request->file('banner_mobile_image'),'restaurant',$restaurant_name,'deals','mobile',true,false);
                        if($request->filled('banner_mobile_image_caption')){
                            $images['caption']=$request->input('banner_mobile_image_caption');
                        }else{
                            $images['caption']=$mobile_image?$mobile_image['caption']:'';
                        }
                        if($images){

                            $banners['mobile_image']=json_encode($images);
                        }
                    }else{
                        if($request->filled('banner_mobile_image_caption')){
                            $images=$mobile_image;
                            $images['caption']=$request->input('banner_mobile_image_caption');
                            $banners['mobile_image']=json_encode($images);
                        }
                    }

                    if($request->hasFile('banner_app_image')){

                        $images=CommonFunctions::imageUploader($request->input('banner_app_image_axis'),$request->file('banner_app_image'),'restaurant',$restaurant_name,'deals','app',true,false);
                        if($request->filled('banner_app_image_caption')){
                            $images['caption']=$request->input('banner_app_image_caption');
                        }else{
                            $images['caption']=$app_image?$app_image['caption']:'';
                        }
                        if($images){
                                $banners['app_image']=json_encode($images);
                        }
                    }else{
                        if($request->filled('banner_app_image_caption')){
                            $images=$app_image;
                            $images['caption']=$request->input('banner_app_image_caption');
                            if($images){
                                $banners['app_image']=json_encode($images);
                            }
                        }
                    }


                    DealBanners::updateOrCreate(
                        ['deal_id' => $id],
                        $banners
                    );
                }

            }else{

                if($request->has('deal_group')){

                    $deal_group=isset($data['deal_group'])?implode(',',$data['deal_group']):'';
                    $deal_group_array=$data['deal_group'];
                    $data_request = [
                        'deal_group'=>$deal_group,
                    ];
                    if(in_array('deal',$deal_group_array)){
                        echo '<pre>';
                        if($request->has('group_price_type') && $request->has('group_price') ){
                            $data_request['group_price_type']=$data['group_price_type'];
                            $data_request['group_price']=$data['group_price'];
                        }
                        if($request->has('group') && count($request->input('group'))){

                            $groups=$request->input('group');

                            //print_r($groups);die;
                            foreach ($groups as $group){
                                if(!empty($group['name']) && !empty($group['quantity_type']) && !empty($group['quantity_value'])){
                                    $group_data=[
                                        'deal_id'=>$id,
                                        'restaurant_id'=>$restaurant_id,
                                        'group_name'=>$group['name'],
                                        'quantity_type'=>$group['quantity_type'],
                                        'quantity'=>$group['quantity_value'],
                                        'is_required'=>$group['is_required']??'0',
                                    ];


                                    if(!empty($group['id'])){
                                        $group_id=$group['id'];
                                        DealGroups::update(
                                            ['deal_id' => $id,'id'=>$group['id']],
                                            $group_data
                                        );

                                    }else{
                                        $groupobj=DealGroups::create($group_data);
                                        $group_id=$groupobj->id;
                                    }

                                    if(isset($group['product']) && count($group['product'])){

                                        foreach ($group['product'] as $prod){
                                            if(!empty($prod['sku']) && !empty($prod['price_type']) && isset($prod['price_type_value']) && isset($prod['free_topping_quantity'])){
                                               $prod_data=[
                                                    'deal_group_id'=>$group_id,
                                                    'deal_id'=>$id,
                                                    'restaurant_id'=>$restaurant_id,
                                                    'sku'=>$prod['sku'],
                                                    'price_type'=>$prod['price_type'],
                                                    'price_type_value'=>$prod['price_type_value'],
                                                    'free_topping_quantity'=>$prod['free_topping_quantity'],
                                                ];


                                                if(!empty($prod['id'])){

                                                    DealGroupItems::update(
                                                        ['deal_group_id' => $group_id,'id'=>$prod['id']],
                                                        $prod_data
                                                    );


                                                }else{
                                                    DealGroupItems::create($prod_data);

                                                }

                                            }

                                        }

                                    }

                                }

                            }
                        }

                    }
                    Deals::where('restaurant_id',$restaurant_id)->where('id', '=' ,$id)->update($data_request);

                    if(in_array('flat_discount',$deal_group_array)){

                        if($request->has('discount_type') && $request->has('discount_value') ){
                            $flat_discounts=[
                                'deal_id'=>$id,
                                'restaurant_id'=>$restaurant_id,
                                'discount_type'=>$data['discount_type']??'',
                                'discount_value'=>$data['discount_value']??'',
                            ];
                            DealFlatDiscounts::updateOrCreate(
                                ['deal_id' => $id],
                                $flat_discounts
                            );
                        }

                    }

                    if(in_array('product',$deal_group_array)){

                        if($request->has('products') && $request->has('discount_percentage') ){
                            $product_data=[
                                'deal_id'=>$id,
                                'restaurant_id'=>$restaurant_id,
                                'type'=>$data['on_specific'],
                                'sku'=> $request->has('products')?implode(',',$data['products']):'',
                                'discount_percentage'=>$data['discount_percentage']??'',
                            ];
                            DealSpecificProductAndCats::updateOrCreate(
                                ['deal_id' => $id],
                                $product_data
                            );
                        }

                    }

                    if(in_array('spend',$deal_group_array)){

                        if($request->has('order_value_type') && $request->has('order_value') ){
                            $spend_data=[
                                'deal_id'=>$id,
                                'restaurant_id'=>$restaurant_id,
                                'order_value_type'=>$data['order_value_type'],
                                'order_value'=>$data['order_value']
                            ];

                            if($request->has('spend_type')){

                                if(in_array('discount',$request->input('spend_type')) && $request->filled('discount_type') && $request->filled('discount_value')) {
                                    $spend_data['is_discount']='1';
                                    $spend_data['discount_type']=$data['discount_type'];
                                    $spend_data['discount_value']=$data['discount_value'];

                                }

                                if(in_array('free_item',$request->input('spend_type')) &&  $request->filled('product_sku')) {
                                    $spend_data['is_free_item']='1';
                                    $spend_data['product_sku']=$request->has('product_sku')?implode(',',$data['product_sku']):'';

                                }
                            }

                            DealSpends::updateOrCreate(
                                ['deal_id' => $id],
                                $spend_data
                            );
                        }

                    }

                }


            }
            DB::commit();
            return redirect('deal/'.$id.'/edit?step='.$step)->with('status_msg', 'Deal has been updated successfully.');


        } catch (\Exception $e) {
            DB::rollback();
            return  redirect('deal/'.$id.'/edit?step='.$step)->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'.$e->getMessage()));

        }

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $deal = Deals::find($id);
        if(!empty($deal)){
            $deal->delete();
            return redirect()->back()->with('status_msg','Deal deleted successfully');
        }
        else{
            return  redirect()->back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));

        }
    }
}
