<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Deal\Http\Controllers'], function()
{

    Route::resource('deal', 'DealController');
    Route::get('deal/{id}/destroy/', 'DealController@destroy');

    Route::post('sku/{type}','DealController@getSku');

});
