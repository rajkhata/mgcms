<?php
namespace Modules\MenuManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemModifierGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'group_name' => 'required',
            'prompt' => 'required',
            'restaurant_id' => 'required',
            //'language_id' => 'required',
            'is_required' => 'required',
            'quantity_type' => 'required',       
         
        ];

        if ($this->attributes->get('quantity_type') == 'Range') {
            $rules['min']='required';
            $rules['max']='required';
        }else{
            $rules['quantity']='required';
        }  

        return $rules;  

    }

    public function messages()
    {
        return [
            'group_name.required' => 'Modifier Group name should not be empty',
            'prompt.required' => 'Modifier Group prompt should not be empty',
           // 'language_id.required' => 'Modifier Group name should not be empty',
            'is_required.required' => 'Is required should not be empty',
            'restaurant_id.required' => 'Select Restaurant',
            'quantity_type.required' => 'Quantity Type should not be empty',
            'min.required' => 'Quantity min should not be empty',
            'max.required' => 'Quantity max should not be empty',
            'quantity.required' => 'Quantity  should not be empty',
           
        ];
    }
}