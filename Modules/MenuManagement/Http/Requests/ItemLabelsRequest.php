<?php
namespace Modules\MenuManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemLabelsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label_name' => 'required',
            'restaurant_id' => 'required',
         
        ];
    }

    public function messages()
    {
        return [
            'label_name.required' => 'Label name should not be empty',
            'restaurant_id.required' => 'Select Restaurant',
           
        ];
    }
}