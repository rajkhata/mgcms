<?php

namespace Modules\MenuManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemAddonsRequest extends FormRequest {

    public $isRulesRequired = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        if ($this->isRulesRequired) {
            $rules = [
                'name' => 'required',
                'restaurant_id' => 'required',
                'addon_group_id' => 'required',
                'item_id' => 'required',
                'addon_price' => 'required',
//            'is_default' => 'required',
//            'is_image_required' => 'required',
            ];
        } else {
            $rules = [];
        }

        return $rules;
    }

    public function messages() {
        return [
            'name.required' => 'Item name should not be empty',
            'is_image_required.required' => 'Is image required should not be empty',
            'restaurant_id.required' => 'Restaurant could not be empty',
            'addon_group_id.required' => 'Addon Group Id should not be empty',
            'item_id' => 'Menu item should not be empty',
            'addon_price.required' => 'Addon Price should not be empty',
        ];
    }

}
