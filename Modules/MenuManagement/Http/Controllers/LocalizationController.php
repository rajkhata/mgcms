<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\MenuManagement\Http\Controllers;

use Modules\MenuManagement\Entities\Localization;
use App\Models\Language;
use App\Helpers\CommonFunctions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use File;
use Config;
use DB;

class LocalizationController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $localization = Localization::where('key','!=', null)->orderBy('key', 'ASC')->orderBy('id', 'DESC')->paginate(20);
        //dd($localization);

        return view('menumanagement::localization.index', compact('localization'));
    }

    /**
     * Create Localization
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $languages = Language::select('id', 'language_name')->get();
        //dd($languages);
        return view('menumanagement::localization.create', compact('languages'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'language_id' => 'required',
                    'key' => 'required|max:255',
                    'value' => 'required|max:255'
        ]);
        if (!$validator->fails()) {
            $data = [
                'language_id' => $request->input('language_id'),
                'key' => $request->input('key'),
                'value' => $request->input('value'),
                'platform' => $request->input('platform'),
                'updated_at' => now(),
                'status' => $request->input('status')
            ];

            Localization::create($data);
            return redirect('/menu/localization')->with('message', 'Localization added successfully');
        } else {

            return Redirect::back()->withErrors($validator->errors())->withInput();
        }
    }

    public function edit($id) {
        $localize = Localization::find($id);
        $languages = Language::select('id', 'language_name')->get();

        return view('menumanagement::localization.edit', compact('localize', 'languages'));
    }

    public function update(Request $request, $id) {
        if (isset($id) && is_numeric($id)) {
            $localize = Localization::find($id);

            $validator = Validator::make($request->all(), [
                        'language_id' => 'required|max:255',
                        'key' => 'required|max:255',
                        'value' => 'required|max:255'
            ]);

            if (!$validator->fails()) {
                $localize->language_id = $request->input('language_id');
                $localize->key = $request->input('key');
                $localize->value = $request->input('value');
                $localize->platform = $request->input('platform');
                $localize->updated_at = now();
                $localize->status = $request->input('status');
                $localize->save();
                return redirect('/menu/localization')->with('message', 'Localization updated successfully');
            } else {

                return Redirect::back()->withErsudorors($validator->errors())->withInput();
            }
        }

        return Redirect::back()->with('message', 'Invalid Id');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
        //DB::beginTransaction();
        try {
            Localization::where('id', $id)->delete();
            return redirect('/menu/localization')->with('message', 'Localization deleted successfully');
        } catch (\Exception $e) {
            return Redirect::back()->with('message', 'Something went wrong, please try again.');
        }
    }

}
