<?php

namespace Modules\MenuManagement\Http\Controllers;

use Modules\MenuManagement\Entities\MenuItemModel;
use Modules\MenuManagement\Entities\MenuItemsVisibility;
use Modules\MenuManagement\Entities\VisibilityHours;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use File;
use Config;
use DB;

class MenuItemsVisibilityController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $restaurantId, $menuItemId) {

        $visibilityHours = VisibilityHours::where(['restaurant_id' => $restaurantId])->orderBy('id', 'DESC')->get();
        $menuItemVisibility = MenuItemsVisibility::where(['menu_item_id' => $menuItemId])->first();

        if ($menuItemVisibility === null) {
            $availableVisibilityId = 0;
        } else {
            $availableVisibilityId = $menuItemVisibility->visibility_duration_week_time_id;
        }

        $visibilityHoursOptions = [0 => '<option value="">--- Item Visibility Slot ---</option>'];

        if ($visibilityHours->count()) {

            foreach ($visibilityHours as $row) {
                $visibilityHoursOptions[] = '<option value="' . $row->id . '" ' . ($row->id == $availableVisibilityId ? 'selected="selected"' : '') . '>' . htmlspecialchars($row->slot_name) . '</option>';
            }
        }

        $data = [
            'status' => true,
            'message' => 'Visibility hours',
            'data' => implode('', $visibilityHoursOptions)
        ];

        return json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $visibilityScheduleId, $menuItemId) {

        $rules = [
            'menu_item_id' => 'required|numeric',
            'visibility_duration_week_time_id' => 'required|numeric',
        ];
        $inputData = [
            'menu_item_id' => $menuItemId,
            'visibility_duration_week_time_id' => $visibilityScheduleId,
        ];

        $validator = Validator::make($inputData, $rules, [
                    'menu_item_id.*' => 'Invalid menu item',
                    'visibility_duration_week_time_id.*' => 'Please select Visibility Schedule',
        ]);

        if ($validator->passes()) {
            $addUpdate = 'added';
            $menuItemVisibility = MenuItemsVisibility::where(['menu_item_id' => $menuItemId])->first();
            if ($menuItemVisibility === null) {
                $menuItemsVisibilityObj = MenuItemsVisibility::create($inputData);
            } else {
                $menuItemsVisibilityObj = MenuItemsVisibility::where(['menu_item_id' => $menuItemId])->update($inputData);
                $addUpdate = 'updated';
            }
            $data = [
                'status' => true,
                'message' => "Visibility schedule $addUpdate successfully",
                'data' => []
            ];
        } else {
            $data = [
                'status' => true,
                'message' => 'Error to add Visibility schedule',
                'data' => $validator->errors()
            ];
        }

        return $data;
    }

}
