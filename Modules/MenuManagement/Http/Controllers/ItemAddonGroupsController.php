<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\MenuManagement\Http\Controllers;

use Modules\MenuManagement\Entities\ItemAddonGroups;
use Modules\MenuManagement\Entities\ItemAddonGroupsLanguage;
use Modules\MenuManagement\Entities\ItemAddons;
use App\Models\Restaurant;
use App\Models\Language;
use App\Helpers\CommonFunctions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use File;
use Config;
use DB;

class ItemAddonGroupsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $restaurantId, $menuItemId, $languageId) {

        $itemAddonGroups = ItemAddonGroups::where(['restaurant_id' => $restaurantId, 'menu_item_id' => $menuItemId])->orderBy('id', 'DESC')->get();
//        $itemAddonGroups = $itemAddonGroupObj->paginate(20);

        if ($itemAddonGroups->count()) {
            // get addons for this group
            $addonGroupIds = [];
            foreach ($itemAddonGroups as $itemAddonGroup) {
                $addonGroupIds[] = $itemAddonGroup->id;
                $itemAddonGroupsLanguage = $itemAddonGroup->itemAddonGroupsLanguage()->where([
                            'item_addon_group_id' => $itemAddonGroup->id,
                            'language_id' => $languageId
                        ])->first();
//                $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
//                            'item_addon_group_id' => $itemAddonGroup->id,
//                            'language_id' => $languageId
//                        ])->first();
                
                if ($itemAddonGroupsLanguage !== null) {
                    $itemAddonGroup->group_name = $itemAddonGroupsLanguage->group_name;
                    $itemAddonGroup->prompt = $itemAddonGroupsLanguage->prompt;
                }
            }

            $itemAddons = ItemAddons::whereIn('addon_group_id', $addonGroupIds)->orderBy('id', 'ASC')->get();
//            $itemAddons = $itemAddonsObj->paginate(20);

            $data = [
                'status' => true,
                'message' => 'Item addon groups',
                'data' => view('menumanagement::item_addon_groups.index', compact('itemAddonGroups', 'itemAddons', 'languageId'))->render()
            ];
        } else {
            $data = [
                'status' => true,
                'message' => 'Item addon groups',
                'data' => 'No addon groups available.'
            ];
        }

        return json_encode($data);
    }

//    public function index(Request $request, $restaurantId = 0) {
//        if ($request->ajax()) {
//
//            try {
//                $itemAddonGroups = ItemAddonGroups::where('restaurant_id', $restaurantId)->orderBy('id', 'DESC')->get();
//                if ($itemAddonGroups->count()) {
//                    $data = [
//                        'status' => true,
//                        'message' => 'Item addon groups listing',
//                        'data' => $itemAddonGroups
//                    ];
//                } else {
//                    $data = [
//                        'status' => true,
//                        'message' => 'Item addon groups listing',
//                        'data' => []
//                    ];
//                }
//            } catch (Exception $exc) {
//                $data = [
//                    'status' => false,
//                    'message' => 'ERROR: ' . $exc->getTraceAsString(),
//                ];
//            }
//
//            return json_encode($data);
//        }
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
        $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();

        return view('item_addon_groups.create', compact('restaurantData', 'languageData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = [
            'group_name' => 'required|max:100',
            'prompt' => 'sometimes|string|max:100',
            'restaurant_id' => 'required|numeric',
            'menu_item_id' => 'required|numeric',
            'language_id' => 'required|numeric|min:1',
            'is_required' => 'required|numeric|min:0|max:1',
            'quantity' => 'required',
            'quantity_type' => 'required|string|in:Value,Range',
        ];

        $validator = Validator::make(Input::all(), $rules, [
                    'language_id.*' => 'Please select valid Language',
                    'restaurant_id.*' => 'Please select Restaurant',
        ]);

        if ($validator->passes()) {
            $inputArr = Input::all();
            $quantityArr = explode(',', $inputArr['quantity']);

            if ($inputArr['quantity_type'] == 'Value' && (!isset($inputArr['quantity']) || !is_numeric($inputArr['quantity']))) {
                $data = [
                    'status' => false,
                    'message' => 'Error: invalid quantity value',
                    'data' => $inputArr
                ];
                return $data;
            } elseif ($inputArr['quantity_type'] == 'Range' && (!isset($quantityArr[0]) || !isset($quantityArr[1]) || !is_numeric($quantityArr[0]) || !is_numeric($quantityArr[1]))) {
                $data = [
                    'status' => false,
                    'message' => 'Error: invalid quantity range value',
                    'data' => $inputArr
                ];
                return $data;
            }
            $inputItems = Input::all();
            if ($inputArr['quantity_type'] == 'Value') {
                $inputItems['quantity'] = (int) $inputItems['quantity'];
            } else {
                $inputItems['quantity'] = (((int) $quantityArr[0]) . ',' . ((int) $quantityArr[1]));
            }
            $itemAddonGroupObj = ItemAddonGroups::create($inputItems);

            $languages = CommonFunctions::getAvailableLanguages();

            foreach ($languages as $languageRow) {
                // save the language data
                $groupLanguageData = [
                    'item_addon_group_id' => $itemAddonGroupObj->id,
                    'group_name' => $inputItems['group_name'],
                    'prompt' => $inputItems['prompt'],
                    'language_id' => $languageRow->id // $inputItems['language_id'],
                ];

                ItemAddonGroupsLanguage::create($groupLanguageData);
            }
            //return redirect()->back()->with('message', 'Item addon group added successfully');
            $data = [
                'status' => true,
                'message' => 'Item addon group added successfully',
                'data' => $itemAddonGroupObj->toArray()//view('menumanagement::item_addon_groups.index', compact('itemAddonGroups', 'itemAddons'))->render()
            ];
        } else {
            $data = [
                'status' => true,
                'message' => 'Error to add Item addon group',
                'data' => $validator->errors()
            ];
        }

        return $data; //redirect()->back()->withErrors($validator->errors())->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (isset($id) && is_numeric($id)) {
            $itemAddonGroup = ItemAddonGroups::find($id);
            $restaurantData = Restaurant::select('id', 'restaurant_name')->orderBy('id', 'DESC')->get();
            $languageData = Language::select('id', 'language_name')->orderBy('language_name', 'ASC')->get();

            return view('item_addon_groups.edit', compact('itemAddonGroup', 'restaurantData', 'languageData'));
        }
        return Redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (isset($id) && is_numeric($id)) {
            $rules = [
                'group_name' => 'required|max:100',
                'prompt' => 'sometimes|string|max:100',
//                'restaurant_id' => 'required|numeric',
//                'menu_item_id' => 'required|numeric',
                'language_id' => 'required|numeric|min:0',
                'is_required' => 'required|numeric|min:0|max:1',
                'quantity' => 'required',
                'quantity_type' => 'required|string|in:Value,Range',
            ];

            $validator = Validator::make(Input::all(), $rules, [
                        'language_id.*' => 'Please select valid Language',
                        'restaurant_id.*' => 'Please select Restaurant',
            ]);

            if ($validator->passes()) {
                $inputArr = Input::except('_method', '_token', 'id');
                $quantityArr = explode(',', $inputArr['quantity']);
                if ($inputArr['quantity_type'] == 'Value' && (!isset($inputArr['quantity']) || !is_numeric($inputArr['quantity']))) {
                    $data = [
                        'status' => false,
                        'message' => 'Error: invalid quantity value: ' . $inputArr['quantity'],
                        'data' => $inputArr
                    ];
                    return $data;
                } elseif ($inputArr['quantity_type'] == 'Range' && (!isset($quantityArr[0]) || !isset($quantityArr[1]) || !((int) $quantityArr[0]) || !((int) $quantityArr[1]))) {
                    $data = [
                        'status' => false,
                        'message' => 'Error: invalid quantity value: ' . $inputArr['quantity'],
                        'data' => $quantityArr
                    ];
                    return $data;
                }

                DB::beginTransaction();

                try {
                    if ($inputArr['quantity_type'] == 'Value') {
                        $inputArr['quantity'] = (int) $inputArr['quantity'];
                    } else {
                        $inputArr['quantity'] = (((int) $quantityArr[0]) . ',' . ((int) $quantityArr[1]));
                    }
                    // update addon item group
                    $groupData = [
//                        'group_name' => $inputArr['group_name'],
//                        'prompt' => $inputArr['prompt'],
                        'is_required' => $inputArr['is_required'],
                        'quantity_type' => $inputArr['quantity_type'],
                        'quantity' => $inputArr['quantity'],
                    ];

                    ItemAddonGroups::where('id', $id)->update($groupData);
                    $groupLanguageData = [
                        'item_addon_group_id' => $id,
                        'group_name' => $inputArr['group_name'],
                        'prompt' => $inputArr['prompt'],
                        'language_id' => $inputArr['language_id'],
                    ];

                    // ItemAddonGroupsLanguage::create($groupLanguageData);
                    $itemAddonGroupsLanguageObj = ItemAddonGroupsLanguage::where([
                                'item_addon_group_id' => $id,
                                'language_id' => $inputArr['language_id']
                            ])->first();

                    if ($itemAddonGroupsLanguageObj !== null) {
                        ItemAddonGroupsLanguage::where('id', $itemAddonGroupsLanguageObj->id)->update($groupLanguageData);
                    } else {
                        ItemAddonGroupsLanguage::create($groupLanguageData);
                    }

                    // update its items
                    foreach ($inputArr as $key => $value) {
                        if (strpos($key, 'price') !== false) {
                            $tmpArr = explode('_', $key);
                            $updateArr = [
                                'addon_price' => $value,
                            ];
                            if (!is_numeric($value)) {
                                $data = [
                                    'status' => false,
                                    'message' => 'Error: invalid price value: ' . $value,
                                    'data' => $inputArr
                                ];
                                DB::rollback();
                                return $data;
                            }

                            ItemAddons::where(['id' => $tmpArr[1], 'addon_group_id' => $id])->update($updateArr);
                        }
                    }
                    DB::commit();
                    $data = [
                        'status' => true,
                        'message' => 'Data updated successfully',
                        'data' => $inputArr//view('menumanagement::item_addon_groups.index', compact('itemAddonGroups', 'itemAddons'))->render()
                    ];
                } catch (Exception $exc) {
                    DB::rollback();
                    // echo $exc->getTraceAsString();
                    $data = [
                        'status' => true,
                        'message' => 'Error to update record',
                        'data' => $exc->getTraceAsString()
                    ];
                }

                return $data; //redirect()->back()->with('message', 'Item addon group updated successfully');
            } else {
                $data = [
                    'status' => true,
                    'message' => 'Error to update addon item group',
                    'data' => $validator->errors()
                ];
                return $data; //redirect()->back()->withErrors($validator->errors())->withInput();
            }
        }
        $data = [
            'status' => true,
            'message' => 'Invalid Id',
            'data' => [$id]
        ];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
        //DB::beginTransaction();
        try {
            ItemAddons::where('addon_group_id', $id)->delete();
            ItemAddonGroups::where('id', $id)->delete();
            //DB::commit();
            $data = [
                'status' => true,
                'message' => 'Item Addon Group has been deleted successfully',
                'data' => []
            ];
            return $data; // back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            //DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => ['error' => $e->getMessage()]
            ];
            return $data; //back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    }

}
