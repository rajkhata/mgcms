<?php

namespace Modules\MenuManagement\Http\Controllers\v2;

use App\Helpers\CommonFunctions;
use App\Models\Restaurant;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\MenuManagement\Entities\ItemModifier;
use Modules\MenuManagement\Entities\ItemModifierGroup;
use Modules\MenuManagement\Entities\ItemModifierGroupLanguage;
use Modules\MenuManagement\Http\Requests\ItemModifierGroupRequest;

class ItemModifierGroupController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurant_id = 0;
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));        
        $restaurantData = Restaurant::select('id','restaurant_name')->whereIn('id',$locations)->orderBy('id','DESC')->get();
        if($restaurantData && count($restaurantData) == 1) { 
           $restaurant_id = $restaurantData[0]['id'];
        }

        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))){
            $restaurant_id = $request->input('restaurant_id');
            $data = ItemModifierGroup::where('restaurant_id',$restaurant_id)->orderBy('id','DESC')->paginate(5);
        }else{
            $data = ItemModifierGroup::orderBy('id','DESC')->whereIn('restaurant_id',$locations)->paginate(5);
        }
       
        return view('menumanagement::ItemModifierGroup.index', compact('data','restaurantData','restaurant_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $restaurantData = CommonFunctions::getRestaurantGroup();   
        $selected_rest_id = 0;
        if(count($restaurantData) == 1) {
            $first_ind_array = current($restaurantData);
            if(count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        }   
       return view('menumanagement::ItemModifierGroup.create', compact('restaurantData', 'selected_rest_id'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ItemModifierGroupRequest $request)
    {
         try {
            DB::beginTransaction();
            
            $data = $request->all();

            $data_request=[
                'restaurant_id' => $data['restaurant_id'],
                'group_name' => $data['group_name'],               
                'prompt' => $data['prompt'],
                //'pos_id' => $data['pos_id'] ?? null,
                //'calorie' => $data['calorie'] ?? null,
                'language_id' =>isset($data['language_id'])?$data['language_id']:1,
                'is_required' => $data['is_required'],               
                'quantity_type' => $data['quantity_type'],              
                'quantity' => $data['quantity'],              
            ];
            if(isset($data['menu_id'])){
                $data_request['menu_item_id']=$data['menu_id'];
            }
          /*  if($data['quantity_type']=='Value'){
                $data_request['min']=$data['quantity'];
                $data_request['max']=$data['quantity'];

            }else{
                $data_request['min']=$data['min'];
                $data_request['max']=$data['max'];
            }
          */

            $insertd_data=ItemModifierGroup::create($data_request);
             $language_data=[
                'modifier_group_id' =>$insertd_data->id,
                'language_id' =>isset($data['language_id'])?$data['language_id']:1,   
                'group_name' => $data['group_name'],               
                'prompt' => $data['prompt'],               
            ];
            ItemModifierGroupLanguage::create($language_data);

            DB::commit();
            if ($request->ajax()) {
                $data = [
                    'success' => true,
                    'message' => 'Item Modifier Group has been created successfully.',
                    'data'    => $this->getModifierGroupHtml($insertd_data),
                ];
            return \Response::json($data, 200);

            }else{
              return back()->with('status_msg', 'Item Modifier Group has been created successfully.');
            }

        } catch (\Exception $e) {
           DB::rollback();
            if ($request->ajax()) {
               return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
           
            }else{
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
            }
         }
    }

    private function getModifierGroupHtml($modifierData)
    {
        if($modifierData->quantity_type == 'Value') {
            $optionHtml = '<option value="Value" selected="selected">Value</option><option value="Range">Range</option>';
        } else {
            $optionHtml = '<option value="Value">Value</option><option value="Range" selected="selected">Range</option>';
        }
        if($modifierData->is_required == 1) {
            $requiredHtml = 'checked="checked"';
            $optionalHtml = '';
        } else {
            $requiredHtml = '';
            $optionalHtml = 'checked="checked"';
        }
        if($modifierData->show_as_dropdown == 1) {
            $yesHtml = 'checked="checked"';
            $noHtml = '';
        } else {
            $yesHtml = '';
            $noHtml = 'checked="checked"';
        }
        if($modifierData->status == 1){
            $activeHtml = 'checked="checked"';
            $inactiveHtml = "";    
        }else{
            $inactiveHtml = 'checked="checked"';
            $activeHtml = "";    
        }
        $html = '<div class="panel panel-default">'
                   .'<div class="panel-heading" role="tab" id="heading'.$modifierData->id.'"> '
                       .'<h4 class="panel-title"> '
                            .'<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$modifierData->id.'" aria-expanded="false" aria-controls="collapse'.$modifierData->id.'">'
                            .$modifierData->group_name.'</a>'
                            .'<span class="delete_edit">'
                                .'<i class="fa fa-trash-o delete_modifier_group" aria-hidden="true" modifiergroup="'.$modifierData->id.'"></i>'
                            .'</span>'
                       .'</h4>'
                   .'</div>'
                .'<div id="collapse'.$modifierData->id.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'.$modifierData->id.'" aria-expanded="true" style="height:0px;">'
                  .'<div class="panel-body">'
                        .'<div class="form-group row form_field__container">'
                            .'<label for="priority" class="col-md-4 col-form-label text-md-right">Name</label>'
                            .'<div class="col-md-4">'
                                .'<div class="input__field">'
                                    .'<input type="text" class="" name="modifiergroup['.$modifierData->id.'][group_name]" value="'.$modifierData->group_name.'" required="">'
                                .'</div>'
                            .'</div>'
                        .'</div>'
                        .'<div class="form-group row form_field__container">'
                            .'<label for="priority" class="col-md-4 col-form-label text-md-right">Prompt</label>'
                            .'<div class="col-md-4">'
                                .'<div class="input__field">'
                                    .'<input type="text" class="" name="modifiergroup['.$modifierData->id.'][prompt]" value="'.$modifierData->prompt.'" required="">'
                                .'</div>'
                            .'</div>'
                        .'</div>'
                        .'<div class="form-group row form_field__container">'
                             .'<label class="col-md-4 col-form-label text-md-right">Quantity Type</label>'
                             .'<div class="col-md-3 quantity_type_select">'
                                .'<select name="modifiergroup['.$modifierData->id.'][quantity_type]" class="quantity_type form-control" required="">'
                                    .$optionHtml
                                .'</select>'
                             .'</div>'
                             .'<div class="col-md-2 quantity_type_val">'
                                .'<div class="value_quantity1">'
                                    .'<div class="input__field">'
                                        .'<input id="quantity" type="text" class="" name="modifiergroup['.$modifierData->id.'][quantity]" value="'.$modifierData->quantity.'" required="" placeholder="Min,Max">'
                                    .'</div>'
                                .'</div>'
                             .'</div>'
                        .'</div>'
                        .'<div class="form-group row form_field__container">'
                          .'<label for="priority" class="col-md-4 col-form-label text-md-right">Is this required?</label>'
                          .'<div class="col-md-4" style="padding-top: 10px;">'
                              .'<label class="radio-inline"><input type="radio" name="modifiergroup['.$modifierData->id.'][is_required]" value="1" '.$requiredHtml.'> Required</label>&nbsp;&nbsp;&nbsp;&nbsp;'
                              .'<label class="radio-inline"><input type="radio" name="modifiergroup['.$modifierData->id.'][is_required]" value="0" '.$optionalHtml.'> Optional</label>'
                          .'</div>'
                        .'</div>'
                        .'<div class="form-group row form_field__container">'
                            .'<label for="priority" class="col-md-4 col-form-label text-md-right">Show as dropdown</label>'
                            .'<div class="col-md-4" style="padding-top: 10px;">'
                                .'<label class="radio-inline"><input type="radio" name="modifiergroup['.$modifierData->id.'][show_as_dropdown]" value="1" '.$yesHtml.'>Yes</label>&nbsp;&nbsp;'
                                .'<label class="radio-inline"><input type="radio" name="modifiergroup['.$modifierData->id.'][show_as_dropdown]" value="0" '.$noHtml.'> No</label>'
                            .'</div>'
                        .'</div>'
                        .'<div class="form-group row form_field__container">'
                        .'<label for="priority" class="col-md-4 col-form-label text-md-right">Status</label>'
                        .'<div class="col-md-4" style="padding-top: 10px;">'
                            .'<label class="radio-inline"><input type="radio" name="modifiergroup['.$modifierData->id.'][status]" value="1" '.$activeHtml.'>Active</label>&nbsp;&nbsp;'
                            .'<label class="radio-inline"><input type="radio" name="modifiergroup['.$modifierData->id.'][status]" value="0" '.$inactiveHtml.'>InActive</label>'
                        .'</div>'
                    .'</div>'
                        .'<div class="form-group row form_field__container">'
                            .'<label for="priority" class="col-md-4 col-form-label text-md-right">Sort Order</label>'
                            .'<div class="col-md-4">'
                                .'<div class="input__field">'
                                    .'<input type="number" class="" name="modifiergroup['.$modifierData->id.'][sort_order]" value="'.$modifierData->sort_order.'" required="">'
                                .'</div>'
                            .'</div>'
                        .'</div>'
                        .'<a href="javascript:void(0)" class="add_modifier" modifiergroup="'.$modifierData->id.'">+add modifier</a>'
                  .'</div>'
                .'</div>'
            .'</div>';

        return $html;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('menumanagement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
       $restaurantData = CommonFunctions::getRestaurantGroup();  
        $data = ItemModifierGroup::find($id);

        return view('menumanagement::ItemModifierGroup.edit', compact('restaurantData','data'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    

    public function update(ItemModifierGroupRequest $request, $id)
    {
         if(!isset($id) || !is_numeric($id)){
            return redirect()->back()->with('err_msg','Invalid Request');
        }
       
        DB::beginTransaction();
        try {
            
            $data = $request->all();    
            $data_request=[
                'restaurant_id' => $data['restaurant_id'],
                'group_name' => $data['group_name'],               
                'prompt' => $data['prompt'],               
                'language_id' =>1,   
                'is_required' => $data['is_required'],               
                'quantity_type' => $data['quantity_type'],              
            ];
            if(isset($data['menu_id'])){
                $data_request['menu_item_id']=$data['menu_id'];
            }           

            ItemModifierGroup::where('id', $id)->update($data_request);
            DB::commit();
            return back()->with('status_msg', 'Item Modifier Group has been updated successfully.');
            
        } catch (\Exception $e) {
            DB::rollback();
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));

        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request,$id)
    {
        DB::beginTransaction();
        try {

            ItemModifier::where('modifier_group_id', $id)->delete();
            ItemModifierGroup::where('id', $id)->delete();
            DB::commit();


            if ($request->ajax()) {
            return \Response::json(['success' => true, 'message' =>'Item Modifier Group has been deleted successfully.'], 200);

            }else{
              return back()->with('status_msg', 'Item Modifier Group has been deleted successfully.');
            }

        }
        catch (\Exception $e){
            DB::rollback();
               if ($request->ajax()) {
                    return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
           
                }else{
                    return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
                }
        }
    }
}
