<?php

namespace Modules\MenuManagement\Http\Controllers\v2;

use App\Helpers\CommonFunctions;
use App\Models\Language;
//use App\Models\MenuCategory;
//use App\Models\MenuCategoryLanguage;
use App\Models\MenuMealType;
use App\Models\MenuMealTypePrice;
use App\Models\MenuSettingCategory;
use App\Models\MenuSettingItem;
use App\Models\MenuSubCategory;
use App\Models\Restaurant;
use App\Modesl\RestaurantPromotion;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Modules\MenuManagement\Entities\ItemAddonGroups;
use Modules\MenuManagement\Entities\ItemAddons;
use Modules\MenuManagement\Entities\ItemModifier;
 
 
use Modules\MenuManagement\Entities\Labels;
use Modules\MenuManagement\Entities\MenuItemLabels;
use Modules\MenuManagement\Entities\MenuItemModel;
use Modules\MenuManagement\Entities\MenuItemsLanguage;
use Modules\MenuManagement\Entities\ModifierCategories;


use App\Models\v2menu\MaAttributeSet;
use App\Models\v2menu\MaAttributeItem;
use App\Models\v2menu\MaAttribute;
use App\Models\v2menu\ProductType;
use Modules\MenuManagement\Entities\v2menu\MenuCategory;
use Modules\MenuManagement\Entities\v2menu\MenuItem;
use Modules\MenuManagement\Entities\v2menu\MenuItemAttribute;
use Modules\MenuManagement\Entities\v2menu\MenuItemPrice;
use Modules\MenuManagement\Entities\v2menu\ItemModifierGroup;
use Modules\MenuManagement\Entities\v2menu\ItemModifierGroupItem;
use Modules\MenuManagement\Entities\v2menu\MenuItemInventory;
use Modules\MenuManagement\Entities\v2menu\ItemModifierGroupAttribute;
use Modules\MenuManagement\Entities\v2menu\ItemModifierGroupItemAttribute;
use Modules\MenuManagement\Entities\v2menu\ItemAddonsGroup;
use Modules\MenuManagement\Entities\v2menu\ItemAddonsGroupAttribute;
use Modules\MenuManagement\Entities\v2menu\ItemAddonsItem;
use Modules\MenuManagement\Entities\v2menu\MenuAvailability;
use Modules\MenuManagement\Entities\v2menu\ItemRelatedProductItem;
use Modules\MenuManagement\Entities\v2menu\ItemRelatedProductGroupAttribute;
use Modules\MenuManagement\Entities\v2menu\ItemRelatedProductGroup;
use Modules\MenuManagement\Entities\v2menu\MenuItemImages;
use Modules\MenuManagement\Entities\v2menu\ItemMealsGroup;
use Modules\MenuManagement\Entities\v2menu\ItemMealsGroupAttribute;
use Modules\MenuManagement\Entities\v2menu\ItemMealsItem;
use Modules\MenuManagement\Entities\v2menu\MenuItemSort;
use App\Models\UserOrderDetail;

class MenuItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        /*$this->middleware('permission:Menu List');
        $this->middleware('permission:Menu Edit', ['only' => ['updateprofile']]);
        $this->middleware('permission:Menu Status', ['only' => ['updateprofile']]);
        $this->middleware('permission:Menu Delete', ['only' => ['updateprofile']]);
        $this->middleware('permission:Menu Add', ['only' => ['updateprofile']]);*/
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request,$cat_id=0,$rest_id=0,$sort_type_id=0,$category_filter_by=0,$product_type_filter_by=0,$status_filter_by=0,$type_filter_by=0)
    {
	$user = Auth::user(); 
	$locations = $this->getRestaurantId();
	$groupRestData = $this->getGroupRestDataList();
	if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            	$rest_id = $locations[0] = $request->input('restaurant_id'); 
	}elseif($user->role=="manager") {
		$rest_id = $locations[0];
	}
	$menu_category = $this->getMaCatList($locations); 
	$product_types =  $this->getProductTypeList();
	$sortAlgo = MenuItemSort::where('restaurant_id',$rest_id)->get()->toArray(); 
        $this->doMenuReSorting($rest_id,$sort_type_id,$locations); 
        $menuItems = $this->getMaMenuItemList($locations,0,null,null,$cat_id,$rest_id,$category_filter_by,$product_type_filter_by,$status_filter_by,$type_filter_by);
	foreach($menuItems as $i=>$m){
		$menuItems[$i]['noofitem_sold'] =count(UserOrderDetail::select('id')->where('menu_id',$m['id'])->get()->toArray());
	}
	return view('menumanagement::v2_menu_item.index', compact('menuItems','rest_id','sort_type_id','sortAlgo','menu_category','cat_id','product_types'
		,'category_filter_by','product_type_filter_by','status_filter_by','type_filter_by'));
    }
    private function doMenuReSorting($rest_id,$sort_type_id,$locations){
	if($sort_type_id>0 ){  
		$del = DB::table('ma_menu_item_sort')->where('restaurant_id',$rest_id)->delete();
		$data = ['restaurant_id'=>$rest_id,'sort_type_id'=>$sort_type_id]; 
		 
		MenuItemSort::create($data);
	}
    }
    public function toggleMenuActive(Request $request)
    {
        $menuId = $request->input('menu_id');
        $status = $request->input('active_flag');

        $menuItem = MenuItem::find($menuId)->update(['status'=>$status]);
        if($menuItem) {
            $data = [
                'message' => 'Updated menu item succussfully.',
                'success' => true
            ];
        } else {
            $data = [
                'message' => 'Something went wrong. Please try again.',
                'success' => false
            ];
        }

        return response()->json($data);
    }			
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function getGroupRestDataList(){
	$groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
	return $groupRestData;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function getProductTypeList(){
	$product_types = ProductType::select('id','name')->where('status', 1)->whereNotIn('id', [1])->orderBy('sort_order', 'Asc')->get()->toArray(); 
	return $product_types;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function getRestaurantId(){
	$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
	return $locations;
    } 
    private function getMaCatList($locations){
	$menu_category = MenuCategory::select('id','name')->orderBy('id', 'Asc')->where('status', 1)->where('parent', 0)->whereIn('restaurant_id', $locations)->get()->toArray();
	foreach($menu_category as $index=>$attr){
		//$index=$attr['id'];
		$menu_category[$index]['menu_sub_category'] = MenuCategory::select('id','name')->orderBy('id', 'Asc')->where('status', 1)->where('parent', $attr['id'])->where('status', 1)->get()->toArray();
	}
	return $menu_category;
    }
    private function cateSubcatTree($locations,$parent_id = 0, $sub_mark = ''){
	     
	   $menu_category = MenuCategory::select('id','name')->orderBy('id', 'Asc')->where('status', 1)->where('parent', $parent_id)->whereIn('restaurant_id', $locations)->get()->toArray();
	    foreach($menu_category as $index=>$row){
		    echo '<option value="'.$row['id'].'">'.$sub_mark.$row['name'].'</option>';
		    $this->cateSubcatTree($row['id'], $sub_mark.'--');
	  }
     }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function getMaAttributeTypeList($ids=null){
	$ma_attribute = MaAttribute::select('id','backend_field_type')->orderBy('sort_order', 'Desc')->whereIn('id', $ids)->where('status', 1)->get()->toArray();
	foreach($ma_attribute as $attr){
		$index = $attr['id']; 
		$ma_attribute[$index] =  $attr['backend_field_type'];
	}
	return $ma_attribute;
    }	
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */	
    private function getMaAttributeList($locations,$type=['Modifier'],$ids=null,$is_customizable=null){
	$ma_attribute1 =null; 
	 
	$ma_attribute  = MaAttribute::whereIn('restaurant_id', $locations)->where('show_in_backend', 'Yes')->where('status', 1);
	$ma_attribute->whereIn('attribute_type', $type);
	if($ids!=null)$ma_attribute->whereIn('id', $ids);
	if($is_customizable!=null)$ma_attribute->where('is_customizable', $is_customizable);
	$ma_attribute = $ma_attribute->orderBy('sort_order', 'asc')->get()->toArray();  
	if(isset($ma_attribute)){ 
		foreach($ma_attribute as $index=>$attr){
	 		$ma_attribute1[$attr['id']] = $ma_attribute[$index];
			$ma_attribute1[$attr['id']]['attribute_id'] = $attr['id'];	
			$ma_attribute1[$attr['id']]['items'] = $this->getMaAttributeItemList($attr['id']);
		}
	}
	return $ma_attribute1;
    }
    private function getMaAttributeItemList($attribute_id,$attribute_item_parent_id=0){
	$ma_attribute1 =null;  
	$ma_attribute  =  MaAttributeItem::orderBy('sort_order', 'Desc')->where('attribute_item_parent_id',$attribute_item_parent_id)
		->where('attribute_id', $attribute_id)->where('status', 1)->get()->toArray();
	foreach($ma_attribute as $index=>$attr){
		 $ma_attribute1[$attr['id']] = $ma_attribute[$index];
		 $ma_attribute1[$attr['id']]['items'] = $this->getMaAttributeItemList($attribute_id,$attr['id']);
	} 	
	return $ma_attribute1;
    }	
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function getMaMenuItemList($locations,$parent_id=0,$ids=null,$notInids=null,$cat_id=0,$rest_id=0,
		$category_filter_by=0,$product_type_filter_by=0,$status_filter_by=0,$type_filter_by=0){
	  
	$sort = MenuItemSort::whereIn('restaurant_id',$locations)->get()->toArray();
	$menuItems = MenuItem::select('ma_product_type.name as product_type','ma_menu_item.*','ma_menu_item_price.price',
	'ma_menu_images.desktop_web_image as image','restaurants.restaurant_name','restaurants.currency_symbol','ma_menu_categories.name as cat_name')
		->whereIn('ma_menu_item.status', [1,0])->whereIn('ma_menu_item.restaurant_id', $locations)
		->leftJoin('restaurants', 'restaurants.id', '=', 'ma_menu_item.restaurant_id')
		->leftJoin('ma_menu_images', 'ma_menu_images.menu_item_id', '=', 'ma_menu_item.id')
		->leftJoin('ma_menu_categories', 'ma_menu_categories.id', '=', 'ma_menu_item.menu_categories_id')
		->leftJoin('ma_menu_item_price', 'ma_menu_item_price.menu_id', '=', 'ma_menu_item.id')	
		->leftJoin('ma_product_type', 'ma_menu_item.product_type_id', '=', 'ma_product_type.id');	
	//->where('parent_id',$parent_id);
	if($ids!=null)$menuItems->whereIn('ma_menu_item.id', $ids);
	if($ids!=null)$menuItems->whereIn('ma_menu_item.id', $ids);	
	if($cat_id>0)$menuItems->where('ma_menu_item.menu_categories_id', $cat_id);
	if($category_filter_by!=null && $category_filter_by>0)$menuItems->where('ma_menu_item.menu_categories_id', $category_filter_by);
	if($product_type_filter_by!=null && $product_type_filter_by > 0)$menuItems->where('ma_menu_item.product_type_id', $product_type_filter_by);
	if($status_filter_by!=null && $status_filter_by=='Active')$menuItems->where('ma_menu_item.status', 1);
	if($status_filter_by!=null && $status_filter_by=='Inactive')$menuItems->where('ma_menu_item.status', 0);
	if($type_filter_by!=null && ($type_filter_by=="Delivery" || $type_filter_by=="Takeout" )){
		//"Delivery" //Tekeout
		  
			      $sql  = "SELECT ma_attribute_item.id FROM `ma_attribute_item` left join ma_attribute on ma_attribute.id=ma_attribute_item.attribute_id where ma_attribute.attribute_code='".strtolower($type_filter_by)."' and ma_attribute.restaurant_id in(".implode(',',$locations).") and ma_attribute.status=1 and ma_attribute_item.attribute_item_name='Yes'";   
			$idsData = DB::select($sql);
			$d=null;   
	 		foreach ($idsData as $dat) {
				$d[]=$dat->id;
			}
			if(isset($d)){
				  $sql  = "SELECT menu_item_id FROM `ma_menu_item_attribute` where attribute_value in (".implode(',',$d).")"; 
				$idsData1 = DB::select($sql);
				$d1=null;   
		 		foreach ($idsData1 as $dat) {
					$d1[]=$dat->menu_item_id;
				} 
				if($d1!=null){ 
					//$ids1 = implode(',',$d1);
					$menuItems->whereIn('ma_menu_item.id',$d1);
				}	
			}
		 	
		
		 
		 
	}
	$menuItems->where('ma_menu_item.is_frontend_visible',1);
	if(isset($sort[0]) && $rest_id>0){
		if($sort[0]['sort_type_id']==1){// order active item
			$menuItems->orderBy('ma_menu_item.status', 'DESC');
		}elseif($sort[0]['sort_type_id']==2){// new item first
			$menuItems->orderBy('ma_menu_item.id', 'DESC');
		}elseif($sort[0]['sort_type_id']==3){// Bestseller products first
			$sql  = "SELECT count(id) as total, menu_id FROM `user_order_details` group by menu_id ORDER BY total desc"; 
			$idsData = DB::select($sql);
			$d=null;   
	 		foreach ($idsData as $dat) {
				$d[]=$dat->menu_id;
			}
			if($d!=null){ 
				$ids = implode(',',$d);
				$menuItems->orderByRaw(\DB::raw("FIELD(ma_menu_item.id, ".$ids." )"));
			}
		}elseif($sort[0]['sort_type_id']==4){// Biggest Saving products first
			$menuItems->orderBy('ma_menu_item_price.special_price', 'DESC');
		}elseif($sort[0]['sort_type_id']==5){// Most Viewed products first
			$menuItems->orderBy('ma_menu_item.most_viewed', 'DESC');
		}elseif($sort[0]['sort_type_id']==6){// ITem Name asc
			$menuItems->orderBy('ma_menu_item.item_name', 'Asc');
		}elseif($sort[0]['sort_type_id']==7){// ITem Name DESC
			$menuItems->orderBy('ma_menu_item.item_name', 'DESC');
		}elseif($sort[0]['sort_type_id']==8){// order active item price
			$menuItems->orderBy('ma_menu_item_price.price', 'ASC');
		}elseif($sort[0]['sort_type_id']==9){// order active item
			$menuItems->orderBy('ma_menu_item_price.price', 'DESC');
		}elseif($sort[0]['sort_type_id']==10){// order active item
			$menuItems->orderBy('ma_menu_item.sort_order', 'ASC');
		}elseif($sort[0]['sort_type_id']==11){// order active item
			$menuItems->orderBy('ma_menu_item.sort_order', 'DESC');
		}
	}else{
	  $menuItems->orderBy('ma_menu_item.id', 'DESC');
	}
 	//noofitem_sold		
	$menuItems = $menuItems->get();	
	return $menuItems;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */ 
    private function getMaAssociatedMenuItemList($locations,$parent_id){
	
	$menuItems = MenuItem::whereIn('ma_menu_item.status', [1,0])->whereIn('restaurant_id', $locations)
		     ->orderBy('id', 'DESC')->where('parent_id',$parent_id);
	 
	 
	$menuItems = $menuItems->get()->toArray();
	    
	foreach($menuItems as $index=>$attr){
		 $menuItems[$index]['items'] = MenuItemAttribute::where('menu_item_id', $attr['id'])->leftJoin('ma_attribute', 'ma_attribute.id', '=', 'ma_menu_item_attribute.attribute_id')->leftJoin('ma_attribute_item', 'ma_attribute_item.id', '=', 'ma_menu_item_attribute.attribute_value')->where('ma_attribute.is_customizable','Yes')->get()->toArray();
	} 
	 
	return $menuItems;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function getMaAssociatedAttributeFieldList($menu_item_id,$selectedIds=null,$type,$is_customizable=null){
	//echo $menu_item_id;
	$menu_item = MenuItem::select('customizable_attribute_ids')->where('id',$menu_item_id);
	
	$menu_item = $menu_item->get()->toArray();  
	$fieldName = null;
	if(isset($menu_item[0]['customizable_attribute_ids']) && $menu_item[0]['customizable_attribute_ids']!=''){
		 
		$ids = explode(',',$menu_item[0]['customizable_attribute_ids']);	 
		$ma_attribute  = MaAttribute::select('attribute_label','id')->whereIn('attribute_type', $type)->whereIn('id', $ids);
		if($is_customizable!=null){
			$ma_attribute->where('is_customizable',$is_customizable);
		}
		$ma_attribute = $ma_attribute->get()->toArray();  
		foreach($ma_attribute as $ar)$fieldName[$ar['id']]=$ar['attribute_label']; 
	}
	return $fieldName;
	
	 
    }		
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function getMenuCategory($locations){
	$cat = MenuCategory::select('id','name')->orderBy('id', 'Asc')->where('status', 1)->whereIn('restaurant_id', $locations)->get();
	return $cat;
    } 
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function getCategory(Request $request){
	$rest_id = $request->get('rest_id') ?? 0;	
	$menu_category = MenuCategory::select('id','name')->orderBy('id', 'Asc')->where('status', 1)->where('parent',0)->where('restaurant_id', $rest_id)
		->get()->toArray();
	return \Response::json(['dataObj'=>$menu_category], 200);
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */	
    public function getSubCategory(Request $request){
	$cat_id = $request->get('cat_id') ?? 0;
	$menu_category = MenuCategory::select('id','name')->orderBy('id', 'Asc')->where('status', 1)->where('parent', $cat_id)
		->get()->toArray();
	return \Response::json(['dataObj'=>$menu_category ], 200);
    }
    /**
     * File upload function
     * @param $image
     * @param $id
     * @return array
     */
    private function file_upload($image, $id,$prfix='',$folderType='attribute') {
        $imageRelPath = config('constants.image.rel_path.'.$folderType);
        $imagePath = config('constants.image.path.'.$folderType);

        $uniqId = uniqid(mt_rand());
        $imageName = $id .$prfix. '_large' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();
        $medImageName = $id .$prfix. '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
	$smallImageName = $id .$prfix. '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                . '_' . $uniqId . '_small' . '.' . $image->getClientOriginalExtension();
	$thumbnailImageName = $id .$prfix. '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                . '_' . $uniqId . '_thumbnail' . '.' . $image->getClientOriginalExtension();	

        if($image->getClientOriginalExtension() != 'svg'){
            $image = Image::make($image->getRealPath());
            $width = $image->width();
            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
            $image->resize((int) ($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'medium' . DIRECTORY_SEPARATOR . $medImageName);
	    $image->resize((int) ($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'small' . DIRECTORY_SEPARATOR . $smallImageName);
	    $image->resize((int) ($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $thumbnailImageName);		
        }
        else {
            // in case of SVGs upload files directly, as cur. ver. of Interverntion doesn't support SVG
            File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . $imageName);
            File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $thumbnailImageName);
	    File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . 'small' . DIRECTORY_SEPARATOR . $smallImageName);
	    File::copy($image->getRealPath(), $imagePath . DIRECTORY_SEPARATOR . 'medium' . DIRECTORY_SEPARATOR . $medImageName);
        }

        return [
            $imageRelPath . $imageName,
            $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
        ];
    }	
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function create()
    {
	$locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
	$menuAttributes =  MaAttribute::whereIn('restaurant_id', $locations)->where('status', 1)->where('attribute_type', 'General')->where('is_customizable', 'Yes');
	 
	$menuAttributes= $menuAttributes->orderBy('sort_order', 'Asc')->get()->toArray();
	$product_types = $this->getProductTypeList();
	$section_tab = 'attribute';
	$locations = $this->getRestaurantId();
	$attribute_list = $this->getMaAttributeList($locations); 
	$menuAttributeSet = MaAttributeSet::select('*')->whereIn('restaurant_id', $locations)->where('status', 1)->get()->toArray();
        return view('menumanagement::v2_menu_item.create', compact('product_types','section_tab','attribute_list','menuAttributeSet','menuAttributes'));
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function generalInfo(Request $request){
	$product_type_id = $request->input('product_type_id');
	$attribute_set_id = $request->input('attribute_set_id');
	//echo "<pre>";print_r($request);die;
	if(isset($product_type_id)){
		//$data = $request->all();
		$customizable_attribute_ids = $request->input('customizable_attribute_ids');
		// print_r($data);die;
		$customizable_attribute_ids = ($customizable_attribute_ids!=null && $customizable_attribute_ids!='')?implode(",",$customizable_attribute_ids):'';
		$groupRestData = $this->getGroupRestDataList();
		$locations =$this->getRestaurantId();
		//$menu_category = $this->getMenuCategory($locations);
		//if($product_type_id==2){
			//$generalAttributeIds = $request->input('attribute_ids');
			//$is_customizable = 'No';
		//}else{	
		$menuAttributeSet =  MaAttributeSet::select('general')->where('id', $attribute_set_id)->where('status', 1)->get()->toArray();
		if(isset($menuAttributeSet[0])) 
			 $generalAttributeIds = explode(',',$menuAttributeSet[0]['general']);
		else $generalAttributeIds = null;
		$is_customizable = 'No';
		//}
		//$is_customizable = ($product_type_id!=4 && $product_type_id!=5 && $product_type_id!=3)?'No':'No';	
		$ma_attribute = $this->getMaAttributeList($locations,['General'],$generalAttributeIds,$is_customizable); 
		$ma_attribute_system  = $this->getMaAttributeList($locations,['System'],null,null);
	 	$selected_rest_id = $locations[0];
		$menu_category = $this->getMaCatList($locations);
		$product_types =  $this->getProductTypeList();
		$section_tab = 'general';
		
		return view('menumanagement::v2_menu_item.generalinfo', compact('groupRestData','selected_rest_id','product_types', 'ma_attribute','section_tab','menu_category','ma_attribute_system','customizable_attribute_ids','product_type_id','attribute_set_id'));
	}else{
		return redirect()->back()->with('message', 'Please select product type!!!');
	}
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    private function createUpdateMaMenuItem($rest_id,$data,$menu_id=null){  
		$item_data = ['restaurant_id'=>$rest_id,
			'pos_id'=>$data['pos_id'],
			'menu_categories_id'=>$data['menu_categories_id'],
			'is_frontend_visible'=>$data['is_frontend_visible'],
			'item_name'=>$data['item_name'],	
			'status'=>$data['status'],
			'sort_order'=>$data['sort_order'],
			'attribute_set_id'=>$data['attribute_set_id'], 
			'product_type_id'=>$data['product_type_id'],
			'is_inventory'=>$data['is_inventory'],
			'sku'=>$data['sku'],
			'customizable_attribute_ids'=> ($data['customizable_attribute_ids'])?$data['customizable_attribute_ids']:'',
		];
        	if($menu_id==null){
			
			$m = MenuItem::create($item_data);
			$price_data = ['restaurant_id'=>$rest_id,'menu_id'=>$m->id,'sku'=>'','price'=>0,'special_price'=>0];
			$mp = MenuItemPrice::create($price_data); 
			return  ($m->id)?$m->id:null;
		}else{
			$item = MenuItem::where('id',$menu_id)->get()->toArray();
			$parent_id = isset($item[0]['parent_id'])?$item[0]['parent_id']:0;
			if($parent_id>0){  
				$parent = MenuItem::where('id',$parent_id)->get()->toArray(); 
				$item_data['customizable_attribute_ids']=$parent[0]['customizable_attribute_ids'];
			}
			 //print_r($item_data);
			// die;
			$m = MenuItem::where('id' , $menu_id)->update($item_data);
			return  $menu_id;
		}
	
    }	
    
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
	$data = $request->all();
	//echo "<pre>"; print_r($data);die; 
	//DB::beginTransaction();
	$locations =$this->getRestaurantId();
	//$restaurant_id = isset($data['restaurant_id'])?;
        $restaurant_id = isset($data['restaurant_id'][0])?$data['restaurant_id']:$locations;
	//print_r($restaurant_id);die;
	foreach($restaurant_id as $rest_id){
		$d[] = $data['menu_id']=$this->createUpdateMaMenuItem($rest_id,$data);
		if(isset($data['menu_id'])){
				$locations =$this->getRestaurantId();
				//$m = MenuItem::find($data['menu_id']);
			 
				foreach($data['attribute_ids'] as $attrId=>$val)
				$attribute_ids[]=$attrId;
				$attributeList = $this->getMaAttributeTypeList($attribute_ids);  
				//upload image
				$files = $request->file('attribute_ids');
				if($request->hasFile('attribute_ids'))
				{
				    foreach ($files as $fieldName=>$image) {
						if(isset($attributeList[$fieldName])){
						 	$imageRes = $this->file_upload($image, $data['menu_id'].'_'.$fieldName);
							//$imageArr['image_svg'] = $imageRes[0];
							$attrData = [
								'menu_item_id'=>$data['menu_id'],
								'attribute_id'=>$fieldName,
								'attribute_value'=>($imageRes[0])??'',
								'attribute_type'=>$attributeList[$fieldName] 
							];
							MenuItemAttribute::create($attrData);
						}
				    }
				}
				 
				// upload other data
				foreach($data['attribute_ids'] as $attrId=>$val){
				   if($attributeList[$attrId] =='Checkbox'){
					$val = implode(',',$val);
				   }
				   if($attributeList[$attrId] =='Multiple Select'){
					$val = implode(',',$val);  
				   }
				   if($attributeList[$attrId] !='File' && $attributeList[$attrId] !='Image'){	
				       if(isset($attributeList[$attrId])){	
						$attrData = [
							'menu_item_id'=>$data['menu_id'],
							'attribute_id'=>$attrId,
							'attribute_value'=>($val)??'',
							'attribute_type'=>$attributeList[$attrId] 
						];
						MenuItemAttribute::create($attrData);
					}
				   }//echo"<pre>";print_r($attrData);
				}//die;
				
		}else return redirect()->back()->with('message', 'Please try again!!!');
	}
	//if($data['is_inventory'] ==0)
	 return redirect('v2/menu/item/'.$d[0].'/addprice')->with('message', 'Menu Item added successfully');
	//else return redirect('v2/menu/item/'.$d[0].'/addinventory')->with('message', 'Menu Item added successfully');
    }
    private function getMenuDetails($menu_id){
	 
	$sql = 'SELECT ma_menu_item_attribute.attribute_value,ma_attribute.attribute_code as field_name,ma_attribute.attribute_label,ma_attribute.backend_field_type,ma_attribute.attribute_prompt,ma_attribute.id as attribute_id, ma_menu_item.* from ma_menu_item left join (`ma_menu_item_attribute` left join ma_attribute on ma_menu_item_attribute.attribute_id=ma_attribute.id) on ma_menu_item.id=ma_menu_item_attribute.menu_item_id where  ma_menu_item.id='.$menu_id;
	$menu_item = DB::select($sql) ;
        //echo"<pre>";print_r($menu_item); die;
	$menu1 =null; 
	if (count($menu_item)) {
	    $menu = $menu_item[0];	 
	    $menu1['static']['id'] = $menu_id;
	    	        $menu1['static']['item_name'] = (isset($menu->item_name))?$menu->item_name:'';
			$menu1['static']['status'] = (isset($menu->status))?$menu->status:'';
			$menu1['static']['sort_order'] = (isset($menu->sort_order))?$menu->sort_order:'';
			$menu1['static']['product_type_id'] = (isset($menu->product_type_id))?$menu->product_type_id:0;
			$menu1['static']['attribute_set_id'] = (isset($menu->attribute_set_id))?$menu->attribute_set_id:'';
			$menu1['static']['menu_categories_id'] = (isset($menu->menu_categories_id))?$menu->menu_categories_id:'';
			$menu1['static']['restaurant_id'] = (isset($menu->restaurant_id))?$menu->restaurant_id:'';
			$menu1['static']['parent_id'] = (isset($menu->parent_id))?$menu->parent_id:'0';
			$menu1['static']['sku'] = (isset($menu->sku))?$menu->sku:'';
			$menu1['static']['is_byp'] = (isset($menu->is_byp))?$menu->is_byp:0;
			$menu1['static']['pos_id'] = (isset($menu->pos_id))?$menu->pos_id:'';
			$menu1['static']['item_price'] = (isset($menu->item_price))?$menu->item_price:'';
			$menu1['static']['customizable_attribute_ids'] = (isset($menu->customizable_attribute_ids))?$menu->customizable_attribute_ids:'';
			$menu1['static']['associated_product'] = (isset($menu->associated_product))?$menu->associated_product:'';
			$menu1['static']['is_frontend_visible'] = (isset($menu->is_frontend_visible))?$menu->is_frontend_visible:'';
			$menu1['static']['is_inventory'] = (isset($menu->is_inventory))?$menu->is_inventory:'';
			$menu1['static']['customizable_attribute_ids'] = (isset($menu->is_inventory))?$menu->customizable_attribute_ids:'';
            foreach ($menu_item as $index => $menu) {  
		//if($menu->attribute_id==''){
			
		//}else{
			$menu1['field_type'] = $menu->backend_field_type;
			if($menu1['field_type']=='Dropdown' || $menu1['field_type']=='Radio' || $menu1['field_type']=='Checkbox' ){
				$attributeItem = MaAttributeItem::select('attribute_item_name')->whereIn('id', explode(',',$menu->attribute_value))->get()->toArray(); 
			  	if (isset($attributeItem[0])) { 
					//$menu1['dynamic'][$menu->attribute_id][$menu->field_name] = $attributeItem[0]['attribute_item_name'];
					//$menu1['dynamic'][$menu->attribute_id] = $attributeItem[0]['attribute_item_name'];
					$menu1['dynamic'][$menu->attribute_id] = explode(',',$menu->attribute_value);
				}else {
					$menu1['dynamic'][$menu->attribute_id] = $menu->attribute_value;
				}   
			}if($menu1['field_type']=='Multiple Select'){
				$attributeItem = MaAttributeItem::select('attribute_item_name','attribute_id','id')->whereIn('id', explode(',',$menu->attribute_value))->get()->toArray(); 
			  	if (isset($attributeItem[0])) { 
					$a = explode(',',$menu->attribute_value); 
					foreach($a as $k)
					$menu1['dynamic'][$menu->attribute_id][$k] = $k;//$attributeItem[0]['id'];//explode(',',$menu->attribute_value);
				}else {
					$menu1['dynamic'][$menu->attribute_id] = $menu->attribute_value;
				}  
			}else{
				$menu1['dynamic'][$menu->attribute_id] = $menu->attribute_value;
			}
			
		 //}
            }
        }
	//echo"<pre>";print_r($menu1); die;
	return $menu1;
     }
     public function updateItemType(Request $request,$menu_id){
	$data = $request->all();
	//print_r($data);die;
	$m = MenuItem::find($menu_id); 
	if(isset($m->id)){
		 $item_data['customizable_attribute_ids']=implode(',',$data['attribute_ids']);	
		 $m1 = MenuItem::where('id' , $menu_id)->update($item_data);
		 
		return redirect('v2/menu/item/'.$menu_id.'/edit-item')->with('message', 'Updated successfully!!!');	 
	}else{
		return redirect()->back()->with('message', 'Please try again!!!');
	}
     }	
     public function editItem(Request $request,$menu_id){
	$m = MenuItem::find($menu_id); 
	if(isset($m->id)){ 
		$product_type_id = $m->product_type_id;
	 	$attribute_ids = $m->attribute_ids;
		$is_inventory = $m->is_inventory;
		$attribute_ids = ($attribute_ids!=null && $attribute_ids!='')?implode(",",$attribute_ids):'';
		$product_types =  $this->getProductTypeList();
		$section_tab = 'type';
		$menu_data = $this->getMenuDetails($menu_id);
		$menuAttributeSet = MaAttributeSet::select('*')->whereIn('restaurant_id', [$m->restaurant_id])->where('status', 1)->get()->toArray();
		// print_r($menu_data);die;
		return view('menumanagement::v2_menu_item.edit-item', compact('section_tab','attribute_ids','product_type_id','product_types','menu_data','menu_id','is_inventory','menuAttributeSet'));
	}else{
		return redirect()->back()->with('message', 'Please select product type!!!');
	}
    }	
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function editGeneralInfo(Request $request,$menu_id){
	$m = MenuItem::find($menu_id); 
	if(isset($m->id)){ 
		$attribute_set_id=$m->attribute_set_id;
		$product_type_id = $m->product_type_id;
		$is_inventory = $m->is_inventory;
	 	$attribute_ids = $m->customizable_attribute_ids;
		$attribute_ids = ($attribute_ids!=null && $attribute_ids!='')?explode(",",$attribute_ids):'';
		$groupRestData = $this->getGroupRestDataList();
		$locations =$this->getRestaurantId();
		//$menu_category = $this->getMenuCategory($locations);
		//$is_customizable = ($product_type_id!=4 && $product_type_id!=5 && $product_type_id!=3)?'No':'No';
		$menuAttributeSet =  MaAttributeSet::select('general')->where('id', $attribute_set_id)->where('status', 1)->get()->toArray();
		if(isset($menuAttributeSet[0])) 
			 $generalAttributeIds = explode(',',$menuAttributeSet[0]['general']);
		else $generalAttributeIds = null;
		$is_customizable = 'No';
		$ma_attribute = $this->getMaAttributeList($locations,['General'],$generalAttributeIds,$is_customizable);
		 
		$ma_attribute_system  = $this->getMaAttributeList($locations,['System'],null,null);
	 	$selected_rest_id = $locations[0];
		$menu_category = $this->getMaCatList($locations);
		$product_types =  $this->getProductTypeList();
		$section_tab = 'general';
		$menu_data = $this->getMenuDetails($menu_id);
		//echo "<pre>";print_r($menu_data);die;
		return view('menumanagement::v2_menu_item.generalinfo-edit', compact('groupRestData','selected_rest_id','product_types', 'ma_attribute','section_tab','menu_category','ma_attribute_system','attribute_ids','product_type_id','menu_data','is_inventory','menu_id'));
	}else{
		return redirect()->back()->with('message', 'Please select product type!!!');
	}
    }
    public function updateGeneralInfo(Request $request,$menu_id)
    {
	$data = $request->all();
	
	//DB::beginTransaction();
	$locations =$this->getRestaurantId();
	
	$data['menu_id']=$this->createUpdateMaMenuItem($locations[0],$data,$menu_id);
	//echo "<pre>";
	//print_r($data);
	
	if(isset($data['menu_id'])){
			$locations =$this->getRestaurantId();
			//$m = MenuItem::find($data['menu_id']);
		        
		        foreach($data['attribute_ids'] as $attrId=>$val)
			$attribute_ids[]=$attrId;
			$attributeList = $this->getMaAttributeTypeList($attribute_ids);  
			//upload image
			$files = $request->file('attribute_ids');
			if($request->hasFile('attribute_ids'))
			{
			    foreach ($files as $fieldName=>$image) {
					if(isset($attributeList[$fieldName])){
					 	$imageRes = $this->file_upload($image, $data['menu_id'].'_'.$fieldName);
						//$imageArr['image_svg'] = $imageRes[0];
						$attrData = [
							'menu_item_id'=>$data['menu_id'],
							'attribute_id'=>$fieldName,
							'attribute_value'=>($imageRes[0])??'',
							'attribute_type'=>$attributeList[$fieldName] 
						];
						$exist = MenuItemAttribute::where('menu_item_id',$data['menu_id'])->where('attribute_id',$fieldName)->get()->toArray(); 
						if(isset($exist[0]['id'])) 
						     $ma = MenuItemAttribute::where('id' , $exist[0]['id'])->update($attrData);
						else MenuItemAttribute::create($attrData);
					}
			    }
			}
			 
			// upload other data
			foreach($data['attribute_ids'] as $attrId=>$val){
			   if($attributeList[$attrId] =='Checkbox'){
				$val = implode(',',$val);
			   }
			   if($attributeList[$attrId] =='Multiple Select'){
				$val = implode(',',$val); print_r($val); 
			   }
			   if($attributeList[$attrId] !='File' && $attributeList[$attrId] !='Image'){	
			       if(isset($attributeList[$attrId])){	
					$attrData = [
						'menu_item_id'=>$data['menu_id'],
						'attribute_id'=>$attrId,
						'attribute_value'=> ($val)??'',
						'attribute_type'=>$attributeList[$attrId] 
					];
					$exist = MenuItemAttribute::where('menu_item_id',$data['menu_id'])->where('attribute_id',$attrId)->get()->toArray(); 
					//print_r($exist);
					if(isset($exist[0]['id'])) 
						     $ma = MenuItemAttribute::where('id' , $exist[0]['id'])->update($attrData);
					else MenuItemAttribute::create($attrData); 
				}//die;
			   }//echo"<pre>";print_r($attrData);
			}//die;
			return redirect()->back()->with('message', 'Menu Item updated successfully');
			
	}else return redirect()->back()->with('message', 'Please try again!!!');
    }	
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function attributeInfo(Request $request){
	$groupRestData = $this->getGroupRestDataList();
        $locations =$this->getRestaurantId();
	$menu_category = $this->getMenuCategory($locations);
	$ma_attribute = $this->getMaAttributeList($locations);
        $selected_rest_id = $locations[0];
        $product_types =  $this->getProductTypeList();
	$section_tab = 'associated';
        return view('menumanagement::v2_menu_item.attributeinfo', compact('section_tab','selected_rest_id','product_types', 'ma_attribute','section_tab','menu_category'));
    }
    
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function listImages(Request $request,$menu_id){
	$m = MenuItem::find($menu_id); 
	if(isset($m->id)){ 
		$section_tab = 'images';
		$is_inventory= $m->is_inventory;
		$product_type_id = $m->product_type_id;
		$menu_images = MenuItemImages::where('menu_item_id',$menu_id)->get()->toArray();
		if(isset($menu_inventory[0]))$menu_inventory=$menu_inventory[0];
        	return view('menumanagement::v2_menu_item.images', compact('section_tab','menu_id','is_inventory','product_type_id','menu_images'));
	}else{
		 return redirect()->back()->with('message', 'Please try again!!!');
	}
    }
    
    
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addUpdateImageInMenuItem(Request $request,$menu_id)
    {
	/*$files = $request->file('image1'); print_r($files);die;
	$filename = $_POST['filename']; print_r($filename);
	$img = $_POST['pngimageData']; print_r($img);die;
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	file_put_contents($filename, $data);*/
	$dataArray = $request->all();
	//$imgid= $dataArray['imgid']; 
	$m = MenuItem::find($menu_id); 
	if(isset($m->id)){
		$im= MenuItemImages::where('menu_item_id',$menu_id)->get()->toArray(); 
		if(isset($im[0]['id'])){
			$imm= MenuItemImages::find($im[0]['id']);
			$imm->delete();
    		}
		$files = $request->file('mobile_app_image');
		if($request->hasFile('mobile_app_image')){
			    foreach ($files as $fieldName=>$image) {
				 $imageRes = $this->file_upload($image, $menu_id.'_'.$fieldName,'mobile_app_images','productimage');
				 //$imageArr['image_svg'] = $imageRes[0];
				 $data['menu_item_id']=$menu_id;
				 $data['mobile_app_image']=$imageRes[0] ;
			    }
			     
		}
		$files = $request->file('desktop_web_image');
		if($request->hasFile('desktop_web_image')){
			    foreach ($files as $fieldName=>$image) {
				 $imageRes = $this->file_upload($image, $menu_id.'_'.$fieldName,'desktop_web_images','productimage');
				 //$imageArr['image_svg'] = $imageRes[0];
				 $data['menu_item_id']=$menu_id;
				 $data['desktop_web_image']=$imageRes[0] ;
			    }
			     
		}
		$files = $request->file('mobile_web_image');
		if($request->hasFile('mobile_web_image')){
			    foreach ($files as $fieldName=>$image) {
				 $imageRes = $this->file_upload($image, $menu_id.'_'.$fieldName,'mobile_web_image','productimage');
				 //$imageArr['image_svg'] = $imageRes[0];
				  $data['menu_item_id']=$menu_id;
				 $data['mobile_web_image']=$imageRes[0] ;
			   }
			     
		}
		//echo "<pre>";print_r($data);die;
		$m = MenuItemImages::create($data);
		 
		 
		return redirect('/v2/menu/item/images/'.$menu_id.'/list')->with('message', 'Images updated successfully!!!');	 
	}else{
		return redirect()->back()->with('message', 'Please try again!!!');
	}
		
    }
    
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addInventoryInfo(Request $request,$menu_id){
	$m = MenuItem::find($menu_id); 
	if(isset($m->id)){ 
		$section_tab = 'inventory';
		$is_inventory= $m->is_inventory;
		$product_type_id = $m->product_type_id;
		$menu_inventory = MenuItemInventory::where('menu_item_id',$menu_id)->get()->toArray();
		if(isset($menu_inventory[0]))$menu_inventory=$menu_inventory[0];
        	return view('menumanagement::v2_menu_item.inventory', compact('section_tab','menu_id','is_inventory','product_type_id','menu_inventory'));
	}else{
		 return redirect()->back()->with('message', 'Please try again!!!');
	}
    }
    
    
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addUpdateInventory(Request $request,$menu_id)
    {
	
	$data = $request->all();
	unset($data['_token']);
	$id= $data['id'];
	unset($data['id']);	
	//print_r($data);die;
	$m = MenuItem::find($menu_id); 
	if(isset($m->id)){
		
		$data['restaurant_id']= $m->restaurant_id;
		$data['meal_type_id']= 0;
		$data['menu_item_id']= 	$m->id;
		if($id>0) $mp = MenuItemInventory::where('id' , $id)->update($data);
		else  $m = MenuItemInventory::create($data);
		 
		return redirect('v2/menu/item/'.$menu_id.'/addinventory')->with('message', 'Inventory updated successfully!!!');	 
	}else{
		return redirect()->back()->with('message', 'Please try again!!!');
	}
		
    }	

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addUpdatePrice(Request $request,$menu_id)
    {
	$data = $request->all();
	unset($data['_token']);
	$id= $data['id'];
	unset($data['id']);
	$m = MenuItem::find($menu_id);  
	if(isset($m->id)){
		if($id>0) $mp = MenuItemPrice::where('id' , $id)->update($data);
		else  $mp = MenuItemPrice::create($data); 
		 return redirect('v2/menu/item/availability/'.$data['menu_id'].'/list')->with('message', 'Menu Item Price added/updated successfully');	
		/*if($m->product_type_id==5){
			return redirect('v2/menu/item/modifiergroup/'.$data['menu_id'].'/list')->with('message', 'Menu Item Price added successfully');
		} else{
			return redirect('v2/menu/item/associatedproduct/'.$data['menu_id'].'/list')->with('message', 'Menu Item Price added successfully');
		} */
		 
	}else{
		return redirect()->back()->with('message', 'Please try again!!!');
	}
		
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addPrice(Request $request,$menu_id){
	$locations =$this->getRestaurantId();
	$m = MenuItem::find($menu_id);
	if($m->id){
		$menu_price = MenuItemPrice::where('menu_id',$menu_id)->get()->toArray();
		if(isset($menu_price[0]))$menu_price=$menu_price[0];
		//print_r($menu_price);die;
		$is_inventory= $m->is_inventory;
		$product_type_id = $m->product_type_id;
		$section_tab = 'price';
        	return view('menumanagement::v2_menu_item.priceinfo', compact('menu_id','section_tab','is_inventory','product_type_id','menu_price'));
	}else redirect('v2/menu/item/create')->with('message', 'Please try again!!');	
    } 
   
    
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */   
    public function listAssociatedProduct(Request $request, $menu_id){
	$menu_data = MenuItem::find($menu_id);
	if($menu_data->id && $menu_data->customizable_attribute_ids!=''){ 
		$attributeIds = explode(',',$menu_data->customizable_attribute_ids); 
		$product_type_id = $menu_data->product_type_id;  
		$section_tab = 'associated';
		$groupRestData = $this->getGroupRestDataList();
		$locations =$this->getRestaurantId();
		$is_customizable = ($product_type_id!=4 && $product_type_id!=5 && $product_type_id!=3)?'Yes':'No'; 
		$ma_attribute = $this->getMaAttributeList($locations,['General'],$attributeIds,$is_customizable);  
		$ma_attribute_system  = null;//$this->getMaAttributeList($locations,['System']);
	 	$selected_rest_id = $locations[0];
		$menu_category = $this->getMaCatList($locations);
		$product_types =  $this->getProductTypeList();
		$is_inventory= $menu_data->is_inventory;
		
		$id =   $menu_data->id;
		$ma_attribute_field_name = $this->getMaAssociatedAttributeFieldList($menu_id,$attributeIds,['General'],$is_customizable);
		$menu_items = $this->getMaAssociatedMenuItemList($locations,$menu_id);  
		//echo "<pre>";print_r($ma_attribute_field_name); die;//print_r($menu_items);die;
		//echo "<pre>";print_r($ma_attribute);die;
		return view('menumanagement::v2_menu_item.listassociateditem', compact('groupRestData','selected_rest_id','product_types', 'ma_attribute','section_tab','menu_category','menu_id','ma_attribute_system','menu_items','ma_attribute_field_name','is_inventory','product_type_id','menu_data'));
		 
	}return redirect()->back()->with('message', 'Invalid Request');
    }	
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */   
    public function addAssociatedProduct(Request $request, $menu_id){
	$data = $request->all();  
	$m = MenuItem::find($menu_id);
	 //print_r($data);die;
	 
	if($m->id){ 
		$mpdata = ['price'=>$data['price'],'special_price'=>$data['special_price'],'special_price_start_date'=>$data['special_price_start_date'],
				'special_price_end_date'=>$data['special_price_end_date']];
		$attribute_ids = isset($data['attribute_ids'])?$data['attribute_ids']:null; 
		unset($data['_token']);
		unset($data['special_price']);
		unset($data['price']);
		unset($data['special_price_end_date']);
		unset($data['special_price_start_date']);

		unset($data['attribute_ids']);
		unset($data['id']);
		unset($data['menu_id']);
		$simpledata = ['status'=>$data['status'],'associated_product'=>'','customizable_attribute_ids'=>'','parent_id'=>$menu_id,'is_frontend_visible'=>0,
				'product_type_id'=>$data['product_type_id'],'menu_categories_id'=>$m->menu_categories_id,'item_name'=>$data['item_name'],
				'restaurant_id'=>$m->restaurant_id,'pos_id'=>$data['pos_id'],'is_frontend_visible'=>$data['is_frontend_visible'],'sku'=>$data['sku']
				,'attribute_set_id'=>$m->attribute_set_id]; 
		$m1 = MenuItem::create($simpledata);
		//add price for item
		$mpdata['menu_id'] =$m1->id; 	
		$mp = MenuItemPrice::create($mpdata);
		if($m->associated_product!='')
		$associated_product = explode(',',$m->associated_product);
		$associated_product[] = $m1->id;
		MenuItem::where('id', $m->id)->update(['associated_product'=>implode(',',$associated_product)]);
		if(isset($attribute_ids)){
			foreach($attribute_ids as $attrId=>$val)
			$attribute_idsList[]=$attrId;
			$attributeList = $this->getMaAttributeTypeList($attribute_idsList);
			foreach($attribute_ids as $attrId=>$val){
			   if($attributeList[$attrId] !='File'){		
				$attrData = [
					'menu_item_id'=>$m1->id,
					'attribute_id'=>$attrId,
					'attribute_value'=>($val)??'',
					'attribute_type'=>$attributeList[$attrId] 
				];
				MenuItemAttribute::create($attrData);
			   }
			}
		}
		/// clone system attribute data from parent to child product
		//$ma_attribute_system  = $this->getMaAttributeList([$m->restaurant_id],['System']);
		 
		//foreach($ma_attribute_system as $ids)$ids_arr[]=$ids['id'];	
		$parentAttribute = MenuItemAttribute::where('menu_item_id',$m->id)->get()->toArray();
		foreach($parentAttribute as $attr_item){
			$attrData = [
				'menu_item_id'=>$m1->id,
				'attribute_id'=>$attr_item['attribute_id'],
				'attribute_value'=>$attr_item['attribute_value'],
				'attribute_type'=>$attr_item['attribute_type'] 
			];
			MenuItemAttribute::create($attrData);
		}
		return redirect()->back()->with('message', 'Associated item created successfully!!!'); 
	}return redirect()->back()->with('err_msg', 'Invalid Request');
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */   
    public function listModifiers(Request $request, $menu_id){
	$menu_data = MenuItem::find($menu_id);
	if($menu_data->id){ 
		//if($m->product_type_id!=5){	
			$type = ['Modifier'];
			if($menu_data->attribute_set_id!=''){ 
			$menuAttributeSet = MaAttributeSet::select('*')->where('id', $menu_data->attribute_set_id)->where('status', 1)->get()->toArray();
				$ModifierGroupFieldList = (isset($menuAttributeSet[0]['system_modifier']) && $menuAttributeSet[0]['system_modifier']!="")?explode(",",$menuAttributeSet[0]['system_modifier']):null;
				$ModifierItemFieldList = (isset($menuAttributeSet[0]['system_modifier_item']) && $menuAttributeSet[0]['system_modifier_item']!="")?explode(",",$menuAttributeSet[0]['system_modifier_item']):null;
			}else{
				$ModifierGroupFieldList=$ModifierItemFieldList=null;
			} 
			$is_inventory= $menu_data->is_inventory;
			$product_type_id = $menu_data->product_type_id;
			$locations =$this->getRestaurantId();
			$attribute_ids = explode(',',$menu_data->customizable_attribute_ids);  
			$modifier_group_data = $this->getMaAttributeList($locations,$type); //to open modifier group form
			
			$ma_attribute_system_modifier =  $this->getMaAttributeList($locations,['SystemModifier'],$ModifierGroupFieldList);
			$ma_attribute_system_modifier_item =  $this->getMaAttributeList($locations,['SystemModifierItem'],$ModifierItemFieldList);
			$section_tab = 'modifiers'; 
	
			$item_modifier_group =  $this->getItemModifierGroupList($menu_id);
			 
			return view('menumanagement::v2_menu_item.modifierinfo', compact('section_tab','menu_id',
			'modifier_group_data','item_modifier_group','ma_attribute_system_modifier',
			'ma_attribute_system_modifier_item','is_inventory','product_type_id','menu_data'));
		//}else return redirect('v2/menu/item')->with('message', 'Product created successfully!!');	
	}return redirect()->back()->with('err_msg', 'Invalid Request');
    }
    public function getModifierItems($group_id,$parent_id=0,$modifier_group_item_id=0) {
        $modifierData = ItemModifierGroupItem::select('ma_modifier_group_items.*','ma_modifier_group_items_attribute.is_primary_field','ma_modifier_group_items_attribute.attribute_id'
			,'ma_modifier_group_items_attribute.attribute_value','ma_attribute.attribute_image','ma_attribute.attribute_image_class','ma_modifier_group_items_attribute.attribute_type'
			,'ma_attribute.attribute_code as field_name','ma_attribute.attribute_description','ma_attribute.attribute_prompt','ma_attribute.is_customizable')
			->leftJoin('ma_modifier_group_items_attribute', 'ma_modifier_group_items_attribute.modifier_group_item_id', '=', 'ma_modifier_group_items.id')	
			->leftJoin('ma_attribute', 'ma_modifier_group_items_attribute.attribute_id', '=', 'ma_attribute.id');
			if($modifier_group_item_id<1){
			if($group_id!=null)$modifierData->where('ma_modifier_group_items.modifier_group_id', '=', $group_id);
				$modifierData->where('ma_modifier_group_items.parent_id', '=', $parent_id);
			}else{
				$modifierData->where('ma_modifier_group_items.id', '=', $modifier_group_item_id);
			}
	$modifierData =  $modifierData->get()->toArray();
	//print_r($modifierData); die;
	$m1=[];
        if (count($modifierData)) {
            foreach ($modifierData as $index => $grp) { 
			//print_r($grp);
			$m[$grp['id']][$index]=$grp;
			$m2[$index][$grp['attribute_id']] = ['value'=>$grp['attribute_value'],'type'=>$grp['attribute_type']];
			//$m2[$index][$grp['attribute_id']]['data'][$grp['attribute_id']] = $grp['attribute_value'];
			//$m[$grp['id']][$index]['attribute_item_child_count'] = 0;
			
			if($grp['is_primary_field']==1){
				$m[$grp['id']][$index]['attribute_item_id'] = $grp['attribute_id'];
				$m[$grp['id']][$index]['attribute_item_id_value'] = $grp['attribute_value'];
				$modifierGroupDataItem = MaAttributeItem::select('attribute_item_name','attribute_item_image','attribute_item_image_class',
					'attribute_item_description','attribute_item_parent_id')->whereIn('id', explode(',',$grp['attribute_value']))->get()->toArray(); 
				$m[$grp['id']][$index]['attribute_item_child_count'] = MaAttributeItem::select('*')
											->whereIn('attribute_item_parent_id', explode(',',$grp['attribute_value']))->get()->count(); 
			  	if (isset($modifierGroupDataItem[0])) {
					$m[$grp['id']][$index]['modifier_name'] = $modifierGroupDataItem[0]['attribute_item_name'];
					$m[$grp['id']][$index]['image'] =  $modifierGroupDataItem[0]['attribute_item_image'];
			 		$m[$grp['id']][$index]['class'] = $modifierGroupDataItem[0]['attribute_item_image_class'];
					$m[$grp['id']][$index]['description'] = $modifierGroupDataItem[0]['attribute_item_description'];
				}else {
					$m[$grp['id']][$index]['modifier_name'] = $grp['attribute_value'];
				}  
			 }else if($grp['attribute_type']=='Checkbox' || $grp['attribute_type']=='Dropdown'){
				$modifierGroupDataItem = MaAttributeItem::select('attribute_item_name')->whereIn('id', explode(',',$grp['attribute_value']))->get()->toArray(); 
			  	if (isset($modifierGroupDataItem[0])) { 
					if($modifierGroupDataItem[0]['attribute_item_name']=="Yes")$m[$grp['id']][$index][$grp['field_name']] = 1; 
					else if($modifierGroupDataItem[0]['attribute_item_name']=="No")$m[$grp['id']][$index][$grp['field_name']] = 0;
					else $m[$grp['id']][$index][$grp['field_name']] = $modifierGroupDataItem[0]['attribute_item_name'];
					$m[$grp['id']][$index]['d'.$grp['attribute_id']] = $modifierGroupDataItem[0]['attribute_item_name'];
				}else {
					 $m[$grp['id']][$index][$grp['field_name']] = $grp['attribute_value'];
					 $m[$grp['id']][$index]['d'.$grp['attribute_id']] = $grp['attribute_value'];
				}  
				 
			 }else if($grp['attribute_type']=='Radio'){
				$modifierGroupDataItem = MaAttributeItem::select('attribute_item_name')->whereIn('id', explode(',',$grp['attribute_value']))->get()->toArray(); 
			  	if (isset($modifierGroupDataItem[0])) { 
					 
					$m[$grp['id']][$index][$grp['field_name']] = ($modifierGroupDataItem[0]['attribute_item_name']=="Yes")?1:0;
				}else {
					 $m[$grp['id']][$index][$grp['field_name']] = $grp['attribute_value'];
				}  
				 
			 }else{
				$m[$grp['id']][$index][$grp['field_name']] = ($grp['attribute_type']=="Float")?(int)$grp['attribute_value']:$grp['attribute_value'];
				$m[$grp['id']][$index]['d'.$grp['attribute_id']] = $grp['attribute_value'];
				
			 }
			 
			  
			 $m[$grp['id']][$index]['modifier_group_id'] = (int) $grp['modifier_group_id']; 
			  
			
			 $m[$grp['id']][$index]['is_customizable'] = $grp['is_customizable'];
			 $m[$grp['id']][$index]['status'] = $grp['status'];
			 $m[$grp['id']][$index]['sort_order'] = $grp['sort_order'];//print_r($m); 
			 $m[$grp['id']][$index]['children'] =  $this->getModifierItems(null,$grp['id']);
			 $m[$grp['id']][$index]['children_id'] =$grp['id'];
			 
		} //print_r($m); die;
		if(isset($m)){
			$i=0;	
			foreach($m as $ar1){
				foreach($ar1 as $k=>$ar2){
					foreach($ar2 as $k2=>$val){
						$m1[$i][$k2]=$val;
						
					}$m1[$i]['attribute_ids']=$m2;
				}$i++;
			}
		}
        } 
        return isset($m1)?$m1:null;
    }	
    public function getModifierItemInfo(Request $request,$id){
	$item_modifier_group =  $this->getModifierItems(0,0,$id); 
	if(isset($item_modifier_group[0]))$item_modifier_group=$item_modifier_group[0];
	else $item_modifier_group=null;
	return \Response::json(['data'=>$item_modifier_group,'error'=>0], 200);
    }
    	
    public function destroyModifierItem($id) {
        DB::beginTransaction();
        try {
            ItemModifierGroupItem::where('id', $id)->delete();
	    ItemModifierGroupItem::where('parent_id', $id)->delete();	
	    ItemModifierGroupItemAttribute::where('modifier_group_item_id', $id)->delete();	
            DB::commit();
            $data = [
                'status' => true,
                'message' => '  Item has been deleted successfully',
                'data' => []
            ];
            return $data;// back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return $data;//back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */ 
    public function listModifierItem(Request $request,$modifier_id){
	$item_modifier_group =  $this->getModifierItems($modifier_id,0); //print_r($item_modifier_group);
	return \Response::json(['data'=>$item_modifier_group], 200);
    } 
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */ 
    public function getModifierListById(Request $request,$id){
	$type = ['Modifier'];
	$locations =$this->getRestaurantId();
	$menuAttributes = $this->getMaAttributeList($locations,$type,[$id]); 
	return \Response::json(['list'=>$menuAttributes], 200);
    }
	
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */ 
    public function destroy(Request $request,$id) {
        DB::beginTransaction();
        try {

	    $menuItemObj =  MenuItem::find($id);
	    DB::table('ma_menu_item_attribute')->where('menu_item_id', $id)->delete();  	
	    DB::table('ma_menu_item')->where('parent_id', $id)->delete();   
	    $menuItemObj->delete();

            DB::commit();
 	    return Redirect::back()->with('message','Menu Item deleted successfully');


        }
        catch (\Exception $e){
            DB::rollback();
               return Redirect::back()->with('message','Please try again!!');
        }
     }
     
     public function getModifiersGroupV5($menu_id) {
	$sql = 'SELECT ma_modifier_group.menu_item_id, ma_attribute.attribute_code  as field_name,ma_attribute.attribute_label,ma_attribute.backend_field_type,ma_attribute.attribute_prompt,ma_modifier_group_attribute.is_primary_field,ma_modifier_group_attribute.attribute_id,attribute_value,ma_attribute.attribute_type,ma_modifier_group.id,ma_modifier_group.sort_order,ma_modifier_group.status FROM `ma_modifier_group` left join (`ma_modifier_group_attribute` left join ma_attribute on ma_modifier_group_attribute.attribute_id=ma_attribute.id) on ma_modifier_group_attribute.modifier_group_id=ma_modifier_group.id where ma_modifier_group.status=1 and ma_modifier_group.menu_item_id='.$menu_id. ' order by ma_modifier_group.sort_order asc';
	$modifierGroupData = DB::select($sql) ;
       // echo"<pre>"; print_r($modifierGroupData);die;	
	$grp1 =null; 
	if (count($modifierGroupData)) {
            foreach ($modifierGroupData as $index => $grp) {  
		unset($grp1);
		$grp1['id'] = $grp->id;
		
		$grp1['field_type'] = $grp->backend_field_type;
		if($grp1['field_type']=='Dropdown' || $grp1['field_type']=='Radio' || $grp1['field_type']=='Checkbox' || $grp1['field_type']=='Multiple Select'){
			$modifierGroupDataItem = MaAttributeItem::select('attribute_item_name')->whereIn('id', explode(',',$grp->attribute_value))->get()->toArray(); 
		  	if (isset($modifierGroupDataItem[0])) { 
				$grp1[$grp->field_name] = $modifierGroupDataItem[0]['attribute_item_name'];
			}else {
				$grp1[$grp->field_name] = $grp->attribute_value;
			}   
		}else{
			$grp1[$grp->field_name] = $grp->attribute_value;
		}
		 
		$modifierGroupData[$index]=$grp1;
                $modifierItems = $this->getModifierItemsV5($grp1['id'],0);
		$modifierGroupData[$index]['modifier_items_list'] = $modifierItems;
            }
        }
	//echo"<pre>";print_r($modifierGroupData);die;
	return $modifierGroupData;
    }			
    public function getModifierItemsV5($group_id,$parent_id) {
        $sql = 'SELECT ma_modifier_group_items.id,ma_modifier_group_items.modifier_group_id,ma_modifier_group_items.pos_id,ma_modifier_group_items.price,
		ma_modifier_group_items.sort_order,ma_modifier_group_items.status,ma_modifier_group_items_attribute.attribute_id,ma_modifier_group_items_attribute.attribute_value,
		 ma_attribute.attribute_code  as field_name,ma_attribute.attribute_label,ma_attribute.backend_field_type,ma_attribute.attribute_prompt from ma_modifier_group_items  
		left join (ma_modifier_group_items_attribute inner join ma_attribute on ma_modifier_group_items_attribute.attribute_id=ma_attribute.id) on 
		ma_modifier_group_items_attribute.modifier_group_item_id=ma_modifier_group_items.id  where ma_modifier_group_items.modifier_group_id=' 
		.$group_id.' order by ma_modifier_group_items.sort_order asc';
	$modifierGroupData = DB::select($sql) ;
       // echo"<pre>"; print_r($modifierGroupData);die;	
	$grp1 =null; 
	if (count($modifierGroupData)) {
            foreach ($modifierGroupData as $index => $grp) {  
		unset($grp1);
		$grp1['id'] = $grp->id;
		
		$grp1['field_type'] = $grp->backend_field_type;
		if($grp1['field_type']=='Dropdown' || $grp1['field_type']=='Radio' || $grp1['field_type']=='Checkbox' || $grp1['field_type']=='Multiple Select'){
			$modifierGroupDataItem = MaAttributeItem::select('attribute_item_name')->whereIn('id', explode(',',$grp->attribute_value))->get()->toArray(); 
		  	if (isset($modifierGroupDataItem[0])) {  
				$grp1[$grp->field_name] = $modifierGroupDataItem[0]['attribute_item_name'];
			}else {
				$grp1[$grp->field_name] = $grp->attribute_value;
			}   
		}else{
			$grp1[$grp->field_name] = $grp->attribute_value;
		}
		 
		$modifierGroupData[$index]=$grp1;
                 
            }
        }
        return $modifierGroupData;
    }
    public function getPrentModifierName($groupItemId,$name=''){
	$pdata = ItemModifierGroupItem::select("parent_id")->where('ma_modifier_group_items.modifier_group_id',$groupItemId)->get()->toArray();
	if(isset($pdata[0]['parent_id']) && $pdata[0]['parent_id']>0){
	    	$name .=$this->getPrentModifierName($pdata[0]['parent_id']);
	}else {
		$data = ItemModifierGroupItemAttribute::select('ma_attribute_item.attribute_item_name')->where('modifier_group_item_id',$groupItemId)->where('is_primary_field',1)
			->leftJoin('ma_attribute', 'ma_modifier_group_items_attribute.attribute_id', '=', 'ma_attribute.id')
			->leftJoin('ma_attribute_item', 'ma_modifier_group_items_attribute.attribute_value', '=', 'ma_attribute_item.id')->get()->toArray();
		$name = $name. isset($data[0]['attribute_item_name'])?$data[0]['attribute_item_name']." -> ":'';
	}
	return $name;
    }
    public function getItemModifierGroupList($menu_id=null,$modifier_id=null){ 
	 
	$data1 = null; 
	$data=ItemModifierGroup::select("ma_modifier_group.*","ma_modifier_group_attribute.attribute_value"
		,'ma_attribute.backend_field_type',"ma_modifier_group_attribute.attribute_id",'ma_attribute.attribute_label as groupname')
		->where('is_primary_field',1)
		->leftJoin('ma_modifier_group_attribute', 'ma_modifier_group.id', '=', 'ma_modifier_group_attribute.modifier_group_id')
		->leftJoin('ma_attribute', 'ma_modifier_group_attribute.attribute_id', '=', 'ma_attribute.id');
	if($modifier_id!=null)$data->where('ma_modifier_group.id',$modifier_id);
	if($menu_id!=null)$data->where('ma_modifier_group.menu_item_id',$menu_id);
	$data= $data->get()->toArray();
	foreach($data as $index=>$fieldList){
		$data[$index]['system_attribute_field'] = 
		ItemModifierGroup::select("ma_modifier_group.*","ma_modifier_group_attribute.attribute_value",'ma_attribute.backend_field_type',
		"ma_modifier_group_attribute.attribute_id",'ma_attribute.attribute_label as groupname','attribute_item_name as groupvalue')
		->where('ma_modifier_group.menu_item_id',$menu_id)->where('is_primary_field',0)->where('ma_modifier_group.id',$fieldList['id'])
		->leftJoin('ma_modifier_group_attribute', 'ma_modifier_group.id', '=', 'ma_modifier_group_attribute.modifier_group_id')
		->leftJoin('ma_attribute', 'ma_modifier_group_attribute.attribute_id', '=', 'ma_attribute.id')
		->leftJoin('ma_attribute_item', 'ma_attribute.id', '=', 'ma_attribute_item.attribute_id')->get()->toArray();  
	}
	foreach($data as $index=>$fieldList){
		$data[$index]['itemmodifiers'] = ItemModifierGroupItem::select("ma_modifier_group_items.*")->where('ma_modifier_group_items.modifier_group_id',$fieldList['id'])
		->get()->toArray();
		//echo"<pre>";print_r($data[$index]);die; 
		foreach($data[$index]['itemmodifiers'] as $j=>$groupItemList){
			$groupItemListAttribute = ItemModifierGroupItemAttribute::where('modifier_group_item_id',$groupItemList['id'])->get()->toArray();
			//echo"<pre>";print_r($groupItemListAttribute);die;
			$groupItemListAttribute1=null;
			unset($groupItemListAttribute1);
	  		$itemAttributeList = null;
			foreach($groupItemListAttribute as $k=>$itemAttributeList){
				$k1= $itemAttributeList['attribute_value'];
				$groupItemListAttribute1[$k1]=$itemAttributeList;
			}
			$parent = $this->getPrentModifierName($groupItemList['parent_id']);
			//$groupItemListAttribute1['parent_name']=$parent;
			$groupItemListAttribute1['fix_status']=$itemAttributeList;
			$groupItemListAttribute1['fix_status']['status']=$groupItemList['status'];
			$groupItemListAttribute1['parent_id']=$itemAttributeList;
			$groupItemListAttribute1['parent_id']['parent_id']=$groupItemList['parent_id'];
			$groupItemListAttribute1['fix_pos_id']=$itemAttributeList;
			$groupItemListAttribute1['fix_pos_id']['pos_id']=$groupItemList['pos_id'];
			$groupItemListAttribute1['fix_price']=$itemAttributeList;
			$groupItemListAttribute1['fix_price']['price']=$groupItemList['price'];
			$groupItemListAttribute1['fix_sort_order']=$itemAttributeList;
			$groupItemListAttribute1['fix_sort_order']['sort_order']=$groupItemList['sort_order'];
			$data[$index]['itemmodifiers'][$j]=$groupItemListAttribute1;			
			//echo"<pre>";print_r($groupItemListAttribute1);die; 
		}
			
		
	} 
	 
	foreach($data as $index=>$fieldList){
		$data1[$index] = $fieldList;
		if(isset($fieldList['system_attribute_field'])) {
			foreach($fieldList['system_attribute_field'] as $i=>$fieldList1){
				unset($data1[$index]['system_attribute_field'][$i]);
				$data1[$index]['system_attribute_field'][$fieldList1['attribute_id']]= $fieldList1;
			}
		}
	}	
	return $data1;
    }
     
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function createModfierGroup(Request $request,$menu_id)
    {
         try {
               // DB::beginTransaction();
            	$locations =$this->getRestaurantId();
            	$data = $request->all();
		//print_r($data);die;
	    	if($data['primary_attribute_id']!=''){
			    //$attrItemObj =  MaAttribute::find($data['primary_attribute_id']);    
			    //$data['attribute_ids'][$data['primary_attribute_id']]=$attrItemObj->attribute_prompt;
			    $data['attribute_ids'][$data['primary_attribute_id']]=$data['primary_attribute_id'];
			    $data_request=[
				'restaurant_id' => $locations[0],
				'status' => $data['status'],               
				'menu_item_id' => $menu_id,             
			    ];
			    $g=ItemModifierGroup::create($data_request);
			    foreach($data['attribute_ids'] as $attrId=>$val)
			    $attribute_idsList[]=$attrId;
			    $attributeList = $this->getMaAttributeTypeList($attribute_idsList);
			    foreach($data['attribute_ids'] as $attrId=>$val){
				   if($attributeList[$attrId] !='File' || $attributeList[$attrId] !='Image'){
					$val = (is_array($val))?implode(',',$val):$val;		
					$attrData = ['menu_item_id'=>$menu_id,
						'modifier_group_id'=>$g->id,
						'attribute_id'=>$attrId,
						'attribute_value'=>($val)??'',
						'is_primary_field'=>($data['primary_attribute_id']==$attrId)?1:0,
						'attribute_type'=>$attributeList[$attrId] 
					];
					ItemModifierGroupAttribute::create($attrData); 
				   }
			    }
		
		    	   //DB::commit();
			   //echo"<pre>";print_r($data);die;
		    	   return back()->with('message', 'Item Modifier Group has been created successfully.');
		}else{
			//DB::rollback();
			 return back()->with('message', 'Something went wrong, please try later!!');
		}
        } catch (\Exception $e) { 
	     echo $e->getMessage();die;
             //DB::rollback();
             //print_r($data_request);die; 
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later!! '.$e->getMessage()));
            
         }
    }
    public function createModfierGroupItem(Request $request,$menu_id)
    {
         try {
           // DB::beginTransaction();
             $locations =$this->getRestaurantId();
             $data = $request->all();
	    // i am here
	     //print_r($data);die; 

	     $price = ($data['price']==null)?0:$data['price'];
	     $data_request =['modifier_group_id'=>$data['modifier_group_id'],'sort_order'=>$data['sort_order'],
		'price'=>$price,'status'=>$data['status'],'parent_id'=>$data['parent_id'],'pos_id'=>$data['pos_id']];
	     if($data['modifier_group_items_id']==0){
		 $m = ItemModifierGroupItem::create($data_request);
		  $modifier_group_item_id =  $m->id;
		 $msg="Item added successfully!";	 
	     }else{
		 $modifier_group_item_id =  $data['modifier_group_items_id'];
		 ItemModifierGroupItem::where('id',$modifier_group_item_id)->update($data_request);
		 $msg="Item updated successfully!";				
	     }
	     $list[] = $data['attribute_id'];
             foreach($data['attribute_ids'] as $attrId=>$val){
	    	$list[]=$attrId;
	     } // iam here
	     $attributeList = $this->getMaAttributeTypeList($list); 		
	     if(isset($modifier_group_item_id)){
		$del = DB::table('ma_modifier_group_items_attribute')->whereIn('modifier_group_item_id', [$modifier_group_item_id])->delete();
		/// add primary attribute like name of item
		$data_request=['modifier_group_item_id'=>$modifier_group_item_id,
				'menu_item_id' => $menu_id,
				'modifier_group_id' => $data['modifier_group_id'],
				'is_primary_field' => 1,               
				'attribute_id' => $data['attribute_id'],              
				'attribute_value' => $data['attribute_id_primary_field'], 
				'attribute_type'=>$attributeList[$data['attribute_id']]          
		];
		$data[$modifier_group_item_id][]=$data_request;				
		ItemModifierGroupItemAttribute::create($data_request);
		/// add secondary attribute like status,sort_order etc
		foreach($data['attribute_ids'] as $attribute_id=>$val){
		 	$val = (is_array($val))?implode(',',$val):
			$val = (isset($val))?$val:0; 
			$data_request = ['modifier_group_item_id'=>$modifier_group_item_id,
			  		'menu_item_id' => $menu_id,
					'modifier_group_id' => $data['modifier_group_id'],
					'is_primary_field' => 0,               
					'attribute_id' => $attribute_id,              
					'attribute_value' => $val, 
					'attribute_type' => $attributeList[$attribute_id]              
					];
			 //if new field added in attribute sec
			 ItemModifierGroupItemAttribute::create($data_request);
			$data[$modifier_group_item_id][]=$data_request;
		 } 
		return \Response::json(['data'=>$data,'error'=>0,'msg'=>$msg], 200);
	    }else{ 	
	    
            return \Response::json(['data'=>$data,'error'=>0,'message'=>'Please try again !!!'], 200);
           } 
        } catch (\Exception $e) {   
		 
	    return \Response::json(['data'=>$data,'error'=>0,'message'=>'Please try again '.$e->getMessage()], 200);
            
            
         }
    }	
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function updatemodifiergroup(Request $request,$menu_id)
    {
         try {
           // DB::beginTransaction();
            $locations =$this->getRestaurantId();
            $data = $request->all();
	   // print_r($data);die;
	    //update modifier group data	
	    foreach($data['primary_attribute_id'] as $attrId=>$val){
	   	 $attribute_idsList[]=$attrId;
	    }foreach($data['secodary_attribute_id'] as $attrId=>$val){
	    	foreach($val as $id=>$val1)
		$attribute_idsList[]=$id;
	    }
	    $attributeList = $this->getMaAttributeTypeList($attribute_idsList);
	    ///update modifirer_group's attribute if any changes done
	    if(is_array($data['secodary_attribute_id'])){
		    foreach($data['secodary_attribute_id'] as $modifier_group_item_id=>$groupItem){
			    $modifier_group_id	= $data['id'][$modifier_group_item_id];	 
			    foreach($groupItem as $item_attribute_id=>$val){
				    if($attributeList[$item_attribute_id] !='File'){
					$val = (is_array($val))?implode(',',$val):$val;		
					$attrData = [ 'attribute_value'=>($val)??''];
					$existNewAttribute = ItemModifierGroupAttribute::where('menu_item_id' , $menu_id)->where('modifier_group_id' , $modifier_group_id)
					->where('attribute_id',$item_attribute_id)->get()->toArray();
					if(isset($existNewAttribute[0])){
						ItemModifierGroupAttribute::where('menu_item_id' , $menu_id)->where('modifier_group_id' , $modifier_group_id)
						->where('attribute_id',$item_attribute_id)->update($attrData);
					}else{
						$val = (is_array($val))?implode(',',$val):$val;		
							$attrData = ['menu_item_id'=>$menu_id,
								'modifier_group_id'=>$modifier_group_id,
								'attribute_id'=>$item_attribute_id,
								'attribute_value'=>($val)??'',
								'is_primary_field'=>0,
								'attribute_type'=>$attributeList[$item_attribute_id] 
							];
						 //if new field added in attribute sec
						 ItemModifierGroupAttribute::create($attrData); 
					}	 
				   }
			    } 
			if(isset($data['status'][$modifier_group_item_id]))
				ItemModifierGroup::where('id' , $modifier_group_id)->update(['status'=>$data['status'][$modifier_group_item_id]]);
			if(isset($data['sort_order'][$modifier_group_item_id]))
				ItemModifierGroup::where('id' , $modifier_group_id)->update(['sort_order'=>$data['sort_order'][$modifier_group_item_id]]);
		   }
	   } //working fine
  

	   
	     
	    return back()->with('status_msg', ' Modifier Group has been updated successfully.');
            

        } catch (\Exception $e) {   
			//echo"<pre>";echo  __LINE__;
	   //  print_r($data);
	     //echo $e->getMessage();
             //DB::rollback();
            // print_r($data_request);
		//die; 
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later!! '.$e->getMessage()));
            
         }
    }	 
    public function deleteModfierGroup(Request $request,$id) {
	//delete group ma_modifier_group,ma_modifier_group_attribute
	//delete group ma_modifier_group_items,ma_modifier_group_items_attribute	
        $attrobj= ItemModifierGroup::find($id); //search group 
	$mitem = ItemModifierGroupAttribute::where('modifier_group_id',$id)->get()->toArray(); //search group attribute list by group id--- array
	/*if(isset($mitem)){
		foreach($mitem as $item){
			$a= ItemModifierGroupAttribute::find($item['id']);
			if($a)$a->delete();//delete group attribute
		}
		 
	}
	if($attrobj)$attrobj->delete(); //delete group


	$mitem = ItemModifierGroupItem::where('modifier_group_id',$id)->get()->toArray();//search group items list by group id--- array
	if(isset($mitem)){
		foreach($mitem as $item){
			$a= ItemModifierGroupItem::find($item['id']);
			if($a)$a->delete();//delete group attribute
		}
		 
	}
	$mitem1 = ItemModifierGroupItemAttribute::where('modifier_group_id',$id)->get()->toArray();//search group items attribute by item id --array
	if(isset($mitem1)){
		foreach($mitem1 as $item1){
			$a= ItemModifierGroupItemAttribute::find($item1['id']); //search group items attribute by id --single object
			if($a)$a->delete();//delete group items attribute 
		}
	}*/
	$del = DB::table('ma_modifier_group_items_attribute')->whereIn('modifier_group_id', [$id])->delete();
	$del = DB::table('ma_modifier_group_items')->whereIn('modifier_group_id', [$id])->delete();
	$del = DB::table('ma_modifier_group_attribute')->whereIn('modifier_group_id', [$id])->delete();
	$del = DB::table('ma_modifier_group')->whereIn('id', [$id])->delete();
	return json_encode(true);
    }
 
    
   
    public function listAddons(Request $request, $menu_id){
	$menuItem = MenuItem::find($menu_id);
	if($menuItem->id){ 
		 	$is_inventory= $menuItem->is_inventory;
			$product_type_id = $menuItem->product_type_id;
			$locations =$this->getRestaurantId();
			$attribute_ids = explode(',',$menuItem->customizable_attribute_ids);  
			if($menuItem->attribute_set_id!=''){ 
			$menuAttributeSet = MaAttributeSet::select('*')->where('id', $menuItem->attribute_set_id)->where('status', 1)->get()->toArray();
				$addonGroupFieldList = (isset($menuAttributeSet[0]['system_addon']) && $menuAttributeSet[0]['system_addon']!="")?explode(",",$menuAttributeSet[0]['system_addon']):null;
				$addonItemFieldList = (isset($menuAttributeSet[0]['system_addon_item']) && $menuAttributeSet[0]['system_addon_item']!="")?explode(",",$menuAttributeSet[0]['system_addon_item']):null;
			}else{
				$addonGroupFieldList=$addonItemFieldList=null;
			} 
			 
			$meal_group_form =  $this->getMaAttributeList($locations,['SystemAddon'],$addonGroupFieldList,null);
			$meal_group_item_form =  $this->getMaAttributeList($locations,['SystemAddonItem'],$addonItemFieldList,null);
			 
	
			$list_addons_group =  $this->fetchAddonsGroupList($menu_id);
			//echo"<pre>"; 
			//print_r($meal_group_form);die;	
			
		 
			$section_tab = 'addons'; 
	
			 
			//echo"<pre>"; 
			//print_r($list_addons_group);die;	
			return view('menumanagement::v2_menu_item.addons', compact('section_tab','menu_id','list_addons_group', 'meal_group_item_form','meal_group_form','is_inventory','product_type_id','menuItem'));
		
	}return redirect()->back()->with('err_msg', 'Invalid Request');
    }
    public function listAddonItems(Request $request, $groupId){
	$addonItem = ItemAddonsGroup::find($groupId);
	if($addonItem->id){ 
		$locations =$this->getRestaurantId();
		$menuItemArr = [];
		$addonObj = ItemAddonsItem::where('addon_group_id', $groupId)->orderBy('id', 'DESC')->get();
		foreach ($addonObj as $record) {
		    $menuItemArr[] = $record->menu_item_id;
		}
		//print_r($menuItemArr);die;
		$data = MenuItem::orderBy('id', 'DESC')->whereNotIn('product_type_id',[3])->where('is_frontend_visible',1)->where('restaurant_id', $locations[0])->whereNotIn('id', $menuItemArr)->get();

		$ret = [
		    'status' => true,
		    'message' => 'Menu items',
		    'data' => view('menumanagement::v2_menu_item.menuitems', compact('data', 'groupId'))->render()
		];

		return json_encode($ret);
	}
	return  null;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function insertAddonItem(Request $request) {
        try {
            DB::beginTransaction();
            //$request->isRulesRequired = false;
            $input = Input::get();

            $data_request = [
                'addon_group_id' => $input['group_id'],
            ];
            $menuItemIds = [];
            foreach ($input['menu_item_id'] as $menuItem) {
                if ($menuItem != '') {
                    $menuItemIds[] = $menuItem;
                }
            }
            
            $menuItemObj = MenuItem::select('ma_menu_item.*','ma_menu_item_price.price')
		->whereIn('ma_menu_item.id', $menuItemIds)->leftJoin('ma_menu_item_price', 'ma_menu_item_price.menu_id', '=', 'ma_menu_item.id')->where('ma_menu_item.status',1)->get();
            ob_start();
            foreach ($menuItemObj as $menuItem) {
 
                $data_request['name'] = $menuItem->item_name;
                $data_request['restaurant_id'] = $menuItem->restaurant_id;
                $data_request['menu_item_id'] = $menuItem->id;
                $priceArr = json_decode($menuItem->size_price, true);
                $data_request['addon_price'] = isset($priceArr[0] ['price'][0]) ? $priceArr[0] ['price'][0] : 0;
            
                ItemAddonsItem::create($data_request);
            }

            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Addon Item has been added successfully',
                'data' => []
            ];
        } catch (\Exception $e) { 
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => [$e->getMessage()]
            ];
        }
        return $data;
    } 		
    public function fetchAddonsGroupList($menu_id) {
	$m1=[];
	$sql ="select ma_addons_group.*,ma_addons_group_attribute.is_primary_field,ma_addons_group_attribute.attribute_id,ma_addons_group_attribute.attribute_value,
			ma_addons_group_attribute.attribute_type,ma_attribute.attribute_code,ma_attribute.attribute_prompt,ma_attribute.is_customizable from ma_addons_group left join 
			(ma_addons_group_attribute left join ma_attribute on ma_addons_group_attribute.attribute_id=ma_attribute.id)  
			on ma_addons_group_attribute.addon_group_id=ma_addons_group.id 
			 where ma_addons_group.menu_item_id=".$menu_id;
	 $modifierGroupData = DB::select($sql) ; 
	 foreach ($modifierGroupData as $index => $grp) {
		//print_r($grp);
		$m1[$grp->id]['id']=$grp->id;
		$m1[$grp->id]['menu_item_id']=$grp->menu_item_id;
		$m1[$grp->id]['sort_order']=$grp->sort_order;
		$m1[$grp->id]['status']=$grp->status;
		$m1[$grp->id]['group_name']=$grp->group_name;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_id']=$grp->attribute_id;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_value']=$grp->attribute_value;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_type']=$grp->attribute_type;		
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_code']=$grp->attribute_code;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_prompt']=$grp->attribute_prompt;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['is_customizable']=$grp->is_customizable;
		$m1[$grp->id]['items']=ItemAddonsItem::where('addon_group_id',$grp->id)->get();	
	 }
	return $m1;
    }
   		
    public function addUpdateAddons(Request $request,$menu_id){
         try {
               // DB::beginTransaction();
            	$locations =$this->getRestaurantId();
            	$data = $request->all();
		//print_r($data);die;//ItemAddonsGroup,ItemAddonsGroupAttribute
		if(isset($data['attribute_ids'])){
			foreach($data['attribute_ids'] as $attrId=>$val)
			$attribute_idsList[]=$attrId;
			$attributeList = $this->getMaAttributeTypeList($attribute_idsList);
			$data_request=[ 'restaurant_id' => $locations[0], 'status' => $data['status'],  
					'menu_item_id' => $menu_id ,'group_name'=>$data['group_name'],'sort_order'=>$data['sort_order']];
			$g=ItemAddonsGroup::create($data_request);
			foreach($data['attribute_ids'] as $attrId=>$val){
				   if($attributeList[$attrId] !='File' || $attributeList[$attrId] !='Image'){
					$val = (is_array($val))?implode(',',$val):$val;		
					$attrData = ['menu_item_id'=>$menu_id,
						'addon_group_id'=>$g->id,
						'attribute_id'=>$attrId,
						'attribute_value'=>($val)??'',
						'is_primary_field'=>0,
						'attribute_type'=>$attributeList[$attrId] 
					];
					ItemAddonsGroupAttribute::create($attrData); 
				   }
			}
			 //DB::commit();
			return back()->with('message', ' Meal Group has been created successfully.');
		}
		//DB::rollback();
		return back()->with('message', 'Something went wrong, please try later!!');	
	    	 
        } catch (\Exception $e) { 
	     echo $e->getMessage();die;
             //DB::rollback();
             //print_r($data_request);die; 
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later!! '.$e->getMessage()));
            
         }
    }
    public function addUpdateAddonsItems(Request $request,$menu_id){
         try {
               // DB::beginTransaction();
            	$locations =$this->getRestaurantId();
            	$data = $request->all();
		//echo"<pre>";print_r($data);die;//ItemAddonsGroup,ItemAddonsGroupAttribute
		//update addon group
		if(isset($data['group_name'])){
			foreach($data['group_name'] as $groupId=>$group_name){
					$data_request=[ 'status' => $data['status'][$groupId],  'group_name'=>$data['group_name'][$groupId],'sort_order'=>$data['sort_order'][$groupId]];
					ItemAddonsGroup::where('id',$groupId)->update($data_request);
			}
			if(isset($data['update_item_price'])){
				foreach($data['update_item_price'] as $itemId=>$price){
						$data_request=[ 'addon_price' => $price];
						ItemAddonsItem::where('id',$itemId)->update($data_request);
				}
			}
			if(isset($data['attribute_ids'])){
				foreach($data['attribute_ids'] as $groupId=>$attribute_ids){
					$del = DB::table('ma_addons_group_attribute')->whereIn('addon_group_id', [$groupId])->delete();
					foreach($attribute_ids as $attrId=>$val)
					$attribute_idsList[]=$attrId;
				
					$attributeList = $this->getMaAttributeTypeList($attribute_idsList);
					foreach($attribute_ids as $attrId=>$val){
						   if($attributeList[$attrId] !='File' || $attributeList[$attrId] !='Image'){
							$val = (is_array($val))?implode(',',$val):$val;		
							$attrData = ['menu_item_id'=>$menu_id,
								'addon_group_id'=>$groupId,
								'attribute_id'=>$attrId,
								'attribute_value'=>($val)??'',
								'is_primary_field'=>0,
								'attribute_type'=>$attributeList[$attrId] 
							];
							ItemAddonsGroupAttribute::create($attrData); 
						   }
					}
				   }
				 //DB::commit();
			 
			}
		return back()->with('message', ' Meal Item has been updated successfully.');
		}	
		
		//DB::rollback();
		return back()->with('message', 'Something went wrong, please try later!!');	
	    	 
        } catch (\Exception $e) { 
	     echo $e->getMessage();die;
             //DB::rollback();
             //print_r($data_request);die; 
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later!! '.$e->getMessage()));
            
         }
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroyAddonGroup($id) {
        DB::beginTransaction();
        try {
	    //addon_group_id	ma_addons_group_attribute
	    $del = DB::table('ma_addons_group_attribute')->whereIn('addon_group_id', [$id])->delete();	
	    $del = DB::table('ma_addons_group_items')->whereIn('addon_group_id', [$id])->delete();
	    $del = DB::table('ma_addons_group')->whereIn('id', [$id])->delete();		
             
            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Addon Item has been deleted successfully',
                'data' => []
            ];
            return $data;// back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return $data;//back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    } 		
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroyAddonItem($id) {
        DB::beginTransaction();
        try {
            ItemAddonsItem::where('id', $id)->delete();
            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Addon Item has been deleted successfully',
                'data' => []
            ];
            return $data;// back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return $data;//back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    } 
    //// Meal tab
    public function listMeals(Request $request, $menu_id){
	$menuItem = MenuItem::find($menu_id);
	if($menuItem->id){ 
		 	//ItemAddonsGroup,ItemAddonsGroupAttribute,ItemAddonsItem
			 
			$is_inventory= $menuItem->is_inventory;
			$product_type_id = $menuItem->product_type_id;
			$locations =$this->getRestaurantId();
			$attribute_ids = explode(',',$menuItem->customizable_attribute_ids);  
			 
			 
			$meal_group_form =  $this->getMaAttributeList($locations,['SystemMeal']);
			$meal_group_item_form =  $this->getMaAttributeList($locations,['SystemMealItem']);
			 
	
			$list_addons_group =  $this->fetchMealsGroupList($menu_id);
			//echo"<pre>"; 
			//print_r($item_addons_group);die;	
			
		 
			$section_tab = 'meal'; 
	
			 
			 
			return view('menumanagement::v2_menu_item.meals', compact('section_tab','menu_id','list_addons_group', 'meal_group_item_form','meal_group_form','is_inventory','product_type_id','menuItem'));
		
	}return redirect()->back()->with('err_msg', 'Invalid Request');
    }
    public function listMealItems(Request $request, $groupId){
	$addonItem = ItemMealsGroup::find($groupId);
	if($addonItem->id){ 
		$locations =$this->getRestaurantId();
		$menuItemArr = [];
		$addonObj = ItemMealsItem::where('meal_group_id', $groupId)->orderBy('id', 'DESC')->get();
		foreach ($addonObj as $record) {
		    $menuItemArr[] = $record->menu_item_id;
		}
		//print_r($menuItemArr);die;
		$data = MenuItem::orderBy('id', 'DESC')->whereNotIn('product_type_id',[3])->where('is_frontend_visible',1)->where('restaurant_id', $locations[0])->whereNotIn('id', $menuItemArr)->get();

		$ret = [
		    'status' => true,
		    'message' => 'Menu items',
		    'data' => view('menumanagement::v2_menu_item.menuitems', compact('data', 'groupId'))->render()
		];

		return json_encode($ret);
	}
	return  null;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function insertMealItem(Request $request) {
        try {
            DB::beginTransaction();
            //$request->isRulesRequired = false;
            $input = Input::get();

            $data_request = [
                'meal_group_id' => $input['group_id'],
            ];
            $menuItemIds = [];
            foreach ($input['menu_item_id'] as $menuItem) {
                if ($menuItem != '') {
                    $menuItemIds[] = $menuItem;
                }
            }
            
            $menuItemObj = MenuItem::select('ma_menu_item.*','ma_menu_item_price.price')
		->whereIn('ma_menu_item.id', $menuItemIds)->leftJoin('ma_menu_item_price', 'ma_menu_item_price.menu_id', '=', 'ma_menu_item.id')->where('ma_menu_item.status',1)->get();
            ob_start();
            foreach ($menuItemObj as $menuItem) {
 
                $data_request['name'] = $menuItem->item_name;
                $data_request['restaurant_id'] = $menuItem->restaurant_id;
                $data_request['menu_item_id'] = $menuItem->id;
                $priceArr = json_decode($menuItem->size_price, true);
                $data_request['meal_price'] = isset($priceArr[0] ['price'][0]) ? $priceArr[0] ['price'][0] : 0;
            
                ItemMealsItem::create($data_request);
            }

            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Meal Item has been added successfully',
                'data' => []
            ];
        } catch (\Exception $e) { 
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => [$e->getMessage()]
            ];
        }
        return $data;
    } 		
    public function fetchMealsGroupList($menu_id) {
	$m1=[];
	$sql ="select ma_meals_group.*,ma_meals_group_attribute.is_primary_field,ma_meals_group_attribute.attribute_id,ma_meals_group_attribute.attribute_value,
			ma_meals_group_attribute.attribute_type,ma_attribute.attribute_code,ma_attribute.attribute_prompt,ma_attribute.is_customizable from ma_meals_group left join 
			(ma_meals_group_attribute left join ma_attribute on ma_meals_group_attribute.attribute_id=ma_attribute.id)  
			on ma_meals_group_attribute.meal_group_id=ma_meals_group.id 
			 where ma_meals_group.menu_item_id=".$menu_id;
	 $modifierGroupData = DB::select($sql) ; 
	 foreach ($modifierGroupData as $index => $grp) {
		//print_r($grp);
		$m1[$grp->id]['id']=$grp->id;
		$m1[$grp->id]['menu_item_id']=$grp->menu_item_id;
		$m1[$grp->id]['sort_order']=$grp->sort_order;
		$m1[$grp->id]['status']=$grp->status;
		$m1[$grp->id]['group_name']=$grp->group_name;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_id']=$grp->attribute_id;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_value']=$grp->attribute_value;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_type']=$grp->attribute_type;		
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_code']=$grp->attribute_code;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['attribute_prompt']=$grp->attribute_prompt;
		$m1[$grp->id]['attribute_ids'][$grp->attribute_id]['is_customizable']=$grp->is_customizable;
		$m1[$grp->id]['items']=ItemMealsItem::where('meal_group_id',$grp->id)->get();	
	 }
	return $m1;
    }
   		
    public function addUpdateMeals(Request $request,$menu_id){
         try {
               // DB::beginTransaction();
            	$locations =$this->getRestaurantId();
            	$data = $request->all();
		//print_r($data);die;//ItemAddonsGroup,ItemAddonsGroupAttribute
		if(isset($data['attribute_ids'])){
			foreach($data['attribute_ids'] as $attrId=>$val)
			$attribute_idsList[]=$attrId;
			$attributeList = $this->getMaAttributeTypeList($attribute_idsList);
			$data_request=[ 'restaurant_id' => $locations[0], 'status' => $data['status'],  
					'menu_item_id' => $menu_id ,'group_name'=>$data['group_name'],'sort_order'=>$data['sort_order']];
			$g=ItemMealsGroup::create($data_request);
			foreach($data['attribute_ids'] as $attrId=>$val){
				   if($attributeList[$attrId] !='File' || $attributeList[$attrId] !='Image'){
					$val = (is_array($val))?implode(',',$val):$val;		
					$attrData = ['menu_item_id'=>$menu_id,
						'meal_group_id'=>$g->id,
						'attribute_id'=>$attrId,
						'attribute_value'=>($val)??'',
						'is_primary_field'=>0,
						'attribute_type'=>$attributeList[$attrId] 
					];
					ItemMealsGroupAttribute::create($attrData); 
				   }
			}
			 //DB::commit();
			return back()->with('message', ' Meal Group has been created successfully.');
		}
		//DB::rollback();
		return back()->with('message', 'Something went wrong, please try later!!');	
	    	 
        } catch (\Exception $e) { 
	     echo $e->getMessage();die;
             //DB::rollback();
             //print_r($data_request);die; 
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later!! '.$e->getMessage()));
            
         }
    }
    public function addUpdateMealsItems(Request $request,$menu_id){
         try {
               // DB::beginTransaction();
            	$locations =$this->getRestaurantId();
            	$data = $request->all();
		//echo"<pre>";print_r($data);die;//ItemAddonsGroup,ItemAddonsGroupAttribute
		//update addon group
		if(isset($data['group_name'])){
			foreach($data['group_name'] as $groupId=>$group_name){
					$data_request=[ 'status' => $data['status'][$groupId],  'group_name'=>$data['group_name'][$groupId],'sort_order'=>$data['sort_order'][$groupId]];
					ItemMealsGroup::where('id',$groupId)->update($data_request);
			}
			if(isset($data['update_item_price'])){
				foreach($data['update_item_price'] as $itemId=>$price){
						$data_request=[ 'meal_price' => $price];
						ItemMealsItem::where('id',$itemId)->update($data_request);
				}
			}
			if(isset($data['attribute_ids'])){
				foreach($data['attribute_ids'] as $groupId=>$attribute_ids){
					$del = DB::table('ma_meals_group_attribute')->whereIn('addon_group_id', [$groupId])->delete();
					foreach($attribute_ids as $attrId=>$val)
					$attribute_idsList[]=$attrId;
				
					$attributeList = $this->getMaAttributeTypeList($attribute_idsList);
					foreach($attribute_ids as $attrId=>$val){
						   if($attributeList[$attrId] !='File' || $attributeList[$attrId] !='Image'){
							$val = (is_array($val))?implode(',',$val):$val;		
							$attrData = ['menu_item_id'=>$menu_id,
								'meal_group_id'=>$groupId,
								'attribute_id'=>$attrId,
								'attribute_value'=>($val)??'',
								'is_primary_field'=>0,
								'attribute_type'=>$attributeList[$attrId] 
							];
							ItemMealsGroupAttribute::create($attrData); 
						   }
					}
				   }
				 //DB::commit();
			 
			}
		return back()->with('message', ' Meal Item has been updated successfully.');
		}	
		
		//DB::rollback();
		return back()->with('message', 'Something went wrong, please try later!!');	
	    	 
        } catch (\Exception $e) { 
	     echo $e->getMessage();die;
             //DB::rollback();
             //print_r($data_request);die; 
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later!! '.$e->getMessage()));
            
         }
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroyMealGroup($id) {
        DB::beginTransaction();
        try {
	    //addon_group_id	ma_addons_group_attribute
	    $del = DB::table('ma_meals_group_attribute')->whereIn('meal_group_id', [$id])->delete();	
	    $del = DB::table('ma_meals_group_items')->whereIn('meal_group_id', [$id])->delete();
	    $del = DB::table('ma_meals_group')->whereIn('id', [$id])->delete();		
             
            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Meal Item has been deleted successfully',
                'data' => []
            ];
            return $data;// back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return $data;//back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    } 		
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroyMealItem($id) {
        DB::beginTransaction();
        try {
            ItemMealsItem::where('id', $id)->delete();
            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Meal Item has been deleted successfully',
                'data' => []
            ];
            return $data;// back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return $data;//back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    } 
    //// end Meal tab

    ///// related product  
     public function fetchRelatedProductList($menu_id) {
	$related_group_item = ItemRelatedProductItem::where('related_product_id', $menu_id)->orderBy('id', 'DESC')->get()->toArray();
	return $related_group_item;
    }	
    public function listRelatedProducts(Request $request, $menu_id){
	$menuItem = MenuItem::find($menu_id);
	if($menuItem->id){ 
		 	//ItemAddonsGroup,ItemAddonsGroupAttribute,ItemAddonsItem
			$is_inventory= $menuItem->is_inventory;
			 $product_type_id = $menuItem->product_type_id; 
			$locations =$this->getRestaurantId();
			$attribute_ids = explode(',',$menuItem->customizable_attribute_ids);  
			 
			$list_related_item =  $this->fetchRelatedProductList($menu_id); 
			//echo"<pre>";print_r($list_related_item);die;
			$section_tab = 'related_products'; 
			return view('menumanagement::v2_menu_item.related_product', compact('section_tab','menu_id','list_related_item','is_inventory','product_type_id','menuItem'));
		
	}return redirect()->back()->with('err_msg', 'Invalid Request');
    }
    public function listRelatedProductItems(Request $request, $menu_id){
	$menuItem = MenuItem::find($menu_id);
	if($menuItem->id){ 
		$locations =$this->getRestaurantId();
		//ItemRelatedProductGroup,ItemRelatedProductGroupAttribute,ItemRelatedProductItem
		$menuItemArr = [];
		$itemObj = ItemRelatedProductItem::where('related_product_id', $menu_id)->orderBy('id', 'DESC')->get();
		foreach ($itemObj as $record) {
		    $menuItemArr[] = $record->menu_item_id;
		}
		//print_r($menuItemArr);die;
		$data = MenuItem::orderBy('id', 'DESC')->where('is_frontend_visible',1)->where('restaurant_id', $locations[0])->whereNotIn('id', $menuItemArr)->get();

		$ret = [
		    'status' => true,
		    'message' => 'Menu items',
		    'data' => view('menumanagement::v2_menu_item.related_product_menuitems', compact('data'))->render()
		];

		return json_encode($ret);
	}
	return  null;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function insertRelatedProductItem(Request $request,$related_product_id) {
        try {
            DB::beginTransaction();
            //$request->isRulesRequired = false;
            $input = Input::get();
	    $menuItemIds = [];
            foreach ($input['menu_item_id'] as $menuItem) {
                if ($menuItem != '') {
                    $menuItemIds[] = $menuItem;
                }
            }
            //ItemRelatedProductGroup,ItemRelatedProductGroupAttribute,ItemRelatedProductItem
            $menuItemObj = MenuItem::select('ma_menu_item.*','ma_menu_item_price.price')
		->whereIn('ma_menu_item.id', $menuItemIds)->leftJoin('ma_menu_item_price', 'ma_menu_item_price.menu_id', '=', 'ma_menu_item.id')->where('ma_menu_item.status',1)->get();
            ob_start();
            foreach ($menuItemObj as $menuItem) {
 
                $data_request['name'] = $menuItem->item_name;
                $data_request['restaurant_id'] = $menuItem->restaurant_id;
                $data_request['menu_item_id'] = $menuItem->id;
                $data_request['related_product_id'] = $related_product_id; 
            
                ItemRelatedProductItem::create($data_request);
            }

            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Related Product Item has been added successfully',
                'data' => []
            ];
        } catch (\Exception $e) { 
            DB::rollback();
            $data = [
                'status' => false,
                'message' => 'Something went wrong, please try later',
                'data' => [$e->getMessage()]
            ];
        }
        return $data;
    } 		
     		
    public function addUpdateRelatedProducts(Request $request,$menu_id){
         try {
               // DB::beginTransaction();
            	$locations =$this->getRestaurantId();
            	$data = $request->all();
		//print_r($data);die;//ItemRelatedProductGroup,ItemRelatedProductGroupAttribute,ItemRelatedProductItem
		if(isset($data['attribute_ids'])){
			foreach($data['attribute_ids'] as $attrId=>$val)
			$attribute_idsList[]=$attrId;
			$attributeList = $this->getMaAttributeTypeList($attribute_idsList);
			$data_request=[ 'restaurant_id' => $locations[0], 'status' => $data['status'],  
					'menu_item_id' => $menu_id ,'group_name'=>$data['group_name'],'sort_order'=>$data['sort_order']];
			$g=ItemRelatedProductGroup::create($data_request);
			foreach($data['attribute_ids'] as $attrId=>$val){
				   if($attributeList[$attrId] !='File' || $attributeList[$attrId] !='Image'){
					$val = (is_array($val))?implode(',',$val):$val;		
					$attrData = ['menu_item_id'=>$menu_id,
						'related_product_group_id'=>$g->id,
						'attribute_id'=>$attrId,
						'attribute_value'=>($val)??'',
						'is_primary_field'=>0,
						'attribute_type'=>$attributeList[$attrId] 
					];
					ItemRelatedProductGroupAttribute::create($attrData); 
				   }
			}
			 //DB::commit();
			return back()->with('message', 'Related Product Group has been created successfully.');
		}
		//DB::rollback();
		return back()->with('message', 'Something went wrong, please try later!!');	
	    	 
        } catch (\Exception $e) { 
	     echo $e->getMessage();die;
             //DB::rollback();
             //print_r($data_request);die; 
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later!! '.$e->getMessage()));
            
         }
    }
    public function addUpdateRelatedProductsItems(Request $request,$menu_id){
         try {
               // DB::beginTransaction();
            	$locations =$this->getRestaurantId();
            	$data = $request->all();
		//echo"<pre>";print_r($data);die;//ItemAddonsGroup,ItemAddonsGroupAttribute
		//update addon group
		if(isset($data['group_name'])){
			foreach($data['group_name'] as $groupId=>$group_name){
					$data_request=[ 'status' => $data['status'][$groupId],  'group_name'=>$data['group_name'][$groupId],'sort_order'=>$data['sort_order'][$groupId]];
					ItemRelatedProductGroup::where('id',$groupId)->update($data_request);
			}
			if(isset($data['update_item_price'])){
				foreach($data['update_item_price'] as $itemId=>$price){
						$data_request=[ 'addon_price' => $price];
						ItemRelatedProductItem::where('id',$itemId)->update($data_request);
				}
			}
			if(isset($data['attribute_ids'])){
				foreach($data['attribute_ids'] as $groupId=>$attribute_ids){
					$del = DB::table('ma_related_product_group_attribute')->whereIn('related_product_group_id', [$groupId])->delete();
					foreach($attribute_ids as $attrId=>$val)
					$attribute_idsList[]=$attrId;
				
					$attributeList = $this->getMaAttributeTypeList($attribute_idsList);
					foreach($attribute_ids as $attrId=>$val){
						   if($attributeList[$attrId] !='File' || $attributeList[$attrId] !='Image'){
							$val = (is_array($val))?implode(',',$val):$val;		
							$attrData = ['menu_item_id'=>$menu_id,
								'related_product_group_id'=>$groupId,
								'attribute_id'=>$attrId,
								'attribute_value'=>($val)??'',
								'is_primary_field'=>0,
								'attribute_type'=>$attributeList[$attrId] 
							];
							ItemRelatedProductGroupAttribute::create($attrData); 
						   }
					}
				   }
				 //DB::commit();
			 
			}
		return back()->with('message', 'Related Product has been updated successfully.');
		}	
		
		//DB::rollback();
		return back()->with('message', 'Something went wrong, please try later!!');	
	    	 
        } catch (\Exception $e) { 
	     echo $e->getMessage();die;
             //DB::rollback();
             //print_r($data_request);die; 
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later!! '.$e->getMessage()));
            
         }
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroyRelatedProductGroup($id) {
        DB::beginTransaction();
        try {
	    //addon_group_id	ma_addons_group_attribute
	    //$del = DB::table('ma_related_product_group_attribute')->whereIn('related_product_group_id', [$id])->delete();	
	    $del = DB::table('ma_related_product_group_items')->whereIn('related_product_group_id', [$id])->delete();
	    //$del = DB::table('ma_related_product_group')->whereIn('id', [$id])->delete();		
             
            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Related Product Item has been deleted successfully',
                'data' => []
            ];
            return $data;// back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return $data;//back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    } 		
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroyRelatedProductItem($id) {
        DB::beginTransaction();
        try {
            ItemRelatedProductItem::where('id', $id)->delete();
            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Related Product Item has been deleted successfully',
                'data' => []
            ];
            return $data;// back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return $data;//back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    } 	
     public function listAvailability(Request $request, $menu_id){
	$locations =$this->getRestaurantId();
	$m = MenuItem::find($menu_id);
	if($m->id){
		$menu_availability = MenuAvailability::where('menu_item_id',$menu_id)->get()->toArray();//print_r($days);die;
		if(isset($menu_availability[0])){
			$menu_availability = $menu_availability[0];
			if($menu_availability['days']!=''){			
				$days = explode(",",$menu_availability['days']);//print_r($days);die;
				foreach($days as $day)$menu_availability[$day]=$day;
			}
		}else 
			$menu_availability = null; 
		 
		$is_inventory= $m->is_inventory;
		$product_type_id = $m->product_type_id;
		$section_tab = 'availability';  
        	return view('menumanagement::v2_menu_item.item_availability', compact('menu_id','section_tab','is_inventory','product_type_id','menu_availability'));
	}else redirect('v2/menu/item/'.$menu_id.'/addprice')->with('message', 'Please try again!!');	
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addUpdateAvailability(Request $request,$menu_id)
    {
	$data = $request->all();//print_r($data);die;
	unset($data['_token']);
	$id= $data['id'];
	unset($data['id']);
	$m = MenuItem::find($menu_id);  
	if(isset($m->id)){
		if(isset($data['days']) || (isset($data['start_time']) && isset($data['end_time'])) ||  (isset($data['start_date']) && isset($data['end_date']))){	
			$data['days'] = implode(",",$data['days']);
			$data['menu_item_id'] = $menu_id;
			if($id>0) $mp = MenuAvailability::where('menu_item_id' , $menu_id)->update($data);
			else  $mp = MenuAvailability::create($data);
			if($m->is_inventory==1){
				return redirect('v2/menu/item/'.$menu_id.'/addinventory')->with('message', 'Item Availability updated successfully!!!');
			} else{
				return redirect('v2/menu/item/images/'.$menu_id.'/list')->with('message', 'Item Availability updated successfully!!!');
			} 
			//return redirect()->back()->with('message', 'Item Availability updated successfully!!!');
		}
		return redirect()->back();
		 
	}else{
		return redirect()->back()->with('message', 'Please try again!!!');
	}
		
    }
    public function clearMenuCache(Request $request){
	$del = DB::table('ma_menu_cache')->delete();
	return view('menumanagement::v2_menu_item.cache-cleard');
    }
    public function imageEditor(Request $request){
	return view('menumanagement::v2_menu_item.image-creater');
    }			  
}
