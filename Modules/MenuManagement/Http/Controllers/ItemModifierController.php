<?php

namespace Modules\MenuManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Restaurant;
use App\Helpers\CommonFunctions;
use Modules\MenuManagement\Entities\ItemModifierGroup;
use Modules\MenuManagement\Entities\ItemModifierGroupLanguage;
use Modules\MenuManagement\Http\Requests\ItemModifierGroupRequest;
use DB;
use Modules\MenuManagement\Entities\ItemModifier;
use Modules\MenuManagement\Entities\ItemModifierLanguage;
use Modules\MenuManagement\Entities\ItemModifierOptionGroup;
use Modules\MenuManagement\Entities\ItemModifierOptionGroupLanguage;
use Modules\MenuManagement\Entities\ItemOptionsModifier;
use Modules\MenuManagement\Entities\ItemOptionsModifierLanguage;


class ItemModifierController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

   
   
    public function saveItemModifierOptionGroup(Request $request)
    {
         try {
            DB::beginTransaction();
            
            $data = $request->all();

            $data_request=[
                'modifier_item_id' => $data['modifier_item_id'],
                'group_name' => $data['group_name'],               
                'prompt' => $data['prompt'],               
                'language_id' =>1,   
                'is_required' => $data['is_required'],               
                'quantity_type' => $data['quantity_type'],              
                'quantity' => $data['quantity'],              
            ];
          
            $modifier_option_group_id = ItemModifierOptionGroup::create($data_request)->id;
            $data_request_lang = [
                'language_id' =>$data['language_id'],   
                'modifier_option_group_id' => $modifier_option_group_id,               
                'group_name' => $data['group_name'],               
                'prompt' => $data['prompt'],               
            ];
            ItemModifierOptionGroupLanguage::create($data_request_lang);
            
            $data=$this->getModifierItemInfo($data['modifier_item_id'], $data['language_id'], 1);
            DB::commit();
            if ($request->ajax()) {
            return \Response::json(['success' => true,'data'=> $data, 'message' =>'Item Modifier Option Group has been created successfully.'], 200);

            }else{
              return back()->with('status_msg', 'Item Modifier Option Group has been created successfully.');
            }

        } catch (\Exception $e) {
           DB::rollback();
            if ($request->ajax()) {
               return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
           
            }else{
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
            }
         }
    }

  

    public function deleteModifierItemOptionGroup(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $modifier_item=$request->input('modifier_item');


            ItemOptionsModifier::where('modifier_option_group_id', $id)->delete(); //delete item option code
            ItemModifierOptionGroup::where('id', $id)->delete();
            DB::commit();

              //$data=$this->getModifierItemInfo($modifier_item,1);
             $data='';
            if ($request->ajax()) {
            return \Response::json(['success' => true,'data'=> $data, 'message' =>'Item Modifier option Group has been deleted successfully.'], 200);

            }else{
              return back()->with('status_msg', 'Item Modifier option Group has been deleted successfully.');
            }

        }
        catch (\Exception $e){
            DB::rollback();
               if ($request->ajax()) {
                    return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
           
                }else{
                    return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
                }
        }
    }

    public function getModifierItemInfo($id, $languageId = 1, $direct = 0) {

        $modifier_item_id = $id;
        $data = ItemModifier::with('modifierItemOptionGroup.modifierItemOptions', 'itemModifierLanguage')->find($id);

        if ($data) {
            $modifierName = $data->modifier_name;
            if ($data->itemModifierLanguage) {
                $itemModifierLanguage = $data->itemModifierLanguage;
                foreach ($itemModifierLanguage as $itemModifierLanguageRow) {
                    if ($itemModifierLanguageRow->language_id == $languageId) {
                        $modifierName = $itemModifierLanguageRow->modifier_name;
                        break;
                    }
                }
            }
            $option_str = '';
            if ($data->modifierItemOptionGroup) {
                $modifierItemOptionGroup = $data->modifierItemOptionGroup;
                // print_r( $modifierItemOptionGroup);
                $option_str = '<div class="panel-group" id="accordionmodifier_item_option_group" role="tablist" aria-multiselectable="true">';
                if (isset($modifierItemOptionGroup) && count($modifierItemOptionGroup)) {
                    $groupcount = 0;
                    foreach ($modifierItemOptionGroup as $modifier_item_option_group) {
                        
                        $modifier_item_option_group_language = ItemModifierOptionGroupLanguage::where([
                            'modifier_option_group_id'=>$modifier_item_option_group->id,
                            'language_id'=> $languageId
                                ])->get();
                        if (count($modifier_item_option_group_language)) {
                            foreach ($modifier_item_option_group_language as $row) {
                                if ($row->language_id == $languageId) {
                                    $modifier_item_option_group->group_name = $row->group_name;
                                    $modifier_item_option_group->prompt = $row->prompt;
                                }
                            }
                        }
                        
                        $modifierItemOptions = $modifier_item_option_group->modifierItemOptions;
                        $modifier_itemoption_str = '';
                        if (count($modifierItemOptions)) {
                            $item_index = 0;
                            $modifier_itemoption_str .= '<table class="table table-striped modifer_option_list"> <thead> <tr> <th>Modifier Option Name</th> <th>Price</th> <th>Options</th> </tr> </thead> <tbody>';
                            foreach ($modifierItemOptions as $itemopt) {
                                $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where([
                                    'modifier_option_id' => $itemopt->id,
                                    'language_id' => $languageId
                                ])->first();
                                if ($itemOptionsModifierLanguage !== null) {
                                    $itemopt->modifier_option_name = $itemOptionsModifierLanguage->modifier_option_name;
                                }
                                $modifier_itemoption_str .= '<tr> <td><div class="form-group">     <input type="hidden" name="modifier_item_option_group[' . $modifier_item_option_group->id . '][modifier_options][' . $item_index . '][modifier_option_id]" value="' . $itemopt->id . '"><input type="text" class="form-control" name="modifier_item_option_group[' . $modifier_item_option_group->id . '][modifier_options][' . $item_index . '][modifier_option_name]" placeholder="Modifier Name" value="' . $itemopt->modifier_option_name . '"></div> </td> <td><div class="form-group"><input type="text" class="form-control" name="modifier_item_option_group[' . $modifier_item_option_group->id . '][modifier_options][' . $item_index . '][modifier_option_price]" placeholder="Price" value="' . $itemopt->price . '"></div></td> <td> <i class="fas fa-times delete_modifier_option" modifieroptionid="' . $itemopt->id . '" ></i>  </td> </tr>';
                                $item_index++;
                            }
                            $modifier_itemoption_str .= '</tbody> </table>';
                        }
                        $modifier_itemoption_str .= '<div class="row"><a href="javascript:void(0)" class="add_modifier_option" modifier_item_option_group="' . $modifier_item_option_group->id . '">+add modifier option</a></div>';

                        $option_str .= '<div class="panel panel-default"> '
                                . '<div class="panel-heading" role="tab" id="heading' . $modifier_item_option_group->id . '"> '
                                . '<h4 class="panel-title"> '
                                . '<a class="collapsed" data-toggle="collapse" data-parent="#accordionmodifier_item_option_group" href="#modifier_item_option_groupcollapse' . $modifier_item_option_group->id . '" aria-expanded="false" aria-controls="collapse' . $modifier_item_option_group->id . '"> '
                                . '' . $modifier_item_option_group->group_name . ' </a> '
                                . '<span class="delete_edit_optiongroup"> '
                                . '<i class="fa fa-trash-o delete_modifier_option_group pull-right" aria-hidden="true" modifier_item_option_group_id="' . $modifier_item_option_group->id . '"  modifier_item="' . $modifier_item_id . '" ></i> '
                                . '<!-- <i class="fa fa-pencil" aria-hidden="true"></i> --> '
                                . '</span> '
                                . '</h4> '
                                . '</div> '
                                . '<div id="modifier_item_option_groupcollapse' . $modifier_item_option_group->id . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' . $modifier_item_option_group->id . '" aria-expanded="false" style="height: 0px;"> '
                                . '<div class="panel-body"> '
                                .'<div class="row">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">Name</label>
                                    <div class="col-md-8">
                                        <div class="input__field">
                                            <input type="text"
                                                   class=""
                                                   name="modifier_item_option_group[' . $modifier_item_option_group->id . '][group_name]" value="' . htmlspecialchars($modifier_item_option_group->group_name) . '" required>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">Prompt</label>
                                    <div class="col-md-8">
                                        <div class="input__field">
                                            <input type="text"
                                                   class=""
                                                   name="modifier_item_option_group[' . $modifier_item_option_group->id . '][prompt]" value="' . htmlspecialchars($modifier_item_option_group->prompt) . '" required>
                                        </div>

                                    </div>
                                </div>'
                                . '<div class="form-group row "> '
                                . '<label class="col-md-4 col-form-label text-md-right">Quantity Type</label> '
                                . '<div class="col-md-3 quantity_type_select"> '
                                . '<select name="modifier_item_option_group[' . $modifier_item_option_group->id . '][quantity_type]" class="quantity_type form-control" required>'
                                . '<option value="Value" ' . ($modifier_item_option_group->quantity_type == "Value" ? "selected" : "") . ' >Value</option> '
                                . '<option value="Range" ' . ($modifier_item_option_group->quantity_type == "Range" ? "selected" : "") . '>Range</option> '
                                . '</select> '
                                . '</div> '
                                . '<div class="col-md-4 quantity_type_val"> '
                                . '<div class="value_quantity1"> '
                                . '<div class="input__field"> '
                                . '<input id="quantity" type="text" class="" name="modifier_item_option_group[' . $modifier_item_option_group->id . '][quantity]" value="' . $modifier_item_option_group->quantity . '" required placeholder="Min,Max"> '
                                . '</div> '
                                . '</div> '
                                . '</div> '
                                . '</div> '
                                . '<div class="form-group row "> '
                                . '<label for="priority" class="col-md-4 col-form-label text-md-right">Is this required?</label> '
                                . '<div class="col-md-8" style="padding-top: 10px;"> '
                                . '<input type="radio" name="modifier_item_option_group[' . $modifier_item_option_group->id . '][is_required]" value="1" ' . ($modifier_item_option_group->is_required == 1 ? "checked" : "") . ' > Required &nbsp;&nbsp;&nbsp;&nbsp; '
                                . '<input type="radio" name="modifier_item_option_group[' . $modifier_item_option_group->id . '][is_required]" value="0" ' . ($modifier_item_option_group->is_required == 0 ? "checked" : "") . '> Optional </div> </div>' . $modifier_itemoption_str . ''
                                . '</div>'
                                . '</div>'
                                . '</div>';



                        $groupcount++;
                    }
                } else {
                    $option_str .= '<div class="text-center">   No Modifiers found</div>';
                }
                $option_str .= '</div>';
            }
            $option_str .= '<a href="#"  modifieritem="' . $id . '" id="modifieroptiongroupopen">+add new modifier option group</a>';

            $str = <<<EOD
<form method="POST" class="form-inline" id="modifiergroup_form_option" action="/menu/item/modifieroption/{$id}" enctype="multipart/form-data">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Modifier Item Option</h4>
    </div>
    <div class="modal-body">
        <div class="col-lg-12">
          <div class="form-group">
            <label class="control-label col-sm-5" for="modifier">Modifier Name :</label>
            <div class="col-sm-7">
            <input type="text" class="form-control" value="{$modifierName}" id="modifier" name="modifier_name" placeholder="Modifier Name">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-6" for="pwd">Price:</label>
            <div class="col-sm-6"> 
            <input type="text" class="form-control" value="{$data->price}"   name="modifier_price" id="modifier_price" placeholder="Modifier Price">
            </div>
          </div>
        </div>
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#options">Options</a></li>
            <!--<li><a data-toggle="tab" href="#dependency">Dependency</a></li>-->
          </ul>

          <div class="tab-content">
              <div id="options" class="tab-pane fade in active">
              <h3>Options</h3>
              {$option_str}
              </div>
              <div id="dependency" class="tab-pane fade">
              <h3>Dependency</h3>
              <p>Some content in Dependency.</p>
              </div>
             
          </div>
                     
       
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-default">Submit</button>

      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div></form>
EOD;
            if ($direct == 0) {
                return \Response::json(['success' => true, 'message' => 'Modifier item details', 'data' => $str], 200);
            } else {
                return $str;
            }
        } else {
            if ($direct == 0) {
                return \Response::json(['success' => false, 'message' => 'No Modifiter Item found '], 500);
            }
        }
    }

    public function saveModifierOption(Request $request,$id){
        
        if(!isset($id) || !is_numeric($id)){
            return redirect()->back()->with('err_msg','Invalid Request');
        }         
        DB::beginTransaction();
        try {

          if($request->has('modifier_name') && $request->has('modifier_price')){

            $modifier_item_data=[
                  'modifier_name'=>$request->post('modifier_name'),
                  'price'=>$request->post('modifier_price'),
              ];

            ItemModifier::where('id',$id)->update($modifier_item_data);
            if (ItemModifierLanguage::where(['modifier_item_id' => $id, 'language_id' => $request->post('language_id')])->count()) {
                ItemModifierLanguage::where(['modifier_item_id' => $id, 'language_id' => $request->post('language_id')])->update(['modifier_name'=>$request->post('modifier_name')]);
            } else {
                ItemModifierLanguage::create([
                    'modifier_item_id'=> $id,
                    'language_id'=> $request->post('language_id'),
                    'modifier_name' => $request->post('modifier_name')
                    ]
                );
            }
            
            if($request->has('modifier_item_option_group')){    
                $modifier_item_option_group=$request->input('modifier_item_option_group');
                foreach( $modifier_item_option_group as $key=>$val){
                    $group_id=$key;
                        
                    $group_data=[
                        'group_name'=>$val['group_name'],
                        'prompt'=>$val['prompt'],
                        'quantity_type'=>$val['quantity_type'],
                        'quantity'=>$val['quantity'],
                        'is_required'=>$val['is_required'],
                    ];
                  
                   ItemModifierOptionGroup::where('id', $key)->update($group_data);
                   
                    if (ItemModifierOptionGroupLanguage::where(['modifier_option_group_id'=>$key, 'language_id' => $request->post('language_id')])->count()) {
                        $group_data_lang=[
                            'group_name'=>$val['group_name'],
                            'prompt'=>$val['prompt'],
                        ];
                        ItemModifierOptionGroupLanguage::where([
                            'modifier_option_group_id'=>$key,
                            'language_id' => $request->post('language_id')
                                ])->update($group_data_lang);
                    } else {
                        $group_data_lang=[
                            'modifier_option_group_id'=>$key,
                            'group_name'=>$val['group_name'],
                            'prompt'=>$val['prompt'],
                            'language_id' => $request->post('language_id')
                        ];
                        ItemModifierOptionGroupLanguage::create($group_data_lang);
                    }
                    
                    if(isset($val['modifier_options']) &&  count($val['modifier_options'])){

                        foreach($val['modifier_options'] as $itemval){

                                if(isset($itemval['modifier_option_id']) &&  !empty($itemval['modifier_option_id'])){

                                   $old_modifier_items=array_column($val['modifier_options'],'modifier_option_id');
                                   ItemOptionsModifier::where('modifier_option_group_id',$group_id)->whereNotIn('id',  $old_modifier_items)->delete();

                                    $option_data=[
                                        'modifier_option_name'=>$itemval['modifier_option_name'],
                                        'price'=>$itemval['modifier_option_price'],
                                    ];

                                   ItemOptionsModifier::where('id', $itemval['modifier_option_id'])->update($option_data);
                                   
                                   if (ItemOptionsModifierLanguage::where(['modifier_option_id'=> $itemval['modifier_option_id'],'language_id' => $request->post('language_id')])->count()) {
                                       $option_data_lang=[
                                            'modifier_option_name'=>$itemval['modifier_option_name'],
                                        ];
                                        ItemOptionsModifierLanguage::where([
                                            'modifier_option_id'=> $itemval['modifier_option_id'],
                                            'language_id' => $request->post('language_id')
                                                ])->update($option_data_lang);
                                   } else {
                                       $option_data_lang=[
                                            'modifier_option_id' => $itemval['modifier_option_id'],
                                            'language_id' => $request->post('language_id'),
                                            'modifier_option_name'=>$itemval['modifier_option_name'],
                                        ];
                                        ItemOptionsModifierLanguage::create($option_data_lang);
                                   }

                                }else{

                                    $option_data=[
                                        'modifier_option_group_id'=>$group_id,
                                        'modifier_option_name'=>$itemval['modifier_option_name'],
                                        'price'=>$itemval['modifier_option_price'],
                                    ];

                                    $modifier_option_id = ItemOptionsModifier::create($option_data)->id;
                                    
                                    $option_data_lang=[
                                        'modifier_option_id' => $modifier_option_id,
                                        'language_id' => $request->post('language_id'),
                                        'modifier_option_name'=>$itemval['modifier_option_name'],
                                    ];
                                    ItemOptionsModifierLanguage::create($option_data_lang);

                                }
                        }

                    }

                }
                
               
            }
             DB::commit();
               if ($request->ajax()) {
                     return \Response::json(['success' => true, 'message' =>'Modifier Option  updated successfully.'], 200);
                }else{
                     return back()->with('section_tab','modifiergroup')->with('status_msg','Modifier Option  updated successfully');
                }
            
          }
           
            
        } catch (\Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
               return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
           
            }else{
               return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
            }
        }
    }



      public function deleteModifierOption(Request $request,$id)
    {
        DB::beginTransaction();
        try {


            ItemOptionsModifier::where('id', $id)->delete(); 
            DB::commit();

            if ($request->ajax()) {
            return \Response::json(['success' => true,'message' =>'Item Modifier option  has been deleted successfully.'], 200);

            }else{
              return back()->with('status_msg', 'Item Modifier option  has been deleted successfully.');
            }

        }
        catch (\Exception $e){
            DB::rollback();
               if ($request->ajax()) {
                    return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
           
                }else{
                    return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
                }
        }
    }

}
