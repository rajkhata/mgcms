<?php

namespace Modules\MenuManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Restaurant;
use Modules\MenuManagement\Entities\MenuItemModel;
use Illuminate\Support\Facades\Input;
use App\Helpers\CommonFunctions;
use Modules\MenuManagement\Entities\ItemAddons;
use Modules\MenuManagement\Entities\ItemAddonGroups;
use Modules\MenuManagement\Http\Requests\ItemAddonsRequest;
use Modules\MenuManagement\Http\Requests\ItemAddonGroupRequest;
use DB;

class ItemAddonsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, $addonGroupId) {
//        $restaurant_id = 0;
//        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
//        $restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
//        if ($restaurantData && count($restaurantData) == 1) {
//            $restaurant_id = $restaurantData[0]['id'];
//        }
//
//        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
//            $restaurant_id = $request->input('restaurant_id');
//            $data = ItemAddons::where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->paginate(5);
//        } else {
//            $data = ItemAddons::orderBy('id', 'DESC')->whereIn('restaurant_id', $locations)->paginate(5);
//        }

        $data = ItemAddons::orderBy('id', 'DESC')->where('addon_group_id', $addonGroupId)->get();

        return view('menumanagement::ItemAddons.index', compact('data'));
    }

    public function menuitems(Request $request, $restaurantId, $addonGroupId) {
//        $restaurant_id = 0;
//        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
//        $restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
//        if ($restaurantData && count($restaurantData) == 1) {
//            $restaurant_id = $restaurantData[0]['id'];
//        }
//
//        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
//            $restaurant_id = $request->input('restaurant_id');
//            $data = ItemAddons::where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->paginate(5);
//        } else {
//            $data = ItemAddons::orderBy('id', 'DESC')->whereIn('restaurant_id', $locations)->paginate(5);
//        }
        $menuItemArr = [];
        $addonObj = ItemAddons::where('addon_group_id', $addonGroupId)->orderBy('id', 'DESC')->get();
        foreach ($addonObj as $record) {
            $menuItemArr[] = $record->item_id;
        }

        $data = MenuItemModel::orderBy('id', 'DESC')->where('restaurant_id', $restaurantId)->whereNotIn('id', $menuItemArr)->get();

        $ret = [
            'status' => true,
            'message' => 'Menu items',
            'data' => view('menumanagement::ItemAddons.menuitems', compact('data', 'addonGroupId'))->render()
        ];

        return json_encode($ret);
    }

    public function groupaddons(Request $request, $addonGroupId) {
        $restaurant_id = 0;
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $restaurantData = Restaurant::select('id', 'restaurant_name')->whereIn('id', $locations)->orderBy('id', 'DESC')->get();
        if ($restaurantData && count($restaurantData) == 1) {
            $restaurant_id = $restaurantData[0]['id'];
        }

        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $restaurant_id = $request->input('restaurant_id');
            $data = ItemAddons::where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->paginate(5);
        } else {
            $data = ItemAddons::orderBy('id', 'DESC')->whereIn('restaurant_id', $locations)->paginate(5);
        }

        return view('menumanagement::ItemAddons.index', compact('data', 'restaurantData', 'restaurant_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $restaurantData = CommonFunctions::getRestaurantGroup();
        $selected_rest_id = 0;
        if (count($restaurantData) == 1) {
            $first_ind_array = current($restaurantData);
            if (count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        }
        return view('menumanagement::ItemAddons.create', compact('restaurantData', 'selected_rest_id'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            //$request->isRulesRequired = false;
            $input = Input::get();

            $data_request = [
                'addon_group_id' => $input['addon_group_id'],
            ];
            $menuItemIds = [];
            foreach ($input['menu_item_id'] as $menuItem) {
                if ($menuItem != '') {
                    $menuItemIds[] = $menuItem;
                }
            }
            
            $menuItemObj = MenuItemModel::whereIn('id', $menuItemIds)->get();
            ob_start();
            foreach ($menuItemObj as $menuItem) {
 
                $data_request['name'] = $menuItem->name;
                $data_request['restaurant_id'] = $menuItem->restaurant_id;
                $data_request['item_id'] = $menuItem->id;
                $priceArr = json_decode($menuItem->size_price, true);
                $data_request['addon_price'] = isset($priceArr[0] ['price'][0]) ? $priceArr[0] ['price'][0] : 0;
            
                ItemAddons::create($data_request);
            }

            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Addon Item has been added successfully',
                'data' => []
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
        }
        return $data;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('menumanagement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        $restaurantData = CommonFunctions::getRestaurantGroup();
        $data = ItemAddons::find($id);

        return view('menumanagement::ItemAddons.edit', compact('restaurantData', 'data'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
//    public function update(ItemAddonsRequest $request, $id) {
//        if (!isset($id) || !is_numeric($id)) {
//            return redirect()->back()->with('err_msg', 'Invalid Request');
//        }
//
//        DB::beginTransaction();
//        try {
//
//            $data = $request->all();
//            $data_request = [
//                'restaurant_id' => $data['restaurant_id'],
//                'group_name' => $data['group_name'],
//                'prompt' => $data['prompt'],
//                'language_id' => 1,
//                'is_required' => $data['is_required'],
//                'quantity_type' => $data['quantity_type'],
//            ];
//            if ($data['quantity_type'] == 'Value') {
//                $data_request['min'] = $data['quantity'];
//                $data_request['max'] = $data['quantity'];
//            } else {
//                $data_request['min'] = $data['min'];
//                $data_request['max'] = $data['max'];
//            }
//
//            ItemAddons::where('id', $id)->update($data_request);
//            DB::commit();
//            return back()->with('status', 'Item Modifier Group has been updated successfully.');
//        } catch (\Exception $e) {
//            DB::rollback();
//            return back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
//        }
//    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
        DB::beginTransaction();
        try {
            ItemAddons::where('id', $id)->delete();
            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Addon Item has been deleted successfully',
                'data' => []
            ];
            return $data;// back()->with('status', 'Item has been deleted successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return $data;//back()->withInput()->withErrors(array('status' => 'Something went wrong, please try later'));
        }
    }

}
