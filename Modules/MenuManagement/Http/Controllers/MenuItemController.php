<?php

namespace Modules\MenuManagement\Http\Controllers;

use App\Helpers\CommonFunctions;
use App\Models\Language;
use App\Models\MenuCategory;
use App\Models\MenuCategoryLanguage;
use App\Models\MenuMealType;
use App\Models\MenuMealTypePrice;
use App\Models\MenuSettingCategory;
use App\Models\MenuSettingItem;
use App\Models\MenuSubCategory;
use App\Models\Restaurant;
use App\Modesl\RestaurantPromotion;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Modules\MenuManagement\Entities\ItemAddonGroups;
use Modules\MenuManagement\Entities\ItemAddons;
use Modules\MenuManagement\Entities\ItemModifier;
use Modules\MenuManagement\Entities\ItemModifierGroup;
use Modules\MenuManagement\Entities\ItemModifierGroupLanguage;
use Modules\MenuManagement\Entities\ItemModifierLanguage;
use Modules\MenuManagement\Entities\Labels;
use Modules\MenuManagement\Entities\MenuItemLabels;
use Modules\MenuManagement\Entities\MenuItemModel;
use Modules\MenuManagement\Entities\MenuItemsLanguage;
use Modules\MenuManagement\Entities\ModifierCategories;

class MenuItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Menu List');
        $this->middleware('permission:Menu Edit', ['only' => ['updateprofile']]);
        $this->middleware('permission:Menu Status', ['only' => ['updateprofile']]);
        $this->middleware('permission:Menu Delete', ['only' => ['updateprofile']]);
        $this->middleware('permission:Menu Add', ['only' => ['updateprofile']]);
    }

    public function index(Request $request)
    {
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        #$groupRestData = Restaurant::group();
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $brandFlag = false;
        if(isset(current($groupRestData)['id']) && current($groupRestData)['id'] == Auth::user()->restaurant_id) {
            $brandFlag = true;
        }
        $menuItems = MenuItemModel::orderBy('menu_category_id', 'DESC')->whereIn('status', [1,0])->whereIn('restaurant_id', $locations)->orderBy('id', 'DESC');
        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuItems->where('restaurant_id', $searchArr['restaurant_id']);
            //$menuItems = MenuItemModel::where('restaurant_id',$searchArr['restaurant_id'])->orderBy('category_id','DESC')->orderBy('id','DESC')->paginate(20);
        }
        if ($request->input('name') && strlen($request->input('name')) <= 100) {
            $searchArr['name'] = $request->input('name');
            $menuItems->where('name', 'like', $searchArr['name'] . '%');
        }

        //$menuItems = $menuItems->where('product_type', 'food_item');// Get Product Type Food Item

        $menuItems = $menuItems->paginate(100000000);
        $adminFlag = $request->get('admin') ?? false;
        return view('menumanagement::menu_item.index', compact('menuItems', 'groupRestData', 'searchArr', 'adminFlag', 'brandFlag'));
    }

    public function product(Request $request)
    {
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        #$groupRestData = Restaurant::group();
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $menuItems = MenuItemModel::orderBy('menu_category_id', 'DESC')->whereIn('restaurant_id', $locations)->orderBy('id', 'DESC');
        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuItems->where('restaurant_id', $searchArr['restaurant_id']);
            //$menuItems = MenuItemModel::where('restaurant_id',$searchArr['restaurant_id'])->orderBy('category_id','DESC')->orderBy('id','DESC')->paginate(20);
        }
        if ($request->input('name') && strlen($request->input('name')) <= 100) {
            $searchArr['name'] = $request->input('name');
            $menuItems->where('name', 'like', $searchArr['name'] . '%');
        }

        $menuItems = $menuItems->where('product_type', 'product');// Get Product Type product

        $menuItems = $menuItems->paginate(20);
        return view('menumanagement::menu_item.index', compact('menuItems', 'groupRestData', 'searchArr', 'selected_rest_id'));
    }

    public function giftcard(Request $request)
    {
        $searchArr = array('restaurant_id' => 0, 'name' => NULL);
        #$groupRestData = Restaurant::group();
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $menuItems = MenuItemModel::orderBy('menu_category_id', 'DESC')->whereIn('restaurant_id', $locations)->orderBy('id', 'DESC');
        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuItems->where('restaurant_id', $searchArr['restaurant_id']);
            //$menuItems = MenuItemModel::where('restaurant_id',$searchArr['restaurant_id'])->orderBy('category_id','DESC')->orderBy('id','DESC')->paginate(20);
        }
        if ($request->input('name') && strlen($request->input('name')) <= 100) {
            $searchArr['name'] = $request->input('name');
            $menuItems->where('name', 'like', $searchArr['name'] . '%');
        }

        $menuItems = $menuItems->where('product_type', 'gift_card');// Get Product Type giftcard

        $menuItems = $menuItems->paginate(20);
        return view('menumanagement::menu_item.index', compact('menuItems', 'groupRestData', 'searchArr', 'selected_rest_id'));
    }

    public function giftCardDetail($id)
    {
        return view('gift_cards.detail');
    }

    public function create($language_id = 1)
    {
        #$groupRestData = Restaurant::group();
       // $groupRestData = CommonFunctions::getRestaurantGroup();
        $groupRestData = CommonFunctions::getRestaurantGroupAll(); //Requirement by Pranav to show all restaurants

        $selected_rest_id = 0;
        if(count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if(count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        }
	     $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
	$categoryData = MenuCategory::whereIn('restaurant_id', $locations)->get();    
        $product_types = config('constants.product_type');
        return view('menumanagement::menu_item.create', compact('groupRestData', 'selected_rest_id','product_types', 'language_id','categoryData'));
    }

    public function store(Request $request)
    {

        // $typeArr = array_values($request->input('type'));
        // $priceArr = array_values($request->input('price'));
        // $request->merge(array('type' => $typeArr));
        // $request->merge(array('price' => $priceArr));
        $rules = [
            'name' => 'required|max:100',
            'product_type' => 'required',
            'restaurant_id' => 'required|exists:restaurants,id',
          //  'language_id = 1' => 'required',
            'language_id' => 'required',
            'status' => 'required|in:0,1',
            //'image_caption' => 'required|max:100',
            'type' => 'required|array',
           // 'description' => 'required|max:250',
            //'image' => 'required',
            'size' => 'sometimes|nullable',
            'menu_category_id' => 'required|numeric|exists:menu_categories,id',
            'manage_inventory' => 'sometimes|nullable|in:0,1',
            'max_allowed_quantity' => 'nullable|required_if:manage_inventory,1|numeric',
            'min_allowed_quantity' => 'nullable|required_if:manage_inventory,1|numeric',
            'inventory' => 'nullable|required_if:manage_inventory,1|numeric',
            //'sub_category_id' => 'numeric|exists:menu_sub_categories,id',
            //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:'.config('constants.image.file_size'),
        ];
        $sizeCount = 0;
        if($request->input('size') && is_Array($request->input('size')) )
        {
            $sizeCount = count($request->input('size'));
            $sizeObj = $request->input('size');
             //size validation commneted on 13-09-2018 By RG as we do not require size for some item
            // foreach ($sizeObj as $k=>$v)
            // {
            //     $rules['size.'.$k] = 'required|max:100';
            //     $rules['type.'.$k] = 'required|in:0,1';
            // }
        }

        if($request->input('type') && is_Array($request->input('type')) )
        {
            $typeObj = $request->input('type');
            foreach ($typeObj as $k=>$v)
            {
                if($v=='1')
                {
                    $rules['price_all.'.$k] = 'required|numeric';
                }
                else if($v=='0')
                {
                    $rules['meal_type.'.$k] = 'required|array';
                }
            }
        }

        if($request->input('meal_type') && is_Array($request->input('meal_type')) )
        {
            $mealTypeObj = $request->input('meal_type');
            foreach ($mealTypeObj as $k=>$v)
            {
                foreach($v as $k1=>$v1)
                {
                    $rules['price.'.$k.'.'.$k1] = 'required|numeric';
                }
            }
        }

        if($request->input('price'))
        {
            $priceObj = $request->input('price');
            foreach($priceObj as $k => $v){
                foreach($v as $k1 => $v1) {
                    if(!is_null($v1) && trim($v1)!='')
                        $rules['meal_type.' . $k.'.'.$k1] = 'required|numeric';
                }
            }
        }
        if($request->input('meal_type'))
        {
            $mealTypeObj = $request->input('meal_type');
            foreach($mealTypeObj as $k => $v){
                foreach($v as $k1 => $v1) {
                    $rules['price.' . $k.'.'.$k1] = 'required|numeric';
                }
            }
        }
        $rulesMsg = [
            'name.*' => 'Please enter valid name upto 100 chars',
            'product_type' => 'Please select Product Type',
            'restaurant_id.*' => 'Please select Restaurant',
            'status.*' => 'Please select Status',
            'image_caption.*' => 'Please enter Image Caption upto 100 chars',
            'meal_type.*' => 'Please select Meal Type',
            'image.image' => 'Please select valid Image',
            'image.mimes' => 'Please select valid Image Type',
            'image.max' => 'Please select size up to'.config('constants.image.file_size'),
            'image.*' => 'Please select Image',
            'price.*' => 'Please enter Price',
            'price_all.*' => 'Please enter numeric Price',
            'type.*' => 'Please select Price Type',
            //'description.required' => 'Please enter Description',
           // 'description.max' => 'Please enter Description upto 250 chars',
            'size.distinct' => 'Sizes cannot be same',
            'size.*' => 'Please enter size upto 100 chars'
        ];

        $validator = Validator::make(Input::all(), $rules, $rulesMsg);
        if ($validator->passes())
        {
            $img_err = 0;
            $max_file_size = config('constants.image.file_size');
            $allowed_image_extension = array("png", "jpg", "jpeg", "gif","svg");
            $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
            if ($request->hasFile('image')) {
                $imageCaptionArr = $request->input('image_caption');
                foreach ($request->file('image') as $k => $image) {
                    if (!file_exists($image->getRealPath())) {
                        $validator->errors()->add('image', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('image', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('image', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
                    else if(!isset($imageCaptionArr[$k]))
                    {
                        $validator->errors()->add('image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(trim($imageCaptionArr[$k])=='' ){
                        $validator->errors()->add('image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(strlen(trim($imageCaptionArr[$k]))>100){
                        $validator->errors()->add('image_caption', 'Please add Image Caption upto 100 chars.');
                        $img_err = 1;
                    }
                }
            }
            if ($request->hasFile('web_image')) {
                $imageCaptionArr = $request->input('web_image_caption');
                foreach ($request->file('web_image') as $k => $image) {
                    if (!file_exists($image->getRealPath())) {
                        $validator->errors()->add('image', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('web_image_caption', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('web_image_caption', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
                    else if(!isset($imageCaptionArr[$k]))
                    {
                        $validator->errors()->add('web_image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(trim($imageCaptionArr[$k])=='' ){
                        $validator->errors()->add('web_image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(strlen(trim($imageCaptionArr[$k]))>100){
                        $validator->errors()->add('web_image_caption', 'Please add Image Caption upto 100 chars.');
                        $img_err = 1;
                    }
                }
            }
            if ($request->hasFile('mobile_web_image')) {
                $imageCaptionArr = $request->input('mobile_web_image_caption');
                foreach ($request->file('mobile_web_image') as $k => $image) {
                    if (!file_exists($image->getRealPath())) {
                        $validator->errors()->add('image', 'Please select Image.');
                        $img_err = 1;
                    } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types)) {
                        $validator->errors()->add('mobile_web_image_caption', 'Please select valid Image.');
                        $img_err = 1;
                    } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                        $validator->errors()->add('mobile_web_image_caption', 'Please upload image up to ' . $max_file_size . ' KB.');
                        $img_err = 1;
                    }
                    else if(!isset($imageCaptionArr[$k]))
                    {
                        $validator->errors()->add('mobile_web_image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(trim($imageCaptionArr[$k])=='' ){
                        $validator->errors()->add('mobile_web_image_caption', 'Please add Image Caption.');
                        $img_err = 1;
                    }
                    else if(strlen(trim($imageCaptionArr[$k]))>100){
                        $validator->errors()->add('mobile_web_image_caption', 'Please add Image Caption upto 100 chars.');
                        $img_err = 1;
                    }
                }
            }
            if($img_err == 0) {
                $itemData = ['restaurant_id' => $request->input('restaurant_id'),
                    'menu_category_id' => $request->input('menu_category_id'),
                    'menu_sub_category_id' => $request->input('menu_sub_category_id'),
                    'name' => $request->input('name'),
                    'product_type' => $request->input('product_type'),
                    'status' => $request->input('status'),
                    'description' =>!empty($request->input('description'))?$request->input('description'):'',
                    'sub_description' =>!empty($request->input('sub_description'))?$request->input('sub_description'):'',
                    'price_flag' => $request->input('type')
                ];

                $sizeObj = $request->input('size');
                $typeObj = $request->input('type');
                $priceObj = $request->input('price');
                $priceAllObj = $request->input('price_all');

                $cnt = 0;
                $sizePriceArr = array();
                foreach($sizeObj as $k=>$v)
                {
                    if($typeObj[$k]=="1")
                    {
                        $priceTmpArr = $availableTmpArr = array();
                        $sizePriceArr[$cnt]['size'] = $v;
                        $priceTmpArr[0] = $priceAllObj[$k];
                        //$sizePriceArr[$cnt]['price'] = $priceAllObj[$k];
                        //$sizePriceArr[$cnt]['menu_meal_type_id'] = 0;
                        //$cnt++;
                        if($request->has('carryout_all') && isset($request->input('carryout_all')[$k])) {
                            $availableTmpArr['is_carryout'] = 1;
                        }else {
                            $availableTmpArr['is_carryout'] = 0;
                        }

                        //set is_delivery and is_carrayout flag
                        if($request->has('delivery_all') && isset($request->input('delivery_all')[$k])) {
                            $availableTmpArr['is_delivery'] = 1;
                        }else {
                            $availableTmpArr['is_delivery'] = 0;
                        }
                    }
                    else
                    {
                        $mealTypeObj = array();
                        if($request->input('meal_type'))
                        {
                            if(isset($request->input('meal_type')[$k]))
                                $mealTypeObj = $request->input('meal_type');
                        }
                        $sizePriceArr[$cnt]['size'] = $v;
                        $priceTmpArr = array(); //echo "<pre>";print_r($request->all());die;
                        foreach($mealTypeObj[$k] as $k1=>$v1)
                        {

                            $priceTmpArr[$v1] = $priceObj[$k][$k1];
                            //$sizePriceArr[$cnt]['price'] = $priceObj[$k][$k1];
                            //$sizePriceArr[$cnt]['menu_meal_type_id'] = $v1;

                            if($request->has('carryout') && isset($request->input('carryout')[$k]) && array_key_exists($k1, $request->input('carryout')[$k])) {
                                $availableTmpArr[$v1]['is_carryout'] = 1;
                            }else {
                                $availableTmpArr[$v1]['is_carryout'] = 0;
                            }

                            if($request->has('delivery') && isset($request->input('delivery')[$k]) && array_key_exists($k1, $request->input('delivery')[$k])) {
                                 $availableTmpArr[$v1]['is_delivery'] = 1;
                            }else {
                                $availableTmpArr[$v1]['is_delivery'] = 0;
                            }
                        }
                    }
                    $sizePriceArr[$cnt]['price'] = (array) $priceTmpArr;
                    $sizePriceArr[$cnt]['available'] = (array)$availableTmpArr;
                    $cnt++;
                }
                // echo "<pre>";
                // print_r($sizePriceArr);
                // exit;
                $itemData['size_price'] = json_encode($sizePriceArr);

                $itemData['gift_wrapping_fees']=$request->input('gift_wrapping_fees')?$request->input('gift_wrapping_fees'):NULL;
                $itemData['gift_message']=$request->input('gift_message')?$request->input('gift_message'):NULL;
                $itemData['display_order']=$request->input('display_order')?$request->input('display_order'):0;
                $itemData['is_popular']=$request->input('is_popular')?$request->input('is_popular'):0;
                $itemData['is_favourite']=$request->input('is_favourite')?$request->input('is_favourite'):0;
                $itemData['manage_inventory']=$request->input('manage_inventory')?$request->input('manage_inventory'):0;
                $itemData['max_allowed_quantity']=$request->input('max_allowed_quantity')?$request->input('max_allowed_quantity'):0;
                $itemData['min_allowed_quantity']=$request->input('min_allowed_quantity')?$request->input('min_allowed_quantity'):0;
                $itemData['inventory']=$request->input('inventory')?$request->input('inventory'):0;


                $menuItemObj = MenuItemModel::create($itemData);



                if (isset($menuItemObj->id) && $request->input('type')) {

                    // image upload
                    $imageArr = [];
                    $imageJsonArray = [];
                    $imageRelPath = config('constants.image.rel_path.menu');
                    $imagePath = config('constants.image.path.menu');

                    $curr_restaurant_name=strtolower(str_replace(' ','-',$menuItemObj->Restaurant->restaurant_name));
                    $imagePath=$imagePath.DIRECTORY_SEPARATOR.$curr_restaurant_name;
                    $imageRelPath=$imageRelPath.$curr_restaurant_name. DIRECTORY_SEPARATOR;

                    (! File::exists($imagePath) ?File::makeDirectory($imagePath):'');
                    (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'thumb') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'thumb' ):'');

                    if ($request->hasFile('image')) {

                        $mobile_app_images=[];
                        $i = 0;
                        $imageCaptionArr = $request->input('image_caption');

                        foreach ($request->file('image') as $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                             $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);

                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                            //$image->move($imagePath, $imageName);
                            $imageArr[] = [
                                'image' => $imageRelPath . $imageName,
                                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'image_caption' => $imageCaptionArr[$i],
                            ];

                            $mobile_app_images[]= [
                                'image' => $imageRelPath . $imageName,
                                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'image_caption' => $imageCaptionArr[$i],
                            ];

                            /* Old field for saving mobile app images
                            if (count($imageArr) > 0) {
                                MenuItemModel::where('id', $menuItemObj->id)
                                    ->update($imageArr[0]);
                            }
                            */
                            $i++;
                        }

                        $imageJsonArray['mobile_app_images']=$mobile_app_images;

                    }
                    // Desktop images
                    if ($request->hasFile('web_image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('web_image_caption');
                       $web_images=[];
                        foreach ($request->file('web_image') as $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                              $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();


                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);

                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                            //$image->move($imagePath, $imageName);
                            $imageWebArr[] = [
                                'web_image' => $imageRelPath . $imageName,
                                'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'web_thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'web_image_caption' => $imageCaptionArr[$i],
                            ];
                             $web_images[]= [
                                'web_image' => $imageRelPath . $imageName,
                                'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'web_thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'web_image_caption' => $imageCaptionArr[$i],
                            ];
                            /*Old field for saving web images
                            if (count($imageWebArr) > 0) {
                                MenuItemModel::where('id', $menuItemObj->id)
                                    ->update($imageWebArr[0]);
                            }*/
                            $i++;
                        }

                        $imageJsonArray['desktop_web_images']=$web_images;

                    }
                    // mobile web
                    if ($request->hasFile('mobile_web_image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('mobile_web_image_caption');
                        $mobile_web_images=[];
                        foreach ($request->file('mobile_web_image') as $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                            $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                            //$image->move($imagePath, $imageName);
                            $imageMobWebArr[] = [
                                'mobile_web_image' => $imageRelPath . $imageName,
                                'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'mobile_web_thumb_sml' =>$imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'mobile_web_image_caption' => $imageCaptionArr[$i],
                            ];
                             $mobile_web_images[]= [
                                'mobile_web_image' => $imageRelPath . $imageName,
                                'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'mobile_web_thumb_sml' =>$imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'mobile_web_image_caption' => $imageCaptionArr[$i],
                            ];

                            /*Old field for saving mobile web images
                            if (count($imageMobWebArr) > 0) {
                                MenuItemModel::where('id', $menuItemObj->id)
                                    ->update($imageMobWebArr[0]);
                            }
                            */
                            $i++;
                        }
                         $imageJsonArray['mobile_web_images']=$mobile_web_images;

                    }

                    MenuItemModel::where('id', $menuItemObj->id)
                                    ->update(['images'=>json_encode($imageJsonArray)]);
                    // CUSTOMIZABLE MENU DATA >>>
                    $menuItemArr = [];
                    $restId = $request->input('restaurant_id');
                    $catId = $request->input('menu_category_id');
                    $menuSetCat = MenuSettingCategory::select('id', 'name', 'is_multi_select','maximum_selection','minimum_selection','equal_selection')->where(['status' => 1, 'restaurant_id' => $restId,'menu_category_id' => $catId])->get()->toArray();

                    foreach($menuSetCat as $menuSet) {
                        $isMandatory = 0;
                        $menuSetName = str_replace(' ', '_', $menuSet['name']);
                        if(isset($_POST[$menuSet['id'].'_is_mandatory'])) {
                            $isMandatory = 1;
                        }

                        if(isset($_POST[$menuSetName]) && count($_POST[$menuSetName])) {
                            // get price of Customization setting item
                            $menuSetItemDBArr = [];
                            $res = MenuSettingItem::select('id','name','price')->whereIn('id', $_POST[$menuSetName])->get()->toArray();
                            array_walk($res, function($item) use (&$menuSetItemDBArr) { $menuSetItemDBArr[$item['id']] = $item; });

                            // check ids in POST menuSetName
                            foreach($request->input($menuSetName) as $menuSetItemId) {
                                $quantArr = [];
                                $sidesArr = [];
                                $defaultSelected = 0;
                                foreach(config('constants.sides') as $side) {
                                    if(isset($_POST[$menuSetItemId.'_'.$side])) {
                                        $sidesArr[] = ['label' => $side, 'is_selected' => 1];
                                    }
                                }
                                if(!count($sidesArr)) {
                                    $sidesArr = null;
                                }
                                foreach(config('constants.quantity') as $quant) {
                                    if(isset($_POST[$menuSetItemId.'_'.$quant])) {
                                        $quantArr[] = ['label'=>$quant, 'is_selected' => 1];
                                    }
                                }
                                if(!count($quantArr)) {
                                    $quantArr = null;
                                }
                                if(isset($_POST[$menuSetItemId.'_default_selected'])) {
                                    $defaultSelected = 1;
                                }
                                $settingExistsIndex = array_search($menuSet['id'], array_column($menuItemArr, 'id'));
                                if($settingExistsIndex === false) {
                                    $menuItemArr[] = [
                                        'id'              => $menuSet['id'],
                                        'label'           => $menuSet['name'],
                                        'is_multi_select' => $menuSet['is_multi_select'],
                                        'maximum_selection' => $menuSet['maximum_selection'],
                                        'minimum_selection' => $menuSet['minimum_selection'],
                                        'equal_selection' => $menuSet['equal_selection'],
                                        'is_mandatory'    => $isMandatory,
                                        'items'           => [
                                            [
                                                'id'               => $menuSetItemId,
                                                'label'            => $menuSetItemDBArr[$menuSetItemId]['name'],
                                                'default_selected' => $defaultSelected,
                                                'quantity'         => $quantArr,
                                                'sides'            => $sidesArr,
                                                'price'            => $menuSetItemDBArr[$menuSetItemId]['price'],
                                            ],
                                        ],
                                    ];
                                } else {
                                    $menuItemArr[$settingExistsIndex]['items'][] = [
                                        'id'               => $menuSetItemId,
                                        'label'            => $menuSetItemDBArr[$menuSetItemId]['name'],
                                        'default_selected' => $defaultSelected,
                                        'quantity'         => $quantArr,
                                        'sides'            => $sidesArr,
                                        'price'            => $menuSetItemDBArr[$menuSetItemId]['price'],
                                    ];
                                }
                            }
                        }
                    }
                    $customizableData = [
                        'is_customizable'   => count($menuItemArr) ? 1 : 0,
                        'customizable_data' => json_encode($menuItemArr)
                    ];
                    MenuItemModel::where('id', $menuItemObj->id)
                        ->update($customizableData);

                    $newMenuItemObj = MenuItemModel::where('id', $menuItemObj->id)->first();
                    $fillables = [
                        'new_menu_items_id' => $newMenuItemObj->id,
                        'item_other_info' => $newMenuItemObj->item_other_info,
                        'gift_message' => $newMenuItemObj->gift_message,
                        'name' => $newMenuItemObj->name,
                        'description' => $newMenuItemObj->description,
                        'sub_description' => $newMenuItemObj->sub_description,
                        'language_id' => ((int)$request->input('language_id') ? $request->input('language_id') : 1)
                    ];

                    MenuItemsLanguage::create($fillables);
                    // <<< CUSTOMIZABLE MENU DATA

                    // menu meal type price
                   /* if($request->input('type')=="1")
                    {
                        $itemPriceData = ['menu_meal_type_id' => 0, 'menu_item_id' => $menuItemObj->id, 'price' => trim($request->input('price_all')),'all_flag'=>$request->input('type')];
                        MenuMealTypePrice::create($itemPriceData);
                    }
                    else {
                        $mealTypeArr = $request->input('meal_type');
                        $priceArr = $request->input('price');
                        foreach ($mealTypeArr as $k => $v) {
                            $itemPriceData = ['menu_meal_type_id' => $v, 'menu_item_id' => $menuItemObj->id, 'price' => $priceArr[$k],'all_flag'=>$request->input('type')];
                            MenuMealTypePrice::create($itemPriceData);
                        }
                    } */
                }
                return  redirect('/menu/item/'.$menuItemObj->id.'/edit')->with('message', 'Menu Item added successfully');
               // return Redirect::back()->with('message', 'Menu Item added successfully');
            }
        }
        $categoryData = array();
        $menuTypeData = array();
        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id')))
        {
            $categoryData = MenuCategory::where('restaurant_id', '=',trim($request->input('restaurant_id')))->get();
            $menuTypeData = MenuMealType::where('restaurant_id', '=',trim($request->input('restaurant_id')))->get();
        }

        //dd($sizeCount);
        return redirect()->back()->withErrors($validator->errors())->withInput()
            ->with('categoryData',$categoryData)
            ->with('menuTypeData',$menuTypeData)
            ->with('sizeCount',$sizeCount);
    }

    public function edit(Request $request,$id, $languageId = 1)
    {
        if(isset($id) && is_numeric($id)) {
            $categoryData = $menuTypeData = $menuTypePriceData = $mealPriceData = array();
            #$groupRestData = Restaurant::group();
           // $groupRestData = CommonFunctions::getRestaurantGroup();
            $groupRestData = CommonFunctions::getRestaurantGroupAll(); //Requirement by Pranav to show all restaurants

            $menuItem = MenuItemModel::find($id);

            $menuItemLangObj = MenuItemsLanguage::where([
                'new_menu_items_id' => $menuItem->id,
                'language_id' => $languageId])->first();

            if ($menuItemLangObj !== null) {
                $menuItem->item_other_info = $menuItemLangObj->item_other_info;
                $menuItem->gift_message = $menuItemLangObj->gift_message;
                $menuItem->name = $menuItemLangObj->name;
                $menuItem->description = $menuItemLangObj->description;
                $menuItem->sub_description = $menuItemLangObj->sub_description;
            }

            $ItemModifierGroup = ItemModifierGroup::orderBy('id','DESC')->where('menu_item_id',$id)->orderBy('sort_order', 'ASC')->get();
 	    //print_r($ItemModifierGroup);die;
            if(count($ItemModifierGroup)) {
                foreach ($ItemModifierGroup as &$row) {
                    if (isset($row->itemmodifiers)) {
                        foreach ($row->itemmodifiers as &$itemmodifiers) {
                            $itemmodifierslang = ItemModifierLanguage::where('modifier_item_id', $itemmodifiers->id)->get();
                            $itemmodifiers->itemModifierLanguage = $itemmodifierslang;
                        }
                    }
                }
            }
//             dd($ItemModifierGroup);
            $ModifierCategories = ModifierCategories::orderBy('id','DESC')->where('restaurant_id',$menuItem->restaurant_id)->get();


            $menuTypeData = $menuTypePriceData = $categoryData = array();
            if(!is_null($menuItem->size_price) && $menuItem->size_price!='') {
                $mealPriceData = json_decode($menuItem->size_price,true);
            }
            if(!is_null($menuItem->customizable_data) && $menuItem->customizable_data!='') {
                $menuItem->customizable_data = json_decode($menuItem->customizable_data,true);
            }
            if (!empty($menuItem)) {
                $categoryData = MenuCategory::where('restaurant_id', '=', $menuItem->restaurant_id)->orderBy('name', 'DESC')->get();
                $subCategoryData = MenuSubCategory::select('id','name', 'priority')
                                        ->where(['restaurant_id' => $menuItem->restaurant_id, 'menu_category_id' => $menuItem->menu_category_id, 'status' => 1])
                                        ->orderBy('priority')->get();

                $subcat_options= app('App\Http\Controllers\MenuSubcategoryController')->getSubCatHtml($menuItem->menu_category_id,$menuItem->restaurant_id,0,NULL,$menuItem->menu_sub_category_id);


                $menuTypeData = MenuMealType::where('restaurant_id', '=', $menuItem->restaurant_id)->orderBy('name')->get();
                $menuTypePriceData = MenuMealTypePrice::where('menu_item_id', '=', $menuItem->id)->orderBy('id', 'DESC')->get();
                /*foreach ($menuTypePriceData as $k => $v) {
                    $mealPriceData[$v->menu_meal_type_id] = $v->price;
                }*/
            }

            // MENU CUSTOMISATION SETTING >>>
            $restId = $menuItem->restaurant_id;
            $menuSetCat = MenuSettingCategory::select('id', 'name', 'is_multi_select')->where(['status' => 1, 'restaurant_id' => $restId, 'menu_category_id' => $menuItem->menu_category_id])->get()->toArray();
            $menuSetCatIds = array_column($menuSetCat, 'id');
            $menuSetItems = MenuSettingItem::select('id', 'menu_category_id', 'menu_setting_category_id', 'name', 'side_flag', 'quantity_flag')
                                ->whereIn('menu_setting_category_id', $menuSetCatIds)->where(['restaurant_id' => $restId, 'status' => 1])->get()->toArray();
            $menuSettingData = [];
            foreach($menuSetCat as $menuCat) {
                $res = array_filter($menuSetItems, function ($i) use ($menuCat) {
                    if($i['menu_setting_category_id'] == $menuCat['id']) {
                        return true;
                    }
                    return false;
                });
                $menuCat['items'] = array_values($res);
                array_push($menuSettingData, $menuCat);
            }
            $hriSides = config('constants.sides');
            $hriQuant = config('constants.quantity');
            // <<< MENU CUSTOMISATION SETTING
         //   echo "<pre>";print_r($mealPriceData);die;
            $product_types = config('constants.product_type');
            $item_id=$id;

            // Labels
            $allLabels = Labels::select('id', 'name', 'type')->get();
            $allLabelsData = [];
            if($allLabels) {
                //$allLabelsData = $allLabels->toArray();
                foreach($allLabels as $label) {
                    $allLabelsData[$label->type][] = ['id' => $label->id,'name' => $label->name,'priority'=>0];
                }
            }
            // Menu selected labels
            $menuLabelData = MenuItemLabels::select('id','labels','size','labels_with_priority')->where('item_id', $id)->first();
            $menuLabels = $servingSize = [];
	    $priorArray = null;	
            if($menuLabelData) {
                $menuLabels = isset($menuLabelData->labels) ? explode(',', $menuLabelData->labels) : [];
	        $menuLabelsPriority = isset($menuLabelData->labels_with_priority) ? json_decode($menuLabelData->labels_with_priority) : null;
                $servingSize = isset($menuLabelData->size) ? explode(',', $menuLabelData->size) : [];
		if($menuLabelsPriority!=null){
			foreach($menuLabelsPriority as $pArray){
			  $priorArray[$pArray[1]] = $pArray[0];
			}
		}
            }
	    if($priorArray!=null){
	    foreach($allLabelsData as $type=>$labels) {
		foreach($labels as $index=>$label){
		  $fid = $label['id'];
		 //$priority = isset($priorArray[$fid])?$priorArray[$fid]:0
		  $allLabelsData[$type][$index]['priority']=isset($priorArray[$fid])?$priorArray[$fid]:0;
		}
	    }} //echo"<pre>";print_r($allLabelsData);die;
            $food_types = ['veg', 'non-veg'];
	    $modifierOptions = DB::select('select * from modifier_options where menu_item_id = :id', ['id' => $id]);
            return view('menumanagement::menu_item.edit', compact('groupRestData','menuItem','categoryData',
                'subCategoryData','subcat_options','menuTypeData','menuTypePriceData','mealPriceData','menuSettingData', 'food_types',
                'hriSides', 'hriQuant','product_types','item_id','ItemModifierGroup','ModifierCategories', 'languageId','allLabelsData',
                'menuLabels','servingSize','modifierOptions'));
             
        }
        return Redirect::back();
    }
    public function unsetDeleted($keys,$arr){
        $allkeys=array_keys($arr);

        foreach($allkeys as $val){

            if (!in_array($val, $keys)){

                 unset($arr[$val]);
            }
        }

     return array_values($arr);

    }


   public function imagesupdate(Request $request,$id)
    {
       if(!isset($id) || !is_numeric($id)){
            return redirect()->back()->with('err_msg','Invalid Request');
        }

        DB::beginTransaction();
        try {

            $menuItemObj= MenuItemModel::find($id);
            $imageRelPath = config('constants.image.rel_path.menu');
            $imagePath = config('constants.image.path.menu');

            $curr_restaurant_name=strtolower(str_replace(' ','-',$menuItemObj->Restaurant->restaurant_name));
            $imagePath=$imagePath.DIRECTORY_SEPARATOR.$curr_restaurant_name;
            $imageRelPath=$imageRelPath.$curr_restaurant_name. DIRECTORY_SEPARATOR;

            (! File::exists($imagePath) ?File::makeDirectory($imagePath):'');
            (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'thumb') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'thumb' ):'');

            $allimages=json_decode($menuItemObj->images,true);
            $imageJsonArray = $allimages;
            $mobile_app_images=isset($allimages['mobile_app_images'])?$allimages['mobile_app_images']:[];
            $web_images=isset($allimages['desktop_web_images'])?$allimages['desktop_web_images']:[];
            $mobile_web_images=isset($allimages['mobile_web_images'])?$allimages['mobile_web_images']:[];

            $imageHidden = !empty($request->input('image_hidden'))?$request->input('image_hidden'):[];
            $mobile_app_images=$this->unsetDeleted($imageHidden,$mobile_app_images);
            $imageJsonArray['mobile_app_images']=$mobile_app_images;

             $webImageHidden = !empty($request->input('web_image_hidden'))?$request->input('web_image_hidden'):[];
            $web_images=$this->unsetDeleted($webImageHidden,$web_images);
            $imageJsonArray['desktop_web_images']=$web_images;

            $mobileWebImageHidden = !empty($request->input('mobile_web_image_hidden'))?$request->input('mobile_web_image_hidden'):[];
            $mobile_web_images=$this->unsetDeleted($mobileWebImageHidden,$mobile_web_images);
            $imageJsonArray['mobile_web_images']=$mobile_web_images;

            /* Mobile app Image save*/
            if ($request->hasFile('image')) {
                $i = 0;
                $imageCaptionArr = $request->input('image_caption');
                foreach ($request->file('image') as $k => $image) {
                    $uniqId = uniqid(mt_rand());
                    $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                    $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                    $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

                    $image = Image::make($image->getRealPath());
                    $width = $image->width();
                    $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                    $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);

                    $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                    //$image->move($imagePath, $imageName);
                    $imageArr[] = [
                        'image' => $imageRelPath . $imageName,
                        'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                        'image_caption' => $imageCaptionArr[$k],
                    ];

                    $mobile_app_images[]= [
                        'image' => $imageRelPath . $imageName,
                        'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                        'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                        'image_caption' => $imageCaptionArr[$k],
                    ];
                    /* comment old image entry
                    if (count($imageArr) > 0) {
                        MenuItemModel::where('id', $menuItemObj->id)
                            ->update($imageArr[0]);
                    }
                    */
                    $i++;
                }
                $mobile_app_images = array_values($mobile_app_images);
                $imageJsonArray['mobile_app_images']=$mobile_app_images;
            }
            /*Desktop Image save*/
            if ($request->hasFile('web_image')) {
                $i = 0;
                $imageCaptionArr = $request->input('web_image_caption');


                foreach ($request->file('web_image') as $k => $image) {
                    $uniqId = uniqid(mt_rand());
                    $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                    $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                    $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();
                    $image = Image::make($image->getRealPath());
                    $width = $image->width();

                    $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                    $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                    $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);
                    //$image->move($imagePath, $imageName);
                    $imageWebArr[] = [
                        'web_image' => $imageRelPath . $imageName,
                        'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                        'web_thumb_sml' => $imageCaptionArr[$k],
                    ];
                   $web_images[]= [
                        'web_image' => $imageRelPath . $imageName,
                        'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                        'web_thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                        'web_image_caption' => $imageCaptionArr[$k],
                    ];
                     /* comment old image entry
                    if (count($imageWebArr) > 0) {
                        MenuItemModel::where('id', $menuItemObj->id)
                            ->update($imageWebArr[0]);
                    }
                    */
                    $i++;
                }
                $web_images = array_values($web_images);
                 $imageJsonArray['desktop_web_images']=$web_images;
            }
              /*Mobile web Image save*/
            if ($request->hasFile('mobile_web_image')) {
                $i = 0;
                $imageCaptionArr = $request->input('mobile_web_image_caption');

                foreach ($request->file('mobile_web_image') as $k => $image) {
                    $uniqId = uniqid(mt_rand());
                    $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                    $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                     $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();
                    $image = Image::make($image->getRealPath());
                    $width = $image->width();
                    $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                    $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);

                    $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                    //$image->move($imagePath, $imageName);
                    $imageMobWebArr[] = [
                        'mobile_web_image' => $imageRelPath . $imageName,
                        'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                        'mobile_web_thumb_sml' => $imageCaptionArr[$k],
                    ];
                     $mobile_web_images[$k]= [
                        'mobile_web_image' => $imageRelPath . $imageName,
                        'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                        'mobile_web_thumb_sml' =>$imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                        'mobile_web_image_caption' => $imageCaptionArr[$k],
                    ];
                    /*
                    if (count($imageMobWebArr) > 0) {
                        MenuItemModel::where('id', $menuItemObj->id)
                            ->update($imageMobWebArr[0]);
                    }
                    */
                    $i++;
                }
                $mobile_web_images = array_values($mobile_web_images);
                 $imageJsonArray['mobile_web_images']=$mobile_web_images;

            }

            $menuItemObj->images=json_encode($imageJsonArray);
            $menuItemObj->save();

            DB::commit();
            return back()->with('section_tab','images')->with('status_msg','Menu Item images updated successfully');

        } catch (\Exception $e) {
            DB::rollback();
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));

        }

     }
    public function update(Request $request,$id)
    { //echo "<pre>";print_r($request->all());die;
        // $typeArr = array_values($request->input('type'));
        // $priceArr = array_values($request->input('price'));
        // $request->merge(array('type' => $typeArr));
        // $request->merge(array('price' => $priceArr));

        if(isset($id) && is_numeric($id)) {
            $rules = [
                'name' => 'required|max:100',
                'product_type' => 'required',
                'restaurant_id' => 'required|exists:restaurants,id',
                'status' => 'required|in:0,1',
                'is_customizable' => 'required|in:0,1',
                //'image_caption' => 'required|max:100',
                'type' => 'required|array',
               // 'description' => 'required|max:250',
                'size' => 'sometimes|nullable',
                'menu_category_id' => 'required|numeric|exists:menu_categories,id',
                //'sub_category_id' => 'numeric|exists:menu_sub_categories,id',
                'manage_inventory' => 'sometimes|in:0,1',
                'max_allowed_quantity' => 'nullable|required_if:manage_inventory,1|numeric',
                'min_allowed_quantity' => 'nullable|required_if:manage_inventory,1|numeric',
                'inventory' => 'nullable|required_if:manage_inventory,1|numeric',
                'image_delete' => 'sometimes|nullable|in:1'
                //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:'.config('constants.image.file_size'),
            ];

            $sizeCount = 0;
            if ($request->input('size') && is_Array($request->input('size'))) {
                $sizeCount = count($request->input('size'));
                $sizeObj = $request->input('size');
                //size validation commneted on 13-09-2018 By RG as we do not require size for some item
                // foreach ($sizeObj as $k => $v) {
                //     $rules['size.' . $k] = 'required|max:100';
                //     $rules['type.' . $k] = 'required|in:0,1';
                // }
            }

            if ($request->input('meal_type') && is_Array($request->input('meal_type'))) {
                $mealTypeObj = $request->input('meal_type');
                foreach ($mealTypeObj as $k => $v) {
                    foreach ($v as $k1 => $v1) {
                        $rules['price.' . $k . '.' . $k1] = 'required|numeric';
                    }
                }
            }

            if ($request->input('price')) {
                $priceObj = $request->input('price');
                foreach ($priceObj as $k => $v) {
                    foreach ($v as $k1 => $v1) {
                        if (!is_null($v1) && trim($v1) != '')
                            $rules['meal_type.' . $k . '.' . $k1] = 'required|numeric';
                    }
                }
            }

            if ($request->input('type') && is_Array($request->input('type'))) {
                $typeObj = $request->input('type');
                foreach ($typeObj as $k => $v) {
                    if ($v == '1') {
                        $rules['price_all.' . $k] = 'required|numeric';
                    } else if ($v == '0') {
                        $rules['meal_type.' . $k] = 'required|array';
                    }
                }
            }
            if($request->file('image'))
            {
                foreach ($request->file('image') as $k => $v)
                {
                    $rules['image_caption.' . $k] = 'required|max:100';
                }
            }
            $rulesMsg = [
                'name.*' => 'Please enter valid name upto 100 chars',
                'product_type' => 'Please select Product Type',
                'restaurant_id.*' => 'Please select Restaurant',
                'status.*' => 'Please select Status',
                'image_caption.*' => 'Please enter Image Caption upto 100 chars',
                'meal_type.*' => 'Please select Meal Type',
                'image.image' => 'Please select valid Image',
                'image.mimes' => 'Please select valid Image Type',
                'image.max' => 'Please select size up to' . config('constants.image.file_size'),
                'image.*' => 'Please select Image',
                'price.*' => 'Please enter Price',
                'price_all.*' => 'Please enter numeric Price',
                'type.*' => 'Please select Price Type',
              //  'description.required' => 'Please enter Description',
                //'description.max' => 'Please enter Description upto 250 chars',
                'size.distinct' => 'Sizes cannot be same',
                'size.*' => 'Please enter size upto 100 chars'
            ];
            $validator = Validator::make(Input::all(), $rules, $rulesMsg);
            if ($validator->passes()) {
                $img_err = 0;
                if ($request->hasFile('image')) {
                    $max_file_size = config('constants.image.file_size');
                    $allowed_image_extension = array("png", "jpg", "jpeg", "gif");
                    $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
                    foreach ($request->file('image') as $image) {
                        if (!file_exists($image->getRealPath())) {
                            $validator->errors()->add('image', 'Please select Image.');
                            $img_err = 1;
                        } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types))  {
                            $validator->errors()->add('image', 'Please select valid Image.');
                            $img_err = 1;
                        } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('image', 'Please upload image up to ' . $max_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                }
                if ($request->hasFile('web_image')) {
                    $max_file_size = config('constants.image.file_size');
                    $allowed_image_extension = array("png", "jpg", "jpeg", "gif");
                    $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
                    foreach ($request->file('web_image') as $image) {
                        if (!file_exists($image->getRealPath())) {
                            $validator->errors()->add('image', 'Please select Image.');
                            $img_err = 1;
                        } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types))  {
                            $validator->errors()->add('image', 'Please select valid Image.');
                            $img_err = 1;
                        } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('image', 'Please upload image up to ' . $max_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                }
                if ($request->hasFile('mobile_web_image')) {
                    $max_file_size = config('constants.image.file_size');
                    $allowed_image_extension = array("png", "jpg", "jpeg", "gif");
                    $allowed_mime_types = array("image/png", "image/jpeg", "image/gif");
                    foreach ($request->file('mobile_web_image') as $image) {
                        if (!file_exists($image->getRealPath())) {
                            $validator->errors()->add('image', 'Please select Image.');
                            $img_err = 1;
                        } else if (!in_array($image->getClientOriginalExtension(), $allowed_image_extension) || !in_array($image->getMimeType(), $allowed_mime_types))  {
                            $validator->errors()->add('image', 'Please select valid Image.');
                            $img_err = 1;
                        } else if (($image->getClientSize() / 1024) > config('constants.image.file_size')) {
                            $validator->errors()->add('image', 'Please upload image up to ' . $max_file_size . ' KB.');
                            $img_err = 1;
                        }
                    }
                }
                if ($img_err == 0) { //echo "<pre>";print_r($request->all());die;
                    $sizeObj = $request->input('size');
                    $typeObj = $request->input('type');
                    $priceObj = $request->input('price');
                    $priceAllObj = $request->input('price_all');
                    $posObj      = $request->input('size_pos_id');
                    $spcPrizeObj = $request->input('happy_hour_price');
                    $srtDateObj  = str_replace(" ","",$request->input('from_time'));
                    $endDateObj  = str_replace(" ","",$request->input('to_time'));
                    $cnt = 0;
                    $sizePriceArr = array();
                    foreach ($sizeObj as $k => $v) {
                        $priceTmpArr = $availableTmpArr = $posIdTmpArr = array();
                        if (isset($typeObj[$k]) && $typeObj[$k] == "1") {
                            $sizePriceArr[$cnt]['size'] = $v;
                            $priceTmpArr[0] = $priceAllObj[$k];
                            //$sizePriceArr[$cnt]['price'] = $priceAllObj[$k];
                            //$sizePriceArr[$cnt]['menu_meal_type_id'] = 0;
                            //$cnt++;
                            //set is_delivery and is_carrayout flag
                            if($request->has('carryout_all') && isset($request->input('carryout_all')[$k])) {
                                $availableTmpArr['is_carryout'] = 1;
                            }else {
                                $availableTmpArr['is_carryout'] = 0;
                            }

                            //set is_delivery and is_carrayout flag
                            if($request->has('delivery_all') && isset($request->input('delivery_all')[$k])) {
                                $availableTmpArr['is_delivery'] = 1;
                            }else {
                                $availableTmpArr['is_delivery'] = 0;
                            }
                        } else {
                            $mealTypeObj = array();
                            if ($request->input('meal_type')) {
                                if (isset($request->input('meal_type')[$k]))
                                    $mealTypeObj = $request->input('meal_type');
                            }
                            $sizePriceArr[$cnt]['size'] = $v;
			    if(isset($mealTypeObj[$k])){
		                    foreach ($mealTypeObj[$k] as $k1 => $v1) {

		                        $priceTmpArr[$v1] = $priceObj[$k][$k1];
		                        //$sizePriceArr[$cnt]['price'] = $priceObj[$k][$k1];
		                        //$sizePriceArr[$cnt]['menu_meal_type_id'] = $v1;
		                        //set available
		                        if($request->has('carryout') && isset($request->input('carryout')[$k]) && array_key_exists($k1, $request->input('carryout')[$k])) {
		                            $availableTmpArr[$v1]['is_carryout'] = 1;
		                        }else {
		                            $availableTmpArr[$v1]['is_carryout'] = 0;
		                        }

		                         if($request->has('delivery') && isset($request->input('delivery')[$k]) && array_key_exists($k1, $request->input('delivery')[$k])) {
		                            $availableTmpArr[$v1]['is_delivery'] = 1;
		                        }else {
		                            $availableTmpArr[$v1]['is_delivery'] = 0;
		                        }


		                    }
			    }

                        }
                        $sizePriceArr[$cnt]['price'] = (array)$priceTmpArr;
                        $sizePriceArr[$cnt]['available'] = (array)$availableTmpArr;
                        $sizePriceArr[ $cnt ]['pos_id']         = $posObj[ $k ];
                        $sizePriceArr[ $cnt ]['happy_hour_price']  = $spcPrizeObj[ $k ];
                        $sizePriceArr[ $cnt ]['from_time']     = $srtDateObj[ $k ];
                        $sizePriceArr[ $cnt ]['to_time']       = $endDateObj[ $k ];
                        $cnt++;
                    }

                    // CUSTOMIZABLE MENU DATA >>>
                    $menuItemArr = [];
                    $restId = $request->input('restaurant_id');
                    $catId = $request->input('menu_category_id');
                    $menuSetCat = MenuSettingCategory::select('id', 'name', 'is_multi_select','maximum_selection','minimum_selection','equal_selection')->where(['status' => 1, 'restaurant_id' => $restId,'menu_category_id' => $catId])->get()->toArray();

                    foreach($menuSetCat as $menuSet) {
                        $isMandatory = 0;
                        $menuSetName = str_replace(' ', '_', $menuSet['name']);
                        if(isset($_POST[$menuSet['id'].'_is_mandatory'])) {
                            $isMandatory = 1;
                        }

                        if(isset($_POST[$menuSetName]) && count($_POST[$menuSetName])) {
                            // get price of Customization setting item
                            $menuSetItemDBArr = [];
                            $res = MenuSettingItem::select('id','name','price')->whereIn('id', $_POST[$menuSetName])->get()->toArray();
                            array_walk($res, function($item) use (&$menuSetItemDBArr) { $menuSetItemDBArr[$item['id']] = $item; });

                            // check ids in POST menuSetName
                            foreach($request->input($menuSetName) as $menuSetItemId) {
                                $quantArr = [];
                                $sidesArr = [];
                                $defaultSelected = 0;
                                foreach(config('constants.sides') as $side) {
                                    if(isset($_POST[$menuSetItemId.'_'.$side])) {
                                        $sidesArr[] = ['label' => $side, 'is_selected' => 1];
                                    }
                                }
                                if(!count($sidesArr)) {
                                    $sidesArr = null;
                                }
                                foreach(config('constants.quantity') as $quant) {
                                    if(isset($_POST[$menuSetItemId.'_'.$quant])) {
                                        $quantArr[] = ['label'=>$quant, 'is_selected' => 1];
                                    }
                                }
                                if(!count($quantArr)) {
                                    $quantArr = null;
                                }
                                if(isset($_POST[$menuSetItemId.'_default_selected'])) {
                                    $defaultSelected = 1;
                                }
                                $settingExistsIndex = array_search($menuSet['id'], array_column($menuItemArr, 'id'));
                                if($settingExistsIndex === false) {
                                    $menuItemArr[] = [
                                        'id'              => $menuSet['id'],
                                        'label'           => $menuSet['name'],
                                        'is_multi_select' => $menuSet['is_multi_select'],
                                        'maximum_selection' => $menuSet['maximum_selection'],
                                        'minimum_selection' => $menuSet['minimum_selection'],
                                        'equal_selection' => $menuSet['equal_selection'],
                                        'is_mandatory'    => $isMandatory,
                                        'items'           => [
                                            [
                                                'id'               => $menuSetItemId,
                                                'label'            => $menuSetItemDBArr[$menuSetItemId]['name'],
                                                'default_selected' => $defaultSelected,
                                                'quantity'         => $quantArr,
                                                'sides'            => $sidesArr,
                                                'price'            => $menuSetItemDBArr[$menuSetItemId]['price'],
                                            ],
                                        ],
                                    ];
                                } else {
                                    $menuItemArr[$settingExistsIndex]['items'][] = [
                                        'id'               => $menuSetItemId,
                                        'label'            => $menuSetItemDBArr[$menuSetItemId]['name'],
                                        'default_selected' => $defaultSelected,
                                        'quantity'         => $quantArr,
                                        'sides'            => $sidesArr,
                                        'price'            => $menuSetItemDBArr[$menuSetItemId]['price'],
                                    ];
                                }
                            }
                        }
                    }
                    // <<< CUSTOMIZABLE MENU DATA
                    $imageCaption = !empty(Input::get('image_caption'))?Input::get('image_caption'):null;
                    //dd($sizePriceArr,json_encode($sizePriceArr));
                    $menuItemObj                       = MenuItemModel::find($id);
                    $menuItemObj->name                 = Input::get('name');
                    $menuItemObj->product_type          = Input::get('product_type');
                    $menuItemObj->size_price           = json_encode($sizePriceArr);
                    $menuItemObj->food_type            = Input::get('food_type');
                    $menuItemObj->pos_id               = Input::get('pos_id');
                    $menuItemObj->sku                  = Input::get('sku');
                    //$menuItemObj->calorie              = Input::get('calorie');
                    $menuItemObj->restaurant_id        = Input::get('restaurant_id');
                    $menuItemObj->is_customizable      = (count($menuItemArr) || Input::get('is_customizable') )  ? 1 : 0;
                    $menuItemObj->customizable_data    = json_encode($menuItemArr);
                    $menuItemObj->status               = Input::get('status');
                    $menuItemObj->image_caption        = !empty($imageCaption)?current(Input::get('image_caption')): NULL;
                    $menuItemObj->menu_category_id     = Input::get('menu_category_id');
                    $menuItemObj->menu_sub_category_id = Input::get('menu_sub_category_id');
                    $menuItemObj->description          = !empty( Input::get('description'))? Input::get('description'):'';
                    $menuItemObj->sub_description          = !empty( Input::get('sub_description'))? Input::get('sub_description'):'';

                    $menuItemObj->gift_wrapping_fees =$request->input('gift_wrapping_fees')?$request->input('gift_wrapping_fees'):NULL;
                    $menuItemObj->gift_message =$request->input('gift_message')?$request->input('gift_message'):NULL;


                    if($request->input('image_delete') && $request->input('image_delete')=='1')
                    {
                        $menuItemObj->image = NULL;
                        $menuItemObj->thumb_image_med = NULL;
                        $menuItemObj->image_caption = NULL;
                    }
                    if($request->input('web_image_delete') && $request->input('web_image_delete')=='1')
                    {
                        $menuItemObj->web_image = NULL;
                        $menuItemObj->web_thumb_med = NULL;
                        $menuItemObj->web_image_caption = NULL;
                    }
                     if($request->input('mobile_web_image_delete') && $request->input('mobile_web_image_delete')=='1')
                    {
                        $menuItemObj->mobile_web_image = NULL;
                        $menuItemObj->mobile_web_thumb_med = NULL;
                        $menuItemObj->mobile_web_image_caption = NULL;
                    }
                    $menuItemObj->display_order = !empty(Input::get('display_order'))?Input::get('display_order'):0;
                    $menuItemObj->is_popular = !empty(Input::get('is_popular'))?Input::get('is_popular'):0;
                    $menuItemObj->is_favourite = !empty(Input::get('is_favourite'))?Input::get('is_favourite'):0;
                    $menuItemObj->manage_inventory =!empty(Input::get('manage_inventory'))?Input::get('manage_inventory'):0;
                    $menuItemObj->max_allowed_quantity =!empty(Input::get('max_allowed_quantity'))?Input::get('max_allowed_quantity'):0;
                    $menuItemObj->min_allowed_quantity =!empty(Input::get('min_allowed_quantity'))?Input::get('min_allowed_quantity'):0;
                    $menuItemObj->inventory =!empty(Input::get('inventory'))?Input::get('inventory'):0;

                    $menuItemObj->save();

                    /*if($request->input('type')=="1")
                    {
                        MenuMealTypePrice::where('menu_item_id', '=', $id)->delete();
                        $itemPriceData = ['menu_meal_type_id' => 0, 'menu_item_id' => $menuItemObj->id, 'price' => trim($request->input('price_all')),'all_flag'=>$request->input('type')];
                        MenuMealTypePrice::create($itemPriceData);
                    }
                    else {
                        if (isset($menuItemObj->id) && $request->input('meal_type')) {
                            MenuMealTypePrice::where('menu_item_id', '=', $id)->delete();
                            $mealTypeArr = $request->input('meal_type');
                            $priceArr = $request->input('price');
                            foreach ($mealTypeArr as $k => $v) {
                                $itemPriceData = ['menu_meal_type_id' => $v, 'menu_item_id' => $menuItemObj->id, 'price' => $priceArr[$k]];
                                MenuMealTypePrice::create($itemPriceData);
                            }
                        }
                    }*/



                    $imageRelPath = config('constants.image.rel_path.menu');
                    $imagePath = config('constants.image.path.menu');

                    $curr_restaurant_name=strtolower(str_replace(' ','-',$menuItemObj->Restaurant->restaurant_name));
                    $imagePath=$imagePath.DIRECTORY_SEPARATOR.$curr_restaurant_name;
                    $imageRelPath=$imageRelPath.$curr_restaurant_name. DIRECTORY_SEPARATOR;

                    (! File::exists($imagePath) ?File::makeDirectory($imagePath):'');
                    (! File::exists($imagePath.DIRECTORY_SEPARATOR . 'thumb') ?File::makeDirectory($imagePath.DIRECTORY_SEPARATOR . 'thumb' ):'');

                    $allimages=json_decode($menuItemObj->images,true);
                    $imageJsonArray = $allimages;
                    $mobile_app_images=isset($allimages['mobile_app_images'])?$allimages['mobile_app_images']:[];
                    $web_images=isset($allimages['desktop_web_images'])?$allimages['desktop_web_images']:[];
                    $mobile_web_images=isset($allimages['mobile_web_images'])?$allimages['mobile_web_images']:[];

                    $imageHidden = !empty($request->input('image_hidden'))?$request->input('image_hidden'):[];
                    $mobile_app_images=$this->unsetDeleted($imageHidden,$mobile_app_images);
                    $imageJsonArray['mobile_app_images']=$mobile_app_images;

                     $webImageHidden = !empty($request->input('web_image_hidden'))?$request->input('web_image_hidden'):[];
                    $web_images=$this->unsetDeleted($webImageHidden,$web_images);
                    $imageJsonArray['desktop_web_images']=$web_images;

                    $mobileWebImageHidden = !empty($request->input('mobile_web_image_hidden'))?$request->input('mobile_web_image_hidden'):[];
                    $mobile_web_images=$this->unsetDeleted($mobileWebImageHidden,$mobile_web_images);
                    $imageJsonArray['mobile_web_images']=$mobile_web_images;

                    /* Mobile app Image save*/
                    if ($request->hasFile('image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('image_caption');
                        foreach ($request->file('image') as $k => $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                            $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);

                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                            //$image->move($imagePath, $imageName);
                            $imageArr[] = [
                                'image' => $imageRelPath . $imageName,
                                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'image_caption' => $imageCaptionArr[$k],
                            ];

                            $mobile_app_images[]= [
                                'image' => $imageRelPath . $imageName,
                                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'image_caption' => $imageCaptionArr[$k],
                            ];
                            /* comment old image entry
                            if (count($imageArr) > 0) {
                                MenuItemModel::where('id', $menuItemObj->id)

                                    ->update($imageArr[0]);
                            }
                            */
                            $i++;
                        }
                        $mobile_app_images = array_values($mobile_app_images);
                        $imageJsonArray['mobile_app_images']=$mobile_app_images;
                    }
                    /*Desktop Image save*/
                    if ($request->hasFile('web_image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('web_image_caption');


                        foreach ($request->file('web_image') as $k => $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                            $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();
                            $image = Image::make($image->getRealPath());
                            $width = $image->width();

                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);
                            //$image->move($imagePath, $imageName);
                            $imageWebArr[] = [
                                'web_image' => $imageRelPath . $imageName,
                                'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'web_thumb_sml' => $imageCaptionArr[$k],
                            ];
                           $web_images[]= [
                                'web_image' => $imageRelPath . $imageName,
                                'web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'web_thumb_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'web_image_caption' => $imageCaptionArr[$k],
                            ];
                             /* comment old image entry
                            if (count($imageWebArr) > 0) {
                                MenuItemModel::where('id', $menuItemObj->id)
                                    ->update($imageWebArr[0]);
                            }
                            */
                            $i++;
                        }
                        $web_images = array_values($web_images);
                         $imageJsonArray['desktop_web_images']=$web_images;
                    }
                      /*Mobile web Image save*/
                    if ($request->hasFile('mobile_web_image')) {
                        $i = 0;
                        $imageCaptionArr = $request->input('mobile_web_image_caption');

                        foreach ($request->file('mobile_web_image') as $k => $image) {
                            $uniqId = uniqid(mt_rand());
                            $imageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '.' . $image->getClientOriginalExtension();

                            $medImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
                             $smlImageName = $menuItemObj->id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                                . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();
                            $image = Image::make($image->getRealPath());
                            $width = $image->width();
                            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
                            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);

                            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

                            //$image->move($imagePath, $imageName);
                            $imageMobWebArr[] = [
                                'mobile_web_image' => $imageRelPath . $imageName,
                                'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'mobile_web_thumb_sml' => $imageCaptionArr[$k],
                            ];
                             $mobile_web_images[$k]= [
                                'mobile_web_image' => $imageRelPath . $imageName,
                                'mobile_web_thumb_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                                'mobile_web_thumb_sml' =>$imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
                                'mobile_web_image_caption' => $imageCaptionArr[$k],
                            ];
                            /*
                            if (count($imageMobWebArr) > 0) {
                                MenuItemModel::where('id', $menuItemObj->id)
                                    ->update($imageMobWebArr[0]);
                            }
                            */
                            $i++;
                        }
                        $mobile_web_images = array_values($mobile_web_images);
                         $imageJsonArray['mobile_web_images']=$mobile_web_images;

                    }
                   //  if ($request->hasFile('image')|| $request->hasFile('web_image') || $request->hasFile('mobile_web_image')) {
                       MenuItemModel::where('id', $menuItemObj->id)->update(['images'=>json_encode($imageJsonArray)]);
                      //}

                    $newMenuItemObj = MenuItemModel::where('id', $menuItemObj->id)->first();
                    $menuItemLangObj = MenuItemsLanguage::where(['new_menu_items_id' => $menuItemObj->id, 'language_id' => $request->input('language_id')])->first();

                    if ($menuItemLangObj !== null) {
                        $fillables = [];
                        if ($request->input('item_other_info') != '') {
                            $fillables['item_other_info'] = $request->input('item_other_info');
                        }
                        if ($request->input('gift_message') != '') {
                            $fillables['gift_message'] = $request->input('gift_message');
                        }
                        if ($request->input('name') != '') {
                            $fillables['name'] = $request->input('name');
                        }
                        //if ($request->input('description') != '') {
                            $fillables['description'] = $request->input('description');
                        //}
                        //if ($request->input('sub_description') != '') {
                            $fillables['sub_description'] = $request->input('sub_description');
                        //}
                        if (count($fillables)) {
                            MenuItemsLanguage::where([
                                'new_menu_items_id' => $menuItemObj->id,
                                'language_id' => $request->input('language_id')
                            ])->update($fillables);
                        }
                    } else {
                        $fillables = [
                            'new_menu_items_id' => $newMenuItemObj->id,
                            'item_other_info' => $newMenuItemObj->item_other_info,
                            'gift_message' => $newMenuItemObj->gift_message,
                            'name' => $newMenuItemObj->name,
                            'description' => $newMenuItemObj->description,
                            'sub_description' => $newMenuItemObj->sub_description,
                            'language_id' => ($request->input('language_id') ? $request->input('language_id') : 1)
                        ];
                        MenuItemsLanguage::create($fillables);
                    }

                    return Redirect::back()->with('message', 'Menu Item updated successfully');
                }
            }
            return redirect()->back()->withErrors($validator->errors())->withInput()->with('sizeCount', $sizeCount);
        }
        return Redirect::back()->with('message', 'Invalid Id');
    }

    public function getCategoryAndType(Request $request)
    {
        $rid = $request->input('rid');
        if(!empty($rid) && is_numeric($rid)) {
            $result['cat'] = MenuCategory::where('restaurant_id', '=', $rid)->get();
            $result['type'] = MenuMealType::where('restaurant_id', '=', $rid)->get();
            return response()->json(array('dataObj' => $result), 200);
        }
        return response()->json(array('err' => 'Invalid Data'), 200);
    }

    public function destroyOld($id)
    {
        $menuItemObj = MenuItemModel::find($id);
        if(!empty($menuItemObj)){
            $menuItemObj->delete();
            return Redirect::back()->with('message','Menu Item deleted successfully');
        }
        else
            return Redirect::back()->with('err_msg','Invalid Item');
    }

    public function getCustomizationOptions(Request $request)
    {

        $restId = $request->input('rid');
        $catId  = $request->input('cid');
        if (!empty($restId) && is_numeric($restId) && !empty($catId) && is_numeric($catId)) {
            // MENU CUSTOMISATION SETTING >>>
            $restId        = $restId;
            $menuSetCat    = MenuSettingCategory::select('id', 'name', 'is_multi_select')->where(['status' => 1, 'restaurant_id' => $restId, 'menu_category_id' => $catId])->get()->toArray();
            $menuSetCatIds = array_column($menuSetCat, 'id');
            $menuSetItems  = MenuSettingItem::select('id', 'menu_category_id', 'menu_setting_category_id', 'name', 'side_flag', 'quantity_flag')
                            ->whereIn('menu_setting_category_id', $menuSetCatIds)->where(['restaurant_id' => $restId, 'status' => 1])->get()->toArray();
            $subCategories = MenuSubCategory::select('id', 'name')->where(['restaurant_id' => $restId, 'menu_category_id' => $catId, 'status' => 1])->orderBy('priority')->get();

            $subcat_options= app('App\Http\Controllers\MenuSubcategoryController')->getSubCatHtml($catId,$restId,0,NULL,0);

            $menuSettingData = [];
            foreach ($menuSetCat as $menuCat) {
                $res              = array_filter($menuSetItems, function ($i) use ($menuCat) {
                    if ($i['menu_setting_category_id'] == $menuCat['id']) {
                        return true;
                    }
                    return false;
                });
                $menuCat['items'] = array_values($res);
                array_push($menuSettingData, $menuCat);
            }

            $result = [
                'menu_settings_data' => $menuSettingData,
                'sub_categories'     => $subCategories,
                'subcat_options'     => $subcat_options,
                'hri_sides'          => config('constants.sides'),
                'hri_quantity'       => config('constants.quantity'),
            ];

            return response()->json(array('dataObj' => $result), 200);
        }

        return response()->json(array('err' => 'Invalid Data'), 200);
    }

    public function modifiergroup(Request $request, $id, $languageId = 1) {

        if (!isset($id) || !is_numeric($id)) {
            return redirect()->back()->with('err_msg', 'Invalid Request');
        }
        $menu_item_id = $id;

        DB::beginTransaction();
        try {
            if ($request->has('modifiergroup')) {
                $modifiergroups = $request->input('modifiergroup');
                foreach ($modifiergroups as $key => $val) {
                    $group_id = $key;

                    $group_data = [
                        'group_name' => $val['group_name'],
                        'prompt' => $val['prompt'],
			'dependent_modifier_group_id'=> (int)$val['dependent_modifier_group_id'],
			'description'=> $val['description'],
                        'pos_id' => isset($val['pos_id']) ? $val['pos_id'] : null,
			
                        //'calorie' => isset($val['calorie']) ? $val['calorie'] : null,
                        'quantity_type' => $val['quantity_type'],
			'status' => ($val['status'])??1,
                        'quantity' => $val['quantity'],
                        'is_required' => $val['is_required'],
                        'sort_order' => $val['sort_order'],
                        'show_as_dropdown' => $val['show_as_dropdown'],
                    ];

                    ItemModifierGroup::where('id', $key)->update($group_data);
                    if (ItemModifierGroupLanguage::where(['modifier_group_id' => $key, 'language_id' => $languageId])->count()) {
                        $lang_data = [
                            'group_name' => $val['group_name'],
                            'prompt' => $val['prompt'],
                        ];
                        ItemModifierGroupLanguage::where(['modifier_group_id' => $key, 'language_id' => $languageId])->update($lang_data);
                    } else {
                        $lang_data = [
                            'modifier_group_id' => $key,
                            'language_id' => $languageId,
                            'group_name' => $val['group_name'],
                            'prompt' => $val['prompt'],
                        ];
                        ItemModifierGroupLanguage::create($lang_data);
                    }

                    if (isset($val['modifier_items']) && count($val['modifier_items'])) {

                        foreach ($val['modifier_items'] as $itemval) {
			    $itemval['modifier_category'] = 0;
			  //ALTER TABLE `modifier_items` ADD `dependent_modifier_item_id` BIGINT(22) NOT NULL DEFAULT '0' AFTER `modifier_group_id`; 
			  //ALTER TABLE `modifier_groups` ADD `dependent_modifier_group_id` BIGINT(22) NOT NULL DEFAULT '0' AFTER `id`; 
                            if (isset($itemval['modifier_id']) && !empty($itemval['modifier_id'])) {

                                $old_modifier_items = array_column($val['modifier_items'], 'modifier_id');
                                ItemModifier::where('modifier_group_id', $group_id)->whereNotIn('id', $old_modifier_items)->delete();
//                                ItemModifierLanguage::whereNotIn('modifier_item_id', $old_modifier_items)->delete();

                                $group_item_data = [
                                    'modifier_category_id' => $itemval['modifier_category'],
                                    'modifier_name' => $itemval['modifier_name'],
				    'dependent_modifier_item_id' => (int)$itemval['dependent_modifier_item_id'], 	
                                    'pos_id'    => isset($itemval['pos_id']) ? $itemval['pos_id'] : null,
                                    //'calorie'    => isset($itemval['calorie']) ? $itemval['calorie'] : null,
                                    'price' => $itemval['modifier_price'],
                                    'special_price' => ($itemval['modifier_special_price'])??0,
                                    'start_date' => ($itemval['modifier_start_date'])??date("Y-m-d"),
                                    'end_date' => ($itemval['modifier_end_date'])??date("Y-m-d"),
                                     'description' => isset($itemval['description']) ? $itemval['description'] : null,
				    'status' => ($itemval['status'])??1,
                                    'sort_order' => $itemval['sort_order'],

                                    'is_selected' => (isset($itemval['is_selected']) && $itemval['is_selected'])?1:0,
                                    'size'                 =>stripslashes($itemval['modifier_size']),
                                ];

                                ItemModifier::where('id', $itemval['modifier_id'])->update($group_item_data);
                                if (ItemModifierLanguage::where(['modifier_item_id'=>$itemval['modifier_id'], 'language_id' => $languageId])->count()) {
                                    $group_item_data_language = [
                                        'modifier_name' => $itemval['modifier_name'],
                                    ];
                                    ItemModifierLanguage::where([
                                        'modifier_item_id'=>$itemval['modifier_id'],
                                        'language_id' => $languageId])->update($group_item_data_language);
                                } else {
                                    $group_item_data_language = [
                                        'modifier_item_id' => $itemval['modifier_id'],
                                        'language_id' => $languageId,
                                        'modifier_name' => $itemval['modifier_name'],
                                    ];
                                    ItemModifierLanguage::create($group_item_data_language);
                                }
                            } else {

                                $group_item_data = [
                                    'modifier_group_id' => $group_id,
                                    'modifier_category_id' => $itemval['modifier_category'],
                                    'modifier_name' => $itemval['modifier_name'],
                                    'pos_id'    => isset($itemval['pos_id']) ? $itemval['pos_id'] : null,
                                    //'calorie'    => isset($itemval['calorie']) ? $itemval['calorie'] : null,
				    'description' => isset($itemval['description']) ? $itemval['description'] : null,
                                    'price' => $itemval['modifier_price'],
                                    'special_price' => ($itemval['modifier_special_price'])??0,
                                    'start_date' => ($itemval['modifier_start_date'])??date("Y-m-d"),
                                    'end_date' => ($itemval['modifier_end_date'])??date("Y-m-d"),
				    'status' => ($itemval['status'])??1,
                                     
                                    'sort_order' => $itemval['sort_order'],
                                    'is_selected' => (isset($itemval['is_selected']) && $itemval['is_selected'])?1:0,
                                    'size'  => stripslashes($itemval['modifier_size'])

                                ];

                                $itemModifierId = ItemModifier::create($group_item_data)->id;
                                $group_item_data_language = [
                                    'modifier_item_id' => $itemModifierId,
                                    'language_id' => $languageId,
                                    'modifier_name' => $itemval['modifier_name'],
                                ];
                                ItemModifierLanguage::create($group_item_data_language);
                            }
                        }
                    }
                }

                DB::commit();
                if ($request->ajax()) {

                    $ItemModifierGroup = ItemModifierGroup::orderBy('id', 'DESC')->where('menu_item_id', $menu_item_id)->get();
                    if(count($ItemModifierGroup)) {
                        foreach ($ItemModifierGroup as &$row) {
                            if (isset($row->itemmodifiers)) {
                                foreach ($row->itemmodifiers as &$itemmodifiers) {
                                    $itemmodifierslang = ItemModifierLanguage::where('modifier_item_id', $itemmodifiers->id)->get();
                                    $itemmodifiers->itemModifierLanguage = $itemmodifierslang;
                                }
                            }
                        }
                    }
                    $data = view('menumanagement::menu_item.modifier_render', compact('ItemModifierGroup', 'languageId'));
                    return \Response::json(['success' => true, 'data' => $data, 'message' => 'Modifier Info  updated successfully.'], 200);
                } else {
                    return back()->with('section_tab', 'modifiergroup')->with('status_msg', 'Modifier Info  updated successfully');
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
            } else {
                return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
            }
        }
    }

    public function deleteModifierItem(Request $request,$id)
      {
        DB::beginTransaction();
        try {

            ItemModifier::where('id', $id)->delete();
            DB::commit();


            if ($request->ajax()) {
            return \Response::json(['success' => true, 'message' =>'Modifier Item has been deleted successfully.'], 200);

            }else{
              return back()->with('status_msg', 'Modifier Item has been deleted successfully.');
            }

        }
        catch (\Exception $e){
            DB::rollback();
               if ($request->ajax()) {
                    return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);

                }else{
                    return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
                }
        }
    }
    public function destroy(Request $request,$id)
      {
        DB::beginTransaction();
        try {

	    $menuItemObj =  MenuItemModel::find($id);
	    $menuItemObj->status = 2;
	    $menuItemObj->save();


            DB::commit();
 	    return Redirect::back()->with('message','Menu Item deleted successfully');


        }
        catch (\Exception $e){
            DB::rollback();
               return Redirect::back()->with('message','Please try again!!');
        }
    }

    /*RG 08-03-2019*/
    public function all_items(Request $request)
    {
        $editFlag = 0;
        if($request->has('edit')) {
            $menuItemId = $request->input('edit');
            $editFlag = 1;
        } else {
            $menuItemId = null;
        }
        $selectedTab = 'general';
        if($request->has('tab')) {
            $selectedTab = $request->input('tab');
        }
        /*$searchArr = array('restaurant_id' => 0, 'name' => NULL);
        #$groupRestData = Restaurant::group();
        $groupRestData = CommonFunctions::getRestaurantGroup();
        if (count($groupRestData) == 1) {
            $first_ind_array = current($groupRestData);
            if (count($first_ind_array['branches']) == 1) {
                $searchArr['restaurant_id'] = $first_ind_array['branches']['0']['id'];
            }
        }
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $menuItems = MenuItemModel::orderBy('menu_category_id', 'DESC')->whereIn('status', [1,0])->whereIn('restaurant_id', $locations)->orderBy('id', 'DESC');
        if ($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))) {
            $searchArr['restaurant_id'] = $request->input('restaurant_id');
            $menuItems->where('restaurant_id', $searchArr['restaurant_id']);
            //$menuItems = MenuItemModel::where('restaurant_id',$searchArr['restaurant_id'])->orderBy('category_id','DESC')->orderBy('id','DESC')->paginate(20);
        }
        if ($request->input('name') && strlen($request->input('name')) <= 100) {
            $searchArr['name'] = $request->input('name');
            $menuItems->where('name', 'like', $searchArr['name'] . '%');
        }
        $menuItems = $menuItems->where('product_type', 'food_item');// Get Product Type Food Item
        $menuItems = $menuItems->paginate(20);*/
        $groupRestData  = Restaurant::group();
        $restaurantId   = Auth::user()->restaurant_id;
        /*$catSubCatList  = MenuCategory::where('restaurant_id', $restaurantId)
            ->select('id', 'name', 'priority')
            ->with(['menuSubCategory' => function($query) {
                $query->select('id', 'name', 'menu_category_id', 'parent', 'priority')
                    ->with('childrenRecursive')->where('parent', 0)
                    ->orderBy('priority');
            }])
            ->orderBy('priority')
            ->get();*/
        $catSubCatList  = MenuCategory::where('restaurant_id', $restaurantId)
            ->select('id', 'name', 'priority')
            ->with(['childrenRecursive' => function($query) {
                $query->select('id', 'name', 'priority', 'parent')->orderBy('priority');
            }])
            ->where('parent', 0)
            ->orderBy('priority')
            ->get();
        /*dump($restaurantId);
        dd($catSubCatList->toArray());*/

        $catSubCatList = isset($catSubCatList) ? $catSubCatList->toArray() : [];

        // Modifier Groups
        $itemModifierGroups = ItemModifierGroup::select('id','menu_item_id','group_name','prompt','quantity_type','quantity')
                                    ->where('restaurant_id', $restaurantId)->groupBy('group_name')
                                    ->with(['itemmodifiers' => function($query) {
                                        $query->select('id','modifier_group_id','modifier_category_id','modifier_name','price')
                                            ->with(['modifiercategory' => function($query) {
                                                $query->select('id','category_name');
                                            }]);
                                    }])
                                    ->get();

        $itemModifierData = [];
        if($itemModifierGroups) {
            foreach($itemModifierGroups as $modGroup){
                $itemModifiers = ItemModifier::select('id','modifier_group_id','modifier_category_id','modifier_name','price')
                    ->where('modifier_group_id', $modGroup->id)
                    ->with(['modifiercategory' => function($query) {
                        $query->select('id','category_name')->groupBy('category_name');
                    }])->get()->toArray();
                $modCats = $modItems = [];
                if($itemModifiers) {
                    foreach($itemModifiers as $items) {
                        $modItems[] = [
                            'id'            => $items['id'],
                            'modifier_name' => $items['modifier_name'],
                            'price'         => $items['price'],
                            'category_id'   => $items['modifiercategory']['id'],
                            'category_name' => $items['modifiercategory']['category_name']
                        ];
                        if(isset($items['modifiercategory']['id'])){
                            if(count($modCats)) {
                                if(!in_array($items['modifiercategory']['id'], array_column($modCats,'id'))) {
                                    $modCats[] = ['id' => $items['modifiercategory']['id'], 'category_name' => $items['modifiercategory']['category_name'].'hi'];
                                }
                            } else {
                                $modCats[] = ['id' => $items['modifiercategory']['id'], 'category_name' => $items['modifiercategory']['category_name'].'hi2'];
                            }
                        } else {
                            $modCats = 'no';
                        }
                    }
                }
                $itemModifierData[] = [
                    'id'                     => $modGroup->id,
                    'group_name'             => $modGroup->group_name,
                    'prompt'                 => $modGroup->prompt,
                    'quantity_type'          => $modGroup->quantity_type,
                    'quantity'               => $modGroup->quantity,
                    'mod_categories'         => $modCats,
                    'mod_items'              => $modItems,
                ];
            }
        }

        // Addon Groups
        $itemAddonData = ItemAddonGroups::select('id','menu_item_id','group_name','prompt','quantity_type','quantity')
            ->where('restaurant_id', $restaurantId)->groupBy('group_name')
            ->with(['addonGroupItems' => function($query) {
                $query->select('id','addon_group_id','name','addon_price');
            }])
            ->get();
        //dd($itemAddonData->toArray());

        // Labels
        $allLabels = Labels::select('id', 'name', 'type')->get();
        $allLabelsData = [];
        if($allLabels) {
            //$allLabelsData = $allLabels->toArray();
            foreach($allLabels as $label) {
                $allLabelsData[$label->type][] = ['id' => $label->id,'name' => $label->name];
            }
        }

        //dd($allLabelsData);
        $modifierCategories = ModifierCategories::orderBy('id', 'DESC')->where('restaurant_id', $restaurantId)->get();
        $product_types = config('constants.product_type');
        // Languages
        $langData = Language::select('id', 'language_name')->where(['restaurant_id' => $restaurantId, 'status' => 1])->get();
        if(Auth::user()) {
            $curSymbol = Auth::user()->restaurant->currency_symbol;
        } else {
            $curSymbol = config('constants.currency');
        }
        return view('menumanagement::menu_item.all_items', compact('restaurantId', 'catSubCatList', 'langData',
            'groupRestData', 'itemModifierData', 'itemAddonData', 'allLabelsData', 'modifierCategories','product_types',
            'editFlag', 'menuItemId', 'selectedTab','curSymbol'));
    }

    /**
     * Created by: Amit Malakar
     * Date: 9-Jul-19
     * Modifier Categories of a Restaurant
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModifierCategories()
    {
        $restaurantId = Auth::user()->restaurant_id;
        $modifierCategories = ModifierCategories::orderBy('id', 'DESC')->where('restaurant_id', $restaurantId)->get();

        return response()->json($modifierCategories);
    }

    /**
     * Created by: Amit Malakar
     * Date: 13-Mar-19
     * Method Category and Sub-Category list All
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function allCatSubCatList(Request $request)
    {
        $restaurantId   = Auth::user()->restaurant_id;

        /*$catSubCatList  = MenuCategory::where('restaurant_id', $restaurantId)
            ->select('id', 'name', 'priority')
            ->with(['menuSubCategory' => function($query) {
                $query->select('id', 'name', 'menu_category_id', 'parent', 'priority')
                    ->with('childrenRecursive')->where('parent', 0)
                    ->orderBy('priority');
            }])
            ->orderBy('priority')
            ->get();*/
        $catSubCatList  = MenuCategory::where('restaurant_id', $restaurantId)
            ->select('id', 'name', 'priority')
            ->with(['childrenRecursive' => function($query) {
                $query->select('id', 'name', 'priority', 'parent')->orderBy('priority');//->where('parent',0);
            }])
            ->where('parent', 0)
            ->orderBy('priority')
            ->get();

        return response()->json($catSubCatList);
    }

    /**
     * Created by: Amit Malakar
     * Date: 25-Mar-19
     * Method to get a Category and its Sub-Categories
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCatSubCat(Request $request)
    {
        $restaurantId   = Auth::user()->restaurant_id;
        $catId = $request->input('sub_cat_id') ? $request->input('sub_cat_id') : $request->input('cat_id');

        $catSubCatList  = MenuCategory::where('restaurant_id', $restaurantId)
            ->where('id', $catId)
            ->select('id', 'name', 'parent', 'priority','description','status', 'image', 'image_icon','slug','pos_id','product_type','is_popular',
                'is_favourite', 'is_delivery', 'is_carryout', 'image_class', 'promotional_banner_image', 'promotional_banner_image_mobile',
                'promotional_banner_url','meta_title', 'meta_keyword', 'promotional_banner_image_app','cat_attachment','language_id'
            )
            /*->with(['menuSubCategory' => function($query) use($subCatId) {
                $query->select('id', 'name', 'menu_category_id', 'parent', 'priority', 'status')
                    ->with('childrenRecursive')->where('parent', 0);
                if($subCatId) {
                    $query->where('id', $subCatId);
                }
                $query->orderBy('priority');
            }])*/
            ->orderBy('priority')
            ->first();

        if($catSubCatList) {
            $catSubCatList->image       = !is_null($catSubCatList->image) ? json_decode($catSubCatList->image,true) : null;
            $catSubCatList->image_icon  = !is_null($catSubCatList->image_icon) ? json_decode($catSubCatList->image_icon,true) : null;
        }

        return response()->json($catSubCatList);
    }

    /**
     * Created by: Amit Malakar
     * Date: 13-Mar-19
     * Method to get menu items for a Cat/SubCat/Both
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMenuItems(Request $request)
    {
        $menuCatId      = $request->input('menu_category_id');
        $menuSubCatId   = $request->input('menu_sub_category_id');
        $searchKey      = $request->input('search_key');
        $restaurantId   = Auth::user()->restaurant_id;

        // modifiers
        // minimum price
        if($menuSubCatId) {     //$menuCatId && $menuSubCatId) {
            $menuItems = MenuItemModel::where('restaurant_id', $restaurantId)
                ->select('id', 'name', 'description', 'status', 'image', 'thumb_image_med', 'size_price', 'images')
                ->with(['modifierGroup'=>function($query){
                    $query->select('id', 'menu_item_id')->with(['itemmodifiers'=>function($qry) {
                        $qry->select('id','modifier_group_id');
                    }]);
                }])
                ->where('menu_category_id', $menuSubCatId)    //(['menu_category_id' => $menuCatId, 'menu_sub_category_id' => $menuSubCatId])
                ->orderBy('id', 'DESC')->get();
        } elseif($menuCatId) {
            $menuItems = MenuItemModel::where('restaurant_id', $restaurantId)
                ->select('id', 'name', 'description', 'status', 'image', 'thumb_image_med', 'size_price', 'images')
                ->with(['modifierGroup'=>function($query){
                    $query->select('id', 'menu_item_id')->with(['itemmodifiers'=>function($qry) {
                        $qry->select('id','modifier_group_id');
                    }]);
                }])
                ->where('menu_category_id', $menuCatId)
                ->orderBy('id', 'DESC')->get();
        }

        // get minimum price for menu Item
        $result = [];
        if($menuItems) {
            $menuItems = $menuItems->toArray();
            foreach($menuItems as $menu) {
                $sizePrice = !empty($menu['size_price']) ? json_decode($menu['size_price'], true) : '';
                $price = is_array($sizePrice) ? array_column($sizePrice, 'price') : '';
                $price = is_array($price) ? current(current($price)) : $price;  // PENDING

                $modifierExists = false;
                if(isset($menu['modifier_group']) && count($menu['modifier_group'])) {
                    if(isset($menu['modifier_group'][0]['itemmodifiers']) && count($menu['modifier_group'][0]['itemmodifiers'])) {
                        $modifierExists = true;
                    }
                }
                $menuImage = '../images/menu_item_placeholder.png';
                $menuImagesArr = [];
                if(!empty($menu['images'])) {
                    $menuImagesArr = json_decode($menu['images'], true);
                    if(isset($menuImagesArr['desktop_web_images'][0]['web_thumb_sml'])) {
                        $file_exists = file_exists(config('constants.image.path.menu').'/../..'.$menuImagesArr['desktop_web_images'][0]['web_thumb_sml']);
                        if($file_exists){
                            $menuImage = '../'.$menuImagesArr['desktop_web_images'][0]['web_thumb_sml'];
                        } else {
                            $menuImage = '../images/menu_item_placeholder.png';
                        }
                    }
                }

                $result[] = [
                    'id'              => $menu['id'],
                    'name'            => $menu['name'],
                    'description'     => $menu['description'],
                    'status'          => $menu['status'],
                    'image'           => $menu['image'],
                    'images'          => $menuImage,
                    'modifier_exists' => $modifierExists,
                    'price'           => $price,
                ];
            }
        }


        return response()->json($result);
    }

    /**
     * Created by: Amit Malakar
     * Date: 28-May-19
     * Method to get menu items for a Cat/SubCat/Both
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMenuItem(Request $request, $menuId)
    {
        // GENERAL
        $menuData       = MenuItemModel::select('id','name','description','product_type','food_type','pos_id','calorie','delivery',
                                        'display_order','takeout','size_price','menu_category_id','menu_sub_category_id','images')
                                    ->where('id', $menuId)->first();

        // MODIFIER
        $modifierRes   = ItemModifierGroup::select('id','menu_item_id','group_name','prompt','pos_id','calorie','is_required','quantity_type','quantity')
                                    ->where('menu_item_id', $menuId)
                                    ->with(['itemmodifiers' => function($query) {
                                        $query->select('id', 'modifier_name', 'price', 'pos_id', 'calorie', 'modifier_group_id','modifier_category_id')
                                                ->with(['modifiercategory' => function($query2) {
                                                    $query2->select('id', 'category_name');
                                                }]);
                                    }])->get();
        $modifierData = [];
        $modCatData   = [];
        $modItemData  = [];
        $modifierRes = $modifierRes ? $modifierRes->toArray() : [];
        foreach($modifierRes as $mod) {
            $modCatData = [];
            $modItemData  = [];
            if(isset($mod['itemmodifiers']) && count($mod['itemmodifiers'])) {
                foreach($mod['itemmodifiers'] as $item) {
                    if(isset($item['modifiercategory']) && count($item['modifiercategory'])) {
                        if(!in_array($item['modifiercategory']['id'], array_column($modCatData,'id'))) {
                            $modCatData[] = [
                                'id'                => $item['modifiercategory']['id'],
                                'category_name'     => $item['modifiercategory']['category_name'],
                            ];
                        }
                    }
                    $modItemData[] = [
                        'id'                => $item['id'],
                        'modifier_name'     => $item['modifier_name'],
                        'modifier_price'    => $item['price'],
                        'modifier_pos_id'   => $item['pos_id'],
                        'modifier_calorie'  => $item['calorie'],
                        'modifier_size'                 => isset($item['size']) ? $item['size'] : '',

                        'category_id'       => $item['modifier_category_id'],
                        'category_name'     => "Choose Flavor",
                    ];
                }
            }
            $modifierData[] = [
                'id'                => $mod['id'],
                'item_id'           => $mod['menu_item_id'],
                'group_name'        => $mod['group_name'],
                'prompt'            => $mod['prompt'],
                'pos_id'            => $mod['pos_id'],
                'calorie'           => $mod['calorie'],
                'quantity'          => $mod['quantity'],
                'quantity_type'     => $mod['quantity_type'],
                'required'          => $mod['is_required'],
                'mod_categories'    => $modCatData,
                'mod_items'         => $modItemData,
            ];
        }

        // ADDONS
        $addonRes      = ItemAddonGroups::select('id','menu_item_id','group_name','prompt','quantity_type','quantity','is_required')
                                            ->where('menu_item_id', $menuId)
                                            ->with(['addonGroupItems' => function($query) {
                                                $query->select('id','addon_group_id','name','addon_price');
                                            }])->get();
        $addonData = [];
        $addonRes = $addonRes ? $addonRes->toArray() : [];
        foreach($addonRes as $addon) {
            $addonItemData = [];
            if(isset($addon['addon_group_items']) && count($addon['addon_group_items'])) {
                foreach($addon['addon_group_items'] as $addonItem) {
                    $addonItemData[] = [
                        'id'            => $addonItem['id'],
                        'addon_group_id'=> $addonItem['addon_group_id'],
                        'name'     => $addonItem['name'],
                        'addon_price'    => $addonItem['addon_price'],
                    ];
                }
            }
            $addonData[] = [
                'id'                => $addon['id'],
                'menu_item_id'      => $addon['menu_item_id'],
                'group_name'        => $addon['group_name'],
                'prompt'            => $addon['prompt'],
                'quantity'          => $addon['quantity'],
                'quantity_type'     => $addon['quantity_type'],
                'required'          => $addon['is_required'],
                'addon_group_items' => $addonItemData,
            ];
        }

        // LABELS
        $labelIdRes      = MenuItemLabels::select('id','labels','size','item_id')
                                            ->where('item_id', $menuId)->first();
        $labelData = [];
        if($labelIdRes) {
            $labelIdRes = $labelIdRes->toArray();
            if(strpos($labelIdRes['size'], ',') !== false ) {
                $serving  = explode(',', $labelIdRes['size']);
            } else {
                $serving  = [1,1];
            }

            $labelData = [
                'item_label_id'     => $labelIdRes['id'],
                'item_id'           => $labelIdRes['item_id'],
                'label_ids'         => $labelIdRes['labels'],
                'serving_size_min'  => $serving[0] <= $serving[1] ? $serving[0] : $serving[1],
                'serving_size_max'  => $serving[0] > $serving[1] ? $serving[0] : $serving[1],
            ];
        }

        // MENU IMAGES
        $imagesData     = strlen($menuData->images) ? json_decode($menuData->images, true) : [];

        $data = [
            'menu_data'      => $menuData,
            'modifier_data'  => $modifierData,
            'addon_data'     => $addonData,
            'label_data'     => $labelData,
            'images_data'    => $imagesData
        ];

        return response()->json($data);
    }

    /**
     * Created by: Amit Malakar
     * Date: 04-Apr-19
     * Method to add/update new menu item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMenuItem(Request $request)
    {
        $general_item_id       = $request->input('general_item_id');
        $restaurant_id         = Auth::user()->restaurant_id;     //$request->input('restaurant_id');
        $gen_cat_sub_cat       = $request->input('gen_cat_sub_cat');
        $gen_item_name         = $request->input('gen_item_name');
        $gen_item_desc         = $request->input('gen_item_desc');
        $gen_item_product_type = $request->input('gen_item_product_type');
        $gen_item_food_type    = $request->input('gen_item_food_type');
        $gen_item_pos_id       = $request->input('gen_item_pos_id');
        $gen_item_calorie      = $request->input('gen_item_calorie');
        $gen_item_display_order= $request->input('gen_item_display_order');
        $gen_delivery          = ($request->input('gen_delivery') == 'true') ? 1 : 0;
        $gen_takeout           = ($request->input('gen_takeout') == 'true') ? 1 : 0;
        $general_size          = $request->input('general_size') ?? [];
        $general_price         = $request->input('general_price');
        $general_is_active     = $request->input('general_is_active');
        $general_is_delivery   = $request->input('general_is_delivery');
        $general_is_carryout   = $request->input('general_is_carryout');
        $sizePrize = [];
        if(count($general_size)) {
            for($i=0; $i<count($general_size); $i++) {
                $sizePrize[] = [
                    'size'        => $general_size[ $i ],
                    'price'       => [ $general_price[ $i ] ],
                    'is_active'   => $general_is_active[ $i ],
                    'is_delivery' => $general_is_delivery[ $i ],
                    'is_carryout' => $general_is_carryout[ $i ],
                ];
            }
            $sizePrize = json_encode($sizePrize);
        }

        $data = [
            'restaurant_id'    => $restaurant_id,
            'product_subtype'  => 'simple',
            'name'             => $gen_item_name,
            'description'      => $gen_item_desc,
            'product_type'     => $gen_item_product_type,
            'food_type'        => $gen_item_food_type,
            'pos_id'           => $gen_item_pos_id,
            'calorie'          => $gen_item_calorie,
            'display_order'    => $gen_item_display_order,
            'delivery'         => $gen_delivery,
            'takeout'          => $gen_takeout,
            'menu_category_id' => $gen_cat_sub_cat,
            'size_price'       => $sizePrize,
            'status'           => 1,
        ];
        if(!is_array($sizePrize) && strlen($sizePrize)) {
            $data['size_price'] = $sizePrize;
        }

        if(!empty($general_item_id)) {
            // update existing menu item
            $general_item_id = (int)$general_item_id;
            MenuItemModel::where('id',$general_item_id)->update($data);
            $res = $data + ['id' => $general_item_id];
        } else {
            // create new menu item
            $res = MenuItemModel::create($data);
        }

        return response()->json($res);
    }

    public function validateCatSlug(Request $request)
    {
        $catId   = $request->input('cat_id');
        $slug       = $request->input('slug');
        $restaurantId = Auth::user()->restaurant_id;
        // find if the slug is unique at restaurant branch level
        $menuCatCount = MenuCategory::where(['slug' => $slug, 'restaurant_id'=>$restaurantId])->where('id','<>', $catId)->count();
        if($menuCatCount) {
            return response()->json(['success' => false, 'message' => 'Slug is not unique. Please try again.'], 400);
        } else {
            return response()->json(['success' => true, 'message' => 'Slug is unique.'], 200);
        }
        return response()->json($menuCatCount);
    }

    public function toggleMenuActive(Request $request)
    {
        $menuId = $request->input('menu_id');
        $status = $request->input('active_flag');

        $menuItem = MenuItemModel::find($menuId)->update(['status'=>$status]);
        if($menuItem) {
            $data = [
                'message' => 'Updated menu item succussfully.',
                'success' => true
            ];
        } else {
            $data = [
                'message' => 'Something went wrong. Please try again.',
                'success' => false
            ];
        }

        return response()->json($data);
    }

    /**
     * Created by: Amit Malakar
     * Date: 25-Mar-19
     * Method to add category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCategory(Request $request)
    {
        // if cat id dont exists then it's create
        $catId        = $request->input('cat_id');
        $createFlag = false;
        if(!$catId) {
            $createFlag = true;
        }
        $parentId     = $request->input('add_cat_id') ?? 0;
        $catName      = $request->input('catName');
        if(!strlen($catName)) {
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
            return Redirect::back()->with('message', 'Category/Sub-Category added successfully');
        }
        /*if($createFlag) {
            $parentId = 0;
        } else {
            $parentId = $selCatId;
        }*/
        $catSlug      = strtolower(str_replace(' ', '_', $catName));
        $restaurantId = Auth::user()->restaurant_id;
        /*// find next priority if parent is selected
        $lastPriority = 0;
        if ($parentId) {
            $priority     = MenuCategory::select('priority')->where('parent', $parentId)->orderBy('priority', 'DESC')->first();
            $lastPriority = $priority->priority ?? 0;
            $lastPriority++;
        }*/

        $data = [
            'restaurant_id'          => $restaurantId,
            'product_type'           => $request->input('product_type'),
            'name'                   => ucwords($catName),
            'slug'                   => $catSlug,
            'parent'                 => $parentId,
            'priority'               => $request->input('priority'), // ?? $lastPriority,
            'status'                 => $request->input('catStatus'),
            'pos_id'                 => $request->input('pos_id'),
            'description'            => $request->input('catDesc'),
            'meta_title'             => $request->input('meta_title'),
            'meta_keyword'           => $request->input('meta_keyword'),
            'is_delivery'            => $request->input('is_delivery') ?? 1,
            'is_carryout'            => $request->input('is_carryout') ?? 1,
            'is_popular'             => $request->input('is_popular') ?? 0,
            'is_favourite'           => $request->input('is_favourite') ?? 0,
            'language_id'            => $request->input('language_id') ?? 1,
            'image_class'            => $request->input('image_class'),
            'promotional_banner_url' => $request->input('promotional_banner_url'),
        ];

        //dd($data,$parentId,$catId,$createFlag);
        //dump([$request->file('cat_sub_cat_image'),$request->file('cat_sub_cat_image_icon')]);
        //dump($_FILES);
        //dd($request->all(),$data,$createFlag);
        if($createFlag) {
            $res = MenuCategory::create($data);
            $catId = $res->id;
            $menuCategoryLangArr = [
                'language_id' => $request->input('language_id') ?? 1,
                'description' => $request->input('description'),
                'sub_description' => $request->input('sub_description'),
                'name' => strtolower($catName),
                'menu_categories_id' => $catId,
            ];
            MenuCategoryLanguage::create($menuCategoryLangArr);

        } else {
            MenuCategory::where('id',$catId)->update($data);
            $menuCategoryLangObj = MenuCategoryLanguage::where([
                'menu_categories_id' => $catId,
                'language_id' => $request->input('language_id') ?? 1,
            ])->first();

            if ($menuCategoryLangObj !== null) {
                $menuCategoryLangArr = [
                    'description' => $request->input('description'),
                    'sub_description' => $request->input('sub_description'),
                    'name' => strtolower($catName),
                ];
                $menuCategoryLangObj->update($menuCategoryLangArr);
            } else {
                $menuCategoryLangArr = [
                    'language_id' => $request->input('language_id') ?? 1,
                    'description' => $request->input('description'),
                    'sub_description' => $request->input('sub_description'),
                    'name' => strtolower($catName),
                    'menu_categories_id' => $catId,
                ];
                MenuCategoryLanguage::create($menuCategoryLangArr);
            }
        }

        // image upload
        $imageRelPath   = config('constants.image.rel_path.category');
        $imagePath      = config('constants.image.path.category');
        $imagePathThumb = $imagePath . DIRECTORY_SEPARATOR . 'thumb';
        $imageJsonArray = [];
        if(!file_exists($imagePath)) {
            mkdir($imagePath, 0777, true);
        }
        if(!file_exists($imagePathThumb)) {
            mkdir($imagePathThumb, 0777, true);
        }
        if ($request->hasFile('cat_sub_cat_image')) {
            $image = $request->file('cat_sub_cat_image');
            $uniqId       = uniqid(mt_rand());
            $imageName    = $catId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_' . $uniqId . '.' . $image->getClientOriginalExtension();
            $medImageName = $catId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
            $smlImageName = $catId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

            $image = Image::make($image->getRealPath());
            $width = $image->width();
            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePathThumb . DIRECTORY_SEPARATOR . $medImageName);
            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePathThumb . DIRECTORY_SEPARATOR . $smlImageName);

            $imageJsonArray['image'] = json_encode([
                'image'           => $imageRelPath . $imageName,
                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
            ]);
        }
        if ($request->hasFile('cat_sub_cat_image_icon')) {
            $image = $request->file('cat_sub_cat_image_icon');
            $uniqId       = uniqid(mt_rand());
            $imageName    = $catId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_icon_' . $uniqId . '.' . $image->getClientOriginalExtension();
            $medImageName = $catId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_icon_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
            $smlImageName = $catId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_icon_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();
            $image        = Image::make($image->getRealPath());
            $width        = $image->width();

            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

            $imageJsonArray['image_icon'] = json_encode([
                'image'           => $imageRelPath . $imageName,
                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
            ]);
        }
        if ($request->hasFile('category_attachment')) {
            $image = $request->file('category_attachment');
            if($request->file('category_attachment')->getClientOriginalExtension()=="pdf" || $request->file('category_attachment')->getClientOriginalExtension()=="docx" || $request->file('category_attachment')->getClientOriginalExtension()=="doc")
            {
                $imageRelPath = config('constants.image.rel_path.category_attachment');
                // $realPath = $request->file('category_attachment')->getRealPath();

                $imagePath = config('constants.image.path.category_attachment');
                $filname = $request->file('category_attachment')->getClientOriginalName();
                $uniqId = uniqid(mt_rand());
                $filnameNew = $catId . '_' . pathinfo($filname, PATHINFO_FILENAME)
                              . '_' . $uniqId . '.' . $request->file('category_attachment')->getClientOriginalExtension();
                $uploaded_file_path = $imageRelPath.$filnameNew;
                $request->file('category_attachment')->move($imagePath, $filnameNew);
                $imageJsonArray['cat_attachment'] = $uploaded_file_path;
            } else {
                $imageRes = $this->category_file_upload($image, $catId);
                $imageJsonArray['cat_attachment'] = $imageRes[0];
            }
        }
        if ($request->hasFile('promo_banner_image_web')) {
            $image = $request->file('promo_banner_image_web');
            $imageRes = $this->category_file_upload($image, $catId);
            $imageJsonArray['promotional_banner_image'] = $imageRes[0];
        }
        if ($request->hasFile('promo_banner_image_mobile')) {
            $image = $request->file('promo_banner_image_mobile');
            $imageRes = $this->category_file_upload($image, $catId);
            $imageJsonArray['promotional_banner_image_mobile'] = $imageRes[0];
        }
        if ($request->hasFile('promo_banner_image_app')) {
            $image = $request->file('promo_banner_image_app');
            $imageRes = $this->category_file_upload($image, $catId);
            $imageJsonArray['promotional_banner_image_app'] = $imageRes[0];
        }
        //dd($imageJsonArray);

        if(count($imageJsonArray)) {
            MenuCategory::where('id',$catId)->update($imageJsonArray);
        }
        //return Redirect::back()->with('message', 'Category/Sub-Category added successfully');
        //return response()->json([ $request->all(), $request->has('cat_sub_cat_image'), $request->has('cat_sub_cat_image_icon') ]);


        return Redirect::back()->with('message', 'Category/Sub-Category added successfully');
    }

    /**
     * File upload function
     * @param $image
     * @param $id
     * @return array
     */
    private function category_file_upload($image, $id)
    {
        $imagePath    = config('constants.image.path.category_attachment');
        $imageRelPath = config('constants.image.rel_path.category_attachment');

        $uniqId       = uniqid(mt_rand());
        $imageName    = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '.' . $image->getClientOriginalExtension();
        $medImageName = $id . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                        . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();


        $image = Image::make($image->getRealPath());
        $width = $image->width();
        $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
        //echo $imageRelPath.$imageName;exit;
        return [
            $imageRelPath.$imageName
        ];
    }

    public function deleteCategoryFile(Request $request)
    {
        $catId   = $request->input('cat_id');
        $type    = $request->input('type');
        $dbField = '';
        $dbFieldType = '';
        if($type == 'image') {
            $dbField = 'image';
            $dbFieldType = 'json';
        } elseif($type == 'image_icon') {
            $dbField = 'image_icon';
            $dbFieldType = 'json';
        } elseif($type == 'attachment') {
            $dbField = 'cat_attachment';
            $dbFieldType = 'plain';
        } elseif($type == 'banner_web') {
            $dbField = 'promotional_banner_image';
            $dbFieldType = 'plain';
        } elseif($type == 'banner_mobile') {
            $dbField = 'promotional_banner_image_mobile';
            $dbFieldType = 'plain';
        } elseif($type == 'banner_app') {
            $dbField = 'promotional_banner_image_app';
            $dbFieldType = 'plain';
        }

        if(!empty($dbField)) {
            $catInfo = MenuCategory::find($catId);
            if($dbFieldType == 'json') {
                $parsedVal = json_decode($catInfo->$dbField, true);
                if(isset($parsedVal[0])) {
                    @unlink(public_path($parsedVal[0]));
                }
                if(isset($parsedVal[1])) {
                    @unlink(public_path($parsedVal[1]));
                }
                if(isset($parsedVal[2])) {
                    @unlink(public_path($parsedVal[2]));
                }
            } else {
                @unlink(public_path($catInfo->$dbField));
            }
            $catInfo->$dbField = null;
            $catInfo->save();
            $res = 1;
        } else {
            $res = 0;
        }

        return response()->json($res);
    }

    public function storeCatSubCat(Request $request)
    {
        $catId = $request->input('cat_id');
        $parentId = $request->input('parent_id');
        $priority = $request->input('sequence');

        $menuCat = MenuCategory::where('id',$catId)->update(['parent' => $parentId, 'priority' => $priority]);
        return response()->json($menuCat);
    }

    public function getModifierGroups(Request $request)
    {
        $restaurantId = Auth::user()->restaurant_id;
        $itemModifierGroups = ItemModifierGroup::where('restaurant_id', $restaurantId)->groupBy('group_name')->get();

        return response()->json($itemModifierGroups);
    }

    public function saveModifierGroups(Request $request)
    {
        $restaurantId = Auth::user()->restaurant_id;
        $mod_data = $request->input('mod_data');

        try {
            DB::beginTransaction();
            foreach($mod_data as $mod) {
                $itemId     = $mod['item_id'];
                $modQty     = is_array($mod['modifier_qty']) ? implode(",", $mod['modifier_qty']) : 1;
                $modReq     = $mod['required'];
                // DELETE existing on edit item
                $itemRes = ItemModifierGroup::select('id')->where(['menu_item_id' => $itemId, 'restaurant_id' => $restaurantId])->get();
                if($itemRes) {
                    $itemRes        = $itemRes->toArray();
                    $itemGroupIds   = array_column($itemRes, 'id');
                    if(count($itemGroupIds)) {
                        ItemModifier::whereIn('modifier_group_id', $itemGroupIds)->delete();
                    }
                }

                // MODIFIER GROUPS
                $modifier_group = [
                    'restaurant_id' => $restaurantId,
                    'menu_item_id'  => $itemId,
                    'group_name'    => $mod['modifier_name'],
                    'language_id'   => 1,
                    'prompt'        => $mod['modifier_prompt'],
                    'is_required'   => $modReq,
                    'quantity_type' => $mod['modifier_type'],
                    'quantity'      => $modQty
                ];
                $modifierGroupId = ItemModifierGroup::create($modifier_group)->id;
                foreach($mod['modifier_cats'] as $modCat) {
                    // MODIFIER CATEGORIES - category_name
                    // check if category name exists for this restaurant
                    $oldModCat = ModifierCategories::select('id')->where(['category_name' => $modCat['category_name'], 'restaurant_id'=>$restaurantId])->first();
                    if(!$oldModCat) {
                        $modifier_cat = [
                            'category_name' => $modCat['category_name'],
                            'restaurant_id' => $restaurantId,
                            'is_required'   => $modReq
                        ];
                        $modifierCatId = ModifierCategories::create($modifier_cat)->id;
                    } else {
                        $modifierCatId = $oldModCat->id;
                    }
                    // MODIFIER ITEMS
                    $modifier_item = [
                        'modifier_group_id'     => $modifierGroupId,
                        'modifier_category_id'  => $modifierCatId,
                        'modifier_name'         => $modCat['modifier_name'],
                        'size'                 => $modCat['modifier_size'],
                        'price'                 => $modCat['modifier_price']
                    ];
                    $modifierItemId = ItemModifier::create($modifier_item)->id;
                }
            }

            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Modifier has been added successfully',
                'data' => []
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
        }

        return response()->json($data);
    }

    public function saveAddonGroups(Request $request)
    {
        $restaurantId = Auth::user()->restaurant_id;
        $addon_data   = $request->input('adn_data');

        try {
            DB::beginTransaction();
            foreach($addon_data as $addon) {
                $addonItems  = $addon['addon_items'];
                $addonQty    = is_array($addon['addon_qty']) ? implode(",", $addon['addon_qty']) : 1;
                $itemId      = $addon['item_id'];
                if($itemId) {
                    # Delete existing addon group and items (back-forward button)
                    ItemAddonGroups::where(['menu_item_id' => $itemId, 'restaurant_id' => $restaurantId])->delete();
                    ItemAddons::where(['item_id' => $itemId, 'restaurant_id' => $restaurantId])->delete();
                    $addonGroupData = [
                        'restaurant_id' => $restaurantId,
                        'menu_item_id'  => $itemId,
                        'group_name'    => $addon['addon_name'],
                        'language_id'   => 1,
                        'prompt'        => $addon['addon_prompt'],
                        'is_required'   => $addon['required'],
                        'quantity_type' => $addon['addon_type'],
                        'quantity'      => $addonQty
                    ];
                    $res = ItemAddonGroups::create($addonGroupData);
                    $addonGroupId = $res->id;
                    foreach($addonItems as $item) {
                        $addonItemData = [
                            'name'              => $item['item_name'],
                            'restaurant_id'     => $restaurantId,
                            'addon_group_id'    => $addonGroupId,
                            'item_id'           => $itemId,
                            'addon_price'       => $item['item_price'],
                        ];
                        ItemAddons::create($addonItemData);
                    }
                }
            }

            DB::commit();
            $data = [
                'status' => true,
                'message' => 'Addon Item has been added successfully',
                'data' => []
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $data = [
                'status' => true,
                'message' => 'Something went wrong, please try later',
                'data' => []
            ];
        }

        return response()->json($data);
    }

    public function saveMenuImage(Request $request)
    {
        $itemId = $request->input('image_item_id');
        $images = $request->file('menu_item_image');

        // image upload
        $imageRelPath   = config('constants.image.rel_path.menu');
        $imagePath      = config('constants.image.path.menu');
        @file_exists($imageRelPath);
        $newImageArr = [];
        if ($request->hasFile('menu_item_image')) {
            $image = $request->file('menu_item_image');
            $uniqId       = uniqid(mt_rand());
            $imageName    = $itemId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_' . $uniqId . '.' . $image->getClientOriginalExtension();
            $medImageName = $itemId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_' . $uniqId . '_med' . '.' . $image->getClientOriginalExtension();
            $smlImageName = $itemId . '_' . pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)
                            . '_' . $uniqId . '_sml' . '.' . $image->getClientOriginalExtension();

            $image = Image::make($image->getRealPath());
            $width = $image->width();
            $image->save($imagePath . DIRECTORY_SEPARATOR . $imageName);
            $image->resize((int)($width / config('constants.image.size.med')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $medImageName);
            $image->resize((int)($width / config('constants.image.size.sml')), null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($imagePath . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName);

            $newImageArr[] = [
                'image'           => $imageRelPath . $imageName,
                'thumb_image_med' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $medImageName,
                'thumb_image_sml' => $imageRelPath . 'thumb' . DIRECTORY_SEPARATOR . $smlImageName,
            ];
        }
        $menuItem = MenuItemModel::select('id', 'images')->where('id', $itemId)->first();
        if ($menuItem) {
            $menuItem   = $menuItem->toArray();
            $menuImages = json_decode($menuItem['images'], true);
            $oldWebImages = [];
            if (isset($menuImages['desktop_web_images'])) {
                $oldWebImages = $menuImages['desktop_web_images'];
            }
            if (isset($newImageArr)) {
                //dump($oldWebImages);
                //dump($newImageArr);
                //array_push($oldWebImages, $newImageArr);
                $oldWebImages += $newImageArr;
                //dump($oldWebImages);
                $menuImages['desktop_web_images'] = $oldWebImages;
                $menuImages                       = json_encode($menuImages);
                MenuItemModel::where('id', $itemId)->update(['images' => $menuImages]);
            }
        }
        return response()->json(true);
        return Redirect::back()->with('message', 'Menu image uploaded successfully');
    }

    public function saveLabelData(Request $request) {
	//echo"<pre>";print_r($request->all());die;
        $itemId      = $request->input('item_id');
        $labelIds    = $request->input('label_ids');
        $sizeMin     = $request->input('serving_size_min');
        $sizeMax     = $request->input('serving_size_max');
	$ingId     = $request->input('ingId');
	//$nlabelIds = $labelIds;
	 
	if($labelIds!=""){
		$labelIds1 = explode(",",$labelIds);
		foreach($labelIds1 as $lid){
			$lid = (int)$lid;
			$s = (int)$ingId[$lid]; 
			if($s>0 && $s!='')$nlabelIds[]=[$s,$lid]; 	
		} 
		$nlabelIds = json_encode($nlabelIds,true);
	}else $nlabelIds = $labelIds;
	//print_r($nlabelIds);die;
	//$ingId = isset($ingId)?implode(",",$ingId):null;
        $restaurantId= Auth::user()->restaurant_id;
	//ALTER TABLE `cms_item_labels` ADD `labels_with_priority` LONGTEXT NULL AFTER `labels`;
        $result = [];
        if ($itemId) {
            if(is_null($labelIds)) {
                $result = MenuItemLabels::where('item_id', $itemId)->forceDelete();
            } else {
                $itemData = MenuItemLabels::where('item_id', $itemId)->first();
                $data = [
                    'restaurant_id' => Auth::user()->restaurant_id,
                    'item_id'       => $itemId,
                    'labels'        => $labelIds,
		    'labels_with_priority'        => $nlabelIds,
                    'size'          => $sizeMin . ',' . $sizeMax,
                ];
                if($itemData) {
                    $result = MenuItemLabels::where('id', $itemData->id)->update($data);
                } else {
                    $result = MenuItemLabels::create($data);
                }
            }
        }

        return response()->json($result);
    }

    /**
     * TEMP ROUTE - TO DELETE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request)
    {
        $groupRestData = CommonFunctions::getRestaurantGroup();
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        // logged in Restaurant ID
        $restaurantId = Auth::user()->restaurant_id;
        // Menu items for the restaurant


        // Get the list of Categories and Sub-Categories
        $catSubCatList = MenuCategory::where('restaurant_id', $restaurantId)
            ->select('id', 'name')
            ->with(['menuSubCategory' => function($query) {
                $query->select('id', 'name', 'menu_category_id', 'parent')
                    ->with('childrenRecursive')->where('parent', 0);
            }])
            ->orderBy('priority')->get();

        /*$catSubCatList = MenuSubCategory::select('id','name','menu_category_id','parent')
                            ->where(['restaurant_id' => $restaurantId, 'menu_category_id' => 408])
                            ->with('childrenRecursive')->where('parent', 0)
                            ->get();*/


        // Get the list of menu items based on Categories
        $menuCatId = '';
        $catMenuList = MenuItemModel::where('restaurant_id', $restaurantId)
            ->where('menu_category_id', $menuCatId)
            ->orderBy('id', 'DESC')->get();


        // Get the list of menu items based on Sub-Categories
        $menuCatId = '';
        $menuSubCatId = '';
        $subCatMenuList = MenuItemModel::where('restaurant_id', $restaurantId)
            ->where(['menu_category_id' => $menuCatId, 'menu_sub_category_id' => $menuSubCatId])
            ->orderBy('id', 'DESC')->get();


        // BY MENU ITEMS
        $menuItem = MenuItemModel::where('restaurant_id', $restaurantId)
            ->with(['MenuCategory' => function($query) {
                $query->select('id', 'name', 'product_type');
            }])
            ->with(['menuSubCategory' => function($query) {
                $query->select('id', 'name');
            }])
            ->orderBy('id', 'ASC')->limit(20)->get();
        $menuItem = count($menuItem) ? $menuItem->toArray() : $menuItem;

        $allProducts = [];
        foreach($menuItem as $menu) {
            $menuId = $menu['id'];
            $menuCat= $menu['menu_category'];

            #$menuSubCat = $menu->menu_sub_category;

        }

        return response()->json(['groupRestData'    => $groupRestData,
                                 'locations'        => $locations,
                                 'restaurant_id'    => $restaurantId,
                                 'cat_sub_cat_list' => $catSubCatList]);
    }
    public function addModiferOptions(Request $request) { //print_r( $request->all());
        $option_name      = $request->input('option_name');
        $option_description    = $request->input('option_description');
	$option_price    = $request->input('option_price');
        $dependent_modifier_id     = $request->input('dependent_modifier_id');
        $modifierItemId     = $request->input('modifierItemId');
	
	$menuItemId     = $request->input('menuItemId');
        $restaurantId     = $request->input('menuItemRestaurantId');
	$status    = $request->input('status');
	$status    = isset($status)?$status:1;
	$option_size    = $request->input('option_size');
	$option_size    = isset($option_size)?$option_size:'';
	$pos_id    = $request->input('pos_id');
	$pos_id    = isset($pos_id)?$pos_id:'';
	$calorie    = $request->input('calorie');
	$calorie    = isset($calorie)?$calorie:0;
	$sort_order    = $request->input('sort_order');
	$sort_order    =isset($sort_order)?$sort_order:0;
        //$restaurantId= Auth::user()->restaurant_id;
	$result = DB::insert('insert into modifier_options (option_name, restaurant_id,dependent_modifier_id,option_amount,modifier_item_id,menu_item_id,option_description,status,option_size,pos_id,calorie,sort_order) values (?,?,?,?,?,?,?,?,?,?,?,?)', [$option_name, $restaurantId,$dependent_modifier_id,$option_price,$modifierItemId,$menuItemId,$option_description,$status,$option_size,$pos_id,$calorie,$sort_order]);
         
	$results1 = DB::select('select * from modifier_options where modifier_item_id = :id', ['id' => $modifierItemId]);
	
        /*if(isset($results[0])){
			DB::table('restaurant_referral_promotions')->where('restaurant_id',$otherInfo['id']) ->update($d);
	 else if($otherInfo['referral_code']!=""){	
		 	DB::insert('insert into restaurant_referral_promotions (referral_code, referral_code_status,restaurant_id,referral_code_amount) values (?, ?,?,?)', [$d['referral_code'], $d['referral_code_status'],$d['restaurant_id'],$d['referral_code_amount']]);
	 }*/

        return response()->json(["status"=>$result,"data"=>$results1]);
    }
    public function listModiferOptions(Request $request,$modifierItemId) { //print_r( $request->all());
        $results1 = DB::select('select * from modifier_options where modifier_item_id = :id order by dependent_modifier_id', ['id' => $modifierItemId]);
	//echo"<pre>";print_r($results1[0]->id);die;
        return response()->json(["data"=>$results1,"count"=>count($results1)]);
    } 	
    
}
