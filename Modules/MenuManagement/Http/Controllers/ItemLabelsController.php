<?php

namespace Modules\MenuManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Restaurant;
use App\Helpers\CommonFunctions;
use Modules\MenuManagement\Entities\ItemLabels;
use Modules\MenuManagement\Http\Requests\ItemLabelsRequest;
use DB;

class ItemLabelsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurant_id = 0;
        $locations = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));        
        $restaurantData = Restaurant::select('id','restaurant_name')->whereIn('id',$locations)->orderBy('id','DESC')->get();
        if($restaurantData && count($restaurantData) == 1) { 
           $restaurant_id = $restaurantData[0]['id'];
        }

        if($request->input('restaurant_id') && is_numeric($request->input('restaurant_id'))){
            $restaurant_id = $request->input('restaurant_id');
            $data = ItemLabels::where('restaurant_id',$restaurant_id)->orderBy('id','DESC')->paginate(5);
        }else{
            $data = ItemLabels::orderBy('id','DESC')->whereIn('restaurant_id',$locations)->paginate(5);
        }
       
        return view('menumanagement::ItemLabels.index', compact('data','restaurantData','restaurant_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $restaurantData = CommonFunctions::getRestaurantGroup();   
        $selected_rest_id = 0;
        if(count($restaurantData) == 1) {
            $first_ind_array = current($restaurantData);
            if(count($first_ind_array['branches']) == 1) {
                $selected_rest_id = $first_ind_array['branches']['0']['id'];
            }
        }   
       return view('menumanagement::ItemLabels.create', compact('restaurantData', 'selected_rest_id'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ItemLabelsRequest $request)
    {
         try {
            DB::beginTransaction();
            
            $data = $request->all();
            ItemLabels::create([
                'restaurant_id' => $data['restaurant_id'],
                'label_name' => $data['label_name'],               
            ]);

            DB::commit();
          return back()->with('status_msg', 'Item Label has been created successfully.');

        } catch (\Exception $e) {
            DB::rollback();
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('menumanagement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
       $restaurantData = CommonFunctions::getRestaurantGroup();  
        $data = ItemLabels::find($id);

        return view('menumanagement::ItemLabels.edit', compact('restaurantData','data'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    

    public function update(ItemLabelsRequest $request, $id)
    {
         if(!isset($id) || !is_numeric($id)){
            return redirect()->back()->with('err_msg','Invalid Request');
        }

        $data = $request->all();       
        DB::beginTransaction();
        try {
           

           ItemLabels::where('id', $id)
                ->update(
                    ['restaurant_id' => $data['restaurant_id'],
                    'label_name'=>$data['label_name']

                     ]);
                DB::commit();
               return back()->with('status_msg', 'Item Label has been updated successfully.');
            

        } catch (\Exception $e) {
            DB::rollback();
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));

        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            ItemLabels::where('id', $id)->delete();
            DB::commit();

            return back()->with('status_msg', 'Item Label has been deleted successfully.');

        }
        catch (\Exception $e){
            DB::rollback();
            return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
        }
    }
}
