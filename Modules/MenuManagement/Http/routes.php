<?php

Route::group(['middleware' => 'web', 'prefix' => '/menu/', 'namespace' => 'Modules\MenuManagement\Http\Controllers'], function() {
    // Route::get('/label/list', 'ItemLabelsController@index');
    Route::get('merchandise', 'MenuItemController@product');
	
    Route::resource('label', 'ItemLabelsController');
    Route::resource('modifier-group', 'ItemModifierGroupController');
    Route::post('item/images/{id}', 'MenuItemController@imagesupdate');
    Route::post('item/modifiergroup/{id}/{languageId}', 'MenuItemController@modifiergroup');
    Route::post('item/modifiergroup/{id}', 'MenuItemController@modifiergroup');
    Route::delete('modifieritem/{id}', 'MenuItemController@deleteModifierItem');
    Route::get('modifieritem/{id}/{languageId}', 'ItemModifierController@getModifierItemInfo');
    Route::get('modifieritem/{id}', 'ItemModifierController@getModifierItemInfo');
    Route::post('item/modifieroptiongroup', 'ItemModifierController@saveItemModifierOptionGroup');
    Route::delete('item/modifieroptiongroup/{id}', 'ItemModifierController@deleteModifierItemOptionGroup');
    Route::post('item/modifieroption/{id}', 'ItemModifierController@saveModifierOption');
    Route::delete('modifieroption/{id}', 'ItemModifierController@deleteModifierOption');

    Route::get('item/{id}/edit/{language_id}', 'MenuItemController@edit');
    Route::get('item/{id}/destroy/', 'MenuItemController@destroy');	
    Route::resource('item', 'MenuItemController');
    
    Route::resource('item-addon-groups', 'ItemAddonGroupsController');
    Route::get('item-addon-groups/list/{restaurantId}/{menuItemId}/{languageId}', 'ItemAddonGroupsController@index');
    Route::post('item-addon-groups/add', 'ItemAddonGroupsController@store');
    Route::put('item-addon-groups/update/{addonGroupId}', 'ItemAddonGroupsController@update');
    Route::delete('item-addon-groups/destroy/{id}', 'ItemAddonGroupsController@destroy');
    Route::resource('item-addons', 'ItemAddonsController');
    Route::get('item-addons/list/{restaurantId}/{addonGroupId}', 'ItemAddonsController@menuitems');
    Route::post('item-addons/add', 'ItemAddonsController@store');
    Route::delete('item-addons/destroy/{id}', 'ItemAddonsController@destroy');

    Route::get('modifier_categories', 'ModifierCategoriesController@index');
    Route::get('modifier_categories/create', 'ModifierCategoriesController@create');
    Route::post('modifier_categories/store', 'ModifierCategoriesController@store');
    Route::get('modifier_categories/{id}/edit', 'ModifierCategoriesController@edit');
    Route::put('modifier_categories/update/{id}', 'ModifierCategoriesController@update');
    Route::delete('modifier_categories', 'ModifierCategoriesController@destroy');

    Route::get('menu-item-visibility/list/{restaurantId}/{menuItemId}', 'MenuItemsVisibilityController@index');
    Route::get('menu-item-visibility/add/{visibilityScheduleId}/{menuItemId}', 'MenuItemsVisibilityController@store');
    Route::get('merchandise', 'MenuItemController@product');
    Route::get('localization', 'LocalizationController@index');
    Route::get('localization/create', 'LocalizationController@create');
    Route::post('localization/store', 'LocalizationController@store');
    Route::get('localization/{id}/edit', 'LocalizationController@edit');
    Route::put('localization/update/{id}', 'LocalizationController@update');
    Route::delete('localization/{id}', 'LocalizationController@destroy');

    Route::get('all_items', 'MenuItemController@all_items');
    Route::get('schedules', 'MenuItemController@schedules');
    Route::get('modifiers', 'MenuItemController@modifiers');
    Route::get('labels', 'MenuItemController@labels');

    // MENU MANAGEMENT - AJAX Calls
    Route::post('get_data',                 'MenuItemController@getData');              // Temp route to test

    Route::get('all_cat_sub_cat',           'MenuItemController@allCatSubCatList');     // All Categories and Sub-Categories
    Route::post('get_cat_sub_cat',          'MenuItemController@getCatSubCat');         // Get a Category and its Sub-Categories
    Route::post('save_cat_sub_cat',         'MenuItemController@storeCatSubCat');       // Store Categories and Sub-Categories - NOT IN USE
    Route::post('menu_items',               'MenuItemController@getMenuItems');         // Menu items for a category or subcategory
    Route::get('menu_item/{id}',            'MenuItemController@getMenuItem');          // Get single menu item details
    Route::post('add_category',             'MenuItemController@addCategory');          // Add new Category
    Route::post('delete_category_file',     'MenuItemController@deleteCategoryFile');   // Delete Category file
    Route::post('add_menu_item',            'MenuItemController@addMenuItem');          // Add/Update new menu item
    Route::get('get_modifier_groups',       'MenuItemController@getModifierGroups');    // Get Modifier Groups
    Route::get('get_modifier_categories',   'MenuItemController@getModifierCategories');// Get Modifier Categories
    Route::post('save_modifier_group',      'MenuItemController@saveModifierGroups');   // Save Modifier Groups
    Route::post('save_addon_group',         'MenuItemController@saveAddonGroups');      // Save Addon Groups
    Route::post('save_label_data',          'MenuItemController@saveLabelData');        // Save Label data
    Route::post('save_menu_image',          'MenuItemController@saveMenuImage');        // Save Modifier Groups
    Route::post('validate_cat_slug',        'MenuItemController@validateCatSlug');      // Save Modifier Groups

    Route::post('toggleactive',             'MenuItemController@toggleMenuActive');     // Toggle menu active status
    Route::post('/item/modifieritem/options/add', 'MenuItemController@addModiferOptions');
    Route::get('/item/modifieritem/options/list/{id}', 'MenuItemController@listModiferOptions');
});

/*
Route::group(['middleware' => 'web', 'prefix' => '/v2/menu', 'namespace' => 'Modules\MenuManagement\Http\Controllers\v2'], function() {
    
    //Route::resource('modifier-group', 'ItemModifierGroupController');
    Route::get('item/{menu_id}/edit-item', 'MenuItemController@editItem');
    Route::get('item/{cat_id}/{rest_id}/{sort_type_id}/list', 'MenuItemController@index');
    Route::get('item/{cat_id}/{rest_id}/{sort_type_id}/{category_filter_by}/{product_type_filter_by}/{status_filter_by}/{type_filter_by}/list', 'MenuItemController@index');			
    Route::post('item/{menu_id}/update-type', 'MenuItemController@updateItemType');	
    Route::post('item/images/{id}', 'MenuItemController@imagesupdate');
    
    //Route::post('item/modifiergroup/{id}', 'MenuItemController@modifiergroup');
    Route::delete('modifieritem/{id}', 'MenuItemController@deleteModifierItem');
    Route::get('modifieritem/{id}/{languageId}', 'ItemModifierController@getModifierItemInfo');
    Route::get('modifieritem/{id}', 'ItemModifierController@getModifierItemInfo');
    Route::post('item/modifieroptiongroup', 'ItemModifierController@saveItemModifierOptionGroup');
    Route::delete('item/modifieroptiongroup/{id}', 'ItemModifierController@deleteModifierItemOptionGroup');
    Route::post('item/modifieroption/{id}', 'ItemModifierController@saveModifierOption');
    Route::delete('modifieroption/{id}', 'ItemModifierController@deleteModifierOption');

    Route::get('item/{id}/edit/{language_id}', 'MenuItemController@edit');
    Route::get('item/{id}/destroy/', 'MenuItemController@destroy');	
    Route::resource('item', 'MenuItemController');
    //////////////////////Route::get('/item/generalinfo', 'MenuItemController@index');	
     
    Route::post('item/generalinfo', 'MenuItemController@generalInfo')->name('menu-item-add');
    //Route::get('item/generalinfo', 'MenuItemController@generalInfo')->name('menu-item-add-redirect');	
    Route::post('item/attributeinfo', 'MenuItemController@attributeInfo');
    Route::post('/get_category', 'MenuItemController@getCategory')->name('category-data');	
    Route::post('/get_sub_category', 'MenuItemController@getSubCategory')->name('category-data');
    Route::get('item/{menu_id}/addprice', 'MenuItemController@addPrice')->name('menu-item-price-add');
    Route::get('item/{menu_id}/addinventory', 'MenuItemController@addInventoryInfo')->name('menu-item-inventory-add');
    Route::post('/item/{menu_id}/updateinventory', 'MenuItemController@addUpdateInventory')->name('menu-item-inventory-update');

    Route::post('/item/{menu_id}/updateprice', 'MenuItemController@addUpdatePrice')->name('menu-item-price-update');
    Route::get('/item/associatedproduct/{menu_id}/list', 'MenuItemController@listAssociatedProduct')->name('menu-item-list-associated');
    Route::post('/item/associatedproduct/{menu_id}/add', 'MenuItemController@addAssociatedProduct')->name('menu-item-add-associated');
    Route::post('item/modifiergroup/{menu_id}', 'MenuItemController@modifiergroup');
    Route::get('item/modifiergroup/{menu_id}/list', 'MenuItemController@listModifiers')->name('menu-modifier-group-list');
    Route::get('item/modifiergroup/item/{modifier_id}', 'MenuItemController@listModifierItem')->name('menu-modifier-item-list');
    Route::get('item/modifiergroup/iteminfo/{id}', 'MenuItemController@getModifierItemInfo')->name('menu-modifier-item-info');		
    Route::post('item/{menu_id}/addmodifiergroup', 'MenuItemController@createModfierGroup')->name('menu-modifier-group-add');
    Route::post('item/{menu_id}/addmodifiergroupitem', 'MenuItemController@createModfierGroupItem')->name('menu-modifier-group-item-add');
    Route::delete('item/modifiergroup/itemdelete/{id}/', 'MenuItemController@destroyModifierItem')->name('menu-addon-item-delete');
    Route::post('item/{menu_id}/updatemodifiergroup', 'MenuItemController@updatemodifiergroup')->name('menu-modifier-group-item-add');

	
    Route::get('/item/{menu_id}/editgeneralinfo', 'MenuItemController@editGeneralInfo')->name('menu-item-edit');
    Route::post('item/{menu_id}/updateitem', 'MenuItemController@updateGeneralInfo')->name('menu-modifier-group-item-add');
    Route::get('item/images/{menu_id}/list', 'MenuItemController@listImages')->name('menu-images-list');
    Route::post('item/images/{menu_id}/updateitemimages', 'MenuItemController@addUpdateImageInMenuItem')->name('menu-item-add-images');
    Route::delete('item/deletemodifiergroup/{id}/', 'MenuItemController@deleteModfierGroup')->name('menu-modifier-group-delete');
    Route::get('item/availability/{menu_id}/list', 'MenuItemController@listAvailability')->name('menu-availability-list'); 
    Route::post('item/{menu_id}/updateavailability', 'MenuItemController@addUpdateAvailability')->name('menu-update-availability');
    Route::get('item/meal/{menu_id}/list', 'MenuItemController@listAddons')->name('menu-addons-list'); 	
    Route::post('item/meal/{menu_id}/addmeal', 'MenuItemController@addUpdateAddons')->name('menu-addons-add'); 
    Route::post('item/meal/{menu_id}/addmealitem', 'MenuItemController@addUpdateAddonsItems')->name('menu-addons-items'); 
    Route::get('item-addons/list/{group_id}', 'MenuItemController@listAddonItems'); 
    Route::post('item-addons/add', 'MenuItemController@insertAddonItem');
    Route::delete('item-addons/deleteitem/{id}/', 'MenuItemController@destroyAddonItem')->name('menu-addon-item-delete');
    Route::delete('item-addons/deletegroup/{id}/', 'MenuItemController@destroyAddonGroup')->name('menu-addon-item-delete');
    

    Route::get('item/related_products/{menu_id}/list', 'MenuItemController@listRelatedProducts')->name('menu-related-list'); 	
    Route::post('item/related_product/{menu_id}/addrelatedproduct', 'MenuItemController@addUpdateRelatedProducts')->name('menu-addons-add'); 
    Route::post('item/relatedproduct/{menu_id}/addrelatedproductitem', 'MenuItemController@addUpdateRelatedProductItems')->name('menu-addons-items'); 
    Route::get('item-relatedproduct/list/{group_id}', 'MenuItemController@listRelatedProductItems'); 
    Route::post('item-relatedproduct/{related_product_id}/add', 'MenuItemController@insertRelatedProductItem');
    Route::delete('item-relatedproduct/deleteitem/{id}/', 'MenuItemController@destroyRelatedProductItem')->name('menu-related-item-delete');
    Route::delete('item-relatedproduct/deletegroup/{id}/', 'MenuItemController@destroyRelatedProductGroup')->name('menu-related-item-delete');	
    
    Route::get('item/meal/{menu_id}/list', 'MenuItemController@listMeals')->name('menu-Meal-list'); 	
    Route::post('item/meal/{menu_id}/addmeal', 'MenuItemController@addUpdateMeals')->name('menu-Meal-add'); 
    Route::post('item/meal/{menu_id}/addmealitem', 'MenuItemController@addUpdateMealsItems')->name('menu-Meal-items'); 
    Route::get('item-meals/list/{group_id}', 'MenuItemController@listMealItems'); 
    Route::post('item-meals/add', 'MenuItemController@insertMealItem');
    Route::delete('item-meals/deleteitem/{id}/', 'MenuItemController@destroyMealItem')->name('menu-Meal-item-delete');
    Route::delete('item-meals/deletegroup/{id}/', 'MenuItemController@destroyMealGroup')->name('menu-Meal-item-delete');
	

    Route::get('item/addons/{menu_id}/list', 'MenuItemController@listAddons')->name('menu-addons-list'); 	
    Route::post('item/addons/{menu_id}/addaddon', 'MenuItemController@addUpdateAddons')->name('menu-addons-add'); 
    Route::post('item/addons/{menu_id}/addaddonitem', 'MenuItemController@addUpdateAddonsItems')->name('menu-addons-items'); 
    Route::get('item-addons/list/{group_id}', 'MenuItemController@listAddonItems'); 
    Route::post('item-addons/add', 'MenuItemController@insertAddonItem');
    Route::delete('item-addons/deleteitem/{id}/', 'MenuItemController@destroyAddonItem')->name('menu-addon-item-delete');
    Route::delete('item-addons/deletegroup/{id}/', 'MenuItemController@destroyAddonGroup')->name('menu-addon-item-delete'); 	
	
    Route::get('clear-cache', 'MenuItemController@clearMenuCache')->name('menu-cache'); 
    Route::get('image-editor', 'MenuItemController@imageEditor')->name('image-editor'); 
    Route::post('toggleactive', 'MenuItemController@toggleMenuActive');     // Toggle menu active status	
});
*/
