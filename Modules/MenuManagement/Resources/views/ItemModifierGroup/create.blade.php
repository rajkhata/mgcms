@extends('layouts.app')
@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>{{ __('Item Modifier Group - Add') }}</h1>

            </div>

        </div>

        <div>

            <div class="card form__container">
                                    @include('menumanagement::common.message')

                <div class="card-body">
                    <div class="form__user__edit">
                        <form method="POST" action="{{ route('label.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row form_field__container">
                                <label for="restaurant_id"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                                <div class="col-md-4">
                                    <select name="restaurant_id" id="restaurant_id"
                                            class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="">Select Restaurant</option>
                                        @foreach($restaurantData as $rest)
                                            <optgroup label="{{ $rest['restaurant_name'] }}">
                                                @foreach($rest['branches'] as $branch)
                                                    <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                  
                                    @if ($errors->has('restaurant_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="group_name" type="text"
                                               class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
                                               name="group_name" value="{{ old('group_name') }}" required>
                                    </div>
                                    @if ($errors->has('group_name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Prompt') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="prompt" type="text"
                                               class="{{ $errors->has('prompt') ? ' is-invalid' : '' }}"
                                               name="prompt" value="{{ old('prompt') }}" required>
                                    </div>
                                    @if ($errors->has('prompt'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('prompt') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Required') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="status1" name="is_required" value="1" {{ (old('is_required')=='1') ? 'checked' :'' }}   > Yes
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="status0" name="is_required" value="0" {{ (old('is_required')=='0') ? 'checked' :'' }}> No
                                @if ($errors->has('is_required'))
                                <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('is_required') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                             <div class="form-group row form_field__container">
                                <label for="quantity_type"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Quantity Type') }}</label>
                                <div class="col-md-4">
                                    <select name="quantity_type" id="quantity_type"
                                            class="form-control{{ $errors->has('quantity_type') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="">Select Quantity Type</option>
                                        <option value="Value">Value</option>
                                        <option value="Range">Range</option>
                                      
                                    </select>
                                  
                                    @if ($errors->has('restaurant_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <br/><br/>
                            <div class=" mb-0 ">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        var check_restaurant = '<?php echo $selected_rest_id;?>';
        if (check_restaurant != 0) {
            // trigger restautaurnat checks
            $("#restaurant_id").val(check_restaurant);
            //$("#restaurant_id").attr('change');
        }
    </script>

@endsection
