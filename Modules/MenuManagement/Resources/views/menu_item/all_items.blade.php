@extends('layouts.app')

@section('content')

@php
    // Recursive function to get Sub categories to nth level
    function generate_sub_cat_recursive($catData) {
        $subCatHtml = '';
        foreach($catData['children_recursive'] as $subcat) {
            $subCatHtml .= '<ul class="accordion-subCategory">'
                            .'<li class="accordion-subcategoryItem">'
                                .'<div class="catergoryName">'
                                    .'<div class="tabTrigger fetch-menu-items" data-cat="'.$subcat['parent'] .'" data-sub-cat="'. $subcat['id'] .'">'. ucwords($subcat['name']) .'</div>'
                                    .'<div class="edit-icon">'
                                        .'<i class="fa fa-pencil" aria-hidden="true"></i>'
                                    .'</div>'
                                .'</div>';
            if(isset($subcat['children_recursive']) && count($subcat['children_recursive'])){
                $subCatHtml .= generate_sub_cat_recursive($subcat);
            }
            $subCatHtml .= '</li>'
                        .'</ul>';
        }
        return $subCatHtml;
    }
    function generate_sub_cat_recursive_new($catData) {
        $subCatHtml = '';
        foreach($catData['children_recursive'] as $subcat) {
            $subCatHtml .= '<ul class="accordion-subCategory-new">'
                            .'<li class="accordion-subcategoryItem-new">'
                                .'<i class="icon-hdd"></i><span class="node-cpe fetch-menu-items" data-cat="'.$subcat['parent'] .'" data-sub-cat="'. $subcat['id'] .'">'. ucwords($subcat['name']) .'</span>'
                                .'<div class="edit-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>';
            if(isset($subcat['children_recursive']) && count($subcat['children_recursive'])){
                $subCatHtml .= generate_sub_cat_recursive_new($subcat);
            }
            $subCatHtml .= '</li>'
                        .'</ul>';
        }
        return $subCatHtml;
    }
@endphp

    <div class="main__container container__custom container__custom_working">

        {{--common in all pages(Mostly)--}}
        <div class="common__pageHeader">

            {{--Page Title--}}
            <div class="common__pageTitle">
                <div class="side-bar-open hidden margin-top-minus-5"><i class="fa fa-bars" aria-hidden="true"></i></div>Menu
            </div>

            {{--Page Search Box--}}
            {{--<div class="common__searchBox">
                <i class="fa fa-search"></i>
                <input type="text" id="search_menu_item" placeholder="Search Menu Items">
            </div>--}}

            {{--Page Additional Functions (Buttons etc.)--}}
            <div class="common__buttonContainer">
                <div class="btn btn__holo" id="add_cat_btn">Add Category</div>
                <div class="btn btn__holo" id="add_item_btn">Add Item</div>
            </div>

        </div>

        {{--Page Body--}}
        <div class="menuItem__container">
            <div class="menuItem__accTabs">

                {{--sortable list--}}
                <div class="accordion-panel">

                    <ul class="accordion-categoryList-new" id="sidebar-category-list">

                        {{--@if(isset($catSubCatList))
                            @foreach($catSubCatList as $cat)
                                <li class="accordion-category">
                                    <div class="catergoryName">
                                        <div class="tabTrigger fetch-menu-items" data-cat="{{ $cat['id'] }}" data-sub-cat="">{{ ucwords($cat['name']) }}</div>
                                        <div class="edit-icon">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </div>
                                    </div>

                                    sub Categories

                                    @php
                                        if(count($cat['children_recursive'])) {
                                            echo generate_sub_cat_recursive($cat);
                                        }
                                    @endphp
                                </li>
                            @endforeach
                        @endif--}}

                        @if(isset($catSubCatList))
                            @foreach($catSubCatList as $cat)
                                <li class="accordion-category-new">

                                    <span class="node-facility fetch-menu-items" data-cat="{{ $cat['id'] }}">{{ ucwords($cat['name']) }}</span>
                                    <div class="edit-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>
                                    {{--sub Categories--}}
                                    @php
                                        if(count($cat['children_recursive'])) {
                                            echo generate_sub_cat_recursive_new($cat);
                                        }
                                    @endphp
                                </li>
                            @endforeach
                        @endif

                    </ul>

                </div>

                {{--Category Container--}}
                <div class="menuItem__tabContainer">
                    {{--All Items--}}
                    <div class="menuItem__allItem">
                        {{-- Breadcrubs --}}
                        <div class="menuItem__headerAlt padding-top-10 padding-bottom-20">
                            <div class="headerTitle">Menu <span> > </span> All Items</div>
                        </div>
                        <div id="menuItem_wrapper"></div>
                    </div>
                    {{--Add Category--}}
                    <div class="menuItem__newCat hidden">
                        <div class="menuItem__header">
                            <div class="headerTitle" id="menu_cat_title">Add a New Category</div>
                        </div>
                        <form id="cat_sub_cat_form" action="/menu/add_category" method="POST" enctype="multipart/form-data">
                        <div class="menuItem__newCatContainer flex-justify-space-between">

                            <div class="categoryContainer col-sm-12 col-lg-5 no-padding-left">

                                <div class="categoryForm margin-top-7 fullWidth">
                                    {{ csrf_field() }}

                                    <div class="row">
                                        <div class="font-weight-800">Select Category</div>
                                        <div class="selct-picker-plain fullWidth">
                                            <select class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700" title="Select Category" data-max-options="2" data-size="8" name="add_cat_id" class="selectpicker" id="cat_dropdown">
                                                <option value="0" selected>Select Category</option>
                                            </select>
                                        </div>
                                    </div>

                                    {{--<div class="row margin-top-30">
                                        <div class="font-weight-800">Sub Category</div>
                                        <div class="selct-picker-plain fullWidth">
                                            <select class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700" name="add_cat_sub_id" class="selectpicker" id="sub_cat_dropdown">
                                                <option value="0" selected>Select Sub-Category</option>
                                            </select>
                                        </div>
                                    </div>--}}

                                    <div class="input__field margin-top-30">
                                        <input type="hidden" name="cat_id" id="cat_id" value=""/>
                                        <input id="catName" type="text" name="catName" autocomplete="off">
                                        <label for="catName">Category Name</label>
                                    </div>

                                    <div class="input__field">
                                        <input id="catDesc" class="" type="text" name="catDesc" autocomplete="off">
                                        <label for="catDesc">Description (Optional)</label>
                                    </div>
                                    <div class="input__field-radio">
                                        <label>Status</label>
                                        <div class="subFunctions" id="catStatus_wrap">
                                            <div class="subVisible">
                                                <i class="icon-eye-open productVisibilityTrigger" aria-hidden="true"></i>
                                                <i class="icon-eye-close hidden productVisibilityTrigger" aria-hidden="true"></i>
                                            </div>
                                            <input type="hidden" name="catStatus" id="catStatus" value="1"/>
                                        </div>
                                    </div>
                                    {{--<div class="addedSubContainer">
                                        <div class="subContainerHeader" id="menu_sub_cat_title">Sub - Category</div>
                                        <div class="subRowContainer">

                                            <div class="subRow">
                                                <div class="subCatName">Hall of Famers</div>
                                                <div class="subFunctions">
                                                    <div class="subVisible">
                                                        <i class="fa fa-eye productVisibilityTrigger" aria-hidden="true"></i>
                                                        <i class="fa fa-eye-slash hidden productVisibilityTrigger" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="subRemove"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="addCategory">
                                        <div class="btn btn__holo">Add Sub - Category</div>
                                    </div>--}}

                                    <div class="input__field margin-top-30">
                                        <input id="priority" type="text" name="priority" autocomplete="off">
                                        <label for="priority">Priority</label>
                                    </div>
                                    <div class="input__field margin-top-30">
                                        <input id="pos_id" type="text" name="pos_id" autocomplete="off">
                                        <label for="pos_id">Pos Id</label>
                                    </div>
                                    <div class="input__field margin-top-30">
                                        <input id="slug" type="text" name="slug" autocomplete="off">
                                        <label for="slug">Slug</label>
                                    </div>
                                    <div class="input__field margin-top-30">
                                        <label for="delivery_carryout">{{ __('Delivery / Carryout') }}</label><br />
                                        <input type="checkbox" value="1" id="is_delivery" name="is_delivery">Delivery
                                        <input type="checkbox" value="1" id="is_carryout" name="is_carryout">Carryout
                                    </div>
                                    <div class="input__field margin-top-30">
                                        <label for="is_popular">{{ __('Is Popular') }}</label><br />
                                        <input id="is_popular" value="1" type="checkbox" class="" name="is_popular">
                                    </div>
                                    <div class="input__field margin-top-30">
                                        <label for="is_favourite">{{ __('Is Favorite') }}</label><br />
                                        <input id="is_favourite" value="1" type="checkbox" class="" name="is_favourite">
                                    </div>
                                    <div class="row margin-top-30">
                                        <div class="font-weight-800">Select Product Type</div>
                                        <div class="selct-picker-plain fullWidth">
                                            <select name="product_type" id="product_type" class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }}">
                                                <option value="">Select Product Type</option>
                                                @foreach($product_types as $key=>$val)
                                                    <option value="{{ $key }}">{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input__field margin-top-30">
                                        <input id="image_class" type="text" name="image_class" autocomplete="off">
                                        <label for="image_class">Image Class</label>
                                    </div>
                                    <div class="input__field margin-top-30">
                                        <input id="meta_title" type="text" name="meta_title" autocomplete="off">
                                        <label for="meta_title">Meta Title</label>
                                    </div>
                                    <div class="input__field margin-top-30">
                                        <input id="meta_keyword" type="text" name="meta_keyword" autocomplete="off">
                                        <label for="meta_keyword">Meta Keyword</label>
                                    </div>


                                </div>


                            </div>

                            <div class="uploadImageContainer col-sm-12 col-lg-5 no-padding-right margin-top-15">
                                <!-- Language Dropdown -->
                                @if(count($langData) > 1)
                                    <div class="uploadedMediaContainer">
                                        <div class="uploadedImagesContainer">
                                            <div class="subContainerHeader">Language</div>
                                            {{--Uploaded Image Row--}}
                                            <div class="uploadedImageRow">
                                                <div class="upload__file fullWidth"> {{--col-xs-6 no-padding-left--}}
                                                    <select class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700" title="Select Category" data-max-options="2" data-size="8" name="language_id" class="selectpicker" id="language_id">
                                                        <option value="">Select Language</option>
                                                        @foreach($langData as $lang):
                                                            <option value="{{ $lang->id }}">{{ $lang->language_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <!-- IMAGE -->
                                <div class="uploadedMediaContainer">

                                    <div class="uploadedImagesContainer">
                                        <div class="subContainerHeader">Image</div>
                                        {{--Uploaded Image Row--}}
                                        <div class="row uploadedImageRow hide">
                                            <div class="col-xs-7 col-md-6 no-padding-left font-size-16">
                                                <span class="font-weight-700">Uploaded Image</span> <span class="text-color-grey"></span>
                                            </div>

                                            <div class="col-xs-5 col-md-6 text-right padding-right-30">
                                                <img src="{{asset('images/cat_image_placeholder.jpg')}}" width="90px" height="60px" id="cat_image" />
                                            </div>

                                            <div class="col-xs-1 no-padding">
                                                <a href="javascript:void(0)" class="cat_file_remove">
                                                    <span class="icon-cancelled text-color-black font-size-16"></span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="uploadedImageRow">
                                            <div class="upload__file fullWidth">
                                                <input type="button" class="btn btn__holo fullWidth font-weight-600" value="Upload Image" />
                                                <input type="file" name="cat_sub_cat_image" class="hide" multiple />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- IMAGE ICON -->
                                <div class="uploadedMediaContainer">
                                    <div class="uploadedImagesContainer">
                                        <div class="subContainerHeader">Image (Icon)</div>
                                        <div class="row uploadedImageRow hide">
                                            <div class="col-xs-7 col-md-6 no-padding-left font-size-16">
                                                <span class="font-weight-700">Uploaded Image</span> <span class="text-color-grey"></span>
                                            </div>
                                            <div class="col-xs-5 col-md-6 text-right padding-right-30">
                                                <img src="{{asset('images/cat_image_placeholder.jpg')}}" width="90px" height="60px" id="cat_image_icon" />
                                            </div>
                                            <div class="col-xs-1 no-padding">
                                                <a href="javascript:void(0)" class="cat_file_remove">
                                                    <span class="icon-cancelled text-color-black font-size-16"></span>
                                                </a>
                                            </div>
                                        </div>
                                        {{--Uploaded Image Row--}}
                                        <div class="uploadedImageRow">
                                            <div class="upload__file fullWidth"> {{--col-xs-6 no-padding-left--}}
                                                <input type="button" class="btn btn__holo fullWidth font-weight-600" value="Upload Image" />
                                                <input type="file" name="cat_sub_cat_image_icon" class="hide" multiple />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ATTACHMENT -->
                                <div class="uploadedMediaContainer">
                                    <div class="uploadedImagesContainer">
                                        <div class="subContainerHeader">Category Attachment</div>
                                        <div class="row uploadedImageRow hide">
                                            <div class="col-xs-7 col-md-6 no-padding-left font-size-16">
                                                <span class="font-weight-700">Uploaded Image</span> <span class="text-color-grey"></span>
                                            </div>
                                            <div class="col-xs-5 col-md-6 text-right padding-right-30">
                                                <img src="{{asset('images/cat_image_placeholder.jpg')}}" width="90px" height="60px" id="category_attachment" />
                                            </div>
                                            <div class="col-xs-1 no-padding">
                                                <a href="javascript:void(0)" class="cat_file_remove">
                                                    <span class="icon-cancelled text-color-black font-size-16"></span>
                                                </a>
                                            </div>
                                        </div>
                                        {{--Uploaded Image Row--}}
                                        <div class="uploadedImageRow">
                                            <div class="upload__file fullWidth"> {{--col-xs-6 no-padding-left--}}
                                                <input type="button" class="btn btn__holo fullWidth font-weight-600" value="Upload File" />
                                                <input type="file" name="category_attachment" class="hide" multiple />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- BANNER WEB -->
                                <div class="uploadedMediaContainer">
                                    <div class="uploadedImagesContainer">
                                        <div class="subContainerHeader">Promotional Banner Image (Web)</div>
                                        <div class="row uploadedImageRow hide">
                                            <div class="col-xs-7 col-md-6 no-padding-left font-size-16">
                                                <span class="font-weight-700">Uploaded Image</span> <span class="text-color-grey"></span>
                                            </div>

                                            <div class="col-xs-5 col-md-6 text-right padding-right-30">
                                                <img src="{{asset('images/cat_image_placeholder.jpg')}}" width="90px" height="60px" id="banner_image_web" />
                                            </div>

                                            <div class="col-xs-1 no-padding">
                                                <a href="javascript:void(0)" class="cat_file_remove">
                                                    <span class="icon-cancelled text-color-black font-size-16"></span>
                                                </a>
                                            </div>
                                        </div>
                                        {{--Uploaded Image Row--}}
                                        <div class="uploadedImageRow">
                                            <div class="upload__file fullWidth"> {{--col-xs-6 no-padding-left--}}
                                                <input type="button" class="btn btn__holo fullWidth font-weight-600" value="Upload Image" />
                                                <input type="file" name="promo_banner_image_web" class="hide" multiple />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- BANNER MOBILE -->
                                <div class="uploadedMediaContainer">
                                    <div class="uploadedImagesContainer">
                                        <div class="subContainerHeader">Promotional Banner Image (Mobile)</div>
                                        <div class="row uploadedImageRow hide">
                                            <div class="col-xs-7 col-md-6 no-padding-left font-size-16">
                                                <span class="font-weight-700">Uploaded Image</span> <span class="text-color-grey"></span>
                                            </div>

                                            <div class="col-xs-5 col-md-6 text-right padding-right-30">
                                                <img src="{{asset('images/cat_image_placeholder.jpg')}}" width="90px" height="60px" id="banner_image_mobile" />
                                            </div>

                                            <div class="col-xs-1 no-padding">
                                                <a href="javascript:void(0)" class="cat_file_remove">
                                                    <span class="icon-cancelled text-color-black font-size-16"></span>
                                                </a>
                                            </div>
                                        </div>
                                        {{--Uploaded Image Row--}}
                                        <div class="uploadedImageRow">
                                            <div class="upload__file fullWidth"> {{--col-xs-6 no-padding-left--}}
                                                <input type="button" class="btn btn__holo fullWidth font-weight-600" value="Upload Image" />
                                                <input type="file" name="promo_banner_image_mobile" class="hide" multiple />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- BANNER APP -->
                                <div class="uploadedMediaContainer">
                                    <div class="uploadedImagesContainer">
                                        <div class="subContainerHeader">Promotional Banner Image (App)</div>
                                        <div class="row uploadedImageRow hide">
                                            <div class="col-xs-7 col-md-6 no-padding-left font-size-16">
                                                <span class="font-weight-700">Uploaded Image</span> <span class="text-color-grey"></span>
                                            </div>
                                            <div class="col-xs-5 col-md-6 text-right padding-right-30">
                                                <img src="{{asset('images/cat_image_placeholder.jpg')}}" width="90px" height="60px" id="banner_image_app" />
                                            </div>
                                            <div class="col-xs-1 no-padding">
                                                <a href="javascript:void(0)" class="cat_file_remove">
                                                    <span class="icon-cancelled text-color-black font-size-16"></span>
                                                </a>
                                            </div>
                                        </div>
                                        {{--Uploaded Image Row--}}
                                        <div class="uploadedImageRow">
                                            <div class="upload__file fullWidth"> {{--col-xs-6 no-padding-left--}}
                                                <input type="button" class="btn btn__holo fullWidth font-weight-600" value="Upload Image" />
                                                <input type="file" name="promo_banner_image_app" class="hide" multiple />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="menuItem__footerButton">
                            {{--<div class="btn btn__primary" id="menu_cat_save">Save</div>--}}
                            <input type="submit" class="btn btn__primary" id="menu_cat_save" value="Save" />
                            <div class="btn btn__holo" id="menu_cat_cancel">Cancel</div>
                        </div>
                    </form>

                    </div>

                    {{--Add a New Item--}}
                    <div class="menuItem__newItem hidden">
                        <div class="menuItem__header">
                            <div class="headerTitle">Add a New Item</div>
                            <input type="hidden" id="edit_item" value="0" />
                        </div>

                        {{--Stepper List--}}
                        <div class="menuItem__stepper">

                            <ul class="stepperContainer">
                                {{--Step--}}
                                <li class="step active">
                                    <a href="#general" aria-controls="general" role="tab" data-toggle="tab" class="step__wrapper">
                                        <div class="stepContainer">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <div class="stepText">General</div>
                                    </a>
                                </li>

                                {{--Step--}}
                                {{--<li class="step">
                                    <a href="#schedules" aria-controls="schedules" role="tab" data-toggle="tab"
                                       class="step__wrapper">
                                        <div class="stepContainer">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <div class="stepText">Schedules</div>
                                    </a>
                                </li>--}}

                                {{--Step--}}
                                <li class="step ">
                                    <a href="#modifiers" aria-controls="modifiers" role="tab" data-toggle="tab" class="step__wrapper">
                                        <div class="stepContainer">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <div class="stepText">Modifiers</div>
                                    </a>
                                </li>

                                {{--Step--}}
                                <li class="step ">
                                    <a href="#addons" aria-controls="addons" role="tab" data-toggle="tab" class="step__wrapper">
                                        <div class="stepContainer">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <div class="stepText">Add-ons</div>
                                    </a>
                                </li>

                                {{--Step--}}
                                <li class="step ">
                                    <a href="#labels" aria-controls="labels" role="tab" data-toggle="tab" class="step__wrapper">
                                        <div class="stepContainer">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <div class="stepText">Labels</div>
                                    </a>
                                </li>

                                {{--Step--}}
                                <li class="step ">
                                    <a href="#images" aria-controls="images" role="tab" data-toggle="tab" class="step__wrapper">
                                        <div class="stepContainer">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <div class="stepText">Images</div>
                                    </a>
                                </li>

                            </ul>

                        </div>

                        {{--Stepper Tabs--}}
                        <div class="menuItem__stepperTabs">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="general">
                                    <input type="hidden" name="item_id" value="" id="general_item_id"/>
                                    <input type="hidden" name="restaurant_id" value="{{ $restaurantId }}" id="restaurant_id"/>


                                    {{--<div class="row ">
                                        <div class="font-weight-800 font-size-13">Restaurants</div>
                                        <div class="selct-picker-plain">
                                            <select data-style="no-background-with-buttonline no-padding-left" data-size="8" name="restaurant_id" id="restaurant_id" class="selectpicker form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>
                                                <option value="">Select Restaurant</option>
                                                @foreach($groupRestData as $rest)
                                                    <optgroup label="{{ $rest['restaurant_name'] }}" data-max-options="2">
                                                        @foreach($rest['branches'] as $branch)
                                                            <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>

                                        <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" required>
                                            <option value="">Select Restaurant</option>
                                            @foreach($groupRestData as $rest)
                                                <optgroup label="{{ $rest['restaurant_name'] }}">
                                                    @foreach($rest['branches'] as $branch)
                                                        <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>--}}
                                    <div class="row margin-top-20">
                                        <div class="font-weight-800 font-size-13">Categories</div>
                                        <div class="selct-picker-plain">
                                            <select data-style="no-background-with-buttonline no-padding-left" title="Select Category" data-max-options="2" data-size="8" name="general_cat_sub_cat" class="selectpicker" id="general_cat_sub_cat"></select>
                                        </div>
                                    </div>
                                    <div class="input__field">
                                        <input id="itemName" type="text" name="item_name" autocomplete="off">
                                        <label for="itemName">Name</label>
                                    </div>

                                    <div class="input__field">
                                        <input id="itemDesc" type="text" name="item_desc" autocomplete="off">
                                        <label for="itemDesc">Description</label>
                                    </div>
                                    <div class="row margin-top-20">
                                        <div class="font-weight-800 font-size-13">Product Type</div>
                                        <div class="selct-picker-plain">
                                            <select data-style="no-background-with-buttonline no-padding-left" title="Select Product Type" data-max-options="2" data-size="8" name="item_product_type" class="selectpicker" id="itemProductType">
                                                <option value="">Select Product Type</option>
                                                <option value="food_item">Food Item</option>
                                                <option value="product">Product</option>
                                                <option value="gift_card">Gift Card</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row margin-top-20">
                                        <div class="font-weight-800 font-size-13">Food Type</div>
                                        <div class="selct-picker-plain">
                                            <select data-style="no-background-with-buttonline no-padding-left" title="Select Food Type" data-max-options="2" data-size="8" name="item_food_type" class="selectpicker" id="itemFoodType">
                                                <option value="">Select Food Type</option>
                                                <option value="veg">Veg</option>
                                                <option value="non-veg">Non-Veg</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input__field">
                                        <input id="itemPosId" type="text" name="item_pos_id" autocomplete="off">
                                        <label for="itemPosId">Pos Id</label>
                                    </div>
                                    <div class="input__field">
                                        <input id="itemCalorie" type="text" name="item_calorie" autocomplete="off">
                                        <label for="itemCalorie">Calorie</label>
                                    </div>
                                    <div class="input__field">
                                        <input id="itemDisplayOrder" type="text" name="item_display_order" autocomplete="off">
                                        <label for="itemDisplayOrder">Display Order</label>
                                    </div>
                                    <div class="general__checkList bb">
                                        {{--Check--}}
                                        {{--<div class="general__check">
                                            <input type="checkbox" id="resOnly" class="styled-checkbox">
                                            <label for="resOnly">Restaurant Only</label>
                                        </div>--}}

                                        {{--Check--}}
                                        <div class="general__check">
                                            <input type="checkbox" id="general_delivery" class="styled-checkbox" value="1">
                                            <label for="general_delivery">Delivery</label>
                                        </div>

                                        {{--Check--}}
                                        <div class="general__check">
                                            <input type="checkbox" id="general_takeout" class="styled-checkbox" value="1">
                                            <label for="general_takeout">Take Out</label>
                                        </div>
                                    </div>

                                    {{--<div class="general__checkList">
                                        --}}{{--Check--}}{{--
                                        <div class="general__check">
                                            <input type="checkbox" id="pricing" class="styled-checkbox">
                                            <label for="pricing">Schedule based pricing <span>(add schedule to the item from Schedules tab)</span></label>
                                        </div>
                                    </div>--}}

                                    <div class="general__addSize">

                                        {{--<div class="general__sizeRow">
                                            <div class="general__size">
                                                8"
                                            </div>
                                            <div class="general__sizeContainer">
                                                <div class="general__sizeOption">
                                                    --}}{{--<div class="general__sizeMenu">
                                                        Lunch
                                                    </div>--}}{{--
                                                    <div class="general__sizePrice">
                                                        $9.99
                                                    </div>
                                                    --}}{{--<div class="general__tax">
                                                        <input type="checkbox" id="taxable" class="styled-checkbox">
                                                        <label for="taxable">Take Out</label>
                                                    </div>--}}{{--
                                                </div>
                                                --}}{{--<div class="general__sizeOption">
                                                    <div class="general__sizeMenu">
                                                        Lunch
                                                    </div>
                                                    <div class="general__sizePrice">
                                                        $9.99
                                                    </div>
                                                    <div class="general__tax">
                                                        <input type="checkbox" id="taxable" class="styled-checkbox">
                                                        <label for="taxable">Take Out</label>
                                                    </div>
                                                </div>--}}{{--
                                            </div>
                                            <div class="general__delete">
                                                <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div>
                                        </div>

                                        <div class="general__sizeRow">
                                            <div class="general__size">
                                                24"
                                            </div>
                                            <div class="general__sizeContainer">
                                                <div class="general__sizeOption">
                                                    --}}{{--<div class="general__sizeMenu">
                                                        Lunch
                                                    </div>--}}{{--
                                                    <div class="general__sizePrice">
                                                        $9.99
                                                    </div>
                                                    --}}{{--<div class="general__tax">
                                                        <input type="checkbox" id="taxable" class="styled-checkbox">
                                                        <label for="taxable">Take Out</label>
                                                    </div>--}}{{--
                                                </div>
                                                --}}{{--<div class="general__sizeOption">
                                                    <div class="general__sizeMenu">
                                                        Lunch
                                                    </div>
                                                    <div class="general__sizePrice">
                                                        $9.99
                                                    </div>
                                                    <div class="general__tax">
                                                        <input type="checkbox" id="taxable" class="styled-checkbox">
                                                        <label for="taxable">Take Out</label>
                                                    </div>
                                                </div>--}}{{--
                                            </div>
                                            <div class="general__delete">
                                                <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div>
                                        </div>--}}

                                        <div id="general_size_price_wrap"></div>

                                        <div class="general__footerButton">
                                            <div class="btn btn__holo" id="general_add_size_btn">Add Size</div>
                                        </div>
                                    </div>

                                    {{--<div class="general__tagContainer">
                                        <div class="general__tagHead">Tags</div>
                                        <div class="general__tags">

                                            <div class="general__tag">Nice <sup> <i class="fa fa-times"
                                                                                    aria-hidden="true"></i></sup></div>
                                            <div class="general__tag">Super <sup><i class="fa fa-times"
                                                                                    aria-hidden="true"></i></sup></div>

                                        </div>

                                    </div>--}}


                                </div>

                                {{--<div role="tabpanel" class="tab-pane" id="schedules">
                                    <div class="schedule__wrapper">
                                        <div class="schedule__header">
                                            Schedules in which the Menu item will appear
                                        </div>

                                        <div class="schedule__container">

                                            <div class="schedule__row">
                                                <div class="schedule__name">Lunch</div>
                                                <div class="schedule__radio">
                                                    <!-- Rounded switch -->
                                                    <label class="switch">
                                                        <input type="checkbox">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="schedule__row">
                                                <div class="schedule__name">Dinner</div>
                                                <div class="schedule__radio">
                                                    <!-- Rounded switch -->
                                                    <label class="switch">
                                                        <input type="checkbox">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="general__footerButton">
                                                <div class="btn btn__holo">Add a New Schedule</div>
                                            </div>

                                        </div>


                                    </div>
                                </div>--}}

                                <div role="tabpanel" class="tab-pane " id="modifiers">
                                    <div class="modifier__wrapper">
                                        <div class="modifier__rowContainer">
                                            <div class="modifier__row">
                                                <div class="row">
                                                    <div class="selct-picker-plain">
                                                        <select data-style="no-background-with-buttonline no-padding-left font-weight-700" title="Select Modifier Group" data-size="8" name="modifier_group_dropdown" id="modifier_group_dropdown" class="selectpicker form-control{{ $errors->has('modifier_group_dropdown') ? ' is-invalid' : '' }}" required>
                                                            @foreach($itemModifierData as $mgroup)
                                                                <option value="{{ $mgroup['id'] }}">{{ $mgroup['group_name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="input__field">
                                                    {{--<select name="modifier_group_dropdown" id="modifier_group_dropdown" class="form-control{{ $errors->has('modifier_group_dropdown') ? ' is-invalid' : '' }}" required>
                                                        <option value="">Select Modifier Group</option>
                                                        @foreach($itemModifierData as $mgroup)
                                                            <option value="{{ $mgroup['id'] }}">{{ $mgroup['group_name'] }}</option>
                                                        @endforeach
                                                    </select>--}}
                                                    <div class="modifier__footerButton" id="modifier_group_add_btn"><div class="btn btn__holo">Add</div></div>
                                                </div>

                                                <div id="modifier_details"></div>
                                            </div>
                                        </div>

                                        <div class="modifier__footerButton ">
                                            {{--<div data-toggle="modal" data-target="#modifierPopup" class="btn btn__holo">--}}
                                            <div data-toggle="modal" data-target="" class="btn btn__holo" id="modifier_group_blank_add_btn">Add New Modifier Group</div>
                                        </div>

                                    </div>


                                </div>

                                <div role="tabpanel" class="tab-pane " id="addons">
                                    <div class="modifier__wrapper">
                                        <div class="modifier__rowContainer">
                                            <div class="modifier__row">

                                                <div class="row">
                                                    <div class="selct-picker-plain">
                                                        <select data-style="no-background-with-buttonline no-padding-left font-weight-700" title="Select Addon Group" data-size="8" name="addon_group_dropdown" id="addon_group_dropdown" class="selectpicker form-control{{ $errors->has('modifier_group_dropdown') ? ' is-invalid' : '' }}" required>
                                                            @foreach($itemAddonData as $adgroup)
                                                                <option value="{{ $adgroup['id'] }}">{{ $adgroup['group_name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="input__field">

                                                    {{--<select name="addon_group_dropdown" id="addon_group_dropdown" class="form-control{{ $errors->has('modifier_group_dropdown') ? ' is-invalid' : '' }}" required>
                                                        <option value="">Select Addon Group</option>
                                                        @foreach($itemAddonData as $adgroup)
                                                            <option value="{{ $adgroup['id'] }}">{{ $adgroup['group_name'] }}</option>
                                                        @endforeach
                                                    </select>--}}
                                                    <div class="modifier__footerButton" id="addon_group_add_btn"><div class="btn btn__holo">Add</div></div>
                                                </div>

                                                <div id="addon_details"></div>

                                            </div>
                                        </div>

                                        <div class="modifier__footerButton ">
                                            {{--<div class="btn btn__holo">Add New Add-On Group</div>--}}
                                            <div data-toggle="modal" data-target="" class="btn btn__holo" id="addon_group_blank_add_btn">Add New Add-On Group</div>
                                        </div>

                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane " id="labels">

                                    <div class="label__wrapper">

                                        <div class="label__container">
                                            @foreach($allLabelsData as $key => $labelData)
                                                @php
                                                    $labelCat = 'Others';
                                                    if($key == 'dietary') {
                                                        $labelCat = 'Dietary Specification';
                                                    } elseif($key == 'food_type') {
                                                        $labelCat = 'Food Type';
                                                    }
                                                @endphp
                                                <div class="label__heading">{{ $labelCat }}</div>
                                                <div class="label__switchContainer">
                                                @foreach($labelData as $label)
                                                    @if($label['name'] == 'Serving Size')
                                                        <div class="label__serves">
                                                            <div class="label__switchHead">{{ $label['name'] }}</div>
                                                            <div class="label__slider">
                                                                <input value="1" id="serving_size_min" type="number" max="10">
                                                                <span>to</span>
                                                                <input value="1" id="serving_size_max" type="number" max="10">
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="label__switch">
                                                            <div class="label__switchHead">{{ $label['name'] }}</div>
                                                            <div class="label__switchToggle padding-right-25">
                                                                <div class="toggle__container">
                                                                    <button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom active" data-toggle="button" aria-pressed="true" autocomplete="off">
                                                                        <div class="handle"></div>
                                                                    </button>
                                                                    <input type="hidden" name="{{ $label['id'] }}" class="label_field" value="1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane " id="images">

                                    <form id="uploadWidget" method="post" action="{{ url('save_menu_image') }}" class="dropzone">
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <div class="fallback">
                                            <input name="file" type="file" />
                                        </div>
                                        {{--<div class="dz-default dz-message">
                                            <div class="image__text">Drag and drop a file to upload</div>
                                            <div class="image__or">or</div>
                                            <div class="image__btnContainer">
                                                <div class="btn btn__holo">Choose a File</div>
                                            </div>
                                        </div>--}}
                                        <input type="hidden" name="image_item_id" value="123">
                                    </form>

                                    {{--<form action="{{ url('/menu/save_menu_image') }}" id="myId"></form>--}}

                                    {{--<form action="/upload" method="POST" class="dropzone" id="myId">
                                        <div class="fallback">
                                            <input name="file" type="file" multiple />
                                            <input type="submit" value="Upload" />
                                        </div>
                                    </form>--}}

                                    <div class="image__container">
                                        <div class="image__uploadBox">

                                            <div class="image__uploadedImageContainer">
                                                <img src="{{ asset('images/dummy-image.png') }} " alt=" uploaded">
                                            </div>

                                            <div class="image__uploadedPlus">
                                                +
                                            </div>

                                            <div class="image__dragToUpload">
                                                <div class="image__text">Drag and drop a file to upload</div>
                                                {{--<div class="image__or">or</div>
                                                <div class="upload__file fullWidth"> --}}{{--col-xs-6 no-padding-left--}}{{--
                                                    <input type="button" class="btn btn__holo fullWidth font-weight-600" value="Upload Image" />
                                                    <input type="file" name="menu_item_image" id="menu_item_image" class="hide" multiple />
                                                </div>--}}
                                            </div>


                                        </div>
                                        <div class="image__delete">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="menuItem__footerButton">
                            <div class="btn btn__primary stepper__next" id="save_continue">Save & Continue</div>
                            <div class="btn btn__holo stepper__prev" id="back_modify">Back & Modify</div>
                        </div>

                    </div>


                </div>
            </div>

        </div>


    </div>


    {{--Modifier Popup--}}

    <div class="modal fade" id="modifierPopup" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="popup-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="menuItem__stepperTabs">
                        <div class="modifier__wrapper">
                            <div class="modifier__rowContainer">
                                <div class="modifier__row">

                                    <div class="modifier__newModifier">

                                        <div class="input__field">
                                            <input id="catName" type="text" name="name">
                                            <label for="catName">Modifier Name</label>
                                        </div>

                                        <div class="input__field">
                                            <input id="price" type="text" name="name">
                                            <label for="price">Price</label>
                                        </div>

                                        <div class="modifier__modalOption">
                                            <div class="quantityLabel">
                                                Options
                                            </div>

                                            <div class="modifier__modalOptionContainer">
                                                <div class="modifier__rowHeadContainer">
                                                    <div class="modifier__rowContainerModal">
                                                        <div class="modifier__rowHead">
                                                            <div class="modifier__switch">
                                                                <label class="switch">
                                                                    <input type="checkbox">
                                                                    <span class="slider round"></span>
                                                                </label>
                                                                <div class="modifier__name">
                                                                    Quantity
                                                                </div>
                                                            </div>
                                                            <div class="modifier__edit">
                                                                <div class="modify-icon">
                                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                                </div>

                                                                <div class="delete-icon">
                                                                    <i class="fa fa-times"
                                                                       aria-hidden="true"></i>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="toggleOptions">
                                                            <div class="modifier__quantity">

                                                                <div class="quantityLabel">
                                                                    Quantity <span>(Range)</span>
                                                                </div>

                                                                <div class="quantityFields">

                                                                    <div class="quantityField">
                                                                        <span>Min</span> <input value="1" type="number">
                                                                    </div>
                                                                    <div class="quantityField">
                                                                        <span>Max</span> <input value="3" type="number">
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="modifier__requiredFields">
                                                                <div class="requiredHeader">
                                                                    Is this required?
                                                                </div>

                                                                <div class="requiredRadio">
                                                                    <input type="radio" id="" name="required" value="1"
                                                                           checked>
                                                                    Required
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <input type="radio" id="" name="required" value="0">
                                                                    Value
                                                                </div>

                                                            </div>

                                                            <div class="modifier__options">

                                                                <div class="optionRowContainer">
                                                                    <div class="optionRow">
                                                                        <div class="option optionCategory">
                                                                            Left
                                                                        </div>


                                                                        <div class="option optionPrice">
                                                                            {{ $curSymbol }}<input type="text" value="1.00">
                                                                        </div>

                                                                    </div>

                                                                    <div class="optionFunctions ">
                                                                        <div class="modify-icon">
                                                                            <i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>
                                                                        </div>

                                                                        <div class="delete-icon">
                                                                            <i class="fa fa-times"
                                                                               aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>


                                                                </div>

                                                                <div class="optionRowContainer">
                                                                    <div class="optionRow">
                                                                        <div class="option optionCategory">
                                                                            Right
                                                                        </div>

                                                                        <div class="option optionPrice">
                                                                            {{ $curSymbol }}<input type="text" value="1.00">
                                                                        </div>

                                                                    </div>

                                                                    <div class="optionFunctions ">
                                                                        <div class="modify-icon">
                                                                            <i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>
                                                                        </div>

                                                                        <div class="delete-icon">
                                                                            <i class="fa fa-times"
                                                                               aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>


                                                                </div>

                                                                <div class="optionRowContainer">
                                                                    <div class="optionRow">
                                                                        <div class="option optionCategory">
                                                                            Whole
                                                                        </div>

                                                                        <div class="option optionPrice">
                                                                            {{ $curSymbol }}<input type="text" value="1.00">
                                                                        </div>

                                                                    </div>

                                                                    <div class="optionFunctions ">
                                                                        <div class="modify-icon">
                                                                            <i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>
                                                                        </div>

                                                                        <div class="delete-icon">
                                                                            <i class="fa fa-times"
                                                                               aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                            </div>

                                                        </div>


                                                    </div>
                                                    <div class="modifier__rowContainerModal">
                                                        <div class="modifier__rowHead">
                                                            <div class="modifier__switch">
                                                                <label class="switch">
                                                                    <input type="checkbox">
                                                                    <span class="slider round"></span>
                                                                </label>
                                                                <div class="modifier__name">
                                                                    Sides
                                                                </div>
                                                            </div>
                                                            <div class="modifier__edit">
                                                                <div class="modify-icon">
                                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                                </div>

                                                                <div class="delete-icon">
                                                                    <i class="fa fa-times"
                                                                       aria-hidden="true"></i>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="toggleOptions">
                                                            <div class="modifier__quantity">

                                                                <div class="quantityLabel">
                                                                    Quantity <span>(Range)</span>
                                                                </div>

                                                                <div class="quantityFields">

                                                                    <div class="quantityField">
                                                                        <span>Min</span> <input value="1" type="number">
                                                                    </div>
                                                                    <div class="quantityField">
                                                                        <span>Max</span> <input value="3" type="number">
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="modifier__requiredFields">
                                                                <div class="requiredHeader">
                                                                    Is this required?
                                                                </div>

                                                                <div class="requiredRadio">
                                                                    <input type="radio" id="" name="required" value="1"
                                                                           checked>
                                                                    Required
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <input type="radio" id="" name="required" value="0">
                                                                    Value
                                                                </div>

                                                            </div>

                                                            <div class="modifier__options">

                                                                <div class="optionRowContainer">
                                                                    <div class="optionRow">
                                                                        <div class="option optionCategory">
                                                                            Left
                                                                        </div>


                                                                        <div class="option optionPrice">
                                                                            {{ $curSymbol }}<input type="text" value="1.00">
                                                                        </div>

                                                                    </div>

                                                                    <div class="optionFunctions ">
                                                                        <div class="modify-icon">
                                                                            <i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>
                                                                        </div>

                                                                        <div class="delete-icon">
                                                                            <i class="fa fa-times"
                                                                               aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>


                                                                </div>

                                                                <div class="optionRowContainer">
                                                                    <div class="optionRow">
                                                                        <div class="option optionCategory">
                                                                            Right
                                                                        </div>

                                                                        <div class="option optionPrice">
                                                                            {{ $curSymbol }}<input type="text" value="1.00">
                                                                        </div>

                                                                    </div>

                                                                    <div class="optionFunctions ">
                                                                        <div class="modify-icon">
                                                                            <i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>
                                                                        </div>

                                                                        <div class="delete-icon">
                                                                            <i class="fa fa-times"
                                                                               aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>


                                                                </div>

                                                                <div class="optionRowContainer">
                                                                    <div class="optionRow">
                                                                        <div class="option optionCategory">
                                                                            Whole
                                                                        </div>

                                                                        <div class="option optionPrice">
                                                                            {{ $curSymbol }}<input type="text" value="1.00">
                                                                        </div>

                                                                    </div>

                                                                    <div class="optionFunctions ">
                                                                        <div class="modify-icon">
                                                                            <i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>
                                                                        </div>

                                                                        <div class="delete-icon">
                                                                            <i class="fa fa-times"
                                                                               aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>

                            <div class="modifier__footerButton ">
                                <div class="btn btn__holo">Add New Add-On Group</div>
                            </div>

                        </div>
                    </div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal fade modal-popup" id="selectMenuCategoryIcons" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Select The Category Icon</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 margin-top-10">
                            <h3>Icons</h3>
                            <ul class="icons-setlist margin-top-20">
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="appetizers-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="appetizers-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/appetizers.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="beverages-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="beverages-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/beverages.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="business-meal-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="business-meal-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/business-meal.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="drinks-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="drinks-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/drinks.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="frozen-pizza-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="frozen-pizza-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/frozen-pizza.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="frozen-pizza-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="frozen-pizza-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/frozen-pizza.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 margin-top-30">
                            <h3>Uploaded Icons</h3>
                            <ul class="icons-setlist margin-top-20">
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="appetizers-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="appetizers-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/appetizers.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="beverages-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="beverages-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/beverages.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="business-meal-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="business-meal-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/business-meal.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="drinks-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="drinks-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/drinks.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                                <li class="flex-box flex-align-item-center">
                                    <input type="radio" id="frozen-pizza-icon" name="menuCategoriesIcon" class="hide"  />
                                    <label for="frozen-pizza-icon" class="flex-box flex-align-item-center flex-justify-center">
                                        <img src="{{ asset('images/svg-icons/frozen-pizza.svg') }}" onload="SVGInject(this)" width="35" />
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="additem" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Addon Item</h4>
                </div>
                <div class="modal-body" style="max-height:400px; min-height: 200px; overflow-y: scroll;">
                    <form method="POST" id="additemform" action="https://cms-qc-main.munchadoshowcase.biz/menu/item-addons" enctype="multipart/form-data">
                        loading...
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="saveAddonItemsToGroupBtn">Add</button>
                </div>
            </div>

        </div>
    </div>


    <link href="{{ asset('css/dropzone.css') }}" type="text/css" rel="stylesheet"/>

    {{--<script src="{{ asset('js/semantic.min.js') }}"></script>--}}
    <script src="{{ asset('js/jquery-sortable.js') }}"></script>
    <script src="{{ asset('js/dropzone.js') }}"></script>

    <script src="{{ asset('/js/addon_group.js') }}"></script>

    <script type="text/javascript">
        var cur_sym = '{{ $curSymbol }}';

        Dropzone.options.uploadWidget = {
            url: "save_menu_image",
            paramName: 'menu_item_image',
            addRemoveLinks : true,
            maxFilesize: 5, // MB
            maxFiles: 5,
            dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Drop files <span class="font-xs">to upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Or Click)</h4></span>',
            dictResponseError: 'Error uploading file!',
            headers: {
                'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
            },
            acceptedFiles: 'image/*',
            init: function() {
                this.on('success', function( file, resp ){
                    console.log( file );
                    console.log( resp );
                    var cat_id = parseInt($('#general_cat_sub_cat').val());
                    console.log('cat_id ' + cat_id);
                    reset_menu_item();
                    $('#sidebar-category-list .fetch-menu-items').each(function() {
                        var cat = parseInt($(this).data('cat'));
                        var scat = parseInt($(this).data('sub-cat'));
                        console.log('cat ' + cat + ' scat ' + scat);
                        if(cat == cat_id || scat == cat_id) {
                            console.log('inside');
                            $(this).trigger('click');
                        } else {
                            console.log('cat_id ' + cat_id + 'cat ' + cat + ' scat ' + scat);
                        }
                    });
                });
                this.on('thumbnail', function(file) {
                    var general_item_id = $('#general_item_id').val();
                    console.log('init - '+general_item_id);
                    $('#image_item_id').val(general_item_id);
                    if ( file.width < 320 || file.height < 320 ) {
                        file.rejectDimensions();
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() {
                    done('The image must be at least 320 x 320px')
                };
            }
        };

        var cat_sub_cat = '';
        var item_modifier_data  = JSON.parse('<?php echo json_encode($itemModifierData); ?>');
        var item_addon_data     = JSON.parse('<?php echo json_encode($itemAddonData); ?>');
        var modifier_categories = JSON.parse('<?php echo json_encode($modifierCategories); ?>');
        function js_ucfirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        function js_ucwords(string) {
            return string.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
        }
        function get_modifier_categories() {
            $.ajax({
                type: 'GET',
                url: '/menu/all_cat_sub_cat',
                success: function (data) {
                    window.modifier_categories = data;
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                    });
                },
                complete: function(data) {

                }
            });
        }
        function generate_sub_cat_recursive(catData) {
            var subCatHtml = '';
            catData.children_recursive.forEach(function(subcat) {
                subCatHtml += '<ul class="accordion-subCategory-new">'
                    +'<li class="accordion-subcategoryItem-new">'
                    +'<i class="icon-hdd"></i><span class="node-cpe fetch-menu-items" data-cat="'+subcat.parent +'" data-sub-cat="'+ subcat.id +'">'+ js_ucwords(subcat.name) +'</span>'
                    +'<div class="edit-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>';
                if(subcat.children_recursive.length){
                    subCatHtml += generate_sub_cat_recursive(subcat);
                }
                subCatHtml += '</li>'
                    +'</ul>';
            });
            return subCatHtml;
        }
        function generate_sub_cat_dropdown_recursive(catData, cat_id, level) {
            var subCatHtml = '';
            var prefix = "|-- ".repeat(level);
            catData.children_recursive.forEach(function(subcat) {
                var sel_atr = '';
                if(subcat.id == cat_id) {
                    sel_atr = 'selected';
                }
                subCatHtml += '<option value="'+subcat.id+'" '+sel_atr+'>'+prefix+js_ucwords(subcat.name)+'</option>';
                if(subcat.children_recursive.length){
                    level += 1;
                    subCatHtml += generate_sub_cat_dropdown_recursive(subcat, cat_id, level);
                }
            });
            return subCatHtml;
            /*var html = '';
            for(var subkey in cat.children_recursive) {
                var subcat = cat.children_recursive[subkey];
                var sel_atr = '';
                if(subcat.id == cat_id) {
                    sel_atr = 'selected';
                }
                html += '<option value="'+subcat.id+'" '+sel_atr+'>|-- '+js_ucwords(subcat.name)+'</option>';
                for(var subkey2 in subcat.children_recursive) {
                    var subcat2 = subcat.children_recursive[subkey2];
                    html += generate_sub_cat_dropdown_recursive(subcat2);
                }
            }
            return html;*/
        }
        function get_all_cat_sub_cat() {
            $.ajax({
                type: 'GET',
                url: '/menu/all_cat_sub_cat',
                success: function (data) {
                    window.cat_sub_cat = data;
                    var html = '';
                    data.forEach(function(cat) {
                        //console.log(cat);
                        /*html_subcat = '';
                        cat.menu_sub_category.forEach(function(sub_cat) {
                            html_subcat += '<ul class="accordion-subCategory">'
                                +'<li class="accordion-subcategoryItem">'
                                +'<div class="catergoryName">'
                                +'<div class="tabTrigger fetch-menu-items" data-cat="'+cat.id+'" data-sub-cat="'+sub_cat.id+'">'+ js_ucwords(sub_cat.name)+'</div>'
                                +'<div class="edit-icon">'
                                +'<i class="fa fa-pencil" aria-hidden="true"></i>'
                                +'</div>'
                                +'</div>'
                                +'</li>'
                                +'</ul>';
                        });*/
                        html += '<li class="accordion-category">'
                            +'<span class="node-facility fetch-menu-items" data-cat="'+cat.id+'">'+ js_ucwords(cat.name) +'</span>'
                            +'<div class="edit-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                            +generate_sub_cat_recursive(cat)
                            +'</li>';
                    });
                    $('#sidebar-category-list').html(html);
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                    });
                },
                complete: function(data) {

                }
            });
        }
        function get_modifier_groups() {
            $.ajax({
                type: 'GET',
                url: '/menu/get_modifier_groups',
                success: function (data) {
                    console.log(data);
                },
                error: function (data, textStatus, errorThrown) {

                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                    });
                },
                complete: function(data) {
                    console.log('complete');
                    console.log(data);
                }
            });
        }
        function generate_menu_html(data) {
            var html = '<ol class="menuItem__allContainer">';
            data.forEach(function(menuitem) {
                var status_class = 'icon-eye-close';
                var cust = '';
                if(menuitem.status) {
                    status_class = 'icon-eye-open';
                }
                if(menuitem.modifier_exists) {
                    cust = '<div class="productCustom">Customizable</div>';
                }
                html += '<li class="menuItem__product">'
                    +'<div class="menuItem__productDesc">'
                    +'<div class="menuItem__productImage"><img src="../'+menuitem.images+'" alt="product image"></div>'
                    +'<div class="menuItem__productDetails">'
                    +'<div class="productName"><a href="javascript:load_menu_edit('+menuitem.id+',\'general\')">'+menuitem.name+'</a></div>'
                    +'<div class="productAddon">'+menuitem.description+'</div>'
                    +cust
                    +'</div>'
                    +'</div>'

                    +'<div class="menuItem__productAll">'
                    +'<div class="menuItem__productStatuses">'
                    +'<div class="menuItem__productStatus">Restaurant Only</div>'
                    +'</div>'

                    +'<div class="menuItem__productPrice">'
                    +cur_sym+' <span>'+menuitem.price+'</span>'
                    +'</div>'
                    +'<div class="menuItem__productVisibility">'
                    +'<i class="fa '+status_class+' productVisibilityTrigger" aria-hidden="true"></i>'
                    +'</div>'
                    +'</div>'
                    +'</li>';
            });
            html += '</ol>';
            return html;
        }

        function qtyRangeValue(quantity,type){

            var quantity_html = '';

            if(type == "Range") {
                quantity_html += '<div class="selct-picker-plain">'
                    +'<select data-style="no-background no-padding-left" data-minValue="'+quantity[0]+'" data-maxValue="'+quantity[1]+'" data-show-subtext="true" data-size="5" name="quantity_type" id="quantity_type" class="selectpicker qtyRangeValueType">'
                    +'<option data-subtext="(Range)" selected value="Range">Quantity</option>'
                    +'<option data-subtext="(Value)" value="Value">Quantity</option>'
                    +'</select>'
                    +'</div>'
                    +'<div class="quantityFields">'
                    +'<div class="quantityField"><span>Min</span><input value="'+quantity[0]+'" class="modifier_qnt" type="number"></div>'
                    +'<div class="quantityField"><span>Max</span><input value="'+quantity[1]+'" class="modifier_qnt" type="number"></div>'
                    +"</div>";
            } else {
                quantity_html += '<div class="selct-picker-plain">'
                    +'<select data-style="no-background no-padding-left" data-minValue="'+quantity[0]+'" data-maxValue="'+quantity[1]+'" data-show-subtext="true" data-size="5" name="quantity_type" id="quantity_type" class="selectpicker qtyRangeValueType">'
                    +'<option data-subtext="(Range)" value="Range">Quantity</option>'
                    +'<option data-subtext="(Value)" selected value="Value">Quantity</option>'
                    +'</select>'
                    +'</div>'
                    +'<div class="quantityFields">'
                    +'<div class="quantityField"><span></span><input value="'+quantity[0]+'" class="modifier_qnt" type="number"></div>'
                    +'</div>';
            }

            return quantity_html;
        }

        var mcID = 0
        function generate_modifier_group_html(data) {
            if(data == '') {
                var quantity = [""];
                var quantity_type = 'Value';
                var group_name = '';
                var prompt = '';
                var pos_id = '';
                var calorie = '';
                var required_html = optional_html = '';

                var modifier_html = '<div id="modifierCategory'+ mcID +'" data-mc-row="'+ mcID +'" class="optionRowContainer">'
                                        +'<div class="optionRow">'
                                            +'<div class="option optionCategory">'
                                            +'<div class="selct-picker-plain fullWidth">'
                                            +'<select data-style="no-background-with-buttonline no-padding-left" class="modifier_cat_dropdown selectpicker">'
                                            +'<option>Select or Add New</option>'
                                            //+'<option>Mustard</option>'
                                            //+'<option>Ketchup</option>'
                                            //+'<option>Relish</option>'
                                            //+'<option data-divider="true"></option>'
                                            //+'<option value="addMore">Add New</option>'
                                            +'</select>'
                                            +'</div>'
                                            +'</div>'
                                            +'<input type="text" class="hide" />'
                                            +'<div class="option optionName"><input type="text" value=""></div>'
                                            +'<div class="option optionSize"><input type="text" value=""></div>'
                                            +'<div class="option optionPrice">'+cur_sym+'<input type="text" value=""></div>'
                                            +'<div class="option optionPosId"><input type="text" value=""></div>'
                                            +'<div class="option optionCalorie"><input type="text" value=""></div>'
                                        +'</div>'
                                        +'<div class="optionFunctions ">'
                                            //+'<div class="modify-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                                            +'<div class="delete-icon"><i class="fa fa-times" aria-hidden="true"></i></div>'
                                        +'</div>'
                                    +'</div>';
                mcID++
            } else {
                var quantity = data.quantity.split(',');
                var quantity_type = data.quantity_type;
                var group_name = data.group_name;
                var prompt = data.prompt;
                var pos_id = data.pos_id;
                var calorie = data.calorie;
                var required_html   = optional_html = '';
                if(data.required == 1) {
                    required_html = 'checked="true"';
                } else {
                    optional_html = 'checked="true"';
                }
                var modifier_html = '';
                console.log(data.mod_categories);
                console.log(data.mod_items);
                data.mod_items.forEach(function(item) {
                    var modifier_cat_html = '<select class="modifier_cat_dropdown"><option>Select or Add New</option>';
                    modifier_categories.forEach(function(cat) {
                        var selected = '';
                        if(cat.id == item.category_id) {
                            selected = 'selected="selected"';
                        }
                        modifier_cat_html += '<option value="'+cat.id+'" '+selected+'>'+cat.category_name+'</option>';
                    });
                    modifier_cat_html += '</select>';
                    modifier_html += '<div class="optionRowContainer">'
                                        +'<div class="optionRow">'
                                            +'<div class="option optionCategory">'
                                                +modifier_cat_html
                                            +'</div>'
                                            +'<div class="option optionName"><input type="text" value="'+item.modifier_name+'"></div>'
                                            +'<div class="option optionSize"><input type="text" value="'+item.modifier_size+'"></div>'
                                            +'<div class="option optionPrice">'+cur_sym+'<input type="text" value="'+item.modifier_price+'"></div>'
                                            +'<div class="option optionPosId"><input type="text" value="'+item.modifier_pos_id+'"></div>'
                                            +'<div class="option optionCalorie"><input type="text" value="'+item.modifier_calorie+'"></div>'
                                        +'</div>'
                                        +'<div class="optionFunctions ">'
                                        //+'<div class="modify-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                                        +'<div class="delete-icon"><i class="fa fa-times" aria-hidden="true"></i></div>'
                                        +'</div>'
                                    +'</div>';
                });
            }
            var modifier_count = $('#modifier_details .modifier__rowHeadContainer').length + 1;
            console.log('modifier_count '+modifier_count);

            /*var quantity_html = '';
            if(quantity.length > 1) {
                quantity_html += '<div class="quantityField"><span>Min</span><input value="'+quantity[0]+'" class="modifier_qnt" type="number"></div>'
                                +'<div class="quantityField"><span>Max</span><input value="'+quantity[1]+'" class="modifier_qnt" type="number"></div>';
            } else {
                quantity_html += '<div class="quantityField"><span></span><input value="'+quantity[0]+'" class="modifier_qnt" type="number"></div>';
            }*/

            var html = '';
            html += '<div class="modifier__rowHeadContainer" id="modifier__rowHeadContainer_'+ modifier_count +'">'
                        +'<div class="modifier__rowHead">'
                            +'<div class="modifier__switch">'
                                //+'<label class="switch"><input type="checkbox"><span class="slider round"></span></label>'
                                +'<div class="modifier__name"><input type="text" class="modifier_name" value="'+group_name+'"></div>'
                            +'</div>'
                            +'<div class="modifier__edit">'
                                +'<div class="modify-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                                +'<div class="delete-icon"><i class="fa fa-times modifier_group_remove_btn" aria-hidden="true"></i></div>'
                            +'</div>'
                        +'</div>'
                        /*+'<div class="modifier__applyMethod">'
                            +'<input type="radio" id="" name="status" value="1" checked>Apply to all Items &nbsp;&nbsp;&nbsp;&nbsp;'
                            +'<input type="radio" id="" name="status" value="0">Apply to this item only</div>'
                        +'</div>'*/

                        +'<div class="modifier__newModifier">'
                            +'<div class="input__field">'
                            +'<input class="mod_prompt" type="text" name="mod_prompt" value="'+prompt+'">'
                            +'<label for="mod_prompt">Modifier Group Name</label>'
                        +'</div>'
                        +'<div class="modifier__newModifier">'
                            +'<div class="input__field">'
                            +'<input class="mod_pos_id" type="text" name="mod_pos_id" value="'+pos_id+'">'
                            +'<label for="mod_pos_id">POS Id</label>'
                        +'</div>'
                        +'<div class="modifier__newModifier">'
                            +'<div class="input__field">'
                            +'<input class="mod_calorie" type="text" name="mod_calorie" value="'+calorie+'">'
                            +'<label for="mod_calorie">Calorie</label>'
                        +'</div>'
                        +'<div class="modifier__quantity">'
                                /*+'<div class="selct-picker-plain">'
                                +'<select data-style="no-background no-padding-left" data-show-subtext="true" data-size="5" name="quantity_type" id="quantity_type" class="selectpicker">'
                                +'<option data-subtext="(Range)" value="Range">Quantity</option>'
                                +'<option data-subtext="(Value)" value="Value">Quantity</option>'
                                +'</select>'
                                +'</div>'
                            //+'<div class="quantityLabel">Quantity <span>('+quantity_type+')</span></div>'
                                +'<div class="quantityFields">'+qtyRangeValue(quantity,quantity_type)+'</div>'*/
                            +qtyRangeValue(quantity,quantity_type)
                            +'</div>'
                            +'<div class="modifier__requiredFields">'
                                +'<div class="requiredHeader">Is this required?</div>'
                                +'<div class="requiredRadio">'
                                +'<div class="tbl-radio-btn margin-right-20">'
                                +'<input type="radio" class="modifier_req" id="id_mod_required'+modifier_count+'" name="modifier_required'+modifier_count+'" value="1" '+required_html+'>'
                                +'<label for="id_mod_required'+modifier_count+'">Required</label>'
                                +'</div>'
                                +'<div class="tbl-radio-btn">'
                                +'<input type="radio" class="modifier_req" id="id_mod_optional'+modifier_count+'" name="modifier_required'+modifier_count+'" value="0" '+optional_html+'>'
                                +'<label for="id_mod_optional'+modifier_count+'">Optional</label>'
                                +'</div>'
                                    //+'<input type="radio" class="modifier_req" name="modifier_required'+modifier_count+'" value="1" checked>Required &nbsp;&nbsp;&nbsp;&nbsp;'
                                    //+'<input type="radio" class="modifier_req" name="modifier_required'+modifier_count+'" value="0"> Optional'
                                +'</div>'
                            +'</div>'
                            +'<div class="modifier__options">'
                                +'<div class="optionRowHeader">'
                                    +'<div class="optionHeadings">'
                                        +'<div class="optionHeading">Modifier Category</div>'
                                        +'<div class="optionHeading">Modifier Name</div>'
                                        +'<div class="optionHeading">Size</div>'
                                        +'<div class="optionHeading">Price</div>'
                                        +'<div class="optionHeading">Pos Id</div>'
                                        +'<div class="optionHeading">Calorie</div>'
                                    +'</div>'
                                    +'<div class="optionSpacer optionFunctions"></div>'
                                +'</div>'
                                +modifier_html
                            +'</div>'
                            +'<div class="row">'
                            +'<div class="fullWidth padding-bottom-20 margin-left-10"><a href="javascript:void(0)" onclick="addNewCategoryRow(this)" class="font-weight-700">+ Add New Category</a></div>'
                            +'</div>'
                        +'</div>'
                    +'</div>';

                    setTimeout(function(){
                        $('#modifier_details .modifier__options .selectpicker').append(divider).append(addoption).selectpicker();
                        $(".selectpicker").selectpicker('refresh');
                    },100)

            return html;
        }

        function addNewCategoryRow(ths){
            var modifier_html = '<div id="modifierCategory'+ mcID +'" data-mc-row="'+ mcID +'" class="optionRowContainer">'
                +'<div class="optionRow">'
                +'<div class="option optionCategory">'
                +'<div class="selct-picker-plain fullWidth">'
                +'<select data-style="no-background-with-buttonline no-padding-left" class="modifier_cat_dropdown selectpicker">'
                +'<option>Select or Add New</option>'
                //+'<option data-divider="true"></option>'
                //+'<option value="addMore">Add New</option>'
                +'</select>'
                +'</div>'
                +'</div>'
                +'<input type="text" class="hide" />'
                +'<div class="option optionName"><input type="text" value=""></div>'
                +'<div class="option optionPrice">'+cur_sym+'<input type="text" value=""></div>'
                +'</div>'
                +'<div class="optionFunctions ">'
                //+'<div class="modify-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                +'<div class="delete-icon"><a href="javascript:removeModifierCategoryRow('+mcID+')"><i class="fa fa-times" aria-hidden="true"></i></a></div>'
                +'</div>'
                +'</div>';
            mcID++;

            $(ths).parents('.modifier__newModifier').find(".modifier__options").append(modifier_html);

            setTimeout(function(){
                $('#modifier_details .modifier__options .selectpicker').append(divider).append(addoption).selectpicker();
                $(".selectpicker").selectpicker('refresh');
            },10)
        }

        window.removeModifierCategoryRow = function(id){
            $("#modifierCategory"+ id).remove();
        }

        $(document).on('change', 'select.qtyRangeValueType', function () {

            var min = ''
            var max = ''

            if(($(this).attr("data-minValue") != "") || ($(this).attr("data-minValue") != null)){
                min = $(this).attr("data-minValue")
                max = $(this).attr("data-maxValue")
            }

            if($(this).val() == "Range"){
                $(this).parents('.modifier__quantity').empty().append(qtyRangeValue([min,max],"Range"))
            } else if ($(this).val() == "Value"){
                $(this).parents('.modifier__quantity').empty().append(qtyRangeValue([min,max],"Value"))
            }

            $(".selectpicker").selectpicker("refresh")

        });

        var content = "<input type='text' class='bss-input' onKeyDown='event.stopPropagation();' onKeyPress='addSelectInpKeyPress(this,event)' onClick='event.stopPropagation()' placeholder='Add item'> <span class='glyphicon glyphicon-plus addnewicon' onClick='addSelectItem(this,event,1);'></span>";
        window.divider = $('<option/>')
            .addClass('divider')
            .data('divider', true);


        window.addoption = $('<option/>', {class: 'addItem'})
            .data('content', content)

        window.addSelectItem = function(t,ev)
        {
            ev.stopPropagation();

            var bs = $(t).closest('.bootstrap-select')
            var txt=bs.find('.bss-input').val().replace(/[|]/g,"");
            var txt=$(t).prev().val().replace(/[|]/g,"");
            if ($.trim(txt)=='') return;

            // Changed from previous version to cater to new
            // layout used by bootstrap-select.
            var p=bs.find('select');
            var o=$('option', p).eq(-2);
            o.before( $("<option>", { "selected": true, "text": txt}) );
            p.selectpicker('refresh');
        }

        window.addSelectInpKeyPress = function(t,ev)
        {
            ev.stopPropagation();

            // do not allow pipe character
            if (ev.which==124) ev.preventDefault();

            // enter character adds the option
            if (ev.which==13)
            {
                ev.preventDefault();
                addSelectItem($(t).next(),ev);
            }
        }



        function generate_addon_group_html(data) {
            if(data == '') {
                var quantity = [""];
                var quantity_type = 'Value';
                var group_name = '';
                var addOn_html = '<div class="optionRowContainer">'
                                        +'<div class="optionRow">'
                                            //+'<div class="option optionCategory">'
                                            //+'<div class="selct-picker-plain fullWidth">'
                                            //+'<select data-style="no-background-with-buttonline no-padding-left" class="add_on_dropdown selectpicker">'
                                            //+'<option>Select or Add New</option>'
                                            //+'</select>'
                                            //+'</div>'
                                            //    +'<select class="modifier_cat_dropdown"><option>Select or Add New</option></select>'
                                            //+'</div>'
                                            +'<div class="option optionName"><input type="text" value="Mushroom" /></div>'
                                            +'<div class="option optionPrice">'+cur_sym+'<input type="text" value="0.00"></div>'
                                        +'</div>'
                                        +'<div class="optionFunctions ">'
                                        //+'<div class="modify-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                                            +'<div class="delete-icon"><i class="fa fa-times" aria-hidden="true"></i></div>'
                                        +'</div>'
                                    +'</div>';
            } else {
                var group_id        = data.id;
                var quantity        = data.quantity.split(',');
                var quantity_type   = data.quantity_type;
                var group_name      = data.group_name;
                var prompt          = data.prompt ? data.prompt : '';
                var required_html   = '';
                if(data.required == 1) {
                    required_html = 'checked="true"';
                }
                var addOn_html      = '';
                data.addon_group_items.forEach(function(item) {
                    var addon_cat_html = '<select class="modifier_cat_dropdown"><option>Select or Add New</option>';
                    /*data.addon_group_items.forEach(function(addon) {
                        var selected = '';
                        if(addon.id == item.category_id) {
                            selected = 'selected="selected"';
                        }
                        addon_cat_html += '<option value="'+addon.id+'" '+selected+'>'+addon.name+'</option>';
                    });*/
                    addon_cat_html += '</select>';
                    addOn_html += '<div class="optionRowContainer">'
                                        +'<div class="optionRow">'
                                            +'<div class="option optionName">'
                                                +item.name
                                            +'</div>'
                                            +'<div class="option optionPrice">'+cur_sym+'<input type="text" value="'+item.addon_price+'"></div>'
                                        +'</div>'
                                        +'<div class="optionFunctions ">'
                                            //+'<div class="modify-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                                            +'<div class="delete-icon"><i class="fa fa-times addon_item_remove_btn" aria-hidden="true"></i></div>'
                                        +'</div>'
                                    +'</div>';
                });
            }
            var addon_count = $('#addon_details .modifier__rowHeadContainer').length + 1;
            console.log('addon_count ' + addon_count);

            /*var quantity_html = '';
            if(quantity.length > 1) {
                quantity_html += '<div class="quantityField"><span>Min</span><input value="'+quantity[0]+'" class="modifier_qnt" type="number"></div>'
                    +'<div class="quantityField"><span>Max</span><input value="'+quantity[1]+'" class="modifier_qnt" type="number"></div>';
            } else {
                quantity_html += '<div class="quantityField"><span></span><input value="'+quantity[0]+'" class="modifier_qnt" type="number"></div>';
            }*/

            var html = '';
            html += '<div class="modifier__rowHeadContainer">'
                        +'<div class="modifier__rowHead">'
                            +'<div class="modifier__switch">'
                                //+'<label class="switch"><input type="checkbox"><span class="slider round"></span></label>'
                                +'<div class="modifier__name"><input type="text" class="modifier_name" value="'+group_name+'"></div>'
                            +'</div>'
                            +'<div class="modifier__edit">'
                                +'<div class="modify-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                                +'<div class="delete-icon"><i class="fa fa-times addon_group_remove_btn" aria-hidden="true"></i></div>'
                            +'</div>'
                        +'</div>'
                        /*+'<div class="modifier__applyMethod">'
                            +'<input type="radio" id="" name="status" value="1" checked>Apply to all Items &nbsp;&nbsp;&nbsp;&nbsp;'
                            +'<input type="radio" id="" name="status" value="0">Apply to this item only</div>'
                        +'</div>'*/

                        +'<div class="modifier__newModifier">'
                            +'<div class="input__field">'
                                +'<input class="addon_prompt" type="text" name="addon_prompt" value="'+prompt+'">'
                                +'<label for="addon_prompt">Add-On Name</label>'
                            +'</div>'

                            +'<div class="modifier__quantity">'
                                +qtyRangeValue(quantity,quantity_type)
                            +'</div>'
                            +'<div class="modifier__requiredFields">'
                                +'<div class="requiredHeader">Is this required?</div>'
                                +'<div class="requiredRadio">'
                                    +'<div class="tbl-radio-btn margin-right-20">'
                                        +'<input type="radio" class="addon_req" id="id_adn_required'+addon_count+'" name="addon_required'+addon_count+'" value="1" '+required_html+'>'
                                        +'<label for="id_adn_required'+addon_count+'">Required</label>'
                                    +'</div>'
                                    +'<div class="tbl-radio-btn">'
                                        +'<input type="radio" class="addon_req" id="id_adn_optional'+addon_count+'" name="addon_required'+addon_count+'" value="0">'
                                        +'<label for="id_adn_optional'+addon_count+'">Optional</label>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                            +'<div class="modifier__options">'
                                +'<div class="optionRowHeader">'
                                    +'<div class="optionHeadings">'
                                        +'<div class="optionHeading">Add-on Name*</div>'
                                        //+'<div class="optionHeading">Modifier Name</div>'
                                        +'<div class="optionHeading">Price</div>'
                                    +'</div>'
                                    +'<div class="optionSpacer optionFunctions"></div>'
                                +'</div>'
                                +addOn_html
                            +'</div>'
                            +'<div class="row">'
                            +'<div class="fullWidth padding-bottom-20 margin-left-10"><a href="javascript:void(0)" onclick="newReloadAddonItems('+group_id+', this)" data-toggle="modal" data-target="#additem" class="font-weight-700 add_addon_item">+ Add Item</a></div>'
                            +'</div>'
                        +'</div>'
                    +'</div>';

            setTimeout(function(){
                $(".selectpicker").selectpicker('refresh');
            },10)


            return html;
        }

        function newReloadAddonItems(group_id, ths) {
            $('.add_addon_item').removeClass('active');
            $(ths).addClass('active');
            var menuItemRestaurantId = $('#restaurant_id').val();
            var url = '/menu/item-addons/list/' + menuItemRestaurantId + '/' + group_id;

            $.ajax({
                url: url,
                dataType: 'json',
                // contentType: "application/json; charset=utf-8",
                type: 'get',
                // data: inputData,
                success: function (data) {
                    $('#additemform').html(data.data);
                    $("#searchiteminput").on("keyup", function () {
                        var value = $(this).val().toLowerCase();
                        $("#myUL li").filter(function () {
                            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                        });
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err = '';
                    $.each(data.responseJSON.errors, function (key, value) {
                        err += '<p>' + value + '</p>';
                    });

                    alertbox('Error-Addon Group Listing', err, function (modal) {
                        setTimeout(function () {
                            modal.modal('hide');
                        }, 2000);
                    });
                },
                complete: function (data) {
                // $('#addongroup').modal('hide');
                }
            });
        }

        function newAddAddonItems() {

            // var menuItemRestaurantId = $('#menuItemRestaurantId').val();
            var url = '/menu/item-addons/add';
            // var inputData = $('#additemform').serialize();
            var inputData = $('#additemform').serializeArray();

            var adn_item_data = [];
            var addOn_html = '';
            $('#additemform').find("[name='menu_item_id[]']:checked").each(function(item) {
                var item_name = $(this).parents('a').text();
                var item_id   = $(this).val();
                addOn_html += '<div class="optionRowContainer">'
                                +'<div class="optionRow">'
                                    +'<div class="option optionName">'
                                        +item_name
                                    +'</div>'
                                    +'<div class="option optionPrice">'+cur_sym+'<input type="text" value="0.00"></div>'
                                +'</div>'
                                +'<div class="optionFunctions ">'
                                    //+'<div class="modify-icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>'
                                    +'<div class="delete-icon"><i class="fa fa-times addon_item_remove_btn" aria-hidden="true"></i></div>'
                                +'</div>'
                            +'</div>';
                adn_item_data.push({
                    item_val: $(this).parents('a').text(),
                    item_id:  item_id
                });
            });



            /*$.ajax({
                url: url,
                dataType: 'json',
                // contentType: "application/json; charset=utf-8",
                type: 'post',
                data: inputData,
                success: function (data) {
                    var alertboxHeader = 'Success';
                    if (data.message.indexOf('Error') !== -1) {
                        alertboxHeader = 'Error';
                    }
                    alertbox(alertboxHeader, data.message, function (modal) {
                        addonGroupPencilToClick = $('#additemform').find('input[name="addon_group_id"]').val();
                        newGetAddonGroups();
                        setTimeout(function () {
                            modal.modal('hide');
                        }, 2000);
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err = '';
                    $.each(data.responseJSON.errors, function (key, value) {
                        err += '<p>' + value + '</p>';
                    });

                    alertbox('Error- item addon add', err, function (modal) {
                        setTimeout(function () {
                            modal.modal('hide');
                        }, 2000);
                    });
                },
                complete: function (data) {
                // $('#addongroup').modal('hide');
                }
            });*/

            return addOn_html;
        }

        function newGetAddonGroups() {
            var menuItemId              = $('#general_item_id').val(); //$('#menuItemId').val();
            var menuItemRestaurantId    = $('#restaurant_id').val();
            var languageId              = $('#menu_item_edit_form_language_id').val();
            var url                     = '/menu/item-addon-groups/list/' + menuItemRestaurantId + '/' + menuItemId + '/' + languageId;

            $.ajax({
                url: url,
                dataType: 'json',
                // contentType: "application/json; charset=utf-8",
                type: 'get',
                success: function (data) {
                    $('#addons .panel-group').html(data.data);
                    if (addonGroupPencilToClick !== null) {
                        $('#'+addonGroupPencilToClick).click();
                    }
                },
                error: function (data, textStatus, errorThrown) {
                    var err = '';
                    $.each(data.responseJSON.errors, function (key, value) {
                        err += '<p>' + value + '</p>';
                    });

                    alertbox('Error-Addon Group Listing', err, function (modal) {
                        setTimeout(function () {
                            modal.modal('hide');
                            $('#addons .panel-group').html('Error to load addon groups.');
                        }, 2000);
                    });
                },
                complete: function (data) {
                // $('#addongroup').modal('hide');
                }
            });
        }

        function reset_menu_item() {
            $('.stepperContainer').find('li').removeClass('active').first().addClass('active');
            $('.menuItem__stepperTabs .tab-pane').removeClass('active');

            $('#general').addClass('active');
            $('#general_item_id').val('');
            $('#general_cat_sub_cat').prop('selectedIndex', 0).selectpicker('refresh');
            $('#general #itemName').val('');
            $('#general #itemDesc').val('');
            $('#general #itemProductType').val('').selectpicker('refresh');
            $('#general #itemFoodType').val('').selectpicker('refresh');
            $('#general #itemPosId').val('');
            $('#general #itemCalorie').val('');
            $('#general #itemDisplayOrder').val('');
            $('#general #general_delivery').prop('checked', false);
            $('#general #general_takeout').prop('checked', false);
            $('#general #general_size_price_wrap').html('');

            $('#modifier_details .modifier__rowHeadContainer').remove();
            $('#modifier_group_dropdown').prop('selectedIndex', 0);

            $('#addon_details .modifier__rowHeadContainer').remove();
            $('#addon_group_dropdown').prop('selectedIndex', 0);

            //$('#labels .custom_toogle_checkbox').attr('aria-pressed', false).removeClass('active');
            $('#labels .custom_toogle_checkbox').attr('aria-pressed', true).addClass('active');
            $('#labels').find('#serving_size_min').val('1');
            $('#labels').find('#serving_size_max').val('1');
            $('#images .image__uploadedImageContainer').find('.uploaded_menu_image').remove();

            $('.menuItem__newItem .headerTitle').html('Add a New Item');
            $('.menuItem__newItem #edit_item').val('0');
        }

        function load_menu_edit(menu_id, selected_tab) {
            //console.log(menu_id);
            $.ajax({
                url: '/menu/menu_item/'+menu_id,
                dataType: 'json',
                // contentType: "application/json; charset=utf-8",
                type: 'get',
                success: function (data) {
                    //console.log(data);
                    reset_menu_item();  // clear all forms
                    get_all_cat_sub_cat();
                    $('.menuItem__allItem').addClass('hidden');
                    $('.menuItem__newCat').addClass('hidden');
                    $('.menuItem__newItem').removeClass('hidden');
                    $('.menuItem__newItem .headerTitle').html('Edit Item');
                    $('.menuItem__newItem #edit_item').val('1');

                    // SET MENU ITEM GENERAL
                    setTimeout(function() {
                        var html = '';
                        for (var key in window.cat_sub_cat) {
                            if(window.cat_sub_cat.hasOwnProperty(key)) {
                                var cat = window.cat_sub_cat[key];
                                html += '<option value="'+cat.id+'">'+js_ucwords(cat.name)+'</option>';
                                cat.children_recursive.forEach(function(sub) {
                                    html += '<option data-icon="glyphicon-eye-open" value="'+sub.id+'">'+js_ucwords(sub.name)+'</option>';
                                    sub.children_recursive.forEach(function(sub_sub){
                                        html += '<option data-icon="glyphicon-fire" value="'+sub_sub.id+'">'+js_ucwords(sub_sub.name)+'</option>';
                                    });
                                });
                                html  += '<option data-divider="true"></option>';
                            }
                        }
                        $('#general_cat_sub_cat').html(html).selectpicker('refresh');
                        if(data.menu_data.menu_sub_category_id) {
                            var cat_id = data.menu_data.menu_sub_category_id;
                        } else {
                            var cat_id = data.menu_data.menu_category_id;
                        }
                        $("#general_cat_sub_cat").val(cat_id).selectpicker('refresh');
                        $('#general_item_id').val(data.menu_data.id);
                        $('#itemName').val(data.menu_data.name).blur();
                        $('#itemDesc').val(data.menu_data.description).blur();
                        $('#itemProductType').val(data.menu_data.product_type).selectpicker('refresh');
                        $('#itemFoodType').val(data.menu_data.food_type).selectpicker('refresh');
                        $('#itemPosId').val(data.menu_data.pos_id).blur();
                        $('#itemCalorie').val(data.menu_data.calorie).blur();
                        $('#itemDisplayOrder').val(data.menu_data.display_order).blur();
                        if(data.menu_data.delivery == '1') {
                            $('#general_delivery').prop('checked', true);
                        } else {
                            $('#general_delivery').prop('checked', false);
                        }
                        if(data.menu_data.takeout == '1') {
                            $('#general_takeout').prop('checked', true);
                        } else {
                            $('#general_takeout').prop('checked', false);
                        }
                        var size_price = JSON.parse(data.menu_data.size_price);
                        var i = 0;
                        var html = '';
                        $.each(size_price, function(key, value) {
                            var size = value.size.replace(/"/g, '&quot;');//.split(' ')[0];
                            //console.log(size);
                            html += '<div class="general__sizeRow font-weight-700" data-id='+i+'>'
                                 + '<div class="general__size"><input type="text" class="general_size" name="general_size[]" value="'+size+'" /></div>'
                                 + '<div class="general__sizeContainer"><div class="general__sizeOption"><div class="general__sizePrice"> '+cur_sym+'<input type="text" class="general_price" name="general_price[]" value="'+value.price.pop()+'" /></div></div></div>'
                                 + '<div class="general__delete"><a href="javascript:removeGeneralAddSizeRow('+ i +')"><i class="fa fa-times" aria-hidden="true"></i></a></div>'
                                 + '</div>';
                            i++;
                        });

                        $('#general_size_price_wrap').append(html);
                    },500);

                    // SET MENU ITEM - MODIFIER
                    $.each(data.modifier_data, function(key, value) {
                        //console.log(value);
                        var modifier_html = generate_modifier_group_html(value);
                        $('#modifier_details').append(modifier_html);
                        $('#modifier_details .mod_prompt').blur();
                        $('#modifier_details .mod_pos_id').blur();
                        $('#modifier_details .mod_calorie').blur();
                    });

                    // SET MENU ITEM - ADDON
                    //console.log('addon');
                    $.each(data.addon_data, function (key, value) {
                        //console.log(key);
                        //console.log(value);
                        var addon_html = generate_addon_group_html(value);
                        $('#addon_details').append(addon_html);
                    });

                    // SET MENU ITEM - LABELS
                    $('#labels .label_field').each(function(index) {
                        var curr_label = $(this).attr('name');
                        if(data.label_data.label_ids) {
                            var label_id_arr = data.label_data.label_ids.split(',');
                            if($.inArray(curr_label, label_id_arr) != -1) {
                                $(this).val('1');
                                $(this).prev('button').addClass('active').attr('aria-pressed', true);
                            } else {
                                $(this).val('0');
                                $(this).prev('button').removeClass('active').attr('aria-pressed', false);
                            }
                        }
                    });
                    $('#serving_size_min').val(data.label_data.serving_size_min);
                    $('#serving_size_max').val(data.label_data.serving_size_max);

                    // SET MENU ITEM - IMAGES
                    var image_html = '';
                    $.each(data.images_data.desktop_web_images, function (key, value){
                        image_html += '<img class="uploaded_menu_image" src="../'+ value.web_thumb_med +'" alt="uploaded">';
                        //console.log(value);
                    });
                    //console.log(image_html);
                    $('#images .image__uploadedImageContainer').append(image_html);

                    $('.menuItem__stepper li.step').addClass('completed');
                    // read url param and set appropriate tab active, default general
                    var active_tab = selected_tab;
                    if(active_tab == '') {
                        active_tab = 'general';
                    }
                    $('.menuItem__stepper li').removeClass('active');
                    $('.menuItem__newItem .menuItem__stepperTabs div.tab-pane').removeClass('active');
                    $('.menuItem__stepper a[href*="#'+active_tab+'"]').parents('li').addClass('active');
                    $('#'+active_tab).addClass('active');

                    window.history.replaceState(null, null, "?edit="+menu_id+"&tab="+selected_tab);
                    //insertParam('edit', menu_id);
                },
                error: function (data, textStatus, errorThrown) {
                    var err = '';
                    $.each(data.responseJSON.errors, function (key, value) {
                        err += '<p>' + value + '</p>';
                    });

                    alertbox('Error Loading Menu Item', err, function (modal) {
                        setTimeout(function () {
                            modal.modal('hide');
                            $('#addons .panel-group').html('Error to load menu item.');
                        }, 2000);
                    });
                },
                complete: function (data) {
                    // $('#addongroup').modal('hide');
                }
            });
        }

        function generate_add_edit_cat_html(data) {

        }

        $(document).ready(function () {
            if({{ $editFlag }}) {
                var menu_id = '{{ $menuItemId }}';
                var selected_tab = '{{ $selectedTab }}';
                console.log('edit - ' + menu_id + ' tab - ' + selected_tab);
                load_menu_edit(menu_id, selected_tab);
            }

            //get_all_cat_sub_cat();

            /*$('.menuItem__newItem .tab-pane').on('click', function() {
                console.log($(this).attr('id'));
            });*/
            /*$('.menuItem__stepper .step__wrapper').on('click', function() { li.step*/
            $('.menuItem__stepper ul.stepperContainer').on('click', function(event) {
                var elem_id = $(this).find('a[aria-controls]').attr('aria-controls');
                console.log(elem_id);
            });

            $('#slug').change(function() {
                var slg = $(this).val();
                var cat_id = $('#cat_id').val();
                console.log(slg);
                $.ajax({
                    type: 'POST',
                    url: '/menu/validate_cat_slug',
                    data: { slug: slg, cat_id: cat_id},
                    success: function (data) {
                        console.log(data);
                        $('#menu_cat_save').removeAttr('disabled');
                    },
                    error: function (data, textStatus, errorThrown) {
                        alertbox('Error',data.responseJSON.message,function(modal){
                        });
                        $('#menu_cat_save').attr('disabled','disabled');
                    }
                });
            });

            $(".accordion-categoryList").sortable({});
            $(".menuItem__allContainer").sortable({});

            // Toggle visible icon
            $('.productVisibilityTrigger').on('click', function () {
                var currentDiv = $(this).parent();
                $(currentDiv).children().toggleClass('hidden')

            });
            $('#saveAddonItemsToGroupBtn').click(function () {
                var addon_html = newAddAddonItems();
                $('.add_addon_item.active').parents('.modifier__newModifier').find('.modifier__options').append(addon_html);
                $('.add_addon_item.active').removeClass('active');
            });

            $('#sidebar-category-list').on('click', '.fetch-menu-items', function() {
                var cat_id = $(this).data('cat');
                var sub_cat_id = $(this).data('sub-cat');
                //console.log(cat_id + ' - ' + sub_cat_id);
                var menu_sel = $(this);
                $.ajax({
                    type: 'POST',
                    url: '/menu/menu_items',
                    data: { menu_category_id: cat_id, menu_sub_category_id: sub_cat_id},
                    success: function (data) {
                        var html = generate_menu_html(data);

                        $('.menuItem__allItem #menuItem_wrapper').html(html);
                        $('li.selectedCategory').removeClass('selectedCategory');
                        $(menu_sel).parents('.accordion-category').addClass('selectedCategory');
                        $(menu_sel).parents('.accordion-subcategoryItem').addClass('selectedCategory');
                        $('.menuItem__allItem').removeClass('hidden');
                        $('.menuItem__newCat').addClass('hidden');
                        $('.menuItem__newItem').addClass('hidden');

                    },
                    error: function (data, textStatus, errorThrown) {

                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        alertbox('Error',err,function(modal){
                        });
                    },
                    complete: function(data) {
                        console.log('complete');
                        console.log(data);
                    }
                });

            });

            $('#search_menu_item').keypress(function() {
                var search_key = $(this).val().trim();
                if(search_key) {
                    console.log(search_key);
                    /*$.ajax({
                        type: 'POST',
                        url: '/menu/menu_items',
                        data: { search_key: search_key },
                        success: function (data) {
                            var html = generate_menu_html(data);

                            $('.menuItem__allItem #menuItem_wrapper').html(html);
                            $('li.selectedCategory').removeClass('selectedCategory');
                        },
                        error: function (data, textStatus, errorThrown) {

                            var err='';
                            $.each(data.responseJSON.errors, function(key, value){
                                err+='<p>'+value+'</p>';
                            });
                            alertbox('Error',err,function(modal){
                            });
                        },
                        complete: function(data) {
                            console.log('complete');
                            console.log(data);
                        }
                    });*/
                }
            });

            // MODIFIER GROUP - ADD EXISTING
            $('#modifier_group_add_btn').on('click', function(){
                var mod_id = parseInt($('#modifier_group_dropdown').val());
                var data = item_modifier_data.find(x => x.id === mod_id);
                if(mod_id && data.id) {
                    var html = generate_modifier_group_html(data);
                    $('#modifier_details').append(html);
                    $('#modifier_details .mod_prompt').blur();
                    $('#modifier_details .mod_pos_id').blur();
                    $('#modifier_details .mod_calorie').blur();
                }
            });
            // MODIFIER GROUP - ADD BLANK
            $('#modifier_group_blank_add_btn').on('click', function(){
                console.log('blank modifier group');
                var data = '';
                var html = generate_modifier_group_html(data);
                $('#modifier_details').append(html);
                $('#modifier_details .mod_prompt').blur();
                $('#modifier_details .mod_pos_id').blur();
                $('#modifier_details .mod_calorie').blur();
            });
            // MODIFIER GROUP - REMOVE
            $('#modifier_details').on('click', '.modifier_group_remove_btn', function() {
                $(this).parents('.modifier__rowHeadContainer').remove();
                console.log('remove modifier btn clicked');
            });

            // ADDONS GROUP - ADD EXISTING
            $('#addon_group_add_btn').on('click', function(){
                var adn_id = parseInt($('#addon_group_dropdown').val());
                var data = item_addon_data.find(x => x.id === adn_id);
                console.log(adn_id);
                console.log(data);
                if(data.id) {
                    var html = generate_addon_group_html(data);
                    $('#addon_details').append(html);
                }
            });
            // ADDONS GROUP - ADD BLANK
            $('#addon_group_blank_add_btn').on('click', function(){
                console.log('blank addon group');
                var data = '';
                var html = generate_addon_group_html(data);
                $('#addon_details').append(html);
            });
            // ADDONS GROUP - REMOVE
            $('#addon_details').on('click', '.addon_group_remove_btn', function() {
                $(this).parents('.modifier__rowHeadContainer').remove();
                console.log('remove addon btn clicked');
            });
            // ADDONS ITEMS - REMOVE
            $('#addon_details').on('click', '.addon_item_remove_btn', function() {
                $(this).parents('.optionRowContainer').remove();
                console.log('remove addon item clicked');
            });

            // ADD CATEGORY
            $('#add_cat_btn').on('click', function() {
                window.history.replaceState(null, null, "/menu/all_items");
                get_all_cat_sub_cat();
                $('#cat_id').val('');
                $('#catName').val('');
                $('#catDesc').val('');
                //ToDo: clear other category fields
                $('#priority').val('');
                $('#pos_id').val('');
                $('#slug').val('');
                $('#is_delivery').prop('checked', false);
                $('#is_carryout').prop('checked', false);
                $('#is_popular').prop('checked', false);
                $('#is_favourite').prop('checked', false);
                $('#product_type').val('');
                $('#image_class').val('');
                $('#meta_title').val('');
                $('#meta_keyword').val('');
                $('#cat_image').attr('src','').parents('div.uploadedImageRow').addClass('hide');
                $('#cat_image_icon').attr('src','').parents('div.uploadedImageRow').addClass('hide');
                $('#category_attachment').attr('src','').parents('div.uploadedImageRow').addClass('hide');
                $('#banner_image_web').attr('src','').parents('div.uploadedImageRow').addClass('hide');
                $('#banner_image_mobile').attr('src','').parents('div.uploadedImageRow').addClass('hide');
                $('#banner_image_app').attr('src','').parents('div.uploadedImageRow').addClass('hide');
                $('#catStatus_wrap .icon-eye-open').removeClass('hidden');
                $('#catStatus_wrap .icon-eye-close').addClass('hidden');
                $('#menu_cat_title').html('Add a New Category');
                $('.menuItem__allItem').addClass('hidden');
                $('.menuItem__newCat').removeClass('hidden');
                $('.menuItem__newItem').addClass('hidden');
                setTimeout(function () {
                    // ToDo: Cat SubCat recursive function
                    var html = '<option value="0" selected>Select Category</option>';
                    for (var key in window.cat_sub_cat) {
                        if(window.cat_sub_cat.hasOwnProperty(key)) {
                            var cat = window.cat_sub_cat[key];
                            html += '<option value="'+cat.id+'">'+js_ucwords(cat.name)+'</option>';
                            html += generate_sub_cat_dropdown_recursive(cat, 0, 1);
                        }
                    }
                    $('#cat_dropdown').html(html).selectpicker('refresh');
                }, 500);
            });

            // SUB CATEGORY DROPDOWN - ON CATEGORY CHANGE
            $('#cat_dropdown').on('change', function(){
                var cat_id = $(this).val();
                console.log(cat_id);
                var html = '<option value="0" selected>Select Sub-Category</option>';
                for (var key in window.cat_sub_cat) {
                    if(window.cat_sub_cat.hasOwnProperty(key)) {
                        var cat = window.cat_sub_cat[key];
                        if(cat.id == cat_id) {
                            selected = 'selected="selected"';
                            cat.children_recursive.forEach(function(sub) {
                                html += '<option value="'+sub.id+'">'+js_ucwords(sub.name)+'</option>'
                            });
                        }
                    }
                }

                $('#sub_cat_dropdown').html(html).selectpicker('refresh');
            });

            // ADD ITEM
            $('#add_item_btn').on('click', function() {
                // clear edit item param from url
                window.history.replaceState(null, null, "/menu/all_items");
                $('.menuItem__stepper li.step').each(function(){
                    $(this).removeClass('completed');
                });
                reset_menu_item();  // clear all forms
                get_all_cat_sub_cat();
                $('.menuItem__allItem').addClass('hidden');
                $('.menuItem__newCat').addClass('hidden');
                $('.menuItem__newItem').removeClass('hidden');
                $('.menuItem__newItem .headerTitle').html('Add a New Item');
                $('.menuItem__newItem #edit_item').val('0');
                $('#general_item_id').val('');
                setTimeout(function () {
                    var html = '';
                    for (var key in window.cat_sub_cat) {
                        if(window.cat_sub_cat.hasOwnProperty(key)) {
                            var cat = window.cat_sub_cat[key];
                            //html += '<optgroup data-max-options="2" label="'+js_ucwords(cat.name)+'">'
                            html += '<option value="'+cat.id+'">'+js_ucwords(cat.name)+'</option>';
                            cat.children_recursive.forEach(function(sub) {
                                html += '<option data-icon="glyphicon-eye-open" value="'+sub.id+'">'+js_ucwords(sub.name)+'</option>';
                                sub.children_recursive.forEach(function(sub_sub){
                                    html += '<option data-icon="glyphicon-fire" value="'+sub_sub.id+'">'+js_ucwords(sub_sub.name)+'</option>';
                                });
                            });
                            // TODO - only till second last
                            html  += '<option data-divider="true"></option>';
                            //html += '</optgroup>';
                        }
                    }
                    $('#general_cat_sub_cat').html(html).selectpicker('refresh');

                }, 500);
            });

            // ADD CATEGORY - SAVE
            /*$('#menu_cat_save').on('click', function() {
                console.log('menu category save');
                var cat_name = $('#catName').val();
                var cat_desc = $('#catDesc').val();
                if($('.categoryForm .hidden').hasClass('fa-eye')) {
                    cat_status = 0;
                } else {
                    cat_status = 1;
                }
                var form = $('#cat_sub_cat_form')[0];
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/menu/add_category',
                    data: data,
                    success: function (data) {
                        console.log('success');
                        console.log(data);
                        get_all_cat_sub_cat();
                    },
                    error: function (data, textStatus, errorThrown) {

                    },
                    complete: function(data) {
                        console.log('complete');
                    }
                });
            });*/

            // ADD CATEGORY - CANCEL
            $('#menu_cat_cancel').on('click', function() {
                $('.menuItem__allItem').removeClass('hidden');
                $('.menuItem__newCat').addClass('hidden');
                $('.menuItem__newItem').addClass('hidden');
            });

            $('#catStatus_wrap').on('click', function() {
                var status = $('#catStatus_wrap .icon-eye-open').hasClass('hidden');
                if(status) {
                    $('#catStatus').val(0);
                } else {
                    $('#catStatus').val(1);
                }
            });

            // EDIT CATEGORY
            $('#sidebar-category-list').on('click', '.edit-icon', function() {
                var menu_item = $(this).siblings('.fetch-menu-items');
                var cat_id = menu_item.data('cat');
                var sub_cat_id = menu_item.data('sub-cat');
                if(window.cat_sub_cat == "") {
                    get_all_cat_sub_cat();
                }
                console.log(cat_id + ' - ' + sub_cat_id);
                $.ajax({
                    type: 'POST',
                    url: '/menu/get_cat_sub_cat',
                    data: { cat_id: cat_id, sub_cat_id: sub_cat_id},
                    success: function (data) {
                        window.history.replaceState(null, null, "/menu/all_items");
                        setTimeout(function () {
                            $('#menu_cat_title').html('Edit Category');
                            $('.menuItem__allItem').addClass('hidden');
                            $('.menuItem__newCat').removeClass('hidden');
                            $('.menuItem__newItem').addClass('hidden');
                            $('#catName').val(data.name).blur();
                            $('#catDesc').val(data.description).blur();
                            $('#priority').val(data.priority).blur();
                            $('#pos_id').val(data.pos_id).blur();
                            $('#slug').val(data.slug).blur();
                            if(data.is_delivery) {
                                $('#is_delivery').prop('checked', true);
                            }
                            if(data.is_carryout) {
                                $('#is_carryout').prop('checked', true);
                            }
                            if(data.is_popular) {
                                $('#is_popular').prop('checked', true);
                            }
                            if(data.is_favourite) {
                                $('#is_favourite').prop('checked', true);
                            }
                            $('#product_type').val(data.product_type);
                            $('#language_id').val(data.language_id).selectpicker('refresh');

                            $('#image_class').val(data.image_class).blur();
                            $('#meta_title').val(data.meta_title).blur();
                            $('#meta_keyword').val(data.meta_keyword).blur();

                            //$('#meta_title').val(data.meta_title).blur();
                            //$('#meta_title').val(data.meta_title).blur();

                            if(cat_id) {
                                console.log("==="+cat_id);
                                var html = '<option value="0">Select Category</option>';
                                for (var key in window.cat_sub_cat) {
                                    if(window.cat_sub_cat.hasOwnProperty(key)) {
                                        var cat = window.cat_sub_cat[key];
                                        var sel_atr = '';
                                        if(sub_cat_id && cat.id == cat_id) {
                                            sel_atr = 'selected';
                                        }
                                        html += '<option value="'+cat.id+'" '+sel_atr+'>'+js_ucwords(cat.name)+'</option>';
                                        /*for(var subkey in cat.children_recursive) {
                                            var subcat = cat.children_recursive[subkey];
                                            var sel_atr = '';
                                            if(subcat.id == cat_id) {
                                                sel_atr = 'selected';
                                            }
                                            html += '<option value="'+subcat.id+'" '+sel_atr+'>|-- '+js_ucwords(subcat.name)+'</option>';
                                        }*/
                                        html += generate_sub_cat_dropdown_recursive(cat, cat_id, 1);
                                    }
                                }
                                $('#cat_dropdown').html(html);
                                var edit_cat_id = cat_id;
                                if(sub_cat_id) {
                                    var edit_cat_id = sub_cat_id;
                                }
                                $('#cat_id').val(edit_cat_id);
                                $("#cat_dropdown option[value*='"+edit_cat_id+"']").remove(); // remove self
                                $('#cat_dropdown').selectpicker('refresh');
                            }

                            /*if(sub_cat_id) {
                                console.log("---"+sub_cat_id);
                                var html = '<option value="0" selected>Select Sub-Category</option>';
                                for (var key in window.cat_sub_cat) {
                                    if(window.cat_sub_cat.hasOwnProperty(key)) {
                                        var cat = window.cat_sub_cat[key];
                                        if(cat.id == cat_id) {
                                            var sel_atr = '';
                                            cat.children_recursive.forEach(function(sub) {
                                                if(sub.id == sub_cat_id) {
                                                    sel_atr = 'selected="selected"';
                                                }
                                                html += '<option value="'+sub.id+'" '+sel_atr+'>'+js_ucwords(sub.name)+'</option>'
                                            });
                                        }
                                    }
                                }
                                $('#sub_cat_dropdown').html(html).selectpicker('refresh');
                                $('#cat_id').val(sub_cat_id);
                            }*/
                            // image
                            if(data.hasOwnProperty('image') && data.image !== null) {
                                if(data.image.hasOwnProperty('thumb_image_sml') && data.image.thumb_image_sml !== null) {
                                    $('#cat_image').attr('src', data.image.thumb_image_sml).parents('div.uploadedImageRow')
                                        .removeClass('hide').attr('data-type', 'image');
                                }
                            }
                            // image icon
                            if(data.hasOwnProperty('image_icon') && data.image_icon !== null) {
                                if(data.image_icon.hasOwnProperty('thumb_image_sml') && data.image_icon.thumb_image_sml !== null) {
                                    $('#cat_image_icon').attr('src', data.image_icon.thumb_image_sml).parents('div.uploadedImageRow')
                                        .removeClass('hide').attr('data-type', 'image_icon');
                                }
                            }
                            // category attachment
                            if(data.hasOwnProperty('cat_attachment') && data.cat_attachment !== null) {
                                $('#category_attachment').attr('src', data.cat_attachment).parents('div.uploadedImageRow')
                                    .removeClass('hide').attr('data-type', 'attachment');
                            }
                            // banner image web
                            if(data.hasOwnProperty('promotional_banner_image') && data.promotional_banner_image !== null) {
                                $('#banner_image_web').attr('src', data.promotional_banner_image).parents('div.uploadedImageRow')
                                    .removeClass('hide').attr('data-type', 'banner_web');
                            }
                            // benner image mobile
                            if(data.hasOwnProperty('promotional_banner_image_mobile') && data.promotional_banner_image_mobile !== null) {
                                $('#banner_image_mobile').attr('src', data.promotional_banner_image_mobile).parents('div.uploadedImageRow')
                                    .removeClass('hide').attr('data-type', 'banner_mobile');
                            }
                            // benner image app
                            if(data.hasOwnProperty('promotional_banner_image_app') && data.promotional_banner_image_app !== null) {
                                $('#banner_image_app').attr('src', data.promotional_banner_image_app).parents('div.uploadedImageRow')
                                    .removeClass('hide').attr('data-type', 'banner_app');
                            }

                            if(data.status == 1) {
                                $('#catStatus_wrap .icon-eye-open').removeClass('hidden');
                                $('#catStatus_wrap .icon-eye-close').addClass('hidden');
                            } else {
                                $('#catStatus_wrap .icon-eye-close').removeClass('hidden');
                                $('#catStatus_wrap .icon-eye-open').addClass('hidden');
                            }
                            $('#catStatus').val(data.status);
                            console.log(data);
                        },500);
                    },
                    error: function (data, textStatus, errorThrown) {
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        alertbox('Error',err,function(modal){
                        });
                    },
                    complete: function(data) {
                        console.log('complete');
                    }
                });
            });

            $('.cat_file_remove').on('click', function () {
                var clicked_elem    = $(this);
                var type            = clicked_elem.parents('.uploadedImageRow').data('type');
                var cat_id          = $('#cat_dropdown').val();
                console.log(type + ' - ' + cat_id);
                $.ajax({
                    type: 'POST',
                    url: '/menu/delete_category_file',
                    data: {
                        'type': type,
                        'cat_id': cat_id
                    },
                    success: function (data) {
                        console.log(data);
                        if(data == 1) {
                            console.log('cleared cat image');
                            var sr = clicked_elem.closest('.uploadedImageRow').addClass('hide').find('img').removeAttr('src');
                        }
                    },
                    error: function (data, textStatus, errorThrown) {
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        alertbox('Error',err,function(modal){
                        });
                    }
                });
            });

            var i = 0;
            $('#general_add_size_btn').on('click', function() {
                var html = '<div class="general__size"><input type="text" class="general_size" name="general_size[]" value="" /></div>'
                        + '<div class="general__sizeContainer"><div class="general__sizeOption"><div class="general__sizePrice"> '
                        +cur_sym+'<input type="text" class="general_price" name="general_price[]" value="" /></div>'
                        +'Is Active<input type="checkbox" class="general_is_active" name="general_is_active[]" value="1" /></div></div>'
                        +'Is Delivery<input type="checkbox" class="general_is_delivery" name="general_is_delivery[]" value="1" /></div>'
                        +'Is Carryout<input type="checkbox" class="general_is_carryout" name="general_is_carryout[]" value="1" /></div>'
                        + '<div class="general__delete"><a href="javascript:removeGeneralAddSizeRow('+ i +')"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
                $('#general_size_price_wrap').append("<div class='general__sizeRow font-weight-700' data-id="+i+">"+html+"</div>");
                i++
            });

            window.removeGeneralAddSizeRow = function(id){
                $("#general_size_price_wrap [data-id='"+ id +"']").remove()
            };

            $('#save_continue').on('click', function(){
                var active_tab = $('.step.active').find('a').attr('aria-controls');
                var item_id = $('#general_item_id').val();
                console.log('active tab - ' + active_tab);
                if(active_tab == 'general') {
                    var gen_size = $("#general_size_price_wrap .general_size").map(function() {
                        return $(this).val();
                    }).get();
                    var gen_price = $("#general_size_price_wrap .general_price").map(function() {
                        return $(this).val();
                    }).get();
                    var gen_is_active = $("#general_size_price_wrap .general_is_active").map(function() {
                        if ($(this).is(":checked")) {
                            return $(this).val();
                        } else {
                            return 0;
                        }
                    }).get();
                    var gen_is_delivery = $("#general_size_price_wrap .general_is_delivery").map(function() {
                        if ($(this).is(":checked")) {
                            return $(this).val();
                        } else {
                            return 0;
                        }
                    }).get();
                    var gen_is_carryout = $("#general_size_price_wrap .general_is_carryout").map(function() {
                        if ($(this).is(":checked")) {
                            return $(this).val();
                        } else {
                            return 0;
                        }
                    }).get();
                    var data = {
                        general_item_id:    item_id,
                        //restaurant_id:        $('#restaurant_id').val(),
                        gen_cat_sub_cat:        $('#general_cat_sub_cat').val(),
                        gen_item_name:          $('#itemName').val(),
                        gen_item_desc:          $('#itemDesc').val(),
                        gen_item_product_type:  $('#itemProductType').val(),
                        gen_item_food_type:     $('#itemFoodType').val(),
                        gen_item_pos_id:        $('#itemPosId').val(),
                        gen_item_calorie:       $('#itemCalorie').val(),
                        gen_item_display_order: $('#itemDisplayOrder').val(),
                        gen_delivery:           $('#general_delivery').prop('checked'),
                        gen_takeout:            $('#general_takeout').prop('checked'),
                        general_size:           gen_size,
                        general_price:          gen_price,
                        general_is_active:      gen_is_active,
                        general_is_delivery:    gen_is_delivery,
                        general_is_carryout:    gen_is_carryout,
                    };
                    $.ajax({
                        type: 'POST',
                        url: '/menu/add_menu_item',
                        data: data,
                        success: function (data) {
                            $('#general_item_id').val(data.id);
                            var size_price = JSON.parse(data.size_price);
                            console.log(size_price);
                            /*$.each(size_price, function(key, value) {
                                console.log(value.size);
                            });*/
                            // $('.step.active').addClass('completed').removeClass('active');
                            // $('.step.active').addClass('completed').removeClass('active');
                            // $('.step.completed').next().find('a').click();
                        },
                        error: function (data, textStatus, errorThrown) {
                            var err='';
                            $.each(data.responseJSON.errors, function(key, value){
                                err+='<p>'+value+'</p>';
                            });
                            alertbox('Error',err,function(modal){
                            });
                        },
                        complete: function(data) {
                            console.log('complete');
                        }
                    });
                } else if(active_tab == 'schedules') {

                } else if(active_tab == 'modifiers') {
                    var mod_data = [];
                    $('#modifier_details').find('.modifier__rowHeadContainer').each(function(index) {
                        var mod_obj = $(this);
                        // modifier name, qty, type
                        var modifier_name = mod_obj.find('.modifier__name input.modifier_name').val();
                        var modifier_prompt = mod_obj.find('.input__field input.mod_prompt').val();
                        var modifier_qty = [];
                        mod_obj.find('.modifier__quantity input.modifier_qnt').each(function(index) {
                            modifier_qty.push($(this).val());
                        });
                        var modifier_type = 'Value';
                        if(modifier_qty.length == 2) {
                            modifier_type = 'Range';
                        }
                        // modifier required
                        var required = mod_obj.find(".modifier__requiredFields input:radio.modifier_req:checked").val();
                        // modifier category, name, price
                        var modifier_cats = [];
                        mod_obj.find('.modifier__options .optionRowContainer').each(function(index){
                            var cat      = $(this).find('.modifier_cat_dropdown').children("option:selected").val();
                            var cat_name = $(this).find('.modifier_cat_dropdown').children("option:selected").text();
                            var name     = $(this).find('.optionName input').val();
                            var price    = $(this).find('.optionPrice input').val();
                            modifier_cats.push({
                                category_id:    cat,
                                category_name:  cat_name,
                                modifier_name:  name,
                                modifier_price: price
                            });
                        });
                        mod_data.push({
                            item_id:            item_id,
                            modifier_name:      modifier_name,
                            modifier_prompt:    modifier_prompt,
                            modifier_qty:       modifier_qty,
                            modifier_type:      modifier_type,
                            required:           required,
                            modifier_cats:      modifier_cats
                        });
                    });
                    console.log(mod_data);
                    if(mod_data.length) {
                        $.ajax({
                            type: 'POST',
                            url: '/menu/save_modifier_group',
                            data: {mod_data},
                            success: function (data) {
                                console.log(data);
                            },
                            error: function (data, textStatus, errorThrown) {
                                var err='';
                                $.each(data.responseJSON.errors, function(key, value){
                                    err+='<p>'+value+'</p>';
                                });
                                alertbox('Error',err,function(modal){
                                });
                            },
                            complete: function(data) {
                                console.log('complete');
                            }
                        });
                    }

                } else if(active_tab == 'addons') {
                    var adn_data = [];
                    $('#addon_details').find('.modifier__rowHeadContainer').each(function(index) {
                        var adn_obj = $(this);
                        // addon name, qty, type
                        var addon_name = adn_obj.find('.modifier__name input.modifier_name').val();
                        var addon_prompt = adn_obj.find('.input__field input.addon_prompt').val();
                        var addon_qty = [];
                        adn_obj.find('.modifier__quantity input.modifier_qnt').each(function(index) {
                            addon_qty.push($(this).val());
                        });
                        var addon_type = 'Value';
                        if(addon_qty.length == 2) {
                            addon_type = 'Range';
                        }
                        // addon required
                        var required = adn_obj.find(".modifier__requiredFields input:radio.addon_req:checked").val();
                        console.log(adn_obj.find(".modifier__requiredFields input:radio.addon_req:checked").val());
                        // addon item name, price
                        var addon_items = [];
                        adn_obj.find('.modifier__options .optionRowContainer').each(function(index) {
                            var cat = $(this).find('.modifier_cat_dropdown').children("option:selected").val();
                            var name = '';
                            if ($(this).find('.optionName input').val()) {
                                name = $(this).find('.optionName input').val();
                            } else {
                                name = $(this).find('.optionName').html();
                            }
                            var price   = $(this).find('.optionPrice input').val();
                            addon_items.push({
                                item_name:  name,
                                item_price: price
                            });
                        });
                        adn_data.push({
                            item_id:            item_id,
                            addon_name:         addon_name,
                            addon_prompt:       addon_prompt,
                            addon_qty:          addon_qty,
                            addon_type:         addon_type,
                            required:           required,
                            addon_items:        addon_items
                        });
                    });
                    console.log(adn_data);
                    if(adn_data) {
                        $.ajax({
                            type: 'POST',
                            url: '/menu/save_addon_group',
                            data: {adn_data},
                            success: function (data) {
                                console.log(data);
                            },
                            error: function (data, textStatus, errorThrown) {
                                var err='';
                                $.each(data.responseJSON.errors, function(key, value){
                                    err+='<p>'+value+'</p>';
                                });
                                alertbox('Error',err,function(modal){
                                });
                            },
                            complete: function(data) {
                                console.log('complete');
                            }
                        });
                    }

                } else if(active_tab == 'labels') {
                    console.log('labels');
                    var label_ids = '';
                    $('.label_field').each(function() {
                        if($(this).val() == "1"){
                            label_ids += $(this).attr('name') +',';
                        }
                    });
                    label_ids = label_ids.replace(/,\s*$/, "");
                    var serving_size_min = $('#serving_size_min').val() ? $('#serving_size_min').val() : 0;
                    var serving_size_max = $('#serving_size_max').val() ? $('#serving_size_max').val() : 0;
                    console.log(label_ids);
                    $.ajax({
                        type: 'POST',
                        url: '/menu/save_label_data',
                        data: {
                            item_id:            item_id,
                            label_ids:          label_ids,
                            serving_size_min:   serving_size_min,
                            serving_size_max:   serving_size_max
                        },
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (data, textStatus, errorThrown) {
                            var err='';
                            $.each(data.responseJSON.errors, function(key, value){
                                err+='<p>'+value+'</p>';
                            });
                            alertbox('Error',err,function(modal){
                            });
                        },
                        complete: function(data) {
                            console.log('complete');
                        }
                    });

                } else if(active_tab == 'images') {

                    var item_id = $('#general_item_id').val();
                    if(item_id) {
                        var fd = new FormData();

                        if(document.getElementById('menu_item_image') !== null) {
                            var ins = document.getElementById('menu_item_image').files.length;
                        }
                        console.log(ins);
                        for (var x = 0; x < ins; x++) {
                            console.log(x);
                            fd.append("item_image[]", document.getElementById('menu_item_image').files[x]);
                            fd.append('item_id', item_id);

                            $.ajax({
                                type: 'POST',
                                url: '/menu/save_menu_image',
                                data: fd,
                                enctype: 'multipart/form-data',
                                processData: false,  // tell jQuery not to process the data
                                contentType: false,   // tell jQuery not to set contentType
                                success: function (data) {
                                    console.log(data);
                                    var cat_id = $('#general_cat_sub_cat').val();
                                    console.log('cat_id ' + cat_id);
                                    reset_menu_item();
                                    $('#sidebar-category-list .fetch-menu-items').each(function() {
                                        var cat = $(this).data('cat');
                                        var scat = $(this).data('sub-cat');
                                        console.log('cat ' + cat + ' scat ' + scat);
                                        if(cat == $('#general_cat_sub_cat').val() || scat == $('#general_cat_sub_cat').val()) {
                                            $(this).trigger('click');
                                            console.log('inside');
                                        }
                                    });
                                },
                                error: function (data, textStatus, errorThrown) {
                                    var err='';
                                    $.each(data.responseJSON.errors, function(key, value){
                                        err+='<p>'+value+'</p>';
                                    });
                                    alertbox('Error',err,function(modal){
                                    });
                                },
                                complete: function(data) {
                                    console.log('complete');
                                }
                            });
                        }
                    }
                }

                console.log('save continue clicked ' + active_tab);
                var edit_item = isEditItem();
                if(edit_item) {
                    var next_elem = $('.step.active').addClass('completed').removeClass('active').next().find('a');
                    next_elem.click();
                    var next_elem_tab = next_elem.attr('aria-controls');
                    window.history.replaceState(null, null, "?edit="+edit_item+"&tab="+next_elem_tab);
                } else {
                    $('.step.active').addClass('completed').removeClass('active');
                    $('.step.completed').next().find('a').click();
                }
                // $('.step.completed').next().find('.step').trigger('click');
                //$('.step.completed').next().find('a').click();
            });

            /*$('#cat_image_remove').on('click', function() {
                var cat_id = $('#cat_id').val();
                var cat_img_src = $('#cat_image').attr('src');

                console.log(cat_id);
                //$('cat_image').removeAttr('src');
            });
            $('#cat_image_icon_remove').on('click', function() {
                var cat_id = $('#cat_id').val();
                var cat_img_icon_src = $('#cat_image_icon').attr('src');
                console.log(cat_id);
                //$('cat_image_icon').removeAttr('src');
            });*/

            $('.stepper__prev').on('click', function () {
                // $('.step.completed:last-child').addClass('active').removeClass('completed');
                if(!isEditItem()) {
                if (!$('.step:first-child').hasClass('active') && !($('.step:last-child').hasClass('active') && $('.step:last-child').hasClass('completed'))) {
                    $('.step.active').prev().removeClass('completed').find('a').click();
                }
                if ($('.step.completed.active')) {
                    $('.step.completed.active').removeClass('completed');
                }
                } else {
                    $('.step.active').prev().find('a').click();
                }


            });


            $(".upload__file > input[type=button]").click(function () {
                $(this).parents(".upload__file").find("input[type=file]").click();
            })

            $(".upload__file input[type=file]").change(function () {
                if(!$('#cat_sub_cat_form').valid()){
                    console.log('form not valid');
                    return false;
                }
            })

            $(".upload__file .submit").click(function () {
                if(!$('#cat_sub_cat_image').valid()){
                    console.log('form not valid');
                    return false;
                }
            })

            $.validator.addMethod('filesize', function(value, element, param) {
                return this.optional(element) || (element.files[0].size <= param)
            });

            $("#cat_sub_cat_form").validate({
                rules: {
                    cat_sub_cat_image: {
                        //required: true,
                        extension: "png|jpe?g",
                        filesize: 1048576
                    },
                    cat_sub_cat_image_icon: {
                        //required: true,
                        extension: "png|jpe?g",
                        filesize: 1048576
                    },
                    /*category_attachment: {
                        //required: true,
                        extension: "png|jpe?g",
                        filesize: 1048576
                    },*/
                    promo_banner_image_web: {
                        //required: true,
                        extension: "png|jpe?g",
                        filesize: 1048576
                    },
                    promo_banner_image_mobile: {
                        //required: true,
                        extension: "png|jpe?g",
                        filesize: 1048576
                    },
                    promo_banner_image_app: {
                        //required: true,
                        extension: "png|jpe?g",
                        filesize: 1048576
                    }
                },
                messages:{
                    cat_sub_cat_image: {
                        extension: "We accept only JPEG and PNG",
                        filesize: "Upload image less Then 1MB"
                    },
                    cat_sub_cat_image_icon: {
                        extension: "We accept only JPEG and PNG",
                        filesize: "Upload image less Then 1MB"
                    },
                    /*category_attachment: {
                        extension: "We accept only JPEG and PNG",
                        filesize: "Upload image less Then 1MB"
                    },*/
                    promo_banner_image_web: {
                        extension: "We accept only JPEG and PNG",
                        filesize: "Upload image less Then 1MB"
                    },
                    promo_banner_image_mobile: {
                        extension: "We accept only JPEG and PNG",
                        filesize: "Upload image less Then 1MB"
                    },
                    promo_banner_image_app: {
                        extension: "We accept only JPEG and PNG",
                        filesize: "Upload image less Then 1MB"
                    }
                },
                ignore: ""
            });
            if(!{{$editFlag}}) {
                $('#sidebar-category-list .fetch-menu-items').first().trigger('click');
            }
        });
    </script>

    <style type="text/css">
        @Color_Facility: navy;
        @Color_Equipment: black;
        @Color_DropBackground: navy;
        @Color_DropForeground: white;
        @Color_Lines: silver;
        @IndentSize: 20px;

        .no-select() {
            -moz-user-select: none;
            -ms-user-select: none;
            -webkit-user-select: none;
            user-select: none;
        }
        .no-wrap() {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        #sidebar-category-list {
        .no-select();

            cursor: default;
            border: none;
            margin: 0px;
            padding: 0px;
            width: auto;
            overflow-y: auto;
            /*border: 1px solid black;*/
            /*margin: 10px;*/
            /*padding: 10px;*/
            /*width: 300px;*/
            /*overflow-y: scroll;*/
            white-space: nowrap;

        ul {
            display: block;
            margin: 0;
            padding: 0 0 0 @IndentSize;
        }

        li {

            display: block;
            margin: 2px;
            padding: 2px 2px 2px 0;

        [class*="node"] {

            display: inline-block;

        &.hover {
             background-color: @Color_DropBackground;
             color: @Color_DropForeground;
         }

        }

        .node-facility {
            color: @Color_Facility;
            font-weight: bold;
        }

        .node-cpe {
            color: @Color_Equipment;
            cursor: pointer;
        }

        }

        li li {
            border-left: 1px solid @Color_Lines;
        &:before {
             color: @Color_Lines;
             font-weight: 300;
             content: "— ";
         }
        }

        }
    </style>
    <script type="text/javascript">
        function isEditItem() {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("edit");
            if(c === 'undefined' || typeof c === undefined || c == null) {
                // add
                return 0;
            } else {
                // edit
                return c;
            }
        }
        function insertParam(key, value)
        {
            key = encodeURI(key); value = encodeURI(value);
            var kvp = document.location.search.substr(1).split('&');
            console.log(kvp);
            var i=kvp.length; var x;
            while(i--) {
                x = kvp[i].split('=');
                if (x[0]==key) {
                    x[1] = value;
                    kvp[i] = x.join('=');
                    break;
                }
            }
            console.log(kvp);
            if(i<0) { kvp[kvp.length] = [key,value].join('='); }

            //this will reload the page, it's likely better to store this until finished
            document.location.search = kvp.join('&');
        }
        var DragAndDrop = (function (DragAndDrop) {
            function shouldAcceptDrop(item) {
                var $target = $(this).closest("li");
                var $item = item.closest("li");
                if ($.contains($item[0], $target[0])) {
                    // can't drop on one of your children!
                    return false;
                }
                return true;
            }

            function itemOver(event, ui) {
            }

            function itemOut(event, ui) {
            }

            function itemDropped(event, ui) {
                console.log('dropped');
                var $target = $(this).closest("li");    // $(this).closest('li');
                var $item = ui.draggable.closest("ul"); // ui.draggable.closest('li');
                var $srcUL = $item.parent("ul");        // $item.parent('ul');
                var $dstUL = $target;                   // $target.children("ul").first();
                // destination may not have a UL yet
                if ($dstUL.length == 0) {
                    $dstUL = $("<ul></ul>");
                    $target.append($dstUL);
                }
                $item.slideUp(50, function () {
                    $dstUL.append($item);
                    if ($srcUL.children("li").length == 0) {
                        $srcUL.remove();
                    }
                    $item.slideDown(50, function () {
                        $item.css('display', '');
                    });
                });

                var parent_id = $target.find('span').data('cat');
                var target_sub_cat = $target.find('span').data('sub-cat');
                if(target_sub_cat) {
                    parent_id = target_sub_cat;
                }
                var cat_id = $item.find('span').data('cat');
                var source_sub_cat = $item.find('span').data('sub-cat')
                if(source_sub_cat) {
                    cat_id = source_sub_cat;
                }
                var sequence = $($target).children('ul').length + 1;
                console.log('cat_id ' + cat_id + ' | parent_id ' + parent_id + ' | sequence ' + sequence);
                $.ajax({
                    type: 'POST',
                    url: '/menu/save_cat_sub_cat',
                    data: {
                        'cat_id': cat_id,
                        'parent_id': parent_id,
                        'sequence': sequence
                    },
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        alertbox('Error',err,function(modal){
                        });
                    }
                });
            }

            DragAndDrop.enable = function (selector) {
                $(selector).find(".node-cpe").draggable({
                    helper: "clone"
                });
                $(selector).find(".node-cpe, .node-facility").droppable({
                    activeClass: "active",
                    hoverClass: "hover",
                    accept: shouldAcceptDrop,
                    over: itemOver,
                    out: itemOut,
                    drop: itemDropped,
                    greedy: true,
                    tolerance: "pointer"
                });
            };
            return DragAndDrop;

        })(DragAndDrop || {});

        /*(function ($) {
            $.fn.beginEditing = function (whenDone) {
                if (!whenDone) {
                    whenDone = function () {
                    };
                }
                var $node = this;
                var $editor = $("<input type='text' style='width:auto; min-width: 25px;'></input>");
                var currentValue = $node.text();
                function commit() {
                    $editor.remove();
                    $node.text($editor.val());
                    whenDone($node);
                }
                function cancel() {
                    $editor.remove();
                    $node.text(currentValue);
                    whenDone($node);
                }
                $editor.val(currentValue);
                $editor.blur(function () {
                    commit();
                });
                $editor.keydown(function (event) {
                    if (event.which == 27) {
                        cancel();
                        return false;
                    } else if (event.which == 13) {
                        commit();
                        return false;
                    }
                });
                $node.empty();
                $node.append($editor);
                $editor.focus();
                $editor.select();
            };
        })(jQuery);*/

        $(function () {
            DragAndDrop.enable("#sidebar-category-list");
            /*$(document).on("dblclick", "#sidebar-category-list *[class*=node]", function () {
                $(this).beginEditing();
            });*/

        });
    </script>



@endsection
