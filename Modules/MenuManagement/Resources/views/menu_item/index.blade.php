@extends('layouts.app')

@section('content')

    <div class="main__container container__custom">
            <div class="reservation__atto">
                <div class="reservation__title col-md-8 no-col-padding">
                    <h1>Menu Items
                        <!-- <span	style="margin:0px 5px;">({{ $menuItems->currentPage() .'/' . $menuItems->lastPage() }})</span>&emsp; -->
                    </h1>
                </div>
                <!-- @if($brandFlag)
                <div class="col-md-4 no-col-padding">
                    <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}">
                        <option value="">Select Restaurant</option>
                        @foreach($groupRestData as $rest)
                            <optgroup label="{{ $rest['restaurant_name'] }}">
                                @foreach($rest['branches'] as $branch)
                                    <option value="{{ $branch['id'] }}" {{ (isset($searchArr['restaurant_id']) && $searchArr['restaurant_id']==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
                @endif -->
            </div>

             @can('Menu Add')
            <div class="addition__functionality">
                {!! Form::open(['method'=>'get']) !!}
                
                <div class="row functionality__wrapper" style="margin-top:0">
                   
                    <div class="row form-group col-md-4 no-col-padding">
                       <span><a href="{{ URL::to('menu/item/create') }}" class="btn btn__primary">Add</a></span>
                    </div>
                
                </div>
                {!! Form::close() !!}
            </div>
            @endcan 

            <div>
                <div class="active__order__container">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @elseif (session()->has('err_msg'))
                        <div class="alert alert-danger">
                            {{ session()->get('err_msg') }}
                        </div>
                    @endif

                    <div class="guestbook__container box-shadow no-margin-top">
                         
			<table id="table_id" class="responsive ui cell-border hover " style="width:100%; margin-bottom:10px;">
			    <thead>
				<tr>
				    <th>Name</th>
				    <th>Restaurant</th>
				    <th>Category</th>
				   <th>Status</th>
				</tr>
			    </thead>
			   <tbody>
				@if(isset($menuItems) && count($menuItems)>0)
                            		@foreach($menuItems as $menuItem)
				<tr>
				      <td>@if(isset($menuItem->menuItemsLanguage) && count($menuItem->menuItemsLanguage)>0)
                                                @php
                                                    $flag = false
                                                @endphp
                                                @foreach($menuItem->menuItemsLanguage as $menuItemsLanguage)
                                                    @if($menuItemsLanguage->language_id == 1)
                                                        @php
                                                            $flag = true
                                                        @endphp
                                                        {{ $menuItemsLanguage->name }}
                                                    @endif
                                                @endforeach
                                                @if(!$flag)
                                                    {{ $menuItem->name}}
                                                @endif
                                            @else
                                                {{ $menuItem->name}}
                                            @endif
					</td>
				    <td>{{ $menuItem->Restaurant->restaurant_name }}</td>
				    <td>{{ isset($menuItem->MenuCategory)?$menuItem->MenuCategory->name:'' }}</td>
                                    
				    <td data-order="{{$menuItem->status}}">
                                        @can('Menu Status')
					<div class="waitList__otp no-padding" style="float: left; margin-right: 20px;">
                                            
                                                <div class="toggle__container" style="margin-left: 0;">
                                                    @php
                                                        if($menuItem->status){
                                                            $ariaPressed = true;
                                                            $activeClass = 'active';
                                                        } else {
                                                            $ariaPressed = false;
                                                            $activeClass = '';
                                                        }
                                                    @endphp
                                                    <button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom menu_item_active {{ $activeClass }}"
                                                            title="Open" data-toggle="button" data-menu="{{ $menuItem->id }}" aria-pressed="{{ $ariaPressed }}" autocomplete="off">
                                                        <div class="handle"></div>
                                                    </button>
                                                </div>
					</div>
                                        @endcan
					 
                                        <a style="margin-top: 10px;" href="{{ URL::to('menu/item/' . $menuItem->id . '/edit') }}" class="glyphicon glyphicon-edit" ></a>
                                         
                                         
                                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
                                            
                                        <a onclick="return confirm('Are you sure, you want to delete it?')" href="{{ URL::to('menu/item/' . $menuItem->id . '/destroy') }}" class="glyphicon glyphicon-trash"></a>
					 
                                       
                                            
				    </td> 	
				</tr>
			    @endforeach
                        @else
                            <tr>
                                 <tdstyle="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
                                No Record Found
                            </td></tr>
                        @endif
			  </tbody>
			 </table>
                        
                </div>

            </div>
    </div>
    <script>
        $('.menu_item_active').on('click', function() {
            var menu_id = $(this).data('menu');
            var active_flag = 1;
            if($(this).hasClass('active')) {
                active_flag = 0;
            }
            $.ajax({
                type: 'delete',
                url: '/menu/toggleactive/',
                type: 'POST',
                data: {
                    'menu_id': menu_id,
                    'active_flag': active_flag
                },
                success: function (data) {
                    alertbox('Updated',data.message,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            panel.remove();
                        }, 2000);
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                        }, 2000);
                    });
                }
            });
        });
	$(document).ready( function () {
	     $('#table_id').DataTable(  {
			responsive: true,
            dom:'<"top"fiB><"bottom"trlp><"clear">',
            buttons: [
//                {
//                    text: 'Add',
//                    action: function () {location.href = "item/create";}
//                },
                // {extend: 'excelHtml5'}, 
                // {extend: 'pdfHtml5'},   
                // {               
                //     extend: 'print',
                //     exportOptions: {
                //         columns: ':visible'
                //     }
                // },
                // 'colvis'
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye"></i>',
                    titleAttr: 'Column Visibility'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                }, 
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                },
                {               
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    exportOptions: {
                        columns: ':visible',
                    },
                }
            ],
            columnDefs: [ {
                targets: -1,
                visible: false
            }],
            language: {
                search: "",
                searchPlaceholder: "Search"
            },
            responsive: true
		});
        //$('#table_id').show();
        //$('#table_id').removeClass('hidden');
	} );
    </script>
 
@endsection
