@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
    <form method="POST" action="{{ route('item.store') }}" enctype="multipart/form-data">
        <input type="hidden" name="language_id" id="menu_item_store_form_language_id" value="1"/>
    <div class="reservation__atto">
        <div class="reservation__title">
            <h1>{{ __('Menu Item - Add') }}</h1>
        </div>
    </div>
    <div>
        <div class="card form__container newwidth no-margin-top">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
              @include('menumanagement::common.include')

            <div class="card-body">
                <div class="form__user__edit newwidth">
                        @csrf
                        <div class="row">
                            
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12">
                                <label for="restaurant_id"
                                    class="col-form-label text-md-right">{{ __('Restaurant') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="restaurant_id" id="restaurant_id"
                                        class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }} selectpicker"
                                        required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Restaurant</option>
                                        @foreach($groupRestData as $rest)
                                        <optgroup label="{{ $rest['restaurant_name'] }}">
                                            @foreach($rest['branches'] as $branch)
                                            <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                            @endforeach
                                        </optgroup>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('restaurant_id'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 pull-right">
                                <label for="menu_category_id"
                                    class="col-form-label text-md-right">{{ __('Category') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="menu_category_id" id="menu_category_id"
                                        class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}selectpicker"  data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Category</option>
                                        @if(session()->has('categoryData'))
                                        @php
                                        $categoryData = session()->get('categoryData');
                                        @endphp
                                        @foreach($categoryData as $cat)
                                        <option value="{{ $cat->id }}" {{ (old('menu_category_id')==$cat->id) ? 'selected' :'' }}>{{ $cat->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('menu_category_id'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('menu_category_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                                <label for="menu_sub_category_id"
                                    class="col-form-label text-md-right">{{ __('Sub Category') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="menu_sub_category_id" id="menu_sub_category_id"
                                        class="form-control {{ $errors->has('menu_sub_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Sub-Category</option>
                                        @if(session()->has('subCategoryData'))
                                        @php
                                        $subCategoryData = session()->get('subCategoryData');
                                        @endphp
                                        @foreach($subCategoryData as $subCat)
                                        <option value="{{ $subCat->id }}" {{ (old('menu_sub_category_id')==$subCat->id) ? 'selected' :'' }}>{{ $subCat->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('menu_sub_category_id'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('menu_sub_category_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                <label for="product_type"
                                    class="col-form-label text-md-right">{{ __('Product Type') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select required name="product_type" id="product_type"
                                        class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Product Type</option>
                                        @foreach($product_types as $key=>$val)
                                        <option value="{{ $key }}" {{ (old('product_type')==$key) ? 'selected' :'' }}>{{ $val }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('product_type'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                                <label for="name" class="col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="input__field">
                                    <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
                                </div>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                <div class="product_type_product hide">
                                    <label for="gift_wrapping_fees" class="col-form-label text-md-right">{{ __('Gift Wrapping Fees') }}</label>
                                    <div class="">
                                        <div class="input__field">
                                            <input id="gift_wrapping_fees" type="text"
                                                class="{{ $errors->has('gift_wrapping_fees') ? ' is-invalid' : '' }}"
                                                name="gift_wrapping_fees" value="{{ old('gift_wrapping_fees') }}" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="product_type_product hide">
                            <!-- <div class="form-group row form_field__container">
                                <label for="gift_wrapping_fees" class="col-md-4 col-form-label text-md-right">{{ __('Gift Wrapping Fees') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="gift_wrapping_fees" type="text"
                                            class="{{ $errors->has('gift_wrapping_fees') ? ' is-invalid' : '' }}"
                                            name="gift_wrapping_fees" value="{{ old('gift_wrapping_fees') }}" >
                                    </div>
                                </div>
                            </div> -->
                           <!--  <div class="form-group row form_field__container">
                                <label for="gift_message" class="col-md-4 col-form-label text-md-right">{{ __('Gift Message') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <textarea maxlength="500" rows="4" name="gift_message" id="gift_message"
                                            class="form-control{{ $errors->has('gift_message') ? ' is-invalid' : '' }}"
                                            >{{ old('gift_message') }}</textarea>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="form-group row form_field__container">
                            
                        </div>

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-30">
                                <label for="description"
                                    class="col-form-label text-md-right">{{ __('Description') }}</label>
                                <div class="">
                                    <div class="custom__message-textContainer">
                                        <textarea maxlength="500" rows="4" name="description" id="description"
                                            class="editor form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                            >{{ old('description') }}</textarea>
                                    </div>
                                    @if ($errors->has('description'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-30 pull-right">
                                <label for="sub_description"
                                    class="col-form-label text-md-right">{{ __('Sub Description') }}</label>
                                <div class="">
                                    <div class="custom__message-textContainer">
                                        <textarea maxlength="500" rows="4" name="sub_description" id="sub_description"
                                            class="editor form-control{{ $errors->has('sub_description') ? ' is-invalid' : '' }}"
                                            >{{ old('sub_description') }}</textarea>
                                    </div>
                                    @if ($errors->has('sub_description'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('sub_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-30">
                                <div class="row">
                                    <label for="status"
                                        class="col-md-3 col-form-label text-md-right no-padding-left">{{ __('Status') }}</label>
                                    <div class="col-md-7" style="">
                                        <label class="radio-inline"><input type="radio" id="status1" name="status"
                                        value="1" {{ (old('status')=='1') ? 'checked' :'' }}> Active</label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <label class="radio-inline"><input type="radio" id="status0" name="status"
                                        value="0" {{ (old('status')=='0') ? 'checked' :'' }}> Inactive</label>
                                        @if ($errors->has('status'))
                                        <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-30 pull-right">
                                <label for="display_order" class=" col-form-label text-md-right">{{ __('Display Order') }}</label>
                                <div class="">
                                    <div class="input__field">
                                        <input id="display_order" type="text"
                                            class="{{ $errors->has('display_order') ? ' is-invalid' : '' }}"
                                            name="display_order" value="{{ old('display_order') }}" >
                                    </div>
                                    @if ($errors->has('display_order'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('display_order') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-30">
                                <div class="row">
                                    <div class="col-sm-4 col-md-3 col-lg-4 col-xs-12 no-padding-left">
                                        <input id="is_popular" type="checkbox"
                                            class="{{ $errors->has('is_popular') ? ' is-invalid' : '' }}"
                                            name="is_popular" value="1" >
                                        @if ($errors->has('is_popular'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('is_popular') }}</strong>
                                            </span>
                                        @endif
                                        <label for="is_popular" class="col-form-label text-md-right">{{ __('Is Popular') }}</label>
                                    </div>
                                    <div class="col-sm-4 col-md-3 col-lg-4 col-xs-12 no-padding-left">
                                        <input id="is_favourite" type="checkbox"
                                            class="{{ $errors->has('is_favourite') ? ' is-invalid' : '' }}"
                                            name="is_favourite" value="1" >
                                        @if ($errors->has('is_favourite'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('is_favourite') }}</strong>
                                            </span>
                                        @endif
                                        <label for="is_favourite" class="col-form-label text-md-right">{{ __('Is Favourite') }}</label>
                                    </div>
                                    <div class="col-sm-4 col-md-3 col-lg-4 col-xs-12 no-padding-left">
                                        <input id="manage_inventory" type="checkbox"
                                            class="{{ $errors->has('manage_inventory') ? ' is-invalid' : '' }}" {{ count(old())==0 ? '' : (old('manage_inventory') == 1 ? 'checked' : '') }}
                                            name="manage_inventory" value="1" />
                                        @if ($errors->has('manage_inventory'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('manage_inventory') }}</strong>
                                            </span>
                                        @endif
                                        <label for="manage_inventory" class="col-form-label text-md-right">{{ __('Manage Inventory') }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-30 pull-right">
                                <div class="" {{ count(old())==0 ? 'style="display:none;"' : (old('manage_inventory') == 1 ? '' : 'style="display:none;"') }}>
                                    <label for="priority" class="col-form-label text-md-right">{{ __('Maximum Allowed Quantity') }}</label>
                                    <div class="">
                                        <div class="input__field">
                                            <input id="max_allowed_quantity" type="text"
                                                class="{{ $errors->has('max_allowed_quantity') ? ' is-invalid' : '' }}"
                                                name="max_allowed_quantity" value="{{ count(old())==0 ? 0 : old('max_allowed_quantity') }}"
                                                />
                                        </div>
                                        @if ($errors->has('max_allowed_quantity'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('max_allowed_quantity') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                                <div class=" manage_inventory_hide_div" {{ count(old())==0 ? 'style="display:none;"' : (old('manage_inventory') == 1 ? '' : 'style="display:none;"') }}>
                                    <label for="priority" class=" col-form-label text-md-right">{{ __('Inventory') }}</label>
                                    <div class="">
                                        <div class="input__field">
                                            <input id="inventory" type="text"
                                                class="{{ $errors->has('inventory') ? ' is-invalid' : '' }}"
                                                name="inventory" value="{{ count(old())==0 ? 0 : old('inventory') }}"
                                                />
                                        </div>
                                        @if ($errors->has('inventory'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('inventory') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- <div class="form-group row form_field__container">
                        <label for="is_popular" class="col-md-4 col-form-label text-md-right">{{ __('Is Popular') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="is_popular" type="checkbox"
                                       class="{{ $errors->has('is_popular') ? ' is-invalid' : '' }}"
                                       name="is_popular" value="1" >
                            </div>
                            @if ($errors->has('is_popular'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('is_popular') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> -->
                    <!-- <div class="form-group row form_field__container">
                        <label for="is_favourite" class="col-md-4 col-form-label text-md-right">{{ __('Is Favourite') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="is_favourite" type="checkbox"
                                       class="{{ $errors->has('is_favourite') ? ' is-invalid' : '' }}"
                                       name="is_favourite" value="1" >
                            </div>
                            @if ($errors->has('is_favourite'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('is_favourite') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> -->
                    <!-- <div class="form-group row form_field__container">
                        <label for="manage_inventory" class="col-md-4 col-form-label text-md-right">{{ __('Manage Inventory') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="manage_inventory" type="checkbox"
                                       class="{{ $errors->has('manage_inventory') ? ' is-invalid' : '' }}" {{ count(old())==0 ? '' : (old('manage_inventory') == 1 ? 'checked' : '') }}
                                       name="manage_inventory" value="1" />
                            </div>
                            @if ($errors->has('manage_inventory'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('manage_inventory') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> -->

                    <!-- <div class="form-group row form_field__container manage_inventory_hide_div" {{ count(old())==0 ? 'style="display:none;"' : (old('manage_inventory') == 1 ? '' : 'style="display:none;"') }}>
                        <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Inventory') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="inventory" type="text"
                                       class="{{ $errors->has('inventory') ? ' is-invalid' : '' }}"
                                       name="inventory" value="{{ count(old())==0 ? 0 : old('inventory') }}"
                                       />
                            </div>
                            @if ($errors->has('inventory'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('inventory') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> -->
                    <!-- <div class="form-group row form_field__container" {{ count(old())==0 ? 'style="display:none;"' : (old('manage_inventory') == 1 ? '' : 'style="display:none;"') }}>
                        <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Maximum Allowed Quantity') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="max_allowed_quantity" type="text"
                                       class="{{ $errors->has('max_allowed_quantity') ? ' is-invalid' : '' }}"
                                       name="max_allowed_quantity" value="{{ count(old())==0 ? 0 : old('max_allowed_quantity') }}"
                                       />
                            </div>
                            @if ($errors->has('max_allowed_quantity'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('max_allowed_quantity') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                            <div class="" {{ count(old())==0 ? 'style="display:none;"' : (old('manage_inventory') == 1 ? '' : 'style="display:none;"') }}>
                                <label for="priority" class=" col-form-label text-md-right">{{ __('Minimum Allowed Quantity') }}</label>
                                <div class="">
                                    <div class="input__field">
                                        <input id="min_allowed_quantity" type="text"
                                            class="{{ $errors->has('min_allowed_quantity') ? ' is-invalid' : '' }}"
                                            name="min_allowed_quantity" value="{{ count(old())==0 ? 0 : old('min_allowed_quantity') }}"
                                            />
                                    </div>
                                    @if ($errors->has('min_allowed_quantity'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('min_allowed_quantity') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right">
                            <label for="is_tax_applicable" class="col-form-label text-md-right">{{ __('Taxable') }}</label>
                            <div class="selct-picker-plain position-relative">
                                <select name="is_tax_applicable" id="is_tax_applicable"
                                    class="form-control {{ $errors->has('is_tax_applicable') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                    <option value="1" Selected>Yes</option>
                                    <option value="0">No</option>
                                </select>
                                @if ($errors->has('is_tax_applicable'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('is_tax_applicable') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-group row form_field__container" {{ count(old())==0 ? 'style="display:none;"' : (old('manage_inventory') == 1 ? '' : 'style="display:none;"') }}>
                        <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Minimum Allowed Quantity') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="min_allowed_quantity" type="text"
                                       class="{{ $errors->has('min_allowed_quantity') ? ' is-invalid' : '' }}"
                                       name="min_allowed_quantity" value="{{ count(old())==0 ? 0 : old('min_allowed_quantity') }}"
                                       />
                            </div>
                            @if ($errors->has('min_allowed_quantity'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('min_allowed_quantity') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row form_field__container">
                        <label for="is_tax_applicable" class="col-md-4 col-form-label text-md-right">{{ __('Taxable') }}</label>
                        <div class="col-md-4">
                            <select name="is_tax_applicable" id="is_tax_applicable"
                                class="form-control {{ $errors->has('is_tax_applicable') ? ' is-invalid' : '' }}">
                                <option value="1" Selected>Yes</option>
                                <option value="0">No</option>
                            </select>
                            @if ($errors->has('is_tax_applicable'))
                            <span class="invalid-feedback">
                            <strong>{{ $errors->first('is_tax_applicable') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div> -->
                    <!-- Mobile app Image-->
                    <div class="form-group row margin-top-30 source-image-div image-caption-div dblock">
                        <label for="image" class="col-md-12 col-form-label text-md-right">{{ __('Mobile app Image') }}</label>
                        <div class="col-md-12">
                            <div class="image_cont_main clearfix">
                                <div class="col-md-10 no-col-padding">
                                    <input id="image" type="file"
                                        class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                                        value="{{ old('image.0') }}" name="image[]">
                                    @if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                    @endif
                                    <div class="input__field">
                                        <input id="image_caption" type="text" class="{{ $errors->has('image_caption') ? ' is-invalid' : '' }}"
                                            name="image_caption[]" value="{{ old('image_caption.0') }}" placeholder="Image Caption">
                                    </div>
                                    @if ($errors->has('image_caption'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image_caption') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <button type="button" imagetype="image" class="btn btn__primary add_new_image">Add New</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Desktop Web Image-->
                <div class="form-group row margin-top-10 source-image-div image-caption-div dblock">
                    <label for="image" class="col-md-12 col-form-label text-md-right">{{ __('Desktop Web Image') }}</label>
                    <div class="col-md-12">
                        <div class="image_cont_main clearfix">
                            <div class="col-md-10  no-col-padding">
                                <input id="web_image" type="file" class="form-control{{ $errors->has('web_image') ? ' is-invalid' : '' }}"
                                value="{{ old('web_image.0') }}" name="web_image[]">
                                @if ($errors->has('web_image'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('web_image') }}</strong>
                                </span>
                                @endif
                                <div class="input__field">
                                    <input id="web_image_caption" type="text"
                                    class="{{ $errors->has('web_image_caption') ? ' is-invalid' : '' }}"
                                    name="web_image_caption[]" value="{{ old('web_image_caption.0') }}"
                                    placeholder="Image Caption">
                                </div>
                                @if ($errors->has('web_image_caption'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('web_image_caption') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <button type="button" imagetype="web_image" class="btn btn__primary add_new_image">Add New</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Mobile web Image -->
                <div class="form-group row margin-top-10 source-image-div image-caption-div dblock">
                    <label for="mobile_web_image" class="col-md-12 col-form-label text-md-right">{{ __('Image') }}</label>
                    <div class="col-md-12">
                        <div class="image_cont_main clearfix">
                            <div class="col-md-10  no-col-padding">
                                <input id="mobile_web_image" type="file"
                                class="form-control{{ $errors->has('mobile_web_image') ? ' is-invalid' : '' }}"
                                value="{{ old('mobile_web_image.0') }}" name="mobile_web_image[]">
                                @if ($errors->has('mobile_web_image'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('mobile_web_image') }}</strong>
                                </span>
                                @endif
                                <div class="input__field">
                                    <input id="mobile_web_image_caption" type="text" class="{{ $errors->has('mobile_web_image_caption') ? ' is-invalid' : '' }}" name="mobile_web_image_caption[]" value="{{ old('mobile_web_image_caption.0') }}" placeholder="Image Caption">
                                </div>
                                @if ($errors->has('mobile_web_image_caption'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('mobile_web_image_caption') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <button type="button" imagetype="mobile_web_image" class="btn btn__primary add_new_image">Add New</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="size_div">
                @php
                $sizeCount = 0;
                if(session()->has('sizeCount'))
                {
                $sizeCount = session()->get('sizeCount');
                }else {
                $sizeCount = 1;
                }
                @endphp
                @if(isset($sizeCount) && $sizeCount>0)
                @for($i=0;$i<$sizeCount;$i++)
                <div id="size_div_{{$i}}" style="padding:20px 0px 0;border: 1px solid #ccc;margin: 10px 15px;">
                    <div class="form-group row form_field__container">
                        <div class="row" style="width:100%">

                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                <label for="size" class="size col-form-label text-md-right">{{ __('Size') }}</label>
                                <div class="">
                                    <div class="input__field">
                                        <input type="text" class="{{ $errors->has('size.'.$i) ? ' is-invalid' : '' }}" name="size[]" value="{{ old('size.'.$i) }}">
                                    </div>
                                    @if ($errors->has('size.'.$i))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('size.'.$i) }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                <div class="row">
                                    <div class="row">
                                        <label for="price" class="col-md-2 col-form-label text-md-right">{{ __('Price') }}</label>
                                        <div class="col-md-2" style="margin:auto;">
                                            <input type='radio' name='type[{{$i}}]' class='meal_type' data-val='{{$i}}'
                        value="1" {{old("type.".$i)=="1" ? 'checked="checked"' : ''}} >&nbsp; All
                                        </div>
                                        <div class="col-md-2" style="padding:auto;">
                                            <input type='radio' name='type[{{$i}}]' class='meal_type' data-val='{{$i}}' value='0' {{old("type.".$i)=="0" ? 'checked="checked"' : ''}} >&nbsp;By Meal Type
                                        </div>
                                        <div class="col-md-10 margin-top-20">
                                            <div class="input__field">
                                                <input class="price_all" placeholder='Price' type='text' size='4' id='price_all' name='price_all[{{$i}}]' value="{{old("price_all.".$i)}}">
                                            </div>
                                            @if ($errors->has('price_all.'.$i))
                                            <span class="invalid-feedback" style="display: block;">
                                                <strong>{{ $errors->first('price_all.'.$i) }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row margin-top-10">
                                        <!-- @19-07-2018 By RG-->
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12" style="margin:auto;padding-top:auto;text-align: center">
                                            <label class="checkbox-inline"> <input type="checkbox" class="delivery_all" name='delivery_all[{{$i}}]' checked ="checked">Is Delivery</label>
                                        </div>
                                    </div>
                                    <div class="row margin-top-10 hide">
                                        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12" style="margin:auto;padding-top:auto;text-align: center">
                                            <label class="checkbox-inline"><input style="margin-left: -18px" type="checkbox" class="carryout_all"  name='carryout_all[{{$i}}]'>Is Carryout</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                    </div>
                                    @if ($errors->has('type.'.$i))
                                    <div class="col-md-8">
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('type.'.$i) }}</strong>
                                        </span>
                                    </div>
                                    @endif
                                    <div class="row col-md-12" style="padding-top: 10px; {{is_null(old("type.".$i)) || old("type.".$i)=="1"?'display:none;':''}}" id="meal_type_{{$i}}">
                                    @if(session()->has('menuTypeData'))
                                    @php
                                    $menuTypeData = session()->get('menuTypeData');
                                    @endphp
                                    @foreach($menuTypeData as $mtype)
                                    <div class="col-md-2" style="margin:auto;padding-top:auto;clear:both;">
                                        <input type='checkbox' name='meal_type[{{$i}}][{{$mtype->id}}]' value="{{$mtype->id}}" {{old("meal_type.".$i.'.'.$mtype->id)? 'checked="checked"' : ''}} >&nbsp;{{$mtype->name}}
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" placeholder='Price' type='text' size='4' name='price[{{$i}}][{{$mtype->id}}]' value="{{old("price.".$i.'.'.$mtype->id)}}">
                                        @if ($errors->has('price.'.$i.'.'.$mtype->id))
                                        <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('price.'.$i.'.'.$mtype->id) }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <!-- @19-07-2018 By RG-->
                                    <div class="col-md-3 " style="margin:auto;padding-top:auto;">
                                        <label class="checkbox-inline"><input type="checkbox" name="delivery[{{$i}}][{{$mtype->id}}]" checked ="checked">Is Delivery</label>
                                    </div>
                                    <div class="col-md-3 hide" style="margin:auto;padding-top:auto;display:none;">
                                        <label class="checkbox-inline"><input type="checkbox" name="carryout[{{$i}}][{{$mtype->id}}]">Is Carryout</label>
                                    </div><br/>
                                    <!-- Delivery / carryout Enable or not-->
                                    @endforeach
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                        <!-- <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
                        <div class="row col-md-8">
                            <div class="row">
                                <div class="col-md-2" style="margin:auto;">
                                    <input type='radio' name='type[{{$i}}]' class='meal_type' data-val='{{$i}}'
                value="1" {{old("type.".$i)=="1" ? 'checked="checked"' : ''}} >&nbsp; All
                                </div>
                                <div class="col-md-10">
                                    <div class="input__field">
                                        <input class="price_all" placeholder='Price' type='text' size='4' id='price_all' name='price_all[{{$i}}]' value="{{old("price_all.".$i)}}">
                                    </div>
                                    @if ($errors->has('price_all.'.$i))
                                    <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('price_all.'.$i) }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row"><br>
                                <div class="col-md-6" style="margin:auto;padding-top:auto;text-align: center">
                                    <label class="checkbox-inline"> <input type="checkbox" class="delivery_all" name='delivery_all[{{$i}}]'>Is Delivery</label>
                                </div><br><br>
                                <div class="col-md-6" style="margin:auto;padding-top:auto;text-align: center">
                                    <label class="checkbox-inline"><input style="margin-left: -18px" type="checkbox" class="carryout_all"  name='carryout_all[{{$i}}]'>Is Carryout</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8" style="padding:auto;margin-top:10px;">
                                    <input type='radio' name='type[{{$i}}]' class='meal_type' data-val='{{$i}}' value='0' {{old("type.".$i)=="0" ? 'checked="checked"' : ''}} >&nbsp;By Meal Type
                                </div>
                            </div>
                            @if ($errors->has('type.'.$i))
                            <div class="col-md-8">
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('type.'.$i) }}</strong>
                                </span>
                            </div>
                            @endif
                            <div class="row col-md-12" style="padding-top: 10px; {{is_null(old("type.".$i)) || old("type.".$i)=="1"?'display:none;':''}}" id="meal_type_{{$i}}">
                            @if(session()->has('menuTypeData'))
                            @php
                            $menuTypeData = session()->get('menuTypeData');
                            @endphp
                            @foreach($menuTypeData as $mtype)
                            <div class="col-md-2" style="margin:auto;padding-top:auto;clear:both;">
                                <input type='checkbox' name='meal_type[{{$i}}][{{$mtype->id}}]' value="{{$mtype->id}}" {{old("meal_type.".$i.'.'.$mtype->id)? 'checked="checked"' : ''}} >&nbsp;{{$mtype->name}}
                            </div>
                            <div class="col-md-4">
                                <input class="form-control" placeholder='Price' type='text' size='4' name='price[{{$i}}][{{$mtype->id}}]' value="{{old("price.".$i.'.'.$mtype->id)}}">
                                @if ($errors->has('price.'.$i.'.'.$mtype->id))
                                <span class="invalid-feedback" style="display: block;">
                                <strong>{{ $errors->first('price.'.$i.'.'.$mtype->id) }}</strong>
                                </span>
                                @endif
                            </div>
                            
                            <div class="col-md-3" style="margin:auto;padding-top:auto;">
                                <label class="checkbox-inline"><input type="checkbox" name="delivery[{{$i}}][{{$mtype->id}}]">Is Delivery</label>
                            </div>
                            <div class="col-md-3" style="margin:auto;padding-top:auto;">
                                <label class="checkbox-inline"><input type="checkbox" name="carryout[{{$i}}][{{$mtype->id}}]">Is Carryout</label>
                            </div><br/>
                            
                            @endforeach
                            @endif
                        </div> -->
                    </div>
                </div>
                @endfor
                @else
                <div id="size_div_0" style="padding:20px 0px;border: 1px solid #ccc;margin: 10px 15px;">
                <div class="form-group row form_field__container">
                <label for="size"
                    class="col-md-4 col-form-label text-md-right">{{ __('Size') }}</label>
                <div class="col-md-4">
                <div class="input__field">
                <input type="text"
                    class="size {{ $errors->has('size') ? ' is-invalid' : '' }}"
                    name="size[]" value="{{ old('size') }}">
                </div>
                @if ($errors->has('size'))
                <span class="invalid-feedback">
                <strong>{{ $errors->first('size') }}</strong>
                </span>
                @endif
                </div>
                </div>
                <div class="form-group row form_field__container">
                <label for="meal type"
                    class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
                <div class="row col-md-8">
                <div class="col-md-2" style="margin:auto;">
                <input type='radio' name='type[0]' class='meal_type' data-val='0'
                value="1" {{old("type")=="1" ? 'checked="checked"' : ''}} >&nbsp;All
                </div>
                <div class="col-md-8">
                <input class="form-control price_all" placeholder='Price'
                type='text' size='4' id='price_all' name='price_all[]'
                value="{{old("price_all")}}" style="width: 25%">
                @if ($errors->has('price_all'))
                <span class="invalid-feedback" style="display: block;">
                <strong>{{ $errors->first('price_all') }}</strong>
                </span>
                @endif
                </div>
                <!-- @19-07-2018 By RG-->
                <div class="col-md-3" style="margin:auto;padding-top:auto;">
                <label class="checkbox-inline"><input type="checkbox"
                class="delivery_all"
                name='delivery_all[]'
                value="{{old("delivery_all")}}" checked ="checked">Is
                Delivery</label>
                </div>
                <div class="col-md-3 hide" style="margin:auto;padding-top:auto;">
                <label class="checkbox-inline"><input type="checkbox"
                class="carryout_all"
                name='carryout_all[]'
                value="{{old("carryout_all")}}">Is
                Carryout</label>
                </div>
                <div class="col-md-12" style="padding:auto;margin-top:10px;">
                <input type='radio' name='type[0]' class='meal_type' data-val='0'
                value='0' {{old("type")=="0" ? 'checked="checked"' : ''}} >&nbsp;By
                Meal Type
                </div>
                @if ($errors->has('type'))
                <div class="col-md-8">
                <span class="invalid-feedback" style="display: block;">
                <strong>{{ $errors->first('type') }}</strong>
                </span>
                </div>
                @endif
                <div class="row col-md-12"
                style="padding-top: 10px; {{is_null(old("type")) || old("type")=="1"?'display:none;':''}}"
                id="meal_type_0">
                @if(session()->has('menuTypeData'))
                @php
                $menuTypeData = session()->get('menuTypeData');
                @endphp
                @foreach($menuTypeData as $mtype)
                <div class="col-md-4" style="margin:auto;padding-top:auto;">
                <input type='checkbox'
                name='meal_type[0][{{$mtype->id}}]'
                value="{{$mtype->id}}" {{old("meal_type.".$mtype->id)? 'checked="checked"' : ''}} >&nbsp;{{$mtype->name}}
                @if ($errors->has('meal_type.'.$i.'.'.$mtype->id))
                <span class="invalid-feedback"
                    style="display: block;">
                <strong>{{ $errors->first('meal_type.'.$i.'.'.$mtype->id) }}</strong>
                </span>
                @endif
                </div>
                <div class="col-md-6"><input class="form-control"
                    placeholder='Price' type='text'
                    size='4'
                    name='price[0][{{$mtype->id}}]'
                    value="{{old("price.".$mtype->id)}}"
                style="width: 50%">
                @if ($errors->has('price.'.$mtype->id))
                <span class="invalid-feedback"
                    style="display: block;">
                <strong>{{ $errors->first('price.'.$mtype->id) }}</strong>
                </span>
                @endif
                </div>
                @endforeach
                @endif
                @if ($errors->has('meal_type'))
                <span class="invalid-feedback" style="display: block;">
                <strong>{{ $errors->first('meal_type') }}</strong>
                </span>
                @endif
                </div>
                </div>
                </div>
            </div>
            @endif
        </div>
        <div style="text-align: right;margin-right: 0%; padding:0 15px;display:none;">
            <input type="button" value="Add" class="btn btn__primary" onclick="add_size_div()">&ensp;
            <input type="button" value="Delete" class="btn btn__cancel" onclick="remove_size_div()">
        </div>
        <div id="menu_settings_data_div"></div>
        <div style="margin-top: 50px;" class="row mb-0">
        <div class="col-md-12 text-center">
        <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
        </div>
        </div>       
    </div>
</div>
</div>
</div>
 </form>
</div>
<script type="text/javascript">
    $("#product_type").change(function () {
           var type = $(this).val();
           if(type=='product'){
            $('.product_type_product').removeClass('hide');
    
           }else{
             $('.product_type_product').addClass('hide');
    
           }
    });            
        var size_div_cnt = 0;
        size_div_cnt = '<?php echo (isset($sizeCount) && $sizeCount > 0) ? ($sizeCount - 1) : 0 ?>';
        size_div_cnt = parseInt(size_div_cnt);
    
        function add_size_div() {
            var restId = $('#restaurant_id :selected').val();
            if (restId == '') {
                alert('Please select Restaurant');
                return;
            }
            var div_html = $('#size_div_0').html();
            size_div_cnt = size_div_cnt + 1;
            $('#size_div').append("<div id='size_div_" + size_div_cnt + "' style='border: 1px solid #ccc; padding:20px 0 0; margin: 10px 15px;'></div>");
            var newIndex = '[' + size_div_cnt + ']';
            var newDataVal = "data-val='" + size_div_cnt + "'";
            div_html = div_html.replace(/\[0\]/g, newIndex);
            div_html = div_html.replace(/data-val=\"0\"/g, newDataVal);
            div_html = div_html.replace('meal_type_0', 'meal_type_' + size_div_cnt);
            $('#size_div_' + size_div_cnt).html(div_html);
            $('#size_div_' + size_div_cnt).find('.price_all').val('');
            $('#size_div_' + size_div_cnt).find('.size').val('');
            $('#size_div_' + size_div_cnt).find('.meal_type').prop('checked', false);
            $('#meal_type_' + size_div_cnt).hide();
        }
    
        function remove_size_div() {
            if (size_div_cnt > 0) {
                $('#size_div_' + size_div_cnt).remove();
                size_div_cnt = size_div_cnt - 1;
            }
        }
    
        function remove_all_size_div() {
            for (i = 1; i <= size_div_cnt; i++) {
                $('#size_div_' + i).remove();
            }
            $('#meal_type_0').html('');
            $('#size_div_0').find('.price_all').val('');
            $('#size_div_0').find('.meal_type').prop('checked', false);
            $('#meal_type_0').hide();
        }
    
        $(function () {
            $('body').on('click', '.meal_type', function () {
                var mealTypeVal = $(this).val();
                var idx = $(this).attr('data-val');
                if (mealTypeVal == '1') {
                    $('#meal_type_' + idx).find('input[type=checkbox]:checked').removeAttr('checked');
                    $('#meal_type_' + idx).find('input[type=text]').val('');
                    $('#meal_type_' + idx).hide();
                }
                else {
                    $('#size_div_' + idx).find('.price_all').val('');
                    $('#meal_type_' + idx).show();
                }
            });
    
            /*$('input[name=type]').click(function(){
                var typeVal = $('input[name=type]:checked').val();
                if(typeVal=='1') {
                    $('#meal_type').find('input[type=checkbox]:checked').removeAttr('checked');
                    $('#meal_type').find('input[type=text]').val('');
                    $('#meal_type').hide();
                }
                else {
                    $('#price_all').val('');
                    $('#meal_type').show();
                }
            });*/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    
            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $('option:selected').val();
                $('#menu_category_id').find('option').not(':first').remove();
                if (rest_id != '') {
                    remove_all_size_div();
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_category_and_type"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj.cat) !== 'undefined') {
                                    $.each(data.dataObj.cat, function (i, item) {
                                        $('#menu_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    });
                                }
                                if ($.type(data.dataObj.type) !== 'undefined') {
                                    var fielsStr = '';
                                    $.each(data.dataObj.type, function (i, item) {
                                        fielsStr = fielsStr + "<div class=\"col-md-2\" style=\"margin:auto;padding-top:auto;clear:both;\"><input type='checkbox' style=\"margin:auto;\" name='meal_type[0][" + item.id + "]' value='" + item.id + "'>&nbsp;" + item.name + "</div><div class=\"col-md-4\"><input placeholder='Price' type='text' size='4' class=\"form-control\" name='price[0][" + item.id + "]'></div>";
                                        //@ RG 19-07-2018
                                        fielsStr += '<div class="col-md-5" style="margin:auto;padding-top:auto;"><label class="checkbox-inline"><input type="checkbox" name=delivery[0][' + item.id + ']  checked ="checked" >Delivery</label></div><div class="col-md-3 hide" style="margin:auto;padding-top:auto;"><label class="checkbox-inline"><input type="checkbox" name="carryout[0][' + item.id + ']"  >Carryout</label></div>';
                                    });
                                    $("#meal_type_0").html(fielsStr);
                                }
                            }
                        }
                    });
                }
            });
            $("#menu_category_id").change(function () {
                var rest_id = $('#restaurant_id').val();
                var cat_id = $(this).val();
                console.log(rest_id + ' - ' + cat_id);
                $('#menu_sub_category_id').find('option').not(':first').remove();
                if (rest_id != '' && cat_id != '') {
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_customization_options"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id + '&cid=' + cat_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj.menu_settings_data) !== 'undefined') {
                                    $('#menu_settings_data_div').html('');
    
                                    // set sub categories
                                    $.each(data.dataObj.sub_categories, function (i, item) {
                                       // $('#menu_sub_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                        $('#menu_sub_category_id').html(data.dataObj.subcat_options);
                                    });
    
                                    // set customization settings
                                    console.log(data.dataObj.menu_settings_data);
                                    var setHtml = '';
    
                                    $.each(data.dataObj.menu_settings_data, function (i, cat) {
                                        var catHtml = '<div class="form-group row form_field__container" style="padding: 20px;">'
                                            + '<label for="image" class="col-md-4 col-form-label text-md-right">' + cat.name + '</label>'
                                            + '<div class="col-md-6">'
                                            + '<table class="table-bordered">'
                                            + '<tr><td><input type="checkbox" name="' + cat.id + '_is_mandatory" value="1" >Is mandatory</td>'
                                            + '<td></td><td></td><td></td></tr>';
                                        $.each(cat.items, function (j, item) {
                                            var sideHtml = '';
                                            $.each(data.dataObj.hri_sides, function (i, side) {
                                                console.log(side);
                                                var sdHtml = '<input type="checkbox" name="' + item.id + '_' + side + '"  value="' + side + '" >' + side + '<br>';
                                                sideHtml += sdHtml;
                                            });
                                            var quantHtml = '';
                                            $.each(data.dataObj.hri_quantity, function (i, quant) {
                                                quantHtml += '<input type="checkbox" name="' + item.id + '_' + quant + '"  value="' + quant + '" >' + quant + '<br>';
                                            });
                                            var itemHtml = '<tr>'
                                                + '<td><input type="checkbox" name="' + cat.name + '[]" value="' + item.id + '" >' + item.name + '</td>'
                                                + '<td>' + sideHtml + '</td>'
                                                + '<td>' + quantHtml + '</td>'
                                                + '<td><input type="checkbox" name="' + item.id + '_default_selected" value="1" >Default Selected</td>'
                                                + '</tr>';
                                            catHtml += itemHtml;
                                        });
                                        catHtml += '</table></div></div>';
                                        setHtml += catHtml;
                                    });
                                    $('#menu_settings_data_div').html(setHtml);
    
                                }
                                if ($.type(data.dataObj.type) !== 'undefined') {
                                    var fielsStr = '';
                                    $.each(data.dataObj.type, function (i, item) {
                                        fielsStr = fielsStr + "<div class=\"col-md-4\" style=\"margin:auto;padding-top:auto;\"><input type='checkbox' style=\"margin:auto;\" name='meal_type[0][" + item.id + "]' value='" + item.id + "'>&nbsp;" + item.name + "</div><div class=\"col-md-6\"><input placeholder='Price' type='text' size='4' class=\"form-control\" name='price[0][" + item.id + "]' style=\"width: 50%\"></div>";
                                        alert('cas323');
    
                                        fielsStr += '<div class="col-md-5" style="margin:auto;padding-top:auto;"><label class="checkbox-inline"><input selected="selected" type="checkbox" name=delivery[0]"' + item.id + '">Is Delivery</label></div><div class="col-md-3 hide" style="margin:auto;padding-top:auto;"><label class="checkbox-inline"><input style="margin-left: -18px; type="checkbox" name="carryout[0]"' + item.id + '   >Is Carryout</label></div>';
                                    });
                                    console.log('Order Logs', fielsStr);
                                    $("#meal_type_0").html(fielsStr);
                                }
                            }
                        }
                    });
                }
            });
            var check_restaurant = '<?php echo $selected_rest_id;?>';
            if (check_restaurant != 0) {
                // trigger restautaurnat checks
                $("#restaurant_id").val(check_restaurant).trigger('change');
                //$("#restaurant_id").attr('change');
            }
        });
    
    
    
    $('.add_new_image').click(function(){
    
    var type=$.trim($(this).attr('imagetype'));
    var filename=type+'[]';
    var lastsib= $(this).parents('.image-caption-div').find(".image_cont_main:last");
    var str='<div class="image_cont_main mt-20 clearfix"> <div class="col-md-10 no-col-padding"> <input id="'+type+'" type="file" class="form-control" name="'+filename+'"> <div class="input__field"> <input id="image_caption" type="text" name="'+type+'_caption[]" placeholder="Image Caption"> </div> </div> <div class="col-md-2"><input type="button" value="Delete" class="btn btn__cancel deleteImage" > </div> </div>';
     lastsib.after(str);
    })
    $(document).on('click','.deleteImage',function(){
    
    
    $(this).parents('.image_cont_main').remove();
    
    })
</script>
<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
        tinymce.init({
            selector: 'textarea.editor',
            menubar:false,
            height: 320,
            theme: 'modern',
            plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });
    });
</script>

@endsection
