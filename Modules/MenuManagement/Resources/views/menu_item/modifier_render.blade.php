
 <?php 
                function modifierCategoriesDropdown($ModifierCategories,$default='',$modifiergroup_id=NULL,$item_index){
                     
                      $catname=$modifiergroup_id!=NULL?"modifiergroup[$modifiergroup_id][modifier_items][".$item_index."][modifier_category]":'modifiercat_';
                      $select_str='<select name="'.$catname.'" class="form-control" required>';
                      foreach($ModifierCategories as $cat){
                        if($cat->id==$default){
                           $select_str.=' <option  selected  value="'.$cat->id.'">'.$cat->category_name.'</option>'; 
                        }else{
                            $select_str.=' <option   value="'.$cat->id.'">'.$cat->category_name.'</option>'; 
                        }
                      }  
                      
                      $select_str.='</select>';
                      return  $select_str;
                }
                //echo $categories;
                ?>
                <div class="row rowpb">
                    <div class="col-md-6"> <h3>Modifiers</h3></div>
                    <div class="col-md-6" style="text-align: right"><a href="#" class="btn btn__primary" data-toggle="modal" data-target="#modifiergroup">Add New Modifier group</a></div>
                </div>

               <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <form method="POST" id="itemodifiergroup_form" action="{{ '/menu/item/modifiergroup/'.$item_id . '/' . $languageId }}" enctype="multipart/form-data">
                        @csrf
                      @if(isset($ItemModifierGroup) && count($ItemModifierGroup))
                                @php     $groupcount=0; @endphp
                      @foreach($ItemModifierGroup as $modifiergroup)
                     

                  <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading{{$modifiergroup->id}}">
                       <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$modifiergroup->id}}" aria-expanded="false" aria-controls="collapse{{$modifiergroup->id}}">
                        @php
                            $modifierGroupName = $modifiergroup->group_name;
                            $modifierPrompt = $modifiergroup->prompt;
                            $modifierPosId = $modifiergroup->pos_id;
			   $dependent_modifier_group_id =$modifiergroup->dependent_modifier_group_id;
				$description = $modifiergroup->description;
                            //$modifierCalorie = $modifiergroup->calorie;
                        @endphp
                        
                        @if(isset($modifiergroup->modifierGroupLangauges) && count($modifiergroup->modifierGroupLangauges)>0)
                            @php
                                $flag = false
                            @endphp
                            @foreach($modifiergroup->modifierGroupLangauges as $modifierGroupLangauge)
                                @if($modifierGroupLangauge->language_id == $languageId)
                                    @php
                                        $flag = true;
                                        $modifierGroupName = $modifierGroupLangauge->group_name;
                                        $modifierPrompt = $modifierGroupLangauge->prompt;
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                        {{$modifierGroupName}}
                        </a> 
                        <span class="delete_edit">
                            <i class="fa fa-trash-o delete_modifier_group" aria-hidden="true" modifiergroup="{{$modifiergroup->id}}" ></i>
<!--                                 <i class="fa fa-pencil" aria-hidden="true"></i>
 -->                    </span>  
                        
                      </h4>

                        </div>
                        <div id="collapse{{$modifiergroup->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$modifiergroup->id}}" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div class="form-group row form_field__container">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Id') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                             {{$modifiergroup->id}}
                                        </div>
                                        @if ($errors->has('group_name'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('group_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row form_field__container">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <input type="text"
                                                   class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
                                                   name="modifiergroup[{{$modifiergroup->id}}][group_name]" value="{{$modifierGroupName}}" required>
                                        </div>
                                        @if ($errors->has('group_name'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('group_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row form_field__container">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Prompt') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <input type="text"
                                                   class="{{ $errors->has('prompt') ? ' is-invalid' : '' }}"
                                                   name="modifiergroup[{{$modifiergroup->id}}][prompt]" value="{{ $modifierPrompt }}" required>
                                        </div>
                                        @if ($errors->has('prompt'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('prompt') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                               <div class="form-group row form_field__container">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <textarea rows="5" cols="100" name="modifiergroup[{{$modifiergroup->id}}][description]" >{{$description}}</textarea>
                                        </div>
                                        @if ($errors->has('group_name'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('group_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row form_field__container">
                                    <label for="pos_id" class="col-md-4 col-form-label text-md-right">{{ __('Dependent Modifier Group Id') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <input type="text" class="{{ $errors->has('dependent_modifier_group_id') ? ' is-invalid' : '' }}" name="modifiergroup[{{$modifiergroup->id}}][dependent_modifier_group_id]" value="{{ $dependent_modifier_group_id }}" >
                                        </div>
                                        @if ($errors->has('dependent_modifier_group_id'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('dependent_modifier_group_id') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                {{--<div class="form-group row form_field__container">
                                    <label for="calorie" class="col-md-4 col-form-label text-md-right">{{ __('Calorie') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <input type="text" class="{{ $errors->has('calorie') ? ' is-invalid' : '' }}" name="modifiergroup[{{$modifiergroup->id}}][calorie]" value="{{ $modifierCalorie }}" >
                                        </div>
                                        @if ($errors->has('calorie'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('calorie') }}</strong></span>
                                        @endif
                                    </div>
                                </div>--}}

                                        <div class="form-group row form_field__container">
                                                   @php //print_r($modifiergroup); @endphp
                                                  <label
                                                         class="col-md-4 col-form-label text-md-right">{{ __('Quantity Type') }}</label>
                                                  <div class="col-md-3 quantity_type_select selct-picker-plain position-relative">
                                                      <select name=" modifiergroup[{{$modifiergroup->id}}][quantity_type]" 
                                                              class="quantity_type form-control{{ $errors->has('quantity_type') ? ' is-invalid' : '' }} selectpicker"
                                                              required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                          <option value="Value" @if($modifiergroup->quantity_type=='Value') selected @endif>Value</option>
                                                          <option value="Range" @if($modifiergroup->quantity_type=='Range') selected @endif>Range</option>
                                                        
                                                      </select>
                                                    
                                                      @if ($errors->has('quantity_type'))
                                                          <span class="invalid-feedback">
                                                          <strong>{{ $errors->first('quantity_type') }}</strong>
                                                      </span>
                                                      @endif
                                                  </div>
                                                   <div class="col-md-2 quantity_type_val">                                  
                                                       <div class="value_quantity1">
                                                            <div class="input__field">
                                                              <input id="quantity" type="text"
                                                                     class="{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
                                                                     name="modifiergroup[{{$modifiergroup->id}}][quantity]" value="{{ $modifiergroup->quantity}}" required placeholder="Min,Max">
                                                             </div>                                        
                                                    </div>                                    
                                                  </div>
                                              </div>
                                                  <div class="form-group row form_field__container">
                                              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Is this required?') }}</label>
                                              <div class="col-md-4" style="padding-top: 10px;">
                                                  <label class="radio-inline"><input type="radio"  name="modifiergroup[{{$modifiergroup->id}}][is_required]" value="1" @if($modifiergroup->is_required==1) checked  @endif > Required</label>
                                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                                  <label class="radio-inline"><input type="radio"  name="modifiergroup[{{$modifiergroup->id}}][is_required]" value="0" @if($modifiergroup->is_required==0) checked @endif> Optional</label>
                                                  @if ($errors->has('is_required'))
                                                  <span class="invalid-feedback" style="display:block;">
                                                  <strong>{{ $errors->first('is_required') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>
                                <div class="form-group row form_field__container hide">
                                    <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Show as dropdown') }}</label>
                                    <div class="col-md-4" style="padding-top: 10px;">
                                        <label class="radio-inline"><input type="radio"  name="modifiergroup[{{$modifiergroup->id}}][show_as_dropdown]" value="1" @if($modifiergroup->show_as_dropdown==1) checked  @endif > Yes</label>
                                        &nbsp;&nbsp;
                                            <label class="radio-inline"><input type="radio"  name="modifiergroup[{{$modifiergroup->id}}][show_as_dropdown]" value="0" @if($modifiergroup->show_as_dropdown==0) checked @endif> No</label>
                                        @if ($errors->has('show_as_dropdown'))
                                            <span class="invalid-feedback" style="display:block;">
                                                  <strong>{{ $errors->first('show_as_dropdown') }}</strong>
                                                  </span>
                                        @endif
                                    </div>
                                </div>
				<div class="form-group row form_field__container">
                                    <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                                    <div class="col-md-4" style="padding-top: 10px;">
                                        <label class="radio-inline"><input type="radio"  name="modifiergroup[{{$modifiergroup->id}}][status]" value="1" @if($modifiergroup->status==1) checked  @endif > Active</label>
                                        &nbsp;&nbsp;
                                            <label class="radio-inline"><input type="radio"  name="modifiergroup[{{$modifiergroup->id}}][status]" value="0" @if($modifiergroup->status==0) checked @endif> InActive</label>
                                        @if ($errors->has('status'))
                                            <span class="invalid-feedback" style="display:block;">
                                                  <strong>{{ $errors->first('status') }}</strong>
                                                  </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row form_field__container">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Sort Order') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <input type="number"
                                                   class="{{ $errors->has('sort_order') ? ' is-invalid' : '' }}"
                                                   name="modifiergroup[{{$modifiergroup->id}}][sort_order]" value="{{ $modifiergroup->sort_order }}" required>
                                        </div>
                                        @if ($errors->has('sort_order'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('sort_order') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                          
                                           @if(count($modifiergroup->itemmodifiers))
                                            <!-- table table-striped modifer_group_list -->
                                            <table class="myTable2 responsive ui cell-border hover modifer_group_list" style="margin-bottom:10px;" width="100%">
                                              <thead>
                                                <tr>
                                                    <th>Modifier Id</th>
                                                    <th>Modifier Name</th>
                                                    <th>Size</th>
                                                    <th>Descriptions</th>
                                                    <th>Price</th>
                                                    {{--<th>Special Price</th>
                                                    <th>Start date</th>
                                                    <th>End date</th>--}}
                                                    <th>Dependent Modifier Item Id</th>
                                                    <th>Sort Order</th>
                                                    <th>Is selected</th>
						    <th>Status</th>	
                                                    <th>Action</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @php $item_index=0; @endphp
                                                 @foreach($modifiergroup->itemmodifiers as $groupitem_modifier)
                                                 
                                                 @php
                                                    $modifierName = $groupitem_modifier->modifier_name;
                                                @endphp

                                                @if(isset($groupitem_modifier->itemModifierLanguage) && count($groupitem_modifier->itemModifierLanguage)>0)
                                                    @php
                                                        $flag = false;
                                                    @endphp
                                                    @foreach($groupitem_modifier->itemModifierLanguage as $itemModifierLanguage)
                                                        @if($itemModifierLanguage->language_id == $languageId)
                                                            @php
                                                                $flag = true;
                                                                $modifierName = $itemModifierLanguage->modifier_name;
                                                            @endphp
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <tr>
                                                  <td>
                                                      <div class="form-group">
                                                        @php  //  modifierCategoriesDropdown($ModifierCategories,$groupitem_modifier->modifier_category_id,$modifiergroup->id,$item_index); @endphp
                                                        <input type="text" class="form-control"  readonly name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][modifier_id]" value="{{$groupitem_modifier->id}}">
							
                                                      </div>

                                                  </td>
                                                  <td><div class="form-group input__field"><input type="text" class="form-control" name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][modifier_name]" placeholder="Modifier Name" value="{{$modifierName}}"></div>
                                                  </td>

                                                    <td>
                                                        <div class="form-group selct-picker-plain position-relative" style="width:82%">
                                                            <select name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][modifier_size]"class="form-control selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                                <option value="">Choose Size</option>
                                                                <?php
                                                                    if(!empty($menuItem->size_price)){
                                                                        $allsizes=json_decode($menuItem->size_price,true);
                                                                      foreach($allsizes as $size){
                                                                      if(!empty($size['size'])){
                                                                          $selected='';
                                                                          if($size['size']==$groupitem_modifier->size){
                                                                              $selected="selected='selected'";
                                                                          }
                                                                        echo   $option= "<option  ".$selected." value='".addslashes($size['size'])."'>".$size['size']."</option>";
                                                                          $modifer_size.=$option;
                                                                      }
                                                                        }
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
						    <td><div class="form-group input__field"><input type="text" class="form-control"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][description]" placeholder="Description" value="{{$groupitem_modifier->description}}"></div></td>	
                                                    {{--<td><div class="form-group input__field"><input type="text" class="form-control"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][calorie]" placeholder="Calorie" value="{{$groupitem_modifier->calorie}}"></div></td> --}}

                                                    <td><div class="form-group input__field"><input type="text" class="form-control"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][modifier_price]" placeholder="Price" value="{{$groupitem_modifier->price}}"></div></td>

                                                    {{--<td><div class="form-group input__field"><input type="text" class="form-control"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][modifier_special_price]" placeholder="Special Price" value="{{$groupitem_modifier->special_price}}"></div></td>
                                                    <td><div class="form-group input__field"><input type="text" class="form-control datetimepicker"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][modifier_start_date]" placeholder="Start date" value="{{$groupitem_modifier->start_date}}"></div></td>
                                                    <td><div class="form-group input__field"><input type="text" class="form-control datetimepicker"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][modifier_end_date]" placeholder="End date" value="{{$groupitem_modifier->end_date}}"></div></td>--}}

                                                    <td><div class="form-group input__field"><input type="text" class="form-control"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][dependent_modifier_item_id]" placeholder="Dependent Modifier Item Id" value="{{$groupitem_modifier->dependent_modifier_item_id}}"></div></td>

                                                    <td><div class="form-group input__field"><input type="text" class="form-control"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][sort_order]" placeholder="Sort Order" value="{{$groupitem_modifier->sort_order}}"></div></td>

                                                    <td><div class="form-group"><input type="checkbox" class="form-control"  name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][is_selected]"  value="1"  {{$groupitem_modifier->is_selected==1?'checked':'notselected
                                                    '}}></div></td>

							                        <td>
                                                        <div class="form-group selct-picker-plain position-relative" style="width:82%">
                                                            <select name="modifiergroup[{{$modifiergroup->id}}][modifier_items][{{$item_index}}][status]"class="form-control selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                                                <option value="">Choose Status</option>
                                                                <option value='1' {{$groupitem_modifier->status==1?'Selected':''}}> Active</option>
 								<option value='0'  {{$groupitem_modifier->status==0?'Selected':''}} >InActive</option>
                                                            </select>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <i class="fas fa-times delete_modifier_item" modifieritem="{{$groupitem_modifier->id}}" ></i> 
                                                        &nbsp;&nbsp;<a onclick="setModifierItemId('{{$groupitem_modifier->id}}')" href="#" data-toggle="modal" data-target="#addmodifieroptions1" >Options</a>
  							<!--a href="#" modifieritem="{{$groupitem_modifier->id}}" class="get_modifier_item">Options</a> -->
                                                  </td>
                                                </tr>
                                                  @php  $item_index++; @endphp
                                                @endforeach
                                               
                                              </tbody>
                                            </table>
                                            @endif
                                            <a href="javascript:void(0)" class="add_modifier" modifiergroup="{{$modifiergroup->id}}">+add modifier</a>
                                          <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addmodifieroptions">Add Modifier</button> -->

                          </div>
                        </div>
                    </div>
                      
                      @php 
                      $groupcount++
                      @endphp
                       @endforeach
                       <div class="row"><button type="submit" class="btn btn__primary pull-right submitbtntopmargin" >Save</button> </div>
                      @else
                      <div class="text-center">   No Modifiers found</div>
                      @endif
                          
                    </form>

              </div>
       <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />

       <script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>

<script>
    var commonOptions = {
        i18n:{
            en: {
                dayOfWeekShort: [
                    "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"
                ]
            }
        },
        format: 'Y-m-d H:i:s',
        formatTime: 'H:i:s'
    };
    $(function() {
        $('.datetimepicker').datetimepicker(commonOptions);
    });
</script>
<script>
 $(document).ready(function() {
    $('.myTable2').DataTable( {
        dom:'<"clear">',
        //dom:'<"top"fiB><"bottom"trlp><"clear">',
        "order": [[2, "desc" ]],
        // buttons: [
        //     {
        //         extend: 'colvis',
        //         text: '<i class="fa fa-eye"></i>',
        //         titleAttr: 'Column Visibility'
        //     },
        //     {
        //         extend: 'excelHtml5',
        //         text: '<i class="fa fa-file-excel-o"></i>',
        //         titleAttr: 'Excel'
        //     }, 
        //     {
        //         extend: 'pdfHtml5',
        //         text: '<i class="fa fa-file-pdf-o"></i>',
        //         titleAttr: 'PDF'
        //     },
        //     {               
        //         extend: 'print',
        //         text: '<i class="fa fa-print"></i>',
        //         titleAttr: 'Print',
        //         exportOptions: {
        //             columns: ':visible',
        //         },
        //     }
        // ],
        // columnDefs: [{
        //     targets: -1,
        //     visible: false
        // }],
        // language: {
        //     search: "",
        //     searchPlaceholder: "Name, Order id, Phone number"
        // },
        responsive: true
    });
    // $( ".collapsed" ).accordion({
    //     alert("hello");
    //     activate: function () {
    //         $( $.fn.dataTable.tables(true) ).DataTable().responsive.recalc();
    //     }
    // });
});

</script> 
