<div class="modal fade" id="addmodifieroptions1" role="dialog" style="z-index: 9999;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Modifier Options</h4>
        </div>
                   <form method="POST" id="modifieroptiongroup_form" action="{{ '/menu/item/modifieritem/options/add' }}" enctype="multipart/form-data">

        <div class="modal-body">
                            @csrf
                       	      <ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#options">Options</a></li>
				
			      </ul>

			      <div class="tab-content">
				  <div id="options" class="tab-pane fade in active">
				  <table width="100%" class="optionlist" border="1">
				  
				  </table>
				   
				  </div>
				   
				 
			      </div> 	
                             
                            <div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="option_name" type="text"
                                               class=""
                                               name="option_name"  required>
                                    </div>
                                    
                                </div>
                            </div>
		

                            <div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Dependent Modifier Item Id') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="dependent_modifier_id" type="text"
                                               class=""
                                               name="dependent_modifier_id"  value="0" required>
                                    </div>
                                    
                                </div>
                            </div>
			     
                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
                             <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="option_price" type="text"
                                               class=""
                                               name="option_price"  value="0" required>
                                    </div>
                                    
                                </div>
                        </div>
			<div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Is this required?') }}</label>
                                <div class="col-md-4">
                                    <div class=" ">
                                        <label class="radio-inline"><input type="radio"  name="is_required" value="1"> Required</label>
                                        &nbsp;&nbsp;&nbsp;&nbsp; <label class="radio-inline"><input type="radio"  name="is_required" value="0">Optional</label>
                                    </div>
                                    
                                </div>
                            </div>
			<div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <textarea id="option_description" class="" rows="5" cols="50" name="option_description" ></textarea>
                                    </div>
                                    
                                </div>
                            </div>
			<div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Sort Order') }}</label>
                             <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="sort_order"  
                                               class="" type="number"
                                               name="sort_order"  value="0" required>
                                    </div>
                                    
                                </div>
                        </div>
			<div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                             <div class="col-md-4">
                                    <div class=" ">
                                        <label class="radio-inline"><input type="radio"  name="status" value="1" > Active</label>
                                        &nbsp;&nbsp; <label class="radio-inline"><input type="radio"  name="status" value="0"  > InActive</label>
                                    </div>
                                    
                                </div>
                        </div>
	
        </div>
	 
        <div class="modal-footer">
	     <input type="hidden" id="menuItemId" name="menuItemId"  value="{{ $menuItem->id }}" />
	     <input type="hidden" id="modifierItemId" name="modifierItemId" value="0" />	
      	     <input type="hidden" id="menuItemRestaurantId" name="menuItemRestaurantId"  value="{{ $menuItem->restaurant_id }}" />
             <button type="button" id="addmodifieroptionsSave" class="btn btn__primary">{{ __('Save') }}</button>

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
            </form>
      </div>
      
    </div>
 </div>
<div class="modal fade" id="modifiergroup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Modifier Group</h4>
        </div>
                   <form method="POST" id="modifiergroup_form" action="{{ route('modifier-group.store') }}" enctype="multipart/form-data">

        <div class="modal-body">
                            @csrf
                       
                            <input type="hidden" name="restaurant_id" value="{{@$menuItem->restaurant_id}}">
                            <input type="hidden" name="menu_id" value="{{$item_id}}">
                            <div class="form-group row form_field__container">
                                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="group_name" type="text"
                                               class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
                                               name="group_name" value="{{ old('group_name') }}" required>
                                    </div>
                                    @if ($errors->has('group_name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row form_field__container">
                                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Prompt') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="prompt" type="text"
                                               class="{{ $errors->has('prompt') ? ' is-invalid' : '' }}"
                                               name="prompt" value="{{ old('prompt') }}" required>
                                    </div>
                                    @if ($errors->has('prompt'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('prompt') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
			    <div class="form-group row form_field__container">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <textarea rows="5" cols="100" name="description" > </textarea>
                                        </div>
                                        @if ($errors->has('group_name'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('group_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            {{--<div class="form-group row form_field__container">
                                <label for="pos_id" class="col-md-4 col-form-label text-md-right">{{ __('POS Id (Modifier)') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}" name="pos_id" value="{{ old('pos_id') }}" >
                                    </div>
                                    @if ($errors->has('pos_id'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('pos_id') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row form_field__container">
                                <label for="calorie" class="col-md-4 col-form-label text-md-right">{{ __('Calorie') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="calorie" type="text" class="{{ $errors->has('calorie') ? ' is-invalid' : '' }}" name="calorie" value="{{ old('calorie') }}" >
                                    </div>
                                    @if ($errors->has('calorie'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('calorie') }}</strong></span>
                                    @endif
                                </div>
                            </div>--}}

                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Required') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <label class="radio-inline"><input type="radio" id="status1" name="is_required" value="1" checked=""  > Yes</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="radio-inline"><input type="radio" id="status0" name="is_required" value="0" {{ (old('is_required')=='0') ? 'checked' :'' }}> No</label>
                                @if ($errors->has('is_required'))
                                <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('is_required') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                                <label for="status"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                                <div class="col-md-3 quantity_type_select">
                                    <select name="status" id="status"
                                            class="quantity_type form-control{{ $errors->has('status') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="1">Active</option>
                                        <option value="0">InActive</option>
                                      
                                    </select>
                                  
                                    @if ($errors->has('status'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                          </div>           
                         <div class="form-group row form_field__container">
                                <label for="quantity_type_addongroup"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Quantity Type') }}</label>
                                <div class="col-md-3 quantity_type_select">
                                    <select name="quantity_type" id="quantity_type_modifiergroup"
                                            class="quantity_type form-control{{ $errors->has('quantity_type') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="Value">Value</option>
                                        <option value="Range">Range</option>
                                      
                                    </select>
                                  
                                    @if ($errors->has('quantity_type'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('quantity_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                       <div class="col-md-3 quantity_type_val">
                               
                                     <div class="value_quantity1">
                                             <div class="">

                                            <input id="quantity" type="text"
                                                   class="quantity {{ $errors->has('quantity') ? ' is-invalid' : '' }} form-control"
                                                   name="quantity" value="{{ old('quantity') }}" required placeholder="Min,Max">
                                              @if ($errors->has('quantity'))
                                              <span class="invalid-feedback">
                                              <strong>{{ $errors->first('quantity') }}</strong>
                                              </span>
                                              @endif
                                           </div>
                                        
                                  </div>

                                    
                                </div>
                            </div>


                            <br/><br/>
                            <div class=" mb-0 ">
                                <div class="col-md-12 text-center">
                                </div>
                            </div>
        </div>
        <div class="modal-footer">
             <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
            </form>
      </div>
      
    </div>
  </div>
