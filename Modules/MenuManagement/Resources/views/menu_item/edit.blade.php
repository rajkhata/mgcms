@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
        <div class="reservation__atto">
          <div class="reservation__title">
            <h1>Menu Item 2- Edit</h1>
            <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
          </div>
        </div>
        <!-- <div class="row" style="padding-top: 20px;">
            <label for="language" class="col-md-1 col-form-label no-col-padding text-md-right" style="padding-top: 5px">
               {{ __('Language') }}
            </label>
             <div class="col-md-3">
              <select name="language" id="language" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }}"required>
                  <option value="1"{{ ($languageId == 1) ? ' selected' :'' }}>English</option>
                  <option value="2"{{ ($languageId == 2) ? ' selected' :'' }}>Chinese</option>              
              </select>
                </div>
        </div> -->
        <div>
            <div  class="card margin-top-0">
                <div class="card-body">
                    <div class="">

                      <ul class="nav nav-tabs">
                        <li class="{{!isset($section_tab)?'active':''}}"><a data-toggle="tab" href="#general">General</a></li>
{{--
                        <li ><a data-toggle="tab" href="#associated">Associated Products</a></li>
--}}
                        <li class="hidden"><a data-toggle="tab" href="#schedule">Schedule</a></li>
                        <li><a data-toggle="tab" href="#modifiers">Modifiers</a></li>
                        <li class="hidden"><a data-toggle="tab" href="#addons">Add-ons</a></li>
                        <li><a data-toggle="tab" href="#labels">Labels</a></li>
                        <li class="{{(isset($section_tab) && $section_tab=='images')?'active':''}} hidden"><a  data-toggle="tab" href="#itemimages">Images</a></li>
                      </ul>

                <div class="tab-content">
                <div id="itemimages" class="tab-pane {{(isset($section_tab) && $section_tab=='images')?'active in':''}}">
                   @include('menumanagement::common.message')

                     <form method="POST" action="{{ '/menu/item/images/'.$item_id }}" enctype="multipart/form-data">
                        @csrf
                        <?php 
                        $allimages=json_decode($menuItem->images,true);
                        /*echo '<pre>';
                        print_r( $allimages);
                        echo '</pre>';*/
                        ?>
                        <div class="form-group row form_field__container source-image-div image-caption-div">
                            <label for="image"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Mobile App Image') }}</label>
                            <div class="col-md-8">
       
                                                                
                            <div class="image_cont_main row nospace rowpb">
                                 @if(empty($allimages['mobile_app_images']))
 
                                <div class="col-md-9">
                                      <div class="col-md-10">
                                        <input id="image" type="file"
                                               class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                                               value="{{ old('image.0') }}" name="image[]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                        @if ($errors->has('image'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        <div class="input__field">
                                            <input id="image_caption" type="text"
                                                   class="{{ $errors->has('image_caption') ? ' is-invalid' : '' }}"
                                                   name="image_caption[]"
                                                   value="{{old('image_caption.0') }}"
                                                   placeholder="Image Caption">
                                        </div>
                                        @if ($errors->has('image_caption.0'))
                                            <span class="invalid-feedback" style="display: block;">
                                                <strong>{{ $errors->first('image_caption.0') }}</strong>
                                            </span>
                                        @endif
                                </div> 
                                 <div class="col-md-2">

                                <button type="button" imagetype="image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                </div>                                      
                            </div>       
                                 @else
                                 <div class="col-md-6 no-col-padding">

                                <button type="button" imagetype="image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                </div>    
                               @endif  
                            </div>

                            @if(isset($allimages['mobile_app_images']) && count($allimages['mobile_app_images'])>0)
                            <?php 
                            $i=0;
                             ?>                             
                             @foreach($allimages['mobile_app_images'] as $key=>$image)
                                <div class="image_cont_main row nospace rowpb">
                                    
                                    <div class="col-md-9 no-col-padding">
                                         <div class="col-md-9 no-col-padding">
                                            <input id="image" type="file"
                                                   class="form-control"
                                                   value="" name="image[{{$key}}]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                            
                                            <div class="input__field">
                                                <input id="image_caption" type="text"
                                                       class=""
                                                       name="image_caption[{{$key}}]"
                                                       value="{{$image['image_caption']}}"
                                                       placeholder="Image Caption">
                                            </div>
                                      </div> 
                                      <div class="col-md-3">
                                           <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >

                                      </div>   
                                </div>
                                <input type="hidden" name="image_hidden[]" value="{{$key}}" >
                                    <div class="col-md-3 ">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['image']}}">
                                            <img class="img-responsive img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['thumb_image_sml'] }}"></a>&emsp;
                                      </div> 
                                    
                                </div>

                              <?php 
                            $i++;
                             ?>
                            @endforeach

                            @endif
                            </div>
                           
                        </div>
                        <hr>
                        <div class="form-group row form_field__container source-image-div image-caption-div">
                            <label for="web_image"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Desktop Web Image') }}</label>
                            <div class="col-md-8">
                                <div class="image_cont_main row nospace rowpb">
                                     @if(empty($allimages['desktop_web_images']))
                                        <div class="col-md-9">
                                            <div class="col-md-10">
                                                <input id="web_image" type="file"
                                                       class="form-control{{ $errors->has('web_image') ? ' is-invalid' : '' }}"
                                                       value="{{ old('web_image.0') }}" name="web_image[]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                @if ($errors->has('web_image'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('web_image') }}</strong>
                                                    </span>
                                                @endif
                                                <div class="input__field">
                                                    <input id="web_image_caption" type="text"
                                                           class="{{ $errors->has('web_image_caption') ? ' is-invalid' : '' }}"
                                                           name="web_image_caption[]"
                                                           value="{{ count(old())==0 ?@$allimages['desktop_web_images'][0]['web_image_caption']  : old('web_image_caption.0') }}"
                                                           placeholder="Image Caption">
                                                </div>
                                                @if ($errors->has('web_image_caption.0'))
                                                    <span class="invalid-feedback" style="display: block;">
                                                        <strong>{{ $errors->first('web_image_caption.0') }}</strong>
                                                    </span>
                                                @endif
                                           </div>
                                            <div class="col-md-2">
                                              <button type="button" imagetype="web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                            </div>
                                    </div>
                                    @else 
                                    <div class="col-md-6 no-col-padding">

                                   <button type="button" imagetype="web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                   </div>      
                                    @endif;
                            </div>
                               @if(isset($allimages['desktop_web_images']) && count($allimages['desktop_web_images'])>0)
                            <?php 
                            $i=0;
                             ?>
                             @foreach($allimages['desktop_web_images'] as $key=>$image)
                                <div class="image_cont_main row nospace rowpb">
                                    
                                    <div class="col-md-9 no-col-padding">

                                       <div class="col-md-9 no-col-padding">
                                            <input id="web_image" type="file"
                                                   class="form-control"
                                                   value="" name="web_image[{{$key}}]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                            
                                            <div class="input__field">
                                                <input id="web_image_caption" type="text"
                                                       class=""
                                                       name="web_image_caption[{{$key}}]"
                                                       value="{{$image['web_image_caption']}}"
                                                       placeholder="Image Caption">
                                            </div>
                                        </div>    
                                        <div class="col-md-2">
                                           <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >  
                                        </div>
                                </div>
                                <input type="hidden" name="web_image_hidden[]" value="{{$key}}" >
                                     <div class="col-md-3 ">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['web_image']}}">
                                            <img class="img-responsive img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['web_thumb_sml'] }}"></a>&emsp;
                                      </div>
                                   
                                </div>

                            @endforeach

                            @endif
                            </div>
                            
                        </div>
 <hr>

                        <div class="form-group row form_field__container source-image-div image-caption-div">
                            <label for="mobile_web_image"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Mobile Web Image') }}</label>
                            <div class="col-md-8">
                                <div class="image_cont_main row nospace rowpb">
                                    @if(empty($allimages['mobile_web_images']))
                            
                                <div class="col-md-9">
                                     <div class="col-md-10">
                                    <input id="mobile_web_image" type="file"
                                           class="form-control{{ $errors->has('mobile_web_image') ? ' is-invalid' : '' }}"
                                           value="{{ old('mobile_web_image.0') }}" name="mobile_web_image[]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                    @if ($errors->has('mobile_web_image'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('mobile_web_image') }}</strong>
                                        </span>
                                    @endif
                                    <div class="input__field">
                                        <input id="mobile_web_image_caption" type="text"
                                               class="{{ $errors->has('mobile_web_image_caption') ? ' is-invalid' : '' }}"
                                               name="mobile_web_image_caption[]"
                                               value="{{old('mobile_web_image_caption.0') }}"
                                               placeholder="Image Caption">
                                    </div>
                                    @if ($errors->has('mobile_web_image_caption.0'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('mobile_web_image_caption.0') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                      <button type="button" imagetype="mobile_web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>

                                </div>
                            </div>
                            @else
                                <div class="col-md-2 no-col-padding">
                                      <button type="button" imagetype="mobile_web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>

                                </div>                            
                            @endif
                            </div>

   @if(isset($allimages['mobile_web_images']) && count($allimages['mobile_web_images'])>0)
                             @foreach($allimages['mobile_web_images'] as $key=>$image)

                                <div class="image_cont_main row nospace">
                                    
                                    <div class="col-md-9  no-col-padding3">
                                         <div class="col-md-9  no-col-padding">
                                        <input id="mobile_web_image" type="file"
                                               class="form-control"
                                               value="" name="mobile_web_image[{{$key}}]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                        
                                        <div class="input__field">
                                            <input id="mobile_web_image_caption" type="text"
                                                   class=""
                                                   name="mobile_web_image_caption[{{$key}}]"
                                                   value="{{$image['mobile_web_image_caption']}}"
                                                   placeholder="Image Caption">
                                        </div>
                                         </div>
                                         <div class="col-md-3">
                                               <input type="button" value="Delete" class="btn  btn-sm btn__cancel deleteImage" >
                                         </div>

                                  </div>
                                  <input type="hidden" name="mobile_web_image_hidden[]" value="{{$key}}" >
                                     <div class="col-md-3">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['mobile_web_image']}}">
                                            <img class="img-responsive  img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['mobile_web_thumb_sml'] }}"></a>&emsp;
                                      </div>
                                   
                                </div>

                            @endforeach

                            @endif

                            </div>
                            
                        </div>
                          <div class="row  mb-0">
                            <div class="col-md-12 text-center" style="padding-top: 20px">
                                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
{{--
                <div id="associated" class="tab-pane fade">
                    <h3>Associated Products</h3>
                    <form class="form-horizontal" action="">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name"  name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="sku">SKU:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="sku"  name="sku">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
--}}
                <div id="general" class="tab-pane {{!isset($section_tab)?'fade in active':''}}">
                <h3>General</h3>
               
                                  {{ Form::model($menuItem, array('route' => array('item.update', $menuItem->id), 'method' => 'PUT', 'files' => true)) }}
                        @csrf
                        <input type="hidden" name="language_id" id="menu_item_edit_form_language_id" value="{{$languageId}}"/>

                        <div class="row">

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12">
                                <label for="restaurant_id"
                                   class="col-form-label text-md-right">{{ __('Restaurant') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="restaurant_id" id="restaurant_id"
                                            class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }} selectpicker"
                                            required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        @foreach($groupRestData as $rest)
                                            <optgroup label="{{ $rest['restaurant_name'] }}">
                                                @foreach($rest['branches'] as $branch)
                                                    <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id'] || (count(old())==0 && $menuItem->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('restaurant_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('restaurant_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 pull-right">
                                <label for="menu_category_id"
                                   class="col-form-label text-md-right">{{ __('Category') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="menu_category_id" id="menu_category_id"
                                            class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Category</option>
                                        @foreach($categoryData as $cat)
                                            <option value="{{ $cat->id }}" {{ (old('menu_category_id')==$cat->id || (count(old())==0 && $menuItem->menu_category_id==$cat->id)) ? 'selected' :'' }}>{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('menu_category_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('menu_category_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                                <label for="menu_sub_category_id"
                                   class="col-form-label text-md-right">{{ __('Sub Category') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="menu_sub_category_id" id="menu_sub_category_id"
                                            class="form-control {{ $errors->has('menu_sub_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Sub-Category</option>
                                    
                                        {!! $subcat_options !!}
                        
                                    </select>
                                    @if ($errors->has('menu_sub_category_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('menu_sub_category_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                <label for="product_type"
                                   class="col-form-label text-md-right">{{ __('Product Type') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="product_type" id="product_type"
                                            class="form-control {{ $errors->has('product_type') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Product Type</option>
                                        @foreach($product_types as $key=>$val)
                                            <option value="{{ $key }}" {{ (old('product_type')==$key || (count(old())==0 && $menuItem->product_type==$key)) ? 'selected' :'' }}>{{ $val }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('product_type'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('product_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!-- end -->

                        <div class="row">
                            <!-- <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                                <label for="food_type" class="col-form-label text-md-right">{{ __('Food Type') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="food_type" id="product_type" class="form-control {{ $errors->has('food_type') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Food Type</option>
                                        @foreach($food_types as $val)
                                            <option value="{{ $val }}" {{ (old('food_type')==$val || (count(old())==0 && $menuItem->food_type===$val)) ? 'selected' : '' }}>{{ ucwords($val) }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('food_type'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('food_type') }}</strong></span>
                                    @endif
                                </div>
                            </div> -->

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                <label for="priority" class="col-form-label text-md-right padbot">{{ __('POS Id (Item)') }}</label>
                                <div class="input__field">
                                    <input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}" name="pos_id" value="{{ count(old())==0 ? $menuItem->pos_id : old('pos_id') }}" >
                                    <!-- <span for="name">{{ __('POS Id (Item)') }}</span> -->
                                </div>
                                @if ($errors->has('pos_id'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('pos_id') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                                <label for="priority" class="col-form-label text-md-right">{{ __('SKU') }}</label>
                                <div class="input__field">
                                    <input id="sku" type="text" class="{{ $errors->has('sku') ? ' is-invalid' : '' }}" name="sku" value="{{ count(old())==0 ? $menuItem->sku : old('sku') }}" >
                                    <!-- <span for="sku">{{ __('SKU') }}</span> -->
                                </div>
                                @if ($errors->has('sku'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('sku') }}</strong></span>
                                @endif
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                <label for="priority" class="col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="input__field">
                                    <input id="name" type="text"
                                           class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ count(old())==0 ? $menuItem->name : old('name') }}"
                                           required>
                                    <!-- <span for="name">{{ __('Name') }}</span> -->
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <!-- end -->
			<div class="row">
                            <!-- <div class="col-sm-5 col-md-5 col-lg-5 margin-top-20 col-xs-12">
                            <label for="calorie" class="col-md-3 col-form-label text-md-right">{{ __('Calorie') }}</label>
                            <div class="col-md-3">
                                <div class="input__field">
                                    <input id="calorie" type="text" class="{{ $errors->has('calorie') ? ' is-invalid' : '' }}" name="calorie" value="{{ count(old())==0 ? $menuItem->calorie : old('calorie') }}" >
                                </div>
                                @if ($errors->has('calorie'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('calorie') }}</strong></span>
                                @endif
                            </div> 
                        </div> -->
                          
                      </div> 
			<div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 margin-top-20 col-xs-12">
                            <label for="customization_view" class="col-md-3 col-form-label text-md-right">{{ __('Details Page') }}</label>
                            <div class="col-md-3">
                                <div class="input__field">
                                    <input id="calorie" type="text" class="{{ $errors->has('customization_view') ? ' is-invalid' : '' }}" name="customization_view" value="{{ count(old())==0 ? $menuItem->customization_view : old('customization_view') }}" > 
                                </div>
                                @if ($errors->has('customization_view'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('customization_view') }}</strong></span>
                                @endif
                            </div>[ (0 or 1 or 2)  1 = DETAIL_VIEW  2 = POPUP_VIEW  0 = NO_DETAIL, Default is 1]
                        </div> </div> 
			<div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 margin-top-20 col-xs-12">
                                <label for="card_templet" class="col-form-label text-md-right">{{ __('Template type') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="card_templet" id="card_templet" class="form-control {{ $errors->has('card_templet') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="">Select Template</option>
                                         
                                            <option value="0" {{ (  ($menuItem->card_templet==0)) ? 'selected' : '' }}>Default Template</option>
					    <option value="1" {{ (  ($menuItem->card_templet==1)) ? 'selected' : '' }}>FULL Width IMAGE</option>
					    
                                        
                                    </select>
                                    @if ($errors->has('food_type'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('food_type') }}</strong></span>
                                    @endif
                                </div>
                            </div></div> 
                        <div class="product_type_product @if($menuItem->product_type=='product') @else hide  @endif">
                            <div class="form-group row form_field__container">
                                <label for="gift_wrapping_fees" class="col-md-3 col-form-label text-md-right">{{ __('Gift Wrapping Fees') }}</label>
                                <div class="col-md-3">
                                    <div class="input__field">
                                        <input id="gift_wrapping_fees" type="text"
                                            class="{{ $errors->has('gift_wrapping_fees') ? ' is-invalid' : '' }}"
                                            name="gift_wrapping_fees" value="{{ count(old())==0 ? $menuItem->gift_wrapping_fees : old('gift_wrapping_fees') }}" >
                                    </div>
                                </div>
                            </div>
                        </div>   

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 margin-top-20 col-xs-12">
                                <label for="description"
                                    class="col-form-label text-md-right">{{ __('Description') }} <!--<span class="mandat">*</span>--></label>
                                <div class="">
                                    <div class="custom__message-textContainer">
                                    <textarea maxlength="500" rows="4" name="description" id="description"
                                            class="editor form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                            >{{ count(old())==0 ? $menuItem->description : old('description')}}</textarea>
                                    </div>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                <label for="sub_description"
                                    class="col-form-label text-md-right">{{ __('Sub Description') }}</label>
                                <div class="">
                                    <div class="custom__message-textContainer">
                                    <textarea maxlength="500" rows="4" name="sub_description" id="sub_description"
                                            class="editor form-control{{ $errors->has('sub_description') ? ' is-invalid' : '' }}"
                                            >{{ count(old())==0 ? $menuItem->sub_description : old('sub_description')}}</textarea>
                                    </div>
                                    @if ($errors->has('sub_description'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('sub_description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20">
                                <label for="is_tax_applicable"
                                   class="col-form-label text-md-right">{{ __('Taxable') }}</label>
                                <div class="selct-picker-plain position-relative">
                                    <select name="is_tax_applicable" id="is_tax_applicable"
                                            class="form-control {{ $errors->has('is_tax_applicable') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                        <option value="1" Selected>Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    @if ($errors->has('is_tax_applicable'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('is_tax_applicable') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right">
                                <label for="display_order" class="col-form-label text-md-right">{{ __('Sort Order') }}</label>
                                <div class="input__field">
                                    <input id="display_order" type="text"
                                        class="{{ $errors->has('display_order') ? ' is-invalid' : '' }}"
                                        name="display_order" value="{{ count(old())==0 ? $menuItem->display_order : old('display_order') }}" >
                                </div>
                                @if ($errors->has('display_order'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('display_order') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20" style="padding-bottom: 10px;">
                                <label for="priority"
                                   class="col-form-label text-md-right" style="display:block">{{ __('Status') }}</label>
                                <label class="radio-inline"> <input type="radio" id="status1" name="status"
                                       value="1" {{ (old('status')=='1' || (count(old())==0 && $menuItem->status=='1')) ? 'checked' :'' }}>
                                Active</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="radio-inline"><input type="radio" id="status0" name="status"
                                       value="0" {{ (old('status')=='0' || (count(old())==0 && $menuItem->status=='0')) ? 'checked' :'' }}>
                                    Inactive</label>
                                @if ($errors->has('status'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-sm-5 col-md-5 col-lg-5 col-xs-12 margin-top-20 pull-right" style="padding-bottom: 10px;">
                                <label for="priority" class="col-form-label text-md-right" style="display:block">{{ __('Is customizable') }}</label>
                                <label class="radio-inline"><input type="radio" id="is_customizable1" name="is_customizable"
                                    value="1" {{ (old('is_customizable')=='1' || $menuItem->is_customizable=='1') ? 'checked' :'' }}>
                                    Yes</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="radio-inline"><input type="radio" id="is_customizable2" name="is_customizable"
                                    value="0" {{ (old('is_customizable')=='0' ||  $menuItem->is_customizable=='0') ? 'checked' :'' }}>
                                    No</label>
                                @if ($errors->has('is_customizable'))
                                    <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('is_customizable') }}</strong>
                                        </span>
                                @endif
                            </div>

                        </div>
                        <!-- end -->

                    <div class="row hidden">
                        <label for="is_popular" class="col-md-4 col-form-label text-md-right">{{ __('Is Popular') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="is_popular" type="checkbox"
                                       class="{{ $errors->has('is_popular') ? ' is-invalid' : '' }}"  @if(isset($menuItem->is_popular) && $menuItem->is_popular==1) checked @endif
                                       name="is_popular" value="1" >
                            </div>
                            @if ($errors->has('is_popular'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('is_popular') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="row hidden">
                        <label for="is_favourite" class="col-md-4 col-form-label text-md-right">{{ __('Is Favourite') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="is_favourite" type="checkbox"
                                       class="{{ $errors->has('is_favourite') ? ' is-invalid' : '' }}" @if(isset($menuItem->is_favourite) && $menuItem->is_favourite==1) checked @endif
                                       name="is_favourite" value="1" >
                            </div>
                            @if ($errors->has('is_favourite'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('is_favourite') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row hidden">
                        <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Manage Inventory') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="manage_inventory" type="checkbox"
                                       class="{{ $errors->has('manage_inventory') ? ' is-invalid' : '' }}" @if(isset($menuItem->manage_inventory) && $menuItem->manage_inventory==1) checked @endif
                                       name="manage_inventory" value="1" />
                            </div>
                            @if ($errors->has('manage_inventory'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('manage_inventory') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row manage_inventory_hide_div  hidden">
                        <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Inventory') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="inventory" type="text"
                                       class="{{ $errors->has('inventory') ? ' is-invalid' : '' }}"
                                       name="inventory" value="{{ count(old())==0 ? $menuItem->inventory : old('inventory') }}"
                                       />
                            </div>
                            @if ($errors->has('inventory'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('inventory') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row hidden">
                        <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Maximum Allowed Quantity') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="max_allowed_quantity" type="text"
                                       class="{{ $errors->has('max_allowed_quantity') ? ' is-invalid' : '' }}"
                                       name="max_allowed_quantity" value="{{ count(old())==0 ? $menuItem->max_allowed_quantity : old('max_allowed_quantity') }}"
                                       />
                            </div>
                            @if ($errors->has('max_allowed_quantity'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('max_allowed_quantity') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
		
                    <div class="row hidden">
                        <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Minimum Allowed Quantity') }}</label>
                        <div class="col-md-4">
                            <div class="input__field">
                                <input id="min_allowed_quantity" type="text"
                                       class="{{ $errors->has('min_allowed_quantity') ? ' is-invalid' : '' }}"
                                       name="min_allowed_quantity" value="{{ count(old())==0 ? $menuItem->min_allowed_quantity : old('min_allowed_quantity') }}"
                                       />
                            </div>
                            @if ($errors->has('min_allowed_quantity'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('min_allowed_quantity') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
		 
                    <?php 
                        $allimages=json_decode($menuItem->images,true);
                        /*echo '<pre>';
                        print_r( $allimages);
                        echo '</pre>';*/
                        ?>
                        <div class="form-group row form_field__container source-image-div image-caption-div no-padding-left no-padding-right margin-top-30">
                            <label for="image" class="col-md-12 col-form-label text-md-right">{{ __('Mobile App Image') }}</label>
                            <div class="col-md-12">
                                <div class="image_cont_main row nospace rowpb">
                                    @if(empty($allimages['mobile_app_images']))
                                        <div class="col-md-9">
                                            <div class="col-md-10">
                                                <input id="image" type="file"
                                                       class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                                                       value="{{ old('image.0') }}" name="image[]">
                                                @if ($errors->has('image'))
                                                    <span class="invalid-feedback">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                                @endif
                                                <div class="input__field">
                                                    <input id="image_caption" type="text"
                                                           class="{{ $errors->has('image_caption') ? ' is-invalid' : '' }}"
                                                           name="image_caption[]"
                                                           value="{{old('image_caption.0') }}"
                                                           placeholder="Image Caption">
                                                </div>
                                                @if ($errors->has('image_caption.0'))
                                                    <span class="invalid-feedback" style="display: block;">
                                                <strong>{{ $errors->first('image_caption.0') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" imagetype="image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-6 no-col-padding">
                                            <button type="button" imagetype="image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                        </div>
                                    @endif
                                </div>
                            @if(isset($allimages['mobile_app_images']) && count($allimages['mobile_app_images'])>0)
                            <?php
                            $i=0;
                             ?>
                             @foreach($allimages['mobile_app_images'] as $key=>$image)
                                <div class="image_cont_main row nospace">
                                    
                                    <div class="col-md-9 no-col-padding">
                                        <div class="col-md-9 no-col-padding">
                                            <input id="image" type="file" class="form-control" value="" name="image[{{$key}}]">
                                            <div class="input__field">
                                                <input id="image_caption" type="text" class="" name="image_caption[{{$key}}]" value="{{$image['image_caption']}}" placeholder="Image Caption">
                                            </div>
                                        </div>
                                        <div class="col-md-3 nomobtab">
                                           <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >
                                        </div>
                                    </div>
                                    <input type="hidden" name="image_hidden[]" value="{{$key}}" >
                                    <div class="col-md-3 nomobtab">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['image']}}">
                                            <img class="img-responsive img-thumbnail" src="{{url('/').'/..'.@$image['thumb_image_sml']}}">
                                        </a>&emsp;
                                    </div>
                                </div>

                              <?php
                            $i++;
                             ?>
                            @endforeach
                            @endif
                            </div>
                        </div>
 <hr>
                        <div class="form-group row form_field__container source-image-div image-caption-div no-padding-left no-padding-right">
                            <label for="web_image" class="col-md-12 col-form-label text-md-right">{{ __('Desktop Web Image') }}</label>
                            <div class="col-md-12">
                                <div class="image_cont_main row nospace rowpb">
                                     @if(empty($allimages['desktop_web_images']))
                                        <div class="col-md-9 no-col-padding">
                                            <div class="col-md-10 no-col-padding">
                                                <input id="web_image" type="file"
                                                       class="form-control{{ $errors->has('web_image') ? ' is-invalid' : '' }}"
                                                       value="{{ old('web_image.0') }}" name="web_image[]">
                                                @if ($errors->has('web_image'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('web_image') }}</strong>
                                                    </span>
                                                @endif
                                                <div class="input__field">
                                                    <input id="web_image_caption" type="text"
                                                           class="{{ $errors->has('web_image_caption') ? ' is-invalid' : '' }}"
                                                           name="web_image_caption[]"
                                                           value="{{ count(old())==0 ?@$allimages['desktop_web_images'][0]['web_image_caption']  : old('web_image_caption.0') }}"
                                                           placeholder="Image Caption">
                                                </div>
                                                @if ($errors->has('web_image_caption.0'))
                                                    <span class="invalid-feedback" style="display: block;">
                                                        <strong>{{ $errors->first('web_image_caption.0') }}</strong>
                                                    </span>
                                                @endif
                                           </div>
                                            <div class="col-md-2">
                                              <button type="button" imagetype="web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                            </div>
                                    </div>
                                    @else 
                                    <div class="col-md-6 no-col-padding">
                                        <button type="button" imagetype="web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>
                                    </div>      
                                    @endif
                            </div>
                               @if(isset($allimages['desktop_web_images']) && count($allimages['desktop_web_images'])>0)
                            <?php 
                            $i=0;
                             ?>
                             @foreach($allimages['desktop_web_images'] as $key=>$image)
                                <div class="image_cont_main row nospace">
                                    
                                    <div class="col-md-9  no-col-padding">
                                       <div class="col-md-9  no-col-padding">
                                            <input id="web_image" type="file"
                                                   class="form-control"
                                                   value="" name="web_image[{{$key}}]">
                                            
                                            <div class="input__field">
                                                <input id="web_image_caption" type="text"
                                                       class=""
                                                       name="web_image_caption[{{$key}}]"
                                                       value="{{$image['web_image_caption']}}"
                                                       placeholder="Image Caption">
                                            </div>
                                        </div>    
                                        <div class="col-md-3 nomobtab">
                                           <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >  
                                        </div>
                                    </div>
                                    <input type="hidden" name="web_image_hidden[]" value="{{$key}}" >
                                    <div class="col-md-3 nomobtab">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['web_image']}}">
                                            <img class="img-responsive img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['web_thumb_sml'] }}">
                                        </a>&emsp;
                                    </div>
                                </div>
                            @endforeach
                            @endif
                            </div>
                        </div>
 <hr>

                        <div class="form-group row form_field__container source-image-div image-caption-div no-padding-left no-padding-right">
                            <label for="mobile_web_image"
                                   class="col-md-12 col-form-label text-md-right">{{ __('Mobile Web Image') }}</label>
                            <div class="col-md-12">
                                <div class="image_cont_main row nospace rowpb">
                                    @if(empty($allimages['mobile_web_images'])))
                            
                                <div class="col-md-9 no-col-padding">
                                     <div class="col-md-10 no-col-padding">
                                    <input id="mobile_web_image" type="file"
                                           class="form-control{{ $errors->has('mobile_web_image') ? ' is-invalid' : '' }}"
                                           value="{{ old('mobile_web_image.0') }}" name="mobile_web_image[]">
                                    @if ($errors->has('mobile_web_image'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('mobile_web_image') }}</strong>
                                        </span>
                                    @endif
                                    <div class="input__field">
                                        <input id="mobile_web_image_caption" type="text"
                                               class="{{ $errors->has('mobile_web_image_caption') ? ' is-invalid' : '' }}"
                                               name="mobile_web_image_caption[]"
                                               value="{{old('mobile_web_image_caption.0') }}"
                                               placeholder="Image Caption">
                                    </div>
                                    @if ($errors->has('mobile_web_image_caption.0'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('mobile_web_image_caption.0') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                      <button type="button" imagetype="mobile_web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>

                                </div>
                            </div>
                            @else
                                <div class="col-md-2 no-col-padding">
                                      <button type="button" imagetype="mobile_web_image" class="btn btn-sm btn__primary add_new_image">Add New</button>

                                </div>                            
                            @endif
                            </div>

   @if(isset($allimages['mobile_web_images']) && count($allimages['mobile_web_images'])>0)
                             @foreach($allimages['mobile_web_images'] as $key=>$image)

                                <div class="image_cont_main row nospace">
                                    
                                    <div class="col-md-9  no-col-padding">
                                         <div class="col-md-9  no-col-padding">
                                        <input id="mobile_web_image" type="file"
                                               class="form-control"
                                               value="" name="mobile_web_image[{{$key}}]">
                                        
                                        <div class="input__field">
                                            <input id="mobile_web_image_caption" type="text"
                                                   class=""
                                                   name="mobile_web_image_caption[{{$key}}]"
                                                   value="{{$image['mobile_web_image_caption']}}"
                                                   placeholder="Image Caption">
                                        </div>
                                         </div>
                                         <div class="col-md-3 nomobtab">
                                               <input type="button" value="Delete" class="btn  btn-sm btn__cancel deleteImage" >
                                         </div>
                                    </div>
                                    <input type="hidden" name="mobile_web_image_hidden[]" value="{{$key}}" >
                                     <div class="col-md-3 nomobtab">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['mobile_web_image']}}">
                                            <img class="img-responsive  img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['mobile_web_thumb_sml'] }}"></a>&emsp;
                                      </div>
                                   
                                </div>

                            @endforeach

                            @endif

                            </div>
                            
                        </div>


                        <div id="size_div" style="background-color: ">
                            @php
                                $sizeCount = 0;
                                if(session()->has('sizeCount'))
                                {
                                    $sizeCount = session()->get('sizeCount');
                                }
                                else if(isset($mealPriceData))
                                {
                                    $sizeCount = count($mealPriceData);
                                }
                            //print_r( $mealPriceData[0]->price[0]);
                            //exit;
                            @endphp
                            @if(isset($sizeCount) && $sizeCount>0)
                                @for($i=0;$i<$sizeCount;$i++)
                                    <div class="size_div" id="size_div_{{$i}}"
                                         style="padding:20px 0px;border: 1px solid #ccc;margin: 10px 15px;">
                                        <div class="form-group row form_field__container no-padding-left no-padding-right">
                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">
                                                <label for="size" class=" col-form-label text-md-right">{{ __('Size') }}</label>
                                                <div class="input__field">
                                                    <input type="text"
                                                        class="size {{ $errors->has('size.'.$i) ? ' is-invalid' : '' }}"
                                                        name="size[]"
                                                        value="{{ (count(old())==0 && isset($mealPriceData[$i]['size'])) ? $mealPriceData[$i]['size'] : old('size.'.$i) }}">
                                                </div>
                                                @if ($errors->has('size.'.$i))
                                                    <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('size.'.$i) }}</strong>
                                                        </span>
                                                @endif
                                            </div>

                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">
                                                <label for="price" class="col-form-label text-md-right">{{ __('Is Available') }}</label style="margin-bottom:10px;">
                                                <div class="row">
                                                    <div class="col-md-6" style="margin:auto;padding-top:auto; padding-left:0px;">
                                                        <label class="checkbox-inline"><input type="checkbox"
                                                                                              class="delivery_all"
                                                                                              name='delivery_all[{{$i}}]' <?php echo count(old()) == 0 && isset($mealPriceData[$i]['available']['is_delivery']) && $mealPriceData[$i]['available']['is_delivery'] == 1 ? 'checked' : '' ?> >Is
                                                            Delivery</label>
                                                    </div>

                                                    <div class="hide" style="margin:auto;padding-top:auto;">
                                                        <label class="checkbox-inline"><input type="checkbox"
                                                                                              class="carryout_all"
                                                                                              name='carryout_all[{{$i}}]'

                                                            <?php echo count(old()) == 0 && isset($mealPriceData[$i]['available']['is_carryout']) && $mealPriceData[$i]['available']['is_carryout'] == 1 ? 'checked' : '' ?>
                                                            >Is Carryout</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">
                                                <label for="price"
                                                    class="col-form-label text-md-right">{{ __('Price') }}</label>
                                                <div class="row">

                                                    <div class="row hidden">
                                                        <div class="col-md-12 no-col-padding" style="margin:auto;">
                                                            <label class="radio-inline"> <input type='radio' name='type[{{$i}}]' class='meal_type'
                                                                data-val='{{$i}}'
                                                                value="1" {{old("type.".$i)=="1" || (count(old())==0 && isset($mealPriceData[$i]['price'][0])) ? 'checked="checked"' : ''}} >
                                                            All</label>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="no-col-padding">
                                                            <div class="input__field">
                                                                <input class="price_all" placeholder='Price'
                                                                    type='text' size='4' id='price_all'
                                                                    name='price_all[{{$i}}]'
                                                                    value="{{(count(old())==0 && isset($mealPriceData[$i]['price'][0])) ? $mealPriceData[$i]['price'][0] : old("price_all.".$i)}}"
                                                                >
                                                            </div>
                                                            @if ($errors->has('price_all.'.$i))
                                                                <span class="invalid-feedback" style="display: block;">
                                                            <strong>{{ $errors->first('price_all.'.$i) }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                            
                                                    

                                                    <div class="row hidden">
                                                        <div class="col-md-8 no-col-padding" style="padding:auto;margin-top:10px;">
                                                            <label class="radio-inline"><input type='radio' name='type[{{$i}}]' class='meal_type'
                                                                data-val='{{$i}}' value='0'
                                                                    {{(old("type.".$i)=="0" || (count(old())==0 && isset($mealPriceData) && !isset($mealPriceData[$i]['price'][0]))) ? 'checked="checked"' : ''}} >&nbsp;By
                                                            Meal Type</label>
                                                        </div>
                                                    </div>

                                                    @if ($errors->has('type.'.$i))
                                                        <div class="col-md-8">
                                                                <span class="invalid-feedback" style="display: block;">
                                                                    <strong>{{ $errors->first('type.'.$i) }}</strong>
                                                                </span>
                                                        </div>
                                                    @endif
                                                    <div class="row col-md-12"
                                                        style="padding-top: 10px; {{(old("type.".$i)=='0' || (count(old())==0 && isset($mealPriceData) && !isset($mealPriceData[$i]['price'][0]))) ? '' : 'display:none;'}}"
                                                        id="meal_type_{{$i}}">
                                                        {{--@if(session()->has('menuTypeData'))
                                                            @php
                                                                $menuTypeData = session()->get('menuTypeData');
                                                            @endphp--}}
                                                        @foreach($menuTypeData as $mtype)
                                                            <div class="row">
                                                                <div class="row">
                                                                    <div class="col-md-12"
                                                                        style="margin:auto;padding-top:auto;">
                                                                        <input type='checkbox'
                                                                            name='meal_type[{{$i}}][{{$mtype->id}}]'
                                                                            value="{{$mtype->id}}" {{(old("meal_type.".$i.'.'.$mtype->id) || ( count(old())==0 && array_key_exists($mtype->id,$mealPriceData[$i]['price'])))? 'checked=checked' : ''}} > {{$mtype->name}}
                                                                        &nbsp;
                                                                    </div>
                                                                </div>
                                                                <div style="margin-top: 5px;" class="row">

                                                                    <div class="col-md-12">
                                                                        <div class="input__field">
                                                                        <input class=""
                                                                                                placeholder='Price'
                                                                                                type='text'
                                                                                                size='4'
                                                                                                name='price[{{$i}}][{{$mtype->id}}]'
                                                                                                value="{{(count(old())==0 && array_key_exists($mtype->id,$mealPriceData[$i]['price'])) ? $mealPriceData[$i]['price'][$mtype->id] : old("price.".$i.'.'.$mtype->id)}}"
                                                                        >
                                                                        </div>
                                                                        @if ($errors->has('price.'.$i.'.'.$mtype->id))
                                                                            <span class="invalid-feedback"
                                                                                style="display: block;">
                                                                <strong>{{ $errors->first('price.'.$i.'.'.$mtype->id) }}</strong>
                                                                </span>
                                                                        @endif
                                                                    </div>
                                                                </div>


                                                            </div>

                                                            
                                
                                                        <label for="available"
                                                    class="col-md-4 col-form-label text-md-right">{{ __('Is Available') }}</label>
                                                            <div style="margin-top: 15px" class="row">
                                                                <div class="col-md-6" style="margin:auto;padding-top:auto;">
                                                                    <label class="checkbox-inline"><input type="checkbox"
                                                                                                        name="delivery[{{$i}}][{{$mtype->id}}]"
                                                                        <?php echo count(old()) == 0 && isset($mealPriceData[$i]['available'][$mtype->id]) && $mealPriceData[$i]['available'][$mtype->id]['is_delivery'] == 1 ? 'checked' : '' ?>
                                                                        >Is Delivery</label>
                                                                </div>
                                                                <div class="hide" style="margin:auto;padding-top:auto;display:none;">
                                                                    <label class="checkbox-inline"><input type="checkbox"
                                                                                                        name="carryout[{{$i}}][{{$mtype->id}}]" <?php echo count(old()) == 0 && isset($mealPriceData[$i]['available'][$mtype->id]) && $mealPriceData[$i]['available'][$mtype->id]['is_carryout'] == 1 ? 'checked' : '' ?> >Is
                                                                        Carryout</label>
                                                                </div>
                                                                <br/>
                                                            </div>
                                                        @endforeach
                                                        {{--@endif--}}
                                                        @if ($errors->has('meal_type.'.$i))
                                                            <span class="invalid-feedback" style="display: block;">
                                                                <strong>{{ $errors->first('meal_type.'.$i) }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- price -->

                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xs-12">
                                                <label for="size" class="col-form-label text-md-right">{{ __('Pos Id') }}</label>
                                                <div class="input__field">
                                                    <input type="text" placeholder="Pos Id" class="size_pos_id {{ $errors->has('pos_id.'.$i) ? ' is-invalid' : '' }}" name="size_pos_id[]" value="{{ (count(old())==0 && isset($mealPriceData[$i]['pos_id'])) ? $mealPriceData[$i]['pos_id'] : old('pos_id.'.$i) }}">
                                                </div>
                                                @if ($errors->has('pos_id.'.$i))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('pos_id.'.$i) }}</strong></span>
                                                @endif
                                            </div>

                                        </div>

                                        <div class="form-group row form_field__container no-padding-left no-padding-right">
                                            <div class="col-md-12">
                                                <a class="btn btn__cancel" href="javascript:void(0);" id="remove_div_{{$i}}" style="{{($i==0)?'display: none':''}}" onclick="remove_div_by_id({{$i}})">Delete</a>
                                            </div>
                                        </div>

                                        <div class="form-group row form_field__container no-padding-left no-padding-right">
                                            <label for="size" class="col-md-12 col-form-label text-md-right">{{ __('Happy Hours Price') }}</label>
                                            <div class="row">
                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <div class="input__field">
                                                    <input type="text" placeholder="Happy Hours Price" class="happy_hour_price {{ $errors->has('happy_hour_price.'.$i) ? ' is-invalid' : '' }}" name="happy_hour_price[]" value="{{ (count(old())==0 && isset($mealPriceData[$i]['happy_hour_price'])) ? $mealPriceData[$i]['happy_hour_price'] : old('happy_hour_price.'.$i) }}">
                                                </div>
                                                @if ($errors->has('happy_hour_price.'.$i))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('happy_hour_price.'.$i) }}</strong></span>
                                                @endif
                                                        </div>
                                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                                <div class="input__field">
                                                    <input type="text" placeholder="From Time" id="from_time" class="timepicker from_time{{$i}} {{ $errors->has('from_time.'.$i) ? ' is-invalid' : '' }} icocal" name="from_time[]" value="{{ (count(old())==0 && isset($mealPriceData[$i]['from_time'])) ? $mealPriceData[$i]['from_time'] : old('from_time.'.$i) }}">
                                                </div>
                                                @if ($errors->has('from_time.'.$i))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('from_time.'.$i) }}</strong></span>
                                                @endif
                                                        </div>
                                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                                <div class="input__field">
                                                    <input type="text" placeholder="End Time" id="to_time"  class="timepicker to_time{{$i}} {{ $errors->has('to_time.'.$i) ? ' is-invalid' : '' }} icocal" name="to_time[]" value="{{ (count(old())==0 && isset($mealPriceData[$i]['to_time'])) ? $mealPriceData[$i]['to_time'] : old('to_time.'.$i) }}">
                                                </div>
                                                @if ($errors->has('to_time.'.$i))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('to_time.'.$i) }}</strong></span>
                                                @endif
                                                        </div>
                                            </div>
					    <script>
						$(function () {
             					    var fTime = "{{ (count(old())==0 && isset($mealPriceData[$i]['from_time'])) ? $mealPriceData[$i]['from_time'] : '00:00' }}";
						    var tTime = "{{ (count(old())==0 && isset($mealPriceData[$i]['to_time'])) ? $mealPriceData[$i]['to_time'] : '00:00' }}";
						    var options1 = {now: fTime, twentyFour: true, showSeconds: true};
						    var options2 = {now: tTime, twentyFour: true, showSeconds: true};
						    $('.to_time{{$i}}').wickedpicker(options2);
						    $('.from_time{{$i}}').wickedpicker(options1);
						});
					    </script>
                                        </div>
                                    </div>
                                @endfor
                            @endif
                        </div>
                        {{--<div class="form-group row form_field__container">
                            <label id="meal_type_lab" for="Meal Type" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
                            <div class="row col-md-8">
                                <div class="col-md-2" style="margin:auto;">
                                    <input type='radio' name='type' value="1" {{$menuItem->price_flag=="1" ? 'checked="checked"' : ''}} >&nbsp;All
                                </div>
                                <div class="col-md-10">
                                    <input class="form-control" placeholder='Price' type='text' size='4' id='price_all' name='price_all' value="{{isset($mealPriceData[0])?$mealPriceData[0]:''}}" style="width: 25%">
                                    @if ($errors->has('price_all'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('price_all') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-8" style="padding:auto;margin-top:10px;">
                                    <input type='radio' name='type' value="0" {{$menuItem->price_flag=="0" ? 'checked="checked"' : ''}} >&nbsp;By Meal Type
                                </div>
                                @if ($errors->has('type'))
                                    <div class="col-md-8">
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('type') }}</strong>
                                        </span>
                                    </div>
                                @endif
                                <div class="col-md-8 row" style="padding-top: 10px; {{$menuItem->price_flag=="1" || old("type")=="1"?'display:none;':''}}" id="meal_type">
                                    @foreach($menuTypeData as $mtype)

                                        <div class="col-md-4" style="margin:auto;"> <input type='checkbox'  name='meal_type[{{$mtype->id}}]' value="{{$mtype->id}}"  {{array_key_exists($mtype->id,$mealPriceData)? 'checked="checked"' : ''}} >&nbsp&nbsp;{{$mtype->name}}&nbsp;</div>
                                        <div class="col-md-8"><input placeholder='Price' class="form-control" type='text' size='4' name='price[{{$mtype->id}}]' value="{{array_key_exists($mtype->id,$mealPriceData)? $mealPriceData[$mtype->id] : ''}}" style="width: 50%">
                                            @if ($errors->has('price.'.$mtype->id))
                                                <span class="invalid-feedback" style="display: block;">
                                                    <strong>{{ $errors->first('price.'.$mtype->id) }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    @endforeach
                                    @if ($errors->has('meal_type'))
                                        <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('meal_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>--}}

                        <div class="hide" style="text-align: right;margin-right: 0%;display:none;">
                            <div class="col-xs-12">
                                <input type="button" value="Add" class="btn btn__primary" onclick="add_size_div()">&ensp;
                                <input type="button" value="Delete" class="btn btn__cancel" onclick="remove_size_div()">
                            </div>
                        </div>
                        <br/><br/>
                        @foreach($menuSettingData as $menuSet)
                            @php
                                $menuCustData = $menuItem->customizable_data ?? '';
                                $menuCustIndex = false;
                                if($menuCustData)
                                    $menuCustIndex = array_search($menuSet['id'], array_column($menuCustData, 'id'));
                            @endphp
                            <div class="form-group row form_field__container newtable" style="padding: 0px;">
                                <label for="image"
                                       class="col-xs-12 col-form-label text-md-right">{{ __(ucwords($menuSet['name'])) }}</label>
                                <div class="col-xs-12">
                                    <table class="table-bordered" style="width:100%;">
                                        <tr>
                                            <td><input type="checkbox" name="{{ $menuSet['id'].'_' }}is_mandatory"
                                                       value="1" @if($menuCustIndex!==false && $menuCustData[$menuCustIndex]['is_mandatory']){{ 'checked="checked"' }} @endif>{{ __('Is mandatory') }}
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @foreach($menuSet['items'] as $item)
                                            @php
                                                $menuCustItemData = '';
                                                $menuCustItemIndex = false;
                                                if($menuCustIndex !== false) {
                                                    $menuCustItemIndex = array_search($item['id'], array_column($menuCustData[$menuCustIndex]['items'], 'id'));
                                                    if($menuCustItemIndex !== false) {
                                                        $menuCustItemData = $menuCustData[$menuCustIndex]['items'][$menuCustItemIndex];
                                                    }
                                                }
                                            @endphp
                                            <tr>
                                                <td><input type="checkbox" name="{{ $menuSet['name'] }}[]"
                                                           value="{{ $item['id'] }}" @if($menuCustItemIndex!==false && $menuCustItemData['id'] == $item['id']){{ 'checked="checked"' }} @endif>{{ __(ucwords($item['name'])) }}
                                                </td>
                                                <td>
                                                    @foreach($hriSides as $side)
                                                        @php
                                                            $sideIndex = false;
                                                            if($menuCustItemData && isset($menuCustItemData['sides'])) {
                                                                $sideIndex = array_search($side, array_column($menuCustItemData['sides'], 'label'));
                                                            }
                                                        @endphp
                                                        <input type="checkbox" name="{{ $item['id'].'_'.$side }}"
                                                               value="{{ $side }}"
                                                               ch="{{ $sideIndex }}" @if($sideIndex!==false && isset($menuCustItemData['sides']) && $menuCustItemData['sides'][$sideIndex]['is_selected']==1){{ 'checked="checked"' }} @endif>{{ __(ucwords($side)) }}
                                                        <br>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach($hriQuant as $quant)
                                                        @php
                                                            $quantIndex = false;
                                                            if($menuCustItemData && isset($menuCustItemData['quantity'])) {
                                                                $quantIndex = array_search($quant, array_column($menuCustItemData['quantity'], 'label'));
                                                            }
                                                        @endphp
                                                        <input type="checkbox" name="{{ $item['id'].'_'.$quant }}"
                                                               value="{{ $quant }}"
                                                               ch="{{ $quantIndex }}" @if($quantIndex!==false && isset($menuCustItemData['quantity']) && $menuCustItemData['quantity'][$quantIndex]['is_selected']==1){{ 'checked="checked"' }} @endif>{{ __(ucwords($quant)) }}
                                                        <br>
                                                    @endforeach
                                                </td>
                                                <td><input type="checkbox" name="{{ $item['id'].'_' }}default_selected"
                                                           value="1" @if($menuCustItemIndex!==false && $menuCustItemData['default_selected']){{ 'checked="checked"' }} @endif>{{ __('Default Selected') }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        @php
                                            $menuCustIndex = false;
                                        @endphp
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    @if ($errors->has('image'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endforeach

                        <div class="row  mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                        </form>
                </div>
                <div id="schedule" class="tab-pane fade">
                    <div class="row rowpb">
                        <div class="col-md-6 "> <h3>Schedule</h3></div>
                        <div class="col-md-6" style="text-align: right">
                            <button type="button" class="btn btn__primary">Add/Update Menu Item Schedule</button>
                        </div>
                    </div>
                    <div class="row rowpb">
                        <div class="col-md-4 "> <p id="visibility_schedule">
                            <select id="visibility_schedule_id" class="form-control">
                                <option value="">--Visibility Schedule--</option>
                            </select>
                            </p></div>
                    </div>
                    <!--data-toggle="modal" data-target="#addvisibilityschedule"-->

                </div>
                <div id="modifiers" class="tab-pane fade">
                    <?php
                    $modifer_size='';
                    if(!empty($menuItem->size_price)){
                        $allsizes=json_decode($menuItem->size_price,true);

                        foreach($allsizes as $size){
                            if(!empty($size['size'])){
                                $selected='';
                              $option= "<option value='".$size['size']."'>".$size['size']."</option>";

                              //  $option= "<option  ".$selected." value='".$size['size']."'>".$size['size']."</option>";

                                $modifer_size.=$option;
                            }
                        }
                    }
                    ?>
             @include('menumanagement::menu_item.modifier_render')


</div>   
  

                <div id="addons" class="tab-pane fade">
                    <div class="row rowpb">
                        <div class="col-md-6"> <h3>Add-ons</h3></div>
                        <div class="col-md-6" style="text-align: right">
                            <button type="button" class="btn btn__primary" data-toggle="modal" data-target="#addongroup">Add New Addon Group</button>
                        </div>
                    </div>
                    <div class="panel-group text-center" id="accordion" role="tablist" aria-multiselectable="true">
                        Loading...
                    </div>

                </div>
                    
                <div id="labels" class="tab-pane fade">
                <h3>Labels</h3>
                    <div class="label__wrapper">

                        <div class="label__container">
                            @foreach($allLabelsData as $key => $labelData)
                                @php
                                    $labelCat = 'Others';
                                    if($key == 'dietary') {
                                        $labelCat = 'Dietary Specification';
                                    } elseif($key == 'food_type') {
                                        $labelCat = 'Food Type';
                                    }elseif($key == 'Ingredients') {
                                        $labelCat = 'Ingredients';
					 
                                    }
                                    if(count($servingSize)==2) {
                                        $serMin = $servingSize[0];
                                        $serMax = $servingSize[1];
                                    } else {
                                        $serMin = 1;
                                        $serMax = 1;
                                    }
                                @endphp
                                <div class="label__heading ptb"><b>{{ $labelCat }}</b></div>
                                <div class="label__switchContainer">
                                    @foreach($labelData as $label)
                                        @if($label['name'] == 'Serving Size')
							
                                            <div class="label__serves">
                                                <div class="label__switchHead ptb">{{ $label['name'] }}</div>
                                                <div class="label__slider row">
                                                    <div class="col-xs-5 col-sm-2 col-md-1 no-col-padding"><input value="{{ $serMin }}" id="serving_size_min" type="number" max="10" class="form-control"></div>
                                                    <div class="col-xs-2 col-sm-2 col-md-1 no-col-padding text-center" style="padding-top: 5px"><span>to</span></div>
                                                    <div class="col-xs-5 col-sm-2 col-md-1 no-col-padding"><input value="{{ $serMax }}" id="serving_size_max" type="number" max="10" class="form-control"></div>
                                                </div>
                                            </div>
                                        @else
					   <?php  $label['priority'] = isset($label['priority'])?$label['priority']:0;
					     if($labelCat == 'Ingredients') {
						//$label['priority'] = isset($label['priority'])?$label['priority']:0;
                                         	$inputBox = ' - <input class="ingId" type="text" data-id="'.$label['id'].'" name="ingId['.$label['id'].']" value="'.$label['priority'].'" style="width:30px;">';
                                    	      }else{
						$inputBox = '<input class="hidden ingId" type="text" data-id="'.$label['id'].'" name="ingId['.$label['id'].']" value="'.$label['priority'].'" style="width:30px;">';
					      } ?>
                                            @php
                                                $activeClass = '';
                                                $ariaPressed = false;
                                                $labelVal = 0;
                                                if(in_array($label['id'], $menuLabels)) {
                                                    $activeClass = 'active';
                                                    $ariaPressed = true;
                                                    $labelVal = 1;
                                                }
                                            @endphp
                                            <div class="label__switch">
                                                <div class="label__switchHead col-lg-6">{{ $label['name'] }} <?php echo $inputBox?></div>
                                                <div class="label__switchToggle col-lg-6">
                                                    <div class="waitList__otp"> <div class="toggle__container">
                                                        <button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom {{$activeClass}}" data-toggle="button" aria-pressed="{{ $ariaPressed }}" autocomplete="off">
                                                            <div class="handle"></div>
                                                        </button>
                                                        <input type="hidden" name="{{ $label['id'] }}" class="label_field" value="{{ $labelVal }}">
                                                        </div> </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <button type="button" class="btn btn__primary" id="old_menu_label_save" >Save Label</button>
                </div>
                <div id="images" class="tab-pane fade">
                <h3>Images</h3>
                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                </div>
                </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



                                </div>
                            </div>


                            <br/><br/>
                            <div class=" mb-0 ">
                                <div class="col-md-12 text-center">
                                </div>
                            </div>
        </div>
        <!-- <div class="modal-footer">
             <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
            </form>
      </div>
      
    </div>
  </div>

    <div class="modal fade" id="addvisibilityschedule" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Visibility Schedule</h4>
                </div>
                <div class="modal-body" style="max-height:400px; min-height: 200px; overflow-y: scroll;">
                    <form method="POST" id="additemsvisibilityform" action="{ { route('menu-items-visibility.store') } }" enctype="multipart/form-data">
                        loading...
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="saveVisibilityItems">Add</button>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="addongroup" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form method="POST" name="addongroup_form" id="addongroup_form" action="{{ route('item-addon-groups.store') }}/add" enctype="multipart/form-data">
                    <input type="hidden" value="{{$menuItem->restaurant_id}}" name="restaurant_id" />
                    <input type="hidden" value="{{$menuItem->language_id}}" name="language_id" />
                    <input type="hidden" value="{{$menuItem->id}}" name="menu_item_id" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Addon Group </h4>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="group_name" type="text"
                                           class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
                                           name="group_name" value="{{ old('group_name') }}" required>
                                </div>
                                @if ($errors->has('group_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('group_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Prompt') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="prompt" type="text"
                                           class="{{ $errors->has('prompt') ? ' is-invalid' : '' }}"
                                           name="prompt" value="{{ old('prompt') }}" required>
                                </div>
                                @if ($errors->has('prompt'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('prompt') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Required') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="status1" name="is_required" value="1" checked=""  > Yes
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="status0" name="is_required" value="0" {{ (old('is_required')=='0') ? 'checked' :'' }}> No
                                @if ($errors->has('is_required'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('is_required') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="quantity_type_addongroup"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Quantity Type') }}</label>
                            <div class="col-md-3 quantity_type_select">
                                <select name="quantity_type" id="quantity_type_addongroup"
                                        class="quantity_type form-control{{ $errors->has('quantity_type') ? ' is-invalid' : '' }}"
                                        required>
                                    <option value="">--Select--</option>
                                    <option value="Value">Value</option>
                                    <option value="Range">Range</option>
                                </select>

                                @if ($errors->has('quantity_type'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('quantity_type') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-3 quantity_type_val">
<!--                                <div class="" >
                                    <div class="input__field">
                                        <input id="min" type="text"
                                               class="{{ $errors->has('min') ? ' is-invalid' : '' }}"
                                               name="min" value="{{ old('min') }}" required placeholder="Min">
                                    </div>
                                    <div class="input__field">
                                        <input id="max" type="text"
                                               class="{{ $errors->has('max') ? ' is-invalid' : '' }}"
                                               name="max" value="{{ old('max') }}" placeholder="Max">
                                    </div>
                                </div>-->
                                <div class="">
                                    <div class="input__field">

                                        <input id="quantity" type="text"
                                               class="{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
                                               name="quantity" value="{{ old('quantity') }}" required placeholder="Quantity">
                                    </div>

                                </div>

                            </div>
                        </div>

                        <br/><br/>
                        <div class=" mb-0 ">
                            <div class="col-md-12 text-center">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="addAddonItemsGroupsBtn" class="btn btn__primary">{{ __('Save') }}</button>
                    </div>
                </form>

            </div>

        </div>
    </div>

    <div class="modal fade" id="additem" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Addon Item</h4>
                </div>
                <div class="modal-body" style="max-height:400px; min-height: 200px; overflow-y: scroll;">
                    <form method="POST" id="additemform" action="{{ route('item-addons.store') }}" enctype="multipart/form-data">
                        loading...
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="saveAddonItemsToGroup">Add</button>
                </div>
            </div>

        </div>
    </div>

    <input type="hidden" id="menuItemId" value="{{ $menuItem->id }}" />
    <input type="hidden" id="menuItemRestaurantId" value="{{ $menuItem->restaurant_id }}" />
      
 
@include('menumanagement::menu_item.item_options')
<script type="text/javascript">
      $("#product_type").change(function () {
           var type = $(this).val();
           if(type=='product'){
            $('.product_type_product').removeClass('hide');

           }else{
             $('.product_type_product').addClass('hide');

           }
    });  
        var size_div_cnt = 0;
        size_div_cnt = '<?php echo (isset($sizeCount) && $sizeCount > 0) ? ($sizeCount - 1) : 0 ?>';
        size_div_cnt = parseInt(size_div_cnt);

        function remove_div_by_id(id) {
            var div_cnt = $('.size_div').length;
            if (div_cnt > 1) {
                $('#size_div_' + id).remove();
            }
        }

        function add_size_div() {
            var restId = $('#restaurant_id :selected').val();
            if (restId == '') {
                alert('Please select Restaurant');
                return;
            }
            //var div_html = $('#size_div_0').html();

            var div_id_str = $('.size_div:first').attr('id');
            var div_id = div_id_str.slice(div_id_str.lastIndexOf("_") + 1);
            var div_html = $('#' + div_id_str).html();

            size_div_cnt = size_div_cnt + 1;
            $('#size_div').append("<div id='size_div_" + size_div_cnt + "' class='size_div' style='border: 1px solid #ccc;padding: 10px 0;margin: 10px 0;'></div>");
            var newIndex = '[' + size_div_cnt + ']';
            var newDataVal = "data-val='" + size_div_cnt + "'";
            div_html = div_html.replace(/\[0\]/g, newIndex);
            div_html = div_html.replace(/data-val=\"0\"/g, newDataVal);

            div_html = div_html.replace('meal_type_' + div_id, 'meal_type_' + size_div_cnt);
            div_html = div_html.replace('remove_div_' + div_id, 'remove_div_' + size_div_cnt);
            div_html = div_html.replace('remove_div_by_id(' + div_id + ')', 'remove_div_by_id(' + size_div_cnt + ')');

            //div_html = div_html.replace('meal_type_0', 'meal_type_'+size_div_cnt);
            $('#size_div_' + size_div_cnt).html(div_html);
            $('#size_div_' + size_div_cnt).find('.size').val('');
            $('#size_div_' + size_div_cnt).find('.price_all').val('');
            $('#size_div_' + size_div_cnt).find('.meal_type').prop('checked', false);
            $('#meal_type_' + size_div_cnt).hide();
            $('#remove_div_' + size_div_cnt).show();
            $('.datetimepicker').datetimepicker(commonOptions);

        }

        function remove_size_div() {
            /*if (size_div_cnt > 0) {
                $('#size_div_' + size_div_cnt).remove();
                size_div_cnt = size_div_cnt - 1;
            }*/
            var div_cnt = $('.size_div').length;
            if (div_cnt > 1) {
                $('.size_div:last').remove();
            }
        }

        function remove_all_size_div() {
            for (i = 1; i <= size_div_cnt; i++) {
                $('#size_div_' + i).remove();
            }
            $('#meal_type_0').html('');
            $('#size_div_0').find('.price_all').val('');
            $('#size_div_0').find('.meal_type').prop('checked', false);
            $('#meal_type_0').hide();
        }

        $(function () {
            $('body').on('click', '.meal_type', function () {
                var mealTypeVal = $(this).val();
                var idx = $(this).attr('data-val');
                if (mealTypeVal == '1') {
                    $('#meal_type_' + idx).find('input[type=checkbox]:checked').removeAttr('checked');
                    $('#meal_type_' + idx).find('input[type=text]').val('');
                    $('#meal_type_' + idx).hide();
                }
                else {
                    $('#size_div_' + idx).find('.price_all').val('');
                    $('#size_div_' + idx).find('.carryout_all:checked').removeAttr('checked');
                    $('#size_div_' + idx).find('.delivery_all:checked').removeAttr('checked');
                    $('#meal_type_' + idx).show();
                }
            });

            /*$('input[name=type]').click(function(){
                var typeVal = $('input[name=type]:checked').val();
                if(typeVal=='1') {
                    $('#meal_type').find('input[type=checkbox]:checked').removeAttr('checked');
                    $('#meal_type').find('input[type=text]').val('');
                    $('#meal_type').hide();
                }
                else {
                    $('#price_all').val('');
                    $('#meal_type').show();
                }
            });*/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $('option:selected').val();
                if (rest_id != '') {
                    $('#menu_category_id').find('option').not(':first').remove();
                    remove_all_size_div();
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_category_and_type"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj.cat) !== 'undefined') {
                                    $.each(data.dataObj.cat, function (i, item) {
                                        $('#menu_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    });
                                }
                                if ($.type(data.dataObj.type) !== 'undefined') {
                                    var fielsStr = '';
                                    $.each(data.dataObj.type, function (i, item) {
                                        fielsStr = fielsStr + "<div class=\"col-md-4\" style=\"margin:auto;padding-top:auto;\"><input type='checkbox' style=\"margin:auto;\" name='meal_type[0][" + item.id + "]' value='" + item.id + "'>&nbsp;" + item.name + "</div><div class=\"col-md-6\"><div class=\"input__field\"><input placeholder='Price' type='text' size='4' class=\"form-control\" name='price[0][" + item.id + "]' style=\"\"></div></div>";
                                    });
                                    $("#meal_type_0").html(fielsStr);
                                }
                            }
                        }
                    });
                }
            });


            // change sub-category @21-09-2018 by RG


             $("#menu_category_id").change(function () {
                var rest_id = $('#restaurant_id').val();
                var cat_id = $(this).val();
                console.log(rest_id + ' - ' + cat_id);
                $('#menu_sub_category_id').find('option').not(':first').remove();
                if (rest_id != '' && cat_id != '') {
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_customization_options"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id + '&cid=' + cat_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                               
                                // set sub categories
                                $.each(data.dataObj.sub_categories, function (i, item) {
                                  //  $('#menu_sub_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    $('#menu_sub_category_id').html(data.dataObj.subcat_options);
                                });
                            
                            }
                        }
                    });
                }
            });

            // check uncheck the checkbox
            $("input[type=checkbox]").on("click", function () {
                if ($(this).prop('checked') == true) {
                    $(this).attr('checked', 'checked');
                } else {
                    $(this).removeAttr('checked');
                }
            });

        });



$('.add_new_image').click(function(){
    var type=$.trim($(this).attr('imagetype'));
    var filename=type+'[]';
    var lastsib= $(this).parents('.image-caption-div').find(".image_cont_main:last");
    var str='<div class="image_cont_main row rowpb"><div class="col-md-9 no-col-padding"> <div class="col-md-9 no-col-padding"> <input id="'+type+'" type="file" class="form-control" name="'+filename+'"> <div class="input__field"> <input id="image_caption" type="text" name="'+type+'_caption[]" placeholder="Image Caption"> </div> </div> <div class="col-md-3 nomobtab"> <input type="button" value="Delete" class="btn btn__cancel deleteImage" > </div> </div> </div>';
     lastsib.after(str);
})
$(document).on('click','.deleteImage',function(){
 $(this).parents('.image_cont_main').remove();
});


function modifierCategoriesDropdown(ModifierCategories,modifiergroup_id,modifier_item_count){

      var catname=modifiergroup_id!=undefined? "modifiergroup["+modifiergroup_id+"][modifier_items]["+modifier_item_count+"][modifier_category]":'modifiercat_';
      var select_str='<select name="'+catname+'" class="form-control" required>';
      
      $.each( ModifierCategories, function( key, value ) {       
          select_str+=' <option    value="'+value.id+'">'+value.category_name+'</option>'; 
      });     
      
      select_str+='</select>';
      return  select_str;
}

$(document).on('click','.add_modifier',function(){

    var modifiergroup=$(this).attr('modifiergroup');
 
    var modifer_group_list=$(this).siblings('.modifer_group_list');
    var modifier_categories=JSON.parse('<?php echo json_encode($ModifierCategories);?>');
     

    var modifier_item_count=modifer_group_list.find('tbody tr').length;
    
    var modifier_name="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][modifier_name]";
    var modifier_price="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][modifier_price]";


     var modifier_item_cat="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][modifier_category]";
   //var modifier_item_cat="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][modifier_category]";  

    var modifier_item_status="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][status]";	
    var modifier_size="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][modifier_size]";
    //var modifier_calorie="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][calorie]";	
    var sort_order="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][sort_order]";
    var dependent_modifier_item_id="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][dependent_modifier_item_id]";
    //var calorie="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][calorie]";
    var is_selected="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][is_selected]";
    var description="modifiergroup["+modifiergroup+"][modifier_items]["+modifier_item_count+"][description]";	

    var modifier_size='<select name="'+modifier_size+'" class="form-control"><option value="">Choose Size</option><?php echo addslashes($modifer_size); ?></select>';
    var modifier_status='<select name="'+modifier_item_status+'" class="form-control"><option value="">Choose Status</option><option value="1">Active</option> <option value="">InActive</option> </select>';	

    if(modifer_group_list.length){
       var data='<tr> <td> <div class="form-group">-</div><input type="hidden" name="'+modifier_item_cat+'" value="0"> </td> <td><div class="form-group"><input type="text" class="form-control" name="'+modifier_name+'" placeholder="Modifier Name" value=""></div> </td> <td><div class="form-group">'+modifier_size+'</div></td><td><div class="form-group"><input type="text" class="form-control" name="'+description+'" placeholder="Description" value=""></div></td> <td><div class="form-group"><input type="text" class="form-control" name="'+modifier_price+'" placeholder="Price" value=""></div></td> <td><div class="form-group"><input type="text" class="form-control" name="'+dependent_modifier_item_id+'" placeholder="Dependent Modifier Item Id" value=""></div></td><td><div class="form-group"><input type="number" class="form-control" name="'+sort_order+'" placeholder="Sort Order" value=""></div></td> <td><div class="form-group"><input type="checkbox" class="form-control" name="'+is_selected+'" value="1" ></div></td> <td><div class="form-group">'+modifier_status+'</div></td><td><i class="fas fa-times modifier_item_temp_dlt"></i> <a href="#"></a></td> </tr>';		
      modifer_group_list.find('tr:last').after(data);

    }else{
       var data='<table class="table table-striped modifer_group_list"> <thead> <tr> <th>Modifier Id</th> <th>Modifier Name</th> <th>Size</th><th>Price</th> <th>POS Id</th><th>Sort Order</th><th>Is selected</th>  <th>Status</th><th>Action</th> </tr> </thead> <tbody> <tr> <td> <div class="form-group">-</div> <input type="hidden" name="'+modifier_item_cat+'" value="0"></td> <td><div class="form-group"><input type="text" class="form-control" name="'+modifier_name+'" placeholder="Modifier Name" value=""></div> </td><td><div class="form-group">'+modifier_size+'</div></td><td><div class="form-group"><input type="text" class="form-control" name="'+description+'" placeholder="Description" value=""></div></td><td><div class="form-group"><input type="text" class="form-control" name="'+modifier_price+'" placeholder="Price" value=""></div></td><td><div class="form-group"><input type="text" class="form-control" name="'+dependent_modifier_item_id+'" placeholder="Dependent Modifier Item Id" value=""></div></td> <td><div class="form-group"><input type="number" class="form-control" name="'+sort_order+'" placeholder="Sort Order" value=""></div></td> <td><div class="form-group"><input type="checkbox" class="form-control" name="'+is_selected+'" value="1" ></div></td> <td><div class="form-group">'+modifier_status+'</div></td><td><i class="fas fa-times modifier_item_temp_dlt"></i> <a href="#"></a></td> </tr> </tbody> </table>';
       $( this).prev('.form_field__container').after( data);

    }
    $('.datetimepicker').datetimepicker(commonOptions);


});

$("#addmodifieroptionsSave").click(function(){
  //console.log($('#modifieroptiongroup_form').serialize());
  $.ajax({
      type: 'POST',
      url: '<?php echo URL::to('/') . "/menu/item/modifieritem/options/add"; ?>',
      data: $('#modifieroptiongroup_form').serialize(),
      success: function (data) {
       	$('.optionlist').html("<tr><td width='25%'><b>ID</b></td><td width='25%'><b>Name</b></td><td width='40%'><b>Dependent Modifier Id</b></td><td width='40%'><b>Action</b></td></tr>");
	       $.each(data.data, function (i, item) {
		    $('.optionlist').append(" <tr><td>" + item.id + "</td><td>" + item.option_name + "</td><td>" + item.dependent_modifier_id + "</td><td>" + item.dependent_modifier_id + "</td></tr>"); 
	       });
        }
  });              
});
function setModifierItemId(id){
	$("#modifierItemId").val(id);
	$.ajax({
      	type: 'GET',
      	url: '<?php echo URL::to('/') . "/menu/item/modifieritem/options/list/"; ?>'+id,
      	success: function (data) {
	if(data.count>0){
       	$('.optionlist').html("<tr><td width='25%'><b>ID</b></td><td width='25%'><b>Name</b></td><td width='40%'><b>Dependent Modifier Id</b></td><td width='40%'><b>Action</b></td></tr>");	
       		$.each(data.data, function (i, item) {
            		$('.optionlist').append(" <tr><td>" + item.id + "</td><td>" + item.option_name + "</td><td>" + item.dependent_modifier_id + "</td><td>" + item.dependent_modifier_id + "</td></tr>"); 
		 
       		});
       
       }else{
	//$('#modifieroptiongroup_form').reset();
	$('.optionlist').html('');
       }
      }
      
  });
}
</script>
<script src="{{ asset('/js/addon_group.js') }}"></script>

  <style>
 .range_quantity,.value_quantity{
    display:none;
}
 #searchiteminput {
    background-image: url('/css/searchicon.png'); /* Add a search icon to input */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
}

#myUL {
    /* Remove default list styling */
    list-style-type: none;
    padding: 0;
    margin: 0;
}

#myUL li a {
    border: 1px solid #ddd; /* Add a border to all links */
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6; /* Grey background color */
    padding: 12px; /* Add some padding */
    text-decoration: none; /* Remove default text underline */
    font-size: 18px; /* Increase the font-size */
    color: black; /* Add a black text color */
    display: block; /* Make it into a block element to fill the whole list */
}

#myUL li a:hover:not(.header) {
    background-color: #eee; /* Add a hover effect to all links, except for headers */
}

.seachitemcheckbox{
    padding-right: 13px;
}

.delete_edit .fa{
    padding-right: 13px;
}

        
        .panel-title > a:before {
    float: right !important;
    font-family: FontAwesome;
    padding-right: 5px;
}
.panel-title .delete_edit {
    float: right !important;
}
.panel-title > a:hover, 
.panel-title > a:active, 
.panel-title > a:focus  {
    text-decoration:none;
}

.submitbtntopmargin{margin-top: 40px;}

    </style>

    <script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
        tinymce.init({
            selector: 'textarea.editor',
            menubar:false,
            height: 320,
            theme: 'modern',
            plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });

        $('#old_menu_label_save').on('click', function() {  
            console.log('labels');
            var label_ids = '';
            $('.label_field').each(function() {
                if($(this).val() == "1"){
                    label_ids += $(this).attr('name') +',';
                }
            });
            label_ids = label_ids.replace(/,\s*$/, "");
            var serving_size_min = $('#serving_size_min').val() ? $('#serving_size_min').val() : 0;
            var serving_size_max = $('#serving_size_max').val() ? $('#serving_size_max').val() : 0;
	    var ingId = new Array();
		$(".ingId").each(function() {
		    var k = parseInt($(this).attr('data-id'));
		    var v = parseInt($(this).val());
		    if(v>0 && v!='')ingId[k]=v;		
		    //console.log(k+"=>"+v)
	 	}); 
	    //var ingId = $('#ingId').val() ? $('#ingId').val() : 0;
            $.ajax({
                type: 'POST',
                url: '/menu/save_label_data',
                data: {
                    item_id:            $('#menuItemId').val(),
                    label_ids:          label_ids,
                    serving_size_min:   serving_size_min,
                    serving_size_max:   serving_size_max,
		    ingId : ingId	
                },
                success: function (data) {
                    console.log(data);
                    location.reload();
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                    });
                },
                complete: function(data) {
                    console.log('complete');
                }
            });

        });
    });
     	
</script>
 
@include('menumanagement::common.include')
<style>
    .label__switch {
        float: left;
        width: 20%;
        /* padding-bottom: 23px; */
    }
    .toggle__container {
        padding-top: 5px;
    }
    .label__heading {
        clear: both;
    }
    .label__serves {
        clear: both;
        margin-bottom: 15px;
        font-weight: bold;
    }
</style>
@endsection
