    <div class="modal fade" id="modifieroptiongroup" role="dialog" style="z-index: 9999;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Modifier Option Group</h4>
        </div>
                   <form method="POST" id="modifieroptiongroup_form" action="{{ '/menu/item/modifieroptiongroup' }}" enctype="multipart/form-data">

        <div class="modal-body">
                            @csrf
                       
                            <input type="hidden" id="modifier_item_id" name="modifier_item_id" value="">
                            <div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="group_name" type="text"
                                               class=""
                                               name="group_name" value="{{ old('group_name') }}" required>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="form-group row form_field__container">
                                <label for="priority"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Prompt') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="prompt" type="text"
                                               class=""
                                               name="prompt" value="{{ old('prompt') }}" required>
                                    </div>
                                    
                                </div>
                            </div>
                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Required') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <input type="radio" id="status1" name="is_required" value="1" checked=""  > Yes
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="status0" name="is_required" value="0" > No
                              
                            </div>
                        </div>
                        
                         <div class="form-group row form_field__container">
                                <label for="quantity_type_addongroup"
                                       class="col-md-3 col-form-label text-md-right">{{ __('Quantity Type') }}</label>
                                <div class="col-md-3 quantity_type_select">
                                    <select name="quantity_type" id="quantity_type_modifiergroup"
                                            class="quantity_type form-control"
                                            required>
                                        <option value="Value">Value</option>
                                        <option value="Range">Range</option>
                                      
                                    </select>
                                  
                                   
                                </div>
                                       <div class="col-md-2 quantity_type_val">
                               
                                     <div class="value_quantity1">
                                             <div class="">

                                            <input id="quantity" type="text"
                                                   class="quantity"
                                                   name="quantity" value="" required placeholder="Min,Max">
                                             
                                           </div>
                                        
                                  </div>

                                    
                                </div>
                            </div>


                            <br/><br/>
                            <div class=" mb-0 ">
                                <div class="col-md-12 text-center">
                                </div>
                            </div>
        </div>
        <div class="modal-footer">
             <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
            </form>
      </div>
      
    </div>
  </div>



    <div class="modal fade" id="modifiergroup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Modifier Group</h4>
        </div>
                   <form method="POST" id="modifiergroup_form" action="{{ route('modifier-group.store') }}" enctype="multipart/form-data">

        <div class="modal-body">
                            @csrf
                       
                            <input type="hidden" name="restaurant_id" value="{{@$menuItem->restaurant_id}}">
                            <input type="hidden" name="menu_id" value="{{$item_id}}">
                            <div class="form-group row form_field__container">
                                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="group_name" type="text"
                                               class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
                                               name="group_name" value="{{ old('group_name') }}" required>
                                    </div>
                                    @if ($errors->has('group_name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row form_field__container">
                                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Prompt') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="prompt" type="text"
                                               class="{{ $errors->has('prompt') ? ' is-invalid' : '' }}"
                                               name="prompt" value="{{ old('prompt') }}" required>
                                    </div>
                                    @if ($errors->has('prompt'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('prompt') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{--<div class="form-group row form_field__container">
                                <label for="pos_id" class="col-md-4 col-form-label text-md-right">{{ __('POS Id (Modifier)') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}" name="pos_id" value="{{ old('pos_id') }}" >
                                    </div>
                                    @if ($errors->has('pos_id'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('pos_id') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row form_field__container">
                                <label for="calorie" class="col-md-4 col-form-label text-md-right">{{ __('Calorie') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
                                        <input id="calorie" type="text" class="{{ $errors->has('calorie') ? ' is-invalid' : '' }}" name="calorie" value="{{ old('calorie') }}" >
                                    </div>
                                    @if ($errors->has('calorie'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('calorie') }}</strong></span>
                                    @endif
                                </div>
                            </div>--}}

                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Required') }}</label>
                            <div class="col-md-4" style="padding-top: 10px;">
                                <label class="radio-inline"><input type="radio" id="status1" name="is_required" value="1" checked=""  > Yes</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="radio-inline"><input type="radio" id="status0" name="is_required" value="0" {{ (old('is_required')=='0') ? 'checked' :'' }}> No</label>
                                @if ($errors->has('is_required'))
                                <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('is_required') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                                <label for="status"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                                <div class="col-md-3 quantity_type_select">
                                    <select name="status" id="status"
                                            class="quantity_type form-control{{ $errors->has('status') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="1">Active</option>
                                        <option value="0">InActive</option>
                                      
                                    </select>
                                  
                                    @if ($errors->has('status'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                          </div>           
                         <div class="form-group row form_field__container">
                                <label for="quantity_type_addongroup"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Quantity Type') }}</label>
                                <div class="col-md-3 quantity_type_select">
                                    <select name="quantity_type" id="quantity_type_modifiergroup"
                                            class="quantity_type form-control{{ $errors->has('quantity_type') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="Value">Value</option>
                                        <option value="Range">Range</option>
                                      
                                    </select>
                                  
                                    @if ($errors->has('quantity_type'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('quantity_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                       <div class="col-md-3 quantity_type_val">
                               
                                     <div class="value_quantity1">
                                             <div class="">

                                            <input id="quantity" type="text"
                                                   class="quantity {{ $errors->has('quantity') ? ' is-invalid' : '' }} form-control"
                                                   name="quantity" value="{{ old('quantity') }}" required placeholder="Min,Max">
                                              @if ($errors->has('quantity'))
                                              <span class="invalid-feedback">
                                              <strong>{{ $errors->first('quantity') }}</strong>
                                              </span>
                                              @endif
                                           </div>
                                        
                                  </div>

                                    
                                </div>
                            </div>


                            <br/><br/>
                            <div class=" mb-0 ">
                                <div class="col-md-12 text-center">
                                </div>
                            </div>
        </div>
        <div class="modal-footer">
             <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
            </form>
      </div>
      
    </div>
  </div>

  
     <div class="modal fade" id="addmodifieroptions" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Modifier</h4>
        </div>
        <div class="modal-body">
           <form method="POST" class="form-horizontal" action="{{ route('label.store') }}" enctype="multipart/form-data">
                            @csrf

              <div class="form-group">
                <label class="control-label col-sm-2" for="modifier">Modifier Name :</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="modifier" name="modifier" placeholder="Modifier Name">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Price:</label>
                <div class="col-sm-10"> 
                <input type="text" class="form-control"  name="modifier_price" id="modifier_price" placeholder="Modifier Price">
                </div>
              </div>
              <!--ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#options">Options</a></li>
                <li><a data-toggle="tab" href="#dependency">Dependency</a></li>
              </ul>

              <div class="tab-content">
                  <div id="options" class="tab-pane fade in active">
                  <h3>Options</h3>
                  <p>Some content.</p>
                  </div>
                  <div id="dependency" class="tab-pane fade">
                  <h3>Dependency</h3>
                  <p>Some content in menu 1.</p>
                  </div>
                 
              </div-->
                         
           </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">Submit</button>

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      <input type="hidden" id="menuItemId" value="{{ $menuItem->id }}" />
      <input type="hidden" id="menuItemRestaurantId" value="{{ $menuItem->restaurant_id }}" />
    </div>
  </div>  
      <!-- Modal content-->



  <div class="modal fade " id="modifier_item_option_info_popup" style="display: none;" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">

    <div class="modal-dialog" role="document">
        <div class="modal-content">


        </div>
        <!-- modal-content -->
    </div>
    <!-- modal-dialog -->
</div>
