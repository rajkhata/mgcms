

	@if ($errors->any())
		<div class="row messageblock no-margin">
		<div class="alert alert-danger no-margin-bottom margin-top-20">
		    <ul>
		        @foreach ($errors->all() as $error)
		            <li>		        	
		            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
{{ $error }}
					</li>
		        @endforeach
		    </ul>
		</div>
		</div>
	     
	@endif
	@if (session('status_msg'))
		<div class="row messageblock no-margin">
	    <div class="alert alert-success no-margin-bottom margin-top-20">
        	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
	        {{ session('status_msg') }}
	    </div>
		</div>
	@endif