<script> 
var columns = [
                {
                    title: '',
                    target: 0,
                    className: 'treegrid-control',
                    data: function (item) {
                        if (item.children) {
                            return '<span>+<\/span>';
                        }
                        return '';
                    }
                },
                {
                    title: 'Modifier Name',
                    target: 1,
                    data: function (item) {
                        return item.modifier_name;
                    }
                },
                {
                    title: 'POS Id',
                    target: 2,
                    data: function (item) {
                        return item.pos_id;
                    }
                },
                {
                    title: 'Price',
                    target: 3,
                    data: function (item) {
                        return item.price;
                    }
                },
		@php $i=4; @endphp  
		@if(isset($ma_attribute_system_modifier_item))
		@foreach($ma_attribute_system_modifier_item as $groupItem)
		{	 
                    title: '{{$groupItem['attribute_label']}}',
                    target: {{$i}},
                    data: function (item) {
			//var ind = "{{'d-'.$groupItem['attribute_id']}}";
			if (typeof item.{{'d'.$groupItem['attribute_id']}} != "undefined") 
                        return item.{{'d'.$groupItem['attribute_id']}};
			else return '';
			 
                    }
                },
		@php $i++; @endphp  
		@endforeach					 
		@endif			  
                {
                    title: 'Sort Order',
                    target: {{$i}},
                    data: function (item) {
                        return item.sort_order;
                    }
                },
                {
                    title: 'Status',
                    target: {{$i++}},
                    data: function (item) {
                        return (item.status==1)?'Yes':'No';
                    }
                },
                {
                    title: 'Action',
                    target: {{$i++}},
                    data: function (item) {
			if(item.attribute_item_child_count > 0)
			var htm = '<button type="button" class="btn btn-info open-child-modal" value="'+item.id+'" onclick="openFormModel('+item.attribute_item_id+','+item.attribute_item_id_value+','+item.id+','+item.modifier_group_id+')">Add Child </button>';
			else var htm = '';	
			    htm += '<button type="button" class="btn btn-info open-modal" value="'+item.id+'">Edit </button>';
		             htm += '<button type="button" class="btn btn-danger delete-link" value="'+item.id+'">Delete </button>';
                        return htm;
                    }
                }
 ];
             
        </script>
       <?php  $languageId=1;  ?>
                <div class="row rowpb">
                    <div class="col-md-6"> <h3></h3></div>
                    <div class="col-md-6" style="text-align: right"><a href="#" class="btn btn__primary" data-toggle="modal" data-target="#modifiergroup">Add New Modifier group</a></div>
                </div>

               <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <form method="POST" id="itemodifiergroup_form" action="{{ '/v2/menu/item/'.$menu_id . '/updatemodifiergroup'}}" enctype="multipart/form-data">
			 
                      @csrf
                      @if(isset($item_modifier_group) && count($item_modifier_group)) 
                      @php     $groupcount=0; @endphp
                      @foreach($item_modifier_group as $modifiergroup)  
                     
                  <div class="panel panel-default" id="deletediv{{$modifiergroup['attribute_id']}}">
                        <div class="panel-heading" role="tab" id="heading{{$modifiergroup['attribute_id']}}">
                        <h3 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$modifiergroup['attribute_id']}}" 
				aria-expanded="false" aria-controls="collapse{{$modifiergroup['attribute_id']}}">
                        
                        {{$modifiergroup['groupname']}}  
                        </a> 
                           <span class="delete_edit">
                                <i class="fa fa-trash-o delete_modifier_group" aria-hidden="true" modifiergroup="{{$modifiergroup['id']}}" ></i>
                               <!--  <i class="fa fa-pencil" aria-hidden="true"></i>-->
                          </span>  
                        
                      </h3>

                        </div>
                        <div id="collapse{{$modifiergroup['attribute_id']}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$modifiergroup['attribute_id']}}" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                
                                <div class="form-group row form_field__container">
                                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-4">
                                    <div class="input__field">
					<select id="primary_attribute_id" name="primary_attribute_id[{{$modifiergroup['attribute_id']}}]"
		                            class="form-control{{ $errors->has('primary_attribute_id') ? ' is-invalid' : '' }}"
		                              required>
		                            <option value="">Select Modifier</option>
					    @if($modifier_group_data)
		                            @foreach($modifier_group_data as $mkey => $modifier)
		                             @if($mkey==$modifier['id']) 
						<option value="{{ $modifier['id'] }}" {{ ($modifiergroup['attribute_id']==$modifier['id']) ? 'selected' :'' }}>{{ $modifier['attribute_prompt'] }}</option>	
					     @endif	
		                            @endforeach
					   @endif
		                        </select>
                                         
                                    </div>
                                    @if ($errors->has('attribute_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('attribute_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div> 
				  @if($ma_attribute_system_modifier)
				  @foreach($ma_attribute_system_modifier as $attr) 
		                        @if($attr['backend_field_type']=='Dropdown')  
					<div class="form-group row form_field__container">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}"
				                class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-4">  
				                <select id="attribute_ids[{{$attr['id']}}]" name="secodary_attribute_id[{{$modifiergroup['attribute_id']}}][{{$attr['id']}}]"
				                    class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}">
				                    <option value="">{{$attr['attribute_prompt']}}</option>
				                     
						            @foreach($attr['items'] as $cat)
						            <option value="{{ $cat['id']}}"
							 	{{(isset($modifiergroup['system_attribute_field'][$attr['id']]['attribute_value']) && $modifiergroup['system_attribute_field'][$attr['id']]['attribute_value']==$cat['id'])?'selected':''}}>
								{{ $cat['attribute_item_name']}}   
							    </option>
						            @endforeach
				                    
				                </select>
				                @if ($errors->has('menu_category_id'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('menu_category_id') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div> 
				      @endif
				      @if($attr['backend_field_type']=='Text') 
					<div class="form-group row form_field__container">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-4">
				                <div class="input__field">
				                    <input   type="text"
				                        class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
				                        id="attribute_ids[{{$attr['id']}}]" name="secodary_attribute_id[{{$modifiergroup['attribute_id']}}][{{$attr['id']}}]" value="{{isset($modifiergroup['system_attribute_field'][$attr['id']]['attribute_value'])?$modifiergroup['system_attribute_field'][$attr['id']]['attribute_value']:''}}" required>
				                </div>
				                @if ($errors->has('name'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('name') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div>
				      @endif
				      @if($attr['backend_field_type']=='Float' || $attr['backend_field_type']=='Number') 
					<div class="form-group row form_field__container">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-4">
				                <div class="input__field">
				                    <input   type="text"
				                        class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
				                        id="attribute_ids[{{$attr['id']}}]" name="secodary_attribute_id[{{$modifiergroup['attribute_id']}}][{{$attr['id']}}]" value="{{isset($modifiergroup['system_attribute_field'][$attr['id']]['attribute_value'])?$modifiergroup['system_attribute_field'][$attr['id']]['attribute_value']:0}}" required>
				                </div>
				                @if ($errors->has('name'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('name') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div>
				      @endif
						
				       
				      @if($attr['backend_field_type']=='Checkbox') 
					<div class="form-group row form_field__container">
					<label for="is_popular" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
					<div class="col-md-4">  <div class="input__field">
					@foreach($attr['items'] as $cat)
				          
				                   <input type="checkbox" id="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" 
							name="secodary_attribute_id[{{$modifiergroup['attribute_id']}}][{{$attr['id']}}][][{{$cat['id']}}]" {{(isset($modifiergroup['system_attribute_field'][$attr['id']]['attribute_value']) &&$modifiergroup['system_attribute_field'][$attr['id']]['attribute_value']==$cat['id'])?'checked':''}}
				                value="{{$cat['id']}}" > {{$cat['attribute_item_name']}} 
				                &nbsp;&nbsp;&nbsp;&nbsp; 
				            
						@endforeach
					</div>
				            @if ($errors->has('is_popular'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('is_popular') }}</strong>
				                </span>
				            @endif
				        </div>
					
				       </div>
				      @endif
				      @if($attr['backend_field_type']=='Radio') 
					<div class="form-group row form_field__container">
					   
				            <label for="status"
				                class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				                <div class="col-md-4" style="padding-top: 10px;">
				                 
							 @foreach($attr['items'] as $cat)
							<input type="radio" name="secodary_attribute_id[{{$modifiergroup['attribute_id']}}][{{$attr['id']}}]" id="attribute_ids[{{$attr['id']}}][]"
				                value="{{$cat['id']}}" {{(isset($modifiergroup['system_attribute_field'][$attr['id']]['attribute_value']) && $modifiergroup['system_attribute_field'][$attr['id']]['attribute_value']==$cat['id'])?'checked':''}}> {{$cat['attribute_item_name']}}
							@endforeach	
						 
				                 
						
				            </div>
				        </div>
				      @endif
				       
				  					   
		                 
				@endforeach 
				@endif
				<div class="form-group row form_field__container">
                                    <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                                    <div class="col-md-4" style="padding-top: 10px;">
                                        <label class="radio-inline"><input type="radio"  name="status[{{$modifiergroup['attribute_id'] }}]" value="1" @if($modifiergroup['status'] ==1) checked  @endif > Active</label>
                                        &nbsp;&nbsp;
                                            <label class="radio-inline"><input type="radio"  name="status[{{$modifiergroup['attribute_id'] }}]" value="0" @if($modifiergroup['status'] ==0) checked @endif> InActive</label>
                                        @if ($errors->has('status'))
                                            <span class="invalid-feedback" style="display:block;">
                                                  <strong>{{ $errors->first('status') }}</strong>
                                                  </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row form_field__container">
                                    <label for="priority"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Sort Order') }}</label>
                                    <div class="col-md-4">
                                        <div class="input__field">
                                            <input type="number"
                                                   class="{{ $errors->has('sort_order') ? ' is-invalid' : '' }}"
                                                   name="sort_order[{{$modifiergroup['attribute_id'] }}]" value="{{ $modifiergroup['sort_order']  }}" required>
                                        </div>
                                        @if ($errors->has('sort_order'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('sort_order') }}</strong>
                                        </span>
                                        @endif
                                    </div>
				</div>
				 <div> 
				     <!--- show list of items -->
				 
					<table id="exampleWithSelect{{$modifiergroup['id'] }}" class="display" cellspacing="0" width="100%"></table> 
					<script> 
				
					$(document).ready(function () {
		         			//console.dir(columns);
						var dt{{$modifiergroup['id'] }} = $('#exampleWithSelect{{$modifiergroup['id'] }}').DataTable({
						    'select': {
							'style': 'multi',
							'selector': 'td:not(:first-child)'
						    },
						    'columns': columns,
						    'ajax': '/v2/menu/item/modifiergroup/item/' + {{$modifiergroup['id'] }} ,
						    'treeGrid': {
							'left': 10,
							'expandIcon': '<span>+<\/span>',
							'collapseIcon': '<span>-<\/span>'
						    },
						    responsive: true,
						    dom:'<"top"fiB><"bottom"trlp><"clear">',
						    buttons: [
							{
							    text: 'Add',
							    action: function () {openFormModel({{$modifiergroup['attribute_id'] }},0,0,{{$modifiergroup['id'] }})}
							},
							// {extend: 'excelHtml5'}, 
							// {extend: 'pdfHtml5'},   
							// {               
							//     extend: 'print',
							//     exportOptions: {
							//         columns: ':visible'
							//     }
							// },
							// 'colvis'
							{
							    extend: 'colvis',
							    text: '<i class="fa fa-eye"></i>',
							    titleAttr: 'Column Visibility'
							},
							{
							    extend: 'excelHtml5',
							    text: '<i class="fa fa-file-excel-o"></i>',
							    titleAttr: 'Excel'
							}, 
							{
							    extend: 'pdfHtml5',
							    text: '<i class="fa fa-file-pdf-o"></i>',
							    titleAttr: 'PDF'
							},
							{               
							    extend: 'print',
							    text: '<i class="fa fa-print"></i>',
							    titleAttr: 'Print',
							    exportOptions: {
								columns: ':visible',
							    },
							}
						    ],
						    columnDefs: [ {
							targets: -1,
							visible: true
						    }],
						    language: {
							search: "",
							searchPlaceholder: "Search"
						    },
						    responsive: true	
						});

						$('h4').on('click', function () {
						    var h4 = $(this);
						    if (h4.hasClass('show')) {
							h4.removeClass('show').addClass('showed').html('-hide code');
							h4.next().removeClass('hide');
						    }
						    else {
							h4.removeClass('showed').addClass('show').html('+show code');
							h4.next().addClass('hide');
						    }
						});
					    });
				  </script>
		                  </div>
		                </div>
		            </div>
				   
		             <input type="hidden" name="id[{{$modifiergroup['attribute_id'] }}]" value="{{$modifiergroup['id'] }}"> 
		              @php 
		              $groupcount++
		              @endphp
                       @endforeach
                       <div class="row"><button type="submit" class="btn btn__primary pull-right submitbtntopmargin" >Save</button> </div>
		        
                      @else
                      <div class="text-center">   No Modifiers found</div>
                      @endif
                       
		     
                    </form>
 
              </div>
<div class="modal fade" id="linkEditorModal" aria-hidden="true">
<form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate=""  enctype="multipart/form-data">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="linkEditorModalLabel">Modifier Item</h4>
			    <input type="hidden" name="modifier_group_id" value="0" id="modifier_group_id">
			    <input type="hidden" name="parent_id" value="0" id="parent_id"> <input type="hidden" name="attribute_id" value="0" id="attribute_id">
			    <input type="hidden" name="modifier_group_items_id" value="0" id="modifier_group_items_id">
                        </div>
                        <div class="modal-body">
			    	<div id="cms-accordion-msg-popup"></div>
				<div class="form-group " > 
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Modifier Name</label>
                                    <div class="col-sm-6 selectparent">
                                         <select name="attribute_id_primary_field" id="attribute_id_primary_field">
					  <option value="">Select</option>
					 </select> 
                                    </div>
				    <div class="col-sm-3 invalid-feedback hide error" id="type_error">
                                         <b id="type_error1" class="error1"></b>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">POS Id</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="pos_id" name="pos_id"
                                               placeholder="Enter POS Id" value="">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="pos_id_error">
                                         <b id="pos_id_error1" class="error1"></b>
                                    </div>
                                </div>
				<div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Price</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control price" id="price" name="price"
                                               placeholder="Enter Label" value="0.00">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="price_error">
                                         <b id="price_error1" class="error1"></b>
                                    </div>
                                </div>
 				 
				 
				@if($ma_attribute_system_modifier_item)
				@foreach($ma_attribute_system_modifier_item as $attr) 
		                      @if($attr['backend_field_type']=='Dropdown')  
					<div class="form-group" >
		                            <label class="col-sm-3 control-label" style="text-align:left!important">{{$attr['attribute_label']}}</label>
		                            <div class="col-sm-6">
		                                 <select class="{{$attr['id']}}" name="attribute_ids[{{$attr['id']}}]" id="attribute_ids[{{$attr['id']}}]" >
						  <option value="">Select </option>
							@if(isset($attr['items'])) 
						            @foreach($attr['items'] as $cat)
						            <option value="{{ $cat['id']}}">{{ $cat['attribute_item_name']}}</option>
						            @endforeach
						        @endif
						 </select> 
		                            </div>
					    <div class="col-sm-3 invalid-feedback hide error" id="type_error">
		                                 <b id="type_error1" class="error1"></b>
		                            </div>
		                        </div> 
					  
				      @endif
				       
				      @if($attr['backend_field_type']=='Text' || $attr['backend_field_type']=='Number' || $attr['backend_field_type']=='Float') 
					<div class="form-group">
		                            <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">{{$attr['attribute_label']}}</label>
		                            <div class="col-sm-6">
		                                <input type="text" class="form-control {{$attr['id']}}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]"
		                                       placeholder="Enter {{$attr['attribute_label']}}" value="{{($attr['backend_field_type']=='Text')?'':0}}">
		                            </div><div class="col-sm-3 invalid-feedback hide error" id="attribute_label_error">
		                                 <b id="attribute_label_error1" class="error1"></b>
		                            </div>
		                        </div>
					 
				      @endif
				       					   
		               @endforeach
				@endif 
				
				
				
                                <div class="form-group">
                                    <label for="inputLink" class="col-sm-3 control-label" style="text-align:left!important">Sort Order</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control sort_order" id="sort_order" name="sort_order"
                                               placeholder="Enter Label" value="99999">
                                    </div><div class="col-sm-3 invalid-feedback hide error" id="sort_order_error">
                                         <b id="sort_order_error1" class="error1"></b>
                                    </div>
                                </div>
				 
				
				  
                                
				<div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align:left!important">Status</label>
                                    <div class="col-sm-9">
                                        <input type="radio" name="status" id="status" value="1" > Active &nbsp; &nbsp; <input type="radio" id="status_no" name="status" value="0"> InActive
                                    </div>
                                </div>
				
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                            
			    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div></form>
            </div>   
<script type="text/javascript" src="{{asset('js/dataTables.treeGrid.js')}}"></script>	
<script src="//cdn.datatables.net/select/1.2.6/js/dataTables.select.js"></script>

<script>
function reloadDataTable(table){  
	//table.ajax.reload();
}
//onclick="openFormModel('+item.attribute_item_id+','+item.attribute_item_id+','+item.id+','+item.modifier_group_id+')
function openFormModel(attribute_id,parent_id,id,group_id){
		    var url = "/v2/systemattribute/item/list/"+ attribute_id+"/"+ parent_id;
		    $("#modifier_group_id").val(group_id);
		    $("#attribute_id").val(attribute_id);	
		    $("#parent_id").val(id);
		    $.get(url, function (data1) {
			   if(data1.error==0){
				var data = data1.data;
				var html = '<select name="attribute_id_primary_field" id="attribute_id_primary_field" >';
				$.each(data, function(key,value) {		
				     	html +='<option value="' + value.id + '" selected>' + value.attribute_item_name + '</option>';
				});	
				html += '</select>';
				$('.selectparent').html(html);	
				$('#modalFormData').trigger("reset");
				$('#btn-save').val("add");
				$('#btn-save').text("Add");		
				$('#linkEditorModal').modal('show');
			    }	
		})
		    	
} 
$("#btn-save").click(function (e) {
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
		e.preventDefault();
        	$('#cms-accordion-msg-popup').html('');
	 
         
		var form = $("#modalFormData")[0];
		var formdata = new FormData(form);
		 
		var state = $('#btn-save').val();
		var type = "POST";
		var id = $('#modifier_group_items_id').val();
		var ajaxurl = '/v2/menu/item/{{$menu_id }}/addmodifiergroupitem';
		$.ajax({
		    type: type,
		    url: ajaxurl,
		    data: formdata,
		    enctype: 'multipart/form-data',
      		    processData: false,  // tell jQuery not to process the data
      		    contentType: false,   // tell jQuery not to set contentType	
		    success: function (data1) {  
			if(data1.error==0){
				showMessageOnPopUp(data1.msg, 1)
				setTimeout(function(){ 
					//$('#modalFormData').trigger("reset");
					showMessageOnPopUp('Updated successfully!' , 1);
					//$('#linkEditorModal').modal('hide');
					setTimeout(function(){ 
						$('#modalFormData').trigger("reset");
						$('#linkEditorModal').modal('hide');
						 
						
						$('#cms-accordion-msg-popup').html('');	
					}, 1000);
					 
				}, 1000);
			}
			$('.error').addClass('hide');
			$('#cms-accordion-msg').html('');
				
			$('#modalFormData').trigger("reset");
		    },
		    error: function (data) {
		       // console.log('Error:', data);
			showMessageOnPopUp('Please try again!' , 0);
			//$('#linkEditorModal').modal('hide');
		    }
		});
	//}
});
jQuery('body').on('click', '.delete-link', function () {
 
        var link_id = $(this).val();  
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "DELETE",
            url: '/v2/menu/item/modifiergroup/itemdelete/' + link_id,
            success: function (data) {
               location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
jQuery('body').on('click', '.open-modal', function () {
           var id = $(this).val(); 
	   $('#cms-accordion-msg-popup').html();	$('#cms-accordion-msg-popup').hide();	
           $.get('/v2/menu/item/modifiergroup/iteminfo/' + id, function (data1) {
	   if(data1.error==0){
		    var data = data1.data; console.log(data);
		    $('#modifier_group_items_id').val(data.id);
		    $('#parent_id').val(data.parent_id);	
		    $('#modifier_group_id').val(data.modifier_group_id);
		    $("#attribute_id").val(data.attribute_item_id);	
		    $('.price').val(data.price);
		    $('.sort_order').val(data.sort_order);
		  	 
		    $.each(data.attribute_ids, function(key,valu1) {
			 $.each(valu1, function(key1,valu) {	 
				if(valu.type=="Dropdown"){	
					$('.'+key1 + ' [value='+valu.value+']').attr('selected', 'true');
					
				}else $('.'+key1).val(valu.value);
  			});
		    });
		    
		    $('#pos_id').val(data.pos_id);
		    //attribute_ids[60]	
		    if(data.status==0)$('#status_no').attr('checked', true);
		    else $('#status').attr('checked', true);
		    	

		    var html = '<select name="attribute_id_primary_field" id="attribute_id_primary_field" >';
		    html +='<option value="' + data.attribute_item_id_value + '" selected>' +data.modifier_name + '</option>';
		    html += '</select>';
		    $('.selectparent').html(html);	

		    //$('select[name^="attribute_id_primary_field"] option:selected').attr("selected",null);
		    //$('select[name^="attribute_item_id"] option[value="'+data.attribute_item_id+'"]').attr("selected","selected");	
		    $('#btn-save').val("update");//img
		    $('#linkEditorModal').modal('show');
	  }
        })
 });	     
$(document).on('click','.delete_modifier_group',function(){
    
    	var link_id=$(this).attr('modifiergroup');
     
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
    	});
        $.ajax({
            type: "DELETE",
            url: '/v2/menu/item/deletemodifiergroup/' + link_id,
            success: function (data) {
                location.reload(); 
                //$("#deletediv" + link_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
});
function showMessageOnPopUp(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	$('#cms-accordion-msg-popup').html(msg);$('#cms-accordion-msg-popup').show();	
         
}	
</script>
 

