<?php  $languageId=1;  ?>
<div class="row rowpb">
    <div class="col-md-6"> 
        <h3></h3>
    </div>
    <div class="col-md-6" style="text-align: right">
        <a href="#" class="btn btn__primary" data-toggle="modal" data-target="#modifiergroup">Add New Meal group</a>
    </div>
</div>
 
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <form method="POST" id="itemodifiergroup_form" action="/v2/menu/item/meal/{{$menu_id}}/addmealitem" enctype="multipart/form-data">
        @csrf
        @if(isset($list_addons_group) && count($list_addons_group))
        @php     $groupcount=0; @endphp
        @foreach($list_addons_group as $addongroup)  
                     
        <div class="panel panel-default" id="deletediv{{$addongroup['id']}}">
            <div class="panel-heading" role="tab" id="heading{{$addongroup['id']}}">
                <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$addongroup['id']}}" aria-expanded="false" aria-controls="collapse{{$addongroup['id']}}">
                        {{$addongroup['group_name']}} 
                    </a> 
                    <span class="delete_edit" onclick="deleteAddonGroup('{{$addongroup['id']}}')">
                        <i class="fa fa-trash-o delete_modifier_group" aria-hidden="true" modifiergroup="{{$addongroup['id']}}" ></i>
                    </span>  
                </h4>
            </div>

            <div id="collapse{{$addongroup['id']}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$addongroup['id']}}" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">

                    <div class="form-group row">
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                            <label for="Sort-Order" class="col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="input__field" style="padding-top: 5px;">
                                <input type="text" class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}" id="group_name" name="group_name[{{$addongroup['id']}}]" value="{{$addongroup['group_name']}}" required>
                            </div>
                            @if ($errors->has('group_name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('group_name') }}</strong>
                            </span>
                            @endif
                        </div> 
                            
                        @if(isset($meal_group_form))	
                        @foreach($meal_group_form as $attr)

                        @if($attr['backend_field_type']=='Dropdown')   
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
                            <div class="selct-picker-plain position-relative">
                                <select id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$addongroup['id']}}][{{$attr['id']}}]"
                                    class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
                                    <option value="">{{$attr['attribute_prompt']}}</option>
                                    @foreach($attr['items'] as $cat)
                                    <option value="{{ $cat['id']}}" {{(isset($addongroup['attribute_ids'][$attr['id']]['attribute_value']) && $addongroup['attribute_ids'][$attr['id']]['attribute_value']==$cat['id'])?'Selected':''}} >{{ $cat['attribute_item_name']}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('menu_category_id'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('menu_category_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> 
                        @endif
                        
                        @if($attr['backend_field_type']=='Text' || $attr['backend_field_type']=='Number' || $attr['backend_field_type']=='Float') 
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
                            <div class="input__field" style="padding-top: 5px;">
                                <input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$addongroup['id']}}][{{$attr['id']}}]" value="{{(isset($addongroup['attribute_ids'][$attr['id']]['attribute_value']))?$addongroup['attribute_ids'][$attr['id']]['attribute_value']:''}}">
                            </div>
                            @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        @endif
                        
                        @if($attr['backend_field_type']=='Checkbox') 
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                            <label for="is_popular" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
                            <div class="clearfix ptb5">
                                <div class="option__wrapper pr15">
                                @foreach($attr['items'] as $cat)
                                    <!-- <label class="radio-inline"> <input type="checkbox" id="attribute_ids[{{$addongroup['id']}}][{{$attr['id']}}][{{$cat['id']}}]" name="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" value="{{$cat['id']}}" {{(isset($addongroup['attribute_ids'][$attr['id']]['attribute_value']) && $addongroup['attribute_ids'][$attr['id']]['attribute_value']==$cat['id'])?'selected':''}}> {{$cat['attribute_item_name']}}</label> -->
                                    <label class="custom_checkbox relative padding-left-25 font-weight-600 pr15"><input class="hide permission" type="checkbox" id="attribute_ids[{{$addongroup['id']}}][{{$attr['id']}}][{{$cat['id']}}]" name="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" value="{{$cat['id']}}" {{(isset($addongroup['attribute_ids'][$attr['id']]['attribute_value']) && $addongroup['attribute_ids'][$attr['id']]['attribute_value']==$cat['id'])?'selected':''}}> {{$cat['attribute_item_name']}} <span class="control_indicator" ></span>
									</label> 
                                    @if ($errors->has('is_popular'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('is_popular') }}</strong>
                                    </span>
                                    @endif
                                @endforeach
                                </div>
                            </div>
                        </div>
                        @endif
                        
                        @if($attr['backend_field_type']=='Radio') 
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                            <label for="status" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
                            <div class="clearfix ptb5" style="padding-top: 4px;">
                                @foreach($attr['items'] as $cat)
                                <!-- <input type="radio" id="attribute_ids[{{$attr['id']}}][]" name="attribute_ids[{{$addongroup['id']}}][{{$attr['id']}}][]"
                                    value="{{$cat['id']}}" {{(isset($addongroup['attribute_ids'][$attr['id']]['attribute_value']) && $addongroup['attribute_ids'][$attr['id']]['attribute_value']==$cat['id'])?'Checked':''}}> {{$cat['attribute_item_name']}} -->
                                <div class="pull-left tbl-radio-btn pr15">
                                    <input type="radio" id="attribute_ids[{{$attr['id']}}][]" name="attribute_ids[{{$addongroup['id']}}][{{$attr['id']}}][]" value="{{$cat['id']}}" {{(isset($addongroup['attribute_ids'][$attr['id']]['attribute_value']) && $addongroup['attribute_ids'][$attr['id']]['attribute_value']==$cat['id'])?'Checked':''}}>
                                    <label for="attribute_ids[{{$attr['id']}}][]">{{$cat['attribute_item_name']}}</label>
                                </div>
                                @endforeach	
                            </div>
                        </div>
                        @endif	

                        @endforeach
                        @endif
                        
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                            <label for="Sort-Order" class="col-form-label text-md-right">{{ __('Sort Order') }}</label>
                            <div class="input__field" style="padding-top: 5px;">
                                <input type="text" class="{{ $errors->has('sort-order') ? ' is-invalid' : '' }}" id="sort_order" name="sort_order[{{$addongroup['id']}}]" value="{{$addongroup['sort_order']}}" required>
                            </div>
                            @if ($errors->has('sort_order'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('sort_order') }}</strong>
                            </span>
                            @endif
                        </div>
                            
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
                            <label for="status" class="col-form-label text-md-right">{{ __('Status') }}</label>
                            <div class="clearfix ptb5" style="padding-top: 4px;">
                                <div class="pull-left tbl-radio-btn pr15">
                                    <input type="radio" id="status1" name="status[{{$addongroup['id']}}]" value="1" {{($addongroup['status']==1)?'checked':''}}>
                                    <label for="status1">Yes</label>
                                </div>
                                <div class="pull-left tbl-radio-btn pr15">
                                    <input type="radio" id="status0" name="status[{{$addongroup['id']}}]" value="0" {{($addongroup['status']==0)?'checked':''}}>
                                    <label for="status0">No</label>
                                </div>
                                <!-- <label class="radio-inline"> <input type="radio" id="status1" name="status[{{$addongroup['id']}}]"
                                value="1"  {{($addongroup['status']==1)?'checked':''}}> Yes</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="radio-inline"><input type="radio" id="status0" name="status[{{$addongroup['id']}}]"
                                value="0" {{($addongroup['status']==0)?'checked':''}}> No</label> -->
                                @if ($errors->has('status'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> 
                    </div>

				    @if($addongroup['items']) 
					<div class="col-md-12 addonbox clearfix">
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-7 tdcol">Meal Name</div>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-3 tdcol">Price</div>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-2 tdcol">Action</div>
                        </div>
                        @foreach($addongroup['items'] as $addonItme) 
                        <div class="row rowbot">
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-7">
                                <!-- <div class="tdcol">Meal Name</div> -->
                                {{$addonItme->name}}
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-3">
                                <!-- <div class="tdcol">Price</div> -->
                                <div class="input__field">
                                    <input type="text" value="{{$addonItme->meal_price}}" name="update_item_price[{{$addonItme->id}}]">
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-2" onclick="deleteAddonItem('{{$addonItme->id}}')">
                                <span style="cursor:pointer" class="glyphicon glyphicon-remove"></span>
                            </div>
                        </div>
					    @endforeach
					</div>
                    @endif
                    
				    <button type="button" class="btn btn__primary mt-20" data-toggle="modal" data-target="#additem" onclick="reloadMealItems({{$addongroup['id']}})">Add Item</button>
				    <button type="submit" class="btn btn__primary mt-20" >Update All</button>
		        </div>
		    </div>
		</div>
				
		<input type="hidden" name="id[{{$addongroup['id'] }}]" value="{{$addongroup['id'] }}"> 
        @php 
        $groupcount++
        @endphp
        @endforeach
        <div class="row">
            <button type="submit" class="btn btn__primary pull-right submitbtntopmargin" >Save</button> 
        </div>
        @else
        <div class="text-center">No Meals found</div>
        @endif       
    </form>
</div>
      
<!-- modal -->
<div class="modal fade" id="additem" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Meal Item</h4>
            </div>
            <div class="modal-body" style="max-height:400px; min-height: 200px; overflow-y: scroll;">
                <form method="POST" id="additemform" action="{{ route('item-addons.store') }}" enctype="multipart/form-data">
                    loading...
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="saveAddonItemsToGroup">Add</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->

<script>
function deleteAddonGroup(itemId){ 
	var url = '/v2/menu/item-meals/deletegroup/'+ itemId;

        $.ajax({
            url: url,
            dataType: 'json',
            //contentType: "application/json; charset=utf-8",
            type: 'delete',
	    //data: inputData,
            success: function (data) {
                  location.reload(); 
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error-Addon Group Listing', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
	    //$('#addongroup').modal('hide');
            }
        });		
}
function deleteAddonItem(itemId){ 
	var url = '/v2/menu/item-meals/deleteitem/'+ itemId;

        $.ajax({
            url: url,
            dataType: 'json',
            //contentType: "application/json; charset=utf-8",
            type: 'delete',
	    //data: inputData,
            success: function (data) {
                  location.reload(); 
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error-Addon Group Listing', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
	    //$('#addongroup').modal('hide');
            }
        });		
}
function reloadMealItems(group_id) {

        //var menuItemRestaurantId = $('#menuItemRestaurantId').val();
        var url = '/v2/menu/item-meals/list/'+ group_id;

        $.ajax({
            url: url,
            dataType: 'json',
            //contentType: "application/json; charset=utf-8",
            type: 'get',
	    //data: inputData,
            success: function (data) {
                $('#additemform').html(data.data);
                $("#searchiteminput").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#myUL li").filter(function () {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error-Meal Group Listing', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
	    //$('#addongroup').modal('hide');
            }
        });
    }

    function addAddonItems() {

//        var menuItemRestaurantId = $('#menuItemRestaurantId').val();
        var url = '/v2/menu/item-meals/add';
        var inputData = $('#additemform').serialize();

        $.ajax({
            url: url,
            dataType: 'json',
//            contentType: "application/json; charset=utf-8",
            type: 'post',
            data: inputData,
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                }
                alertbox(alertboxHeader, data.message, function (modal) {
                    addonGroupPencilToClick = $('#additemform').find('input[name="addon_group_id"]').val();
                    getAddonGroups();
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);

                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error- item addon add', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    }

    function updateAddonItemsGroups(udpateForm) {

        var url = udpateForm.attr('action');
        var inputData = udpateForm.serialize();

        $.ajax({
            url: url,
            dataType: 'json',
//            contentType: "application/json; charset=utf-8",
            type: 'post',
            data: inputData,
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                }
                alertbox(alertboxHeader, data.message, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });
                //alert(err);
                alertbox('Error- item meal group add: ' + err, err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
//            $('#addongroup').modal('hide');
            }
        });
    }
$('#saveAddonItemsToGroup').click(function () {
        addAddonItems();
    });
</script>
