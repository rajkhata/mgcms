@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
    <div class="reservation__atto">
        <div class="reservation__title">
            <h1>Menu Item - Meals</h1>
            <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
        </div>
    </div>
         
    <div class="card margin-top-0">
        <div class="card-body">
            <div class="">
                @include('menumanagement::v2_menu_item.tab')
                <div class="tab-content">
                    @include('menumanagement::common.message')
 			        <div id="price" class="{{($section_tab=='price')?'fade in active':''}}">
		                @csrf
				        <div id="price" class="{{($section_tab=='price')?'fade in active':''}}">
		                    @csrf
			 	            @include('menumanagement::v2_menu_item.meal_render')
                        </div>
 		            </div>
 	            </div>
	        </div>
	    </div>
    </div>

    <input type="hidden" id="menuItemId" value="{{$menu_id}}" />
    <input type="hidden" id="menuItemRestaurantId" value="{{ $menu_id }}" />
      
    @include('menumanagement::v2_menu_item.item_meal_modals') 

     <script type="text/javascript">
      $("#product_type").change(function () {
           var type = $(this).val();
           if(type=='product'){
            $('.product_type_product').removeClass('hide');

           }else{
             $('.product_type_product').addClass('hide');

           }
    });  
        var size_div_cnt = 0;
        size_div_cnt = '<?php echo (isset($sizeCount) && $sizeCount > 0) ? ($sizeCount - 1) : 0 ?>';
        size_div_cnt = parseInt(size_div_cnt);

        function remove_div_by_id(id) {
            var div_cnt = $('.size_div').length;
            if (div_cnt > 1) {
                $('#size_div_' + id).remove();
            }
        }

        function add_size_div() {
            var restId = $('#restaurant_id :selected').val();
            if (restId == '') {
                alert('Please select Restaurant');
                return;
            }
            //var div_html = $('#size_div_0').html();

            var div_id_str = $('.size_div:first').attr('id');
            var div_id = div_id_str.slice(div_id_str.lastIndexOf("_") + 1);
            var div_html = $('#' + div_id_str).html();

            size_div_cnt = size_div_cnt + 1;
            $('#size_div').append("<div id='size_div_" + size_div_cnt + "' class='size_div' style='border: 1px solid #ccc;padding: 10px 0;margin: 10px 0;'></div>");
            var newIndex = '[' + size_div_cnt + ']';
            var newDataVal = "data-val='" + size_div_cnt + "'";
            div_html = div_html.replace(/\[0\]/g, newIndex);
            div_html = div_html.replace(/data-val=\"0\"/g, newDataVal);

            div_html = div_html.replace('meal_type_' + div_id, 'meal_type_' + size_div_cnt);
            div_html = div_html.replace('remove_div_' + div_id, 'remove_div_' + size_div_cnt);
            div_html = div_html.replace('remove_div_by_id(' + div_id + ')', 'remove_div_by_id(' + size_div_cnt + ')');

            //div_html = div_html.replace('meal_type_0', 'meal_type_'+size_div_cnt);
            $('#size_div_' + size_div_cnt).html(div_html);
            $('#size_div_' + size_div_cnt).find('.size').val('');
            $('#size_div_' + size_div_cnt).find('.price_all').val('');
            $('#size_div_' + size_div_cnt).find('.meal_type').prop('checked', false);
            $('#meal_type_' + size_div_cnt).hide();
            $('#remove_div_' + size_div_cnt).show();
            $('.datetimepicker').datetimepicker(commonOptions);

        }

        function remove_size_div() {
            /*if (size_div_cnt > 0) {
                $('#size_div_' + size_div_cnt).remove();
                size_div_cnt = size_div_cnt - 1;
            }*/
            var div_cnt = $('.size_div').length;
            if (div_cnt > 1) {
                $('.size_div:last').remove();
            }
        }

        function remove_all_size_div() {
            for (i = 1; i <= size_div_cnt; i++) {
                $('#size_div_' + i).remove();
            }
            $('#meal_type_0').html('');
            $('#size_div_0').find('.price_all').val('');
            $('#size_div_0').find('.meal_type').prop('checked', false);
            $('#meal_type_0').hide();
        }

        $(function () {
            $('body').on('click', '.meal_type', function () {
                var mealTypeVal = $(this).val();
                var idx = $(this).attr('data-val');
                if (mealTypeVal == '1') {
                    $('#meal_type_' + idx).find('input[type=checkbox]:checked').removeAttr('checked');
                    $('#meal_type_' + idx).find('input[type=text]').val('');
                    $('#meal_type_' + idx).hide();
                }
                else {
                    $('#size_div_' + idx).find('.price_all').val('');
                    $('#size_div_' + idx).find('.carryout_all:checked').removeAttr('checked');
                    $('#size_div_' + idx).find('.delivery_all:checked').removeAttr('checked');
                    $('#meal_type_' + idx).show();
                }
            });

            /*$('input[name=type]').click(function(){
                var typeVal = $('input[name=type]:checked').val();
                if(typeVal=='1') {
                    $('#meal_type').find('input[type=checkbox]:checked').removeAttr('checked');
                    $('#meal_type').find('input[type=text]').val('');
                    $('#meal_type').hide();
                }
                else {
                    $('#price_all').val('');
                    $('#meal_type').show();
                }
            });*/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $('option:selected').val();
                if (rest_id != '') {
                    $('#menu_category_id').find('option').not(':first').remove();
                    remove_all_size_div();
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_category_and_type"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj.cat) !== 'undefined') {
                                    $.each(data.dataObj.cat, function (i, item) {
                                        $('#menu_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    });
                                }
                                if ($.type(data.dataObj.type) !== 'undefined') {
                                    var fielsStr = '';
                                    $.each(data.dataObj.type, function (i, item) {
                                        fielsStr = fielsStr + "<div class=\"col-md-4\" style=\"margin:auto;padding-top:auto;\"><input type='checkbox' style=\"margin:auto;\" name='meal_type[0][" + item.id + "]' value='" + item.id + "'>&nbsp;" + item.name + "</div><div class=\"col-md-6\"><div class=\"input__field\"><input placeholder='Price' type='text' size='4' class=\"form-control\" name='price[0][" + item.id + "]' style=\"\"></div></div>";
                                    });
                                    $("#meal_type_0").html(fielsStr);
                                }
                            }
                        }
                    });
                }
            });


            // change sub-category @21-09-2018 by RG


             $("#menu_category_id").change(function () {
                var rest_id = $('#restaurant_id').val();
                var cat_id = $(this).val();
                console.log(rest_id + ' - ' + cat_id);
                $('#menu_sub_category_id').find('option').not(':first').remove();
                if (rest_id != '' && cat_id != '') {
                    $('#loader').removeClass('hidden');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/get_customization_options"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id + '&cid=' + cat_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') {
                               
                                // set sub categories
                                $.each(data.dataObj.sub_categories, function (i, item) {
                                  //  $('#menu_sub_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    $('#menu_sub_category_id').html(data.dataObj.subcat_options);
                                });
                            
                            }
                        }
                    });
                }
            });

            // check uncheck the checkbox
            $("input[type=checkbox]").on("click", function () {
                if ($(this).prop('checked') == true) {
                    $(this).attr('checked', 'checked');
                } else {
                    $(this).removeAttr('checked');
                }
            });

        });



$('.add_new_image').click(function(){
   
    var type=$.trim($(this).attr('imagetype'));
    var filename=type+'[]';
    var lastsib= $(this).parents('.image-caption-div').find(".image_cont_main:last");
    var str='<div class="image_cont_main row rowpb"><div class="col-md-9 no-col-padding"> <div class="col-md-9 no-col-padding"> <input id="'+type+'" type="file" class="form-control" name="'+filename+'"> <div class="input__field"> <input id="image_caption" type="text" name="'+type+'_caption[]" placeholder="Image Caption"> </div> </div> <div class="col-md-3 nomobtab"> <input type="button" value="Delete" class="btn btn__cancel deleteImage" > </div> </div> </div>';
     lastsib.after(str);
})
$(document).on('click','.deleteImage',function(){


    $(this).parents('.image_cont_main').remove();
  
});

function selectOptions(data,modifiergroup_id,parent,level){
	var parentTxt= '';
	var select_str ='';	console.log(modifiergroup_id)
	$.each( data, function( key, value ) {  console.log(key)
	  if(modifiergroup_id==key) {  
		$.each( value.items, function( key1, value1 ) {
		  if(parent>0) {
			level++;
			for(j=0;j<=level;j++)
			parentTxt += '-'; 
		  }else{
			level = 0;
		  }  
		  select_str+=' <option    value="'+value1.id+'">'+parentTxt+value1.attribute_item_name+'</option>';
		  
		  //select_str+= selectOptions(data,modifiergroup_id,key,level);
		});
	   }
        });
	return select_str;
}	
function modifierItemDropdown(modGroupdata,modifiergroup_id,modifier_item_count,parent_id){

      var catname=modifiergroup_id!=undefined? "modifiergroup["+modifiergroup_id+"][modifier_items]["+modifier_item_count+"][modifier_item]":'modifiercat_';
      var select_str='<select name="'+catname+'" class="form-control" required>';
      select_str+= selectOptions(modGroupdata,modifiergroup_id,0,0);
           
      
      select_str+='</select>';
      return  select_str;
}

$(document).on('click','.add_modifier',function(){
    
    var modifiergroup=$(this).attr('modifiergroup');
    //alert(modifiergroup)
    var modifer_group_list=$(this).siblings('.modifer_group_list');
    
     
    if(modifer_group_list.length){
     	
       	var data = $('.td'+modifiergroup).val()	
      modifer_group_list.find('tr:last').after(data);

    }else{
         
     	
       
       var data = $('.table'+modifiergroup).val()	
       $( this).prev('.form_field__container').after( data);

    }
    $('.datetimepicker').datetimepicker(commonOptions);


})

</script> 
<!--script src="{{ asset('/js/addon_group.js') }}"></script-->
@endsection