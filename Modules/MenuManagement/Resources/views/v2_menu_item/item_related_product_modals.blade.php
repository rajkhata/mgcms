  <div class="modal fade" id="modifiergroup" role="dialog">
    <div class="modal-dialog">
      
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Product Group</h4>
        </div>
                   <form method="POST" id="modifiergroup_form" action="/v2/menu/item/related_product/{{$menu_id}}/addrelatedproduct" enctype="multipart/form-data">

                      <div class="modal-body">
                            @csrf
                       
                             
                            <input type="hidden" name="menu_id" value="{{$menu_id}}">
                            <div class="form-group row form_field__container">
				            <label for="Sort-Order" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
				            <div class="col-md-4">
				                <div class="input__field">
				                    <input   type="text"
				                        class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
				                        id="group_name" name="group_name" value="" required>
				                </div>
				                @if ($errors->has('group_name'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('group_name') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div> 
			    @if(isset($related_group_form))	
			    @foreach($related_group_form as $attr)
		                      @if($attr['backend_field_type']=='Dropdown')   
					<div class="form-group row form_field__container">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}"
				                class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-4">
				                <select id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]"
				                    class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}" >
				                    <option value="">{{$attr['attribute_prompt']}}</option>
				                     
				                    @foreach($attr['items'] as $cat)
				                    <option value="{{ $cat['id']}}"  >{{ $cat['attribute_item_name']}}</option>
				                    @endforeach
				                    
				                </select>
				                @if ($errors->has('menu_category_id'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('menu_category_id') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div> 
				      @endif
				      @if($attr['backend_field_type']=='Text' || $attr['backend_field_type']=='Number' || $attr['backend_field_type']=='Float') 
					<div class="form-group row form_field__container">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-4">
				                <div class="input__field">
				                    <input   type="text"
				                        class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
				                        id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]"
								 value="{{ old('name') }}"  >
				                </div>
				                @if ($errors->has('name'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('name') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div>
				      @endif
				      @if($attr['backend_field_type']=='Price') 
					<div class="form-group row form_field__container">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-4">
				                <div class="input__field">
				                    <input   type="text"
				                        class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
				                        id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{ old('name') }}"  >
				                </div>
				                @if ($errors->has('name'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('name') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div>
				      @endif	
				       
				      @if($attr['backend_field_type']=='Checkbox') 
					<div class="form-group row form_field__container">
					<label for="is_popular" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
					<div class="col-md-4">
					@foreach($attr['items'] as $cat)
				            <div class="input__field">
				                 <label class="radio-inline"> <input type="checkbox" id="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" 
							name="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]"
				                value="{{$cat['id']}}" > {{$cat['attribute_item_name']}}</label>
				                &nbsp;&nbsp;&nbsp;&nbsp; 
				            </div>
				            @if ($errors->has('is_popular'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('is_popular') }}</strong>
				                </span>
				            @endif
					@endforeach
				        </div>
					
				       </div>
				      @endif
				      @if($attr['backend_field_type']=='Radio') 
					<div class="form-group row form_field__container">
					   
				            <label for="status"
				                class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				                <div class="col-md-4" style="padding-top: 10px;">
				                 
							 @foreach($attr['items'] as $cat)
							<input type="radio" id="attribute_ids[{{$attr['id']}}][]" name="attribute_ids[{{$attr['id']}}][]"
				                value="{{$cat['id']}}" > {{$cat['attribute_item_name']}}
							@endforeach	
						 
				                 
						
				            </div>
				        </div>
				      @endif
				       
				  					   
		                @endforeach
				@endif
				<div class="form-group row form_field__container">
				            <label for="Sort-Order" class="col-md-4 col-form-label text-md-right">{{ __('Sort Order') }}</label>
				            <div class="col-md-4">
				                <div class="input__field">
				                    <input   type="text"
				                        class="{{ $errors->has('sort-order') ? ' is-invalid' : '' }}"
				                        id="sort_order" name="sort_order" value="99999" required>
				                </div>
				                @if ($errors->has('sort_order'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('sort_order') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div>
				<div class="form-group row form_field__container">
		                    <label for="status"
		                        class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
		                    <div class="col-md-4" style="padding-top: 10px;">
		                        <label class="radio-inline"> <input type="radio" id="status1" name="status"
		                        value="1"  checked> Yes</label>
		                        &nbsp;&nbsp;&nbsp;&nbsp;
		                        <label class="radio-inline"><input type="radio" id="status0" name="status"
		                        value="0" > No</label>
		                        @if ($errors->has('status'))
		                        <span class="invalid-feedback" style="display:block;">
		                        <strong>{{ $errors->first('status') }}</strong>
		                        </span>
		                        @endif
		                    </div>
		                </div> 
                             
        <div class="modal-footer">
             <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
            </form>
      </div>
      
    </div>
  </div>

 
      <!-- Modal content-->



  <div class="modal fade " id="modifier_item_option_info_popup" style="display: none;" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
	</div>
        <!-- modal-content -->
    </div>
    <!-- modal-dialog -->
</div>
