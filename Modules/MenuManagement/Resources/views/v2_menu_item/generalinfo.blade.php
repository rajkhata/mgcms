@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
	<div class="reservation__atto">
		<div class="reservation__title">
			<h1>Menu Item - Add</h1>
			<style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
		</div>
	</div>

    <div class="row" style="padding-top: 20px;">
        <label for="language" class="hide  col-md-1 col-form-label no-col-padding text-md-right" style="padding-top: 5px">
            {{ __('Language') }}
        </label>
        <div class="col-md-3">
            <select name="language" id="language" class="hide form-control{{ $errors->has('language') ? ' is-invalid' : '' }}"required>
                  <option value="1" >English</option>
                  <option value="2" >Chinese</option>              
            </select>
        </div>
	</div>
	
    <div class="card margin-top-0">
	    @if(session()->has('message'))
		<div class="alert alert-success">
			{{ session()->get('message') }}
		</div>
		@endif

        <div class="card-body">
			<div class="">
                <ul class="nav nav-tabs">
					<li class="{{($section_tab=='general')?'active':' '}} "><a data-toggle="tab" href="#" id="generaltab">General</a></li> 

				</ul>
				
                <div class="tab-content">
                @include('menumanagement::common.message')
 					<div id="attribute" class="{{($section_tab=='associated')?'fade in active':''}}">
		          		<form method="POST" action="{{ route('item.store') }}" enctype="multipart/form-data" id="form1">
		           		@csrf
					
							<div class="form-group row">
								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
									<label for="restaurant_id" class="col-form-label text-md-right">{{ __('Restaurant') }}</label>
									<div class="selct-picker-plain position-relative">
										<select name="restaurant_id[]" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>
											@foreach($groupRestData as $rest)
											<optgroup label="{{ $rest['restaurant_name'] }}">
												@foreach($rest['branches'] as $branch)
												<option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
												@endforeach
											</optgroup>
											@endforeach
										</select>
										@if ($errors->has('restaurant_id'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('restaurant_id') }}</strong>
										</span>
										@endif
									</div>
								</div>
								
								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
									<label for="menu_categories_id" class="col-form-label text-md-right">{{ __('Category') }}</label>
									<div class="selct-picker-plain position-relative">
										<select name="menu_categories_id" id="menu_categories_id"
											class="form-control {{ $errors->has('menu_categories_id') ? ' is-invalid' : '' }} selectpicker" required data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top">
											@foreach($menu_category as $cat)	
											<option value="{{ $cat['id'] }}" {{ (old('menu_category_id')==$cat['id']) ? 'selected' :'' }}>{{ $cat['name'] }}</option>
											@foreach($cat['menu_sub_category'] as $subcat)
											<option value="{{ $subcat['id'] }}" {{ (old('menu_category_id')==$subcat['id']) ? 'selected' :'' }}>--{{ $subcat['name'] }}</option>
											@endforeach
											@endforeach
										</select>
										@if ($errors->has('menu_category_id'))
										<span class="invalid-feedback">
										<strong>{{ $errors->first('menu_category_id') }}</strong>
										</span>
										@endif
									</div>
								</div>
							</div>
							
							<div class="form-group row">
								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
									<label for="item_name" class="col-form-label text-md-right">{{ __('Name') }}</label>
									<div class="input__field">
										<input id="item_name" type="text" class="{{ $errors->has('item_name') ? ' is-invalid' : '' }}" name="item_name" value="{{ old('item_name') }}" required>
									</div>
									@if ($errors->has('item_name'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('item_name') }}</strong>
									</span>
									@endif
								</div>

								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
									<label for="sku" class="col-form-label text-md-right">{{ __('SKU') }}</label>
									<div class="input__field">
										<input id="sku" type="text" class="{{ $errors->has('sku') ? ' is-invalid' : '' }}" name="sku" value="" required>
									</div>
									@if ($errors->has('sku'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('sku') }}</strong>
									</span>
									@endif
								</div>
							</div>
								
							<div class="form-group row">
								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
									<label for="pos_id" class="col-form-label text-md-right">{{ __('POS ID') }}</label>
									<div class="input__field">
										<input id="pos_id" type="text" class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}" name="pos_id" value="">
									</div>
									@if ($errors->has('pos_id'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('pos_id') }}</strong>
									</span>
									@endif
								</div>

								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
									<label for="menu_categories_id" class="col-form-label text-md-right">{{ __('Product Type') }}</label>
									<div class="selct-picker-plain position-relative">
										<select name="product_type_id" id="product_type_id" 
											class="form-control {{ $errors->has('select_product_type') ? ' is-invalid' : '' }}" readonly>
										     
										    @foreach($product_types as $key=>$val)
											@if($product_type_id==$val['id'])
											<option value="{{ $val['id'] }}" selected>{{ $val['name'] }}</option>
											@endif
										    @endforeach
										</select>
										@if ($errors->has('menu_category_id'))
										<span class="invalid-feedback">
										<strong>{{ $errors->first('menu_category_id') }}</strong>
										</span>
										@endif
									</div>
								</div>
							</div> 
							 
							<!-- firstcol -->
							<div class="fieldboxCol row">
							@if($ma_attribute_system) 
							@foreach($ma_attribute_system as $attr)
								@if($attr['backend_field_type']=='Dropdown')   
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-20">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}"
										class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="selct-picker-plain position-relative">
										<select id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]"
											class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" {{($attr['is_required']==1)?'Required':''}}>
											<option value="">Select {{$attr['attribute_prompt']}}</option>
											@if(isset($attr['items'])) 
											@foreach($attr['items'] as $cat)
											<option value="{{ $cat['id']}}">{{ $cat['attribute_item_name']}}</option>
											@endforeach
											@endif
										</select>
										@if ($errors->has('menu_category_id'))
										<span class="invalid-feedback">
										<strong>{{ $errors->first('menu_category_id') }}</strong>
										</span>
										@endif
									</div>
								</div> 
								@endif
								@if($attr['backend_field_type']=='Multiple Select')   
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-20">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="selct-picker-plain position-relative">
										<select id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}][]"
												class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" multiple {{($attr['is_required']==1)?'Required':''}}>
												@if(isset($attr['items'])) 
												@foreach($attr['items'] as $cat)
												<option value="{{ $cat['id']}}">{{ $cat['attribute_item_name']}}</option>
												@endforeach
												@endif
										</select>
										@if ($errors->has('menu_category_id'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('menu_category_id') }}</strong>
										</span>
										@endif
									</div>
								</div> 
								@endif
								@if($attr['backend_field_type']=='Text') 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-21">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="input__field">
										<input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{ old('name') }}" {{($attr['is_required']==1)?'Required':''}}>
									</div>
									@if ($errors->has('name'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
								@endif
								@if($attr['backend_field_type']=='Number' ) 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-21">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="input__field">
										<input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{ old('name') }}" {{($attr['is_required']==1)?'Required':''}} value='0'> i.e. [0,1,2 ...etc]
									</div>
									@if ($errors->has('name'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
								@endif
								@if($attr['backend_field_type']=='Float' ) 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-21">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="input__field">
										<input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{ old('name') }}" {{($attr['is_required']==1)?'Required':''}}  value='0'> i.e. [0.00,1.00,2.00 ...etc]
									</div>
									@if ($errors->has('name'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
								@endif
								@if($attr['backend_field_type']=='Editor') 
								<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-20">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div>
										<div class="custom__message-textContainer">
											<textarea maxlength="500" rows="4" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" class="editor form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" {{($attr['is_required']==1)?'Required':''}}></textarea>
										</div>
										@if ($errors->has('description'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('description') }}</strong>
										</span>
										@endif
									</div>
								</div>
								@endif
								@if($attr['backend_field_type']=='Checkbox') 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-20">
									<label for="is_popular" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="clearfix ptb5">
										<div class="option__wrapper pr15">
											@if(isset($attr['items']))
											@foreach($attr['items'] as $cat)
											<label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" type="checkbox" id="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" name="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" value="{{$cat['id']}}" {{($attr['is_required']==1)?'Required':''}}> {{$cat['attribute_item_name']}} <span class="control_indicator" ></span>
											</label>
											 
											@endforeach
											@endif
										</div>
									</div>
								</div>
								@endif
								@if($attr['backend_field_type']=='Radio') 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-20">
									<label for="is_popular" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="clearfix ptb5">
										@if(isset($attr['items'])) 
										@php $i=0; @endphp
										@foreach($attr['items'] as $cat)
										<div class="pull-left tbl-radio-btn pr15">
											<input type="radio" id="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{$cat['id']}}" {{($attr['is_required']==1)?'Required':''}}>
											<label for="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]">{{$cat['attribute_item_name']}} </label>
											 
											@php $i++; @endphp
										</div>
										@endforeach
										@endif
									</div>
								</div>
								@endif
								@if($attr['backend_field_type']=='File'  || $attr['backend_field_type']=='Image') 
								<!-- Mobile app Image-->
								<div class="col-sm-12 col-md-8 col-lg-8 col-xs-12 margin-top-20 source-image-div image-caption-div dblock">
									<label for="image" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="">
										<div class="image_cont_main clearfix">
											<div class="no-col-padding">
												<input type="file"
													class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
													value="{{ old('image.0') }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" {{($attr['is_required']==1)?'Required':''}}>
												@if ($errors->has('image'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('image') }}</strong>
												</span>
												@endif     
											</div>
										</div>
									</div>
								</div>
								@endif 					   
								@endforeach
							@endif 
							</div>
							<!-- firstcol -->

							<!-- Second col -->
							<div class="fieldboxCol row">
							@if($ma_attribute)
							@foreach($ma_attribute as $attr) 
								@if($attr['backend_field_type']=='Dropdown')   
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-20">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="selct-picker-plain position-relative">
										<select id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" {{($attr['is_required']==1)?'Required':''}}>
											<option value="">Select {{$attr['attribute_prompt']}}</option>
											@if(isset($attr['items'])) 
											@foreach($attr['items'] as $cat)
											<option value="{{ $cat['id']}}">{{ $cat['attribute_item_name']}}</option>
											@endforeach
											@endif
										</select>
										@if ($errors->has('menu_category_id'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('menu_category_id') }}</strong>
										</span>
										@endif
									</div>
								</div> 
								@endif
								@if($attr['backend_field_type']=='Multiple Select')   
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-20">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="selct-picker-plain position-relative">
										<select id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}][]"
												class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" multiple {{($attr['is_required']==1)?'Required':''}}>
												@if(isset($attr['items'])) 
												@foreach($attr['items'] as $cat)
												<option value="{{ $cat['id']}}">{{ $cat['attribute_item_name']}}</option>
												@endforeach
												@endif
										</select>
										@if ($errors->has('menu_category_id'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('menu_category_id') }}</strong>
										</span>
										@endif
									</div>
								</div> 
								@endif
								@if($attr['backend_field_type']=='Text' ) 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-21">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="input__field">
										<input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{ old('name') }}" {{($attr['is_required']==1)?'Required':''}}>
									</div>
									@if ($errors->has('name'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
								@endif
								@if($attr['backend_field_type']=='Number' ) 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-21">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="input__field">
										<input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{ old('name') }}" {{($attr['is_required']==1)?'Required':''}} value="0"> i.e [0,1,2 ...etc]
									</div>
									@if ($errors->has('name'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
								@endif
								@if($attr['backend_field_type']=='Float' ) 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-21">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="input__field">
										<input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{ old('name') }}" {{($attr['is_required']==1)?'Required':''}} value="0.00"> i.e [0.00,1.00,2.00 ...etc]
									</div>
									@if ($errors->has('name'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
								@endif	
								@if($attr['backend_field_type']=='Editor') 
								<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-20 dblock">
									<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div>
										<div class="custom__message-textContainer">
											<textarea maxlength="500" rows="4" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" class="editor form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" {{($attr['is_required']==1)?'Required':''}}></textarea>
										</div>
										@if ($errors->has('description'))
										<span class="invalid-feedback">
										<strong>{{ $errors->first('description') }}</strong>
										</span>
										@endif
									</div>
								</div>
								@endif
								@if($attr['backend_field_type']=='Checkbox') 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-20">
									<label for="is_popular" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="clearfix ptb5">
										<div class="option__wrapper pr15">
										@if(isset($attr['items'])) 
										@foreach($attr['items'] as $cat)
											<label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" type="checkbox" id="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" name="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" value="{{$cat['id']}}" {{($attr['is_required']==1)?'Required':''}}> {{$cat['attribute_item_name']}} <span class="control_indicator"></span>
											</label>
											 
										@endforeach
										@endif
										</div>
									</div>
								</div>
								@endif
								@if($attr['backend_field_type']=='Radio') 
								<div class="col-sm-6 col-md-4 col-lg-4 col-xs-12 margin-top-20">
									<label for="is_popular" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="clearfix ptb5">
										@if(isset($attr['items'])) 
										@php $i=0; @endphp
										@foreach($attr['items'] as $cat)
										<div class="pull-left tbl-radio-btn pr15">
											<input type="radio" id="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" name="attribute_ids[{{$attr['id']}}]"
											value="{{$cat['id']}}" {{($attr['is_required']==1)?'Required':''}}>
											<label for="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]"> {{$cat['attribute_item_name']}}</label>
											 
											@php $i++; @endphp
										</div>
										@endforeach
									@endif
									</div>
								</div>
								@endif
								@if($attr['backend_field_type']=='File' || $attr['backend_field_type']=='Image')
								<div class="col-sm-12 col-md-8 col-lg-8 col-xs-12 margin-top-20 source-image-div image-caption-div dblock">
									<label for="image" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
									<div class="">
										<div class="image_cont_main clearfix">
											<div class="no-col-padding">
												<input type="file"
													class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
													value="{{ old('image.0') }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" {{($attr['is_required']==1)?'Required':''}}>
												@if ($errors->has('image'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('image') }}</strong>
												</span>
												@endif     
											</div>
										</div>
									</div>
								</div>
								@endif 					   
								@endforeach
							@endif 
							</div>
							<!-- second col -->

							<div class="form-group row">
								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
									<label for="Sort-Order" class="col-form-label text-md-right">{{ __('Status') }}</label>
									<div class="selct-picker-plain position-relative">
										<select name="status" id="status" class="form-control {{ $errors->has('status') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>
											<option value="">Select </option>
											<option value="1" selected>Active</option>
											<option value="0" >InActive</option>
										</select>
										@if ($errors->has('status'))
										<span class="invalid-feedback" style="display:block;">
											<strong>{{ $errors->first('status') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10">
									<label for="Sort-Order" class="col-form-label text-md-right">{{ __('Sort Order') }}</label>
									<div class="input__field">
										<input type="text" class="{{ $errors->has('sort-order') ? ' is-invalid' : '' }}" id="sort_order" name="sort_order" value="99999" required>
									</div>
									@if ($errors->has('sort_order'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('sort_order') }}</strong>
									</span>
									@endif
								</div>
							</div>
								
							<div class="form-group row ">
								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 margin-top-10">
									<label for="is_frontend_visible" class="col-form-label text-md-right">{{ __('Visiblity') }}</label>
									<div class="selct-picker-plain position-relative">
										<select name="is_frontend_visible" id="is_frontend_visible"
											class="form-control{{ $errors->has('is_frontend_visible') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>
											<option value="">Select</option>
											<option value="0" >Not Visible Individually</option>
											<option value="1" >Visible</option>
											 
										</select>
										@if ($errors->has('is_frontend_visible'))
										<span class="invalid-feedback">
										<strong>{{ $errors->first('is_frontend_visible') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="col-sm-6 col-md-6 col-lg-5 col-xs-12 pull-right margin-top-10 {{($product_type_id=2 || $product_type_id=6)?'hide':''}}">
									<div class="clearfix"><label for="status" class="col-form-label text-md-right">{{ __('Inventory') }}</label></div>
									 
									<div class=" margin-left-30 pull-left tbl-radio-btn pr15">
										<input type="radio"  class="" id="is_inventoryno" name="is_inventory" value="0" checked>
										<label for="is_inventoryno">No</label>
										 
									</div>

									 <div class=" margin-left-30 pull-left tbl-radio-btn pr15">
										 
										&nbsp;&nbsp;<input type="radio" id="is_inventory" name="is_inventory" value="1">
										<label for="is_inventory">Yes</label>
									</div>
										
									@if ($errors->has('is_inventory'))
									<span class="invalid-feedback" style="display:block;">
										<strong>{{ $errors->first('is_inventory') }}</strong>
									</span>
									@endif
								</div>
							</div>
								
							 
								
							<input type="hidden" name="customizable_attribute_ids" value="{{$customizable_attribute_ids}}" /> 
							 
							<input type="hidden" name="attribute_set_id" value="{{$attribute_set_id}}" />  
							<div style="margin-top: 50px;" class="row mb-0">
								<div class="col-md-12 text-center">
									<button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
								</div>
							</div>     
						</form>
						<div class="form-group row form_field__container hide">
							<div class="dropdown dropdown-tree" id="example"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 
</div>
    
<script type="text/javascript">
            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $(this).val();	
                $('#menu_category_id').find('option').not(':first').remove();
                if (rest_id != '') {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/v2/menu/get_category"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rest_id=' + rest_id,
                        success: function (data) {
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj) !== 'undefined') {
                                    $.each(data.dataObj, function (i, item) {
                                        $('#menu_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    });
                                }
                           }
                        }
                    });
                }
            });
            $("#menu_category_id").change(function () {
                var rest_id = $('#restaurant_id').val();
                var cat_id = $(this).val();
                console.log(rest_id + ' - ' + cat_id);
                $('#menu_sub_category_id').find('option').not(':first').remove();
                if (rest_id != '' && cat_id != '') {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/v2/menu/get_sub_category"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id + '&cat_id=' + cat_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') { 
				$.each(data.dataObj, function (i, item) {
				    $('#menu_sub_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                });
                            }
                        }
                    });
                }
            });
</script>
<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
        tinymce.init({
            selector: 'textarea.editor',
            menubar:false,
            height: 320,
            theme: 'modern',
            plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });
    });
</script> 
<script>
    //code added by jawed to facilitate FCK editor
     
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
function selecttab(id){
	$( "#generaltab" ).click();
	$( "#attribute" ).removeClass('active');
}
function productTypeChange(){
	var id = $("#select_product_type").val();
	var htm = '';
	jQuery('.attributelist').html(htm);
	if(id>0){
		$.get('/v2/systemattribute/list/producttype/' + id, function (data) {
		   $.each(data.list, function(key, value) { 	 
		        htm += ' <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12" style="padding-bottom: 10px;"><label class="radio-inline">';
			htm += '<input type="checkbox" id="attribute_group" name="attribute_group[]" value="'+value.id+'" >'+value.attribute_label+'</label> &nbsp;&nbsp;&nbsp;&nbsp; </div>';
		   });
		
	     	jQuery('.attributelist').html(htm);
		});		
	}
}
</script>

<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{ URL::to('css/dropdowntree.css')}}" rel="stylesheet" type="text/css">
<!--<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
 
<script src="{{ URL::to('js/dropdowntree.js')}}"></script>
<style>
.container { margin:150px auto;}
</style>
 
<script>
$('#item_name').keyup(function(){
    var name = $('#item_name').val();
    var skuName = name;
     var skuName = skuName.replace(/'/g, "");
     var skuName = skuName.replace(/"/g, " Inch");
     var skuName = skuName.toLowerCase();
     var skuName = skuName.replace(/ /g, "-");
     
    	$('#sku').val(skuName);    
    
});
$('#item_name').focusout(function() {
	 var name = $('#item_name').val();
    var skuName = name;
     
     var skuName = skuName.replace(/"/g, " Inch");
     var skuName = skuName.replace(/'/g, "");
     var skuName = skuName.toLowerCase();
     var skuName = skuName.replace(/ /g, "-");
     
    	$('#sku').val(skuName);   
});
 
</script>
@endsection
