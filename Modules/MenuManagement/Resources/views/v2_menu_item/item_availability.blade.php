@extends('layouts.app')
@section('content')

<div class="main__container container__custom">

        <div class="reservation__atto">
          <div class="reservation__title">
            <h1>Menu Item - Add/Edit Availability</h1>
            <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
          </div>
        </div>
         
        <div  class="card ">
		@if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            <div class="card-body">
              <div class="">

                        @include('menumanagement::v2_menu_item.tab')


                <div class="tab-content">
                  @include('menumanagement::common.message')
 			<div id="price" class="{{($section_tab=='price')?'fade in active':''}}">
		           
		          <form method="POST" action="/v2/menu/item/{{$menu_id}}/updateavailability" enctype="multipart/form-data" id="form1">
		           @csrf
				    
				 <div class="row addtionalRow2">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-20">
                                                    <h4>Avaiability</h4>
                                                    
                                                    <div class="row margin-top-10">
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">Start Date</label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                            <div class="input__field"><input type="text" class="datetimepicker color-black fullWidth date_start icocal" value="{{isset($menu_availability['start_date'])?$menu_availability['start_date']:''}}"   name="start_date" id="start_date" placeholder="Start Date" /></div>
                                                        </div>
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 tabhide">&nbsp;</div>

                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">End Date</label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                            <div class="input__field"><input type="text" class="datetimepicker color-black fullWidth date_end icocal" value="{{isset($menu_availability['end_date'])?$menu_availability['end_date']:''}}"   name="end_date" id="end_date" placeholder="End Date" /></div>
                                                        </div>
                                                    </div>
 						    <div class="row  margin-top-20">
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">Start Time</label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                            <div class="input__field"><input type="text" class="timepickerstart color-black fullWidth time_start icotime" value="{{isset($menu_availability['start_time'])?$menu_availability['start_time']:''}}"    name="start_time" id="time_start" placeholder="Start Time" /></div>
                                                        </div>
                                                        <div class="col-sm-0 col-md-0 col-lg-2 col-xs-12 tabhide">&nbsp;</div>

                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">End Time</label>
                                                        </div>
                                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                                            <div class="input__field"><input type="text" class="timepickerend color-black fullWidth time_end icotime" value="{{isset($menu_availability['end_time'])?$menu_availability['end_time']:''}}"  name="end_time" id="time_end" placeholder="End Time" /></div>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-top-20">
                                                        <h4>Reccurence</h4>
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                                            <label for="pos_code" class="col-form-label text-md-right padbot">Days</label>
                                                        </div>
                                                        <div class="col-sm-10 col-md-10 col-lg-10 col-xs-12 formob">
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Sunday" {{isset($menu_availability['Sunday'])?'checked':''}}>&nbsp;&nbsp;&nbsp;Sunday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Monday" {{isset($menu_availability['Monday'])?'checked':''}}>&nbsp;&nbsp;&nbsp;Monday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Tuesday" {{isset($menu_availability['Tuesday'])?'checked':''}}>&nbsp;&nbsp;&nbsp;Tuesday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Wednesday" {{isset($menu_availability['Wednesday'])?'checked':''}}>&nbsp;&nbsp;&nbsp;Wednesday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Thursday" {{isset($menu_availability['Thursday'])?'checked':''}}>&nbsp;&nbsp;&nbsp;Thursday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Friday" {{isset($menu_availability['Friday'])?'checked':''}}>&nbsp;&nbsp;&nbsp;Friday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                            <div class="option__wrapper">
                                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Saturday" {{isset($menu_availability['Saturday'])?'checked':''}}>&nbsp;&nbsp;&nbsp;Saturday <span class="control_indicator"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
				 <input type="hidden" name="id" value="{{isset($menu_availability['id'])?$menu_availability['id']:0}}" />  
				 
				<div style="margin-top: 50px;" class="row mb-0">
					<div class="col-md-12 text-center">
						<button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
					</div>
				</div>     
	 		  </form>
	 		</div>
 		</div>
 	     </div>
	   </div>
	</div>
</div>

 <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />

       <script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
    
    $(function() {
        $('.datetimepicker').datepicker({ dateFormat: 'yy-mm-dd' });
	//$('.timepicker').timepicker();	
	

	$('.timepickerstart').timepicker({
	    timeFormat: 'h:mm p',
	    interval: 5,
	    minTime: '00',
	    maxTime: '11:59pm',
	    defaultTime: '{{isset($menu_availability['start_time'])?$menu_availability['start_time']:''}}',
	    startTime: '00:00',
	    dynamic: false,
	    dropdown: true,
	    scrollbar: true
	});
        $('.timepickerend').timepicker({
	    timeFormat: 'h:mm p',
	    interval: 5,
	    minTime: '00',
	    maxTime: '11:59pm',
	    defaultTime: '{{isset($menu_availability['end_time'])?$menu_availability['end_time']:''}}',
	    startTime: '00:00',
	    dynamic: false,
	    dropdown: true,
	    scrollbar: true
	});


    });

</script>    
 

@endsection
