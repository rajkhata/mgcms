@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
	<div class="reservation__atto">
		<div class="reservation__title">
			<h1>Menu Item - Add/Edit Price</h1>
			<style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
		</div>
	</div>
        
	<div id="cms-accordion-msg">
	@if(session()->has('message'))
		<div class="alert alert-success margin-top-20">
			{{ session()->get('message') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	</div>	
            
    <div class="card margin-top-0">
        <div class="card-body">
		@include('menumanagement::v2_menu_item.tab')

        	<div class="tab-content">
                @include('menumanagement::common.message')
 				<div id="price" class="{{($section_tab=='price')?'fade in active':''}}">
		        	<form method="POST" action="/v2/menu/item/{{$menu_id}}/updateprice" enctype="multipart/form-data" id="form1" onsubmit="return checkForm()">
						@csrf
					   
						<div class="form-group row">
							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-20">
								<label for="name" class="col-form-label text-md-right">{{ __('Price') }}</label>
								<div class="input__field">
									<input id="item_price" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="price" value="{{isset($menu_price['price'])?$menu_price['price']:'0.00'}}" required>
								</div>
								@if ($errors->has('price'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('price') }}</strong>
								</span>
								@endif
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-20">
								<label for="name" class="col-form-label text-md-right">{{ __('Special Price') }}</label>
								<div class="input__field">
									<input id="special_price" type="text" class="{{ $errors->has('special_price') ? ' is-invalid' : '' }}"
										name="special_price"   value="{{isset($menu_price['special_price'])?$menu_price['special_price']:'0.00'}}" >
								</div>
								@if ($errors->has('special_price'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('special_price') }}</strong>
								</span>
								@endif
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-20">
								<label for="name" class="col-form-label text-md-right">{{ __('Special Price Start Date') }}</label>
								<div>
									<div class="input__field">
										<input id="special_price_start_date" type="text"
											class="{{ $errors->has('special_price_start_date') ? ' is-invalid' : '' }} datetimepicker"
											name="special_price_start_date" value="{{isset($menu_price['special_price_start_date'])?$menu_price['special_price_start_date']:''}}" >
									</div>
									@if ($errors->has('special_price_start_date'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('special_price_start_date') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-top-20">
								<label for="name" class="col-form-label text-md-right">{{ __('Special Price End Date') }}</label>
								<div class="">
									<div class="input__field">
										<input id="special_price_end_date" type="text"
											class="{{ $errors->has('special_price_end_date') ? ' is-invalid' : '' }} datetimepicker" 
											name="special_price_end_date" value="{{isset($menu_price['special_price_end_date'])?$menu_price['special_price_end_date']:''}}" >
									</div>
									@if ($errors->has('special_price_end_date'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('special_price_end_date') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
						
				 		<input type="hidden" name="id" value="{{isset($menu_price['id'])?$menu_price['id']:0}}" />  
						<input type="hidden" name="menu_id" value="{{$menu_id}}" />  
						<div style="margin-top: 50px;" class="row mb-0">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
							</div>
						</div>     
	 		  		</form>
	 			</div>
 			</div>
	   	</div>
	</div>
</div>
</div>


<link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
<script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>
<script>
    var commonOptions = {
        i18n:{
            en: {
                dayOfWeekShort: [
                    "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"
                ]
            }
        },
        format: 'Y-m-d H:i:s',
        formatTime: 'H:i:s'
    };
    $(function() {
        $('.datetimepicker').datetimepicker(commonOptions);
    });
function checkForm(){ 
	jQuery('#cms-accordion-msg').html('');
	var price = $('#item_price').val(); 
	var special_price = $('#special_price').val();	
	if(price==special_price){ 
		var msg = 'Special price must be less than price!!!--'  ;
		showMessage(msg , 0);
		return false;
	}else if(special_price > price){
		var msg = 'Special price must be less than price!!!'  ;
		showMessage(msg , 0);
		return false;
	}
	jQuery('#cms-accordion-msg').html('');
	return true;
}	
function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg').html(msg);	    
}
</script>    
@endsection