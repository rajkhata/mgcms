@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
	<div class="reservation__atto">
		<div class="reservation__title">
			<h1>Menu Item - Add/Edit Associated Item</h1>
			<style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
		</div>
	</div>
         
	<div id="cms-accordion-msg">
		@if(session()->has('message'))
		<div class="alert alert-success margin-top-20">
			{{ session()->get('message') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
	</div>	

    <div class="card margin-top-0">
        <div class="card-body">
			@include('menumanagement::v2_menu_item.tab')
            <div class="tab-content">
                @include('menumanagement::common.message')
 				<div id="attribute" class="{{($section_tab=='associated')?'fade in active':''}}">
		        	<table id="table_id" class="responsive ui cell-border hover" style="width:100%; margin-bottom:10px;">
			    		<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								@if(isset($ma_attribute_field_name) && count($ma_attribute_field_name)>0)
								@foreach($ma_attribute_field_name as $fieldItemId=>$fieldItem)	
								<th>{{$fieldItem}}</th>
								@endforeach
								@endif
								<th>Status</th>
							</tr>
			    		</thead>
			   			<tbody>
							@if(isset($menu_items) && count($menu_items)>0)
                            @foreach($menu_items as $menuItem)
							<tr> 
								<td>{{$menuItem['id']}}</td>
					      		<td>{{$menuItem['item_name']}}</td>
					    		@if(isset($ma_attribute_field_name) && count($ma_attribute_field_name)>0)
					    		@foreach($ma_attribute_field_name as $fieldItemId=>$fieldItem)	
						       	@if(isset($menuItem['items']) && count($menuItem['items'])>0)
					    		@foreach($menuItem['items'] as $i=>$ar)	
								@if($fieldItemId==$ar['attribute_id'])
					    		<td>{{$ar['attribute_item_name'] }}</td>
								@endif
								@endforeach
		                        @endif
					     		@endforeach
		                        @endif
					    		<td data-order="{{$menuItem['id']}}">
									<div class="waitList__otp no-padding" style="float: left; margin-right: 20px;">
		                            	<div class="toggle__container" style="margin-left: 0;">
											@php
												if($menuItem['status']){
													$ariaPressed = true;
													$activeClass = 'active';
												} else {
													$ariaPressed = false;
													$activeClass = '';
												}
											@endphp
		                                    <button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom menu_item_active {{ $activeClass }}" title="Open" data-toggle="button" data-menu="{{ $menuItem['id'] }}" aria-pressed="{{ $ariaPressed }}" autocomplete="off">
		                                        <div class="handle"></div>
		                                    </button>
		                                </div>
									</div>
		                            <a style="margin-top: 10px;" href="{{ URL::to('/v2/menu/item/' . $menuItem['id'] . '/editgeneralinfo') }}" class="glyphicon glyphicon-edit" ></a>
		                                  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;   
		                            <a onclick="return confirm('Are you sure, you want to delete it?')" href="{{ URL::to('v2/menu/item/' . $menuItem['id'] . '/destroy') }}" class="hide glyphicon glyphicon-trash"></a>
					    		</td> 	
							</tr>
				    		@endforeach
		                	@else
		                    <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
		                        <td>&nbsp;</td>
		                        <td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
		                	@endif
			  			</tbody>
					</table>


			  		<h4 class="asstitle">Add New Item</h4>
					<form method="POST" action="/v2/menu/item/associatedproduct/{{$menu_id}}/add" enctype="multipart/form-data" id="form1" onsubmit="return checkForm()">
						<div class="form-group row">
							@csrf
							@if($ma_attribute)
							@foreach($ma_attribute as $attr)

							@if($attr['backend_field_type']=='Dropdown' )   
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
								<div class="selct-picker-plain position-relative">
									<select id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]"
										class="form-control generatesku {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>
										<option value="">{{$attr['attribute_prompt']}}</option>
										@foreach($attr['items'] as $cat)
										<option value="{{ $cat['id']}}">{{ $cat['attribute_item_name']}}</option>
										@endforeach
									</select>
									@if ($errors->has('menu_category_id'))
									<span class="invalid-feedback">
									<strong>{{ $errors->first('menu_category_id') }}</strong>
									</span>
									@endif
								</div>
							</div> 
							@endif		   
							@endforeach 
							@endif

							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="item_name" class="col-form-label text-md-right">{{ __('Name') }}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input id="item_name" type="text"
										class="{{ $errors->has('item_name') ? ' is-invalid' : '' }}"
										name="item_name" value="{{$menu_data->item_name}}" required>
								</div>
								@if ($errors->has('item_name'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('item_name') }}</strong>
								</span>
								@endif
							</div> 
							
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="sku" class="col-form-label text-md-right">{{ __('SKU') }}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input id="sku" type="text" class="{{ $errors->has('sku') ? ' is-invalid' : '' }}" name="sku" value="" required>
								</div>
								@if ($errors->has('sku'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('sku') }}</strong>
								</span>
								@endif
							</div> 
							
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="pos_id" class="col-form-label text-md-right">{{ __('POS ID') }}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input id="pos_id" type="text"
										class="{{ $errors->has('pos_id') ? ' is-invalid' : '' }}"
										name="pos_id" value="" >
								</div>
								@if ($errors->has('pos_id'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('pos_id') }}</strong>
								</span>
								@endif
							</div> 
					
							@if($ma_attribute_system)
							@foreach($ma_attribute_system as $attr)

							@if($attr['backend_field_type']=='Dropdown')   
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
								<div class="selct-picker-plain position-relative">
									<select id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]"
										class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>
										<option value="">{{$attr['attribute_prompt']}}</option>
										@foreach($attr['items'] as $cat)
										<option value="{{ $cat['id']}}">{{ $cat['attribute_item_name']}}</option>
										@endforeach
									</select>
									@if ($errors->has('menu_category_id'))
									<span class="invalid-feedback">
									<strong>{{ $errors->first('menu_category_id') }}</strong>
									</span>
									@endif
								</div>
							</div> 
							@endif
							
							@if($attr['backend_field_type']=='Text' || $attr['backend_field_type']=='Float' || $attr['backend_field_type']=='Number') 
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" id="attribute_ids[{{$attr['id']}}]" name="attribute_ids[{{$attr['id']}}]" value="{{ old('name') }}" required>
								</div>
								@if ($errors->has('name'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
							@endif
						
							@if($attr['backend_field_type']=='Checkbox') 
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="is_popular" class="col-form-label text-md-right">{{$attr['attribute_label']}}</label>
								@foreach($attr['items'] as $cat)
								<div class="clearfix ptb5">
									<div class="option__wrapper pr15">
										<!-- <label class="radio-inline"> <input type="checkbox" id="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" name="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]"
										value="{{$cat['id']}}" > {{$cat['attribute_item_name']}}</label> -->
										<label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" type="checkbox" id="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" name="attribute_ids[{{$attr['id']}}][{{$cat['id']}}]" value="{{$cat['id']}}">{{$cat['attribute_item_name']}} <span class="control_indicator" ></span>
                                    </label>
									</div>
								</div>
								@if ($errors->has('is_popular'))
									<span class="invalid-feedback">
									<strong>{{ $errors->first('is_popular') }}</strong>
									</span>
								@endif
								@endforeach
							</div>
							@endif
							
							@if($attr['backend_field_type']=='Radio') 
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								@foreach($attr['items'] as $cat)
								<label for="status" class="col-form-label text-md-right">{{$cat['attribute_item_name']}}</label>
								<div class="clearfix ptb5" style="padding-top: 4px;">
									<div class="pull-left tbl-radio-btn pr15">
										<!-- <label class="radio-inline"> <input type="radio" id="attribute_ids[{{$attr['id']}}][]" name="attribute_ids[{{$attr['id']}}][]" value="{{$cat['id']}}" > {{$cat['attribute_item_name']}}</label> -->
										<input type="radio" id="attribute_ids[{{$attr['id']}}][]" name="attribute_ids[{{$attr['id']}}][]" value="{{$cat['id']}}">
                                    	<label for="attribute_ids[{{$attr['id']}}][]">{{$cat['attribute_item_name']}}</label>
									</div>
									@if ($errors->has('status'))
									<span class="invalid-feedback" style="display:block;">
										<strong>{{ $errors->first('status') }}</strong>
									</span>
									@endif
								@endforeach
								</div>
							</div>
							@endif
								
							@endforeach
							@endif
							
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="name" class="col-form-label text-md-right">{{ __('Price') }}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input id="item_price" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="price" value="0.00" required>
								</div>
								@if ($errors->has('price'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('price') }}</strong>
								</span>
								@endif
							</div>
							
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="name" class="col-form-label text-md-right">{{ __('Special Price') }}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input id="special_price" type="text" class="{{ $errors->has('special_price') ? ' is-invalid' : '' }}" name="special_price"   value="0.00" >
								</div>
								@if ($errors->has('special_price'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('special_price') }}</strong>
								</span>
								@endif
							</div>
							
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="name" class="col-form-label text-md-right">{{ __('Special Price Start Date') }}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input id="special_price_start_date" type="text" class="{{ $errors->has('special_price_start_date') ? ' is-invalid' : '' }} datetimepicker" name="special_price_start_date" value="" >
								</div>
								@if ($errors->has('special_price_start_date'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('special_price_start_date') }}</strong>
								</span>
								@endif
							</div>
							
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="name" class="col-form-label text-md-right">{{ __('Special Price End Date') }}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input id="special_price_end_date" type="text" class="{{ $errors->has('special_price_end_date') ? ' is-invalid' : '' }} datetimepicker" name="special_price_end_date" value="" >
								</div>
								@if ($errors->has('special_price_end_date'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('special_price_end_date') }}</strong>
								</span>
								@endif
							</div>
					
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="status" class="col-form-label text-md-right">{{ __('Status') }}</label>
								<div class="clearfix ptb5" style="padding-top: 4px;">
									<div class="pull-left tbl-radio-btn pr15">
										<input type="radio" id="status1" name="status" value="1" checked>
										<label for="status1">Active</label>
									</div>
									<div class="pull-left tbl-radio-btn pr15">
										<input type="radio" id="status0" name="status" value="0">
										<label for="status1">Inactive</label>
									</div>
									<!-- <label class="radio-inline"> <input type="radio" id="status1" name="status"
									value="1"  checked> Active</label>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<label class="radio-inline"><input type="radio" id="status0" name="status"
									value="0" > Inactive</label> -->
									@if ($errors->has('status'))
									<span class="invalid-feedback" style="display:block;">
									<strong>{{ $errors->first('status') }}</strong>
									</span>
									@endif
								</div>
							</div>
							
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="Sort-Order" class="col-form-label text-md-right">{{ __('Sort Order') }}</label>
								<div class="input__field" style="padding-top: 5px;">
									<input type="text" class="{{ $errors->has('sort-order') ? ' is-invalid' : '' }}" id="sort_order" name="sort_order" value="99999" required>
								</div>
								@if ($errors->has('sort_order'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('sort_order') }}</strong>
								</span>
								@endif
							</div>
							
							<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 margin-top-20">
								<label for="restaurant_id" class="col-form-label text-md-right">{{ __('Visiblity') }}</label>
								<div class="selct-picker-plain position-relative">
									<select name="is_frontend_visible" id="is_frontend_visible" class="form-control{{ $errors->has('is_frontend_visible') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" required>
										<option value="0" selected>Not Visible Individually</option>
										<option value="1" >Visible</option>
									</select>
									@if ($errors->has('restaurant_id'))
									<span class="invalid-feedback">
										<strong>{{ $errors->first('restaurant_id') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="clearfix"></div>
							<input type="hidden"  name="is_inventory" value="0" >
							<input type="hidden" name="menu_id" value="{{$menu_id}}" /> 
							<input type="hidden" name="product_type_id" value="5" />  
							<div style="margin-top: 50px;" class="row mb-0">
								<div class="col-md-12 text-center">
									<button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
								</div>
							</div>   
						</div>  
					</form>
					   
					<input id="item_name1" type="hidden" class="{{ $errors->has('item_name') ? ' is-invalid' : '' }}" name="item_name1" value="{{$menu_data->item_name}}" required>	
					<div class="form-group row form_field__container hide">
						<div class="dropdown dropdown-tree" id="example"> </div>
					</div>

	 				</div>
 				</div>
 	    	</div>
		</div>
	</div>
</div>

<link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
<script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>   
<script type="text/javascript">
 
$('#item_name').focusout(function() {
    	 
    	product_name = $('#item_name').val();
        var skuname = product_name.replace(/ /g, "-");
	 var skuname = skuname.replace(/"/g, "-inch-");
    	$('#sku').val(skuname); 
});
$('.generatesku').change(function() {
	var pname ='';	
	product_name = $('#item_name1').val();
	var pname = $('.generatesku option:selected').text();
	var cmpName = pname.replace(/"/g, " Inch ");
	var prodname = cmpName +' '+ product_name;

	var sku = pname.toLowerCase();
     	
	var sku = sku.replace(/"/g, "-inch-");	
		
    	var sku = sku +' '+ product_name;
	var skuname = sku.replace(/ /g, "-");
	$('#item_name').val(prodname);
    	$('#sku').val(skuname); 
     
});

       $(document).ready( function () {
	     $('#table_id').DataTable(  {
			    responsive: true,
			    dom:'<"top"fiB><"bottom"trlp><"clear">',
			    buttons: [
				 
				{
				    extend: 'colvis',
				    text: '<i class="fa fa-eye"></i>',
				    titleAttr: 'Column Visibility'
				},
				{
				    extend: 'excelHtml5',
				    text: '<i class="fa fa-file-excel-o"></i>',
				    titleAttr: 'Excel'
				}, 
				{
				    extend: 'pdfHtml5',
				    text: '<i class="fa fa-file-pdf-o"></i>',
				    titleAttr: 'PDF'
				},
				{               
				    extend: 'print',
				    text: '<i class="fa fa-print"></i>',
				    titleAttr: 'Print',
				    exportOptions: {
				        columns: ':visible',
				    },
				}
			    ],
			    columnDefs: [ {
				targets: -1,
				visible: true
			    }],
			    language: {
				search: "",
				searchPlaceholder: "Search"
			    },
			    responsive: true
		});
        //$('#table_id').show();
        //$('#table_id').removeClass('hidden');
	} );
      

 
 var commonOptions = {
        i18n:{
            en: {
                dayOfWeekShort: [
                    "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"
                ]
            }
        },
        format: 'Y-m-d H:i:s',
        formatTime: 'H:i:s'
    };
    $(function() {
        $('.datetimepicker').datetimepicker(commonOptions);
    });
function checkForm(){ 
	jQuery('#cms-accordion-msg').html('');
	var price = $('#item_price').val(); 
	var special_price = $('#special_price').val();	
	if(price==special_price){ 
		var msg = 'Special price must be less than price!!!--'  ;
		showMessage(msg , 0);
		return false;
	}else if(special_price > price){
		var msg = 'Special price must be less than price!!!'  ;
		showMessage(msg , 0);
		return false;
	}
	jQuery('#cms-accordion-msg').html('');
	return true;
}	
function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg').html(msg);	
         
}
$('.menu_item_active').on('click', function() {
            var menu_id = $(this).data('menu');
            var active_flag = 1;
            if($(this).hasClass('active')) {
                active_flag = 0;
            }
            $.ajax({
                type: 'delete',
                url: '/v2/menu/toggleactive/',
                type: 'POST',
                data: {
                    'menu_id': menu_id,
                    'active_flag': active_flag
                },
                success: function (data) {
                    alertbox('Updated',data.message,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            panel.remove();
                        }, 2000);
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                        }, 2000);
                    });
                }
            });
        });
</script> 
@endsection
