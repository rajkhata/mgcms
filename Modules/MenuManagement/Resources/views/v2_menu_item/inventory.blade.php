@extends('layouts.app')
@section('content')

<div class="main__container container__custom">

        <div class="reservation__atto">
          <div class="reservation__title">
            <h1>Menu Item - Add/Edit Inventory</h1>
            <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
          </div>
        </div>
         
        <div  class="card ">
		@if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            <div class="card-body">
              <div class="">

                      @include('menumanagement::v2_menu_item.tab')
                <div class="tab-content">
                  @include('menumanagement::common.message')
 			<div id="inventory" class="{{($section_tab=='inventory')?'fade in active':''}}">
		           
		          <form method="POST" action="/v2/menu/item/{{$menu_id}}/updateinventory" enctype="multipart/form-data" id="form1">
		           @csrf
				    
				<div class="form-group row form_field__container">
		                    <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('Quantity') }}</label>
		                    <div class="col-md-4">
		                        <div class="input__field">
		                            <input id="quantity" type="text"
		                                class="{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
		                                name="quantity" value="{{isset($menu_inventory['quantity'])?$menu_inventory['quantity']:0}}" required>
		                        </div>
		                        @if ($errors->has('quantity'))
		                        <span class="invalid-feedback">
		                        <strong>{{ $errors->first('quantity') }}</strong>
		                        </span>
		                        @endif
		                    </div>
		                </div>
				 <div class="form-group row form_field__container">
		                    <label for="max_quantity" class="col-md-4 col-form-label text-md-right">{{ __('Max Allowed Quantity') }}</label>
		                    <div class="col-md-4">
		                        <div class="input__field">
		                            <input id="max_quantity" type="text"
		                                class="{{ $errors->has('max_quantity') ? ' is-invalid' : '' }}"
		                                name="max_quantity" value="{{isset($menu_inventory['max_quantity'])?$menu_inventory['max_quantity']:0}}" required>
		                        </div>
		                        @if ($errors->has('max_quantity'))
		                        <span class="invalid-feedback">
		                        <strong>{{ $errors->first('max_quantity') }}</strong>
		                        </span>
		                        @endif
		                    </div>
		                </div>
				<div class="form-group row form_field__container">
		                    <label for="min_quantity" class="col-md-4 col-form-label text-md-right">{{ __('Min Allowed Quantity') }}</label>
		                    <div class="col-md-4">
		                        <div class="input__field">
		                            <input id="price" type="text"
		                                class="{{ $errors->has('min_quantity') ? ' is-invalid' : '' }}"
		                                name="min_quantity" value="{{isset($menu_inventory['min_quantity'])?$menu_inventory['min_quantity']:0}}" required>
		                        </div>
		                        @if ($errors->has('min_quantity'))
		                        <span class="invalid-feedback">
		                        <strong>{{ $errors->first('min_quantity') }}</strong>
		                        </span>
		                        @endif
		                    </div>
		                </div>
				  <input type="hidden" name="id" value="{{isset($menu_inventory['id'])?$menu_inventory['id']:0}}" />  
				<div style="margin-top: 50px;" class="row mb-0">
					<div class="col-md-12 text-center">
						<button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
					</div>
				</div>     
	 		  </form>
	 		</div>
 		</div>
 	     </div>
	   </div>
	</div>
</div>

 <link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />

       <script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>

<script>
    var commonOptions = {
        i18n:{
            en: {
                dayOfWeekShort: [
                    "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"
                ]
            }
        },
        format: 'Y-m-d H:i:s',
        formatTime: 'H:i:s'
    };
    $(function() {
        $('.datetimepicker').datetimepicker(commonOptions);
    });

</script>    
 

@endsection
