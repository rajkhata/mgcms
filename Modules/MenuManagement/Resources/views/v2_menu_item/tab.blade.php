<ul class="nav nav-tabs">
			<li class="{{($section_tab=='type')?'active':' '}} hide"><a href="/v2/menu/item/{{$menu_id}}/edit-item">Type</a></li>	
			  
			<li class="{{($section_tab=='general')?'active':' '}}"><a href="/v2/menu/item/{{$menu_id}}/editgeneralinfo">General</a></li>
			<li class="{{($section_tab=='price')?'active':' '}}"><a  href="/v2/menu/item/{{$menu_id}}/addprice">Price</a></li>
			<li class="{{($section_tab=='availability')?'active':''}}"><a  href="/v2/menu/item/availability/{{$menu_id}}/list">Availability</a></li>
			<?php if($is_inventory==1){?>
			<li class="{{($section_tab=='inventory')?'active':''}}"><a href="/v2/menu/item/{{$menu_id}}/addinventory">Inventory</a></li>
			<?php }?>
			<li class="{{($section_tab=='images')?'active':''}}"><a  href="/v2/menu/item/images/{{$menu_id}}/list">Images</a></li>
                        
			
			<?php if($product_type_id==2){?>
			<li class="{{($section_tab=='associated')?'active':''}}"><a  href="/v2/menu/item/associatedproduct/{{$menu_id}}/list">Associated Products</a></li>
			<?php }elseif($product_type_id==3){?>	
				 <li class="{{($section_tab=='meal')?'active':''}}"><a  href="/v2/menu/item/meal/{{$menu_id}}/list">Meal</a></li>	 	
			<?php }else {?>
			
                       	<li class="{{($section_tab=='modifiers')?'active':''}}"><a  href="/v2/menu/item/modifiergroup/{{$menu_id}}/list">Modifiers</a></li>
			<li class="{{($section_tab=='addons')?'active':''}}"><a  href="/v2/menu/item/addons/{{$menu_id}}/list">Addons</a></li>
			<?php }?>
                        <li class="{{($section_tab=='related_products')?'active':''}}"><a  href="/v2/menu/item/related_products/{{$menu_id}}/list">Related Products</a></li>
</ul>
