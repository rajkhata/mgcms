@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
    <div class="reservation__atto">
        <div class="reservation__title">
            <h1>Menu Item - Add/Edit Price</h1>
            <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
        </div>
    </div>
         
    <div class="card margin-top-0">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="card-body">
            <div class="">
                @include('menumanagement::v2_menu_item.tab')
                <div class="tab-content">
                @include('menumanagement::common.message')
 			        <div id="price" class="{{($section_tab=='price')?'fade in active':''}}">
		                <form method="POST" action="/v2/menu/item/images/{{$menu_id}}/updateitemimages" enctype="multipart/form-data" id="form1">
                            @csrf
                            
				            <div class="form-group row source-image-div image-caption-div">
                            <label for="image" class="col-xs-12 col-form-label text-md-right">{{ __('Mobile App Image') }}</label>
                            <div class="">               
                            <div class="image_cont_main row nospace rowpb">
                                 @if(empty($allimages['mobile_app_images']))
 
                                <div class="">
                                      <div class="col-md-8">
                                        <input id="image" type="file"
                                               class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                                               value="{{ old('image.0') }}" name="mobile_app_image[]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                        @if ($errors->has('image'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        <div class="input__field">
                                            <input id="image_caption" type="text"
                                                   class="{{ $errors->has('image_caption') ? ' is-invalid' : '' }}"
                                                   name="mobile_app_image_caption[]"
                                                   value="{{old('image_caption.0') }}"
                                                   placeholder="Image Caption">
                                        </div>
                                        @if ($errors->has('image_caption.0'))
                                            <span class="invalid-feedback" style="display: block;">
                                                <strong>{{ $errors->first('image_caption.0') }}</strong>
                                            </span>
                                        @endif
                                </div> 
                                 <div class="col-md-4">

                                <button type="button" imagetype="mobile_app_image" class="btn btn-sm btn__primary add_new_image mob-mt-10">Add New</button>
                                </div>                                      
                            </div>       
                                 @else
                                 <div class="col-md-6 no-col-padding">

                                <button type="button" imagetype="mobile_app_image" class="btn btn-sm btn__primary add_new_image mob-mt-10">Add New</button>
                                </div>    
                               @endif  
                            </div>

                            @if(isset($allimages['mobile_app_images']) && count($allimages['mobile_app_images'])>0)
                            <?php 
                            $i=0;
                             ?>                             
                             @foreach($allimages['mobile_app_images'] as $key=>$image)
                                <div class="image_cont_main row nospace rowpb">
                                    
                                    <div class="col-md-9">
                                         <div class="col-md-9">
                                            <input id="image" type="file"
                                                   class="form-control"
                                                   value="" name="mobile_app_image[{{$key}}]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                            
                                            <div class="input__field">
                                                <input id="image_caption" type="text"
                                                       class=""
                                                       name="mobile_app_image_caption[{{$key}}]"
                                                       value="{{$image['image_caption']}}"
                                                       placeholder="Image Caption">
                                            </div>
                                      </div> 
                                      <div class="col-md-3">
                                           <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >

                                      </div>   
                                </div>
                                <input type="hidden" name="mobile_app_image_hidden[]" value="{{$key}}" >
                                    <div class="col-md-3 ">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['image']}}">
                                            <img class="img-responsive img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['thumb_image_sml'] }}"></a>&emsp;
                                      </div> 
                                    
                                </div>

                              <?php 
                            $i++;
                             ?>
                            @endforeach

                            @endif
                            </div>
                           
                        </div>
 <hr>
                        <div class="form-group row source-image-div image-caption-div">
                            <label for="web_image"
                                   class="col-xs-12 col-form-label text-md-right">{{ __('Desktop Web Image') }}</label>
                            <div class="">
                                <div class="image_cont_main row nospace rowpb">
                                     @if(empty($allimages['desktop_web_images']))
                                        <div class="">
                                            <div class="col-md-8">
                                                <input id="desktop_web_image" type="file"
                                                       class="form-control{{ $errors->has('desktop_web_images') ? ' is-invalid' : '' }}"
                                                       value="{{ old('web_image.0') }}" name="desktop_web_image[]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                                @if ($errors->has('web_image'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('web_image') }}</strong>
                                                    </span>
                                                @endif
                                                <div class="input__field">
                                                    <input id="desktop_web_image_caption" type="text"
                                                           class="{{ $errors->has('web_image_caption') ? ' is-invalid' : '' }}"
                                                           name="desktop_web_image_caption[]"
                                                           value="{{ count(old())==0 ?@$allimages['desktop_web_image'][0]['web_image_caption']  : old('web_image_caption.0') }}"
                                                           placeholder="Image Caption">
                                                </div>
                                                @if ($errors->has('web_image_caption.0'))
                                                    <span class="invalid-feedback" style="display: block;">
                                                        <strong>{{ $errors->first('web_image_caption.0') }}</strong>
                                                    </span>
                                                @endif
                                           </div>
                                            <div class="col-md-4">
                                              <button type="button" imagetype="desktop_web_image" class="btn btn-sm btn__primary add_new_image mob-mt-10">Add New</button>
                                            </div>
                                    </div>
                                    @else 
                                    <div class="col-md-6 no-col-padding">

                                   <button type="button" imagetype="desktop_web_image" class="btn btn-sm btn__primary add_new_image mob-mt-10">Add New</button>
                                   </div>      
                                    @endif
                            </div>
                               @if(isset($allimages['desktop_web_images']) && count($allimages['desktop_web_images'])>0)
                            <?php 
                            $i=0;
                             ?>
                             @foreach($allimages['desktop_web_images'] as $key=>$image)
                                <div class="image_cont_main row nospace rowpb">
                                    
                                    <div class="col-md-9 no-col-padding">

                                       <div class="col-md-9 no-col-padding">
                                            <input id="web_image" type="file"
                                                   class="form-control"
                                                   value="" name="desktop_web_image[{{$key}}]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                            
                                            <div class="input__field">
                                                <input id="desktop_web_image_caption" type="text"
                                                       class=""
                                                       name="web_image_caption[{{$key}}]"
                                                       value="{{$image['web_image_caption']}}"
                                                       placeholder="Image Caption">
                                            </div>
                                        </div>    
                                        <div class="col-md-2">
                                           <input type="button" value="Delete" class="btn btn-sm btn__cancel deleteImage" >  
                                        </div>
                                </div>
                                <input type="hidden" name="desktop_web_image_hidden[]" value="{{$key}}" >
                                     <div class="col-md-3 ">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['desktop_web_image']}}">
                                            <img class="img-responsive img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['web_thumb_sml'] }}"></a>&emsp;
                                      </div>
                                   
                                </div>

                            @endforeach

                            @endif
                            </div>
                            
                        </div>
 <hr>

                        <div class="form-group row source-image-div image-caption-div">
                            <label for="mobile_web_image"
                                   class="col-xs-12 col-form-label text-md-right">{{ __('Mobile Web Image') }}</label>
                            <div class="">
                                <div class="image_cont_main row nospace rowpb">
                                    @if(empty($allimages['mobile_web_images']))
                            
                                <div class="">
                                     <div class="col-md-8">
                                    <input id="mobile_web_image" type="file"
                                           class="form-control{{ $errors->has('mobile_web_image') ? ' is-invalid' : '' }}"
                                           value="{{ old('mobile_web_image.0') }}" name="mobile_web_image[]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                    @if ($errors->has('mobile_web_image'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('mobile_web_image') }}</strong>
                                        </span>
                                    @endif
                                    <div class="input__field">
                                        <input id="mobile_web_image_caption" type="text"
                                               class="{{ $errors->has('mobile_web_image_caption') ? ' is-invalid' : '' }}"
                                               name="mobile_web_image_caption[]"
                                               value="{{old('mobile_web_image_caption.0') }}"
                                               placeholder="Image Caption">
                                    </div>
                                    @if ($errors->has('mobile_web_image_caption.0'))
                                        <span class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('mobile_web_image_caption.0') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                      <button type="button" imagetype="mobile_web_image" class="btn btn-sm btn__primary add_new_image mob-mt-10">Add New</button>

                                </div>
                            </div>
                            @else
                                <div class="col-md-2 no-col-padding">
                                      <button type="button" imagetype="mobile_web_image" class="btn btn-sm btn__primary add_new_image mob-mt-10">Add New</button>

                                </div>                            
                            @endif
                            </div>

   @if(isset($allimages['mobile_web_images']) && count($allimages['mobile_web_images'])>0)
                             @foreach($allimages['mobile_web_images'] as $key=>$image)

                                <div class="image_cont_main row nospace">
                                    
                                    <div class="col-md-9  no-col-padding3">
                                         <div class="col-md-9  no-col-padding">
                                        <input id="mobile_web_image" type="file"
                                               class="form-control"
                                               value="" name="mobile_web_image[{{$key}}]" accept="image/gif, image/jpeg, image/png, image/jpg">
                                        
                                        <div class="input__field">
                                            <input id="mobile_web_image_caption" type="text"
                                                   class=""
                                                   name="mobile_web_image_caption[{{$key}}]"
                                                   value="{{$image['mobile_web_image_caption']}}"
                                                   placeholder="Image Caption">
                                        </div>
                                         </div>
                                         <div class="col-md-3">
                                               <input type="button" value="Delete" class="btn  btn-sm btn__cancel deleteImage" >
                                         </div>

                                  </div>
                                  <input type="hidden" name="mobile_web_image_hidden[]" value="{{$key}}" >
                                     <div class="col-md-3">
                                        <a data-fancybox="gallery" href="{{url('/').'/..'.@$image['mobile_web_image']}}">
                                            <img class="img-responsive  img-thumbnail" 
                                                    src="{{ url('/').'/..'.@$image['mobile_web_thumb_sml'] }}"></a>&emsp;
                                      </div>
                                   
                                </div>

                            @endforeach

                            @endif
				@if($menu_images)
				@foreach($menu_images as $key=>$image)
					 <div class="col-md-9  no-col-padding3">
						 <img class="img-responsive  img-thumbnail" 
                                                    src="{{ url('/').$image['mobile_web_image'] }}"> &emsp;
							
					   </div>
					<div class="col-md-9  no-col-padding3">
						 <img class="img-responsive  img-thumbnail" 
                                                    src="{{ url('/').$image['mobile_app_image'] }}"> &emsp;
							
					   </div>
					<div class="col-md-9  no-col-padding3">
						 <img class="img-responsive  img-thumbnail" 
                                                    src="{{ url('/').$image['desktop_web_image'] }}"> &emsp;
							
					   </div>
				@endforeach
				@endif
                            </div>
				  <div class="clearfix"></div>
				<div style="margin-top: 50px;" class="row mb-0">
					<div class="col-md-12 text-center">
						<button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
					</div>
				</div>     
	 		  </form>
	 		</div>
 		</div>
 	     </div>
	   </div>
	</div>
</div>


<link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
<script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>

<script>
$('.add_new_image').click(function(){
   
    var type=$.trim($(this).attr('imagetype'));
    var filename=type+'[]';
    var lastsib= $(this).parents('.image-caption-div').find(".image_cont_main:last");
    var str='<div class="image_cont_main row rowpb"><div class="col-md-9"> <div class="col-md-9 no-col-padding"> <input id="'+type+'" type="file" class="form-control" name="'+filename+'"> <div class="input__field"> <input id="image_caption" type="text" name="'+type+'_caption[]" placeholder="Image Caption"> </div> </div> <div class="col-md-3 nomobtab"> <input type="button" value="Delete" class="btn btn__cancel deleteImage" > </div> </div> </div>';
     lastsib.after(str);
})
$(document).on('click','.deleteImage',function(){


    $(this).parents('.image_cont_main').remove();
  
});
</script>    
 

@endsection
