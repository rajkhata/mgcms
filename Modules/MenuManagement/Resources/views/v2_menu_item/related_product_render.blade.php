<?php  $languageId=1;  ?>
                
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <table id="table_id" class="responsive ui cell-border hover " style="width:100%; margin-bottom:10px;">
        <thead>
            <tr>
                <th>Id</th>  
                <th>Name</th> 
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @csrf
        @if($list_related_item) 
            @foreach($list_related_item as $itme) 
            <tr> 
                <td> {{$itme['id']}} </td>
                <td> {{$itme['name']}} </td>
                <td><a onclick="deleteAddonItem('{{$itme['id']}}')" href="#" class="glyphicon glyphicon-trash"></a> </td>
            </tr>		  
            @endforeach
        @else
        <tr>
            <td> </td>  
            <td> </td> 
            <td> </td> 
        </tr>
        @endif	
        </tbody>
    </table> 
</div> 
	  	  
<div class="modal fade" id="additem" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Item</h4>
            </div>
            <div class="modal-body" style="max-height:400px; min-height: 200px; overflow-y: scroll;">
                <form method="POST" id="additemform" action="{{ route('item-addons.store') }}" enctype="multipart/form-data">
                    loading...
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="saveAddonItemsToGroup">Add</button>
            </div>
        </div>
    </div>
</div>

<script>
function deleteAddonItem(itemId){ 
	var url = '/v2/menu/item-relatedproduct/deleteitem/'+ itemId;

        $.ajax({
            url: url,
            dataType: 'json',
            //contentType: "application/json; charset=utf-8",
            type: 'delete',
	    //data: inputData,
            success: function (data) {
                  location.reload(); 
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error- Listing', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
	    //$('#addongroup').modal('hide');
            }
        });		
}
function reloadMealItems(menu_id) {

        //var menuItemRestaurantId = $('#menuItemRestaurantId').val();
        var url = '/v2/menu/item-relatedproduct/list/'+ menu_id;

        $.ajax({
            url: url,
            dataType: 'json',
            //contentType: "application/json; charset=utf-8",
            type: 'get',
	    //data: inputData,
            success: function (data) {
                $('#additemform').html(data.data);
                $("#searchiteminput").on("keyup", function () {
                    var value = $(this).val().toLowerCase();
                    $("#myUL li").filter(function () {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error-Addon Group Listing', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
	    //$('#addongroup').modal('hide');
            }
        });
    }

    function addAddonItems() {

//        var menuItemRestaurantId = $('#menuItemRestaurantId').val();
        var url = '/v2/menu/item-relatedproduct/{{$menu_id}}/add';
        var inputData = $('#additemform').serialize();

        $.ajax({
            url: url,
            dataType: 'json',
//            contentType: "application/json; charset=utf-8",
            type: 'post',
            data: inputData,
            success: function (data) {
                var alertboxHeader = 'Success';
                if (data.message.indexOf('Error') !== -1) {
                    alertboxHeader = 'Error';
                }
                alertbox(alertboxHeader, data.message, function (modal) {
                    //addonGroupPencilToClick = $('#additemform').find('input[name="addon_group_id"]').val();
                    //getAddonGroups();
                    setTimeout(function () {
                        modal.modal('hide');
			window.location = "/v2/menu/item/related_products/{{$menu_id}}/list";
                    }, 2000);

                });
            },
            error: function (data, textStatus, errorThrown) {
                var err = '';
                $.each(data.responseJSON.errors, function (key, value) {
                    err += '<p>' + value + '</p>';
                });

                alertbox('Error- item addon add', err, function (modal) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 2000);
                });
            },
            complete: function (data) {
		//$('#addongroup').modal('hide');
            }
        });
    }

     
 
$(document).ready( function () {
	     var tabl = $('#table_id').DataTable(  {
			responsive: true,
            dom:'<"top"fiB><"bottom"trlp><"clear">',
            buttons: [
                {
                    text: 'Add Item',
                    action: function () {
			reloadMealItems({{$menu_id}});
		    },
		     className: ' data-newworkorder'
                },
                
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye"></i>',
                    titleAttr: 'Column Visibility'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                }, 
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                },
                {               
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    exportOptions: {
                        columns: ':visible',
                    },
                }
            ],
            columnDefs: [ {
                targets: -1,
                visible: true
            }],
            language: {
                search: "",
                searchPlaceholder: "Search"
            },
            responsive: true
    });
    $('.data-newworkorder') .attr('data-toggle', 'modal') .attr('data-target', '#additem'); 
    $('#saveAddonItemsToGroup').click(function () {  addAddonItems();  });
});
</script>