@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
    <div class="reservation__atto">
        <div class="reservation__title">
            <h1>Menu Item - Add/Edit Availability</h1>
            <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
        </div>
    </div>

    <div class="row" style="padding-top: 20px;">
        <label for="language" class="hide col-md-1 col-form-label no-col-padding text-md-right" style="padding-top: 5px">{{ __('Language') }}</label>
        <div class="col-md-3">
            <select name="language" id="language" class="hide form-control{{ $errors->has('language') ? ' is-invalid' : '' }}"required>
                <option value="1" >English</option>
                <option value="2" >Chinese</option>              
            </select>
        </div>
    </div>

    <div  class="card margin-top-0">
		@if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="card-body">
            <div class="">
            @include('menumanagement::v2_menu_item.tab')
                <div class="tab-content">
                @include('menumanagement::common.message')
 			        <div id="price" class="{{($section_tab=='price')?'fade in active':''}}">
		                <form method="POST" action="/v2/menu/item/{{$menu_id}}/updateavailability" enctype="multipart/form-data" id="form1">
		                    @csrf
				    
				            <div class="row addtionalRow2">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 margin-top-20">
                                    <h4>Avaiability</h4>

                                    <div class="row">
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                            <label for="pos_code" class="col-form-label text-md-right padbot">Start Time</label>
                                        </div>
                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                            <div class="input__field"><input type="text" class=" color-black fullWidth time_start icotime"   name="start_time" id="time_start" placeholder="Start Time" /></div>
                                        </div>
                                        <div class="col-sm-0 col-md-0 col-lg-2 col-xs-12 tabhide">&nbsp;</div>

                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                            <label for="pos_code" class="col-form-label text-md-right padbot">End Time</label>
                                        </div>
                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                            <div class="input__field"><input type="text" class=" color-black fullWidth time_end icotime"   name="end_time" id="time_end" placeholder="End Time" /></div>
                                        </div>
                                    </div>

                                    <div class="row margin-top-20">
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                            <label for="pos_code" class="col-form-label text-md-right padbot">Start Date</label>
                                        </div>
                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                            <div class="input__field"><input type="text" class=" color-black fullWidth date_start icocal"   name="start_date" id="start_date" placeholder="Start Date" /></div>
                                        </div>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12 tabhide">&nbsp;</div>

                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                            <label for="pos_code" class="col-form-label text-md-right padbot">End Date</label>
                                        </div>
                                        <div class="col-sm-4 col-md-4 col-lg-2 col-xs-12">
                                            <div class="input__field"><input type="text" class=" color-black fullWidth date_end icocal"   name="end_date" id="start_date" placeholder="End Date" /></div>
                                        </div>
                                    </div>

                                    <div class="row margin-top-20">
                                        <h4>Reccurence</h4>
                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                            <label for="pos_code" class="col-form-label text-md-right padbot">Days</label>
                                        </div>
                                        <div class="col-sm-10 col-md-10 col-lg-10 col-xs-12 formob">
                                            <div class="option__wrapper">
                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Sunday"> Sunday <span class="control_indicator"></span>
                                                </label>
                                            </div>
                                            <div class="option__wrapper">
                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Monday"> Monday <span class="control_indicator"></span>
                                                </label>
                                            </div>
                                            <div class="option__wrapper">
                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Tuesday"> Tuesday <span class="control_indicator"></span>
                                                </label>
                                            </div>
                                            <div class="option__wrapper">
                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Wednesday"> Wednesday <span class="control_indicator"></span>
                                                </label>
                                            </div>
                                            <div class="option__wrapper">
                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Thursday"> Thursday <span class="control_indicator"></span>
                                                </label>
                                            </div>
                                            <div class="option__wrapper">
                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Friday"> Friday <span class="control_indicator"></span>
                                                </label>
                                            </div>
                                            <div class="option__wrapper">
                                                <label class="custom_checkbox relative padding-left-25 font-weight-600"><input class="hide permission" id="" name="days[]" type="checkbox" value="Saturday"> Saturday <span class="control_indicator"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

				            <input type="hidden" name="id" value="{{isset($menu_price['id'])?$menu_price['id']:0}}" />  
				            <input type="hidden" name="menu_id" value="{{$menu_id}}" />  
                            <div style="margin-top: 50px;" class="row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                                </div>
                            </div>     
                        </form>
	 		        </div>
 		        </div>
 	        </div>
	    </div>
	</div>
</div>

<link href="{{asset('css/jquery.datetimepicker.css')}}" rel='stylesheet' />
<script type="text/javascript" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>

<script>
$(document).ready(function(){
    $('.date_start').datetimepicker({
        viewMode: 'days',
        format: 'DD/MM/YYYY'
    });     
});
 
    /*$avaiability=[];
                if($request->filled('days')){
                    $days=implode(',',$data['days']);
                    $avaiability['days']=$days;
                }
                $avaiability['start_time']=$request->filled('start_time')?$request->input('start_time'):'';
                $avaiability['end_time']=$request->filled('end_time')?$request->input('end_time'):'';
                $avaiability['start_date']= $request->input('start_date')?date('Y-m-d', strtotime($request->input('start_date'))):'';
                $avaiability['end_date']=$request->input('end_date')? date('Y-m-d', strtotime($request->input('end_date'))):'';

                if(count($avaiability)){
                    $avaiability['deal_id']=$dealobj->id;
                    DealAvailability::create($avaiability);
                }*/
</script>    
 

@endsection
