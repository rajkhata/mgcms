@extends('layouts.app')
@section('content')

<div class="main__container container__custom">

        <div class="reservation__atto">
          <div class="reservation__title">
            <h1>Menu Item - Add</h1>
            <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
          </div>
        </div>
        <div class="row" style="padding-top: 20px;">
            <label for="language" class="col-md-1 col-form-label no-col-padding text-md-right" style="padding-top: 5px">
               {{ __('Language') }}
            </label>
             <div class="col-md-3">
              <select name="language" id="language" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }}"required>
                  <option value="1" >English</option>
                  <option value="2" >Chinese</option>              
              </select>
             </div>
        </div>
        <div  class="card ">
            <div class="card-body">
              <div class="">

                    @include('menumanagement::v2_menu_item.tab')


                <div class="tab-content">
                  @include('menumanagement::common.message')
 			<div id="attribute" class="{{($section_tab=='general')?'fade in active':''}}">
		           
		          <form method="POST" action="{{ route('item.store') }}" enctype="multipart/form-data">
		           @csrf
				   <div class="form-group row form_field__container">
		                    <label for="restaurant_id"
		                        class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
		                    <div class="col-md-4">
		                        <select name="restaurant_id" id="restaurant_id"
		                            class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
		                            required>
		                            <option value="">Select Restaurant</option>
		                            @foreach($groupRestData as $rest)
		                            <optgroup label="{{ $rest['restaurant_name'] }}">
		                                @foreach($rest['branches'] as $branch)
		                                <option value="{{ $branch['id'] }}" {{ (old('restaurant_id')==$branch['id']) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
		                                @endforeach
		                            </optgroup>
		                            @endforeach
		                        </select>
		                        @if ($errors->has('restaurant_id'))
		                        <span class="invalid-feedback">
		                        <strong>{{ $errors->first('restaurant_id') }}</strong>
		                        </span>
		                        @endif
		                    </div>
		                </div>
		                <div class="form-group row form_field__container">
		                    <label for="menu_category_id"
		                        class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>
		                    <div class="col-md-4">
		                        <select name="menu_category_id" id="menu_category_id"
		                            class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}">
		                            <option value="">Select Category</option>
		                             
		                            @foreach($menu_category as $cat)
		                            <option value="{{ $cat->id }}" {{ (old('menu_category_id')==$cat->id) ? 'selected' :'' }}>{{ $cat->name }}</option>
		                            @endforeach
		                            
		                        </select>
		                        @if ($errors->has('menu_category_id'))
		                        <span class="invalid-feedback">
		                        <strong>{{ $errors->first('menu_category_id') }}</strong>
		                        </span>
		                        @endif
		                    </div>
		                </div>
		                <div class="form-group row form_field__container">
		                    <label for="menu_sub_category_id"
		                        class="col-md-4 col-form-label text-md-right">{{ __('Sub Category') }}</label>
		                    <div class="col-md-4">
		                        <select name="menu_sub_category_id" id="menu_sub_category_id"
		                            class="form-control {{ $errors->has('menu_sub_category_id') ? ' is-invalid' : '' }}">
		                            <option value="">Select Sub-Category</option>
		                             
		                        </select>
		                        @if ($errors->has('menu_sub_category_id'))
		                        <span class="invalid-feedback">
		                        <strong>{{ $errors->first('menu_sub_category_id') }}</strong>
		                        </span>
		                        @endif
		                    </div>
		                </div>  
				@foreach($ma_attribute as $attr)
		                      @if($attr['backend_field_type']=='Dropdown')   
					<div class="form-group row form_field__container">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}"
				                class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-4">
				                <select name="menu_category_id" id="menu_category_id"
				                    class="form-control {{ $errors->has('menu_category_id') ? ' is-invalid' : '' }}">
				                    <option value="">{{$attr['attribute_prompt']}}</option>
				                     
				                    @foreach($attr['items'] as $cat)
				                    <option value="{{ $cat['attribute_item_value']}}">{{ $cat['attribute_item_name']}}</option>
				                    @endforeach
				                    
				                </select>
				                @if ($errors->has('menu_category_id'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('menu_category_id') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div> 
				      @endif
				      @if($attr['backend_field_type']=='Text') 
					<div class="form-group row form_field__container">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-4">
				                <div class="input__field">
				                    <input id="name" type="text"
				                        class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
				                        name="{{str_replace(' ','_',$attr['attribute_label'])}}" value="{{ old('name') }}" required>
				                </div>
				                @if ($errors->has('name'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('name') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div>
				      @endif
				      @if($attr['backend_field_type']=='Editor') 
					<div class="form-group row form_field__container dblock">
				            <label for="{{str_replace(' ','_',$attr['attribute_label'])}}"
				                class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
				            <div class="col-md-8">
				                <div class="custom__message-textContainer">
				                    <textarea maxlength="500" rows="4" name="{{str_replace(' ','_',$attr['attribute_label'])}}" id="{{str_replace(' ','_',$attr['attribute_label'])}}"
				                        class="editor form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
				                        ></textarea>
				                </div>
				                @if ($errors->has('description'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('description') }}</strong>
				                </span>
				                @endif
				            </div>
				        </div>
				      @endif
				      @if($attr['backend_field_type']=='Checkbox') 
					<div class="form-group row form_field__container">
					@foreach($attr['items'] as $cat)
				        <label for="is_popular" class="col-md-4 col-form-label text-md-right">{{str_replace(' ','_',$attr['attribute_label'])}}</label>
				        <div class="col-md-4">
				            <div class="input__field">
				                <input id="{{str_replace(' ','_',$cat['attribute_item_name'].$cat['attribute_item_value'])}}" name="{{str_replace(' ','_',$cat['attribute_item_name'])}}"
				                       class="{{ $errors->has('is_popular') ? ' is-invalid' : '' }}"
				                        value="1" >
				            </div>
				            @if ($errors->has('is_popular'))
				                <span class="invalid-feedback">
				                <strong>{{ $errors->first('is_popular') }}</strong>
				                </span>
				            @endif
				        </div>
					@endforeach
				    </div>
				      @endif
				      @if($attr['backend_field_type']=='Radio') 
					<div class="form-group row form_field__container">
					    @foreach($attr['items'] as $cat)
				            <label for="status"
				                class="col-md-4 col-form-label text-md-right">{{$cat['attribute_item_name']}}</label>
				                <div class="col-md-4" style="padding-top: 10px;">
				                <label class="radio-inline"> <input type="radio" id="{{str_replace(' ','_',$cat['attribute_item_name'].$cat['attribute_item_value'])}}" name="{{str_replace(' ','_',$cat['attribute_item_name'])}}"
				                value="{{$cat['attribute_item_value']}}" > {{$cat['attribute_item_name']}}</label>
				                &nbsp;&nbsp;&nbsp;&nbsp; 
				                @if ($errors->has('status'))
				                <span class="invalid-feedback" style="display:block;">
				                <strong>{{ $errors->first('status') }}</strong>
				                </span>
				                @endif
						@endforeach
				            </div>
				        </div>
				      @endif
				      @if($attr['backend_field_type']=='File') 
					<!-- Mobile app Image-->
					    <div class="form-group row form_field__container source-image-div image-caption-div dblock">
						<label for="image" class="col-md-4 col-form-label text-md-right">{{$attr['attribute_label']}}</label>
						<div class="col-md-8">
						    <div class="image_cont_main clearfix">
						        <div class="col-md-10 no-col-padding">
						            <input id="{{str_replace(' ','_',$attr['attribute_label'])}}" type="file"
						                class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
						                value="{{ old('image.0') }}" name="{{str_replace(' ','_',$attr['attribute_label'])}}[]">
						            @if ($errors->has('image'))
						            <span class="invalid-feedback">
						            <strong>{{ $errors->first('image') }}</strong>
						            </span>
						            @endif
						            <div class="input__field">
						                <input id="image_caption_{{str_replace(' ','_',$attr['attribute_label'])}}" type="text" class="{{ $errors->has('image_caption') ? ' is-invalid' : '' }}"
						                    name="image_caption_{{str_replace(' ','_',$attr['attribute_label'])}}[]" value="{{ old('image_caption.0') }}" placeholder="Image Caption">
						            </div>
						            @if ($errors->has('image_caption'))
						            <span class="invalid-feedback">
						                <strong>{{ $errors->first('image_caption') }}</strong>
						            </span>
						            @endif
						        </div>
						        <div class="col-md-2">
						            <button type="button" imagetype="image" class="btn btn__primary add_new_image">Add New</button>
						        </div>
						    </div>
						</div>
					    </div>
					</div>
				      @endif 					   
		               @endforeach 
				<div style="margin-top: 50px;" class="row mb-0">
				<div class="col-md-12 text-center">
				<button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
				</div>
				</div>     
	 		  </form>
	 		</div>
 		</div>
 	     </div>
	   </div>
	</div>
</div>

     

    

    
<script type="text/javascript">
        
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    
            $("#restaurant_id").change(function () {
                var rest_id = '';
                rest_id = $(this).val();	
                $('#menu_category_id').find('option').not(':first').remove();
                if (rest_id != '') {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/v2/menu/get_category"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rest_id=' + rest_id,
                        success: function (data) {
                            if ($.type(data.dataObj) !== 'undefined') {
                                if ($.type(data.dataObj) !== 'undefined') {
                                    $.each(data.dataObj, function (i, item) {
                                        $('#menu_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    });
                                }
                           }
                        }
                    });
                }
            });
            $("#menu_category_id").change(function () {
                var rest_id = $('#restaurant_id').val();
                var cat_id = $(this).val();
                console.log(rest_id + ' - ' + cat_id);
                $('#menu_sub_category_id').find('option').not(':first').remove();
                if (rest_id != '' && cat_id != '') {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo URL::to('/') . "/v2/menu/get_sub_category"; ?>',
                        data: '_token = <?php echo csrf_token() ?>&rid=' + rest_id + '&cat_id=' + cat_id,
                        success: function (data) {
                            $('#loader').addClass('hidden');
                            if ($.type(data.dataObj) !== 'undefined') { 
				$.each(data.dataObj, function (i, item) {
				    $('#menu_sub_category_id').append('<option value="' + item.id + '">' + item.name + '</option>');
                                });
                            }
                        }
                    });
                }
            });
             
         
    
    
    
      
</script>
<script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    //code added by jawed to facilitate FCK editor
    $( document ).ready(function() {
        tinymce.init({
            selector: 'textarea.editor',
            menubar:false,
            height: 320,
            theme: 'modern',
            plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
        });
    });
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
function selecttab(id){
	$( "#generaltab" ).click();
	$( "#attribute" ).removeClass('active');
}
function productTypeChange(){
	var id = $("#select_product_type").val();
	var htm = '';
	jQuery('.attributelist').html(htm);
	if(id>0){
		$.get('/v2/systemattribute/list/producttype/' + id, function (data) {
		   $.each(data.list, function(key, value) { 	 
		        htm += ' <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12" style="padding-bottom: 10px;"><label class="radio-inline">';
			htm += '<input type="checkbox" id="attribute_group" name="attribute_group[]" value="'+value.id+'" >'+value.attribute_label+'</label> &nbsp;&nbsp;&nbsp;&nbsp; </div>';
		   });
		
	     	jQuery('.attributelist').html(htm);
		});		
	}
}
</script>

@endsection
