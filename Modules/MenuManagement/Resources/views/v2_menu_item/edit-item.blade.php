@extends('layouts.app')
@section('content')

<div class="main__container container__custom">

        <div class="reservation__atto">
          <div class="reservation__title">
            <h1>Menu Item - Edit</h1>
            <style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
          </div>
        </div>
         
        <div>
	    <div id="cms-accordion-msg">
		@if(session()->has('message'))
		        <div class="alert alert-success margin-top-20">
		            {{ session()->get('message') }}
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                <span aria-hidden="true">&times;</span>
		            </button>
		        </div>
		
		@endif
		</div>	
            <div  class="card ">
                <div class="card-body">
                    <div class="">

                        @include('menumanagement::v2_menu_item.tab')

                <div class="tab-content">
                 
                   @include('menumanagement::common.message')

                <div id="attribute" class="{{($section_tab=='attribute')?'fade in active':''}}">
                   
                  <form method="POST" action="/v2/menu/item/{{$menu_data['static']['id']}}/update-type" enctype="multipart/form-data" id="form1" onsubmit="return checkProductTypeSelected();">
                   @csrf
		   <div class="col-sm-12 col-md-6 col-lg-3 col-xs-12">
                                <label for="product_type"
                                   class="col-form-label text-md-right">{{ __('Attribute Set') }}</label>
                                <select name="attribute_set_id" id="attribute_set_id" class="form-control {{ $errors->has('attribute_set_id') ? ' is-invalid' : '' }}" onchange="productTypeChange()">
                                    <option value="0">Attribute Group</option>
                                    @foreach($menuAttributeSet as $key=>$val)
					@if($menu_data['static']['attribute_set_id']==$val['id'])
                                        <option value="{{ $val['id'] }}" selected>{{ $val['name'] }}</option>
					@endif
                                    @endforeach
                                </select>
                                @if ($errors->has('attribute_set_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('attribute_set_id') }}</strong>
                                    </span>
                                @endif
                   </div> 
		   <div class="col-sm-12 col-md-6 col-lg-3 col-xs-12">
                                <label for="product_type"
                                   class="col-form-label text-md-right">{{ __('Product Type') }}</label>
                                <select name="product_type_id" id="product_type_id" class="form-control {{ $errors->has('select_product_type') ? ' is-invalid' : '' }}" onchange="productTypeChange()">
                                    <option value="0">Select Product Type</option>
                                    @foreach($product_types as $key=>$val)
					@if($menu_data['static']['product_type_id']==$val['id'])
                                        <option value="{{ $val['id'] }}" selected>{{ $val['name'] }}</option>
					@endif
                                    @endforeach
                                </select>
                                @if ($errors->has('product_type_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('product_type_id') }}</strong>
                                    </span>
                                @endif
                   </div><br><br><br><br>
                   <div class="form-group row form_field__container attributelist">
			 
		   </div>  
		   <span> <button type="submit" form="form1"  class="btn btn__primary" value="Submit">{{ __('Update') }}</button> </span>   
		 </form> 
                </div>        
 		</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
 </div>
 </div>
 </div>
 </form>
 </div>
 </div>
 </div>

<script> 
productTypeChange()
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
function selecttab(id){
	$( "#generaltab" ).click();
	$( "#attribute" ).removeClass('active');
}
function checkProductTypeSelected(){
	var id = $( "#product_type_id" ).val();
	var attribute_set_id = $("#attribute_set_id").val(); 
	if(attribute_set_id>0){
		if(id>0){alert(id)
			if(id==2){ //check customizable attribute selected or not
				alert(id)
				var attribute_ids_length = $('input[name="attribute_ids[]"]:checked').length; 
				alert(attribute_ids_length)
				if(attribute_ids_length>0)return true;
				else{
				 	showMessage('Select Atlease one customizable attribute' , 0);
				 	return false;
				}
			}else{
				return true;
			}
		}else{
		  showMessage('Select Product type' , 0);
		 return false; 
		}
	}else{
		  showMessage('Select Attribute Group' , 0);
		 return false; 
	}
}
function productTypeChange(){
	var id = $("#product_type_id").val(); 
	var htm = '';
        jQuery('.attributelist').html(htm); 
	var attribute_set_id = $("#attribute_set_id").val(); 
	if(attribute_set_id>0){
		if(id!=0){
		//if(id<10){	
			if(id>0){
				jQuery('#cms-accordion-msg').html('');		
				$.get('/v2/systemattribute/list/producttype/' + id + '/'+attribute_set_id, function (data) {
				   $.each(data.list, function(key, value) { 	 
					htm += ' <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12" style="padding-bottom: 10px;"><label class="radio-inline">';
					htm += '<input type="checkbox" id="attribute_ids" name="attribute_ids[]" value="'+value.id+'" checked>'+value.attribute_label+'</label> &nbsp;&nbsp;&nbsp;&nbsp; </div>';
				   });
				
			     	jQuery('.attributelist').html(htm);
				});		
			}else{  
				showMessage('Select Product type' , 0);
			}
		}else if(id==0) {
			 
				showMessage('Select Product type' , 0);
		}
	}else if(attribute_set==0) {
			 
				showMessage('Select Attribute Group' , 0);
	}
}

function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg').html(msg);	
         
}
</script>

@endsection
