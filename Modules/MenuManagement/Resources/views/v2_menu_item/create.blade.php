@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
	<div class="reservation__atto">
		<div class="reservation__title">
			<h1>Menu Item - Add</h1>
			<style> .image_cont_main{top: 14px!important; position: relative;} .source-image-div .col-md-8{}</style>
		</div>
	</div>

    <div class="row" style="padding-top: 20px;">
        <label for="language" class="hide col-md-1 col-form-label no-col-padding text-md-right" style="padding-top: 5px">
            {{ __('Language') }}
        </label>
        <div class="col-md-3">
            <select name="language" id="language" class="hide form-control{{ $errors->has('language') ? ' is-invalid' : '' }}"required>
                  <option value="1" >English</option>
                  <option value="2" >Chinese</option>              
            </select>
		</div>
	</div>
	
    <div>
	    <div id="cms-accordion-msg">
			@if(session()->has('message'))
			<div class="alert alert-success margin-top-20">
				{{ session()->get('message') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			@endif
		</div>	
			
		<div class="card margin-top-0">
			<div class="card-body">
                <div class="">
                    <ul class="nav nav-tabs">
						<li class="{{($section_tab=='attribute')?'active':' '}}"><a data-toggle="tab" href="#">Type</a></li>
					</ul>
					
                	<div class="tab-content">
                   		@include('menumanagement::common.message')
						<div id="attribute" class="{{($section_tab=='attribute')?'fade in active':''}}">
                   			<form method="POST" action="/v2/menu/item/generalinfo" enctype="multipart/form-data" id="form1" onsubmit="return checkProductTypeSelected();">
								@csrf
								<div class="form-group row">
									<div class="col-sm-12 col-md-6 col-lg-3 col-xs-12 margin-top-10">
										<label for="product_type" class="col-form-label text-md-right">{{ __('Attribute Set') }}</label>
										<div class="selct-picker-plain position-relative">
											<select name="attribute_set_id" id="attribute_set_id" class="form-control {{ $errors->has('attribute_set_id') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" onchange="productTypeChange()">
												<option value="0">Attribute Group</option>
												@foreach($menuAttributeSet as $key=>$val)
													<option value="{{ $val['id'] }}" >{{ $val['name'] }}</option>
												@endforeach
											</select>
										</div>
										@if ($errors->has('attribute_set_id'))
											<span class="invalid-feedback">
												<strong>{{ $errors->first('attribute_set_id') }}</strong>
											</span>
										@endif
									</div> 

									<div class="col-sm-12 col-md-6 col-lg-3 col-xs-12 margin-top-10">
										<label for="product_type" class="col-form-label text-md-right">{{ __('Product Type') }}</label>
										<div class="selct-picker-plain position-relative">
											<select name="product_type_id" id="product_type_id" class="form-control {{ $errors->has('select_product_type') ? ' is-invalid' : '' }} selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" onchange="productTypeChange()">
												<option value="0">Select Product Type</option>
												@foreach($product_types as $key=>$val)
													<option value="{{ $val['id'] }}" >{{ $val['name'] }}</option>
												@endforeach
											</select>
										</div>
										@if ($errors->has('product_type_id'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('product_type_id') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="form-group row attributelist" style="display:none;">
									<div class="col-sm-6 col-md-12 col-lg-12 col-xs-12" style="padding-bottom: 10px;">
									<div class="clearfix ptb5">
									@foreach($menuAttributes as $key=>$val)
										
											<div class="option__wrapper pr15">
											<!-- <label class="radio-inline">
												<input type="checkbox" id="{{$val['id']}}" name="customizable_attribute_ids[]" value="{{$val['id']}}">{{$val['attribute_label']}}
											</label> -->
											<label class="custom_checkbox relative padding-left-25 font-weight-600 pr15"><input class="hide permission" type="checkbox" id="{{$val['id']}}" name="customizable_attribute_ids[]" value="{{$val['id']}}">{{$val['attribute_label']}} <span class="control_indicator"></span>
                                    		</label>
											</div>
										
									@endforeach
									</div>
									</div>
								</div>  

								<div class="form-group row">
									<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
										<button type="submit" form="form1"  class="btn btn__primary" value="Submit">{{ __('Create') }}</button> 
									</div> 
								</div>  
		 					</form> 
                		</div>   
                	</div>
                </div>
                </div>
            </div>
        </div>
    </div>
 </div>
 </div>
 </div>
 </form>
 </div>
 </div>
 </div>

<script> 
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
function selecttab(id){
	$( "#generaltab" ).click();
	$( "#attribute" ).removeClass('active');
}
function checkProductTypeSelectedOld(){
	var id = $( "#product_type_id" ).val();
	var attribute_set_id = $("#attribute_set_id").val(); 
	if(attribute_set_id>0){
		if(id>0){
			return true;
		}else{
		  showMessage('Select Product type' , 0);
		 return false; 
		}
	}else{
		  showMessage('Select Attribute Group' , 0);
		 return false; 
	}
}
function checkProductTypeSelected(){
	var id = $( "#product_type_id" ).val();
	var attribute_set_id = $("#attribute_set_id").val(); 
	if(attribute_set_id>0){
		if(id>0){ 
			if(id==2){ //check customizable attribute selected or not
				 
				var attribute_ids_length = $('input[name="customizable_attribute_ids[]"]:checked').length; 
				 
				//alert(attribute_ids_length)
				if(attribute_ids_length>0){
					jQuery('.attributelist').hide();
					return true;
				}else{
				 	showMessage('Please select at least one customizable attribute.' , 0);
				 	return false;
				}
			}else{
				jQuery('.attributelist').hide();
				return true;
			}
		}else{
		  showMessage('Select Product type' , 0);
		  return false; 
		}
	}else{
		  showMessage('Select Attribute Group' , 0);
		 return false; 
	}
}
function productTypeChange(){
	var id = $("#product_type_id").val(); 
	//var htm = '';
        jQuery('.attributelist').hide();
	var attribute_set_id = $("#attribute_set_id").val(); 
	if(attribute_set_id>0){
		if(id==2){
		//if(id<10){	
			if(id>0){
				jQuery('#cms-accordion-msg').html('');		
				/*$.get('/v2/systemattribute/list/producttype/' + id + '/'+attribute_set_id, function (data) {
				   $.each(data.list, function(key, value) { 
					 	 
					 htm += ' <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12" style="padding-bottom: 10px;"><label class="radio-inline">';
					 htm += '<input type="checkbox" id="'+value.id+'" name="customizable_attribute_ids['+key+']" value="'+value.id+'">'+value.attribute_label+'</label> &nbsp;&nbsp;&nbsp;&nbsp; </div>';
				   });
				
			     	jQuery('.attributelist').show();
				});*/
				jQuery('.attributelist').show();		
			}else{  
				jQuery('.attributelist').hide();
				showMessage('Select Product type' , 0);
			}
		}else if(id==0) {
			 jQuery('.attributelist').hide();
				showMessage('Select Product type' , 0);
		}
	}else if(attribute_set==0) {
			 jQuery('.attributelist').hide();
				showMessage('Select Attribute Group' , 0);
	}
}

function showMessage(msg , type){
	if(type==1)var msg = '<div class="alert alert-success margin-top-20">' + msg;
	else 	
	var msg = '<div class="alert alert-danger margin-top-20">' + msg;
            msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            msg += '<span aria-hidden="true">&times;</span>';
            msg += '</button> </div>';
	jQuery('#cms-accordion-msg').html(msg);	
         
}
</script>
@endsection