@extends('layouts.app')
@section('content')

<div class="main__container container__custom">
	<div class="reservation__atto">
		<div class="reservation__title col-md-9 no-col-padding">
			<h1>Menu Items - List</h1>
		</div>
	</div>

	<div class="addition__functionality menupage box-shadow">
		<div class="row functionality__wrapper margin-top-0">
			<div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
			    <div id="sortcol" class="selct-picker-plain position-relative">
					<select name="menu_sort_by" id="menu_sort_by" class="form-control selectpicker" 
					 data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" style="background:transparent!important">
						<option value="0" {{!isset($sortAlgo[0])?'Selected':''}}>Sort By</option>
						<option value="1" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==1)?'Selected':''}}>Move Inactive to bottom</option>
						<option value="2" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==2)?'Selected':''}}>Newest products first</option>
						<option value="3" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==3)?'Selected':''}}>Bestseller products first</option>
						<option value="4" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==4)?'Selected':''}}>Biggest Saving products first</option>
						<option value="5" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==5)?'Selected':''}}>Most Viewed products first</option>
						<option value="6" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==6)?'Selected':''}}>Name: Ascending</option>
						<option value="7" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==7)?'Selected':''}}>Name: Descending</option>
						<option value="8" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==8)?'Selected':''}}>Price: Ascending</option>
						<option value="9" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==9)?'Selected':''}}>Price: Descending</option>
						<option value="10" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==10)?'Selected':''}}>Sort Order: Ascending</option>
						<option value="11" {{(isset($sortAlgo[0]['sort_type_id']) && $sortAlgo[0]['sort_type_id']==11)?'Selected':''}}>Sort Order: Descending</option>
					</select>
			    </div>
			 </div>
			 
			<div class="col-md-1">
				<button class="btn btn__primary" style="margin-left: 5px;" type="submit" onclick="shortMenu()">Sort</button>
			</div>
		</div> 
		 
	    <div class="row form-group functionality__wrapper margin-top-0">
			<div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
			    <div id="sortcol" class="selct-picker-plain position-relative">
					<select name="category_filter_by" id="category_filter_by" class="form-control selectpicker" 
					 data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" style="background:transparent!important">
						<option value="0" >By Category</option>
						@foreach($menu_category as $rest)
						  <option value="{{ $rest['id'] }}" {{ ($category_filter_by==$rest['id']) ? 'selected' :'' }}>{{ $rest['name'] }}</option>
						@endforeach
					</select>
			    </div>
			 </div>
			 
			<div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
			    <div id="sortcol" class="selct-picker-plain position-relative">
					<select name="product_type_filter_by" id="product_type_filter_by" class="form-control selectpicker" 
					 data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" style="background:transparent!important">
						<option value="0" >By Product Type</option>
						@foreach($product_types as $key=>$val)
							  <option value="{{ $val['id'] }}" {{ ($product_type_filter_by==$val['id']) ? 'selected' :'' }}>{{ $val['name'] }}</option>
						@endforeach
					</select>
			    </div>
			</div>
			 
			<div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
			    <div id="sortcol" class="selct-picker-plain position-relative">
					<select name="status_filter_by" id="status_filter_by" class="form-control selectpicker" 
					 data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" style="background:transparent!important">
						<option value="0" >By Status</option>
						<option value="Inactive" {{ ($status_filter_by=="Inactive") ? 'selected' :'' }} >Inactive</option>
						<option value="Active"  {{ ($status_filter_by=="Active") ? 'selected' :'' }}>Active</option> 
					</select>
			    </div>
			 </div>
			 
			<div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
			    <div id="sortcol" class="selct-picker-plain position-relative">
					<select name="type_filter_by" id="type_filter_by" class="form-control selectpicker" 
					 data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" style="background:transparent!important">
						<option value="-1" >By Delivery & Tekeout</option>
						<option value="Delivery" {{ ($type_filter_by=="Delivery") ? 'selected' :'' }} >Delivery</option>
						<option value="Takeout"  {{ ($type_filter_by=="Takeout") ? 'selected' :'' }} >Takeout</option> 
					</select>
			    </div>
			 </div>
			 
			<div class="col-md-1">
				<button class="btn btn__primary" style="margin-left: 5px;" type="submit" onclick="filterMenu()">Search</button>
			</div>
	     </div>	
	</div> 

    <div>
        <div class="active__order__container">
            @if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
			@elseif (session()->has('err_msg'))
				<div class="alert alert-danger">
					{{ session()->get('err_msg') }}
				</div>
			@endif

            <div class="guestbook__container box-shadow no-margin-top">           
				<table id="table_id" class="responsive ui cell-border hover " style="width:100%; margin-bottom:10px;">
			    	<thead>
                		<tr>
                     		<th>Id</th>
							<th>Name</th><th>SKU</th><th>Price</th>
							<th>Product Type</th>
							<th>Category</th><th>Restaurant</th><th>Total Orders</th>
							<th>Img</th>
							<th>Status</th>
						</tr>
			    	</thead>
			   		<tbody>
					@if(isset($menuItems) && count($menuItems)>0)
                    @foreach($menuItems as $menuItem)
						<tr>
							<td> {{$menuItem->id}}</td>
				      		<td> {{$menuItem->item_name}}</td>
							<td> {{$menuItem->sku}}</td><td> {{$menuItem->currency_symbol}}{{number_format((float)$menuItem->price, 2, '.', '')}}</td>
					 		<td> {{$menuItem->product_type}}</td>
				    		<td> {{$menuItem->cat_name}} </td><td> {{$menuItem->restaurant_name}} </td>
				    		<td> {{$menuItem->noofitem_sold}} </td>
                            <td> <img src="{{$menuItem->image}}" width="50" height="50"> </td>
				    		<td data-order="{{$menuItem->id}}">
                      			<div class="waitList__otp no-padding" style="float: left; margin-right: 20px;">
                                	<div class="toggle__container" style="margin-left: 0;">
										@php
											if($menuItem->status){
												$ariaPressed = true;
												$activeClass = 'active';
											} else {
												$ariaPressed = false;
												$activeClass = '';
											}
										@endphp
										<button type="button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom menu_item_active {{ $activeClass }}" title="Open" data-toggle="button" data-menu="{{ $menuItem->id }}" aria-pressed="{{ $ariaPressed }}" autocomplete="off">
											<div class="handle"></div>
										</button>
                                    </div>
								</div>
                                <a style="margin-top: 10px;" href="{{ URL::to('v2/menu/item/' . $menuItem->id . '/editgeneralinfo') }}" class="glyphicon glyphicon-edit" ></a>
                                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;   
                                <a onclick="return confirm('Are you sure, you want to delete it?')" href="{{ URL::to('v2/menu/item/' . $menuItem->id . '/destroy') }}" class="glyphicon glyphicon-trash"></a>
							</td> 	
						</tr>
			    		@endforeach
                        @else
                        <tr> 
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
                        @endif
			  		</tbody>
			 	</table>
            </div>
		</div>
	</div>
	

    <script>
        $('.menu_item_active').on('click', function() {
            var menu_id = $(this).data('menu');
            var active_flag = 1;
            if($(this).hasClass('active')) {
                active_flag = 0;
            }
            $.ajax({
                type: 'delete',
                url: '/v2/menu/toggleactive/',
                type: 'POST',
                data: {
                    'menu_id': menu_id,
                    'active_flag': active_flag
                },
                success: function (data) {
                    alertbox('Updated',data.message,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                            panel.remove();
                        }, 2000);
                    });
                },
                error: function (data, textStatus, errorThrown) {
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                    });
                    alertbox('Error',err,function(modal){
                        setTimeout(function() {
                            modal.modal('hide');
                        }, 2000);
                    });
                }
            });
        });
	$(document).ready( function () {
	     var table = $('#table_id').DataTable(  {
			responsive: true,
		    	dom:'<"top"fiB><"bottom"trlp><"clear">',
			bSort: false,
		    	buttons: [
		        {
		            text: 'Add',
		            action: function () {location.href = "/v2/menu/item/create";}
		        },
		       {
		            extend: 'colvis',
		            text: '<i class="fa fa-eye"></i>',
		            titleAttr: 'Column Visibility'
		        },
		        {
		            extend: 'excelHtml5',
		            text: '<i class="fa fa-file-excel-o"></i>',
		            titleAttr: 'Excel'
		        }, 
		        {
		            extend: 'pdfHtml5',
		            text: '<i class="fa fa-file-pdf-o"></i>',
		            titleAttr: 'PDF'
		        },
		        {               
		            extend: 'print',
		            text: '<i class="fa fa-print"></i>',
		            titleAttr: 'Print',
		            exportOptions: {
		                columns: ':visible',
		            },
		        }
		    ],
		    columnDefs: [ {
		        targets: -1,
		        visible: true
		    }],
		    language: {
		        search: "",
		        searchPlaceholder: "Search"
		    },
		    responsive: true
		});
		$("#list_filter input").on('keyup click', function() {
		    table.columns([0, 1, 2, 3]).search($(this).val()).draw();
		});
        //$('#table_id').show();
        //$('#table_id').removeClass('hidden');
	} );
    function shortMenu(){
	var type_id = $('#menu_sort_by').val();
	//if(type_id>0)
	window.location = '/v2/menu/item/0/{{$rest_id}}/'+type_id+'/list'; 
    }
    function filterMenu(){
	var type_id = $('#menu_sort_by').val();
	var category_filter_by = $('#category_filter_by').val();
	var product_type_filter_by = $('#product_type_filter_by').val();
	var status_filter_by = $('#status_filter_by').val();
	var type_filter_by = $('#type_filter_by').val();
	window.location = '/v2/menu/item/0/{{$rest_id}}/'+type_id+'/'+category_filter_by+'/'+product_type_filter_by+'/'+status_filter_by+'/'+type_filter_by+'/list'; 
    }	
    </script>
 
@endsection
