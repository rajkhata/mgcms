@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Localization - Edit') }}</h1>

    </div>

  </div>

  <div class="card form__container">
    @if(session()->has('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}
    </div>
    @endif
    <div class="card-body">
      <div class="form__user__edit">
          <form method="POST" action="/menu/localization/update/{{$localize->id}}" accept-charset="UTF-8" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="PUT">
              <input name="id" type="hidden" value="{{$localize->id}}">
        <?php //{{ Form::model($localize, array('route' => array('localization.update', $localize->id), 'method' => 'PUT', 'files' => true)) }} ?>
        @csrf
        <div class="form-group row form_field__container">
          <label for="language_id" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
          <div class="col-md-4">
            <select name="language_id" id="lang_id" class="form-control" >
              <option value="">Select Language</option>
              @foreach($languages as $lang)
              <option value="{{ $lang->id }}" {{ (old('lang_id')==$lang->id || (count(old())==0 && $localize->language_id==$lang->id)) ? 'selected' :'' }}>{{ $lang->language_name }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group row form_field__container">
          <label for="key" class="col-md-4 col-form-label text-md-right">{{ __('Localize Key') }}</label>
          <div class="col-md-4">
            <div class="input__field">
              <input id="key" type="text" class="{{ $errors->has('key') ? ' is-invalid' : '' }}"
              name="key" value="{{ count(old())==0 ? $localize->key : old('key') }}" required autocomplete="off">
            </div>
            @if ($errors->has('fname'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('key') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group row form_field__container">
          <label for="value" class="col-md-4 col-form-label text-md-right">{{ __('Localize Name') }}</label>
          <div class="col-md-4">
            <div class="input__field">
              <input id="value" type="text" class="{{ $errors->has('value') ? ' is-invalid' : '' }}"
              name="value" value="{{ count(old())==0 ? $localize->value : old('value') }}" required autocomplete="off">
            </div>
            @if ($errors->has('value'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('value') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group row form_field__container">
          <label for="platform" class="col-md-4 col-form-label text-md-right">{{ __('platform') }}</label>
          <div class="col-md-4">
            <select name="platform" id="platform" class="form-control{{ $errors->has('platform') ? ' is-invalid' : '' }}" >
              <option value="all" {{($localize->platform =='all') ? 'selected' :''}}>All</option>
              <option value="web" {{($localize->platform =='web') ? 'selected' :''}}>Web</option>
              <option value="ios" {{($localize->platform =='ios') ? 'selected' :''}}>App(ios)</option>
              <option value="android" {{($localize->platform =='android') ? 'selected' :''}}>App(android)</option>
            </select>
          </div>
        </div>
        <div class="form-group row form_field__container">
          <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
          <div class="col-md-4" style="padding-top: 10px;">
            <input type="radio" id="status1" name="status" value="1" {{ (old('status')=='1' || (count(old())==0 && $localize->status=='1')) ? 'checked' :'' }}> Active
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" id="status0" name="status" value="0" {{ (old('status')=='0' || (count(old())==0 && $localize->status=='0')) ? 'checked' :'' }}> Inactive
            @if ($errors->has('status'))
            <span class="invalid-feedback" style="display:block;">
              <strong>{{ $errors->first('status') }}</strong>
            </span>
            @endif
          </div>
        </div>


        <br /><br />
        <div class="row text-center mb-0">
          <div class="text-center col-md-12">
            <button type="submit" class="btn btn__primary">{{ __('Update') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


</div>
@endsection
