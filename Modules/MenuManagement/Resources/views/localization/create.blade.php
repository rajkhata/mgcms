@extends('layouts.app')
@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Localization - Create') }}</h1>

    </div>

  </div>
  <div>

    <div class="card form__container">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form_localization">
          <form method="POST" action="/menu/localization/store" enctype="multipart/form-data">
            @csrf
            <div class="form-group row form_field__container">
              <label for="lang_id" class="col-md-4 col-form-label text-md-right">{{ __('Language') }}</label>
              <div class="col-md-4">
                <select name="language_id" id="lang_id" class="form-control" >
                  @foreach($languages as $lang)
                  <option value="{{ $lang->id }}" >{{ $lang->language_name }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('Localize Key') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="key" type="text" class="{{ $errors->has('key') ? ' is-invalid' : '' }}"
                  name="key" value="{{ old('key') }}" required autocomplete="off">
                </div>
                @if ($errors->has('key'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('key') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group row form_field__container">
              <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Localize Name') }}</label>
              <div class="col-md-4">
                <div class="input__field">
                  <input id="value" type="text" class="{{ $errors->has('value') ? ' is-invalid' : '' }}"
                  name="value" value="{{ old('value') }}" required autocomplete="off">
                </div>
                @if ($errors->has('value'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('value') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="platform" class="col-md-4 col-form-label text-md-right">{{ __('Platform') }}</label>
              <div class="col-md-4">
                <select name="platform" id="platform" class="form-control{{ $errors->has('platform') ? ' is-invalid' : '' }}" >
                  <option value="all" >All</option>
                  <option value="web" >Web</option>
                  <option value="ios" >App(ios)</option>
                  <option value="android" >App(android)</option>
                </select>
              </div>
            </div>


            <div class="form-group row form_field__container">
              <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
              <div class="col-md-4" style="padding-top: 10px;">
                <input type="radio" id="status1" name="status" value="1" checked> Active
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="status0" name="status" value="0" > Inactive
                @if ($errors->has('status'))
                <span class="invalid-feedback" style="display:block;">
                  <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
              </div>
            </div>


            <br /><br />
            <div class="row  mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">{{ __('Create') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection
