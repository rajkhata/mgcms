@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Users<span style="margin:0px 5px;">({{ $localization->currentPage() .'/' . $localization->lastPage() }})</span>&emsp;</h1>
    </div>
    <span style="float: right;margin:0px 5px;"><a href="{{ URL::to('menu/localization/create') }}" class="btn btn__primary">Add</a></span>
  </div>
  <div class="addition__functionality">
    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">

      <div class="row form-group col-md-2">
        <div class="input__field">
          <input id="sName" class="" type="text" name="name" value="{{isset($searchArr['name']) ? $searchArr['name'] : ''}}">
          <label for="sName">Name</label>
        </div>
      </div>
      <div class="row form-group col-md-3">
        <button class="btn btn__primary" style="margin-left: 5px;" type="submit" >Search</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>

  <div>

    <div class="active__order__container">

      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @elseif (session()->has('err_msg'))
      <div class="alert alert-danger">
        {{ session()->get('err_msg') }}
      </div>
      @endif

      <div class="guestbook__container box-shadow">
        <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">
          <div class="row col-md-11">
            <div class="col-md-2" style="font-weight: bold;">Key</div>
            <div class="col-md-3" style="font-weight: bold;">Value</div>
            <div class="col-md-1" style="font-weight: bold;">Language</div>
            <div class="col-md-1" style="font-weight: bold;">Platform</div>
            <div class="col-md-2" style="font-weight: bold;">Created At</div>
            <div class="col-md-2" style="font-weight: bold;">Updated At</div>
            <div class="col-md-1" style="font-weight: bold;">Status</div>
          </div>

          <div class="col-md-1">
            <div class="col-md-12" style="font-weight: bold;">Action</div>
          </div>


        </div>
        @if(isset($localization) && count($localization)>0)
        @foreach($localization as $local)
        <div class="guestbook__customer-details-content rest__row">
          <div class="row col-md-11">
            <div class="col-md-2" style="margin: auto 0px; word-wrap: break-word;">{{ $local->key }}</div>
            <div class="col-md-3" style="margin: auto 0px; word-wrap: break-word;">{{ $local->value }}</div>
            <div class="col-md-1" style="margin: auto 0px; word-wrap: break-word;">{{ $local->language->language_name }}</div>
            <div class="col-md-1" style="margin: auto 0px; word-wrap: break-word;">{{ $local->platform}}</div>
            <div class="col-md-2" style="margin: auto 0px; word-wrap: break-word;">{{ $local->created_at }}</div>
            <div class="col-md-2" style="margin: auto 0px; word-wrap: break-word;">{{ $local->updated_at }}</div>
            <div class="col-md-1" style="margin: auto 0px; word-wrap: break-word;">{{ ($local->status==1)?'Active':($local->status==0?'Inactive':'') }}</div>
          </div>
          <div class="row col-md-1" style="margin: auto 0px;">
            <div class="col-md-12">
              <a style="width:100%;margin:0 auto;" href="{{ URL::to('/menu/localization/' . $local->id . '/edit') }}" class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
            </div>
            <div class="col-md-12">
                <form method="POST" action="/menu/localization/{{$local->id}}" accept-charset="UTF-8">
                    <input name="_method" type="hidden" value="DELETE">
                    @csrf           
                    <button class="btn btn-default" type="submit">Delete</button>
              </form>
              
              </div>
            </div>
          </div>
          @endforeach
          @else
          <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
            No Record Found
          </div>
          @endif
        </div>

        @if(isset($localization) && count($localization)>0)
        <div class="text-center" style="margin: 0px auto;">
          {{ $localization->appends($_GET)->links()}}
        </div>
        @endif
      </div>

    </div>
  </div>
  @endsection
