@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Modifier Category - Edit') }}</h1>

    </div>

  </div>
  <div>

    <div class="card form__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit"><?php // {{ route('modifier_categories.add') }}?>
         <?php // {{ Form::model($modifierCatg, array('route' => array('modifier_categories.update', $modifierCatg->id), 'method' => 'PUT', 'files' => true)) }} ?>

          <form method="POST" action="/menu/modifier_categories/update/{{ $modifierCatg->id }}" enctype="multipart/form-data">
            @csrf
            <input id="category_name" type="hidden" value="{{ $modifierCatg->id }}" />
            <input name="_method" type="hidden" value="PUT">
            <div class="form-group row form_field__container">
              <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
              <div class="col-md-6">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" >
                  <option value="">Select restaurant</option>
                  @foreach($restaurantData as $rest)
                  <option value="{{ $rest->id }}" {{ count(old())==0 ? ($modifierCatg->restaurant_id == $rest->id ? 'selected' : '') : ( (old('restaurant_id')==$rest->id) ? 'selected' :'' ) }}>{{ $rest->restaurant_name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Modifier Category Name *') }}</label>

              <div class="col-md-6">
                <div class="input__field">
                  <input id="category_name" type="text" class="{{ $errors->has('category_name') ? ' is-invalid' : '' }}"
                         name="category_name" value="{{ old('category_name') ? old('category_name') : (count(old())==0 ? $modifierCatg->category_name : NULL)}}" required>
                </div>
                @if ($errors->has('category_name'))
                  <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('category_name') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="is_required" class="col-md-4 col-form-label text-md-right">{{ __('Required *') }}</label>
              <div class="col-md-6" style="margin:auto 0px">
                  <input type="radio"  name="is_required" required="" value="1" {{ count(old())==0 ? ($modifierCatg->is_required ? 'checked':'') : ( (old('is_required')=='1') ? 'checked' :'' ) }}> Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio"  name="is_required" required="" value="0" {{ count(old())==0 ? ( !$modifierCatg->is_required ? 'checked':'') : ( (old('is_required')=='0') ? 'checked' :'' ) }}> No
                @if ($errors->has('is_required'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('is_required') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class=" row  mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">
                  {{ __('Update') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection