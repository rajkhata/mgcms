@extends('layouts.app')

@section('content')
<div class="main__container container__custom">
  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>{{ __('Create Modifier Category') }}</h1>

    </div>

  </div>
  <div>

    <div class="card form__container">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      <div class="card-body">
        <div class="form__user__edit"><?php // {{ route('modifier_categories.add') }}?>
          <form method="POST" action="/menu/modifier_categories/store" enctype="multipart/form-data">
            @csrf
            <div class="form-group row form_field__container">
              <label for="restaurant_id" class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
              <div class="col-md-6">
                <select name="restaurant_id" id="restaurant_id" class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}" >
                  <option value="">Select restaurant</option>
                  @foreach($restaurantData as $rest)
                  <option value="{{ $rest->id }}" {{(old('restaurant_id')==$rest->id) ? 'selected' :'' }}>{{ $rest->restaurant_name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('restaurant_id'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('restaurant_id') }}</strong>
                </span>
                @endif
              </div>
            </div>
<!--
            <div class="form-group row form_field__container">
              <label for="language_id" class="col-md-4 col-form-label text-md-right">{{ __('Default Language') }}</label>
              <div class="col-md-4">
                <select name="language_id" id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" >
                  <option value="">Select Language</option>
                  @foreach($languageData as $lang)
                  <option value="{{ $lang->id }}" {{ (old('language_id')==$lang->id) ? 'selected' :'' }}>{{ isset($lang->language_name)?ucfirst($lang->language_name):'' }}</option>
                  @endforeach
                </select>
                @if ($errors->has('language_id'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('language_id') }}</strong>
                </span>
                @endif
              </div>
            </div>-->

            <div class="form-group row form_field__container">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Modifier Category Name *') }}</label>

              <div class="col-md-6">
                <div class="input__field">
                  <input id="category_name" type="text" class="{{ $errors->has('category_name') ? ' is-invalid' : '' }}"
                         name="category_name" value="{{ old('category_name') }}" required>
                </div>
                @if ($errors->has('category_name'))
                  <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('category_name') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group row form_field__container">
              <label for="is_required" class="col-md-4 col-form-label text-md-right">{{ __('Required *') }}</label>
              <div class="col-md-6" style="margin:auto 0px">
                  <input type="radio"  name="is_required" required="" value="1" {{ (old('is_required')=='1') ? 'checked' :'' }}> Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio"  name="is_required" required="" value="0" {{ (old('is_required')=='0') ? 'checked' :'' }}> No
                @if ($errors->has('is_required'))
                <span class="invalid-feedback" style="display: block;" >
                  <strong>{{ $errors->first('is_required') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class=" row  mb-0">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btn__primary">
                  {{ __('Add') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">

//    $(document).ready(function() {
//        $.ajaxSetup({
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            }
//        });
//        $('#country_id').on('change', function () {
//
//            var ajaxurl = '/get_states';
//
//            $('#state_id').find('option').not(':first').remove();
//            $.ajax({
//                type: 'POST',
//                url: '<?php // echo URL::to('/') . "/get_states"; ?>',
//                data: '_token = <?php // echo csrf_token() ?>&country_id=' + this.value,
//                success: function (data) {
//                    if ($.type(data.dataObj) !== 'undefined') {
//                        $.each(data.dataObj, function (i, item) {
//                            $('#state_id').append('<option value="' + item.id + '">' + item.state + '</option>');
//                        });
//                    }
//                }
//            });
//        });
//
//        $('#state_id').on('change', function () {
//            var ajaxurl = '/get_cities_by_state';
//            $('#city_id').find('option').not(':first').remove();
//            $.ajax({
//                type: 'POST',
//                url: '<?php // echo URL::to('/') . "/get_cities_by_state"; ?>',
//                data: '_token = <?php // echo csrf_token() ?>&state_id=' + this.value,
//                success: function (data) {
//                    if ($.type(data.dataObj) !== 'undefined') {
//                        $.each(data.dataObj, function (i, item) {
//                            $('#city_id').append('<option value="' + item.id + '">' + item.city_name + '</option>');
//                        });
//                    }
//                }
//            });
//        });
//
//    });
</script>

@endsection