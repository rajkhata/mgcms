@csrf                      
<div class="form-group row form_field__container">
    <input type="text" id="searchiteminput"  placeholder="Search for Menu Items..">
</div>
<!--onkeyup="searchForMenuItems()"-->
<ul id="myUL">
    @foreach($data as $menuItem)
    <li>
        <a href="#"><span class="seachitemcheckbox"><input name="menu_item_id[]" type="checkbox" value="{{$menuItem->id}}"></span>{{ $menuItem->name }}</a>
    </li>
    @endforeach
</ul>
<br/><br/>
<input type="hidden" name="addon_group_id" value="{{$addonGroupId}}" />
