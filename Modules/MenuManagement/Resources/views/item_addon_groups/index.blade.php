<?php
mb_internal_encoding("UTF-8");
mb_http_output('UTF-8')
?>
@foreach($itemAddonGroups as $itemAddonGroup)

<div class="panel panel-default" id="addonGroupPanel{{ $itemAddonGroup->id }}">   
    <div class="panel-heading" role="tab" id="addonGroup{{ $itemAddonGroup->id }}">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAddonGroup{{ $itemAddonGroup->id }}" aria-expanded="true" aria-controls="collapseAddonGroup{{ $itemAddonGroup->id }}">
                {{ $itemAddonGroup->group_name }}
                <span class="delete_edit">
                    <i class="fa fa-trash-o" aria-hidden="true" onclick="deleteGroup({{ $itemAddonGroup->id }})"></i>
                    <i class="fa fa-pencil" aria-hidden="true" id="pencilAddonGroup{{ $itemAddonGroup->id }}"></i>
                </span>   
            </a>
        </h4>
    </div>

    <div id="collapseAddonGroup{{ $itemAddonGroup->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="addonGroup{{ $itemAddonGroup->id }}">
        <div class="panel-body">
            <form name="AddonGroupForm_{{ $itemAddonGroup->id }}" method="post" action="{{ route('item-addon-groups.store') }}/update/{{ $itemAddonGroup->id }}" onsubmit="updateAddonItemsGroups($(this)); return false;">
                @csrf
                <input name="_method" type="hidden" value="PUT">
                <input type="hidden" name="addon_group_id" value="{{ $itemAddonGroup->id }}" />
                <input type="hidden" name="language_id" value="{{ $languageId }}" />
                <div class="form-group row form_field__container">
                    <label class="col-md-3 col-form-label text-align-left">{{ __('Group Name') }}</label>
                    <div class="col-md-5 group_name_val">
                        <div class="">
                            <div class="input__field">
                                <input type="text"
                                       class="{{ $errors->has('group_name') ? ' is-invalid' : '' }}"
                                       name="group_name" value="{{ count(old()) ? old('group_name') : $itemAddonGroup->group_name }}" required placeholder="Group Name">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row form_field__container">
                    <label class="col-md-3 col-form-label text-align-left">{{ __('Prompt') }}</label>
                    <div class="col-md-5 group_name_val">
                        <div class="">
                            <div class="input__field">
                                <input type="text"
                                       class="{{ $errors->has('prompt') ? ' is-invalid' : '' }}"
                                       name="prompt" value="{{ count(old()) ? old('prompt') : $itemAddonGroup->prompt }}" required placeholder="Prompt">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row form_field__container">
                    <label class="col-md-3 col-form-label text-align-left">{{ __('Quantity Type') }}</label>
                    <div class="col-md-3 quantity_type_select">
                        <select name="quantity_type" 
                                class="quantity_type form-control{{ $errors->has('quantity_type') ? ' is-invalid' : '' }}"
                                required id="quantity_type{{ $itemAddonGroup->id }}">
                            <option value="">--Select--</option>
                            <option value="Value" <?php echo ($itemAddonGroup->quantity_type == 'Value' ? 'selected' : '') ?>>Value</option>
                            <option value="Range" <?php echo ($itemAddonGroup->quantity_type == 'Range' ? 'selected' : '') ?>>Range</option>
                        </select>
                        @section('footer_scripts')
                        <script type="text/javascript">
                        <?php echo '$(\'#quantity_type' . $itemAddonGroup->id . '\').trigger(\'change\');'; ?>
                        </script>
                        @stop
                        @if ($errors->has('quantity_type'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('quantity_type') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-md-2 quantity_type_val">
                        <div class="">
                            <div class="input__field">

                                <input id="quantity" type="text"
                                       class="{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
                                       name="quantity" value="{{ count(old()) ? old('quantity') : $itemAddonGroup->quantity }}" required placeholder="Quantity">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group row form_field__container">
                    <label for="priority" class="col-md-3 col-form-label text-align-left">{{ __('Is this required?') }}</label>
                    <div class="col-md-4 text-align-left" style="padding-top: 10px;">
                        <label class="radio-inline"><input type="radio" id="status1" name="is_required" value="1" <?php echo ($itemAddonGroup->is_required == '1' ? 'checked="checked"' : '') ?>  > Required</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <label class="radio-inline"><input type="radio" id="status0" name="is_required" value="0" <?php echo ($itemAddonGroup->is_required == '0' ? 'checked="checked"' : '') ?>> Optional</label>
                        @if ($errors->has('is_required'))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('is_required') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            
            <div class="col-md-12 addonbox">
                <div class="col-md-4" style="font-weight: bold;">Addon Name</div>
                <div class="col-md-4" style="font-weight: bold;">Price</div>
                <div class="col-md-2" style="font-weight: bold;">Action</div>
            </div>
            <!--<form name="AddonItemsForm_{{ $itemAddonGroup->id }}">-->
                @foreach($itemAddons as $itemAddon)
                @if ($itemAddonGroup->id == $itemAddon->addon_group_id)
                <div class="row col-md-12" id="addon_item_row_{{$itemAddon->id}}">
                    <div class="col-md-4 pt-5">
                        {{ $itemAddon->name }}
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="noneCol" name="price_{{$itemAddon->id}}" value="{{ $itemAddon->addon_price }}" style="width: 80%;" />
                    </div>
                    <div class="col-md-2 pt-5">
                        <span style="cursor: pointer;" onclick="deleteItemAddons({{ $itemAddon->id }}, 'addon_item_row_{{$itemAddon->id}}')">X</span>
                    </div>
                </div>
                @endif
                @endforeach
            
            <br>
            <button type="button" class="btn btn__primary mt-20" data-toggle="modal" data-target="#additem" onclick="reloadAddonItems({{$itemAddonGroup->id}})">Add Item</button>
            <button type="submit" class="btn btn__primary mt-20" >Update All</button>
        </form>
        </div>
    </div>
</div>
@endforeach