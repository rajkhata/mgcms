@extends('layouts.app')
@section('content')
    <div class="main__container container__custom">
        <div class="reservation__atto">
            <div class="reservation__title">
                <h1>{{ __('Item  Label - Edit') }}</h1>

            </div>

        </div>

        <div>

            <div class="card form__container">
                 @include('menumanagement::common.message')

                <div class="card-body">
                    <div class="form__user__edit">
                        {{ Form::model($data, array('route' => array('label.update', $data->id), 'method' => 'PUT')) }}

                        @csrf

                        <div class="form-group row form_field__container">
                            <label for="restaurant_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                            <div class="col-md-4">
                                <select name="restaurant_id" id="restaurant_id"
                                        class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                                        required>
                                    @foreach($restaurantData as $rest)
                                        <optgroup label="{{ $rest['restaurant_name'] }}">
                                            @foreach($rest['branches'] as $branch)
                                                <option value="{{ $branch['id'] }}" {{ ((old('restaurant_id')==$branch['id']) || (isset($data->restaurant_id) && $data->restaurant_id==$branch['id'])) ? 'selected' :'' }}>{{ $branch['restaurant_name'] }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                                
                                @if ($errors->has('restaurant_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-4">
                                <div class="input__field">
                                    <input id="label_name" type="text"
                                           class="{{ $errors->has('label_name') ? ' is-invalid' : '' }}"
                                           name="label_name" value="{{ isset($data->label_name)?$data->label_name:old('label_name') }}"
                                           required>
                                </div>
                                @if ($errors->has('label_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('label_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       

                        <br/><br/>
                        <div class=" row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

  
@endsection
