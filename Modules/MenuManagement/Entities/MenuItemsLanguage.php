<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Models\Language;
use Modules\MenuManagement\Entities\MenuItemModel;

class MenuItemsLanguage extends Model {

    protected $table = 'new_menu_items_language';
    protected $fillable = [
        'new_menu_items_id', 'item_other_info', 'gift_message', 'name', 'description', 'sub_description', 'language_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Language() {
        return $this->belongsTo('App\Models\Language', 'language_id', 'id');
    }

    public function MenuItem() {
        return $this->hasMany('Modules\MenuManagement\Entities\MenuItemModel', 'new_menu_items_id', 'id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
