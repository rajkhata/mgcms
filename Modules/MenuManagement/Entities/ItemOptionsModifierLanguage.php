<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemOptionsModifierLanguage extends Model
{
	use SoftDeletes;

    protected $fillable = ['modifier_option_id','modifier_option_name', 'language_id'];
    protected $table='modifier_item_options_language';

    protected $dates = ['deleted_at'];

    public function language() {
        return $this->belongsTo('App\Models\Language');
    }
    
    public function modifieroption()
    {
        return $this->belongsTo('Modules\MenuManagement\Entities\ItemOptionsModifier', 'modifier_option_id');
    }
   
}
