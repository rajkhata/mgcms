<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifierOptionGroup extends Model {

    use SoftDeletes;

    protected $fillable = ['restaurant_id', 'group_name', 'prompt', 'language_id', 'is_required', 'quantity_type', 'quantity', 'modifier_item_id'];
    protected $table = 'modifier_item_option_groups';
    protected $dates = ['deleted_at'];

    public function itemmodifier() {
        return $this->belongsTo('Modules\MenuManagement\Entities\ItemModifier', 'modifier_item_id');
    }

    public function modifierItemOptions() {
        return $this->hasMany('Modules\MenuManagement\Entities\ItemOptionsModifier', 'modifier_option_group_id');
    }

    public function itemModifierOptionGroupLanguage() {
        return $this->hasMany('Modules\MenuManagement\Entities\ItemModifierOptionGroupLanguage', 'modifier_option_group_id');
    }

}
