<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Models\Language;

class Localization extends Model {

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    protected $table = 'localization';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language_id', 'key', 'value', 'platform', 'status'
    ];

    public function language() {
        return $this->belongsTo(Language::class);
    }

}
