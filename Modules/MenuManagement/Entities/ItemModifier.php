<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifier extends Model {

    use SoftDeletes;

    //protected $fillable = ['modifier_group_id', 'modifier_category_id', 'modifier_name', 'price','size','is_selected', 'item_image','pos_id','calorie','sort_order'];
    protected $guarded = ['id'];

    protected $table = 'modifier_items';
    protected $dates = ['deleted_at'];

    public function modifiercategory() {
        return $this->belongsTo('Modules\MenuManagement\Entities\ModifierCategories', 'modifier_category_id');
    }

    public function modifierItemOptionGroup() {
        return $this->hasMany('Modules\MenuManagement\Entities\ItemModifierOptionGroup', 'modifier_item_id');
    }

    public function itemModifierLanguage() {
        return $this->hasMany('Modules\MenuManagement\Entities\ItemModifierLanguage', 'modifier_item_id');
    }

}
