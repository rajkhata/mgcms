<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MenuManagement\Entities\MenuItemsLanguage;

class Labels extends Model {

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'cms_labels';



}
