<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemLabels extends Model
{
	use SoftDeletes;

    protected $fillable = ['restaurant_id','label_name'];
    protected $table='item_labels';

    protected $dates = ['deleted_at'];

    
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
   
}
