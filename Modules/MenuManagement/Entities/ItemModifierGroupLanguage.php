<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifierGroupLanguage extends Model {

    use SoftDeletes;

    protected $fillable = ['modifier_group_id', 'language_id', 'group_name', 'prompt'];
    protected $table = 'modifier_groups_language';
    protected $dates = ['deleted_at'];

    public function modifierGroup() {
        return $this->belongsTo('Modules\MenuManagement\Entities\ItemModifierGroup');
    }

    public function language() {
        return $this->belongsTo('App\Models\Language');
    }

}
