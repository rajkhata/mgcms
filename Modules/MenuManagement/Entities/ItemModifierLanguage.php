<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifierLanguage extends Model {

    use SoftDeletes;

    protected $fillable = ['modifier_item_id', 'language_id', 'modifier_name'];
    protected $table = 'modifier_items_language';
    protected $dates = ['deleted_at'];

    public function itemModifier() {
        return $this->belongsTo('Modules\MenuManagement\Entities\ItemModifier');
    }

    public function language() {
        return $this->belongsTo('App\Models\Language');
    }

}
