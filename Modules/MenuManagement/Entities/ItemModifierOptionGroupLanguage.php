<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifierOptionGroupLanguage extends Model {

    use SoftDeletes;

    protected $fillable = ['modifier_option_group_id', 'language_id', 'group_name', 'prompt'];
    protected $table = 'modifier_item_option_groups_language';
    protected $dates = ['deleted_at'];

    public function language() {
        return $this->belongsTo('App\Models\Language');
    }

    public function itemModifierOptionGroup() {
        return $this->belongsTo('Modules\MenuManagement\Entities\ItemModifierOptionGroup', 'modifier_option_group_id');
    }

}
