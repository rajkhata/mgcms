<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;

class VisibilityHours extends Model
{
	protected $table = 'cms_restaurant_visibility_duration_week_time';
    protected $fillable = ['restaurant_id', 'slot_name', 'slot_code', 'is_dayoff', 'calendar_day', 'slot_detail'];
}