<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemAddons extends Model {

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name',
        'restaurant_id',
        'addon_group_id',
        'item_id',
        'addon_price',
        'is_default',
        'is_image_required'
    ];
    protected $table = 'item_addons';

    public function restaurant() {
        return $this->belongsTo('App\Models\Restaurant');
    }

    public function itemAddonGroup() {
        return $this->belongsTo('Modules\MenuManagement\Entities\ItemAddonGroups');
    }

    public function addongroup()
    {
        return $this->belongsTo('Modules\MenuManagement\Entities\ItemAddonGroups','addon_group_id');
    }

}
