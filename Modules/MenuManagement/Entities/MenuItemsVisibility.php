<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;

class MenuItemsVisibility extends Model {

    protected $table = 'menu_items_visibility';
    protected $fillable = ['menu_item_id', 'visibility_duration_week_time_id'];
    
    public function menuItem() {
        return $this->belongsTo('Modules\MenuManagement\Entities\MenuItemModel', 'menu_item_id', 'id');
    }

}
