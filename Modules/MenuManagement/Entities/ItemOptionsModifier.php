<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemOptionsModifier extends Model {

    use SoftDeletes;

    protected $fillable = ['modifier_option_group_id', 'modifier_option_name', 'price', 'item_image'];
    protected $table = 'modifier_item_options';
    protected $dates = ['deleted_at'];

    public function modifieroptiongroup() {
        return $this->belongsTo('Modules\MenuManagement\Entities\ItemModifierOptionGroup', 'modifier_option_group_idIndex');
    }

    public function itemOptionsModifierLanguage() {
        return $this->hasMany('Modules\MenuManagement\Entities\ItemOptionsModifierLanguage', 'modifier_option_id');
    }

}
