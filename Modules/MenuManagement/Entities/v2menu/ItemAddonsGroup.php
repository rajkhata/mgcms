<?php

namespace Modules\MenuManagement\Entities\v2menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemAddonsGroup extends Model
{
	use SoftDeletes;

    protected $table='ma_addons_group';
    protected $guarded = ['id'];
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
}
