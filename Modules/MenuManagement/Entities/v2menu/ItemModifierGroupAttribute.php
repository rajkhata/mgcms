<?php

namespace Modules\MenuManagement\Entities\v2menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MenuManagement\Entities\MenuItemsLanguage;

class ItemModifierGroupAttribute extends Model {

    //use SoftDeletes;


    protected $table = 'ma_modifier_group_attribute';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];


     

    protected $hidden = [
        'created_at', 'updated_at'
    ];
 
}
