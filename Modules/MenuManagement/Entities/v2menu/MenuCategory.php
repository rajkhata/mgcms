<?php

namespace Modules\MenuManagement\Entities\v2menu;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuCategoryLanguage;

class MenuCategory extends Model
{
    protected $table = 'ma_menu_categories';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
    
    public function parent()
    {
        return $this->belongsTo('App\Models\MenuCategory', 'parent');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public function children()
    {
        return $this->hasMany('App\Models\MenuCategory', 'parent');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

}
