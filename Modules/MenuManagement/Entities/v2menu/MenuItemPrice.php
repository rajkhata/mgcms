<?php

namespace Modules\MenuManagement\Entities\v2menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MenuManagement\Entities\MenuItemsLanguage;

class MenuItemPrice extends Model {

    //use SoftDeletes;


    protected $table = 'ma_menu_item_price';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant()
    {
        return $this->belongsTo('Modules\MenuManagement\Entities\v2menu\MenuItem','menu_item_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];
   
}
