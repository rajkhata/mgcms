<?php

namespace Modules\MenuManagement\Entities\v2menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MenuManagement\Entities\MenuItemsLanguage;

class ItemAddonsItem extends Model {

    //use SoftDeletes;


    protected $table = 'ma_addons_group_items';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];


    
    protected $hidden = [
        'created_at', 'updated_at'
    ];
   
}
