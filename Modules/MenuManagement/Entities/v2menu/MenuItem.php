<?php

namespace Modules\MenuManagement\Entities\v2menu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MenuManagement\Entities\MenuItemsLanguage;

class MenuItem extends Model {

    //use SoftDeletes;


    protected $table = 'ma_menu_item';	
     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant','restaurant_id','id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function MenuCategory() {
        return $this->belongsTo('App\Models\MenuCategory', 'menu_category_id', 'id');
    }

    
}
