<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItemLabels extends Model
{
	use SoftDeletes;

    protected $fillable = ['restaurant_id', 'item_id', 'labels', 'size','labels_with_priority'];
    protected $table    = 'cms_item_labels';
    protected $dates    = ['deleted_at'];
    
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
   
}
