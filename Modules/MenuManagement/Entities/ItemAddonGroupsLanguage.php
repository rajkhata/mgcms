<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Models\Language;
use Modules\MenuManagement\Entities\ItemAddonGroups;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemAddonGroupsLanguage extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    protected $table = 'item_addon_groups_language';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_addon_group_id', 'group_name', 'language_id', 'prompt'
    ];

    public function itemAddonGroups() {
        return $this->belongsTo(ItemAddonGroups::class);
    }

    public function language() {
        return $this->belongsTo(Language::class);
    }

}
