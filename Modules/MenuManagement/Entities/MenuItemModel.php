<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MenuManagement\Entities\MenuItemsLanguage;

class MenuItemModel extends Model {

    use SoftDeletes;


    protected $fillable = [ 
        'restaurant_id',
        'menu_category_id',
        'menu_sub_category_id',
        'name','size_price',
        'status',
        'description',
        'sub_description',
        'thumb_image_med',
        'image_caption',
        'size_price',
        'product_type',
        'gift_wrapping_fees',
        'gift_message',
        'display_order',
        'is_popular',
        'is_favourite',
        'manage_inventory',
        'max_allowed_quantity',
        'min_allowed_quantity',
        'inventory',
        'delivery',
        'takeout',
        'food_type',
        'pos_id',
        'sku',
        'calorie',
        'is_customizable'
        ];
    protected $dates = ['deleted_at'];
    protected $table = 'new_menu_items';



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function Restaurant() {
        return $this->belongsTo('App\Models\Restaurant', 'restaurant_id', 'id');
    }

    public function MenuCategory() {
        return $this->belongsTo('App\Models\MenuCategory', 'menu_category_id', 'id');
    }

    public function menuSubCategory() {
        return $this->belongsTo('App\Models\MenuSubCategory', 'menu_sub_category_id', 'id');
    }

    public function MenuMealTypePrice() {
        return $this->hasMany('App\Models\MenuMealTypePrice', 'menu_item_id', 'id');
    }

    public function menuItemsLanguage() {
        return $this->hasMany('Modules\MenuManagement\Entities\MenuItemsLanguage', 'new_menu_items_id', 'id');
    }

    public function modifierGroup()
    {
        return $this->hasMany('Modules\MenuManagement\Entities\ItemModifierGroup', 'menu_item_id', 'id');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];


}
