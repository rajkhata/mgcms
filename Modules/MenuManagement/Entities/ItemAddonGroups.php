<?php

namespace Modules\MenuManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Models\Restaurant;
use App\Models\Language;
use Modules\MenuManagement\Entities\MenuItemModel;
use Modules\MenuManagement\Entities\ItemAddonGroupsLanguage;
use Modules\MenuManagement\Entities\ItemAddons;

use Illuminate\Database\Eloquent\SoftDeletes;

class ItemAddonGroups extends Model {
    
//    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    protected $table = 'item_addon_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id', 'menu_item_id', 'group_name', 'language_id', 'prompt', 'is_required', 'quantity_type', 'quantity'
    ];

    public function restaurant() {
        return $this->belongsTo(Restaurant::class);
    }

    public function language() {
        return $this->belongsTo(Language::class);
    }
    
    public function menuItem() {
        return $this->belongsTo(MenuItemModel::class);
    }

    public function addonGroupItems() {
        return $this->hasMany('Modules\MenuManagement\Entities\ItemAddons', 'addon_group_id');
    }
    
    public function itemAddons() {
        return $this->hasMany(ItemAddons::class, 'item_addon_group_id', 'id');
    }
    
    public function itemAddonGroupsLanguage() {
        return $this->hasMany(ItemAddonGroupsLanguage::class, 'item_addon_group_id', 'id');
    }
}
