@extends('layouts.app')
@section('content')
    <div class="main__container container__custom new-design-labels">

        <div class="row margin-top-30">
            {{ Form::model($data, array('route' => array('pages.update', $data->id), 'method' => 'PUT', 'files' => true)) }}
            @csrf

            <div class="reservation__atto flex-direction-row">
                <div class="reservation__title margin-top-5">
                    <h1>Content Page - Edit</h1>
                </div>
                <div class="guestbook__sec2">
                    <div class="margin-left-10">
                        <a href="{{ URL::to('content/pages') }}"
                           class="btn btn__primary font-weight-600 width-120 box-shadow">All Pages</a>
                        {{--<a href="javascript:void(0)" class="btn btn__primary font-weight-600 export_guest width-120 box-shadow">Save</a>
                        <a href="#" class="btn btn__white width-120 margin-left-10 box-shadow">Discard</a>--}}
                    </div>
                </div>
            </div>

            @include('menumanagement::common.message')

            <div class="row margin-top-20 white-box box-shadow padding-top-20 padding-bottom-20">
                <div class="col-xs-12 padding-left-right-30">
                    <h4 class="font-weight-700 col-xs-6"><i class="fa fa-angle-right margin-right-10 font-weight-700"
                                                            aria-hidden="true"></i> Content</h4>
                    <button type="submit" role="button"
                            class="btn btn__primary font-weight-600 pull-right margin-right-15 width-120">Save
                    </button>
                </div>

                <div class="col-xs-12 padding-left-right-30 margin-bottom-10">
                    <div class="col-xs-12 col-sm-5 margin-top-20">
                        <div class="input__field">
                            <input id="group_name" type="text"
                                   class="{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                   name="title" value="{{ isset($data->title)?$data->title:old('title') }}" required>
                            <span for="group_name">Title</span>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                        <div class="input__field">
                            <input id="slug" type="text"
                                   class="{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                                   name="slug" value="{{ isset($data->slug)?$data->slug:old('slug') }}" required>
                            <span for="contentSlug">Slug</span>
                            @if ($errors->has('slug'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('slug') }}</strong></span>
                            @endif
                        </div>
                    </div>
            <div class="col-xs-12 margin-top-20">
                        <div class="input__field">
                            <input id="meta_title" type="text"
                                   class="{{ $errors->has('meta_title') ? ' is-invalid' : '' }}"
                                   name="meta_title"
                                   value="{{ isset($data->meta_title)?$data->meta_title:old('meta_title') }}"
                                   required>
                            <span for="metaTitle">Meta Title</span>
                            @if ($errors->has('meta_title'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('meta_title') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 margin-top-20">
                        <div class="input__field">
                            <input id="meta_keyword" type="text"
                                   class="{{ $errors->has('meta_keyword') ? ' is-invalid' : '' }}"
                                   name="meta_keyword"
                                   value="{{ isset($data->meta_keyword)?$data->meta_keyword:old('meta_keyword') }}"
                                   required>
                            <span for="metaKeyword">Meta Keyword</span>
                            @if ($errors->has('meta_keyword'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('meta_keyword') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 margin-top-10">
                        <div class="text-color-grey font-size-12">Meta Description</div>
                        <div class="row textarea__input">
                            <textarea maxlength="500" rows="1" name="meta_description" id="meta_description"
                                      class="{{ $errors->has('meta_description') ? ' is-invalid' : '' }}"
                            required >{{ isset($data->meta_description)?$data->meta_description:old('meta_description') }}</textarea>
                        </div>
                        @if ($errors->has('meta_description'))
                            <span class="invalid-feedback"><strong>{{ $errors->first('meta_description') }}</strong></span>
                        @endif
                    </div>

                    <div class="col-xs-12 margin-top-10 ">
                        {{--  <div class="text-color-grey">Description</div>--}}
                        <div class="row textarea__input">
                            <textarea maxlength="500" rows="1" name="description" id="description"
                                      class="editor {{ $errors->has('description') ? ' is-invalid' : '' }}"
                            >{{ isset($data->description)?$data->description:old('description') }}</textarea>
                        </div>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
                        @endif


                    </div>
                    <?php if (!in_array($data->slug, $filterd_slugs)){ ?>
                    <div class="col-xs-12 col-sm-5 margin-top-20">
                        <div class="pull-left font-weight-700">Status</div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="status_active" name="status" checked
                                   value="1" {{ (isset($data->status) && $data->status==1) ? 'checked' :'' }} >
                            <label for="status_active">Active</label>
                        </div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="status_inActive" name="status"
                                   value="0" {{ (isset($data->status) && $data->status==0) ? 'checked' :'' }}>
                            <label for="status_inActive">In Active</label>
                        </div>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback"
                                  style="display:block;"><strong>{{ $errors->first('status') }}</strong></span>
                        @endif
                    </div>
                    <?php } ?>
                    <div class="col-xs-12 col-sm-5 margin-top-20">
                        <div class="pull-left font-weight-700">Include in navigation</div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="include_in_navigation_yes" name="include_in_navigation" checked
                                   value="1" {{ (isset($data->include_in_navigation) && $data->include_in_navigation==1) ? 'checked' :'' }} >
                            <label for="include_in_navigation_yes">Yes</label>
                        </div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="include_in_navigation_no" name="include_in_navigation"
                                   value="0" {{ (isset($data->include_in_navigation) && $data->include_in_navigation==0) ? 'checked' :'' }}>
                            <label for="include_in_navigation_no">No</label>
                        </div>
                        @if ($errors->has('include_in_navigation_yes'))
                            <span class="invalid-feedback"
                                  style="display:block;"><strong>{{ $errors->first('include_in_navigation_yes') }}</strong></span>
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-20">
                        <div class="input__fieldq">
                            <span for="user_group_promotion " class="font-weight-700">User Group Promotion</span>

                            <select id="user_group_promotion" name="user_group_promotion" class="form-control">
                                <option value="">--Select--</option>
                                <?php
                                $group_options = '';
                                foreach ($user_groups as $group){
                                $group_options .= '<option value="' . $group->id . '" >' . $group->user_group . ' </option>';
                                ?>
                                <option value="<?php echo $group->id;?>" <?php if ($data->user_group_promotion == $group->id) {
                                    echo 'selected';
                                }?>><?php echo $group->user_group;?> </option>
                                <?php } ?>
                            </select>

                            @if ($errors->has('user_group_promotion'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('user_group_promotion') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    
                        <div class="col-xs-12 col-sm-5 margin-top-20">
                            <div class="input__fieldq">
                <div for="group_name" class="pull-left font-weight-700 margin-bottom-20 margin-top-20 ">Sort Order</div>
                                <input id="sort_order" type="number"
                                       class="{{ $errors->has('sort_order') ? ' is-invalid' : '' }} form-control"
                                       name="sort_order" value="{{ isset($data->sort_order)?$data->sort_order:old('sort_order') }}" style=" width: 60%; margin-left: 10px;  " >
                                
                                @if ($errors->has('sort_order'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('sort_order') }}</strong></span>
                                @endif
                            </div>
                        </div>

                
            </div>

            <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-20">
                <div class="col-xs-12 padding-left-right-30">
                    <h4 class="font-weight-700 col-xs-6"><i class="fa fa-angle-right margin-right-10 font-weight-700"
                                                            aria-hidden="true"></i> Web Page</h4>
                    <a class="btn btn__holo font-weight-600 pull-right margin-right-15 width-120" target="_blank"
                       href="{{$restaurant_data->source_url}}/{{$data->slug}}?inline_editable=1&api_token={{$api_token}}&_token={{ csrf_token() }}">Edit</a>
                </div>

                <div class="col-xs-12 padding-left-right-30 margin-top-30 margin-bottom-10">
                    <div class="col-xs-12">
                        <iframe src="{{$restaurant_data->source_url}}/{{$data->slug}}" class="website-preview"
                                frameborder="0" height="800"></iframe>
                    </div>
                </div>
            </div>

            <div class="row margin-top-20 white-box box-shadow padding-top-20 padding-bottom-20 ">
                <div class="col-xs-12 padding-left-right-30">
                    <h4 class="font-weight-700 col-xs-6"><i class="fa fa-angle-right margin-right-10 font-weight-700"
                                                            aria-hidden="true"></i> Banners<a href="#"
                                                                                              class="position_plus padding-left-right-30"><i
                                    class="fa fa-plus"></i></a></h4>
                </div>

                <div class="col-xs-12 padding-left-right-30 margin-bottom-10 margin-top-10">
                    <fieldset>

                        <div id="position_container">
                            <?php

                            if(isset($data->promo_banners) && !empty($data->promo_banners)){

                            $promo_banners = json_decode($data->promo_banners, 1);
                            // print_r($promo_banners);
                            if(is_array($promo_banners) && count($promo_banners)){
                            foreach ($promo_banners as $key=>$pos){
                            ?>


                            <div class="form-group row form_field__container position_container content__row">


                                <div class="col-md-11 content__imageRow">
                                    <div class="col-md-12">
                                        <input type="hidden" class="input__field" name="ID[{{$key}}]"
                                               value="<?php echo @$pos['Id'];?>">
                                        <div class="col-md-4">
                                            <div class="">
                                                <select id="user_group_id" name="UserGroup[{{$key}}]"
                                                        class="form-control">
                                                    <option value="">--Select User Group--</option>
                                                    <?php
                                                    foreach ($user_groups as $group){
                                                    ?>
                                                    <option value="<?php echo $group->id;?>" <?php if (@$pos[
                                                    'UserGroup']==$group->id){echo
                                                    'selected';}?>><?php echo $group->user_group;?> </option>
                                                    <?php } ?>


                                                </select>
                                            </div>
                                        </div>
                    <div class="col-md-4">
                                            <div class="">
                                                <input type="text" class="form-control" name="AltText[{{$key}}]"
                                                       value="<?php echo $pos['AltText'];?>" placeholder="Alt Text">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="">
                                                <select class="form-control" name="Position[{{$key}}]">
                                                    <option value="Header" <?php if (isset($pos['Position']) && $pos['Position'] == 'Header') {
                                                        echo 'selected';
                                                    }?>>Header
                                                    </option>
                                                    <option value="Footer" <?php if (isset($pos['Position']) && $pos['Position'] == 'Footer') {
                                                        echo 'selected';
                                                    }?>>Footer
                                                    </option>
                                                    <option value="Deal" <?php if (isset($pos['Position']) && $pos['Position'] == 'Deal') {
                                                        echo 'selected';
                                                    }?>>Deal
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-4 typeparent">
                                            <div class="">
                                                <select class="form-control fieldtype_banner" name="Type[{{$key}}]">
                                                    <option value="Text" <?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                        echo 'selected';
                                                    }?>>Text
                                                    </option>
                                                    <option value="Image" <?php if (isset($pos['Type']) && $pos['Type'] == 'Image') {
                                                        echo 'selected';
                                                    }?>>Image
                                                    </option>
                                                    <option value="Video" <?php if (isset($pos['Type']) && $pos['Type'] == 'Video') {
                                                        echo 'selected';
                                                    }?>>Video
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="align-centerflex">
                                                Web <input type="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                    echo 'text';
                                                } else {
                                                    echo 'file';
                                                }?>" class="form-control" name="Value[{{$key}}]"
                                                       value="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                           echo $pos['Value'];
                                                       }?>" placeholder="Value">
                                                @if(isset($pos['Type']) && $pos['Type']=='Image' && isset($pos['Value'])  && $pos['Value']!='')
                                                    <div class="aligncenter-order1">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value']}}"><span
                                                                    class="glyphicon glyphicon-picture"></span></a>&emsp;
                                                    </div>
                                                @endif
                                                @if(isset($pos['Type']) && $pos['Type']=='Video' && isset($pos['Value'])  && $pos['Value']!='')
                                                    <div class="">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value']}}"></a>&emsp;
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                       <div class="align-centerflex">
                                                Mobile <input type="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                    echo 'text';
                                                } else {
                                                    echo 'file';
                                                }?>" class="form-control" name="Value_Mobile[{{$key}}]"
                                                       value="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                           echo $pos['Value_Mobile'];
                                                       }?>" placeholder="Value_Mobile">
                                                @if(isset($pos['Type']) && $pos['Type']=='Image' && isset($pos['Value_Mobile']) && $pos['Value_Mobile']!='')
                                                    <div class="aligncenter-order1">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value_Mobile']}}"><span
                                                                    class="glyphicon glyphicon-picture"></span></a>&emsp;
                                                    </div>
                                                @endif
                                                @if((isset($pos['Type']) && $pos['Type']=='Video') && isset($pos['Value_Mobile']) && $pos['Value_Mobile']!='')
                                                    <div class="">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value_Mobile']}}"></a>&emsp;
                                                    </div>
                                                @endif
                                            </div>  
                                            <!--div class="">
                                                <!--select class="form-control" name="Display[{{$key}}]">
                                                    <option value="Web" <?php if (isset($pos['Display']) && $pos['Display'] == 'Web') {
                                                        echo 'selected';
                                                    }?>>Web
                                                    </option>
                                                    <option value="Mobile" <?php if (isset($pos['Display']) && $pos['Display'] == 'Mobile') {
                                                        echo 'selected';
                                                    }?>>Mobile
                                                    </option>
                                                    <option value="App" <?php if (isset($pos['Display']) && $pos['Display'] == 'App') {
                                                        echo 'selected';
                                                    }?>>App
                                                    </option>
                                                </select>
                                            </div-->
                                        </div>
                                    </div>                                    
                                    
                                    <div class="col-md-12">                                        
                                         <div class="col-md-4 mobileapp">
                       <div class="align-centerflex">
                                                App <input type="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                    echo 'text';
                                                } else {
                                                    echo 'file';
                                                }?>" class="form-control" name="Value_App[{{$key}}]"
                                                       value="<?php if (isset($pos['Type']) && $pos['Type'] == 'Text') {
                                                           echo $pos['Value_App'];
                                                       }?>" placeholder="Value_App">
                                                @if(isset($pos['Type']) && $pos['Type']=='Image' && isset($pos['Value_App']) && $pos['Value_App']!='')
                                                    <div class="aligncenter-order1">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value_App']}}"><span
                                                                    class="glyphicon glyphicon-picture"></span></a>&emsp;
                                                    </div>
                                                @endif
                                                @if((isset($pos['Type']) && $pos['Type']=='Video') && isset($pos['Value_App']) && $pos['Value_App']!='')
                                                    <div class="">
                                                        <a data-fancybox="gallery"
                                                           href="{{url('/').'/'.$pos['Value_App']}}"></a>&emsp;
                                                    </div>
                                                @endif
                                            </div>  
                                            
                                        </div>
                                        
                                        
                                        <div class="col-md-4">
                                            <div class="">
                                                <input type="text" class="form-control" name="Link[{{$key}}]"
                                                       value="<?php echo $pos['Link'];?>" placeholder="Link">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="">
                                                <input type="number" class="form-control" name="SortOrder[{{$key}}]"
                                                       value="<?php echo $pos['SortOrder'];?>" placeholder="Sort Order">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="col-md-12">
                                    <div class="col-md-4">
                                            <div class="">
                                                <select class="form-control" name="Status[{{$key}}]">
                                                    <option value="1" <?php if (isset($pos['Status']) && $pos['Status'] == '1') {
                                                        echo 'selected';
                                                    }?>>Active
                                                    </option>
                                                    <option value="0" <?php if (isset($pos['Status']) && $pos['Status'] == '0') {
                                                        echo 'selected';
                                                    }?>>In Active
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>

                                <div class="col-md-1">&nbsp;&nbsp;
                                    <a href="#" class="position_minus">
                                        Delete
                                    </a>
                                </div>

                            </div>


                            <?php
                            }}
                            }
                            ?>

                        </div>
                    </fieldset>
                </div>
            </div>


        </div>
    <!--
        <div>

            <div class="card form__container">


                <div class="card-body">
                    <div class="form__user__edit">
                    {{ Form::model($data, array('route' => array('pages.update', $data->id), 'method' => 'PUT')) }}

    @csrf
            --><!--
                        <div class="form-group row form_field__container">
                            <label for="restaurant_id"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Restaurant') }}</label>
                            <div class="col-md-4">
                                <select name="restaurant_id" id="restaurant_id"
                                        class="form-control{{ $errors->has('restaurant_id') ? ' is-invalid' : '' }}"
                                        required>
                                    <option value="">Select Restaurant</option>
                                    <?php //echo $restaurantData;?>

                                </select>

                                @if ($errors->has('restaurant_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('restaurant_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        --><!--

                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>
                            <div class="col-md-8">
                                <div class="input__field">
                                    <input id="group_name" type="text"
                                           class="{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                           name="title" value="{{ isset($data->title)?$data->title:old('title') }}" required>
                                </div>
                                @if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Slug') }}</label>
                            <div class="col-md-8">
                                <div class="input__field">
                                    <input id="slug" type="text"
                                           class="{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                                           name="slug" value="{{ isset($data->slug)?$data->slug:old('slug') }}" required>
                                </div>
                                @if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                            <div class="col-md-8">
                                <div class="input__field">

                                         <textarea maxlength="500" rows="4" name="description" id="description"
                                                   class="hidden form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                         >{{ isset($data->description)?$data->description:old('description') }}</textarea>

                                    <iframe src="{{$restaurant_data->source_url}}/{{$data->slug}}" frameborder="0" style="overflow:hidden;height:400px;width:100%" height="400px" width="100%"></iframe>

                                    <a  class="openbuilder_link" target="_blank" href="{{$restaurant_data->source_url}}/{{$data->slug}}?inline_editable=1&api_token={{$api_token}}&_token={{ csrf_token() }}">Edit in Builder</a>
                                </div>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Meta Keyword') }}</label>
                            <div class="col-md-8">
                                <div class="input__field">
                                    <input id="meta_keyword" type="text"
                                           class="{{ $errors->has('meta_keyword') ? ' is-invalid' : '' }}"
                                           name="meta_keyword" value="{{ isset($data->meta_keyword)?$data->meta_keyword:old('meta_keyword') }}" required>
                                </div>
                                @if ($errors->has('meta_keyword'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('meta_keyword') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row form_field__container">
                            <label for="priority"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Meta Description') }}</label>
                            <div class="col-md-8">
                                <div class="input__field">
                                         <textarea maxlength="500" rows="4" name="meta_description" id="meta_description"
                                                   class="form-control{{ $errors->has('meta_description') ? ' is-invalid' : '' }}"
                                         >{{ isset($data->meta_description)?$data->meta_description:old('meta_description') }}</textarea>
                                </div>
                                @if ($errors->has('meta_description'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('meta_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                             <?php if (!in_array($data->slug, $filterd_slugs)){ ?>
            <div class="form-group row form_field__container">
                <label for="priority" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                            <div class="col-md-8" style="padding-top: 10px;">
                                <input type="radio" id="status" name="status" checked value="1" {{ (isset($data->status) && $data->status==1) ? 'checked' :'' }} > Active
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" id="status" name="status" value="0" {{ (isset($data->status) && $data->status==0) ? 'checked' :'' }}> InActive
                                @if ($errors->has('status'))
        <span class="invalid-feedback" style="display:block;">
    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
            </div>
        </div>
<?php } ?>


            <br/><br/>
            <div class=" row mb-0">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn__primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    -->
    </div>
    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on('click', '.position_plus', function (e) {
                var lenofbox = $('.position_container').length;
                e.preventDefault();
                var groups = '<?php echo $group_options;?>';

                // $("#position_container").append('<div class="form-group row form_field__container position_container"> <div class="col-md-2"> <div class=""><input type="text" class="input__field" name="position_type['+lenofbox+']" placeholder="Type"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position['+lenofbox+']" placeholder="position"></div> </div> <div class="col-md-2"> <div class=""><input type="text" class="" name="position_price['+lenofbox+']" placeholder="price"></div> </div><div class="col-md-2"> <div class=""><input type="checkbox" class="" name="is_selected_position['+lenofbox+']" value="1">Default Position</div> </div> <div class="col-md-2"> <div class=""><input type="file" class="" name="position_image['+lenofbox+']"></div> </div> <div class="col-md-2"> &nbsp;&nbsp; <a href="#" class="position_minus"><i class="fa fa-minus"></i></a> </div> </div>');
                $("#position_container").append('<div class="form-group row form_field__container position_container content__row"><div class="col-md-11 content__imageRow"><div class="col-md-12"><div class="col-md-4"><input type="hidden" class="input__field" name="ID[' + lenofbox + ']" value=""> <div class=""><select class="form-control" name="UserGroup[' + lenofbox + ']"> <option value="">--Select User Group--</option> ' + groups + '</select></div> </div><div class="col-md-4"> <div class=""><input type="text" class="form-control" name="AltText[' + lenofbox + ']" placeholder="Alt Text"> </div> </div><div class="col-md-4"> <div class=""><select class="form-control" name="Position[' + lenofbox + ']"> <option value="Header">Header</option> <option value="Footer">Footer</option> <option value="Deal">Deal</option></select></div> </div></div><div class="col-md-12"><div class="col-md-4 typeparent"> <div class=""><select class="form-control fieldtype_banner" name="Type[' + lenofbox + ']"> <option value="Text">Text</option> <option value="Image">Image</option> <option value="Video">Video</option> </select></div> </div><div class="col-md-4"> <div class="">Web<input type="text" class="form-control" name="Value[' + lenofbox + ']" placeholder="Value"></div> </div><div class="col-md-4"> <div class="">Mobile<input type="text" class="form-control" name="Value_Mobile[' + lenofbox + ']" placeholder="Value"></div> </div></div><div class="col-md-12"><div class="col-md-4 mobileapp"> <div class="">App<input type="text" class="form-control" name="Value_App[' + lenofbox + ']" placeholder="Value"></div> </div><div class="col-md-4"> <div class=""><input type="text" class="form-control" name="Link[' + lenofbox + ']" placeholder="Link"></div> </div><div class="col-md-4"> <div class=""><input type="number" class="form-control" name="SortOrder[' + lenofbox + ']" placeholder="Sort Order"></div> </div><div class="col-md-4"> <div class=""><select class="form-control" name="Status[' + lenofbox + ']"> <option value="1">Active</option> <option value="0">In Active</option> </select></div> </div></div></div><div class="col-md-1"> &nbsp;&nbsp; <a href="#" class="position_minus">Delete</a></div></div>');

            });

            $(document).on('click', '.position_minus', function (e) {

                e.preventDefault();
                $(this).parents('.position_container').empty().remove();
            });


            $(document).on('change', '.fieldtype_banner', function (e) {
                e.preventDefault();
                var type = $(this).val();
                if (type == 'Text') {
            var nxt =  $(this).parents('.typeparent').next();   
                    $(this).parents('.typeparent').next().find('input').attr('type', 'text');
            $(nxt).next().find('input').attr('type', 'text');   
                    $('.mobileapp').find('input').attr('type', 'text');
                } else {
            var nxt =  $(this).parents('.typeparent').next();   
                    $(this).parents('.typeparent').next().find('input').attr('type', 'file');
            $(nxt).next().find('input').attr('type', 'file');   
                    $('.mobileapp').find('input').attr('type', 'file');

                }

            });


        });
    </script>

    <script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        //code added by jawed to facilitate FCK editor
        $(document).ready(function () {
            tinymce.init({
                selector: 'textarea.editor',
               // menubar: false,
                height: 320,
                theme: 'modern',
                plugins: 'code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
                toolbar1: 'code| formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
            });
        });
    </script>
    <style>
        .form__container {
            max-width: 90% !important;
        }

        .form__user__edit {
            width: 100% !important;

        }

        .openbuilder_link {
            font-size: 20px;
            text-decoration: underline !important;
            float: right;
            font-weight: bold;
        }
    </style>
@endsection

