@extends('layouts.app')

@section('content')
<div class="main__container container__custom">

  @include('menumanagement::common.message')

  <div class="reservation__atto">
    <div class="reservation__title">
      <h1>Content<span style="margin:0px 5px;" class="hidden">({{ $data->currentPage() .'/' . $data->lastPage() }})</span>&emsp;</h1>
    </div>
    <div class="guestbook__sec2">
      <div class="margin-left-10">
        <a href="{{ URL::to('content/pages/create') }}" class="btn btn__primary width-90">Add</a>
      </div>
    </div>
  </div>
  <!--
  <div class="addition__functionality">
    {!! Form::open(['method'=>'get']) !!}
    <div class="row functionality__wrapper">
      <div class="row form-group col-md-2">
        <select name="restaurant_id" id="restaurant_id" class="form-control">
          <option value="">Select Restaurant</option>


          <?php //echo $restaurantData;?>
        </select>
      </div>
      <div class="row form-group col-md-5">
        <button class="btn btn__primary" style="margin-left: 5px;" type="submit" >Search</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
  -->

  <!--Bookings-->
  <div class="booking">

    <div class="archive_reservation">

      <div class="booking_container tab-content clearfix box-shadow margin-top-20">
        <div class="booking_content tab-pane active no-margin">
          <div class="row row__reservation-head row__reservation-archive header-color font-weight-700">
            <div class="col-xs-2 no-padding-left">Title</div>
            <div class="col-xs-3">Restaurant</div>
            <div class="col-xs-3">Slug</div>
            <div class="col-xs-1">Status</div>
          </div>
          <div>



            @if(isset($data) && count($data)>0)
              @foreach($data as $record)

            <div class="row  row__customer row__new-customer row__customer-archive no-padding">
              <div class="row__reservation">


                <div class="col-xs-2 no-padding-left font-weight-700">{{ $record->title}}</div>
                <div class="col-xs-3 font-weight-700">{{ @$record->restaurant->restaurant_name }}</div>
                <div class="col-xs-3 font-weight-700">{{ @$record->slug }}</div>
                <div class="col-xs-1 font-weight-700"><spam class="text-color-{{ ($record->status==1?'green':'red')}}">{{ ($record->status==1?'Active':'Inactive')}}</spam></div>
                <div class="col-xs-3 text-right no-padding-right">
                 <?php //if (!in_array($record->slug, $filterd_slugs)){ ?>
                  <div class="pull-right margin-left-15">
                    {!! Form::open(['method' => 'DELETE', 'route' => ['pages.destroy', $record->id]]) !!}
                    @csrf
                    <button class="btn btn__holo width-90 font-weight-600" type="submit">Delete</button>
                    {!! Form::close() !!}
                  </div>
                <?php //} ?>

                  <div class="pull-right">
                    
                    <a href="{{ URL::to('content/pages/' . $record->id . '/edit') }}" class="btn btn__primary width-90">Edit</a>
                   
                  </div>


                </div>

              </div>
            </div>

              @endforeach
            @else
              <div class="text-center col-xs-12 col-sm-12 margin-top-bottom-40">
                <strong>No Record Found</strong>
              </div>
            @endif

          </div>
        </div>
      </div>
    </div>
  </div>

  <div>

    <div class="active__order__container">

      {{--@include('menumanagement::common.message')--}}

      {{--<div class="guestbook__container box-shadow">
        <div class="guestbook__table-header hidden-xs hidden-sm hidden-tablet row plr0">

          <div class="col-md-10">
            <div class="col-md-3" style="font-weight: bold;">Title</div>
            <div class="col-md-3" style="font-weight: bold;">Restaurant</div>
            <div class="col-md-3" style="font-weight: bold;">slug</div>

          </div>

          <div class="col-md-2">
            <div class="col-md-12" style="font-weight: bold;">Action</div>
          </div>

        </div>
        @if(isset($data) && count($data)>0)
          @foreach($data as $record)
          <div class="guestbook__customer-details-content rest__row">
            <div class="col-md-10 order__row">
              <div class="col-md-3">{{ $record->title}}</div>
              <div class="col-md-3">{{ @$record->restaurant->restaurant_name }}</div>
              <div class="col-md-3">
                <span class="label label-{{ ($record->status==1?'success':'danger')}}">{{ ($record->status==1?'Active':'Inactive')}}</span>
                </div>
            </div>
            <div class="row col-md-2">
              <div class="col-md-12">
                <a style="width:100%;margin:10px 0;" href="{{ URL::to('content/pages/' . $record->id . '/edit') }}" class="btn btn__primary"><span class="fa fa-pencil"></span> Edit</a>
              </div>
              <div class="col-md-12">
                {!! Form::open(['method' => 'DELETE', 'route' => ['pages.destroy', $record->id]]) !!}
                @csrf
                <button style="width:100%;margin:10px 0;" class="btn btn__cancel" type="submit"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
          @endforeach
        @else
        <div class="row" style="text-align: center;justify-content: center;border:1px solid rgba(0, 0, 0, 0.03);">
          No Record Found
        </div>
        @endif
      </div>--}}
      {{--@if(isset($data) && count($data)>0)
      <div class="text-center" style="margin: 0px auto;text-align: center;">
        {{ $data->appends($_GET)->links()}}
      </div>
      @endif--}}
    </div>

  </div>
</div>
@endsection
