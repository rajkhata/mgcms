@extends('layouts.app')
@section('content')
    <div class="main__container container__custom new-design-labels">
        <div class="row">

            <form method="POST" action="{{ route('pages.store') }}" enctype="multipart/form-data">
                @csrf


                <?php
                $default_content='<div class="is-section is-section-100 is-shadow-1 is-bg-grey"><div class="is-boxes"><div class="is-box is-box-7 is-bg-light is-dark-text type-sixcaps-robotomono"><div class="is-boxes"><div class="is-box-centered"><div class="is-container is-builder container is-content-980 is-content-right edge-x-0"><div class="row clearfix"><div class="column full" contenteditable="true"><h2>Beauty in Simplicity</h2></div></div><div class="row clearfix"><div class="column full" contenteditable="true"><div class="spacer height-40" contenteditable="false"></div></div></div><div class="row clearfix"><div class="column half" contenteditable="true"><p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem Ipsum is dummy text of the typesetting industry. Consectetur sit amet vulputate vel, dapibus sit amet lectus. Vivamus leo ante.</p></div><div class="column half" contenteditable="true"><p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem Ipsum is dummy text of the typesetting industry. Consectetur sit amet vulputate vel, dapibus sit amet lectus. Vivamus leo ante.</p></div></div></div></div></div></div><div class="is-box is-box-5 is-bg-light is-dark-text"><div class="is-boxes"><div class="is-overlay"><div class="is-overlay-content"></div></div><div class="is-box-centered"><div class="is-container is-builder container is-content-640 is-content-right edge-x-1"><div class="row clearfix"><div class="column full" contenteditable="true"> <img src="builder/assets/designs/images/sarah-dorweiler-128577-unsplash-rFDQi1.jpg" style="border-radius: 500px;"></div></div></div></div></div></div></div> ></div>';

                ?>

            <div class="reservation__atto flex-direction-row">
                <div class="reservation__title margin-top-5">
                    <h1>Content Page</h1>
                </div>
                {{--<div class="guestbook__sec2">
                    <div class="margin-left-10">
                        <a href="javascript:void(0)" class="btn btn__primary font-weight-600 width-120 box-shadow">Save</a>
                        <a href="#" class="btn btn__white width-120 margin-left-10 box-shadow">Discard</a>
                    </div>
                </div>--}}
            </div>

                @include('sitebuilder::common.message')

            <div class="row white-box box-shadow margin-top-20 padding-top-20 padding-bottom-20">
                <div class="col-xs-12 padding-left-right-30">
                    <h4 class="font-weight-700 col-xs-6 margin-top-10"><i class="fa fa-angle-right margin-right-10 font-weight-700" aria-hidden="true"></i> Content</h4>
                    {{--<a href="javascript:void(0)" class="btn btn__primary font-weight-600 pull-right margin-right-15 width-120">Save</a>--}}
                    <button type="submit" role="button" class="btn btn__primary width-120 pull-right">Save</button>
                </div>

                <div class="col-xs-12 padding-left-right-30 margin-bottom-10">
                    <div class="col-xs-12 col-sm-5 margin-top-20">
                        <div class="input__field">
                            <input id="group_name" type="text"
                                   class="{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                   name="title" value="{{ old('title') }}" required>
                            <span for="group_name">Title</span>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-20 pull-right">
                        <div class="input__field">
                            <input id="slug" type="text"
                                   class="{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                                   name="slug" value="{{ old('slug') }}" required>
                            <span for="slug">Slug</span>
                            @if ($errors->has('slug'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('slug') }}</strong></span>
                            @endif
                        </div>
                    </div>
		    <div class="col-xs-12 margin-top-20">
                        <div class="input__field">
                            <input id="meta_title" type="text"
                                   class="{{ $errors->has('meta_title') ? ' is-invalid' : '' }}"
                                   name="meta_title" value="{{ old('meta_title') }}" >
                            <span for="metaTitle">Meta Title</span>
                            @if ($errors->has('meta_title'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('meta_title') }}</strong></span>
                            @endif

                        </div>
                    </div>
                    <div class="col-xs-12 margin-top-20">
                        <div class="input__field">
                            <input id="meta_keyword" type="text"
                                   class="{{ $errors->has('meta_keyword') ? ' is-invalid' : '' }}"
                                   name="meta_keyword" value="{{ old('meta_keyword') }}" >
                            <span for="meta_keyword">Meta Keyword</span>
                            @if ($errors->has('meta_keyword'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('meta_keyword') }}</strong></span>
                            @endif

                        </div>
                    </div>
                    <div class="col-xs-12 margin-top-20">
                        <div class="fullWidth input__field textarea__input">
                            <textarea maxlength="500" rows="1" name="meta_description" id="meta_description"
                                      class=" {{ $errors->has('meta_description') ? ' is-invalid' : '' }}"
                            >{{ old('meta_description') }}</textarea>
                            <span for="meta_description">Meta Description</span>

                            @if ($errors->has('meta_description'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('meta_description') }}</strong></span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 margin-top-20">
                        <div class="text-color-grey font-size-12">Description</div>
                        <div class="row textarea__input margin-top-10">
                            <textarea maxlength="500" rows="1" name="description" id="description"
                                      class="editor {{ $errors->has('description') ? ' is-invalid' : '' }}"
                            >{{ old('description')?old('description'):$default_content }}</textarea>
                            @if ($errors->has('description'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
                            @endif
                        </div>
                    </div>


                    <div class="col-xs-12 col-sm-5 margin-top-20">
                        <div class="pull-left font-weight-700">Status</div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="status" name="status" checked value="1" {{ (old('status')=='1') ? 'checked' :'' }}   >
                            <label for="status">Active</label>
                        </div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="status" name="status" value="0" {{ (old('status')=='0') ? 'checked' :'' }}>
                            <label for="status">In Active</label>
                        </div>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('status') }}</strong></span>
                        @endif
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-20">
                        <div class="pull-left font-weight-700">Include in navigation</div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="include_in_navigation_yes" name="include_in_navigation" checked value="1" {{ (old('include_in_navigation')=='1') ? 'checked' :'' }}   >
                            <label for="include_in_navigation_yes">Yes</label>
                        </div>
                        <div class="pull-left tbl-radio-btn margin-left-30">
                            <input type="radio" id="include_in_navigation_no" name="include_in_navigation" value="0" {{ (old('include_in_navigation')=='0') ? 'checked' :'' }}>
                            <label for="include_in_navigation_no">No</label>
                        </div>
                        @if ($errors->has('include_in_navigation'))
                            <span class="invalid-feedback" style="display:block;"><strong>{{ $errors->first('include_in_navigation') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
            </form>

            <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-20">
                <div class="col-xs-12 padding-left-right-30">
                    <h4 class="font-weight-700 col-xs-6 margin-top-10"><i class="fa fa-angle-right margin-right-10 font-weight-700" aria-hidden="true"></i> Web Page</h4>
                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 pull-right margin-right-15 width-120">Edit</a>
                </div>

                <div class="col-xs-12 padding-left-right-30 margin-top-30 margin-bottom-10 hide">
                    <iframe id="ifrm" src="https://www.saltyiguana.com/" class="website-preview" frameborder="0" height="800" ></iframe>
                </div>
            </div>






    </div>
    <script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        //code added by jawed to facilitate FCK editor
        $( document ).ready(function() {
            tinymce.init({
                selector: 'textarea.editor',
                menubar:false,
                height: 320,
                theme: 'modern',
                plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                image_advtab: true,
                templates: [
                    { title: 'Test template 1', content: 'Test 1' },
                    { title: 'Test template 2', content: 'Test 2' }
                ],
            });
        });
    </script>

    <style>
        .form__container{
            max-width: 90%!important;
        }
        .form__user__edit {
            width: 100%!important;

        }

    </style>
@endsection
