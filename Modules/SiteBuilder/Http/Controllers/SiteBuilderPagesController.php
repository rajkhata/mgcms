<?php
    namespace Modules\SiteBuilder\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Routing\Controller;
    use App\Models\Restaurant;
    use App\Helpers\CommonFunctions;
    use Modules\SiteBuilder\Entities\SiteBuilderPages;
    use Modules\SiteBuilder\Http\Requests\SiteBuilderPagesRequest;

    use DB;



    class SiteBuilderPagesController extends Controller
    {


        public function __construct() {
            $this->middleware('auth');
        }


        public function index(Request $request) {

           // $restaurantData=CommonFunctions::getRestaurantWithBranches();
            $inputData=[];
            $restaurant_id     = \Auth::user()->restaurant_id;
           /* $parent_restaurant = CommonFunctions::getRestaurantParentInfo($restaurant_id);

            if($parent_restaurant->parent_restaurant_id>0 )
		$data = SiteBuilderPages::where(['restaurant_id' => $parent_restaurant->parent_restaurant_id])->orderBy('id', 'DESC');
	    else $data = SiteBuilderPages::where(['restaurant_id' => $restaurant_id])->orderBy('id', 'DESC');*/
            $data = SiteBuilderPages::where(['restaurant_id' => $restaurant_id])->orderBy('id', 'DESC');


            $data = $data->paginate(20);
            $filterd_slugs = ['home','reservation','enquiry','events','careers','catering','about','contact','menu','privacy-policy']; //whiltelist slugs

           return view('sitebuilder::sb_pages.index', compact('data','filterd_slugs'));

        }

        public function destroy(Request $request,$id)
        {

            try {
                DB::beginTransaction();
                $restaurant_id     = \Auth::user()->restaurant_id;
                //$parent_restaurant = CommonFunctions::getRestaurantParentInfo($restaurant_id);

               $filterd_slugs = ['home','about','contact']; //whiltelist slugs


                $pageobj=SiteBuilderPages::where('id',$id)
                     ->whereIn('slug', $filterd_slugs)->first();
                 if($pageobj) {
                     $slug=$pageobj->slug;
                     return back()->withInput()->withErrors(array('status_msg' => 'You cannot delete page with '.$slug.' slug'));
                 }else{
                   // SiteBuilderPages::where('restaurant_id', $parent_restaurant->parent_restaurant_id)->where('id', '=', $id)->delete();
                    SiteBuilderPages::where('restaurant_id', $restaurant_id)->where('id', '=', $id)->delete();
                    DB::commit();

                    if ($request->ajax()) {
                        return \Response::json(['success' => true, 'message' => 'Site Builder page  has been deleted successfully.'], 200);

                    } else {
                        return back()->with('status_msg', 'Site Builder page  has been deleted successfully.');
                    }
                }

            }
            catch (\Exception $e){
                DB::rollback();
                if ($request->ajax()) {
                    return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);

                }else{
                    return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
                }
            }
        }

        public function create()
        {
            //$restaurantData = CommonFunctions::getRestaurantWithBranches();
            $restaurantData='';
            return view('sitebuilder::sb_pages.create', compact('restaurantData'));
        }

        public function store(SiteBuilderPagesRequest $request)
        {
            try {
                DB::beginTransaction();

                $data              = $request->all();
                $restaurant_id     = \Auth::user()->restaurant_id;
               // $parent_restaurant = CommonFunctions::getRestaurantParentInfo($restaurant_id);

                $pageobj=SiteBuilderPages::where('slug', $data['slug'])
                    ->where('restaurant_id', $restaurant_id)->first();
                if($pageobj) {
                    return back()->withInput()->withErrors(array('status_msg' => 'Page already exist with this slug'));
                }else{
		 
		$meta_tags = json_encode([["slug"=>$data['slug'],"type"=>"meta","label"=>"title","content"=>$data['meta_title']],["slug"=>$data['slug'],"type"=>"meta","label"=>"description","content"=>$data['meta_description']],["slug"=>$data['slug'],"type"=>"meta","label"=>"keywords","content"=>$data['meta_keyword']]]);
                $data_request = [
                    'restaurant_id'    => $restaurant_id,
                    'title'            => $data['title'],
                    'slug'             => $data['slug'],
		    'meta_title'	=> $data['meta_title'],
		    'meta_tags' =>$meta_tags,
                    'meta_description' => $data['meta_description'],
                    'meta_keyword'     => $data['meta_keyword'],
                    'description'      => $data['description'],
                    'status'           => $data['status'],
                    'include_in_navigation'  => $data['include_in_navigation'],
                ];


                SiteBuilderPages::create($data_request);


                DB::commit();
                if ($request->ajax()) {
                    return \Response::json(['success' => true, 'message' => 'Site Builder page has been created successfully.'], 200);

                } else {
                    return back()->with('status_msg', 'Site Builder page has been created successfully.');
                }
               }

            } catch (\Exception $e) {
                DB::rollback();
                if ($request->ajax()) {
                    return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);

                }else{
                    return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
                }
            }
        }
        public function edit($id)
        {
            $restaurant_id     = \Auth::user()->restaurant_id;
            $restaurant_data = CommonFunctions::getRestaurantParentInfo($restaurant_id);
            $user_groups = DB::table('user_groups')
                ->whereIn('restaurant_id', [0,$restaurant_id])
                ->where(['status'=>1])
                ->get();
	   /* if($restaurant_data->parent_restaurant_id>0 )
            $data = SiteBuilderPages::where('restaurant_id', $restaurant_data->parent_restaurant_id)->where('id', '=' ,$id)->first();
	    else $data = SiteBuilderPages::where('restaurant_id',$restaurant_id)->where('id', '=' ,$id)->first();*/
            $data = SiteBuilderPages::where('restaurant_id',$restaurant_id)->where('id', '=' ,$id)->first();
            $restaurantData = CommonFunctions::getRestaurantWithBranches(NULL,$data->restaurant_id);
            $api_token = \Auth::user()->api_token;
            $filterd_slugs = ['home']; //whiltelist slugs

            return view('sitebuilder::sb_pages.edit', compact('restaurant_data','data','api_token','filterd_slugs','user_groups'));
        }

        public function update(SiteBuilderPagesRequest $request, $id)
        {

            if(!isset($id) || !is_numeric($id)){
                return redirect()->back()->with('err_msg','Invalid Request');
            }

           DB::beginTransaction();
           try {

                $data              = $request->all();
                $restaurant_id     = \Auth::user()->restaurant_id;
               // $restaurant_data = CommonFunctions::getRestaurantParentInfo($restaurant_id);
                $pageobj=SiteBuilderPages::where('slug', $data['slug'])
                    ->where('restaurant_id', $restaurant_id)->where('id', '!=' ,$id)->first();
                if($pageobj) {
                    return back()->withInput()->withErrors(array('status_msg' => 'Page already exist with this slug'));
                }else{
                 if(!isset($data['status']) || (isset($data['slug']) && $data['slug']=='home' )){
                       $status=1;  
                 }else  if(isset($data['status']) && $data['slug']!='home' ){
                        $status=$data['status'];  
                 }else{
                       $status=1;
                 }
		$meta_tags = json_encode([["slug"=>$data['slug'],"type"=>"meta","label"=>"title","content"=>$data['meta_title']],["slug"=>$data['slug'],"type"=>"meta","label"=>"description","content"=>$data['meta_description']],["slug"=>$data['slug'],"type"=>"meta","label"=>"keywords","content"=>$data['meta_keyword']]]);
                $data_request      = [
                    //'restaurant_id' =>$parent_restaurant->parent_restaurant_id,
                    'title'            => $data['title'],
                    'slug'             => $data['slug'],
                    'meta_description' => $data['meta_description'],
                    'meta_title'     => $data['meta_title'],
		    'meta_keyword'     => $data['meta_keyword'],	
		    'meta_tags' =>$meta_tags,	
                    'description'      => $data['description'],
                    'include_in_navigation'      => $data['include_in_navigation'],
                    'user_group_promotion'      => $data['user_group_promotion'],
                    'sort_order'      => $data['sort_order'],
                    'status'           =>$status,
                ];


                SiteBuilderPages::where('restaurant_id', $restaurant_id)->where('id', '=' ,$id)->update($data_request);
                $pagedata=SiteBuilderPages::where('restaurant_id', $restaurant_id)->where('id', '=' ,$id)->first();
                $promo_banners=json_decode($pagedata->promo_banners,1);
                $old_positions=$promo_banners;
                    $banners=NULL;
                    if(!$request->has('Type')){
                        $banners=NULL;
                        $promo_banners=NULL;
                    }else{

                        $imagePath     = config('constants.builder.upload_path');

                        $imageRelPath  = config('constants.builder.upload_main_path');
                        if(empty($pagedata->promo_banners)){
                            $promo_banners=[];
                            if($request->has('Type')){

                                $field_type=$request->input('Type');

                                foreach($field_type as $key=> $type) {

                                    if(!empty($type)) {

                                        $Display = $request->input('Display')[$key] ?? '';
                                        $Position = $request->input('Position')[$key] ?? '';
                                        $AltText = $request->input('AltText')[$key] ?? '';

                                        $Link = $request->input('Link')[$key] ?? '';
                                        $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                        $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                        $Status = $request->input('Status')[$key] ?? 0;
                                        $imageNameBase = $restaurant_id . '_' . str_replace(' ', '_', $Position);

					if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                            $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                            $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                            $Value_Mobile = $imageArr['image'];
                                        } else {
                                            $Value_Mobile = $request->input('Value_Mobile')[$key] ?? '';
                                        }
                                        
                                        if ($type!='Text' && @$request->file('Value')[$key]) {
                                            $imageName = $imageNameBase .time(). '_Banner_';
                                            $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                            $Value = $imageArr['image'];
                                        } else {
                                            $Value = $request->input('Value')[$key] ?? '';
                                        }
                                        
                                        if ($type!='Text' && @$request->file('Value_App')[$key]) {
                                            $imageName = $imageNameBase .time(). '_App_Banner_';
                                            $imageArr = CommonFunctions::fileUpload($request->file('Value_App')[$key], $imageName, $imagePath, $imageRelPath, false);
                                            $Value_App = $imageArr['image'];
                                        } else {
                                            $Value_App = $request->input('Value_App')[$key] ?? '';
                                        }
                                        
                                                                             
                                        
                                        if ((!empty($Value) || !empty($Value_Mobile) || !empty($Value_App)) && !empty($type)) {

                                            $promo_banners[] = [
                                                'Id' => $key,
                                                'Display' => $Display,
                                                'Position'=>$Position,
                                                'Type' => $type,
                                                'Value' => $Value,
						'Value_Mobile' => $Value_Mobile,
                                                'Value_App' => $Value_App,
                                                'AltText'=>$AltText,
                                                'Link' => $Link,
                                                'UserGroup' => $UserGroup,
                                                'SortOrder' => $SortOrder,
                                                'Status' => $Status
                                            ];

                                        }
					 
                                    }

                                }
                            }
                        }else{
                            $old_id=[];
                           $alloldids=$request->input('ID');
                           foreach ($alloldids as $oldid){
                              if($oldid!=''){
                                  $old_id[]=$oldid;
                              }

                           }
                            //$old_id = array_filter($request->input('ID'));
                            $updated_positions=[];
                            $last_id=0;
                            $last_ids=[];
                            if(count($old_positions)){
                                foreach ($old_positions as $pos){

                                    if (in_array($pos['Id'], $old_id)){
                                        $last_id=(int)$pos['Id'];
                                        $last_ids[]=$last_id;
                                        //  unset($pos);
                                        $updated_positions[]=$pos;

                                    }
                                }

                                $last_id=count($last_ids)?max($last_ids):0;
                            }

                            $newPosItem =$updated_positions;
                            $field_type=$request->input('Type');
                            $new_additions=[];

                            foreach($field_type as $key=> $type){

                                if(!isset($request->input('ID')[$key]) || $request->input('ID')[$key]==''){
					//print_r("iff---");
                                    if(!empty($type)) {

                                        $Display = $request->input('Display')[$key] ?? '';
                                        $Position = $request->input('Position')[$key] ?? '';
                                        $AltText = $request->input('AltText')[$key] ?? '';

                                        $Link = $request->input('Link')[$key] ?? '';
                                        $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                        $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                        $Status = $request->input('Status')[$key] ?? 0;
                                        $imageNameBase = $restaurant_id . '_' . str_replace(' ', '_', $Position);

					if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                            $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                            $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                            $Value_Mobile = $imageArr['image'];
                                        } else {
                                            $Value_Mobile = $request->input('Value_Mobile')[$key] ?? '';
                                        }
                                        if ($type!='Text' && @$request->file('Value')[$key]) {
                                            $imageName = $imageNameBase .time(). '_Banner_';
                                            $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                            $Value = $imageArr['image'];
                                        } else {
                                            $Value = $request->input('Value')[$key] ?? '';
                                        }
                                        
                                        
                                        if ($type!='Text' && @$request->file('Value_App')[$key]) {
                                            $imageName = $imageNameBase .time(). '_App_Banner_';
                                            $imageArr = CommonFunctions::fileUpload($request->file('Value_App')[$key], $imageName, $imagePath, $imageRelPath, false);
                                            $Value_App = $imageArr['image'];
                                        } else {
                                            $Value_App = $request->input('Value_App')[$key] ?? '';
                                        }
                                        
                                                                    
                                        
                                        if ((!empty($Value) || !empty($Value_Mobile) || !empty($Value_App)) && !empty($type)) {
                                            $last_id = $last_id + 1;
                                            $new_additions[] = [
                                                'Id' => $last_id,
                                                'Display' => $Display,
                                                'Position'=>$Position,
                                                'Type' => $type,
                                                'Value' => $Value,
						'Value_Mobile' => $Value_Mobile,
                                                'Value_App' => $Value_App,
                                                'AltText'=>$AltText,
                                                'Link' => $Link,
                                                'UserGroup' => $UserGroup,
                                                'SortOrder' => $SortOrder,
                                                'Status' => $Status
                                            ];
                                        }

                                    }

                                }else{

                                    foreach ($updated_positions as $upkey=>$posup){
                                         //print('hi2-');
                                        //dump($posup);
                                        if(trim($posup['Id'])==trim($request->input('ID')[$key])) {
                                            $Display = $request->input('Display')[$key] ?? '';
                                            $Position = $request->input('Position')[$key] ?? '';
                                            $AltText = $request->input('AltText')[$key] ?? '';

                                            $Link = $request->input('Link')[$key] ?? '';
                                            $UserGroup = $request->input('UserGroup')[$key] ?? '';
                                            $SortOrder = $request->input('SortOrder')[$key] ?? '';
                                            $Status = $request->input('Status')[$key] ?? 0;
                                            $imageNameBase = $restaurant_id . '_' . str_replace(' ', '_', $Position);
                                           // $last_id = $last_id + 1;
                                            $newmatched=[
                                                'Id' => $posup['Id'],
                                                'Display' => $Display,
                                                'Position'=>$Position,
                                                'Type' => $type,
                                                'AltText'=>$AltText,
                                                'Link' => $Link,
                                                'UserGroup' => $UserGroup,
                                                'SortOrder' => $SortOrder,
                                                'Status' => $Status
                                            ];

                                            if ($type!='Text' && @$request->file('Value')[$key]) {
                                                $imageName = $imageNameBase.time() . '_Banner_';
                                                $imageArr = CommonFunctions::fileUpload($request->file('Value')[$key], $imageName, $imagePath, $imageRelPath, false);
                                                $newmatched['Value']= $imageArr['image'];
                                            } else {
                                                if($type=='Text'){
                                                    $newmatched['Value'] = $request->input('Value')[$key] ?? '';

                                                }else{
                                                    $newmatched['Value'] = $posup['Value'];
                                                }
                                            }
                                            //$newPosItem[$upkey]=$newmatched;
					    if ($type!='Text' && @$request->file('Value_Mobile')[$key]) {
                                                $imageName = $imageNameBase .time(). '_Mobile_Banner_';
                                                $imageArr = CommonFunctions::fileUpload($request->file('Value_Mobile')[$key], $imageName, $imagePath, $imageRelPath, false);
                                                $newmatched['Value_Mobile']= $imageArr['image'];
                                            } else {
                                                if($type=='Text'){
                                                    $newmatched['Value_Mobile'] = $request->input('Value_Mobile')[$key] ?? '';

                                                }else{
                                                    $newmatched['Value_Mobile'] = $posup['Value_Mobile'];
                                                }
                                            }
                                            
                                            
                                            if ($type!='Text' && @$request->file('Value_App')[$key]) {
                                                $imageName = $imageNameBase .time(). '_App_Banner_';
                                                $imageArr = CommonFunctions::fileUpload($request->file('Value_App')[$key], $imageName, $imagePath, $imageRelPath, false);
                                                $newmatched['Value_App']= $imageArr['image'];
                                            } else {
                                                if($type=='Text'){
                                                    $newmatched['Value_App'] = $request->input('Value_App')[$key] ?? '';

                                                }else{
                                                    $newmatched['Value_App'] = $posup['Value_App'];
                                                }
                                            }
                                            
                                            $newPosItem[$upkey]=$newmatched;	
                                        }

                                    }

                                }

                            }

                            if(count($new_additions)){
                                $newPosItem=array_merge($newPosItem,$new_additions);
                            }

                            $promo_banners=$newPosItem;
                        }
                  }
                  //print_r($promo_banners);die;

                    $pagedata->promo_banners =(is_array($promo_banners) && count($promo_banners))? json_encode($promo_banners):NULL;
                    $pagedata->save();

              DB::commit();

              return back()->with('status_msg', 'Site Builder page has been updated successfully.');
            }

           } catch (\Exception $e) {
                DB::rollback();
                return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'.$e->getMessage()));

            }
        }

        public function updatePage(Request $request)
        {
            try {
                DB::beginTransaction();

                $data = $request->all();
                $restaurant_id = \Auth::user()->restaurant_id;
              //  $parent_restaurant=CommonFunctions::getRestaurantParentInfo($restaurant_id);

                SiteBuilderPages::where('slug', $data['slug'])
                    ->where('restaurant_id', $restaurant_id)
                    ->update(['description' =>$data['description']]);
                DB::commit();

                //if ($request->ajax()) {
                    return \Response::json(['success' => true, 'message' =>'Site Builder page has been updated successfully.'], 200);

              /*  }else{
                    return back()->with('status_msg', 'Site Builder page has been updated successfully.');
                }*/

            } catch (\Exception $e) {
                DB::rollback();
               // if ($request->ajax()) {
                    return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);

               /* }else{
                    return back()->withInput()->withErrors(array('status_msg' => 'Something went wrong, please try later'));
                }*/
            }
        }

        public function saveImage(Request $request)
        {
                header('Cache-Control: no-cache, must-revalidate');
                //Specify url path
                $path =config('constants.builder.upload_path');
                $baseurl= url(config('constants.builder.upload_main_path')).'/';
               // $baseurl=explode('://',$baseurl)[1];
                //Read image
                $count = $request['count'];
                $b64str = $request['hidimg-' . $count];
                $imgname = $request['hidname-' . $count];
                $imgtype = $request['hidtype-' . $count];

                $customvalue = $request['hidcustomval-' . $count]; //Get customvalue

                //Generate random file name here
                if($imgtype == 'png'){
                 $image = $imgname . '-' . base_convert(rand(),10,36) . '.png';
                } else {
                 $image = $imgname . '-' . base_convert(rand(),10,36) . '.jpg';
                }



                //Check folder. Create if not exist
                $pagefolder = $path;
                if (!file_exists($pagefolder)) {
                  mkdir($pagefolder, 0777);
                }


                //Optional: If using customvalue to specify upload folder
                if ($customvalue!='') {
                    $pagefolder = $path . $customvalue. '/';
                    $baseurl=$baseurl.$customvalue. '/';
                    if (!file_exists($pagefolder)) {
                         mkdir($pagefolder, 0777);
                    }
                }



                //Save image

                $success = file_put_contents($pagefolder . $image, base64_decode($b64str));
                if ($success === FALSE) {

                    if (!file_exists($path)) {
                        echo "<html><body onload=\"alert('Saving image to folder failed..')\"></body></html>";
                    } else {
                        echo "<html><body onload=\"alert('Saving image to folder failed.')\"></body></html>";
                    }

                } else {
                    //Replace image src with the new saved file
                    echo "<html><body onload=\"parent.document.getElementById('img-" . $count . "').setAttribute('crossorigin','anonymous');parent.document.getElementById('img-" . $count . "').setAttribute('src','" . $baseurl . $image . "');  parent.document.getElementById('img-" . $count . "').removeAttribute('id') \"></body></html>";
                }

        }

        public function saveImageLarge(Request $request)
        {
            header('Cache-Control: no-cache, must-revalidate');

            function image_resizeImageLarge($src, $dst, $width, $height){

                //echo "<html><body onload=\"alert('".$dst."')\"></body></html>";
                //exit();

                list($w, $h) = getimagesize($src);

                $type = strtolower(substr(strrchr($src,"."),1));
                if($type == 'jpeg') $type = 'jpg';
                switch($type){
                    case 'bmp': $img = imagecreatefromwbmp($src); break;
                    case 'gif': $img = imagecreatefromgif($src); break;
                    case 'jpg': $img = imagecreatefromjpeg($src); break;
                    case 'png': $img = imagecreatefrompng($src); break;
                    default : return "Unsupported picture type!";
                }

                $ratio = min($width/$w, $height/$h);
                $width = $w * $ratio;
                $height = $h * $ratio;
                $x = 0;

                $new = imagecreatetruecolor($width, $height);

                // preserve transparency
                if($type == "gif" or $type == "png"){
                    imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
                    imagealphablending($new, false);
                    imagesavealpha($new, true);
                }

                imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

                switch($type){
                    case 'bmp': imagewbmp($new, $dst); break;
                    case 'gif': imagegif($new, $dst); break;
                    case 'jpg': imagejpeg($new, $dst); break;
                    case 'png': imagepng($new, $dst); break;
                }
                return true;
            }

//Specify url path
            $path = config('constants.builder.upload_path');
            $baseurl= url(config('constants.builder.upload_main_path')).'/';


//Get customvalue
            $customvalue =$request->input('hidRefId'); //Custom value (optional). If you set "customval" parameter in ContentBuilder, the value can be read here.


//Check folder. Create if not exist
            $pagefolder = $path;
            if (!file_exists($pagefolder)) {
                mkdir($pagefolder, 0777);
            }


//Optional: If using customvalue to specify upload folder
            if ($customvalue!='') {
                $pagefolder = $path . $customvalue. '/';
                $baseurl=$baseurl.$customvalue. '/';

                if (!file_exists($pagefolder)) {
                    mkdir($pagefolder, 0777);
                }
            }




            $fileobj=$request->file('fileImage');

            $filename = $fileobj->getClientOriginalName();
            $file_temp=$fileobj->getPathName();
            $imageFileType = $fileobj->getClientOriginalExtension();

            $uploadOk = 1;
           // $imageFileType = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
           // $check = getimagesize($request->file('fileImage')["tmp_name"]);
            if($fileobj->isValid()) {
                //echo "File is an image - " . $check["mime"];
                $uploadOk = 1;
            } else {
                echo "<html><body onload=\"alert('File is not an image.')\"></body></html>";
                exit();
                $uploadOk = 0;
            }

// Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
                echo "<html><body onload=\"alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.')\"></body></html>";
                exit();
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "<html><body onload=\"alert('Sorry, your file was not uploaded.')\"></body></html>";
                exit();
            } else {

                $random = base_convert(rand(),10,36) . date("his");
                //$pic_type = strtolower(strrchr($filename,"."));
                $pic_type ='.'.$imageFileType;
                $pic_name = "original$random$pic_type";
                move_uploaded_file($file_temp, $pagefolder . $pic_name);

                //Save image
                if (true !== ($pic_error = @image_resizeImageLarge($pagefolder . $pic_name, $pagefolder . "$random$pic_type", 1600, 1600))) { //Resize image to max 1600x1600 to safe size
                    echo "<html><body onload=\"alert('".$pic_error."')\"></body></html>";
                    exit();
                }

                unlink($pagefolder . $pic_name); //delete original

                //Replace image src with the new saved file
                echo "<html><body onload=\"parent.applyLargerImage('" . $baseurl . "$random$pic_type" . "')\"></body></html>";

            }





        }

        public function saveImageModule(Request $request)
        {
            ob_start();
            header('Cache-Control: no-cache, must-revalidate');
            function image_resizeImageModule($src, $dst, $width, $height){

                //echo "<html><body onload=\"alert('".$dst."')\"></body></html>";
                //exit();

                list($w, $h) = getimagesize($src);

                $type = strtolower(substr(strrchr($src,"."),1));
                if($type == 'jpeg') $type = 'jpg';
                switch($type){
                    case 'bmp': $img = imagecreatefromwbmp($src); break;
                    case 'gif': $img = imagecreatefromgif($src); break;
                    case 'jpg': $img = imagecreatefromjpeg($src); break;
                    case 'png': $img = imagecreatefrompng($src); break;
                    default : return "Unsupported picture type!";
                }

                $ratio = min($width/$w, $height/$h);
                $width = $w * $ratio;
                $height = $h * $ratio;
                $x = 0;

                $new = imagecreatetruecolor($width, $height);

                // preserve transparency
                if($type == "gif" or $type == "png"){
                    imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
                    imagealphablending($new, false);
                    imagesavealpha($new, true);
                }

                imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

                switch($type){
                    case 'bmp': imagewbmp($new, $dst); break;
                    case 'gif': imagegif($new, $dst); break;
                    case 'jpg': imagejpeg($new, $dst); break;
                    case 'png': imagepng($new, $dst); break;
                }
                return true;
            }

//Specify url path
            $path =config('constants.builder.upload_path');
            $baseurl= url(config('constants.builder.upload_main_path')).'/';


//Get customvalue
            $customvalue =$request->input('hidCustomVal'); //Custom value (optional). If you set "customval" parameter in ContentBuilder, the value can be read here.


//Check folder. Create if not exist
            $pagefolder = $path;
            if (!file_exists($pagefolder)) {
                mkdir($pagefolder, 0777);
            }


//Optional: If using customvalue to specify upload folder
            if ($customvalue!='') {
                $pagefolder = $path . $customvalue. '/';
                $baseurl=$baseurl.$customvalue. '/';

                if (!file_exists($pagefolder)) {
                    mkdir($pagefolder, 0777);
                }
            }


            $fileobj=$request->file('fileImage');

            $filename = $fileobj->getClientOriginalName();
            $file_temp=$fileobj->getPathName();
            $imageFileType = $fileobj->getClientOriginalExtension();

            $uploadOk = 1;
            //$imageFileType = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
           // $check = getimagesize($file_temp);
            if($fileobj->isValid()) {
                //echo "File is an image - " . $check["mime"];
                $uploadOk = 1;
            } else {
                echo "<html><body onload=\"alert('File is not an image.')\"></body></html>";
                exit();
                $uploadOk = 0;
            }

// Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
                echo "<html><body onload=\"alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.')\"></body></html>";
                exit();
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "<html><body onload=\"alert('Sorry, your file was not uploaded.')\"></body></html>";
                exit();
            } else {

                $random = base_convert(rand(),10,36) . date("his");
                //$pic_type = strtolower(strrchr($filename,"."));
                $pic_type ='.'.$imageFileType;
                $pic_name = "original$random$pic_type";
                move_uploaded_file($file_temp, $pagefolder . $pic_name);

                //Save image
                if (true !== ($pic_error = @image_resizeImageModule($pagefolder . $pic_name, $pagefolder . "$random$pic_type", 1600, 1600))) { //Resize image to max 1600x1600 to safe size
                    echo "<html><body onload=\"alert('".$pic_error."')\"></body></html>";
                    exit();
                }

                unlink($pagefolder . $pic_name); //delete original

                //Replace image src with the new saved file
                echo "<html><body onload=\"parent.sliderImageSaved('" . $baseurl . "$random$pic_type" . "')\"></body></html>";

            }





        }

        public function saveCover(Request $request)
        {


            header('Cache-Control: no-cache, must-revalidate');
            function image_resizeCover($src, $dst, $width, $height, $crop=0){

                if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";

                $type = strtolower(substr(strrchr($src,"."),1));
                if($type == 'jpeg') $type = 'jpg';
                switch($type){
                    case 'bmp': $img = imagecreatefromwbmp($src); break;
                    case 'gif': $img = imagecreatefromgif($src); break;
                    case 'jpg': $img = imagecreatefromjpeg($src); break;
                    case 'png': $img = imagecreatefrompng($src); break;
                    default : return "Unsupported picture type!";
                }
                if($w < $width or $h < $height) {
                    $width = 1629;
                    $height = 850;
                }
                if($w < $width or $h < $height) {
                    $width = 1533;
                    $height = 800;
                }
                if($w < $width or $h < $height) {
                    $width = 1438;
                    $height = 750;
                }
                if($w < $width or $h < $height) {
                    $width = 1380;
                    $height = 720;
                }
                if($w < $width or $h < $height) {
                    $width = 1342;
                    $height = 700;
                }
                if($w < $width or $h < $height) {
                    $width = 1246;
                    $height = 650;
                }
                if($w < $width or $h < $height) {
                    $width = 1150;
                    $height = 600;
                }
                if($w < $width or $h < $height) {
                    $width = 1054;
                    $height = 550;
                }
                if($w < $width or $h < $height) {
                    $width = 958;
                    $height = 500;
                }
                if($w < $width or $h < $height) {
                    $width = 863;
                    $height = 450;
                }
                if($w < $width or $h < $height) {
                    $width = 767;
                    $height = 400;
                }
                if($w < $width or $h < $height) {
                    $width = 671;
                    $height = 350;
                }
                if($w < $width or $h < $height) {
                    $width = 575;
                    $height = 300;
                }
                if($w < $width or $h < $height) {
                    return "Picture is too small. Minimum dimension: 575 x 350 pixels.";
                }

                // resize
                if($crop){
                    $ratio = max($width/$w, $height/$h);
                    $h = $height / $ratio;
                    $x = ($w - $width / $ratio) / 2;
                    $w = $width / $ratio;
                }
                else{
                    $ratio = min($width/$w, $height/$h);
                    $width = $w * $ratio;
                    $height = $h * $ratio;
                    $x = 0;
                }

                $new = imagecreatetruecolor($width, $height);

                // preserve transparency
                if($type == "gif" or $type == "png"){
                    imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
                    imagealphablending($new, false);
                    imagesavealpha($new, true);
                }

                imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

                switch($type){
                    case 'bmp': imagewbmp($new, $dst); break;
                    case 'gif': imagegif($new, $dst); break;
                    case 'jpg': imagejpeg($new, $dst); break;
                    case 'png': imagepng($new, $dst); break;
                }
                return true;
            }

             //Specify url path
            $path = config('constants.builder.upload_path');
            $baseurl= url(config('constants.builder.upload_main_path')).'/';
            //$request->input('hidcustomval')

           //Get customvalue
            $customvalue = $request->input('hidcustomval');


           //Check folder. Create if not exist
            $pagefolder = $path;
            if (!file_exists($pagefolder)) {
                mkdir($pagefolder, 0777);
            }


           //Optional: If using customvalue to specify upload folder
            if ($customvalue!='') {
                $pagefolder = $path . $customvalue. '/';
                $baseurl=$baseurl.$customvalue. '/';

                if (!file_exists($pagefolder)) {
                    mkdir($pagefolder, 0777);
                }
            }
            $fileobj=$request->file('fileCover');

            $filename = $fileobj->getClientOriginalName();
            $file_temp=$fileobj->getPathName();
            $imageFileType = $fileobj->getClientOriginalExtension();

            $uploadOk = 1;
           // $imageFileType = strtolower(pathinfo($filename,PATHINFO_EXTENSION));

            // Check if image file is a actual image or fake image
            //$check = getimagesize($request->file('fileCover')["tmp_name"]);
            if($fileobj->isValid()) {
                //echo "File is an image - " . $check["mime"];
                $uploadOk = 1;
            } else {
                echo "<html><body onload=\"alert('File is not an image.')\"></body></html>";
                exit();
                $uploadOk = 0;
            }

// Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
                echo "<html><body onload=\"alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.')\"></body></html>";
                exit();
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "<html><body onload=\"alert('Sorry, your file was not uploaded.')\"></body></html>";
                exit();
            } else {

                $random = base_convert(rand(),10,36) . date("his");
                //$pic_type = strtolower(strrchr($filename,"."));
                $pic_type ='.'.$imageFileType;
                $pic_name = "original$random$pic_type";
                $resp=move_uploaded_file($file_temp, $pagefolder . $pic_name);
                print_r($resp);
                //Save image
                if (true !== ($pic_error = @image_resizeCover($pagefolder . $pic_name, $pagefolder . "$random$pic_type", 1600, 1600))) { //Resize image to max 1600x1600 to safe size
                    echo "<html><body onload=\"alert('".$pic_error."')\"></body></html>";
                    exit();
                }

                unlink($pagefolder . $pic_name); //delete original

                //Replace image src with the new saved file
                echo "<html><body onload=\"parent.applyBoxImage('" . $baseurl . "$random$pic_type')\"></body></html>";

            }



        }

    }
