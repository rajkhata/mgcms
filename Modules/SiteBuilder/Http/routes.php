<?php
    header('Access-Control-Allow-Origin: *');
    header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
Route::group(['middleware' => 'web', 'prefix' => 'content', 'namespace' => 'Modules\SiteBuilder\Http\Controllers'], function()
{
   // Note:ACL NEED TO BE IMPLEMENT LATER STAGE
    //Route::get('/', 'SiteBuilderController@index');
    Route::resource('pages', 'SiteBuilderPagesController');

});



    Route::group(['middleware' => 'auth:api','prefix' => 'api/sitebuilder', 'namespace' => 'Modules\SiteBuilder\Http\Controllers'], function()
    {
        Route::post('updatepage','SiteBuilderPagesController@updatePage');

        Route::post('saveimage','SiteBuilderPagesController@saveImage');
        Route::post('saveimage-large','SiteBuilderPagesController@saveImageLarge');
        Route::post('saveimage-module','SiteBuilderPagesController@saveImageModule');
        Route::post('savecover','SiteBuilderPagesController@saveCover');

    });
