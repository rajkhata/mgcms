<?php
    namespace Modules\SiteBuilder\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class SiteBuilderPagesRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            $rules= [
                'title' => 'required',
               // 'restaurant_id' => 'required',
                'slug' => 'required',
                 'meta_description' => 'required',
                'meta_keyword' => 'required',
                'description' => 'required',
              //  'status' => 'required',

            ];



            return $rules;

        }

        public function messages()
        {
            return [
                'title.required' => 'Title should not be empty',
                //'restaurant_id.required' => 'Restaurant  should not be empty',
                'slug.required' => 'Slug should not be empty',
                'meta_description.required' => 'Meta Description should not be empty',
                'meta_keyword.required' => 'Meta Keyword should not be empty',
                'description.required' => 'Description should not be empty',
                //'status.required' => 'Status  should not be empty',

            ];
        }
    }
