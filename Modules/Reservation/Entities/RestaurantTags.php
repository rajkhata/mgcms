<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantTags extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $table = 'cms_restaurant_tags';
    protected $fillable = ['restaurant_id','tags'];
}
