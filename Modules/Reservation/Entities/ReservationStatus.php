<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;

class ReservationStatus extends Model
{
    protected $table = 'cms_restaurant_resv_status';
    protected $fillable = ['name', 'color', 'icon'];

    /**
     * Get the tables for the floor.
     */
    public function reservations()
    {
        return $this->hasMany('Modules\Reservation\Entities\Reservation');
    }

    public static function getAllStatus()
    {
        return self::pluck( 'slug','id');
    }

    public static function getStatusForCurrentReservation()
    {
        return self::where('slug', '!=', config('reservation.status_slug.cancelled'))->pluck('id', 'slug');
    }

    public static function getStatusForUpcomingReservation()
    {
        return self::whereIn('slug', [config('reservation.status_slug.reserved'), config('reservation.status_slug.running_late'), config('reservation.status_slug.confirmed')])->pluck('id', 'slug');
    }

    public static function getStatusForWaitlistReservation()
    {
        return self::where('slug', config('reservation.status_slug.waitlist'))->pluck('id', 'slug');
    }

    public static function getReservedStatusId()
    {
        return self::where('slug', config('reservation.status_slug.reserved'))->value('id');
    }

    public static function getCancelledStatusId()
    {
        return self::where('slug', config('reservation.status_slug.cancelled'))->value('id');
    }

    public static function getNoShowStatusId()
    {
        return self::where('slug', config('reservation.status_slug.no_show'))->value('id');
    }

    public static function getSeatedStatusId()
    {
        return self::where('slug', config('reservation.status_slug.seated'))->value('id');
    }
    public static function getPartiallySeatedStatusId()
    {
        return self::where('slug', config('reservation.status_slug.partially_seated'))->value('id');
    }
    public static function getWaitlistStatusId()
    {
        return self::where('slug', config('reservation.status_slug.waitlist'))->value('id');
    }

    public static function getRunningLateStatusId()
    {
        return self::where('slug', config('reservation.status_slug.running_late'))->value('id');
    }

    public static function getConfirmedStatusId()
    {
        return self::where('slug', config('reservation.status_slug.confirmed'))->value('id');
    }

    public static function getFinishedStatusId()
    {
        return self::where('slug', config('reservation.status_slug.finished'))->value('id');
    }
}
?>