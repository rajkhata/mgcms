<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;

class YieldManagement extends Model
{
    protected $table    = 'yield_management';
    protected $fillable = ['restaurant_id', 'floor_id', 'data_before', 'html_before', 'data_after', 'html_after', 'xtime'];

    public function floor()
    {
        return $this->belongsTo('Modules\Reservation\Entities\Floor');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

}

?>