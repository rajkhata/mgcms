<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;

class TableServer extends Model
{
    protected $table = 'cms_restaurant_resv_table_servers';
    protected $fillable = ['restaurant_id', 'server_id', 'floor_id', 'table_ids', 'slot_id', 'slot_name', 'assigned_date'];

    public function floor()
    {
        return $this->belongsTo('Modules\Reservation\Entities\Floor', 'floor_id', 'id');
    }

    public function server()
    {
        return $this->belongsTo('Modules\Reservation\Entities\Server', 'server_id', 'id');
    }
}
?>
