<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'user_restaurant_resv';
    protected $primaryKey = 'id';
    protected $fillable = ['restaurant_id', 'parent_restaurant_id', 'user_id', 'floor_id', 'table_id', 'fname', 'lname', 'email', 'phone', 'start_time', 'end_time', 'status_id', 'yield_override', 'source',  'notify_me', 'reserved_seat', 'wait_time', 'mobile', 'slot_time','receipt_no','is_reviewed','agent','host_name','user_ip','user_instruction','restaurant_comment', 'special_occasion', 'host', 'cancellation_reason', 'is_guest', 'restaurant_name', 'payment_gatway', 'cancellation_charge_type', 'cancellation_charge', 'total_cancellation_charge', 'total_charged_amount', 'stripe_charge_id', 'card_number', 'card_type', 'stripe_card_id', 'total_refund_amount', 'payment_status','seated_at','payment_link_token'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    
    public function table()
    {
        return $this->belongsTo('Modules\Reservation\Entities\Table', 'table_id');
    }

    public function status()
    {
        return $this->belongsTo('Modules\Reservation\Entities\ReservationStatus', 'status_id');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant', 'restaurant_id');
    }

    public function getReservationByUser($userId,$emailId, $restArray = array())
    {   
        if($userId) {
            return self::where('user_id', $userId)->whereIn('restaurant_id', $restArray)->get();
        }else {
            return self::where('email',$emailId)->whereIn('restaurant_id', $restArray)->get();
        }
        
    }
}
?>
