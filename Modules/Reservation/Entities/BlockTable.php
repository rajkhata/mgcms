<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlockTable extends Model
{
    protected $table = 'cms_restaurant_resv_block_tables';
    protected $fillable = ['restaurant_id', 'floor_id', 'table_id', 'start_time', 'end_time'];

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
}
?>