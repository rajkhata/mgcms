<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TableType extends Model
{
	protected $table = 'cms_restaurant_resv_floor_table_types';
    protected $fillable = ['restaurant_id', 'name', 'min_seat', 'max_seat', 'tat'];

    use SoftDeletes;

  
    protected $dates = ['deleted_at'];

    /*public function reservations()
    {
        return $this->hasMany('Modules\Reservation\Entities\Reservation', 'table_id', 'id');
    }

    public function tables()
    {
        return $this->hasMany('Modules\Reservation\Entities\Table', 'type_id', 'id');
    }*/

}
?>