<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Server extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];


    protected $table = 'cms_restaurant_resv_servers';
    protected $fillable = ['restaurant_id', 'fname', 'lname', 'email', 'mobile','color'];

    public function tableservers()
    {
        return $this->hasMany('Modules\Reservation\Entities\TableServer');
    }
}
?>