<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;

class CustomWorkingHours extends Model
{
	protected $table = 'cms_restaurant_resv_custom_time';
    protected $fillable = ['restaurant_id', 'is_dayoff','calendar_date', 'slot_detail', 'custom_name'];
}
?>