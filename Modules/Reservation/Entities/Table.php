<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];


    protected $table = 'cms_restaurant_resv_floor_tables';
    protected $fillable = ['restaurant_id', 'floor_id', 'layout', 'name', 'configuration','min','max'];

    public function floor()
    {
        return $this->belongsTo('Modules\Reservation\Entities\Floor');
    }

    public function reservations()
    {
        return $this->hasMany('Modules\Reservation\Entities\Reservation', 'table_id', 'id');
    }

    /*public function tableType()
    {
        return $this->belongsTo('Modules\Reservation\Entities\TableType', 'type_id', 'id');
    }*/
}
?>