<?php

namespace Modules\Reservation\Entities;

use Illuminate\Database\Eloquent\Model;

class WorkingHours extends Model
{
	protected $table = 'cms_restaurant_resv_week_time';
    protected $fillable = ['restaurant_id', 'is_dayoff', 'calendar_day', 'slot_detail'];
}
?>