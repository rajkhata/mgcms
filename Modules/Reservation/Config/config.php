<?php

return [
    'name' => 'Reservation',
    'min_for_online_reservation'=>[
        'min' => 1,
        'max' => 30
    ],
    'max_for_online_reservation'=>[
        'min' => 1,
        'max' => 30
    ],
    'allow_guest_advance_online_reservation_type'=>['Days','Months'],
    'allow_guest_advance_online_reservation_days'=>[
        'min' => 1,
        'max' => 30
    ],
    'allow_guest_advance_online_reservation_months'=>[
        'min' => 1,
        'max' => 12
    ],
    'cut_off_time_hour'=>[
        'min' => 1,
        'max' => 24
    ],
    'cut_off_time_day'=>[
        'min' => 1,
        'max' => 15
    ],
    'cut_off_time_type'=>['Hour','Days'],
    'working_hour_type' => [
        'day'  => 'day',
        'date' =>  'date'
    ],
    'calendar_days' => [
        0 => 'sun',
        1 => 'mon',
        2 => 'tue',
        3 => 'wed',
        4 => 'thu',
        5 => 'fri',
        6 => 'sat'
    ],
    'week' => 'week',
    'month' => 'month',
    'week_days' => 7,
    'day' => 1,
    'status' => [
        'reserved' => 'Reserved',
        'cancelled' => 'Cancelled',
        'no_show' => 'No Show',
        'running_late' => 'Running Late',
        'confirmed' => 'Confirmed',
        'partially_seated' => 'Partially Seated',
        'seated' => 'Seated',
        'starters' => 'Starters',
        'entree' => 'Entrée',
        'main_course' => 'Main Course',
        'dessert' => 'Dessert',
        'bill_dropped' => 'Bill Dropped',
        'paid' => 'Paid',
        'finished' => 'Finished',
        'waitlist' => 'Waitlist'
    ],
    'status_slug' => [
        'reserved' => 'reserved',
        'cancelled' => 'cancelled',
        'no_show' => 'no_show',
        'running_late' => 'running_late',
        'confirmed' => 'confirmed',
        'partially_seated' => 'partially_seated',
        'seated' => 'seated',
        'starters' => 'starters',
        'entree' => 'entree',
        'main_course' => 'main_course',
        'dessert' => 'dessert',
        'bill_dropped' => 'bill_dropped',
        'paid' => 'paid',
        'finished' => 'finished',
        'waitlist' => 'waitlist'
    ],
    'status_slug_to_be_cancelled' => [
        'reserved' => 'reserved',
        'running_late' => 'running_late',
        'confirmed' => 'confirmed',
        'partially_seated' => 'partially_seated',
    ],
    'status_slug_to_be_update' => [
        'reserved' => 'reserved',
        'running_late' => 'running_late',
        'confirmed' => 'confirmed',
    ],
    'status_slug_check_for_book_reservation' => [
        'reserved' => 'reserved',
        'running_late' => 'running_late',
        'confirmed' => 'confirmed',
        'partially_seated' => 'partially_seated',
        'seated' => 'seated',
        'starters' => 'starters',
        'entree' => 'entree',
        'main_course' => 'main_course',
        'dessert' => 'dessert',
        'bill_dropped' => 'bill_dropped',
        'paid' => 'paid'
    ],
    'source' => [
        'online'=>'Online',
        'offline'=>'Offline',
        'walk_in'=>'Walk In'
    ],
    'guest_category' => [
        'New' => 'New',
        'Repeat' => 'Repeat',
        'VIP' => 'VIP'
    ],
    'slot_interval_time' => 15,
    'time_left_to_extend' => 5,
    'restaurant_timing' => [
        'open_time' => '00:00',
        'close_time' => '24:00'
    ],
    'reservation_category' => [
        'current' => 'current',
        'upcoming' => 'upcoming',
        'waitlist' => 'waitlist',
        'archive'  => 'archive'
    ],
    'current_next_status' => [
        'reserved' => [
            'cancelled' => 'Cancelled',
            'no_show' => 'No Show',
            'running_late' => 'Running Late',
            'confirmed' => 'Confirmed',
            'partially_seated' => 'Partially Seated',
            'seated' => 'Seated'
        ],
        'cancelled' => [
            'reserved' => 'Reserved'
        ],
        'no_show' => [
            'reserved' => 'Reserved'
        ],
        'running_late' => [
            'reserved' => 'Reserved',
            'cancelled' => 'Cancelled',
            'no_show' => 'No Show',
            'confirmed' => 'Confirmed',
            'partially_seated' => 'Partially Seated',
            'seated' => 'Seated'
        ],
        'confirmed' => [
            'cancelled' => 'Cancelled',
            'no_show' => 'No Show',
            'running_late' => 'Running Late',
            'partially_seated' => 'Partially Seated',
            'seated' => 'Seated'
        ],
        'partially_seated' => [
            'reserved' => 'Reserved',
            'cancelled' => 'Cancelled',
            'seated' => 'Seated',
        ],
        'seated' => [
            'reserved' => 'Reserved',
            'starters' => 'Starters',
            'entree' => 'Entrée',
            'main_course' => 'Main Course',
            'dessert' => 'Dessert',
            'bill_dropped' => 'Bill Dropped',
            'paid' => 'Paid',
            'finished' => 'Finished'
        ],
        'starters' => [
            'seated' => 'Seated',
            'entree' => 'Entrée',
            'main_course' => 'Main Course',
            'dessert' => 'Dessert',
            'bill_dropped' => 'Bill Dropped',
            'paid' => 'Paid',
            'finished' => 'Finished'
        ],
        'entree' => [
            'seated' => 'Seated',
            'main_course' => 'Main Course',
            'dessert' => 'Dessert',
            'bill_dropped' => 'Bill Dropped',
            'paid' => 'Paid',
            'finished' => 'Finished'
        ],
        'main_course' => [
            'seated' => 'Seated',
            'entree' => 'Entrée',
            'dessert' => 'Dessert',
            'bill_dropped' => 'Bill Dropped',
            'paid' => 'Paid',
            'finished' => 'Finished'
        ],
        'dessert' => [
            'seated' => 'Seated',
            'entree' => 'Entrée',
            'main_course' => 'Main Course',
            'bill_dropped' => 'Bill Dropped',
            'paid' => 'Paid',
            'finished' => 'Finished'
        ],
        'bill_dropped' => [
            'seated' => 'Seated',
            'starters' => 'Starters',
            'entree' => 'Entrée',
            'main_course' => 'Main Course',
            'paid' => 'Paid',
            'finished' => 'Finished'
        ],
        'paid' => [
            'seated' => 'Seated',
            'finished' => 'Finished'
        ],
        'finished' => [

        ]
    ],
    'upcoming_next_status' => [
        'reserved' => [
            'cancelled' => 'Cancelled',
            'confirmed' => 'Confirmed',
            'partially_seated' => 'Partially Seated',
            'seated' => 'Seated'
        ],
        'no_show' => [
            'reserved' => 'Reserved'
        ],
        'confirmed' => [
            'cancelled' => 'Cancelled',
            'no_show' => 'No Show',
            'running_late' => 'Running Late',
            'partially_seated' => 'Partially Seated',
            'seated' => 'Seated'
        ],
        'running_late' => [
            'reserved' => 'Reserved',
            'cancelled' => 'Cancelled',
            'no_show' => 'No Show',
            'confirmed' => 'Confirmed',
            'partially_seated' => 'Partially Seated',
            'seated' => 'Seated'
        ]
    ],
    'waitlist_next_status' => [
        'waitlist' => [
            'seated' => 'Seated',
            'cancelled' => 'Cancelled',
        ]
    ],
    'special_occasion' => [
        'birthday' => 'Birthday',
        'anniversary' => 'Anniversary',
        'date_night' => 'Date Night',
        'business_meal' => 'Business Meal',
        'celebration' => 'Celebration'
    ],
    'wait_time_no_show' => 5,
    'future_upcoming_next_status' => [
        'reserved' => [
            'cancelled' => 'Cancelled',
            'confirmed' => 'Confirmed',
        ],
        'cancelled' => [
            'reserved' => 'Reserved'
        ],
        'confirmed' => [
            'cancelled' => 'Cancelled'
        ]
    ],
    'grid_view' => 'grid',

    'status_slug_check_for_grid_reservation' => [
        'past' =>
            [
            'partially_seated' => 'partially_seated',
            'seated' => 'seated',
            'starters' => 'starters',
            'entree' => 'entree',
            'main_course' => 'main_course',
            'dessert' => 'dessert',
            'bill_dropped' => 'bill_dropped',
            'paid' => 'paid',
            'finished' => 'finished'
            ],
        'current_upcoming' => [
            'reserved' => 'reserved',
            'running_late' => 'running_late',
            'confirmed' => 'confirmed',
            'partially_seated' => 'partially_seated',
            'seated' => 'seated',
            'starters' => 'starters',
            'entree' => 'entree',
            'main_course' => 'main_course',
            'dessert' => 'dessert',
            'bill_dropped' => 'bill_dropped',
            'paid' => 'paid',
            'finished' => 'finished',
            'waitlist' => 'waitlist'
        ]
    ],
    'reservation_type' => [
        'enquiry' => 'enquiry',
        'full' => 'full',
        'third_party' => 'third_party'
    ],
    'payment_gateway' => [
        'stripe' => [
            'key'    => env('STRIPE_KEY'),
            'secret' => env('STRIPE_SECRET'),
        ],
    ],
    'payment_gateway_type' => [
        'stripe' => 'stripe',
        'froogal' => 'froogal'
    ],

    'cancellation_charge_type'=>[
        'per_cover'=>'Per Cover',
        'flat_rate'=>'Flat Rate'
    ],
    'past_reservation_status_change'=>[
        'partially_seated' => 'partially_seated',
        'seated' => 'seated',
        'starters' => 'starters',
        'entree' => 'entree',
        'main_course' => 'main_course',
        'dessert' => 'dessert',
        'bill_dropped' => 'bill_dropped',
        'paid' => 'paid'
    ],
    'timer' => [
        'currently_seated_time' => 'currently_seated_time',
        'remaining_time' => 'remaining_time',
        'next_resv_time' => 'next_resv_time'
    ],
    'payment_status'     => [
        'charged' => 'charged',
        'refunded' => 'refunded',
        'pending' => 'pending',
        'failed' => 'failed'
    ]
];
