<?php

namespace Modules\Reservation\Http\ViewComposers;

use App\Models\Restaurant;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

use Modules\Reservation\Entities\Floor;


class SideMenuComposer
{
  

    public function __construct(Floor $floor)
    {
        $this->floor=$floor;
        // Dependencies automatically resolved by service container...
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $floormenu = [];
        $timezone = 'UTC';
        if(\Auth::check()){
          $floormenu=$this->floor->get_floor_menu();
          $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => \Auth::user()->restaurant_id])->first();
            if($timeZoneResult && $timeZoneResult->time_zone) {
                $timezone = $timeZoneResult->time_zone;
            }
        }  
     $view->with(['floormenu' => $floormenu, 'timezone' => $timezone]);
    }
}