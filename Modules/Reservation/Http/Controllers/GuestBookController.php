<?php

namespace Modules\Reservation\Http\Controllers;
use App\Models\UserOrder;
use App\Models\StaticBlockResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Reservation\Entities\ReservationStatus;
use Modules\Reservation\Entities\Table;
use DB;
use URL;
use Session;
use App\Models\User;
use App\Models\UserAddresses;
use Illuminate\Support\Facades\Input;
use Modules\Reservation\Entities\RestaurantTags;
use Modules\Reservation\Entities\Tags;
use Carbon\Carbon;
use App\Models\Restaurant;
use App\Helpers\CommonFunctions;

class GuestBookController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
     public function __construct()
    {        
        $this->middleware('permission:Guestbook');
         $this->middleware('permission:Guestbook-view', ['only' => ['getGuestDetail']]);
  //      $this->middleware('permission:Guestbook-edit ', ['only' => ['updateGuestDetail']]);
    }
    public function index(Request $request)
    {
        $tags=app('Modules\Reservation\Http\Controllers\FloorController')->gettagslist();
 
        $restaurant_detail = Restaurant::where('id', \Auth::user()->restaurant_id)->first();
        if ($request->session()->exists('restaurant_id')&& Session::get('restaurant_id')!='All') {
            // user find in session
            $rest_id = Session::get('restaurant_id');
            $parent_restaurant_id = $rest_id;
        }
        else{

            $parent_restaurant_id = $restaurant_detail->parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $restaurant_detail->parent_restaurant_id;
        }

         #Rg on 11-10-2018 to show orders and reservation of particular Restaurant
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        #check Reservation allowed
        $reservation_locations = Restaurant::whereIn('id', $restArray)->where('reservation', 1)->select(DB::raw('count(reservation) as reservation_count'))->get()->toArray();

        $guests =  User::where('users.restaurant_id', $parent_restaurant_id)
                       ->select('users.id', 'users.fname', 'users.lname', 'users.referral_code', 'users.email', 'users.mobile', 'category', DB::raw("COUNT(user_orders.user_id) as order_count"),DB::raw("MIN(user_orders.created_at) as firstactivity"),DB::raw("MAX(user_orders.created_at) as lastactivity"))

                       ->leftJoin('user_orders', 'user_orders.user_id', '=', 'users.id')->groupBy('user_orders.user_id')
                       ->with(['orders' => function($query) use($restArray) {
                           // NOTE - created_at should be placed at first (for sorting)
                             $query->select('created_at', 'id AS order_id', 'user_id', 'total_amount','status')
                            ->whereIn('restaurant_id', $restArray)->orderBy('created_at', 'desc');
                                 // as discussed with Prakash & Adit (24-Jun-19) status only pending
                              // ->whereNotIn('status',['pending'])->orderBy('created_at', 'desc');
                           if(Auth::user()->role != 'admin') {
                               $query->where('product_type', '<>', 'gift_card');
                           }
                       }, 'reservations' => function($query) use($restArray)  {
                           // NOTE - start_time should be placed at first (for sorting)
                             $query->select('start_time', 'created_at', 'id AS reservation_id', 'user_id')
                            ->whereIn('restaurant_id', $restArray)
                            ->orderBy('start_time', 'desc');
                          }
                      ]);

      //  if ($request->isMethod('post') || $request->isMethod('get')) {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            if($request->has('guest_category')){
                $guest_cat=array_keys($request->input('guest_category'));
                $guests = $guests->whereIn('category',$guest_cat);
            }

            if($request['special_occasion_type']){

             //   if($request['special_occasion']){
                $occasions =array();
                if($request['doa']!='')
                {

                    array_push($occasions,$request['doa']);
                }
                if($request['dob']!='')
                {

                    array_push($occasions,$request['dob']);
                }

                 $special_occasion_type=$request['special_occasion_type'];

                 $guests =$guests->where(function ($query) use ($occasions, $special_occasion_type){

                        if( $special_occasion_type=='week'){
                            if(in_array('dob',$occasions)){
                                 //$query->whereNotNull("dob");
                                 $query->WhereRaw('WEEKOFYEAR(dob)=WEEKOFYEAR(NOW())');
                            }
                            if(in_array('doa',$occasions)){
                                //$query->whereNotNull("doa");
                                $query->orWhereRaw('WEEKOFYEAR(doa)=WEEKOFYEAR(NOW())');
                            }

                        }elseif ($special_occasion_type=='month') {
                          if(in_array('dob',$occasions)){
                              //$query->whereNotNull("dob");
                              $query->WhereMonth('dob', '=', date('m'))->whereNotNull("dob");
                            }
                             if(in_array('doa',$occasions)){

                                 //$query->whereNotNull("doa");

                               $query->orWhereMonth('doa', '=', date('m'))->whereNotNull("doa");
                             //  dd($d);
                            }
                        }
                });
            }
            if($request->has('tags') && !empty($request->input('tags'))){
                $guest_tags_input=$request->input('tags');
                $guest_tags_input = array_map('trim', explode(',', $guest_tags_input));
                $guests =$guests->where(function ($q) use($guest_tags_input){
                      foreach($guest_tags_input as $tag){
                        $q= $q->whereJsonContains('tags', $tag,'or');
                        // $query->orWhereRaw('JSON_CONTAINS(tags, \'["Chese Lover"]\')');
                     }
                });
             }
            if($reservation_locations['0']['reservation_count'] > 0){
                if($request->has('reservation_min') && $request->has('reservation_max')){
                        $reservation_min=$request->input('reservation_min');
                        $reservation_max=$request->input('reservation_max');
                       $guests = $guests->has('reservations', '>=', $reservation_min)->has('reservations', '<=', $reservation_max);
                }
            }

            if($request['order_min']!=''|| $request['order_max']!=''){

                  $order_min=$request['order_min'];
                   $order_max= $request['order_max'];

                    $guests = $guests->has('active_orders', '>=', $order_min)->has('active_orders', '<=', $order_max);
                 //     $guests = $guests->has('active_orders', '<=', $order_max);


                    //->has('order', '>=', $order_min)->has('order', '<=', $order_max);
            }

            // NTRS-1032 & NTRS-1035
            if($request['order_averagespend_min']!='' || $request['order_averagespend_max'] !=''){
                $order_averagespend_min = $request['order_averagespend_min'];
                       $order_averagespend_max = $request['order_averagespend_max'];
              //  echo 'enter';


                        if($request['order_averagespend_min']!=0) {

                            $guests->whereNotIn('user_orders.status', ['pending'])
                            ->whereNotIn('user_orders.status', ['refund']);
                        }
                              $guests->havingRaw("SUM(user_orders.total_amount) / COUNT(DISTINCT(user_orders.id)) >= $order_averagespend_min")
                              ->havingRaw("SUM(user_orders.total_amount) / COUNT(DISTINCT(user_orders.id)) <= $order_averagespend_max")
                            ->orHavingRaw("SUM(user_orders.total_amount) / COUNT(DISTINCT(user_orders.id)) IS NULL");
                              //->orHavingRaw("COUNT(user_orders.id)=0"); // removed as this condition not returning 0 order data
             //  dd($guests);
            }
       }

        if($request['search']['value']){
           $keyword = trim($request['search']['value']);
           $guests = $guests->where(function ($query) use ($keyword){
                      $query->where('users.fname', 'like', '%' . $keyword. '%')
                      ->orWhere('users.lname', 'like', '%' . $keyword. '%')
                      ->orWhere(DB::raw("CONCAT( users.fname,  ' ', users.lname )"), 'like', '%' . $keyword. '%')
                      ->orWhere('users.email', 'like', '%' . $keyword. '%')
                      ->orWhere('users.mobile', 'like', '%' . $keyword. '%');
            });
        }

        // Filter Sorting (Frequency - Orders, Reservations, Recency - Orders, Reservations)
        $sorted = [];
        if($request->input('filter_sort') && $guests->count()>0) {
            $guests      = $guests->paginate(10000000);
            $filter_sort = $request->input('filter_sort');
            switch ($filter_sort) {
                case 'frequency_orders':
                    $sorted = $guests->sortByDesc(function ($guests) {
                        $ordersCount = $guests->orders->count();
                        if ($ordersCount) {
                            $recentOrder = $guests->orders->first();
                            $oldestOrder = $guests->orders->last();
                            if ($recentOrder) {
                                $recentDate = Carbon::createFromFormat('Y-m-d H:i:s', $recentOrder->created_at)->month;
                            }
                            if ($oldestOrder) {
                                $oldestDate = Carbon::createFromFormat('Y-m-d H:i:s', $oldestOrder->created_at)->month;
                            }
                            $diffMonths = count(range($oldestDate, $recentDate));

                            return $frequencyOrders = $ordersCount / $diffMonths;
                            //dd([$recentOrder->toArray(), $oldestOrder->toArray(), $recentDate, $oldestDate, $ordersCount, $diffMonths,$frequencyOrders]);
                        } else {
                            return 0;
                        }
                    });
                    break;

                case 'frequency_reservations':
                    $sorted = $guests->sortByDesc(function ($guests) {
                        $resvCount = $guests->reservations->count();
                        if ($resvCount) {
                            $recentResv = $guests->reservations->first();
                            $oldestResv = $guests->reservations->last();
                            if ($recentResv) {
                                $recentDate = Carbon::createFromFormat('Y-m-d H:i:s', $recentResv->start_time)->month;
                            }
                            if ($oldestResv) {
                                $sTime      = $oldestResv->start_time ? $oldestResv->start_time : $oldestResv->created_at;
                                $oldestDate = Carbon::createFromFormat('Y-m-d H:i:s', $sTime)->month;
                            }
                            $diffMonths = count(range($oldestDate, $recentDate));

                            return $frequencyResv = $resvCount / $diffMonths;
                        } else {
                            return 0;
                        }
                        //dd([$recentResv->toArray(), $oldestResv->toArray(), $recentDate, $oldestDate, $resvCount, $diffMonths,$frequencyOrders]);
                    });
                    break;

                case 'recency_orders':
                    $sorted = $guests->sortByDesc(function ($guests) {
                        return $guests->orders->first(function ($value, $key) {
                            return Carbon::createFromFormat('Y-m-d H:i:s', $value['created_at'])->timestamp;
                        });
                    });
                    break;

                case 'recency_reservations':
                    $sorted = $guests->sortByDesc(function ($guests) {
                        return $guests->reservations->first(function ($value, $key) {
                            return Carbon::createFromFormat('Y-m-d H:i:s', $value['start_time'])->timestamp;
                        });
                    });
                    break;

                default:    // recency_reservations
                    $sorted = $guests->sortByDesc(function ($guests) {
                        return $guests->reservations->first(function ($value, $key) {
                            return Carbon::createFromFormat('Y-m-d H:i:s', $value['start_time'])->timestamp;
                        });
                    });
                    break;
            }
        }

        //dd($guests->get()->toArray());
        DB::unprepared("SET sql_mode = ''");
        //\Log::info($guests->get());
       if($request->has('export') && $request->input('export') && count($sorted)==0 && !$request->ajax()){
           $guests = $guests->orderBy('id', 'DESC')->get();
        }
        else if(count($sorted) == 0 && !$request->ajax() ) {

          $guests = $guests->orderBy('id', 'DESC')->get();;

        }

       // echo "<pre>";print_r($guests->toArray());die;
        // max reservations, orders, avg. orders spend
        /*$maxReservation = 0;
        $maxOrder       = 0;
        $avgSpend       = 0;
        foreach ($guests as &$guest) {
            $maxOrder       = (count($guest->orders) > $maxOrder) ? count($guest->orders) : $maxOrder;
            if($reservation_locations['0']['reservation_count'] > 0){
                $maxReservation = (count($guest->reservations)) > $maxReservation ? count($guest->reservations) : $maxReservation;
            }
            $totalSpent     = 0;
            foreach ($guest->orders as $order) {
                if($order->status!='cancelled' &&  $order->status!='rejected' &&  $order->status!='pending'){
                    $totalSpent += $order->total_amount;
                }
                
            }
            $guest->total_order_spend=$totalSpent;
            if($totalSpent > 0 || $maxOrder > 0){
                $avgSpend = ($totalSpent / $maxOrder) > $avgSpend ? $totalSpent / $maxOrder : $avgSpend;
            }
        }
        $avgSpend = ceil($avgSpend);*/
        $data1 = $this->getFilterValues($guests);
        // Get all guest details, ignoring the pagination
   //    DB::enableQueryLog();
       $allGuestData = User::where('users.restaurant_id', $parent_restaurant_id)
            ->with(['orders' => function($query) use($restArray) {
                $query->select('id AS order_id', 'user_id', 'total_amount','status')
                    ->whereIn('restaurant_id', $restArray)
                    ->whereNotIn('status',['pending']);
            }, 'reservations' => function($query) use($restArray)  {
                $query->select('id AS reservation_id', 'user_id')
                    ->whereIn('restaurant_id', $restArray);
            }
        ])->get();


        $filterValues = $this->getFilterValues($allGuestData);
/*  $query = DB::getQueryLog();
    $query = end($query);
    print_r($query);*/
       // $filterValues = $this->getFilterValues($allGuestData);
  //    print_r($filterValues);
        $maxReservation = 0;
        $maxOrder       = 0;
        $avgSpend       = 0;
        if($filterValues) {
            $maxReservation = $filterValues['maxReservation'];
            $maxOrder       = $filterValues['maxOrder'];
            $avgSpend       = $filterValues['avgSpend'];
        }
        if($request->has('export') && $request->input('export')){
                $this->export($guests);
        }
        if ($request->ajax()) {
           /* if(count($sorted)) {
                $guests = $sorted->values()->all();
            }*/
            /***********************************************************************/
            $columns = array(
                0 => "users.fname",
                1 => "users.email",
		2 => "users.referral_code",
                3 =>  "order_count",
                4 =>  "firstactivity",
                5 => "lastactivity"
            );

	   
       $total = $guests->get();
          //  $guests->toSql()
            $totalData =  count($total);
            if($totalData==0||$total==''||$total==NULL)
            {
               // return view('reservation::guestbook.partial.guest_render', compact('guests', 'reservation_locations'));
                $json_data = array(
                    "draw" => intval($request['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                    "recordsTotal" => intval($totalData),  // total number of records
                    "recordsFiltered" => intval($totalData), // total number of records after searching, if there is no searching then totalFiltered = totalData
                    "data" => ''  ,
                    'start' => 0,
                    'length' => 0
                );
                echo  json_encode($json_data);
                exit;
            }
            else{
                $guests = $guests->orderBy($columns[$request['order'][0]['column']],$request['order'][0]['dir'])->offset($request['start'])->limit($request['length'])->get();

                $data = array();
                foreach($guests as $guest) {
                    $nestedData = array();

                    $url=  "'".URL::to('guestbook/detail/'. $guest['id'])."'";
                    $link = '<span  onclick="getGuestbookDetail('.$url.')" >';
                    $nestedData[] = $link .$guest['fname'].' '.$guest['lname'].'</span>';
                    $nestedData[] = $link.$guest['email'].'</span>';
		     $nestedData[] = $link.$guest['referral_code'].'</span>';
                    if(Auth::user()) {
                        $curSymbol = Auth::user()->restaurant->currency_symbol;
                    } else {
                        $curSymbol = config('constants.currency');
                    }
                    $transaction='<span>'. $guest->active_orders()->count().' Orders</span>';
                    if($reservation_locations['0']['reservation_count'] > 0) {
                        $transaction .='<br class="hidden-br"/>'.$guest->reservations->count().'Reservations</span>';
                     }
                    $transaction .='<p class="item-price font-weight-500">'.(($guest->active_orders()->count()!=0)? ($guest->active_orders()->sum('total_amount') > 0 ? $curSymbol.round(($guest->active_orders()->sum('total_amount')), 2) : 'No Transaction') : 'No Transaction').'</p>';
                        $nestedData[] = $transaction;
                        $first_last_activity_arr = getFirstAndLastActivityDates($guest);
                        $first_last =   ($first_last_activity_arr['first_activity_date']) ? \Carbon\Carbon::parse($first_last_activity_arr['first_activity_date'])->format('M d, Y') : 'N/A' ;
                        //  echo    'first_activity_date---'.$first_last_activity_arr['first_activity_date'];

                        $nestedData[] = $first_last;
                        $last_activity =  ($first_last_activity_arr['last_activity_date']) ? \Carbon\Carbon::parse($first_last_activity_arr['last_activity_date'])->format('M d, Y') : 'N/A' ;
                        $nestedData[] = $last_activity ;
                        $data[] = $nestedData;

                }
                $json_data = array(
                    "draw" => intval($request['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                    "recordsTotal" => intval($totalData),  // total number of records
                    "recordsFiltered" => intval($totalData), // total number of records after searching, if there is no searching then totalFiltered = totalData
                    "data" => $data  ,
                    'start' => $request['start'],
                    'length' => $request['length']
                );

            echo  json_encode($json_data);
            }
            exit;
        }

        return view('reservation::guestbook.index', compact('guests','tags', 'maxOrder', 'maxReservation', 'avgSpend', 'reservation_locations'));
    }

    /**
     * Get Guest filter values (avgSpend,maxOrder,maxReservation,totalSpent)
     * @param $allGuestData
     * @return array
     */
    public function getFilterValues($allGuestData)
    {
        $maxReservation = 0;
        $maxOrder       = 0;
        $avgSpend       = 0;
        $newTotal       = [];
        $totalSpent     = 0;

        foreach ($allGuestData as &$allGuest) {

                 $maxOrder = (  $allGuest->active_orders()->count() > $maxOrder) ?   $allGuest->active_orders()->count() : $maxOrder;
            //    $maxOrder = (count($allGuest->orders) > $maxOrder) ? count($allGuest->orders) : $maxOrder;
                $maxReservation = (count($allGuest->reservations)) > $maxReservation ? count($allGuest->reservations) : $maxReservation;
                // 17-Dec-18, PE-3296, Discussed with Pravish, check avg. based on each user not all users.
                $gstOrder = 0;//count($allGuest->orders);
                $gstReservation = count($allGuest->reservations);
                $totalSpent = 0;
                foreach ($allGuest->orders as $order) {
                    // as discussed with Prakash & Adit (24-Jun-19) status only pending
                    if ($order->status != 'pending') { // $order->status != 'cancelled' && $order->status != 'rejected'

                        $totalSpent += $order->total_amount;
                        if ($order->status != 'refunded')
                            $gstOrder++;
                    }
                }
                $allGuest->total_order_spend = $totalSpent;
                if ($totalSpent > 0 && $gstOrder > 0) {
                    $newTotal[] = $totalSpent . ' / ' . $gstOrder . ' = ' . $totalSpent / $gstOrder . ' > ' . $avgSpend;
                    $avgSpend = ($totalSpent / $gstOrder) > $avgSpend ? $totalSpent / $gstOrder : $avgSpend;
                }
            }
            $avgSpend = ceil($avgSpend);

        return [
            'avgSpend'       => $avgSpend,
            'maxOrder'       => $maxOrder,
            'maxReservation' => $maxReservation,
            'totalSpent'     => $totalSpent,
            'newTotal'       => $newTotal,
        ];
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }
    
    /**
     * Remove the specified resource from storage.
     * @return Response
     */

    public function destroy($id)
    {
        $tag  =html_entity_decode(Input::get('tag'));
        DB::beginTransaction();
        try {
            $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
            $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
            $guestBookObj = User::where('restaurant_id', $parent_restaurant_id)->where('id', $id)->first();
            if(!$guestBookObj || !$guestBookObj->tags || !$tag){
                return \Response::json(['success' => false, 'message'=>'Tags detail not found'], 200);
            }
            $tag = (array)trim($tag);
            $guest_tags_db = ($guestBookObj->tags)?json_decode($guestBookObj->tags, true):array();
            $merged_array = array_values(array_diff($guest_tags_db, $tag));
            $guestBookObj->tags = ($merged_array) ? json_encode($merged_array) : NULL;
            $guestBookObj->save();
            DB::commit();
            return \Response::json(['success' => true, 'data' => $merged_array], 200);
        }catch(\Exception $e){
            return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
            DB::rollback();
        }
    }

    public function deleteItem($id)
    {
        $item  = html_entity_decode(Input::get('item'));
        DB::beginTransaction();
        try {
            $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
            $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;

            $guestBookObj = User::where('restaurant_id', $parent_restaurant_id)->where('id', $id)->first();
            if(!$guestBookObj || !$guestBookObj->items || !$item){
                return \Response::json(['success' => false, 'message'=>'Item detail not found'], 200);
            }
            $item = (array)trim($item);
            $guest_items_db = ($guestBookObj->items)?json_decode($guestBookObj->items, true):array();
            $merged_array = array_values(array_diff($guest_items_db, $item));
            $guestBookObj->items = ($merged_array) ? json_encode($merged_array) : NULL;
            $guestBookObj->save();
            DB::commit();
            return \Response::json(['success' => true, 'data' => $merged_array], 200);
        }catch(\Exception $e){
            return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
            DB::rollback();
        }
    }

    /**
     * Get a list of Reservations and Orders in latest first
     * @param $userId
     * @param $year
     * @return array
     */
    public function getReservationAndOrderForListing($userId, $year) {
        $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
        $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
         #Rg on 10-10-2018 to show orders and reservation of particular Restaurant
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        $guestResvOrders = User::where('users.restaurant_id', $parent_restaurant_id)->where('id', $userId)
            ->with([
                'reservations' => function ($query) use($year, $restArray) {
                    $query->select('user_id', 'id', 'restaurant_id', 'source', 'start_time', 'end_time', 'created_at')
                        ->with(['restaurant' => function( $query){
                                $query->select('restaurants.id', 'restaurants.restaurant_name');
                          }])
                        ->whereIn('restaurant_id', $restArray)
                        ->whereNotIn('status_id', [ReservationStatus::getCancelledStatusId(),ReservationStatus::getNoShowStatusId()])
                        ->whereYear('start_time', $year);
                    //$query->orderBy('start_time', 'desc');
                }, 'orders'    => function ($query) use($year, $restArray) {  // foreign key field is required for select
                    $query->select('user_id', 'restaurant_id', 'id','product_type','order_type', 'delivery_date', 'total_amount', 'created_at')->with(['restaurant' => function( $query){
                            $query->select('restaurants.id', 'restaurants.restaurant_name');
                      }])
                        ->whereIn('restaurant_id', $restArray)
                        ->whereNotIn('status',['pending'])
                        ->whereYear('created_at', $year);
                    if(Auth::user()->role != 'admin') {
                        $query->where('product_type', '<>', 'gift_card');
                    }
                    //$query->orderBy('start_time', 'desc');
                },
            ])
            ->first(); 
        $resvOrders = [];
        if ($guestResvOrders) {
            /*$guestResvOrders = $guestResvOrders->toArray();
            $guestResrv  = $guestResvOrders['reservations'];
            $guestOrders = $guestResvOrders['orders'];*/
            //dd($guestResvOrders->toArray());
            foreach($guestResvOrders->reservations as $resv) {
                if(!empty($resv->start_time)) {
                    $resvOrders[] = [
                        'id'            => $resv->id,
                        'source'        => ucfirst($resv->source),
                        'branch'        => $resv->restaurant->restaurant_name,
                        'amount'        => 'N/A',
                        'date'          => $resv->start_time,
                        'readable_date' => Carbon::createFromFormat('Y-m-d H:i:s', $resv->start_time)->format('d M'),
                        'type'          => 'reservation'
                    ];
                }
            }

            foreach($guestResvOrders->orders as $order) {
                if(!empty($order->delivery_date)) {
                    $resvOrders[] = [
                        'id'            => $order->id,
                        'source'        => ucfirst($order->order_type),
                        'product_type'  => $order->product_type,
                        'branch'        => $order->restaurant->restaurant_name,
                        'amount'        => $order->total_amount,
                        'date'          => $order->delivery_date,
                        'readable_date' => Carbon::createFromFormat('Y-m-d', $order->delivery_date)->format('d M'),
                        'type'          => 'order'
                    ];
                }
            }
            // sort by date ascending
            // SORT RESERVATIONS BY END TIME DESC
            usort($resvOrders, [$this, 'dateTimeCompare']);
        }

        return $resvOrders;
    }

    /**
     * Method to sort by date
     * @param $a
     * @param $b
     * @return false|int
     */
    public function dateTimeCompare($a, $b)
    {
        $time1 = strtotime($a['date']);
        $time2 = strtotime($b['date']);

        return $time2 - $time1;
    }
  
    public function getGuestDetail($id,Request $request)
    {

      $billingaddress ='';
      //  $tags=app('Modules\Reservation\Http\Controllers\FloorController')->gettagslist();

        $restaurant_detail = Restaurant::where('id', \Auth::user()->restaurant_id)->first();

        $is_reservation_allowed=$restaurant_detail->is_reservation_allowed;
        $parent_restaurant_id = $restaurant_detail->parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $restaurant_detail->parent_restaurant_id;

        #Rg on 10-10-2018 to show orders and reservation of particular Restaurant
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));

        $reservation_locations = Restaurant::whereIn('id', $restArray)->where('reservation', 1)->select(DB::raw('count(reservation) as reservation_count'))->get()->toArray();

        $guest = User::where('restaurant_id', $parent_restaurant_id)->where('id', $id)
                ->with(['reservations' => function($query) use ($restArray) {
                        $query->whereIn('restaurant_id', $restArray)->orderBy('start_time', 'desc')->with('status');
                }, 'orders' => function($query) use ($restArray) {                    
                        $query->whereIn('restaurant_id', $restArray)->whereNotIn('status',['pending'])->orderBy('created_at', 'desc');
                        if(Auth::user()->role != 'admin') {
                            $query->where('product_type', '<>', 'gift_card');
                        }
                },'address' => function($query)  {
                        $query->select('street','address1', 'address2','city', 'state','zipcode', 'user_id')->orderBy('created_at', 'desc')->limit(1);
                }
            ])->first();


//echo "<pre>";
//print_r( $guest); die;
        $userActivityDates = getFirstAndLastActivityDates($guest);
        $firstActYear      = ($userActivityDates['first_activity_date']) ? (int)Carbon::createFromFormat('Y-m-d H:i:s', $userActivityDates['first_activity_date'])->format('Y'):null;
        $lastActYear       = ($userActivityDates['last_activity_date']) ? (int)Carbon::createFromFormat('Y-m-d H:i:s', $userActivityDates['last_activity_date'])->format('Y'): null;
        $currentYear       = (int)date('Y');
        $actRangeYearArr = [];
        if(!is_null($firstActYear) && !is_null($lastActYear)){
            $actRangeYearArr   = range($firstActYear, $lastActYear);
        }
        if (count($actRangeYearArr)) {
            if($lastActYear !== $currentYear) {
                $actRangeYearArr[] = $currentYear;
            }
            rsort($actRangeYearArr);
        } else {
        $actRangeYearArr = [$currentYear];
        }

        // guest Reservation and Orders listing
        $resvOrders = $this->getReservationAndOrderForListing($id, $currentYear);
     $guest->tags = isset($guest->tags) && !empty($guest->tags) ? json_decode($guest->tags) :'';

        $guest->items = isset($guest->items) && !empty($guest->items) ? json_decode($guest->items):'';
        $all_category = config('reservation.guest_category');
        $total_resv_count = $total_walk_in_count = $total_cancelled_count = $total_orders = $total_no_show = 0 ;
        if($guest && $guest->reservations){
            #commnted by RG on 15-10-2018 as we are parsing on balde file
            $guest->dob = isset($guest->dob)&&!empty($guest->dob)?date('d-m-Y', strtotime($guest->dob)):$guest->dob;
            $guest->doa = isset($guest->doa)&&!empty($guest->doa)?date('d-m-Y', strtotime($guest->doa)):$guest->doa;
            foreach ($guest->reservations as $reservation){
                $reservation_start_date_time = ($reservation->start_time) ? $reservation->start_time : $reservation->created_at;
                $reservation_start_date_time = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_detail->id, $reservation_start_date_time);
                $reservation->reservation_day = $reservation_start_date_time->format('l');
                $reservation->reservation_date = $reservation_start_date_time->format('d M Y');
                $reservation->reservation_time= $reservation_start_date_time->format('h:i A');
                if($reservation->status->slug == config('reservation.status_slug.cancelled')){
                $total_cancelled_count++;
                }
                if($reservation->source == config('reservation.source.walk_in')){
                $total_walk_in_count++;
                }
                if($reservation->status->slug == config('reservation.status_slug.no_show')){
                $total_no_show++;
                }
                $total_resv_count++;
                $total_orders++;
            }
        }
        $total_order_spend     = 0;
        $total_orders = isset($guest->orders) && !empty($guest->orders)? $guest->orders->count() :0;
        
        $total_order_for_avr = 0;
        foreach ($guest->orders as $order) {
            if($order->status!='cancelled' &&  $order->status!='rejected' &&  $order->status!='pending'){
                $total_order_spend += $order->total_amount;
                $total_order_for_avr++;
            }
        }

        if($guest->category == 'Repeat') {
            $all_category = array_diff($all_category, ['New']);
        } elseif($guest->category == 'New') {
            $all_category = array_diff($all_category, ['Repeat']);
        } else {    // PE-2700, new requirement
            if($total_orders+$total_resv_count <= 1) {
                $all_category = array_diff($all_category, ['Repeat']);
            } else {
                $all_category = array_diff($all_category, ['New']);
            }
        }
        //echo "<pre>";print_r($guest->address['0']->address1);die;
        //  if ($request->ajax()) {
        //     return \Response::json([ 'data'=> compact('is_reservation_allowed','guest', 'all_category', 'total_resv_count', 'total_walk_in_count', 'total_cancelled_count', 'total_orders', 'total_no_show','tags', 'resvOrders','total_order_spend', 'reservation_locations')], 200);
        //   die;
        //  }           

        //\Log::info($guest);exit;
        /**************************************get restaurant_tags *****************************************************/

         $tags=$this->gettagslist();
         /******************user address**************************************************/
      if($guest->address!='')
       {
          $address= $guest->address;
          if(!empty($address[0]->address1)&&!empty($address[0]->city)&&!empty($address[0]->state)&&!empty($address[0]->zipcode)) {
              $billingaddress = $address[0]->address1 . ', ' . $address[0]->city . ', ' . $address[0]->state . ', ' . $address[0]->zipcode;
          }
          elseif($guest->billing_address!=''){
              $billingaddress =$guest->billing_address;
          }
          else{
              $order_address =  UserOrder::select('address','city','state','zipcode')->whereIn('restaurant_id', $restArray)->where('user_id',$id)->orderBy('id', 'DESC')->limit(1)->get()->toArray();
              if(count($order_address)) {
                  if ($order_address[0]['address'] != '') {
                      $billingaddress = $order_address[0]['address'] . ', ' . $order_address[0]['city'] . ', ' . $order_address[0]['state'] . ', ' . $order_address[0]['zipcode'];
                  }
              }
          }
       }
       elseif($guest->billing_address!=''){
           $billingaddress =$guest->billing_address;
       }
       else{
           $order_address =  UserOrder::select('address','city','state','zipcode')->whereIn('restaurant_id', $restArray)->where('user_id',$id)->orderBy('id', 'DESC')->limit(1)->get()->toArray();
            if(count($order_address)) {
                if ($order_address[0]['address'] != '') {
                    $billingaddress = $order_address[0]['address'] . ', ' . $order_address[0]['city'] . ', ' . $order_address[0]['state'] . ', ' . $order_address[0]['zipcode'];
                }
            }
     }
//print_r($order_address);

         /*****************count enquiry data**************************************************/
      //  $order_address =  UserOrder::select('address','city','state','zipcode')->whereIn('restaurant_id', $restArray)->where('user_id',$id)->orderBy('id', 'DESC')->limit(1)->get()->toArray();
 //echo 'enter--'; print_r($order_address );
        $countcontact =  StaticBlockResponse::where('enquiry_type','contact')->where('email',$guest->email)->get()->count();
        $countcatering =  StaticBlockResponse::where('enquiry_type','catering')->where('email',$guest->email)->get()->count();
         $countreservation =  StaticBlockResponse::where('enquiry_type','reservation')->where('email',$guest->email)->get()->count();
         $countevent =  StaticBlockResponse::where('enquiry_type','private_event')->where('email',$guest->email)->get()->count();
         $countcareer =  StaticBlockResponse::where('enquiry_type','career')->where('email',$guest->email)->get()->count();


        /******************************************************************************************************************/
      return view('reservation::guestbook.detail', compact('is_reservation_allowed','guest', 'all_category', 'total_resv_count', 'total_walk_in_count', 'total_cancelled_count', 'total_orders', 'total_no_show','tags', 'resvOrders', 'actRangeYearArr','total_order_spend', 'reservation_locations','countcontact','countcatering','countreservation','countevent','countcareer','billingaddress','total_order_for_avr'));

    }

    /**
     * Ajax call route, for Guestbook year based reservations & orders listing
     * @param $userId
     * @param $year
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reservationOrderListing($userId, $year)
    {
        $reservations = $this->getReservationAndOrderForListing($userId,$year);

        return view('reservation::guestbook.partial.reservation-list', compact('reservations'));
    }

    public function updateTags(Request $request)
    {
        $cust_id = $request->get('cust_id');
        $tags = $request->get('tags');
        if($tags){
            if(!is_array($tags)){
                $tags = (array)trim($tags);
            }
            DB::beginTransaction();
            try {
            $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
            $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
            $guest = User::where('restaurant_id', $parent_restaurant_id)->where('id', $cust_id)->first();
            $guest_tags_db = ($guest->tags)?array_filter(json_decode($guest->tags)):array();
            $merged_array = array_merge($guest_tags_db, $tags);
            $merged_array = array_unique($merged_array);
            $guest->tags = ($merged_array) ? json_encode($merged_array) : NULL;
            $guest->save();
            DB::commit();
            return \Response::json(['success' => true, 'data'=> $merged_array, 'message'=>'Tags added successfully'], 200);
            }catch(\Exception $e){
            return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
            DB::rollback();
            }
        }
    }

    public function updateItems(Request $request)
    {
        $cust_id = $request->get('cust_id');
        $items = $request->get('items');
        if($items){
            if(!is_array($items)){
                $items = (array)trim($items);
            }
            DB::beginTransaction();
            try {
            $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
            $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
            $guest = User::where('restaurant_id', $parent_restaurant_id)->where('id', $cust_id)->first();
            $guest_items_db = ($guest->items)?json_decode($guest->items):array();
            $merged_array = array_merge($guest_items_db, $items);
            $merged_array = array_unique($merged_array);
            $guest->items = ($merged_array) ? json_encode($merged_array) : NULL;
            $guest->save();
            DB::commit();
            return \Response::json(['success' => true, 'data'=> $merged_array, 'message'=>'
            Items added successfully'], 200);
            }catch(\Exception $e){
            return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
            DB::rollback();
            }
        }
    }

    public function updateGuestDetail(Request $request)
    {
        $cust_id = $request->get('id');
        $input = $request->all();
        DB::beginTransaction();
        //try {
        $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
        $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;

        $guest  = User::where('restaurant_id', $parent_restaurant_id)->where('id',$cust_id)->first();
        if(isset($input['birthday'])&&!empty($input['birthday'])){
            $guest->dob = Carbon::createFromFormat('d-m-Y', $input['birthday'])->format('Y-m-d');
        }

        if(isset($input['anniversary'])&&!empty($input['anniversary'])){
            $guest->doa = Carbon::createFromFormat('d-m-Y', $input['anniversary'])->format('Y-m-d');
        }
        if((isset($input['fl_name'])&&!empty($input['fl_name']))) {
            $first_name = $input['fl_name'];
            $last_name = '';
            if(strpos($input['fl_name'], ' ') !== false){
                $input['fl_name'] = explode(' ', $input['fl_name'],2);
                $first_name = ($input['fl_name'][0])?$input['fl_name'][0]:$input['fl_name'];
                $last_name = ($input['fl_name'][1])?$input['fl_name'][1]:'';
            }
            $guest->fname = $first_name;
            $guest->lname = $last_name;
        }

        if((isset($input['tag'])&&!empty($input['tag']))) {
            if(!empty($input['tag'])) {
                $guest->tags = $input['tag'];
            }
        }
        if(isset($input['email'])&&!empty($input['email'])){
            $guest->email = $input['email'];
        }
        if(isset($input['mobile'])&&!empty($input['mobile'])){
            $guest->mobile = $input['mobile'];
        }

        if($request->has('description')){
            $guest->description = $input['description'];
        }
        if(isset($input['category'])&&!empty($input['category'])){
            $guest->category = $input['category'];
        }

        if(isset($input['address'])&&!empty($input['address'])){
            $guest->shipping_address = $input['address'];
            //Need to change this with new table
        }
        $guest->save();
        DB::commit();
        return \Response::json(['success' => true, 'message'=>'Details updated successfully'], 200);
        /*}catch(\Exception $e){
        return \Response::json(['success' => false, 'message' => $e->getMessage()], 500);
        DB::rollback();
        }*/
    }

    public function getGuestDetails($id)
    {
       $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
       $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
       $guest = User::where('restaurant_id', $parent_restaurant_id)->where('id', $id)->first();
       $guest->dob = isset($guest->dob)&&!empty($guest->dob)?date('m-d-Y', strtotime($guest->dob)):$guest->dob;
       $guest->doa = isset($guest->doa)&&!empty($guest->doa)?date('m-d-Y', strtotime($guest->doa)):$guest->doa;
       return $guest; 
    }

    public function guestSearch(Request $request)
    {
        $keyword = $request->get('term');
        $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
        $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
        $guests = User::select(['id','fname','mobile'])
        ->where('restaurant_id', $parent_restaurant_id);

        /*->with(['reservations' => function($query) {
            $query->orderBy('start_time', 'desc');
            }])*/
        if($keyword){
           $keyword = trim($keyword);
           $guests = $guests->where(function ($query) use ($keyword){
                      $query->where('fname', 'like', '%' . $keyword. '%');
                      //->orWhere(DB::raw("CONCAT( fname,  ' ', lname )"), 'like', '%' . $keyword. '%');
            });
            $guests = $guests->get();
             $guestarray=[];
            if(count($guests)){
              
              foreach($guests as $guest){
                   $data=[];
                    $data['id']=$guest->id;
                   $data['label']=$guest->fname.' ('.$guest->mobile.')';
                   $data['value']=$guest->fname;
                  $guestarray[]=$data;     
              }
            } 
         return \Response::json($guestarray, 200);
        }
       
    }

    /* Rahul gupta Export Guest Users */
    
    public function export($guests)
    { 
      
       // echo "<pre>";print_r($guests->toArray());die;
        $filename = 'Report_'.time().'.csv';
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment;filename="'.$filename.'";');
        $file = fopen('php://output', 'w');

        $columns = array('First Name', 'Last Name', 'Email', 'Mobile', 'Orders', 'Reservation', 'Revenue', 'First Activity', 'Last Activity');
        fputcsv($file, $columns);

        foreach($guests as $guest) { 
            $first_last_activity_arr = getFirstAndLastActivityDates($guest);

            $first = ($first_last_activity_arr['first_activity_date']) ? \Carbon\Carbon::parse($first_last_activity_arr['first_activity_date'])->format('m-d-Y') : 'N/A';
            $last = ($first_last_activity_arr['last_activity_date']) ? \Carbon\Carbon::parse($first_last_activity_arr['last_activity_date'])->format('m-d-Y') : 'N/A' ;

            fputcsv($file, array($guest->fname, $guest->lname, $guest->email, $guest->mobile, $guest->orders->count(), $guest->reservations->count(), $guest->orders->sum('total_amount'),  $first,  $last ));
        }

        fclose($file);
       
        exit;
    }
    public function gettagslist()
    {

        $tags_data=[];

        $tags=Tags::get(['id','name']);
        if($tags){
            foreach ($tags as $value) {
                $tags_data[]=$value->name;
            }
        }

        return $tags_data;


    }


}
