<?php

namespace Modules\Reservation\Http\Controllers;

use App\Helpers\CommonFunctions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Modules\Reservation\Entities\CustomWorkingHours;
use Modules\Reservation\Entities\ReservationStatus;
use Modules\Reservation\Entities\Table;
use Modules\Reservation\Entities\TableType;
use Modules\Reservation\Entities\WorkingHours;
use Modules\Reservation\Entities\Floor;
use DB;
use Modules\Reservation\Http\Requests\WeekWorkingHours;
use Modules\Reservation\Entities\Reservation;
use Carbon\Carbon;

class WorkingHoursController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $calendar_days = config('reservation.calendar_days');
        $table_type = TableType::where('restaurant_id', \Auth::user()->restaurant_id)->orderBy('max_seat', 'ASC')->get();
        $floors = Floor::select(['id', 'type', 'name'])->where('completed_step', 2)->where('restaurant_id', \Auth::user()->restaurant_id)->get();
        return view('reservation::working_hours.index', compact('table_type', 'calendar_days', 'floors'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('workinghours::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(WeekWorkingHours $request)
    {
        try {
            $slot_days_not_override = [];
            $type = $request->get('type');
            $inputData = $this->getModifiedInputArrayNew($request);
            if ($inputData) {
                DB::beginTransaction();
                if ($type == config('reservation.working_hour_type.day')) {
                    if (isset($inputData['calendar_day']) && !empty($inputData['calendar_day'])) {
                        WorkingHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => $inputData['calendar_day']], $inputData);
                    }
                } else {
                    $schd_id = $request->has('schd_id') ? $request->get('schd_id') : 0;
                    $action = ($schd_id == 0) ? 'add' : 'update';
                    if ($this->subRangeExistInRange($inputData['calendar_date'], $action)) {
                        return \Response::json(['success' => false, 'errors' => ['Slots are already exist in this date range, please select different range']], 422);
                    }

                    if ($this->checkCurrentReservations($inputData['calendar_date'], $type) === false) {
                        return \Response::json(['success' => false, 'errors' => ['Custom setting cannot be created as there are ongoing reservations in this date range.']], 422);
                    }
                    if ($this->checkUpcomingReservations($inputData['calendar_date'], $type) === false) {
                        return \Response::json(['success' => false, 'errors' => ['Custom setting cannot be created as there are upcoming reservations in this date range.']], 422);
                    }
                    CustomWorkingHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'id' => $schd_id], $inputData);
                }
                if ($request->has('repeat_day') && $type == config('reservation.working_hour_type.day')) {
                    $repeat_arr = array_keys($request->get('repeat_day'));
                    $input_calendar_day = $inputData['calendar_day'];
                    foreach ($repeat_arr as $day) {
                        if ($input_calendar_day != $day) {
                            $calendar_days = config('reservation.calendar_days');
                            $request->merge(['calendar_day' => array_search($day, $calendar_days)]);
                            $request->merge(['repeat' => true]);
                            $inputData = $this->getModifiedInputArrayNew($request);
                            if ($inputData && (isset($inputData['calendar_day']) && !empty($inputData['calendar_day']))) {
                                if (isset($inputData['slot_days_not_override']) && !empty($inputData['slot_days_not_override'])) {
                                    array_push($slot_days_not_override, ucfirst($inputData['slot_days_not_override']));
                                }
                                WorkingHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => $day], $inputData);
                            }
                        }
                    }
                }
                DB::commit();
                if (count($slot_days_not_override) > 0) {
                    $message = 'Existing shift setting on ' . implode(',', $slot_days_not_override) . ' cannot be overridden as there are upcoming reservations on that day';
                } else {
                    $message = 'Hour setting has been created.';
                }
                return \Response::json(['success' => true, 'message' => $message], 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('workinghours::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('workinghours::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(WeekWorkingHours $request)
    {
        $type = $request->get('type');
        $schd_id = $request->get('schd_id');
        $inputData = $this->getModifiedInputArrayNew($request);
        if(isset($inputData['errors']) && !empty($inputData['errors'])){
            return \Response::json(['errors' => [$inputData['errors']]], 422);
        }
        $slot_days_not_override = [];
        if ($inputData) {
            DB::beginTransaction();
            try {
                if ($type == config('reservation.working_hour_type.day')) {
                    WorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $inputData['calendar_day'])->update($inputData);
                    if ($request->has('repeat_day')) {
                        $repeat_arr = array_keys($request->get('repeat_day'));
                        $input_calendar_day = $inputData['calendar_day'];
                        foreach ($repeat_arr as $day) {
                            if ($input_calendar_day != $day) {
                                $calendar_days = config('reservation.calendar_days');
                                $request->merge(['calendar_day' => array_search($day, $calendar_days)]);
                                $request->merge(['repeat' => true]);
                                $inputData = $this->getModifiedInputArrayNew($request);
                                if ($inputData && (isset($inputData['calendar_day']) && !empty($inputData['calendar_day']))) {
                                    if (isset($inputData['slot_days_not_override']) && !empty($inputData['slot_days_not_override'])) {
                                        array_push($slot_days_not_override, ucfirst($inputData['slot_days_not_override']));
                                    }
                                    WorkingHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => $day], $inputData);
                                }
                            }
                        }
                    }
                } else {
                    CustomWorkingHours::find($schd_id)->update($inputData);
                }
                DB::commit();

                if (count($slot_days_not_override) > 0) {
                    $message = 'Existing shift setting on ' . implode(',', $slot_days_not_override) . ' cannot be overridden as there are upcoming reservations on that day';
                } else {
                    $message = 'Hour setting has been updated';
                }
                return \Response::json(['success' => true, 'message' => $message], 200);
            } catch (\Exception $e) {
                DB::rollback();
                response()->json(['errors' => [$e->getMessage()]], 500)->send();
            }
        }
        return \Response::json(['errors' => ['Schedule setting could not be updated']], 422)->send();
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */

    public function destroy($id)
    {
        $type = Input::get('type');
        $slot_id = Input::get('slot_id');
        DB::beginTransaction();
        try {
            if ($type == config('reservation.working_hour_type.day')) {
                $calendar_days = config('reservation.calendar_days');
                $calendar_day = $calendar_days[$id];
                $scheduleHours = WorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $calendar_day)->first();
            } else {
                $scheduleHours = CustomWorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('id', $id)->first();
            }
            if ($scheduleHours) {
                if ($type == config('reservation.working_hour_type.day')) {
                    $dayOrDate = $scheduleHours->calendar_day;
                } else {
                    $dayOrDate = $scheduleHours->calendar_date;
                }
                if (empty($scheduleHours->slot_detail) || $scheduleHours->slot_detail == NULL) {
                    $scheduleHours->delete($id);
                    DB::commit();
                    return \Response::json(['success' => true], 200);
                }
                if(empty($slot_id) || $slot_id <= 0){
                    $time_range = NULL;
                }else{
                    $slot_detail_db = json_decode($scheduleHours->slot_detail, true);
                    $exist = array_search($slot_id, array_column($slot_detail_db, 'slot_id'));
                    $time_range = $slot_detail_db[$exist]['open_time'] . '-' . $slot_detail_db[$exist]['close_time'];
                }
                if($this->isCurrentTimingSlot($time_range, $dayOrDate, $type)){
                    response()->json(['errors' => ["Ongoing shift cannot be deleted."]], 422)->send();
                    die();
                }

                if ($this->checkCurrentReservations($dayOrDate, $type, $time_range) === false) {
                    response()->json(['errors' => ["The slot cannot be deleted as it has ongoing reservations."]], 422)->send();
                    die();
                }
                if ($this->checkUpcomingReservations($dayOrDate, $type, $time_range) === false) {
                    response()->json(['errors' => ["The slot cannot be deleted as it has upcoming reservations."]], 422)->send();
                    die();
                }

                if(empty($slot_id) || $slot_id <= 0){
                    $scheduleHours->delete($id);
                    DB::commit();
                    return \Response::json(['success' => true], 200);
                }
                unset($slot_detail_db[$exist]);
                $slot_detail_db = array_values($slot_detail_db); // 'reindex' array
                $slot_detail_db = !empty($slot_detail_db) ? $slot_detail_db : 0;
                $scheduleHours->slot_detail = json_encode($slot_detail_db);
                $scheduleHours->save();
                DB::commit();
                return \Response::json(['success' => true], 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    public function getWorkingHoursSchedule()
    {
        $eventData = WorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->get()->where('calendar_day', '!=', '');
        $events = $date_range = [];
        $inc = 0;
        $calendar_days = config('reservation.calendar_days');
        $dayoff = WorkingHours::where('restaurant_id', '=', \Auth::user()->restaurant_id)
            ->get(['is_dayoff', 'calendar_day'])
            ->groupBy('calendar_day')
            ->map(function ($day) {
                return $day->pluck('is_dayoff')->take(1);
            })->toArray();
        foreach ($calendar_days as $day_num => $day) {
            $date_range[$day_num]['id'] = $day_num;
            $date_range[$day_num]['title'] = ucfirst($day);
            $date_range[$day_num]['dayoff'] = isset($dayoff[$day]) ? $dayoff[$day][0] : 0;
        }
        $calendar_days = array_flip($calendar_days);
        //\Log::info($date_range);
        $slot_name_arr = array();
        foreach ($eventData as $event) {
            if ($event->is_dayoff) {
                $events[$inc]['id'] = $inc;
                $events[$inc]['start'] = date("Y-m-d 00:00:00");
                $events[$inc]['end'] = date("Y-m-d 24:00:00");
                $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                $inc++;
            } elseif (empty($event->slot_detail)) {
                $events[$inc]['id'] = $inc;
                \Log::info($event->calendar_day);
                $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                $inc++;
            } else {
                $slot_detail = json_decode($event->slot_detail, true);
                if ($slot_detail) {
                    foreach ($slot_detail as $slot) {
                        $events[$inc]['id'] = $inc;
                        $events[$inc]['slot_id'] = $slot['slot_id'];
                        $events[$inc]['resourceId'] = $calendar_days[$event->calendar_day];
                        $events[$inc]['start'] = date("Y-m-d H:i:s", strtotime($slot['open_time']));
                        $events[$inc]['end'] = date("Y-m-d H:i:s", strtotime($slot['close_time']));
                        $events[$inc]['title'] = '<div class="coverDiv" title="Covers"><i class="fa fa-users" aria-hidden="true"></i> <span>' . $slot['available_cover_count'] . '</span></div><div class="tableDiv" title="Tables"><span class="icon-reservation"></span> <span>' . $slot['available_table_count'] . '</span></div>';
                        $date_range[$calendar_days[$event->calendar_day]]['slot_name'][] = $slot['slot_name'];
                        $inc++;
                    }
                }
            }
        }
        $events = ($events) ?? '';
        //\Log::info($date_range);
        return json_encode(array("events" => $events, 'date_range' => $date_range));
    }

    public function getCustomWorkingHoursSchedule()
    {
        $eventData = CustomWorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->get();
        $events = $date_range = [];
        $inc = 0;
        foreach ($eventData as $key => $event) {
            $slot_detail = json_decode($event->slot_detail, true);
            $resource_id = $event->id;
            $calendar_dates = json_decode($event->calendar_date, true);
            $custom_name = ($event->custom_name) ? $event->custom_name : '';
            $title = date('dS M', strtotime($calendar_dates['start_date'])) . '-' . date('dS M', strtotime($calendar_dates['end_date'])) . " " . $custom_name;
            $date_range[$key]['title'] = $title;
            $date_range[$key]['id'] = $resource_id;
            $date_range[$key]['dayoff'] = $event->is_dayoff;
            if ($event->is_dayoff) {
                $events[$inc]['id'] = $inc;
                $events[$inc]['start'] = date("Y-m-d 00:00:00");
                $events[$inc]['end'] = date("Y-m-d 24:00:00");
                $events[$inc]['resourceId'] = $resource_id;
                $inc++;
            } elseif (empty($event->slot_detail)) {
                $events[$inc]['id'] = $inc;
                $events[$inc]['resourceId'] = $resource_id;
                $inc++;
            } else {
                foreach ($slot_detail as $slot) {
                    $events[$inc]['id'] = $inc;
                    $events[$inc]['resourceId'] = $resource_id;
                    $events[$inc]['slot_id'] = $slot['slot_id'];
                    $events[$inc]['start'] = date("Y-m-d H:i:s", strtotime($slot['open_time']));
                    $events[$inc]['end'] = date("Y-m-d H:i:s", strtotime($slot['close_time']));
                    $events[$inc]['title'] = '<div class="coverDiv" title="Covers"><i class="fa fa-users" aria-hidden="true"></i> <span>' . $slot['available_cover_count'] . '</span></div><div class="tableDiv" title="Tables"><span class="icon-reservation"></span> <span>' . $slot['available_table_count'] . '</span></div>';
                    $date_range[$key]['slot_name'][] = $slot['slot_name'];
                    $inc++;
                }
            }
        }
        $events = ($events) ?? '';
        $date_range = ($date_range) ? array_values($date_range) : '';
        //\Log::info($date_range);
        return json_encode(array("events" => $events, 'date_range' => $date_range));
    }

    public function dayOffToggle()
    {
        $status = Input::get('status');
        $resourceId = Input::get('resourceId');
        $resourceId = ($resourceId) ? str_replace('dayoff_', '', $resourceId) : 0;
        $type = Input::get('type');
        $custom_name = Input::get('custom_name');
        $status = ($status == 'true') ? 0 : 1;
        $inputData['restaurant_id'] = \Auth::user()->restaurant_id;
        $inputData['is_dayoff'] = $status;
        $inputData['slot_detail'] = NULL;
        $inputData['custom_name'] = $custom_name;
        DB::beginTransaction();
        try {
            if ($type == config('reservation.working_hour_type.day')) {
                $working_schedule = WorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', strtolower($resourceId))->first();
                if ($working_schedule && empty($working_schedule->slot_detail)) {
                    WorkingHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => strtolower($resourceId)], $inputData);
                } else {
                    if ($this->checkCurrentReservations(strtolower($resourceId), $type) === false) {
                        response()->json(['errors' => ["The slot cannot be deleted as it has ongoing reservations."]], 422)->send();
                        die();
                    }
                    if ($this->checkUpcomingReservations(strtolower($resourceId), $type) === false) {
                        response()->json(['errors' => ["The slot cannot be deleted as it has upcoming reservations."]], 422)->send();
                        die();
                    }
                    WorkingHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'calendar_day' => strtolower($resourceId)], $inputData);
                }
            } else {
                if ($resourceId) {
                    $inputData['calendar_date'] = $this->getCustomStartEndDate($resourceId);
                } else {
                    $start_date = Carbon::createFromFormat('m-d-Y', Input::get('start_date'))->format('Y-m-d');
                    $end_date = Carbon::createFromFormat('m-d-Y', Input::get('end_date'))->format('Y-m-d');

                    $calendar_dates = ["start_date" => $start_date, "end_date" => $end_date];
                    $inputData['calendar_date'] = $calendar_dates ? json_encode($calendar_dates) : $calendar_dates;
                    if ($this->subRangeExistInRange($inputData['calendar_date'], 'add')) {
                        return \Response::json(['success' => false, 'errors' => ['Slots are already exist in this date range, please select different range']], 422);

                    }
                }
                $working_schedule = CustomWorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_date', 'LIKE', $inputData['calendar_date'])->first();
                //\Log::info($working_schedule);
                if ($working_schedule && empty($working_schedule->slot_detail)) {
                    CustomWorkingHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'id' => $resourceId], $inputData);
                } else {
                    if ($this->checkCurrentReservations($inputData['calendar_date'], $type) === false) {
                        if ($resourceId) {
                            response()->json(['errors' => ["The slot cannot be deleted as it has ongoing reservations."]], 422)->send();
                        }else{
                            response()->json(['errors' => ["This setting cannot created as it has ongoing reservations."]], 422)->send();
                        }
                        die();
                    }
                    if ($this->checkUpcomingReservations($inputData['calendar_date'], $type) === false) {
                        if ($resourceId) {
                            response()->json(['errors' => ["The slot cannot be deleted as it has upcoming reservations."]], 422)->send();
                        }else{
                            response()->json(['errors' => ["This setting cannot created as it has upcoming reservations."]], 422)->send();
                        }
                        die();
                    }
                    CustomWorkingHours::updateOrCreate(['restaurant_id' => \Auth::user()->restaurant_id, 'id' => $resourceId], $inputData);
                }
            }
            DB::commit();
            return \Response::json(['success' => true], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    private function unset_old_slots($slot_detail_db, $slot_id_array)
    {

        foreach ($slot_id_array as $slotId) {

            $exist = array_search($slotId, array_column($slot_detail_db, 'slot_id'));

            unset($slot_detail_db[$exist]);
        }
        return array_values($slot_detail_db);
    }

    private function getModifiedInputArrayNew($request)
    {
        $inputData = array();
        $type = $request->get('type');
        $available_table_count = NULL;//
        $available_tables = array();//
        $pacing = array();//
        $calendar_day = $request->get('calendar_day');
        if ($type == config('reservation.working_hour_type.day')) {
            $calendar_days = config('reservation.calendar_days');
            $calendar_day = $calendar_days[$calendar_day];
            $inputData['calendar_day'] = $calendar_day;
            $working_schedule = WorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $inputData['calendar_day'])->first();
            $dayorDate = $calendar_day;
        } else {
            if ($request->has('schd_id') && !empty($request->get('schd_id'))) {
                $inputData['calendar_date'] = $this->getCustomStartEndDate($request->get('schd_id'));
            } else {

                $start_date = $request->has('start_date') ? Carbon::createFromFormat('m-d-Y', $request->get('start_date'))->format('Y-m-d') : date('Y-m-d');

                $end_date = $request->has('end_date') ? Carbon::createFromFormat('m-d-Y', $request->get('end_date'))->format('Y-m-d') : date('Y-m-d');

                $calendar_dates = ["end_date" => $end_date, "start_date" => $start_date];
                $inputData['calendar_date'] = $calendar_dates ? json_encode($calendar_dates) : $calendar_dates;
                $inputData['custom_name'] = $request->get('custom_name');
            }
            $dayorDate = $inputData['calendar_date'];

            $working_schedule = CustomWorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_date', 'LIKE', $inputData['calendar_date'])->first();

        }

        $slot_name = $request->get('slot_name');
        $slot_detail['slot_name'] = $slot_name;
        $slot_detail["open_time"] = $request->get('open_time');
        $slot_detail["close_time"] = $request->get('close_time');
        $slot_detail['floors'] = json_encode($request->get('floors'));
        $slot_detail['max_covers_limit'] = $request->get('max_covers_limit');
        $slot_detail['equal_pacing'] = $request->get('equal_pacing') ? 1 : 0;

        $available_cover_count = $request->get('available_cover_count');//

        $number_of_tables = $request->get('number_of_tables');
        $table_cover_count = $request->get('table_cover_count');

        $t_count = 0;
        $pacing = [];
        if($number_of_tables>0){
            foreach ($number_of_tables as $key => $count) {
                $available_tables[$t_count]['table_type'] = $key;
                $available_tables[$t_count]['table_cover_count'] = $table_cover_count[$key];
                $available_tables[$t_count]['table_count'] = !empty($count) && ($count != 0) ? (int)$count : 0;
                $t_count++;
            }
            if ($slot_detail['equal_pacing']) {
                $equal_pacing_reservations = $request->get('equal_pacing_reservations');
                $equal_pacing_covers = $request->get('equal_pacing_covers');
                $allsolts = getSlotWithIntevals($slot_detail["open_time"], $slot_detail["close_time"], config('reservation.slot_interval_time'));
                $pace_count = 0;
                foreach ($allsolts as $key => $value) {
                    $pacing[$pace_count]['time'] = $value;
                    $pacing[$pace_count]['reservations'] = $equal_pacing_reservations;
                    $pacing[$pace_count]['covers'] = $equal_pacing_covers;
                    $pace_count++;
                }
            } else {
                $pacing_reservation = $request->get('pacing_reservation');
                $pacing_cover = $request->get('pacing_cover');
                $pace_count = 0;
                foreach ($pacing_reservation as $key => $count) {
                    $pacing[$pace_count]['time'] = $key;
                    $pacing[$pace_count]['reservations'] = $count;
                    $pacing[$pace_count]['covers'] = $pacing_cover[$key];
                    $pace_count++;
                }
            }
        }


        $calendar_dates = NULL;
        $isDayOff = $request->has('is_dayoff') ? 1 : 0;
        $inputData['restaurant_id'] = \Auth::user()->restaurant_id;
        $inputData['is_dayoff'] = $isDayOff;

        $slot_detail["available_cover_count"] = !empty($table_cover_count) ? array_sum($table_cover_count) : 0;
        $slot_detail['available_table_count'] = !empty($number_of_tables) ? array_sum($number_of_tables) : 0;
        $slot_detail['available_tables'] = json_encode($available_tables);
        $slot_detail['pacing'] = !empty($pacing) ? json_encode($pacing): null;
        $slot_detail['party_size'] = json_encode($request->get('party_size'));


        if ($working_schedule && !empty($working_schedule->slot_detail)) {
            //\Log::info('hee');
            $slot_detail_db = json_decode($working_schedule->slot_detail, true);
            // for update existing slots

            //\Log::info('ALL REQUEST' . json_encode($request->all()));
            $is_exist = false;
            if ($request->has('repeat')) {
                $slot_check = $this->isTimeSlotSettingExistAlready($slot_detail_db, $request->get('open_time'), $request->get('close_time'));
                //\Log::info([$slot_detail_db, $request->get('open_time'), $request->get('close_time')]);
                //\Log::info('Repeating' . json_encode($slot_check));
                if ($slot_check != false && is_array($slot_check)) {

                    $is_exist = true;
                    $slot_counter = 1;
                    //\Log::info($slot_check);
                    //\Log::info($working_schedule->calendar_day . "****");
                    foreach ($slot_check as $slotId) {
                        $exist = array_search($slotId, array_column($slot_detail_db, 'slot_id'));
                        //\Log::info($working_schedule->calendar_day."****");
                        //\Log::info([$slotId, array_column($slot_detail_db, 'slot_id'), $exist, $working_schedule->calendar_day, $slot_detail_db]);

                        $time_range = $slot_detail_db[$exist]['open_time'] . '-' . $slot_detail_db[$exist]['close_time'];

                        $dayOrDate = $working_schedule->calendar_day;
                        if ($this->checkCurrentReservations($dayOrDate, $type, $time_range) === true
                            && $this->checkUpcomingReservations($dayOrDate, $type, $time_range) === true && $this->isCurrentTimingSlot($time_range, $dayOrDate, $type)===false) {
                            if ($slot_counter == 1) {

                                $slot_detail_db = $this->unset_old_slots($slot_detail_db, $slot_check);

                            }

                            //  $slotId = $this->getExistingSlotIdForDayRepeat($slot_detail_db, $request->get('open_time'), $request->get('close_time'));
                            // if($slotId>0){
                            //$slot_detail["slot_id"] = $slotId;
                            // $slot_detail_db[$exist] = $slot_detail;

                            // }else{
                            if (count($slot_detail_db)) {
                                $slot_detail["slot_id"] = max(array_column($slot_detail_db, 'slot_id')) + 1;
                                array_push($slot_detail_db, $slot_detail);
                            } else {
                                $slot_detail["slot_id"] = 1;
                                $slot_detail_db[$exist] = $slot_detail;

                            }

                            // }

                            //\Log::info('repeating_no_reservation');

                        } else {
                            $inputData['slot_days_not_override'] = $inputData['calendar_day'];
                        }
                        $slot_counter;

                    }


                } else {
                    if (count($slot_detail_db)) {
                        $slot_detail["slot_id"] = max(array_column($slot_detail_db, 'slot_id')) + 1;
                        array_push($slot_detail_db, $slot_detail);
                    } else {
                        $slot_detail["slot_id"] = 1;
                        array_push($slot_detail_db, $slot_detail);

                    }
                }

            } else {

                if (($request->has('slot_id') && !empty($request->get('slot_id')))) {

                    $slot_id = $request->get('slot_id');
                    $exist = array_search($slot_id, array_column($slot_detail_db, 'slot_id'));
                    $slot_detail["slot_id"] = $slot_id;
                    $existing_slot_db = $slot_detail_db[$exist];

                    $slot_detail_db[$exist] = $slot_detail;
                    //\Log::info($existing_slot_db);
                    //\Log::info([$dayorDate, $type, $slot_detail_db["open_time"].'-'.$slot_detail_db["close_time"]]);
                    if($this->isCurrentTimingSlot($existing_slot_db["open_time"].'-'.$existing_slot_db["close_time"], $dayorDate, $type)){
                        $inputData['errors'] = "Ongoing shift cannot be edited.";
                        return $inputData;
                    }
                    $current_reservation = $this->checkCurrentReservations($dayorDate, $type, $existing_slot_db["open_time"].'-'.$existing_slot_db["close_time"], 'all');
                    if (is_array($current_reservation) || is_object($current_reservation)) {
                        if(count($current_reservation) > 0){
                            if ($this->isAnyChangeImpactOnReservation($existing_slot_db, $slot_detail)) {
                                $inputData['errors'] = "The shift cannot be edited as it has ongoing reservations.";
                            } else {
                                $this->updateSlotTimeInReservation($current_reservation, $slot_detail["open_time"] . '-' . $slot_detail["close_time"]);
                            }
                        }
                    }else{
                        $inputData['errors'] = "The shift cannot be edited as it has ongoing reservations.";
                        return $inputData;
                    }
                    $upcoming_reservation = $this->checkUpcomingReservations($dayorDate, $type, $existing_slot_db["open_time"].'-'.$existing_slot_db["close_time"], 'all');
                    \Log::info($upcoming_reservation);
                    if ($upcoming_reservation && count($upcoming_reservation)>0){
                        \Log::info($existing_slot_db, $slot_detail);
                        if($this->isAnyChangeImpactOnReservation($existing_slot_db, $slot_detail)){
                            $inputData['errors'] = "The shift cannot be edited as it has upcoming reservations.";
                        }else{
                            $this->updateSlotTimeInReservation($upcoming_reservation, $slot_detail["open_time"].'-'.$slot_detail["close_time"]);
                        }
                    }
                } else {
                    //for insert multiple slots
                    \Log::info('if slotid less than zero');

                    $slot_detail["slot_id"] = max(array_column($slot_detail_db, 'slot_id')) + 1;
                    array_push($slot_detail_db, $slot_detail);
                }


            }
            $inputData['slot_detail'] = json_encode($slot_detail_db);

        } else {
            //for insert first time
            $slot_detail["slot_id"] = 1;
            $inputData['slot_detail'] = json_encode([$slot_detail]);
        }
        return $inputData;
    }

    public function getNewTableTypes($floors)
    {
        $data = [];
        $floors = json_decode($floors);
        $tables = Table::where('restaurant_id', \Auth::user()->restaurant_id)->whereIn('floor_id', $floors)->groupBy('max')->select('max as tabletype', DB::raw('count(*) as total'))->get();
        if ($tables) {
            $data = $tables->toArray();
        }

        return $data;
    }

    public function workingSlot(Request $request, $type = 'Add')
    {

        if ($type == 'add') {
            $floors = $request->get('floors');

            $tabletypes = $this->getNewTableTypes($floors);
            if (count($tabletypes)) {
                return \Response::json(['data' => $tabletypes], 200);

            } else {
                return \Response::json(['errors' => ["No Table type found"]], 422);
                die;
            }

        }

    }

    public function getSlotDetail($schd_id)
    {
        $type = Input::get('type');
        $slot_id = Input::get('slot_id');
        $slot_detail = array();
        if ($type == config('reservation.working_hour_type.day')) {
            $calendar_days = config('reservation.calendar_days');
            $calendar_day = $calendar_days[$schd_id];
            $inputData['calendar_day'] = $calendar_day;
            $slotData = WorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $calendar_day)->first();
        } else {
            $slotData = CustomWorkingHours::find($schd_id);
        }
        if ($slotData) {
            $slot_details = json_decode($slotData->slot_detail, true);
            if ($slot_details) {
                $exist = array_search($slot_id, array_column($slot_details, 'slot_id'));
                $slot_detail = $slot_details[$exist];
                $slot_detail['schd_id'] = $slotData->id;
                $slot_detail['slot_id'] = $slot_detail['slot_id'];
                $slot_detail['slot_name'] = $slot_detail['slot_name'];
                $slot_detail['available_cover_count'] = $slot_detail['available_cover_count'];
                $slot_detail['available_tables'] = json_decode($slot_detail['available_tables']);
                $slot_detail['is_dayoff'] = $slotData->is_dayoff;
            }
        }


        return json_encode(array("slot_detail" => $slot_detail));
    }

    public function getCustomStartEndDate($slot_id)
    {
        $custom = CustomWorkingHours::find($slot_id);
        $custom_dates = ($custom) ? $custom->calendar_date : NULL;
        return $custom_dates;
    }

    public function subRangeExistInRange($date_range, $action)
    {
        $date_range_arr = json_decode($date_range, true);
        $subRangeCount = CustomWorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where(function ($query) use ($date_range_arr) {
            return $query->where('calendar_date->start_date', '<=', $date_range_arr['start_date'])
                ->where('calendar_date->end_date', '>=', $date_range_arr['start_date']);
        })->orWhere(function ($query) use ($date_range_arr) {
            return $query->where('calendar_date->start_date', '<=', $date_range_arr['end_date'])
                ->where('calendar_date->end_date', '>=', $date_range_arr['end_date']);
        });

        if ($action == 'update') {
            return false;
        }
        $subRangeCount = $subRangeCount->count();
        if ($subRangeCount) {
            return true;
        }
        return false;
    }

    public function isTimeSlotSettingExistAlready($slot_detail, $start_time, $end_time)
    {
        if ($slot_detail <= 0) {
            return false;
        }
        $overlap_slots = [];
        foreach ($slot_detail as $slot) {

            if (!rangesNotOverlapOpen($slot['open_time'], $slot['close_time'], $start_time, $end_time) || !rangesNotOverlapClosed($slot['open_time'], $slot['close_time'], $start_time, $end_time)) {
                // return true;
                $overlap_slots[] = $slot['slot_id'];
            }
        }
        if (count($overlap_slots)) {
            return $overlap_slots;
        } else {
            return false;

        }
    }

    private function getExistingSlotIdForDayRepeat($slot_detail_db, $open_time, $close_time)
    {
        foreach ($slot_detail_db as $slot_db) {
            if ($slot_db['open_time'] == $open_time && $slot_db['close_time'] == $close_time) {
                return $slot_db['slot_id'];
            }
        }
        return 0;
    }

    public function checkUpcomingReservations($dateOrDay, $type, $time_range = NULL, $record='first')
    {
        $restaurant_id = Auth::user()->restaurant_id;
        $status_list = ReservationStatus::getStatusForUpcomingReservation();
        $upcoming = Reservation::where('restaurant_id', \Auth::user()->restaurant_id);
        $upcoming = $upcoming->where('start_time', '>', Carbon::now());
        $upcoming = $upcoming->whereIn('status_id', [$status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.confirmed')]]);

        if ($time_range) {
            $upcoming = $upcoming->where('slot_time', $time_range);
            $time_range = explode('-', $time_range);
        } else {
            $time_range[0] = '00:01';
            $time_range[1] = '23:59';
        }
        if ($type == config('reservation.working_hour_type.day')) {
            $restaurant_tz = CommonFunctions::getRestaurantTimeZone($restaurant_id);
            $upcoming = $upcoming->where(DB::raw("LOWER(date_format(CONVERT_TZ(start_time,'UTC','".$restaurant_tz."'), \"%a\"))"), $dateOrDay);
        } else {
            $dateOrDay = json_decode($dateOrDay, true);
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $dateOrDay['start_date'] . ' ' . $time_range[0] . ':00');
            $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $dateOrDay['end_date'] . ' ' . $time_range[1] . ':00');
            $upcoming = $upcoming->where('start_time', '<', $end_date_time)->where('end_time', '>', $start_date_time);
        }
        if($record=='first'){
            $upcoming = $upcoming->first();
        }else{
            return $upcoming->get();
        }
        \Log::info($upcoming);
        if ($upcoming) {
            return false;
        }
        return true;
    }

    public function checkCurrentReservations($dateOrDay, $type, $time_range_org = NULL, $record='first')
    {
        $restaurant_id = Auth::user()->restaurant_id;
        $status_list = ReservationStatus::getStatusForCurrentReservation();
        $current = Reservation::where('restaurant_id', \Auth::user()->restaurant_id);
        $current = $current->where('start_time', '<=', Carbon::now())->where('end_time', '>=', Carbon::now());
        $current = $current->whereIn('status_id', [$status_list[config('reservation.status_slug.confirmed')], $status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.partially_seated')], $status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')], $status_list[config('reservation.status_slug.running_late')]]);

        if ($time_range_org) {
            $current = $current->where('slot_time', $time_range_org);
            $time_range = explode('-', $time_range_org);
        } else {
            $time_range[0] = '00:01';
            $time_range[1] = '23:59';
        }
        //$current_day = strtolower(Carbon::now()->format('D'));
        //$current_date = Carbon::today()->toDateString();
        //$current_time = Carbon::now()->format('H:i');
        //\Log::info($current_day."***".$current_date);exit;
        if ($type == config('reservation.working_hour_type.day')) {
            /*if($time_range_org && $current_day==$dateOrDay && isInputTimeExistInTimeRange($time_range_org, $current_time)){
                return false;
            }*/
            $restaurant_tz = CommonFunctions::getRestaurantTimeZone($restaurant_id);
            $current = $current->where(DB::raw("LOWER(date_format(CONVERT_TZ(start_time,'UTC','".$restaurant_tz."'), \"%a\"))"), $dateOrDay);
        } else {
            $dateOrDay = json_decode($dateOrDay, true);
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $dateOrDay['start_date'] . ' ' . $time_range[0] . ':00');
            $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $dateOrDay['end_date'] . ' ' . $time_range[1] . ':00');
            /*if ($time_range_org && ($current_date >= Carbon::parse($dateOrDay['start_date'])->format('Y-m-d') && $current_date <= Carbon::parse($dateOrDay['end_date'])->format('Y-m-d')) && isInputTimeExistInTimeRange($time_range_org, $current_time)){
                return false;
            }*/
            $current = $current->where('start_time', '<', $end_date_time)->where('end_time', '>', $start_date_time);
        }
        if($record=='first'){
            $current = $current->first();
        }else{
            //\Log::info($current->get());
            return $current->get();
        }
        //\Log::info($current);
        if ($current) {
            return false;
        }
        return true;
    }


    /*private function cancelReservation($dateOrDay, $restaurant_id, $type, $time_range=NULL)
    {
        $reservations = Reservation::with('status')->where('restaurant_id', $restaurant_id)->whereHas('status', function($query) {
            $query->where('slug', config('reservation.status_slug.reserved'));
        });
        if($time_range){
            $reservations = $reservations->where('slot_time', $time_range);
            $time_range = explode('-', $time_range);
        }else{
            $time_range[0] = '00:01';
            $time_range[1] = '23:59';
        }
        if($type == config('reservation.working_hour_type.day')) {
            $reservations = $reservations->where(DB::raw('LOWER(date_format(start_time, "%a"))'), $dateOrDay);
        }else {
            $dateOrDay = json_decode($dateOrDay, true);
            $start_date_time = $dateOrDay['start_date'].' '.$time_range[0].':00';
            $end_date_time = $dateOrDay['end_date'].' '.$time_range[1].':00';
            $reservations = $reservations->where('start_time', '<', $end_date_time)->where('end_time', '>', $start_date_time);
        }
        $reservations->update(['status_id' => ReservationStatus::getCancelledStatusId()]);
    }*/

    public function checkForCurrentOrUpcomingReservation()
    {
        $id = Input::get('id');
        $action = Input::get('action');
        $type = Input::get('type');
        if($action=='delete'){
            $slot_id = Input::get('slot_id');
            try {
                if ($type == config('reservation.working_hour_type.day')) {
                    $calendar_days = config('reservation.calendar_days');
                    $calendar_day = $calendar_days[$id];
                    $scheduleHours = WorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', $calendar_day)->first();
                } else {
                    $scheduleHours = CustomWorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('id', $id)->first();
                }
                if(!$scheduleHours){
                    response()->json(['errors' => ["Hours setting does not exist."]], 422)->send();
                }
                if ($type == config('reservation.working_hour_type.day')) {
                    $dayOrDate = $scheduleHours->calendar_day;
                } else {
                    $dayOrDate = $scheduleHours->calendar_date;
                }
                if (empty($scheduleHours->slot_detail) || $scheduleHours->slot_detail == NULL) {
                    return \Response::json(['success' => true], 200);
                }

                if(empty($slot_id) || $slot_id <= 0){
                    $time_range = NULL;
                }else{
                    $slot_detail_db = json_decode($scheduleHours->slot_detail, true);
                    $exist = array_search($slot_id, array_column($slot_detail_db, 'slot_id'));
                    $time_range = $slot_detail_db[$exist]['open_time'] . '-' . $slot_detail_db[$exist]['close_time'];
                }

                if($this->isCurrentTimingSlot($time_range, $dayOrDate, $type)){
                    response()->json(['errors' => ["Ongoing shift cannot be deleted."]], 422)->send();
                    die();
                }

                if ($this->checkCurrentReservations($dayOrDate, $type, $time_range) === false) {
                    response()->json(['errors' => ["The slot cannot be deleted as it has ongoing reservations."]], 422)->send();
                    die();
                }
                if ($this->checkUpcomingReservations($dayOrDate, $type, $time_range) === false) {
                    response()->json(['errors' => ["The slot cannot be deleted as it has upcoming reservations."]], 422)->send();
                    die();
                }
                return \Response::json(['success' => true], 200);
            } catch (\Exception $e) {
                return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
            }
        }

        if($action=='dayoff'){
            $resourceId = Input::get('resourceId');
            $resourceId = ($resourceId) ? str_replace('dayoff_', '', $resourceId) : 0;
            $type = Input::get('type');
            try {
                if ($type == config('reservation.working_hour_type.day')) {
                    $working_schedule = WorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_day', strtolower($resourceId))->first();
                    if ($working_schedule && empty($working_schedule->slot_detail)) {
                        return \Response::json(['success' => true], 200);
                    } else {
                        if ($this->checkCurrentReservations(strtolower($resourceId), $type) === false) {
                            response()->json(['errors' => ["The day cannot be closed as it has ongoing reservations."]], 422)->send();
                            die();
                        }
                        if ($this->checkUpcomingReservations(strtolower($resourceId), $type) === false) {
                            response()->json(['errors' => ["The day cannot be closed as it has upcoming reservations."]], 422)->send();
                            die();
                        }
                        return \Response::json(['success' => true], 200);
                    }
                } else {
                    if ($resourceId) {
                        $inputData['calendar_date'] = $this->getCustomStartEndDate($resourceId);
                    } else {
                        $start_date = Carbon::createFromFormat('m-d-Y', Input::get('start_date'))->format('Y-m-d');
                        $end_date = Carbon::createFromFormat('m-d-Y', Input::get('end_date'))->format('Y-m-d');

                        $calendar_dates = ["start_date" => $start_date, "end_date" => $end_date];
                        $inputData['calendar_date'] = $calendar_dates ? json_encode($calendar_dates) : $calendar_dates;
                    }
                    $working_schedule = CustomWorkingHours::where('restaurant_id', \Auth::user()->restaurant_id)->where('calendar_date', 'LIKE', $inputData['calendar_date'])->first();
                    if ($working_schedule && empty($working_schedule->slot_detail)) {
                        return \Response::json(['success' => true], 200);
                    } else {
                        if ($this->checkCurrentReservations($inputData['calendar_date'], $type) === false) {
                            response()->json(['errors' => ["The day cannot be closed as it has ongoing reservations."]], 422)->send();
                            die();
                        }
                        if ($this->checkUpcomingReservations($inputData['calendar_date'], $type) === false) {
                            response()->json(['errors' => ["The day cannot be closed as it has upcoming reservations."]], 422)->send();
                            die();
                        }
                        return \Response::json(['success' => true], 200);
                    }
                }
                return \Response::json(['success' => true], 200);
            } catch (\Exception $e) {
                return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
            }
        }
    }

    /*
     * After discussion it with Pravish, do not allow change in anything except start and end time edit
     */
    private function isAnyChangeImpactOnReservation($db_slot_detail, $input_slot_detail)
    {
        if(empty($db_slot_detail) || empty($input_slot_detail)){
            return false;
        }
        // check open and close time
        $utc = new \DateTimeZone('UTC');
        $start1 = new \DateTime($db_slot_detail['open_time'], $utc);
        $start2 = new \DateTime($input_slot_detail['open_time'], $utc);

        if($start2 > $start1){
            return true;
        }
        /*if($start1 > $start2){
            return false;
        }*/

        $end1 = new \DateTime($db_slot_detail['close_time'], $utc);
        $end2 = new \DateTime($input_slot_detail['close_time'], $utc);

        if($end2 < $end1){
            return true;
        }
        /*if($end1 < $end2){
            return false;
        }*/
        //check existing floor in new array
        /*if(!$db_slot_detail['floors'] === array_intersect($db_slot_detail['floors'], $db_slot_detail['floors'])){
            return true;
        }

        $available_table_db = isset($db_slot_detail['available_tables']) && !empty($db_slot_detail['available_tables']) ? json_decode($db_slot_detail['available_tables']):null;

        $available_table_input = isset($input_slot_detail['available_tables']) && !empty($input_slot_detail['available_tables']) ? json_decode($input_slot_detail['available_tables']):null;

        if(!$available_table_db || !$available_table_input){
            return false;
        }*/
        return false;
    }

    public function updateSlotTimeInReservation($reservations, $slot_time)
    {
        if ($reservations->count() > 0){
            foreach ($reservations as $reservation){
                $reservation->slot_time = $slot_time;
                $reservation->save();
            }
        }
    }

    private function isCurrentTimingSlot($time_range_org, $dayorDate, $type)
    {
        $restaurant_id = Auth::user()->restaurant_id;
        $current_day = strtolower(Carbon::now()->format('D'));
        $current_date = Carbon::today()->toDateString();
        $current_time = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'H:i');
        if ($type == config('reservation.working_hour_type.day')) {
            if ($time_range_org && $current_day == $dayorDate && isInputTimeExistInTimeRange($time_range_org, $current_time)) {
                return true;
            }
        } else {
            $dateOrDay = json_decode($dayorDate, true);
            if ($time_range_org && ($current_date >= Carbon::parse($dateOrDay['start_date'])->format('Y-m-d') && $current_date <= Carbon::parse($dateOrDay['end_date'])->format('Y-m-d')) && isInputTimeExistInTimeRange($time_range_org, $current_time)) {
                return true;
            }
        }
        return false;
    }

    public function getCurrentSlotRange($current_time, $time_range)
    {
        if ($time_range && isInputTimeExistInTimeRange($time_range, $current_time)) {
            \Log::info([$time_range, $current_time]);

            return true;
        }
        return false;
    }
}