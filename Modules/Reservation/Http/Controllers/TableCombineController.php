<?php

namespace Modules\Reservation\Http\Controllers;

use App\Helpers\CommonFunctions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Reservation\Entities\BlockTable;
use Illuminate\Routing\Controller;
use Modules\Reservation\Entities\CombineTable;
use Modules\Reservation\Entities\Floor;
use Modules\Reservation\Entities\Table;
use Illuminate\Support\Facades\Validator;
use DB;

class TableCombineController extends Controller
{

    public function combineTables($floor_id, Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $all_tables = json_decode($data['tables'], true);
            $restaurant_id = \Auth::user()->restaurant_id;
            $validator = Validator::make($all_tables, [
                '*.id' => 'required:integer',
                '*.tableName' => 'required|max:50',
                '*.tableMinGuest' => 'required:integer',
                '*.tableMaxGuest' => 'required:integer',
            ]);
            if ($validator->fails()) {
                return \Response::json(['errors' => ['Some required field missings']], 422);
            }
            $table_ids = implode(',', array_sort(array_column($all_tables, 'id')));
            $table_max = array_sum(array_column($all_tables, 'tableMaxGuest'));
            $table_min = $data['min'];
            $already_exist = CombineTable::where('restaurant_id', $restaurant_id)->where('floor_id', $floor_id)->where('table_id', $table_ids)->first();
            if ($already_exist) {
                return \Response::json(['errors' => ['Table combination is already exist']], 422);
            }
            CombineTable::create([
                'restaurant_id' => $restaurant_id,
                'floor_id' => $floor_id,
                'table_id' => $table_ids,
                'min' => $table_min,
                'max' => $table_max]);
            DB::commit();
            return \Response::json(['title' => 'Table combination has been saved', 'message' => 'Floor configuration updated successfully'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return \Response::json(['errors' => [$e->getMessage()]], 422);
        }
    }

    public function deleteTableCombination($combine_table_id)
    {
        DB::beginTransaction();
        try {
            $restaurant_id = \Auth::user()->restaurant_id;
            CombineTable::where('restaurant_id', $restaurant_id)->where('id', $combine_table_id)->delete();
            DB::commit();
            return \Response::json(['title' => 'Floor Deleted', 'message' => 'Table combination has been deleted successfully'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return \Response::json(['errors' => ['Unable to delete the table combination']], 422);
        }
    }

    public function getTableCombinationByFloor($restaurant_id, $floor_id)
    {
        $table_combinations = CombineTable::where('restaurant_id', $restaurant_id)->where('floor_id', $floor_id)->get();
        foreach ($table_combinations as $combination){
            $table_combination_id = explode(',', $combination->table_id);
            $combination_name = Table::where('floor_id', $floor_id)->whereIn('id', $table_combination_id)->select(DB::raw("GROUP_CONCAT(name SEPARATOR '+') as combination_name"))->first();
            $combination->combination_name = $combination_name->combination_name;
        }
        return $table_combinations;
    }

}
