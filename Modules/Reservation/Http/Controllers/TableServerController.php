<?php

namespace Modules\Reservation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Reservation\Entities\Floor;
use Modules\Reservation\Entities\ReservationStatus;
use Modules\Reservation\Entities\Server;
use Modules\Reservation\Entities\Table;
use Carbon\Carbon;
use Modules\Reservation\Entities\TableServer;
use DB;

class TableServerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(Server $server_obj, Table $table)
    {
        $this->obj = $server_obj;
        $this->tableobj = $table;
    }

    public function index(Request $request)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $assign_date = Input::has('assign_date') ? Input::get('assign_date') : Carbon::now()->format('m-d-Y');
        $floor_id = Input::get('floor_id');
        $slot_id = Input::get('slot_id');
        if(!$slot_id || $slot_id == "undefined"){
            $slot_names = $this->getSlotNameArray($assign_date, $restaurant_id);
            $slot_id = isset($slot_names[0]['slot_id'])?$slot_names[0]['slot_id']:0;
        }
        $assign_date_format = Carbon::createFromFormat('m-d-Y', $assign_date)->format('Y-m-d');
        $servers = Server::with(['tableservers'=>function($query) use($restaurant_id, $assign_date_format, $slot_id){
            $query->with(['floor'=>function($query){
                $query->select(['id', 'name']);
            }])->where('restaurant_id', $restaurant_id)->whereDate('assigned_date', $assign_date_format)->where('slot_id', $slot_id)->select(['id','server_id','table_ids', 'floor_id']);
        }])->where('restaurant_id',$restaurant_id)->select(['id', 'fname', 'lname', 'mobile', 'color'])->latest()->get();
        foreach ($servers as $server) {
            $table_name_arr=$floor_name=[];
            $total_cover = 0;
            if (isset($server->tableservers) && !empty($server->tableservers)) {
                foreach ($server->tableservers as $table_detail){
                    //\Log::info($table_detail->server_id);
                    //\Log::info($table_detail->floor);
                    if (isset($table_detail->floor) && !empty($table_detail->floor)) {
                        if (isset($table_detail->table_ids) && !empty($table_detail->table_ids)) {
                            $table_info = json_decode($table_detail->table_ids, true);
                            foreach ($table_info as $table_data) {
                                $table_name_arr[] = $table_data['tableText'];
                                $total_cover+=$table_data['maxGuests'];
                            }
                        }
                        $floor_name[] = $table_detail->floor->name;
                    }
                }
                $server->floor_name = !empty($floor_name)?array_unique($floor_name):[];
                $server->table_name = !empty($table_name_arr)?array_unique($table_name_arr):[];
                $server->total_cover = $total_cover;
            }
        }
        //\Log::info($servers);
        if ($request->ajax()) {
            return view("reservation::server.partial.server-list", compact('servers', 'assign_date','slot_id'));
            exit;
        }
        return view('reservation::server.index', compact('servers', 'slot_names', 'assign_date','slot_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request, $server_id)
    {
        $assign_date = Input::has('assign_date') ? Input::get('assign_date') : Carbon::now()->format('m-d-Y');
        $slot_id = Input::get('slot_id');
        $restaurant_id = \Auth::user()->restaurant_id;
        $floor = new Floor();
        $floormenu = $floor->get_floor_menu();
        $floor_id = Input::get('floor_id');

        $first_floor_id = !empty($floor_id) ? $floor_id : (isset($floormenu) && !empty($floormenu[0]) ? $floormenu[0]->id:0);

        $server_detail = $this->obj->where('restaurant_id',$restaurant_id)->where('id', $server_id)->first();
        $floor = Floor::select(['id', 'type','completed_step','layout_config'])->where('restaurant_id', $restaurant_id)
                ->with(['tables'=>function($query){
                    $query->select(['floor_id','id', 'configuration', 'name']);
                }])
                ->where('id',$first_floor_id)
                ->first();
        $floor_data=[];
        if( $floor) {
            $floor = $floor->toArray();
            $floor_data[0] = isset($floor['layout_config']) ? $floor['layout_config'] : null;
            $tables = [];
            if (count($floor['tables'])) {
                foreach ($floor['tables'] as $tbl) {
                    $config = $tbl['configuration'] ? json_decode($tbl['configuration'], true) : null;
                    $config['id'] = $tbl['id'];
                    $server_info = $this->getTableServerForFloorView($first_floor_id, $tbl['id'], $slot_id, $config, $server_detail, $assign_date);
                    $config['server'] = $server_info;
                    $config['reservations'] = [];
                    $config['tableAvailability'] = 'Available';
                    $tables[] = json_encode($config);
                }
            }
            $floor_data[1] = isset($tables) ? $tables : null;
            $floor_data[2] = ReservationStatus::getAllStatus()->toJson();
            $floor_data = json_encode($floor_data);
        }
        $slot_names = $this->getSlotNameArray($assign_date, $restaurant_id);
        $server_short_name = substr($server_detail->fname, 0,1).substr($server_detail->lname, 0,1);
        if ($request->ajax()) {
            return \Response::json(['data'=>['floor_data'=>$floor_data, 'server_short_name'=>$server_short_name, 'assign_date'=>$assign_date]], 200);
            exit;
        }
        return view('reservation::server.assign', compact('slot_names', 'server_detail', 'floor_data', 'assign_date'));
    }


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $restaurant_id = \Auth::user()->restaurant_id;
            $data = $request->all();
            //\Log::info($data);
            $assign_date = Carbon::createFromFormat('m-d-Y', $data['assign_date'])->format('Y-m-d');
            /*$table_server = TableServer::where('restaurant_id', $restaurant_id)->whereDate('assigned_date', $assign_date)->where('server_id', $data['server_id'])->where('slot_id', $data['slot_id'])->where('floor_id', '!=', $data['floor_id'])->first();
            \Log::info($table_server);
            if ($table_server) {
                return \Response::json(['errors' => ['Can not assign two floor tables in same time slot']], 422);
            }*/
            DB::beginTransaction();
            //\Log::info($data['tables']);
            $tableServer = TableServer::where('restaurant_id', $restaurant_id)->whereDate('assigned_date', $assign_date)->where('server_id', $data['server_id'])->where('slot_id', $data['slot_id'])->where('floor_id', $data['floor_id'])->first();
            $msg = 'Table assignment was saved successfully.';
            if ($tableServer) {
                if (empty($data['tables'])) {
                    $msg = "Table assignment was removed successfully.";
                    if ($tableServer->exists) {
                        $tableServer->delete();
                    }
                }else{
                    $tableServer->update([
                        'table_ids' => $data['tables'],
                    ]);
                }
            } else {
                if (!empty($data['tables'])) {
                    $tableServer = TableServer::create([
                        'restaurant_id' => $restaurant_id,
                        'server_id' => $data['server_id'],
                        'floor_id' => $data['floor_id'],
                        'slot_id' => $data['slot_id'],
                        'slot_name' => $data['slot_name'],
                        'table_ids' => $data['tables'],
                        'assigned_date' => $assign_date
                    ]);
                }
            }
            DB::commit();
            return \Response::json(['message' => [$msg]], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return back()->withInput()->withErrors(array('message' => 'Something went wrong, please try later'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('reservation::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('reservation::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {

    }

    public function getSlotNameArray($assign_date)
    {
        $slot_name_arr = [];
        if(!empty($assign_date) && $assign_date!='undefined'){
            $restaurant_id = \Auth::user()->restaurant_id;
            $assign_date = Carbon::createFromFormat('m-d-Y', $assign_date)->format('Y-m-d');
            $scheduled_hours = app('Modules\Reservation\Http\Controllers\ReservationController')->getWorkingHours($assign_date, $restaurant_id);
            if($scheduled_hours && !$scheduled_hours->is_dayoff && $scheduled_hours->slot_detail){
                $slot_detail_arr = json_decode($scheduled_hours->slot_detail, true);
                usort($slot_detail_arr, function ($item1, $item2) {
                    return strtotime($item1['open_time']) <=> strtotime($item2['open_time']);
                });
                //\Log::info($slot_detail_arr);
                foreach ($slot_detail_arr as $key => $slot_detail){
                    $slot_name_arr[$key]['slot_name'] = $slot_detail['slot_name'];
                    $slot_name_arr[$key]['slot_id'] = $slot_detail['slot_id'];
                }
            }
        }
        //\Log::info($slot_name_arr);
        \Log::info(array_unique($slot_name_arr, SORT_REGULAR));
        return count($slot_name_arr)>0?array_unique($slot_name_arr, SORT_REGULAR):$slot_name_arr;
    }

    public function getTableServerInfo($floor_id, $table_id, $slot_id, $assign_date, $server_id=null)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $server_table_info = TableServer::with(['server' => function($query) {
            $query->select('id', 'fname', 'lname', 'color');
            }
        ])->where('restaurant_id', $restaurant_id);
        /*if($server_id){
            $server_table_info = $server_table_info->where('server_id', $server_id);
        }*/
        $server_table_info = $server_table_info->where('floor_id', $floor_id)->whereDate('assigned_date', $assign_date)->where('slot_id', $slot_id)->whereRaw("json_contains(`table_ids`, '{\"id\" : $table_id}')")->first();
        return $server_table_info;
    }

    public function getTableServerForFloorView($floor_id, $table_id, $slot_id, $config, $server_detail, $assign_date)
    {
        $assign_date = Carbon::createFromFormat('m-d-Y', $assign_date)->format('Y-m-d');
        $table_server = $this->getTableServerInfo($floor_id, $table_id, $slot_id, $assign_date, $server_detail->id);
        if($table_server && ($table_server->server && !empty($table_server->server))){
            $server_table_id = !empty($config) && !empty($config['tableID']) ? $config['tableID'] : null;
            $server_color = $table_server->server->color;
            $server_name = (($table_server->server->fname) ? $table_server->server->fname[0] : '').(($table_server->server->lname) ? $table_server->server->lname[0] : '');
            $server_id = (($table_server->server->id) ? $table_server->server->id : '');
        }else{
            $server_color = $server_detail->color;
            $server_name = (($server_detail->fname) ? $server_detail->fname[0] : '').(($server_detail->lname) ? $server_detail->lname[0] : '');
            $server_table_id = "";
            $server_id = "";
        }
        $server[0]['serverTableColor'] = "#".$server_color;
        $server[0]['serverTableId'] = $server_table_id;
        $server[0]['serverName'] = $server_name;
        $server[0]['serverId'] = $server_id;
        return $server;
    }
}
