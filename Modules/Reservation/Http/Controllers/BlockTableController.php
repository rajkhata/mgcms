<?php

namespace Modules\Reservation\Http\Controllers;

use App\Helpers\CommonFunctions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Reservation\Entities\BlockTable;
use Illuminate\Routing\Controller;
use Modules\Reservation\Entities\Floor;
use Modules\Reservation\Entities\Table;


class BlockTableController extends Controller
{
    public function checkAvailabilityForTableBlock(Request $request)
    {
        $ms = microtime(true);
        $block_date = $request->get('block_date');
        $block_date = empty($block_date)?Carbon::today()->toDateString()->format('Y-m-d'):$block_date;
        if($block_date == 'Today'){
            $block_date = Carbon::today()->toDateString();
        } elseif ($block_date == 'Tomorrow') {
            $block_date = Carbon::tomorrow()->toDateString();
        }else{
            $block_date = Carbon::createFromFormat('m-d-Y', $block_date)->format('Y-m-d');
        }
        $start_time = $request->get('block_start_time');
        $start_time = date('H:i', strtotime($start_time));
        $resume_time = $request->get('block_resume_time');
        $resume_time = date('H:i:s', strtotime($resume_time));
        $floor_id = $request->get('block_floor');
        $table_ids = $request->get('block_tables');
        $restaurant_id = Auth::user()->restaurant_id;
        try {
            $response = $this->checkAvailability($restaurant_id, $block_date, $start_time, $resume_time, NULL, $floor_id, $ms);
            if(isset($response['data']) && !empty($response['data'])){
                return \Response::json($response, 200);
            }
            return \Response::json($response, 422);
        }catch(\Exception $e){
            $me = microtime(true) - $ms;
            return \Response::json(['errors' => [ $e->getMessage() ], 'xtime' => $me], 500);
        }
    }

    private function checkAvailability($restaurant_id, $block_date, $start_time, $resume_time, $table_ids = null, $floor_id=null, $ms)
    {
        $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now')->format('s');
        $start_date_time = Carbon::parse($block_date . ' ' . $start_time.":".$localCurrentTime)->format('Y-m-d H:i:s');
        /*if ($start_date_time < $localCurrentTime) {
            $me = microtime(true) - $ms;
            return ['errors' => ['Table block cannot be made for a past time.'], 'xtime' => $me];
        }*/
        $scheduled_hours = app('Modules\Reservation\Http\Controllers\ReservationController')->getWorkingHours($block_date, $restaurant_id);
        if (!$scheduled_hours) {
            $me = microtime(true) - $ms;
            return ['errors' => ['Restaurant is not operational currently. This could be because the timeslot was not defined.'], 'xtime' => $me];
        }
        if ($scheduled_hours->is_dayoff) {
            $me = microtime(true) - $ms;
            return ['errors' => ['Restaurant is closed. No reservations can be made on this date.'], 'xtime' => $me];
        }
        $slots_data = app('Modules\Reservation\Http\Controllers\ReservationController')->getAvailableSlots($scheduled_hours);
        if (!$slots_data['success']) {
            $me = microtime(true) - $ms;
            return ['errors' => [$slots_data['message']], 'xtime' => $me];
        }
        $resume_date_time = Carbon::parse($block_date . ' ' . $resume_time)->format('Y-m-d H:i:s');
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
        $resume_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $resume_date_time);
        if (isStartTimeEndTimeExistInTimeRangeArr($slots_data['slots_availability']['slot_range'], $start_time, $resume_time)) {
            $floor_tables = $this->getAvailableFloorAndTables($restaurant_id, $start_date_time, $resume_date_time, $table_ids, $floor_id);
            if($floor_tables){
                $me = microtime(true) - $ms;
                return ['data' => $floor_tables, 'errors' => null, 'xtime' => $me];
            }
            $me = microtime(true) - $ms;
            return ['errors' => ['tables are not available'], 'xtime' => $me];
        } else {
            $me = microtime(true) - $ms;
            return ['errors' => ['No tables are available at the selected timeslot.'], 'xtime' => $me];
        }
    }

    public function getBlockTableCountBetweenDates($start_date_time, $end_date_time, $restaurant_id, $table_id = NULL)
    {
        $blockTableArr = array();
        $blockTables = BlockTable::where('restaurant_id', $restaurant_id)
            ->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time);
        if (is_array($table_id)) {
            $blockTables = $blockTables->whereIn('table_id', $table_id);
        } elseif ($table_id) {
            $blockTables = $blockTables->where('table_id', $table_id);
        }
        $blockTables = $blockTables->groupBy('table_id')->select('table_id', DB::raw("sum(reserved_seat) as reserved_seat"), DB::raw("count(*) as table_count"))->get();
        if ($blockTables->count() > 0) {
            foreach ($blockTables as $blockTable) {
                if($blockTable->table_id){
                    if (strpos($blockTable->table_id, ',')) {
                        $multiTableIds = explode(',', $blockTable->table_id);
                        if (count($multiTableIds)) {
                            foreach ($multiTableIds as $multiTab) {
                                $tableDetail = Table::where('restaurant_id', $restaurant_id)->where('id', '=', $multiTab)->first();
                                if ($tableDetail) {
                                    $blockTableArr[$multiTab]['table_id']      = $multiTab;
                                    $blockTableArr[$multiTab]['reserved_seat'] = $blockTable->reserved_seat;
                                }
                            }
                        }
                    } else {
                        $blockTable->load('table');
                        $blockTableArr[$blockTable->table_id]['table_id']      = $blockTable->table_id;
                        $blockTableArr[$blockTable->table_id]['reserved_seat'] = $blockTable->reserved_seat;
                    }
                }
            }
        }
        return $blockTableArr;
    }



    private function getAvailableFloorAndTables($restaurant_id, $start_date_time, $resume_date_time, $table_ids, $floor_id)
    {
        $available_floor_table_info = [];
        $reservation_detail_arr = app('Modules\Reservation\Http\Controllers\ReservationController')->getReservationCountBetweenDates($start_date_time, $resume_date_time, $restaurant_id, $table_ids);
        $floor_table_info = Floor::select('id', 'name')->where('restaurant_id', $restaurant_id);
        if($floor_id){
            $floor_table_info = $floor_table_info->where('id', $floor_id);
        }
        if (is_array($table_ids)) {
            $floor_table_info = $floor_table_info->whereHas('tables', function($query) use($table_ids){
                $query->whereIn('id', $table_ids);
            });
        } elseif ($table_ids) {
            $floor_table_info = $floor_table_info->whereHas('tables', function($query) use($table_ids){
                $query->where('id', $table_ids);
            });
        }
        $floor_table_info = $floor_table_info->where('completed_step', 2)->with(['tables' => function($query)use($table_ids) {
            if(is_array($table_ids)) {
                $query=$query->whereIn('id', $table_ids);
            } elseif ($table_ids){
                $query=$query->where('id', $table_ids);
            }
            $query->select('id', 'floor_id', 'name', 'min', 'max');
        }]);
        $floor_table_info = $floor_table_info->get()->toArray();
        $blocked_table = $this->getBlockedTableArr($restaurant_id, $floor_id=null, $table_id=null, $start_date_time, $resume_date_time);
        foreach ($floor_table_info as $floor_key=>$floor) {
            foreach ($floor['tables'] as $table_key=>$table) {
                if(Carbon::parse($start_date_time)->format('Y-m-d')==Carbon::today()->toDateString()){
                    $current_reservation = app('Modules\Reservation\Http\Controllers\FloorController')->getTableReservations($table['id'], 'current', Carbon::parse($start_date_time)->format('Y-m-d'));
                    if($current_reservation && count($current_reservation)>0){
                        continue;
                    }
                }
                if(count($blocked_table)==0  || ($blocked_table && !in_array($table['id'], $blocked_table))){
                    //\Log::info($table['id']);
                    //\Log::info($reservation_detail_arr);
                    if((count($reservation_detail_arr)==0 || !isset($reservation_detail_arr[$table['id']])) || (($reservation_detail_arr && isset($reservation_detail_arr[$table['id']])) && ($reservation_detail_arr[$table['id']]['table_count']<=0))){
                        $available_floor_table_info[$floor_key]['id'] = $floor['id'];
                        $available_floor_table_info[$floor_key]['name'] = $floor['name'];
                        $available_floor_table_info[$floor_key]['tables'][$table_key] = $table;
                    }
                }
            }
        }
        return $available_floor_table_info;
    }


    public function blockTable(Request $request)
    {
        $ms = microtime(true);
        try {
            $block_date = $request->get('block_date');
            $block_date = empty($block_date) ? Carbon::today()->toDateString()->format('Y-m-d') : $block_date;
            if ($block_date == 'Today') {
                $block_date = Carbon::today()->toDateString();
            } elseif ($block_date == 'Tomorrow') {
                $block_date = Carbon::tomorrow()->toDateString();
            } else {
                $block_date = Carbon::createFromFormat('m-d-Y', $block_date)->format('Y-m-d');
            }
            $restaurant_id = Auth::user()->restaurant_id;
            $start_time = $request->get('block_start_time');
            $start_time = date('H:i', strtotime($start_time));
            $start_date_time = Carbon::parse($block_date . ' ' . $start_time)->format('Y-m-d H:i:s');
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);

            $resume_time = $request->get('block_resume_time');
            $resume_time = date('H:i:s', strtotime($resume_time));
            $resume_date_time = Carbon::parse($block_date . ' ' . $resume_time)->format('Y-m-d H:i:s');
            $resume_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $resume_date_time);

            $floor_id = $request->get('block_floor');
            $table_ids = $request->get('block_tables');

            $response = $this->checkAvailability($restaurant_id, $block_date, $start_time, $resume_time, $table_ids, $floor_id, $ms);
            if (isset($response['data']) && !empty($response['data'])) {
                // save block table data
                BlockTable::create([
                    'restaurant_id' => $restaurant_id, 'start_time' => $start_date_time, 'end_time' => $resume_date_time, 'floor_id' => $floor_id, 'table_id' => implode(',', $table_ids)]);
                return \Response::json(['message' => 'The table(s) are blocked successfully. Click on the table to unblock.'], 200);
            }
            return \Response::json($response, 422);
        } catch (\Exception $e) {
            $me = microtime(true) - $ms;
            return \Response::json(['errors' => [$e->getMessage()], 'xtime' => $me], 500);
        }
    }

    public function getBlockTableInfo($restaurant_id, $block_date, $floor_id=null, $table_id=null)
    {
        $block_table = BlockTable::where('restaurant_id', $restaurant_id);
        if($floor_id){
            $block_table = $block_table->where('floor_id', $floor_id);
        }
        if($table_id){
            $block_table = $block_table->whereRaw("FIND_IN_SET('".$table_id."', table_id)");
        }
        return $block_table->whereDate('start_time', '=', $block_date)->where(function ($query) {
            return $query->where('start_time', '<=', Carbon::now())->where('end_time', '>=', Carbon::now());
        })->first();
    }

    public function isTableAlreadyBlocked($restaurant_id, $floor_id=null, $table_id, $block_date)
    {
        $block_table = BlockTable::where('restaurant_id', $restaurant_id);
        if($floor_id){
            $block_table = $block_table->where('floor_id', $floor_id);
        }
        if($table_id){
            $block_table = $block_table->whereRaw("FIND_IN_SET('".$table_id."', table_id)");
        }

      $block_table = $block_table->whereDate('start_time', '=', $block_date)->where(function ($query) {
            return $query->where('start_time', '<=', Carbon::now())->where('end_time', '>=', Carbon::now());
        })->first();

      if($block_table){
            return true;
        }
        return false;
    }

    public function getBlockedTableArr($restaurant_id, $floor_id=null, $table_id=null, $start_date_time, $end_date_time)
    {
        $blocked_table = [];
        $block_table = BlockTable::where('restaurant_id', $restaurant_id);
        if($floor_id){
            $block_table = $block_table->where('floor_id', $floor_id);
        }
        if($table_id){
            $block_table = $block_table->whereRaw("FIND_IN_SET('".$table_id."', table_id)");
        }
        $block_table = $block_table->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time)->get()->toArray();
        foreach ($block_table as $key=>$table){
            if (strpos($table['table_id'], ',')) {
                $blocked_table = array_merge($blocked_table, explode(',', $table['table_id']));
            }else{
                array_push($blocked_table, $table['table_id']);
            }
        }
        $blocked_table = count($blocked_table)==0?$blocked_table:array_unique($blocked_table);
        return $blocked_table;
    }

    public function resumeBooking(Request $request)
    {
        $ms = microtime(true);
        try {
            $blocked_table = $request->get('table_id');
            $blocked_date = $request->get('blocked_date');
            $restaurant_id = Auth::user()->restaurant_id;
            $block_table = BlockTable::where('restaurant_id', $restaurant_id)->whereRaw("FIND_IN_SET('" . $blocked_table . "', table_id)")->whereDate('start_time', '=', $blocked_date)->where(function ($query) {
                return $query->where('start_time', '<=', Carbon::now())->where('end_time', '>=', Carbon::now());
            })->first();
            $block_table->end_time = Carbon::now();
            $block_table->save();
            return \Response::json(['message' => 'The table is now unblocked and available to take new reservations.'], 200);
        } catch (\Exception $e) {
            $me = microtime(true) - $ms;
            return \Response::json(['errors' => [$e->getMessage()], 'xtime' => $me], 500);
        }
    }


}
