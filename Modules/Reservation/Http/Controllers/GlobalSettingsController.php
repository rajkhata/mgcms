<?php

namespace Modules\Reservation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\Reservation\Http\Requests\ReservationSettingRequest;
use Modules\Reservation\Http\Requests\NotificationSettingRequest;
use App\Models\Restaurant;
use DB;

class GlobalSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurant=Restaurant::select('reservation_settings')->find(\Auth::user()->restaurant_id);
        if($restaurant && is_array($restaurant->reservation_settings)){
                $reservation_settings=$restaurant->reservation_settings;
        }else{
               $reservation_settings=[];
        }
        return view('reservation::global-setting.index',compact('reservation_settings'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ReservationSettingRequest $request)
    {

        $restaurant = Restaurant::find(\Auth::user()->restaurant_id);
        //\Log::info($request->all());

        if (!$restaurant) {

            return back()->withInput()->withErrors(array('message' => ' No restaurant found'));
        } else {
            DB::beginTransaction();
            try {

                if (!empty($restaurant->reservation_settings)) {
                    $settings = $restaurant->reservation_settings;
                    // $settings=[];

                } else {
                    $settings = [];
                }
                //$data=$request->validated();
                $data = $request->except(['form_type', '_token']);

                $data['cuttoff_time'] = $request->get('cuttoff_type') == 'Hour' ? $request->get('cut_off_time_hour') : $request->get('cut_off_time_day');
                unset($data['cut_off_time_hour']);
                unset($data['cut_off_time_day']);

                $data['allow_guest_advance_reservation_time'] = $request->get('allow_guest_advance_online_reservation_type') == 'Days' ? $request->get('allow_guest_advance_online_reservation_days') : $request->get('allow_guest_advance_online_reservation_months');
                unset($data['allow_guest_advance_online_reservation_days']);
                unset($data['allow_guest_advance_online_reservation_months']);
                $settings['reservation'] = $data;

                $restaurant->reservation_settings = $settings;
                $restaurant->save();
                DB::commit();
                return back()->withInput()->with(['status' => 'Reservation setting saved', 'form_type' => 'reservation']);

            } catch (\Exception $e) {
                DB::rollback();
                return back()->withInput()->withErrors(array('message' => 'Unable to update the settings'));
            }

        }
    }

 public function notificationsettings(NotificationSettingRequest $request)
    { 

       $restaurant=Restaurant::find(\Auth::user()->restaurant_id);
      
           if(!$restaurant){

                return back()->withInput()->withErrors(array('message' => ' No restaurant found'));
            }else{
              
                DB::beginTransaction();
                try {
                   if(!empty($restaurant->reservation_settings)){
                      $settings=$restaurant->reservation_settings;

                  }else{
                        $settings=[];
                   }

                    $settings['notification']=$request->except(['form_type','_token']);
                    $restaurant->reservation_settings=$settings; 
                    $restaurant->save();  
                     DB::commit();

                return back()->withInput()->with(['status'=>'Notification setting saved','form_type'=>'notification']);
                                   }catch (\Exception $e){                    
                    DB::rollback();
                    return back()->withInput()->withErrors(array('message' =>'Unable to update the settings'));
                }   

            }          
    }

    public function getNotificationSetting($restaurant_id)
    {
        $notification_setting['cust_resv_sms_flag'] = true;
        $notification_setting['cust_resv_email_flag'] = true;
        $notification_setting['manager_resv_incoming_email_flag'] = true;
        $notification_setting['manager_resv_modified_email_flag'] = true;
        $notification_setting['manager_resv_cancelled_email_flag'] = true;
        $notification_setting['manager_resv_emails'] = null;
        $reservation_settings = Restaurant::where('id', $restaurant_id)->value('reservation_settings');
        if(!empty($reservation_settings) && !is_array($reservation_settings)){
            $reservation_settings = json_decode($reservation_settings, true);
        }
        if (empty($reservation_settings) || empty($reservation_settings['notification'])) {
            return $notification_setting;
        }
        $notification_setting['cust_resv_sms_flag'] = $reservation_settings['notification']['guest_sms_on_reservation']==0?false:true;

        $notification_setting['cust_resv_email_flag'] = $reservation_settings['notification']['guest_email_on_reservation']==0?false:true;

        $notification_setting['manager_resv_incoming_email_flag'] = $reservation_settings['notification']['restaurant_receive_notification_incoming']==0?false:true;

        $notification_setting['manager_resv_modified_email_flag'] = $reservation_settings['notification']['restaurant_receive_notification_modified']==0?false:true;

        $notification_setting['manager_resv_cancelled_email_flag'] = $reservation_settings['notification']['restaurant_receive_notification_cancelled']==0?false:true;

        $notification_setting['manager_resv_emails'] = !empty($reservation_settings['notification']['restaurant_receive_notification_emails'])?$reservation_settings['notification']['restaurant_receive_notification_emails']:null;

        return $notification_setting;
    }

    public function isHostNameRequired()
    {
        $host_required = Restaurant::where('id', \Auth::user()->restaurant_id)->select('reservation_settings->reservation->require_hostname_accepting as host_required')->get()->first();
        if (isset($host_required->host_required) && !empty($host_required->host_required) &&  str_replace('"', '', $host_required->host_required) == '1'){
            return true;
        }
        return false;
    }
}
