<?php

    namespace Modules\Reservation\Http\Controllers;

    use Carbon\Carbon;
    use DB;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Routing\Controller;
    use Illuminate\Support\Facades\Auth;
    use Modules\Reservation\Entities\YieldManagement;
    use YieldReservation;

    class YieldController extends Controller
    {
        /**
         * Show the form for creating a new resource.
         * @return Response
         */
        public function create()
        {
            return view('reservation::create');
        }

        /**
         * Store a newly created resource in storage.
         * @param  Request $request
         * @return Response
         */
        public function store(Request $request)
        {
        }

        /**
         * Show the specified resource.
         * @return Response
         */
        public function show($id)
        {
            if (is_numeric($id)) {
                $yield = YieldManagement::where('id', $id)->select('id', 'restaurant_id', 'floor_id', 'html_before', 'html_after', 'xtime', 'created_at')->get();
            } elseif ($id == 'last') {
                $yield = YieldManagement::select('id', 'restaurant_id', 'floor_id', 'html_before', 'html_after', 'xtime', 'created_at')->orderBy('id', 'desc')->take(1)->get();
            } else {
                $offset = explode("-", $id)[1];
                $yield  = YieldManagement::select('id', 'restaurant_id', 'floor_id', 'html_before', 'html_after', 'xtime', 'created_at')->orderBy('id', 'desc')->take($offset)->get();
            }
            foreach ($yield as $row) {
                $date = Carbon::parse($row->created_at)->format('d M Y, D, h:m:s a');
                $html = "<h4>Id - " . $row->id . " (".$date.")</h4>";
                $html .= "<h4>Restaurant id - " . $row->restaurant_id . "</h4>";
                $html .= "<h4>Floor id - " . $row->floor_id . "</h4>";
                $html .= "<h4>Original</h4>";
                $html .= $row->html_before;
                $html .= "<br /><br /><h4>Optimized (Time - " . $row->xtime . " s) </h4>";
                $html .= $row->html_after;
                echo $html;
            }

            exit;
        }

        /**
         * Show the form for editing the specified resource.
         * @return Response
         */
        public function edit()
        {
            return view('reservation::edit');
        }

        /**
         * Update the specified resource in storage.
         * @param  Request $request
         * @return Response
         */
        public function update(Request $request)
        {

        }

        /**
         * Remove the specified resource from storage.
         * @return Response
         */
        public function destroy()
        {

        }

        /**
         * Main route, index
         * @return \Illuminate\Http\JsonResponse
         */
        public function index()
        {
            $ms = microtime(true);

            $yieldObj         = new YieldReservation();
            $newBookingDetail = [
                'start_time' => '2018-07-30 11:30:00',
                'end_time'   => '2018-07-30 12:30:00',
                'party_size' => 2,
            ];
            $res              = $yieldObj->optimize($newBookingDetail,Auth::user()->restaurant_id);

            $data  = $res;
            $error = null;

            $me = microtime(true) - $ms;

            return response()->json(['data' => $data, 'errors' => $error, 'xtime' => $me]);
        }
    }
