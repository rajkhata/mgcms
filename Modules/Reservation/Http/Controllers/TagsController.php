<?php

namespace Modules\Reservation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Reservation\Entities\Tags;
use Modules\Reservation\Entities\RestaurantTags;
use DB;
class TagsController extends Controller
{
    public function __construct(Tags $tags,RestaurantTags $resttags){
      $this->obj=$tags;
      $this->resttags=$resttags;
    }
    public function index()
    {


        $tags= $this->obj->where('type','master')->latest()->get();

      $custom_tags= $this->obj->where(function($query){
            $query->where('type','custom');
            $query->where('created_by',\Auth::user()->restaurant_id);
       })->latest()->get();

        $restaurant_tags= $this->resttags->where('restaurant_id',\Auth::user()->restaurant_id)->first(['tags']);
        $restaurant_tags=$restaurant_tags?$restaurant_tags->toArray():[];       
      //  return view('reservation::tags.index',compact('tags','restaurant_tags','custom_tags'));
                return view('reservation::tags.restaurant_tags',compact('tags','restaurant_tags','custom_tags'));

    }

  
    
     public function customtags(Request $request,$tag)
    {
       
        $tags= $this->obj->where('name',$tag)->where(function($query){
            $query->where('type','custom');
            $query->where('created_by',\Auth::user()->restaurant_id)->orWhere(function($query){
                        $query->where('type','master');
           });
         })->latest()->first();

        if($tags){
              return \Response::json([ 'errors'=>['Tags already exists']], 422);
         }else{
             DB::beginTransaction();
                try {
                    $restaurantId = \Auth::user()->restaurant_id;
                    $data = new Tags;
                    $data->name = $tag;
                    $data->slug =strtolower($tag);
                    $data->type ='custom';
                    $data->created_by = $restaurantId;
                    $data->save();
                    // Add to restaurant_tags table
                    $tag_id      = $data->id;
                    $restTagInfo = RestaurantTags::where('restaurant_id', $restaurantId)->first();
                    if ($restTagInfo) {
                        $restTags          = json_decode($restTagInfo->tags, true);
                        if(!in_array($tag_id, $restTags)) {
                            array_push($restTags,$tag_id);
                            $restTagInfo->tags = json_encode($restTags);
                            $restTagInfo->save();
                        }
                    } else {
                        $restTags = RestaurantTags::create([
                            'restaurant_id' => $restaurantId,
                            'tags'  => json_encode((array)$tag_id)
                        ]);
                    }

                    DB::commit();
                    return \Response::json(['message'=>'Custom Tags added successfully!'], 200);

                }catch (\Exception $e){
                    
                    DB::rollback();
                   return \Response::json([ 'errors'=>['Tags could not added']], 422);

                }


        }
       
    }
   public function deletecustomtag(Request $request,$id)
    {

       DB::beginTransaction();
          try {
             
              $this->obj->where('created_by',\Auth::user()->restaurant_id)
                  ->where('id', $id)
                  ->delete();
              $this->resttags->where('restaurant_id',\Auth::user()->restaurant_id)->update(['tags' => DB::raw('JSON_REMOVE(tags , \'$."'.$id.'"\')')]);

             DB::commit();
              return \Response::json(['message'=>'Custom Tag deleted successfully!'], 200);

          }catch (\Exception $e){
              
              DB::rollback();
             return \Response::json([ 'errors'=>['Tags could not deleted']], 422);

          }

    }
     public function updatecustomtags(Request $request)
    {

      $validatedData = $request->validate(
                [
                  'tag_id' => 'required',
                  'tag_name' => 'required',
                ],
                [
                  'tags_id.required'=>'Tag id is required',
                  'tag_name.required'=>'Tag name is required',
                ]
            );

             DB::beginTransaction();
                try {
                   
                    $this->obj->where('created_by',\Auth::user()->restaurant_id)
                        ->where('id', $request->get('tag_id'))
                        ->update(['name' =>  $request->get('tag_name'),'slug'=>strtolower( $request->get('tag_name'))]);

                    DB::commit();
                    return \Response::json(['message'=>'Custom Tag updated successfully!'], 200);

                }catch (\Exception $e){
                    
                    DB::rollback();
                   return \Response::json([ 'errors'=>['Tag could not added']], 422);

                }


       
    }
    public function store(Request $request)
    {

            $validatedData = $request->validate(
                ['tags.*' => 'required'],
                ['tags.*.required'=>'Tags are required']
            );
            $restaurant_id=\Auth::user()->restaurant_id;
            
            $tags=$request->get('tags');
            $tags=array_keys($tags,1);

            $addTags = array_keys($request->input('tags'), 1);
            $delTags = array_keys($request->input('tags'), 0);
            $restTagInfo = RestaurantTags::where('restaurant_id', $restaurant_id)->first();

            if($restTagInfo) {
                $existingTags = json_decode($restTagInfo->tags, true);
                if(count($addTags)) {
                    $existingTags = array_unique(array_merge($existingTags, $addTags));
                }
                if(count($delTags)) {
                    $existingTags = array_values(array_diff($existingTags, $delTags));
                }
                $finalTagsArr = $existingTags;
            } else {
                $finalTagsArr = $tags;
            }
          
                DB::beginTransaction();
                try {

                     $floor=$this->resttags->updateOrCreate(
                        [ 'restaurant_id' =>$restaurant_id],
                        [
                        'restaurant_id' =>$restaurant_id,
                        'tags' =>json_encode($finalTagsArr),
                        
                       ]);
                    DB::commit();
                 return \Response::json(['message'=>'Tags updated successfully!'], 200);


                }catch (\Exception $e){
                    
                    DB::rollback();
                   return \Response::json([ 'errors'=>['Tags could not updated']], 422);

                }

           
      
       
       
    }

}
