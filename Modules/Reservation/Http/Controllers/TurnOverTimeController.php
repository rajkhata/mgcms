<?php

namespace Modules\Reservation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Reservation\Entities\TableType;
use Modules\Reservation\Entities\Table;
use DB;
use Modules\Reservation\Http\Requests\TableTypeRequest;


class TurnOverTimeController extends Controller
{
   
       public function __construct(TableType $tabletype){
            $this->obj=$tabletype;
       }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $table_type = TableType::where('restaurant_id', \Auth::user()->restaurant_id)->orderBy('max_seat', 'ASC')->get();
        $tables = array();
        $i=0;
        $total_tat=count($table_type);
        foreach ($table_type as $type){
            $tables['table_type_name'][] = $type->name;
           if(($i+1)==$total_tat){
                            $tables['min_max'][] = $type->min_seat.'+';

            }else{
                 $tables['min_max'][] = $type->min_seat.' - '.$type->max_seat;

            }
            $tables['tat'][] = $type->tat;
            $tables['id'][] = $type->id;
            $i++;
        }
         $tables['table_types']= $table_type->toArray();
        return view('reservation::turnovertime.index', compact('tables'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('workinghours::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $tat_data = $request->get('tat');
        if($tat_data){
            DB::beginTransaction();
            try {
                foreach ($tat_data as $table_id => $tat){
                    TableType::find($table_id)->update(['tat' => $tat]);
                }
                DB::commit();
                return redirect('configure/turnovertime')->with('success', 'You have successfully changed the turnover times.');
            }catch (\Exception $e){
                return redirect('configure/turnovertime')->with('danger', 'Turnover times could not be saved.');
                DB::rollback();
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('workinghours::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('workinghours::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(TableTypeRequest $request,$id)
    {
            $restaurant_id=\Auth::user()->restaurant_id;
            $name=$request->input('name');
            $req=$request->all();

            $tabletype=$this->obj->where('restaurant_id',$restaurant_id)->where('name',$name)->where('id','!=',$id)->first();
            
            if(isset($tabletype)){
                
                return \Response::json([ 'errors'=>['Table type name already exists.']], 422);

            }else{
                DB::beginTransaction();
                try {
                   
                    $floor=$this->obj->where('restaurant_id',$restaurant_id)->where('id',$id)->update([
                        'name' => $name,
                        'min_seat' =>$req['min'],
                        'max_seat' =>$req['max'],
                        'tat' =>$req['tat'],
                        ]);

                    DB::commit();
                    return \Response::json(['message'=>'Table type updated'], 200);

                }catch (\Exception $e){

                    DB::rollback();
                    return \Response::json([ 'errors'=>$e->getMessage()], 422);
                }
            } 
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */

    public function destroy($id)
    {
            DB::beginTransaction();
            try {
                /*$tablecheck=Table::where('restaurant_id', \Auth::user()->restaurant_id)->where('type_id',$id)->first(['id']);
                if( $tablecheck){
                return \Response::json([ 'errors'=>['Please first delete the table using this table type']], 422);

                }else{*/
                    $deleted=$this->obj->where('restaurant_id', \Auth::user()->restaurant_id)
                     ->where('id',$id)->delete();
                    DB::commit();  
                                    return \Response::json(['message'=>'Table type deleted'], 200);

                //}
               
              

            }catch (\Exception $e){

                DB::rollback();
                return \Response::json([ 'errors'=>$e->getMessage()], 422);

            }

    }


    public function add(TableTypeRequest $request)
    {

            $restaurant_id=\Auth::user()->restaurant_id;
            $name=$request->input('name');
            $req=$request->all();

            $tabletype=$this->obj->where('restaurant_id',$restaurant_id)->where('name',$name)->first();
            
            if(isset($tabletype)){
                
                return \Response::json([ 'errors'=>['Table type name already exists.']], 422);

            }else{
                DB::beginTransaction();
                try {
                   
                    $floor=$this->obj->create([
                        'name' => $name,
                        'restaurant_id' =>$restaurant_id,
                        'min_seat' =>$req['min'],
                        'max_seat' =>$req['max'],
                        'tat' =>$req['tat'],
                        ]);

                    DB::commit();
                    return \Response::json(['message'=>'Table type added'], 200);

                }catch (\Exception $e){

                    DB::rollback();
                    return \Response::json([ 'errors'=>$e->getMessage()], 422);
                }
            }      
    }

    public function getAllTableTypeInfo($restaurant_id)
    {
        $table_types = [];
        $all_table_type = TableType::where('restaurant_id', $restaurant_id)->get()->toArray();
        foreach ($all_table_type as $table_type){
            $table_types[$table_type['id']] = $table_type;
        }
        return $table_types;
    }

    public function getTableTypeIdByPartySize($restaurant_id, $party_size)
    {
        return TableType::where('restaurant_id', $restaurant_id)->where('min_seat', '<=', $party_size)->where('max_seat', '>=', $party_size)->value('id');
    }
}
