<?php

namespace Modules\Reservation\Http\Controllers;

use App\Helpers\CommonFunctions;
use App\Models\CmsUser;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Reservation\Entities\CombineTable;
use Modules\Reservation\Entities\CustomWorkingHours;
use Modules\Reservation\Entities\Floor;
use Modules\Reservation\Entities\Table;
use Modules\Reservation\Entities\ReservationStatus;
use Modules\Reservation\Entities\Reservation;
use Carbon\Carbon;
use Modules\Reservation\Entities\TableType;
use Modules\Reservation\Entities\RestaurantTags;
use Modules\Reservation\Entities\Tags;
use DB;
use Modules\Reservation\Entities\WorkingHours;
use Validator;

class FloorController extends Controller
{
    public function __construct(Floor $floorobj,Table $table)
    {
        $this->obj=$floorobj;
        $this->tableobj=$table;
    }

    
    public function index()
    {
        $floors= $this->obj->where('restaurant_id',\Auth::user()
                           ->restaurant_id)->latest()->get();
        if(count($floors)){
            $floors_list=$floors->toArray();
            $floor_names=array_column($floors_list, 'name');
            return view('reservation::floor.listing',compact('floors','floor_names'));
        }else{         
           return redirect()->route('welcomefloor');
        }        
    }

    public function isCurrentOrUpcomingReservationExist($floor_id=null,$tables=[])
    {
        if(count($tables)){
            $tables = implode(',', $tables);
        }

        if($this->isCurrentReservationExist($floor_id, $tables)){
            return 'current_reservation';
        }

        if($this->isUpcomingReservationExist($floor_id, $tables)){
            return 'upcoming_reservation';
        }
        return false;
    }

    private function isCurrentReservationExist($floor_id=null,$tables=[])
    {
        $status_list = ReservationStatus::getStatusForCurrentReservation();
        $current=Reservation::where('restaurant_id', \Auth::user()->restaurant_id);

        if($floor_id!=null){
            $current=$current->where('floor_id',$floor_id);
        }
        if(!empty($tables)){
            $current=$current->whereRaw("FIND_IN_SET('".$tables."', table_id)");
        }
        $current=$current->where('start_time', '<=', Carbon::now())
            ->where('end_time', '>=', Carbon::now())
            ->whereIn('status_id', [$status_list[config('reservation.status_slug.confirmed')], $status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.partially_seated')], $status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')], $status_list[config('reservation.status_slug.running_late')]])
            ->first();
        if( $current){
            return true;
        }
        return false;
    }

    private function isUpcomingReservationExist($floor_id=null,$tables=[])
    {
        $status_list = ReservationStatus::getStatusForUpcomingReservation();
        $upcoming=Reservation::where('restaurant_id', \Auth::user()
            ->restaurant_id);
        if($floor_id!=null){
            $upcoming=$upcoming->where('floor_id',$floor_id);
        }
        if(!empty($tables)){
            $upcoming=$upcoming->whereRaw("FIND_IN_SET('".$tables."', table_id)");
        }
        $upcoming=$upcoming->where('start_time', '>', Carbon::now())
            ->whereIn('status_id', [$status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.confirmed')]])
            ->first();
        if( $upcoming){
            return true;
        }
        return false;
    }

    public function tableDelete($id)
    {
        DB::beginTransaction();
            try {
               /* Deleting dependent fields as well like reservation*/
               $resp=$this->isCurrentOrUpcomingReservationExist(null,[$id]);
               if($resp===false){
                   $this->changeWorkingHoursSetting($id);
                   $this->tableobj->where('restaurant_id', \Auth::user()->restaurant_id)
                    ->where('id',$id)->delete();
                  DB::commit();
                return \Response::json(['message'=>'Table  deleted'], 200);
               }elseif ($resp=='current_reservation'){
                   response()->json([ 'errors'=>["The table cannot be deleted as it has ongoing reservations."]], 422)->send();
                   die();
               }else{
                   response()->json([ 'errors'=>["The table cannot be deleted as it has upcoming reservations. Move reservations on this table to another table and try deleting."]], 422)->send();
                   die();
               }
            }catch (\Exception $e){
                DB::rollback();
                return \Response::json([ 'errors'=>['PLease try again later']], 422);
            }
    }

    public function tableUpdate(Request $request, $id)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $data = $request->all();
        if (count($data) && !empty($data['tableText'])) {
            if (!isset($data['id']) || (isset($data['id']) && $id != $data['id'])) {
                return \Response::json(['success'=>false, 'message' => 'Invalid request'], 422);
            }
            $table = $this->tableobj->where('restaurant_id', $restaurant_id)->where('id', '!=', $id)->where('name', $data['tableText']);
            if(isset($data['floorId']) && !empty($data['floorId'])){
                $table = $table->where('floor_id', $data['floorId']);
            }
            $table = $table->first();
            if (!$table) {
                $resp = $this->isCurrentOrUpcomingReservationExist(null, [$id]);
                if ($resp === false) {
                    try {
                        DB::beginTransaction();
                        $floor_table = $this->tableobj->where('restaurant_id', $restaurant_id)->where('id', $id)->first();
                        //\Log::info($floor_table->max."**".$data['maxGuests']);
                        if($floor_table->max!=$data['maxGuests']){
                            $this->changeWorkingHoursSetting($id);
                        }
                        $floor_table->max=$data['maxGuests'];
                        $floor_table->min=$data['minGuests'];
                        $floor_table->layout = strtolower($data['tableShape']);
                        $floor_table->name = $data['tableText'];
                        $floor_table->configuration = json_encode($data);
                        $floor_table->save();
                        DB::commit();
                        return \Response::json(['success'=>true, 'message' => 'Table  updated'], 200);
                    } catch (\Exception $e) {
                        DB::rollback();
                        return \Response::json(['success'=>false, 'errors' => $e->getMessage()], 422);
                    }
                } elseif ($resp == 'current_reservation') {
                    response()->json(['success'=>false, 'errors' => ["The table cannot be edited as it has ongoing reservations."]], 422)->send();
                    die();
                } else {
                    response()->json(['success'=>false, 'errors' => ["The table cannot be edited as it has upcoming reservations. Move reservations on this table to another table and try deleting."]], 422)->send();
                    die();
                }
            } else {
                return \Response::json(['success'=>false, 'errors' => ['Table name is already exists']], 422);
            }
        } else {
            return \Response::json(['success'=>false, 'errors' => ['Nothing to update']], 422);
        }
    }

      /*
     *Display the welcome page for floor module
     */
    public function welcome()
    {
        return view('reservation::floor.welcome');
    }

    /**
     * Show first step form for floor/patio.
     * @return Response
     */
    public function create($type)
    {
        $floors= $this->obj->where('restaurant_id',\Auth::user()
            ->restaurant_id)->select('id', 'name')->get()->toArray();
        return view('reservation::floor.create',compact('type', 'floors'));
    }

    /**
     * Show floor/patio  configure screen step2.
     */
    public function configure($floor_id)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $floors= $this->obj->where('restaurant_id', $restaurant_id)->select('id', 'name')->get()->toArray();
        $floor=$this->obj->where('restaurant_id', $restaurant_id)->where('id',$floor_id)->with('tables')->first();

        $floor_data=[];
        $floor_data[0]=isset($floor->layout_config)?$floor->layout_config:null;

        $floor_data[1]=isset($floor->tables)?array_column($floor->tables->toArray(),'configuration'):null;
        $resp = $this->isCurrentOrUpcomingReservationExist($floor_id);
        $floor_data[2]['reservations']['current_reservation'] = ($resp=='current_reservation')?true:false;
        $floor_data[2]['reservations']['upcoming_reservation'] = ($resp=='upcoming_reservation')?true:false;
        $floor_data=json_encode($floor_data);
        $table_combinations = '';
        //Table combination list
        //$table_combinations = app('Modules\Reservation\Http\Controllers\TableCombineController')->getTableCombinationByFloor($restaurant_id, $floor_id);
        return view('reservation::floor.configure_floor',compact('floor','floor_data', 'floors', 'table_combinations'));
    }

    public function getTablestoDelete($floor_id,$excluding)
    { 
        $restaurant_id=\Auth::user()->restaurant_id;
        return $tables=$this->tableobj->where('restaurant_id',$restaurant_id)
                      ->where('floor_id',$floor_id)->whereNotIn('id',$excluding)->get(['id']);

    }

    public function nameValiationTables($floor_id,$excluding)
    { 
        $restaurant_id=\Auth::user()->restaurant_id;
        $table=$this->tableobj->where('restaurant_id',$restaurant_id)->where('floor_id',$floor_id)->whereNotIn('id',$excluding)->get(['id']);

        if( $table){
            return \Response::json([ 'errors'=>[$name.' already exists']], 422);
        }        
    }

    /**
     * Save floor/patio  configuration step2.
     */

    public function saveFloorConfig(Request $request,$floor_id)
    { 

          if(isset($floor_id)){
              $restaurant_id=\Auth::user()->restaurant_id;
              $data=$request->all();
  
            if(isset($data[1]) && !empty($data[1])){       

                 $validator = Validator::make($data[0], [
                    'floorType' => 'required',
                    'floorTypeColumn' => 'required',
                    'floorTypeRow' => 'required',
                     'floorName' => 'required|max:50',

                ]);

                if ($validator->fails()) {
                  
                    return \Response::json([ 'errors'=>$validator], 422);
                }
                   $floor=$this->obj->where('restaurant_id',$restaurant_id)->where('id','!=',$floor_id)->where('name',$data[0]['floorName'])->first(['id']);
                if(!$floor){
                 DB::beginTransaction();
                
                try {
                   
                $floor=$this->obj->where('restaurant_id',$restaurant_id)->where('id',$floor_id)->update([
                        'layout' =>strtolower($data[0]['floorType']),
                        'name'=> $data[0]['floorName'],
                        'completed_step' =>2,
                        'layout_config' =>json_encode($data[0]),
                        ]);
                if($floor){
                    $all_tables=$data[1];                   
                    $validator = Validator::make($all_tables, [
                        '*.id' => 'required:integer',
                       // '*.tableType' => 'required:integer',
                        '*.tableText' => 'required|max:50',
                        '*.minGuests' => 'required:integer',
                        '*.maxGuests' => 'required:integer',
                    ]);
                    
                     if ($validator->fails()) {
                  
                        return \Response::json([ 'errors'=>['Some required field missings']], 422);
                     }
                    
                    $tablenames=array_column($all_tables,'tableText');
                    foreach(array_count_values($tablenames) as $val => $c){                       
                           if($c>1){
                             return \Response::json([ 'errors'=>['Some duplicate table found']], 422);
                           }
                    }
     
                    $newtables=[];
                    $oldtables=[];
                    $alreadytables=[];
                    foreach($all_tables as $table){

                        $name=$table['tableText'];                     
                        $id=$table['id'];
                       // $tableType=$table['tableType'];

                        if(isset($table['id']) && $table['id']==0 ){                            
                            
                            $newtables[]=[
                                 'restaurant_id'=>$restaurant_id, 
                                 'floor_id'=>$floor_id,
                                 'name'=>$name,
                                 //'type_id'=>$tableType,
                                 'min'=>$table['minGuests'],
                                 'max'=>$table['maxGuests'],
                                 'layout'=>strtolower($table['tableShape']),
                                 'configuration'=>json_encode($table),

                            ];
                              /*
                               $this->tableobj->restaurant_id=$restaurant_id;
                                $this->tableobj->floor_id=$floor_id;
                                $this->tableobj->name=$name;
                                $this->tableobj->type_id=$tableType;
                                $this->tableobj->configuration=json_encode($table);
                                $this->tableobj->save();
                                $table['id']=$this->tableobj->id;
                                $this->tableobj->configuration=json_encode($table);
                                $this->tableobj->save();              

                              */
                        
                        }else if(isset($table['id']) && $table['id']!=0 ){
                            $alreadytables[]=$table['id'];
                            $oldtables[]=[
                                 'id'=>$table['id'],
                                 'configuration'=>json_encode($table),

                            ];


                        }                       
                    }
                    //print_r($alreadytables);
                    $alltables=$this->getTablestoDelete($floor_id,$alreadytables);
                    //print_r($alltables);
                    $todelete=[];
                    if( $alltables){
                        $todelete= $alltables->toArray();
                        
                    }

                    //\Log::info(array_column($todelete, 'id'));
                    if(count($todelete)){
                        $resp=$this->isCurrentOrUpcomingReservationExist($floor_id, array_column($todelete, 'id'));
                        if( $resp===false){
                            $newtable=new Table();
                            $newtable->where('restaurant_id',$restaurant_id)->where('floor_id',$floor_id)->whereIn('id',$todelete)->delete();

                        }
                    }

                    
                   
                  if(count($newtables)){
                      foreach($newtables as $tab){
                          $newtable=new Table();
                          $newtable->restaurant_id=$tab['restaurant_id'];
                          $newtable->floor_id=$tab['floor_id'];
                          $newtable->name=$tab['name'];
                          $newtable->layout=$tab['layout'];
                         // $newtable->type_id=$tab['type_id'];
                           $newtable->min=$tab['min'];
                          $newtable->max=$tab['max'];
                          $newtable->configuration=$tab['configuration'];
                          $newtable->save();
                          $configuation=json_decode($tab['configuration'],true);
                          $configuation['id']=$newtable->id;
                          $newtable->configuration=json_encode($configuation);
                          $newtable->save();

                      }

                  }

                    if(count($oldtables)){
                        foreach($oldtables as $tab){

                            $newtable=new Table();
                        $newtable->where('restaurant_id',$restaurant_id)->where('floor_id',$floor_id)->where('id',$tab['id'])
                        ->update(
                           [
                           'configuration'=>$tab['configuration'],
                           ]
                       );

                        }

                    }



                    DB::commit();
                    return \Response::json(['title'=>'Floor Updated','message'=>'Floor configuration updated successfully'], 200);

                }

                }catch (\Exception $e){

                    DB::rollback();
                    return \Response::json([ 'errors'=>$e->getMessage()], 422);
                }
            }else{
                    return \Response::json([ 'errors'=>['Floor Name already exists. Please provide unique Floor Name.']], 422);

            }
            } 
          }
    }


    /**
     * Save the floor name step1.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request,$type)
    {
       if(isset($type)){

            $validatedData = $request->validate(
                ['name' => 'required|max:50'],
                ['name.required'=>'Please enter the '.ucfirst($type).' name.']
            );
            $restaurant_id=\Auth::user()->restaurant_id;
            $name=$request->input('name');
            $foor=$this->obj->where('restaurant_id',$restaurant_id)->where('name',$name)->first();

            if(isset($foor)){
                return back()->withInput()->withErrors(array('message' => ucfirst($type) .' Name already exists. Please provide unique '.ucfirst($type).' Name.'));
            }else{
               
                DB::beginTransaction();
                try {
                     $floor=$this->obj->create([
                    'name' => $name,
                    'restaurant_id' =>$restaurant_id,
                    'type' =>$type,
                    'completed_step' =>1,
                 ]);
                    DB::commit();
                    return redirect()->route('configure_floor', ['floor_id' =>  $floor->id]);
                   // return redirect('floor')->withInput()->with('status','Floor added');


                }catch (\Exception $e){
                    
                    DB::rollback();
                    return back()->withInput()->withErrors(array('message' => 'Try another floor name'));
                }

            }
      
       }      
       
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('reservation::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('reservation::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($floor_id)
    {

        DB::beginTransaction();
            try {
               $resp=$this->isCurrentOrUpcomingReservationExist($floor_id,[]);
         
                if($resp===false){
                   
                $this->tableobj->where('restaurant_id', \Auth::user()->restaurant_id)
                 ->where('floor_id',$floor_id)->delete();   

                $this->obj->where('restaurant_id', \Auth::user()->restaurant_id)
                ->where('id',$floor_id)->delete();
             
                 DB::commit();
               return \Response::json(['title'=>'Floor Deleted','message'=>'Floor has been deleted successfully'], 200);
                }elseif ($resp=='current_reservation'){
                    response()->json([ 'errors'=>["The floor cannot be deleted as it has ongoing reservations."]], 422)->send();
                    die();
                }else{
                    response()->json([ 'errors'=>["The floor cannot be deleted as it has upcoming reservations. Move reservations on this floor to another floor and try deleting."]], 422)->send();
                    die();
                }
            }catch (\Exception $e){

                DB::rollback();
              return \Response::json([ 'errors'=>['Unable to delete the floor']], 422);

            }
    }


    public function duplicate(Request $request, $floor_id)
    {
        $floorname = $request->input('floorname');
        $validatedData = $request->validate(
            ['floorname' => 'required|max:50'],
            ['floorname.required' => 'Please enter the floor name.']
        );
        $restaurant_id = \Auth::user()->restaurant_id;

        $floor_check = $this->obj->where('restaurant_id', $restaurant_id)->where('name', $floorname)->first();

        if (isset($floor_check)) {
            return \Response::json(['errors' => ['Floor Name already exists. Please provide unique Floor Name.']], 422);
        } else if (!$floor_id && !$floorname) {
            return \Response::json(['errors' => ['Invalid request ']], 422);
        } else {
            DB::beginTransaction();
            try {
                $floorobj = $this->obj->where('restaurant_id', \Auth::user()->restaurant_id)
                    ->where('id', $floor_id)->with('tables')->first();

                if ($floorobj) {

                    $configuation = json_decode($floorobj->layout_config, true);
                    $configuation['floorName'] = $floorname;
                    $configuation = json_encode($configuation);

                    $floor = $this->obj->create([
                        'name' => $floorname,
                        'restaurant_id' => $floorobj->restaurant_id,
                        'type' => $floorobj->type,
                        'layout' => $floorobj->layout,
                        'completed_step' => $floorobj->completed_step,
                        'duplicate_from_id' => $floor_id,
                        'layout_config' => $configuation,
                    ]);

                    if ($floor && (isset($floorobj->tables) && count($floorobj->tables) > 0)) {
                        foreach ($floorobj->tables as $tbl) {
                            $newtable = new Table();
                            $newtable->restaurant_id = $tbl->restaurant_id;
                            $newtable->floor_id = $floor->id;
                            $newtable->name = $tbl->name;
                            //$newtable->type_id=$tbl->type_id;
                            $newtable->min = $tbl->min;
                            $newtable->max = $tbl->max;
                            $newtable->layout = $tbl->layout;
                            $newtable->configuration = $tbl->configuration;
                            $newtable->save();
                            $configuation = json_decode($tbl->configuration, true);
                            $configuation['id'] = $newtable->id;
                            $newtable->configuration = json_encode($configuation);
                            $newtable->save();
                        }
                    }
                    DB::commit();
                    return \Response::json(['floor_id' => $floor->id, 'title' => 'Duplicate Floor Created', 'message' => ' A duplicate floor plan has been created.'], 200);
                }
            } catch (\Exception $e) {
                DB::rollback();
                return \Response::json(['errors' => ['Unable to duplicate the floor']], 422);
            }
        }
    }

    public function getTables($id)
    {
        try {
            $tables = Table::where('floor_id', $id)->get();
            return \Response::json(['success' => true, 'data' => $tables], 200);
        }catch (\Exception $e){
            return \Response::json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    public function listFloorReservations(Request $request, $id)
    {
        try {
            $date = $request->get('date');
            $type = $request->get('type')?$request->get('type'):null;
            $floodata=$this->getReservationByFloor($id, $date,null, $type);
            return \Response::json(['data'=>$floodata], 200);
        }catch (\Exception $e){
            return \Response::json([ 'errors'=>['No reservation found']], 422);
        }
    }

    public function getReservationByFloor($floor_id,$date=null,$status=null,$type='current', $timer_type=null)
    {
        $restaurant_id=\Auth::user()->restaurant_id;
        $date = empty($date)?CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d'):$date;
        if($date == 'Today'){
            $date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d');
        } elseif ($date == 'Tomorrow') {
            $date = Carbon::tomorrow()->toDateString();
        }else{
            $date = Carbon::createFromFormat('m-d-Y', $date)->format('Y-m-d');
        }
        if(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d')!=$date || $type==config('reservation.grid_view')){
            $floor =$this->obj->select(['id', 'type','completed_step','layout_config'])->where('restaurant_id', $restaurant_id)
                ->with(['tables'=>function($query){
                    $query->select(['floor_id','id', 'configuration']);
                }])
                ->where('id',$floor_id)
                ->first();
        }else if ($type == config('reservation.reservation_category.current')) {
            $floor =$this->obj->select(['id', 'type','completed_step','layout_config'])->where('restaurant_id', $restaurant_id)
                ->with(['tables'=>function($query){
                    $query->select(['floor_id','id', 'configuration']);
                }])->where('id',$floor_id)->first();

        }elseif ($type == config('reservation.reservation_category.upcoming')) {
            $floor =$this->obj->select(['id', 'type','completed_step','layout_config'])->where('restaurant_id', $restaurant_id)
                ->with(['tables'=>function($query){
                    $query->select(['floor_id','id', 'configuration']);
                }])->where('id',$floor_id)->first();
        }else{
            $floor =$this->obj->select(['id', 'type','completed_step','layout_config'])->where('restaurant_id', $restaurant_id)
                ->with(['tables'=>function($query){
                    $query->select(['floor_id','id', 'configuration']);
                }])->where('id',$floor_id)->first();
        }
        $floor_data=[];
        if($type==config('reservation.grid_view')){
            $working_hours_slot_range = $this->getWorkingHoursSlotRange($date);
        }
        if( $floor){
            $floor= $floor->toArray();
            $floor_data[0]=isset($floor['layout_config'])?$floor['layout_config']:null;
            $tables=[];
            if(count($floor['tables'])){
                $i=0;
                foreach($floor['tables'] as $tbl){
                    $config =$tbl['configuration']?json_decode($tbl['configuration'],true):null;
                    $config['id']=$tbl['id'];
                    $server = $this->getFloorTableServers($date, $floor_id, $tbl['id']);
                    $config['server'] = $server;
                    $table_reservation = $this->getTableReservations($tbl['id'], $type, $date);
                    if($type==config('reservation.grid_view')){
                        $config['reservations'] = $this->getGridReservation($tbl['id'], $table_reservation, $working_hours_slot_range, $date);
                    }else{
                        $config['reservations']=$table_reservation;
                    }
                    $config['timer'] = $this->getTableTimer($restaurant_id, $date, $tbl['id'], $timer_type);
                    $config['tableAvailability']=count($table_reservation)?$table_reservation[0]->status->slug:'Available';
                    $config['blocked']= app('Modules\Reservation\Http\Controllers\BlockTableController')->isTableAlreadyBlocked($restaurant_id, $floor_id, $tbl['id'], $date);
                    $tables[]=json_encode($config);
                    $server_name_arr[$i]['name'] = $server[0]['serverFullName'];
                    $server_name_arr[$i]['color'] = $server[0]['serverTableColor'];
                    $i++;
                }

            }
            //\Log::info($tables);
            $tables = $this->setLinkIconColorForMultipleTable($tables);
            $floor_data[1]=isset($tables)?$tables:null;
            $floor_data[2]=ReservationStatus::getAllStatus()->toJson();
            if(isset($tables) && $type==config('reservation.grid_view')){
                $floor_data[3] = ($this->getFlorCoverCountForGrid($floor_id, $date));
            }
            if(isset($server_name_arr) && !empty($server_name_arr)){
                $floor_data[4] = array_unique($server_name_arr, SORT_REGULAR);
            }
            //$current_time_slot_range = app('Modules\Reservation\Http\Controllers\ReservationController')->getCurrentSlotRange($restaurant_id, $date);
            //$floor_data[5] = $current_time_slot_range;
        }
        return  $floor_data=json_encode($floor_data);
    }

    public function gettagslist()
    {
          $restaurant_id=\Auth::user()->restaurant_id;

            $tags=RestaurantTags::where('restaurant_id',$restaurant_id)->first(['tags']);
            
            if(!isset($tags) || (isset( $tags) && empty( $tags['tags']))){
                
                return [];


            }else{
                $tags_data=[];
                $tags_array=json_decode($tags['tags'],true);
                $tags=Tags::whereIn('id',$tags_array)->get(['id','name']);
                foreach ($tags as $value) {
                  $tags_data[]=$value->name;
                }
               return $tags_data;
                
            } 
    }
    public function getFloorView($id)
    {
       $tags=$this->gettagslist();
      
      $restaurant_id = \Auth::user()->restaurant_id;
      $date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d');
      $date_start_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 00:00:01"));
      $floor_data=$this->getReservationByFloor($id, Carbon::createFromFormat('Y-m-d', $date)->format('m-d-Y'));
      $status_current = ReservationStatus::getStatusForCurrentReservation();

       $current_reservations = Reservation::with(['user', 'status', 'table'])->where('restaurant_id', \Auth::user()->restaurant_id)->where(function ($query) {
           $query->where('payment_status', '!=', config('reservation.payment_status.pending'))->orWhereNull('payment_status');
       })->where('floor_id',$id)->whereDate('start_time', '=', $date_start_time->format('Y-m-d'))->where(function ($query) use ($status_current, $restaurant_id, $date_start_time) {
            return $query->where('start_time', '<=', Carbon::now())->where('end_time', '>=', Carbon::now())->whereIn('status_id', [$status_current[config('reservation.status_slug.confirmed')], $status_current[config('reservation.status_slug.reserved')], $status_current[config('reservation.status_slug.partially_seated')], $status_current[config('reservation.status_slug.seated')], $status_current[config('reservation.status_slug.starters')], $status_current[config('reservation.status_slug.entree')], $status_current[config('reservation.status_slug.main_course')], $status_current[config('reservation.status_slug.dessert')], $status_current[config('reservation.status_slug.bill_dropped')], $status_current[config('reservation.status_slug.running_late')], $status_current[config('reservation.status_slug.paid')]])->orWhere(function ($query) use($status_current){
                $query->where('start_time', '<', Carbon::now())->whereIn('status_id', [$status_current[config('reservation.status_slug.seated')], $status_current[config('reservation.status_slug.starters')], $status_current[config('reservation.status_slug.entree')], $status_current[config('reservation.status_slug.main_course')], $status_current[config('reservation.status_slug.dessert')], $status_current[config('reservation.status_slug.bill_dropped')], $status_current[config('reservation.status_slug.paid')]]);
            });
        })->orderBy('start_time', 'asc')->get();
        $hosts = CmsUser::where('restaurant_id', \Auth::user()->restaurant_id)->pluck('name', 'id');
        $all_special_occasion = config('reservation.special_occasion');
        $current_reservations_data = $this->getReservationArr($current_reservations, config('reservation.reservation_category.current'));
        $floor_data_arr = json_decode($floor_data, true);
        $servers = !empty($floor_data_arr) ? (isset($floor_data_arr[4]) && !empty($floor_data_arr[4]) ? $floor_data_arr[4]: []) : [];
        $current_time_slot_range = app('Modules\Reservation\Http\Controllers\ReservationController')->getCurrentSlotRange($restaurant_id, Carbon::parse($date_start_time)->format('Y-m-d'));
        return view('reservation::table-management.index', compact('floor_data', 'current_reservations_data', 'all_special_occasion', 'hosts','tags', 'servers', 'current_time_slot_range'))->with('current_floor',$id);
    }

    public function getFloorReservations($floor_id, Request $request)
    {
        $date = $request->has('date')?$request->get('date'):null;
        $restaurant_id = \Auth::user()->restaurant_id;
        $type = $request->get('type');
        $keyword = $request->get('keyword');
        $date = empty($date)?Carbon::today()->toDateString():$date;
        if($date == 'Today' || $date == NULL){
            $date = Carbon::today()->format('Y-m-d');
        } elseif ($date == 'Tomorrow') {
            $date = Carbon::tomorrow()->format('Y-m-d');
        } else {
            $date = Carbon::createFromFormat('m-d-Y', $date)->format('Y-m-d');
        }
        //\Log::info($date);
        $date_start_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 00:00:00"));
        $date_end_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 24:00:00"));
        //\Log::info(Carbon::parse($date." 00:00:01"));
        //\Log::info(CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 23:59:59")));
        $reservations = Reservation::where('restaurant_id', \Auth::user()->restaurant_id);
        if($type != config('reservation.reservation_category.waitlist')) {
            $reservations = $reservations->where('floor_id', $floor_id)->where(function ($query) {
                $query->where('payment_status', '!=', config('reservation.payment_status.pending'))->orWhereNull('payment_status');
            });
        }
        $reservations = $reservations->with(['user', 'status', 'table']);

        if ($keyword) {
            $keyword = trim($keyword);
            $reservations = $reservations->where(function ($query) use ($keyword) {
                return $query->where('fname', 'like', '%' . $keyword . '%')
                    ->orWhere('lname', 'like', '%' . $keyword . '%')
                    ->orWhere(DB::raw("CONCAT( fname,  ' ', lname )"), 'like', '%' . $keyword . '%')
                    ->orWhere('mobile', 'like', '%' . $keyword . '%')
                    ->orWhere('receipt_no', 'like', '%' . $keyword. '%');
            });
        }
        if ($type == config('reservation.reservation_category.current')) { //Need some changes in logic
            $status_list = ReservationStatus::getStatusForCurrentReservation();
            $reservations = $reservations->whereDate('start_time', '=', $date_start_time->format('Y-m-d'))->where(function ($query) use ($status_list, $restaurant_id, $date_end_time, $date_start_time) {
                return $query->where('start_time', '<=', Carbon::now())->where('end_time', '>', Carbon::now())
                ->whereIn('status_id', [$status_list[config('reservation.status_slug.confirmed')], $status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.partially_seated')], $status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.paid')]])->orWhere(function ($query) use($status_list){
                        $query->where('start_time', '<', Carbon::now())->whereIn('status_id', [$status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')]]);
                    });
            })->orderBy('start_time', 'asc')->get();
        } elseif ($type == config('reservation.reservation_category.upcoming')) {
            $status_list = ReservationStatus::getStatusForUpcomingReservation();
            //\Log::info([$date_start_time->format('Y-m-d'), Carbon::today()->toDateString()]);
            if($date_start_time->format('Y-m-d') > Carbon::today()->toDateString()){
                $reservations = $reservations->where('start_time', '<', $date_end_time)->where('end_time', '>=', $date_start_time);
            }else{
                $reservations = $reservations->where('start_time', '>', Carbon::now())->where('end_time', '<=', $date_end_time);
            }
            $reservations = $reservations->whereIn('status_id', [$status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.confirmed')]])->get();
        } else{
            $status_list = ReservationStatus::getStatusForWaitlistReservation();
            $reservations = $reservations->where('start_time', '<', $date_end_time)/*->where('end_time', '>=', $date_start_time)/*->where('source', config('reservation.source.walk_in'))*/->where(function ($query) use ($status_list) {
                return $query->where('status_id', $status_list[config('reservation.status_slug.waitlist')]);
            })->get();
        }
        //\Log::info($status_list);
        return $this->getReservationArr($reservations, $type);
    }

    public function getNextReservationStatuses($reservationDetail, $type, $date)
    {
        $current_status = $reservationDetail->status->name;
        $restaurant_id = \Auth::user()->restaurant_id;
        $current_date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d');

        // if reservation is past date
        if($reservationDetail->payment_status==config('reservation.payment_status.pending')){
            //$next_reservation_status = (array)$current_status;
            $next_reservation_status[] = 'Cancelled';
            //$next_reservation_status = array_unique($next_reservation_status);
        }else{
            $all_status_list = array_flip(config('reservation.status'));
            if($type == config('reservation.reservation_category.archive')){
                $next_reservation_status[] = 'Finished';
            }else{
                $next_reservation_status = config('reservation.'.$type."_next_status.".$all_status_list[$current_status]);
                if($type==config('reservation.reservation_category.upcoming') && CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, Carbon::parse($date))->format('Y-m-d') > $current_date ){
                    //\Log::info('not here');
                    $next_reservation_status = config('reservation.future_upcoming_next_status.'.$all_status_list[$current_status]);
                }
                $next_reservation_status = array_values($next_reservation_status);
            }
        }
        $reservation_status = ReservationStatus::whereIn('name', $next_reservation_status)->get(['name', 'id', 'color', 'icon'])->toArray();
        foreach ($reservation_status as $key=>$status){
            $icon = json_decode($status['icon'], true);
            $color = json_decode($status['color'], true);
            $reservation_status[$key]['icon'] = $icon['sidebar'];
            $reservation_status[$key]['color'] = $color['sidebar'];
        }
        return $reservation_status;
    }

    public function getReservationTableInfo($table_id, Request $request){
       
        $table=$this->tableobj->where('restaurant_id', \Auth::user()->restaurant_id)->where('id',$table_id)->first();
        $restaurant_id = \Auth::user()->restaurant_id;
        $total_reservations=0;
        $total_covers=0;
        if($table){
            
              $resp='<div class="modal-body scroller"> <div class="row table-popup-contain"> <div class="seated-table__sec2">';
            $newwalkin='';
            /******************Current Reservations*******************************/
             $date=$request->get('date');
             $date = ($date=='Today' || $date==NULL)?Carbon::today()->toDateString():$date;
             $date =($date=='Tomorrow') ? Carbon::tomorrow()->toDateString():$date;

            $date_start_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 00:00:00"));
            $date_end_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 24:00:00"));
            //\Log::info([Carbon::today()->toDateString(), $date_start_time->format('Y-m-d')]);

            if($date_start_time->format('Y-m-d') == Carbon::today()->toDateString()){
                //\Log::info([Carbon::today()->toDateString(), date('Y-m-d', strtotime($date))]);
                $newwalkin='<a href="#" class="btn border--btn walkinnew new_walkin_button" data-toggle="modal"  data-target="#newWalkinModal" floor-id="'.$table->floor_id.'" table-id="'.$table->id.'">New Walk-in</a>';
                $status_list = ReservationStatus::getStatusForCurrentReservation();
                $current_reservation = Reservation::with(['user', 'status', 'table'])->where('restaurant_id', \Auth::user()->restaurant_id)->where(function ($query) {
                    $query->where('payment_status', '!=', config('reservation.payment_status.pending'))->orWhereNull('payment_status');
                })->whereRaw("FIND_IN_SET('".$table_id."', table_id)")->whereDate('start_time', '=',$date_start_time->format('Y-m-d'))->where(function ($query) use ($status_list) {
                    return $query->where('start_time', '<=', Carbon::now())->where('end_time', '>=', Carbon::now())->whereIn('status_id', [$status_list[config('reservation.status_slug.confirmed')], $status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.partially_seated')], $status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')], $status_list[config('reservation.status_slug.running_late')]])->orWhere(function ($query) use($status_list) {
                        $query->where('start_time', '<', Carbon::now())->whereIn('status_id', [$status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')]]);
                    });
                })->orderBy('start_time', 'asc')->get();
                //\Log::info($current_reservation);

                $reservation_data_current = $this->getReservationArr($current_reservation, config('reservation.reservation_category.current'));
                if(count($reservation_data_current)>0){
                    $total_reservations=1;
                    $resp.='<ul class="nav nav-pills seat__status"> <li class=""> <h4>Current</h4> </li> </ul>';
                    foreach ($reservation_data_current as $reservation_current){
                        $startTime = Carbon::parse(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id)->toDateTimeString());
                        $finishTime = Carbon::parse($reservation_current['end_time_withdate']); 
                        $totalsecs=$finishTime->diffInSeconds($startTime);
                       $totalDuration =gmdate('H:i', $totalsecs);
                       //$out_time=strtotime($reservation_current['end_time_withdate'])*1000;
                       $out_time=$reservation_current['end_time_withdate'];


                        $options='';
                        $total_covers+=$reservation_current['reserved_seat'];
                        foreach($reservation_current['next_status'] as $status_key=>$status_val){
                            $options.='<option value="'.$status_val['id'].'" style="background-color: '.$status_val['color'].'" data-icon="'.$status_val['icon'].'">'.$status_val['name'].'</option>';
                        }
                        $resp.='<div class="tab-content"> <div id="Current" class="current--sec"> <div class="person_detail"> <div class="person_name text-capitalize"><div><span class="'.$reservation_current['status_icon']['sidebar'].'" aria-hidden="true" style="color: '.$reservation_current['status_color']['sidebar'].'"></span> <h4>'.$reservation_current['name'].'</h4> </div> <p class="text-ellipsis margin-left-40">'.date('h:i A', strtotime($reservation_current['start_time'])).' - '.$reservation_current['reserved_seat'].' People - '.$reservation_current['table_name'].'</p> </div> <div class="complete__btn  margin-left-40"> <div class="complete__btn dropdown--picker orange-btn"><select id="current_status" reservation_id="'.$reservation_current['id'].'"  current_status="'.$reservation_current['status_name'].'" name="current_status '.$reservation_current['status_icon']['sidebar'].'" class="table_type selectpicker current_status" data-style="status-'.$reservation_current['status_slug'].'" required=""> <option style="background-color:'.$reservation_current['status_color']['sidebar'].'" data-icon="'.$reservation_current['status_icon']['sidebar'].'" value="" selected="selected">'.$reservation_current['status_name'].'</option>'.$options.' </select> </div> </div> </div> <div class="time__section"> <div class="time_sec1">Time remaining until end of assigned time:&nbsp; <span>(HH:MM)</span></div> <div class="time_sec2" out_time="'.$out_time.'"  end_time="'.$reservation_current['end_time'].'">'.$totalDuration.'</div> </div> </div> </div>';

                    }
                    $resp.='</div> </div>';
                }
            }
            /******************Current Reservations*******************************/

            /*****************Block table*********************/
            $block_tables = app('Modules\Reservation\Http\Controllers\BlockTableController')->getBlockTableInfo(\Auth::user()->restaurant_id, $date_start_time->format('Y-m-d'), $table->floor_id, $table->id);
            if($block_tables){
                $remaining_minutes = Carbon::parse($block_tables->end_time)->diffInMinutes(Carbon::now());
                $resp.='<div class="block-tables"><div class="title"><h4>TABLE BLOCKED</h4></div><div class="time__section"><div class="time_sec1">Time remaining until end of block:</div><div class="time_sec2">'.$remaining_minutes.'<span> min</span></div></div><div class="resume-booking row"><a href="javascript:void(0);" class="btn_ btn__primary text-center fullWidth" onclick="resumeBooking('.$table->id.',\''.$date_start_time->format('Y-m-d').'\')">Resume Booking</a></div></div>';
            }

            /******************Upcoming Reservations*******************************/
            $status_list = ReservationStatus::getStatusForUpcomingReservation();
            $upcomming_reservations = Reservation::with(['user', 'status', 'table'])->where('restaurant_id', \Auth::user()->restaurant_id)->where(function ($query) {
                $query->where('payment_status', '!=', config('reservation.payment_status.pending'))->orWhereNull('payment_status');
            })->whereRaw("FIND_IN_SET('".$table_id."', table_id)");

            if($date_start_time->format('Y-m-d') > Carbon::today()->toDateString()){
                $upcomming_reservations = $upcomming_reservations->where('start_time', '<', $date_end_time)->where('end_time', '>=', $date_start_time);
            }else{
                $upcomming_reservations = $upcomming_reservations->where('start_time', '>', Carbon::now())->where('end_time', '<=', $date_end_time);
            }
            $upcomming_reservations=$upcomming_reservations->whereIn('status_id', [$status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.confirmed')]])->get();

            $reservation_data_upcoming = $this->getReservationArr($upcomming_reservations, config('reservation.reservation_category.upcoming'));
            //\Log::info($reservation_data_upcoming);
             if(count($reservation_data_upcoming)>0){
                 $total_reservations=$total_reservations+count($reservation_data_upcoming);
                 $resp.='<ul class="nav nav-pills seat__status"> <li class=""> <h4>upcoming</h4> </li> </ul><div class="tab-content"> <div class="upcoming-sec">';
                  foreach ($reservation_data_upcoming as $reservation_upcoming){
                     $options='';
                      $total_covers+=$reservation_upcoming['reserved_seat'];
                      foreach($reservation_upcoming['next_status'] as $status_key=>$status_val){
                          $options.='<option value="'.$status_val['id'].'" style="background-color: '.$status_val['color'].'" data-icon="'.$status_val['icon'].'">'.$status_val['name'].'</option>';
                      }
                      $resp.='<div class="person_detail"> <div class="person_name text-capitalize"> <div><span class="'.$reservation_upcoming['status_icon']['sidebar'].'" aria-hidden="true" style="color: '.$reservation_upcoming['status_color']['sidebar'].'"></span><h4>'.$reservation_upcoming['name'].'</h4></div> <p class="text-ellipsis margin-left-40">'.date('h:i A', strtotime($reservation_upcoming['start_time'])).'- '.$reservation_upcoming['reserved_seat'].' People - '.$reservation_upcoming['table_name'].' </p> </div> <div class="cancle__btn"> <div class=" dropdown--picker green-btn margin-left-40"> <select id="current_status"  current_status="'.$reservation_upcoming['status_name'].'" reservation_id="'.$reservation_upcoming['id'].'" name="current_status '.$reservation_upcoming['status_icon']['sidebar'].'" class="table_type selectpicker current_status" required="" data-style="upcoming-waitlist status-'.$reservation_upcoming['status_slug'].'"> <option style="background-color:'.$reservation_upcoming['status_color']['sidebar'].'" data-icon="'.$reservation_upcoming['status_icon']['sidebar'].'" value="" selected="selected">'.$reservation_upcoming['status_name'].'</option>'.$options.' </select> </div> </div> </div>';

                  }
                 $resp.='</div> </div>';
             }

            /******************Upcoming Reservations*******************************/
            $resp.='</div> </div> </div>';
            $tabletype=$table->max?$table->max:'';
            $res_header='<div class="modal-header"> <a type="button" class="close icon-close" data-dismiss="modal" aria-label="Close"></a> <div class="seated-table__sec1"> <div class="row sidebar-head"> <div class="col-xs-7">'.$total_reservations.' Reservations</div> <div class="col-xs-5 text-right">'.$total_covers.' Covers</div> </div> <div class="table_sec1"> <div class="table__name"> <div> <span class="table-color hide" style="background-color: #c3f58e;"></span> <span>Table '.$table->name.'</span> </div> </div> <div class="hidden">Server: Alex D.</div> </div> <div class="table_sec2"> <div class="table_details">'.$tabletype.' Seats</div> <div class="table_details">'.$table->floor->name.'</div> </div> <div class="table_sec3"> <div class="assign__btn text-center"> <a href="#" class="btn bookingnew" data-toggle="modal" data-target="#book-a-table" floor-id="'.$table->floor_id.'" table-id="'.$table->id.'">New Reservation</a> '.$newwalkin.' </div> </div> </div> </div>';

            echo $res_header.$resp;
        }
    }


    private function getReservationArr($reservations_obj, $type)
    {
        $reservations_data=[];
        $restaurant_id = \Auth::user()->restaurant_id;
        if($reservations_obj) {
            $i=0;
            foreach ($reservations_obj as $reservation) {
                $reservations_data[$i]['id'] = $reservation->id;
                if(empty($reservation->fname) && empty($reservation->lname)){
                    $reservations_data[$i]['name'] = 'Walk In';
                }else{
                    $reservations_data[$i]['name'] = $reservation->fname . ' ' . $reservation->lname;
                }
                $reservations_data[$i]['start_time'] = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->start_time)->format('h:i A');
                $reservations_data[$i]['end_time'] = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->end_time)->format('H:i');
                $reservations_data[$i]['end_time_withdate'] = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->end_time);
                $reservations_data[$i]['reserved_seat'] = $reservation->reserved_seat;
                if(strpos($reservation->table_id, ',')) {
                    $tableIdArr = explode(',', $reservation->table_id);
                    $tableNameArr = Table::whereIn('id', $tableIdArr)->with(['floor'=>function($query){ $query->select('id', 'name'); }])->get();
                    $tableNames = $floorName = '';
                    if(count($tableNameArr)) {
                        $tableNameArr = $tableNameArr->toArray();
                        $tableNames = implode('+', array_column($tableNameArr, 'name'));
                        $floorName = isset($tableNameArr[0]['floor']['name']) ? $tableNameArr[0]['floor']['name'] : '';
                    }
                    $reservations_data[$i]['floor_id'] = $reservation->floor_id ?? '';
                    $reservations_data[$i]['floor_name'] = $floorName;
                    $reservations_data[$i]['table_id'] = $reservation->table_id ?? '';
                    $reservations_data[$i]['table_name'] = $tableNames;
                } else {
                    $reservations_data[$i]['floor_id'] = $reservation->floor->id ?? '';
                    $reservations_data[$i]['floor_name'] = $reservation->floor->name ?? '';
                    $reservations_data[$i]['table_id'] = $reservation->table->id ?? '';
                    $reservations_data[$i]['table_name'] = $reservation->table->name ?? '';
                }
                $reservations_data[$i]['status_name'] = $reservation->status->name;
                $reservations_data[$i]['status_slug'] = $reservation->status->slug;
                $reservations_data[$i]['status_icon'] = ($reservation->status->icon) ? json_decode($reservation->status->icon, true) : '';
                $reservations_data[$i]['status_color'] = ($reservation->status->color) ? json_decode($reservation->status->color, true) : '';
                $reservations_data[$i]['source'] = $reservation->source;

                $reservations_data[$i]['next_status'] =$this->getNextReservationStatuses($reservation, $type, $reservation->start_time);
                $reservations_data[$i]['user_type'] = isset($reservation->user->category) ? getCustomerTypeAndIcon($reservation->user->category) : '';
                $reservations_data[$i]['special_occasion'] = getSpecialOccasionNameAndIcon($reservation->special_occasion);
                $reservations_data[$i]['is_no_show'] = $this->getNoShowStatus($reservation->start_time, $reservation->end_time, $reservation->status->slug);
                $waitofid='wait_'.$reservation->id;
                $reservations_data[$i]['wait_for']=!isset($_COOKIE[$waitofid])?0:1;
                $reservations_data[$i]['late_minutes'] = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->start_time)->diffInMinutes(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id));
                $resvTimeLeft                          = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->end_time)->diffInMinutes(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id));
                if(CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->end_time)->lt(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id))) {
                    $resvTimeLeft = -$resvTimeLeft;
                }
                $reservations_data[$i]['time_left']    = $resvTimeLeft;
                $isAboutToFinish                       = 0;
                if (($reservation->status->slug == config('reservation.status_slug.seated') || $reservation->status->slug == config('reservation.status_slug.starters') || $reservation->status->slug == config('reservation.status_slug.entree') || $reservation->status->slug == config('reservation.status_slug.main_course') || $reservation->status->slug == config('reservation.status_slug.dessert') || $reservation->status->slug == config('reservation.status_slug.bill_dropped') || $reservation->status->slug == config('reservation.status_slug.paid')) && ($resvTimeLeft <= config('reservation.time_left_to_extend') && $resvTimeLeft>=0)) {
                    $isAboutToFinish = 1;
                }
                $reservations_data[$i]['is_about_to_finish'] = $isAboutToFinish;
                $reservations_data[$i]['finish_status']      = ReservationStatus::getFinishedStatusId();
                $i++;
            }
        }
        //\Log::info($reservations_data);
        return $reservations_data;
    }

    private function getNoShowStatus($reservation_start_time, $reservation_end_time, $reservation_staus_flag)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        if (($reservation_start_time < CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id) && $reservation_end_time > CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id)) && ($reservation_staus_flag==config('reservation.status_slug.reserved') || $reservation_staus_flag==config('reservation.status_slug.confirmed') || $reservation_staus_flag==config('reservation.status_slug.running_late'))) {
            return true;
        }
        return false;
    }

    private function getGridReservation($table_id, $reservations, $working_slot_arr, $date)
    {
        //\Log::info($reservations);
        $grid_reservations = [];
        $inc = 0;
        $restaurant_id = \Auth::user()->restaurant_id;
        if(empty($working_slot_arr)){
            foreach ($reservations as $reservation) {
                $resv_start_time = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation['start_time']);
                $resv_end_time = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation['end_time']);
                if(($resv_end_time < CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id) && in_array($reservation['status']['slug'], config('reservation.status_slug_check_for_grid_reservation.past'))) || ($resv_end_time > CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id) && in_array($reservation['status']['slug'], config('reservation.status_slug_check_for_grid_reservation.current_upcoming')))){
                    $status_color_sidebar = $status_icon_sidebar = null;
                    if(!empty($reservation['status'])){
                        $status_color = json_decode($reservation['status']['color'], true);
                        $status_icon = json_decode($reservation['status']['icon'], true);
                        $status_color_sidebar = (isset($status_color['sidebar']))? $status_color['sidebar']: null;
                        $status_icon_sidebar = (isset($status_icon['sidebar']))? $status_icon['sidebar']: null;
                    }
                    $grid_reservations[$inc]['resv_id'] = $reservation['id'];
                    $grid_reservations[$inc]['id'] = $inc;
                    $grid_reservations[$inc]['resourceId'] = $table_id;
                    //$grid_reservations[$inc]['start'] = Carbon::parse($resv_start_time)->format('Y-m-d H:i:s');
                    //$grid_reservations[$inc]['end'] = Carbon::parse($resv_end_time)->format('Y-m-d H:i:s');
                    $grid_reservations[$inc]['start'] = $resv_start_time;
                    $grid_reservations[$inc]['end'] = $resv_end_time;
                    $full_name = empty($reservation['fname']) && empty($reservation['lname'])? 'Walk In' : $reservation['fname'] . ' ' . $reservation['lname'];
                    $grid_reservations[$inc]['title']='<div data-toggle="tooltip" title="'.$full_name.' '.$reservation['reserved_seat'].'" class="grid-view__container"> <div class="grid-view__icon" style="background-color:'.$status_color_sidebar.';float: left;padding: 10px;color: #fff;"> <i class="'.$status_icon_sidebar.'"></i> </div> <div class="grid-view__contentwrapper" style="float: left; padding: 7px; color: '.$status_color_sidebar.';background-color: '.hex2rgba($status_color_sidebar, 0.2).';"> <div class="grid-view__content"> <span class="name  text-capitalize">'.$full_name.'</span> <span class="cover">('.$reservation['reserved_seat'].')</span> </div> </div> </div>';
                    $grid_reservations[$inc]['type'] = 'table';
                    $inc++;
                }
            }
        }else{
            $evenOddInc = 0;
            foreach ($working_slot_arr as $slot_arr) {
                $reservation_exist = false;
                $oddEvenClass = ($evenOddInc%2==0)?'even':'odd';
                $slot_arr = explode('-', $slot_arr);
                $slot_start_time = $slot_arr[0];
                $slot_end_time = $slot_arr[1];
                $slot_start_date_time = Carbon::parse($date." ".$slot_start_time.':00')->format('Y-m-d H:i:s');
                $slot_end_date_time = Carbon::parse($date." ".$slot_end_time.':00')->format('Y-m-d H:i:s');
                //\Log::info($slot_start_date_time."****".$slot_end_date_time);
                //\Log::info($reservations);
                if (!$reservations || count($reservations)==0) {
                    $grid_reservations[$inc]['id'] = $inc;
                    $grid_reservations[$inc]['resourceId'] = $table_id;
                    $grid_reservations[$inc]['resv_id'] = null;
                    $grid_reservations[$inc]['start'] = $slot_start_date_time;
                    $grid_reservations[$inc]['end'] = $slot_end_date_time;
                    if($slot_start_date_time < CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id) || $slot_end_date_time < CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id)){
                        $oddEvenClass.=' past_time';
                    }
                    $grid_reservations[$inc]['title'] = "<div class='grid-view__slotAvailable ".$oddEvenClass."'></div>";
                    $grid_reservations[$inc]['type'] = 'book';
                    $inc++;
                }else{
                    foreach ($reservations as $reservation) {
                        $resv_start_time = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation['start_time']);
                        $resv_end_time = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation['end_time']);
                        if((!empty($resv_start_time) && !empty($resv_end_time)) &&(!rangesNotOverlapOpen($slot_start_time, $slot_end_time, Carbon::parse($resv_start_time)->format('H:i:s'), Carbon::parse($resv_end_time)->format('H:i:s')) || !rangesNotOverlapClosed($slot_start_time, $slot_end_time, Carbon::parse($resv_start_time)->format('H:i:s'), Carbon::parse($resv_end_time)->format('H:i:s')))){
                            if(!in_array($reservation['id'], array_column($grid_reservations, 'resv_id')) && (($resv_end_time < CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id) && in_array($reservation['status']['slug'], config('reservation.status_slug_check_for_grid_reservation.past'))) || ($resv_end_time > CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id) && in_array($reservation['status']['slug'], config('reservation.status_slug_check_for_grid_reservation.current_upcoming'))))){
                                $status_color_sidebar = $status_icon_sidebar = null;
                                if(!empty($reservation['status'])){
                                    $status_color = json_decode($reservation['status']['color'], true);
                                    $status_icon = json_decode($reservation['status']['icon'], true);
                                    $status_color_sidebar = (isset($status_color['sidebar']))? $status_color['sidebar']: null;
                                    $status_icon_sidebar = (isset($status_icon['sidebar']))? $status_icon['sidebar']: null;
                                }
                                $grid_reservations[$inc]['resv_id'] = $reservation['id'];
                                $grid_reservations[$inc]['id'] = $inc;
                                $grid_reservations[$inc]['resourceId'] = $table_id;
                                $grid_reservations[$inc]['start'] = Carbon::parse($resv_start_time)->format('Y-m-d H:i:s');
                                $grid_reservations[$inc]['end'] = Carbon::parse($resv_end_time)->format('Y-m-d H:i:s');
                                $full_name = empty($reservation['fname']) && empty($reservation['lname'])? 'Walk In' : $reservation['fname'] . ' ' . $reservation['lname'];
                                $grid_reservations[$inc]['title']='<div data-toggle="tooltip" title="'.$full_name.' '.$reservation['reserved_seat'].'" class="grid-view__container"> <div class="grid-view__icon" style="background-color:'.$status_color_sidebar.';float: left;padding: 10px;color: #fff;"> <i class="'.$status_icon_sidebar.'"></i> </div> <div class="grid-view__contentwrapper" style="float: left; padding: 7px; color: '.$status_color_sidebar.';background-color: '.hex2rgba($status_color_sidebar,0.2).';"> <div class="grid-view__content"> <span class="name  text-capitalize">'.$full_name.'</span> <span class="cover">('.$reservation['reserved_seat'].')</span> </div> </div> </div>';
                                $grid_reservations[$inc]['type'] = 'table';
                                $inc++;
                            }else{
                                $reservation_exist = true;
                                continue;
                            }
                            $reservation_exist = true;
                        }
                    }
                    if(!$reservation_exist){
                            $grid_reservations[$inc]['id'] = $inc;
                            $grid_reservations[$inc]['resv_id'] = null;
                            $grid_reservations[$inc]['resourceId'] = $table_id;
                            $grid_reservations[$inc]['start'] = $slot_start_date_time;
                            $grid_reservations[$inc]['end'] = $slot_end_date_time;
                            if($slot_start_date_time < CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id) || $slot_end_date_time < CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id)){
                                $oddEvenClass.=' past_time';
                            }
                            $grid_reservations[$inc]['title'] = "<div class='grid-view__slotAvailable ".$oddEvenClass."'> </div>";
                            $grid_reservations[$inc]['type'] = 'book';
                            $inc++;
                        }
                    }
                $evenOddInc++;
            }
        }
        return $grid_reservations;
    }

    private function getWorkingHoursSlotRange($date)
    {
        $slot_arr = [];
        $restaurant_id = \Auth::user()->restaurant_id;
        $working_hours = app('Modules\Reservation\Http\Controllers\ReservationController')->getWorkingHours($date, $restaurant_id);
        //\Log::info($working_hours);
        if ($working_hours && !$working_hours->is_dayoff) {
            $slot_detail = !is_null($working_hours->slot_detail) ? json_decode($working_hours->slot_detail, true):[];
            if(count($slot_detail)>0){
                foreach ($slot_detail as $slot) {
                    $working_slot_arr = getSlotsRangeFromTimeRange($slot['open_time'], $slot['close_time'], config('reservation.slot_interval_time'));
                    $slot_arr = array_merge($slot_arr, $working_slot_arr);
                }
                if(!empty($slot_arr)){
                    $slot_arr = array_sort($slot_arr);
                }
            }
        }
        if(count($slot_arr)>0){
            $slot_arr = array_unique($slot_arr);
        }
        return $slot_arr;
    }

    public function getTableReservations($table_id, $type, $date)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $table_reservation = Reservation::with(['status'=>function($query){
            $query->select(['id','name','slug','color','icon']);
        }])->where('restaurant_id', $restaurant_id)->whereRaw("FIND_IN_SET('".$table_id."', table_id)");
        if($type != config('reservation.reservation_category.waitlist')) {
            $table_reservation = $table_reservation->where(function ($query) {
                $query->where('payment_status', '!=', config('reservation.payment_status.pending'))->orWhereNull('payment_status');
            });
        }
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 00:00:00"));
        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 24:00:00"));
        if(Carbon::today()->toDateString()!=$date || $type==config('reservation.grid_view')){
            $status_list = ReservationStatus::getStatusForCurrentReservation();
            $table_reservation = $table_reservation->where('start_time', '<', $end_date_time)->where('end_time', '>', $start_date_time)->whereIn('status_id', [$status_list[config('reservation.status_slug.confirmed')], $status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.partially_seated')], $status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.finished')]]);
        }else if ($type == config('reservation.reservation_category.current')) {
            $status_list = ReservationStatus::getStatusForCurrentReservation();
            $table_reservation = $table_reservation->whereDate('start_time', '=', Carbon::parse($start_date_time)->format('Y-m-d'))
                ->where(function ($query) use ($status_list, $restaurant_id) {
                    return $query->where('start_time', '<=', Carbon::now())->where('end_time', '>=', Carbon::now())->whereIn('status_id', [$status_list[config('reservation.status_slug.confirmed')], $status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.partially_seated')], $status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')], $status_list[config('reservation.status_slug.running_late')]])->orWhere(function ($query) use($status_list){
                        $query->where('start_time', '<', Carbon::now())->whereIn('status_id', [$status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')]]);
                    });
                });
        }elseif ($type == config('reservation.reservation_category.upcoming')) {
            $status_list = ReservationStatus::getStatusForUpcomingReservation();
            $table_reservation = $table_reservation->where('start_time', '>', Carbon::now())->where('end_time', '<=', Carbon::today()->endOfDay())->whereIn('status_id', [$status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.confirmed')]]);
        }else {
            $status_list = ReservationStatus::getStatusForWaitlistReservation();
            $table_reservation = $table_reservation->where('start_time', '<', $end_date_time)/*->where('end_time', '>=', $start_date_time)/*->where('source', config('reservation.source.walk_in'))*/->where(function ($query) use ($status_list) {
                return $query->where('status_id', $status_list[config('reservation.status_slug.waitlist')]);
            });
        }
        $table_reservation = $table_reservation->select(['id','fname','lname', 'table_id','user_id','created_at','start_time','end_time','reserved_seat','status_id'])->orderBy('start_time', 'asc')->get();
        return $table_reservation;
    }

    private function setLinkIconColorForMultipleTable($configArr)
    {
        $linkTableIdColorArr = [];
        foreach ($configArr as $key=>$config){
            $config = json_decode($config, true);
            $reservation = isset($config['reservations'])&&!empty($config['reservations'])?$config['reservations'][0]:null;
            if(($reservation && isset($reservation['table_id'])) && strpos($reservation['table_id'], ',')) {
                $color = randomHex();
                if(!array_key_exists($reservation['table_id'], $linkTableIdColorArr) || empty($linkTableIdColorArr)){
                    $linkTableIdColorArr[$reservation['table_id']] = $color;
                }
                $config['reservations'][0]['tables_link_color'] = $linkTableIdColorArr[$reservation['table_id']];
                $configArr[$key] = json_encode($config);
            }
        }
        return $configArr;
    }

    private function changeWorkingHoursSetting($table_id)
    {
        $table_info = Table::find($table_id);
        $restaurant_id = \Auth::user()->restaurant_id;
        if($table_info && $table_info->floor_id){
            $current_date = Carbon::today()->toDateString();
            $custom_working_hours = CustomWorkingHours::where('calendar_date->end_date', '>=', $current_date)
                ->where('restaurant_id', $restaurant_id)
                ->where(DB::raw('slot_detail->\'$[*].floors\''), 'LIKE', DB::raw('json_array("%'.$table_info->floor_id.'%")'))
                ->get();
            if($custom_working_hours){
                $this->updateWorkingHoursSlotDetails($custom_working_hours, $table_info);
            }
            $week_working_hours = WorkingHours::where('restaurant_id', $restaurant_id)
                ->where(DB::raw('slot_detail->\'$[*].floors\''), 'LIKE', DB::raw('json_array("%'.$table_info->floor_id.'%")'))
                ->get();
            if($week_working_hours){
                $this->updateWorkingHoursSlotDetails($week_working_hours, $table_info);
            }
        }
    }

    private function updateWorkingHoursSlotDetails($working_hours, $table_info)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        foreach ($working_hours as $hour_key=>$hours){
            if($hours && !empty($hours->slot_detail)){
                $slot_details = json_decode($hours->slot_detail, true);
                $change_impact = false;
                foreach ($slot_details as $slot_key=>$slot){
                    $floors = isset($slot['floors'])&&!empty($slot['floors'])?json_decode($slot['floors'], true):null;
                    $party_size_arr = json_decode($slot['party_size'], true);
                    if($floors && in_array($table_info->floor_id, $floors)){
                        $tableTypeCount = Table::whereIn('floor_id', $floors)->where('max', $table_info->max)->count();
                        $available_table = json_decode($slot['available_tables'], true);
                        foreach ($available_table as $key=>$tables){
                            if($tables['table_type']==$table_info->max && $tables['table_count']>=$tableTypeCount){
                                $available_table_type_count = $tableTypeCount-1;
                                $available_table[$key]['table_count'] = $available_table_type_count;
                                $to_time = strtotime($slot['open_time']);
                                $from_time = strtotime($slot["close_time"]);
                                $minute = round(abs($from_time - $to_time) / 60,2);
                                $table_type_id = app('Modules\Reservation\Http\Controllers\TurnOverTimeController')->getTableTypeIdByPartySize($restaurant_id, $table_info->max);
                                $tat = $party_size_arr[$table_type_id];

                                $cover_count = $available_table_type_count * $table_info->max * round($minute/$tat);
                                if($tables['table_cover_count']>$cover_count){
                                    $available_table[$key]['table_cover_count'] = (integer)$cover_count;
                                }
                                $slot_details[$slot_key]['available_tables'] = json_encode($available_table);
                                $change_impact = true;
                                break;
                            }
                        }
                        if($change_impact){
                            $available_table_cover_count = array_sum(array_column($available_table, 'table_cover_count'));
                            if($slot['max_covers_limit'] > $available_table_cover_count){
                                $slot_details[$slot_key]['max_covers_limit'] = $available_table_cover_count;
                            }
                        }
                    }
                }
                if($change_impact){
                    $hours->slot_detail = json_encode($slot_details);
                    $hours->save();
                }
            }
        }
    }

    // method for cover count display in grid view
    private function getFlorCoverCountForGrid($floor_id, $date)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $slots = getSlotsFromTimeRange(config('reservation.restaurant_timing.open_time'), config('reservation.restaurant_timing.close_time'), config('reservation.slot_interval_time'));
        $coverCountArr = [];
        foreach ($slots as $slot){
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." ".$slot))->format('Y-m-d H:i:s');
            $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." ".$slot)->addMinutes(config('reservation.slot_interval_time')))->format('Y-m-d H:i:s');
            $cover_count = 0;
            if($end_date_time < Carbon::now()){
                $status = config('reservation.status_slug_check_for_grid_reservation.past');
            }else{
                $status = config('reservation.status_slug_check_for_grid_reservation.current_upcoming');
            }
            $reservation_count = Reservation::where('restaurant_id', $restaurant_id)->where(function ($query) {
                $query->where('payment_status', '!=', config('reservation.payment_status.pending'))->orWhereNull('payment_status');
            })->where('floor_id', $floor_id)->whereHas('status', function ($query) use($status) {
                return $query->whereIn('slug', $status);
            })->where('start_time', '<', $end_date_time)
                ->where('end_time', '>', $start_date_time)
                ->select(DB::Raw('sum(reserved_seat) as reserved_seat'), DB::Raw('count(*) as reserved_table'))
                ->first();
            if($reservation_count){
                $cover_count = ($reservation_count->reserved_seat) ? $reservation_count->reserved_seat : 0;
            }
            $coverCountArr[] = $cover_count;
        }
        return $coverCountArr;
    }

    private function getFloorTableServers($date, $floor_id, $table_id)
    {
        $slot_id = $this->getSlotIdByDate($date);
        $table_server = app('Modules\Reservation\Http\Controllers\TableServerController')->getTableServerInfo($floor_id, $table_id, $slot_id, $date);
        $server_color =  $server_table_id = $server_name = $server_full_name = $server_id = '';
        if($table_server && ($table_server->server && !empty($table_server->server))){
            $server_table_id = !empty($config) && !empty($config['tableID']) ? $config['tableID'] : null;
            $server_color = $table_server->server->color;
            $server_name = (($table_server->server->fname) ? $table_server->server->fname[0] : '').(($table_server->server->lname) ? $table_server->server->lname[0] : '');
            $server_full_name = $table_server->server->fname." ".$table_server->server->lname;
            $server_id = $table_server->server->id;
        }
        $server[0]['serverTableColor'] = "#".$server_color;
        $server[0]['serverTableId'] = $server_table_id;
        $server[0]['serverName'] = $server_name;
        $server[0]['serverFullName'] = $server_full_name;
        $server[0]['serverId'] = $server_id;
        return $server;
    }

    public function getSlotIdByDate($date)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $working_hours = app('Modules\Reservation\Http\Controllers\ReservationController')->getWorkingHours($date, $restaurant_id);
        if ($working_hours && !$working_hours->is_dayoff) {
            $slot_detail = !is_null($working_hours->slot_detail) ? json_decode($working_hours->slot_detail, true) : [];
            if (count($slot_detail) > 0) {
                $current_time = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'H:i');
                foreach ($slot_detail as $slot) {
                    if (isBetween($slot['open_time'], $slot['close_time'], $current_time)) {
                        return $slot['slot_id'];
                    }
                }
            }
        }
        return false;
    }

    private function getTableTimer($restaurant_id, $date, $table_id, $type)
    {
        $currently_seated_time = $remaining_time = $upcoming_resv_time = 0;
        $date_start_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 00:00:00"));

        //if(!empty($type)){
            $status_list = ReservationStatus::getStatusForCurrentReservation();
            $reservation_detail = Reservation::where('restaurant_id', $restaurant_id)->whereRaw("FIND_IN_SET('".$table_id."', table_id)")->whereDate('start_time', '=', $date_start_time->format('Y-m-d'))
                ->where(function ($query) use ($status_list) {
                    return $query->where('start_time', '<=', Carbon::now())->where('end_time', '>=', Carbon::now())->whereIn('status_id', [$status_list[config('reservation.status_slug.confirmed')], $status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.partially_seated')], $status_list[config('reservation.status_slug.seated')], $status_list[config('reservation.status_slug.starters')], $status_list[config('reservation.status_slug.entree')], $status_list[config('reservation.status_slug.main_course')], $status_list[config('reservation.status_slug.dessert')], $status_list[config('reservation.status_slug.bill_dropped')], $status_list[config('reservation.status_slug.paid')], $status_list[config('reservation.status_slug.running_late')]]);
                })
                ->select('id','start_time','end_time','seated_at')->first();
            if($reservation_detail){
                //if($type==config('reservation.timer.currently_seated_time')){
                    $currently_seated_time = Carbon::now()->diffInSeconds(Carbon::parse($reservation_detail->seated_at));
                //}
                //if($type==config('reservation.timer.remaining_time')){
                    $remaining_time = Carbon::parse($reservation_detail->end_time)->diffInSeconds(Carbon::now());
                //}
            }
            //if($type==config('reservation.timer.next_resv_time')){
                $upcoming_resv_time = $this->getNextReservationTime($restaurant_id, $table_id, $date);
            //}
            //$upcoming_resv = $this->getUpcomingReservation($restaurant_id, $date, $table_id);
        //}
        $timer[0][config('reservation.timer.currently_seated_time')] = $currently_seated_time;
        $timer[0][config('reservation.timer.remaining_time')] = $remaining_time;
        $timer[0][config('reservation.timer.next_resv_time')] = $upcoming_resv_time;
        //$timer[0]['upcoming_reservations'] = $upcoming_resv;
        //\Log::info($timer);
        return $timer;
    }

    private function getNextReservationTime($restaurant_id, $table_id, $date)
    {
        $status_list = ReservationStatus::getStatusForUpcomingReservation();
        $upcoming_resv_time = 0;
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 00:00:00"));
        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 24:00:00"));
        $upcoming=Reservation::where('restaurant_id', $restaurant_id)->whereRaw("FIND_IN_SET('".$table_id."', table_id)")->where('start_time', '>', Carbon::now())
            ->where('end_time', '<=', $end_date_time)
            ->whereIn('status_id', [$status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.confirmed')]])
            ->orderBy('start_time', 'asc')->select('id','start_time','end_time')->first();
        if( $upcoming){
            //$upcoming_resv_time =  Carbon::parse($upcoming->start_time)->diffInMinutes(Carbon::now());
            $upcoming_resv_time =  CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, Carbon::parse($upcoming->start_time))->format('Y-m-d H:i:s');
        }
        return $upcoming_resv_time;
    }

    public function getUpcomingReservation($restaurant_id, $date, $table_id)
    {
        $status_list = ReservationStatus::getStatusForUpcomingReservation();
        $upcomming_reservations = Reservation::where('restaurant_id', $restaurant_id)->where(function ($query) {
            $query->where('payment_status', '!=', config('reservation.payment_status.pending'))->orWhereNull('payment_status');
        })->whereRaw("FIND_IN_SET('".$table_id."', table_id)");
        $date_start_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 00:00:00"));
        $date_end_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 24:00:00"));

        if($date_start_time->format('Y-m-d') > Carbon::today()->toDateString()){
            $upcomming_reservations = $upcomming_reservations->where('start_time', '<', $date_end_time)->where('end_time', '>=', $date_start_time);
        }else{
            $upcomming_reservations = $upcomming_reservations->where('start_time', '>', Carbon::now())->where('end_time', '<=', $date_end_time);
        }
        return $upcomming_reservations->whereIn('status_id', [$status_list[config('reservation.status_slug.reserved')], $status_list[config('reservation.status_slug.running_late')], $status_list[config('reservation.status_slug.confirmed')]])->toArray();
    }
}
