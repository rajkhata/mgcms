<?php

namespace Modules\Reservation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Reservation\Entities\Server;
use Modules\Reservation\Entities\TableServer;
use Modules\Reservation\Http\Requests\ServerRequest;
use DB;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('reservation::server.assign');
    }


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ServerRequest $request)
    {
        try {
            $restaurant_id = \Auth::user()->restaurant_id;
            $data = $request->all();
            $server = Server::where('restaurant_id', $restaurant_id)->where('color', $data['color'])->first();
            if ($server) {
                return \Response::json(['errors' => ['Server color should be unique for all servers']], 422);
            }
            DB::beginTransaction();
            Server::create([
                'restaurant_id' => $restaurant_id,
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'mobile' => $data['mobile'],
                'email' => $data['email'],
                'color' => $data['color']
            ]);
            DB::commit();
            return \Response::json(['message' => ['Server has been created successfully.']], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return back()->withInput()->withErrors(array('message' => 'Something went wrong, please try later'));
        }
    }



    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($server_id)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $server = Server::where('restaurant_id', $restaurant_id)->where('id', $server_id)->first();
        if(!$server){
            return \Response::json([ 'errors'=>['Server details could not found.']], 422);
        }
        return \Response::json([ 'data'=> $server], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('reservation::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $data = $request->all();
        $server = Server::where('restaurant_id', $restaurant_id)->where('color', $data['color'])->where('id', '!=', $data['server_id'])->first();
        if ($server) {
            return \Response::json(['errors' => ['Server color should be unique for all servers']], 422);
        }
        DB::beginTransaction();
        try {
            $server = Server::where('restaurant_id', $restaurant_id)->where('id', $data['server_id'])->first();
            if ($server) {
                $server->fname = $data['fname'];
                $server->lname = $data['lname'];
                $server->mobile = $data['mobile'];
                $server->email = $data['email'];
                $server->color = $data['color'];
                $server->save();
            }
            DB::commit();
            return \Response::json(['message' => ['Server has been updated successfully.']], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return back()->withInput()->withErrors(array('message' => 'Something went wrong, please try later'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($server_id)
    {
        DB::beginTransaction();
        try {
            Server::where('restaurant_id', \Auth::user()->restaurant_id)
                ->where('id', $server_id)->delete();
            TableServer::where('server_id', $server_id)->delete();
            DB::commit();
            return \Response::json(['title' => 'Server Deleted', 'message' => 'Server has been deleted successfully'], 200);
        }
        catch (\Exception $e){
            DB::rollback();
            return \Response::json([ 'errors'=>['Unable to delete the server']], 422);
        }
    }
}
