<?php

namespace Modules\Reservation\Http\Controllers;

use App\Events\TableReservation;
use App\Events\TableReservationCancel;
use App\Events\TableReservationModify;
use App\Events\TableReservationPaymentLink;
use App\Events\TableReservationRefund;
use App\Events\TableWaitlist;
use App\Helpers\ApiPayment;
use App\Models\Restaurant;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Modules\Reservation\Entities\BlockTable;
use Modules\Reservation\Entities\CustomWorkingHours;
use Modules\Reservation\Entities\Floor;
use Modules\Reservation\Entities\Reservation;
use Modules\Reservation\Entities\ReservationStatus;
use Modules\Reservation\Entities\Table;
use Modules\Reservation\Entities\WorkingHours;
use Modules\Reservation\Http\Requests\CheckValidity;
use YieldReservation;
use App\Helpers\CommonFunctions;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $availability_status = false;
    protected $availability_msg = false;



    public function index()
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $current_date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d');
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($current_date." 00:00:00"));
        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($current_date." 24:00:00"));
        $reservations = Reservation::where('restaurant_id', \Auth::user()->restaurant_id)->with(['user', 'status'])->whereHas('status', function($query) {
            $query->where('slug', '!=', config('reservation.status_slug.cancelled'));
        })->where('start_time', '<', $end_date_time)->where('end_time', '>', $start_date_time)->orderBy('start_time', 'desc')->get();
        $reservations = $this->getModifiedReservationsForListing($reservations);
        $restaurant_detail = Restaurant::where('id', \Auth::user()->restaurant_id)->first();
        $rest_allow_resv = $restaurant_detail->reservation==0 ? false : true;
        return view('reservation::reservation.index', compact('reservations', 'rest_allow_resv'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('reservation::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('reservation::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('reservation::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        DB::beginTransaction();
        //\Log::info($request->all());
        try {
            $restaurant_id = $request->has('restaurant_id') ? $request->get('restaurant_id') : Auth::user()->restaurant_id;
            $id = $request->get('reservation_id');
            $reservation_date = $request->get('reservation_date');
            $start_time = $request->get('reservation_time');
            $table_id = $request->get('table_id');
            if (is_array($table_id)) {
                $table_id = implode(',', $table_id);
            }
            $floor_id = $request->get('floor_id');
            $party_size = $request->get('party_size');
            $host = $request->get('host_name');
            $special_instruction = $request->get('special_instruction');
            $special_occasion = $request->has('special_occasion') ? $request->get('special_occasion') : null;
            $special_occasion = !empty($special_occasion) ? config('reservation.special_occasion.' . $special_occasion) : null;
            $fname = $request->get('fname');
            $lname = $request->get('lname');
            $email = $request->get('email');
            $phone = $request->get('phone');
            $birthday = $request->get('dob');
            $tat = $request->get('tat');
            $birthday = !empty($birthday) ? Carbon::createFromFormat('m-d-Y', $birthday)->format('Y-m-d') : null;
            $anniversary = $request->get('doa');
            $anniversary = !empty($anniversary) ? Carbon::createFromFormat('m-d-Y', $anniversary)->format('Y-m-d') : null;
            $tags = $request->has('tags') && !empty($request->get('tags')) ? explode(',', $request->get('tags')) : null;
            $description = $request->has('guest_note') && !empty($request->get('guest_note')) ? $request->get('guest_note') : null;
            $user_id = $request->get('user_id');
            $slot_range = $request->get('slot_range');
            if ($request->input('yield_cancelled') > 1) {
                $yieldOverride = 1;
            } else {
                $yieldOverride = 0;
            }
            $inputData = array();
            if ($reservation_date && $start_time) {
                $reservation_date = Carbon::createFromFormat('m-d-Y', $reservation_date)->format('Y-m-d');
                $start_time = Carbon::parse($start_time)->format('H:i');
                $inputData['start_time'] = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($reservation_date . ' ' . $start_time)->format('Y-m-d H:i:s'));
            }
            $inputData['reserved_seat'] = $party_size;
            $reservation = Reservation::where('id', $id)->whereHas('status', function ($query) {
                $query->whereIn('slug', config('reservation.status_slug_to_be_update'));
            })->whereDate('start_time', '>=', Carbon::today()->toDateString())->first();
            $payment_change = false;
            //\Log::info($reservation);
            if ($reservation) {
                // get turnover time by using party size
                if (empty($tat)) {
                    $response = $this->checkAvailability($reservation_date, $start_time, $party_size, $floor_id, $table_id, $walkInFlag = NULL, $restaurant_id, $id, config('reservation.source.offline'));
                    /*if (!empty($response['errors'])) {
                        return \Response::json($response, 422);
                    }
                    if (!isset($response['data']['floor_table_info']) || empty($response['data']['floor_table_info'])) {
                        return \Response::json(['errors' => ['Reservation could not be updated']], 422);
                    }*/
                    if (!isset($response['data']['tat']) || empty($response['data']['tat'])) {
                        return \Response::json(['errors' => ['Reservation could not be updated']], 422);
                    }
                    $tat = $response['data']['tat'];
                    /*if (empty($tat) || empty($slot_range) || empty($floor_id) || empty($table_id)) {
                        return \Response::json(['errors' => ['Reservation could not be updated']], 422);
                    }*/
                    if (empty($tat) || empty($floor_id) || empty($table_id)) {
                        return \Response::json(['errors' => ['Reservation could not be updated']], 422);
                    }
                }
                $userId = null;
                $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
                $parent_restaurant_id = $parent_restaurant_id == 0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
                if (!empty($user_id)) {
                    $user = User::where('restaurant_id', $parent_restaurant_id)->where('id', $user_id)->first();
                    $user->dob = $birthday;
                    $user->doa = $anniversary;
                    $user->description = $description;
                    if (!empty($tags)) {
                        $guest_tags_db = ($user->tags) ? array_filter(json_decode($user->tags)) : array();
                        $merged_array = array_merge($guest_tags_db, $tags);
                        $merged_array = array_unique($merged_array);
                        $user->tags = ($merged_array) ? json_encode(array_values($merged_array)) : NULL;
                    }
                    $user->save();
                    $userId = $user->id;
                } else {
                    $user = User::where('restaurant_id', $parent_restaurant_id)->where(function ($query) use ($email, $phone) {
                        $query->where('email', $email)->orWhere('mobile', $phone);
                    })->first();

                    if ($request->has('register_guest')) {
                        if ($user) {
                            return \Response::json(['errors' => ['User already registered']], 422);
                        }
                        $username = strtolower($fname) . "_" . strtolower($lname);
                        if (!empty($tags)) {
                            $tags = json_encode(array_unique($tags));
                        }
                        $user = User::create(['restaurant_id' => $parent_restaurant_id, 'category' => config('reservation.guest_category.New'), 'email' => $email, 'phone' => $phone, 'fname' => $fname, 'lname' => $lname, 'status' => 1, 'username' => $username, 'password' => bcrypt('123456'), 'mobile' => $phone, 'tags' => $tags, 'dob' => $birthday, 'doa' => $anniversary, 'description' => $description]);
                        $userId = $user->id;
                    } else {
                        $userId = $user ? $user->id : null;
                    }
                }

                $close_time = Carbon::parse($start_time)->addMinutes($tat)->format('H:i:s');

                /*$floor_table_info = reset($response['data']['floor_table_info']);
                if ($floor_table_info) {
                    $table_info = reset($floor_table_info['tables']);
                }*/
                //$slot_range = $response['data']['slot_range'];
                $inputData['end_time'] = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($reservation_date . ' ' . $close_time)->format('Y-m-d H:i:s'));
                //$inputData['table_id'] = $table_info['id'];
                $inputData['status_id'] = ReservationStatus::getReservedStatusId();
                $inputData['reserved_seat'] = $party_size;
                $inputData['floor_id'] = $floor_id;
                $inputData['table_id'] = $table_id;
                $inputData['slot_time'] = $slot_range;
                $inputData['host_name'] = $request->getHost();
                $inputData['special_occasion'] = $special_occasion;
                $inputData['user_instruction'] = $special_instruction;
                $inputData['host'] = $host;
                $inputData['user_id'] = $userId;
                $inputData['fname'] = $fname;
                $inputData['lname'] = $lname;
                $inputData['email'] = $email;
                $inputData['phone'] = $phone;
                $inputData['mobile'] = $phone;
                $inputData['yield_override'] = $yieldOverride;
                $payment_integration_data = $this->getCancellationChargeInfo($restaurant_id);
                if($reservation->payment_status == config('reservation.payment_status.pending')){
                    if($payment_integration_data['success']){
                        if((isset($payment_integration_data['data']['charge']) && !empty($payment_integration_data['data']['charge'])) && (isset($payment_integration_data['data']['charge_type']) && !empty($payment_integration_data['data']['charge_type']))){
                            $tot_amount = $amount = $payment_integration_data['data']['charge'];
                            if($payment_integration_data['data']['charge_type']=="per_cover"){
                                $tot_amount = $amount*intval($party_size);
                            }
                            if(!empty($reservation->total_cancellation_charge) && (intval($reservation->total_cancellation_charge) && intval($tot_amount))){
                                $payment_change = true;
                            }
                            // Generate new unique key
                            if($payment_change){
                                $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now');
                                $payment_link_token = base64_encode($localCurrentTime->timestamp.'#'.$reservation->id);
                                //\Log::info($payment_link_token);
                                $inputData['cancellation_charge_type'] = $payment_integration_data['data']['charge_type'];
                                $inputData['cancellation_charge'] = $amount;
                                $inputData['total_cancellation_charge'] = $tot_amount;
                                $inputData['payment_status'] = config('reservation.payment_status.pending');
                                $inputData['payment_link_token'] = $payment_link_token;
                            }
                        }
                    }
                }
                $modify_mail_send_flag = $this->getModifySentMailFlag($reservation, $inputData);
                $reservation->update($inputData);
                if ($userId) {
                    CommonFunctions::changeUserCategory($userId);
                }
                DB::commit();
                $msg = 'Reservation details updated successfully.';
                if ($modify_mail_send_flag) {
                    if ($payment_change) {
                        try {
                            // generate payment link
                            \Log::info('Table Reservation payment event CALLED');
                            event(new TableReservationPaymentLink($reservation, 'Customer'));
                        } catch (\Exception $e) {
                            \Log::info('Notification not sent');
                            \Log::info($e);
                        }
                        $msg = 'Reservation details updated successfully. Payment link is sent to customer\'s email. Notify customer to complete reservation by making the payment within 15 minutes.';
                    } else {
                        try {
                            \Log::info('Table Reservation event CALLED');
                            event(new TableReservationModify($reservation, 'Manager'));
                        } catch (\Exception $e) {
                            \Log::info('Notification not sent');
                            \Log::info($e);
                        }
                    }
                }
                return \Response::json(['message' => $msg], 200);
            }
            return \Response::json(['errors' => ['Reservation could not be updated']], 422);
        } catch (\Exception $e) {
            DB::rollback();
            \Log::info($e);
            return \Response::json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /*
     * Change request TR2-15 Reservation modification mail should be sent when below things are modified
     * First Name
     * Email
     * Last Name
     * Phone Number
     * Number of People
     * Time
     * Date
     * Occasion
     * Special Instructions
     */
    private function getModifySentMailFlag($reservation, $inputData)
    {
        $flag = false;
        if($reservation->fname != $inputData['fname'] || $reservation->lname != $inputData['lname'] || $reservation->email != $inputData['email'] || $reservation->mobile != $inputData['mobile'] || $reservation->reserved_seat != $inputData['reserved_seat'] || $reservation->start_time != $inputData['start_time'] || $reservation->end_time != $inputData['end_time'] || $reservation->special_occasion != $inputData['special_occasion'] || $reservation->user_instruction != $inputData['user_instruction']){
            $flag = true;
        }
        return $flag;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {

    }

    public function getReservations(Request $request)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $keyword = $request->get('keyword') ? trim($request->get('keyword')) : '';
        $start_date = Carbon::createFromFormat('m-d-Y', $request->get('start_date'))->format('Y-m-d');
        $end_date = Carbon::createFromFormat('m-d-Y', $request->get('end_date'))->format('Y-m-d');
        $days = dateDiff($start_date, $end_date);
        if ($days == config('reservation.day')) {
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($start_date." 00:00:00"));
            $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($start_date." 24:00:00"));
            $reservations = Reservation::where('restaurant_id', \Auth::user()->restaurant_id)->whereHas('status', function($query) {
                $query->where('slug', '!=', config('reservation.status_slug.cancelled'));
            })->where('start_time', '<', $end_date_time)->where('end_time', '>', $start_date_time);
            if(!empty($keyword)) {
                $reservations = $reservations->where(function ($query) use ($keyword) {
                    $query->where('fname', 'like', '%' . $keyword . '%')
                        ->orWhere('lname', 'like', '%' . $keyword . '%')
                        ->orWhere(DB::raw("CONCAT( fname,  ' ', lname )"), 'like', '%' . $keyword . '%')
                        ->orWhere('email', 'like', '%' . $keyword . '%')
                        ->orWhere('mobile', 'like', '%' . $keyword . '%')
                        ->orWhere('receipt_no', 'like', '%' . $keyword. '%');
                });
            }
            $reservations = $reservations->orderBy('start_time', 'desc')->with('user')->get();
            $reservations = $this->getModifiedReservationsForListing($reservations);
            return json_encode(array("reservations" => $reservations));
        } elseif ($days == config('reservation.week_days')) {
            $reservations = $this->getWeekAppointmentSlotWiseGrouping($start_date, $end_date, $restaurant_id);
            return $reservations;
        } else {
            $reservations = $this->getMonthAppointmentSlotWiseGrouping($start_date, $end_date, $restaurant_id);
            //\Log::info($reservations);
            return $reservations;
        }
        //return $this->createEventsForAppointment($reservations);
    }

    private function getModifiedReservationsForListing($reservations)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        foreach ($reservations as $reservation){
            if(($reservation->status)){
                $reservation->waiting_icon =  $reservation->status->slug == config('reservation.status_slug.waitlist')? $reservation->status->slug : null;
            }
            if(isset($reservation->user) && isset($reservation->user->category)){
                $reservation->user_category_icon = strtolower($reservation->user->category);
            }
            $reservation->reservation_start_time = ($reservation->start_time)? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->start_time)->format('h:i A'): '';
            $reservation->reservation_start_date = ($reservation->start_time)? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->start_time)->format('m-d-Y'): '';

            $reservation->full_name = empty($reservation->fname) && empty($reservation->lname)? $name = 'Walk In' : $reservation->fname . ' ' . $reservation->lname;
            $reservation->payment_status = isset($reservation->payment_status) && !empty($reservation->payment_status)? ucfirst($reservation->payment_status==config('reservation.payment_status.charged')?'hold':$reservation->payment_status) : 'N/A';

            if($reservation->status){
                $status_color = ($reservation->status->color) ?json_decode($reservation->status->color, true):null;
                $reservation->status_color = isset($status_color['sidebar'])?$status_color['sidebar']:'';
                $status_icon = ($reservation->status->icon) ?json_decode($reservation->status->icon, true):null;
                $reservation->status_icon = isset($status_icon['sidebar'])?$status_icon['sidebar']:'';
                if($reservation->source == config('reservation.source.online')){
                    $reservation->source_icon = 'icon-online';
                }elseif($reservation->source == config('reservation.source.offline')){
                    $reservation->source_icon = 'fa fa-phone';
                }else{
                    $reservation->source_icon = 'icon-waiting-source';
                }
            }
        }
        return $reservations;
    }

    public function getArchive(Request $request)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $keyword = $request->get('keyword') ? trim($request->get('keyword')) : '';
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $date = $request->get('date');
        $reservations = Reservation::where('restaurant_id', \Auth::user()->restaurant_id)->with(['user', 'status']);
        if ($start_date) {
            $start_date = Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d');
            $start_date = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($start_date." 00:00:00"));
            $reservations = $reservations->where('start_time', '>=', $start_date);
        }
        if ($end_date) {
            $end_date = Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d');
            $end_date = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($end_date." 24:00:00"));
            $reservations = $reservations->where('end_time', '<=', $end_date);
        }
        if(!empty($keyword)) {
            $reservations = $reservations->where(function ($query) use ($keyword){
                $query->where('fname', 'like', '%' . $keyword. '%')
                    ->orWhere('lname', 'like', '%' . $keyword. '%')
                    ->orWhere(DB::raw("CONCAT( fname,  ' ', lname )"), 'like', '%' . $keyword. '%')
                    ->orWhere('email', 'like', '%' . $keyword. '%')
                    ->orWhere('mobile', 'like', '%' . $keyword. '%')
                    ->orWhere('receipt_no', 'like', '%' . $keyword. '%');
            });
        }
        if ($date) {
            $temp_date = Carbon::createFromFormat('m-d-Y', $date)->format('Y-m-d');
            $temp_date_start = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($temp_date." 00:00:00"));
            $temp_date_end = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($temp_date." 24:00:00"));
            $reservations =  $reservations->where('start_time', '<', $temp_date_end)->where('end_time', '>', $temp_date_start);

        }

        $reservations = $reservations->where('end_time', '<', Carbon::now())->orderBy('start_time', 'desc')->paginate(10);
        $view = 'reservation::reservation.partial.reservation_render';
        $reservations = $this->getModifiedReservationsForListing($reservations);

        if ($request->ajax()) {
            return view($view, compact('reservations'));
            exit;
        }
        return view('reservation::reservation.archive', compact('reservations', 'date'));
    }

    public function getAll(Request $request,$id=null)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $keyword = $request->get('keyword') ? trim($request->get('keyword')) : '';
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $reservations = Reservation::where('restaurant_id', Auth::user()->restaurant_id)->with(['user', 'status']);
        if(!empty($id)){
            $reservations = $reservations->where('id',$id);
        }
        if ($start_date || $end_date) {
            //$start_date = ($start_date) ? date('Y-m-d', strtotime($start_date)) : '';
            //$end_date = ($end_date) ? date('Y-m-d', strtotime($end_date)) : '';
            if ($start_date) {
                $start_date = Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d');
                $start_date = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($start_date." 00:00:00"));
                $reservations = $reservations->where('start_time', '>=', $start_date);
            }
            if ($end_date) {
                $end_date = Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d');
                $end_date = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($end_date." 24:00:00"));
                $reservations = $reservations->where('end_time', '<=', $end_date);
            }
        }
        if(!empty($keyword)) {
            $reservations = $reservations->where(function ($query) use ($keyword){
                $query->where('fname', 'like', '%' . $keyword. '%')
                    ->orWhere('lname', 'like', '%' . $keyword. '%')
                    ->orWhere(DB::raw("CONCAT( fname,  ' ', lname )"), 'like', '%' . $keyword. '%')
                    ->orWhere('email', 'like', '%' . $keyword. '%')
                    ->orWhere('mobile', 'like', '%' . $keyword. '%')
                    ->orWhere('receipt_no', 'like', '%' . $keyword. '%');
            });
        }
        if ($request->has('user_id')) {
            $reservations = $reservations->where('user_id', $request->get('user_id'))->get();
            $view = 'reservation::guestbook.partial.reservation-list';
        } else {
            $view = 'reservation::reservation.partial.reservation_render';
            $reservations = $reservations->orderBy('start_time', 'desc')->paginate(10);
        }
        $reservations = $this->getModifiedReservationsForListing($reservations);
        if ($request->ajax()) {
            return view($view, compact('reservations'));
            exit;
        }
        return view('reservation::reservation.all', compact('reservations'));
    }

    public function getUpcoming(Request $request)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $keyword = $request->get('keyword') ? trim($request->get('keyword')) : '';
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $date = $request->get('date');
        $reservations = Reservation::where('restaurant_id', \Auth::user()->restaurant_id)->with(['user', 'status'])->whereHas('status', function($query) {
            $query->where('slug', '!=', config('reservation.status_slug.cancelled'));
        });
        if ($start_date || $end_date) {
            //$start_date = ($start_date) ? date('Y-m-d', strtotime($start_date)) : '';
            //$end_date = ($end_date) ? date('Y-m-d', strtotime($end_date)) : '';
            if ($start_date) {
                $start_date = Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d');
                $start_date = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($start_date." 00:00:00"));
                $reservations = $reservations->where('start_time', '>=', $start_date);
            }
            if ($end_date) {
                $end_date = Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d');
                $end_date = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($end_date." 24:00:00"));
                $reservations = $reservations->where('end_time', '<=', $end_date);
            }
        }
        if(!empty($keyword)) {
            $reservations = $reservations->where(function ($query) use ($keyword){
                $query->where('fname', 'like', '%' . $keyword. '%')
                    ->orWhere('lname', 'like', '%' . $keyword. '%')
                    ->orWhere(DB::raw("CONCAT( fname,  ' ', lname )"), 'like', '%' . $keyword. '%')
                    ->orWhere('email', 'like', '%' . $keyword. '%')
                    ->orWhere('mobile', 'like', '%' . $keyword. '%')
                    ->orWhere('receipt_no', 'like', '%' . $keyword. '%');
            });
        }
        if ($date) {
            $reservations = $reservations->where(function ($query) use ($date, $restaurant_id) {
                $temp_date = Carbon::createFromFormat('m-d-Y', $date)->format('Y-m-d');
                $temp_date_start = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($temp_date." 00:00:00"));
                $temp_date_end = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($temp_date." 24:00:00"));
                $reservations =  $query->where('start_time', '<', $temp_date_end)->where('end_time', '>', $temp_date_start)->get();
                $reservations = $this->getModifiedReservationsForListing($reservations);
                return $reservations;
            });
        } else {
            $current_date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d');
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($current_date." 00:00:00"));
            $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($current_date." 24:00:00"));
            $reservations = $reservations->where('start_time', '>', $end_date_time)/*->where('end_time', '>', $start_date_time)*/;
        }
        $reservations = $reservations->orderBy('start_time', 'desc')->paginate();
        $reservations = $this->getModifiedReservationsForListing($reservations);
        if ($request->ajax()) {
            return view('reservation::reservation.partial.reservation_render', compact('reservations'));
            exit;
        }
        \Log::info('not here');
        return view('reservation::reservation.upcoming', compact('reservations', 'date'));
    }

    public function getReservationDetails($id, Request $request)
    {
        $restaurant_id = \Auth::user()->restaurant_id;
        $reservationDetail = Reservation::where('restaurant_id', $restaurant_id)->where('id', $id)->with(['user', 'status'])->first();
        $walkInFlag = $request->input('walk_in') ?? 0;
        if (!$reservationDetail->floor_id && !$walkInFlag) {
            $reservationDetail->extra_fields = $reservationDetail->extra_fields ? json_decode($reservationDetail->extra_fields): null;
            $reservationDetail->start_time = ($reservationDetail->start_time)? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservationDetail->start_time)->format('m-d-Y H:i'): '';

            return json_encode(array("reservation_detail" => $reservationDetail));
        }
        //with(['table.floor', 'user', 'status'])->
        if(strpos($reservationDetail->table_id, ',')) {
            $multiTableIds = explode(',', $reservationDetail->table_id);
            if(count($multiTableIds)) {
                $tableDetail = Table::where('restaurant_id', $restaurant_id)->whereIn('id', $multiTableIds)->with('floor')->get();
                if($tableDetail) {
                    $tempTableDetail = $tableDetail->toArray();
                    $tableNames = implode('+', array_column($tempTableDetail, 'name'));
                    $floorName = isset($tempTableDetail[0]['floor']['name']) ? $tempTableDetail[0]['floor']['name'] : '';
                    $reservationDetail->table = $tableDetail;
                    $reservationDetail->floor_name = $floorName;
                    $reservationDetail->table_name = $tableNames;
                }
            }
        } else {
            $reservationDetail->load('table.floor');
        }
        $reservation_start_time = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservationDetail->start_time);
        $cancelFlag = false;
        $statusChangeFlag = false;
        $can_modify_reservation = false;
        if($reservationDetail->source==config('reservation.source.walk_in') || $reservationDetail->payment_status!=config('reservation.payment_status.charged')){
            $can_modify_reservation = true;
        }
        $reservationDetail->reservation_start_time = ($reservationDetail->start_time)? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservationDetail->start_time)->format('h:i A'): '';
        $reservationDetail->reservation_start_date = ($reservationDetail->start_time)? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservationDetail->start_time)->format('m-d-Y'): '';
        $current_date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'm-d-Y');

        if (in_array($reservationDetail->status->slug, config('reservation.status_slug_to_be_update'))) {
            if ($reservationDetail->start_time > Carbon::now()){
                $type = config('reservation.reservation_category.upcoming');
                $cancelFlag = $statusChangeFlag = true;
            }elseif($reservationDetail->start_time <= Carbon::now() && $reservationDetail->end_time > Carbon::now()){
                $type = config('reservation.reservation_category.current');
                $cancelFlag = $statusChangeFlag = true;
            }else{
                $type = config('reservation.reservation_category.archive');
            }
        }else{
            $type = config('reservation.reservation_category.archive');
            if(($reservationDetail->start_time <= Carbon::now() && $reservationDetail->end_time > Carbon::now()) || (in_array($reservationDetail->status->slug, config('reservation.past_reservation_status_change')) && $reservationDetail->reservation_start_date==$current_date)) {
                $statusChangeFlag = true;
                $type = config('reservation.reservation_category.current');
            }
        }
        $reservationDetail->can_change_status = $statusChangeFlag;
        if(isset($reservationDetail->user) && !empty($reservationDetail->user)) {
            if(isset($reservationDetail->user->tags) && !empty($reservationDetail->user->tags)){
                $reservationDetail->user->tags = json_decode($reservationDetail->user->tags);
            }
            unset($reservationDetail->user->stripe_customer_id);
        }
        $reservationDetail->is_cancellable = $cancelFlag;
        $reservationDetail->can_modify_reservation = $can_modify_reservation;

        if($walkInFlag) {
            $type = config('reservation.reservation_category.waitlist');
        }
        $reservationDetail->next_status = app('Modules\Reservation\Http\Controllers\FloorController')->getNextReservationStatuses($reservationDetail, $type, $reservation_start_time);
        $reservationDetail->status_name = $reservationDetail->status->name;
        $reservationDetail->status_icon = ($reservationDetail->status->icon) ? json_decode($reservationDetail->status->icon, true) : '';
        $reservationDetail->status_color = ($reservationDetail->status->color) ? json_decode($reservationDetail->status->color, true) : '';
        if ($reservationDetail->status->slug == config('reservation.status_slug.cancelled')) {
            $updated_at = $reservationDetail->updated_at;
            $reservationDetail->cancelled_at = ($updated_at) ? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, Carbon::parse($updated_at))->format('m-d-Y h:i A'): '';
            $reservationDetail->total_charged_amount = ($reservationDetail->total_charged_amount) ? intval($reservationDetail->total_charged_amount) : 0;

        }
        $reservationDetail->cancellation_charge = ($reservationDetail->cancellation_charge) ? intval($reservationDetail->cancellation_charge) : 0;
        $reservationDetail->payment_status = isset($reservationDetail->payment_status) && !empty($reservationDetail->payment_status)? ucfirst($reservationDetail->payment_status==config('reservation.payment_status.charged')?'hold':$reservationDetail->payment_status) : 'N/A';
        $total_resv_count = $total_walk_in_count = $total_cancelled_count = $total_orders = 0;
        if ($reservationDetail) {
            $user_reservations = Reservation::where('user_id', $reservationDetail->user_id)->get();
            foreach ($user_reservations as $reservation) {
                if ($reservation->status->slug == config('reservation.status_slug.cancelled')) {
                    $total_cancelled_count++;
                }
                if ($reservation->source == config('reservation.source.walk_in')) {
                    $total_walk_in_count++;
                }
                $total_resv_count++;
                $total_orders++;
            }
            unset($reservationDetail->stripe_charge_id);
            unset($reservationDetail->stripe_card_id);
        }
        \Log::info($reservationDetail->next_status);
        $tags = app('Modules\Reservation\Http\Controllers\FloorController')->gettagslist();
        return json_encode(array("reservation_detail" => $reservationDetail, 'tags' => $tags, 'total_resv_count' => $total_resv_count, 'total_walk_in_count' => $total_walk_in_count, 'total_cancelled_count' => $total_cancelled_count, 'total_orders' => $total_orders));
    }

    public function cancelReservation($id, Request $request)
    {
        $cancellation_reason = $request->get('cancellation_reason');
        $reservationDetail = Reservation::where('restaurant_id', \Auth::user()->restaurant_id)->where('id', $id)->where('start_time', '>', Carbon::now())->first();
        \Log::info($reservationDetail);
        if (!$reservationDetail) {
            return json_encode(['success' => false, 'message' => 'Reservation could not be cancelled']);
        }
        if ($reservationDetail) {
            $cancelled_status_id = ReservationStatus::getCancelledStatusId();
            $reservationDetail->status_id = $cancelled_status_id;
            $reservationDetail->cancellation_reason = $cancellation_reason;
            $reservationDetail->save();
            if($reservationDetail->user_id){
                CommonFunctions::changeUserCategory($reservationDetail->user_id);
            }
            event(new TableReservationCancel($reservationDetail,'Manager'));
        }
        return json_encode(['success' => true, 'message' => 'Reservation has been cancelled successfully']);
    }

    public function getReservationCountBetweenDates($start_date_time, $end_date_time, $restaurant_id, $table_id = NULL, $reservation_id=NULL)
    {
        $reservationArr = array();
        $reservations = Reservation::where('restaurant_id', $restaurant_id);//->with('table');
        if($reservation_id){
            $reservations = $reservations->where('id', '!=', $reservation_id);
        }
        $reservations = $reservations->whereHas('status', function($query) {
                $query->whereIn('slug', config('reservation.status_slug_check_for_book_reservation'));
            })->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time);
        if (is_array($table_id)) {
            $reservations = $reservations->whereRaw("CONCAT(',', table_id, ',') REGEXP ',(".implode('|', $table_id).")'");
        } elseif ($table_id) {
            $reservations = $reservations->whereRaw("FIND_IN_SET('".$table_id."', table_id)");
        }
        $reservations = $reservations->groupBy('table_id')->select('table_id', DB::raw("sum(reserved_seat) as reserved_seat"), DB::raw("count(*) as table_count"))->get();
        if ($reservations->count() > 0) {
            foreach ($reservations as $reservation) {
                if($reservation->table_id){
                    if (strpos($reservation->table_id, ',')) {
                        $multiTableIds = explode(',', $reservation->table_id);
                        if (count($multiTableIds)) {
                            foreach ($multiTableIds as $multiTab) {
                                $tableDetail = Table::where('restaurant_id', $restaurant_id)->where('id', '=', $multiTab)->first();
                                if ($tableDetail) {
                                    $tempTableDetail                            = $tableDetail->toArray();
                                    $reservationArr[$multiTab]['table_id']      = $multiTab;
                                    $reservationArr[$multiTab]['reserved_seat'] = $reservation->reserved_seat;
                                    $reservationArr[$multiTab]['table_count']   = $reservation->table_count;
                                    $reservationArr[$multiTab]['table_type']    = $tempTableDetail['max'];
                                }
                            }
                        }
                        //\Log::info([$reservation,$tableNames,$floorName,$reservation->table->max]);die();
                    } else {
                        $reservation->load('table');
                        \Log::info($reservation);
                        $reservationArr[$reservation->table_id]['table_id']      = $reservation->table_id;
                        $reservationArr[$reservation->table_id]['reserved_seat'] = $reservation->reserved_seat;
                        $reservationArr[$reservation->table_id]['table_count']   = $reservation->table_count;
                        $reservationArr[$reservation->table_id]['table_type']    = ($reservation->table) ? $reservation->table->max:null;
                    }
                }
            }
        }
        return $reservationArr;
    }

    public function userReservation()
    {
        $restaurant_id = Input::get('restaurant_id');
        $slots = getSlotsFromTimeRange(config('reservation.restaurant_timing.open_time'), config('reservation.restaurant_timing.close_time'), config('reservation.slot_interval_time'), true);
        $partySizeArr = Restaurant::where('id', $restaurant_id)->select(DB::raw('REPLACE(json_extract(reservation_settings, "$.reservation.min_for_online_reservation"), \'"\', \'\')  as min, REPLACE(json_extract(reservation_settings, "$.reservation.max_for_online_reservation"), \'"\', \'\') as max'))->first()->toArray();

        // as discussed with pravish, if setting is not exist then default should be 1 to 10
        $min_party = !empty($partySizeArr) && (($partySizeArr['min']) && $partySizeArr['min'] > 0) ? $partySizeArr['min'] : 1;
        $max_party = !empty($partySizeArr) && (($partySizeArr['max']) && $partySizeArr['max'] > 0) ? $partySizeArr['max'] : 10;
        return view('reservation::reservation.add', compact('slots', 'min_party', 'max_party'));
    }

    public function checkAvailabilityAndGetAvailableSlots(CheckValidity $request)
    {
        $source = $request->input('source');
        $walkInFlag = $request->input('walk_in');
        $restaurant_id = $request->has('restaurant_id') ? $request->get('restaurant_id') : Auth::user()->restaurant_id;
        if($walkInFlag) {
            // slot interval time
            // as discussed with Prabish, current time 10:15 slots 10:00-10:30 & 10:30-11:00, start 10:15 end 10:45
            //$reservation_date = Carbon::now()->format('Y-m-d');//date('Y-m-d', time());
            //$start_time       = Carbon::now()->format('H:i');//date('H:i', time());
            // timezone changes
            $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now');
            $reservation_date = $localCurrentTime->format('Y-m-d');//date('Y-m-d', time());
            $start_time       = $localCurrentTime->format('H:i');//date('H:i', time());
        } else {
            $reservation_date = $request->get('reservation_date');
            $reservation_date = Carbon::createFromFormat('m-d-Y', $reservation_date)->format('Y-m-d');
            $start_time = $request->get('reservation_time');
            $start_time = $request->has('slot_time') ? $request->get('slot_time') : $start_time;
            $start_time = date('H:i', strtotime($start_time));
        }
        $party_size = $request->get('party_size');
        //$table_id = $request->get('table_id');
        //$floor_id = $request->get('floor_id');
        $table_id= $floor_id= NULL;
        $reservation_id = $request->get('reservation_id');
        $response = $this->checkAvailability($reservation_date, $start_time, $party_size, $floor_id, $table_id, $walkInFlag, $restaurant_id, $reservation_id,$source);
        if(empty($response['errors'])){
            return \Response::json($response, 200);
        }
        return \Response::json($response, 422);
    }

    private function checkAvailability($reservation_date, $start_time, $party_size, $floor_id=NULL, $table_id = NULL, $walkInFlag=NULL, $restaurant_id, $reservation_id=NULL, $source)
    {
        $ms = microtime(true);
        $warning_message = null;
        $start_date_time = Carbon::parse($reservation_date.' '.$start_time)->format('Y-m-d H:i:s');
        //\Log::info($walkInFlag);
        $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now');
        if (!$reservation_id && !$walkInFlag && $start_date_time < $localCurrentTime) {
            $me = microtime(true) - $ms;
            return ['errors' => ['Reservations cannot be made for a past time.'], 'xtime' => $me];
        }
        $reservationInfo = Reservation::where('id', $reservation_id)->whereHas('status', function($query) {
            $query->whereIn('slug', config('reservation.status_slug_to_be_update'));
        })->first();
        $tat = 0;
        if($reservationInfo) {
            if($walkInFlag && $reservationInfo->end_time <= Carbon::now()){
                $me = microtime(true) - $ms;
                return ['errors' => ['Reservations cannot be updated for a past time'], 'xtime' => $me];
            }else if(!$walkInFlag && $start_date_time < $localCurrentTime){
                $me = microtime(true) - $ms;
                return ['errors' => ['Reservations cannot be updated for a past time'], 'xtime' => $me];
            }
            $tat = Carbon::parse($reservationInfo->end_time)->diffInMinutes(Carbon::parse($reservationInfo->start_time));
        }
      
        $global_setting_resp = $this->checkGlobalSettings($restaurant_id, $source, $start_date_time, $party_size);
        if(!$global_setting_resp['success']){
            $me = microtime(true) - $ms;
            return ['errors' => [$global_setting_resp['message']], 'xtime' => $me];
        }
        try {
        $scheduled_hours = $this->getWorkingHours($reservation_date, $restaurant_id);
        if (!$scheduled_hours) {
            $me = microtime(true) - $ms;
            return ['errors' => ['Restaurant is not operational currently. This could be because the timeslot was not defined.'], 'xtime' => $me];
        }
        if ($scheduled_hours->is_dayoff) {
            $me = microtime(true) - $ms;
            return ['errors' => ['Restaurant is closed. No reservations can be made on this date.'], 'xtime' => $me];
        }
        $slots_data = $this->getAvailableSlots($scheduled_hours);
        if (!$slots_data['success']) {
            $me = microtime(true) - $ms;
            return ['errors' => [$slots_data['message']], 'xtime' => $me];
        }

        $tat = $this->getTAT($tat, $slots_data, $restaurant_id, $party_size, $start_time, $walkInFlag);
        if (isset($tat['success']) && !$tat['success']) {
            $me = microtime(true) - $ms;
            return ['errors' => [$tat['message']], 'xtime' => $me];
        }
        $end_time = Carbon::parse($start_time)->addMinutes($tat)->format('H:i:s');
        $time_interval = config('reservation.slot_interval_time');
        // NEW WALK IN
        if(!is_null($table_id) && is_string($table_id) && strpos($table_id, ',')) {
            $table_id = explode(',', $table_id);
        }
        if($walkInFlag || (isset($reservationInfo->source) && $reservationInfo->source == config('reservation.source.walk_in'))) {
           $newFloorDataArr = $this->walkInAvailibility($slots_data, $start_time, $end_time, $party_size,$reservation_date,$restaurant_id,$floor_id,$table_id, $reservation_id, $source, $tat, $warning_message);
            if(isset($newFloorDataArr['data']) && !is_null($newFloorDataArr['data'])) {
                // check if table, floor data is there
                $floor_id      = $reservationInfo->floor_id ?? '';
                if(!empty($floor_id)) {
                    $table_ids     = array_filter(@explode(',', $reservationInfo->table_id));
                    $j             = 0;
                    foreach ($newFloorDataArr['data']['floor_table_info'] as $floors) {
                        if ($floor_id == $floors['id']) {
                            $tableArr = array_column($floors['tables'], 'id');
                            for ($i = 0; $i < count($table_ids); $i++) {
                                if (!in_array($table_ids[$i], $tableArr)) {
                                    $tempTablesArr                                               = Table::where('id', $table_ids[$i])->select('id', 'floor_id', 'name', 'min', 'max')->first();
                                    $newFloorDataArr['data']['floor_table_info'][$j]['tables'][] = $tempTablesArr;
                                }
                            }
                        }
                        $j++;
                    }
                }
                $me = microtime(true) - $ms;
                $warning_message = $warning_message ? $warning_message : $newFloorDataArr['message'];
                return ['data' => $newFloorDataArr['data'], 'errors'=>null, 'message' => $warning_message, 'xtime' => $me];
            } else {
                $errorMsg = isset($newFloorDataArr['message']) && !empty($newFloorDataArr['message']) ? $newFloorDataArr['message'] : 'Covers not available';
                $me = microtime(true) - $ms;
                return ['data' =>null, 'errors' => [$errorMsg], 'message' => $errorMsg, 'xtime' => $me];
            }
        }

        if (isStartTimeEndTimeExistInTimeRangeArr($slots_data['slots_availability']['slot_range'], $start_time, $end_time)) {
            if($slots_data['slots_availability']['max_covers_limit'][$start_time] < $party_size || $slots_data['slots_availability']['table_count'][$start_time] < 1)
            {
                if($source == config('reservation.source.online')){
                    $me = microtime(true) - $ms;
                    return ['errors' => ['There is no table available at the restaurant for the requested time. Please choose another time.'], 'xtime' => $me];
                }else{
                   $warning_message = 'Maximum covers limit for this shift has been reached. However, you can still book for this timeslot.';
                }
            }

            $response = $this->checkReservationSlotsAvailableForGivenSlot($start_time, $end_time, $party_size, $time_interval, $reservation_date, $restaurant_id, $slots_data['slots_availability'], $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
            if (!$response['success']) {
                $me = microtime(true) - $ms;
                return ['errors' => [$response['message']], 'xtime' => $me];
            }
            //\Log::info($response);
            // YIELD MANAGEMENT
            $newBookingDetail = [
                'start_time' => $start_time,
                'end_time'   => $end_time,
                'party_size' => $party_size,
            ];
            $yieldObj         = new YieldReservation();
            $yieldRes         = $yieldObj->optimize($newBookingDetail, $restaurant_id);
            if (isset($yieldRes['available_slots']) && count($yieldRes['available_slots'])) {
                $floorTableData          = $yieldObj->getFloorTableData($yieldRes, $response['data'],$party_size);
                $floorTableData['yield'] = 1;
            } else {
                $floorTableData          = $yieldObj->getAllFloorTableData($response['data'], $party_size);
                $floorTableData['yield'] = 0;
            }
            $floorTableData['floor_table_info'] = $response['data']['floor_table_info'];
            //$floorTableData = $response['data'];
            $me = microtime(true) - $ms;
            $warning_message = ($warning_message)?$warning_message:$response['message'];
            return ['data'=>$floorTableData,'errors'=>null, 'message'=>$warning_message, 'xtime' => $me];
        } elseif (checkEndTimeExceedFromRange($slots_data['slots_availability']['slot_range'], $start_time, $end_time)){
            $me = microtime(true) - $ms;
            if($source == config('reservation.source.online')){
                return ['errors' => ['There is no table available at the restaurant for the requested time. Please choose another time.'], 'xtime' => $me];
            }else{
                return ['errors' => ['Turnover time of selected party size is greater than the remaining shift time.'], 'xtime' => $me];
            }
        }
        else {
            //\Log::info('hhhh');
            if(!$reservation_id){
                $near_by_slots = $this->getNearBySlots($start_time, $end_time, $slots_data['slots'], $party_size, $time_interval, $reservation_date, $restaurant_id, $slots_data['slots_availability'], $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
                if (empty($near_by_slots)) {
                    $me = microtime(true) - $ms;
                    return ['errors' => ['No tables are available at the selected timeslot.'], 'xtime' => $me];
                }
                $me = microtime(true) - $ms;
                return ['data'=>['available_nearby_slots'=>$near_by_slots],'errors'=>null, 'message'=>'No tables are available at the selected timeslot. You can choose a nearby timeslot.', 'xtime' => $me];
            }
        }
        $me = microtime(true) - $ms;
        return ['errors' => ['No tables are available at the selected timeslot.'], 'xtime' => $me];
        }catch(\Exception $e){
            \Log::info($e);
            $me = microtime(true) - $ms;
            return ['errors' => [ $e->getMessage() ], 'xtime' => $me];
        }
    }

    public function intersectTableIds($arr1, $arr2) {
        $temp1      = array_reduce($arr1, 'array_merge', []);
        $temp2      = array_reduce($arr2, 'array_merge', []);
        $intersect  = array_unique(array_intersect($temp1,$temp2));
        $res = [];
        for($i=0; $i<count($arr1); $i++) {
            $temp = [];
            for($j=0; $j<count($arr1[$i]); $j++) {
                if(in_array($arr1[$i][$j], $intersect)) {
                    $temp[] = $arr1[$i][$j];
                }
            }
            if(count($temp)){
                $res[] = array_unique($temp);
            }
        }
        return $res;
    }
    /*public function compareDeepValues($val1, $val2)
    {
        return strcmp(serialize($val1), serialize($val2));
    }*/

    /**
     * User walkin functionality, check for overlapping slots availibility
     * @param $slots_data
     * @param $start_time
     * @param $end_time
     * @param $party_size
     * @param $reservation_date
     * @param $restaurant_id
     * @param $floor_id
     * @param $table_id
     * @return array
     */
    public function walkInAvailibility($slots_data, $start_time, $end_time, $party_size, $reservation_date, $restaurant_id, $floor_id, $table_id, $reservation_id=NULL, $source, $tat, $warning_message=NULL)
    {
        $source = config('reservation.source.walk_in');
        $time_interval = config('reservation.slot_interval_time');
        $i             = 0;
        $tableCount    = $slots_data['slots_availability']['table_count'];
        $startIndex    = -1;
        $endIndex      = -1;
        // check for available slots
        //return [$tableCount, array_slice($tableCount, 4, 1)];
        ksort($tableCount);
        foreach ($tableCount as $slotsKey => $slotsValue) {
            if (strtotime($slotsKey) >= strtotime($start_time) && $startIndex < 0) {
                //return [$slotsKey,$start_time,$i,$tableCount];
                $startIndex = $i-1;
            }
            if (strtotime($slotsKey) >= strtotime($end_time) && $endIndex < 0) {
                $endIndex = $i-1;
            }
            $i++;
        }
        if($startIndex < 0 || $endIndex <= 0) {
            return ['data' => null, 'message' => 'No tables are available at the selected timeslot.', 'success' => false];
        }

        $slotsCount = $endIndex - $startIndex + 1;
        $floorData = [];
        $temp = [];
        $startKey = $startIndex;
        $slotRange = [];
        for($i=0; $i<$slotsCount; $i++) {
            $startKeyTime   = key(array_slice($tableCount, $startKey, 1));
            $startKey++;
            $endKeyTime     = key(array_slice($tableCount, $startKey, 1));
            $temp[] = [$startKeyTime,$endKeyTime];
            \Log::info($startKeyTime."****".$endKeyTime);
            $floorData[$i] = $this->checkReservationSlotsAvailableForGivenSlot($startKeyTime, $endKeyTime, $party_size, $time_interval, $reservation_date, $restaurant_id, $slots_data['slots_availability'], $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
            \Log::info($floorData[$i]);
            $slotRange[] = isset($floorData[$i]['data']['slot_range']) ? $floorData[$i]['data']['slot_range'] : '';
        }
        //\Log::info($floorData);
        if(isset($floorData[0]['success']) && !$floorData[0]['success']) {
            $errorMsg = $floorData[0]['message'] ? $floorData[0]['message'] : 'No tables are available at the selected timeslot.';
            return ['data' => null, 'message' => $errorMsg, 'success' => false];
        }
        // check if all slots are equal
        if(count(array_unique($slotRange)) != 1 && end($slotRange) != '') {
            // all slot range aren't same
            return ['data' => null, 'message' => 'Turnover time of selected party size is greater than the remaining shift time.', 'success' => false];
        } else {
            // if slots available then start booking

            // find floor intersect
            $floorTableIds = [];
            foreach($floorData as $key => $value) {
                $floorTableIds[] = isset($value['data']['floor_table_info']) ? array_column($value['data']['floor_table_info'], 'id') : '';
            }
            // get all floor and table ids
            $floorIds = [];
            $tableIds = [];
            $i = 0;
            foreach($floorData as $floors) {
                if(isset($floors['data']['floor_table_info'])) {
                    $floorIds[] = array_column($floors['data']['floor_table_info'], 'id');
                    foreach($floors['data']['floor_table_info'] as $floor) {
                        $tableIds[$i][] = isset($floor['tables']) ? array_column($floor['tables'], 'id') : '';
                    }
                    $i++;
                }
            }
            // get common floor ids
            $commonFloorIds = [];
            for($i=0; $i<count($floorIds); $i++) {
                if(count($commonFloorIds)) {
                    $commonFloorIds = array_intersect($floorIds[$i], $commonFloorIds);
                } elseif(isset($floorIds[$i+1])) {
                    $commonFloorIds = array_intersect($floorIds[$i], $floorIds[$i+1]);
                }
            }
            // get common table ids
            $commonTableIds = [];
            for($i=0; $i<count($tableIds); $i++) {
                if(count($commonTableIds) && isset($tableIds[$i])) {
                    $commonTableIds = $this->intersectTableIds($commonTableIds, $tableIds[$i]);
                    //$commonTableIds = array_uintersect($commonTableIds, $tableIds[$i], 'compareDeepValues');
                } elseif(isset($tableIds[$i]) && isset($tableIds[$i+1])) {
                    $commonTableIds = $this->intersectTableIds($tableIds[$i], $tableIds[$i + 1]);
                    //$commonTableIds = array_uintersect($tableIds[$i], $tableIds[$i + 1], 'compareDeepValues');
                }
            }
            if(count($commonTableIds)) {
                $commonTableIds = array_values($commonTableIds);
            }
            //return [$floorIds,$tableIds,$commonFloorIds,$commonTableIds];
            $newFloorDataArr = [];

            $temp = [];
            foreach($floorData as $floors) {
                if(isset($floors['data']['floor_table_info'])) {
                    $tablesArr = [];
                    $tb = 0;

                    foreach($floors['data']['floor_table_info'] as $floor) {
                        foreach($floor['tables'] as $tables) {
                            if(isset($commonTableIds[$tb]) && in_array($tables['id'], $commonTableIds[$tb])) {  //if(in_array($tables['id'], $commonTableIds[$tb])) {
                                $tablesArr[$tb][] = $tables;
                            }
                        }
                        // check if multiple tables
                        $reqTablesFlag = false;
                        if(!is_array($table_id)) {
                            $reqTablesFlag = true;
                        } elseif(is_array($table_id) && array_column($tablesArr[$tb], 'id')==array_map('intval',$table_id)) {
                            $reqTablesFlag = true;
                        }
                        $floorIdExists = isset($newFloorDataArr['data']['floor_table_info']) ? in_array($floor['id'], array_column($newFloorDataArr['data']['floor_table_info'], 'id')) : false;
                        if(in_array($floor['id'], $commonFloorIds) && isset($tablesArr[$tb]) && count($tablesArr[$tb]) && !$floorIdExists && $reqTablesFlag) {
                            $temp[] = $floor['id'];
                            $newFloorDataArr['data']['floor_table_info'][] = [
                                'id'        => $floor['id'],
                                'name'      => $floor['name'],
                                'tables'    => $tablesArr[$tb]
                            ];
                            $newFloorDataArr['data']['slot_range'] = $floors['data']['slot_range'];
                        }
                        $tb++;
                    }
                }
            }
            //\Log::info($newFloorDataArr);\Log::info('grgrgrgr');
            if (count($newFloorDataArr) && isset($newFloorDataArr['data'])) {
                $newFloorDataArr['data']['tat'] = $tat;
                $newFloorDataArr['message']     = isset($floorData[0]['message']) ? $floorData[0]['message'] : '';
                $newFloorDataArr['success']     = isset($floorData[0]['success']) ? $floorData[0]['success'] : '';

                return $newFloorDataArr;
            } else {
                return ['data' => null, 'message' => 'No tables are available at the selected timeslot.', 'success' => false];
            }
        }
    }

    /**
     * Waitlist - no tables available
     * Add user, party size, waiting time, instructions
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function waitListAdd(Request $request)
    {
        $ms     = microtime(true);
        $userId = $request->input('userid');
        $registerGuest = $request->input('register_guest') ?? 0;
        $restaurant_id = Auth::user()->restaurant_id;
        $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
        $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
        $restaurantData = Restaurant::find($restaurant_id);

        if ($userId < 1 && $registerGuest) {
            $userData = [
                'restaurant_id' => $parent_restaurant_id,
                'fname'         => $request->input('firstName'),
                'lname'         => $request->input('lastName'),
                'email'         => $request->input('email'),
                'password'      => bcrypt('123456'),
                'mobile'        => $request->input('phoneNo'),
                'phone'         => $request->input('phoneNo'),
            ];
            $user     = User::create($userData);
            $userId   = $user->id;
        }

        $resrvData   = [
            'user_id'          => $userId,
            'parent_restaurant_id'=>$parent_restaurant_id,
            'restaurant_id'    => $restaurant_id,
            'floor_id'         => null,
            'table_id'         => null,
            'fname'            => $request->input('firstName'),
            'lname'            => $request->input('lastName'),
            'email'            => $request->input('email'),
            'phone'            => $request->input('phoneNo'),
            'mobile'           => $request->input('phoneNo'),
            'start_time'       => Carbon::now()->format('Y-m-d'),
            'reserved_seat'    => $request->input('party_size'),
            'wait_time'        => $request->input('wait_time'),
            'source'           => 'Walk In',
            'notify_me'        => $request->input('sms_notification'),
            'user_instruction' => $request->input('instruction'),
            'status_id'        => ReservationStatus::getWaitlistStatusId(),
            'slot_time'        => null,
            'host_name'        => $request->getHost(),
            'user_ip'          => $request->ip(),
            'receipt_no'       => generateReservationReceipt(),
            'restaurant_name' => $restaurantData->restaurant_name
        ];
        $reservation = Reservation::create($resrvData);
        if ($userId) {
            CommonFunctions::changeUserCategory($userId);
        }
        if ($request->input('sms_notification') == 1) {
            event(new TableWaitlist($reservation, 'Manager','this is custom waitlist message'));
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => 1, 'message' => 'You have successfully added the customer to the waitlist.', 'xtime' => $me], 201);
    }

    /**
     * Online add to waitlist
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function onlineWaitListAdd(Request $request)
    {
        $ms              = microtime(true);
        $restaurantId    = $request->input('restaurant_id');
        $date            = $request->input('reservation_date');
        $time            = $request->input('reservation_time');
        $reservationDate = Carbon::createFromFormat('m-d-Y', $date)->format('Y-m-d');
        $startTime       = Carbon::createFromFormat('m-d-Y H:i', $date . ' ' . $time)->format('H:i:s');
        $source          = ucfirst($request->input('source')) ?? 'Online';
        $partySize       = $request->input('party_size');


        $scheduled_hours = $this->getWorkingHours($reservationDate, $restaurantId);
        $tat             = 0;
        if ($scheduled_hours) {
            $slots_data = $this->getAvailableSlots($scheduled_hours);
            if ($slots_data['success']) {
                $table_types   = app('Modules\Reservation\Http\Controllers\TurnOverTimeController')->getAllTableTypeInfo($restaurantId);
                $table_type_id = app('Modules\Reservation\Http\Controllers\TurnOverTimeController')->getTableTypeIdByPartySize($restaurantId, $partySize);
                if ($table_type_id) {
                    $tat = isset($slots_data['slots_availability']['party_size'][$startTime][$table_type_id]) && !empty($slots_data['slots_availability']['party_size'][$startTime][$table_type_id]) ? $slots_data['slots_availability']['party_size'][$startTime][$table_type_id] : $table_types[$table_type_id]['tat'];
                }
            }
        }
        // As discussed with Pravish if tat is not defined then consider it as defalut slot interval, do not show validation message
        if ($tat == 0) {
            $tat = config('reservation.slot_interval_time');
        }
        $startTime = $reservationDate . ' ' . $startTime;
        $endTime = Carbon::parse($startTime)->addMinutes($tat)->format('Y-m-d H:i:s');

        //return [$startTime,$endTime,$tat];

        $resrvData   = [
            /*'user_id'          => null,
            'floor_id'         => null,
            'table_id'         => null,
            'wait_time'        => null,
            'slot_time'        => null,*/
            'restaurant_id'    => $request->input('restaurant_id'),
            'fname'            => $request->input('fname'),
            'lname'            => $request->input('lname'),
            'email'            => $request->input('email'),
            'phone'            => $request->input('phone'),
            'mobile'           => $request->input('phone'),
            'start_time'       => $startTime,
            'end_time'         => $endTime,
            'reserved_seat'    => $partySize,
            'source'           => 'Online',
            'notify_me'        => $request->input('notify_me') ?? 0,
            'user_instruction' => $request->input('special_instructions'),
            'status_id'        => ReservationStatus::getWaitlistStatusId(),
            'host_name'        => $request->getHost(),
            'user_ip'          => $request->ip(),
            'receipt_no'       => generateReservationReceipt(),
        ];
        $reservation = Reservation::create($resrvData);
        //$reservation = Reservation::find(474);
        if ($request->input('notify_me') == 1) {
            event(new TableWaitlist($reservation, 'Manager', 'this is custom waitlist message'));
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => 1, 'message' => 'You have successfully added the customer to the waitlist.', 'xtime' => $me], 201);
    }

    /**
     * Online waitlist - callback URL from Clickatell
     * Check customer response if reservation is confirmed
     * @param Request $request
     */
    public function onlineWaitlistStatus(Request $request)
    {
        $data = $request->all();
        \Log::debug($data);

        if(0) {
            // get last 10 digit of mobile
            $mobile      = $request->input('mobile');
            $mobile      = substr($mobile, -10);
            $reservation = Reservation::where('mobile', $mobile)->first();

            dd($reservation);
            // get confirmed and cancelled status ids
            $confirmedStatusId = ReservationStatus::getConfirmedStatusId();
            $cancelledstatusId = ReservationStatus::getCancelledStatusId();

            // get the user's reservation status
            $userNewStatusId   = $request->input('status_id');

            // mark the reservation Confirmed or Cancelled
            if ($userNewStatusId == $confirmedStatusId) {
                $reservation->status_id = $confirmedStatusId;
            } else if ($userNewStatusId == $cancelledstatusId) {
                $reservation->status_id = $cancelledstatusId;
            }
            
            // update the reservation
            $reservation->save();
        }

    }

    /**
     * Update walkin user details
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function waitListUpdate($id, Request $request)
    {
        $ms     = microtime(true);
        $email = $request->input('email');
        $phone = $request->input('phoneNo');
        $fname = $request->input('firstName');
        $lname = $request->input('lastName');
        $userId = $request->input('userid');

        $registerCustomer = $request->input('register_customer') ?? 0;
        $parent_restaurant_id = Restaurant::where('id', \Auth::user()->restaurant_id)->value('parent_restaurant_id');
        $parent_restaurant_id = $parent_restaurant_id==0 ? \Auth::user()->restaurant_id : $parent_restaurant_id;
        if ($registerCustomer != 1) {
            if ($request->input('userid')) {
                $userId = $request->input('userid');
            } else {
                $userId = null;
            }
        } else {
            if ($request->input('userid')) {
                $userId = $request->input('userid');
            } else {
                $user = User::where('restaurant_id', $parent_restaurant_id)->where(function ($query) use ($email, $phone) {
                    $query->where('email', $email)->orWhere('mobile', $phone);
                })->first();
                $username = strtolower($fname) . "_" . strtolower($lname);
                if (!$user) {
                    $user = User::create(['restaurant_id' => $parent_restaurant_id, 'category' => config('reservation.guest_category.New'), 'email' => $email, 'phone' => $phone, 'fname' => $fname, 'lname' => $lname, 'status' => 1, 'username' => $username, 'password' => bcrypt('123456'), 'mobile' => $phone]);
                    $userId = $user->id;
                }
            }
        }

        $reservation                   = Reservation::find($id);
        $reservation->user_id          = $userId;
        $reservation->floor_id         = $request->input('floor_plan_walkin') ?? null;
        $reservation->table_id         = $request->input('table_plan_walkin') ?? null;
        $reservation->fname            = $request->input('firstName') ?? null;
        $reservation->lname            = $request->input('lastName') ?? null;
        $reservation->email            = $request->input('email') ?? null;
        $reservation->phone            = $request->input('phoneNo') ?? null;
        $reservation->mobile           = $request->input('phoneNo') ?? null;
        $reservation->reserved_seat    = $request->input('party_size');
        $reservation->wait_time        = $request->input('wait_time');
        $reservation->notify_me        = $request->input('sms_notification');
        $reservation->user_instruction = $request->input('instruction');
        $reservation->save();
        if ($userId) {
            CommonFunctions::changeUserCategory($userId);
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => 1, 'message' => 'Wait list updated.', 'xtime' => $me], 201);
    }

    /**
     * Send customer and template SMS notification to walkin user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function waitListSmsNotification(Request $request)
    {
        $ms     = microtime(true);
        $reservationId = $request->input('reservation_id');
        $customMessage = $request->input('custom_message') ?? '';
        $reservation = Reservation::find($reservationId);

        if(!empty($customMessage)) {
            event(new TableWaitlist($reservation, 'Manager',$customMessage));
        } else {
            event(new TableWaitlist($reservation, 'Manager'));
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => 1, 'message' => 'SMS successfully sent.', 'xtime' => $me], 201);
    }

    public function getWorkingHours($input_date, $restaurant_id)
    {
        // Check restaurant availability in custom working hours
        $custom_hours = CustomWorkingHours::where('calendar_date->start_date', '<=', $input_date)
            ->where('calendar_date->end_date', '>=', $input_date)
            ->where('restaurant_id', $restaurant_id)
            ->first();
        if ($custom_hours) {
            return $custom_hours;
        }
        // Check restaurant availability in week day working hours
        $input_day = getDayOfWeekByDate($input_date);
        $week_hours = WorkingHours::where('calendar_day', $input_day)
            ->where('restaurant_id', $restaurant_id)
            ->first();
        return $week_hours;
    }

    public function getAvailableSlots($hour_schedule)
    {
        $slot_detail = ($hour_schedule->slot_detail) ? json_decode($hour_schedule->slot_detail, true) : NULL;
        if (!$slot_detail) {
            return ['success' => false, 'message' => 'Restaurant is not operational currently. This could be because the timeslot was not defined.'];
        }
        $slot_arr = $slot_availability = array();
        foreach ($slot_detail as $slot) {
            if(!empty($slot)) {
                $slot_arr_new = json_decode($slot['pacing'], true);
                $available_table = ($slot['available_tables']) ? json_decode($slot['available_tables'], true):null;
                $table_available_count = array_sum(array_column($available_table, 'table_count'));
                $floors_available = json_decode($slot['floors'], true);
                $party_size = json_decode($slot['party_size'], true);
                $max_covers_limit = $slot['max_covers_limit'];
                $slot_range =  $slot['open_time'] . '-' . $slot['close_time'];
                if(!empty($slot_arr_new)){
                    foreach ($slot_arr_new as $slot_key=>$slot_time) {
                        if(isset($slot_time['time']) && !empty($slot_time['time'])) {
                            $slot_availability['floors'][$slot_time['time']] = $floors_available;
                            $slot_availability['party_size'][$slot_time['time']] = $party_size;
                            $slot_availability['table'][$slot_time['time']] = !empty($available_table)? $available_table:null;
                            $slot_availability['max_covers_limit'][$slot_time['time']] = $max_covers_limit;
                            $slot_availability['table_count'][$slot_time['time']] = $table_available_count;
                            //$slot_availability['cover_count'][$slot_time['time']] = $cover_available;
                            $slot_availability['slot_range'][$slot_time['time']] = $slot_range;
                            $slot_availability['pacing'][$slot_time['time']] = $slot_time;
                        }
                    }
                    $slot_arr = array_merge($slot_arr, $slot_arr_new);
                }
            }
        }
        $slot_arr = !empty($slot_arr)?array_sort($slot_arr):$slot_arr;
        return ['success' => true, 'slots' => $slot_arr, 'slots_availability' => $slot_availability];
    }

    public function getNearBySlots($start_time, $end_time, $slot_arr, $party_size, $time_interval, $reservation_date, $restaurant_id, $slot_availability, $floor_id, $table_id = NULL, $reservation_id=NULL, $source, $tat, $warning_message=NULL)
    {
        $available_block = array();
        $closest_blocks = [];
        $closest_before = timeBefore($start_time, $slot_arr);
        if ($closest_before) {
            $closest_blocks[] = $closest_before;
        }
        if ($closest_before) {
            $closest_before = timeBefore($closest_before, $slot_arr);
            if ($closest_before) {
                $closest_blocks[] = $closest_before;
            }
        }
        $closest_after = timeAfter($start_time, $slot_arr);
        if ($closest_after) {
            $closest_blocks[] = $closest_after;
        }
        if ($closest_after) {
            $closest_after = timeAfter($closest_after, $slot_arr);
            if ($closest_after) {
                $closest_blocks[] = $closest_after;
            }
        }
        $closest_blocks = !empty($closest_blocks) ? array_unique($closest_blocks) : $closest_blocks;
        foreach ($closest_blocks as $block) {
            $response = $this->checkReservationSlotsAvailableForGivenSlot($block, $end_time, $party_size, $time_interval, $reservation_date, $restaurant_id, $slot_availability, $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
            if ($response['success']) {
                $available_block[] = $block;
            }
        }
        $available_block = !empty($available_block) ? array_unique($available_block) : $available_block;
        return $available_block;
    }

    public function checkReservationSlotsAvailableForGivenSlot($start_time, $end_time, $party_size, $time_interval, $reservation_date, $restaurant_id, $slot_availability, $floor_id=NULL, $table_id = NULL, $reservation_id=NULL, $source, $tat, $warning_message=NULL)
    {
        //\Log::info($slot_availability);
        if (isStartTimeEndTimeExistInTimeRangeArr($slot_availability['slot_range'], $start_time, $end_time)) {
            if (isset($slot_availability) && $slot_availability['table'][$start_time] && !empty($slot_availability['table'][$start_time])) {
                $table_avail = $this->checkTableAvailableOrNot($slot_availability, $reservation_date, $start_time, $end_time, $party_size, $restaurant_id, $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
                if ($table_avail['success']) {
                    //\Log::info([$warning_message , $table_avail['message']]);
                    $table_avail['message'] = ($warning_message)?$warning_message:$table_avail['message'];
                    return $table_avail;
                }
                //return ['success' => false, 'message' => 'There is no table available at the restaurant for the requested time. Please choose another time.'];
                return ['success' => false, 'message' => $table_avail['message']];
            }
            return ['success' => false, 'message' => 'There is no table available at the restaurant for the requested time. Please choose another time.'];
        }
        return ['success' => false, 'message' => 'The restaurant is not operational at the requested time. Please choose another time.'];
    }

    public function reserveTable(Request $request)
    {
        $ms = microtime(true);
        $walkInFlag = $request->input('walk_in');
        $restaurant_id = $request->has('restaurant_id') ? $request->get('restaurant_id') : Auth::user()->restaurant_id;
        $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now');
        if ($walkInFlag) {
            $reservation_date = $localCurrentTime->format('Y-m-d');//date('Y-m-d', time());
            $wait_time        = ($request->input('wait_time'));
            $start_time       = $localCurrentTime->addMinutes($wait_time)->format('H:i');// date('H:i', time() + $wait_time * 60);
        } else {
            $reservation_date = $request->get('reservation_date');
            $reservation_date = Carbon::createFromFormat('m-d-Y', $reservation_date)->format('Y-m-d');
            $start_time = $request->get('reservation_time');
            $start_time = $request->has('slot_time') ? $request->get('slot_time') : $start_time;
            $start_time = Carbon::parse($start_time)->format('H:i'); //date('H:i', strtotime($start_time));
        }
        $table_id = $request->get('table_id');
        $floor_id = $request->get('floor_id');
        $party_size = $request->get('party_size');
        $source = $request->get('source');
        //\Log::info('*******ddddd*********'.$source);

        $response = $this->checkAvailability($reservation_date, $start_time, $party_size, $floor_id, $table_id, $walkInFlag, $restaurant_id, null, $source);
        if($response['errors']){
            return \Response::json($response, 422);
        }
        $end_time = Carbon::parse($start_time)->addMinutes($response['data']['tat'])->format('H:i:s');
        $response_data = $response['data'];
        $slot_range = $response_data['slot_range'];
        $fname = $request->get('fname');
        $lname = $request->get('lname');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $birthday = $request->get('dob');
        $host = $request->get('host_name');
        $birthday = !empty($birthday)?Carbon::createFromFormat('m-d-Y', $birthday)->format('Y-m-d'):null;
        $anniversary = $request->get('doa');
        $anniversary = !empty($anniversary)?Carbon::createFromFormat('m-d-Y', $anniversary)->format('Y-m-d'):null;
        $tags = $request->has('tags')&&!empty($request->get('tags')) ? explode(',', $request->get('tags')): null;
        $description = $request->has('guest_note') && !empty($request->get('guest_note')) ? $request->get('guest_note') : null;
        //$host = isset(Auth::user()->restaurant_id)?Auth::user()->id:null;
        $special_instruction = $request->get('special_instruction');
        $special_occasion = $request->has('special_occasion')?$request->get('special_occasion'):null;
        $special_occasion = !empty($special_occasion)?config('reservation.special_occasion.'.$special_occasion):null;
        $notify_me = $request->has('notify_me') ? $request->get('notify_me') : 0;
        $user_id = $request->get('user_id');
        $restaurant_id = \Auth::user()->restaurant_id;
        $restaurantData = Restaurant::find($restaurant_id);

        try {
            $parent_restaurant_id = Restaurant::where('id', $restaurant_id)->value('parent_restaurant_id');
            $parent_restaurant_id = $parent_restaurant_id==0 ? $restaurant_id : $parent_restaurant_id;
            if (count($response_data) <= 0) {
                $me = microtime(true) - $ms;
                return \Response::json(['errors' => ['Covers not available'], 'xtime' => $me], 422);
            }
            if ($walkInFlag) {
                $phone = $request->input('number');
                $email = $request->input('email');
                $fname = $request->input('first_name');
                $lname = $request->input('last_name');
                $registerCustomer = $request->input('register_customer');
                if ($registerCustomer != 1) {
                    if ($request->input('userid')) {
                        $userId = $request->input('userid');
                    } else {
                        $userId = null;
                    }
                } else {
                    if ($request->input('userid')) {
                        $userId = $request->input('userid');
                    } else {
                        $user = User::where('restaurant_id', $parent_restaurant_id)->where(function ($query) use ($email, $phone) {
                            $query->where('email', $email)->orWhere('mobile', $phone);
                        })->first();
                        $username = strtolower($fname) . "_" . strtolower($lname);
                        if (!$user) {
                            if (!empty($tags)) {
                                $tags = json_encode(array_unique($tags));
                            }
                            $user = User::create(['restaurant_id' => $parent_restaurant_id, 'category' => config('reservation.guest_category.New'), 'email' => $email, 'phone' => $phone, 'fname' => $fname, 'lname' => $lname, 'status' => 1, 'username' => $username, 'password' => bcrypt('123456'), 'mobile' => $phone, 'tags' => $tags, 'dob' => $birthday, 'doa' => $anniversary, 'description' => $description]);
                        } else {
                            if (!empty($tags)) {
                                $guest_tags_db = ($user->tags) ? array_filter(json_decode($user->tags)) : array();
                                $merged_array = array_merge($guest_tags_db, $tags);
                                $merged_array = array_unique($merged_array);
                                $user->tags = ($merged_array) ? json_encode(array_values($merged_array)) : NULL;
                            }
                            $user->dob = $birthday;
                            $user->doa = $anniversary;
                            $user->description = $description;
                            $user->save();
                        }
                        $userId = $user->id;
                    }
                }
                $start_date_time = getDateTime($reservation_date, $start_time);
                $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
                $end_date_time = getDateTime($reservation_date, $end_time);
                $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
                $notify_me = ($notify_me) ? 1 : 0;
                $seated_status_id = ReservationStatus::getSeatedStatusId();
                if(is_array($table_id)) {
                    $table_id = implode(',',$table_id);
                }
                //\Log::info($userId);
                $reservation = Reservation::create([
                  
                   'parent_restaurant_id'=>$parent_restaurant_id, 'restaurant_id' => $restaurant_id, 'user_id' => $userId, 'fname' => $fname, 'lname' => $lname, 'email' => $email, 'phone' => $phone, 'start_time' => $start_date_time, 'end_time' => $end_date_time, 'seated_at' => $start_date_time, 'status_id' => $seated_status_id, 'source' => config('reservation.source.walk_in'), 'reserved_seat' => $party_size, 'notify_me' => $notify_me, 'floor_id' => $floor_id, 'table_id' => $table_id, 'mobile' => $phone, 'slot_time' => $slot_range, 'host_name' => $request->getHost(), 'user_ip' => $request->ip(), 'receipt_no' => generateReservationReceipt(), 'special_occasion' => $special_occasion, 'user_instruction' => $special_instruction, 'host' => $host, 'restaurant_name' => $restaurantData->restaurant_name]);

                if ($userId) {
                    CommonFunctions::changeUserCategory($userId);
                }
                try {
                    \Log::info('TableReservation event CALLED');
                    event(new TableReservation($reservation, 'Customer'));
                } catch (\Exception $e) {
                    \Log::info($e);
                }
                $me = microtime(true) - $ms;

                return \Response::json(['data' => $response_data, 'message' => 'You have successfully seated the walk-in customer.', 'xtime' => $me], 200);
            }
            // reserve table for offline/online
            $floor_table_info = reset($response_data['floor_table_info']);
            if (count($floor_table_info) <= 0) {
                $me = microtime(true) - $ms;
                return \Response::json(['errors' => ['Reservation slots are not available'], 'xtime' => $me], 422);
            }
            $table_info = reset($floor_table_info['tables']);

            $userId = null;
            if (!empty($user_id)) {
                $user = User::where('restaurant_id', $parent_restaurant_id)->where('id', $user_id)->first();
                $user->dob = $birthday;
                $user->doa = $anniversary;
                $user->description = $description;
                if (!empty($tags)) {
                    $guest_tags_db = ($user->tags) ? array_filter(json_decode($user->tags)) : array();
                    $merged_array = array_merge($guest_tags_db, $tags);
                    $merged_array = array_unique($merged_array);
                    $user->tags = ($merged_array) ? json_encode(array_values($merged_array)) : NULL;
                }
                $user->save();
                $userId = $user->id;
            } else {
                $user = User::where('restaurant_id', $parent_restaurant_id)->where(function ($query) use ($email, $phone) {
                    $query->where('email', $email)->orWhere('mobile', $phone);
                })->first();
                if($request->has('register_guest')){
                    if ($user) {
                        return \Response::json(['errors' => ['User already registered']], 422);
                    }
                    $username = strtolower($fname) . "_" . strtolower($lname);
                    if (!empty($tags)) {
                        $tags = json_encode(array_unique($tags));
                    }
                    $user = User::create(['restaurant_id' => $parent_restaurant_id, 'category' => config('reservation.guest_category.New'), 'email' => $email, 'phone' => $phone, 'fname' => $fname, 'lname' => $lname, 'status' => 1, 'username' => $username, 'password' => bcrypt('123456'), 'mobile' => $phone, 'tags' => $tags, 'dob' => $birthday, 'doa' => $anniversary, 'description' => $description]);
                    $userId = $user->id;
                }else{
                    $userId = $user ? $user->id : null;
                }
            }
            $start_date_time = getDateTime($reservation_date, $start_time);
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
            $end_date_time = getDateTime($reservation_date, $end_time);
            $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
            $notify_me = ($notify_me) ? 1 : 0;
            $reserved_status_id = ReservationStatus::getReservedStatusId();
            // to discuss with RAHUL K.
            if(is_array($table_id)) {
                $table_id = implode(',',$table_id); // $table_info['id']
            }
            if(empty($table_id)){
                $table_id = $table_info['id'];
            }
            if($request->input('yield_cancelled') > 1) {
                $yieldOverride = 1;
            } else {
                $yieldOverride = 0;
            }
            $msg = 'You have successfully created the reservation.';
            $receipt_number = generateReservationReceipt();
            $data = ['parent_restaurant_id'=>$parent_restaurant_id, 'restaurant_id' => $restaurant_id, 'user_id' => $userId, 'fname' => $fname, 'lname' => $lname, 'email' => $email, 'phone' => $phone, 'start_time' => $start_date_time, 'end_time' => $end_date_time, 'status_id' => $reserved_status_id, 'source' => $source, 'reserved_seat' => $party_size, 'notify_me' => $notify_me, 'floor_id' => $floor_table_info['id'], 'table_id' => $table_id, 'mobile' => $phone, 'slot_time' => $slot_range,
                'host_name' => $request->getHost(),
                'user_ip' => $request->ip(),
                'receipt_no' => $receipt_number,
                'special_occasion' => $special_occasion,
                'user_instruction' => $special_instruction,
                'host' => $host, 'yield_override' => $yieldOverride, 'restaurant_name' => $restaurantData->restaurant_name];
            \Log::info($data);
            $reservation = Reservation::create($data);
            $payment_integration_data = $this->getCancellationChargeInfo($restaurant_id);
            \Log::info($payment_integration_data);
            if($payment_integration_data['success']){
                if((isset($payment_integration_data['data']['charge']) && !empty($payment_integration_data['data']['charge'])) && (isset($payment_integration_data['data']['charge_type']) && !empty($payment_integration_data['data']['charge_type']))){
                    $tot_amount = $amount = $payment_integration_data['data']['charge'];
                    if($payment_integration_data['data']['charge_type']=='per_cover'){
                        $tot_amount = $amount*intval($party_size);
                    }
                    // Generate new unique key

                    $reservation_update = Reservation::find($reservation->id);
                    $payment_link_token = base64_encode($localCurrentTime->timestamp.'#'.$reservation_update->id);
                    //\Log::info($payment_link_token);
                    $reservation_update->cancellation_charge_type = $payment_integration_data['data']['charge_type'];
                    $reservation_update->cancellation_charge = $amount;
                    $reservation_update->total_cancellation_charge = $tot_amount;
                    $reservation_update->payment_status = config('reservation.payment_status.pending');
                    $reservation_update->payment_gatway = config('reservation.payment_gateway_type.stripe');
                    $reservation_update->payment_link_token = $payment_link_token;
                    $reservation_update->save();
                }
                $msg = 'You have successfully created the reservation. Payment link is sent to customer\'s email. Notify customer to complete reservation by making the payment within 15 minutes.';
                try{
                    \Log::info('Table Reservation payment event CALLED');
                    event(new TableReservationPaymentLink($reservation_update, 'Customer'));
                }catch(\Exception $e){
                    \Log::info('Notification not sent');
                    \Log::info($e);
                }
            }else{
                try{
                    \Log::info('Table Reservation event CALLED');
                    event(new TableReservation($reservation, 'Customer'));
                }catch(\Exception $e){
                    \Log::info('Notification not sent');
                    \Log::info($e);
                }
            }

            if($userId){
               CommonFunctions::changeUserCategory($userId);
            }
            return \Response::json(['success' => true, 'message' => $msg], 200);
        }catch(\Exception $e){
            $me = microtime(true) - $ms;
            \Log::info($e);
            return \Response::json(['errors' => [$e->getMessage()], 'xtime' => $me], 500);
        }
    }

    private function createEventsForAppointment($reservations)
    {
        $total_resv_count = 0;
        $total_cover_count = 0;
        $events = [];
        foreach ($reservations as $key => $reservation) {
            $events[$key]['start'] = $reservation->start;
            $events[$key]['end'] = $reservation->end;

            $events[$key]['title'] = "<div class='time-wrapper'><div class='resv-container'><div class='resv-count'>$reservation->reservation_count</div><div class='event-text'>Reservations</div></div><div class='resv-container'><div class='resv-count'>$reservation->cover</div><div class='event-text'>Covers</div></div></div>";
            //$events[$key]['title'] = "<div class='time-wrapper' data-date='".Carbon::parse($reservation->start)->format('Y-m-d')."'><div class='resv-container'><div class='resv-count'>$reservation->reservation_count</div><div class='event-text'>Reservations</div></div><div class='resv-container'></div></div>";
            $total_resv_count += $reservation->reservation_count;
            $total_cover_count += $reservation->cover;
            $reservation_date = Carbon::parse($reservation->start)->format('Y-m-d'); // date('Y-m-d', strtotime($reservation->start));
            if ($reservation_date > Carbon::today()->toDateString()) {
                $url = url('/reservation/upcoming?date=' . Carbon::parse($reservation->start)->format('m-d-Y')); // date('Y-m-d', strtotime($reservation->start))
            } elseif ($reservation_date < Carbon::today()->toDateString()) {
                $url = url('/reservation/archive?date=' . Carbon::parse($reservation->start)->format('m-d-Y')); // date('Y-m-d', strtotime($reservation->start))
            } else {
                $url = url('/reservation');
            }
            $events[$key]['url'] = $url;
        }
        //\Log::info(json_encode(array("events" => $events, 'total_reservation' => $total_resv_count)));
        return json_encode(array("events" => $events, 'total_reservation' => $total_resv_count, 'total_cover_count'=>$total_cover_count));
    }

    private function getWeekAppointmentSlotWiseGrouping($start_date, $end_date, $restaurant_id)
    {
        $date_range = date_range($start_date, $end_date);
        $total_resv_count = 0;
        $total_cover_count = 0;
        $events = [];
        $i=0;
        foreach ($date_range as $date) {
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date . " 00:00:00"));
            $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date . " 24:00:00"));
            $reservations = Reservation::where('restaurant_id', \Auth::user()->restaurant_id)->whereHas('status', function ($query) {
                $query->whereNotIn('slug', [config('reservation.status_slug.cancelled'), config('reservation.status_slug.waitlist')]);
            })->select(DB::raw('count(*) as reservation_count'), DB::raw('sum(reserved_seat) as cover'), "slot_time")->where('start_time', '<', $end_date_time)->where('end_time', '>', $start_date_time)->groupBy("slot_time")->get();
            foreach ($reservations as $key => $reservation) {
                if ($reservation->slot_time) {
                    $time_range = $reservation->slot_time;
                    $time_range = explode('-', $time_range);
                    //\Log::info($time_range);
                    //$reservation_date = $date;
                    $reservation_date = $start_date_time->format('Y-m-d');
                    $start_time = Carbon::parse($time_range[0] . ":00")->format('H:i:s');
                    $reservation->start = Carbon::parse($reservation_date . " " . $start_time)->format('Y-m-d H:i:s');
                    $end_time = $time_range[1] . ":00";
                    $reservation->end = Carbon::parse($reservation_date . " " . $end_time)->format('Y-m-d H:i:s');
                    $events[$i]['start'] = $reservation->start;
                    $events[$i]['end'] = $reservation->end;

                    $events[$i]['title'] = "<div class='time-wrapper'><div class='resv-container'><div class='resv-count'>$reservation->reservation_count</div><div class='event-text'>Reservations</div></div><div class='resv-container'><div class='resv-count'>$reservation->cover</div><div class='event-text'>Covers</div></div></div>";
                    $total_resv_count += $reservation->reservation_count;
                    $total_cover_count += $reservation->cover;
                    //$reservation_date = Carbon::parse($reservation->start)->format('Y-m-d');
                    if ($reservation_date > Carbon::today()->toDateString()) {
                        $url = url('/reservation/upcoming?date=' . Carbon::parse($reservation->start)->format('m-d-Y'));
                    } elseif ($reservation_date < Carbon::today()->toDateString()) {
                        $url = url('/reservation/archive?date=' . Carbon::parse($reservation->start)->format('m-d-Y'));
                    } else {
                        $url = url('/reservation');
                    }
                    $events[$i]['url'] = $url;
                    $i++;
                }
            }
        }
        //\Log::info($events);
        return json_encode(array("events" => $events, 'total_reservation' => $total_resv_count, 'total_cover_count' => $total_cover_count));
    }

    private function getMonthAppointmentSlotWiseGrouping($start_date, $end_date, $restaurant_id)
    {
        $date_range = date_range($start_date, $end_date);
        $total_resv_count = 0;
        $total_cover_count = 0;
        $events = [];
        $i=0;
        foreach ($date_range as $key=>$date) {
            $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date . " 00:00:00"));
            $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date . " 24:00:00"));
            $reservations = Reservation::where('restaurant_id', \Auth::user()->restaurant_id)->whereHas('status', function ($query) {
                $query->whereNotIn('slug', [config('reservation.status_slug.cancelled'), config('reservation.status_slug.waitlist')]);
            })->where('start_time', '<', $end_date_time)->where('end_time', '>', $start_date_time)->select(DB::raw('count(*) as reservation_count'), DB::raw('sum(reserved_seat) as cover'))->groupBy('restaurant_id')->first();
                if (isset($reservations->reservation_count) && !empty($reservations->reservation_count)) {
                    $reservation_date = $date;
                    $reservations->start = Carbon::parse($reservation_date . " 00:00:00")->format('Y-m-d H:i:s');
                    $reservations->end = Carbon::parse($reservation_date . " 24:00:00")->format('Y-m-d H:i:s');
                    $events[$i]['start'] = $reservations->start;
                    $events[$i]['end'] = $reservations->end;
                    $events[$i]['title'] = "<div class='time-wrapper'><div class='resv-container'><div class='resv-count'>$reservations->reservation_count</div><div class='event-text'>Reservations</div></div><div class='resv-container'><div class='resv-count'>$reservations->cover</div><div class='event-text'>Covers</div></div></div>";
                    $total_resv_count +=$reservations->reservation_count;
                    $total_cover_count += $reservations->cover;
                    $reservation_date = Carbon::parse($reservations->start)->format('Y-m-d');
                    if ($reservation_date > Carbon::today()->toDateString()) {
                        $url = url('/reservation/upcoming?date=' . Carbon::parse($reservations->start)->format('m-d-Y'));
                    } elseif ($reservation_date < Carbon::today()->toDateString()) {
                        $url = url('/reservation/archive?date=' . Carbon::parse($reservations->start)->format('m-d-Y'));
                    } else {
                        $url = url('/reservation');
                    }
                    $events[$i]['url'] = $url;
                    $i++;

                }
            }
        return json_encode(array("events" => $events, 'total_reservation' => $total_resv_count, 'total_cover_count' => $total_cover_count));
    }

    private function checkTableAvailableOrNot($slot_availability, $reservation_date, $start_time, $end_time, $party_size, $restaurant_id, $floor_id=NULL, $table_id = NULL, $reservation_id=NULL, $source, $tat, $warning_message=NULL)
    {
        $start_date_time = getDateTime($reservation_date, $start_time);
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
        $end_date_time = getDateTime($reservation_date, $end_time);
        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
        //if($source == config('reservation.source.online')){
            //\Log::info('heree');
            $reserved_seat_info = $this->getTotalReservationCountInDateTimeRange($reservation_date, $slot_availability['slot_range'][$start_time], $restaurant_id, $reservation_id);
            $total_seat_reserved = $reserved_seat_info->reserved_seat;
            $total_table_reserved = $reserved_seat_info->reserved_table;
            $available_seat = $slot_availability['max_covers_limit'][$start_time] - $total_seat_reserved;
           // $available_tables = $slot_availability['table_count'][$start_time] - $total_table_reserved;

            $reserved_table_info = $this->getTotalReservationCountInGivenRange($start_date_time, $end_date_time, $restaurant_id, $reservation_id);
            $available_tables = $slot_availability['table_count'][$start_time] - $reserved_table_info->reserved_table;
            if ($available_seat < $party_size || $available_tables < 1) {
                if($source == config('reservation.source.online')) {
                    return ['success' => false, 'message' => 'There is no table available at the restaurant for the requested time. Please choose another time.'];
                }else{
                    $warning_message = "Maximum covers limit for this shift has been reached. However, you can still book for this timeslot.";
                }
            }
            $pacing_response = $this->checkPacing($restaurant_id, $reservation_id, $slot_availability, $reservation_date, $start_time, $end_time, $party_size, $start_date_time, $end_date_time);
            if(!$pacing_response['success']){
                if($source == config('reservation.source.online')) {
                    return ['success' => false, 'message' => $pacing_response['message']];
                }else{
                    $warning_message = ($warning_message)?$warning_message:'The pacing limit set for this time slot has been reached. However, you can still book for this timeslot.';
                }
            }
        //}
        $available_floor_table_info = $this->getAvailableFloorAndTables($restaurant_id, $source, $start_date_time, $end_date_time, $slot_availability, $start_time, $table_id, $floor_id, $party_size, $reservation_id, $reservation_date);
        //\Log::info($available_floor_table_info);exit;
        if(isset($available_floor_table_info) && count($available_floor_table_info)>0){
            return ['success' => true, 'message' => $warning_message, 'data'=>['floor_table_info' => $available_floor_table_info, 'slot_range' => $slot_availability['slot_range'][$start_time], 'tat' => $tat]];
        }
        return ['success' => false, 'message' => 'There is no table available at the restaurant for the requested time. Please choose another time.'];
    }

    public function getTotalReservationCountInDateTimeRange($reservation_date, $time_range, $restaurant_id, $reservation_id=NULL)
    {
        $time_range = explode('-', $time_range);
        $start_date_time = $reservation_date . ' ' . $time_range[0] . ':00';
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
        $end_date_time = $reservation_date . ' ' . $time_range[1] . ':00';
        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
        $reservation_count = Reservation::where('restaurant_id', $restaurant_id);

        if($reservation_id){
            $reservation_count = $reservation_count->where('id', '!=', $reservation_id);
        }
        $reservation_count = $reservation_count->whereHas('status', function ($query) {
                return $query->whereIn('slug', config('reservation.status_slug_check_for_book_reservation'));
            })->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time)
            ->select(DB::Raw('sum(reserved_seat) as reserved_seat'), DB::Raw('count(*) as reserved_table'))
            ->first();
        //\Log::info([$reservation_count, $start_date_time, $end_date_time, date_default_timezone_get()]);
        return $reservation_count;
    }

    public function getTotalReservationCountInGivenRange($start_date_time, $end_date_time, $restaurant_id, $reservation_id=NULL, $floor_id=NULL)
    {
        $reservation_count = Reservation::where('restaurant_id', $restaurant_id);
        if($reservation_id){
            $reservation_count = $reservation_count->where('id', '!=', $reservation_id);
        }
        if($floor_id){
            $reservation_count = $reservation_count->where('floor_id', $floor_id);
        }
        $reservation_count = $reservation_count->whereHas('status', function ($query) {
            return $query->whereIn('slug', config('reservation.status_slug_check_for_book_reservation'));
        })->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time)
            ->select(DB::Raw('sum(reserved_seat) as reserved_seat'), DB::Raw('count(*) as reserved_table'))
            ->first();
        return $reservation_count;
    }

    public function changeStatusReservation($id, Request $request)
    {
        DB::beginTransaction();
        $new_status = $request->input('new_status');
        $type = $request->input('type');
        $message = 'Reservation status has been changed successfully';
        $restaurant_id = \Auth::user()->restaurant_id;
        if($request->has('no_show')){
            $new_status = ReservationStatus::getNoShowStatusId();
            $message = 'Reservation marked as no show';
        }
        if($new_status){
            $reservation_status = ReservationStatus::find($new_status);
            if($reservation_status){
                try {
                    $reservationDetail = Reservation::with('table')->where('restaurant_id', \Auth::user()->restaurant_id)->where('id', $id)->first();
                    if (!$reservationDetail) {
                        return \Response::json([ 'errors'=>['Reservation could not be changed']], 422);
                    } elseif($new_status == ReservationStatus::getSeatedStatusId() || $new_status == ReservationStatus::getPartiallySeatedStatusId()) {
                        // if reserved/confirmed to seated/partially seated then check availability
                        $curResFloorId = $reservationDetail->floor_id ?? $request->input('floor_id');
                        $curResTableId = $reservationDetail->table_id ?? $request->input('table_id');
                        \Log::info($curResTableId);
                        $curStartTime  = Carbon::now();
                        $curEndTime    = '';
                        if (!$reservationDetail->start_time || !$reservationDetail->end_time) {

                        } else {
                            $curResStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $reservationDetail->start_time);
                            $curResEndTime   = Carbon::createFromFormat('Y-m-d H:i:s', $reservationDetail->end_time);
                            $curResTat       = $curResEndTime->diffInMinutes($curResStartTime);
                            $curEndTime      = $curStartTime->copy()->addMinutes($curResTat)->format('Y-m-d H:i:s');
                        }
                        $curStartTime = $curStartTime->format('Y-m-d H:i:s');
                        //return [$curStartTime,$curEndTime];
                        // status ids
                        $checkStatusIds = [ReservationStatus::getReservedStatusId(), ReservationStatus::getCancelledStatusId(), ReservationStatus::getNoShowStatusId(),ReservationStatus::getRunningLateStatusId(),ReservationStatus::getConfirmedStatusId(),ReservationStatus::getFinishedStatusId()];
                        $checkResv      = Reservation::where('id', '!=', $id)
                            ->where('floor_id', $curResFloorId);
                        if(strpos($curResTableId, ',')) {
                            $curResTableId = str_replace(',', '|',  $curResTableId);
                            $checkResv = $checkResv->whereRaw("CONCAT(',', table_id, ',') REGEXP ',(".$curResTableId.")'");
                        } else {
                            $checkResv = $checkResv->whereRaw("FIND_IN_SET('".$curResTableId."', table_id)");
                        }
                        $checkResv = $checkResv->whereNotIn('status_id', $checkStatusIds)
                            ->whereDate('start_time', Carbon::today()->toDateString())
                            ->where(function ($query) use ($curStartTime, $curEndTime) {
                                $query->where('start_time', '<=', $curStartTime);//->where('end_time', '>=', $curStartTime);
                                if ($curEndTime) {
                                    $query->orWhere('start_time', '<=', $curEndTime)->where('end_time', '>=', $curEndTime);
                                }
                            })->get();
                        if (count($checkResv)) {
                            return \Response::json(['errors' => ['Status could not be changed']], 422);
                        }
                    }
                    //for change reservation status from view details
                    // NTRS-1001
                    //if(empty($type)){
                        if ($reservationDetail->start_time > Carbon::now()){
                            $type = config('reservation.reservation_category.upcoming');
                        }elseif($reservationDetail->start_time <= Carbon::now() && $reservationDetail->end_time > Carbon::now()){
                            $type = config('reservation.reservation_category.current');
                        }

                    //}
                    if ($reservationDetail) {
                        $date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d');
                        $date_start_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($date." 00:00:01"));
                        if(($date_start_time->format('Y-m-d') == Carbon::today()->toDateString() || $reservationDetail->source == config('reservation.source.walk_in')) && (isset($type) && ($type==config('reservation.reservation_category.upcoming') || $type==config('reservation.reservation_category.waitlist'))) && ($reservation_status->slug==config('reservation.status_slug.seated') || $reservation_status->slug==config('reservation.status_slug.partially_seated'))){
                            $reservation_time_local = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'H:i');
                            $reservation_time = Carbon::now()->format('H:i:s');
                            $reservation_date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d');;
                            $floor_id = $reservationDetail->floor_id;
                            if(!$reservationDetail->floor_id){
                                $floor_id = $request->input('floor_id');
                            }
                            if(strpos($request->input('table_id'), ',')) {
                                $table_id = explode(',', $request->input('table_id'));
                            } else {
                                $table_id = $reservationDetail->table_id;
                            }
                            $response = $this->checkAvailability($reservation_date, $reservation_time_local, $reservationDetail->reserved_seat, $floor_id, $table_id, true, $reservationDetail->restaurant_id, $reservationDetail->id,$type);
                            if(!empty($response['errors'])){
                                //return $response;
                                return \Response::json($response, 422);
                            }
                            if($reservationDetail->source == config('reservation.source.walk_in')){
                                if((isset($response['data']['floor_table_info'][0]) || !empty($response['data']['floor_table_info'][0])) && (isset($response['data']['floor_table_info'][0]['tables']) || !empty($response['data']['floor_table_info'][0]['tables']))){
                                    if (is_array($table_id)) {   // multiple tables selected
                                        if ($table_id == array_column($response['data']['floor_table_info'][0]['tables'], 'id')) {
                                            $table_id = implode(',', $table_id);
                                        } else {
                                            return \Response::json(['errors' => ['Status could not be changed']], 422);
                                        }
                                    } else {
                                        $tables   = current($response['data']['floor_table_info'][0]['tables']);
                                        $table_id = $tables['id'];
                                    }
                                    //$table_id = $tables['id'];
                                    $tat = $response['data']['tat']; //$tables['table_type']['tat'];
                                    $reservationDetail->floor_id = $floor_id;
                                    $reservationDetail->table_id = $table_id;
                                }else{
                                    return \Response::json(['errors'=>['Status could not be changed']], 422);
                                }
                            }else{
                                $tat = $response['data']['tat'];
                            }
                            $end_time = Carbon::parse($reservation_time)->addMinutes($tat)->format('H:i:s');
                            //$end_time = addMinutesInTime($reservation_time, $tat);
/*                            $start_date_time = getDateTime($reservation_date, $reservation_time);
                            $end_date_time = getDateTime($reservation_date, $end_time);*/
                            $start_date_time = getDateTime($reservation_date, $reservation_time);
                            //$start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
                            $end_date_time = getDateTime($reservation_date, $end_time);
                            //$end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
                            $reservationDetail->start_time = $start_date_time;
                            $reservationDetail->end_time = $end_date_time;
                        }
                        // update seated_at when change status to seated or partially seated
                        if($new_status == ReservationStatus::getSeatedStatusId() || $new_status == ReservationStatus::getPartiallySeatedStatusId()) {
                            $reservationDetail->seated_at = Carbon::now()->format('Y-m-d H:i:s');;
                        }

                        if($request->has('cancellation_reason')){
                            $reservationDetail->cancellation_reason = $request->input('cancellation_reason');
                        }

                        if ($new_status == ReservationStatus::getFinishedStatusId()) {
                            // update end_time
                            $reservation_date            = Carbon::today()->toDateString();
                            $reservation_time            = Carbon::now()->format('H:i:s');
                            //do not change end time if past reservation and after seated status not finished
                            if($reservationDetail->end_time < Carbon::now()){

                            }else{
                                $reservationDetail->end_time = getDateTime($reservation_date, $reservation_time);
                            }
                        }

                        $reservationDetail->status_id = $new_status;
                        $reservationDetail->save();
                        if ($reservationDetail->status->slug == 'cancelled') {
                            $message = 'Reservation has been cancelled successfully';
                            //payment deduction for reservation cancellation
                            try {
                                $curResStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $reservationDetail->start_time);
                                $curTime   = Carbon::now()->format('Y-m-d H:i:s');
                                $remainingTime       = $curResStartTime->diffInHours($curTime);
                                if($remainingTime>24 && intval($reservationDetail->total_cancellation_charge)>0 && $reservationDetail->payment_status=='charged'){
                                    $apiObj = new ApiPayment();
                                    $stripeRefundData = [
                                        'charge' => $reservationDetail->stripe_charge_id,
                                        'amount'   => ($reservationDetail->total_charged_amount * 100),
                                        'reason' => 'requested_by_customer',
                                    ];
                                    $refundResponse = $apiObj->createRefund($stripeRefundData);
                                    if(!isset($refundResponse['errorMessage'])) {
                                        /*$apiObj = new ApiPayment();
                                        $responsePayment = $apiObj->releaseCharge($reservationDetail->stripe_charge_id);*/
                                        $reservation_update = Reservation::find($id);
                                        $reservation_update->total_refund_amount = $reservation_update->total_charged_amount;
                                        $reservation_update->payment_status = 'refunded';
                                        $reservation_update->save();

                                        $refund_data = array(
                                            'reservation_id' => $reservation_update->id,
                                            'refund_value' => $reservation_update->total_charged_amount,
                                            'amount_refunded' => $reservation_update->total_charged_amount,
                                            'charge_id' => $reservationDetail->stripe_charge_id,
                                            'refund_id' =>  $refundResponse->id,
                                            'reason' =>  $stripeRefundData['reason'],
                                            'host_name' =>  $request->getHost(),
                                            'created_at' => Carbon::now(),
                                            'updated_at' => Carbon::now(),
                                        );
                                        DB::table('refund_histories')->insert($refund_data);
                                        event(new TableReservationRefund($reservation_update, 'Customer'));
                                    }
                                }else{
                                    event(new TableReservationCancel($reservationDetail, 'Manager'));
                                }
                            } catch (\Exception $e) {
                                \Log::info($e);
                            }
                            //end code
                            /*try {
                                event(new TableReservationCancel($reservationDetail, 'Manager'));
                            }catch (\Exception $e) {
                                \Log::info($e);
                                //DB::rollback();
                                //return \Response::json(['errors' => $e->getMessage()], 500);
                            }*/
                        }

                        /* Refund transaction code */
                        if ($reservationDetail->status->slug == 'seated') {
                            if ($reservationDetail->payment_status=='charged' && (isset($reservationDetail->total_cancellation_charge) && !empty(intval($reservationDetail->total_cancellation_charge)))) {
                                //echo $reservation->total_cancellation_charge;
                                try{
                                    $apiObj = new ApiPayment();
                                    $stripeRefundData = [
                                        'charge' => $reservationDetail->stripe_charge_id,
                                        'amount'   => ($reservationDetail->total_charged_amount * 100),
                                        'reason' => 'requested_by_customer',
                                    ];
                                    $refundResponse = $apiObj->createRefund($stripeRefundData);
                                    \Log::info($refundResponse);
                                    if(!isset($refundResponse['errorMessage'])) {
                                        $reservation_update = Reservation::find($id);
                                        $reservation_update->total_refund_amount = $reservation_update->total_charged_amount;
                                        $reservation_update->payment_status = 'refunded';
                                        $reservation_update->save();

                                        $refund_data = array(
                                            'reservation_id' => $reservation_update->id,
                                            'refund_value' => $reservation_update->total_charged_amount,
                                            'amount_refunded' => $reservation_update->total_charged_amount,
                                            'charge_id' => $reservationDetail->stripe_charge_id,
                                            'refund_id' => $refundResponse->id,
                                            'reason' => $stripeRefundData['reason'],
                                            'host_name' => $request->getHost(),
                                            'created_at' => Carbon::now(),
                                            'updated_at' => Carbon::now(),
                                        );
                                        DB::table('refund_histories')->insert($refund_data);
                                        event(new TableReservationRefund($reservation_update, 'Customer'));
                                    }
                                    \Log::info('Refunded');
                                }catch (Exception $e){
                                    \Log::info($e);
                                }
                            }

                        }

                        /* End here */

                        if($reservationDetail->status->slug=='cancelled' || $reservationDetail->status->slug=='no_show'){
                            if($reservationDetail->user_id){
                                CommonFunctions::changeUserCategory($reservationDetail->user_id);
                            }
                        }
                    }
                    DB::commit();
                    return \Response::json(['message'=>$message], 200);
                }catch (\Exception $e) {
                    DB::rollback();
                    return \Response::json(['errors'=>['Status could not be changed', 'temp' => $e->getMessage()]], 422);
                }
            }
            return \Response::json([ 'errors' => 'Status not found'], 422);
        }
        return \Response::json([ 'errors' => 'Status not found'], 422);
    }

    /**
     * Exntend reservation till next slot
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function extendReservation($id)
    {
        $reservationDetail = Reservation::where('id', $id)->first();
        $restaurant_id = \Auth::user()->restaurant_id;
        if (!$reservationDetail) {
            return \Response::json(['errors' => ['Reservation could not be extended.']], 422);
        } else {
            $slotInterval       = config('reservation.slot_interval_time');
            $endTime            = $reservationDetail->end_time;
            $result['end_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $endTime)->format('Y-m-d H:i:s');
            $endTimeMin         = (integer)Carbon::createFromFormat('Y-m-d H:i:s', $endTime)->format('i');
            $endTimeMinReminder = $endTimeMin%$slotInterval;
            /*if ($endTimeMin < $slotInterval) {
                // make it next half-an-hour
                $addMinutes = $slotInterval - $endTimeMin;
            } else {
                // make it next hour
                $addMinutes = $slotInterval * 2 - $endTimeMin;
            }*/
            $addMinutes = $slotInterval - $endTimeMinReminder;
            //\Log::info($addMinutes);
            $reservationDetail->end_time = Carbon::createFromFormat('Y-m-d H:i:s', $endTime)->addMinutes($addMinutes)->format('Y-m-d H:i:s');
            $reservationDetail->save();
            $newEndTime = Carbon::createFromFormat('Y-m-d H:i:s', $reservationDetail->end_time)->format('H:i');
            $newEndTime = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $newEndTime)->format('h:i A');
            $message    = "Reservation has been extended till {$newEndTime}.";

            return response()->json(['message' => $message], 200);
        }
    }

    private function checkGlobalSettings($restaurant_id, $source, $reservation_date_time, $party_size)
    {
        $reservation_settings = Restaurant::where('id', $restaurant_id)->value('reservation_settings');
        if (empty($reservation_settings) || empty($reservation_settings['reservation'])) {
            return ['success' => true];
        }
        if ($source == config('reservation.source.online')) {
            //check online reservation accept or not
            if (!$reservation_settings['reservation']['accept_online_reservation']) {
                return ['success' => false, 'message' => 'Restaurant does not accept online reservation'];
            }
            //Min/Max setting for online reservation
            if ($party_size < $reservation_settings['reservation']['min_for_online_reservation'] || $party_size > $reservation_settings['reservation']['max_for_online_reservation']) {
                return ['success' => false, 'message' => 'Restaurant does not support this party size'];
            }
            //check online reservation cutoff and advance time
            if ($reservation_settings['reservation']['cuttoff_type'] || $reservation_settings['reservation']['allow_guest_advance_online_reservation_type']) {
                $advanceType = ($reservation_settings['reservation']['allow_guest_advance_online_reservation_type'])?$reservation_settings['reservation']['allow_guest_advance_online_reservation_type']:null;
                $advanceTime = ($reservation_settings['reservation']['allow_guest_advance_reservation_time'])?$reservation_settings['reservation']['allow_guest_advance_reservation_time']:null;
                $cutOffType = ($reservation_settings['reservation']['cuttoff_type'])?$reservation_settings['reservation']['cuttoff_type']:null;
                $cutOffTime = ($reservation_settings['reservation']['cuttoff_time'])?$reservation_settings['reservation']['cuttoff_time']:null;

                if(!empty($advanceType) && !empty($advanceTime)){
                    if($reservation_date_time > $this->addTimeInDateTime($advanceType, $advanceTime)){
                        return ['success' => false, 'message' => "Restaurant allow guests to make reservation in $advanceTime $advanceType advance"];
                    }
                }
                if(!empty($cutOffType) && !empty($cutOffTime)){
                    if($reservation_date_time < $this->addTimeInDateTime($cutOffType, $cutOffTime)){
                        return ['success' => false, 'message' => "Restaurant reservation cut-off time is $cutOffTime $cutOffType"];
                    }
                }
            }
        }
        return ['success' => true];
    }

    private function addTimeInDateTime($timeType, $timeTypeCount)
    {
        $restaurant_id = Auth::user()->restaurant_id;
        if($timeType == config('reservation.cut_off_time_type')[0]){
            return CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id)->addHours($timeTypeCount);
        }elseif ($timeType == config('reservation.cut_off_time_type')[1]){
            return CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id)->addDays($timeTypeCount);
        }elseif ($timeType == config('reservation.allow_guest_advance_online_reservation_type')[1]){
            return CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id)->addMonths($timeTypeCount);
        }
    }

    private function getAvailableFloorAndTables($restaurant_id, $source, $start_date_time, $end_date_time, $slot_availability, $start_time, $table_id, $floor_id, $party_size, $reservation_id, $reservation_date)
    {
        //\Log::info($slot_availability);
        $available_floor_table_info = [];
        $reservation_detail_arr = $this->getReservationCountBetweenDates($start_date_time, $end_date_time, $restaurant_id, $table_id, $reservation_id);
        $table_type_reservation_count = $this->getReservationCountTableTypeWise($restaurant_id, $reservation_date, $slot_availability['slot_range'][$start_time], $reservation_id);
        $floor_table_info = Floor::select('id', 'name')->where('restaurant_id', $restaurant_id);
        if($floor_id){
            $floor_table_info = $floor_table_info->where('id', $floor_id);
        }else{
            if($source == config('reservation.source.online')) {
                $floor_table_info = $floor_table_info->whereIn('id', $slot_availability['floors'][$start_time]);
            }
        }
        if (is_array($table_id)) {
            $floor_table_info = $floor_table_info->whereHas('tables', function($query) use($table_id){
                $query->whereIn('id', $table_id);
            });
        } elseif ($table_id) {
            $floor_table_info = $floor_table_info->whereHas('tables', function($query) use($table_id){
                $query->where('id', $table_id);
            });
        }
        $floor_table_info = $floor_table_info->where('completed_step', 2)->with(['tables' => function($query)use($slot_availability, $start_time, $table_id) {
            if(is_array($table_id)) {
                $query=$query->whereIn('id', $table_id);
            } elseif ($table_id){
                $query=$query->where('id', $table_id);
            }
            $query->select('id', 'floor_id', 'name', 'min', 'max');
        }]);
        
        $floor_table_info = $floor_table_info->get()->toArray();
        $blocked_table = app('Modules\Reservation\Http\Controllers\BlockTableController')->getBlockedTableArr($restaurant_id, $floor_id=null, $table_id=null, $start_date_time, $end_date_time);
        foreach ($floor_table_info as $floor_key=>$floor) {
            foreach ($floor['tables'] as $table_key=>$table) {
                //check table is currently occupied or not
                if($source==config('reservation.source.walk_in')){
                    if($reservation_date==Carbon::today()->toDateString()){
                        $current_reservation = app('Modules\Reservation\Http\Controllers\FloorController')->getTableReservations($table['id'], 'current', $reservation_date);
                        \Log::info($current_reservation);
                        if($current_reservation && count($current_reservation)>0){
                            continue;
                        }
                    }
                }
                //code end
                $table_type = $table['max'];
                $table_type_arr = array();
                foreach ($slot_availability['table'][$start_time] as $slot_key=>$slot_arr){
                    if($slot_availability['table'][$start_time][$slot_key]['table_type']==$table_type){
                        $table_type_arr = $slot_availability['table'][$start_time][$slot_key];
                    }
                }
                $available_table_seat = isset($table_type_arr['table_cover_count'])?$table_type_arr['table_cover_count']:0;
                $available_tables = isset($table_type_arr['table_count'])?$table_type_arr['table_count']:0;
                $available_table = true;
                if (!empty($table_type_reservation_count) && isset($table_type_reservation_count[$table['max']])) {
                    $table_reserved_seat = (isset($table_type_reservation_count[$table['max']]) && !empty($table_type_reservation_count[$table['max']])) ? $table_type_reservation_count[$table['max']]['reserved_seat'] : 0;
                    $table_reserved = (isset($table_type_reservation_count[$table['max']]) && !empty($table_type_reservation_count[$table['max']])) ? $table_type_reservation_count[$table['max']]['table_count'] : 0;

                    $available_table_seat = $available_table_seat - $table_reserved_seat;
                    $available_tables = $available_tables-$table_reserved;
                    if($available_tables <= 0){
                        $available_table = false;
                    }
                }
                if($source == config('reservation.source.online')) {
                    //\Log::info([$table, $available_table_seat, $party_size, $table['max'], $available_table]);
                    if ($available_table_seat >= $party_size && $table['max'] >= $party_size && $available_table) {
                        $available_floor_table_info[$floor_key]['id'] = $floor['id'];
                        $available_floor_table_info[$floor_key]['name'] = $floor['name'];
                        $available_floor_table_info[$floor_key]['tables'][$table_key] = $table;
                    }
                    //\Log::info($available_floor_table_info);
                }else{
                    if(count($blocked_table)==0  || ($blocked_table && !in_array($table['id'], $blocked_table))) {
                        if ((count($reservation_detail_arr) == 0 || !isset($reservation_detail_arr[$table['id']])) || (($reservation_detail_arr && isset($reservation_detail_arr[$table['id']])) && ($reservation_detail_arr[$table['id']]['table_count'] <= 0))) {
                            $available_floor_table_info[$floor_key]['id'] = $floor['id'];
                            $available_floor_table_info[$floor_key]['name'] = $floor['name'];
                            $available_floor_table_info[$floor_key]['tables'][$table_key] = $table;
                        }
                    }
                }
            }
        }
        return $available_floor_table_info;
    }

    private function getReservationCountTableTypeWise($restaurant_id, $reservation_date, $time_range, $reservation_id=null, $table_id=null)
    {
        $reservationArr = array();
        $time_range = explode('-', $time_range);

        $start_date_time = $reservation_date . ' ' . $time_range[0] . ':00';
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
        $end_date_time = $reservation_date . ' ' . $time_range[1] . ':00';
        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
        $reservations = Reservation::where('restaurant_id', $restaurant_id)->with('table');
        if($reservation_id){
            $reservations = $reservations->where('id', '!=', $reservation_id);
        }
        $reservations = $reservations->whereHas('status', function($query) {
            $query->whereIn('slug', config('reservation.status_slug_check_for_book_reservation'));
        })->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time)
            ->select(DB::Raw('sum(reserved_seat) as reserved_seat'), DB::Raw('count(*) as reserved_table'));
        if ($table_id) {
            $reservations = $reservations->where('table_id', $table_id);
        }
        $reservations = $reservations->get();
        if(isset($reservations->table) && !empty($reservations->table)){
            foreach ($reservations as $key=>$reservation){
                $reservationArr[$reservations->table->max]['table_id'] = $reservation->table->id;
                $reservationArr[$reservations->table->max]['reserved_seat'] = $reservation->reserved_seat;
                $reservationArr[$reservations->table->max]['table_count'] = $reservation->reserved_table;
                $reservationArr[$reservations->table->max]['table_type'] = $reservations->table->max;
            }
        }
        return $reservationArr;
    }

    private function checkPacing($restaurant_id, $reservation_id=null, $slot_availability, $reservation_date, $start_time, $end_time, $party_size, $start_date_time, $end_date_time)
    {
        $reserved_seat_info = $this->getTotalReservationCountInGivenRange($start_date_time, $end_date_time, $restaurant_id, $reservation_id);
        $total_seat_reserved = $reserved_seat_info->reserved_seat;
        $total_table_reserved = $reserved_seat_info->reserved_table;
        $available_seat = $slot_availability['pacing'][$start_time]['covers'] - $total_seat_reserved;
        $available_tables = $slot_availability['pacing'][$start_time]['reservations'] - $total_table_reserved;
        if ($available_seat < $party_size || $available_tables < 1) {
            return ['success' => false, 'message' => 'The pacing limit set for this time slot has been reached.'];
        }
        return ['success' => true, 'message' => 'Seats are available'];
    }

    public function checkCancellationChargesInfo($id)
    {
        try{
            $reservation_details = Reservation::find($id);
            $data = "";
            if($reservation_details){
                $curResStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $reservation_details->start_time);
                $curTime   = Carbon::now()->format('Y-m-d H:i:s');
                $remainingTime       = $curResStartTime->diffInHours($curTime);
                if($remainingTime<24 && $reservation_details->payment_status==config('reservation.payment_status.charged') && intval($reservation_details->total_cancellation_charge)>0){
                    $data = ['cancellation_charge' => $reservation_details->cancellation_charge, 'cancellation_charge_type'=>$reservation_details->cancellation_charge_type, 'total_cancellation_charge'=>$reservation_details->total_cancellation_charge];
                }
                return response()->json(['success'=>true, 'message' => 'Request processed succesfully', 'data'=>$data], 200);
            }
        }catch (\Exception $e){
            \Log::info($e);
            return \Response::json(['success'=>false, 'errors' => [$e]], 500);
        }
    }

    public function releaseReservationAmountCron()
    {
        $previous_date = Carbon::now()->subHours(24)->format('Y-m-d');
        //echo $previous_date;
        $checkStatusIds = [ReservationStatus::getReservedStatusId(), ReservationStatus::getCancelledStatusId(), ReservationStatus::getNoShowStatusId(),ReservationStatus::getRunningLateStatusId(),ReservationStatus::getConfirmedStatusId()];
        $all_reservation = Reservation::whereDate('end_time', $previous_date)->whereNotIn('status_id', $checkStatusIds)->whereNotNull('stripe_charge_id')->where('stripe_charge_id', '!=', '')->where('payment_status', config('reservation.payment_status.charged'))->where('release_amount_status_cron', 0)->get();
        //\Log::info($all_reservation);
        //print_r($all_reservation);exit;
        foreach ($all_reservation as $reservation) {
            if (isset($reservation->total_cancellation_charge) && !empty(intval($reservation->total_cancellation_charge))) {
                try{
                    $apiObj = new ApiPayment();
                    $stripeRefundData = [
                        'charge' => $reservation->stripe_charge_id,
                        'amount'   => ($reservation->total_charged_amount * 100),
                        'reason' => 'requested_by_customer',
                    ];
                    $refundResponse = $apiObj->createRefund($stripeRefundData);
                    if(!isset($refundResponse['errorMessage'])) {
                        $reservation->total_refund_amount = $reservation->total_charged_amount;
                        $reservation->payment_status = 'refunded';
                        $reservation->release_amount_status_cron = 1;
                        $reservation->save();

                        $refund_data = array(
                            'reservation_id' => $reservation->id,
                            'refund_value' => $reservation->total_charged_amount,
                            'amount_refunded' => $reservation->total_charged_amount,
                            'charge_id' => $reservation->stripe_charge_id,
                            'refund_id' => $refundResponse->id,
                            'reason' => $stripeRefundData['reason'],
                            'host_name' => $_SERVER['HTTP_HOST'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        );
                        DB::table('refund_histories')->insert($refund_data);
                        event(new TableReservationRefund($reservation, 'Customer'));
                    }
                }catch (Exception $e){
                    \Log::info($e);
                }
            }
        }
    }

    private function getCancellationChargeInfo($restaurant_id)
    {
        $paymentInfoArr = Restaurant::where('id', $restaurant_id)->select(DB::raw('REPLACE(json_extract(reservation_settings, "$.reservation.cancellation_charge"), \'"\', \'\')  as charge, REPLACE(json_extract(reservation_settings, "$.reservation.cancellation_charge_type"), \'"\', \'\') as charge_type'))->first()->toArray();
        if (empty($paymentInfoArr) || empty($paymentInfoArr['charge']) || empty($paymentInfoArr['charge_type'])) {
            return ['success' => false, 'error' => 'No setting found'];
        }
        return ['success' => true, 'data' => $paymentInfoArr];
    }

    public function paymentLinkExpireCron()
    {
        $cancelled_status_id = ReservationStatus::getCancelledStatusId();
        $all_reservation = Reservation::where('source', config('reservation.source.offline'))->where('total_cancellation_charge', '>', 0)->where('status_id','!=',$cancelled_status_id)->whereNotNull('payment_link_token')->where('payment_status', config('reservation.payment_status.pending'))->get();
        //\Log::info($all_reservation);
        foreach ($all_reservation as $reservation) {
            try {
                $curResTat = Carbon::now()->diffInMinutes($reservation->updated_at);
                if ($curResTat > 15) {
                    $reservation->payment_link_token = NULL;
                    $reservation->payment_status = config('reservation.payment_status.failed');
                    $reservation->status_id = $cancelled_status_id;
                    $reservation->save();
                    if ($reservation->user_id) {
                        CommonFunctions::changeUserCategory($reservation->user_id);
                    }
                    event(new TableReservationCancel($reservation, 'Manager'));
                }
            } catch (Exception $e) {
                \Log::info($e);
            }
        }
    }

    public function getCurrentSlotRange($restaurant_id, $date)
    {
        $scheduled_hours = $this->getWorkingHours($date, $restaurant_id);
        $time_range = '';
        if ($scheduled_hours && !$scheduled_hours->is_dayoff && $scheduled_hours->slot_detail) {
            $slot_detail_arr = json_decode($scheduled_hours->slot_detail, true);
            $current_time = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'H:i');
            foreach ($slot_detail_arr as $key => $slot_detail) {
                \Log::info($current_time . "****" . $slot_detail['close_time']);
                if ((strtotime($current_time) < strtotime($slot_detail['close_time'])) && (strtotime($current_time) > strtotime($slot_detail['open_time']))) {
                    $time_range = $slot_detail['open_time'] . '-' . $slot_detail['close_time'];
                    if (app('Modules\Reservation\Http\Controllers\WorkingHoursController')->getCurrentSlotRange($current_time, $time_range)) {
                        return $time_range;
                    }
                }
            }
        }
        return $time_range;
    }

    public function export(Request $request)
    {
        $restArray = CommonFunctions::getRestaurantDetails(array('r2.id', 'r2.restaurant_name'));
        $start_date = $request->get('from_date');
        $end_date = $request->get('to_date');
        $restaurant_id = \Auth::user()->restaurant_id;
        $start_date = Carbon::createFromFormat('m-d-Y', $start_date)->format('Y-m-d');
        $start_date = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($start_date." 00:00:00"));
        $end_date = Carbon::createFromFormat('m-d-Y', $end_date)->format('Y-m-d');
        $end_date = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, Carbon::parse($end_date." 24:00:00"));
        $reservationData = Reservation::select('id','receipt_no','source','fname','email','lname','phone','created_at', 'start_time', 'reserved_seat', 'payment_status', 'payment_gatway', 'total_charged_amount', 'total_refund_amount', 'stripe_charge_id','restaurant_name','is_guest', 'restaurant_id', 'parent_restaurant_id', 'status_id')
            ->with([
                'restaurant' => function ($query) {
                    $query->with([
                        'city' => function ($query1) {
                            $query1->select('id', 'city_name');
                        }])->select('id','restaurant_name', 'city_id');
            } , 'user' => function ($query) {
                $query->select('id', 'fname', 'lname', 'email','mobile');
            }, 'status' => function ($query) {
                    $query->select('id', 'name');
                }
            ])
            ->where('created_at', '>=', $start_date)
            ->where('created_at', '<=', $end_date)
            ->whereIn('restaurant_id', $restArray)
            ->orderBy('id', 'DESC')
            ->get();
        //return $reservationData;
        $filename = 'Reservation_Report_'.time().'.csv';
        header('Content-Type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment;filename="'.$filename.'";');
        $file = fopen('php://output', 'w');

        $columns =    array(
            "Transaction #",
            "Payment Receipt ID",
            "Reservation Type",
            "Customer Name",
            "Phone",
            "Created Date",
            "Reservation Date",
            "Reservation Time",
            "Party Size",
            "Restaurant Reserved For",
            "Location Reserved For",
            "Reservation Status",
            "Payment Status",
            "Total $ Amount of Transaction AFTER Discount",
            "Coupon or Discount Amount ($)",
            "deal_discount",
            "Tax $ Amount",
            "Refunded Amount",
            "Amount Final",
            "STRIPE Charge Id",
            "Payment Gateway",
            "City"
        );
        fputcsv($file, $columns);
        $parent_restaurant_name = '';
        foreach($reservationData as $reservation) {
            if(empty($parent_restaurant_name)){
                $parent_restaurant_name = Restaurant::where('id', $reservation->parent_restaurant_id)->value('restaurant_name');
            }
            $city = $reservation->restaurant->city->city_name;
            /*if($reservation->is_guest) {
                $name = $reservation->fname.' '.$reservation->lname;
                $email =  $reservation->email;
                $phone = $reservation->phone;
            }else {
                if(isset($reservation->user->fname)) {
                    $name = $reservation->user->fname.' '.$reservation->user->lname;
                    $email =  $reservation->user->email;
                    $phone = $reservation->user->mobile;
                } else {
                    $name = $reservation->fname.' '.$reservation->lname;
                    $email =  $reservation->email;
                    $phone = $reservation->phone;
                }
            }*/
            $final_amount = round(($reservation->total_charged_amount - $reservation->total_refund_amount), 2);
            $payment_status = isset($reservation->payment_status) && !empty($reservation->payment_status)? ucfirst($reservation->payment_status==config('reservation.payment_status.charged')?'hold':$reservation->payment_status) : 'N/A';
            $reservation_created_date = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->created_at)->format('m-d-Y h:i A');
            $reservation_start_time = CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->start_time);
            if(Auth::user()) {
                $curSymbol = Auth::user()->restaurant->currency_symbol;
            } else {
                $curSymbol = config('constants.currency');
            }

            $data = array(
                $reservation->id,
                $reservation->receipt_no,
                $reservation->source,
                ucwords($reservation->fname.' '.$reservation->lname),
                $reservation->phone,
                $reservation_created_date,
                $reservation_start_time->format('m-d-Y'),
                $reservation_start_time->format('h:i A'),
                $reservation->reserved_seat,
                $parent_restaurant_name,
                $reservation->restaurant->restaurant_name,
                $reservation->status->name,
                $payment_status,
                $curSymbol.$reservation->total_charged_amount,
                $curSymbol.'0',
                $curSymbol.'0',
                $curSymbol.'0',
                $curSymbol.$reservation->total_refund_amount,
                $curSymbol.$final_amount,
                $reservation->stripe_charge_id,
                'Stripe',
                ucfirst($city),
            );
            fputcsv($file, $data);
        }
        fclose($file);
        exit;
    }

    public function getTAT($tat, $slots_data, $restaurant_id, $party_size, $start_time, $walkInFlag)
    {
        $table_types = app('Modules\Reservation\Http\Controllers\TurnOverTimeController')->getAllTableTypeInfo($restaurant_id);
        $table_type_id = app('Modules\Reservation\Http\Controllers\TurnOverTimeController')->getTableTypeIdByPartySize($restaurant_id, $party_size);
        if(!$table_type_id) {
            return ['success' => false, 'message' => 'No turnover time setting found for this party size.'];
        }
        if($walkInFlag && $tat==0){
            $slot_range = $slots_data['slots_availability']['slot_range'];
            foreach ($slot_range as $slot){
                if(isInputTimeExistInRange($slot, $start_time)){
                    $input_time_slot = explode('-', $slot);
                    $tat = $slots_data['slots_availability']['party_size'][$input_time_slot[0]][$table_type_id];
                    break;
                }
            }
        }

        $tat = isset($slots_data['slots_availability']['party_size'][$start_time][$table_type_id]) && !empty($slots_data['slots_availability']['party_size'][$start_time][$table_type_id])?$slots_data['slots_availability']['party_size'][$start_time][$table_type_id]:(!empty($tat)?$tat:$table_types[$table_type_id]['tat']);
        // As discussed with Pravish if tat is not defined then consider it as defalut slot interval, do not show validation message
        if(empty($tat)) {
            $tat = config('reservation.slot_interval_time');
        }
        return $tat;
    }
}
