<?php
namespace Modules\Reservation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservationSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accept_online_reservation' => 'required',
            'min_for_online_reservation' => 'required',
            'max_for_online_reservation' => 'required',
            //'allow_guest_advance_reservation_time' => 'required',
            'allow_guest_advance_online_reservation_type' => 'required',
            //'cuttoff_time' => 'required',
            'cuttoff_type' => 'required',
            'require_hostname_accepting' => 'required',
            'enable_online_waitlist' => 'required',
           
        ];
    }

    
}