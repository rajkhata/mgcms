<?php
namespace Modules\Reservation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TableTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'min' => 'required',
            'max' => 'required',
            'tat' => 'required',
           
        ];
    }

    public function messages()
    {
        return [
            
            'name.required' => 'Table type name should not be empty',
            'min.required' => 'Table type minimum party size should not be empty',
            'max.required' => 'Table type maximum party size should not be empty',
            'tat.required' => 'Table type turn around time should not be empty',
        ];
    }
}