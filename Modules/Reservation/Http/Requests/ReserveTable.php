<?php
namespace Modules\Reservation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReserveTable extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'party_size' => 'required',
            'reservation_date' => 'required',
            'reservation_time' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric|digits:10'
        ];
    }

    public function messages()
    {
        return [
            'party_size.required' => 'Party size should not be empty',
            'reservation_date.required' => 'Reservation date should not be empty',
            'reservation_time.required' => 'Reservation time should not be empty',
            'fname.required' => 'First name should not be empty',
            'lname.required' => 'Last name should not be empty',
            'email.required' => 'Email should not be empty',
            'email.email' => 'Email should be valid',
            'phone.required' => 'Phone number should not be empty',
            'phone.numeric' => 'Phone number should be numeric',
            'phone.digits' => 'Phone number should be 10 digit only',
        ];
    }
}