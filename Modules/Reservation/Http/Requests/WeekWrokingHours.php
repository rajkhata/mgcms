<?php
namespace Modules\Reservation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WeekWorkingHours extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slot_name' => 'required|max:20',
            //'available_cover_count' => 'required|digits_between:1,8',
            //'tableTypeCount.*'      => 'required|digits_between:1,8'
            'available_cover_count' => 'nullable|digits_between:1,8',
            'tableTypeCount.*'      => 'nullable|digits_between:1,8'
        ];
    }

    public function messages()
    {
        return [
            'slot_name.required' => 'Slot name should not be empty',
            'slot_name.max' => 'Slot name length cannot exceed 20 character',
            //'available_cover_count.required' => 'Cover available for online reservation should not be empty',
            'available_cover_count.*.digits_between'     => 'Cover available for online reservation cannot exceed 3 digits',
            //'tableTypeCount.*.required'     => 'Number of table should not be empty',
            'tableTypeCount.*.digits_between'     => 'Number of table cannot exceed 2 digits',
        ];
    }
}