<?php
namespace Modules\Reservation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckValidity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'party_size' => 'required',
            'reservation_date' => 'sometimes',
            'reservation_time' => 'sometimes',
            //'fname' => 'required',
            //'lname' => 'required',
            //'phone' => 'required',
            //'email' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            //'fname.required' => 'First name should not be empty',
            //'lname.required' => 'Last name should not be empty',
            //'phone.required' => 'Phone should not be empty',
            //'email.required' => 'Email should not be empty',
            //'email.email' => 'Email should be valid',
            'party_size.required' => 'Party size should not be empty',
            'reservation_date.required' => 'Reservation date should not be empty',
            'reservation_time.required' => 'Reservation time should not be empty',
        ];
    }
}