<?php
namespace Modules\Reservation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['fname'] = 'required';
        $rules['lname'] = 'required';
        $rules['color'] = 'required';
        if ($this->method() == 'PUT')
        {
            $rules['mobile'] = 'required|unique:cms_restaurant_resv_servers,mobile,' . $this->get('server_id');
        }
        else
        {
            $rules['mobile'] = 'required|unique:cms_restaurant_resv_servers,mobile,NULL,id,deleted_at,id';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'fname.required' => "Please enter the server's first name.",
            'lname.required' => "Please enter the server's last name.",
            'mobile.required' => "Please enter the server's mobile number.",
            'color.required' =>  "Please choose the server's color.",
        ];
    }
}