<?php
namespace Modules\Reservation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'guest_email_on_reservation' => 'required',
            'guest_sms_on_reservation' => 'required',
            'guest_reservation_reminder_via_sms' => 'required',
            'restaurant_receive_notification_incoming' => 'required',
            'restaurant_receive_notification_modified' => 'required',
            'restaurant_receive_notification_cancelled' => 'required',
            'restaurant_receive_notification_emails' => 'required',
           
           
        ];
    }

    
}





