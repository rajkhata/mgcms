<?php
use App\Http\Middleware\CmsUserActionLogs;
Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'reservation', 'namespace' => 'Modules\Reservation\Http\Controllers'], function()
{
    Route::get('/', 'ReservationController@index');
    Route::post('update', 'ReservationController@update');
    Route::get('listing', 'ReservationController@getReservations');
    Route::get('archive', 'ReservationController@getArchive');
    Route::get('all/{id?}', 'ReservationController@getAll');
    Route::get('upcoming', 'ReservationController@getUpcoming');
    Route::get('detail/{id}', 'ReservationController@getReservationDetails');
    Route::post('cancel/{id}', 'ReservationController@cancelReservation');
    Route::post('change-status/{id}', 'ReservationController@changeStatusReservation');
    Route::post('extend/{id}', 'ReservationController@extendReservation');
    Route::post('cancellation/charge/{id}', 'ReservationController@checkCancellationChargesInfo');
    //Route::get('release', 'ReservationController@releaseReservationAmountCron');
    //Route::get('expire_link', 'ReservationController@paymentLinkExpireCron');
    Route::get('export', 'ReservationController@export');

});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'configure', 'namespace' => 'Modules\Reservation\Http\Controllers'], function()
{
    //Configure hours week/custom routes
    Route::resource('working-hours', 'WorkingHoursController');
    Route::get('working-hours-schedule', 'WorkingHoursController@getWorkingHoursSchedule');
    Route::get('custom-working-hours-schedule', 'WorkingHoursController@getCustomWorkingHoursSchedule');
    Route::post('working-hours-dayoff', 'WorkingHoursController@dayOffToggle');
    Route::get('slot-detail/{id}', 'WorkingHoursController@getSlotDetail');
    Route::post('slot/{type}', 'WorkingHoursController@workingSlot');
    Route::get('working-hours-check-reservation', 'WorkingHoursController@checkForCurrentOrUpcomingReservation');

    //configure turnover time routes
    Route::post('turnovertime/add', 'TurnOverTimeController@add');
    Route::post('turnovertime/update/{id}', 'TurnOverTimeController@update');

    Route::resource('turnovertime', 'TurnOverTimeController');
    Route::resource('global-settings', 'GlobalSettingsController');
    Route::resource('shift-overview', 'ShiftOverviewController');


    //Route::resource('guest-tags', 'GuestTagsController');

    // Route::get('guest-tags', 'TagsController@index');
    //Route::post('guest-tags', 'TagsController@store');

     Route::get('tags', 'TagsController@index');
    Route::post('tags', 'TagsController@store');
    Route::get('custom-tag/{tag}', 'TagsController@customtags');
    Route::post('custom-tag/update', 'TagsController@updatecustomtags');

    Route::delete('custom-tag/delete/{id}', 'TagsController@deletecustomtag');

    Route::post('notification-settings', 'GlobalSettingsController@notificationsettings');
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'guestbook', 'namespace' => 'Modules\Reservation\Http\Controllers'], function()
{
    Route::get('/', 'GuestBookController@index')->name('guest-list')->middleware(CmsUserActionLogs::class);;
    Route::post('/', 'GuestBookController@index')->name('guest-list')->middleware(CmsUserActionLogs::class);;
    Route::get('detail/{id}', 'GuestBookController@getGuestDetail')->name('guest-details')->middleware(CmsUserActionLogs::class);;
    Route::post('update-tags', 'GuestBookController@updateTags')->name('tag-update')->middleware(CmsUserActionLogs::class);;
    Route::post('update-items', 'GuestBookController@updateItems')->name('gustbook-updateitem')->middleware(CmsUserActionLogs::class);;
    Route::post('update-details', 'GuestBookController@updateGuestDetail')->name('guestbook-update-details')->middleware(CmsUserActionLogs::class);;
    Route::delete('delete-tag/{id}', 'GuestBookController@destroy')->name('guestbook-delete')->middleware(CmsUserActionLogs::class);;
    Route::delete('delete-item/{id}', 'GuestBookController@deleteItem')->name('guestbook-delete-item')->middleware(CmsUserActionLogs::class);;
    Route::get('guestdetail/{id}', 'GuestBookController@getGuestDetails')->name('guestbook-details')->middleware(CmsUserActionLogs::class);;

    Route::get('search', 'GuestBookController@guestSearch')->middleware(CmsUserActionLogs::class);;
    // ajax route for guestbook year based restaurant & orders listing
    Route::get('user/{id}/listing/{year}', 'GuestBookController@reservationOrderListing')->middleware(CmsUserActionLogs::class);;
    #RG 09-10-2018 export Guest users
    Route::get('export', 'GuestBookController@export')->middleware(CmsUserActionLogs::class);;
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'floor', 'namespace' => 'Modules\Reservation\Http\Controllers'], function()
{
    Route::get('/', 'FloorController@index');
    Route::get('create/{type}', 'FloorController@create');
    Route::post('create/{type}', 'FloorController@store');

    Route::get('configure/{floor_id}', 'FloorController@configure')->name('configure_floor');
    Route::post('configure/{floor_id}', 'FloorController@saveFloorConfig')->name('save_configure_floor');

    Route::get('welcome', 'FloorController@welcome')->name('welcomefloor');
    Route::get('{id}/tables', 'FloorController@getTables');
    //Route::get('{$id}/tables', 'FloorController@getTables');
    Route::delete('delete/{floor_id}', 'FloorController@destroy');
    Route::get('duplicate/{floor_id}', 'FloorController@duplicate');
    Route::get('{id}', 'FloorController@getFloorView');
    Route::get('{id}/reservations', 'FloorController@getFloorReservations');
    Route::post('{id}', 'FloorController@listFloorReservations');

    Route::delete('table/delete/{id}', 'FloorController@tableDelete');
    Route::post('table/update/{id}', 'FloorController@tableUpdate');

    Route::get('reservation/{table_id}', 'FloorController@getReservationTableInfo');
    Route::post('{id}/combine-table', 'TableCombineController@combineTables');
    Route::delete('combine-table/{id}', 'TableCombineController@deleteTableCombination');
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'table-management', 'namespace' => 'Modules\Reservation\Http\Controllers'], function()
{
    Route::get('floor-view/{id}', 'TableManagementController@getFloorView');

    Route::get('{id}', 'FloorController@getFloorView');
    Route::get('{id}/reservations', 'FloorController@getFloorReservations');
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'server', 'namespace' => 'Modules\Reservation\Http\Controllers'], function()
{
    Route::get('listing', 'TableServerController@index');
    Route::get('assign/{id}', 'TableServerController@create');
    Route::post('add', 'ServerController@store');
    Route::get('{id}', 'ServerController@show');
    Route::put('update/{id}', 'ServerController@update');
    Route::delete('delete/{id}', 'ServerController@destroy');
    Route::get('slot-name-by-date/{date}', 'TableServerController@getSlotNameArray');
    Route::post('table-assign', 'TableServerController@store');
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'reservation', 'namespace' => 'Modules\Reservation\Http\Controllers'], function() {
    Route::post('check-availability', '\Modules\Reservation\Http\Controllers\ReservationController@checkAvailabilityAndGetAvailableSlots');
    Route::post('book-table', '\Modules\Reservation\Http\Controllers\ReservationController@reserveTable');
    Route::post('wait-list-add', '\Modules\Reservation\Http\Controllers\ReservationController@waitListAdd');
    Route::post('online-wait-list-add', '\Modules\Reservation\Http\Controllers\ReservationController@onlineWaitListAdd');
    Route::put('wait-list-update/{id}', '\Modules\Reservation\Http\Controllers\ReservationController@waitListUpdate');
    Route::post('wait-list-sms', '\Modules\Reservation\Http\Controllers\ReservationController@waitListSmsNotification');
    // temp route for yield management, will be removed later
    Route::get('yield', '\Modules\Reservation\Http\Controllers\YieldController@index');
    Route::get('yield/{id}', '\Modules\Reservation\Http\Controllers\YieldController@show');
});

Route::group(['middleware' => ['web', 'auth']], function()
{
    Route::get('offers',function(){
      return view('reservation::offers.index');
    });
    Route::get('reviews-feedback',function(){
      return view('reservation::reviews-feedback.index');
    });
    Route::get('testnotification','\Modules\Reservation\Http\Controllers\ReservationController@test');
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'tags', 'namespace' => 'Modules\Reservation\Http\Controllers'], function()
{
    Route::get('/', 'TagsController@index');
    Route::post('/', 'TagsController@store');

});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'block-table', 'namespace' => 'Modules\Reservation\Http\Controllers'], function()
{
    Route::post('check-availability', 'BlockTableController@checkAvailabilityForTableBlock');
    Route::post('save', 'BlockTableController@blockTable');
    Route::post('resume-booking', 'BlockTableController@resumeBooking');
});
