@extends('layouts.app')
@section('content')
    {{--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet'/>
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print'/>
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet'/>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <script>
        new Chartist.Line('.ct-chart', {
            labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
            series: [[0, 5, 2, 4, 2, 0]]
        }, {
            height: 300
        });
    </script>

    <style>

        .ct-series-a .ct-area, .ct-series-a .ct-slice-donut-solid, .ct-series-a .ct-slice-pie {
            fill: url(#MyGradient)
        }

        .ct-grid {
            stroke: unset;
        }

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script>
        var bar_ctx = document.getElementById('myChart').getContext('2d');

        gradient = bar_ctx.createLinearGradient(0, 0, 0, 450);
        gradient.addColorStop(0, 'rgba(255, 0,0, 0.5)');
        gradient.addColorStop(0.5, 'rgba(255, 0, 0, 0.25)');
        gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');


        var data = {
            labels: ["02:00", "04:00", "06:00", "08:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00", "22:00", "00:00"],
            datasets: [{
                fillColor: gradient,
                backgroundColor: gradient,
                pointBackgroundColor: 'orange',
                borderWidth: 1,
                borderColor: '#911215',
                strokeColor: "#ff6c23",
                pointColor: "#fff",
                pointStrokeColor: "#ff6c23",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "#ff6c23",
                data: [0, 50, 100, 150, 81, 70, 60, 150, 54, 50, 0]
            }]
        };
        var options = {
            responsive: true,
            datasetStrokeWidth: 3,
            pointDotStrokeWidth: 4,
            maintainAspectRatio: true,
            animation: {
                easing: 'easeInOutQuad',
                duration: 520
            }, scales: {
                xAxes: [{
                    gridLines: {
                        color: 'rgba(200, 200, 200, 0.05)',
                        lineWidth: 1
                    }
                }],
                yAxes: [{
                    gridLines: {
                        color: 'rgba(200, 200, 200, 0.08)',
                        lineWidth: 1
                    }
                }]
            }, elements: {
                line: {
                    tension: 0.4
                }
            },
            legend: {
                display: false
            },
            point: {
                backgroundColor: 'white'
            },
            tooltips: {
                titleFontFamily: 'Open Sans',
                backgroundColor: 'orange',
                titleFontColor: 'white',
                tooltipFontStyle: "bold",
                caretSize: 5,
                cornerRadius: 2,
            }
        };

        var chartInstance = new Chart(bar_ctx, {
            type: 'line',
            data: data,
            options: options
        });

    </script>--}}
    <!-- SHIFT OVERVIEW START  -->
    <div class="row">
        <div class="col-xs-12 shift__overview">
            <div class="row flex-box flex-align-item-center overflow--visible">
                <div class="col-xs-4 shift__view">
                    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
                    <div class="flex-define">
                        <i class="icon-calendar"></i>&nbsp;
                        <input type="text" class="table_datePicker width-120" id="datepickerShiftOverview" value="Today" readonly/>
                        <i class="fa fa-circle fa-fw greenyellow" aria-hidden="true"></i>
                    </div>
                    <div class="flex-define margin-top-10 ">
                        <i class="fa fa-cutlery fa-fw" aria-hidden="true"></i>
                        <div class="selct-picker-plain">
                            <select class="selectpicker" data-style="no-background no-padding-left no-padding-top no-padding-bottom" name="slot_name"
                                    id="slot_name">
                                <option value="1" selected>Lunch</option>
                                <option value="2">Dinner</option>
                                <option value="3">Breakfast</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-xs-8 pull-right reservation-info">
                    <ul>
                        <li>50 Covers</li>
                        <li>17 Bookings</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main__container">
        <div class="row">
            <div class="col-xs-12 no-padding">
                <div class="flex-define overview">
                    <div class="col-xs-6 no-padding">
                        <h3>Shift Overview</h3>
                    </div>
                    <div class="col-xs-5 col-xs-offset-1 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2 add-note no-padding-right">
                        <button data-toggle="modal" data-target="#add-a-note" class="pull-right btn btn__primary fullWidth">Add a Note</button>
                        {{--<a href="javascript:void()" data-toggle="modal" id="bookATable" data-target="#add-a-note" class="pull-right btn btn__primary fullWidth">Add a Note</a>--}}
                    </div>
                </div>
            </div>

            <div class="col-sm-12 no-padding">
                <div class="col-xs-6 col-md-6 col-lg-6 no-padding-left">
                    <div class="separate">
                        <i class="fa fa-users fa-fw" aria-hidden="true"></i> Large Party Size (1)
                        <div class="flex-justify margin-top-20">
                            <div class="col-sm-2">14:40</div>
                            <div class="col-sm-8">Evan Thompson</div>
                            <div class="col-sm-2">9</div>
                        </div>
                    </div>
                    <div class="separate">
                        <i class="fa fa-comment fa-fw" aria-hidden="true"></i> Guest Special Requests
                        <div class="flex-justify margin-top-20">
                            <div class="col-sm-2">14:40</div>
                            <div class="col-sm-8">Issac
                                <p>Bussiness meet Give a quite corner</p></div>
                            <div class="col-sm-2"></div>
                        </div>
                    </div>
                    <div class="separate">
                        <i class="fa  fa-birthday-cake fa-fw" aria-hidden="true"></i> Special Occasions (1)
                        <div class="flex-justify margin-top-20">
                            <div class="col-sm-2">14:40</div>
                            <div class="col-sm-8">Evan Thompson</div>
                            <div class="col-sm-2">8</div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-5 col-xs-offset-1 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2 no-padding-right">
                    <div class="shift__notes margin-top-30">
                        <h4>Shift Notes</h4>
                        <div class="row">
                            <div class="fullWidth margin-bottom-10">
                                <h5 class="padding-right-25">Block 2 tables
                                    <span class="pull-right edit-delete-note">
                                        <div class="dropdown">
                                            <a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">
                                                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="javascript:void(0)">Edit</a></li>
                                                <li><a href="javascript:void(0)">Delete</a></li>
                                            </ul>
                                        </div>
                                    </span>
                                </h5>
                                <p>Investors visiting at 14:00</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="fullWidth margin-bottom-10">
                                <h5 class="padding-right-25">Block 4 tables
                                    <span class="pull-right edit-delete-note">
                                        <div class="dropdown">
                                            <a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">
                                                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="javascript:void(0)">Edit</a></li>
                                                <li><a href="javascript:void(0)">Delete</a></li>
                                            </ul>
                                        </div>
                                    </span>
                                </h5>
                                <p>Investors visiting at 14:00</p>
                            </div>
                        </div>
                    </div>
                    <div class="shift__notes margin-top-20">
                        <h4>Special Event</h4>
                        <div class="row">
                            <div class="fullWidth margin-bottom-10">
                                <h5>Tasing Menu
                                    <span class="pull-right edit-delete-note">
                                        <div class="dropdown">
                                            <a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">
                                                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="javascript:void(0)">Edit</a></li>
                                                <li><a href="javascript:void(0)">Delete</a></li>
                                            </ul>
                                        </div>
                                    </span>
                                </h5>
                                <p>Special Tasting menu by the Chef</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="fullWidth margin-bottom-10">
                                <h5>Special Menu
                                    <span class="pull-right edit-delete-note">
                                        <div class="dropdown">
                                            <a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">
                                                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="javascript:void(0)">Edit</a></li>
                                                <li><a href="javascript:void(0)">Delete</a></li>
                                            </ul>
                                        </div>
                                    </span>
                                </h5>
                                <p>Special Tasting menu by the Chef</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <!--Modal-->
    <div class="modal fade" id="add-a-note" tabindex="-1" role="dialog" aria-labelledby="addanoteLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <!--Content -->
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Add Note</h4>
                </div>
                <div class="modal-body">
                    <form id="addANote" class="validation-msg" action="">
                        <div class="col-sm-12">
                            <div class="input__field">
                                <input type="text" name="note_title" class="required" id="note_title">
                                <span for="note_title">Title</span>
                            </div>
                        </div>
                        <div class="col-sm-12 margin-top-25 note-type">
                            <div class="fullWidth bottom-line">
                                <div class="col-xs-6 no-padding-left margin-top-5 input-label">Type</div>
                                <div class="col-xs-6 selct-picker-plain no-padding-right">
                                    <select id="note_type" data-style="no-background-with-buttonline no-padding-left" name="note_type" class="selectpicker" >
                                        <option value="">Select</option>
                                        <option value='0'>Special Events</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 note-text">
                            <div class="form-group input__field">
                                <textarea class="edit-field" wrap="hard" name="note_message" rows="1" maxlength="300" id="note_message"></textarea>
                                <span class="input-label">Note</span>
                            </div>
                        </div>

                        <div class="col-sm-12 text-center margin-top-25">
                            <button type="button" class="btn_ btn__primary add-note-btn" >Add</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>


    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- SHIFT OVERVIEW END  -->
@endsection
