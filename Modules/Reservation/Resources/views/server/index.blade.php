@extends('layouts.app')
@section('content')

    <div class="main__container">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <!--Header-->
        <div class="reservation__atto">
            <div class="server__sec1 margin-top-5">
                <h1>Servers</h1>
                <div class="server-calendar">
                    <div class="calendar">
                        <i class="fa icon-calendar" aria-hidden="true"></i>
                        <input type="text" id="serverdatepicker" readonly />
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                    <div class="food-type">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>

                        <div class="selct-picker-plain list">
                            <select class="selectpicker" data-style="no-background" name="slot_name" id="slot_name">
                                @foreach($slot_names as $slot_arr)
                                    <option value="{{$slot_arr['slot_id']}}">{{$slot_arr['slot_name']}}</option>
                                @endforeach
                            </select>
                        </div>

                        <span id="slotValidation" class="font-weight-600 color-red padding-left-10 padding-top-bottom-6 hide">
                            Shifts are not defined for this date
                        </span>

                    </div>
                </div>
            </div>
            <!-- add server button -->
            <div class="pull-right add_server_btn">
                <a class="btn" href="#" data-toggle="modal" data-target="#addserver" id="add_server_btn">
                    Add Servers
                </a>
            </div>
        </div>


        <div class="server__container box-shadow server_list">
            @include("reservation::server.partial.server-list")
        </div>
        <script>
            var server_list = {!! $servers !!};
        </script>
        @include("reservation::server.partial.add-server")
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="{{asset('js/server.js')}}"></script>
@endsection
