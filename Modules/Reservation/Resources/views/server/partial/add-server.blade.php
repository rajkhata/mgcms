<!--Modal-->
<div class="modal fade" id="addserver" tabindex="-1" role="dialog" aria-labelledby="addserverLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
    <!--Content -->
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Add a New Server</h4>
            </div>
            <div class="modal-body">
                <form id="addNewServer" class="validation-msg" action="{{url('server/add')}}">
                <div class="col-sm-6">
                    <div class="input__field">
                        <input type="text" name="fname" class="required" id="fname">
                        <span for="fname">First Name</span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input__field">
                        <input type="text" name="lname" class="required" id="lname">
                        <span for="lname">Last Name</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input__field">
                        <input type="text" name="mobile" onkeypress='insertNumberOnly(event)' maxlength="10" class="required number" id="mobile" pattern="[0-9]*">
                        <span for="mobile">Mobile</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group input__field">
                        <input type="email" name="email" id="email">
                        <span  for="Email">Email (optional)</span>
                    </div>
                </div>

                <div class="col-sm-12 pick-color">
                    <h5>Pick a Color</h5>
                    <ul>
                        <li>
                            <input type="radio" name="color" id="1" value="98b3eb">
                            <label for="1" class="server__color" style="background-color: #98b3eb;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="2" value="d998ea">
                            <label for="2" class="server__color" style="background-color: #d998ea;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="3" value="ffcc00">
                            <label for="3" class="server__color" style="background-color: #ffcc00;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="4" value="f0ff00">
                            <label for="4" class="server__color" style="background-color: #f0ff00;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="5" value="a2cd01">
                            <label for="5" class="server__color" style="background-color: #a2cd01;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="6" value="11b411">
                            <label for="6" class="server__color" style="background-color: #11b411;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="7" value="d8d8d8">
                            <label for="7" class="server__color" style="background-color: #d8d8d8;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="8" value="056205">
                            <label for="8" class="server__color" style="background-color: #056205;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="9" value="d8d8d8">
                            <label for="9" class="server__color" style="background-color: #d8d8d8;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="10" value="9c9a9a">
                            <label for="10" class="server__color" style="background-color: #9c9a9a;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="11" value="4d4c4c">
                            <label for="11" class="server__color" style="background-color: #4d4c4c;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" name="color" id="12" value="000000">
                            <label for="12" class="server__color" style="background-color: #000000;">
                                <div class="colorpicker"></div>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12">
                    <div class="add-custom-color">
                        <input type="radio" name="color" id="customServerColor" value="">
                        <label for="customServerColor" class="no-margin-bottom" id="styleSpan"></label>
                        <div class="add-color">
                            <button id="add-color-btn" onclick="addCustomColorChecked()" class="jscolor
                                {valueElement:'customServerColor',styleElement:'styleSpan',value:'ffffff'}">
                                + &nbsp;Add Custom Color
                            </button>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12 text-center">
                    <input type="hidden" name="server_id" value="0" id="server_id">
                    <button type="button" class="btn_ btn__primary saveCloseBtn" >Save & Close</button>
                </div>
            </form>
        </div>
            <div class="modal-footer"></div>
    </div>
</div>
 <script src="{{ asset('js/jscolor.js') }}"></script>