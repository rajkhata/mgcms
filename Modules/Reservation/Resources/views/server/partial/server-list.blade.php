<!-- table headings start-->
<div class="server__table-header">
    <div class="col-sm-2 no-padding-left">Full Name</div>
    {{--<div class="col-sm-1">Status</div>--}}
    <div class="col-sm-2">Contact</div>
    <div class="col-sm-2">Floor</div>
    <div class="col-sm-2">Tables Assigned</div>
    <div class="col-sm-1 text-center">Covers</div>
    <div class="col-sm-1"></div>
    <div class="col-sm-2 no-padding">Actions</div>
</div>
<!-- table headings End-->
<div class="server__details-wrapper">
    <!--listing start-->
    @foreach($servers as $server)
        <div class="server-listing">

            <div class="col-sm-2 no-padding">
                <div class="server__name flex-box flex-align-item-center">
                    <span class="icon-circle" style="color: #{{$server->color}};"></span>
                    <p>{{$server->fname}} {{$server->lname}}</p>
                </div>
            </div>

            {{--<div class="col-sm-1 server__status">
                --}}{{--Available--}}{{--
            </div>--}}

            <div class="col-sm-2 contact-no">
                {{$server->mobile}}
            </div>

            <div class="col-sm-2 floor-assigned">
                {{implode(', ', $server->floor_name)}}
            </div>

            <div class="col-sm-2 table-assign">
                <ul>
                    @if(!empty($server->table_name))
                        <li>{{implode(', ', $server->table_name)}}</li>
                    @endif
                </ul>
            </div>
            <div class="col-sm-1 cover text-center">
                {{$server->total_cover}}
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-2 no-padding assign__table">
                <div class="assign_btn">
                    @if($slot_id)
                        <a href="{{url("server/assign/$server->id?assign_date=$assign_date")}}" class="btn assign_table">Assign Tables</a>
                    @else
                        <a href="javascript:void(0)" class="btn assign_table" disabled="disabled">Assign Tables</a>
                    @endif
                </div>

                <div class="cover edit-server">
                    <div class="dropdown dropup">
                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#" onclick="editServer({{$server->id}})">Edit</a></li>
                            <li><a href="#" onclick="deleteServer({{$server->id}})">Delete</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    @if($servers->count()==0)
        <div class="row">
        <div class="text-center col-sm-12 margin-top-bottom-40" >
            <strong>No Record Found</strong>
        </div>
        </div>
    @endif
</div>