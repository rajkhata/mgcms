@extends('layouts.app')
@section('content')
<div class="main__container server-tables-assignment">
    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>

    <!--Header-->
    <div class="row">
      <div class="server__sec1 pull-left margin-top-5">
          <h1>Servers</h1>
          <div class="server-calendar">
              <div class="calendar">
                  <i class="fa icon-calendar" aria-hidden="true"></i>
                  <input type="text" id="serverdatepicker_assign" readonly />
                  <i class="fa fa-angle-down" aria-hidden="true"></i>
              </div>
              <div class="food-type">
                  <i class="fa fa-clock-o" aria-hidden="true"></i>

                  <div class="selct-picker-plain assign">
                      <select class="selectpicker" data-style="no-background" name="slot_name" id="slot_name">
                          @foreach($slot_names as $slot_arr)
                              <option value="{{$slot_arr['slot_id']}}">{{$slot_arr['slot_name']}}</option>
                          @endforeach
                      </select>
                  </div>

                  <span id="slotValidation" class="font-weight-600 color-red padding-left-10 padding-top-bottom-6 hide">
                      Shifts are not defined for this date
                  </span>

              </div>
          </div>
      </div>
      <div class="pull-right add_tables_btn">
        <a class="btn btn__primary padding-left-25 padding-right-25 {{count($slot_names)<=0?'pointer-none':''}}" disabled="disabled" href="javascipt:void(0)" id="assign_server">
          Done
        </a>
        <input type="hidden" name="server_id" id="server_id" value="{{$server_detail->id}}">
      </div>
    </div>

    <div class="row">
      <div class="server__container box-shadow margin-top-20">
        <div class="server__details-wrapper">
            <div class="row server_select_table_header padding-top-bottom-20">
                <div class="pull-left no-padding-left padding-top-bottom-10">
                    Select tables for quick assign
                </div>
                <div class="pull-right col-sm-7 col-md-6 col-lg-4 no-padding-right">
                    <div class="col-sm-7 table_assign_name padding-top-bottom-10 text-right">
                        <i class="icon-circle" style="color: #{{$server_detail->color}};"></i>
                        Assigned to <span style="color: #{{$server_detail->color}};">{{$server_detail->fname}} {{$server_detail->lname}}</span>
                    </div>
                    <div class="col-sm-5 selct-picker-plain no-padding-right">
                        <select class="selectpicker" data-style="no-background" id="floor_name" name="floor_name">
                            @foreach ($floormenu as $floor)
                                <option value="{{$floor->id}}">{{$floor->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row floor-tables-area">
                <div id="floor-tables-view" class="margin-top-30 floor-tables-view no-padding"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://cdn.rawgit.com/konvajs/konva/2.1.3/konva.min.js"></script>
    <script>
        var floor_and_tables = {!! $floor_data !!};
        var  currentServerId = "{!! $server_detail->id !!}";
        var currentServerName = "{!! substr($server_detail->fname, 0,1) !!}{!! substr($server_detail->lname, 0,1) !!}";

        //console.log(floor_and_tables1);
        //var floor_and_tables=["{\"floorName\": \"vb V\", \"floorType\": \"Square\", \"floorTypeRow\": \"5\", \"floorTypeColumn\": \"5\"}",["{\"id\":30,\"server\":[{\"serverName\":\"JN\",\"serverTableId\":\"\",\"serverTableColor\":\"#ff0000\"}],\"tableID\":\"tableId_0\",\"maxGuests\":\"1\",\"minGuests\":\"1\",\"tableText\":\"A1\",\"tableShape\":\"Round\",\"tableWidth\":120,\"tableXAxis\":540,\"tableYAxis\":80,\"tableHeight\":120,\"tableBottomId\":\"tablebottomId_0\",\"tableBottomXAxis\":60,\"tableBottomYAxis\":60,\"reservations\":[],\"tableAvailability\":\"Available\"}","{\"id\":31,\"server\":[{\"serverName\":\"JN\",\"serverTableId\":\"\",\"serverTableColor\":\"#ff0000\"}],\"tableID\":\"tableId_1\",\"maxGuests\":\"5\",\"minGuests\":\"1\",\"tableText\":\"YU\",\"tableShape\":\"Round\",\"tableWidth\":120,\"tableXAxis\":320,\"tableYAxis\":260,\"tableHeight\":120,\"tableBottomId\":\"tablebottomId_1\",\"tableBottomXAxis\":60,\"tableBottomYAxis\":60,\"reservations\":[],\"tableAvailability\":\"Available\"}","{\"id\":32,\"server\":[{\"serverName\":\"JN\",\"serverTableId\":\"\",\"serverTableColor\":\"#ff0000\"}],\"tableID\":\"tableId_2\",\"maxGuests\":\"7\",\"minGuests\":\"1\",\"tableText\":\"ERT\",\"tableShape\":\"Round\",\"tableWidth\":120,\"tableXAxis\":100,\"tableYAxis\":380,\"tableHeight\":120,\"tableBottomId\":\"tablebottomId_2\",\"tableBottomXAxis\":60,\"tableBottomYAxis\":60,\"reservations\":[],\"tableAvailability\":\"Available\"}"],"{\"12\":\"bill_dropped\",\"2\":\"cancelled\",\"5\":\"confirmed\",\"11\":\"dessert\",\"9\":\"entree\",\"14\":\"finished\",\"10\":\"main_course\",\"3\":\"no_show\",\"13\":\"paid\",\"6\":\"partially_seated\",\"1\":\"reserved\",\"4\":\"running_late\",\"7\":\"seated\",\"8\":\"starters\",\"15\":\"waitlist\"}"];
        //console.log(floor_and_tables);
    /*$("#serverdatepicker_assign").datepicker({
        dateFormat: 'mm-dd-yy',
        minDate: 0
    }).datepicker("setDate", '<?php echo $assign_date; ?>');*/
    </script>
    <script src="{{asset('js/floor.js')}}"></script>
    <script src="{{asset('js/server.js')}}"></script>
@endsection
