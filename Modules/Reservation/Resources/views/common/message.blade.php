

	@if ($errors->any())
		<div class="row messageblock">
		<div class="alert alert-danger">
		    <ul>
		        @foreach ($errors->all() as $error)
		            <li>		        	
		            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
{{ $error }}
					</li>
		        @endforeach
		    </ul>
		</div>
		</div>
	     
	@endif
	@if (session('status'))
		<div class="row messageblock">
	    <div class="alert alert-success">
        	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
	        {{ session('status') }}
	    </div>
		</div>
	@endif
