@extends('layouts.app')
@section('content')
    <div class="main__container">
    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <form name="tat_form" id="tat_form" method="POST" action="/configure/turnovertime">
            <!--Header-->
            <div class="reservation__atto">
                <div class="reservation__title">
                    <h1>Turnover Time</h1>
                    <p class="note margin-top-20"><strong>Specify Turnover time (in minutes) for different party sizes</strong></p>
                </div>
            </div>
            @if (session('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('success') }}
                    </div>
            @endif
            @if(session('danger'))
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{session('danger')}}
                </div>
            @endif
            <!--Turn over time-->
            <div class="turnOver__box ">
                
                <div class="box-shadow dis--flex pull-left">
                   <!--
                    <div class="header__row row">

                        <div class="col-xs-3 col-sm-3 col-md-3 first_col">
                            Table Type
                        </div>
                        @if(isset($tables['table_type_name']) && !empty($tables['table_type_name']))
                            @foreach($tables['table_type_name'] as $typename)
                                <div class="col-xs-3 col-sm-3 col-md-3 table__type">
                                    {{$typename}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                -->

                    <div class="partySize__row row">

                        <div class="col-xs-3 col-sm-3 col-md-3 first_col">
                            <div class="icon">
                                <img src="{{asset('images/user.png')}}">
                            </div>

                            <div class="column_header">
                                Party Size<br />
                                <span>(Persons)</span>
                            </div>

                        </div>

                        @if(isset($tables['min_max']) && !empty($tables['min_max']))
                            @foreach($tables['min_max'] as $min_max)
                                <div class="col-xs-3 col-sm-3 col-md-3 party_type">
                                    {{$min_max}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="turnOver__row row">

                        <div class="col-xs-3 col-sm-3 col-md-3 first_col">
                            <div class="icon">
                                <img src="{{asset('images/clock_icon.png')}}">
                            </div>

                            <div class="column_header">
                                Turnover time<br />
                                <span>(Minutes)</span>
                            </div>
                        </div>
                        @if(isset($tables['id']) && !empty($tables['id']))
                            @foreach($tables['id'] as $key => $id)
                                <div class="col-xs-3 col-sm-3 col-md-3 turover_type">
                                    <div class="center slider">
                                        <div class="input-group">
              <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-number no-padding text-center" data-type="minus" data-field="tat[{{$id}}]">
                  <i class="fa fa-minus margin-left-5"></i>
                </button>
              </span>
                                            <input type="text" name="tat[{{$id}}]" onfocus="this.blur()" class="form-control input-number" min="15" max="600" value="{{(isset($tables['tat'][$key]) && !empty($tables['tat'][$key]))?$tables['tat'][$key]:config('reservation.slot_interval_time')}}" data-inc-val="{{config('reservation.slot_interval_time')}}" readonly="readonly">
                                            <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-number no-padding text-center" data-type="plus" data-field="tat[{{$id}}]">
                  <i class="fa fa-plus margin-left-5"></i>
                </button>
              </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                   <!--

                <div class="row edit-cross-sec">
                    <div class="col-lg-3 col-md-3 col-sm-3"></div>
                    @if(isset($tables['id']) && !empty($tables['id']))
                        @foreach($tables['id'] as $key => $id)

                            <div class="col-lg-3 col-md-3 col-sm-3 edit-cross-icon text-center" tabltype_data='@json($tables["table_types"][$key])'>
                                <i class="fa fa-pencil edit_table_type" onclick='openEditTableTypeform({{$id}},@json($tables["table_types"][$key]))' table_type_id="{{$id}}" aria-hidden="true"></i>
                                <i class="fa fa-times delete_table_type" aria-hidden="true" table_type_id="{{$id}}"></i>
                            </div>

                        @endforeach
                    @endif

                </div>
            -->

            </div>



            <!--Submit Button-->
            <div class="turnOver__submitBtn">
                <!--
                <i class="fas fa-pencil-alt"></i>
                                   
 
                <button class="btn__add inc-width" data-toggle="modal" data-target="#add-tabletype-modal" type="button">Add new table type</button>
                           -->
                <button class="btn__add margin-top-20" type="submit">Save</button> 


                {{csrf_field()}}
            </div>
        </form>


    </div>

    <!-- Modal -->
    <div id="add-tabletype-modal" class="modal modal-popup fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Add Table Type</h4>
                </div>
                <div class="modal-body">
                    <form name="addtabletype" action="" id="add-tabletype">
                        <div class="row form-field-control mt-10">
                            <div class="col-lg-6 col-xs-6 col-lg-offset-1">Table Type</div>
                            <div class="col-lg-5 col-xs-6">
                                <input type="text" name="name" class="form-control" ></div>
                        </div>
                        <div class="row form-field-control mt-10">
                            <div class="col-lg-6 col-xs-6 col-lg-offset-1">Minimun Capacity</div>
                            <div class="col-lg-5 col-xs-6 ">
                                <div class="center slider">
                                    <div class="input-group">
              <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="min">
                  <i class="fa fa-minus"></i>
                </button>
              </span>

                                        <input type="text" name="min" class="form-control input-number turnovertime_input" min="1"  value="2" max="300" readonly="readonly">
                                        <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="min">
                  <i class="fa fa-plus">
                  </i>
                </button>
              </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-field-control mt-10">
                            <div class="col-lg-6 col-xs-6 col-lg-offset-1">Maximum Capacity</div>
                            <div class="col-lg-5 col-xs-6 ">
                                <div class="center slider">
                                    <div class="input-group">
              <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="max">
                  <i class="fa fa-minus"></i>
                </button>
              </span>

                                        <input type="text" name="max" class="form-control input-number turnovertime_input"  readonly="readonly" min="1"  value="2"  max="300"  >
                                        <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="max">
                  <i class="fa fa-plus">
                  </i>
                </button>
              </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-field-control mt-10">
                            <div class="col-lg-6 col-xs-6 col-lg-offset-1">Turnover Time<br> (Minutes)</div>
                            <div class="col-lg-5 col-xs-6 ">
                                <div class="center slider">
                                    <div class="input-group">
              <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="tat">
                  <i class="fa fa-minus"></i>
                </button>
              </span>

                                        <input type="text" name="tat" class="form-control input-number" min="30"  value="30"   max="600" readonly="readonly">
                                        <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="tat">
                  <i class="fa fa-plus">
                  </i>
                </button>
              </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"  onclick="saveTabeltype()">Add</button>
                </div>
            </div>

        </div>
    </div>



    <div id="update-tabletype-modal" class="modal modal-popup fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Update Table Type</h4>
                </div>


                <div class="modal-body">
                    <form name="updatetabletype" action="" id="update-tabletype">
                        <div class="row form-field-control mt-10">
                            <div class="col-lg-6 col-xs-6 col-lg-offset-1">Table Type</div>
                            <div class="col-lg-5 col-xs-6">
                                <input type="hidden" name="table_type_id"  class="table_type_id" >
                                <input type="text" name="name"  class="name form-control" ></div>
                        </div>
                        <div class="row form-field-control mt-10">
                            <div class="col-lg-6 col-xs-6 col-lg-offset-1">Minimun Capacity</div>
                            <div class="col-lg-5 col-xs-6 ">
                                <div class="center slider">
                                    <div class="input-group">
              <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="min">
                  <i class="fa fa-minus"></i>
                </button>
              </span>
                                        <input type="text" name="min"  class="form-control input-number min
                                        turnovertime_input" min="1"  value="2" max="300"   readonly="readonly">
                                        <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="min">
                  <i class="fa fa-plus">
                  </i>
                </button>
              </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-field-control mt-10">
                            <div class="col-lg-6 col-xs-6 col-lg-offset-1">Maximum Capacity</div>
                            <div class="col-lg-5 col-xs-6 ">
                                <div class="center slider">
                                    <div class="input-group">
              <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="max">
                  <i class="fa fa-minus"></i>
                </button>
              </span>
                                        <input type="text" name="max" class="form-control max input-number turnovertime_input"  readonly="readonly" min="1"  value="2"   max="300" >
                                        <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="max">
                  <i class="fa fa-plus">
                  </i>
                </button>
              </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-field-control mt-10">
                            <div class="col-lg-6 col-xs-6 col-lg-offset-1">Turnover Time<br> (Minutes)</div>
                            <div class="col-lg-5 col-xs-6 ">
                                <div class="center slider">
                                    <div class="input-group">
              <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="tat">
                  <i class="fa fa-minus"></i>
                </button>
              </span>
                                        <input type="text" name="tat" class="form-control tat input-number" min="30"  value="30" max="600"  readonly="readonly">
                                        <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="tat">
                  <i class="fa fa-plus">
                  </i>
                </button>
              </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default updatebtn"  >Update</button>
                </div>
            </div>

        </div>
    </div>
    <script src="{{asset('js/turnovertime.js')}}"></script>
@endsection