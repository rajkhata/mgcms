@extends('layouts.app')
<!--Content-->
@section('content')
<script>

  var floor_and_tables={!! $floor_data !!};
  var floor_url="<?php echo URL::current()?>";
function wait(cname, cvalue,curr) {
      curr.parents('.reservationStatusPanel__user').removeClass('reserved__timer__late');
      console.log($(this).text());
      var min=5;
      var d = new Date();
      d.setTime(d.getTime() + ( min * 60 * 1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
      $('.'+cname).hide();
}  

</script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.timepicker.css')}}" />
      <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
      <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
      <link href="{{asset('css/grid-calendar-view.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />

<style id="statusStyle"></style>


  <div id="tableManagement__wrapper" class="tableManagement__wrapper">
    <div class="floor_nav">
    

        <div class="sub__navbar col-sm-12  no-padding">

            <div class="col-sm-12 no-padding">
            <div class="side-bar-open hidden no-margin margin-top-5"><i class="fa fa-bars" aria-hidden="true"></i></div>
                <div class="selct-picker-plain col-sm-3 col-md-5">
                  
                    @foreach ($floormenu as $floor)                   
                    @if($current_floor==$floor->id)
                    <div class="floorlayout__name" data-toggle="tooltip" data-placement="right" title="{{$floor->name}}">
                        {{$floor->name}}
                    </div>
                    @endif
                    @endforeach
                    
                </div>

                <div class="booking__type pull-right no-padding margin-right-60 col-sm-8 col-md-7 flex-justify-content-end">
                    <div class="booking__btn new no-margin-left" data-toggle="modal" id="bookATable" data-target="#book-a-table">
                    New Reservation
                    </div>
                    <div class="booking__btn walkin 1" data-toggle="modal" id="new_walkin_button" data-target="#newWalkinModal">
                    New Walk-in
                    </div>
                    <div class="booking__btn" data-toggle="modal" id="block_a_table" data-target="#blockTables">
                        Block Tables
                    </div>
                </div>
                
            </div>
            <div class="col-sm-12 floor-views margin-top-5 no-padding tablet-margin-left-25">
                <div class="current__date floor_fetch col-sm-2">
                        <span class="icon-calendar" aria-hidden="true"></span>
                        <input type="text"  readonly id="datepicker" class="table_datePicker" />
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                        <i class="fa fa-circle greenColor hide" aria-hidden="true"></i>
                    </div>
                <div class="sub__navbar__right no-padding">
                    <div class="view__type">

                        <div id="floor-view-btn" title="Floor View" class="view active">
                            <span class="fa fa-th-large" aria-hidden="true"></span>
                            <span class="view__title">Floor View</span>
                        </div>

                        <div id="grid-view-btn" title="Grid View" class="view">
                            <span class="fa fa-bars" aria-hidden="true"></span>
                            <span class="view__title">Grid View</span>
                        </div>



                        {{--<span class="selct-picker-plain">
                            <select class="selectpicker" data-style="no-background no-padding-left">
                                <option value="">Show Timer</option>
                                <option value="currentReservationTime" >Current Reservation Time</option>
                                <option value="remainingTime">Remaining Time</option>
                                <option value="nextReservationTime">Next Reservation Time</option>
                            </select>
                        </span>--}}

                        <div id="show-hide-timer" class="dropdown no-padding view">
                            <span class="fa fa-clock-o" aria-hidden="true"></span>

                            <button id="showTimer" class="btn btn-link dropdown-toggle no-padding padding-right-25" title="Show Timer" type="button" data-toggle="dropdown">
                                <span class="showTimer">Show Timer</span>
                                <span class="fa fa-angle-down"></span>
                            </button>

                            <button id="hideTimer" onclick="floorResetTimer();" class="btn btn-link no-padding hide" title="Hide Timer" type="button">
                                <span class="hideTimer">Hide Timer</span>
                            </button>

                            <ul class="dropdown-menu no-padding auto-width">
                                <li>
                                    <a href="javascript:currentlySeatedTime();">Currently Seated Time</a>
                                </li>
                                <li>
                                    <a href="javascript:timeRemaining();">Time Remaining</a>
                                </li>
                                <li>
                                    <a href="javascript:nextReservationTime();">Next Reservation</a>
                                </li>
                            </ul>
                        </div>


                    </div>

                    {{--<div class="status__atto hide">

                        <div class="status seated" data-toggle="modal" data-target="#newWaitlistModal">
                        Seated
                        </div>

                        <div class="status reserved" data-toggle="modal" data-target="#manageWaitlistModal">
                        Reserved
                        </div>

                        <div class="status available" data-toggle="modal" data-target="#seatedtable">
                        Available
                        </div>

                    </div>--}}
                </div>
                
            </div>
        </div>
       

        
    </div>
    <!--Sub-nav-->
    <div class="floor_nav hide">
      <div class="sub__navbar">

        <div class="current__floor ">
          <div class="selct-picker-plain">                        
            
              @foreach ($floormenu as $floor)                   
              @if($current_floor==$floor->id)
              <div class="floorlayout__name text-ellipsis" data-toggle="tooltip" data-placement="right" title="{{$floor->name}}">
                  {{$floor->name}}
              </div>
              @endif
              @endforeach
            
          </div>

          <div class="current__date floor_fetch col-sm-11 padding-left-zero">
            <span class="icon-calendar" aria-hidden="true"></span>
            <input type="text"  readonly id="datepicker" style="width: 100%;" class="table_datePicker" />
            <i class="fa fa-angle-down" aria-hidden="true"></i>
            <i class="fa fa-circle greenColor hide" aria-hidden="true"></i>
          </div>
        </div>
      </div>
        <div class="sub__navbar__right">
        <div class="view__type">

            <div id="floor-view-btn" title="Floor View" class="view active">
              <span class="fa fa-th-large" aria-hidden="true"></span>
              <span class="view__title">Floor View</span>
            </div>

            <div id="grid-view-btn" title="Grid View" class="view">
              <span class="fa fa-bars" aria-hidden="true"></span>
              <span class="view__title">Grid View</span>
            </div>

            <div class="view" title="Show Timer">
              <span class="fa fa-clock-o" aria-hidden="true"></span>
                {{--<span class="selct-picker-plain">
                    <select class="selectpicker" data-style="no-background no-padding-left">
                        <option value="">Show Timer</option>
                        <option value="currentReservationTime" >Current Reservation Time</option>
                        <option value="remainingTime">Remaining Time</option>
                        <option value="nextReservationTime">Next Reservation Time</option>
                    </select>
                </span>--}}

                <div id="show-hide-timer" class="dropdown no-padding hide">
                    <button class="btn btn-link dropdown-toggle no-padding-left" type="button" data-toggle="dropdown">
                        <span class="showTimer">Show Timer</span>
                        <span onclick="floorResetTimer();" class="hideTimer hide">Hide Timer</span>
                        <span class="fa fa-angle-down"></span>
                    </button>

                    <ul class="dropdown-menu no-padding auto-width">
                        <li>
                            <a href="javascript:currentlySeatedTime();">Currently Seated Time</a>
                        </li>
                        <li>
                            <a href="javascript:timeRemaining();">Time Remaining</a>
                        </li>
                        <li>
                            <a href="javascript:nextReservationTime();">Next Reservation</a>
                        </li>
                    </ul>
                </div>
            </div>



          {{--<div class="status__atto hide">

            <div class="status seated" data-toggle="modal" data-target="#newWaitlistModal">
              Seated
            </div>

            <div class="status reserved" data-toggle="modal" data-target="#manageWaitlistModal">
              Reserved
            </div>

            <div class="status available" data-toggle="modal" data-target="#seatedtable">
              Available
            </div>

          </div>--}}
        </div>






      <div class="booking__type margin-right-70">
        <div class="booking__btn new" data-toggle="modal" data-target="#book-a-table">
          New Reservation
        </div>
        <div class="booking__btn walkin 1" data-toggle="modal" id="new_walkin_button" data-target="#newWalkinModal">
          New Walk-in
        </div>
        <div class="booking__btn hide" data-toggle="modal" data-target="#blockTables">
            Block Tables
        </div>
      </div>

        </div>
    </div>

    <div class="main__container">
    
      <div id="floor-view" >
          @if(!empty($servers))
              <div class="server__status-bar">
                  <div class="server__title">
                      <span class="fa fa-users" aria-hidden="true"></span>Servers:
                  </div>
                  <div id="server_listing" class="flex-box flex-align-item-center"></div>
              </div>
          @endif
          <div class="floor-tables-area">
            <div id="floor-tables-view" class="margin-top-30 box-shadow floor-tables-view"></div>
          </div>
      </div>



        <style id="gridTimerStyle"></style>
      <div id="grid-view" class=" box-shadow hide">
        <div id="grid-calendar"></div>

          <!--
        <div class="grid-clock-container hide">
        <div id="grid-clock" class="grid-clock">15:30</div>
          </div>
-->

          <?php
              $datetime = \App\Helpers\CommonFunctions::getRestaurantCurrentLocalTime(\Illuminate\Support\Facades\Auth::user()->restaurant_id, 'c');
          ?>

          <script>
              window.timeZoneType = "{{$timezone}}";
              window.nowTimeShow = "{{ $datetime }}"
          </script>

    </div>
      </div>
    <div class="tablePlacement__order">

    </div>

    <div class="side-section">

        <div title="Reservation Status" class="open_rightBar"><span class="fa icon-open-sidebar" aria-hidden="true"></span></div>

        <div class="total-covers">
            <h4 class="text-center margin-bottom-0 total_cover_current"><?=count($current_reservations_data)>0?array_sum(array_column($current_reservations_data,'reserved_seat')):0?></h4>
            <p class="text-center font-size-12 margin-top-5">Covers</p>
        </div>
        <ul>
            <li><a href="#current" data-trigger="tab">
                    <span class="icon-current-reservation" aria-hidden="true"></span>
                    <p class="font-size-12 margin-top-5">Current</p>
                </a>
            </li>
            <li><a href="#upcoming" data-trigger="tab">
                    <span class="icon-upcoming-reservation" aria-hidden="true"></span>
                    <p class="font-size-12 margin-top-5">Upcoming</p>
                </a>
            </li>
            <li><a href="#waitlist" data-trigger="tab">
                    <span class="icon-waiting1" aria-hidden="true"></span>
                    <p class="font-size-12 margin-top-5">Waitlist</p>
                </a>
            </li>
        </ul>
    </div>
        
  </div>

  @include("reservation::table-management.partial.new-walkin")
  @include("reservation::table-management.partial.new-waitlist")
  @include("reservation::table-management.partial.manage-waitlist")
  @include("reservation::table-management.partial.new__booking")
  @include('reservation::reservation.partial.confirm-cancel')
  @include('reservation::table-management.partial.block-table')
  @include('reservation::reservation.partial.reservation-details')
  @include("reservation::reservation.partial.cancel-pop-up")
  @include("reservation::table-management.partial.show-popup")
@endsection

<!--Right side bar-->

@section('side_bar_right')
  <div class="sidebar_right__content floor-view-sidebar">

    <div class="close__sidebar hide"><span class="fa fa-angle-right" aria-hidden="true"></span></div>
    
    <!--Content-->
    <div class="reservationSide__header">
        
      <div class="sideBar__heading fullWidth">
        <div class="row">
        <a type="button" class="close fa fa-angle-right no-margin-top margin-bottom-10 popupclose" data-dismiss="modal" aria-label="Close"></a>
        </div>
        <div class="row">
          <div class="col-xs-7 total_reservation">{{count($current_reservations_data)}} Reservations</div>
          <div class="col-xs-5 text-right total_cover"><?=count($current_reservations_data)>0?array_sum(array_column($current_reservations_data,'reserved_seat')):0?> Covers</div>
        </div>
      </div>

    </div>

    <!--Reservation Status Tabs-->
    <div id="reservations" role="tablist" class="sideBar-reservationStatusTabs nav nav-tabs">
      <!--Tab-->

      <a href="#current" aria-selected="true" data-toggle="tab" class="nav-link status_tab current active">
        <span class="icon-current-reservation" aria-hidden="true"></span>&nbsp;&nbsp;Current
      </a>

      <!--Tab-->
      <a href="#upcoming" aria-selected="false" data-toggle="tab" class="nav-link status_tab upcoming">
          <span class="icon-upcoming-reservation" aria-hidden="true"></span>  Upcoming
      </a>

      <!--Tab-->
      <a href="#waitlist" aria-selected="false" data-toggle="tab" class="nav-link status_tab waitlist">
        <span class="icon-waiting1" aria-hidden="true"></span>&nbsp;&nbsp;Waitlist
      </a>

    </div>
      <div class="right-searchbar">
          <i class="fa fa-search" aria-hidden="true"></i>
          <input type="text" placeholder="Search by Name, Phone or Reservation ID"  name="keyword" id="keyword"/>
      </div>

    <!--Reservation Status Panels-->
    <div class="sideBar-reservationStatusPanels tab-content ">
      <!--Current-->
        <div id="current" role="tabpanel" class="reservationStatusPanel tab-pane active">

        <!--User-->
            <div class="current_div scroller">
        @foreach($current_reservations_data as $reservation)

        <div class="reservationStatusPanel__user @if($reservation['is_no_show'] && $reservation['wait_for']==0) reserved__timer__late  @endif seated__user" reservation-id="{{$reservation['id']}}" reservation-status-slug="{{$reservation['status_slug']}}">

          <div class="reservationStatusPanel__user--details">

            <div class="reservationStatusPanel-user__name" data-toggle="modal" data-target="#tableManagement__popup">

                <div><span class="{{$reservation['status_icon']['sidebar']}}" aria-hidden="true" style="color: {{$reservation['status_color']['sidebar']}}"></span><h3 onclick="openReservationDetailPopUp({{ $reservation['id'] }})">{{$reservation['name']}}</h3></div>
                <div class="special--occasion">
                    @if($reservation['special_occasion'])
                    <span data-toggle="tooltip" class="{{$reservation['special_occasion']['icon']}}" title="{{$reservation['special_occasion']['name']}}" aria-hidden="true"></span>
                    @endif
                    @if($reservation['user_type'])
                        <span class="{{$reservation['user_type']['icon']}}" title="{{$reservation['user_type']['name']}}" aria-hidden="true"></span>
                    @endif
                </div>
            </div>

            <div class="reservationStatusPanel-user__size margin-left-40">

              <div class="user__time">
                <span>{{$reservation['start_time']}}</span>
              </div>

              <span class="dash">-</span>

              <div class="numberOfPeople">
                <span>{{$reservation['reserved_seat']}}</span> People
              </div>

              <span class="dash">-</span>
              <div class="tableId text-ellipsis">
                {{$reservation['table_name']}}
              </div>
            </div>

              <select id="status_current" data-style="margin-left-40 status-{{$reservation['status_slug']}}" name="status" class="sidbar_status dropdown__text_ul selectpicker show-tick" required="">
                  <option style="background-color: {{$reservation['status_color']['sidebar']}}" data-icon="{{$reservation['status_icon']['sidebar']}}" value="" selected="selected">{{$reservation['status_name']}}</option>
                  @foreach($reservation['next_status'] as $status)
                      <option style="background-color: {{$status['color']}}" data-icon="{{$status['icon']}}" value="{{$status['id']}}">{{$status['name']}}</option>
                  @endforeach
              </select>
              @if($reservation['is_no_show'] && $reservation['wait_for']==0)
                  <div class="reserved__timer <?php echo 'wait_'.$reservation['id'];?>">

                      <div class="reserved__timer_header">
                          The Guest is late by: <span class="timer_minute">{{$reservation['late_minutes']}} min</span>
                      </div>

                      <div class="timer_cta">
                          <a href="#" class="timer_noShow" onclick="markAsNoShow(<?=$reservation['id']?>, '<?= $reservation['status_slug']?>',$(this))">
                              <span class="noShow__icon" aria-hidden="true">
                                  <img src="{{asset('images/cross_icon.png')}}">
                              </span>
                              <span class="cta__content">Mark as<br />No Show</span>
                          </a>

                          <a href="#" class="timer_wait" onclick="wait('<?php echo 'wait_'.$reservation['id'];?>',<?php echo $reservation['id'];?>,$(this))" >
                              <span class="wait__icon" aria-hidden="true">
                                  <img src="{{asset('images/clock_icon.png')}}">
                              </span>
                              <span class="cta__content">Wait another<br />5 min</span>
                          </a>

                      </div>

                  </div>
              @elseif($reservation['is_about_to_finish'])
                  @php
                  $resv_message = 'Reservation is about to finish';
                  if($reservation['time_left'] < 0) {
                      $reservation['time_left'] = -$reservation['time_left'];
                      $resv_message = 'Reservation end time exceeded by';
                  }
                  @endphp
                  <div class="show-timer-container <?php echo 'wait_'.$reservation['id'];?>">
                      <div class="reserved__timer_header">
                          {{ $resv_message }}: <span class="timer_minute"> {{ $reservation['time_left'] }}</span> min
                      </div>
                      <div class="timer_cta">
                          <a href="#" class="timer_noShow" onclick="changeReservationStatus('<?php echo $reservation['id']; ?>','<?php echo $reservation['finish_status']; ?>','current',null,null,'<?php echo $reservation['floor_id']; ?>','<?php echo $reservation['table_id']; ?>')">
                              <span class="noShow__icon icon-cancelled" aria-hidden="true"></span>
                              <span class="cta__content">Mark as<br />Finished</span>
                          </a>

                          <a href="#" class="timer_wait" onclick="extendTillNextSlot('<?php echo 'wait_'.$reservation['id']; ?>', '<?php echo $reservation['id']; ?>', $(this))">
                              <span class="wait__icon icon-anti_clockwisetimer" aria-hidden="true"></span>
                              <span class="cta__content">Extend till<br />Next slot</span>
                          </a>

                      </div>
                  </div>
              @endif
          </div>

        </div>
        @endforeach
            </div>





      </div>

      <!--Upcoming-->
      <div id="upcoming" role="tabpanel" class="reservationStatusPanel tab-pane">
          <div class="upcoming_div scroller"></div>
      </div>

      <!--Waitlist-->
      <div id="waitlist" role="tabpanel" class="reservationStatusPanel tab-pane">
          <div class="waitlist_div"></div>
      </div>

    </div>
    
 
  </div>
  
  <script src="{{URL::asset('js/schedular/moment.min.js')}}"></script>
  <script src="{{URL::asset('js/schedular/timezone.js')}}"></script>
  <script src="{{URL::asset('js/schedular/timezone-with-data.js')}}"></script>
  <script src="{{URL::asset('js/schedular/fullcalendar.min.js')}}"></script>
  <script src="{{URL::asset('js/schedular/scheduler.min.js')}}"></script>
  <script src="{{URL::asset('js/reservation.js')}}"></script>
  <script src="{{URL::asset('js/turnovertime.js')}}"></script>
  <script src="https://cdn.rawgit.com/konvajs/konva/2.1.3/konva.min.js"></script>
  <script src="{{URL::asset('js/floor.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('js/jquery-ui-1.12.1.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.js')}}"></script>
  <script language="javascript" type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
  <script src="{{URL::asset('js/tags/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>
  <script src="{{URL::asset('js/guestbook.js')}}" type="text/javascript" charset="utf-8"></script>

  <!-- Modal -->
<div class="modal right fade create-table-reservation" id="reservation_info_popup" style="display: none;" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">

    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <a type="button" class="close icon-close" data-dismiss="modal" aria-label="Close"></a>
                <div class="seated-table__sec1">
                    <div class="row sidebar-head">
                    <div class="col-xs-7">2 Reservations</div>
                    <div class="col-xs-5 text-right">4 Covers</div>
                    </div>
                    <div class="table_sec1">
                        <div class="table__name">
                            <div>
                                <span class="table-color" style="background-color: #c3f58e;"></span>
                                <span>Table D2</span>
                            </div>
                        </div>
                        <div>Server: Alex D.</div>
                    </div>
                    <div class="table_sec2">
                        <div class="table_details">4 People</div>
                        <div class="table_details">Floor 2</div>
                    </div>


                    <div class="table_sec3">

                        <div class="assign__btn text-center">
                            <a href="#" class="btn">New Reservation</a>
                            <a href="#" class="btn border--btn">New Walk-in</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="modal-body xcx">
                <div  class="table-popup-contain">

                    <div class="seated-table__sec2">

                        <ul class="nav nav-pills seat__status">
                            <li class="">
                                <h4>Current</h4>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div id="Current" class="current--sec">

                                <div class="person_detail">
                                    <div class="person_name">
                                        <h4><i class="fa fa-user" aria-hidden="true"></i> Divya</h4>
                                        <p>3:30 PM - 2 People </p>
                                    </div>
                                    <div class="complete__btn margin-left-40">

                                        <div class="complete__btn dropdown--picker orange-btn">
                                            <select id="table_type" name="table_type" class="table_type selectpicker" required="">

                                                                <option value="">Select</option>
                                                                <option min="2" max="4" tat="30">B1</option>
                                                                <option min="4" max="6" tat="60">B2</option>
                                                                <option min="6" max="8" tat="90">B3</option>
                                                        </select>
                                        </div>
                                  
                                  
                                   
                                    </div>
                                </div>
                                <div class="time__section">
                                    <div class="time_sec1">Time remaining until end of assigned time:</div>
                                    <div class="time_sec2">23:09 <span>min</span></div>
                                </div>

                            </div>
                        </div>


                        <ul class="nav nav-pills seat__status">
                            <li class="">
                                <h4>upcoming</h4>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div  class="upcoming-sec">
                                <div class="person_detail">
                                    <div class="person_name">
                                        <h4><i class="fa fa-user" aria-hidden="true"></i> Divya</h4>
                                        <p>3:30 PM - 2 People </p>
                                    </div>
                                    <div class="cancle__btn">
                          
                                     <div class=" dropdown--picker green-btn">
                                              <select id="table_type" name="table_type" class="table_type selectpicker" data-style="margin-left-40" required="">

                                                                  <option value="">Select</option>
                                                                  <option min="2" max="4" tat="30">Reserved</option>
                                                                  <option min="4" max="6" tat="60">Cancelled</option>
                                                                  <option min="6" max="8" tat="90">B3</option>
                                                          </select>
                                          </div>
                             
                                    </div>
                                </div>


                                

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- modal-content -->
    </div>
    <!-- modal-dialog -->
</div>
<!-- modal -->


<script type="text/javascript">
setInterval(function () {
    if(window.localStorage.getItem("reservation_going_on")===null){
        if(window.localStorage.getItem("gridActiveTab") === null) {
            floorGridViewUpdate();
        }
    }
}, 10000);

setInterval(function () {
    if(window.localStorage.getItem("reservation_going_on")===null){
        if(window.localStorage.getItem("gridActiveTab") !== null) {
            if($('#grid-view').length === 1) {
                getUpdateGridView();
            }
        }
    }
}, 30000);

</script>

@endsection
 
