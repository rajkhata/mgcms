<!--Modal-->
<div class="modal right fade" id="customMessageModal" tabindex="-1" role="dialog" aria-labelledby="customMessageModalLabel" aria-hidden="true" data-backdrop="false" data-backdrop="static">
  <div class="modal-dialog customMessage-modal">
    <div class="modal-content">
      <!--Content -->

      <!--Popup Container-->
      <div class="custom__message-container">

        <div class="custom__message-header">
          <button type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></button>
          <p>Custom Message</p>
        </div>

        <div class="custom__message-textContainer">
          <textarea id="custom_sms" placeholder="Add your message here..." maxlength="300" rows="5"></textarea>
        </div>

        <div class="customMessage__btn-container">
          <a href="#" class="btn btn__primary " id="custom_sms_send">
            Send
          </a>
        </div>

      </div>

    </div>
  </div>
</div>
