<!-- Modal -->
	<div class="modal right fade" id="blockTables" tabindex="-1"  role="dialog" aria-labelledby="blockTablesModalLabel" data-backdrop="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content scroller full-content">

				<div class="modal-header" >
					<a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>				
				</div>
				<!--Popup Container-->
				<div class="modal-body">
					<div class="blocktable-content">
						<div class="heading">Block a Table</div>
					</div>

					<div class="border-sepration fullWidth margin-bottom-20"></div>
					<form id="table-block" class="validation-msg" autocomplete="off">

					<div class="row">
						<label class="error message_div fullWidth text-center position-static margin-top-30" style="display:none"></label>
					</div>

					<div class="blocktable-content">
						<!-- start block -->
						<div class="row input__fullWidth">
							<label class="title-label">Start Block</label>

							<div class="pull-right width-90">
								<input id="blockTables-start-timepicker" type="text" class="edit__field text-right timepickerInputWidth padding-right-30" value="" name="block_start_time" onkeypress="return false" autocomplete="off" onfocus="this.blur()">
								<i class="fa fa-angle-down" aria-hidden="true"></i>
							</div>
						</div>

						<!---time diffrence -->
						<div class="resumetimings hide">
							<div class="text-center arrow">
								<i class="fa fa-angle-up angle up" aria-hidden="true"></i>
							</div>

							<div class="time">
								<i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
								<span>00:00</span>
							</div>

							<div class="text-center arrow">
								<i class="fa fa-angle-down angle" aria-hidden="true"></i>
							</div>

						</div>

						<!-- resume blocking -->
						<div class="row input__fullWidth resume-booking-table">
							<label class="title-label">Resume Bookings</label>
							<div class="pull-right width-90">
								<input id="blockTables-resume-timepicker" type="text"
									class="edit__field text-right timepickerInputWidth padding-right-30 without-cursor"
									value="" name="block_resume_time" onkeypress="return false" autocomplete="off">
								<i class="fa fa-angle-down" aria-hidden="true"></i>
							</div>
						</div>

						<!--Floor-->
						<div class="check-avilability">
							<a  class="btn_ btn__primary check-availability__btn fullWidth" id="check_block_avail_button">Check Availability</a>
						</div>
						<div class="floor-table-block" style="display: none">
							<div class="row input__fullWidth fullWidth margin-bottom-0">
								<label class="title-label">Floor</label>
								<div class="selct-picker-plain fullWidth">
									<select class="selectpicker" data-size="4" data-dropdown-align-right="auto"
											data-style="text-left no-background" id="block_floor" name="block_floor">
										<option value="">Select Floor</option>
									</select>
								</div>
							</div>
							<!-- TABLE-->
							<div id="multipleTable" class="row input__fullWidth fullWidth">
								<label class="title-label">Table</label>
								<div class="selct-picker-plain fullWidth">
									<select class="selectpicker" data-size="4" data-dropdown-align-right="auto"
											data-style="text-left no-background" id="block_tables" name="block_tables[]" multiple>
										<option value="">Select Table</option>
									</select>
								</div>
							</div>
							<!-- button -->
							<div class="confirm_button">
								<a class="btn_ btn__primary check-availability__btn fullWidth confirm-block">
									Confirm Block
								</a>
							</div>
						</div>
					</div>
						<input id="current_time_slot_range" value="{{$current_time_slot_range}}" type="hidden">
					</form>
				</div>

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->
