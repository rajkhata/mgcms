  <link href="{{asset('css/multiple-emails.css')}}" rel="stylesheet" type="text/css">
  <script src="{{asset('js/multiple-emails.js')}}" type="text/javascript" charset="utf-8"></script>
    <!--    authentication modal--->
     <div class="modal fade modal-popup" id="authentication-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Please enter your password to complete the action</h4>
          </div>
          
          <div class="modal-body text-center desc">
            
            <form id="authentication-form" class="validation-msg" action="">
            <div class="row border-row border-botton-none">
                    <div class="col-sm-5 text-left">
                        <label for="">User Name</label>
                    </div>
                    <div class="col-sm-5 text-left">

                      <div class="input__field fullWidth">
                                <input class="" type="text" name="auth_name" id="auth_name" required>
                           
                            </div>

                      

                    </div>
                </div>
            <div class="row border-row border-botton-none ">
                    <div class="col-sm-5 text-left">
                        <label for="">Password</label>
                    </div>
                    <div class="col-sm-5 text-left">

                      <div class="input__field fullWidth">
                                <input class="required-entry" type="password" name="auth_password" id="auth_password" required>
                           
                            </div>

                      

                    </div>
                </div>
              </form>
          </div>
          <div class="modal-footer">
            <input type="submit" id="auth-ok" class="btn btn__primary btn-ok" value="ok">
            <button type="button" class="btn btn__cancel" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    
<!--    --invite modal---->
        <div class="modal fade modal-popup" id="invite-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Let Your friends Know About Your Culinary Discovery!</h4>
          </div>
          
          <div class="modal-body text-center desc">
            
            <form id="invite_form" class="validation-msg" action="">
                        <div class="row">
                         <div class="col-sm-12  no-padding text-left">
                         <label for="">Invite Friends to Join Your Reservation *</label>
                         <div><label class="sub-level_text" for="">Enter email address</label></div>
                        	<div class='form-group invite_emails_container email_tags'>
					<input type='text' name="invite_emails"  class='form-control invite_emails' value=''>
	
			
			</div>
                        </div>
                        
                           <div class="col-sm-12 margin-top-20 no-padding text-left">
                         <label for="">Include a Personalized Message </label>
                               <div><label class="sub-level_text" for="">To:</label></div>
                           <textarea name="personalized_msg" rows="4" class="personalized-msg"></textarea>
                            </div>
                </div>
              </form>
          </div>
          <div class="modal-footer">
            <input type="submit" id="invite_send" class="btn btn__primary btn-ok" value="Send">
            
          </div>
        </div>
      </div>
    </div>
    
<!-- Modal -->
<div class="modal  right fade book-a-table" data-backdrop="false" id="book-a-table" tabindex="-1" role="dialog" aria-labelledby="book-a-tableModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content scroller full-content">

            <div class="modal-header">
                <a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
            </div>

            <div class="right--popup-wrapper">

                <!--New book table Form-->
                <div class="new__waitlistForm-container border-sepration">

                    <div class="waitlistForm_header">
                        Book a Table
                    </div>


                    <div class="form_field__container">
                        <form id="customer_detail" autocomplete="off">
                            <div class="input__field pull-left">
                                <input type="text" name="fname" id="fname" class="fname_booking text-capitalize reservation-book" maxlength="20" />
                                <span for="fullName">First Name</span>
                            </div>
                            <div class="input__field">
                                <input type="text" name="lname" id="lname" class="text-capitalize" maxlength="20" />
                                <span for="lastName">Last Name</span>
                            </div>

                            <div class="input__field fullWidth">
                                <input type="text" name="phone" id="phone" pattern="[0-9]*" maxlength="10"/>
                                <span for="number">Mobile</span>
                            </div>
                            <div class="input__field fullWidth">
                                <input type="text" name="email" id="email" >
                                <span for="email">Email</span>
                            </div>
                            <div class="register_customer">
                                <label class="custom_checkbox relative">
                                    <input class="hide" type="checkbox" name="register_guest" id="register_guest" value="1">Register Guest
                                    <span class="control_indicator"></span>
                                </label>
                                <!-- <span class="font-weight-700">Register Guest</span>
                                <div class="toggle__container pull-right waitList__otp no-padding margin-top-10">
                                    <button type="button" class="btn btn-xs btn-secondary btn-toggle" name="register_guest" id="register_guest" value="" title="Open" data-toggle="button" aria-pressed="false" autocomplete="off">
                                        <div class="handle"></div>
                                    </button>
                                </div> -->
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="text-center add_view" style="display: none;">
                                <a class="add_view_check" id="add-view-guest" href="#">Add / View Guest Details</a>
                            </div>

                            <div class="add-view-guest-list">

                                <ul class="nav nav-tabs add-view-gues-tabs">
                                    <li class="active"><a data-toggle="tab" href="#person"><i class="icon-user" aria-hidden="true"></i></a></li>
                                    <li id="clock_li"><a data-toggle="tab" href="#clock" id="reservation_history"><i class="icon-anti_clockwisetimer" aria-hidden="true"></i></a></li>

                                </ul>

                                <div class="tab-content">
                                    <div id="person" class="tab-pane fade in active">
                                        <div class="two-in-one-row">
                                            <div class="row input__fullWidth">
                                                <label for="">Orders</label>
                                                <div class="input__field ">
                                                    <input type="number" name="" id="total_orders" placeholder="0" value="0" readonly>
                                                </div>
                                            </div>

                                            <div class="row input__fullWidth">
                                                <label for="">Reservations</label>
                                                <div class="input__field ">
                                                    <input type="number" name="" id="total_resv_count"  value="0" placeholder="0" readonly>
                                                </div>
                                            </div>

                                            <div class="row input__fullWidth">
                                                <label for="">Walk-ins</label>
                                                <div class="input__field ">
                                                    <input type="number" name="" id="total_walk_in_count" value="0" placeholder="0" readonly>
                                                </div>
                                            </div>

                                            <div class="row input__fullWidth">
                                                <label for="">Cancellations</label>
                                                <div class="input__field ">
                                                    <input type="number" name="" id="total_cancelled_count" value="0" placeholder="0" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row input__fullWidth">

                                            <label class="tag" for="">Tag</label>
                                            <div class="tag-plus icon">
                                                <div class="tags_div">



                                                </div>
                                                <!--                                                <i class="fa fa-plus" aria-hidden="true"></i>-->
                                                <form id="tag_form" >
                                                    <ul id="guest_tags_bookings "></ul>
                                                    <input type="text" placeholder="Please select the existing tags" name="tags" id="guest_tags_booking">
                                                </form>

                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="fullWidth bookatablenote guest_note">
                                            <!-- <input type="text" name="guest_note" id="guest_note" > -->
                                            <span for="guest_note">Guest Notes</span>
                                            <textarea class="edit-field" wrap="hard" name="guest_note" rows="1" maxlength="300" id="guest_note"></textarea>

                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row input__fullWidth">
                                            <span class="spcl_dates" for="">Special Dates</span>
                                            <label for="">Birthday</label>
                                            <div class="input__field ">
                                                <input type="text" class="text-center dob" id="dob" name="dob" readonly="readonly" placeholder="">
                                                <div class="icon-calendar" aria-hidden="true"></div>
                                            </div>
                                        </div>

                                        <div class="row input__fullWidth">
                                            <span class="spcl_dates" for="">Special Dates</span>
                                            <label for="">Anniversary</label>
                                            <div class="input__field ">
                                                <input type="text" class="text-center doa" id="doa" name="doa" readonly="readonly" placeholder="">
                                                <div class="icon-calendar" aria-hidden="true"></div>
                                            </div>
                                        </div>


                                    </div>

                                    <div id="clock" class="tab-pane fade">

                                        <ul class="clock--tab oldreservations_bookinform">



                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>



            <form id="reservation-booking" class="validation-msg"  autocomplete="off">
                <div class="form_field__container border-sepration">
                    <div class="row b_check_availability_change hide">
                        <div class="reservation-text b_reservation_text fullWidth"></div>
                        <div class="row input__fullWidth near_by_slots edit-input">
                            <ul class="time--buttons">
                            </ul>
                        </div>
                        <div class="change-Date text-center b_change_date"><a href="javascript:void(0)"><span class="icon-calendar text-color" aria-hidden="true"></span>&nbsp;&nbsp;Change Date</a></div>
                    </div>
                    <div class="row">
                        <label class="error message_div fullWidth text-center position-static margin-top-30" style="display:none"></label>
                    </div>
                    <div class="row b_date_time_party">
                        <div class="row input__fullWidth">
                            <label for="">Date</label>
                            <div><input class="input__date start__date bore-none reservation-book text-center block" type="text" id="reservation_date" name="reservation_date" readonly="readonly" placeholder="" required="">

                                <div class="icon-calendar" aria-hidden="true"></div>
                            </div>
                        </div>
                        <!-- Waiting Time-->
                        <div class=" row input__fullWidth">
                            <label class="">
                                Time
                            </label>
                            <div class="pull-right text-right">

                                <input type="text" class="edit__field text-right padding-right-30 timepicker reservation-book text-uppercase" value="" name="reservation_time" id="book-a-table-tp" onkeypress="return false" onfocus="this.blur()" />

                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </div>
                        </div>
                        <!--Party-size-->
                        <div class=" row input__fullWidth">
                            <label class="">
                                Party Size
                            </label>
                            <div class="edit__field">
                                <!--Value Box-->
                                <div class="number__box">
                                    <div class="input-group">
                        <span class="input-group-btn">
                          <button data-type="minus" data-field="party_size" type="button" class="btn btn-danger btn-number minus-btn">
                          <i class="fa fa-minus" aria-hidden="true"></i>
                        </button>
                        </span>
                                        <input type="text" name="party_size" class="form-control input-number party_size reservation-book turnovertime_input" min="1" max="30" value="1" onfocus="this.blur()" readonly="readonly">
                                        <span class="input-group-btn">
                            <button type="button" class="btn btn-success btn-number plus-btn" data-type="plus" data-field="party_size">
                          <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form_field__container border-sepration floor_table edit-input">
                    <div class="row input__fullWidth">
                        <label for="">Floor</label>
                        <div id="currentFloorName" class="selct-picker-plain">
                            <select class="selectpicker reservation-book" data-dropdown-align-right="auto" data-size="5" name="floor_id" id="floors">
                                <option value="">Select Floor</option>
                            </select>
                        </div>
                    </div>
                    <!-- Waiting Time-->
                    <div class=" row input__fullWidth">
                        <label class="">
                            Table
                        </label>
                        <div id="multipleTableSelectpicker" class="selct-picker-plain">
                            <select class="selectpicker reservation-book" data-dropdown-align-right="auto" data-size="5" name="table_id[]" id="tables" multiple>
                                <option value="">Select Table</option>
                            </select>
                        </div>
                    </div>
                    <!--Party-size-->
                    <div class=" row input__fullWidth">
                        <label class="">
                            Special Occasion
                        </label>
                        <div class="selct-picker-plain">
                            <select class="selectpicker reservation-book" name="special_occasion" data-size="5">
                                <option value="">Select</option>
                                @foreach($all_special_occasion as $special_occasion_slug=>$special_occasion)
                                    <option value="{{$special_occasion_slug}}">{{$special_occasion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class=" row input__fullWidth">
                        <span class="input__field fullWidth">
                            <input type="text" name="special_instruction" id="special_instruction" class="reservation-book">
                            <span for="text">Special Instruction</span>
                        </span>
                    </div>

                    <!--Add Instructions box-->
                    <div class="add__instruction">
                        <div class="form_field__container no-padding">
                            <div class="row input__fullWidth host_name edit-input">
                                <label for="">Host</label>
                                <div class="selct-picker-plain"><select class="selectpicker reservation-book dropup" data-dropup-auto="false" name="host_name" @if(app('Modules\Reservation\Http\Controllers\GlobalSettingsController')->isHostNameRequired()) {{"required"}} @endif >
                                        <option value="">Select</option>
                                        @foreach($hosts as $host_id=>$host)
                                            <option value="{{$host_id}}">{{$host}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="source" value="Offline">
                <div class="form_field__container newWaitlist__btn-container rem-btn-border">
                    <input type="hidden" name="yield_cancelled" id="yield_cancelled" value="0"/>
                    <button type="button" class="btn_ btn__primary check-availability__btn fullWidth"
                            id="check_avail_button" button-name="Check Availability">
                        Check Availability
                    </button>
                    <button type="button" class="btn_ btn__primary check-availability__btn fullWidth edit-input"
                            id="book_button" button-name="Create Reservation">
                        Create Reservation
                    </button>
                    <input type="hidden" name="user_id" id="user_id">
                </div>
                <!--Button Container-->
            </form>
        </div>
    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{asset('css/reservation/tags/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/reservation/tags/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/tags/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>
<style> .ui-front { z-index: 10000!important;}</style>

<script type="text/javascript">

    /*
     var availableTags=<?php echo  json_encode($tags)?>;
            $('#guest_tags_booking').tagit({
                itemName: 'item',
                fieldName: 'tags[]',
                autocomplete: {delay: 0, minLength: 2},
                availableTags     :availableTags,
                beforeTagAdded: function(event, ui) {
                    if($.inArray(ui.tagLabel, availableTags)==-1) return false;
               }

            });
            */

    var availableTags=<?php echo  json_encode($tags)?>;

    $('#guest_tags_booking').tagit({
        //itemName: 'item',
        // fieldName: 'tags[]',
        //autocomplete: {delay: 0, minLength: 2},
        allowDuplicates:false,
        availableTags     :availableTags,
        placeholderText : "Please select the existing tags",
        beforeTagAdded: function(event, ui) {
            $("#guest_tags_booking").find('.ui-autocomplete-input').val('');
            if($.inArray(ui.tagLabel, availableTags)==-1) return false;
        }

    });

</script>
