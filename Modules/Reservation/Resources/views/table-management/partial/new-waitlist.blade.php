


<!-- Modal -->
	<div class="modal right fade newWaitlistModal " data-backdrop="false" id="newWaitlistModal" tabindex="-1" role="dialog" aria-labelledby="newWaitlistModalLabel">
        <form id="waitlist_no_tables_form" class="validation-msg" autocomplete="off" >
		<div class="modal-dialog" role="document">
			<div class="modal-content scroller full-content">

				<div class="modal-header">
					<a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
					
				</div>

				<div class="modal-body ">
					<div class="new__waitlist-wrapper">

        <!--New Waitlist Form-->
        <div class="new__waitlistForm-container fullWidth">

          <div class="waitlistForm_header fullWidth">
           New Waitlist - 
           <h5>No Tables Available</h5>
          </div>

          <div class="form_field__container fullWidth">
              <div class="two-in-oneflex">
            <div class="input__field">
              <input type="hidden" name="userid" id="userid" />
              <input type="text" name="firstName" id="fname" class="fname_booking" />
                <span for="firstName">First Name<i class="colorRed">*</i></span>
            </div>

            <div class="input__field">
              <input type="text" name="lastName" id="lname" />
              <span for="lastName">Last Name</span>
            </div>

              </div>

            <div class="input__field fullWidth">
              <input type="email" name="email" id="email" required />
                <span for="email">Email<i class="colorRed">*</i></span>
            </div>

            <div class="input__field fullWidth">
              <input type="text" name="phoneNo" maxlength="10" id="phone" pattern="[0-9]*" required />
                <span for="number">Mobile<i class="colorRed">*</i></span>
                <input type="hidden" name="walk_in" value="1" />
            </div>
              <div class="register_customer">
                  <label class="custom_checkbox relative">
                      <input class="hide" type="checkbox" name="register_guest" id="register_customer" value="1"> Register Guest
                      <span class="control_indicator"></span>
                  </label>
                  <!-- <span class="font-weight-700">Register Guest</span>
                    <div class="toggle__container pull-right waitList__otp no-padding margin-top-10">
                        <button type="button" class="btn btn-xs btn-secondary btn-toggle" name="register_guest" id="register_guest" value="" title="Open" data-toggle="button" aria-pressed="false" autocomplete="off">
                            <div class="handle"></div>
                        </button>
                    </div> -->
              </div>

          </div>

          

        </div>

        <!--Manage Waitlist Pane-->
        <div class="newWaitlistManage-container">


          <!--Waiting Time / Party Size-->
          <div class="edit__details">
            <!--Party-size-->
            <div class="edit__detail">
              <div class="edit__head">
                Party Size
              </div>
              <div class="edit__field">
                <!--Value Box-->
                <div class="number__box">
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button data-type="minus" data-field="party_size" type="button" class="btn btn-danger minus-btn">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    </span>
                    <input type="text" name="party_size" class="form-control party_size" diff="1" min="1" max="30" value="1" onfocus="this.blur()" readonly >
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-success plus-btn" data-type="plus" data-field="party_size">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <!-- Waiting Time-->
            <div class="edit__detail">
              <div class="edit__head">
                Waiting Time
              </div>
              <!-- <input type="text" class="edit__field time timepicker" value="20min" name="start_time_1" id="start_time_1" \=""> -->
              <div class="edit__field">
                <!--Value Box-->
                <div class="number__box">
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button data-type="minus" data-field="wait_time" type="button" class="btn btn-danger  minus-btn">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    </span>
                    <input type="text" name="wait_time" class="form-control  wait_time text-center" min="5" max="120" value="5"  diff="5" value="5" onfocus="this.blur()" readonly>
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-success  plus-btn" data-type="plus" data-field="wait_time">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    </span>
                  </div>
                </div>
              </div>
           </div>


          </div>


        </div>
        
        <div class="waitList__otp">
            <p>Send SMS notification on status of wait</p>
            <div class="toggle__container">

              <button type="button" id="sms_notification_button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom active" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                <div class="handle"></div>
              </button>
                <input type="hidden" id="sms_notification" value="1" name="sms_notification">
            </div>

          </div>

       <div>

           
           
            <!--Add Instructions box-->
            <div class="add__instruction">
              <div class="form_field__container">

                <div class="input__field">
                  <input type="text" name="instruction" id="instruction" />
                  <span for="instruction">Add instructions here...</span>
                </div>

                <div class="newWaitlist__btn-container">
                <button type="button" class="btn_ btn__primary check-availability__btn waitlist__btn fullWidth waitlist_no_tables" button-name="Save & Close">
                    Save & Close
                </button>
              {{--<a href="#" class="btn_ btn__primary waitlist__btn fullWidth waitlist_no_tables">
                Save & Close
</a>--}}

            </div>
             
              </div>
            </div>

            <!--Button Container-->

         

          </div>
     
     
      </div>
				</div>

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
        </form>
	</div><!-- modal -->
	


