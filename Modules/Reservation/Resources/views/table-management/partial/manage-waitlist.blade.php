

<!-- Modal -->
	<div class="modal right fade manageWaitlistModal" data-backdrop="false" id="manageWaitlistModal" tabindex="-1" role="dialog" aria-labelledby="manageWaitlistModalLabel" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content  scroller full-content">

				<div class="modal-header">
				<a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
				
				</div>

				<div class="modal-body ">
					 <!--Popup Container-->
           
      <div class="new__waitlist-wrapper">
          <form id="waitlist_popup" class="validation-msg">
        <!--New Waitlist Form-->
        <div class="new__waitlistForm-container fullWidth margin-bottom-20">

        <div class="waitlist-edit">
                  <button type="button" class="edit_guest_details"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                  <button type="button" class="update_guest_details update_guest_details-third edit-input"><i class="fa fa-check" aria-hidden="true"></i></button>
                  <button type="button" class="cancel_edit_guest_details cancel_edit_guest_details-third edit-input"><i class="fa fa-times" aria-hidden="true"></i></button>
              </div>
          <div class="waitlistForm_header">
            Manage Waitlist
          </div>
         
          <div class="manage__user_details">
           <div class="input__field pull-left">
              <input type="text" name="firstName" id="fname" class="fname_booking"/>
              <span for="fullName">First Name</span>
               <input type="hidden" name="reservation_id" id="reservation_id" value=""/>
               <input type="hidden" name="userid" id="userid" value="" />
            </div>

            <div class="input__field">
              <input type="text" name="lastName" id="lname" />
              <span for="lastName">Last Name</span>
            </div>



            <div class="input__field fullWidth">
              <input type="email" name="email" id="email" required="">
              <span for="email">Email</span>
            </div>

            <div class="input__field fullWidth">
              <input type="text" name="phoneNo" id="phone" required="" pattern="[0-9]*" maxlength="10">
              <span for="number">Mobile</span>
            </div>

          <div class="register_customer">
              <label class="custom_checkbox relative">
                  <input class="hide" type="checkbox" id="register_customer" name="register_customer" value="1">Register as customer
                  <span class="control_indicator"></span>
              </label>
              <!-- <span class="font-weight-700">Register Guest</span>
              <div class="toggle__container pull-right waitList__otp no-padding margin-top-10">
                  <button type="button" class="btn btn-xs btn-secondary btn-toggle" name="register_guest" id="register_guest" value="" title="Open" data-toggle="button" aria-pressed="false" autocomplete="off">
                      <div class="handle"></div>
                  </button>
              </div> -->
          </div>

            <div class="row input__fullWidth fullWidth">
              <label for="">Floor</label>
              <div class="selct-picker-plain">
                  <select class="selectpicker" data-dropdown-align-right="auto" id="floor_plan_walkin" name="floor_plan_walkin">
                      <option value="">Select Floor</option>
                  </select>
              </div>
            </div>

            <div class="row input__fullWidth fullWidth">
              <label for="">Table</label>
              <div id="multipleTableSelectpicker" class="selct-picker-plain">
                  <select data-dropdown-align-right="auto" id="table_plan_walkin" name="table_plan_walkin" class="selectpicker reservation-book"     multiple>
                      <option value="">Select Table</option>
                  </select>
              </div>
            </div>
          </div>


        </div>
              <div class="text-center errors margin-bottom-30" id="b_warning_message"></div>
        <div class="seated-option">
          <select id="waitlist_status" data-style="status-waitlist" name="status" class="dropdown__text_ul selectpicker show-tick">

          </select>
        </div>
        <!--Manage Waitlist Pane-->
        <div class="manageWaitlistManage-container">

          <!--Waiting Time / Party Size-->
          <div class="edit__details">
             <!--Party-size-->
             <div class="edit__detail">
              <div class="edit__head">
                Party Size
              </div>
              <div class="edit__field">
                <!--Value Box-->
                <div class="number__box">
                  <div class="input-group">
                  <span class="input-group-btn hide">
                      <button data-type="minus" data-field="party_size" type="button" class="btn btn-danger minus-btn">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    </span>
                    <input type="text" name="party_size" id="waitlist_party_size" class="form-control input-number party_size turnovertime_input"  diff="1" min="1" max="30" value="2" >
                    <span class="input-group-btn hide">
                        <button type="button" class="btn btn-success plus-btn" data-type="plus" data-field="party_size">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <!-- Waiting Time-->
            <div class="edit__detail">
              <div class="edit__head">
                Waiting Time
              </div>
              <!-- <input type="text" class="edit__field time timepicker" value="20min" name="start_time_1" id="start_time_1" \=""> -->
              <div class="edit__field">
                <!--Value Box-->
                <div class="number__box">
                  <div class="input-group">
                    <span class="input-group-btn hide">
                      <button data-type="minus" data-field="wait_time" type="button" class="btn btn-danger minus-btn">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    </span>
                    <input type="text" name="wait_time" id="waitlist_wait_time" class="form-control input-number wait_time text-center turnovertime_input" diff="5" min="1" max="120" value="5">
                    <span class="input-group-btn hide">
                        <button type="button" class="btn btn-success plus-btn" data-type="plus" data-field="wait_time">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    </span>
                  </div>
                </div>
              </div>
           </div>

          </div>

      
    

        </div>
        
            <div class="waitList__otp">
            <p>Send SMS notification on status of wait</p>
            <div class="toggle__container">

              <button type="button" id="sms_notification_button" name="sms_notification_button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom active" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                <div class="handle"></div>
              </button>
                <input type="hidden" id="sms_notification" value="1" name="sms_notification">
            </div>

          </div>
          <!--Special Instructions-->
          <div class="special__otherInformation">
            <!-- <p>
              "<span>First date so it needs to be a more intimate table, near the window, if possible</span>"
            </p> -->
            <div class="guest-note">
              <div class="waitlist-edit-note pull-right">
                  {{--<button type="button" class="edit_guest_details"><i class="fa fa-pencil" aria-hidden="true"></i></button>--}}
                  <button type="button" class="update_guest_details update_guest_details-third edit-input"><i class="fa fa-check" aria-hidden="true"></i></button>
                  <button type="button" class="cancel_edit_guest_details cancel_edit_guest_details-third edit-input"><i class="fa fa-times" aria-hidden="true"></i></button>
              </div>
              <h3>
              
              <textarea class="edit-field autoExpand" wrap="hard" name="instruction" data-min-rows="2" rows="2" id="instruction"></textarea>
              
            </h3>
            </div>
          </div>
          </form>
                <!--Button Container-->

          <div class="manageWaitlist__btn-container">
            <a class="btn_ btn__primary manageWaitlist__btn fullWidth" id="template_sms_send">
              Table Ready
            </a>

<!--
            <div class="btn_ btn__primary manageWaitlist__btn">
              More Delay
            </div>
-->

            <a class="btn_ btn__primary manageWaitlist__btn border-btn-prime  fullWidth" data-toggle="modal" data-target="#customMessageModal">
              Custom Message
            </a>

          </div>


      </div>
				</div>

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->
	
@include("reservation::table-management.partial.custom-message")
<script>


</script>