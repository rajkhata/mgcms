<!-- Modal -->
<div class="modal right fade show-popup" id="show-popup" tabindex="-1" role="dialog" aria-labelledby="show-popupLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content  scroller full-content">

            <div class="modal-header">
                <a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
            </div>

            <div class="modal-body">
                <!--Popup Container-->
                <div class="show-timer-container">
                    <div class="reserved__timer_header">
                          The Guest is late by : <span class="timer_minute"> 01:20</span> min
                      </div>
                      <div class="timer_cta">

                          <a href="#" class="timer_noShow">
                              <span class="noShow__icon icon-cancelled" aria-hidden="true"></span>
                              <span class="cta__content">Mark as<br />No Show</span>
                          </a>

                          <a href="#" class="timer_wait">
                              <span class="wait__icon icon-anti_clockwisetimer" aria-hidden="true"></span>
                              <span class="cta__content">Wait another<br />30 min</span>
                          </a>

                      </div>                    
                </div>

            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
