<!-- Modal -->

	<div class="modal right fade newWalkinModal" id="newWalkinModal" tabindex="-1" role="dialog" aria-labelledby="newWalkinModalLabel" data-backdrop="false">

		<div class="modal-dialog" role="document">
			<div class="modal-content full-content">

				<div class="modal-header">
					<a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
				
				</div>

				<div class="modal-body ">
                    <input type="hidden" name="table_id" id="table_id" class="form-control input-number table_id" value="0">
                    <input type="hidden" name="floor_id" id="floor_id" class="form-control input-number floor_id" value="0">
				  <!--Popup Container-->
      <div class="newWalkin__content">

        <div class="walkin__header">
          <p class="newWalkin_heading">
            New Walk-in
          </p>
        </div>

        <div class="walkin__content__cbc">
          <!--First Step-->
          <div class="walkIn-party__size first__step-walkin">

            <div class="walkIn__partysize-text">
              Party Sizes
            </div>

            <div class="walkIn__slider">

              <div class="number__box">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button data-type="minus" data-field="party_size" type="button" class="btn btn-danger minus-btn">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                  </span>
                  <input type="text" name="walkInparty_size" id="party_size" class="form-control party_size" diff="1" min="1" max="30" value="5" onfocus="this.blur()" readonly>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-success  plus-btn" data-type="plus" data-field="party_size">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                  </span>
                </div>
              </div>

            </div>

          </div>
          <label class="hide error max--cover message_div" style="display:none"></label>

          <!--Second Step-->
          <div class="second__step-walkin">
            <div class="seat__plan">
                <form id="walkin_floor_table_form">
              <div class="table_plan-walkin">
                <div>
                  <select id="floor_plan_walkin" data-width="100%"  data-style="simple__selectpicker" name="walkin_floor" class="selectpicker required" data-width="fit">
                    <option value="">Select Floor</option>
                  </select>
                </div>
                <div class="table__text hide">
                  Table ID
                </div>
                <div class="table__selectContainer">
                  <!-- <select class="table_plan_walkin">

                  </select> -->
                  <div >
                  Table ID
                                </div>
                  <div id="multipleTableSelectpicker" class="selct-picker-plain">
                    {{--<select id="table_plan_walkin" data-width="fit" data-style="simple__selectpicker" name="walkin_table" class="selectpicker required" data-width="fit">--}}
                    <select id="table_plan_walkin" class="selectpicker reservation-book" data-dropdown-align-right="auto" name="walkin_table" multiple>
                      <option value="">Select Table</option>
                    </select>
                  </div>
                            </div>
                </div>
                </form>
              </div>
                <!--add Customer details-->
                <div class="add_customer_details__walkin">
                  <span>+</span>Add Customer Details
                </div>
            <div class="text-center errors" id="b_warning_message"></div>
            </div>

          </div>

          </div>
          <!--Third Step-->
          <form name="third_step_walkin" class="validation-msg" id="third_step_walkin" autocomplete="off" >
            <div class="third__step-walkin">

              <div class="form_field__container">
                <div class="input__field pull-left">
                <input type="hidden" name="userid" id="userid" />
                  <input type="text" name="firstname" id="fname" class="fname_booking reservation-book" />
                  <span for="firstName">First Name*</span>
                </div>
                <div class="input__field">
                  <input type="text" name="lastname" id="lname" />
                  <span for="lastName">Last Name</span>
                </div>
              </div>

              <div class="form_field__container cust-spacing ">
                <div class="input__field fullWidth">
                  <input type="text" name="phoneNo" id="phone" pattern="[0-9]*" maxlength="10"/>
                  <span for="number">Mobile*</span>
                </div>
                <div class="input__field fullWidth">
                  <input type="email" name="email" id="email" />
                  <span for="email">Email*</span>
                </div>
              </div>

                <div class="register_customer">
                  <label class="custom_checkbox relative">
                      <input class="hide" type="checkbox" id="register_customer" name="register_customer" value="1">Register as customer
                      <span class="control_indicator"></span>
                  </label>
                </div>
             
              
            </div>
            <input type="hidden" name="source" value="Walk In">
          </form>

          <!--Avail-->
          <div class="check__availability">

            <a class="btn__checkwalkIn check__avail_btn fullWidth availability">
              Check Availability
            </a>

          </div>
          <!--Seat-->
          <div class="seat__avail ">
            <a href="#" class="btn__checkwalkIn seat_btn fullWidth">
              Seat
</a>
          </div>

          <!--Seat-->
          <div class="seat__done">
           
               <a href="#" class="btn__checkwalkIn done_btn fullWidth">Done</a>
           
          </div>

        </div>


      </div>
				</div>

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->