@extends('layouts.app')
@section('content')
    <div class="main__container col-sm-12 max-fullWidth">
        <div class="create-floor__wrapper col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">


            <img src="{{asset('images/createfloor_bg.jpg')}}" class="create-floor__media" alt="create floor background image">
            
            <div class="create-floor__container">
                <div class="create-floor__content-container">
                    <h1 class="text-center">Create New {{ucfirst($type)}}</h1>
                    <form id="add-floor" class="validation-msg" method="post" autocomplete="off">
                            @csrf

                        <div class="form-group input__field relative margin-top-30 margin-bottom-0">
                            <i class="fa fa-check-circle greenColor hide" aria-hidden="true"></i>
                           <input type="text" maxlength="25" id="name" name="name"  floortype="{{ucfirst($type)}}"class="input-control noSpecialChars padding-right-25">
                            <span class="form-control-placeholder" for="name">{{ucfirst($type)}} Name</span>
                        </div>
                        <div class="text-center add-floor"><button disabled="disabled" class="btn btn__primary">Add & Configure {{ucfirst($type)}}</button></div>
                    </form>

                    @include('reservation::common.message')

                </div> 

            </div>

        </div>
    </div>

    <script>
        var floor_list = '<?=json_encode($floors);?>';

        function getArrayIndexOf(array, value ) {
            var l = array.length;
            for (var k = 0; k < l; k++) {
                if (array[k].name == value) {
                    return true;
                }
            }
            return false;
        }

        //setup before functions
        var typingTimer;                //timer identifier
        var uniqueFloorNameInterval = 1000;  //time in ms, 5 second for example
        var $input = $("#add-floor #name");

        //on keyup, start the countdown
        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(uniqueFloorName, uniqueFloorNameInterval);
        });
        // paste
        $input.bind('paste', function() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(uniqueFloorName, uniqueFloorNameInterval);
        })
        //cut
        $input.bind('cut', function() {
            typingTimer = setTimeout(uniqueFloorName, uniqueFloorNameInterval);
        });
        
        //on keydown, clear the countdown
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });
        //cut
        $input.bind('cut', function() {
            typingTimer = setTimeout(uniqueFloorName, uniqueFloorNameInterval);
        });
        function uniqueFloorName(){
            //do something
            if($("#add-floor").valid() && getArrayIndexOf(JSON.parse(floor_list),$input.val())){
                $input.parents("#add-floor").find('i.fa-check-circle').addClass('hide');
                $( "<label for='name' class='error'>Please use another name.</label>" ).insertAfter(".form-group");
                $input.parents('#add-floor').find('button.btn').attr('disabled','disabled');
            } else {
                if(($input.val() == "") || (!$("#add-floor").valid())){
                    $input.parents('#add-floor').find('i.fa-check-circle').addClass('hide');
                    $input.parents('#add-floor').find('button.btn').attr('disabled','disabled');
                } else {
                    $input.parents('#add-floor').find('i.fa-check-circle').removeClass('hide')
                    $input.parents('#add-floor').find('button.btn').removeAttr('disabled');
                }
            }
        }

    </script>

@endsection
