@extends('layouts.app')
@section('content')
<script>

  var floor_and_tables={!! $floor_data !!};

    window.floor_list = '<?=json_encode($floors);?>';

</script>
<div id="tableManagement__wrapper" class="main__container floor-tables-management">
<div class="side-bar-open hidden no-margin-left"><i class="fa fa-bars" aria-hidden="true"></i></div>
  <div title="Reservation Status" class="open_rightBar hide"><span class="fa fa-angle-left" aria-hidden="true"></span></div>

  <div id="alert" class="alert hide max-fullWidth margin-top-40">
      <strong>Alert!</strong> <span id="msgTableOutside" class="hide"></span> <span id="msgTableOverlap" class="hide"></span> <span>Please reposition to save the floor plan.</span>
  </div>

  <div id="layout_area" class="pull-left hide">
    <div class="col-xs-12 text-center margin-top-30 margin-bottom-30">
      <button id="saveLayout" onclick="saveLayout();" class="btn btn__primary hide">Save Floor Plan</button>
      <a id="resetLayout" href="javascript:resetLayout();" class="btn btn__primary hide">Reset</a>
      <a id="cancelLayout" href="/floor" class="btn btn__holo hide">Cancel</a>
      <a id="undoLayout" href="javascript:void(0);" onclick="window.location.reload();return false" class="btn btn__primary hide">Undo</a>
    </div>
    <div class="fullWidth">
      <div id="layout" class="pull-left"></div>
    </div>
    
  </div>
 

</div>
@endsection

@section('side_bar_right')

<div class="sidebar_right__content floor-tables-sidebar">
    <div class="sideBar__heading fullWidth">
        <div class="row">
            <a type="button" class="close fa fa-angle-right floor-open no-margin-top margin-bottom-10 popupclose" data-dismiss="modal" aria-label="Close"></a>
        </div>
    </div>
        <div class="side-section">

            <div title="Reservation Status" class="open_rightBar"><span class="fa icon-open-sidebar floor-close" aria-hidden="true"></span></div>

            <ul>
                <li><a href="javascript:void(0);">
                        <span class="icon-seated"></span>
                        <p class="font-size-12">Layout</p>

                    </a>
                </li>
            </ul>
        </div>

  <div class="floor-configration__title">
  
    <form id="floorNameForm" class="relative">
      <i class="fa fa-check-circle greenColor hide" aria-hidden="true"></i>
      <input type="text" maxlength="25" name="floorName" id="floorName" data-floorId="{{$floor->id}}" data-floorName="{{$floor->name}}" value="{{$floor->name}}" placeholder="Floor Name" class="noSpecialChars padding-right-25"  />
    </form>
  </div>
<div id="floorCreateProperties" class=" fullWidth scroller">
   <div class="panel-group fullWidth" id="accordion">
    <div class="panel panel-default floor-configration__bg fullWidth">
      <div class="panel-heading fullWidth" >
        <h4 class="panel-title @if($floor->completed_step==2)collapsed @endif" data-toggle="collapse" data-parent="#accordion" href="#collapse1">Floor Layout <small></small></h4>
      </div>
      <div id="collapse1" class="panel-collapse fullWidth collapse @if($floor->completed_step==1)in @endif">
        <div class="panel-body">
          <ul id="floor-configration__layout">
            <li class="@if($floor->completed_step==2 && $floor->layout=='square')active @endif">
              <a href="javascript:void(0)" data-floorLayout="Square" data-minColumn="5" data-minRow="5" >
                <span class="floor_shape"><span class="custom-icons icon-square"></span></span>
                <span class="floor-type-name">Square</span>
              </a>
            </li>
            <li class="@if($floor->completed_step==2 && $floor->layout=='rectangle')active @endif">
              <a href="javascript:void(0)" data-floorLayout="Rectangle" data-minColumn="7" data-minRow="5" >
                <span class="floor_shape"><span class="custom-icons icon-rectangle"></span></span>
                <span class="floor-type-name">Rectangle</span>
              </a>
            </li>
            <li class="@if($floor->completed_step==2 && $floor->layout=='round')active @endif">
              <a href="javascript:void(0)" data-floorLayout="Round" data-minColumn="5" data-minRow="5">
                <span class="floor_shape"><span class="custom-icons icon-circle"></span></span>
                <span class="floor-type-name">Round</span>
              </a>
            </li>
            <li>
              <a href="javascript:void(0)" data-floorLayout="Custom Shape" class="hide">
                <span class="floor_shape"><img src="{{asset('images/custom-shape.png')}}" ></span>
                <span class="floor-type-name">Custom Shape</span>
              </a>
            </li>
          </ul>

          <div id="floor_property" class="col-sm-12 rightbar__property__wrapper hide">

            <div class="rightbar__heading">Floor Properties</div>
            <div class="rightbar__subHeading hide">Square</div>

            <div class="calc__row column-controler"  data-shape="column">
              <div class="calc__row__title">Column</div>
              <div class="calc__row__area">
                <button type="button" class="size-dec-btn column-dec" value="−"><i class="fa fa-minus" aria-hidden="true"></i></button>
                <input type="text" class="text-center" name="quantity" readonly value="5" maxlength="3" data-diff="1" data-min="5" data-max="10" size="1">
                <button type="button" class="size-inc-btn" value="+"><i class="fa fa-plus" aria-hidden="true"></i></button>
              </div>
            </div>

            <div class="calc__row row-controler" data-shape="row">
              <div class="calc__row__title">Row</div>
              <div class="calc__row__area">
                <button type="button" class="size-dec-btn row-dec" value="−"><i class="fa fa-minus" aria-hidden="true"></i></button>
                <input type="text" class="text-center" name="quantity" readonly value="5" maxlength="1" data-diff="1" data-min="5" data-max="10" data-max="1000" size="1">
                <button type="button" class="size-inc-btn" value="+"><i class="fa fa-plus" aria-hidden="true"></i></button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="panel panel-default floor-configration__bg fullWidth @if($floor->completed_step==1)hide @endif ">
      <div class="panel-heading fullWidth">
        <h4 class="panel-title @if($floor->completed_step!=2)collapsed @endif" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" href="#collapse2">Tables <small></small></h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse fullWidth  @if($floor->completed_step==2)in @endif">
       <div class="tables-scroller">
        <div class="panel-body table__type-tab">
        <ul class="nav nav-pills">
          <li class="active">
              <a data-toggle="pill" data-minGuest="1" data-maxGuest="10" data-tableType="Round" href="#circle">
                  <span class="custom-icons icon-circle"></span>
              </a>
          </li>
          <li>
              <a data-toggle="pill" data-minGuest="1" data-maxGuest="4" data-tableType="Square" href="#circle">
                  <span class="custom-icons icon-square"></span>
              </a>
          </li>
          <li>
              <a data-toggle="pill" data-minGuest="1" data-maxGuest="10" data-tableType="Rectangle" href="#circle">
                  <span class="custom-icons icon-rectangle"></span>
              </a>
          </li>
         </ul>
  
        <div class="tab-content">
        <div id="circle" class="tab-pane active">
          <div class="rightbar__property__wrapper">
            <div class="rightbar__heading">Tables Properties</div>
            <div class="rightbar__subHeading hide"><span>Round</span> Table</div>
          <form id="circleTableProperties" class="validation-msg" autocomplete="off">
            <div class="table__id margin-top-30">
              <div class="table__id-value input__field relative">
                <input type="text" id="tableId" class="text-uppercase" autocomplete="off" onkeyup="this.value = this.value.toUpperCase();" maxlength="3" name="tableId" />
                <span>Table ID</span>
              </div>
          </div>

          

          <div class="calc__row margin-top-10">
            <div class="guest__title">Min Guests</div>
            <div class="guest__inc_dec">
              <button type="button" class="size-dec-btn-cmn" value="−"><i class="fa fa-minus" aria-hidden="true"></i></button>
              <input type="text" class="minGuest text-center" readonly name="quantity" value="1" data-diff="1" data-min="1" data-max="10" size="1">
              <button type="button" class="size-inc-btn-cmn" value="+"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
          </div>

          <div class="calc__row ">
            <div class="guest__title">Max Guests</div>
            <div class="guest__inc_dec">
              <button type="button" class="size-dec-btn-cmn" value="−"><i class="fa fa-minus" aria-hidden="true"></i></button>
              <input type="text" class="maxGuest text-center" readonly name="quantity" value="1" data-diff="1" data-min="1" data-max="10" size="1">
               <button type="button" class="size-inc-btn-cmn" value="+"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
          </div>

          <div class="guest__done-btn margin-top-30">
            <a href="javascript:void(0);" class="btn btn__primary createTable">Done</a>
            <a href="javascript:updateTable();" class="btn btn__primary updateTable hide">Update</a>
            <a href="javascript:cancelUpdateTable();" class="btn btn__holo cancelUpdateTable hide">Cancel</a>
          </div>

        </form>
        </div>
      </div>
      </div>
    </div>
        <div id="tableListing" class="col-xs-12 padding-bottom-10"></div>
          </div>
  </div>
</div>
  <!-- conbination rules -->
  <div class="panel panel-default floor-configration__bg fullWidth hide">
    <div class="panel-heading fullWidth" >
          <h4 class="panel-title collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" href="#collapse3">Combination Rules <small></small></h4>
    </div>
    <div id="collapse3" class="panel-collapse fullWidth collapse">
        <div class="panel-body">
          <div class="combination">
              <div class="col-sm-12 margin-top-10">
                 <ul class="combination-heading">
                    <li>Combinations</li>
                    <li>Min</li>
                    <li>Max</li>
                </ul>
              </div>
                <form id="tableCombinationRules">
                    <div id="table-combination-add-container" class="col-sm-12">
                        @if(!$table_combinations)
                            <div id="noCRsetValidation" class="font-weight-600 padding-top-10 text-center">No combination rules set.</div>
                        @else
                            @foreach($table_combinations as $combination)
                                <ul class="combination-list">
                                    <li>{{$combination->combination_name}}</li>
                                    <li>{{$combination->min}}</li>
                                    <li>{{$combination->max}}</li>
                                    <li><a href="#" onclick="deleteTableCombination({{$combination->id}})"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                                </ul>
                            @endforeach
                        @endif
                        <ul class="combination-list add-CR-list hide">
                            <li class="width-120">&nbsp;</li>
                            <li><input type="text" maxlength="2" value="0"></li>
                            <li>0</li>
                            <li></li>
                        </ul>
                    </div>
                    <div class="combination-add text-center padding-top-bottom-20" id="table-combination-add">
                        <a href="javascript:addTableCombinationRules()" class="btn btn__primary">Add</a>
                    </div>
                    <div class="combination-save text-center padding-top-bottom-20 hide" id="table-combination-save">
                        <a href="javascript:saveTableCombinationRules()" class="btn btn__primary font-weight-600">Save</a>
                        <a href="javascript:cancelTableCombinationRules()" class="btn btn__cancel font-weight-600 margin-left-10">Cancel</a>
                    </div>
                </form>


             <!-- combination suggestions -->
             <div class="combination suggestion-list col-sm-12 padding-bottom-20 hide">
             <h5 class="clear-float suggestion-title">Suggested Combination </h5>
            
             <ul class="combination-heading">
                <li>Combinations</li>
                <li>Min</li>
                <li>Max</li>
            </ul>
            <div id="table-combination-list">
             <ul class="combination-list">
               <li>A1 + A2</li>
                <li><input type="text" maxlength="2" value="2"></li>
                <li>6</li>
                <li class="waitList__otp no-padding">
                  <div class="toggle__container">
                    <button type="button" id="" name="" class="btn btn-xs btn-secondary btn-toggle  active" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                      <div class="handle"></div>
                    </button>
                  </div>
                </li>
             </ul>
             <ul class="combination-list">
               <li>A1 + A2</li>
                <li><input type="text" maxlength="2" value="2"></li>
                <li>6</li>
                <li class="waitList__otp no-padding">
                  <div class="toggle__container">
                    <button type="button" id="" name="" class="btn btn-xs btn-secondary btn-toggle  active" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                      <div class="handle"></div>
                    </button>
                  </div>
                </li>
             </ul>
            </div>

</div>

            <div class="combination-add hide"></div>

        </div>
  </div>
  </div>

  </div>
     <script src="https://cdn.rawgit.com/konvajs/konva/2.1.3/konva.min.js"></script>
  <script src="{{URL::asset('js/floor-configuration.js')}}" type="text/javascript" charset="utf-8"></script>
   <script>
       var floor_url="<?php echo URL::current()?>";
   </script>


    </div>

    </div>
    </div>
    </div>

@endsection
