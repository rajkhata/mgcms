@extends('layouts.app')

@section('content')

    <div class="main__container col-sm-12">
        <div class="create-floor__wrapper col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">

            <img src="{{asset('images/welcome-image.png')}}" class="create-floor__media" alt="welcome background image">

            <div class="create-floor__container margin15">
                <div class="col-sm-2"> <img src="{{asset('images/welcome.png')}}" class="welcome__img" alt="welcome background image"></div>
                <div class="create-floor__content-container welcome margin-top-30">
                    <h1 class="text-center">Welcome!</h1>
                    <p class="welcome__text">Let's start configuring your restaurant</p>

                    <div class="text-center welcome-add__floor">
                        <a href="{{ url('floor/create/floor') }}" class="btn btn__primary">Add Floor</a>
                        <a href="{{ url('floor/create/patio') }}" class="btn btn__secondary">Add Patio</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
