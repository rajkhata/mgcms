@extends('layouts.app')
@section('content')

 <div class="main__container">
 <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
 <script>

  var floor_names=@json($floor_names);


</script>
    <!-- floor listing -->
    <div class="row">
        <div class="floor__button margin-bottom-30">
            <a href="{{ url('floor/create/floor') }}" class="btn btn__primary">Add Floor</a>
            <a href="{{ url('floor/create/patio') }}" class="btn btn__primary">Add Patio</a>
        </div>
    </div>
    <div class="floor__wrapper col-sm-12">
        <div class="floor__container col-md-12 col-md-offset-0 col-lg-10 col-lg-offset-1">
                                                @include('reservation::common.message')


           <div class="row">



               <script>

                   console.log(<?php echo $floors?>.length)

               </script>

            @if(!$floors->isEmpty())
                @foreach ($floors as $floor)


                
                      <div class="col-sm-6 col-md-4 col-lg-4 floor-box">
                            <!-- floor listing-->
                            <div class="floor__element @if($floor->type=='patio') @elseif($floor->type=='floor' && $floor->completed_step!=2) floor__background-secondary @endif ">
                                <!-- floor name -->
                                <div class="floor__name  @if($floor->type=='patio') floor__name-alt 
                                @elseif($floor->type=='floor' && $floor->completed_step!=2) floor__name-secondary
                                @endif ">{{$floor->name}}</div>

                                <div class="floor__image">
                                    @if($floor->type=='patio') 
                                     <img src="{{asset('images/floor_1.png')}}">

                                    @elseif($floor->type=='floor' && $floor->completed_step!=2) 
                                   <img src="{{asset('images/floor_1.png')}}"> 

                                    @elseif($floor->type=='floor' && $floor->completed_step==2)
                                     <img src="{{asset('images/floor_1.png')}}"> 

                                    @endif
                                     
                                </div>

                                <div class="floor__details-add">
                                <ul class="floor__details-edit">
                                        <li><a href="/floor/configure/{{$floor->id}}"><span class="fa fa-pencil" aria-hidden="true"></span><span>Edit</span></a></li>
                                        @if($floor->completed_step==2)
                                        <li><a href="javascript:void(0)" class="update_floor" floor_id="{{$floor->id}}"><span  class="icon-duplicate" aria-hidden="true"></span><span>Duplicate</span></a></li>
                                         @endif
                                        <li><a href="javascript:void(0)" class="delete_floor" floor_id="{{$floor->id}}"><span class="fa fa-trash-o" aria-hidden="true"></span><span>Delete</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                @endforeach
            @else
             No floor/Patio
            @endif
                



           </div>

           

        </div>
    </div>

 </div>


 @endsection