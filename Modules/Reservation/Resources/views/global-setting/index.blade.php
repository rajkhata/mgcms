@extends('layouts.app')
@section('content')
  <link href="{{asset('css/multiple-emails.css')}}" rel="stylesheet" type="text/css">
  <script src="{{asset('js/multiple-emails.js')}}" type="text/javascript" charset="utf-8"></script>
   <div class="main__container notificationcontainer margin-lr-auto">
   <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>

<?php 
    $form_type=old('form_type')?old('form_type'):'reservation';

    $min_for_online_reservation_lower = config('reservation.min_for_online_reservation.min');
    $min_for_online_reservation_upper = config('reservation.min_for_online_reservation.max');
    $max_for_online_reservation_lower = config('reservation.max_for_online_reservation.min');
    $max_for_online_reservation_upper = config('reservation.max_for_online_reservation.max');

    $allow_guest_advance_online_reservation_days = config('reservation.allow_guest_advance_online_reservation_days');
    $allow_guest_advance_online_reservation_months = config('reservation.allow_guest_advance_online_reservation_months');

    $allow_guest_advance_online_reservation_type = config('reservation.allow_guest_advance_online_reservation_type');

    $cut_off_time_hour = config('reservation.cut_off_time_hour');
    $cut_off_time_day = config('reservation.cut_off_time_day');

    $cut_off_time_type = config('reservation.cut_off_time_type');
    $chancellation_charge_type = config('reservation.cancellation_charge_type');
?>
 <div class="row">
  <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">

      @include('reservation::common.message')

   <ul class="nav nav-pills globel_tabs">

       {{--<li class="working__tab">
           <a href="#ordering_setting" data-toggle="tab" aria-expanded="false">Ordering Settings</a>
       </li>--}}

       <li class="working__tab <?php echo
       (!isset($form_type) || (isset($form_type) && $form_type=='reservation'))?'active':'';
       ?>">
          <a href="#reservation_setting " data-toggle="tab" aria-expanded="true">Reservation</a>
        </li>
        <li class="working__tab  <?php echo
(isset($form_type) && $form_type=='notification')?'active':'';
?>" >
          <a href="#notification_setting" data-toggle="tab" aria-expanded="false">Notification</a>
        </li>    
     
      </ul>
      <div class="tab-content clearfix working-hours">
          <div class="tab-pane hide" id="ordering_setting">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 card--head no-padding">
                      <form>
                          <div class="box-shadow res--sett-content">
                              <div class="row">
                                  <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Accept Deilvery Orders</div>
                                  <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right">
                                      <div class="waitList__otp no-padding">
                                          <div class="toggle__container">
                                              <button type="button" id="enable_deilvery_orders_button" class="btn btn-xs btn-secondary btn-toggle" data-toggle="button" aria-pressed="true" autocomplete="off">
                                                  <div class="handle"></div>
                                              </button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Accept Takeout Orders</div>
                                  <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right">
                                      <div class="waitList__otp no-padding">
                                          <div class="toggle__container">
                                              <button type="button" id="enable_takeout_orders_button" class="btn btn-xs btn-secondary btn-toggle" data-toggle="button" aria-pressed="true" autocomplete="off">
                                                  <div class="handle"></div>
                                              </button>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="row setting-save-btn">
                                  <button type='submit'>Save</button>
                              </div>

                          </div>
                      </form>
                  </div>
              </div>

          </div>

          <div class="tab-pane <?php echo
          (!isset($form_type) || (isset($form_type) && $form_type=='reservation'))?'active':'';
          ?>" id="reservation_setting">
           <div class="row">


       <div class="col-lg-12 col-md-12 col-sm-12 card--head no-padding">
           <form  method="post" action="" name="reservation_settings" id="reservation_settings">
                   @csrf

          <input type="hidden"  value="reservation" name="form_type">


<!--
               <div class="tag--head text-center top-left-rem-rad">

                   Reservation Settings
               </div>
-->
                <div class="box-shadow res--sett-content">
                    <div class="row">
                   <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Accept Online Reservation</div>
                   <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                   <div class="waitList__otp no-padding">
                    <div class="toggle__container">

                      <?php 

                      if(isset($reservation_settings['reservation']['accept_online_reservation']) && $reservation_settings['reservation']['accept_online_reservation']==1){
                       $accept_online_reservation_button='active';
                      }else{
                       $accept_online_reservation_button='';
                      }

                       ?>
                        <button type="button" id="selectpickeraccept_online_reservation_button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom <?php echo $accept_online_reservation_button; ?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                    <div class="handle"></div>
                                  </button>
                        <input type="hidden"  value="<?php echo isset($reservation_settings['reservation']['accept_online_reservation'])?$reservation_settings['reservation']['accept_online_reservation']:0; ?>" name="accept_online_reservation" id="accept_online_reservation">
                    </div>
                   </div>                   
                        </div>
               </div>
                    <div class="row">
                   <div class="col-lg-7 col-md-7 col-sm-7 no-padding-left text-left">Min/Max setting for online reservation</div>
                   <div class="col-lg-5 col-md-5 col-sm-5 no-padding-right">
                        <div class="two-in--row adjust-bug">
                        <span>Min</span>
                         <div class="selct-picker-plain">
                            <select data-size="5" data-style="no-background-with-buttonline" class="selectpicker le reservation-book min_for_online_reservation" name="min_for_online_reservation" >
                             @for($min_for_online_reservation_lower;$min_for_online_reservation_lower<=$min_for_online_reservation_upper;$min_for_online_reservation_lower++)
                                <option value="{{$min_for_online_reservation_lower}}" 
                                <?php 
                                if(isset($reservation_settings['reservation']['min_for_online_reservation']) && $reservation_settings['reservation']['min_for_online_reservation']==$min_for_online_reservation_lower){ echo 'selected';} ?> >{{$min_for_online_reservation_lower}}</option>
                              @endfor 
                            </select>
                        </div>
                         <span>Max</span>
                          <div class="selct-picker-plain">
                            <select data-size="5" data-style="no-background-with-buttonline" class="selectpicker ge reservation-book max_for_online_reservation" name="max_for_online_reservation" >
                                @for($max_for_online_reservation_lower;$max_for_online_reservation_lower<=$max_for_online_reservation_upper;$max_for_online_reservation_lower++)
                                <option value="{{$max_for_online_reservation_lower}}" 
                                <?php 
                                if(isset($reservation_settings['reservation']['max_for_online_reservation']) && $reservation_settings['reservation']['max_for_online_reservation']==$max_for_online_reservation_lower){ echo 'selected';} ?> >{{$max_for_online_reservation_lower}}</option>
                              @endfor
                            </select>
                        </div>
                       </div>
                   </div>                   
               </div>
                    <div class="row">
                   <div class="col-lg-7 col-md-7 col-sm-7 no-padding-left text-left">Allow guests to make reservation in advance</div>
                   <div class="col-lg-5 col-md-5 col-sm-5 no-padding-right">
                       <div class="two-in--row cutoff">
                    
                         <div class="selct-picker-plain">
         <select data-dropup-auto="false" data-style="no-background-with-buttonline" data-size="5" class="selectpicker reservation-book allow_guest_advance_online_reservation_days <?php if(@$reservation_settings['reservation']['allow_guest_advance_online_reservation_type']=='Months'){echo 'hide';}?>" name="allow_guest_advance_online_reservation_days" >
                                   @for($i=$allow_guest_advance_online_reservation_days['min'];$i<=$allow_guest_advance_online_reservation_days['max'];$i++)
                                 <option value="{{$i}}" 
                                <?php 
                                if(isset($reservation_settings['reservation']['allow_guest_advance_reservation_time']) && $reservation_settings['reservation']['allow_guest_advance_reservation_time']==$i){ echo 'selected';} ?> >{{$i}}</option>
                               @endfor  
                            </select>
                             <select data-dropup-auto="false" data-style="no-background-with-buttonline" data-size="5" class="selectpicker reservation-book allow_guest_advance_online_reservation_months <?php if(!isset($reservation_settings['reservation']['allow_guest_advance_online_reservation_type']) || $reservation_settings['reservation']['allow_guest_advance_online_reservation_type']=='Days'){echo 'hide';}?>" name="allow_guest_advance_online_reservation_months" >
                                   @for($i=$allow_guest_advance_online_reservation_months['min'];$i<=$allow_guest_advance_online_reservation_months['max'];$i++)
                                 <option value="{{$i}}" 
                                <?php 
                                if(isset($reservation_settings['reservation']['allow_guest_advance_reservation_time']) && $reservation_settings['reservation']['allow_guest_advance_reservation_time']==$i){ echo 'selected';} ?> >{{$i}}</option>
                               @endfor  
                            </select>
                            
                        </div>
                        <div class="selct-picker-plain">
                            <select data-style="no-background-with-buttonline" class="selectpicker reservation-book allow_guest_advance_online_reservation_type" name="allow_guest_advance_online_reservation_type" >
                                 @foreach($allow_guest_advance_online_reservation_type  as $val)
                                <option value="{{$val}}" 
                                <?php 
                                if(isset($reservation_settings['reservation']['allow_guest_advance_online_reservation_type']) && $reservation_settings['reservation']['allow_guest_advance_online_reservation_type']==$val){ echo 'selected';} ?> >{{$val}}</option>
                               @endforeach  
                            </select>
                        </div>

                         </div>
                       
                   </div>                   
               </div>
                    <div class="row">
                   <div class="col-lg-5 col-md-5 col-sm-5 no-padding-left text-left">Cutoff time</div>
                   <div class="col-lg-7 col-md-7 col-sm-7 no-padding-right">
                         <div class="two-in--row cutoff">
                       

                         <div class="selct-picker-plain ">
                            <select data-dropup-auto="false" data-size="5" class="selectpicker reservation-book cut_off_time_hour <?php if(@$reservation_settings['reservation']['cuttoff_type']=='Day'){echo 'hide';}?>" name="cut_off_time_hour" >
                              <option value="0.5"
                                  <?php 
                                if(isset($reservation_settings['reservation']['cuttoff_time']) && $reservation_settings['reservation']['cuttoff_time']=='0.5'){ echo 'selected';} ?>
                              >0.5</option>
                                  @for($i=$cut_off_time_hour['min'];$i<=$cut_off_time_hour['max'];$i++)
                                 <option value="{{$i}}" 
                                <?php 
                                if(isset($reservation_settings['reservation']['cuttoff_time']) && $reservation_settings['reservation']['cuttoff_time']==$i){ echo 'selected';} ?> >{{$i}}</option>
                               @endfor 
                            </select>


                             <select data-style="no-background-with-buttonline" data-size="5" class="selectpicker reservation-book cut_off_time_day <?php if(!isset($reservation_settings['reservation']['cuttoff_type']) || $reservation_settings['reservation']['cuttoff_type']=='Hour'){echo 'hide';}?>" name="cut_off_time_day" >
                                   @for($i=$cut_off_time_day['min'];$i<=$cut_off_time_day['max'];$i++)
                                 <option value="{{$i}}" 
                                <?php 
                                if(isset($reservation_settings['reservation']['cuttoff_time']) && $reservation_settings['reservation']['cuttoff_time']==$i){ echo 'selected';} ?> >{{$i}}</option>
                               @endfor  
                            </select>
                    
                        </div>
                             <div class="selct-picker-plain">
                            <select data-dropup-auto="false" data-size="5" data-style="no-background-with-buttonline" class="selectpicker reservation-book cuttoff_type" name="cuttoff_type" >
                                 @foreach($cut_off_time_type  as $val)
                                <option value="{{$val}}" 
                                <?php 
                                if(isset($reservation_settings['reservation']['cuttoff_type']) && $reservation_settings['reservation']['cuttoff_type']==$val){ echo 'selected';} ?> >{{$val}}</option>
                               @endforeach  
                            </select>
                        </div>
                       </div>
                   </div>                   
               </div>

                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 no-padding-left text-left">Cancellation Charges</div>
                        <div class="col-lg-4 col-md-4 col-sm-4 no-padding-right">
                            <div class="two-in--row cancellation_charges">
                                <div class="cancellation_charges_amt margin-right-15 input-group" style="width:60px !important";>
                                    <span class="input-group-addon font-weight-600 cancellation-value">$</span>
                                    <input type="text" maxlength="3" onkeypress="return isNumberKey(event)" class="form-control font-weight-600 color-black " name="cancellation_charge" id="cancellation_charges_amt" value="{{(isset($reservation_settings['reservation']['cancellation_charge']) && !empty($reservation_settings['reservation']['cancellation_charge']))?$reservation_settings['reservation']['cancellation_charge']:0}}">
                                </div>
                                <div class="selct-picker-plain" style="width:120px !important;">
                                    <select data-style="no-background-with-buttonline no-padding-left" data-size="5" class="selectpicker reservation-book" name="cancellation_charge_type" >
                                        @foreach($chancellation_charge_type as $type_key=>$type)
                                            <option value={{$type_key}} {{(isset($reservation_settings['reservation']['cancellation_charge_type']) && $reservation_settings['reservation']['cancellation_charge_type']==$type_key)? 'selected':''}}>{{$type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                   <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Require Host name for accepting in-house/phone reservation</div>
                   <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                         <div class="waitList__otp no-padding">
                          <div class="toggle__container">
                            <?php 

                        if(isset($reservation_settings['reservation']['require_hostname_accepting']) && $reservation_settings['reservation']['require_hostname_accepting']==1){
                           $require_hostname_accepting='active';
                        }else{
                           $require_hostname_accepting='';
                        }

                        ?>
                        <button type="button" id="require_hostname_accepting_button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom <?php echo $require_hostname_accepting;?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                    <div class="handle"></div>
                                  </button>
                        <input type="hidden" value="<?php echo isset($reservation_settings['reservation']['require_hostname_accepting'])?$reservation_settings['reservation']['require_hostname_accepting']:0; ?>" name="require_hostname_accepting" id="require_hostname_accepting">
                    </div>
                       </div>
                   </div>                   
               </div>
                    <div class="row hide">
                   <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Enable Online Waitlist</div>
                   <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                         <div class="waitList__otp no-padding">
                          <div class="toggle__container">
                         <?php 

                        if(isset($reservation_settings['reservation']['enable_online_waitlist']) && $reservation_settings['reservation']['enable_online_waitlist']==1){
                           $enable_online_waitlist='active';
                        }else{
                           $enable_online_waitlist='';
                        }

                        ?>
                        <button type="button" id="enable_online_waitlist_button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom <?php echo $enable_online_waitlist;?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                    <div class="handle"></div>
                                  </button>
                        <input type="hidden" value="<?php echo isset($reservation_settings['reservation']['enable_online_waitlist'])?$reservation_settings['reservation']['enable_online_waitlist']:0; ?>" name="enable_online_waitlist" id="enable_online_waitlist">
                             </div>
                             </div>
                       
                   </div>                   
               </div>
                                  <div class="setting-save-btn">

        <button type='submit'>
                  Save
                </button>

    </div>
               </div>
           </form>
       </div>
       
   </div>
          </div>
          
           <div class="tab-pane box-shadow <?php echo
(isset($form_type) && $form_type=='notification')?'active':'';
?> " id="notification_setting">
<div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12 card--head no-padding">
         <form class="validation-msg"  method="post" action="/configure/notification-settings" name="notification_settings" id="notification_settings">
                   @csrf

          <input type="hidden"  value="notification" name="form_type">

               
<!--
               <div class="tag--head text-center top-left-rem-rad">
                   Notification Settings
               </div>
-->
                <div class="box-shadow notification-setting">
           <div class="seprator">
                    <div class="head--part">Guest Notification</div>
                    <div class="row">
                   <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Send SMS Notification on Reservation</div>
                   <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                   <div class="waitList__otp no-padding">
                    <div class="toggle__container">
                        <?php 

                      if(isset($reservation_settings['notification']['guest_sms_on_reservation']) && $reservation_settings['notification']['guest_sms_on_reservation']==1){
                       $guest_sms_on_reservation='active';
                      }else{
                       $guest_sms_on_reservation='';
                      }

                       ?>
                        <button type="button" id="guest_sms_on_reservation_button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom <?php echo $guest_sms_on_reservation;?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                    <div class="handle"></div>
                                  </button>
                        <input type="hidden" id="guest_sms_on_reservation" value="<?php echo isset($reservation_settings['notification']['guest_sms_on_reservation'])?$reservation_settings['notification']['guest_sms_on_reservation']:0; ?>" name="guest_sms_on_reservation">
                    </div>
                       </div>
                       </div>
                       </div>
                       <div class="row">
                   <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Send Email Notification on Reservation</div>
                   <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                   <div class="waitList__otp no-padding">
                    <div class="toggle__container">
                        <?php 

                      if(isset($reservation_settings['notification']['guest_email_on_reservation']) && $reservation_settings['notification']['guest_email_on_reservation']==1){
                       $guest_email_on_reservation='active';
                      }else{
                       $guest_email_on_reservation='';
                      }

                       ?>
                        
                        <button type="button" id="guest_email_on_reservation_button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom  <?php echo $guest_email_on_reservation;?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                    <div class="handle"></div>
                                  </button>
                        <input type="hidden" id="guest_email_on_reservation" value="<?php echo isset($reservation_settings['notification']['guest_email_on_reservation'])?$reservation_settings['notification']['guest_email_on_reservation']:0; ?>" name="guest_email_on_reservation">
                    </div>
                       </div>
                       </div>
                       </div>
                    <div class="row">
                   <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Send Reservations reminders via SMS</div>
                   <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                   <div class="waitList__otp no-padding">
                    <div class="toggle__container">
                         <?php 

                      if(isset($reservation_settings['notification']['guest_reservation_reminder_via_sms']) && $reservation_settings['notification']['guest_reservation_reminder_via_sms']==1){
                       $guest_reservation_reminder_via_sms='active';
                      }else{
                       $guest_reservation_reminder_via_sms='';
                      }

                       ?>
                        <button type="button" id="guest_reservation_reminder_via_sms_button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom <?php echo $guest_reservation_reminder_via_sms;?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                    <div class="handle"></div>
                                  </button>
                        <input type="hidden" id="guest_reservation_reminder_via_sms" value="<?php echo isset($reservation_settings['notification']['guest_reservation_reminder_via_sms'])?$reservation_settings['notification']['guest_reservation_reminder_via_sms']:0; ?>" name="guest_reservation_reminder_via_sms">
                    </div>
                       </div>
                       </div>
                       </div>
                    </div>
                    
          <div class="seprator">
                    <div class="head--part">Restaurant Notification</div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 no-padding-left">
                            Receive Email Notification on :
                        </div>
                       
                      
                       
                    </div>
                    <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Incoming reservation

</div>
                            <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                                <div class="waitList__otp no-padding">
                                    <div class="toggle__container">
                                       <?php 

                                if(isset($reservation_settings['notification']['restaurant_receive_notification_incoming']) && $reservation_settings['notification']['restaurant_receive_notification_incoming']==1){
                                 $restaurant_receive_notification_incoming='active';
                                }else{
                                 $restaurant_receive_notification_incoming='';
                                }

                                 ?>
                                        <button type="button" id="" class="btn btn-xs btn-secondary btn-toggle dayoff_custom <?php echo $restaurant_receive_notification_incoming;?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                             <div class="handle"></div>
                                        </button>

                                         <input type="hidden" id="guest_reservation_reminder_via_sms" value="<?php echo isset($reservation_settings['notification']['restaurant_receive_notification_incoming'])?$reservation_settings['notification']['restaurant_receive_notification_incoming']:0; ?>" name="restaurant_receive_notification_incoming">
                                    </div>
                                </div>
                            </div>
                       </div>
                       <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Modified reservation</div>
                            <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                                <div class="waitList__otp no-padding">
                                    <div class="toggle__container">
                                       <?php 

                                if(isset($reservation_settings['notification']['restaurant_receive_notification_modified']) && $reservation_settings['notification']['restaurant_receive_notification_modified']==1){
                                 $restaurant_receive_notification_modified='active';
                                }else{
                                 $restaurant_receive_notification_modified='';
                                }

                                 ?>
                                        <button type="button" id="" class="btn btn-xs btn-secondary btn-toggle dayoff_custom <?php echo $restaurant_receive_notification_modified;?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                             <div class="handle"></div>
                                        </button>

                                         <input type="hidden" id="guest_reservation_reminder_via_sms" value="<?php echo isset($reservation_settings['notification']['restaurant_receive_notification_modified'])?$reservation_settings['notification']['restaurant_receive_notification_modified']:0; ?>" name="restaurant_receive_notification_modified">
                                    </div>
                                </div>
                            </div>
                       </div>
                       <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 no-padding-left text-left">Cancelled reservation</div>
                            <div class="col-lg-2 col-md-2 col-sm-2 no-padding-right ">
                                <div class="waitList__otp no-padding">
                                     <div class="toggle__container">
                                       <?php 

                                if(isset($reservation_settings['notification']['restaurant_receive_notification_cancelled']) && $reservation_settings['notification']['restaurant_receive_notification_cancelled']==1){
                                 $restaurant_receive_notification_cancelled='active';
                                }else{
                                 $restaurant_receive_notification_cancelled='';
                                }

                                 ?>
                                        <button type="button" id="" class="btn btn-xs btn-secondary btn-toggle dayoff_custom <?php echo $restaurant_receive_notification_cancelled;?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                             <div class="handle"></div>
                                        </button>

                                         <input type="hidden" id="guest_reservation_reminder_via_sms" value="<?php echo isset($reservation_settings['notification']['restaurant_receive_notification_cancelled'])?$reservation_settings['notification']['restaurant_receive_notification_cancelled']:0; ?>" name="restaurant_receive_notification_cancelled">
                                    </div>
                                </div>
                            </div>
                       </div>
                     <!--    <div class="col-lg-7 col-md-7 col-sm-7 noti-email-check-align hide">
                              <div>
                                <label class="custom_checkbox relative">
                                  <?php 
                                   if(isset($reservation_settings['notification']['restaurant_receive_notification_incoming']) && $reservation_settings[
                                    'notification']['restaurant_receive_notification_incoming']==1){
                                   echo 'checked';
                                  }?>
                                    <input  <?php 
                                   if(isset($reservation_settings['notification']['restaurant_receive_notification_incoming']) && $reservation_settings[
                                    'notification']['restaurant_receive_notification_incoming']==1){
                                   echo 'checked';
                                  }?> class="hide" type="checkbox" name="restaurant_receive_notification_incoming" id="" value="1"> <span class="m-r-10">Incoming</span> reservation
                                    <span class="control_indicator"></span>
                                </label>
                            </div>
                                <div >
                                <label class="custom_checkbox relative">
                                    <input  <?php 
                                   if(isset($reservation_settings['notification']['restaurant_receive_notification_modified']) && $reservation_settings[
                                    'notification']['restaurant_receive_notification_modified']==1){
                                   echo 'checked';
                                  }?> class="hide" type="checkbox" name="restaurant_receive_notification_modified" id="" value="1"> <span class="m-r-10">Modified</span> reservation
                                    <span class="control_indicator"></span>
                                </label>
                            </div>
                             <div >
                                <label class="custom_checkbox relative">
                                    <input  <?php 
                                   if(isset($reservation_settings['notification']['restaurant_receive_notification_cancelled']) && $reservation_settings[
                                    'notification']['restaurant_receive_notification_cancelled']==1){
                                   echo 'checked';
                                  }?>  class="hide" type="checkbox" name="restaurant_receive_notification_cancelled" id="" value="1"> <span class="m-r-10">Cancelled</span> reservations
                                    <span class="control_indicator"></span>
                                </label>
                            </div>
                        </div> -->
                    </div>
                    <div class="row margin-top-20">
                        <div class="col-sm-12 no-padding-left head--part">
                        Email address for receiving notifications 
                        </div>
                            <div class="col-sm-12 margin-top-20 no-padding">
                        	<div class='form-group email_tags'>
					<input type='text' name="restaurant_receive_notification_emails"  class='form-control restaurant_receive_notification_emails' value='<?php echo isset($reservation_settings['notification']['restaurant_receive_notification_emails'])?trim($reservation_settings['notification']['restaurant_receive_notification_emails']):''; ?>'>
	
			
			</div>
                        </div>
                        

                        
                    </div>
                    <div class="setting-save-btn">

<button type='submit'>
           Save
         </button>

</div>
               </div>
                  
               </div>
               
           </form>
       </div>
       
   </div>
          </div>
</div>
       </div>
 </div>
 <script>
$(document).ready(function(){ 
    
jQuery.validator.addMethod("le", function(value, element, param) {
    
    //console.log(this.optional(element) + " LE " + value + " LE " +$("select[name="+param+"]").val())
    
    $(".selectpicker").selectpicker('refresh');
    
	return this.optional(element) || Number(value) < Number($("select[name="+param+"]").val());
}, "less");	
    
jQuery.validator.addMethod("ge", function(value, element, param) {
    
    //console.log(this.optional(element) + " Ge " + value + " GE " + $("select[name="+param+"]").val())
    
    $(".selectpicker").selectpicker('refresh');
    
	return this.optional(element) || Number(value) > Number($("select[name="+param+"]").val());
}, "greater");

	$("#reservation_settings").validate({
		rules:{
            min_for_online_reservation:{
                //required:true,
                le: "max_for_online_reservation"
            },
            max_for_online_reservation:{
                //required:true,
                ge: "min_for_online_reservation"                    
            },
        },
		messages:{
			min_for_online_reservation:{
                le: "Must be less than max value"
            },
			max_for_online_reservation:{
                ge: "Must be greater than min value"
            },
		},
	});

});
     
     //Plug-in function for the bootstrap version of the multiple email
		$(function() {
			//To render the input device to multiple email input using BootStrap icon
			$('.restaurant_receive_notification_emails').multiple_emails({position: "top"});
			//OR $('#example_emailBS').multiple_emails("Bootstrap");
//            $('.restaurant_receive_notification_emails').change( function(){
//                console.log($(this).val());
//                return false;
//			});

		});
     $(document).on("keypress", 'form', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
        e.preventDefault();
        return false;
    }
});
     
</script>
 
@endsection