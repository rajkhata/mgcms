@extends('layouts.app')
@section('content')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
  <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
  <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />

  <input type="hidden" id="schd_id" name="schd_id" value="0">
                  <input type="hidden" id="slot_id" name="slot_id" value="0">

  <?php
  $datetime = \App\Helpers\CommonFunctions::getRestaurantCurrentLocalTime(\Illuminate\Support\Facades\Auth::user()->restaurant_id, 'c');
  ?>

  <script>
      window.nowTimeShow = "{{ $datetime }}"
  </script>

  <div class="main__container container__custom_working">
  <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
  <div class="row margin-bottom-30">
<h1>Set Operational Hours</h1>
   </div>
    <div id="exTab1" class=" ">
      <ul  class="nav nav-pills globel_tabs">
        <li class="active working__tab">
          <a href="#working_hours" data-toggle="tab">Set Weekly Rules</a>
        </li>
        <li class="working__tab">
          <a href="#custom_working_hours" data-toggle="tab">Create a Custom Rule</a>
        </li>    
     
      </ul>
      
         

      <div class="tab-content clearfix working-hours">
        <div class="tab-pane active box-shadow" id="working_hours"  >
<!--          <h5 class="tab-head">Working Hours</h5>-->

          <div class="workinghrs__calendar">
            <div id='calendar'></div>
          </div>
        </div>
        <div class="tab-pane customhrs__calendar" id="custom_working_hours">
          <h5 class="tab-head">Create a custom rule for a specific day or range of days that overrides the standard setting for every week.</h5>

            <div id='custom_calendar_view' class="box-shadow"></div>

          <div class="col-sm-12 margin-top-30">
            <div  class="btn_ btn__primary btn__custom-setting" data-toggle="modal" data-target="#customSettingModal">
              Add New
            </div>
          </div>



        </div>
      </div>
    </div>
    <input type="hidden" id="type" name="type" value="day" />

    <!--modal custom working hours-->
    <div class="modal fade" id="customSettingModal" tabindex="-1" role="dialog" aria-labelledby="customSettingModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog modal-lg customHrs__modal">
        <div class="modal-content">
          <div class="workinghrs__header">
            <h4>Add a Custom Working Hour</h4>
          </div>
          <div class="modal-body">
            <div class="custom_hrsModal__container">
              <div class="alert alert-danger" style="display:none"></div>
              <form id="custom_hours" class="" ethod="post" action="{{url('configure/working-hours-dayoff')}}">
                <div class="form-group margin-top-10">
                    <div class="input__field relative names pull-left col-sm-3 no-padding-left">
                        <input type="text" maxlength="15" name="custom_name" id="custom_name">
                        <span class="" for="custom">Enter Name</span>
                    </div>
                    <div class="pull-right">
                        <div class="col-sm-5 custom__date no-padding-left no-padding-right">
                            <label for="table-available" class="col-form-label datePicker__head">Start Date</label>

                              <div class="input__date-container">
                                <span class="fa fa-calendar" aria-hidden="true"></span>
                                <input class="input__date start__date required" type="text" id="start_date" name="start_date" readonly="readonly" >
                              </div>
                        </div>
                        <div class="col-sm-5 custom__date margin-left-40 no-padding-left no-padding-right">
                              <label for="table-available" class="col-form-label datePicker__head">End Date</label>

                              <div class="input__date-container">
                                <span class="fa fa-calendar" aria-hidden="true"></span>
                                <input class="input__date end__date required" type="text" id="end_date" name="end_date" readonly="readonly">
                              </div>
                        </div>

                  <div class="toggle__container text-right col-sm-2 margin-left-40 margin-top-10 no-padding">

                    <button type="button" class="btn btn-xs btn-secondary btn-toggle dayoff_custom active" title="Open" data-toggle="button" aria-pressed="true" data-status="false" autocomplete="off">
                      <div class="handle"></div>
                    </button>
                  </div>

                  <div class="handle"></div>
                  <input type="hidden" class="schd_id" name="schd_id" value="0">
                  <input type="hidden" class="slot_id" name="slot_id" value="0">
                    </div>
                </div>
              </form>
              <div class="form-group">
                <div class="custom_modalHrs">
                  <div id='custom_calendar'></div>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <div type="button" class="btn btn__primary custom_done font-weight-600 width-120 hide">Save</div>
            <button type="button" class="btn btn__cancel font-weight-600 width-120" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>



  <div class="modal  fade working--hours" id="availabilityModal" tabindex="-1" role="dialog" aria-labelledby="availabilityModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content full-content white-smoke">
          <form id="week_hours" action="{{url('configure/working-hours')}}" class="validation-msg">
<input type="hidden" class="schd_id" name="schd_id" value="0">
                  <input type="hidden" class="slot_id" name="slot_id" value="0">

        <div class="modal-header">
        <a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>

        </div>

        <div class="modal-body ">
           <!--Popup Container-->

            <div id="availabilityModalAlert" class="alert" style="display: none;"></div>
           
                <div class="row border-row shift-time">
                    <div class="col-sm-5">
                        <label for="">Shift Time</label>
                    </div>
                    <div class="col-sm-7">

                        <div class="col-sm-3">
                            <div class="to-text">From</div>
                        </div>

                        <div class="input__fullWidth col-sm-3 no-padding">
                            <input type="text" onkeypress="return false;" class="edit__field timepicker reservation-book open_time" onfocus="this.blur()" value="" name="open_time" id="book-a-table-tp" />
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </div>

                        <div class="col-sm-3">
                            <div class="to-text">To</div>
                        </div>

                        <div class="input__fullWidth col-sm-3 no-padding">
                            <input type="text" onkeypress="return false;" class="edit__field timepicker reservation-book close_time" onfocus="this.blur()" value="" name="close_time" id="book-a-table-tp" />
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </div>

                    </div>
                </div>
                
                 <div class="row border-row shift-name">
                    <div class="col-sm-6">
                        <label for="">Shift Name</label>
                    </div>
                    <div class="col-sm-6">

                      <div class="input__field fullWidth">
                                <i class="fa fa-check-circle greenColor hide" aria-hidden="true"></i>
                                <input class="slot_name" type="text" name="slot_name" id="slot_name" maxlength="20">
                                <div id="uniqueSlotName" class="font-size-12 color-red text-center font-weight-600 hide"></div>
                            </div>

                      

                    </div>
                </div>
                
                
                <div class="row border-row padding-top-bottom-15 available-floors">
                      <div class="col-sm-6">
                        <label>Available Floors</label>
                    </div>
                       <div class="col-sm-6 text-right">
                        <div id="multipleFloorsSelectpicker" class="selct-picker-plain">
                           <select name="floors[]" data-style="no-background" class="selectpicker" multiple>
                               @foreach($floors as $floor)
                                   <option value="{{$floor->id}}">{{$floor->name}}</option>
                               @endforeach
                           </select>
                        </div>


                     {{--<ul class="unstyled centered pull-right slot_available_floors no-margin">
                      @foreach($floors as $floor)
                      <li class="margin-top-bottom-5">
                        <input class="styled-checkbox" id="styled-checkbox-{{$floor->id}}" type="checkbox" name="floors[]" value="{{$floor->id}}">
                        <label for="styled-checkbox-{{$floor->id}}" class="text-capitalize">{{$floor->name}}</label>
                      </li>
                      @endforeach
                    </ul>--}}
                        
                    </div>
                    </div>
                
                <div class="row border-row">
                <div class="accordion slot_form_tabs" id="accordionExample">
              <div class="card white-smoke">

    <div class="row" class="weekdays_row_heading">
        <div class="col-sm-6 padding-top-bottom-5">
            <label>Table Availability</label>
        </div>
      <div class="col-sm-6 text-right no-padding-right">
        <button type="button" class="btn btn__holo collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseTwo" >
            <span class="btn__change">Change</span>
            <span class="btn__done">Done</span>
        </button>
                
      </div>
    </div>

<div id="collapseOne" class="collapse tabl_type_section " aria-labelledby="headingOne" data-parent="#accordionExample">
  <div class="card-body table_type_inner">
      

     
      </div>
    </div>
  </div>
            </div>
                </div>
                
                <div class="row border-row">
                <div class="accordion slot_form_tabs" id="accordionExample1">
              <div class="card white-smoke">

      <div class="row" class="weekdays_row_heading">
          <div class="col-sm-6 padding-top-bottom-5">
            <label>Pacing</label>
          </div>
          <div class="col-sm-6 text-right no-padding-right">
        <button type="button" class="btn btn__holo collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <span class="btn__change">Change</span>
            <span class="btn__done">Done</span>
        </button>
          </div>
      </div>


    <div id="collapseTwo" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample1">
      <div class="card-body">
      
      <p>Specify maximum cover for each seating interval.</p>
      
      <div class="pacingcontainer">
          </div>
      </div>
    </div>
  </div>
            </div>
                </div>
                
                <div class="row border-row">
                <div class="accordion slot_form_tabs" id="accordionExample2">
              <div class="card white-smoke">

      <div class="row" class="weekdays_row_heading">
          <div class="col-sm-6 padding-top-bottom-5">
                <label>Adjust Turnover Time</label>
          </div>
          <div class="col-sm-6 text-right no-padding-right">
                <button type="button" class="btn btn__holo collapsed" data-toggle="collapse" data-target="#adjust-turnoverTime" aria-expanded="true" aria-controls="adjust-turnoverTime">
                      <span class="btn__change">Change</span>
                      <span class="btn__done">Done</span>
                 </button>
          </div>
                
      </div>


    <div id="adjust-turnoverTime" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample2">
      <div class="card-body">
      
      <p>Specify Turnover Time (in minutes) for diffrent party sizes </p>
      
      <div class="row">
              <?php
                    $i=0;
                    $total_tat=count($table_type);
                    ?>
                  @foreach($table_type as $type)

                      <div class="col-sm-2 padding-left-right-10 text-center table_types tot_outer" data-turnoverTime="{{$type->tat}}" min="{{$type->min_seat}}" max="{{$type->max_seat}}">
                    @if(($i+1)==$total_tat)
                    {{$type->min_seat.'+'}}
                    @else
                      {{$type->min_seat.'-'.$type->max_seat}}

                    @endif 
                    </div>

                  <?php
                        $i++;
                   ?>
                  @endforeach
        
      </div>
      <div class="row">
                <?php
                    $i=0;
                    ?>
                  @foreach($table_type as $type)


                     <div class="col-sm-2 padding-left-right-10 tot_outer">
                   <div class="edit__field">
                       
                            <div class="number__box tinny">
                                <div class="input-group">
                    <span class="input-group-btn">
                      <button data-type="minus" data-field="party_size" type="button" class="btn btn-danger text-center btn-number minus-btn">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    </span>
                       <input type="text" name="party_size[{{$type->id}}]" readonly class="form-control text-center input-number party_size reservation-book table_types_tat"  diff="{{config('reservation.slot_interval_time')}}" min="1" max="600" tat_id="{{$type->id}}" data-turnoverTime="{{$type->tat}}"  data-min="{{$type->min_seat}}" data-max="{{$type->max_seat}}" value="{{$type->tat}}">
                                    <span class="input-group-btn">
                        <button type="button" class="btn btn-success btn-number plus-btn text-center" data-type="plus" data-field="party_size">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                    </span>
                                </div>
                            </div>
                        </div>
          </div>

                  <?php
                        $i++;
                   ?>
                  @endforeach
         
         
      </div>
      
      </div>
    </div>
  </div>
            </div>
                </div>
                
                     
                    
                    
                      <div class="row border-row day_repeat_div" style="display: none">
                      <div class="col-lg-3 col-md-3 col-sm-3 text-left">
                        <label for="">Repeat for</label>
                    </div>
                       <div class="col-lg-9 col-md-9 col-sm-9 padd-zero">
                    <ul class="unstyled centered"> 
                      @foreach($calendar_days as $daynum => $day)                       
                          <li>
                            <input class="styled-checkbox check__box__input repeat_{{$day}}" id="repeat_{{$day}}" data-dayName="{{ucfirst($day)}}" type="checkbox" name="repeat_day[{{$day}}]" >
                            <label for="repeat_{{$day}}">{{ucfirst($day)}}</label>
                          </li>
                      @endforeach
                    </ul>
                        
                    </div>
                    </div>
                    
               <div class="form_field__container newWaitlist__btn-container rem-btn-border text-center editable_footer">
                  <button type="button" disabled class="btn btn__primary save__btn done done_slot_btn margin-bottom-30" id="" button-name="Check Availability">
                      Done
                  </button>

                </div>

                 <div class="form_field__container newWaitlist__btn-container rem-btn-border text-center non_editable_footer margin-top-30 margin-bottom-30" style="display: none;">
                  <button type="button" class="btn btn__primary edit"><span class="fa fa-pencil"></span>&nbsp;Edit</button>
                  <button type="button" class="btn btn__primary deleteworkingslot deleteslot" ><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</button>
                </div>

                
        </div>
    </form>
      </div><!-- modal-content -->
    </div><!-- modal-dialog -->
  </div><!-- modal -->
    
    <div class="modal fade modal-popup" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
          </div>
          <div class="modal-body text-center desc">
            <p>You are about to delete a custom setting. Do you want to proceed?</p>
          </div>
          <div class="modal-footer">
            <a class="btn btn__primary btn-ok">Yes</a>
            <button type="button" class="btn btn__holo" data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>


    <!--modal-->
    <div class="modal fade modal-popup" id="slot-overlap-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Slot Overlap</h4>
          </div>
          <div class="modal-body text-center">
            <p>Please select the different time slot.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn__cancel" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>

    <!--modal-->
    <div class="modal fade modal-popup" id="slot-overwrite-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Overlap Slot</h4>
          </div>
          <div class="modal-body text-center">
            <p>Please select the different time slot.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>

    <!--modal-->
    <div class="modal fade modal-popup" id="confirm-turnover-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close popup__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Turnover Time Alert</h4>
          </div>
          <div class="modal-body text-center">
            <p>Kindly select a slot time greater than or equal to <span id="minTurnoverTime"></span> minutes.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn__cancel font-weight-600" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="{{asset('js/jquery-ui-1.12.1.js')}}"></script>
  <script src="{{asset('js/schedular/moment.min.js')}}"></script>
  <script src="{{asset('js/schedular/fullcalendar.min.js')}}"></script>
  <script src="{{asset('js/schedular/scheduler.min.js')}}"></script>
   <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.timepicker.css')}}" />
 <script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.js')}}"></script>
  <script src="{{asset('js/working_hours.js')}}"></script>
  
  
@endsection