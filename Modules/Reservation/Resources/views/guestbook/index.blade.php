@extends('layouts.app')
@section('content')
    <div class="main__container guestbook-listing-page">
        <!--Header-->
        <div class="row">
            <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
            <div class="reservation__atto flex-align-item-baseline flex-direction-row flex-column-tablet">
                <div class="reservation__title margin-top-5">
                    <h1>Guestbook</h1>
                </div>
                <!-- <div class="guestbook__sec2 no-padding-left-tablet">

                    <div class="search-icon no-margin-right">
                        <form id="search" method="get" action="{{url('guestbook')}}">
                            <i class="fa fa-search"></i>
                            <input type="text" name="keyword" placeholder="Name and Phone number" id="keyword"
                                   value="{{ Request::has('keyword')?trim(Request::get('keyword')):'' }}">
                            <button style="border-radius: 20px;" class="btn btn__primary font-weight-600" type="submit">
                                Search
                            </button>
                        </form>
                    </div>


                    <div class="margin-left-10">
                        <a href="#" onclick="event.preventDefault()"
                           class="btn btn__primary font-weight-600 guestbook-filters" data-toggle="modal"
                           data-target="#filterModal">Filter</a>
                        <a href="javascript:void(0)"
                           class="btn btn__holo font-weight-600 export_guest margin-left-10"><i class="fa icon-export"
                                                                                                aria-hidden="true"></i>
                            Export</a>
                        <a href="#" class="btn btn__primary hide">Add</a>
                    </div>

                </div> -->
            </div>


            <div class="guest_listing">

                <div class="gustbtnmob">
                    <a href="#" onclick="event.preventDefault()" class="btn btn__primary font-weight-600 guestbook-filters" data-toggle="modal" data-target="#filterModal">Filter</a>
                </div>
                @include('reservation::guestbook.partial.guest_render')

            </div>
        </div>
    </div>

    <script>
        $("body").addClass("guestbook-detail-page");
    </script>

    @include("reservation::guestbook.partial.filter", ['maxReservation' => $maxReservation, 'maxOrder'=>$maxOrder, 'avgSpend'=>$avgSpend])
    <script src="{{asset('js/guestbook.js')}}" type="text/javascript" charset="utf-8"></script>
@endsection
