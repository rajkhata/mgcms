<div class="detailPopup removeuserdetails" id="userdata1">

<div class="">
        <style>/*.a_nd_order > .row:first-child, .section_popup:after, .popup_orderdetail, .panel_list_order{display:none!important;height:0px}*/ /*.a_nd .u_contentsection{padding-top:0;}*/ table{border-collapse: initial;}.printcontainer{display: none !important;}</style> <style media="print"> @page{size: auto; margin: 0;}.a_nd_order > .row:first-child, .section_popup:after, .popup_orderdetail, .panel_list_order{display: none !important; height: 0px}.a_nd .u_contentsection{padding-top: 0;}table{border-collapse: initial;}</style>


        <div>
        <div id="overlayOD"></div>
            <div id="orderDetailBox">
                <div class="closeDiv gustbok">
                    <a href="javascript:;" class="slideClose getuserdetails">
                        <i class="fa fa-close" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="guestbookDetails__body" id="guestbookMob">

                <div class="guestbook__detail">
                <!--Details Header-->
                <div class="detail__header clearfix">
                    <div class="guestbook__user">
                        <div class="guestbook__details-edit">
                            <!-- Commnetd by RG on 22-10-2018 Becuase Customer Edit Address-->
                             @can('Guestbook-edit')
                            <button type="button" class="edit_guest_details">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                             @endcan
                            <button type="button" class="update_guest_details update_guest_details-first edit-input"><i class="fa fa-check" aria-hidden="true"></i></button>
                            <button type="button" class="cancel_edit_guest_details cancel_edit_guest_details-first edit-input"><i class="fa fa-times" aria-hidden="true"></i></button>
                        </div>
                        <!--<div class="guestbook_imgDiv">
                            <div class="guestbook_dpContainer">
                                <?php 
                                   $pic =  $guest->display_pic_url ?? 'images/user_default.png';
                                ?>
                                <img src={{asset($pic)}} alt="display-pic" />
                            </div>
                        </div> -->
                        <form id="basicInfoForm_first">
                            <input type="hidden" name="userval" id="userval" value="<?php   if($guest->is_registered==1){   echo "Register User";
                                            }else{
                                                echo "Guest User";
                                            }             ?>"/>
                            <div class="guestbook__user-info row">
                                {{--<h3 class="editable" contenteditable="true">{{$guest->fname}} {{$guest->lname}}</h3>--}}

                                <div class="col-xs-12">
                                    <h3 class="guestName text-capitalize"><span>{{$guest->fname}} {{$guest->lname}}</span><b><div id="user_guestname">(
                                            @php
                                            if($guest->is_registered==1){
                                                echo "Register User";
                                            }else{
                                                echo "Guest User";
                                            }                                            
                                            @endphp    
                                            )</div></b><input class="edit-field text-capitalize" name="fl_name" type="text" value="{{$guest->fname}} {{$guest->lname}}" disabled="disabled" id="fl_name" maxlength="32"/></h3>
                                </div>
                                <div class="col-xs-12"><!-- Dummy Data -->
                                    <p class="guestaddress">{{$billingaddress}}</p>
                                </div>
                                {{--
                                  <p>VIP Customer</p>
                                  --}}
                                <div class="col-xs-12">
                                    <p class="selct-picker-plain category pointer-none">
                                     <!--   <select class="category selectpicker" data-style="no-background no-padding-left text-color-grey" data-width="auto" name="category" id="category">
                                            @foreach($all_category as $category)
                                                <option value="{{$category}}" <?php if($category == $guest->category){ echo 'selected'; } ?>>{{$category}}</option>
                                            @endforeach
                                        </select>-->
                                    </p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xs-12">
                                    <h4 class="no-margin-top" ><i class="icon-birthday text-colo-grey"></i> <span class="user__info"><input class="edit-field" autocomplete="off" type="text" id="birthday" name="birthday" value="{{($guest->dob)?$guest->dob:''}}" disabled="disabled" readonly="readonly" placeholder="dd/mm/yyyy"/></span></h4>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xs-12">
                                    <h4 class="mb0"><i class="icon-anniversary text-colo-grey"></i> <span class="user__info"><input class="edit-field" autocomplete="off" type="text" id="anniversary" name="anniversary" value="{{($guest->doa)?$guest->doa:''}}" disabled="disabled"  readonly="readonly" placeholder="dd/mm/yyyy"/></span></h4>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xs-12">
                                    <h4><i class="fa fa-envelope text-colo-grey"></i>  <span title="{{$guest->email}}" class="user__info"><input  class="edit-field text-ellipsis" type="text" name="email" id="email" value="{{$guest->email}}" disabled="disabled" readonly maxlength="50" placeholder="Email ID"/></span></h4><!--  text-ellipsis -->
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xs-12">
                                    <h4><i class="icon-contact-us text-colo-grey"></i> <span class="user__info"><input class="edit-field" maxlength="10" type="text" name="mobile" disabled="disabled" value="{{$guest->mobile}}" id="mobile"  pattern="[0-9]*" placeholder="xxxxxxxxxx" /></span></h4>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-4 col-xs-12">
                                    <h4><i class="icon-add-tag text-colo-grey"></i>
                                    <span class="user__info"><div class="selct-picker-plain position-relative" style="max-width:195px; width:100%">


                                        <select multiple name="add_tag" id="addtag" class="form-control selectpicker"  data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top" title="ADD TAG">
                                           @foreach($tags as $tag)
                                            <option value="{{$tag}}" <?php if(!empty($guest->tags)){ if(in_array($tag, $guest->tags)) { echo "selected"; } }?>>{{$tag}}</option>

                                            @endforeach
                                        </select>

                                   </div></span></h4>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="guestbook_userDetails hide">

                        <div class="guestbook_userDetails-edit">
                            <button type="button" class="edit_guest_details hide"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            <button type="button" class="update_guest_details update_guest_details-second edit-input hide"><i class="fa fa-check" aria-hidden="true"></i></button>
                            <button type="button" class="cancel_edit_guest_details cancel_edit_guest_details-second edit-input hide"><i class="fa fa-times" aria-hidden="true"></i></button>
                        </div>
                        <form class="guest_book_info_basic" action="{{url('/guestbook/update-details')}}" id="basicInfoForm">
                            <?php
                                if($guest->address && isset($guest->address['0'])) {
                                    $latest_address = $guest->address['0'];
                                    $myAddress = $latest_address->address1;
                                    if ($latest_address->address2) {
                                        $myAddress = $myAddress . ' ' . $latest_address->address2;
                                    }
                                    $myAddress = $myAddress . ' ' . $latest_address->city . ' ' . $latest_address->state . ' ' . $latest_address->zipcode;
                                }else {
                                    $myAddress = "";
                                }
                             ?>
                            <div class="details__customer">
                                <h3 class="hidden">Customer Details</h3>
                                <!-- <h4><i class="fa fa-envelope text-colo-grey"></i>  <span title="{{$guest->email}}" class="user__info"><input  class="edit-field text-ellipsis" type="text" name="email" id="email" value="{{$guest->email}}" disabled="disabled" maxlength="50" placeholder="Email ID"/></span></h4>
                                <h4><i class="icon-contact-us text-colo-grey"></i> <span class="user__info"><input class="edit-field" maxlength="15" type="text" name="mobile" value="{{$guest->mobile}}" disabled="disabled" id="mobile"  pattern="[0-9]*" placeholder="xxxxxxxxxx" /></span></h4> -->
                                    {{--
                                <h4><i class="icon-home text-colo-grey"></i> <span class="user__info"><textarea wrap="hard" rows="1" maxlength="100" class="edit-field" type="text" name="address"  id="address" disabled="disabled">{{$myAddress}}</textarea></span></h4>
                                --}}
                            </div>
                           
                        </form>
                    </div>
                </div>

                <!--Details AddTag-->
                <div class="details__addTag hide">
                    <div class="addTag__container">
                        <!--Button to add tag-->
                        <div class="add_tag">
                            <span>Add Tag</span>
                        </div>

                        <!--TAGS-->
                        <div id="guest_detail_tag">
                            @if($guest->tags)
                                <div class="tags_div">
                                    @foreach($guest->tags as $tag)
                                        <div class="detail__tag">
                                            <span class="tag_name">{{$tag}}</span><span class="text-icon"> ×</span>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="edit-input" id="guest_tags_div">
                        <form id="tag_form" action="{{url('guestbook/update-tags')}}">
                            <ul id="guest_tags"></ul>
                            <button type="button" class="btn btn__primary add_tag_button">Add</button>
                            <button type="button" class="btn btn__holo close_tag_button">Close</button>
                            <input type="hidden" name="cust_id" id="cust_id" value="{{$guest->id}}">
                            <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                        </form>
                    </div>
                </div>
                <?php
                    $first_last_activity_arr = getFirstAndLastActivityDates($guest);
                  // $total_order_spend = isset($guest->orders) && !empty($guest->orders)? round(collect($guest->orders)->sum('total_amount'), 2) : 0;
                    $total_order_count = isset($guest->orders) && !empty($guest->orders)? $guest->orders->count() :0;
                    //$avg_spend = !empty($total_order_spend) && !empty($total_order_count) ? round($total_order_spend/$total_order_count, 2) : 0;
                    $avg_spend = !empty($total_order_spend) ? round($total_order_spend/$total_order_for_avr, 2) : 0;
                    
                    if(Auth::user()) {
                        $curSymbol = Auth::user()->restaurant->currency_symbol;
                    } else {
                        $curSymbol = config('constants.currency');
                    }
                ?>
                <!--Details Body-->
                <div class="detail_body">

                    <div class="detailBody__header margin-top-15">
                        <h2>
                            Overview
                        </h2>
                    </div>

                    <div class="details__body-box">
                        <!--Stats-->

                        <div class="details__stats-box clearfix">
                            <div class="stats__megaBox">
                            @if($is_reservation_allowed)

                                <!--Stat-->
                                <div class="details__stat">

                                    <div class="stat__title">
                                        <span>Reservations</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Total social reach over the selected time period</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>

                                    <div class="stat__number">
                                        {{$reservation_locations['0']['reservation_count'] > 0 ? $total_resv_count : 'N/A'}}
                                    </div>

                                </div>

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>Reservations Frequency (per month)</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Total social reach over the selected time period</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">
                                        {{$reservation_locations['0']['reservation_count'] > 0 ? $first_last_activity_arr['frequency_reservations'] : 'N/A'}}
                                    </div>
                                </div>
                                @endif

                                <!--Stat-->
                                <div class="details__stat">

                                    <div class="stat__title">
                                        <span>Orders</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Total No. of Orders by customer.</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">
                                        {{$total_order_count}}
                                    </div>
                                </div>

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>Orders Frequency (per month)</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Order data produced on a monthly basis.</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">
                                        {{$first_last_activity_arr['frequency_orders']}}
                                    </div>
                                </div>

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>Total Spend</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Total amount spent through out.</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">
                                        @if($total_order_spend)
                                         {{ $curSymbol. $total_order_spend}}
                                        @else
                                            {{'No Transaction'}}
                                        @endif
                                    </div>
                                </div>
                            @if($is_reservation_allowed)

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>Reviews & Tips</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Total social reach over the selected time period</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">
                                        N/A
                                    </div>
                                </div>
                                @endif

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>Average Spend</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Average the total spend with Number of orders fulfilled.</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">
                                        @if($avg_spend)
                                            {{$curSymbol.$avg_spend}}
                                        @else
                                            {{'No Transaction'}}
                                        @endif
                                    </div>
                                </div>

                            @if($is_reservation_allowed)

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>Walk-ins</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Total social reach over the selected time period</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">{{ $reservation_locations['0']['reservation_count'] > 0 ? $total_walk_in_count : 'N/A' }}</div>
                                </div>
                                @endif

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>First Activity</span>
                                    </div>
                                    <div class="stat__number">
                                        {{--{{(count($guest->reservations)>0)?date('m-d-Y', strtotime($guest->reservations[count($guest->reservations)-1]->start_time)):'N/A'}}--}}
                                        {{($first_last_activity_arr['first_activity_date']) ? \Carbon\Carbon::parse($first_last_activity_arr['first_activity_date'])->format('m-d-Y') : 'N/A' }}
                                    </div>
                                </div>
                            @if($is_reservation_allowed)

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>Cancellations</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Total social reach over the selected time period</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">
                                        {{$reservation_locations['0']['reservation_count'] > 0 ?  $total_cancelled_count : 'N/A'}}
                                    </div>
                                </div>
                                 @endif
                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>Last Activity</span>
                                    </div>
                                    <div class="stat__number">
                                        {{--{{(count($guest->reservations)>0)?date('m-d-Y', strtotime($guest->reservations[0]->start_time)):'N/A'}}--}}
                                        {{($first_last_activity_arr['last_activity_date']) ? \Carbon\Carbon::parse($first_last_activity_arr['last_activity_date'])->format('m-d-Y') : 'N/A' }}
                                    </div>
                                </div>
                            @if($is_reservation_allowed)

                                <!--Stat-->
                                <div class="details__stat">
                                    <div class="stat__title">
                                        <span>No Show</span>
                                        <!--tooltip -->
                                        <div class="ntooltip h3">
                                            <div class="ttip">Total social reach over the selected time period</div>
                                        </div>
                                        <!--tooltip -->
                                    </div>
                                    <div class="stat__number">
                                        {{$reservation_locations['0']['reservation_count'] > 0 ? $total_no_show : 'N/A'}}
                                    </div>
                                </div>
                                @endif
                            </div>
                            <!-- fav item -->
                            @if($guest->items)
                            <div class="fav-list clearfix">
                                <h4>Favourite Item(s)</h4>
                              <?php  // echo '<pre>'; print_r($guest); ?>
                                <div class="scroller">
                                    <ul class="item_ul">

                                          <?php  $favt_item = array_count_values($guest->items);
                                            arsort($favt_item);
                                            $result =array();
                                            $result= array_slice($favt_item, 0, 5);
                                          ?>
                                        @foreach(array_keys($result) as $item)
                                                <li>
                                                    <span class="item_name">{{$item}}</span><!--<span class="text-icon-item"> ×</span>-->
                                                </li>
                                            @endforeach

                                    </ul>
                                </div>

                                <!-- <div class="btn__add">
                                    Add Item
                                </div>
                                <div class="edit-input" id="guest_items_div">
                                    <form id="item_form" action="{{url('guestbook/update-items')}}">
                                        <ul id="guest_items"></ul>
                                        <button type="button" class="btn btn__primary add_item_button">Add</button>
                                         <button type="button" class="btn btn__holo close_item_button">Close</button>
                                        <input type="hidden" name="cust_id" id="cust_id" value="{{$guest->id}}">
                                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                                    </form>
                                </div> -->
                            </div>

                            @endif

                            <!-- fav item -->
                            <div class="guest-note__box">
                                <!--Guest Note-->
                                <div class="position:relative">
                                    <h4>Guest Notes</h4>
                                    <div class="detail_body-edit">
                                        <button type="button" class="edit_guest_details"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        <button type="button" class="update_guest_details update_guest_details-third edit-input"><i class="fa fa-check" aria-hidden="true"></i></button>
                                        <button type="button" class="cancel_edit_guest_details cancel_edit_guest_details-third edit-input"><i class="fa fa-times" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <div class="guest-note">
                                    <h3><textarea class="edit-field"  wrap="hard" name="description" rows="4" maxlength="300" id="description" disabled="disabled" placeholder="No Notes: ">{{$guest->description}}</textarea></h3>
                                </div>
                            </div>
                            <!-- fav item -->
                            @if($countcontact!=0||$countcatering!=0||$countcareer!=0||$countevent!=0||$countreservation!=0)
                            <div class="fav-list clearfix">
                                <h4>Enquiries</h4>
                                <div class="stats__megaBox">
                                    @if($countcontact!=0)
                                    <div class="details__stat">
                                        <div class="stat__title">
                                            <span>Contact</span>
                                        </div>
                                        <div class="stat__number">
                                            {{$countcontact}}
                                        </div>
                                    </div>
                                    @endif
                                    @if($countcatering!=0)
                                    <div class="details__stat">
                                        <div class="stat__title">
                                            <span>Catering</span>
                                        </div>
                                        <div class="stat__number">
                                           {{$countcatering}}
                                        </div>
                                    </div>
                                   @endif
                                    @if($countcareer!=0)
                                    <div class="details__stat">
                                        <div class="stat__title">
                                            <span>Career</span>
                                        </div>
                                        <div class="stat__number">
                                          {{$countcareer}}
                                        </div>
                                    </div>
                                    @endif
                                   @if($countevent!=0)
                                        <div class="details__stat">
                                        <div class="stat__title">
                                            <span>Events</span>
                                        </div>
                                        <div class="stat__number">
                                          {{$countevent}}
                                        </div>
                                    </div>
                                  @endif
                                  @if($countreservation!=0)
                                    <div class="details__stat">
                                        <div class="stat__title">
                                            <span>Reservation</span>
                                        </div>
                                        <div class="stat__number">
                                           {{$countreservation}}
                                        </div>
                                    </div>
                                  @endif
                                </div>
                            </div>
                        @endif
                            <!-- fav item -->

                        </div>
                        <!--Favourite-item-->
                        <div class="details__favItems-box margin-top-10 hide">
                            <div class="fav-list">
                                <h4><!--tooltip -->
                            <div class="ntooltip h2">
                                <div class="ttip">Total social reach over the selected time period</div>
                            </div>
                            <!--tooltip --> Favourite Item(s):</h4>

                                <div class="scroller">
                                    <ul class="item_ul">
                                        @if($guest->items)
                                            @foreach($guest->items as $item)
                                                <li>
                                                    <span class="item_name">{{$item}}</span><span class="text-icon-item"> ×</span>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>

                                <div class="btn__add">
                                    Add Item
                                </div>
                                <div class="edit-input" id="guest_items_div">
                                    <form id="item_form" action="{{url('guestbook/update-items')}}">
                                        <ul id="guest_items"></ul>
                                        <button type="button" class="btn btn__primary add_item_button">Add</button>
                                         <button type="button" class="btn btn__holo close_item_button">Close</button>
                                        <input type="hidden" name="cust_id" id="cust_id" value="{{$guest->id}}">
                                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--Favourite-item-->
                    </div>

                </div>
            </div>



            <!--Activity Details-->
            <div class="guestbook__activities box-shadow">
                <div class="guestbook__activities-container">
                    <!--  tab section start -->
                    <div class="tab-sec">
                        <ul class="tab-menu">
                            <li class="active">
                                <a href="javascript:void(0)">Orders Archives</a>
                                {{--<a href="#tab-1">Activities</a>--}}
                            </li>
                            <li class="hide">
                                <a href="#tab-2">Reviews</a>
                            </li>
                        </ul>
                        <!-- tab1 start-->
                        <div class="tab-content" id="tab-1">
                            <div class="guestbook__activities-selectYear">
                                <div class="select__year">
                                    Select Year
                                </div>
                                <div class="year selct-picker-plain">
                                    <select id="guestbook_activity_year" class="selectpicker" data-style="no-background">
                                        @foreach($actRangeYearArr as $yr)
                                            <option>{{ $yr }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="activities__container">
                                <div class="guestbook__activities-activity">
                                    <!--Activity Box  -->
                                    @include('reservation::guestbook.partial.reservation-list', ['reservations' => $resvOrders, 'curSymbol' => $curSymbol])
                       
                                    <!--Activity Box  -->
                                    <div class="guestbook__activity-box hide">
                                        <div class="guestbook__activity_dateBox">
                                            16 Oct
                                        </div>
                                        <!--Reservation Type / Branch / Amount Spent-->
                                        <a class="guestbook_detailBox-container" href="/user_order/1929/details/product ">
                            

                                <div class="guestbook__activity_detailBox text-left col-sm-4 no-padding-right">
                                                        <span>Order Type</span>
                                    <h4>Delivery</h4>
                                </div>

                                <div class="guestbook__activity_detailBox text-left col-sm-5">
                                    <span>Branch</span>
                                    <h4>Katana Chicago</h4>
                                </div>

                                <div class="guestbook__activity_detailBox text-left col-sm-3 no-padding-left">
                                    <span>Amount Spent</span>
                                    
                                    <h4> $ <span>72.80</span></h4>
                                </div>
                            
                                </a>
                </div>
            </div>
                </div>

    </div>
</div>
<style>
    #ui-tooltip-0, #ui-tooltip-1 { display:none!important;}
    </style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/reservation/tags/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/reservation/tags/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css"> 

    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
        <!-- <script language="javascript" type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script> -->
        <script language="javascript" type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
        <script src="{{asset('js/tags/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>

        <script>
            $("body").addClass("guestbook-detail-page");
            
            var availableTags=<?php echo  json_encode($tags)?>;
            $('#guest_tags').tagit({
                itemName: 'item',
                fieldName: 'tags[]',
                //autocomplete: {delay: 0, minLength: 2},
                allowDuplicates:false,
                availableTags     :availableTags,
                beforeTagAdded: function(event, ui) {
                    $("#guest_tags").find('.ui-autocomplete-input').val('');
                    if($.inArray(ui.tagLabel, availableTags)==-1) return false;
               }

            });
            $('#guest_items').tagit({
                itemName: 'item',
                fieldName: 'items[]'
            });
            $(document).ready(function(){
                 $('.user__info').tooltip({
                    position: { my: "left bottom-40%", at: "left center" }
                });
            });

            $(document).ready(function(){
            var boxWidth = $("#orderDetailBox").width();
            $(".slideLeft").click(function(){
                $("#orderDetailBox").animate({
                    width: 0
                });
                $("#overlayOD").show();
            });
            $(".slideClose").click(function(){
                $("#orderDetailBox").animate({
                    width: boxWidth
                });
                $("#overlayOD").hide();
            });
        });
var tags_val='' ;
    $(function() {

        $('#addtag').change(function(e) {
            tags_val='';
            var tags = $(e.target).val();
            tags_val=  encodeURIComponent(JSON.stringify(tags));
            });
        $(".getuserdetails").click(function(){
            $('.removeuserdetails').remove();
        });
        });


        </script>
<script src="{{asset('js/guestbook.js')}}" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">