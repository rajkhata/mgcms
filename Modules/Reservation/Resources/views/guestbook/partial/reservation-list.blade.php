@php if(Auth::user()) {
$curSymbol = Auth::user()->restaurant->currency_symbol;
} else {
$curSymbol = config('constants.currency');
}
@endphp
@if(count($reservations))

    @foreach($reservations as $reservation)


        <div class="guestbook__activity-box">
            <div class="guestbook__activity_dateBox">
                {{ $reservation['readable_date'] }}
            </div>
            <!--Reservation Type / Branch / Amount Spent-->
            @php
                if($reservation['type']=='reservation') {
                    $link = '/reservation/all/'.$reservation['id'];
                } elseif ($reservation['product_type'] =='gift_card') {
                    $link = '/order-gift/'.$reservation['id'];
                } elseif ($reservation['product_type'] =='product') {
                    $link = '/user_order/'.$reservation['id'].'/mngdetails/product';
                } else {
                    $link = '/user_order/'.$reservation['id'].'/mngdetails/food_item';
                }
            @endphp
        <!--    <a class="guestbook_detailBox-container" href="@php echo $link; @endphp">-->
                <a class="guestbook_detailBox-container" href="javascript:void(0)" onclick="getOrderDetaillist('@php echo $link; @endphp')">

                <div class="guestbook__activity_detailBox" >
                    @php if($reservation['type']=='reservation') { $type='Reservation'; } else { $type='Order'; } @endphp
                    <span>{{ $type }} Type</span>
                    <h4>@php if(isset($reservation['product_type']) && $reservation['product_type']=='gift_card') { echo 'Gift Card'; } else { echo $reservation['source']; } @endphp</h4>
                </div>

                <div class="guestbook__activity_detailBox">
                    <span>Branch</span>
                    <h4>{{ $reservation['branch'] }}</h4>
                </div>

                <div class="guestbook__activity_detailBox">
                    <span>Amount Spent</span>
                    {{--<h4>$<span>20.50</span></h4>--}}
                    <h4>@if($reservation['amount']!=='N/A') {{$curSymbol}} @endif<span>{{ $reservation['amount'] }}</span></h4>
                </div>
              
</a>
        </div>
    @endforeach


@else

<div>No result</div>
@endif
<script>
    function getOrderDetaillist($url){

        $.ajax({
            type: 'GET',
            url: $url,
            data: '_token=<?php echo csrf_token();?>',
            success: function ($data) {
                $('#userdata1').hide();
                $('body').prepend($data);
                $("body #orderDetailBox").css({"right": "0%","top":"0", "visibility": "visible","position":"fixed"});
                $(".slideClose").off('click');
                $(".slideClose").on('click', function(){
                    $("#orderDetailBox").css({"right": "-100%", "visibility": "hidden","position":"fixed"});
                    $('#userdata').show();
                });
                $(".selectpicker").selectpicker();
            }
        });
    }
    /**
function getdeliverylist($url){
 //   alert($url);
   // return false;
//$('.removeuserdetails').remove();
//$('.detailPopup').remove();

    $('#userdata').hide();
$.ajax({
type: 'GET',
url: $url,
data: '_token=<?php echo csrf_token();?>',
success: function ($data) {

$('body').prepend($data);
$("body #orderDetailBox").css({"right": "0%","top":"0", "visibility": "visible","position":"fixed"});
$(".slideClose").off('click');

$(".slideClose").on('click', function(){
$("#orderDetailBox").css({"right": "-100%", "visibility": "hidden","position":"fixed"});
//$('.mngorder').show();
});
$(".selectpicker").selectpicker();
}
});
}**/
</script>