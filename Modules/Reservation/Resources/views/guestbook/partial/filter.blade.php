<!-- Modal -->
<div class="modal right fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" data-backdrop="static">
<style type="text/css">
     .modal-backdrop { z-index:48!important}
    </style>
	<div class="modal-dialog" role="document">
		<div class="modal-content scroller full-content">

            <div class="modal-header gustbok">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
                <a class="close fa fa-close" data-dismiss="modal" aria-label="Close"></a>
                <!-- close icon-close gray -->
            </div>

            <div class="modal-body">
                <form name="guestfilters" action="post" id="guestfilters">
                <!--Popup Container-->
                <div class="filter__content margin-bottom-30">
                    <div class="filter">Filter</div>
                    <div class="pull-right remove-filter" id="guest_remove_all_filters"><a href="#">Remove All Filter</a></div>
                </div>

                <!--    <div class="separator"></div>-->
               <input type="hidden"  name="is_filter" value="1">
                    <div class="panel-group" id="accordion">
                    <!--   <div class="panel panel-default filter-accordion">
                          <div class="panel-heading">
                                <h4 class="panel-title collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false">Category</h4> 
                            </div>
                            <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body Category">
                                    @foreach(config('reservation.guest_category') as $cat)
                                    <label class="custom_checkbox relative">
                                        <input class="hide guest_filter_checkbox" type="checkbox" name="guest_category[{{$cat}}]" value="1">{{$cat}}
                                        <span class="control_indicator"></span>
                                    </label>
                                    @endforeach
                                    
                                    
                                </div>
                            </div>
                        </div>-->
                    <!--     <div class="separator"></div>
                  <div class="panel panel-default filter-accordion">
                       <div class="panel-heading">
                                <h4 class="panel-title collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse2">Tags
                                </h4>
                            </div>
                           <div id="collapse2" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="filter__tag hide">
                                        <ul>
                                            {{--<li><span class="tag_name">Tag2</span>
                                                <span class="text-icon"> ×</span>
                                            </li>
                                            <li><span class="tag_name">Tag2</span>
                                                <span class="text-icon"> ×</span>
                                            </li>--}}
                                        </ul>
                                </div>
                                    <div class="circle pull-right hide"><a href=""><i class="fa fa-plus" aria-hidden="true"></i></a></div>
                                    <div class="pull-left"> 
                                     <input type="text" name="tags" id="guest_tags_filter">
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="separator"></div>-->
                        @if($reservation_locations['0']['reservation_count'] > 0)
                        <div class="panel panel-default filter-accordion">
                            <div class="panel-heading">
                                <h4 class="panel-title collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse3">Reservations
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                <div class="slidecontainer">
                                    <div id="slider-range-min-reservation">
                                        <div id="min-reservation" class="ui-slider-handle"></div>
                                        <div id="max-reservation" class="ui-slider-handle"></div>
                                    </div>                                        
                                    <div class="flterslider">
                                        <input type="hidden" name="reservation_min" id="min-reservation-input" value="">
                                        <input type="hidden" name="reservation_max" id="max-reservation-input" value="">
                                        
                                        <div class="minValue"></div>
                                        <div class="maxValue"></div>
                                    </div>                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="separator"></div>
                        @endif
                        <div class="panel panel-default filter-accordion">
                            <div class="panel-heading">
                                <h4 class="panel-title collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse4">Orders
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="slidecontainer">
                                    <div id="slider-range-min-order" class="minmaxorder">
                                    <!-- <input type="text" id="min-order" readonly>
                                    <input type="text" id="max-order" readonly> -->
                                    
                                        <div id="min-order" class="ui-slider-handle"></div>
                                        <div id="max-order" class="ui-slider-handle "></div>
                                    </div>                                        
                                    <div class="flterslider">
                                        <input type="hidden" name="order_min"  id="min-order-input" value="">
                                        <input type="hidden" name="order_max"   id="max-order-input" value="">

                                        <div class="minValue"></div>
                                        <div class="maxValue"></div>
                                    </div>                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="separator"></div>
                        <div class="panel panel-default filter-accordion">
                            <div class="panel-heading">
                                <h4 class="panel-title collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse5">Average Spend
                            </div>
                            <div id="collapse5" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                <div class="slidecontainer">
                                    <div id="slider-range-min-spend"  class="minmavgspend">
                                        <div id="min-spend" class="ui-slider-handle"></div>
                                        <div id="max-spend" class="ui-slider-handle"></div>
                                    </div>                                        
                                    <div class="flterslider">
                                        <input type="hidden" name="order_averagespend_min"  id="min-spend-input" value="">
                                        <input type="hidden" name="order_averagespend_max"  id="max-spend-input" value="">

                                        <div class="minValue"></div>
                                        <div class="maxValue"></div>
                                    </div>                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="separator"></div>
                        <div class="panel panel-default filter-accordion">
                            <div class="panel-heading">
                                <h4 class="panel-title collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse6">Special Occasion
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="Special-Occasion">
                                        <div class="tbl-radio-btn">
                                            <input type="radio" id="Week" name="special_occasion_type" value="week" class="radiobutton" checked="true">
                                            <label for="Week">This Week</label>
                                        </div>
                                        <div class="tbl-radio-btn">
                                            <input type="radio" id="Month" name="special_occasion_type"  class="radiobutton" value="month">
                                            <label for="Month">This Month</label>
                                        </div>
                                    </div>
                                    <div class="Special-Occasion margin-top-20 ">
                                        <label class="custom_checkbox relative">
                                        Birthday
                                            <input class="hide guest_filter_checkbox"  type="checkbox" id="dob" name="special_occasion[dob]" value="dob">
                                            <span class="control_indicator"></span>
                                        </label>
                                        <label class="custom_checkbox relative">
                                        Anniversary
                                            <input class="hide guest_filter_checkbox"   type="checkbox"  id="doa" name="special_occasion[doa]" value="doa" >
                                            <span class="control_indicator"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="separator"></div>
                        
                       <!-- <div class="sortby">Sort by
                            <div class="selct-picker-plain">
                                <select class="selectpicker" id="guest_filter_sort" name="filter_sort">
                                    <option value="">Select</option>
                                    <option value="frequency_orders">Frequency Orders</option>
                                    @if($reservation_locations['0']['reservation_count'] > 0)
                                    <option value="frequency_reservations">Frequency Reservations</option>
                                    @endif
                                    <option value="recency_orders">Recency Orders</option>
                                    @if($reservation_locations['0']['reservation_count'] > 0)
                                    <option value="recency_reservations">Recency Reservations</option>
                                    @endif
                                </select>
                            </div>
                        </div>--->
                        <div class="filter__content">
                <!-- <div class="filter__btn">
                    <button type="submit"  class="btn btn__primary">
                        Apply Filters
                    </button>
                </div> -->
                </div>
                    </div>
                
                    
            </div>

        </div>
    </div>

</div>
<style> .ui-front { z-index: 10000!important;}</style>

<script src="{{asset('js/jquery-ui-1.12.1.js')}}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{asset('css/reservation/tags/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/reservation/tags/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('js/tags/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">


    var max_reservation = <?php echo $maxReservation; ?>;
    var slider_avg_resv = <?php echo ceil($maxReservation/8); ?>;
     var max_orders      = <?php echo $maxOrder; ?>;
    var slider_avg_orders = <?php echo ceil($maxOrder/8); ?>;
    var avg_spend       = <?php echo $avgSpend; ?>;
    var slider_avg_spend = <?php echo ceil($avgSpend/8); ?>;

    $(document).ready(function () {
        sliderRange("#slider-range-min-reservation", "#min-reservation", "#max-reservation", "#min-reservation-input", "#max-reservation-input", 0, max_reservation, 0, max_reservation, 0, slider_avg_resv);
        sliderRange("#slider-range-min-order", "#min-order", "#max-order", "#min-order-input", "#max-order-input", 0, max_orders, 0, max_orders, 0, slider_avg_orders);
        sliderRange("#slider-range-min-spend", "#min-spend", "#max-spend", "#min-spend-input", "#max-spend-input", 0, avg_spend, 0, avg_spend, 1, slider_avg_spend);
    });

    window.reset_sliders = function () {
        sliderRange("#slider-range-min-reservation", "#min-reservation", "#max-reservation", "#min-reservation-input", "#max-reservation-input", 0, max_reservation, 0, max_reservation, 0, slider_avg_resv);
        sliderRange("#slider-range-min-order", "#min-order", "#max-order", "#min-order-input", "#max-order-input", 0, max_orders, 0, max_orders, 0, slider_avg_orders);
        sliderRange("#slider-range-min-spend", "#min-spend", "#max-spend", "#min-spend-input", "#max-spend-input", 0, avg_spend, 0, avg_spend, 1, slider_avg_spend);
    }



     var availableTags=<?php echo  json_encode($tags)?>;

            $('#guest_tags_filter').tagit({
                //itemName: 'item',
               // fieldName: 'tags[]',
                //autocomplete: {delay: 0, minLength: 2},
                allowDuplicates:false,
                availableTags     :availableTags,
                beforeTagAdded: function(event, ui) {
                    $("#guest_tags_filter").find('.ui-autocomplete-input').val('');
                    if($.inArray(ui.tagLabel, availableTags)==-1) return false;
               }

            });

</script>
