<div class="guestbook__container fullWidth box-shadow margin-top-0" style="padding:10px!important;">
    <table id="myTable2" class="responsive ui cell-border hover" style="margin-bottom:10px;" width="100%">
        <thead>
        <tr id="firstTR">
            <th>Name</th>
            <th>Email</th>
	    <th>Referral Code</th>
            <th>Transactions</th>
            <th>First Activity</th>
            <th>Last Activity</th>

        </tr>
        </thead>

    </table>
</div>

<script>
    var minorder,maxorder,minspend,maxspend,doa,dob,special_occasion_type;
    minorder='';
    maxorder ='';
    minspend ='';
    maxspend ='';
    doa='';
    dob='';
    special_occasion_type ='',
    $(document).ready(function () {

    function guestbook(minorder='',maxorder='', minspend='', maxspend='',doa='',dob='',special_occasion_type='') {

            var dataTable = $('#myTable2').DataTable({
                "bDestroy": true,
                'stateSave': true,
                "processing": true,
                "serverSide": true,
                "aaSorting": [[2, "desc"]],
                "ajax": {
                    url: "/guestbook", // json datasource
                    data: {val:'post',order_min: minorder, order_max:maxorder,order_averagespend_min:minspend,order_averagespend_max:maxspend,doa:doa,dob:dob,special_occasion_type:special_occasion_type}, // Set the POST variable array and adds action: getEMP
                    type: 'post', // method , by default get
                },
                error: function () { // error handling

                },
                dom: '<"top"fiB><"bottom"trlp><"clear">',
                buttons: [
                    {
                        extend: 'colvis',
                        text: '<i class="fa fa-eye"></i>',
                        titleAttr: 'Column Visibility'
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'Excel',
                        exportOptions: {columns: ':visible'}
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF',
                        exportOptions: {columns: ':visible'}
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        titleAttr: 'Print',
                        exportOptions: {
                            columns: ':visible',
                        },
                    }
                ],
                language: {
                    search: "",
                    searchPlaceholder: "Search"
                },
                responsive: true

            });

    }
        guestbook();



    $("#slider-range-min-order, #min-order, #max-order, #min-order-input, #max-order-input").click(function(){

        minorder = $("#min-order-input").val();
     //   minorder = minorder.replace('$', '');
        maxorder = $("#max-order-input").val();
    //    maxorder =maxorder.replace('$', '');
      //  alert(maxorder);
        guestbook(minorder,maxorder,minspend,maxspend);

    });

        $("#slider-range-min-spend, #min-spend, #max-spend, #min-spend-input, #max-spend-input").click(function(){
            minspend = $("#min-spend-input").val();
     //     minspend = minspend.replace('$', '');
            maxspend = $("#max-spend-input").val();
          //  maxspend = maxspend.replace('$', '');
       console.log(minspend+'------'+maxspend);
            guestbook(minorder,maxorder,minspend,maxspend);

        });
     /*   $(".radiobutton").click(function() {
           alert();
        });*/
        $('.guest_filter_checkbox').on('change', function() {
            doa ='';
            dob='';
            special_occasion_type='';
            if($("#doa").prop("checked") == true){
              doa =$("#doa").val();
            }
             if($("#dob").prop("checked") == true){
                 dob =$("#dob").val();
            }

            special_occasion_type =    $('input[name=special_occasion_type]:checked').val();
           if(($("#doa").prop("checked") == false && $("#dob").prop("checked") == false))
           {    special_occasion_type='';
               guestbook(minorder,maxorder,minspend,maxspend,doa,dob,special_occasion_type);
           }
           else{
               guestbook(minorder,maxorder,minspend,maxspend,doa,dob,special_occasion_type);
           }


        });
    });

</script>
