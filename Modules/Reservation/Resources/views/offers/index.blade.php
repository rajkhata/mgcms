@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link href="{{asset('reservation/css/dashboard.css')}}" rel='stylesheet' />

    <div class="main__container container__custom_working">


        <div class="row">
            <!--Header-->
            <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
            <div class="reservation__atto flex-direction-row">
                <div class="reservation__title margin-top-5">
                    <h1>General Info</h1>
                </div>
                <div class="guestbook__sec2">
                    <div class="margin-left-10">
                        <a href="javascript:void(0)" class="btn btn__primary font-weight-600 export_guest width-120 box-shadow">Save</a>
                        <a href="#" class="btn btn__white width-120 margin-left-10 box-shadow">Discard</a>
                    </div>

                </div>
            </div>

            <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                <div class="col-xs-12 text-center text-uppercase font-weight-700">Basic Information</div>
                <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-bottom-10">
                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="restaurantName" type="text" name="restaurant_name" autocomplete="off">
                            <label for="restaurantName">Restaurant Name</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="restaurantDes" type="text" name="restaurant_des" autocomplete="off">
                            <label for="restaurantDes">Description or Tagline</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="restaurantType" type="text" name="restaurant_type" autocomplete="off">
                            <label for="restaurantType">Type of Restaurant(Cuisine)</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-15 pull-right">
                        <div class="font-size-13 font-weight-800">Restaurant Features</div>
                        <div class="selct-picker-plain">
                            <select class="selectpicker" data-style="no-background-with-buttonline no-padding-left margin-top-5 no-padding-top font-weight-700">
                                <option value="0" selected>Super Admin</option>
                                <option value="1">Admin</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                <div class="col-xs-12 text-center text-uppercase font-weight-700">Contact Information</div>
                <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-bottom-10">
                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="title" type="text" name="title" autocomplete="off">
                            <label for="title">Title</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="name" type="text" name="name" autocomplete="off">
                            <label for="name">Name</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="businessAddress" type="text" name="business_address" autocomplete="off">
                            <label for="businessAddress">Business Address</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="address2" type="text" name="address2" autocomplete="off">
                            <label for="address2">Address 2</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="street" type="text" name="street" autocomplete="off">
                            <label for="street">Street</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="country" type="text" name="country" autocomplete="off">
                            <label for="country">Country</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="city" type="text" name="city" autocomplete="off">
                            <label for="city">City</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="zipCode" type="text" name="zip_code" autocomplete="off">
                            <label for="zipCode">Zip Code</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="contactPerson" type="text" name="contact_person" autocomplete="off">
                            <label for="contactPerson">Contact Person</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="businessPhone" type="text" name="business_phone" autocomplete="off">
                            <label for="businessPhone">Business Phone</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="phone" type="text" name="phone" autocomplete="off">
                            <label for="phone">Phone</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="email" type="text" name="email" autocomplete="off">
                            <label for="email">Email</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row white-box box-shadow padding-top-20 padding-bottom-20 margin-top-30">
                <div class="col-xs-12 text-center text-uppercase font-weight-700">Locations</div>
                <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 margin-top-10">
                    <div class="selct-picker-plain selectpicker-auto-width auto-width margin-left-15">
                        <select class="selectpicker" data-width="auto" data-style="no-background no-padding-left margin-top-5 no-padding-top font-weight-700">
                            <option value="0" selected>My Restaurant 2</option>
                            <option value="1">My Restaurant 3</option>
                        </select>
                        <span class="font-weight-700">Locations</span>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-1 margin-top-10">
                    <div class="tbl-radio-btn margin-left-15">
                        <input type="radio" id="Week" name="special_occasion_type" value="week" />
                        <label for="Week">Default Location</label>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-1 margin-bottom-10">
                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="title" type="text" name="title" autocomplete="off">
                            <label for="title">Location Name</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="name" type="text" name="name" autocomplete="off">
                            <label for="name">Location Address</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="locationAddress2" type="text" name="location_address2" autocomplete="off">
                            <label for="locationAddress2">Address 2</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="locationStreet" type="text" name="location_street" autocomplete="off">
                            <label for="locationStreet">Street</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="locationCity" type="text" name="location_city" autocomplete="off">
                            <label for="locationCity">City</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="locationCountry" type="text" name="location_country" autocomplete="off">
                            <label for="locationCountry">Country</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="locationContactPerson" type="text" name="location_contact_person" autocomplete="off">
                            <label for="locationContactPerson">Contact Person</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="locationZipCode" type="text" name="location_zip_code" autocomplete="off">
                            <label for="locationZipCode">Zip Code</label>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 margin-top-30">
                        <div class="input__field">
                            <input id="locationPhone" type="text" name="location_phone" autocomplete="off">
                            <label for="locationPhone">Phone</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 margin-top-30 pull-right">
                        <div class="input__field">
                            <input id="locationEmail" type="text" name="location_email" autocomplete="off">
                            <label for="locationEmail">Email</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row margin-top-30">
            <div class="menuItem__db__stepper">

                <ul class="stepperContainer flex-box overflow-visible flex-wrap">

                    <li class="step active flex-box flex-justify-center white-box box-shadow">
                        <div class="step__wrapper flex-justify-space-between">
                            <div class="stepContainer">
                                <i class="icon-tick-mark" aria-hidden="true"></i>
                            </div>
                            <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                <p>
                                    <span class="icon-add-general-Info"></span>
                                    Add General Info<br/>About Your Business
                                </p>
                                <a href="#" class="btn btn__holo width-120">Add Info</a>
                            </div>
                        </div>
                    </li>

                    <li class="step active flex-box flex-justify-center white-box box-shadow">
                        <div class="step__wrapper flex-justify-space-between">
                            <div class="stepContainer">
                                <i class="icon-tick-mark" aria-hidden="true"></i>
                            </div>
                            <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                <p>
                                    <span class="icon-edit-website"></span>
                                    Edit Website Design
                                </p>
                                <a href="#" class="btn btn__holo width-120">Edit</a>
                            </div>
                        </div>
                    </li>

                    <li class="step flex-box flex-justify-center white-box box-shadow">
                        <div class="step__wrapper flex-justify-space-between">
                            <div class="stepContainer">
                                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                            </div>
                            <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                <p>
                                    <span class="icon-edit-website-content"></span>
                                    Edit Website<br/>Content Page
                                </p>
                                <a href="#" class="btn btn__primary width-120">Edit</a>
                            </div>
                        </div>
                    </li>

                    <li class="step active flex-box flex-justify-center white-box box-shadow">
                        <div class="step__wrapper flex-justify-space-between">
                            <div class="stepContainer">
                                <i class="icon-tick-mark" aria-hidden="true"></i>
                            </div>
                            <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                <p>
                                    <span class="icon-create-menu"></span>
                                    Create Your Menu
                                </p>
                                <a href="#" class="btn btn__holo width-120">Create Menu</a>
                            </div>
                        </div>
                    </li>

                    <li class="step flex-box flex-justify-center white-box box-shadow">
                        <div class="step__wrapper flex-justify-space-between">
                            <div class="stepContainer">
                                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                            </div>
                            <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                <p>
                                    <span class="icon-setup-online-order"></span>
                                    Set Up Online Ordering
                                </p>
                                <a href="#" class="btn btn__primary width-120">Set Up</a>
                            </div>
                        </div>
                    </li>

                    <li class="step flex-box flex-justify-center white-box box-shadow">
                        <div class="step__wrapper flex-justify-space-between">
                            <div class="stepContainer">
                                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                            </div>
                            <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                <p>
                                    <span class="icon-link-your-social-account"></span>
                                    line Your<br/>Social Accounts
                                </p>
                                <a href="#" class="btn btn__primary width-120">Link</a>
                            </div>
                        </div>
                    </li>

                    <li class="step active flex-box flex-justify-center white-box box-shadow">
                        <div class="step__wrapper flex-justify-space-between">
                            <div class="stepContainer">
                                <i class="icon-tick-mark" aria-hidden="true"></i>
                            </div>
                            <div class="stepText flex-box flex-justify-space-between flex-direction-column">
                                <p>
                                    <span class="icon-connect-to-your-domain"></span>
                                    Connect To<br/>Your Domain
                                </p>
                                <a href="#" class="btn btn__holo width-120">Connect</a>
                            </div>
                        </div>
                    </li>{{----}}

                </ul>

            </div>
        </div>

        <div id="exTab1" class="hide">
            <ul  class="nav nav-pills globel_tabs">
                <li class="active working__tab">
                    <a href="#working_hours" data-toggle="tab">Week</a>
                </li>
                <li class="working__tab">
                    <a href="#custom_working_hours" data-toggle="tab">Custom</a>
                </li>
            </ul>

            <div class="tab-content clearfix working-hours">
                <div class="tab-pane active box-shadow" id="working_hours">
                    <ul class="nav nav-pills offer__tab">
                        <li class="active"><a data-toggle="pill" href="#mon">Mon</a></li>
                        <li><a data-toggle="pill" href="#tue">Tue</a></li>
                        <li><a data-toggle="pill" href="#web">Web</a></li>
                        <li><a data-toggle="pill" href="#thur">Thur</a></li>
                        <li><a data-toggle="pill" href="#fri">Fri</a></li>
                        <li><a data-toggle="pill" href="#sat">Sat</a></li>
                        <li><a data-toggle="pill" href="#sun">Sun</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="mon" class="tab-pane fade in in active">
                            <div style="height:300px;" class="ct-chart ct-perfect-fourth"></div>
                        </div>
                        <div id="tue" class="tab-pane fade">
                            <canvas id="myChart" width="1500" height="400"></canvas>
                        </div>
                        <div id="web" class="tab-pane fade">
                            <h3>WEB</h3>
                        </div>
                        <div id="thur" class="tab-pane fade">
                            <h3>THUR</h3>
                        </div>
                        <div id="FRI" class="tab-pane fade">
                            <h3>FRI</h3>
                        </div>
                        <div id="sat" class="tab-pane fade">
                            <h3>SAT</h3>
                        </div>
                        <div id="sun" class="tab-pane fade">
                            <h3>SUN</h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 margin-top-30 text-center offer__btn">
                    <a class="btn btn__secondary width-120">Edit</a>
                    <a class="btn btn__primary width-120">Save</a>
                </div>
                <div class="tab-pane customhrs__calendar" id="custom_working_hours">
                    <h5 class="tab-head">Create a custom rule for a specific day or range of days that overrides the standard setting for every week.</h5>

                    <div class="custom__setting-btn box-shadow text-align-center">
                        <div  class="btn_ btn btn__holo font-weight-600" data-toggle="modal" data-target="#customSettingModal">
                            Add a Custom Setting
                        </div>
                    </div>

                    <div id='custom_calendar_view' class="box-shadow"></div>

                </div>
            </div>
        </div>
        <input type="hidden" id="type" name="type" value="day" />
        <def>
            <linearGradient id="MyGradient">
                <stop offset="5%" stop-color="#F60" />
                <stop offset="95%" stop-color="#FF6" />
            </linearGradient>
        </def>



    </div>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <script>
        new Chartist.Line('.ct-chart', {
            labels: [1, 2, 3, 4, 5, 6, 7, 8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],
            series: [[0,5, 2, 4, 2, 0]]
        }, {
            height: 300
        });
    </script>

    <style>
        .ct-series-a .ct-area, .ct-series-a .ct-slice-donut-solid, .ct-series-a .ct-slice-pie {fill:url(#MyGradient)}
        .ct-grid {
            stroke:unset;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script>

        $('span[data-toggle="popover"]').popover();

        var bar_ctx = document.getElementById('myChart').getContext('2d');
        gradient = bar_ctx.createLinearGradient(0, 0, 0, 450);
        gradient.addColorStop(0, 'rgba(255, 0,0, 0.5)');
        gradient.addColorStop(0.5, 'rgba(255, 0, 0, 0.25)');
        gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');

        var data= {
            labels: ["02:00","04:00","06:00","08:00","10:00","12:00","14:00","16:00","18:00","20:00","22:00","00:00"],
            datasets: [{
                fillColor : gradient,
                backgroundColor: gradient,
                pointBackgroundColor: 'orange',
                borderWidth: 1,
                borderColor: '#911215',
                strokeColor : "#ff6c23",
                pointColor : "#fff",
                pointStrokeColor : "#ff6c23",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "#ff6c23",
                data: [0,50, 100, 150,81,70,60,150,54,50,0]
            }]
        };
        var options = {
            responsive: true,
            datasetStrokeWidth : 3,
            pointDotStrokeWidth : 4,
            maintainAspectRatio: true,
            animation: {
                easing: 'easeInOutQuad',
                duration: 520
            },scales: {
                xAxes: [{
                    gridLines: {
                        color: 'rgba(200, 200, 200, 0.05)',
                        lineWidth: 1
                    }
                }],
                yAxes: [{
                    gridLines: {
                        color: 'rgba(200, 200, 200, 0.08)',
                        lineWidth: 1
                    }
                }]
            },elements: {
                line: {
                    tension: 0.4
                }
            },
            legend: {
                display: false
            },
            point: {
                backgroundColor: 'white'
            },
            tooltips: {
                titleFontFamily: 'Open Sans',
                backgroundColor: 'orange',
                titleFontColor: 'white',
                tooltipFontStyle: "bold",
                caretSize: 5,
                cornerRadius: 2,
            }
        };
        var chartInstance = new Chart(bar_ctx, {
            type: 'line',
            data: data,
            options: options
        });

    </script>
@endsection