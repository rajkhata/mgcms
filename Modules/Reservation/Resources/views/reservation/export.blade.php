<div class="modal fade" id="reservation_pdf_pop" role="dialog">
    <div class="modal-dialog vertical-center">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-left">Export Reservations</h5>
            </div>
            <div class="modal-body">
                @if(Route::getCurrentRoute()->getName() !='home')
                    <div class="row margin-bottom-30">
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 text-left">
                            <input type="radio" name="1" class="export_type" value="all">
                            <label>Full Report</label>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 text-right">
                            <input type="radio" name="1" class="export_type" value="filtered" checked="checked">
                            <label>Filtered Report</label>
                        </div>
                    </div>
                @endif
                <div class="row text-left edit-res-pop">
                    <div class="col-lg-6 col-xs-6 edit-label">Select From Date :</div>
                    <div class="col-lg-6 col-xs-6 text-right">
                        <div class="form-group1 pull-right">
                            <div class='input-group date'>
                                <input type="text" class="full-width down-btn datepickerStart text-right datepicker_style btn btn__holo" id="datepickerStart" readonly='true' value="">
                                <i class="fa fa-calendar position" aria-hidden="true"></i>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-xs-6 edit-label text-left">Select To Date :</div>
                    <div class="col-lg-6 col-xs-6 text-right">
                        <div class="form-group1 pull-right">
                            <div class='input-group date'>
                                <input type="text" class="full-width down-btn datepickerEnd text-right datepicker_style btn btn__holo" id="datepickerEnd" readonly='true' value="">
                                <i class="fa fa-calendar position" aria-hidden="true"></i>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer text-center">
                <div class="col-lg-6 col-xs-6 text-left padd-zero">
                    <a href="javascript:void(0)" data-dismiss="modal" class="btn btn__primary font-weight-600 down-btn " >Cancel</a>
                </div>
                <div class="text-right padd-zero">
                    <a href="javascript:void(0)" class="btn btn__holo font-weight-600 submit_download export_reservation" rel = "food_item-active" id="cmd"><i class="fa icon-export" aria-hidden="true"></i>Export</a>
                </div>
            </div>


        </div>

    </div>
</div>

<script>
    $('#reservation_pdf_pop').on('shown.bs.modal', function (e) {
        $("#reservation_pdf_pop #datepickerStart").datepicker({
            maxDate: 0,
            dateFormat: 'mm-dd-yy',
            onClose: function( selectedDate ) {
                $( "#reservation_pdf_pop #datepickerEnd" ).datepicker( "option", "minDate", selectedDate );
            }
        }).datepicker('setDate', '-1m');


        $("#reservation_pdf_pop #datepickerEnd").datepicker({
            maxDate: 0,
            dateFormat: 'mm-dd-yy',
            onClose: function( selectedDate ) {
                $("#reservation_pdf_pop #datepickerStart" ).datepicker( "option", "maxDate", selectedDate );
            }
        }).datepicker('setDate', 'today');
    });
    $("body").on("click", ".modal-backdrop", function(e) {
        if ($(e.target).hasClass('modal-backdrop')) {
            $('#reservation_pdf_pop').modal('hide');
        }
    });
    $('#reservation_pdf_pop').on('click','.export_reservation', function() {
        var from_date = $('#reservation_pdf_pop #datepickerStart').val();
        var to_date = $('#reservation_pdf_pop #datepickerEnd').val();
        window.open(
            '/reservation/export?from_date='+from_date+'&to_date='+to_date,
            '_blank' // <- This is what makes it open in a new window.
        );
    });
</script>




