<!-- Modal -->
<div class="modal right fade cancel-reservation" id="cancel-reservation" tabindex="-1" role="dialog" aria-labelledby="cancelReservationLabel" data-backdrop="static" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content  scroller full-content">

            <div class="modal-header">
                <a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>

            </div>

            <div class="modal-body ">
                <!--Popup Container-->
                <div class="cancelreservation__content">
                    <div class="cancelreservation--head">
                        <h3>Cancel Reservation</h3>
                        <b>Please select a reason you are rejecting this reservation</b>
                    </div>
                    <div class="cancelreservation--content">
                        <div class="tbl-radio-btn">
                            <input type="radio" id="cancellation_reason1" name="cancellation_reason" value="Customer called to cancel the reservation" checked>
                            <label for="cancellation_reason1">Customer called to cancel the reservation</label>
                        </div>
                        <div class="tbl-radio-btn">
                            <input type="radio" id="cancellation_reason2" name="cancellation_reason" value="Customer made duplicate reservation">
                            <label for="cancellation_reason2">Customer made duplicate reservation</label>
                        </div>
                        <div class="tbl-radio-btn">
                            <input type="radio" id="other" value="other" name="cancellation_reason" >
                            <label class="other" for="other">Other</label>
                            <div class="input__field fullWidth boxx hide margin-bottom-20">
                                <textarea type="text" name="cancellation_reason_other" class="autoExpand" data-min-rows='2' rows="2" placeholder="Add cancellation reason here" id="cancellation_reason_other"></textarea>
                            </div>
                        </div>
                        <p class="warning_text"></p>
                        <div class="clearfix"></div>
                        <a class="btn_ btn__primary check-availability__btn cancel_reservation fullWidth margin-top-20">
                            Confirm Cancellation
                        </a>
                        <input type="hidden" name="reservation_id">
                        <input type="hidden" name="reservation_status_new">
                        <input type="hidden" name="reservation_status">
                        <input type="hidden" name="target" value="null">
                    </div>
                </div>
            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
