<!-- Modal -->
<div class="modal right fade xx" id="reservation-details" tabindex="-1" role="dialog"
     aria-labelledby="reservation-detailsModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">

        <div class="modal-content scroller full-content">
            <div class="modal-header">
                <a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
            </div>
            <form action="/reservation/update" method="POST" id="reservation-edit" class="reservation-edit validation-msg">
                <div class="right--popup-wrapper">

                    <div class="reservation-details__header">
                        Reservation ID:<span id="resv_id_title"></span>
                    </div>
                    <!-- edit reservation details -->
                    <div class="form_field__container fullWidth">
                        <!-- form start -->
                        <div class="input__field relative pull-left">
                            <input type="text" name="fname" id="fname" readonly="readonly" class="fname_booking_detail reservation-book"  maxlength="20">
                            <span for="fullName">First Name</span>
                        </div>

                        <div class="input__field relative">
                            <input type="text" name="lname" id="lname" readonly="readonly" maxlength="20">
                            <span for="lastName">Last Name</span>
                        </div>

                        <div class="input__field relative fullWidth">
                            <input type="text" name="phone" id="mobile" readonly="readonly" maxlength="10">
                            <span for="number">Mobile</span>
                        </div>

                        <div class="input__field fullWidth relative">
                            <input type="text" name="email" id="email" readonly="readonly">
                            <span for="email">Email</span>
                        </div>
                        <!--- form end -->
                        <!-- check box -->
                        <div class="register_customer">
                            <label class="custom_checkbox relative">
                                <input class="hide" type="checkbox" name="register_guest" id="register_guest" value="1">Register Guest
                                <span class="control_indicator"></span>
                            </label>
                        </div>

                        <div class="clearfix"></div>
                        <!-- hide reservation details -->
                        <div class="text-center add_view1" style="display: none;">
                            <a href="#" id="add-view-guest1" class="add_view_check">Add / View Guest Details</a>
                        </div>

                        <div class="add-view-guest-list1">

                            <ul class="nav nav-tabs add-view-gues-tabs">
                                <li class="active"><a href="#person1" data-toggle="tab"><i class="icon-user"
                                                                                           aria-hidden="true"></i></a>
                                </li>
                                <li><a href="#clock_detail" data-toggle="tab" id="reservation_history"><i
                                                class="icon-anti_clockwisetimer" aria-hidden="true"></i></a></li>
                            </ul>
                            <!-- tab content start -->
                            <div class="tab-content">
                                <!-- person tab -->
                                <div id="person1" class="tab-pane fade in active">
                                    <div class="two-in-one-row reservation-details">
                                        <div class="row input__fullWidth">
                                            <label for="">Orders</label>
                                            <div class="input__field number">
                                                <input type="number" name="" id="total_orders" placeholder="0"
 value="0" readonly/>
                                            </div>
                                        </div>

                                        <div class="row input__fullWidth">
                                            <label for="">Reservations</label>
                                            <div class="input__field number">
                                                <input type="number" name="" id="total_resv_count" placeholder="0" value="0" readonly>
                                            </div>
                                        </div>

                                        <div class="row input__fullWidth">
                                            <label for="">Walk-ins</label>
                                            <div class="input__field number">
                                                <input type="number" name="" id="total_walk_in_count" placeholder="0" value="0" readonly>
                                            </div>
                                        </div>

                                        <div class="row input__fullWidth">
                                            <label for="">Cancellations</label>
                                            <div class="input__field number">
                                                <input type="number" name="" id="total_cancelled_count" placeholder="0"
                                                       value="0" readonly>
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>

                                        <div class="row input__fullWidth fullWidth">

                                            <label for="">Tag</label>
                                            <div class="tag-plus icon pointer-none">
                                                <div class="tags_div" id="reservation_tags_div"></div>

                                                <ul id="guest_tags_bookings"></ul>
                                                <input type="text" name="tags" value="" id="guest_tags_booking">

                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="input__field fullWidth bookatablenote guest_note relative">
                                            <!--<input type="text" name="guest_note" id="guest_note">-->
                                            <div class="fullWidth font-weight-800">Guest Notes</div>
                                            <textarea class="edit-field" wrap="hard" name="guest_note" rows="2" maxlength="300" id="guest_note" style="min-height:20px;"></textarea>
                                        </div>
                                         <div class="clearfix"></div>
                                        <div class="row input__fullWidth fullWidth special">
                                            <span class="spcl_dates" for="">Special Dates</span>
                                            <label for="">Birthday</label>
                                            <div class="input__field ">
                                                <input type="text" class="text-center dob" id="detail_dob" name="dob"
                                                       readonly="readonly" placeholder="">
                                                <div class="fa fa-calendar" aria-hidden="true"></div>
                                            </div>
                                        </div>

                                        <div class="row input__fullWidth fullWidth special">
                                            <span class="spcl_dates" for="">Special Dates</span>
                                            <label for="">Anniversary</label>
                                            <div class="input__field ">
                                                <input type="text" class="text-center doa" id="detail_doa" name="doa"
                                                       readonly="readonly" placeholder="">
                                                <div class="fa fa-calendar" aria-hidden="true"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- clock tab-->
                                <!--<div id="clock1" class="tab-pane fade">
                                   
                                </div>-->
                                <div id="clock_detail" class="tab-pane fade">

                                    <ul class="clock--tab oldreservations_bookinform">
                                        No record found.
                                    </ul>
                                </div>

                            </div>
                            <!-- tab content end -->
                        </div>

                    </div>


                    <div class="border-sepration fullWidth"></div>

                    <div class="b_check_availability_change fullWidth hide">
                        <div class="row">
                            <div class="reservation-text b_reservation_text fullWidth"></div>
                        </div>
                        <div class="row input__fullWidth near_by_slots edit-input">
                            <ul class="time--buttons">
                            </ul>
                        </div>
                        <div class="change-Date text-center b_change_date"><a href="javascript:void(0)"><span class="icon-calendar text-color" aria-hidden="true"></span>&nbsp;&nbsp;Change Date</a></div>
                    </div>

                    <div class="border-sepration fullWidth no-padding"></div>

                    <div class="form_field__container fullWidth">
                        <div class="row">
                            <label class="errors message_div fullWidth text-center position-static margin-top-30"
                                   style="display:none"></label>
                        </div>
                        <div class="row b_reservation_data">
                            <div class="row input__fullWidth">
                                <label for="">Date</label>
                                <div class="pointer-none reserve">
                                    <input id="reservationdeatails-datepicker"
                                           class="input__date start__date bore-none reservation-book text-center block"
                                           type="text" name="reservation_date" readonly="readonly" required="">
                                    <div class="fa fa-calendar" aria-hidden="true"></div>
                                </div>
                            </div>
                            <!--Time-->
                            <div class=" row input__fullWidth">
                                <label class="">
                                    Time
                                </label>
                                <div class="pointer-none reserve pull-right auto-width xx">
                                    <input id="reservationdeatails-timepicker" type="text"
                                           class="edit__field text-right padding-right-30 reservationdeatailstimepicker reservation-book text-uppercase no-padding"  onkeypress="return false;"
                                           value="" name="reservation_time" onfocus="this.blur()" readonly/>
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </div>
                            </div>
                            <!--Party Sizer-->
                            <div class=" row input__fullWidth">
                                <label>Party Size</label>
                                <div class="edit__field no-padding-top">
                                    <div class="number__box">
                                        <div class="input-group">
                                            <span class="input-group-btn hide">
                                                <button data-type="minus" data-field="party_size" type="button"
                                                        class="btn btn-danger minus-btn">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                            <input type="text" name="party_size" id="party_size"
                                                   class="form-control party_size reservation-book 11" diff="1" min="1" max="30" value="1"
                                                   readonly="">
                                            <span class="input-group-btn hide">
                                                <button type="button" class="btn btn-success plus-btn no-padding-right" data-type="plus"
                                                        data-field="party_size">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Floor-->
                        <div class="row input__fullWidth">
                            <label for="">Floor</label>
                            <div id="currentFloorName" class="selct-picker-plain pointer-none">
                                <select class="selectpicker reservation-book" data-size="5" data-dropdown-align-right="auto" name="floor_id" id="floors">
                                    <option value="">Select Floor</option>
                                </select>
                            </div>
                        </div>
                        <!-- TABLE-->
                        <div class=" row input__fullWidth">
                            <label>Table</label>
                            <div id="multipleTableSelectpicker" class="selct-picker-plain pointer-none">
                                <select class="selectpicker reservation-book" data-size="5" name="table_id[]" id="tables" required multiple>
                                    <option value="">Select Table</option>
                                </select>
                            </div>
                        </div>
                        <!-- payment -->
                        <div class="row input__fullWidth font-size-13">
                            <div class="font-weight-800 margin-bottom-10">Payment</div>
                            <div class="font-weight-600 payment_status padding-left-10">Refunded</div>
                        </div>
                        <!--Special Occasion-->
                        <div class=" row input__fullWidth">
                            <label>Special Occasion</label>
                            <div id="multipleTableSelectpicker" class="selct-picker-plain pointer-none">
                                <select class="selectpicker reservation-book" data-dropdown-align-right="auto" name="special_occasion"
                                        id="special_occasion">
                                    <option value="">Select</option>
                                    <?php foreach (config('reservation.special_occasion') as $special_occasion_slug => $special_occasion) { ?>
                                        <option value="<?php echo $special_occasion_slug; ?>"><?php echo $special_occasion; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!--Special Instruction-->
                        <div class=" row input__fullWidth pointer-none reserve">
                                <span class="input__field fullWidth no-margin-top">
                                    <input type="text" name="special_instruction" id="special_instruction" class="reservation-book">
                                    <span for="text">Special Instruction</span>
                                </span>
                        </div>
                        <!--Host-->
                        <div class=" row input__fullWidth">
                            <label>Host</label>
                            <div id="multipleTableSelectpicker" class="selct-picker-plain pointer-none">
                                <select class="selectpicker reservation-book" data-dropdown-align-right="auto" name="host_name" id="host" @if(app('Modules\Reservation\Http\Controllers\GlobalSettingsController')->isHostNameRequired()) {{"required"}} @endif>
                                    <option value="">Select</option>
                                    <?php use App\Models\CmsUser;

                                    $hosts = CmsUser::where('restaurant_id', \Auth::user()->restaurant_id)->pluck('name', 'id');
                                    foreach ($hosts as $host_id => $host) { ?>
                                        <option value="<?php echo $host_id; ?>"><?php echo $host; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-sepration fullWidth"></div>
                <!-- status dropupdown -->

                <div class="form_field__container fullWidth margin-top-20 margin-bottom-0 statusbar status-dropdown">
                </div>
                <div class="cancellation_msg form_field__container fullWidth">
                    <p></p>
                    <p></p>
                </div>
                <!--- modify button -->
                <div class="form_field__container rem-btn-border">
                    <button type="button"
                            class="btn_ btn__primary check-availability__btn fullWidth modify-reservation_btn">
                        Modify Reservation
                    </button>
                    <button type="button" id="resv_detail_check_availability"
                            class="btn_ btn__primary check-availability__btn fullWidth check hide">
                        Check Availability
                    </button>
                    <button type="button" class="btn_ btn__primary check-availability__btn fullWidth save hide">
                        Save
                    </button>
                    <button type="button" class="btn_ btn__holo check-availability__btn fullWidth cancle hide">
                        Cancel
                    </button>
                </div>
                <input type="hidden" name="reservation_id" id="reservation_id" class="reservation-book">
                <input type="hidden" name="user_id" id="user_id">
                <input type="hidden" name="yield_cancelled" id="yield_cancelled" value="0" class="reservation-book"/>
                <input type="hidden" name="tat" id="tat" value="0" class="reservation-book"/>
                <input type="hidden" name="slot_range" id="slot_range" value="0" class="reservation-book"/>
                <input type="hidden" name="can_modify_reservation" id="can_modify_reservation" class="reservation-book"/>
            </form>
        </div>

    </div>
</div>
</div>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  
 
