{{--<div class="booking_container tab-content clearfix box-shadow no-margin-top">
    <div class="booking_content tab-pane active">--}}
{{--<div class="row row__reservation-head row__reservation-archive flex-box all-reservation">
    <div class="col-sm-5 col-lg-5 no-padding-left">
        <div class="col-sm-6 col-md-4 show-tablet-50 table-title no-padding-left light-grey-color">
            Name
        </div>
        <div class="col-sm-6 col-md-5 show-tablet-50 no-padding-left table-title light-grey-color">
            Email
        </div>
        <div class="col-sm-2 table-title no-padding-left light-grey-color hidden-sm hidden-tablet">
            Phone
        </div>
    </div>
    <div class="col-sm-2 table-title no-padding-left">
        Date
    </div>
    <div class="col-sm-1 table-title no-padding-left">
        Time
    </div>
    <div class="col-sm-1 no-padding table-title text-center">
        Party Size
    </div>
    <div class="col-sm-1 table-title text-center">
        Status
    </div>
    <div class="col-sm-1 table-title text-center">
        Source
    </div>
    <div class="col-sm-2 col-lg-1 table-title no-padding-right">
        &nbsp;
    </div>
</div>--}}

<div class="guestbook__container fullWidth box-shadow order-listing">
    <div class="guestbook__table-header fullWidth">
        <div class="col-xs-4 col-sm-5 col-lg-5 no-padding-left">
            <div class="col-xs-6 col-sm-6 col-md-4 show-tablet-50 no-padding-left">
                Name
            </div>
            <div class="col-xs-6 col-sm-6 col-md-4 show-tablet-50 no-padding-left">
                Email
            </div>
            <div class="col-xs-7 col-sm-2 col-md-4 no-padding-left hidden-sm hidden-tablet">
                Phone
            </div>
        </div>
        <div class="col-xs-2 col-sm-1 col-lg-1 no-padding">Date</div>
        <div class="col-xs-2 col-sm-2 col-lg-2 no-padding-right padding-left-25 text-center no-padding">Party Size</div>
        <div class="col-xs-1 col-sm-1 col-lg-1 text-center no-padding">Status</div>
        <div class="col-xs-1 col-sm-1 col-lg-1 text-center no-padding">Source</div>
        <div class="col-xs-1 col-sm-1 col-lg-1 text-center no-padding-left">Payment</div>
        <div class="col-xs-2 col-sm-2 col-lg-2"></div>
    </div>

    <!--Duplicate this row if new customer-->
    @foreach($reservations as $reservation)
        <div class="guestbook__cutomer-details-wrapper">
            <div class="row row__customer row__reservation manage__container guestbook__customer-details-content fullWidth {{ !empty($reservation->waiting_icon) ? 'waitlist__customer-color': '' }}">
                <div class="col-xs-4 col-sm-5 col-lg-5 no-padding-left flex-box flex-align-item-center tablet-flex-wrap  overflow-visible">
                    <div class="col-xs-6 col-sm-6 col-md-4 show-tablet-50 table-content customer_name customer__{{ !empty($reservation->waiting_icon) ? $reservation->waiting_icon: $reservation->user_category_icon  }} no-padding-left">
                        <a href='{{ $reservation->user_id ? "/guestbook/detail/$reservation->user_id" : "#" }}' target="_blank" class="fullWidth font-size-16 text-ellipsis @if(!empty($reservation->waiting_icon))text-color-grey @else @if(!empty($reservation->user_category_icon))margin-top-25 @endif @endif">{{ $reservation->full_name }}</a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 show-tablet-50 table-content no-padding-left text-ellipsis" data-toggle="tooltip" title="{{$reservation->email}}">
                        {{$reservation->email}}
                    </div>
                    <div class="col-xs-7 col-sm-2 col-md-4 table-content show-tablet-60 no-padding">
                        <i class="fa fa-phone hidden show-tablet font-size-14" aria-hidden="true"></i> {{$reservation->mobile}}
                    </div>
                </div>
                <div class="col-xs-2 col-sm-1 col-lg-1 table-content no-padding-left text-nowrap margin-top-15 tablet-margin-top-0">
                    {{ $reservation->reservation_start_date }}<br/>
                    <b class="text-color-grey font-weight-600 font-size-13">{{ $reservation->reservation_start_time }}</b>
                </div>
                <div class="col-xs-2 col-sm-2 col-lg-2 table-content no-padding-right padding-left-25 flex-verti-center">
                   <div class="customer_size {{ !empty($reservation->waiting_icon) ? 'customer_size-waitlist': '' }}"> {{$reservation->reserved_seat}}</div>
                </div>
                <div class="col-xs-1 col-sm-1 col-lg-1 table-content icon-size text-center flex-verti-center no-padding" data-toggle="tooltip" title="{{$reservation->status->name}}">
                    <span class="{{$reservation->status_icon}}" style="color:{{$reservation->status_color}}"></span>
                </div>
                <div class="col-xs-1 col-sm-1 col-lg-1 table-content icon-size text-center flex-verti-center" data-toggle="tooltip" title="{{$reservation->source}}">
                <span class="{{$reservation->source_icon}}"></span>
                </div>
                <div class="col-xs-1 col-sm-1 col-lg-1 table-content text-center no-padding-left paytment-refund">
                 {{$reservation->payment_status}}
                </div>
                <div class="col-xs-1 col-sm-1 col-lg-1 guest_btn table-content text-right no-padding">
                    <div class="btn btn__primary font-weight-600 {{$reservation->floor_id ? 'resv_detail' : (!empty($reservation->waiting_icon) ? 'resv_detail_waitlist': 'resv_detail_enquiry')}} archive" data-toggle="modal" data-target="#{{$reservation->floor_id ? 'reservation-details' : (!empty($reservation->waiting_icon) ? 'manageWaitlistModal': 'reservation-enquiry-details')}}" id="resv_{{$reservation->id}}">
                      View
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @if($reservations->count()==0)
        <div class="text-center col-sm-12 margin-top-bottom-40" >
            <strong>No Record Found</strong>
        </div>
    @endif

</div>
{{--    </div>
</div>--}}
<div class="pagination__wrapper">
{{--{{ $reservations->links() }}--}}
{{ $reservations->links('pagination.default') }}
</div>
