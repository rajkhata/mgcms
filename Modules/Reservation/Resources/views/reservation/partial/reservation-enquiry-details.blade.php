<div class="modal right fade" id="reservation-enquiry-details" tabindex="-1" role="dialog"
     aria-labelledby="reservation-enquiry-detailsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <div class="modal-content scroller full-content">
            <div class="modal-header">
                <a type="button" class="close icon-close gray" data-dismiss="modal" aria-label="Close"></a>
            </div>
            <div class="right--popup-wrapper">

                <div class="reservation-details__header">
                    Reservation ID: <span id="enquiry_resv_id_title"></span>
                </div>
                <!-- edit reservation details -->
                <div class="form_field__container fullWidth">
                    <!-- form start -->
                    <div class="input__field relative pull-left">
                        <input type="text" name="enquiry_fname" id="enquiry_fname" readonly="readonly"
                               class="fname_booking_detail">
                        <span for="fullName">First Name</span>
                    </div>

                    <div class="input__field relative">
                        <input type="text" name="enquiry_lname" id="enquiry_lname" readonly="readonly">
                        <span for="lastName">Last Name</span>
                    </div>

                    <div class="input__field relative fullWidth">
                        <input type="number" name="enquiry_phone" id="enquiry_mobile" readonly="readonly">
                        <span for="number">Mobile</span>
                    </div>

                    <div class="input__field fullWidth relative">
                        <input type="text" name="enquiry_email" id="enquiry_email" readonly="readonly">
                        <span for="email">Email</span>
                    </div>
                </div>
                <div class="border-sepration fullWidth"></div>

                <div class="form_field__container fullWidth">

                    <div class="row">
                        <div class="row input__fullWidth">
                            <label for="">Date</label>
                            <div class="pointer-none reserve">
                                <input id="enquiry_reservation_date"
                                       class="input__date start__date bore-none text-right block padding-right-30"
                                       type="text" name="enquiry_reservation_date" readonly />
                                <div class="fa fa-calendar" aria-hidden="true"></div>
                            </div>
                        </div>
                        <!--Time-->
                        <div class=" row input__fullWidth">
                            <label class="">
                                Time
                            </label>
                            <div class="pull-right auto-width margin-bottom-10">
                                <input id="enquiry_reservation_time" type="text"
                                       class="edit__field text-right text-uppercase padding-right-30"
                                       value="" name="enquiry_reservation_time" readonly />
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </div>
                        </div>
                        <!--Party Sizer-->
                        <div class=" row input__fullWidth">
                            <label>Party Size</label>
                            <div class="edit__field no-padding">
                                <div class="number__box">
                                    <div class="input-group">
                                        <input type="text" name="enquiry_party_size" id="enquiry_party_size"
                                               class="form-control party_size text-right" value="1"
                                               readonly />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="border-sepration fullWidth"></div>

            <div class="form_field__container fullWidth">
                <!--extra field-->
                <div id="extra_field" class="margin-top-20 fullWidth margin-bottom-30"></div>
            </div>

        </div>

    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
