@extends('layouts.app')
@section('content')
    <link href="{{asset('css/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('css/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <link href="{{asset('css/scheduler.min.css')}}" rel='stylesheet' />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.timepicker.css')}}" />

    <div class="main__container container__custom reservation-listing-page">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <!--Type of Customer-->
        <div class="reservation__atto flex-align-item-start">
            <div class="reservation__title margin-top-5">
                <h1>Reservations</h1>
                <!-- <div class="values booking_count font-weight-600">Total Covers <span>{{ collect($reservations)->sum('reserved_seat') }}</span></div> -->
            </div>
            <div class="pull-right col-xs-8 col-sm-10 col-md-10 col-lg-9 top_right_btns flex-align-end no-padding-right">

                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6 pull-right margin-top-5 flex-box flex-justify-space-arround flex-direction-row no-padding reservation-all-btns">
                    <a class="btn btn__primary" href="{{url('reservation/all')}}">
                        All
                    </a>
                    <a class="btn btn__primary" href="{{url('reservation/archive')}}">
                        Archive
                    </a>
                    <a class="btn btn__primary" href="{{url('reservation/upcoming')}}">
                        Upcoming
                    </a>
                </div>

                <div class="col-xs-12 pull-right col-sm-6 col-md-6 col-lg-5 reservation__searchbar">
                    <i class="fa fa-search fa-fw" aria-hidden="true"></i>
                    <input type="text" id="reservation_search" onkeyup="" placeholder="Search by Name, Phone or Reservation ID" title="">
                </div>

            </div>

        </div>
        <!--Bookings-->
        <div class="booking">
            <div class="booking-nav">
                <div class="col-xs-4 col-sm-4 no-padding">
                    <div class="col-xs-6 col-sm-6 col-md-4 text-center no-padding">
                        <h5>Total Bookings</h5>
                        <div class="values booking_count">{{$reservations->count()}}</div>
                    </div>
                    @if(!$reservations->isEmpty() && $reservations[0]->floor_id)
                    <div class="col-xs-6 col-sm-6 col-md-4 text-center no-padding">
                        <h5>Total Covers</h5>
                        <div class="values booking_covers">{{ collect($reservations)->sum('reserved_seat') }}</div>
                    </div>
                    @endif
                    <!-- <div class="col-sm-4"></div> -->
                </div>
                <!-- Reservation Tab  Start-->
                <div class="col-xs-4 col-sm-4 text-center booking_tab nav nav-tabs reservation_listing" id="reservationTab">
                  <ul>

                    <li class="time today active">
                        <span class="icon-calendar calendar-icon" aria-hidden="true"></span> 
                        <a href="#today_reservation" data-toggle="tab" class="reservation-tab">Today</a>
                    </li>

                    <li class="time week">
                        <a href="#week_reservation" data-toggle="tab" class="reservation-tab">Week</a>
                    </li>

                    <li class="time month">
                        <a href="#month_reservation" data-toggle="tab" class="reservation-tab">Month</a>
                    </li>

                  </ul>
                </div>
                <!-- Reservation Tab  End-->

                <div class="col-xs-4 col-sm-4"></div>

            </div>

            <div class="booking_container tab-content clearfix box-shadow">
            <!--Today-->
                <div class="booking_content tab-pane active" id="today_reservation">
                    <div class="row row__reservation no-padding-left">
                        <div class="col-xs-5 col-sm-5 col-lg-7 no-padding-left">
                            <div class="col-xs-6 col-sm-6 col-md-4 show-tablet-50 table-title no-padding-left light-grey-color">
                                Name
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-5 show-tablet-50 no-padding-left table-title light-grey-color">
                                Email
                            </div>
                            <div class="col-xs-6 col-sm-2 table-title no-padding-left light-grey-color hidden-sm hidden-tablet">
                               Phone
                            </div>
                        </div>
                        <!-- <div class="col-sm-2 table-title">
                            Date
                        </div> -->
                        <div class="col-xs-1 col-sm-1 table-title light-grey-color no-padding-left">
                            Time
                        </div>
                        <div class="col-xs-2 col-sm-2 table-title party-size-header text-center light-grey-color">
                            Party Size
                        </div>
                        <div class="col-xs-1 col-sm-1 table-title no-padding text-center light-grey-color">
                            Status
                        </div>
                        <div class="col-xs-1 col-sm-1 table-title text-center light-grey-color">
                            Source
                        </div>
                        <div class="col-xs-1 col-sm-1 col-lg-1 table-title light-grey-color">
                            Payment
                        </div>
                        <div class="col-xs-1 col-sm-1 col-lg-1 table-title">

                        </div>
                    </div>
                    <div class="today_reservation">
                        <!--Duplicate this row if new customer-->
                        @foreach($reservations as $reservation)
                            <div class="row row__reservation row__customer flex-align-item-start {{ !empty($reservation->waiting_icon) ? 'waitlist__customer-color': '' }}">
                                <div class="col-xs-5 col-sm-5 col-lg-7 no-padding-left flex-box flex-align-item-start tablet-flex-wrap overflow-visible">
                                    <div class="col-xs-6 col-sm-6 col-md-4 show-tablet-50 table-content {{$reservation->user_id ? 'customer__name' : ''}}  customer__{{ !empty($reservation->waiting_icon) ? $reservation->waiting_icon: $reservation->user_category_icon  }} no-padding-left">
                                        <?php if($reservation->user_id) { ?>
                                            <a href='{{ $reservation->user_id ? "/guestbook/detail/$reservation->user_id" : "#" }}' class="text-ellipsis fullWidth {{ !empty($reservation->waiting_icon) ? 'text-color-grey': '' }}">{{ $reservation->full_name }}</a>
                                        <?php } else { ?>
                                            <div class="text-ellipsis">{{ $reservation->full_name }}</div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-5 show-tablet-50 table-content no-padding-left text-ellipsis" data-toggle="tooltip" title="{{$reservation->email}}">{{$reservation->email}}</div>
                                    <div class="col-xs-6 col-sm-2 show-tablet-50 table-content no-padding"><i class="fa fa-phone font-size-13 hidden show-tablet" aria-hidden="true"></i> {{$reservation->mobile}}</div>
                                </div>
                                <!-- <div class="col-sm-2 table-content">Sep 26</div> -->
                                <div class="col-xs-1 col-sm-1 table-content customer_time no-padding text-nowrap">{{ $reservation->reservation_start_time }}</div>
                                <div class="col-xs-2 col-sm-2 table-content no-padding">
                                    <div class="customer_size {{ !empty($reservation->waiting_icon) ? 'customer_size-waitlist': '' }}">
                                        {{$reservation->reserved_seat}}
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 table-content icon-size no-padding-left no-padding text-center flex-verti-center" data-toggle="tooltip" title="{{$reservation->status->name}}">
                                    <span class="{{$reservation->status_icon}}" style="color: {{$reservation->status_color}};"></span>
                                </div>
                                <div class="col-xs-1 col-sm-1 table-content text-center flex-verti-center" data-toggle="tooltip" title="{{$reservation->source}}">
                                    <i class="{{$reservation->source_icon}}" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-lg-1 table-content no-padding-right paytment-refund">
                                    {{$reservation->payment_status}}
                                </div>
                                <div class="col-xs-1 col-sm-1 col-lg-1 table-content text-right guest_btn no-padding-right">
                                    <div class="btn btn_res btn__primary font-weight-600 {{$reservation->floor_id ? 'resv_detail' : (!empty($reservation->waiting_icon) ? 'resv_detail_waitlist': 'resv_detail_enquiry')}} today_resv" data-toggle="modal" data-target="#{{$reservation->floor_id ? 'reservation-details' : (!empty($reservation->waiting_icon) ? 'manageWaitlistModal': 'reservation-enquiry-details')}}" id="resv_{{$reservation->id}}">
                                        View
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center col-sm-12 margin-top-bottom-40 no_record" style="display: {{$reservations->count()==0 ? '' : 'none'}}">
                        <strong>No Record Found</strong>
                    </div>
                </div>
                <div class="tab-pane" id="week_reservation" style="width: 100%;">
                  <div class="week_reservation_calendar-container">
                    <div id='week_reservation_calendar'></div>
                  </div>
                </div>
                <div class="tab-pane" id="month_reservation" style="width: 100%;">
                  <div class="month_reservation_calendar-container">
                    <div id='month_reservation_calendar'></div>
                  </div>
                </div>
            </div>

        </div>
        <?php
        $datetime = \App\Helpers\CommonFunctions::getRestaurantCurrentLocalTime(\Illuminate\Support\Facades\Auth::user()->restaurant_id, 'c');
        ?>

        <script>
            window.timeZoneType = "{{$timezone}}";
            window.nowTimeShow = "{{ $datetime }}"
        </script>
        @include('reservation::reservation.partial.reservation-details')
        @include('reservation::reservation.partial.reservation-enquiry-details')
        @include('reservation::reservation.partial.confirm-cancel')
        @include("reservation::reservation.partial.cancel-pop-up")
        @include("reservation::table-management.partial.manage-waitlist")
    </div>
    <script src="{{asset('js/schedular/moment.min.js')}}"></script>
    <script src="{{asset('js/schedular/fullcalendar.min.js')}}"></script>
    <script src="{{asset('js/reservation.js')}}"></script>
    <script src="{{asset('js/turnovertime.js')}}"></script>
    <script src="{{URL::asset('js/jquery-ui-1.12.1.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.timepicker.js')}}"></script>


    <link href="{{asset('css/reservation/tags/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/reservation/tags/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('js/tags/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>
    <style> .ui-front { z-index: 10000!important;}</style>
@endsection

