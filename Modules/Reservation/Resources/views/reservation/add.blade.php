<!DOCTYPE html>
<html lang="en">

<head>
    <title>Reserve Table</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/reservation/reserve-table.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="container-fluid header-section">
    <div class="col-xs-12 text-center">
        <img src="images/dish-icon.png" />
    </div>
</div>

<div class="reservationForm-area">

<form id="reservation">
    
<div class="container reserve-table-container">


    <div class="col-xs-12 text-center title">MAKE A RESERVATION</div>
    <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 reserve-table__wrapper">

        <div class="alert alert-danger" style="display:none"></div>
        <div class="alert alert-success" style="display:none"></div>

        <div class="reservation-flex">

            <div class="reservation-table__date position-relative">
                <div class="party_size"></div>
                <i class="fa fa-calendar" aria-hidden="true"></i>
                <input class="datepicker__tab" type="text" id="reservation_date" name="reservation_date">
                <div class="cal-drop"></div>
            </div>

            <div class="reservation-table__party-details party-sec">
<!--                <div class="reservation-table__party-title"></div>-->
                <div class="reserve-table__select-box">
                   <i class="fa fa-users" aria-hidden="true"></i>
                    <select name="party_size" id="party_size">
                        @for($i=$min_party; $i<=$max_party; $i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </div>
            </div>

            


                <div class="reservation-table__time">
<!--                    <div class="reservation-table__party-title"></div>-->
                    <div class="reserve-table__select-box">
                       <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <select name="reservation_time" id="reservation_time">
                            @foreach($slots as $slot)
                                <option value="{{$slot}}">{{$slot}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
              
        </div>
        <div class="reserve-table__btn check">
            <button type="button" class="btn btn-default btn-lg availability">Check Availability</button>
        </div>

        <div class="check-avilable pull-right hide">
        </div>
        {{csrf_field()}}
        <div class="clearfix"></div>
        <input type="hidden" name="restaurant_id" id="restaurant_id" value="<?php if(isset($_GET['restaurant_id']) && !empty($_GET['restaurant_id'])){ echo $_GET['restaurant_id'];} ?>">
        <div class="reserve-table__container" style="display: none;">
           <div class="card--layout">
            <div class="row form-group">
                <div class="col-sm-6">
                    <input type="text" name="fname" class="form-control" placeholder="First Name" >
                </div>
                <div class="col-sm-6">
                    <input type="text" name="lname" class="form-control" placeholder="Last Name" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-6">
                    <input type="text" name="phone" class="form-control" placeholder="Phone" >
                </div>
                <div class="col-sm-6">
                    <input type="text" name="email" class="form-control" placeholder="Email" >
                </div>
            </div>

            <div class="row form-group">
               <div class="col-sm-12">
                   <input type="text" name="special_instructions" class="form-control" placeholder="Special Instructions" id="special_instrutions">
               </div>
            </div>

            <div class="row">
                <div class="col-sm-12 margin10">
                    <label class="custom_checkbox">
                        <input type="checkbox" name="notify_me" value="1">
                        <span class="control_indicator"></span>
                        I want to get text update about my reservation
                    </label>
                </div>

            </div>
               <input type="hidden" name="register_customer" value="1">
               <input type="hidden" name="source" value="{{config('reservation.source.online')}}">
               {{--<div class="register_customer">
                   <label class="custom_checkbox relative">
                       <input class="hide" type="checkbox" name="register_guest" id="register_guest" value="1">Register guest
                       <span class="control_indicator"></span>
                   </label>
               </div>--}}
            </div>
            <div class="row">
                <div class="reserve-table__btn">
                    <button type="submit" class="btn btn-default btn-lg complete-reservation">Complete Reservation</button>
                    <button type="button" class="btn btn-default btn-lg waitlist" id="online_waitlist">Request Waitlist</button>
                </div>
            </div>

        </div>
    </div>
</div>
</form>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script language="javascript" type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script language="javascript" type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
<script>
    $(function() {

        $( "#reservation" ).validate({
            rules: {
                party_size: {
                    required: true,
                },
                reservation_date: {
                    required: true,
                },
                reservation_time: {
                    required: true,
                },
                fname: {
                    required: true,
                },
                lname: {
                    required: true,
                },
                phone: {
                    required: true,
                    phoneUS: true //or look at the additional-methods.js to see available phone validations
                },
                email: {
                    required: true,
                    email: true
                }
            }
        });

        $(document).ready(function() {
            $("#reservation").submit(function(e){
                //alert('submit intercepted');
                e.preventDefault(e);
            });
            $('#reservation #online_waitlist').hide();
            $('#reservation #special_instrutions').hide();
        });

        $("#reservation_date").datepicker({
            dateFormat: 'mm-dd-yy',
            minDate: 0,
            onSelect: function () {
                //$(this).trigger('change');
                //getArchives(1);
            }
        }).datepicker('setDate', 'today').attr('readonly', 'readonly');
        $('.availability').on('click', function () {
            $('#loader').removeClass('hidden');
            $('.alert-danger, .alert-success').hide();
            var formdata = $('#reservation').serialize();
            var slotHtml = '';
            $.ajax({
                type: 'POST',
                url: '/check-availability',
                data: formdata,
                dataType: 'json', //mispelled
                success: function (data) {
                    $('#loader').addClass('hidden');
                    if(data.data.available_nearby_slots){
                        $.each(data.data.available_nearby_slots, function (key, slot) {
                            slotHtml+='<label class="custom_checkbox">'+slot+'<input class="hide" type="radio" name="slot_time" value="'+slot+'"><span class="control_indicator"></span></label>';
                        });
                        $('.check-avilable').html(slotHtml).removeClass('hide');
                    }
                    if(data.message) {
                        $('.alert-success').show();
                        $('.alert-success').html(data.message);
                    }
                    $('.check-avilable').html(slotHtml).removeClass('hide');
                    $('.reserve-table__btn.check').hide();
                    $('#reservation #special_instrutions').hide();
                    $('#reservation #online_waitlist').hide();
                    $('#reservation .complete-reservation').show();
                    $('.reserve-table__container').show();
                },
                error: function (data, textStatus, errorThrown) {
                    $('#loader').addClass('hidden');
                    var err='';
                    $.each(data.responseJSON.errors, function(key, value){
                        err+='<p>'+value+'</p>';
                        if(value === 'There is no table available at the restaurant for the requested time. Please choose another time.'
                            || value === 'No tables are available at the selected timeslot.') {
                            $('#reservation #special_instrutions').show();
                            $('#reservation #online_waitlist').show();
                            $('#reservation .complete-reservation').hide();
                            $('#reservation .availability').hide();
                            $('#reservation .reserve-table__container').show();
                        }
                    });
                    $('.alert-danger').empty().html(err);
                    $('.alert-danger').show();
                }
            });
        });
        $('#reservation #online_waitlist').on('click', function() {
            // get date, time, party_size and create online waitlist
            var formdata = $('#reservation').serialize();
            console.log(formdata);
            $.ajax({
                type: 'POST',
                url: '/reservation/online-wait-list-add',
                data: formdata,
                dataType: 'json', //mispelled
                success: function (data) {
                    console.log(data);
                },
                error: function (data, textStatus, errorThrown) {
                    console.log(data);
                }
            });
        });
        $('.complete-reservation').on('click', function (event) {
            event.preventDefault();
            if ( $("#reservation").valid() ) {
                $('#loader').removeClass('hidden');
                $('.alert-danger, .alert-success').empty();
                var restaurant_id = $("#restaurant_id").val();
                var formdata = $('#reservation').serialize();
                $.ajax({
                    type: 'POST',
                    url: 'table-reservation',
                    data: formdata,
                    dataType: 'json', //mispelled
                     success: function (data) {
                        $('#loader').addClass('hidden');
                        $('.alert-success').show();
                        $('.alert-success').html(data.message);
                        setInterval(function(){
                         window.location.href = '/reserve-table?restaurant_id='+restaurant_id;
                     }, 1000);
                    },
                    error: function (data, textStatus, errorThrown) {
                        $('#loader').addClass('hidden');
                        var err='';
                        $.each(data.responseJSON.errors, function(key, value){
                            err+='<p>'+value+'</p>';
                        });
                        $('.alert-danger').empty().html(err);
                        $('.alert-danger').show();
                        setInterval(function(){ 
                            window.location.href = '/reserve-table?restaurant_id='+restaurant_id;
                        }, 1000);
                    }
                });
            }

        });

    });
</script>

<!-- Slider Script -->
<script src="{{URL::asset('js/backgroundCycleSlider.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("body").backgroundCycle({
            imageUrls: [
                'images/bg1.jpg',
                'images/bg4.jpg'
            ],
            fadeSpeed: 2000,
            duration: 4000,
            backgroundSize: SCALING_MODE_COVER
        });
    });
</script>
<!-- End Slider Script -->
<div id="loader" class="hidden">
    <span style="background-image: url({{asset('images/Rolling.gif')}})"></span>
    <img src="{{asset('images/Rolling.gif')}}" class="hidden">
</div>
</body>

</html>
