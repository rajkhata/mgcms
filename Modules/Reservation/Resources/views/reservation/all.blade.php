@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.timepicker.css')}}" />
    <div class="main__container container__custom reservation-listing-page">
        <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
        <!--Type of Customer-->
        <div class="reservation__atto flex-align-item-start">
            <div class="reservation__title margin-top-5">
                <h1>All</h1>
            </div>

            <div class="pull-right no-padding flex-box flex-wrap flex-justify-content-end padding-left-10">

                <div class="col-xs-6 col-sm-6 col-md-12 col-lg-4 reservation__searchbar all no-margin-right tablet-margin-bottom-15">
                    <i class="fa fa-search fa-fw" aria-hidden="true"></i>
                    <input type="text" id="reservation_search" onkeyup="" placeholder="Search by Name, Phone or Reservation ID" title="">
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 top_right_btns no-padding">

                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4 pull-right margin-top-5 no-padding flex-box flex-justify-space-arround">
                        <a href="javascript:void(0)" class="reset btn btn__primary fullWidth font-weight-600 no-margin-left" id="reset_filter">Reset</a>
                        <a href="{{url('reservation')}}" class="btn btn__primary fullWidth archive__back pull-right">Back</a>
                    </div>

                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-8 pull-right filter-start-end-date margin-top-10 no-padding-right">
                        <div class="col-xs-6 col-sm-6 archive__page_item no-padding-left">
                            <div class="col-xs-12 col-sm-12 flex-box no-padding-left">
                                <div class="col-xs-6 col-sm-6 no-padding font-weight-600 text-nowrap">Start Date</div>
                                <input class="datepicker__tab start_date text-right font-weight-600" title="Start Date" type="text" autocomplete="off" id="start_date_all" name="start_date_all">
                                <i class="icon-calendar"></i>
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 archive__page_item no-padding-left">
                            <div class="col-xs-12 col-sm-12 flex-box no-padding-left">
                                <div class="col-xs-6 col-sm-6 no-padding font-weight-600 text-nowrap">End Date</div>
                                <input type="text" class="datepicker__tab text-right end_date font-weight-600" title="End Date" autocomplete="off" id="end_date_all" name="end_date_all">
                                <i class="icon-calendar"></i>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            {{--<div class="pull-right col-sm-9 col-md-8 col-lg-6 top_right_btns same-width no-padding-right">
                <!-- search bar start-->
                <div class="col-sm-5 reservation__searchbar all">
                    <i class="fa fa-search fa-fw" aria-hidden="true"></i>
                    <input type="text" id="reservation_search" onkeyup="" placeholder="Search by Name and Phone number" title="">
                </div>

                <div class="col-sm-7 pull-right margin-top-5 flex-box flex-justify-space-arround flex-direction-row no-padding-right">
                    <a href="{{url('reservation')}}" class="btn btn__primary archive__back pull-right">
                        Back
                    </a>
                </div>
            </div>--}}
        </div>
        <!--Bookings-->
        <div class="booking">
            {{--<div class="booking-nav archive__page_nav">

                <div class="heading_filter">
                    <div class="table__heading" >All</div>
                    <div class="archive__page_navtab">

                        --}}{{--<div class="archive__page_item text-center">
                            Total Bookings <br> {{$reservations->total()}}
                        </div>
                        <div class="archive__page_item text-center">
                            Total Covers <br> {{ collect($reservations)->sum('reserved_seat') }}
                        </div>--}}{{--
                        <div class="archive__page_item">
                            <i class="fa fa-calendar" aria-hidden="true"></i> Select Date
                        </div>


                        <div class="archive__page_item">
                            <input class="datepicker__tab start_date" title="Start Date" type="text" autocomplete="off" id="start_date_all" name="start_date_all">
                            <label>From</label>
                        </div>

                        <div class="archive__page_item">
                            <input type="text" class="datepicker__tab end_date" title="End Date" autocomplete="off" id="end_date_all" name="end_date_all">
                            <label>To</label>
                        </div>
                        <div class="archive__page_item text-center">
                            <button type="button" class="reset btn btn__primary" id="reset_filter">Reset</button>
                        </div>
                    </div>
                </div>

            </div>--}}


            <div class="archive_reservation">
                @include('reservation::reservation.partial.reservation_render')
            </div>

        </div>
    </div>
    </div>
    @include('reservation::reservation.partial.reservation-details')
    @include('reservation::reservation.partial.reservation-enquiry-details')
    @include('reservation::reservation.partial.confirm-cancel')
    @include("reservation::reservation.partial.cancel-pop-up")
    @include("reservation::table-management.partial.manage-waitlist")
    <script src="{{URL::asset('js/schedular/moment.min.js')}}"></script>
    <script src="{{URL::asset('js/schedular/fullcalendar.min.js')}}"></script>
    <script src="{{URL::asset('js/reservation.js')}}"></script>
    <script src="{{URL::asset('js/turnovertime.js')}}"></script>
    <script src="{{URL::asset('js/jquery-ui-1.12.1.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.js')}}"></script>

    <link href="{{asset('css/reservation/tags/jquery.tagit.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/reservation/tags/tagit.ui-zendesk.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('js/tags/tag-it.js')}}" type="text/javascript" charset="utf-8"></script>
    <style> .ui-front { z-index: 10000!important;}</style>
@endsection
