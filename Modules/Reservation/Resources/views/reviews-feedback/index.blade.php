@extends('layouts.app')
@section('content')
  
<div class="main__container">
    <!--- side bar only tablet view --->
    <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <!--- page heading ----->
    <div class="reservation__atto flex-direction-row">
        <div class="reservation__title margin-top-5">
            <h1>Reviews & Feedback</h1>
        </div>
    </div>

    <!---------------------- review section ---------------->
    <div class="review_feedback">
        <div class="review_feedback__container box-shadow fullWidth">
            <!---------- tab menu --------------->
            <ul class="nav nav-pills review-tab review-tab-all">
                <li class="active"><a data-toggle="pill" onclick="openTab('#today-all')" href="#all">All</a></li>
                <li><a data-toggle="pill" onclick="openTab('#past-month-all')" href="#pastmonth">Past Month</a></li>
                <li><a data-toggle="pill" onclick="openTab('#past-week-all')" href="#pastweek">Past Week</a></li>
            </ul> 
            <!------------- tab content----------------------->
            <div class="col-sm-12 tab-content margin-bottom-30 margin-top-30">

                <!------------- all tab ---------------------->
                <div id="all" class="tab-pane fade in active">

                    <div class="col-sm-2">
                        <!---------- interbal tab menu --------------->
                        <ul class="nav nav-pills nav-stacked review-tab">
                            <li class="active"><a class="no-padding-left no-padding-right" href="javascript:void(0)" data-trigger="tab"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>All</a></li>
                            <li><a href="javascript:void(0)" class="no-padding-left no-padding-right"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>Orders</a></li>
                            <li><a href="javascript:void(0)" class="no-padding-left no-padding-right"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>Reservations</a></li>
                        </ul>
                        <!------------------------------------>
                    </div>

                    <div class="col-sm-10 content-division">
                        <div id="today-all">
                            <!---- rating section ------->
                            <div class="col-sm-3">
                                <div class="stars-rating">
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <div class="rating-count text-center">
                                    <h3>200</h3>
                                    <p>Ratings</p>
                                </div>
                                <div class="would-recommand">
                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    <span class="color-black font-weight-800">80%</span>
                                    <span>Would recommend</span>
                                </div>
                            </div>
                            <!----------- chart section ------>
                            <div class="col-sm-9 border-right no-padding-right">
                                <ul class="review-feedback-cat">
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Food</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Service</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Value</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Ambiance</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Delivery</div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                                <ul class="review-feedback-progress">
                                    <li><span>5</span></li>
                                    <li><progress class="progress--bar" value="80" max="100"></progress></li>
                                    <li>80/<span class="gray-hund">100</span></li>

                                </ul>

                                <ul class="review-feedback-progress">
                                    <li><span>4</span></li>
                                    <li><progress class="progress--bar" value="60" max="100"></progress></li>
                                    <li>60/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>3</span></li>
                                    <li><progress class="progress--bar" value="40" max="100"></progress></li>
                                    <li>40/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>2</span></li>
                                    <li><progress class="progress--bar" value="30" max="100"></progress></li>
                                    <li>30/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>1</span></li>
                                    <li><progress class="progress--bar" value="20" max="100"></progress></li>
                                    <li>20/<span class="gray-hund">100</span></li>
                                </ul>
                            </div>

                            <div class="col-xs-12 review-feedback-bott-sec no-padding-right">
                                <div class="booking-nav archive__page_nav">
                                    <div class="col-sm-3 flex-box flex-align-item-center">
                                        <div class="input__field-review-feedback">
                                            <i class="fa fa-search"></i>
                                            <input id="name" class="search__input" type="text" name="name" placeholder="Search..." value="">
                                            <label for="name"></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 flex-box flex-align-item-center overflow-visible">
                                        <div class="col-sm-8 no-padding-right">
                                            <div class="col-sm-6 archive__page_item">
                                                <i class="icon-calendar rev-feed-cal" aria-hidden="true"></i>
                                                <input class="datepicker__tab start_date no-padding text-color-white" title="Start Date" type="text" autocomplete="off" id="start_date_rev_feed" name="start_date_rev_feed" value="" readonly="readonly">
                                                <label>From</label>
                                                <div class="rev-feed-arrow margin-right-20"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
                                            </div>

                                            <div class="col-sm-6 archive__page_item">
                                                <i class="icon-calendar rev-feed-cal" aria-hidden="true"></i>
                                                <input type="text" class="datepicker__tab end_date no-padding text-color-white" title="End Date" autocomplete="off" id="end_date_rev_feed" name="end_date_rev_feed" value="" readonly="readonly">
                                                <label>To</label>
                                                <div class="rev-feed-arrow margin-right-20"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 archive__page_item rev-feed-sotby no-padding-right">

                                            <div class="selct-picker-plain">
                                                <select class="selectpicker" data-style="no-background no-padding-left no-padding-top no-padding-bottom" name="slot_name"
                                                        id="slot_name">
                                                    <option value="1" selected>Last updated date</option>
                                                    <option value="2">Negative Reviews</option>
                                                    <option value="3">Positive Reviews</option>
                                                </select>
                                            </div>
                                            <label class="" for="">Sort By</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-12 review__section scroller">
                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile2.png')}}"></span>
                                            <span class="user__name">John S. </span>
                                           </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>08:30 PM</b></span><span class='col-sm-4'>Party Size<br/><b>30 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile.png')}}"></span>
                                            <span class="user__name">Martha M.</span>
                                        </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>10:10 PM</b></span><span class='col-sm-4'>Party Size<br/><b>5 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile2.png')}}"></span>
                                            <span class="user__name">John S. </span>
                                           </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="top" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="top" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>08:30 PM</b></span><span class='col-sm-4'>Party Size<br/><b>30 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!------------------------all tab end ---------->
                <!------------- past month tab ----------------->
                <div id="pastmonth" class="tab-pane fade">
                    <div class="col-sm-2">
                        <!---------- interbal tab menu --------------->
                        <ul class="nav nav-pills nav-stacked review-tab">
                            <li class="active"><a class="no-padding-left no-padding-right" href="javascript:void(0)" data-trigger="tab"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>All</a></li>
                            <li><a href="javascript:void(0)" class="no-padding-left no-padding-right"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>Orders</a></li>
                            <li><a href="javascript:void(0)" class="no-padding-left no-padding-right"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>Reservations</a></li>
                        </ul>
                        <!------------------------------------>
                    </div>

                    <div class="col-sm-10 content-division">
                        <div id="pastmonth-all">
                            <!---- rating section ------->
                            <div class="col-sm-3">
                                <div class="stars-rating">
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <div class="rating-count text-center">
                                    <h3>20</h3>
                                    <p>Ratings</p>
                                </div>
                                <div class="would-recommand">
                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    <span class="color-black font-weight-800">20%</span>
                                    <span>Would recommend</span>
                                </div>
                            </div>
                            <!----------- chart section ------>
                            <div class="col-sm-9 border-right no-padding-right">
                                <ul class="review-feedback-cat">
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Food</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Service</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Value</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Ambiance</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Delivery</div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                                <ul class="review-feedback-progress">
                                    <li><span>5</span></li>
                                    <li><progress class="progress--bar" value="10" max="100"></progress></li>
                                    <li>10/<span class="gray-hund">100</span></li>

                                </ul>

                                <ul class="review-feedback-progress">
                                    <li><span>4</span></li>
                                    <li><progress class="progress--bar" value="30" max="100"></progress></li>
                                    <li>30/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>3</span></li>
                                    <li><progress class="progress--bar" value="25" max="100"></progress></li>
                                    <li>25/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>2</span></li>
                                    <li><progress class="progress--bar" value="10" max="100"></progress></li>
                                    <li>10/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>1</span></li>
                                    <li><progress class="progress--bar" value="20" max="100"></progress></li>
                                    <li>20/<span class="gray-hund">100</span></li>
                                </ul>
                            </div>

                            <div class="col-xs-12 review-feedback-bott-sec no-padding-right">
                                <div class="booking-nav archive__page_nav">
                                    <div class="col-sm-3 flex-box flex-align-item-center">
                                        <div class="input__field-review-feedback">
                                            <i class="fa fa-search"></i>
                                            <input id="name" class="search__input" type="text" name="name" placeholder="Search..." value="">
                                            <label for="name"></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 flex-box flex-align-item-center overflow-visible">
                                        <div class="col-sm-8 no-padding-right">
                                            <div class="col-sm-6 archive__page_item">
                                                <i class="icon-calendar rev-feed-cal" aria-hidden="true"></i>
                                                <input class="datepicker__tab start_date no-padding text-color-white" title="Start Date" type="text" autocomplete="off" id="start_date_rev_feed_past_month" name="start_date_rev_feed_past_month" value="" readonly="readonly">
                                                <label>From</label>
                                                <div class="rev-feed-arrow margin-right-20"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
                                            </div>

                                            <div class="col-sm-6 archive__page_item">
                                                <i class="icon-calendar rev-feed-cal" aria-hidden="true"></i>
                                                <input type="text" class="datepicker__tab end_date no-padding text-color-white" title="End Date" autocomplete="off" id="end_date_rev_feed_past_month" name="end_date_rev_feed_past_month" value="" readonly="readonly">
                                                <label>To</label>
                                                <div class="rev-feed-arrow margin-right-20"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 archive__page_item rev-feed-sotby no-padding-right">

                                            <div class="selct-picker-plain">
                                                <select class="selectpicker" data-style="no-background no-padding-left no-padding-top no-padding-bottom" name="slot_name"
                                                        id="slot_name">
                                                    <option value="1" selected>Last updated date</option>
                                                    <option value="2">Negative Reviews</option>
                                                    <option value="3">Positive Reviews</option>
                                                </select>
                                            </div>
                                            <label class="" for="">Sort By</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-12 review__section scroller">
                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile2.png')}}"></span>
                                            <span class="user__name">John S. </span>
                                        </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>08:30 PM</b></span><span class='col-sm-4'>Party Size<br/><b>30 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile.png')}}"></span>
                                            <span class="user__name">Martha M.</span>
                                        </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>10:10 PM</b></span><span class='col-sm-4'>Party Size<br/><b>5 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile2.png')}}"></span>
                                            <span class="user__name">John S. </span>
                                        </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="top" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="top" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>08:30 PM</b></span><span class='col-sm-4'>Party Size<br/><b>30 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-------------------past month tab end ------------->
                <!--------------- past week tab ---------------->
                <div id="pastweek" class="tab-pane fade">
                    <div class="col-sm-2">
                        <!---------- interbal tab menu --------------->
                        <ul class="nav nav-pills nav-stacked review-tab">
                            <li class="active"><a class="no-padding-left no-padding-right" href="javascript:void(0)" data-trigger="tab"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>All</a></li>
                            <li><a href="javascript:void(0)" class="no-padding-left no-padding-right"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>Orders</a></li>
                            <li><a href="javascript:void(0)" class="no-padding-left no-padding-right"><i class="icon-reservation" aria-hidden="true">&nbsp;</i>Reservations</a></li>
                        </ul>
                        <!------------------------------------>
                    </div>

                    <div class="col-sm-10 content-division">
                        <div id="pastweek-all">
                            <!---- rating section ------->
                            <div class="col-sm-3">
                                <div class="stars-rating">
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star checked" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <div class="rating-count text-center">
                                    <h3>200</h3>
                                    <p>Ratings</p>
                                </div>
                                <div class="would-recommand">
                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    <span class="color-black font-weight-800">80%</span>
                                    <span>Would recommend</span>
                                </div>
                            </div>
                            <!----------- chart section ------>
                            <div class="col-sm-9 border-right no-padding-right">
                                <ul class="review-feedback-cat">
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Food</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Service</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Value</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Ambiance</div>
                                    </li>
                                    <li>
                                        <div class="stars-rating small">
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star checked" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <div class="font-weight-600">Delivery</div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                                <ul class="review-feedback-progress">
                                    <li><span>5</span></li>
                                    <li><progress class="progress--bar" value="80" max="100"></progress></li>
                                    <li>80/<span class="gray-hund">100</span></li>

                                </ul>

                                <ul class="review-feedback-progress">
                                    <li><span>4</span></li>
                                    <li><progress class="progress--bar" value="60" max="100"></progress></li>
                                    <li>60/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>3</span></li>
                                    <li><progress class="progress--bar" value="40" max="100"></progress></li>
                                    <li>40/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>2</span></li>
                                    <li><progress class="progress--bar" value="30" max="100"></progress></li>
                                    <li>30/<span class="gray-hund">100</span></li>

                                </ul>
                                <ul class="review-feedback-progress">
                                    <li><span>1</span></li>
                                    <li><progress class="progress--bar" value="20" max="100"></progress></li>
                                    <li>20/<span class="gray-hund">100</span></li>
                                </ul>
                            </div>

                            <div class="col-xs-12 review-feedback-bott-sec no-padding-right">
                                <div class="booking-nav archive__page_nav">
                                    <div class="col-sm-3 flex-box flex-align-item-center">
                                        <div class="input__field-review-feedback">
                                            <i class="fa fa-search"></i>
                                            <input id="name" class="search__input" type="text" name="name" placeholder="Search..." value="">
                                            <label for="name"></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 flex-box flex-align-item-center">
                                        <div class="col-sm-8 no-padding-right">
                                            <div class="col-sm-6 archive__page_item">
                                                <i class="icon-calendar rev-feed-cal" aria-hidden="true"></i>
                                                <input class="datepicker__tab start_date no-padding text-color-white" title="Start Date" type="text" autocomplete="off" id="start_date_rev_feed_past_week" name="start_date_rev_feed_past_week" value="" readonly="readonly">
                                                <label>From</label>
                                                <div class="rev-feed-arrow margin-right-20"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
                                            </div>

                                            <div class="col-sm-6 archive__page_item">
                                                <i class="icon-calendar rev-feed-cal" aria-hidden="true"></i>
                                                <input type="text" class="datepicker__tab end_date no-padding text-color-white" title="End Date" autocomplete="off" id="end_date_rev_feed_past_week" name="end_date_rev_feed_past_week" value="" readonly="readonly">
                                                <label>To</label>
                                                <div class="rev-feed-arrow margin-right-20"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 archive__page_item rev-feed-sotby no-padding-right">

                                            <div class="selct-picker-plain">
                                                <select class="selectpicker" data-style="no-background no-padding-left no-padding-top no-padding-bottom" name="slot_name"
                                                        id="slot_name">
                                                    <option value="1" selected>Last updated date</option>
                                                    <option value="2">Negative Reviews</option>
                                                    <option value="3">Positive Reviews</option>
                                                </select>
                                            </div>
                                            <label class="" for="">Sort By</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-12 review__section scroller">
                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile2.png')}}"></span>
                                            <span class="user__name">John S. </span>
                                        </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>08:30 PM</b></span><span class='col-sm-4'>Party Size<br/><b>30 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile.png')}}"></span>
                                            <span class="user__name">Martha M.</span>
                                        </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="bottom" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>10:10 PM</b></span><span class='col-sm-4'>Party Size<br/><b>5 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-lg-12 rev-feed-details">
                                    <div class="col-lg-3 user--info">
                                        <div class="user--pic">
                                            <span><img src="{{asset('images/user-profile2.png')}}"></span>
                                            <span class="user__name">John S. </span>
                                        </div>
                                        <div class="review__heading">
                                            <i class="icon-reservation" aria-hidden="true">&nbsp;</i>
                                            Restaurant Reviews
                                        </div>
                                        <div class="review__title">&nbsp;5 &nbsp;&nbsp;&nbsp;&nbsp;Reservations</div>
                                        <div class="review__title">&nbsp;4 &nbsp;&nbsp;&nbsp;&nbsp;Orders</div>
                                    </div>
                                    <div class="col-lg-9 user--info-right padding-top-10">
                                        <div class="">
                                            <span class="stars-rating popover-rating" role="button" onclick="return false" data-html="true" data-trigger="hover" data-placement="top" data-content="
                                            <div class='row'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Food</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Service</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Value</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Ambiance</span></div>
                                            <div class='row margin-top-10'><span class='stars-rating col-sm-6 no-padding'><span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star checked'></span> <span class='fa fa-star'></span></span><span class='col-sm-6'>Delivery</span></div>">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                            <span>20 March 2018</span>
                                            <a href="javascript:void(0)" class="boot-pop-over" onclick="return false" data-html="true" data-trigger="hover" data-placement="top" title="<span class='row flex-box padding-bottom-10'><span class='col-sm-4'>Date<br/><b>20 March 2019</b></span><span class='col-sm-4'>Time<br/><b>08:30 PM</b></span><span class='col-sm-4'>Party Size<br/><b>30 People<b></span></span>" data-content="Celebrating Birthday. Give corner seat. Extra guest may arrive.">View Details</a>
                                        </div>
                                        <div class="">
                                            The food and service was good. Ambiance can be better. Enjoyed the dessert.
                                        </div>
                                        <div  class="user-input-post">
                                            <input type="text" placeholder="Reply..." >
                                            <button>Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--------------------- past week end ---------->

            </div>
            
        
        </div>
    </div>
    <!-------------------------------------------------------->
</div>
<link href="{{asset('css/reservation/review_feedback.css')}}" rel='stylesheet' />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
<script src="{{URL::asset('js/jquery-ui-1.12.1.js')}}"></script>
<script>
$(document).ready(function(){
                  
    //console.log($( "#start_date_rev_feed" ).datepicker());
//    $( "#start_date_rev_feed" ).datepicker();
//    $( "#end_date_rev_feed" ).datepicker();

    $( "#start_date_rev_feed" ).datepicker({
        showAnim: "slideDown",
        dateFormat: 'mm-dd-yy',
        minDate:0,
        onSelect: function(){
            $(this).trigger('change');
            $(this).addClass('addDate');
        }
    }).attr('readonly','readonly');
    $('#end_date_rev_feed').datepicker({
        showAnim: "slideDown",
        beforeShow : function(){
            $( this ).datepicker('option', {
                minDate: $('#start_date_rev_feed').val()?$('#start_date_rev_feed').val():0,
                //maxDate:0,
                dateFormat: 'mm-dd-yy',
                onSelect: function(dateText, inst){
                    $(this).trigger('change');
                    $(this).addClass('addDate');
                    //$("#start_date_upcoming").datepicker('option', 'maxDate', dateText);


                }
            });
        }
    }).attr('readonly','readonly');

    $( "#start_date_rev_feed_past_month" ).datepicker({
        showAnim: "slideDown",
        dateFormat: 'mm-dd-yy',
        minDate:0,
        onSelect: function(){
            $(this).trigger('change');
            $(this).addClass('addDate');
        }
    }).attr('readonly','readonly');
    $('#end_date_rev_feed_past_month').datepicker({
        showAnim: "slideDown",
        beforeShow : function(){
            $( this ).datepicker('option', {
                minDate: $('#start_date_rev_feed_past_month').val()?$('#start_date_rev_feed_past_month').val():0,
                //maxDate:0,
                dateFormat: 'mm-dd-yy',
                onSelect: function(dateText, inst){
                    $(this).trigger('change');
                    $(this).addClass('addDate');
                    //$("#start_date_upcoming").datepicker('option', 'maxDate', dateText);


                }
            });
        }
    }).attr('readonly','readonly');

    $( "#start_date_rev_feed_past_week" ).datepicker({
        showAnim: "slideDown",
        dateFormat: 'mm-dd-yy',
        minDate:0,
        onSelect: function(){
            $(this).trigger('change');
            $(this).addClass('addDate');
        }
    }).attr('readonly','readonly');
    $('#end_date_rev_feed_past_week').datepicker({
        showAnim: "slideDown",
        beforeShow : function(){
            $( this ).datepicker('option', {
                minDate: $('#start_date_rev_feed_past_week').val()?$('#start_date_rev_feed_past_week').val():0,
                //maxDate:0,
                dateFormat: 'mm-dd-yy',
                onSelect: function(dateText, inst){
                    $(this).trigger('change');
                    $(this).addClass('addDate');
                    //$("#start_date_upcoming").datepicker('option', 'maxDate', dateText);


                }
            });
        }
    }).attr('readonly','readonly');
    

    // Enables popover
    $(".boot-pop-over").popover().on('shown.bs.popover', function () {
       $(".popover").addClass("review-viewDetails")
    });

    $(".popover-rating").popover().on('shown.bs.popover', function () {
        $(".popover").addClass("review-rating")
    });

});
</script>

@endsection
