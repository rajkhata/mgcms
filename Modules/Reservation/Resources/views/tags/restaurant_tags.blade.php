@extends('layouts.app')
@section('content')
<?php

$restaurant_tags=(isset($restaurant_tags['tags']) && !empty($restaurant_tags['tags']))?json_decode($restaurant_tags['tags'],true):[];

$restaurant_tags_keys=[];
?>

   @if($restaurant_tags)
     @foreach($restaurant_tags as $key1=> $val1)  

      <?php 
      if (!in_array($val1, $restaurant_tags_keys)) {
            $restaurant_tags_keys[]=$val1;

         }
       ?>

   
    
    @endforeach
@endif  
<script type="text/javascript">
    var restauranttags_keys='<?php echo json_encode($restaurant_tags_keys)?>';
    var tags='<?php echo json_encode($tags->toArray())?>';

</script>
   <div class="main__container margin-lr-auto">

   <div class="col-sm-12 col-lg-8 col-lg-offset-2">
   <div class="row margin-bottom-30">
       <div class="col-sm-8 no-padding-left">
           <div class="side-bar-open hidden"><i class="fa fa-bars" aria-hidden="true"></i></div>
           <h1 class="margin-top-10">Guest Tags</h1>
       </div>
       <div class="col-sm-4 no-padding-right">
            <div class="search-icon no-margin-right custom--search ">
                <form id="search" method="get" action="" class="tags_search_filter">
                    <i class="fa fa-search"></i>
                    <input type="text" name="tag_keyword" placeholder="Search" id="tag_keyword" value="">
                    {{--<button type="button" class="sett-search-btn" id="search"></button>--}}
                </form>
            </div>
       </div>
       
   </div>
    <ul class="nav nav-pills globel_tabs" id="customtagtab">
        <li class="working__tab tags_tab_head active" type="predefined">
          <a href="#predefined_tags" data-toggle="tab" aria-expanded="true">Predefined Tags</a>
        </li>
        <li class="working__tab tags_tab_head customtag" type="custom">
          <a href="#custom_tags" data-toggle="tab" aria-expanded="false">Custom Tags</a>
        </li> 
     
      </ul>
      <div class="tab-content clearfix working-hours">
        <div class="tab-pane fade in box-shadow active" id="predefined_tags">
       <div class="row">
        <div class="col-sm-12 card--head no-padding">
            <form method="post" id="alltags">
                
          
                <div class="select-guest scroller box-shadow">
                  <div class="row  res--sett-content gust-tag-cont">
            
                     @foreach($tags as $tag)

                     <div class="update_custom_tag_form">
                      <div class="col-md-4 col-sm-6 tag_listing_box" val="{{$tag->name}}">
                        <div class="guest--tag--bar">
                        <span>{{$tag->name}}</span>
                        <div class="waitList__otp no-padding">
                        <div class="toggle__container">
                        <button type="button" id="sms_notification_button" class="btn btn-xs btn-secondary btn-toggle custom_toogle_checkbox dayoff_custom <?php if (in_array($tag->id, $restaurant_tags_keys)) { echo 'active'; } ?>" title="Open" data-toggle="button" aria-pressed="true" autocomplete="off">
                                <div class="handle"></div>
                              </button>
                        <input type="hidden" id="restauranttags" name="tags[{{$tag->id}}]" value="<?php if (in_array($tag->id, $restaurant_tags_keys)) { echo 1; }else{echo 0;} ?>">
                        </div>
                        </div>  
                        </div>
                      </div>
                     </div>
                   @endforeach
            
                </div>
                <div class="text-center savecustomtag"><button class="btn btn__primary savetags">Save</button></div>
                </div>
            </form>
        </div>
       
       </div>
          </div>
          <div class="tab-pane fade box-shadow active" id="custom_tags">
              <div class="col-sm-12 card--head no-padding">
           
                <div id="custom-tags-scroller" class="select-guest box-shadow col-sm-12">
                  <div class="row  res--sett-content gust-tag-cont custom--tag-sett">

                  @foreach($custom_tags as $tag)  
                                  <form name="update_custom_tag_form" class="update_custom_tag_form">
                    <div class="col-md-12 col-sm-12 update_custom_tag_container  tag_listing_box" val="{{$tag->name}}">
                      <div class="guest--tag--bar edit-customtag">
                        <span class="custom__tag-name">{{$tag->name}}</span>
                        <input class="edit-field hide custom_tag" name="custom_tag[]" type="text" tag_id="{{$tag->id}}" value="{{$tag->name}}"   maxlength="18">
                        <div class="waitList__otp no-padding">
                          <span class="custom__tags-edit">
                            <button type="button" class="delete-custom-tag edit_custom_tag_delete" tag_id="{{$tag->id}}">
                              <i class="fa fa fa-times" aria-hidden="true"></i>
                            </button>
                            
                            <button type="button" class="edit_custom_tag " >
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="cancel_edit_custom_tag cancel_edit_custom_tag-third hide "  >
                              <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="update_custom_tag custom-tag-third hide">
                              <i class="fa fa-check" aria-hidden="true"></i>
                            </button>
                          </span>
                        </div>  
                      </div>
                    </div>
                  </form>
                   @endforeach


                  </div>
                 
                  <div class="col-sm-12">
                  <div class="add-customtag">
                    <button type="button" class="addbtn margin-bottom-30" data-toggle="collapse" data-target="#addcustom" aria-expanded="false"> 
                   Add</button>
                    <div class="collapse" id="addcustom" aria-expanded="false">
                    <form id="addcustom_form" action="" class="addcustom_form">
                      <!-- <input type="text" maxlength="20" class=""> -->
                      <div class="col-sm-6">
                      <div class="guest--tag--bar edit-customtag">
                       
                        <input class="edit-field new_custom_tag" name="new_custom_tag" type="text"  id="" maxlength="18"  placeholder="Add Tag">
                        <div class="waitList__otp no-padding">
                          <span class="custom__tags-edit">
                            <button type="button" class="close_tags">
                              <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="add_tags">
                              <i class="fa fa-check" aria-hidden="true"></i>
                            </button>
                          </span>
                        </div>  
                      </div>
                    </div>
                      <!-- <div class="margin-top-20">
                        
                        <button type="button" class="btn btn__primary">Save</button>
                      </div> -->
                    </form>
</div>
                  </div>
                    
                  </div>
                  <!-- save button -->
                  {{--<div class="text-center savecustomtag"><a href="" class="btn btn__primary">Save1</a></div>--}}
                  </div>
                </div>
              </div>
                   
              
                </div>
            </div>
          </div>
       </div>
    
       </div> 
</div>

@endsection
