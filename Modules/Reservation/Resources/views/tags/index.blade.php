@extends('layouts.app')
@section('content')
<?php

$restaurant_tags=(isset($restaurant_tags['tags']) && !empty($restaurant_tags['tags']))?json_decode($restaurant_tags['tags'],true):[];

$restaurant_tags_keys=[];
$restaurant_tags_val=[];
?>

   @if($restaurant_tags)
    @foreach($restaurant_tags as $key=> $val)  
     @foreach($val as $key1=> $val1)  

      <?php 
      if (!in_array($key1, $restaurant_tags_keys)) {
            $restaurant_tags_keys[]=$key1;
            $restaurant_tags_val[$key1]=$val1;

         }
       ?>

     @endforeach
    
    @endforeach
@endif  

<script type="text/javascript">
    var restauranttags='<?php echo json_encode($restaurant_tags_val)?>';
    var restauranttags_keys='<?php echo json_encode($restaurant_tags_keys)?>';
    var tags='<?php echo json_encode($tags->toArray())?>';

</script>
<div class="main__container">

    <div class="row tags">
        <div class="col-lg-4 col-lg-offset-1 ">

            <div class="tab--list box-shadow">
                <div class="tag--head header__row">Pre defined Tags Inventory	</div>
                <select name="from" id="undo_redo" class="form-control tags--select alltags" size="13" multiple="multiple" required="">
                 @foreach($tags as $tag)  

                    <option <?php if (in_array($tag->id, $restaurant_tags_keys)) { echo 'disabled="disabled"    selected="selected"'; } ?> value="{{$tag->id}}" >
                    {{$tag->name}}
                    </option>
                  @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-2 text-center">
            <div class="arrow-l-r">
                <a id="tagmoveright" class="btn btn__primary" href="javascript:void(0);"> <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                <!--
                <a  id="tagmoveleft" class="btn btn__primary" href="javascript:void(0);"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
               -->
            </div>
        </div>
        <div class="col-lg-4">

            <div class="tab--list box-shadow">
                <div class="tag--head header__row">Tags avialable on system</div>
                <form name="restaurant_tags_form" id="restaurant_tags_form"> 
                <select name="restaurant_tags" id="undo_redo" class="form-control tags--select restaurant_tags" size="13"  multiple="multiple" >
                @if($restaurant_tags_val)
                     @foreach($restaurant_tags_val as $key1=> $val1)  
                         <option value="{{$key1}}" selected="selected" disabled="">{{$val1}}</option>
                    
                    @endforeach
                @endif  
                </select>
                </form>
            </div>
        </div>

    </div>

    <div class="text-center">
        <button class="btn__add savetags" type="submit">Save</button>
    </div>

</div>
@endsection
